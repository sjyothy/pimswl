//============================================================================   
     
     var alertValues="";
	 getAlerts();
     function openPopupReceivePayment()
     {
		var screen_width = 1025;
		var screen_height = 450;
		var popup_width = screen_width-180;
		var popup_height = screen_height;
		var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;		      
		var popup = window.open('receivePayment.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		popup.focus();
	} 	
	function openPopupEditPaymentSchedule()
	{
		var screen_width = 1024;
       	var screen_height = 350;
       	var popup_width = screen_width-128;
       	var popup_height = screen_height;
       	var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
       	window.open('EditPaymentSchedule.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=40,resizable=yes,scrollbars=yes,status=yes');
	
	}
	function showPersonReadOnlyPopup(personId)
	{
	     var screen_width = 1024;
           var screen_height = 470;
           var popup_width = screen_width-200;
           var popup_height = screen_height;
           var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		   window.open('AddPerson.jsf?personId='+personId+'&viewMode=popup&readOnlyMode=true','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,resizable=no,titlebar=no,dialog=yes');
		 
	}
    function openLoadReportPopup(pageName) 
	{	
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}

    function showUploadPopup()
	{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
	}
        
	function showAddNotePopup()
	{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
	}
     function openRemarksPopup()
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-750;
        var popup_height = screen_height-580;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('cancelRemarksPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,titlebar=no,dialog=yes');
        popup.focus();
	} 
       function onReceivePaymentClosed()
	{

		  document.getElementById( document.forms[0].name+":btnCollectPayment" ).disabled = false;
	}     	
   function confirmRepalceChequedaysExceed( error,errorMsg  )
   {
	    if(  error=='true' ) 
	    {
	     if( !confirm( errorMsg ) )
	      return false;
	    }
	    return true;
   }       
   function openPaymentDetailsPopUp( queryString )
	{
	
	   var screen_width   = 0.65 * screen.width ;
	   var screen_height  = screen.height-400;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   window.open('paymentDetailsPopUp.jsf?'+queryString,'_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',resizable=yes,scrollbars=yes,status=yes'
	              );
	   
        //var popup = window.open('paymentDetailsPopUp.jsf?'+queryString+"",'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,titlebar=no,dialog=yes');
        //popup.focus();
	} 
	function openBeneficiaryPopup( beneficiaryId )
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-100;
        var popup_height = 500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('ManageBeneficiary.jsf?pageMode=MODE_SELECT_ONE_POPUP&personId='+beneficiaryId+"",'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	function openFile(  )
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('inheritanceFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	function openAssociateCostCenters()
	{
	
	    var screen_width = 1024;
       	var screen_height = 350;
       	var popup_width = screen_width-128;
       	var popup_height = screen_height;
       	var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
       	window.open('associateCostCenterPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=40,resizable=yes,scrollbars=yes,status=yes');
	
	
	
	}
	function   showAttachmentPopup(paymentReceiptDetailId)
	{
	   var screen_width = 1024;
	   var screen_height = 300;
	   var screen_top = screen.top;
	   window.open('attachCheque.jsf?PAYMENT_RECEIPT_DETAIL_ID='+paymentReceiptDetailId,'_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	/**This method will remove the non numeric Character from a field
	*Please register control to this function as below
	*onkeyup="removeNonNumeric(this)"
	*onkeypress="removeNonNumeric(this)"
	*onkeydown="removeNonNumeric(this)"
	*onblur="removeNonNumeric(this)"
	*onchange="removeNonNumeric(this)"
	**/
	function removeNonNumeric(control){
	var receivedValue = control.value;
	receivedValue = receivedValue.replace(/[^0-9 .]/g, '');
	control.value = receivedValue;
}
	/**This method will remove the non numeric Character from a field
	*Please register control to this function as below
	*onkeyup="removeNonInteger(this)"
	*onkeypress="removeNonInteger(this)"
	*onkeydown="removeNonInteger(this)"
	*onblur="removeNonInteger(this)"
	*onchange="removeNonInteger(this)"
	**/
	function removeNonInteger(control){
	var receivedValue = control.value;
	receivedValue = receivedValue.replace(/[^0-9]/g, '');
	control.value = receivedValue;
}
function removeNonAlphaAndAssign(control){
	var receivedValue = control.value;
	receivedValue = receivedValue.replace(/[^A-Z a-z]/g, '');
	control.value = receivedValue;
}
function removeNonAlpha(control){
	var receivedValue = control.value;
	receivedValue = receivedValue.replace(/[^A-Z a-z]/g, '');
	return receivedValue;
}
function removeMoreThanOneNonAlpha(control){
	var receivedValue = removeNonAlpha(control);
	receivedValue = receivedValue.substr(0,1);
	control.value = receivedValue;
	}

// function will clear input elements on ever form on HTML page
function clearForms() {   

}
function isNumeric(strString)
   //  check for valid numeric strings	
   {
   var strValidChars = "0123456789.-";
   var strChar;
   var blnResult = true;

   if (strString.length == 0) return false;

   //  test strString consists of valid characters listed above
   for (i = 0; i < strString.length && blnResult == true; i++)
      {
      strChar = strString.charAt(i);
      if (strValidChars.indexOf(strChar) == -1)
         {
         blnResult = false;
         }
      }
   return blnResult;
   }
   function clearThirdPartyFields(form){
   		var formName = form.name;
		document.getElementById(formName+":unitNumber").value="";
		document.getElementById(formName+":floorNumber").value="";
		document.getElementById(formName+":description").value="";
	} 
	function removeExtraCharacter(control,maxSize){
	var controlValue = control.value;
	if(controlValue.length > maxSize){
	controlValue = controlValue.substr(0,maxSize);
	control.value = controlValue;
	}
	}
	function stringToJSONObject(jsonString){
		eval("var jsonObject = "+ jsonString);
		return jsonObject;
	}
	function   openResearchForm()
	{
	   var screen_width = 900;
	   var screen_height = 470;
	   var screen_top = screen.top;
	    window.open('ResearchFormBeneficiary.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank',
	                'width='+(screen_width)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes,resizable=yes,titlebar=no');
	    
	}
	function   openResearchFormFamilyVillage()
	{
	   var screen_width = 900;
	   var screen_height = 470;
	   var screen_top = screen.top;
	    window.open('ResearchFormBeneficiary.jsf?pageMode=MODE_SELECT_ONE_POPUP&context=familyVillage','_blank',
	                'width='+(screen_width)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes,resizable=yes,titlebar=no');
	    
	}
	 function openEditUnitPopup(unitId)
	{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/2+150;
		      var popup_height = screen_height/2+20;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('editUnitPopup.jsf?UNIT_ID='+unitId,'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
	}
	
	function openLeaseContractPopup(contractId)
	{
		      var screen_width = screen.width;
		      var screen_height = 450;
		      var popup_width = screen_width-300;
		      var popup_height = screen_height;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 +100;
		      //var windowFeatures = '_blank'+',width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes';
		      var popup = window.open('LeaseContract.jsf?contractId='+contractId+'&'+
	                                  'PAGE_MODE_VIEW=PAGE_MODE_VIEW&viewMode=POPUP','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
	
	}
	function openSplitingPopup()
	{
		var screen_width = 850;
       	var screen_height = 500;
       	var popup_width = screen_width-128;
       	var popup_height = screen_height;
       	var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
       	window.open('splitPaymentSchedule.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=40,resizable=yes,scrollbars=yes,status=yes');
	}
	function openAddPaymentSchedulePopup( txttotalContractValue, psStatusInReq, ownerShipTypeId)
	{
		var screen_width = 1024;
	    var screen_height = 450;
	    var contractValue="";
	    var psStatusToUse="";
	    if( txttotalContractValue.length>0 )
	    {
	      contractValue='&totalContractValue=' +txttotalContractValue;
	    }
	    if( psStatusInReq.length > 0 )
	    {
	      psStatusToUse = '&PAYMENT_STATUS_IN_REQUEST='+psStatusInReq;
	    }
	    window.open('PaymentSchedule.jsf?1=1'+contractValue+psStatusToUse+"&ownerShipTypeId="+ownerShipTypeId,
	                '_blank','width='+(screen_width-300)+',height='+(screen_height)+',left=200,top=200,scrollbars=yes,status=yes');
	}
	
	function openManageEndowmentPrograms(programId)
	{
	   var screen_width   = 0.75 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /4.6;
	   var screen_top     = screen.height/6;
	   window.open('endowmentPrograms.jsf?pageMode=POPUP&programId='+programId,'_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	}
	
	function   openEndowmentProgramsPopup()
	{
	   var screen_width   = 0.75 * screen.width ;
	   var screen_height  = screen.height-220;
	   var screen_left    = screen.width /6.5;
	   var screen_top     = screen.height/6;
	   window.open('endowmentProgramSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+	
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	}
	function   openSearchEndowmentsPopup(context)
	{
		
	   var screen_width   = 0.65 * screen.width ;
	   var screen_height  = screen.height-270;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   window.open('endowmentSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP&context='+context,'_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	}
	
	function   openAddGrpSuppliersPopup()
	{
		
	   var screen_width   = 0.65 * screen.width ;
	   var screen_height  = screen.height-270;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   window.open('AddGrpSuppliers.jsf','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	}
	
	function disableDisburseButton(control)
	{
			
			var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
			control.nextSibling.onclick();
			
			return 
		
	}
		
	function openAddMultipleBenefificiaryForDisbursementPopup()
	{
	
	   var screen_width   = 0.65 * screen.width ;
	   var screen_height  = screen.height-270;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   window.open(
					'addBeneficiaryPopup.jsf?viewMode=popup','_blank',
					' width=' +(screen_width) +
	                ',height='+(screen_height)+
	                ',left='  +(screen_left)  +
	                ',top='   +(screen_top)   +
				    ',status=yes,resizable=yes,scrollbars=yes'
				   );
	
	}
	
	function   openEndowmentManagePopup(endowmentId)
	{
	   var screen_width   = 0.75 * screen.width ;
	   var screen_height  = screen.height-220;
	   var screen_left    = screen.width /6.5;
	   var screen_top     = screen.height/6;
	   window.open('endowmentManage.jsf?pageMode=POPUP&endowmentId='+endowmentId,'_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+	
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	}
	
	
	function openInheritenceFileSearch( context,pageMode  )
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('InheritenceFileSearch.jsf?pageMode='+pageMode+'&context='+context,'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	function openInheritenceBeneficiarySearch( context,pageMode  )
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-460;
        var popup_height = 520;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('SearchBeneficiary.jsf?pageMode='+pageMode+'&context='+context,'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	function openBlockingDetailsPopup( personId,fileId)
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('BlockingDetailsPopup.jsf?personId='+personId+'&fileId='+fileId,'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	function openMinorMaintenancePopup( requestId)
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('minorMaintenanceRequest.jsf?requestId='+requestId,'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	function openBeneficiaryDisbursementDetailsPopup( personId,fileId,requestId)
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('beneficiaryDisbursementDetailsPopup.jsf?personId='+personId+'&fileId='+fileId+'&requestId='+requestId,'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	
	function onOpenModifyAssetPopup( pageMode )
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('modifyEstateLimitationAssetsPopup.jsf?pageMode='+pageMode ,'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	function openManageAssetPopup( assetId )
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-100;
        var popup_height = 500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open(
        						 'ManageAssets.jsf?pageMode=MODE_SELECT_ONE_POPUP&ASSET_ID='+assetId,
        						 '_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        						 ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes'
        					   );
        popup.focus();
	
	}
	
	function openAssetWiseRevenueDetailsPopup( )
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('AssetWiseRevenueDetailsPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	function openZakatPersonalAccountTransLinkPopup( zakatId )
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('ZakatPersonalAccountTransLinkPopup.jsf?zakatId='+zakatId,'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	function openZakatHistoryPopup( zakatId )
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('ZakatHistoryPopup.jsf?zakatId='+zakatId,'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	function openReleaseTasksPopup( )
	{
	
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('releaseTaskPopup.jsf?','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	function openArchivedFilesPopup( )
	{
	
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('fileNetDocumentsPopup.jsf?','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	function openPayVendorNotificationPopup( )
	{
	
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-600;
        var popup_height = 500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open(
        							'PayVendorNotificationPopup.jsf?',
        						 	'_blank',
        						 	'width='+popup_width  +
        						 	',height='+popup_height+
        						 	',left='+ leftPos +
        						 	',top=' + topPos + 
        						 	',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes'
        						 );
        popup.focus();
	
	}
	function   openManageMasrafPopup(masrafId)
	{
	   var screen_width   = 0.75 * screen.width ;
	   var screen_height  = screen.height-220;
	   var screen_left    = screen.width /6.5;
	   var screen_top     = screen.height/6;
	   window.open('manageMasarif.jsf?pageMode=READONLY&masrafId='+masrafId,'_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+	
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	   popup.focus();
	}
	
	
	function openManagePersonBeneficiaryPopup(personId)
	{
	
	   var screen_width   = 0.75 * screen.width ;
	   var screen_height  = screen.height-270;
	   var screen_left    = screen.width /6.5;
	   var screen_top     = screen.height/6;
	   window.open('EndowmentPersonBeneficiaryBean.jsf?personId='+personId,'_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+	
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	   popup.focus();
	}
	
	function openEvaluateEndowmentsPopup()
	{
	   
	   var screen_width   = 0.60 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   
	   window.open('evaluateEndowmentsPopup.jsf?','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	}
	
	function   openFinancialAspectsPopup()
	{
	   var screen_width = 900;
	   var screen_height = 470;
	   var screen_top = screen.top;
	    window.open('financialAspectsPopup.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank',
	                'width='+(screen_width)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes,resizable=yes,titlebar=no');
	    	         popup.focus();
	}
	
	function   openDistributePopUp()
	{
	
	   var screen_width = 900;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('openFileDistributePopUp.jsf','_blank','width='+(screen_width)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    	         popup.focus();
	}
	
	function   openDistributedCollectionsPopUp()
	{
	   var screen_width = 900;
	   var screen_height = 500;
	   var screen_top = screen.top;
	   
	    window.open('distributedCollectionsPopup.jsf','_blank',
	                'width='+(screen_width)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes,resizable=yes,titlebar=no');
	    	         popup.focus();
	}
	
	  function openDistributePopup( args )
	  {
	                    
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-550;
        var popup_height = screen_height-270;
        var leftPos = (screen_width-popup_width)/1.5, topPos = (screen_height-popup_height)/2;
        var win = window.open( 'distributePopUp.jsf?'+args+"",'_blank','width='+popup_width+',height='+popup_height+
                     ',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes' );
         popup.focus();
	 }
	 
	 function openStatementofAccountEndowmentCriteriaPopup()
	 {
	   
	   var screen_width   = 0.60 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   
	   window.open('statementofAccountEndowmentCriteriaPopup.jsf?','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	 } 
	 
	 function openStatementofAccountMasrafCriteriaPopup()
	 {
	   
	   var screen_width   = 0.60 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   
	   window.open('statementOfAccountMasrafCriteriaPopup.jsf?','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	 } 
	 function openRequestDisbursementDetailsForMasrafPopup( masrafId,requestId)
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('masrafRequestDisbursementDetailsPopup.jsf?masrafId='+masrafId+'&requestId='+requestId,'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	function openManageExpensePopup( requestId )
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('endowmentExpense.jsf?requestId='+requestId,'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	function openDistributeEndowmentRevenuePopup()
	{
	
	   var screen_width   = 0.65 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   window.open('distributeEndowmentRevenue.jsf','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	}

	function openComplaintDetailsPopup( complaintId )
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('complaintDetails.jsf?pageMode=POPUP&complaintId='+complaintId,'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	function openReadEIDDataPopup( context )
	{
	
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-600;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('ReadEIDData.jsf?context='+context,'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}

	function getAlerts()
	{
	  $jq.ajax(
		  			{
				      	type: 'GET',
				      	cache: false,
				      	dataType: 'text json',
						url: "../alerts/getNewOnlineRequests",
						success: function (response)
					  	{
					  	   getAlertsSuccess(response);
					  	},
					    error: function (errorCode) 
					    {
		               		getAlertsError( errorCode);
		            	},
		            	complete:function()
		            	{
		            							
		            	  setTimeout(getAlerts, (60000*7));
		            	}
			   		}
			);
		
	}		
	function onAlertsClick()
	{
			if (document.getElementById('alertDiv').className  == "ALERT_DIV_DISABLE")
			{
			  	//document.getElementById('alertDiv').className = "ALERT_DIV_ENABLE";
				getAlerts();
				 window.location="requestSearch.jsf?source=newonlinerequestcount";   	 
	   		}
	   		else
	   		{
	   			document.getElementById('alertDiv').className  = "ALERT_DIV_DISABLE";

	   		}
	}
	
	function getAlertsError(errorCode)
	{
 	//	alert("Error:"+ errorCode.response);
	}
		
	function getAlertsSuccess(response)
	{
		
		if(response.count>0)
		{
			var span = document.getElementById("alertCountBadge");
			span.className  = "ALERT_BADGE";
			while( span.firstChild ) 
			{
			    span.removeChild( span.firstChild );
			}
			span.appendChild( document.createTextNode(response.count) );
			
			
		}
	}
	
	
	function showAddRejectReasonPopup()
	{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/2;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      var popup = window.open('addHousingRejectReason.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
	}

	
	function openFamilyVillageBeneficiaryPopup( personId )
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-50;
        var popup_height = 500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        
        var popup = window.open('manageBeneficiaryFamilyVillage.jsf?pageMode=MODE_SELECT_ONE_POPUP&personId='+personId,
        									'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        									',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	function openFamilyVillageVillaSearchPopup( )
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-100;
        var popup_height = 500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('searchFamilyVillageVilla.jsf?pageMode=MODE_SELECT_ONE_POPUP',
        									'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        									',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	function openFamilyVillageAddBeneficiaryPopup(  inheritanceFileId, request)
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-200;
        var popup_height = 500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('AddFamilyVillageBeneficiary.jsf?inheritanceFileId='+inheritanceFileId+'&requestId='+request,
        									'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        									',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	function openFamilyVillageFamilyAspectSocialPopup(  personId )
	{
	     
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = 300;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('familyVillageFamilyAspectSocialPopup.jsf?personId='+personId,
        									'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        									',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	function openFamilyVillageFamilyAspectFinancialPopup(  personId )
	{
	     
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = 300;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('familyVillageFamilyAspectFinancialPopup.jsf?personId='+personId,
        									'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        									',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	function openFamilyVillageFamilyAspectPsychologyPopup(  personId )
	{
	     
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = 300;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('familyVillageFamilyAspectPsychologyPopup.jsf?personId='+personId,
        									'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        									',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	function openFamilyVillagePsychologyConditionPopup(  personId )
	{
	     
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = 300;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('familyVillagePsychologicalAspectConditionPopup.jsf?personId='+personId,
        									'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        									',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	function openFamilyVillagePsychologyTestsPopup(  personId )
	{
	     
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = 300;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('familyVillagePsychologicalAspectTestPopup.jsf?personId='+personId,
        									'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        									',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	function openFamilyVillageInterventionPlanPopup(  personId )
	{
	     
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = 300;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('familyVillageInterventionPlanPopup.jsf?personId='+personId,
        						'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        						',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	
	function openFamilyVillageProblemAspectPopup(  personId )
	{
	     
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = screen.height-300;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('familyVillageProblemAspectPopup.jsf?personId='+personId,
        						'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        						',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	}
	
	function openFamilyVillageHealthAspectChronicPopup(  personId )
	{
	     
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = screen.height-300;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('familyVillageHealthAspectChronicPopup.jsf?personId='+personId,
        						'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        						',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	}
	function openFamilyVillageHealthAspectAcutePopup(  personId )
	{
	     
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = screen.height-300;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('familyVillageHealthAspectAcutePopup.jsf?personId='+personId,
        						'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        						',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	}
	function openFamilyVillageMedicalHistoryPopup(  personId )
	{
	     
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = screen.height-300;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('familyVillageHealthAspectMedicalHistoryPopup.jsf?personId='+personId,
        						'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        						',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	}
	
	function openFamilyVillageEducationAspectPopup(  personId )
	{
	     
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = screen.height-300;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('familyVillageEducationAspectPopup.jsf?personId='+personId,
        						'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        						',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	}
	function openSearchFamilyVillageFiles(pageMode)
	{
	     
	     var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-300;
        var popup_height = 450;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open('searchFamilyVillageFiles.jsf?pageMode='+pageMode,'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	
	}
	function openFamilyVillageBehaviorAspectsGeneralPopup(  personId )
	{
	     
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = screen.height-300;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('familyVillageBehaviorAspectsGeneralPopup.jsf?personId='+personId,
        						'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        						',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	}
	function openFamilyVillageBehaviorAspectsOthersPopup(  personId )
	{
	     
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = screen.height-300;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('familyVillageBehaviorAspectsOthersPopup.jsf?personId='+personId,
        						'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        						',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	}
	
	function openSurveyForm(  personId,surveyTypeId,surveyInstanceId)
	{
	     
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = screen.height-300;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('surveyForm.jsf?personId='+personId+'&surveyTypeId='+surveyTypeId+'&surveyInstanceId='+surveyInstanceId,
        						'_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + 
        						',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes');
        popup.focus();
	}
	