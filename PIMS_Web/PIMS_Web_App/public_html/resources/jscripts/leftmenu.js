  var scrolling_images = null;
  var scrolling = false;
  var running = false;
  var threshold_sum = 0;
  var index = 0;
  var thcount=0;
  
  function selectMenuItem(id) 
  {
  	document.getElementById(id).style.backgroundColor = '#f9a63c';
  }
  
  function do_scroll_Up(id, threshold, max, imageCount) {
    if (scrolling) return;
    if (index > 0) {
        index--;
        scroll_Up(id, threshold, max, imageCount);        
    }
  }
  function scroll_Up(id, threshold, max, imageCount) {
      scrolling = true;
      if (threshold_sum >= max) {
            scrolling = false;
            threshold_sum = 0;
            scroll_Stop();
            return;
      }
      
      var d = document.getElementById(id);
      d.scrollTop = d.scrollTop - threshold;


      scrolling_images = window.setTimeout(function() {
          if (scrolling && thcount < 6) {
          	thcount++;
            threshold_sum += threshold; 
            scroll_Up(id, threshold, max, imageCount);
          } else {
          	scroll_Stop();
          }
      }, 10);
  }

  function do_scroll_Down(id, threshold, max, imageCount) {
    if (scrolling) return;
    if (index < imageCount - 1) {
    	
       index++;
       scroll_Down(id, threshold, max, imageCount);
    }
  }
  
  function scroll_Down(id, threshold, max, imageCount) {
      scrolling = true;

      if (threshold_sum >= max) {
            scrolling = false;
            threshold_sum = 0;
            scroll_Stop();
            return;
      } 
      var d = document.getElementById(id);
      d.scrollTop = d.scrollTop + threshold ;

      scrolling_images = window.setTimeout(function() {
          if (scrolling && thcount < 7) {
         	thcount++;
            threshold_sum += threshold;             
            scroll_Down(id, threshold, max, imageCount);
          } else {
          	scroll_Stop();
          }
      }, 10);
  }

  function scroll_Stop() {
  		thcount=0;
  		scrolling=false;
        window.clearTimeout(scrolling_images);
  }
  


function scrollUp(threshold, divid){
	var d = document.getElementById(divid);
	d.scrollTop = d.scrollTop - threshold ;
}

function scrollDown(threshold, divid){
	var d = document.getElementById(divid);
	d.scrollTop = d.scrollTop + threshold ;
}
 
   function showhide(thediv,totaldiv)
  {			
  var totaldiv ;
 
 			 var thediv;
			 //hide All div
			 for(i=1;i<=totaldiv;i++)
			 {
				 	 thediv_tohide="div_"+i
					 //dont not hide the which is current open for toggling purpose
						if(i!=thediv)
						{	
					     document.getElementById(thediv_tohide).style.display = "none";
					     //alert($(thediv_tohide).up().up().innerHTML);	
					      thediv_up_toshow="up_"+i;
 					      thediv_bottom_toshow="bottom_"+i;
						  thediv_down_toshow="down_"+i;
					     document.getElementById(thediv_up_toshow).style.display = "block";
						 document.getElementById(thediv_bottom_toshow).style.display = "block";
						 document.getElementById(thediv_down_toshow).style.display = "none";
						 }
			 }
			  
 			  thediv_toshow="div_"+thediv;
  			  thediv_up_toshow="up_"+thediv;
    		  thediv_down_toshow="down_"+thediv;
			  thediv_bottom_toshow="bottom_"+thediv
			   
			   //toggle
			   
			   
			   if(document.getElementById(thediv_toshow).style.display == "none")
				{
				document.getElementById(thediv_toshow).style.display = "block";
				document.getElementById(thediv_up_toshow).style.display = "none";
				document.getElementById(thediv_down_toshow).style.display = "block";
			    document.getElementById(thediv_bottom_toshow).style.display = "none";
			}
			else
			{
				document.getElementById(thediv_toshow).style.display = "none";
				document.getElementById(thediv_up_toshow).style.display = "block";
				document.getElementById(thediv_down_toshow).style.display = "none";
				document.getElementById(thediv_bottom_toshow).style.display = "block";
			}
		 
				//document.getElementById(thediv_bottom_toshow).style.display = "block";
			//	showotherbottom(thediv,totaldiv); 

			 
  
	}  
	
	




 
 
 function hidealldiv(totaldiv)
 
 {

    var totaldiv ;
 			 var thediv;
			 //hide All div
			 for(i=1;i<=totaldiv;i++)
			 {
				 	 thediv_tohide="div_"+i
							
					    document.getElementById(thediv_tohide).style.display = "none";	
					   thediv_up_toshow="up_"+i
						  thediv_down_toshow="down_"+i
					     document.getElementById(thediv_up_toshow).style.display = "block";
					    document.getElementById(thediv_down_toshow).style.display = "none";
						 
			 }
 }
	 
 
 
 
function showalldiv(totaldiv)
 
 {
  var totaldiv ;
  
 			 var thediv;
			 //hide All div
			 for(i=1;i<=totaldiv;i++)
			 {
				 	 thediv_tohide="div_"+i
							
					    document.getElementById(thediv_tohide).style.display = "block";	
					   thediv_up_toshow="up_"+i
						  thediv_down_toshow="down_"+i
					     document.getElementById(thediv_up_toshow).style.display = "none";
					    document.getElementById(thediv_down_toshow).style.display = "block";
						 
			 }
 }

var scrollStep=1; // Scroll Step is 1 and it should be otherwise it wont scroll smoothly

/** 
	Calling this functions will start scrolling the div DOWN for the given id.
	@param id - Id for the div to be scrolled DOWN
*/
function startScrollDown(id){ 
	clearTimeout(scrollTimerDown);  
	document.getElementById(id).scrollTop+=scrollStep;  
	scrollTimerDown=setTimeout("startScrollDown('"+id+"')",10); // Timeout is set to 10 for optimal smooth scrolling  
}  

/** 
	Calling this functions will start scrolling the div UP for the given id.
	@param id - Id for the div to be scrolled UP
*/
function startScrollUp(id){ 
	clearTimeout(scrollTimerUp); 
	document.getElementById(id).scrollTop-=scrollStep;  
	scrollTimerUp=setTimeout("startScrollUp('"+id+"')",10); 
}  

var scrollTimerDown="";  
var scrollTimerUp="";  

/* 
	Calling this function will stop the scrolling
*/	
function stopScroll(){ 
	clearTimeout(scrollTimerDown);  
	clearTimeout(scrollTimerUp); 
}

/*
On mouse move from the scrollers scrolling will stop 
*/
document.onmousemove=function(){stopScroll()}  
 