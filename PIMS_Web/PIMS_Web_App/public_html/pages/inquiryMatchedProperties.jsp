<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">	 
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg" />
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr >
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['inquiryMatchedProperties.heading']}" styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" >

										<h:form id="frm" style="width:97%" enctype="multipart/form-data">
											<table border="0" class="layoutTable">

												<tr>

													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$inquiryMatchedProperties.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$inquiryMatchedProperties.successMessages}" />
													</td>
												</tr>
											</table>

											<div class="MARGIN" style="width: 98%">
												<table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
										        </table>
												<rich:tabPanel style="width:100%;height:235px;" headerSpacing="0">
												
														<!-- Inquiry Details Tab - Start -->
														<rich:tab label="#{msg['inquiryMatchedProperties.tab.inquiryDetails.heading']}">
															<t:div  style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid id="followUpDetailsTable" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4"
																	columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">	
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.applicationNumber']}:" ></h:outputLabel>
																	<h:inputText readonly="true" style="width: 160px;height:20px;" styleClass="READONLY" value="#{pages$inquiryMatchedProperties.inquiryView.applicationNumber}"></h:inputText>
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.status']}:"></h:outputLabel>
																	<h:inputText readonly="true" style="width: 160px;height:20px;" styleClass="READONLY" value="#{pages$inquiryMatchedProperties.isEnglishLocale ? pages$inquiryMatchedProperties.inquiryView.applicationStatusEn : pages$inquiryMatchedProperties.inquiryView.applicationStatusAr}"></h:inputText>
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.priority']}:"></h:outputLabel>
																	<h:inputText readonly="true" style="width: 160px;height:20px;" styleClass="READONLY" value="#{pages$inquiryMatchedProperties.isEnglishLocale ? pages$inquiryMatchedProperties.inquiryView.priorityEn : pages$inquiryMatchedProperties.inquiryView.priorityAr}"></h:inputText>
																	
																	<h:outputLabel  styleClass="LABEL"  value="#{msg['inquiryApplication.personNumber']}:"></h:outputLabel>
																	<h:inputText readonly="true" style="width: 160px;height:20px;" styleClass="READONLY" value="#{pages$inquiryMatchedProperties.inquiryView.personView.socialSecNumber}"></h:inputText>
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.inquirerName']}:"></h:outputLabel>
																	<h:inputText readonly="true" style="width: 160px;height:20px;" styleClass="READONLY" value="#{pages$inquiryMatchedProperties.inquiryView.personView.personFullName}"></h:inputText>
																	
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.inquiryDate']}:"></h:outputLabel>
																	<h:inputText readonly="true" style="width: 160px;height:20px;" styleClass="READONLY" value="#{pages$inquiryMatchedProperties.inquiryView.inquiriesDate}">
																		<f:convertDateTime pattern="#{pages$inquiryMatchedProperties.dateFormat}" timeZone="#{pages$inquiryMatchedProperties.timeZone}" />
																	</h:inputText>
																	
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Inquiry Details Tab - End -->

														
														<!-- Matched Units Tab - Start -->
														<rich:tab label="#{msg['inquiryMatchedProperties.tab.matchedUnits.heading']}">
															<t:div styleClass="contentDiv" style="width:99%">
																<t:dataTable id="dataTable" 
																	rows="#{pages$inquiryMatchedProperties.paginatorRows}"
																	value="#{pages$inquiryMatchedProperties.matchedUnitViewList}"																	
																	preserveDataModel="false" preserveSort="false" var="dataItem"
																	rowClasses="row1,row2" rules="all" renderedIfEmpty="true" 
																	width="100%">
																	
																	<t:column>
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.select']}" />
																		</f:facet>
																		<h:selectBooleanCheckbox value="#{dataItem.selectedDefaultTrue}" />																		
																	</t:column>
																	
																	<t:column sortable="true">
																		<f:facet name="header" >
																			<t:outputText value="#{msg['unit.unitNumber']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.unitNumber}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
																	</t:column>
																	
																	<t:column sortable="true" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['cancelContract.tab.unit.propertyname']}"  />
																		</f:facet>
																		<t:outputText value="#{dataItem.propertyCommercialName}" styleClass="A_LEFT" style="white-space: normal;" />
																	</t:column>
																	
																	<t:column sortable="true" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['inquiryApplication.propertyType']}"  />
																		</f:facet>
																		<t:outputText value="#{pages$inquiryMatchedProperties.isEnglishLocale ? dataItem.propertyTypeEn : dataItem.propertyTypeAr}" styleClass="A_LEFT" style="white-space: normal;" />
																	</t:column>
																	
																	<t:column>
																		<f:facet name="header">
																			<t:outputText value="#{msg['unit.noOfBed']}" />
																		</f:facet>
																		<t:outputText value ="#{dataItem.noOfBed}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
																	</t:column>
														
															        <t:column>
																		<f:facet name="header">
																			<t:outputText value="#{msg['unit.noOfLiving']}" />
																		</f:facet>
																		<t:outputText value ="#{dataItem.noOfLiving}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
																	</t:column>
										     
						        								    <t:column>
																		<f:facet name="header">
																			<t:outputText value="#{msg['unit.noOfBath']}" />
																		</f:facet>
																		<t:outputText value ="#{dataItem.noOfBath}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
																	</t:column>
																	
																	<t:column>
																		<f:facet name="header">
																		 <t:outputText value="#{msg['unit.rentValue']}" />
																		</f:facet>
																		<t:outputText value ="#{dataItem.rentValue}" styleClass="A_RIGHT_NUM" style="white-space: normal;">
																			<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,##0.00"/>
																		</t:outputText>
																	</t:column>
																	
																	<t:column>
																		<f:facet name="header">
																			<t:outputText value= "#{msg['commons.Emirate']}" />
																		</f:facet>
																		<t:outputText value="#{pages$inquiryMatchedProperties.isEnglishLocale ? dataItem.countryEn : dataItem.countryAr}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
																	</t:column>
																	
																	<t:column>
																		<f:facet name="header">
																			<t:outputText value= "#{msg['unit.unitArea']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.unitArea}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
																	</t:column>
																	
																</t:dataTable>
															</t:div>
															
															<t:div styleClass="contentDivFooter" style="width:100%">
																			<t:panelGrid id="rqtkRecNumTable" columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD">
																				<t:div id="rqtkRecNumDiv" styleClass="RECORD_NUM_BG">
																					<t:panelGrid id="rqtkRecNumTbl" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
																						<h:outputText value="#{msg['commons.recordsFound']}"/>
																						<h:outputText value=" : "/>
																						<h:outputText value="#{pages$inquiryMatchedProperties.recordSize}"/>
																					</t:panelGrid>
																				</t:div>																				  
																			      <CENTER>
																				   <t:dataScroller id="rqtkscroller" for="dataTable" paginator="true"
																					fastStep="1"  paginatorMaxPages="#{pages$inquiryMatchedProperties.paginatorMaxPages}" immediate="false"
																					paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true" 
																				    pageIndexVar="pageNumber"
																				    styleClass="SCH_SCROLLER"
																				    paginatorActiveColumnStyle="font-weight:bold;"
																				    paginatorRenderLinkForActive="false" 
																					paginatorTableStyle="grid_paginator" layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">
								
																					    <f:facet  name="first">
																							<t:graphicImage    url="../#{path.scroller_first}"  id="lblFRequestHistory"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastrewind">
																							<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRRequestHistory"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastforward">
																							<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFRequestHistory"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="last">
																							<t:graphicImage  url="../#{path.scroller_last}"  id="lblLRequestHistory"></t:graphicImage>
																						</f:facet>
									
								                                                      <t:div id="rqtkPageNumDiv"   styleClass="PAGE_NUM_BG">
																					   <t:panelGrid id="rqtkPageNumTable"  styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0" >
																					 					<h:outputText  styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																										<h:outputText   styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																						</t:panelGrid>
																							
																						</t:div>
																				 </t:dataScroller>
																				 </CENTER>																		      
																	        </t:panelGrid>
								                           				</t:div>
																	</rich:tab>                                                        
														<!-- Matched Units Tab - End -->
                                                        
                                                        
                                                        <!-- Attachments Tab - Start -->
														<rich:tab label="#{msg['contract.attachmentTabHeader']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<!-- Attachments Tab - End -->
														
														
														<!-- Comments Tab - Start -->
                                                         <rich:tab label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														<!-- Comments Tab - End -->
																												
													</rich:tabPanel>
												
											<table cellpadding="0" cellspacing="0"  style="width:100%;">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
													<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td colspan="6" class="BUTTON_TD">
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	action="#{pages$inquiryMatchedProperties.btnCancel_Click}">
															</h:commandButton>
															<h:commandButton styleClass="BUTTON"
																	style="width: 150px"
																	value="#{msg['commons.sendnotification']}"
																	action="#{pages$inquiryMatchedProperties.btnSendNotification_Click}">
															</h:commandButton>															
														</td>
													</tr>
												</table>
											</div>
								
								
									    </h:form>
									</div>
								

									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
           </div>
		</body>
	</html>
</f:view>
