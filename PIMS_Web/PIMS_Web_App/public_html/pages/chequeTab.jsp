<h:panelGrid columns="4"
	rendered="#{pages$receivePayment.chequeCurrent}">
	<h:outputText value="#{msg['payment.amount']}"></h:outputText>
	<h:inputText style="text-align:right; height: 16px" id="f2"
		binding="#{pages$receivePayment.transactionAmount}"></h:inputText>
											<h:outputText  value="#{msg['payment.bank']}"></h:outputText>

	<h:selectOneMenu id="f1" binding="#{pages$receivePayment.banksMenu}">
		<f:selectItems value="#{pages$receivePayment.banks}" />
	</h:selectOneMenu>
	<h:outputText value="#{msg['payment.chequeDate']}"></h:outputText>
	<h:inputText id="f3" binding="#{pages$receivePayment.chequeDate}" readonly="true" style="text-align:right; height: 16px"></h:inputText>



													      	 <h:graphicImage style="width: 16;height: 16;border: 0" id="image"
															                 url="../resources/images/calendar.gif"
															                 onclick="javascript:Calendar(this,'_id0:f3');"/>
													 <h:graphicImage style="width: 16;height: 16;border: 0" 
															                 url="../resources/images/delete.gif"
															                 onclick="javascript:clearFields('_id0:f3');"/>
												                 
	<h:outputText value="#{msg['payment.accountNo']}">		</h:outputText>
	<h:inputText id="f5" binding="#{pages$receivePayment.accountNo}" style="text-align:right; height: 16px"></h:inputText>
<h:outputText value="#{msg['payment.chequeNo']}"></h:outputText>

	<h:inputText id="f4" binding="#{pages$receivePayment.chequeNo}" style="text-align:right; height: 16px"></h:inputText>
	

</h:panelGrid>
