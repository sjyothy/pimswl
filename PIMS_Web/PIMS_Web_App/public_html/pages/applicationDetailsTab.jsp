<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function openSearchPersonPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	   window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');	    
	}
	
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		document.getElementById('frm:hdnSelectedPersonId').value = personId;	 
		document.getElementById('frm:lnkReceiveApplicant').onclick();
	}	
</script>

<t:panelGrid id="pnlGrdAppDetailsTabFields" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4">
		
	<h:outputLabel styleClass="LABEL" value="#{msg['applicationDetails.number']}:" />
	<h:inputText readonly="true" styleClass="READONLY" value="#{pages$applicationDetailsTab.requestView.requestNumber}" />
	
	<h:outputLabel styleClass="LABEL" value="#{msg['applicationDetails.status']}:" />	
	<h:inputText readonly="true" styleClass="READONLY" value="#{pages$applicationDetailsTab.englishLocale ? pages$applicationDetailsTab.requestView.statusEn : pages$applicationDetailsTab.requestView.statusAr}" />
	
	<h:outputLabel styleClass="LABEL" value="#{msg['applicationDetails.date']}:" />	
	<rich:calendar value="#{pages$applicationDetailsTab.requestView.requestDate}"				    
				   locale="#{pages$applicationDetailsTab.locale}"					 
				   datePattern="#{pages$applicationDetailsTab.dateFormat}"
				   popup="true" 
				   showApplyButton="false" 
				   enableManualInput="false"
				   disabled="true"
				   inputClass="READONLY" />
	
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*"/>
		<h:outputLabel styleClass="LABEL" value="#{msg['applicationDetails.applicantName']}:" />
	</h:panelGroup>
	<h:panelGroup>
		<h:inputText readonly="true" styleClass="READONLY" value="#{pages$applicationDetailsTab.requestView.applicantView.personFullName}" />
		<h:graphicImage title="#{msg['commons.search']}"
						style="MARGIN: 1px 1px -5px;cursor:hand" 
						onclick="openSearchPersonPopup();"
						url="../resources/images/app_icons/Search-tenant.png">
		</h:graphicImage>
		<h:inputHidden id="hdnSelectedPersonId" value="#{pages$applicationDetailsTab.requestView.applicantView.personId}" />
		<a4j:commandLink id="lnkReceiveApplicant" action="#{pages$applicationDetailsTab.onReceiveApplicant}" reRender="pnlGrdAppDetailsTabFields" />
	</h:panelGroup>
	
	<h:outputLabel styleClass="LABEL" value="#{msg['applicationDetails.applicantType']}:" />
	<h:inputText readonly="true" styleClass="READONLY" value="#{pages$applicationDetailsTab.englishLocale ? pages$applicationDetailsTab.requestView.applicantView.personTypeEn : pages$applicationDetailsTab.requestView.applicantView.personTypeAr}" />
	
	<h:outputLabel styleClass="LABEL" value="#{msg['applicationDetails.cell']}:" />
	<h:inputText readonly="true" styleClass="READONLY" value="#{pages$applicationDetailsTab.requestView.applicantView.cellNumber}" />
	
	<h:outputLabel styleClass="LABEL" value="#{msg['applicationDetails.dsescription']}:" />	
	<h:inputTextarea value="#{pages$applicationDetailsTab.requestView.description}" style="width: 185px;" />
	
</t:panelGrid>