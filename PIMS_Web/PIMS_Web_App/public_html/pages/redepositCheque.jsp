<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



    
<f:view>
        <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
       <f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
 
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>						
            <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
			
			


			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			
 	</head>
 	        
		<script language="javascript" type="text/javascript">
	function clearWindow()
	{
            document.getElementById("formPaymentSchedule:dateTimePaymentDueOn").value="";
			document.getElementById("formPaymentSchedule:txtAmount").value="";
			var cmbPaymentMode=document.getElementById("formPaymentSchedule:selectPaymentMode");
			
			cmbPaymentMode.selectedIndex = 0;
			    $('formPaymentSchedule:dateTimePaymentDueOn').component.resetSelectedDate();
	        
	}
	
 	</script>
 	

				<!-- Header -->
	
		<body class="BODY_STYLE">
	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
							<tr>
							<td class="HEADER_TD">
										<h:outputLabel value="#{msg['redepositCheque.title']}" styleClass="HEADER_FONT"/>
								</td>
							
							</tr>
						</table>
					        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
                                 <tr valign="top">
                                  <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
                                   </td>    
									<td width="100%" height="99%" valign="top" nowrap="nowrap">
									<div  class="SCROLLABLE_SECTION"  >
									  
										<h:form id="formPaymentSchedule" style="width:92%"  enctype="multipart/form-data">
											<table border="0" class="layoutTable" >
												<tr>
													<td >
													    <h:outputText    id="errormsg"        escape="false" styleClass="ERROR_FONT" value="#{pages$redepositCheque.errorMessages}"/>
												        <h:inputHidden id="hdnRowId" value="#{pages$redepositCheque.rowId}"> </h:inputHidden>   	
													</td>
												</tr>
											</table>
										
										<div class="MARGIN" style="width:100%" >
											
										<div  style="width:90%" >
										
										<table cellpadding="1px" cellspacing="1px"  >
												
												<tr>
													<td>
														<h:outputLabel styleClass="mandatory" value="*"/>
													</td>
													<td > 
														<h:outputLabel styleClass="LABEL"   value="#{msg['redepositCheque.lbl.reason']}">:</h:outputLabel>
													</td>
												    
													<td>
													<t:inputTextarea styleClass="TEXTAREA" value="#{pages$redepositCheque.remarks}" rows="6" style="width: 320px;"/>
													
													</td>
												</tr>
												<tr>
													<td>
													&nbsp;&nbsp;
													</td>
												   <td width="35%"> 
														&nbsp;&nbsp;
													</td>
													<td class="BUTTON_TD">
												          <h:commandButton type="submit" styleClass="BUTTON" 
												                     action="#{pages$redepositCheque.sendToParent}" value="#{msg['redepositCheque.btnRedeposit']}"/>	
                                           			</td>
												</tr>
												
										</table>
										</div>
                                        </div>
                                       
			    </h:form>
			 </div>
									</td>
									</tr>
									</table>
			
			</td>
    </tr>
    
    </table>
			
		</body>
	</html>
</f:view>

