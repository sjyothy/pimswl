<%-- 
  - Author: Wasif A Kirmani
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Financial Configuration
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	    
	    function resetValues()
      	{
      	    document.getElementById("searchFrm:tenderNumber").value="";
			document.getElementById("searchFrm:tenderTypeCombo").selectedIndex=0;
      	    $('searchFrm:issueDateFrom').component.resetSelectedDate();
			$('searchFrm:issueDateTo').component.resetSelectedDate();
			$('searchFrm:endDateFrom').component.resetSelectedDate();
			$('searchFrm:endDateTo').component.resetSelectedDate();
			document.getElementById("searchFrm:propertyName").value="";
			document.getElementById("searchFrm:tenderDescription").value="";
			
        }
        
        
        
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<body class="BODY_STYLE">
		<div class="containerDiv">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>

				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['financialAccConfiguration.add.heading']}"
										styleClass="HEADER_FONT" />

								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td width="100%" height="466px" valign="top">
									<div class="SCROLLABLE_SECTION  AUC_SCH_SS">
										<h:form id="searchFrm" style="WIDTH: 97.6%;">
										
											<table border="0" class="layoutTable">
												<tr>
													<td>
														<h:outputText value="#{pages$addFinancialConfiguration.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />
														<h:outputText value="#{pages$addFinancialConfiguration.successMessages}"
															escape="false" styleClass="INFO_FONT" />
													</td>
												</tr>
											</table>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="font-size: 0px;">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="font-size: 0px;">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="font-size: 0px;">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
													
                                                       <tr>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['grp.paymentType']}:"></h:outputLabel>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:selectOneMenu id="paymentTypeMenu"
																	binding="#{pages$addFinancialConfiguration.paymentTypeMenu}"
																	style="width: 192px;" required="false">
																	<f:selectItems value="#{pages$addFinancialConfiguration.paymentTypeList}" />
																</h:selectOneMenu>
															</td>
		
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['grp.ownerShipMenu']}:"></h:outputLabel>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:selectOneMenu id="ownerShipMenu"
																	binding="#{pages$addFinancialConfiguration.ownerShipMenu}"
																	style="width: 192px;" required="false">
																	<f:selectItems
																		value="#{pages$ApplicationBean.propertyOwnershipType}" />
																</h:selectOneMenu>
															</td>

															
														</tr>
													<tr>
															<td  style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL" value="#{msg['financialAccConfiguration.paymentMethod']}:"/>
															</td>
															<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:selectOneMenu id="paymentMethodMenu" binding="#{pages$addFinancialConfiguration.paymentMethod}" style="width: 192px;"  >
																    <f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1" />
																	<f:selectItems value="#{pages$addFinancialConfiguration.paymentMethodList}" />
																    <a4j:support event="onchange" reRender="cmbbankRemitAcc,bankRemAccName,GRPAccountNumber"
																	    action="#{pages$addFinancialConfiguration.populateBankRemitAccViewDetails}"/>
																</h:selectOneMenu>
																	
															</td>
															
															<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL" value="#{msg['financialAccConfiguration.bankAccName']}:"/>
															</td>
															<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:selectOneMenu id="cmbbankRemitAcc" binding="#{pages$addFinancialConfiguration.bankRemitAccount}" style="width: 280px;" >
																	<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1" />
																	<f:selectItems value="#{pages$addFinancialConfiguration.comboBankRemitAccViewFromPaymentMethod}" />
																    <a4j:support event="onchange" reRender="bankRemAccName,GRPAccountNumber"
																	    action="#{pages$addFinancialConfiguration.populateBankRemitAccViewDetails}"/>
																</h:selectOneMenu>
															</td>
                                                   </tr>
												   <tr>
													 		<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL" value="#{msg['financialAccConfiguration.bankName']}:"/>
															</td>
															<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:inputText id="bankRemAccName" style="width: 280px;" maxlength="80" readonly="true"
																	binding="#{pages$addFinancialConfiguration.bankRemitanceAccountName}">
																</h:inputText>
															</td>
															
															<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL" value="#{msg['financialAccConfiguration.bankNo']}:"/>
															</td>
															<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:inputText id="GRPAccountNumber" style="width: 280px;" maxlength="100" readonly="true"
																	binding="#{pages$addFinancialConfiguration.GRPAccountNumber}">
																</h:inputText>
															</td>
												</tr>
                                                <tr>
													<td
															style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
															<h:outputLabel styleClass="LABEL" value="#{msg['grp.TrxType']}:"></h:outputLabel>
													</td>
													<td
															style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
															<h:selectOneMenu id="XXTransactionTypeMenu"
																binding="#{pages$addFinancialConfiguration.trxTypeMenu}"
																style="width: 250px;" required="false">
																
																<f:selectItems value="#{pages$ApplicationBean.xxAmafTransactionTypes}" />
															</h:selectOneMenu>
													</td>
													<td
															style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
															<h:outputLabel styleClass="LABEL" value="#{msg['grp.TrxTypeDebit']}:"></h:outputLabel>
													</td>
													<td
															style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
															<h:selectOneMenu id="XXTransactionTypeMenuDebit"
																binding="#{pages$addFinancialConfiguration.trxTypeMenuDebit}"
																style="width: 250px;" required="false">
																<f:selectItems value="#{pages$ApplicationBean.xxAmafTransactionTypes}" />
															</h:selectOneMenu>
													</td>
												</tr>
												<tr>
															
															
															
                                                           <td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL" value="#{msg['financialAccConfiguration.receivableActivity']}:"/>
															</td>
															<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:selectOneMenu id="cmbRecActivity" binding="#{pages$addFinancialConfiguration.activityName}" style="width: 280px;" >
																	<f:selectItems value="#{pages$ApplicationBean.xxAmafReceiveableActivity}" />
																</h:selectOneMenu>
															</td>															
															
															
															<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL" value="#{msg['financialAccConfiguration.amafReceiptType']}:"></h:outputLabel>
															</td>
															<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:selectOneMenu id="XXAmafReceiptType"
																		binding="#{pages$addFinancialConfiguration.receiptType}"
																		style="width: 250px;" required="false">
																		
																		<f:selectItems value="#{pages$ApplicationBean.xxAmafReceiptType}" />
																</h:selectOneMenu>
															</td>
													  </tr>
													  <tr>
														<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
															<h:outputLabel styleClass="LABEL" value="#{msg['financialAccConfiguration.memoLineName']}:"></h:outputLabel>
														</td>
														<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
															<h:selectOneMenu id="XXMemoLineMenu"
																binding="#{pages$addFinancialConfiguration.memoLine}"
																style="width: 250px;" required="false">
																<f:selectItems value="#{pages$ApplicationBean.xxAmafMemoLine}" />
															</h:selectOneMenu>
														</td>

															
													 </tr>
														
														
														<tr>
															<td class="BUTTON_TD JUG_BUTTON_TD" colspan="4">
																<table cellpadding="1px" cellspacing="1px">
																	<tr>
																		<td>
																			<h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}"
																				action="#{pages$addFinancialConfiguration.addConfiguration}"
																				style="width: 80px" />

																			
																			
																		</td>
																		<td>
																			<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}"
																				action="#{pages$addFinancialConfiguration.backToSearch}"
																				style="width: 80px" />

																			
																			
																		</td>
																		
													   </tr>
																</table>
															</td>
														</tr>
													</table>
												</div>
											</div>
																			</div>

									</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>

					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>

				</tr>
			</table>
			</containerDiv>
		</body>
	</html>
</f:view>