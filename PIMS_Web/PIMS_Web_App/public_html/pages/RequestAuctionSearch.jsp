
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



<script language="JavaScript" type="text/javascript">	    
	    function resetValues()
      	{
      		document.getElementById("searchFrm:auctionNumber").value="";
			document.getElementById("searchFrm:auctionTitle").value="";
      	    $('searchFrm:auctionDate').component.resetSelectedDate();
      	    $('searchFrm:auctionCreationDate').component.resetSelectedDate();
      	    document.getElementById("searchFrm:auctionStatuses").selectedIndex=0;
			document.getElementById("searchFrm:auctionStartTime").value="";
			document.getElementById("searchFrm:auctionEndTime").value="";
			document.getElementById("searchFrm:venueName").value="";
        }
       	
       	function RowDoubleClick(auctionId, auctionNumber, auctionDate, auctionTitle, venueName)
		{
	      window.opener.document.getElementById("auctionRequestForm:auctionID").value = auctionId;
		  window.opener.document.getElementById("auctionRequestForm:aNo").value = auctionNumber;
		  window.opener.document.getElementById("auctionRequestForm:aDate").value = auctionDate;
		  window.opener.document.getElementById("auctionRequestForm:aTitle").value = auctionTitle;
		  window.opener.document.getElementById("auctionRequestForm:aVenue").value = venueName;
		  window.opener.document.getElementById("auctionRequestForm:auctionUnitsOfRequest").value = "";
		}
		function closeWindow()
		{
		  window.close();
		}

	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	


</script>
<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}"  style="overflow:hidden;">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			
		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

	</head>

	<body class="BODY_STYLE">

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">						
					</td>
				</tr>

				<tr width="100%">
					<td width="83%" valign="top" class="divBackgroundBody" colspan="2">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['auction.searchHeading']}" styleClass="HEADER_FONT"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION AUC_SCH_SS">
									<h:form id="searchFrm" style="width:98%">
									<table>
										<tr>
											<td colspan="7">
												<h:messages>
													<h:outputText value="#{pages$RequestAuctionSearch.errorMessages}"
														escape="false" />
												</h:messages>
											</td>
										</tr>
									</table>
									<div class="MARGIN"> 
									<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
									<div class="DETAIL_SECTION">
										<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
										<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" border="0">
											
											<tr>
												<td>
													<h:outputLabel value="#{msg['auction.number']}:"></h:outputLabel>
												</td>												
												<td>
													<h:inputText id="auctionNumber"
														value="#{pages$RequestAuctionSearch.auctionView.auctionNumber}"
														style="width: 186px; height: 16px" maxlength="20"></h:inputText>
												</td>
												<td colspan="1">
													<h:outputLabel value="#{msg['auction.title']}:"></h:outputLabel>
												</td>												
												<td colspan="1">
													<h:inputText id="auctionTitle"
														value="#{pages$RequestAuctionSearch.auctionView.auctionTitle}"
														style="width: 186px; height: 16px" maxlength="250"></h:inputText>													
												</td>
											</tr>
											<tr>
												<td colspan="1">
													<h:outputLabel value="#{msg['auction.date']}:"></h:outputLabel>
												</td>
												<td colspan="1">
													<rich:calendar id="auctionDate"
													value="#{pages$RequestAuctionSearch.auctionView.auctionDateVal}"
							                     	locale="#{pages$RequestAuctionSearch.locale}"
							                     	popup="true"
							                     	datePattern="#{pages$RequestAuctionSearch.dateFormat}"
							                     	showApplyButton="false" enableManualInput="false"
							                     	cellWidth="24px" cellHeight="22px"
							                     	style="width:186px; height:16px"/>												
												</td>
												<td colspan="1">
													<h:outputLabel value="#{msg['auction.creationDate']}:" />
												</td>
												<td colspan="1">
													<rich:calendar id="auctionCreationDate" 
													value="#{pages$RequestAuctionSearch.auctionView.createdOn}"
													locale="#{pages$RequestAuctionSearch.locale}" popup="true" 
													datePattern="#{pages$RequestAuctionSearch.dateFormat}" 
													showApplyButton="false" 
													enableManualInput="false" cellWidth="24px" 
													cellHeight="22px" style="width:186px; height:16px"/>
												</td>
											</tr>
											<tr>
												<td colspan="1">
													<h:outputLabel value="#{msg['auction.startTime']}:"></h:outputLabel>
												</td>
												<td colspan="1">
													<h:inputText id="auctionStartTime"
														value="#{pages$RequestAuctionSearch.auctionView.auctionStartTime}"
														style="width: 186px; height: 16px"></h:inputText>

												</td>
												<td colspan="1">
													<h:outputLabel value="#{msg['auction.endTime']}:" />
												</td>
												<td colspan="1">
													<h:inputText id="auctionEndTime"
														value="#{pages$RequestAuctionSearch.auctionView.auctionEndTime}"
														style="width: 186px; height: 16px"></h:inputText>
												</td>
											</tr>
											<tr>
												<td colspan="1">
													<h:outputLabel value="#{msg['auction.venue']}:"></h:outputLabel>
												</td>
								
												<td colspan="1">
													<h:inputText id="venueName"
														value="#{pages$RequestAuctionSearch.auctionView.auctionVenue.venueName}"
														style="width: 186px; height: 16px" maxlength="50"></h:inputText>
												</td>
												<td colspan="1">
													&nbsp;
												</td>
												<td colspan="1">
													&nbsp;
												</td>
											</tr>
											<tr>
												<td colspan="4" class="BUTTON_TD">
													<h:commandButton styleClass="BUTTON"
														value="#{msg['commons.search']}"
														action="#{pages$RequestAuctionSearch.searchAuctions}"
														style="width: 75px" tabindex="7"></h:commandButton>
													&nbsp;
													<h:commandButton styleClass="BUTTON"
														value="#{msg['commons.clear']}"
														onclick="javascript:resetValues();" style="width: 75px"
														tabindex="7"></h:commandButton>
													&nbsp;
													<h:commandButton styleClass="BUTTON"
														value="#{msg['commons.cancel']}"
														style="width: 75px"
														tabindex="7"
														onclick="javascript:closeWindow();"></h:commandButton>
													&nbsp;
												</td>
											</tr>
										</table>
										</div></div>
										<br><br>
										<div class="MARGIN">
										<div class="imag">&nbsp;</div>
										<div class="contentDiv" style="width: 98.3%">
											<t:dataTable id="dt1"
												value="#{pages$RequestAuctionSearch.auctionDataList}"
												binding="#{pages$RequestAuctionSearch.dataTable}" rows="10"
												preserveDataModel="false" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												width="100%">

												<t:column id="col1" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['auction.number']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.auctionNumber}" />
												</t:column>
												<t:column id="col2" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['auction.title']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.auctionTitle}" />
												</t:column>
												<t:column id="col3" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['auction.creationDate']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.auctionCreationDateString}" />
												</t:column>
												<t:column id="col4" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['auction.date']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.auctionDate}" />
												</t:column>
												<t:column id="col5" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['auction.startTime']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.auctionStartTime}" />
												</t:column>
												<t:column id="col6" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['auction.endTime']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.auctionEndTime}" />
												</t:column>
												<t:column id="col7" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['auction.venue']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.auctionVenue.venueName}" />
												</t:column>
												<t:column id="col8" sortable="true" 
													rendered="#{pages$RequestAuctionSearch.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['auction.status']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.auctionStatus}" />
												</t:column>
												<t:column id="col9" sortable="true" 
													rendered="#{pages$RequestAuctionSearch.isArabicLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['auction.status']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.auctionStatusAr}" />
												</t:column>
												<t:column id="col10" sortable="false" width="30">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.select']}" />
													</f:facet>
													
													<t:commandLink
														actionListener="#{pages$RequestAuctionSearch.auctionDetailsViewMode}"
														rendered="#{pages$RequestAuctionSearch.isSelectable}" 
														onclick='javascript:RowDoubleClick(#{dataItem.auctionId}, "#{dataItem.auctionNumber}", "#{dataItem.auctionDate}", "#{dataItem.auctionTitle}", "#{dataItem.auctionVenue.venueName}");'>
														<t:graphicImage id="selectIcon" title="#{msg['commons.select']}" 
														url="../resources/images/app_icons/Select-Auction.png"/>														
													</t:commandLink>
												</t:column>
											</t:dataTable>
										</div>
										<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:100%;" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$RequestAuctionSearch.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:50%">
			
													<t:dataScroller id="scroller" for="dt1" paginator="true"
														fastStep="1" paginatorMaxPages="2" immediate="false"
														styleClass="scroller" paginatorTableClass="paginator"
														renderFacetsIfSinglePage="true" 												
														paginatorTableStyle="grid_paginator" layout="singleTable" 
														paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
														paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
														pageIndexVar="pageNumber">
					
														<f:facet name="first">
															
															<t:graphicImage url="../resources/images/first_btn.gif" id="lblF"></t:graphicImage>
														</f:facet>
					
														<f:facet name="fastrewind">
															<t:graphicImage url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>
														</f:facet>
					
														<f:facet name="fastforward">
															<t:graphicImage url="../resources/images/next_btn.gif" id="lblFF"></t:graphicImage>
														</f:facet>
					
														<f:facet name="last">
															<t:graphicImage url="../resources/images/last_btn.gif" id="lblL"></t:graphicImage>
														</f:facet>
														<t:div styleClass="PAGE_NUM_BG">
															<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>										
														</t:div>	
													</t:dataScroller>
												</td>
												</tr>
											</table>
										</t:div>
										</div>
									</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
	</body>
</html>
</f:view>