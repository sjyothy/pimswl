<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



    
<f:view>
        <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
       <f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
 
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>						
            <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
			


			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			
 	</head>
 	        
		<script language="javascript" type="text/javascript">
	function clearWindow()
	{
            //document.getElementById("formPaymentSchedule:dateTimePaymentDueOn").value="";
			//document.getElementById("formPaymentSchedule:txtAmount").value="";
			//var cmbPaymentMode=document.getElementById("formPaymentSchedule:selectPaymentMode");
			
			//cmbPaymentMode.selectedIndex = 0;
			//  $('formPaymentSchedule:dateTimePaymentDueOn').component.resetSelectedDate();
	        
	}
	function addToContract()

      {

      

        window.opener.document.forms[0].submit();

        

        window.close();

      }
	
    </script>
 	

				<!-- Header -->
	
		<body class="BODY_STYLE">
	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<% System.out.println("- ONE"); %>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr><td class="HEADER_TD">
										<h:outputLabel value="#{msg['contract.paymentSchedule.GeneratePayments']}" styleClass="HEADER_FONT"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
					        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
                                 <tr valign="top">
                                  <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
                                   </td>    
									<td width="100%" height="450px" valign="top" nowrap="nowrap">
									<div  class="SCROLLABLE_SECTION"  >
									  
										<h:form id="formPaymentSchedule" style="width:92%"  enctype="multipart/form-data">
											<table border="0" class="layoutTable" >
											
												<tr>
												
													<td colspan="1">
												
														<h:outputText  escape="false" value="#{pages$generateServiceContractPayments.errorMessages}"/>
												
													</td>
												</tr>
											</table>
										
										<div class="MARGIN" style="width:100%" >
											<table cellpadding="0" cellspacing="0" width="90%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
											</table>
										<div class="DETAIL_SECTION" style="width:90%" >
										<h:outputLabel value="#{msg['paymentSchedule.paymentDetails']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="1px"
														class="DETAIL_SECTION_INNER" width="100%">

														<tr>
															<td width="25%"><h:outputLabel value="#{msg['contract.TotalContractValue']}">:</h:outputLabel></td>
															<td width="25%">
																&nbsp;&nbsp;
															    <h:inputText disabled="true" style="width:85%" styleClass="A_RIGHT_NUM" readonly="true" maxlength="15" id="txtTotalContractValue" value="#{pages$generateServiceContractPayments.amount}">
																</h:inputText>
															</td>
														</tr>
														<tr>
															<td>
															   <h:outputLabel value="#{msg['generatePayments.startDate']}">:</h:outputLabel>
															</td>
															<td>
															   <rich:calendar id="startDate" value="#{pages$generateServiceContractPayments.startDate}" popup="true" showApplyButton="false" datePattern="#{pages$generateServiceContractPayments.dateformat}" enableManualInput="false" cellWidth="24px" inputStyle="width: 85%">
															   </rich:calendar>
															</td>
															<td> 
														        <h:outputLabel  value="#{msg['generateServiceContractPayment.paymentPeriod']}">:</h:outputLabel>
													        </td>
													        <td>
														        <h:selectOneMenu id="selectpaymentDuration" style="width: 100px;" 
														           value="#{pages$generateServiceContractPayments.selectOnePaymentPeriod}">
														          <f:selectItems value="#{pages$generateServiceContractPayments.paymentPeriodList}"/>
					                                            </h:selectOneMenu>
													        </td>
														</tr>	

														<tr>
															
															
															<td colspan="4" class="BUTTON_TD">
																<h:commandButton type="submit" styleClass="BUTTON"
																	action="#{pages$generateServiceContractPayments.generatePayments}"
																	value="#{msg['contract.paymentSchedule.GeneratePayments']}" style="width: 151px"/>
																<h:commandButton styleClass="BUTTON" type="button"
																	value="#{msg['commons.reset']}" style="width: 54px"
																	onclick="javascript:clearWindow();" />
															</td>
														</tr>
													</table>


												</div>
                                          </div>
                                          <br/>
                                          <div id="paddingDiv"  >
								          
						                 <t:div id="dataListDiv" styleClass="contentDiv"  style="width:90%"  >
												<t:dataTable id="test3"
													rows="5" width="100%"
													value="#{pages$generateServiceContractPayments.paymentSchedules}"
													binding="#{pages$generateServiceContractPayments.dataTable}"
													preserveDataModel="true" preserveSort="true" var="dataItem"
							
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												>
												<t:column id="colDueOn">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentDueOn']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.paymentDueOn}" >
														 <f:convertDateTime pattern="#{pages$generateServiceContractPayments.dateformat}" 
													timeZone="#{pages$generateServiceContractPayments.timeZone}"/>
													</t:outputText>
												</t:column>
												<t:column id="colTypeEn" rendered="#{pages$generateServiceContractPayments.isEnglishLocale}">


													<f:facet name="header">

														<t:outputText  value="#{msg['paymentSchedule.paymentType']}" />

													</f:facet>
													<t:outputText     styleClass="A_LEFT"  title="" value="#{dataItem.typeEn}" />
												</t:column>
												<t:column id="colTypeAr" rendered="#{pages$generateServiceContractPayments.isArabicLocale}">


													<f:facet name="header">

														<t:outputText  value="#{msg['paymentSchedule.paymentType']}" />

													</f:facet>
													<t:outputText     styleClass="A_LEFT"  title="" value="#{dataItem.typeAr}" />
												</t:column>
												<t:column id="colModeEn" rendered="#{pages$generateServiceContractPayments.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentMode']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.paymentModeEn}" />
												</t:column>
												<t:column id="colModeAr" rendered="#{pages$generateServiceContractPayments.isArabicLocale}">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentMode']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.paymentModeAr}" />
												</t:column>
												<t:column id="colAmount" >
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.amount']}" />
													</f:facet>
													<t:outputText styleClass="A_RIGHT_NUM" title="" value="#{dataItem.amount}" />
												</t:column>
													 
											  </t:dataTable>										
											</t:div>
											  <t:div id="pagingDivPaySch" styleClass="contentDivFooter" style="width:91.7%;" >

												<t:dataScroller id="scrollerPaySch" for="test3" paginator="true"
													fastStep="1" paginatorMaxPages="15" immediate="false"
													styleClass="scroller" paginatorTableClass="paginator" 
													renderFacetsIfSinglePage="true" paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">
                                                	<f:facet  name="first">
														
														<t:graphicImage  url="../#{path.scroller_first}" id="lblFPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="fastrewind">
														<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="fastforward">
														<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="last">
														<t:graphicImage   url="../#{path.scroller_last}"  id="lblLPaySch"></t:graphicImage>
													</f:facet>

												</t:dataScroller>
									
                                           </t:div>
						                 
						                  </div> 
						                   <table width="92%" >
											<tr>
												
												
											 <td colspan="6" class="BUTTON_TD"
                                              		 
                                                > 
                                           		
											 <h:commandButton style="width: 134px" 
											 styleClass="BUTTON" value="#{msg['paidFacilities.sendToContract']}"  action="#{pages$generateServiceContractPayments.sendToContract}"/>
											</td>
											</tr>
											</table> 
			    </h:form>
			 </div>
									</td>
									</tr>
									</table>
			
			</td>
    </tr>
    
    </table>
			
		</body>
	</html>
</f:view>

