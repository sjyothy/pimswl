<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">	
</script>

<t:panelGrid rendered="#{pages$sessionDetailsTab.isTabModeUpdatable}" id="pnlGrdFields" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4">
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*"/>
		<h:outputLabel styleClass="LABEL" value="#{msg['sessionDetailsTab.judgeName']}:" />
	</h:panelGroup>
	<h:inputText value="#{pages$sessionDetailsTab.sessionDetailView.judgeName}" />
	
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*"/>
		<h:outputLabel styleClass="LABEL" value="#{msg['sessionDetailsTab.place']}:" />
	</h:panelGroup>
	<h:inputText value="#{pages$sessionDetailsTab.sessionDetailView.place}" />
	
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*"/>
		<h:outputLabel styleClass="LABEL" value="#{msg['sessionDetailsTab.date']}:" />
	</h:panelGroup>
	<rich:calendar value="#{pages$sessionDetailsTab.sessionDetailView.date}"				    
					locale="#{pages$sessionDetailsTab.locale}"					 
					datePattern="#{pages$sessionDetailsTab.dateFormat}"
					popup="true" 
					showApplyButton="false" 
					enableManualInput="false" />
	
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*"/>
		<h:outputLabel styleClass="LABEL" value="#{msg['sessionDetailsTab.time']}:" />
	</h:panelGroup>		
	<h:panelGroup>
		<h:selectOneMenu value="#{pages$sessionDetailsTab.sessionDetailView.timeHH}" style="width: 45px;">
			<f:selectItem itemValue="" itemLabel="HH" />
			<f:selectItems value="#{pages$ApplicationBean.hours}" />
		</h:selectOneMenu>
		<h:outputLabel styleClass="LABEL" value=" : " />
		<h:selectOneMenu value="#{pages$sessionDetailsTab.sessionDetailView.timeMM}" style="width: 45px;">
			<f:selectItem itemValue="" itemLabel="MM" />
			<f:selectItems value="#{pages$ApplicationBean.minutes}" />
		</h:selectOneMenu>
	</h:panelGroup>
	
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*"/>
		<h:outputLabel styleClass="LABEL" value="#{msg['sessionDetailsTab.decision']}:" />
	</h:panelGroup>
	<t:panelGroup colspan="3">
	<h:inputTextarea value="#{pages$sessionDetailsTab.sessionDetailView.decision}" style="width: 100%;" />
	</t:panelGroup>
	<h:panelGroup rendered="#{! pages$sessionDetailsTab.parentTakharuj}">
		<h:outputLabel styleClass="mandatory" value="*"/>
		<h:outputLabel styleClass="LABEL" value="#{msg['sessionDetailsTab.maturityDate']}:" />
	</h:panelGroup>
	<rich:calendar value="#{pages$sessionDetailsTab.sessionDetailView.maturityDate}"				    
					locale="#{pages$sessionDetailsTab.locale}"					 
					datePattern="#{pages$sessionDetailsTab.dateFormat}"
					popup="true" 
					showApplyButton="false" 
					enableManualInput="false"
					rendered="#{! pages$sessionDetailsTab.parentTakharuj}"
					 />
		
</t:panelGrid>

<t:panelGrid rendered="#{pages$sessionDetailsTab.isTabModeUpdatable}" id="pnlGrdActions" styleClass="BUTTON_TD" cellpadding="1px" width="100%" cellspacing="5px">
	<h:panelGroup>
		<h:commandButton styleClass="BUTTON"
						value="#{msg['commons.saveButton']}"
						action="#{pages$sessionDetailsTab.onSessionDetailSave}" />
	</h:panelGroup>
</t:panelGrid>

<t:div styleClass="contentDiv" style="width:98%">																
	<t:dataTable id="sessionDetailViewDataTable"  
				rows="#{pages$sessionDetailsTab.paginatorRows}"
				value="#{pages$sessionDetailsTab.sessionDetailViewList}"
				binding="#{pages$sessionDetailsTab.sessionDetailViewDataTable}"																	
				preserveDataModel="false" preserveSort="false" var="dataItem"
				rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
																	
		<t:column width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['sessionDetailsTab.judgeName']}" />
			</f:facet>
			<t:outputText value="#{dataItem.judgeName}" />
		</t:column>
																																		
		<t:column width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['sessionDetailsTab.place']}" />
			</f:facet>
			<t:outputText value="#{dataItem.place}" />																		
		</t:column>
																	
		<t:column width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['sessionDetailsTab.date']}" />
			</f:facet>
			<t:outputText value="#{dataItem.date}" >
				<f:convertDateTime pattern="#{pages$sessionDetailsTab.dateFormat}" timeZone="#{pages$sessionDetailsTab.timeZone}" />
			</t:outputText>																		
		</t:column>
		
		<t:column width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['sessionDetailsTab.time']}" />
			</f:facet>
			<t:outputText value="#{dataItem.timeHH}:#{dataItem.timeMM}" />																		
		</t:column>
		
		<t:column width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['sessionDetailsTab.decision']}" />
			</f:facet>
			<t:outputText value="#{dataItem.decision}"  />																		
		</t:column>
		
		<t:column width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['sessionDetailsTab.maturityDate']}" />
			</f:facet>
			<t:outputText value="#{dataItem.maturityDate}"  >
				<f:convertDateTime pattern="#{pages$sessionDetailsTab.dateFormat}" timeZone="#{pages$sessionDetailsTab.timeZone}" />
			</t:outputText>	
																				
		</t:column>
																	
		<t:column width="10%" sortable="false" rendered="#{pages$sessionDetailsTab.isTabModeUpdatable}">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.action']}" />
			</f:facet>
			<a4j:commandLink onclick="if (!confirm('#{msg['sessionDetail.confirm.delete']}')) return" action="#{pages$sessionDetailsTab.onSessionDetailDelete}" rendered="#{empty dataItem.sessionDetailViewId}" reRender="sessionDetailViewDataTable,sessionDetailViewGridInfo">
				<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />
			</a4j:commandLink>																																		
		</t:column>
		
	</t:dataTable>										
</t:div>

<t:div id="sessionDetailViewGridInfo" styleClass="contentDivFooter" style="width:99%">
	<t:panelGrid id="rqtkRecNumTableId"  columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
		<t:div id="rqtkRecNumDivId" styleClass="RECORD_NUM_BG">
			<t:panelGrid id="rqtkRecNumTblId" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
				<h:outputText value="#{msg['commons.recordsFound']}"/>
				<h:outputText value=" : "/>
				<h:outputText value="#{pages$sessionDetailsTab.sessionDetailViewListRecordSize}"/>																						
			</t:panelGrid>
		</t:div>
																				  
		<CENTER>
			<t:dataScroller id="rqtkscrollerId" for="sessionDetailViewDataTable" paginator="true"
							fastStep="1"  
							paginatorMaxPages="#{pages$sessionDetailsTab.paginatorMaxPages}" 
							immediate="false"
							paginatorTableClass="paginator"
							renderFacetsIfSinglePage="true" 
							pageIndexVar="pageNumber"
							styleClass="SCH_SCROLLER"
							paginatorActiveColumnStyle="font-weight:bold;"
							paginatorRenderLinkForActive="false" 
							paginatorTableStyle="grid_paginator" layout="singleTable"
							paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;">
								
							<f:facet name="first">
								<t:graphicImage url="../#{path.scroller_first}" id="lblFRequestHistoryId"></t:graphicImage>
							</f:facet>
									
							<f:facet name="fastrewind">
								<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFRRequestHistoryId"></t:graphicImage>
							</f:facet>
									
							<f:facet name="fastforward">
								<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFRequestHistoryId"></t:graphicImage>
							</f:facet>
									
							<f:facet name="last">
								<t:graphicImage url="../#{path.scroller_last}" id="lblLRequestHistoryId"></t:graphicImage>
							</f:facet>
									
							<t:div id="rqtkPageNumDivId" styleClass="PAGE_NUM_BG">
								<t:panelGrid id="rqtkPageNumTableId" styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0">
									<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
									<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
								</t:panelGrid>																							
							</t:div>
			</t:dataScroller>
		</CENTER>																		      
	</t:panelGrid>
</t:div>											