<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
	<script type="text/javascript">
	
		
	</script>
	<t:div style="width:100%;">
		<t:panelGrid columns="1" cellpadding="0" cellspacing="0">
			<t:div styleClass="BUTTON_TD">
			<pims:security screen="Pims.ConstructionMgmt.ProjectMilestones.AddMilestone" action="create">
				<t:commandButton id="btnAddMilestone" styleClass="BUTTON" rendered="#{pages$projectMilestonesTab.canAddMilestone}" value="#{msg['projectMilestone.addButton']}" actionListener="#{pages$projectMilestonesTab.openMilstonePopup}"  style="width: 100px;margin: 3px;" />
			</pims:security>
			</t:div>
			<t:div styleClass="contentDiv" style="width:98%;">
				<t:dataTable id="milestoneTable" renderedIfEmpty="true" width="100%" cellpadding="0" cellspacing="0" var="dataItem" binding="#{pages$projectMilestonesTab.dataTable}" value="#{pages$projectMilestonesTab.projectMilestoneList}"
				 	preserveDataModel="false" preserveSort="false" rowClasses="row1,row2" rules="all" rows="9">
					<t:column sortable="true" width="200" id="milestoneNumberCol">
						<f:facet name="header">
							<t:outputText value="#{msg['projectMilestone.milestoneNumber']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.mileStoneNumber}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="milestoneNameCol">
						<f:facet name="header">
							<t:outputText value="#{msg['projectMilestone.milestoneName']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.mileStoneName}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="milestoneDescriptionCol">
						<f:facet name="header">
							<t:outputText value="#{msg['projectMilestone.milestoneDescription']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.description}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="completionPercentageCol">
						<f:facet name="header">
							<t:outputText value="#{msg['projectMilestone.completionPercentage']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.completionPercentage}" styleClass="A_RIGHT_NUM" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="completionDateCol">
						<f:facet name="header">
							<t:outputText value="#{msg['projectMilestone.completionDate']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.completionDate}" style="white-space: normal;">
							<f:convertDateTime pattern="#{pages$projectMilestonesTab.dateFormat}" timeZone="#{pages$projectMilestonesTab.timeZone}" />
						</t:outputText>
					</t:column>
					<t:column width="120" rendered = "#{pages$projectMilestonesTab.canAddMilestone}">
						<f:facet name="header">
							<t:outputText value="#{msg['commons.action']}"/>
						</f:facet>
						<pims:security screen="Pims.ConstructionMgmt.ProjectMilestones.EditMilestone" action="create">
							<t:commandLink actionListener="#{pages$projectMilestonesTab.editMilestone}" >
								<h:graphicImage title="#{msg['commons.edit']}" url="../resources/images/edit-icon.gif" />
							</t:commandLink>
						</pims:security>
						<t:outputLabel value=" "></t:outputLabel>
						<pims:security screen="Pims.ConstructionMgmt.ProjectMilestones.DeleteMilestone" action="create">
							<t:commandLink actionListener="#{pages$projectMilestonesTab.deleteMilestone}"
										   onclick="if (!confirm('#{msg['projectMilestone.messages.confirmDelete']}')) return">
								<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />
							</t:commandLink>
						</pims:security>
					</t:column>
				</t:dataTable>
			</t:div>
			<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99.1%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']}"/>
						<h:outputText value=" : "/>
						<h:outputText value="#{pages$projectMilestonesTab.recordSize}" />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="milestoneScroller" for="milestoneTable" paginator="true"  
						fastStep="1" paginatorMaxPages="5" immediate="false"
						paginatorTableClass="paginator"
						renderFacetsIfSinglePage="true" 												
						paginatorTableStyle="grid_paginator" layout="singleTable" 
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" 
						pageIndexVar="milstonePageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
							<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.milstonePageNumber}"/>
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
		</t:div>
		</t:panelGrid>
	</t:div>