<%-- 
  - Author: Muhammad Ahmed
  - Date: 23/12/2010
  - Description: Used for Displaying Takharuj Effects 
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function openSearchPersonPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	   window.open('SearchPerson.jsf?viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');	    
	}
	function closeWindow()
		{
		  window.close();
		}
	
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		document.getElementById('frm:selectedToPersonId').value = personId; 
		document.getElementById('frm:lnkReceiveToBeneficiary').onclick();
	}
	
	function closeAndPassTakharujInheritedAssetView()
	{
		window.opener.receiveTakharujInheritedAssetView();
		window.close();
	}	
	
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				
				<tr>
					<td width="100%" valign="top" class="divBackgroundBody">
						<table width="100%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['takharujDetails.takharujEffects']}" styleClass="HEADER_FONT" />
								</td>								
							</tr>
						</table>
						<table width="100%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">								
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" >
										<h:form id="frm" style="width:97%" enctype="multipart/form-data">
											<table border="0" class="layoutTable">
												<tr>
													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$TakharujEffect.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$TakharujEffect.successMessages}" />													    
													</td>
												</tr>
											</table>
											
											
											
											<t:div styleClass="contentDiv" style="width:99%">																
												<t:dataTable id="takharujDetailViewDataTable"  
															rows="#{pages$TakharujEffect.paginatorRows}"
															value="#{pages$TakharujEffect.takharujEffectViewList}"
															binding="#{pages$TakharujEffect.takharujDetailViewDataTable}"																	
															preserveDataModel="false" preserveSort="false" var="dataItem"
															rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
																												
													<t:column width="20%" sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['takharujDetails.beneficiaryName']}" />
														</f:facet>
														<t:outputText  value="#{dataItem.beneficiary}" />
													</t:column>
																																													
													<t:column width="15%" sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['takharujDetails.currentShare']}" />
														</f:facet>
														<t:outputText  value="#{dataItem.currentShare}" />																		
													</t:column>
													
													<t:column width="15%" sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['takharujDetails.afterTakharujShare']}" />
														</f:facet>
														<t:outputText  value="#{dataItem.afterTakharujShare}" />																		
													</t:column>
																												
												</t:dataTable>										
											</t:div>
											<br/>
										
										<t:div styleClass="BUTTON_TD">
											<h:commandButton styleClass="BUTTON"
																	value="#{msg['generateFloors.Close']}"
																	onclick="javascript:window.close();"
																	 />
																	</t:div>
											
										<%--	<t:div id="takharujDetailViewGridInfo" styleClass="contentDivFooter" style="width:100%">
												<t:panelGrid columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
													<t:div styleClass="RECORD_NUM_BG">
														<t:panelGrid columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
															<h:outputText value="#{msg['commons.recordsFound']}"/>
															<h:outputText value=" : "/>
															<h:outputText value="#{pages$TakharujEffect.takharujDetailViewListRecordSize}"/>																						
														</t:panelGrid>
													</t:div>
																															  
													<CENTER>
														<t:dataScroller for="takharujDetailViewDataTable" paginator="true"
																		fastStep="1"  
																		paginatorMaxPages="#{pages$TakharujEffect.paginatorMaxPages}" 
																		immediate="false"
																		paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true" 
																		pageIndexVar="pageNumber"
																		styleClass="SCH_SCROLLER"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false" 
																		paginatorTableStyle="grid_paginator" layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;">
																			
																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
																		</f:facet>
																				
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
																		</f:facet>
																				
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
																		</f:facet>
																				
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
																		</f:facet>
																				
																		<t:div styleClass="PAGE_NUM_BG">
																			<t:panelGrid styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0">
																				<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																				<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																			</t:panelGrid>																							
																		</t:div>
														</t:dataScroller>
													</CENTER>																		      
												</t:panelGrid>
											</t:div>--%>
									    </h:form>
									</div>									
								</td>								
							</tr>
						</table>
					</td>
				</tr>				
			</table>
           </div>
		</body>
	</html>
</f:view>