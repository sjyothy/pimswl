<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">	
</script>

<t:panelGrid rendered="#{pages$reviewDetailsTab.isTabModeUpdatable}"
	styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%"
	cellspacing="5px" columns="4">
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['reviewDetailsTab.request']}:" />
	</h:panelGroup>
	<h:inputTextarea readonly="true" styleClass="READONLY"
		value="#{pages$reviewDetailsTab.reviewRequestView.rfc}"
		style="width: 185px;" />

	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['reviewDetailsTab.review']}:" />
	</h:panelGroup>
	<h:inputTextarea
		value="#{pages$reviewDetailsTab.reviewRequestView.reviewDesc}"
		style="width: 185px;" />

	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['reviewDetailsTab.decision']}:" />
	</h:panelGroup>
	<h:selectOneMenu
		value="#{pages$reviewDetailsTab.reviewRequestView.decision}">
		<f:selectItem itemValue="1" itemLabel="#{msg['commons.agree']}" />
		<f:selectItem itemValue="0" itemLabel="#{msg['commons.disagree']}" />
		<f:selectItem itemValue="2"
			itemLabel="#{msg['researchFormBenef.others']}" />
	</h:selectOneMenu>

	<h:outputLabel style="font-weight:normal;"
		value="#{msg['accountManage.accountDescription']} :">
	</h:outputLabel>
	<h:inputText binding="#{pages$reviewDetailsTab.htmlOtherDescision}"></h:inputText>
</t:panelGrid>

<t:panelGrid rendered="#{pages$reviewDetailsTab.isTabModeUpdatable}"
	styleClass="BUTTON_TD" cellpadding="1px" width="100%" cellspacing="5px">
	<h:panelGroup>
		<h:commandButton styleClass="BUTTON"
			value="#{msg['commons.saveButton']}"
			action="#{pages$reviewDetailsTab.onSave}" />
	</h:panelGroup>
</t:panelGrid>

<t:div styleClass="contentDiv" style="width:98%">
	<t:dataTable id="reviewRequestViewDataTable"
		rows="#{pages$reviewDetailsTab.paginatorRows}"
		value="#{pages$reviewDetailsTab.reviewRequestViewList}"
		binding="#{pages$reviewDetailsTab.reviewRequestViewDataTable}"
		preserveDataModel="false" preserveSort="false" var="dataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">

		<t:column width="12%" sortable="true" id ="requestOn">
			<f:facet name="header" >
				<t:outputText value="#{msg['reviewDetailsTab.requestOn']}">
					<f:convertDateTime timeZone="#{pages$reviewDetailsTab.timeZone}"
						pattern="#{pages$reviewDetailsTab.dateFormat}" />
				</t:outputText>
			</f:facet>
			<t:outputText value="#{dataItem.createdOn}"  id ="requestOntxt"/>
		</t:column>
		<t:column width="12%" sortable="true" id ="sendtocol">
			<f:facet name="header" >
				<t:outputText value="#{msg['mems.payment.request.label.sendTo']}" />
			</f:facet>
			<t:outputText value="#{dataItem.groupName}" id ="sendtogrp"/>
		</t:column>

		<t:column width="12%" sortable="true" id ="requestby">
			<f:facet name="header" >
				<t:outputText value="#{msg['reviewDetailsTab.requestBy']}"  id ="requestbylbl"/>
			</f:facet>
			<t:outputText value="#{dataItem.createdBy}" id ="requestbytxt" />
		</t:column>

		<t:column width="20%" sortable="true" id ="rfccol">
			<f:facet name="header" >
				<t:outputText value="#{msg['reviewDetailsTab.request']}" id ="rfccollbl"/>
			</f:facet>
			<t:outputText value="#{dataItem.rfc}" id ="rfccoltxt"/>
		</t:column>

		<t:column width="12%" sortable="true" id ="reviewoncol">
			<f:facet name="header">
				<t:outputText value="#{msg['reviewDetailsTab.reviewOn']}" id ="reviewoncollbl">
					<f:convertDateTime timeZone="#{pages$reviewDetailsTab.timeZone}"
						pattern="#{pages$reviewDetailsTab.dateFormat}" />
				</t:outputText>
			</f:facet>
			<t:outputText value="#{dataItem.reviewedOn}" id ="reviewontxt"/>
		</t:column>

		<t:column width="12%" sortable="true" id ="reviewbycol">
			<f:facet name="header">
				<t:outputText value="#{msg['reviewDetailsTab.reviewBy']}" id ="reviewbylbl"/>
			</f:facet>
			<t:outputText value="#{dataItem.reviewedBy}" id ="reviewbycltxt"/>
		</t:column>

		<t:column width="20%" sortable="true" id ="reviewcol">
			<f:facet name="header">
				<t:outputText value="#{msg['reviewDetailsTab.review']}" id ="reviewcollbl"/>
			</f:facet>
			<t:outputText value="#{dataItem.reviewDesc}" id ="reviewcoltxt"/>
		</t:column>

		<t:column width="11%" sortable="true" id ="reviewdeccol">
			<f:facet name="header">
				<t:outputText value="#{msg['reviewDetailsTab.decision']}" id ="reviewdeccollbl"/>
			</f:facet>
			<%--<t:outputText value="#{empty dataItem.decision ? '' : (dataItem.decision == '1' ? msg['commons.agree'] : msg['commons.disagree'])}" />--%>
			<t:outputText id ="reviewdectxt"
				value="#{empty dataItem.decision? '':
			(dataItem.decision == '2' ? dataItem.descisonDesc : (dataItem.decision == '1' ? msg['commons.agree'] : msg['commons.disagree']))
			}" />

		</t:column>
	</t:dataTable>
</t:div>

<t:div id="reviewRequestViewGridInfo" styleClass="contentDivFooter"
	style="width:99%">
	<t:panelGrid columns="2" cellpadding="0" cellspacing="0" width="100%"
		columnClasses="RECORD_NUM_TD,BUTTON_TD">
		<t:div styleClass="RECORD_NUM_BG">
			<t:panelGrid columns="3"
				columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
				cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
				<h:outputText value="#{msg['commons.recordsFound']}" />
				<h:outputText value=" : " />
				<h:outputText
					value="#{pages$reviewDetailsTab.reviewRequestViewListRecordSize}" />
			</t:panelGrid>
		</t:div>

		<CENTER>
			<t:dataScroller for="reviewRequestViewDataTable" paginator="true"
				fastStep="1"
				paginatorMaxPages="#{pages$reviewDetailsTab.paginatorMaxPages}"
				immediate="false" paginatorTableClass="paginator"
				renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
				styleClass="SCH_SCROLLER"
				paginatorActiveColumnStyle="font-weight:bold;"
				paginatorRenderLinkForActive="false"
				paginatorTableStyle="grid_paginator" layout="singleTable"
				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;">

				<f:facet name="first">
					<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
				</f:facet>

				<f:facet name="fastrewind">
					<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
				</f:facet>

				<f:facet name="fastforward">
					<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
				</f:facet>

				<f:facet name="last">
					<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
				</f:facet>

				<t:div styleClass="PAGE_NUM_BG">
					<t:panelGrid styleClass="PAGE_NUM_BG_TABLE" columns="2"
						cellpadding="0" cellspacing="0">
						<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}" />
						<h:outputText styleClass="PAGE_NUM"
							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
							value="#{requestScope.pageNumber}" />
					</t:panelGrid>
				</t:div>
			</t:dataScroller>
		</CENTER>
	</t:panelGrid>
</t:div>
