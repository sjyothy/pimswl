<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="com.avanza.core.util.Logger"%>
<%@page import="com.crystaldecisions.report.web.viewer.CrystalReportViewer"%>
<%@page import="com.crystaldecisions.sdk.occa.report.reportsource.IReportSource"%>>
<%@page import="com.crystaldecisions.report.web.viewer.CrPrintMode"%>
<%@page import="com.avanza.pims.report.constant.ReportConstant"%>
<%@page import="com.avanza.pims.report.POJOReports.POJOReportManager"%>
<%@page import="com.avanza.pims.report.POJOReports.POJOReportSpecification"%>

<%
	Logger logger = Logger.getLogger(this.getClass());
	try
	{	
		logger.logInfo(" --- Started --- ");
		POJOReportSpecification pojoReportSpecification = (POJOReportSpecification) session.getAttribute( ReportConstant.Keys.POJO_REPORT_DATASOURCE_KEY );
		POJOReportManager pojoReportManager = new POJOReportManager( pojoReportSpecification );
		IReportSource rptSource = pojoReportManager.getReportSource();
		CrystalReportViewer crystalReportPageViewer = new CrystalReportViewer();
		crystalReportPageViewer.setReportSource(rptSource);
		crystalReportPageViewer.setPrintMode( CrPrintMode.ACTIVEX );
        crystalReportPageViewer.setHasToggleGroupTreeButton(false);
        crystalReportPageViewer.setDisplayGroupTree(false);
        crystalReportPageViewer.setEnableDrillDown(false);
        crystalReportPageViewer.setEnableLogonPrompt( false );
        crystalReportPageViewer.setHasPrintButton(true);
        crystalReportPageViewer.setHasPageBottomToolbar(false);
        crystalReportPageViewer.setOwnPage(true);
        crystalReportPageViewer.setOwnForm(true);        
		crystalReportPageViewer.processHttpRequest(request, response, session.getServletContext(), out); 
		logger.logInfo(" --- Successfully Completed --- "); 
	}
	catch ( Exception e )
	{
		logger.LogException(" --- Exception --- ", e);
	}	

%>
