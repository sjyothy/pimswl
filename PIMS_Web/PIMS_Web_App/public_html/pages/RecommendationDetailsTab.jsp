<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%-- Please remove above taglibs after completing your tab contents--%>
<t:div styleClass="contentDiv" style="width:95%">


	<t:dataTable id="researchRecomm"
		rows="10"
		value="#{pages$RecommendationDetailsTab.recommendationList}"
		 preserveDataModel="false"
		preserveSort="false" var="dataItem" rowClasses="row1,row2" rules="all"
		renderedIfEmpty="true" width="100%">
		
		<t:column id="col1" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['recommenationDetils.caringType']}"
					style="white-space: normal;" />
			</f:facet>

			<t:outputText style="white-space: normal;"
				value="#{pages$RecommendationDetailsTab.isEnglishLocale?dataItem.caringType.caringTypeNameEn:dataItem.caringType.caringTypeNameAr}" />
		</t:column>
		<t:column id="colRcmdn" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['recommenationDetils.recommendationNo']}"
					style="white-space: normal;" />
			</f:facet>

			<t:outputText style="white-space: normal;"
				value="#{dataItem.recommendationNumber}" />
		</t:column>
	
		<t:column id="col2" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['recommenationDetils.status']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				value="#{pages$RecommendationDetailsTab.isEnglishLocale?dataItem.statusEn:dataItem.statusAr}" />
		</t:column>
		<t:column id="col3" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['recommenationDetils.DisbursementReq']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText
				value="#{dataItem.disbursementRequired=='1'?msg['label.Disbursement.required']:msg['label.Disbursement.notRequired']}"
				style="white-space: normal;" />
		</t:column>

		<t:column id="col4" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['recommenationDetils.DisbursementList']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText
				value="#{pages$RecommendationDetailsTab.isEnglishLocale?dataItem.disbursementListTypeEn:dataItem.disbursementListTypeAr}"
				style="white-space: normal;" />
		</t:column>



		<t:column id="col5" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['disType.urgent']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText
				value="#{dataItem.isPeriodic== '1'?msg['commons.no']:msg['commons.yes']}"
				style="white-space: normal;" />
		</t:column>

		<t:column id="col6" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['recommenationDetils.period']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText
				value="#{pages$RecommendationDetailsTab.isEnglishLocale?dataItem.durationFreqEn:dataItem.durationFreqAr}"
				style="white-space: normal;" />
		</t:column>
		<t:column id="col7" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['recommenationDetils.fromDate']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText value="#{dataItem.startDate}"
				style="white-space: normal;" />
		</t:column>
		<t:column id="col8" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['recommenationDetils.ToDate']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText value="#{dataItem.endDate}"
				style="white-space: normal;" />
		</t:column>
		<t:column id="col9" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['recommenationDetils.disburseSrc']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText
				value="#{pages$RecommendationDetailsTab.isEnglishLocale?dataItem.disburseSrc.srcNameEn:dataItem.disburseSrc.srcNameAr}"
				style="white-space: normal;" />
		</t:column>
		<t:column id="colAmont" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['recommenationDetils.amount']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText value="#{dataItem.amount}" style="white-space: normal;" />
		</t:column>
		<t:column id="col11" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['recommenationDetils.comments']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText value="#{dataItem.comments}"
				style="white-space: normal;" />
		</t:column>
		


	</t:dataTable>

</t:div>
<%--Column 3,4 Ends--%>
