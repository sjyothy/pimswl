<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript" src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript"> 
         	           
         function showSearchConstructionTenderPopUp()
	     {
	     
	     var  screen_width = 1024;
	     var screen_height = 450;
	     window.open('constructionTenderSearch.jsf?VIEW_MODE=popup&context=TenderInquiry','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=20,top=150,scrollbars=yes,status=yes');
       	}
	      
	      function showSearchTenderPopUp()
		   {
			var screen_width = screen.width;
			var screen_height = screen.height;
			var popup_width = screen_width-180;
			var popup_height = screen_height-240;
			var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
			
			var popup = window.open('tenderSearch.jsf?VIEW_MODE=popup&context=TenderInquiry','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
			popup.focus();
		   }

		function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		
		function populateContractor(contractorId, contractorNumber, contractorNameEn, contractorNameAr, licenseNumber, licenseSource, officeNumber){
			document.getElementById("detailsFrm:contractorId").value=contractorId;
		    document.forms[0].submit();
		}
		
		function populateTender(tenderId,tenderNumber,tenderDescription)
		{
			 document.getElementById("detailsFrm:tenderId").value=tenderId;
		     document.forms[0].submit();
		}
		
		function submitForm()
		{
			window.document.forms[0].submit();
		}	
	 
			

</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 <script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
	 

	
</head>
	<body class="BODY_STYLE">
	   <div class="containerDiv">
	<%	
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header --> 
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="2">
				<jsp:include page="header.jsp" />
			</td>
		</tr>
		<tr width="100%">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{pages$extendApplicationDetails.heading}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>
				<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%">
					<tr valign="top">
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
						<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
							<div class="SCROLLABLE_SECTION" style="height:475px;width:100%;#height:466px;">
									<h:form id="detailsFrm" enctype="multipart/form-data" style="WIDTH: 96%;">
									<div >
										<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
											<tr>
												<td>
													<h:outputText value="#{pages$extendApplicationDetails.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$extendApplicationDetails.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
									</div>
								<div> 
								<h:inputHidden id="contractorId" value="#{pages$extendApplicationDetails.hiddenContractorId}"></h:inputHidden>
								<h:inputHidden id="tenderId" value="#{pages$extendApplicationDetails.hiddenTenderId}"></h:inputHidden>
						<t:div styleClass="TAB_DETAIL_SECTION" style="width:97%; margin:10px;" rendered="#{pages$extendApplicationDetails.showActionPanel}">
							<t:panelGrid id="actionGrid" cellpadding="1px" width="100%" cellspacing="5px" styleClass="TAB_DETAIL_SECTION_INNER" columns="4">
											
											<h:outputLabel id="newClosureDateLabel" styleClass="LABEL" value="#{msg['extendApplication.details.newClosureDate']}: " rendered="#{pages$extendApplicationDetails.showClosureDate}" />
						                    <rich:calendar id="newClosureDate" value="#{pages$extendApplicationDetails.newClosureDate}" inputStyle="width: 186px; height: 18px" locale="#{pages$extendApplicationDetails.locale}" popup="true" datePattern="#{pages$extendApplicationDetails.dateFormat}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px" rendered="#{pages$extendApplicationDetails.showClosureDate}" disabled="#{pages$extendApplicationDetails.newClosureDateReadonlyMode}"/>
						                    <h:outputLabel id="followUpDateLabel"styleClass="LABEL" value="#{msg['extendApplication.details.followUpDate']}: " rendered="#{pages$extendApplicationDetails.showFollowUpDate}" />
						                    <rich:calendar id="followUpDate" value="#{pages$extendApplicationDetails.followUpDate}" inputStyle="width: 186px; height: 18px" locale="#{pages$extendApplicationDetails.locale}" popup="true" datePattern="#{pages$extendApplicationDetails.dateFormat}" showApplyButton="false" enableManualInput="false" rendered="#{pages$extendApplicationDetails.showFollowUpDate}" disabled="#{pages$extendApplicationDetails.followUpDateReadonlyMode}"/>
						                   <h:panelGroup>	
										      <h:outputLabel styleClass="mandatory" value="*"/>	 
									        <h:outputLabel id="remarksLabel" styleClass="LABEL"  value="#{msg['commons.remarks']}: " rendered="#{pages$extendApplicationDetails.showRemarks}"></h:outputLabel>
									        </h:panelGroup>
									        <t:inputTextarea styleClass="TEXTAREA" value="#{pages$extendApplicationDetails.remarks}" rows="6" style="width: 200px;" />
											<h:outputLabel id="label1" value=" "/>
											<t:panelGroup rendered="#{pages$extendApplicationDetails.showApprove}">
												<h:selectBooleanCheckbox  value="#{pages$extendApplicationDetails.publish}" valueChangeListener="#{pages$extendApplicationDetails.doPublish}"></h:selectBooleanCheckbox>
										        <t:outputText value="#{msg['extendApplication.details.messages.publishCheckBoxLabel']}" />
											</t:panelGroup>
											<t:panelGroup rendered="#{!pages$extendApplicationDetails.showApprove}">
											</t:panelGroup>
											<h:outputLabel id="label2" value=" "/>
											<h:outputLabel id="label3" value=" "/>
											
											<h:outputLabel id="label4" value=" "/>
											<t:panelGroup rendered="#{pages$extendApplicationDetails.showPublish || pages$extendApplicationDetails.showApprove}">
												<h:selectBooleanCheckbox  value="#{pages$extendApplicationDetails.closeRelated}" valueChangeListener="#{pages$extendApplicationDetails.doCloseRelated}" ></h:selectBooleanCheckbox>
										        <t:outputText value="#{msg['extendApplication.details.messages.closeRelatedApplications']}" />
											</t:panelGroup>
											<t:panelGroup rendered="#{!(pages$extendApplicationDetails.showPublish || pages$extendApplicationDetails.showApprove)}">
											</t:panelGroup>
											<h:outputLabel id="label5" value=" "/>
											<h:outputLabel id="label6" value=" "/>
											
											<t:div style="height:10px;"/>
						   	</t:panelGrid>	              
						</t:div>		
						<div class="AUC_DET_PAD">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
						<div class="TAB_PANEL_INNER"> 
							<rich:tabPanel binding="#{pages$extendApplicationDetails.tabPanel}" id="tabPanel" switchType="server" style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%" >
								<rich:tab id="requestDetailsTab" label="#{msg['requestDetailsTab.header']}">
									<%@  include file="extendRequestTab.jsp"%>
								</rich:tab>
							    <rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}">
									<%@  include file="attachment/attachment.jsp"%>
								</rich:tab>
								<rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}">
									<%@ include file="notes/notes.jsp"%>
								</rich:tab>
								<rich:tab label="#{msg['commons.tab.requestHistory']}" action="#{pages$extendApplicationDetails.requestHistoryTabClick}" rendered="#{pages$extendApplicationDetails.showHistory}">
	                                <%@ include file="../pages/requestTasks.jsp"%>
								</rich:tab>
							</rich:tabPanel>
							</div>
							<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT" /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID" style="width:100%;" /></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
							</table>
						<t:div styleClass="BUTTON_TD" style="padding-top:10px;">
							<t:commandButton styleClass="BUTTON" 
								value="#{msg['commons.cancel']}"
								onclick="if (!confirm('#{msg['extendApplication.details.confirm.cancel']}')) return false" 
					        	action="#{pages$extendApplicationDetails.cancel}"
					        	tabindex="12" 
								rendered="#{pages$extendApplicationDetails.showCancel}" 
					        	>
					       	</t:commandButton>
							<pims:security screen="Pims.Miscellaneous.ExtendApplicationDetails.SaveRequest" action="create">
						 		<t:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" action="#{pages$extendApplicationDetails.saveExtendRequest}" tabindex="13" rendered="#{pages$extendApplicationDetails.showSave}"/>
							</pims:security>
							<pims:security screen="Pims.Miscellaneous.ExtendApplicationDetails.SaveFollowUp" action="create">
						 		<t:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" action="#{pages$extendApplicationDetails.completeFollowUp}" tabindex="14" rendered="#{pages$extendApplicationDetails.showFollowUpSave}"/>
							</pims:security>
							<pims:security screen="Pims.Miscellaneous.ExtendApplicationDetails.SubmitRequest" action="create">
								<t:commandButton styleClass="BUTTON" value="#{msg['commons.submitButton']}" onclick="if (!confirm('#{msg['extendApplication.details.confirm.submit']}')) return false" action="#{pages$extendApplicationDetails.done}" tabindex="15" rendered="#{pages$extendApplicationDetails.showSubmit}" />
							</pims:security>
							<pims:security screen="Pims.Miscellaneous.ExtendApplicationDetails.PublishRequest" action="create">
								<t:commandButton styleClass="BUTTON" value="#{msg['advertisement.publish']}" onclick="if (!confirm('#{msg['extendApplication.details.confirm.publish']}')) return false" action="#{pages$extendApplicationDetails.publishRequest}" tabindex="16" rendered="#{pages$extendApplicationDetails.showPublish}" />
							</pims:security>
							<pims:security screen="Pims.Miscellaneous.ExtendApplicationDetails.CancelRequest" action="create">
								<t:commandButton styleClass="BUTTON" value="#{msg['common.cancelRequest']}" onclick="if (!confirm('#{msg['extendApplication.details.confirm.cancelRequest']}')) return false" action="#{pages$extendApplicationDetails.cancelRequest}" tabindex="17" rendered="#{pages$extendApplicationDetails.showCancelRequest}" style="width: 95px;"/>
							</pims:security>
							<pims:security screen="Pims.Miscellaneous.ExtendApplicationDetails.RejectRequest" action="create">
								<t:commandButton styleClass="BUTTON" value="#{msg['commons.reject']}" onclick="if (!confirm('#{msg['extendApplication.details.confirm.reject']}')) return false" action="#{pages$extendApplicationDetails.rejectRequest}" tabindex="18" rendered="#{pages$extendApplicationDetails.showReject}" />
							</pims:security>
							<pims:security screen="Pims.Miscellaneous.ExtendApplicationDetails.ApproveRequest" action="create">
								<t:commandButton styleClass="BUTTON" value="#{msg['commons.approve']}" onclick="if (!confirm('#{msg['extendApplication.details.confirm.approve']}')) return false" action="#{pages$extendApplicationDetails.approveRequest}" tabindex="19" rendered="#{pages$extendApplicationDetails.showApprove}" />
							</pims:security>
						</t:div>
						</div>
								</div>
							</h:form>
					  </div> 
						</td>			
					</tr>
				</table>
			</td>
		</tr>
		<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
	</table>
	</div>
</body>
</html>
</f:view>