
<script type="text/javascript">
	function   showPersonPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	   var isSelectBlackList = document.getElementById(document.forms[0].name+":hdnIsSelectBlackList").value;
	   if(isSelectBlackList =='0')
	     window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	   else if(isSelectBlackList =='1')
	     window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup&SELECT_BLACK_LIST=true','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
	
	function populateApplicantInParentPerson( control )
	{	
	    var personId  = control.options[control.selectedIndex].value;
	    
		populatePerson('APPLICANT','',personId,'','','','');    			 
	}
						
</script>

<t:div style="width:100%" styleClass="TAB_DETAIL_SECTION">


	<t:panelGrid id="applicantDetailsTable" cellpadding="1px"
		width="100%" cellspacing="5px" columns="4"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:outputLabel styleClass="LABEL"
			value="#{msg['lbl.organization']}:" />
		<h:inputText 
			value="#{pages$ApplicantDetails.txtOrganization}"
			></h:inputText>
			
                
		<h:outputLabel styleClass="LABEL"
			value="#{msg['property.emirates']}:"></h:outputLabel>
			
		<h:selectOneMenu 
			value="#{pages$ApplicantDetails.stateId}"
			tabindex="3">
			<f:selectItem itemLabel="#{msg['commons.All']}"
				itemValue="-1" />
			<f:selectItems
				value="#{pages$ApplicationBean.emiratesList}" />
		</h:selectOneMenu>
																		
			
			
		<h:outputLabel styleClass="LABEL"
			value="#{msg['commons.lbl.requester']}:"></h:outputLabel>
		<h:inputText 
			value="#{pages$ApplicantDetails.txtRequester}" />
			
			
		<h:outputLabel styleClass="LABEL"
			value="#{msg['commons.lbl.jobTitle']}:"></h:outputLabel>
		<h:inputText 
			value="#{pages$ApplicantDetails.txtJobTitle}" />
		
		
	</t:panelGrid>
</t:div>
