
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<SCRIPT>	
function closeWindow()
	{
	  //window.opener.document.getElementById('conductAuctionForm:isResultPopupCalled').value = "Y";
	  window.opener.document.forms[0].submit();
	  window.close();
	}
       </SCRIPT>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />

			<style>
		span{
			PADDING-RIGHT: 5px; 
			PADDING-LEFT: 5px;
		}
		</style>
			<script language="javascript" type="text/javascript">
	function clearWindow()
	{
        
            document.getElementById("formDuplicationFloorsPopup:txtFloorPrefix").value="";
			document.getElementById("formDuplicationFloorsPopup:txtStartNo").value="";	
			document.getElementById("formDuplicationFloorsPopup:txtEndNo").value="";
			window.close();
			
	}
	

	
</script>



		</head>
		<body dir="${sessionScope.CurrentLocale.dir}"
			lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<tr width="100%">

					<td width="100%" valign="top" class="divBackgroundBody"
						height="610px">
						<table width="100%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr height="1%">
								<td>
									<table width="100%" class="greyPanelTable" cellpadding="0"
										cellspacing="0" border="0">
										<tr>
											<td class="HEADER_TD">
												<h:outputLabel value="#{msg['receiveProperty.floor.duplicateFloor']}"
													styleClass="HEADER_FONT" />
											</td>
											<td width="100%">
												&nbsp;
											</td>
										</tr>

									</table>

									<table width="100%" class="greyPanelMiddleTable"
										cellpadding="0" cellspacing="0" border="0">
										<tr valign="top">
											<td height="100%" valign="top" nowrap="nowrap"
												background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
												width="1">
											</td>

											<td width="100%" height="576px" valign="top" nowrap="nowrap">




												<div>
													<h:form id="formDuplicationFloorsPopup" style="width:90%">


												<table border="0" class="layoutTable"
													style="margin-left: 10px; margin-right: 10px;">
													<tr>
														<td>
															<h:outputText
																value="#{pages$DuplicateFloorsPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />

														</td>
													</tr>
												</table>
												</div>
														<div class="MARGIN">
															
															<div >
																<table cellpadding="1px" cellspacing="2px"
																	>



																	<tr>

																		<td width="20%" >
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.floor.floorPrefix']}:"></h:outputLabel>
																		</td>
																		<td width="20%">
																			<h:inputText style="width:155px;" id="txtFloorPrefix"
																				binding="#{pages$DuplicateFloorsPopup.floorPrefix}">
																			</h:inputText>
																		</td>
																		<td>&nbsp;</td><td>&nbsp;</td>
</tr><tr>
																		<td width="20%">
																																				<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.floor.startNumber']}:"></h:outputLabel></t:panelGroup>
																		</td>
																		<td width="20%">
																			<h:inputText style="width:155px;" id="txtStartNo"
																				binding="#{pages$DuplicateFloorsPopup.startNumber}">
																			</h:inputText>
																		</td>



																		<td width="20%">
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.floor.endNumber']}:"></h:outputLabel></t:panelGroup>
																		</td>
																		<td width="20%">
																			<h:inputText style="width:155px;"
																				id="txtEndNo"
																				binding="#{pages$DuplicateFloorsPopup.endNumber}">
																			</h:inputText>
																		</td>



																	</tr>

																	<tr>
																		<td colspan="2">
																			<h:selectBooleanCheckbox
																				binding="#{pages$DuplicateFloorsPopup.addWithUnits}"
																				value="#{pages$DuplicateFloorsPopup.addWithUnitsVar}">
																				<h:outputText
																					value="#{msg['receiveProperty.floor.addWithUnits']}" />
																			</h:selectBooleanCheckbox>
																		</td>
																	</tr>
																	<tr>

																		<td colspan="6" class="BUTTON_TD">
																			<h:commandButton type="submit" styleClass="BUTTON" style="width: 100px;"
																				value="#{msg['receiveProperty.floor.generateFloors']}"
																				action="#{pages$DuplicateFloorsPopup.duplicateFloors}"></h:commandButton>


																			<h:commandButton styleClass="BUTTON" type="button"
																				value="#{msg['commons.cancel']}"
																				onclick="javascript:clearWindow();" />

																		</td>
																	</tr>
															
																</table>
															</div>
														</div>


													
															
													</h:form>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
		</body>
</f:view>
</html>
