<%-- 
  - Author: Kamran Ahmed
  - Date:
  - Copyright Notice:
  - @(#)\
  - Description: This report will show the details of checks details matching the following criteria provided as an input.
  --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%-- 
		<% UIViewRoot viewRoot = (UIViewRoot)session.getAttribute("view");
           if (viewRoot.getLocale().toString().equals("en"))
           { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>
        <%
            } 
            else
            { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_ar.js"></script>
        <%
            } 
        %>
--%>

<script type="text/javascript">

	function openLoadReportPopup(pageName) 
	{	
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />


			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
				 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
		

		</head>

		<body>
		<div style="width:1024px;">
			

			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
						   
							<td colspan="2">
						        <jsp:include page="header.jsp"/>
						     </td>
						    
							
		       </tr>
		       <tr width="100%">						
						<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
						</td>					    
						<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['report.chequeDetailsReport']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="0%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									>
								</td>
								<td width="100%" height="99%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:450px;">
										<h:form id="searchFrm" >
											
											<div class="MARGIN" style="width:96%">
												<table cellpadding="0" cellspacing="0" >
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
													<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width:99.6%;">
             		<h:outputLabel value="#{msg['commons.report.criteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>				
							<table cellpadding="0px" cellspacing="0px" class="DETAIL_SECTION_INNER" >
			
      									  
												
									<tr><td><h:outputLabel styleClass="LABEL" value="#{msg['tenants.id']} :"/></td>
									<td><h:inputText id="txtTenantId" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.tenantId}" styleClass="INPUT_TEXT" maxlength="20"/></td>
									<td><h:outputLabel styleClass="LABEL" value="#{msg['tenants.type']} :"/></td>
									<td><h:selectOneMenu id="cboTenantType" styleClass="SELECT_MENU" required="false" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.tenantType}">

																				<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}"/>
																				<f:selectItems value="#{pages$ApplicationBean.tenantType}"/>
																			</h:selectOneMenu></td>
									</tr>
									<tr>
									   <td>
										 
									  <h:outputLabel styleClass="LABEL" value="#{msg['tenants.name']} :"></h:outputLabel></td>
									  <td >
									    
													
									  <h:inputText id="txtTenantName" styleClass="INPUT_TEXT" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.tenantName}">
									        </h:inputText></td>
									  
									  <td>
										 
									  <h:outputLabel styleClass="LABEL" value="#{msg['chequeList.paymentNumber']} :"></h:outputLabel></td>
									  <td >
									    
													
									  <h:inputText id="txtPaymentNumber" styleClass="INPUT_TEXT" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.paymentNumber}">
									        </h:inputText></td>
									</tr>		
									
									<tr> 
									<td><h:outputLabel styleClass="LABEL" value="#{msg['grp.paymentType']} :"></h:outputLabel>
									 
									 </td>
									 <td  >
									 
									<h:selectOneMenu id="selectPaymentType" styleClass="SELECT_MENU" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.paymentType}">
									       <f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1" />
									       <f:selectItems value="#{pages$ApplicationBean.paymentTypeList}" />
										</h:selectOneMenu></td>
									<td>
										 
									  <h:outputLabel styleClass="LABEL" value="#{msg['chequeList.message.paymentStatus']} :"></h:outputLabel></td>
									  <td >
									    
													
									  <h:selectOneMenu id="selectPaymentStatus" styleClass="SELECT_MENU" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.paymentStatus}">
									       <f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1" />
									       <f:selectItems value="#{pages$ApplicationBean.paymentScheduleStatusList}" />
										</h:selectOneMenu></td>	
									</tr>
									<tr>
									    <td>
											
										<h:outputLabel styleClass="LABEL" value="#{msg['chequeList.receiptNumber']} :"></h:outputLabel></td>
										<td><h:inputText id="txtReceiptNumber" styleClass="INPUT_TEXT" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.receiptNumber}">
									        </h:inputText>
										
									        
										</td>
										
										<td>
										
										<h:outputLabel styleClass="LABEL" value="#{msg['chequeList.receiptDate']} :"></h:outputLabel></td>
										<td>
										
										<rich:calendar id="cboReceiptDate" 
										value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.receiptDate}" 
										popup="true" 
										datePattern="#{pages$crChequeDetailsList.dateFormat}" 
										showApplyButton="false" locale="#{pages$crChequeDetailsList.locale}" 
										enableManualInput="false" 
										cellWidth="24px" cellHeight="22px" 
								        style="width:188px; height:16px"
								        inputStyle="width: 186px; height: 18px"
										>
										</rich:calendar></td>
												
												
									</tr>
									<tr>
										<td><h:outputLabel styleClass="LABEL" value="#{msg['property.commercialName']} :"></h:outputLabel>
											
										</td>
										<td>
										
										<h:inputText styleClass="INPUT_TEXT" id="txtPropertyName" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.propertyName}"></h:inputText></td>
										
										<td>
										
										<h:outputLabel styleClass="LABEL" value="#{msg['property.ownership']}"> Type:</h:outputLabel></td>
										<td>
										
										<h:selectOneMenu id="txtPropertyOwnershipType" styleClass="SELECT_MENU" required="false" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.propertyOwnershipType}">
																	<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																	<f:selectItems value="#{pages$ApplicationBean.propertyOwnershipType}" />
																</h:selectOneMenu></td>
									</tr>
									<tr>
									   
										
										
										
									</tr>
									<tr>
									 	<td>
										<h:outputLabel  styleClass="LABEL" value="#{msg['unit.unitNumber']} :"></h:outputLabel>
										</td>
										<td>
										<table  border="0" class="layoutTable">
											<h:inputText   id="txtUnitNumber"  styleClass="INPUT_TEXT" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.unitNumber}">
											</h:inputText>
												 </table>
										</td>
										 <td>
										   
										 <h:outputLabel styleClass="LABEL" value="#{msg['bouncedChequesList.chequeNumberCol']} :"></h:outputLabel></td>
										<td  >
									        
										<h:inputText id="txtChequeNumber" styleClass="INPUT_TEXT" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.chequeNumber}">
									        </h:inputText></td>
										</tr>
										 <tr>  
      								    <td>
										 
										<h:outputLabel styleClass="LABEL" value="#{msg['chequeList.chequeOwnerName']} :"></h:outputLabel></td>
										<td  >
										 
										<h:inputText id="txtOwnerName" styleClass="INPUT_TEXT" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.chequeOwnerName}">
										 </h:inputText></td>
										
										<td>
										 
										<h:outputLabel styleClass="LABEL" value="#{msg['cheque.amount']} :" /></td>
										<td  >
										 
										<h:inputText id="txtChequeAmount" styleClass="INPUT_TEXT" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.chequeAmount}">
									 </h:inputText></td>
      								
      								</tr>
      							
      								<tr>  
      								    <td>
										 
										<h:outputLabel styleClass="LABEL" value="#{msg['chequeList.dueOnFrom']} :"></h:outputLabel></td>
										<td  >
										 
										<rich:calendar id="dueDate" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.dueDate}" 
										popup="true" datePattern="#{pages$crChequeDetailsList.dateFormat}" 
										showApplyButton="false" locale="#{pages$crChequeDetailsList.locale}" 
										enableManualInput="false" 
										cellWidth="24px" cellHeight="22px" 
								        style="width:188px; height:16px"
								        inputStyle="width: 186px; height: 18px"
										>
										</rich:calendar></td>
										
      								    <td>
										 
										<h:outputLabel styleClass="LABEL" value="#{msg['chequeList.dueOnTo']} :"></h:outputLabel></td>
										<td>
										 
										<rich:calendar id="todueDate" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.dueDateTo}" 
										popup="true" datePattern="#{pages$crChequeDetailsList.dateFormat}" 
										showApplyButton="false" locale="#{pages$crChequeDetailsList.locale}" 
										enableManualInput="false" 
										cellWidth="24px" cellHeight="22px" 
								        style="width:188px; height:16px"
								        inputStyle="width: 186px; height: 18px"
										>
										</rich:calendar></td>

      								</tr>
      									<tr>  
      								    <td>
										 
										<h:outputLabel styleClass="LABEL" value="#{msg['contract.startDateFrom']} :"></h:outputLabel></td>
										<td  >
										 
										<rich:calendar id="clndrContrctStartDateFrom" 
										value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.contractStartDateFrom}" 
										popup="true" datePattern="#{pages$crChequeDetailsList.dateFormat}" 
										showApplyButton="false" locale="#{pages$crChequeDetailsList.locale}" 
										enableManualInput="false" 
										cellWidth="24px" cellHeight="22px" 
								        style="width:188px; height:16px"
								        inputStyle="width: 186px; height: 18px"
										>
										</rich:calendar></td>
										
										<td>
										 
										<h:outputLabel styleClass="LABEL" value="#{msg['contract.startDateTo']} :"></h:outputLabel></td>
										<td  >
										 
										<rich:calendar id="clndrContrctStartDateTo" 
										value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.contractStartDateTo}"
										 popup="true" datePattern="#{pages$crChequeDetailsList.dateFormat}" 
										 showApplyButton="false" locale="#{pages$crChequeDetailsList.locale}" 
										 enableManualInput="false" 
										cellWidth="24px" cellHeight="22px" 
								        style="width:188px; height:16px"
								        inputStyle="width: 186px; height: 18px"
										 >
										 </rich:calendar></td>
      								
      								</tr>
      								<tr>  
      								    <td>
										 <h:outputLabel styleClass="LABEL" value="#{msg['date.expiryDateFrom']} :"/>
										</td>
										<td  >
										 
										<rich:calendar id="expiryDateFrom" 
										value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.expiryDateFrom}" 
										popup="true" 
										datePattern="#{pages$chequeList.dateFormat}" 
										showApplyButton="false" locale="#{pages$chequeList.locale}" 
										enableManualInput="false" 
										cellWidth="24px" cellHeight="22px" 
								        style="width:188px; height:16px"
								        inputStyle="width: 186px; height: 18px"
										>
										</rich:calendar></td>
										
										<td>
										 
										<h:outputLabel styleClass="LABEL" value="#{msg['date.expiryDateTo']} :"></h:outputLabel></td>
										<td  >
										 
										<rich:calendar id="expiryDateTo" 
										value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.expiryDateFrom}" 
										popup="true" 
										datePattern="#{pages$crChequeDetailsList.dateFormat}" 
										showApplyButton="false" 
										locale="#{pages$crChequeDetailsList.locale}" 
										enableManualInput="false" 
										cellWidth="24px" cellHeight="22px" 
								        style="width:188px; height:16px"
								        inputStyle="width: 186px; height: 18px"
										>
										</rich:calendar></td>
      								
      								</tr>
      								<tr>
      								<td>
      								<h:outputLabel styleClass="LABEL" value="#{msg['contract.contractNumber']} :" /></td>
      								<td>
      								<h:inputText id="txtContractNumber" styleClass="INPUT_TEXT" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.contractNumber}">
									 </h:inputText></td>
      								<td><h:outputLabel styleClass="LABEL" value="#{msg['chequeList.chequeType']} :"></h:outputLabel></td>
      								<td><h:selectOneMenu id="chequeType" styleClass="SELECT_MENU" required="false" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.chequeType}">
																	<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																	<f:selectItems value="#{pages$ApplicationBean.chequeType}" />
																</h:selectOneMenu></td>
      								</tr>
      								
      								<tr>
      																		<td>
										 
										<h:outputLabel styleClass="LABEL" value="#{msg['chequeList.bankNameCol']} :"></h:outputLabel></td>
										<td  >
										 
										<h:selectOneMenu id="bankName" styleClass="SELECT_MENU" required="false" value="#{pages$crChequeDetailsList.chequeDetailsListReportCriteria.bankName}">
																	<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																	<f:selectItems value="#{pages$ApplicationBean.bankList}" />
																</h:selectOneMenu></td>
      								<td>&nbsp;</td>
      								
      								</tr>
      								
      									<tr>
														<td colspan="4">
															<DIV class="A_RIGHT" style="padding-bottom:4px;">
																	<h:commandButton id="submitButton" styleClass="BUTTON" type="submit" action="#{pages$crChequeDetailsList.cmdView_Click}" value="#{msg['commons.view']}" immediate="false"> 
                              	                       </h:commandButton>
												       
															</DIV>
														</td>
													</tr>	
												</table>
											</div>
											</div>
										</h:form>
									</div>
								</td></tr>
							</table>
						</td>
						</tr>
						<tr>
						    <td colspan="2">
						    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
						       		<tr><td class="footer"><h:outputLabel value= "#{msg['commons.footer.message']}" />
						       				</td></tr>
					    		</table>
					        </td>
					    </tr> 
					</table>
					</div>
			</body>
		</html>
	</f:view> 
      										