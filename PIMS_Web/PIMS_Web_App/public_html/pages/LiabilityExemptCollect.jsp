<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js" />


<script type="text/javascript">
		function validate() 
		{
		   maxlength=500;
		   if(document.getElementById("formRequest:txt_remarks").value.length>=maxlength) 
		   {
		     document.getElementById("formRequest:txt_remarks").value=
		     document.getElementById("formRequest:txt_remarks").value.substr(0,maxlength);  
		   }
		}
		
		function onParticipantChkChangeStart( changedChkBox )
		{
	        document.getElementById('formRequest:onClickMarkUnMark').onclick();
			document.getElementById('formRequest:disablingDiv').style.display='block';
		}
		function onParticipantTypeSelectionChanged(changedChkBox)
	    {
	        document.getElementById('formRequest:disablingDiv').style.display='block';
		    document.getElementById('formRequest:onParticipantTypeChange').onclick();
	    }
	    	    
	    
	    function  onRemoveDisablingDiv ()
		{
			document.getElementById('formRequest:disablingDiv').style.display='none';
		}
		
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />


	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />


		</head>

		<%-- Header --%>
		<body class="BODY_STYLE">
			<div class="containerDiv">

				<table width="99%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr>
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" height="84%" valign="top"
							class="divBackgroundBody">
							<h:form id="formRequest" style="height:95%;width:99.7%;"
								enctype="multipart/form-data">
								<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>

								<table class="greyPanelTable" cellpadding="0" cellspacing="0"
									border="0">
									<tr>
										<td class="HEADER_TD" style="width: 70%;">
											<h:outputLabel value="#{msg['liabilityExemptCollect.title']}" styleClass="HEADER_FONT" />
										</td>
									</tr>
								</table>

								<table height="95%" width="99%" class="greyPanelMiddleTable"
									cellpadding="0" cellspacing="0" border="0">
									<tr valign="top">
										<td height="100%" valign="top" nowrap="nowrap"
											background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
											width="1">
										</td>
										<td height="100%" valign="top" nowrap="nowrap">
											<div class="SCROLLABLE_SECTION">
												<t:panelGrid id="layoutTable" border="0"
													styleClass="layoutTable" width="94%"
													style="margin-left:15px;margin-right:15px;" columns="1">
													<h:messages></h:messages>
													<h:outputText id="successmsg" escape="false"
														styleClass="INFO_FONT"
														value="#{pages$LiabilityExemptCollect.successMessages}" />
													<h:outputText id="errormsg" escape="false"
														styleClass="ERROR_FONT"
														value="#{pages$LiabilityExemptCollect.errorMessages}" />

												</t:panelGrid>
												<t:panelGrid cellpadding="1px" width="100%"
													cellspacing="5px" styleClass="TAB_DETAIL_SECTION_INNER"
													columns="4">
													<h:outputLabel styleClass="LABEL"
														value="#{msg['liabilityExemptCollect.beneficiary']}" />
													<h:commandLink id="lnkBeneficiaryName"
														value="#{pages$LiabilityExemptCollect.liablitiesVO.beneficiary}"
														onclick="javaScript:openBeneficiaryPopup(#{pages$LiabilityExemptCollect.liablitiesVO.beneficiaryId});" />

													<h:outputLabel styleClass="LABEL" value="#{msg['liabilityExemptCollect.balance']}"
														rendered="#{! pages$LiabilityExemptCollect.exemptMode}"></h:outputLabel>
													<h:inputText id="txtBenefBalance" readonly="true"
														rendered="#{! pages$LiabilityExemptCollect.exemptMode}"
														styleClass="READONLY"
														value="#{pages$LiabilityExemptCollect.liablitiesVO.beneficiaryBalance}">
													</h:inputText>
													

													<h:outputLabel styleClass="LABEL" value="#{msg['liabilityExemptCollect.disbursement']}" />
													<h:inputText id="txtDisbursementNumber" readonly="true"
														styleClass="READONLY"
														value="#{pages$LiabilityExemptCollect.liablitiesVO.refNum}"></h:inputText>

													<h:outputLabel styleClass="LABEL" value="#{msg['liabilityExemptCollect.amount']}" />
													<h:inputText id="txtAmount" readonly="true"
														styleClass="READONLY"
														value="#{pages$LiabilityExemptCollect.liablitiesVO.benefAmount}"></h:inputText>

													<h:outputLabel styleClass="LABEL" value="#{msg['liabilityExemptCollect.disbursementSrc']}"
														rendered="#{pages$LiabilityExemptCollect.exemptMode}"></h:outputLabel>
													<h:selectOneMenu id="selectDisbursementSrc"
													   disabled="#{pages$LiabilityExemptCollect.viewMode}"
														rendered="#{pages$LiabilityExemptCollect.exemptMode}"
														value="#{pages$LiabilityExemptCollect.liablitiesVO.exemptLiabilityFromDisburseSrcId}">
														<f:selectItems
															value="#{pages$LiabilityExemptCollect.disSrcList}" />
													</h:selectOneMenu>


												</t:panelGrid>

												<table cellpadding="1px" cellspacing="3px" width="95%">
													<tr>

														<td colspan="12" class="BUTTON_TD">

															<h:commandButton type="submit"
																rendered="#{! pages$LiabilityExemptCollect.exemptMode && !pages$LiabilityExemptCollect.viewMode}"
																styleClass="BUTTON"
																onclick="if (!confirm('#{msg['liabilityExemptCollect.messages.confirmCollect']}')) return false"
																action="#{pages$LiabilityExemptCollect.onCollect}"
																value="#{msg['liabilityExemptCollect.btn.Collect']}" />
															<h:commandButton type="submit"
																rendered="#{pages$LiabilityExemptCollect.exemptMode && !pages$LiabilityExemptCollect.viewMode}"
																styleClass="BUTTON"
																onclick="if (!confirm('#{msg['liabilityExemptCollect.messages.confirmExempt']}')) return false"
																action="#{pages$LiabilityExemptCollect.onExempt}"
																value="#{msg['liabilityExemptCollect.btn.Exempt']}" />

														</td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
								</table>

							</h:form>
						</td>
					</tr>
					<tr>
						<td colspan="2">

							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>

