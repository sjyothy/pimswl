<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%-- Please remove above taglibs after completing your tab contents--%>
<t:div styleClass="contentDiv" style="width:95%">
	<t:dataTable id="disbursementGrid" width="100%" styleClass="grid"
		value="#{pages$DisbursementListTab.disbursementDataList}" rows="10"
		preserveDataModel="true" preserveSort="false" var="dataItem"
		rowClasses="row1,row2">




		<t:column id="amount" width="10%" sortable="false"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['PeriodicDisbursements.columns.amount']}" />
			</f:facet>
			<t:outputText value="#{dataItem.amount}" />
		</t:column>


		<t:column id="dateFrom" width="10%" sortable="false"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['PeriodicDisbursements.columns.from']}" />
			</f:facet>
			<t:outputText value="#{dataItem.dateFrom}">
				<f:convertDateTime timeZone="#{pages$ManageBeneficiaryBasicInfoTab.timeZone}" />
			</t:outputText>
		</t:column>
		<t:column id="dateTo" width="10%" sortable="false"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['PeriodicDisbursements.columns.to']}">
					<f:convertDateTime timeZone="#{pages$ManageBeneficiaryBasicInfoTab.timeZone}" />
				</t:outputText>
			</f:facet>
			<t:outputText value="#{dataItem.dateTo}" />
		</t:column>
		<t:column id="listTypeEn" width="10%" sortable="false"
			rendered="#{pages$DisbursementListTab.isEnglishLocale}"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['disList.disbursementListType']}" />
			</f:facet>
			<t:outputText value="#{dataItem.listTypeEn}" />
		</t:column>
		<t:column id="listTypeAr" width="10%" sortable="false"
			rendered="#{!pages$DisbursementListTab.isEnglishLocale}"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['disList.disbursementListType']}" />
			</f:facet>
			<t:outputText value="#{dataItem.listTypeAr}" />
		</t:column>
		<t:column id="status" width="10%" sortable="false"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['mems.periodic.disb.label.status']}" />
			</f:facet>
			<t:outputText
				value="#{pages$DisbursementListTab.isEnglishLocale?dataItem.statusEn:dataItem.statusAr}" />
		</t:column>



	</t:dataTable>
</t:div>
<%--Column 3,4 Ends--%>
