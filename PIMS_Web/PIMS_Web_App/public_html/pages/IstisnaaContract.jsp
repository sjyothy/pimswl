


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
        
		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
</script>
    
<f:view>
        <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
       <f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
 
    
    <html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>						
            <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
			
			
 	</head>
     
<script language="javascript" type="text/javascript">
	function validate() {
      maxlength=500;
       
       
       if(document.getElementById("formRequest:txt_remarks").value.length>=maxlength) {
          document.getElementById("formRequest:txt_remarks").value=
                                        document.getElementById("formRequest:txt_remarks").value.substr(0,maxlength);  
       
      }
    }
    function showSearchProjectPopUp(pageModeKey,selectOnePopKey,AllowedStatusKey,AllowedStatusValue,AllowedTypeKey,AllowedTypeValue)
    {
	     var  screen_width = 1024;
	     var screen_height = 450;
	     
	     window.open('projectSearch.jsf?'+pageModeKey+'='+selectOnePopKey+'&context=BOTContract','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=20,top=150,scrollbars=yes,status=yes');    
    
    }
    function populateProject(projectId,projectName,projectNumber)
    {
    		document.getElementById("formRequest:hdnProjectId").value=projectId;
		    //document.forms[0].submit();
		    
		    document.getElementById("formRequest:cmdAddProject_Click").onclick();
		    
    }

	
	function showPopup(personType)
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	
	    window.open('SearchPerson.jsf?persontype=INVESTOR&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		    document.getElementById("formRequest:hdnOwnerId").value=personId;
			document.getElementById("formRequest:cmdAddPerson_Click").onclick();
	}
	function showPersonReadOnlyPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 450;
	    window.open('AddPerson.jsf?personId='+document.getElementById("formRequest:hdnOwnerId").value+'&viewMode=popup&readOnlyMode=true','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	function showPropertySearchPopUp()
	 {
		var screen_width = 1024;
		var screen_height = 450;
		window.open('propertyList.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	 }
	function populateProperty(propertyId)
	{
	   
	  document.getElementById("formRequest:hdnPropertyId").value=propertyId;
	  document.getElementById("formRequest:cmdAddProject_Click").onclick();
	}
    </script>
 	 

				<!-- Header -->
	
		<body class="BODY_STYLE" >
		<div class="containerDiv">
	     
			<table width="99%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
					<c:when test="${!pages$IstisnaaContract.isViewModePopUp}">
					 <td colspan="2">
				        <jsp:include page="header.jsp"/>
				     </td>
				     </c:when>
		        </c:choose>
	            </tr>
				<tr>
				    <c:choose>
					 <c:when test="${!pages$IstisnaaContract.isViewModePopUp}">
					  <td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					  </td>
				     </c:when>
		            </c:choose>
					<td width="83%"  height="84%" valign="top" class="divBackgroundBody">
						<h:form id="formRequest" style="width:99.7%;height:95%"  enctype="multipart/form-data">
						<table  class="greyPanelTable"   cellpadding="0"
							cellspacing="0" border="0">
							<tr>
							    <td class="HEADER_TD" style="width:70%;" >
										 <h:outputLabel value="#{pages$IstisnaaContract.pageTitle}"  styleClass="HEADER_FONT"/>
								</td>
						        <td >
									&nbsp;
								</td>
								
								
							</tr>
						</table>
					        <table height="95%" width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
                                 <tr valign="top">
                                  <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
                                   </td>    
									<td  height="100%" valign="top" nowrap="nowrap">
									<div  class="SCROLLABLE_SECTION"  >
											<t:panelGrid id="hiddenFields"  border="0" styleClass="layoutTable" width="94%" style="margin-left:15px;margin-right:15px;" columns="1" >
													    <h:outputText    id="successmsg"      escape="false" styleClass="INFO_FONT"  value="#{pages$IstisnaaContract.successMessages}"/>
														<h:outputText    id="errormsg"        escape="false" styleClass="ERROR_FONT" value="#{pages$IstisnaaContract.errorMessages}"/>
														<h:inputHidden   id="hdnContractId"   value="#{pages$IstisnaaContract.contractId}" />
														<h:inputHidden   id="hdnContractCreatedOn" value="#{pages$IstisnaaContract.contractCreatedOn}" />
														<h:inputHidden   id="hdnContractCreatedBy" value="#{pages$IstisnaaContract.contractCreatedBy}" />
														<h:inputHidden   id="hdnRequestId"    value="#{pages$IstisnaaContract.requestId}" />
														<h:inputHidden   id="hdnProjectId"  value="#{pages$IstisnaaContract.hdnProjectId}"/>
														<h:inputHidden   id="hdnPropertyId" value="#{pages$IstisnaaContract.hdnPropertyId}"/>
														<h:inputHidden   id="hdnOwnerId" value="#{pages$IstisnaaContract.hdnOwnerId}"/>
														
														
														<a4j:commandLink id="cmdAddProject_Click" reRender="projectDetailsDiv,hiddenFields" action="#{pages$IstisnaaContract.tabProjectDetails_Click}"/>
														<a4j:commandLink id="cmdAddPerson_Click"  reRender="contractDetailsTab,hiddenFields" action="#{pages$IstisnaaContract.populateInvestorInfoInTab}"/>
											</t:panelGrid>
										<div class="MARGIN" style="width:94%;margin-bottom:0px;" >
   
                                         <t:panelGrid id ="tbl_Action" width="100%" border="0" columns="1"  binding="#{pages$IstisnaaContract.tbl_Action}"  >
                                           <t:panelGrid id ="tbl_Remarks" cellpadding="1px" cellspacing="3px" width="100%" border="0" columns="2"  >
                                         
                                         <t:panelGroup>
											<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
										    <t:outputLabel  value="#{msg['commons.remarks']}:" />
										  </t:panelGroup>  
									        
                                          <t:inputTextarea id="txt_remarks" cols="100" value="#{pages$IstisnaaContract.txtRemarks}" 
                                                           onblur="javaScript:validate();" onkeyup="javaScript:validate();" 
                                                           onmouseup="javaScript:validate();" 
                                                           onmousedown="javaScript:validate();" onchange="javaScript:validate();" 
                                                           onkeypress="javaScript:validate();" />
                                         </t:panelGrid>
                                          <t:panelGrid id ="tbl_ActionBtn" cellpadding="1px" cellspacing="3px" width="100%" border="0" columns="1"  columnClasses="BUTTON_TD">
                                         
                                          <t:panelGroup>
                                          
                                                 <pims:security screen="Pims.ConstructionMgmt.BOTContract.ReviewRequired" action="view">
                                                     <h:commandButton id= "btnReviewReq" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$IstisnaaContract.btnReviewReq_Click}"
			                                           			binding="#{pages$IstisnaaContract.btnReviewReq}"  
			                                           			value="#{msg['commons.sendButton']}" />
                                                 </pims:security> 	 			                                           			
			                                     <pims:security screen="Pims.ConstructionMgmt.BOTContract.ApproveReject" action="view">      			
			                                          <h:commandButton id= "btnApprove" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$IstisnaaContract.btnApprove_Click}"
			                                           			binding="#{pages$IstisnaaContract.btnApprove}"  
			                                           			value="#{msg['commons.approve']}" />
			                                          <h:commandButton id= "btnReject" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$IstisnaaContract.btnReject_Click}"
			                                           			binding="#{pages$IstisnaaContract.btnReject}"  
			                                           			value="#{msg['commons.reject']}" />
			                                     </pims:security> 			
			                                     <pims:security screen="Pims.ConstructionMgmt.BOTContract.Review" action="view">
			                                          <h:commandButton id= "btnReview" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$IstisnaaContract.btnReview_Click}"
			                                           			binding="#{pages$IstisnaaContract.btnReview}"  
			                                           			value="#{msg['commons.reviewButton']}" />
			                                     </pims:security>
			                                     <pims:security screen="Pims.ConstructionMgmt.BOTContract.Complete" action="view">
			                                         <h:commandButton id= "btnComplete" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$IstisnaaContract.btnComplete_Click}"
			                                           			binding="#{pages$IstisnaaContract.btnComplete}"  
			                                           			value="#{msg['commons.complete']}" />
			                                       </pims:security> 			
			                                       <pims:security screen="Pims.ConstructionMgmt.BOTContract.Print" action="view">
			                                         <h:commandButton id= "btnPrint" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$IstisnaaContract.btnPrint_Click}"
			                                           			binding="#{pages$IstisnaaContract.btnPrint}"
			                                           			value="#{msg['commons.print']}" />
			                                       </pims:security>			
                                         </t:panelGroup>
                                         </t:panelGrid>
                                       </t:panelGrid>
											
										<table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
										</table>	
						 				  <rich:tabPanel id="tabPanel" binding="#{pages$IstisnaaContract.tabPanel}"  style="width:100%;height:320px;" headerSpacing="0">
						 				           <rich:tab id="tabContractDetails" label="#{msg['contract.tab.BasicInfo']}"  title="#{msg['contract.tab.BasicInfo']}"  >
									                           <%@ include file="../pages/IstisnaaContractDetailsTab.jsp"%>
													</rich:tab>
													<rich:tab  id="tabProject" binding="#{pages$IstisnaaContract.tabProjectDetails}" action="#{pages$IstisnaaContract.tabProjectDetails_Click}"   title="#{msg['constructionContract.tab.projectDetails']}" label="#{msg['constructionContract.tab.projectDetails']}" >
				                                            <%@ include file="../pages/projectDetailsTabBOT.jsp"%>
				                                   	</rich:tab>
													<rich:tab  id="tabPaymentSchedule" binding="#{pages$IstisnaaContract.tabPaymentTerms}" action="#{pages$IstisnaaContract.tabPaymentTerms_Click}"   title="#{msg['contract.paymentTerms']}" label="#{msg['contract.paymentTerms']}" >
				                                            <%@ include file="../pages/serviceContractPaySchTab.jsp"%>
				                                   	</rich:tab>
												    <rich:tab label="#{msg['contract.tabHeading.RequestHistory']}"  title="#{msg['commons.tab.requestHistory']}"  action="#{pages$IstisnaaContract.tabAuditTrail_Click}">
									                           <%@ include file="../pages/requestTasks.jsp"%>
													</rich:tab>
													<rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}" action= "#{pages$IstisnaaContract.tabAttachmentsComments_Click}">
															   <%@  include file="attachment/attachment.jsp"%>
													</rich:tab>
											        <rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}" action= "#{pages$IstisnaaContract.tabAttachmentsComments_Click}">
															   <%@ include file="notes/notes.jsp"%>
													</rich:tab>
												
				                            </rich:tabPanel>
				                           </div>
                                    
                                   
                                        <table cellpadding="0" cellspacing="0"  style="width:94%;margin-left:10px;">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
										
										<table cellpadding="1px" cellspacing="3px" width="95%" >
											<tr>
											
											    <td colspan="12" class="BUTTON_TD">
											    <pims:security screen="Pims.ConstructionMgmt.BOTContract.Persist" action="view">
                                                    <h:commandButton type="submit"  styleClass="BUTTON" 
                                           			action="#{pages$IstisnaaContract.btnSave_Click}"
                                           			binding="#{pages$IstisnaaContract.btnSave}"  
                                           			value="#{msg['commons.saveButton']}" />
                                           		</pims:security>
                                           		<pims:security screen="Pims.ConstructionMgmt.BOTContract.SendForApproval" action="view">
                                           		 <h:commandButton id= "btnSendForApproval" type="submit"  styleClass="BUTTON" 
                                           			action="#{pages$IstisnaaContract.btnSendForApproval_Click}"
                                           			binding="#{pages$IstisnaaContract.btnSend_For_Approval}"  
                                           			value="#{msg['commons.sendButton']}" />
                                           		</pims:security>	
										             
										        </td>
		                                   </tr>
		                                </table>
                                          
                                   </div>
                            </div>
									</td>
									</tr>
									</table>
			</h:form>
			</td>
    </tr>
    <tr>
			<td colspan="2">
			
			    <table width="100%" cellpadding="0" cellspacing="0" border="0">
			       <tr><td class="footer"><h:outputLabel value="#{msg['commons.footer.message']}" /></td></tr>
			    </table>
			</td>
    </tr>
    </table>
			</div>
		</body>
	</html>
</f:view>

