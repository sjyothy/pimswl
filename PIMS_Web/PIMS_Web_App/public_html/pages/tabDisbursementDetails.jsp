
<t:div id="tabDDetails" style="width:100%;">
	<t:panelGrid id="ddTab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblBenef" styleClass="LABEL"
				value="#{msg['mems.payment.request.label.Beneficiary']}:" />
		</h:panelGroup>
		<h:selectOneMenu id="benefList"
			binding="#{pages$PaymentRequests.cmbBeneficiaryList}"
			style="width: 200px;"
			styleClass="#{pages$PaymentRequests.paymentCatCssClass}"
			readonly="#{ pages$PaymentRequests.isInvalidStatus && 
			            !(
			              pages$PaymentRequests.isNewStatus ||
                          pages$PaymentRequests.isRejectedResubmitted ||
			              pages$PaymentRequests.isFileAssociated
			              )
			            }"
			onchange="javascript:onBenefChangeStart(this);">
			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems value="#{pages$PaymentRequests.beneficiaryList}" />

		</h:selectOneMenu>


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="amnt" styleClass="LABEL"
				value="#{msg['commons.amount']}:"></h:outputLabel>
		</h:panelGroup>
		<h:inputText id="inAmnt" maxlength="20"
			readonly="#{pages$PaymentRequests.readOnlyAmount}"
			styleClass="#{pages$PaymentRequests.amountReadOnlyClass}"
			binding="#{pages$PaymentRequests.txtAmount}" />

		<pims:security
			screen="Pims.MinorMgmt.MatureSharePaymentRequest.Approve"
			action="create">


			<h:outputLabel id="lblPaymentSrc" styleClass="LABEL"
				rendered="#{pages$PaymentRequests.isApprovalRequired ||pages$PaymentRequests.isSuspended || pages$PaymentRequests.isJudgeSessionDone|| pages$PaymentRequests.isFinanceRejected ||
				            pages$PaymentRequests.isReviewDone}"
				value="#{msg['mems.payment.request.label.paymentSrc']}:" />

			<h:selectOneMenu id="paymentSrc"
				binding="#{pages$PaymentRequests.cmbPaymentSource}"
				style="width: 200px;"
				rendered="#{pages$PaymentRequests.isApprovalRequired || pages$PaymentRequests.isSuspended ||pages$PaymentRequests.isJudgeSessionDone|| pages$PaymentRequests.isFinanceRejected ||
				            pages$PaymentRequests.isReviewDone}">
				<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$PaymentRequests.paymentSource}" />
				<a4j:support event="onchange" reRender="extPartyTxt,errorMessages"
					action="#{pages$PaymentRequests.handlePaymentSrcChange}" />
			</h:selectOneMenu>


			<h:outputLabel id="extParty" styleClass="LABEL"
				rendered="#{pages$PaymentRequests.isApprovalRequired || pages$PaymentRequests.isSuspended ||pages$PaymentRequests.isJudgeSessionDone|| pages$PaymentRequests.isFinanceRejected ||
				            pages$PaymentRequests.isReviewDone}"
				value="#{msg['mems.payment.request.lable.extPartyName']}:"></h:outputLabel>
			<h:inputText id="extPartyTxt" maxlength="20"
				rendered="#{pages$PaymentRequests.isApprovalRequired || pages$PaymentRequests.isSuspended ||pages$PaymentRequests.isJudgeSessionDone|| pages$PaymentRequests.isFinanceRejected ||
				            pages$PaymentRequests.isReviewDone}"
				binding="#{pages$PaymentRequests.txtExternalPartyName}" />
		</pims:security>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblDesc" styleClass="LABEL"
				value="#{msg['commons.description']}:">
			</h:outputLabel>
		</h:panelGroup>

		<h:inputTextarea id="in" style="width: 200px;"
			readonly="#{pages$PaymentRequests.readOnlyMode}"
			styleClass="#{pages$PaymentRequests.readOnlyClass}"
			binding="#{pages$PaymentRequests.txtDescription}">
		</h:inputTextarea>

		<h:outputLabel id="lblBalance" styleClass="LABEL"
			value="#{msg['mems.normaldisb.label.balance']}:">
		</h:outputLabel>
		<t:panelGroup>
			<h:inputText id="inBalance" readonly="true" styleClass="READONLY"
				binding="#{pages$PaymentRequests.txtBalance}">
			</h:inputText>
			<%-- 
		<t:commandLink actionListener="#{pages$PaymentRequests.openAvailableBankTransfersPopUp}"															>
																<h:graphicImage id="editIcon"
																	title="#{msg['commons.edit']}"
																	url="../resources/images/edit-icon.gif" />
															</t:commandLink>
										
			--%>
		</t:panelGroup>
		<h:outputLabel id="lblBlockBalance" styleClass="LABEL"
			value="#{msg['blocking.lbl.blockedamount']}"></h:outputLabel>
		<h:panelGroup>
			<h:inputText id="blockinBalance" styleClass="READONLY"
				readonly="true" binding="#{pages$PaymentRequests.blockingBalance}"
				maxlength="20" />
			<h:commandLink id="lnkBlockingDetails"
				action="#{pages$PaymentRequests.onOpenBlockingDetailsPopup}">
				<h:graphicImage id="openBlockingDetailsPopup"
					style="margin-right:5px;"
					title="#{msg['blocking.lbl.blockingDetails']}"
					url="../resources/images/app_icons/manage_tender.png" />
			</h:commandLink>
		</h:panelGroup>
		<h:outputLabel id="labelrequestedAmount" styleClass="LABEL"
			value="#{msg['commons.requestedAmount']}:">
		</h:outputLabel>
		<h:panelGroup>
			<h:inputText id="requestedAmount" readonly="true"
				styleClass="READONLY"
				binding="#{pages$PaymentRequests.requestedAmount}">
			</h:inputText>
			<h:commandLink id="lnkDisbDetails"
				action="#{pages$PaymentRequests.onOpenBeneficiaryDisbursementDetailsPopup}">
				<h:graphicImage id="onOpenBeneficiaryDisbursementDetailsPopup"
					style="margin-right:5px;"
					title="#{msg['commons.disbursementDetails']}"
					url="../resources/images/app_icons/manage_tender.png" />
			</h:commandLink>
		</h:panelGroup>

		<h:outputLabel id="labelAVBalance" styleClass="LABEL"
			value="#{msg['commons.availableBalance']}:">
		</h:outputLabel>
		<h:inputText id="availableBalance" readonly="true"
			styleClass="READONLY"
			binding="#{pages$PaymentRequests.availableBalance}">
		</h:inputText>
	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;">

		<pims:security
			screen="Pims.MinorMgmt.MatureSharePaymentRequest.Create"
			action="create">
			<h:commandButton styleClass="BUTTON"
				rendered="#{pages$PaymentRequests.isNewStatus || 
							pages$PaymentRequests.isRejectedResubmitted  
							
							
							}"
				action="#{pages$PaymentRequests.saveBeneficiary}"
				value="#{msg['commons.saveButton']}">
			</h:commandButton>
		</pims:security>

		<!--  save beneficiary button when the request is approval required -->
		<pims:security
			screen="Pims.MinorMgmt.MatureSharePaymentRequest.Approve"
			action="create">
			<h:commandButton styleClass="BUTTON"
				rendered="#{pages$PaymentRequests.isApprovalRequired ||
							pages$PaymentRequests.isSuspended || 
							pages$PaymentRequests.isJudgeSessionDone || 
							pages$PaymentRequests.isReviewDone || 
							pages$PaymentRequests.isFinanceRejected
						   }"
				action="#{pages$PaymentRequests.updateBeneficiaryWhenApprovalRequired}"
				value="#{msg['commons.saveButton']}">
			</h:commandButton>
		</pims:security>
	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="prdc" rows="15" width="100%"
			value="#{pages$PaymentRequests.disbursementDetails}"
			binding="#{pages$PaymentRequests.disbursementTable}"
			preserveDataModel="false" preserveSort="false" var="unitDataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="checkbox" style="width:auto;white-space: normal;"
				rendered="#{pages$PaymentRequests.isApprovalRequired ||
							pages$PaymentRequests.isSuspended ||	 
							pages$PaymentRequests.isJudgeSessionDone || 
							pages$PaymentRequests.isReviewDone || 
							pages$PaymentRequests.isFinanceRejected
						   }">
				<f:facet name="header">
				</f:facet>
				<h:selectBooleanCheckbox id="idbox" immediate="true"
					value="#{unitDataItem.selected}">
				</h:selectBooleanCheckbox>
			</t:column>

			<t:column id="beneficiary" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdBenef"
						value="#{msg['mems.payment.request.label.beneficiaries']}" />
				</f:facet>
				<t:commandLink
					actionListener="#{pages$PaymentRequests.openManageBeneficiaryPopUp}"
					value="#{unitDataItem.beneficiariesName}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="amnt" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAmnt"
						value="#{msg['mems.payment.request.label.Amount']}" />
				</f:facet>
				<t:outputText id="amountt" styleClass="A_LEFT"
					value="#{unitDataItem.amount}" />
			</t:column>
			<t:column id="pmtSrc" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdPmtSrc"
						value="#{msg['mems.payment.request.label.paymentSrc']}" />
				</f:facet>
				<t:outputText id="paymentSrc" styleClass="A_LEFT"
					value="#{pages$PaymentRequests.isEnglishLocale?unitDataItem.srcTypeEn:unitDataItem.srcTypeAr}" />
			</t:column>
			<t:column id="extPtNamehd" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdExtPtName"
						value="#{msg['mems.payment.request.lable.extPartyName']}" />
				</f:facet>
				<t:outputText id="extPtName" styleClass="A_LEFT"
					value="#{unitDataItem.externalPartyName}" />
			</t:column>
			<t:column id="desc" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdTo"
						value="#{msg['mems.payment.request.lable.Desc']}" />
				</f:facet>
				<t:outputText id="p_w" styleClass="A_LEFT"
					value="#{unitDataItem.description}" />
			</t:column>

			<t:column id="amount" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAmoun" value="#{msg['commons.action']}" />
				</f:facet>
				<h:commandLink
					action="#{pages$PaymentRequests.openAssociateCostCenters}"
					rendered="#{unitDataItem.showAssoCostCentersPopUp && (pages$PaymentRequests.isApprovalRequired || pages$PaymentRequests.isSuspended ||pages$PaymentRequests.isJudgeSessionDone)}">
					<h:graphicImage id="associateCostCenterIcon"
						title="#{msg['commons.cmdLink.openAssociateCostCenter']}"
						url="../resources/images/app_icons/Add-Fee-sub-type.png"
						width="18px;" />
				</h:commandLink>
				<pims:security
					screen="Pims.MinorMgmt.MatureSharePaymentRequest.Create"
					action="create">
					<h:commandLink action="#{pages$PaymentRequests.deleteBeneficiary}"
						onclick="if (!confirm('#{msg['mems.common.alertDelete']}')) return false;"
						rendered="#{pages$PaymentRequests.isNewStatus || pages$PaymentRequests.isRejectedResubmitted}">
						<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
							url="../resources/images/delete.gif" width="18px;" />
					</h:commandLink>
				</pims:security>
				<h:commandLink action="#{pages$PaymentRequests.editBeneficiary}">
					<h:graphicImage id="editIcon" title="#{msg['commons.edit']}"
						url="../resources/images/edit-icon.gif" width="18px;" />
				</h:commandLink>
				<pims:security
					screen="Pims.MinorMgmt.MatureSharePaymentRequest.Approve"
					action="create">
					<h:commandLink action="#{pages$PaymentRequests.editPaymentDetails}"
						rendered="#{(
									 pages$PaymentRequests.isApprovalRequired ||
									 pages$PaymentRequests.isSuspended || 
									 pages$PaymentRequests.isJudgeSessionDone || 
									 pages$PaymentRequests.isFinanceRejected || 
									 pages$PaymentRequests.isReviewDone|| pages$PaymentRequests.isComplete
									 )
													&& empty unitDataItem.externalPartyName}">
						<h:graphicImage id="pymtIcon"
							title="#{msg['mems.payment.request.label.paymentDetails']}"
							url="../resources/images/app_icons/Pay-Fine.png" width="18px;" />
					</h:commandLink>
				</pims:security>
			</t:column>
		</t:dataTable>
	</t:div>
	<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99%;">
		<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
			cellspacing="1">
			<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
				<t:div styleClass="JUG_NUM_REC_ATT">
					<h:outputText value="#{msg['commons.records']}" />
					<h:outputText value=" : " />
					<h:outputText value="#{pages$PaymentRequests.totalRows}" />
				</t:div>
			</t:panelGroup>
			<t:panelGroup>
				<t:dataScroller id="shareScrollers" for="prdc" paginator="true"
					fastStep="1" immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false" pageIndexVar="pageNumber"
					styleClass="JUG_SCROLLER">
					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
					</f:facet>
					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
					</f:facet>
					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
					</f:facet>
					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
					</f:facet>
					<t:div styleClass="PAGE_NUM_BG" rendered="true">
						<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}" />
						<h:outputText styleClass="PAGE_NUM"
							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
							value="#{requestScope.pageNumber}" />
					</t:div>
					<t:div>
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<h:outputText styleClass="PAGE_NUM"
										style="font-size:9;color:black;"
										value="#{msg['mems.normaldisb.label.totalAmount']}:" />
								</td>
								<td>
									<h:outputText styleClass="PAGE_NUM"
										style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:9;font-weight:bold;color:black"
										value="#{pages$NormalDisbursement.totalDisbursementCount}" />
								</td>
							</tr>
						</table>
					</t:div>
				</t:dataScroller>
			</t:panelGroup>
		</t:panelGrid>
	</t:div>
</t:div>


