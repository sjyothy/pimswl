<%-- 
  - Author: Anil Verani
  - Date: 19/01/2010
  - Copyright Notice:
  - @(#)
  - Description: Used for selecting printer & printing the report 
  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>					
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['commons.print.option']}" styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" >

										<h:form id="frm" style="width:97%" enctype="multipart/form-data">
											
											<table border="0" class="layoutTable">
												<tr>

													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$printReportManage.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$printReportManage.successMessages}" />
													</td>
												</tr>
											</table>

											<div class="MARGIN" style="width: 98%">
												
												<table cellpadding="1" cellspacing="1" width="100%">
												
													<tr>
														<td width="5%">&nbsp;</td>														
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td width="5%">&nbsp;</td>
													</tr>
												
													<tr>
														<td width="5%">&nbsp;</td>
														<td width="15%">
															<h:outputLabel styleClass="LABEL" value="#{msg['commons.select.printer']}:" />
														</td>
														<td>
															<h:selectOneMenu value="#{pages$printReportManage.printer}">
																<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
																<f:selectItems value="#{pages$ApplicationBean.printerList}" />
															</h:selectOneMenu>
														</td>
														<td width="5%">&nbsp;</td>														
													</tr>
													
													<tr>
														<td width="5%">&nbsp;</td>
														<td width="15%">
															<h:outputLabel styleClass="LABEL" value="#{msg['commons.no.of.copies']}:" />
														</td>
														<td>
															<h:inputText value="#{pages$printReportManage.noOfCopies}" />																
														</td>
														<td width="5%">&nbsp;</td>														
													</tr>
													
													<tr>
														<td width="5%">&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td width="5%">&nbsp;</td>														
													</tr>
												</table>
												
												<table cellpadding="0" cellspacing="0" width="50%">
													<tr>
														<td colspan="4" class="BUTTON_TD">
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.print']}"
																	action="#{pages$printReportManage.onPrint}">
															</h:commandButton>																					
														</td>
													</tr>
												</table>
											</div>								
								
									    </h:form>
									</div>
								

									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>				
			</table>
           </div>
		</body>
	</html>
</f:view>
