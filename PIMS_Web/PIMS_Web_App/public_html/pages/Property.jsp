<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache,must-revalidate,no-store">
		<meta http-equiv="expires" content="0">

		<link rel="stylesheet" type="text/css" href="../resources/css/simple_en.css" />
		<link rel="stylesheet" type="text/css" href="../resources/css/amaf_en.css" />
		<link rel="stylesheet" type="text/css" href="../resources/css/table.css" />
		<title>PIMS</title>
	</head>
	<body>
		<f:view>
			<body>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr>
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td background="../resources/images/Grey panel/Grey-Panel-left-1.jpg" height="38" style="background-repeat: no-repeat;" width="100%">
										<font style="font-family: Trebuchet MS; font-weight: regular; font-size: 19px; margin-left: 15px; top: 30px;">
											Property
										</font>
										<!--<IMG src="../resources/images/Grey panel/Grey-Panel-left-1.jpg"/>-->
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap" background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
									</td>
									<td width="500px" height="100%" valign="top" nowrap="nowrap">
										<h:form>
											<h:inputHidden value="#{pages$Property.propertyBean.propertyId}" binding="#{pages$Property.hdnPropertyId}"></h:inputHidden>
											<table style="width: 100%">
												<tr>
													<td width="25%">
														<h:outputText value="Property Reference Number :"></h:outputText>
													</td>
													<td width="25%">
														<h:inputText value="#{pages$Property.propertyBean.propertyNumber}" binding="#{pages$Property.txtPropertyNumber}"></h:inputText>
													</td>
													<td width="25%">
														<h:outputText value="Financial Account Number :"></h:outputText>
													</td>
													<td width="25%">
														<h:inputText value="#{pages$Property.propertyBean.financialAccountNo}" binding="#{pages$Property.txtFinancialAccountNumber}"></h:inputText>
													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Type :"></h:outputText>
													</td>
													<td>
														<h:selectOneMenu value="#{pages$Property.propertyBean.typeId}" binding="#{pages$Property.ddlTypeId}">
															<f:selectItem itemLabel="[Select]" itemValue="" />
															<f:selectItems value="#{pages$ApplicationBean.propertyTypeListEn}" />
														</h:selectOneMenu>
													</td>
													<td>
														<h:outputText value="Endowed Name :"></h:outputText>
													</td>
													<td>
														<h:inputText value="#{pages$Property.propertyBean.endowedName}" binding="#{pages$Property.txtEndowedName}"></h:inputText>
													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Category :"></h:outputText>
													</td>
													<td>
														<h:selectOneMenu value="#{pages$Property.propertyBean.categoryId}" binding="#{pages$Property.ddlCategoryId}">
															<f:selectItem itemLabel="[Select]" itemValue="" />
															<f:selectItems value="#{pages$ApplicationBean.propertyCategoryListEn}" />
														</h:selectOneMenu>
													</td>
													<td>
														<h:outputText value="Commercial Name :"></h:outputText>
													</td>
													<td>
														<h:inputText
															value="#{pages$Property.propertyBean.commercialName}" binding="#{pages$Property.txtCommercialName}"></h:inputText>
													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Usage :"></h:outputText>
													</td>
													<td>
														<h:selectOneMenu value="#{pages$Property.propertyBean.usageTypeId}" binding="#{pages$Property.ddlUsageTypeId}">
															<f:selectItem itemLabel="[Select]" itemValue="" />
															<f:selectItems value="#{pages$ApplicationBean.propertyUsageListEn}" />
														</h:selectOneMenu>
													</td>
													<td>
														<h:outputText value="Land Number :"></h:outputText>
													</td>
													<td>
														<h:inputText value="#{pages$Property.propertyBean.landNumber}" binding="#{pages$Property.txtLandNumber}"></h:inputText>
													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Status :"></h:outputText>
													</td>
													<td>
														<h:selectOneMenu value="#{pages$Property.propertyBean.statusId}" binding="#{pages$Property.ddlStatusId}">
															<f:selectItem itemLabel="[Select]" itemValue="" />
															<f:selectItems value="#{pages$ApplicationBean.propertyStatusListEn}" />
														</h:selectOneMenu>
													</td>
													<td>
														<h:outputText value="Department Code :"></h:outputText>
													</td>
													<td>
														
													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Investment Purpose :"></h:outputText>
													</td>
													<td>
														<h:selectOneMenu value="#{pages$Property.propertyBean.investmentPurposeId}" binding="#{pages$Property.ddlInvestmentPurposeId}">
															<f:selectItem itemLabel="[Select]" itemValue="" />
														</h:selectOneMenu>
													</td>
													<td>
														<h:outputText value="Municipality Plan Number :"></h:outputText>
													</td>
													<td>
														<h:inputText value="#{pages$Property.propertyBean.municipalityPlanNo}" binding="#{pages$Property.txtMunicipalityPlanNo}"></h:inputText>
													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Number of Units :"></h:outputText>
													</td>
													<td>
														<h:inputText value="#{pages$Property.propertyBean.noOfUnits}" binding="#{pages$Property.txtNoOfUnits}"></h:inputText>
													</td>
													<td>
														<h:outputText value="Project Number :"></h:outputText>
													</td>
													<td>
														<h:inputText value="#{pages$Property.propertyBean.projectNo}" binding="#{pages$Property.txtProjectNo}"></h:inputText>
													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Number of Floors :"></h:outputText>
													</td>
													<td>
														<h:inputText value="#{pages$Property.propertyBean.noFloors}" binding="#{pages$Property.txtNoFloors}"></h:inputText>
													</td>
													<td>
														<h:outputText value="Land Area :"></h:outputText>
													</td>
													<td>
														<h:inputText value="#{pages$Property.propertyBean.landArea}" binding="#{pages$Property.txtLandArea}"></h:inputText>
													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Mezzaning Floors :"></h:outputText>
													</td>
													<td>
														<h:inputText value="#{pages$Property.propertyBean.noOfMezzanineFloors}" binding="#{pages$Property.txtNoOfMezzanineFloors}"></h:inputText>
													</td>
													<td>
														<h:outputText value="Built in Area :"></h:outputText>
													</td>
													<td>
														<h:inputText value="#{pages$Property.propertyBean.builtInArea}" binding="#{pages$Property.txtBuiltInArea}"></h:inputText>
													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Parking Floors :"></h:outputText>
													</td>
													<td>
														<h:inputText
															value="#{pages$Property.propertyBean.noOfParkingFloors}" binding="#{pages$Property.txtNoOfParkingFloors}"></h:inputText>
													</td>
													<td>
														<h:outputText value="Coordinates :"></h:outputText>
													</td>
													<td>
														
													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Number of Flats :"></h:outputText>
													</td>
													<td>
														<h:inputText value="#{pages$Property.propertyBean.noOfFlats}" binding="#{pages$Property.txtNoOfFlats}"></h:inputText>
													</td>
													<td>
														<h:outputText value="Location Chart :"></h:outputText>
													</td>
													<td>
														
													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Number of Lifts :"></h:outputText>
													</td>
													<td>
														<h:inputText value="#{pages$Property.propertyBean.noOfLifts}" binding="#{pages$Property.txtNoOfLifts}"></h:inputText>
													</td>
													<td>
														<h:outputText value="Contact Information :"></h:outputText>
													</td>
													<td>
														
													</td>
												</tr>
												<tr>
													<td></td>
													<td></td>
													<td></td>
													<td>
														<h:commandButton value="Save" type="submit" action="#{pages$Property.btnSaveAction}"></h:commandButton>
														<h:commandButton value="Cancel" type="submit" action="#{pages$Property.btnCancelAction}"></h:commandButton>
													</td>
												</tr>
												<tr>
													<td colspan="4">
														<t:panelTabbedPane width="100%" serverSideTabSwitch="false">
															<t:panelTab label="Facilities">
																<%--
																<h:dataTable
																	value="#{pages$Property.propertyBean.propertyFacilities}"
																	var="rec" border="1" width="100%">
																	<h:column id="column1">
																		<f:facet name="header">
																			<h:outputText value="Facility"></h:outputText>
																		</f:facet>
																		<h:outputText value="#{rec.facility.facilityName}"></h:outputText>
																	</h:column>
																	<h:column id="column2">
																		<h:outputText value="#{rec.accountNo}"></h:outputText>
																		<f:facet name="header">
																			<h:outputText value="Account Number"></h:outputText>
																		</f:facet>
																	</h:column>
																	<h:column id="column3">
																		<h:outputText value="#{rec.facility.class}"></h:outputText>
																		<f:facet name="header">
																			<h:outputText value="Mandatory"></h:outputText>
																		</f:facet>
																	</h:column>
																	<h:column id="column4">
																		<h:outputText value="#{rec.facility.class}"></h:outputText>
																		<f:facet name="header">
																			<h:outputText value="Paid"></h:outputText>
																		</f:facet>
																	</h:column>
																	<h:column id="column5">
																		<h:outputText value="#{rec.facility.class}"></h:outputText>
																		<f:facet name="header">
																			<h:outputText value="From"></h:outputText>
																		</f:facet>
																	</h:column>
																	<h:column id="column6">
																		<h:outputText value="#{rec.facility.class}"></h:outputText>
																		<f:facet name="header">
																			<h:outputText value="To"></h:outputText>
																		</f:facet>
																	</h:column>
																	<h:column id="column7">
																		<h:outputText value="#{rec.facility.class}"></h:outputText>
																		<f:facet name="header">
																			<h:outputText value="Rent"></h:outputText>
																		</f:facet>
																	</h:column>
																	<h:column id="column8">
																		<h:outputText value="#{rec.facility.class}"></h:outputText>
																		<f:facet name="header">
																			<h:outputText value="Actions"></h:outputText>
																		</f:facet>
																	</h:column>
																</h:dataTable>
																--%>
															</t:panelTab>
															<t:panelTab label="Insurance">
															</t:panelTab>
															<t:panelTab label="Representative">
															</t:panelTab>
															<t:panelTab label="Remarks">
															</t:panelTab>
														</t:panelTabbedPane>
													</td>
												</tr>
											</table>
										</h:form>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</body>
		</f:view>
</html>
