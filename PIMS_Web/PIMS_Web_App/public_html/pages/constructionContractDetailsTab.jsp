
<script type="text/javascript">

						
</script>
<t:div styleClass="TAB_DETAIL_SECTION" style="width:100%">
	<t:panelGrid id="contractDetailsTable" cellpadding="1px" width="100%"
		cellspacing="5px" styleClass="TAB_DETAIL_SECTION_INNER" columns="4"
		columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">

		<h:outputLabel styleClass="LABEL"
			value="#{msg['commons.referencenumber']}" />
		<h:inputText maxlength="20" id="txtContractRefNum"
			styleClass="READONLY" readonly="true"
			binding="#{pages$constructionContractDetailsTab.txtContractNo}"
			value="#{pages$constructionContractDetailsTab.contractNo}"
			></h:inputText>
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel id="lblContractType" styleClass="LABEL"
				value="#{msg['contract.contractType']}"></h:outputLabel>
		</h:panelGroup>
		<h:inputText maxlength="20" id="txtContractType" styleClass="READONLY"
			readonly="true"
			binding="#{pages$constructionContractDetailsTab.txtContractType}"
			value="#{pages$constructionContractDetailsTab.contractType}"
			></h:inputText>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['serviceContract.tender.number']}"></h:outputLabel>
		<h:panelGroup>
			<h:inputText id="txtTenderNumber" 
				styleClass="READONLY" readonly="true"
				binding="#{pages$constructionContractDetailsTab.txtTenderNum}"
				title="#{pages$constructionContractDetailsTab.txtTenderNum}"
				value="#{pages$constructionContractDetailsTab.tenderNum}"></h:inputText>
			<h:graphicImage url="../resources/images/app_icons/Search-tenant.png"
				onclick="javaScript:showSearchTenderPopUp('#{pages$constructionContractDetailsTab.tenderSearchKey_tenderType}','#{pages$constructionContractDetailsTab.tenderSearchKey_WinnerContractorPresent}','#{pages$constructionContractDetailsTab.tenderSearchKey_NotBindedWithContract}');"
				binding="#{pages$constructionContractDetailsTab.imgSearchTender}"
				style="MARGIN: 0px 0px -4px;" alt="#{msg[commons.searchTender]}"></h:graphicImage>
			<h:graphicImage url="../resources/images/delete_icon.png"
				onclick="javaScript:document.getElementById('formRequest:cmdDeleteTender').onclick();"
				binding="#{pages$constructionContractDetailsTab.imgRemoveTender}"
				style="MARGIN: 0px 0px -4px;" alt="#{msg[commons.delete]}"></h:graphicImage>

		</h:panelGroup>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['serviceContract.tender.description']}"></h:outputLabel>
		<h:inputText id="txtTenderDesc" styleClass="READONLY" readonly="true"
			binding="#{pages$constructionContractDetailsTab.txtTenderDesc}"
			value="#{pages$constructionContractDetailsTab.tenderDesc}"
			title="#{pages$constructionContractDetailsTab.tenderDesc}"></h:inputText>
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['serviceContract.contractor.number']}"></h:outputLabel>
		</h:panelGroup>
		<h:panelGroup>
			<h:inputText id="txtContractorNumber" 
				styleClass="READONLY" readonly="true"
				binding="#{pages$constructionContractDetailsTab.txtContractorNum}"
				title="#{pages$constructionContractDetailsTab.contractorNum}"
				value="#{pages$constructionContractDetailsTab.contractorNum}"></h:inputText>
			<h:graphicImage url="../resources/images/app_icons/Search-Person.png"
				onclick="javaScript:showSearchContractorPopUp('#{pages$constructionContractDetailsTab.contractorScreenQueryStringViewMode}','#{pages$constructionContractDetailsTab.contractorScreenQueryStringPopUpMode}');"
				binding="#{pages$constructionContractDetailsTab.imgSearchContractor}"
				style="MARGIN: 0px 0px -4px;" alt="#{msg[contract.searchTenant]}"></h:graphicImage>
		</h:panelGroup>
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['serviceContract.contractor.name']}"></h:outputLabel>
		</h:panelGroup>
		<h:inputText id="txtContractorName" styleClass="READONLY"
			readonly="true" 
			binding="#{pages$constructionContractDetailsTab.txtContractorName}"
			value="#{pages$constructionContractDetailsTab.contractorName}"
			title="#{pages$constructionContractDetailsTab.contractorName}"></h:inputText>
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['contract.date.Start']}"></h:outputLabel>
		</h:panelGroup>
		<rich:calendar id="contractStartDate"
			value="#{pages$constructionContractDetailsTab.contractStartDate}"
			popup="true"
			datePattern="#{pages$constructionContractDetailsTab.dateFormat}"
			binding="#{pages$constructionContractDetailsTab.startDateCalendar}"
			timeZone="#{pages$constructionContractDetailsTab.timeZone}"
			showApplyButton="false" enableManualInput="false" cellWidth="24px"
			
			locale="#{pages$constructionContractDetailsTab.locale}">
		</rich:calendar>


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel
				value="#{msg['constructionContract.contractExecutionTime']}"></h:outputLabel>
		</h:panelGroup>

		<rich:calendar id="datetimeContractEndDate"
			value="#{pages$constructionContractDetailsTab.contractEndDate}"
			binding="#{pages$constructionContractDetailsTab.endDateCalendar}"
			popup="true"
			datePattern="#{pages$constructionContractDetailsTab.dateFormat}"
			timeZone="#{pages$constructionContractDetailsTab.timeZone}"
			showApplyButton="false" enableManualInput="false" cellWidth="24px"
			>
		</rich:calendar>
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputText value="#{msg['serviceContract.PaymentType']}:"></h:outputText>
		</h:panelGroup>
		<h:selectOneMenu id="selectPaymentType" 
			value="#{pages$constructionContractDetailsTab.selectPaymentType}"
			binding="#{pages$constructionContractDetailsTab.cmbSelectPaymentType}">
			<f:selectItem itemValue="-1"
				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
			<f:selectItems
				value="#{pages$ApplicationBean.constructionPaymentTypeList}" />
			<a4j:support event="onchange"
				reRender="txtProjectPercentage,totalContract"
				action="#{pages$constructionContract.onConstructionPaymentType}" />

		</h:selectOneMenu>



		<h:outputLabel styleClass="LABEL"
			value="#{msg['serviceContract.ProjectPercentage']}"></h:outputLabel>
		<h:panelGroup>
			<h:inputText id="txtProjectPercentage"
				onchange="javascript:calculateProjectAmount(); " 
				styleClass="A_RIGHT_NUM #{pages$constructionContractDetailsTab.readOnlyPercentage}"
				maxlength="15"
				binding="#{pages$constructionContractDetailsTab.txtProjectPercentage}"
				value="#{pages$constructionContractDetailsTab.projectPercentage}">%
			    </h:inputText>

			<a4j:commandLink id="lnkCalculatePayment"
				action="#{pages$constructionContract.onCalculatePayment}"
				reRender="totalContract,txtBankGuaranteeAmount" />
		</h:panelGroup>
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['contract.Amount']}"></h:outputLabel>
		</h:panelGroup>
		<h:panelGroup>
		<h:inputText id="totalContract" 
			onchange="javascript:calculatePercentageChange();"
			styleClass="A_RIGHT_NUM #{pages$constructionContractDetailsTab.readOnlyAmount}"
			maxlength="15"
			binding="#{pages$constructionContractDetailsTab.txtTotalAmount}"
			value="#{pages$constructionContractDetailsTab.totalAmount}">
		</h:inputText>
			<a4j:commandLink id="lnk2PercentageChange"
				action="#{pages$constructionContract.guaranteePercentageChange}"
				reRender="txtBankGuaranteeAmount" />
		</h:panelGroup>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['serviceContract.guarantee.percentage']}"></h:outputLabel>
		<h:panelGroup>
			<h:inputText id="txtGuaranteePercentage"
				readonly="#{pages$constructionContractDetailsTab.guaranteeFieldReadOnly}"
				onchange="javascript:calculatePercentageChange();"
				 styleClass="A_RIGHT_NUM" maxlength="7"
				binding="#{pages$constructionContractDetailsTab.txtGuaranteePercentage}"
				value="#{pages$constructionContractDetailsTab.guaranteePercentage}">%
		    </h:inputText>

			<a4j:commandLink id="lnkPercentageChange"
				action="#{pages$constructionContract.guaranteePercentageChange}"
				reRender="txtBankGuaranteeAmount" />
		</h:panelGroup>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['serviceContract.guarantee.bankAmount']}"></h:outputLabel>
		<h:inputText id="txtBankGuaranteeAmount" 
			styleClass="A_RIGHT_NUM READONLY" maxlength="15" 
			binding="#{pages$constructionContractDetailsTab.txtGuaranteeAmont}"
			value="#{pages$constructionContractDetailsTab.guaranteeAmont}"></h:inputText>

		<h:outputText value="#{msg['serviceContract.bankName']}:"></h:outputText>
		<h:selectOneMenu id="selectBank" 
			readonly="#{pages$constructionContractDetailsTab.guaranteeFieldReadOnly}"
			value="#{pages$constructionContractDetailsTab.selectOneBank}"
			binding="#{pages$constructionContractDetailsTab.cmbBank}">
			<f:selectItem itemValue="-1"
				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
			<f:selectItems value="#{pages$constructionContractDetailsTab.banks}" />
		</h:selectOneMenu>

		<h:outputLabel value="#{msg['serviceContract.guarantee.expiryDate']}"></h:outputLabel>
		<rich:calendar id="guaranteeExpiryDate"
			disabled="#{pages$constructionContractDetailsTab.guaranteeFieldReadOnly}"
			value="#{pages$constructionContractDetailsTab.guaranteeExpiry}"
			binding="#{pages$constructionContractDetailsTab.guaranteeExpiryDate}"
			popup="true"
			datePattern="#{pages$constructionContractDetailsTab.dateFormat}"
			timeZone="#{pages$constructionContractDetailsTab.timeZone}"
			showApplyButton="false" enableManualInput="false" cellWidth="24px"
		>
		</rich:calendar>

	</t:panelGrid>
</t:div>
