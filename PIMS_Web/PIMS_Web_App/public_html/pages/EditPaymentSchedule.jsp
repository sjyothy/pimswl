<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">




<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />



			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

		</head>

		<script language="javascript" type="text/javascript">
	function clearWindow()
	{
            document.getElementById("formPaymentSchedule:dateTimePaymentDueOn").value="";
			document.getElementById("formPaymentSchedule:txtAmount").value="";
			var cmbPaymentMode=document.getElementById("formPaymentSchedule:selectPaymentMode");
			
			cmbPaymentMode.selectedIndex = 0;
			    $('formPaymentSchedule:dateTimePaymentDueOn').component.resetSelectedDate();
	        
	}
 	function addToContract()
    {
        window.opener.document.forms[0].submit();
        window.close();
    }
	
    </script>


		<!-- Header -->

		<body class="BODY_STYLE">

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">

					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['contract.paymentSchedule']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="99%" valign="top" nowrap="nowrap">
									

										<h:form id="formPaymentSchedule" style="width:92%"
											enctype="multipart/form-data">
											<div class="SCROLLABLE_SECTION">
											<table border="0" class="layoutTable">

												<tr>

													<td colspan="3">

														<h:outputText escape="false"
															value="#{pages$EditPaymentSchedule.errorMessages}" />

													</td>
												</tr>
											</table>

											<div class="MARGIN" style="width: 100%">
												<table cellpadding="0" cellspacing="0" width="90.3%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG 
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width: 90%">
													<h:outputLabel
														value="#{msg['paymentSchedule.paymentDetails']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="1px" class="DETAIL_SECTION_INNER">


														<tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td width="15%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['paymentSchedule.paymentType']}">:</h:outputLabel>
															</td>
															<td>
																<h:selectOneMenu id="selectpaymentType" readonly="true"
																	style="width: 85%"
																	disabled="#{pages$EditPaymentSchedule.readonlyMode}"
																	value="#{pages$EditPaymentSchedule.selectedPaymentType}">
																	<f:selectItems
																		value="#{pages$EditPaymentSchedule.paymentTypeList}" />
																</h:selectOneMenu>
															</td>
														</tr>
                                                        <tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td width="15%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['paymentSchedule.paymentDueOn']}">:</h:outputLabel>
																<h:inputHidden id="hdnDateTimeDueOn"
																	value="#{pages$EditPaymentSchedule.paymentDueOn}" />
															</td>
															<td>
																<rich:calendar id="dateTimePaymentDueOn"
																	value="#{pages$EditPaymentSchedule.paymentDueOn}"
																	popup="true" showApplyButton="false"
																	datePattern="#{pages$EditPaymentSchedule.dateformat}"
																	enableManualInput="false" cellWidth="24px"
																	locale="#{pages$EditPaymentSchedule.locale}"
																	disabled="#{pages$EditPaymentSchedule.readonlyMode}"
																	inputStyle="width: 85%" />
															</td>
														</tr>
														<tr>
															<td>
																&nbsp;&nbsp;
															</td>


															<td width="15%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['paymentSchedule.paymentMode']}">:</h:outputLabel>
															</td>
															<td>
																<h:selectOneMenu id="selectpaymentMode"
																	style="width: 85%"
																	value="#{pages$EditPaymentSchedule.selectedPaymentMode}"
																	disabled="#{pages$EditPaymentSchedule.readonlyMode && pages$EditPaymentSchedule.cancelContractBean}">

																	<f:selectItems
																		value="#{pages$EditPaymentSchedule.paymentModeList}" />
																</h:selectOneMenu>
															</td>
														</tr>

														
														<c:choose>
															<c:when
																test="${pages$EditPaymentSchedule.multiplePaymentDueOn}">
																<tr>
																	<td>
																		&nbsp;&nbsp;
																	</td>
																	<td width="15%">
																		<h:outputLabel
																			value="#{msg['paymentSchedule.percentage']}">:</h:outputLabel>
																	</td>

																	<td width="85%">
																		<h:inputText id="txtPercentage"
																			readonly="#{pages$EditPaymentSchedule.readonlyMode}"
																			value="#{pages$EditPaymentSchedule.txtCompletionPercentage}"
																			style="width: 85%" styleClass="A_RIGHT_NUM"
																			maxlength="3" disabled="true"></h:inputText>
																	</td>
																</tr>
																<tr>
																	<td>
																		&nbsp;&nbsp;
																	</td>
																	<td width="15%">
																		<h:outputLabel id="lblMileStone"
																			value="#{msg['paymentSchedule.mileStone']}">:</h:outputLabel>
																	</td>
																	<td width="85%">
																		<h:selectOneMenu id="selectMileStones" disabled="true"
																			style="width: 85%"
																			value="#{pages$EditPaymentSchedule.selectedMileStone}">

																			<f:selectItems
																				value="#{pages$EditPaymentSchedule.mileStoneList}" />
																		</h:selectOneMenu>
																	</td>
																</tr>
															</c:when>
														</c:choose>
														<tr>
															<td>
																&nbsp;&nbsp;
															</td>

															<td width="15%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['paymentSchedule.amount']}">:</h:outputLabel>
															</td>
															<td>
																<h:inputText style="width: 85%" styleClass="A_RIGHT_NUM"
																	maxlength="15" id="txtAmount"
																	readonly="#{pages$EditPaymentSchedule.readonlyMode || !pages$EditPaymentSchedule.amountEditable}"
																	value="#{pages$EditPaymentSchedule.amount}">

																</h:inputText>
															</td>

														</tr>

														<tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td width="15%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['paymentSchedule.description']}">:</h:outputLabel>
															</td>
															<td>
																<h:inputText id="txtPaymentDescription"
																	style="width: 85%" maxlength="500"
																	value="#{pages$EditPaymentSchedule.description}"
																	readonly="#{pages$EditPaymentSchedule.readonlyMode}" />
															</td>
														</tr>
                                                        <tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td width="15%">
																<h:outputLabel styleClass="LABEL" 
																rendered="#{ !pages$EditPaymentSchedule.readonlyMode &&
																            ( pages$EditPaymentSchedule.systemPayment ||
																              pages$EditPaymentSchedule.nonSystemPaymentExemptionAllowed )
																           }"
																	value="#{msg['paymentSchedule.exempted']}:"></h:outputLabel>
															</td>
															<td>
																<t:selectBooleanCheckbox 
																rendered="#{ !pages$EditPaymentSchedule.readonlyMode && 
																            ( pages$EditPaymentSchedule.systemPayment ||
																              pages$EditPaymentSchedule.nonSystemPaymentExemptionAllowed )
																           }"   
																value="#{pages$EditPaymentSchedule.exempted}" />
															</td>
														</tr>
                                                        <tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td width="15%">
																<h:outputLabel styleClass="LABEL" rendered="#{ !pages$EditPaymentSchedule.readonlyMode && ( pages$EditPaymentSchedule.systemPayment ||
																              pages$EditPaymentSchedule.nonSystemPaymentExemptionAllowed )
																           }"
																	value="#{msg['paymentSchedule.systemPaymentChangeReason']}:"></h:outputLabel>
															</td>
															<td>
															    <t:inputTextarea id="txtReason" styleClass="TEXTAREA" value="#{pages$EditPaymentSchedule.reason}"
																rendered="#{ !pages$EditPaymentSchedule.readonlyMode && ( pages$EditPaymentSchedule.systemPayment ||
																              pages$EditPaymentSchedule.nonSystemPaymentExemptionAllowed )
																           }"/>
																
															</td>
														</tr>
													</table>

												</div>
												<table width="90%">
													<tr>

														<td colspan="6" class="BUTTON_TD">
															<h:commandButton type="submit" styleClass="BUTTON"
																action="#{pages$EditPaymentSchedule.btnAdd_Click}"
																value="#{msg['commons.Add']}"
																rendered="#{!pages$EditPaymentSchedule.readonlyMode || !pages$EditPaymentSchedule.cancelContractBean}" />
															
															<t:commandButton styleClass="BUTTON"
																value="#{msg['commons.closeButton']}"
																onclick="javascript:window.close();"
																style="width: 75px; margin-right: 2px;margin-left: 2px;"
																rendered="#{pages$EditPaymentSchedule.readonlyMode}" />

														</td>
													</tr>
												</table>
											</div>
											</div>
						                </h:form>
						
						
						
					          </td>
				         </tr>
			          </table>

			</td>
			</tr>

			</table>

		</body>
	</html>
</f:view>

