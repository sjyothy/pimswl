

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}"
style="overflow:hidden;">
<head>
		<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
		

		<script language="javascript" type="text/javascript">
  
   
  
	 function sendToParent()//tenantId,tenantNum,typeId)
	{
	   
      alert("in here"); 
     //  window.opener.populateTenant(tenantId,tenantNum,typeId);
       window.opener.document.forms[0].submit();
		window.close();
	  
	}
	
		function clearWindow()
	{
	
	




	        document.getElementById("formFacilityList:txtName").value="";
		    document.getElementById("formFacilityList:txtrentValue").value="";
		         
	        var cmbFacilityType=document.getElementById("formFacilityList:selectFacilityType");
		     cmbFacilityType.selectedIndex = 0;       
    


	
		    }
	
    </script>

<%

   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1

   response.setHeader("Pragma","no-cache"); //HTTP 1.0

   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

%>



	</head>
<body dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">
     <div class="containerDiv">
    <!-- Header --> 
<table width="100%" cellpadding="0" cellspacing="0"  border="0">
    <tr>
    <td colspan="2">
        <jsp:include page="header.jsp"/>
    </td>
    </tr>

<tr width="100%">
			<%if (request.getAttribute("modeSelectOnePopUp")!=null) {%>
			<input type="hidden" id ="personName" value="<%=request.getAttribute("modeSelectOnePopUp") %>" >
			<%} else { %>
    <td class="divLeftMenuWithBody" width="17%" >
        <jsp:include page="leftmenu.jsp"/>
    </td>
    		<%}%>
    <td width="83%"  valign="top" class="divBackgroundBody">
        <table class="greyPanelTable" cellpadding="0" cellspacing="0"  border="0">
          <tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['facility.facilityList']}" styleClass="HEADER_FONT"/>
						</td>
						<td width="100%">
							&nbsp;
						</td>
					</tr>
        </table>
    
        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
          <tr valign="top">
             <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
             </td> 
	
  <td width="100%"  valign="top" nowrap="nowrap">
			
			<h:form id="formFacilityList"> 
			<div class="SCROLLABLE_SECTION" 	>
		<div style="height:25px;">	
			<table border="0" class="layoutTable">
												<tr>
      									    	      <td colspan="6">
														<h:outputText value="#{pages$FacilityList.errorMessages}" escape="false" styleClass="INFO_FONT" style="padding:10px;"/>
													  </td>
												</tr>
									</table>
			</div>						
			  <div class= "MARGIN" style="width:95%;">
				<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
				</table>
				 <div class="DETAIL_SECTION" style="width:99.6%;">
             		<h:outputLabel value="#{msg['inquiry.searchcriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>				
							<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" width="100%" >
									<tr>
										<td width="97%">
											<table width="100%">
			
      									    	
												
												<tr>
													<td width="25%">
														<h:outputLabel value="#{msg['facility.facilityName']}:" styleClass="LABEL"></h:outputLabel>
													</td>
													<td width="25%" >
															<h:inputText   id="txtName" value="#{pages$FacilityList.name}"/>
														
													</td>
													<td width="25%">
														<h:outputLabel value="#{msg['facility.rentValue']}:" styleClass="LABEL"></h:outputLabel>
													</td>
													<td  width="25%">
												        <h:inputText  id="txtrentValue"  styleClass="A_RIGHT_NUM" value="#{pages$FacilityList.rentValue}">
												        
												        </h:inputText>
													</td>
													
												</tr>
												<tr>
												   
												      <td>
														<h:outputLabel value="#{msg['facility.facilityType']}:"  styleClass="LABEL"></h:outputLabel>
													</td>
													<td >
															
													    <h:selectOneMenu  id="selectFacilityType" value="#{pages$FacilityList.selectOneFacilityType}" >
													       <f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1"/>
															<f:selectItems value="#{pages$ApplicationBean.facilityType}"/>
														</h:selectOneMenu>
													
													</td>
													
													</tr>
														
													</td>													
													
												
												</table>
											</td>
											<td width="3%">
												&nbsp;
											</td>
										</tr>
											
																								
											<tr>
												 
                                                <td colspan="4"  class="BUTTON_TD">
                              	                       <h:commandButton styleClass="BUTTON" type= "submit" value="#{msg['commons.search']}"  action="#{pages$FacilityList.btnSearch_Click}" > </h:commandButton>
												       <h:commandButton styleClass="BUTTON" type="button" onclick="javascript:clearWindow();" value="#{msg['commons.clear']}" >  </h:commandButton>
													   <pims:security screen="Pims.PropertyManagement.Facility.FacilityList" action="create">   
                                      						<h:commandButton styleClass="BUTTON" type= "submit" value="#{msg['commons.Add']}" binding="#{pages$FacilityList.btnAddFacility}" action="#{pages$FacilityList.btnAddFacility_Click}">	
									 						</h:commandButton>
							      					   </pims:security>	
							      					   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                		        </td>
                                          </tr>
									       
									       <h:inputHidden id="hdnMode"  value="#{pages$FacilityList.hdnMode}"  />
									       				
      									 </table>
      									 </div>
                                 		</div>		    
                   			               
                   			              	<div id="divheader" class="MARGIN" style="width:95%;">
                   		                      <div class="imag">&nbsp;</div> 
                   			                    <div id="conDiv" class="contentDiv"  style="width:99%" >
										         <t:dataTable id="FacilityGrid"
													value="#{pages$FacilityList.gridDataList}"
													binding="#{pages$FacilityList.dataTable}"
												    rows="#{pages$FacilityList.paginatorRows}"
												    preserveDataModel="true" preserveSort="false" var="dataItem"
												    rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												    width="100%">

													<t:column id="name"  sortable="true">
														<f:facet name="header" >
															<t:outputText value="#{msg['facility.facilityName']}" />
														</f:facet>
														<t:outputText  id="facname" styleClass="A_LEFT"  value="#{dataItem.facilityName}" style="white-space: normal;"/>
													</t:column>
													<t:column id="description" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['facility.description']}" />
													</f:facet>
													<t:outputText id="desc" styleClass="A_LEFT" value="#{dataItem.description}"
													style="white-space: normal;" />
													</t:column>
													<t:column id="rentValue"  >
														<f:facet name="header">
															<t:outputText value= "#{msg['facility.rentValue']}" />
														</f:facet>
															<t:outputText id="ren" styleClass="A_RIGHT_NUM" value="#{dataItem.rent}" 
														style="white-space: normal;"/>
												</t:column>
					                       
					                       				
								
							<pims:security screen="Pims.PropertyManagement.Facility.FacilityList" action="create">
								<t:column id="statusbtn" >
									<f:facet name="header">
										<t:outputText value="#{msg['commons.actions']}" />
									</f:facet>
									
									<t:commandLink action="#{pages$FacilityList.cmdFacilityName_Click}"  >&nbsp;
									   <h:graphicImage id="editIcon" title="#{msg['commons.edit']}" url="../resources/images/edit-icon.gif" />&nbsp;
									</t:commandLink>
									
								  <t:commandLink id="sadads"  rendered="#{dataItem.enable}"    action="#{pages$FacilityList.cmdStatus_Click}">&nbsp;
									 <h:graphicImage id="enableIcon" title="#{msg['commons.clickDisable']}" url="../resources/images/app_icons/enable.png" />&nbsp;
								  </t:commandLink>
								  
								  <t:commandLink id="dasdd" rendered="#{dataItem.disable}"  action="#{pages$FacilityList.cmdStatus_Click}">&nbsp;
									 <h:graphicImage id="disableIcon" title="#{msg['commons.clickEnable']}" url="../resources/images/app_icons/disable.png" />&nbsp;
								  </t:commandLink>
								   <t:commandLink id="sdasd"  action="#{pages$FacilityList.cmdDelete_Click}">&nbsp;
									 <h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
								  </t:commandLink>
									<t:outputText />
								</t:column>
                             </pims:security>                
								
                                            
                                        </t:dataTable>										
											
												
											</div>
											<div id="footer" class="contentDivFooter" style="width: 100%;">
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$FacilityList.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
											<CENTER>
												<t:dataScroller id="scroller" for="FacilityGrid" paginator="true"
													fastStep="1" paginatorMaxPages="#{pages$FacilityList.paginatorMaxPages}" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 
												    pageIndexVar="pageNumber"
												    styleClass="SCH_SCROLLER"
												    paginatorActiveColumnStyle="font-weight:bold;"
												    paginatorRenderLinkForActive="false" 
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

								                    	<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
													
													<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>
													
												</t:dataScroller>
												</CENTER>
												</td></tr>
												</table>
                                            </div>
                                          </div> 
											
												
</div>
			</h:form>
			
			</td>
			</tr>
			</table>
			</td>
			</tr>
			<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
			</body>
		</f:view>
	
</html>
