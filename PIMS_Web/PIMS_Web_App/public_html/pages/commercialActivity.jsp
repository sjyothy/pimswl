     <t:div styleClass="contentDiv" style="width:90%"  >
		                                       <t:dataTable id="commercialActivityDataTable" styleClass="grid" 
													rows="5"
													value="#{pages$commercialActivity.commercialActivityList}"
													binding="#{pages$commercialActivity.commercialActivityDataTable}"
													preserveDataModel="false" preserveSort="false" var="commercialActivityItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												>
												<t:column id="colActivityName" >


													<f:facet name="header" >

														<t:outputText value="#{msg['contract.tab.CommercialActivity.Name']}" />

													</f:facet>
													<t:outputText  styleClass="A_LEFT" value="#{commercialActivityItem.commercialActivityName}" />
												</t:column>
												<t:column id="colActivityDescEn" rendered="#{pages$LeaseContract.isEnglishLocale}">


													<f:facet name="header">

														<t:outputText  value="#{msg['contract.tab.CommercialActivity.description']}" />

													</f:facet>
													<t:outputText title="" styleClass="A_LEFT" value="#{commercialActivityItem.commercialActivityDescEn}" />
												</t:column>
												<t:column id="colActivityDescAr" rendered="#{pages$LeaseContract.isArabicLocale}">


													<f:facet name="header">

														<t:outputText  value="#{msg['contract.tab.CommercialActivity.description']}" />

													</f:facet>
													<t:outputText title=""  styleClass="A_LEFT" value="#{commercialActivityItem.commercialActivityDescAr}" />
												</t:column>
												<t:column id="colActivitySelected" >


													<f:facet name="header">
														<t:outputText  value="#{msg['commons.select']}" />
													</f:facet>
													    <t:selectBooleanCheckbox value="#{commercialActivityItem.isSelected}"  />
												       
												</t:column>
												
											  </t:dataTable>										
											</t:div>
											<t:div styleClass="contentDivFooter" style="width:90%">

												<t:dataScroller id="scrollerd" for="commercialActivityDataTable" paginator="true"
													fastStep="1" paginatorMaxPages="2" immediate="false"
													styleClass="scroller" paginatorTableClass="paginator" 
													renderFacetsIfSinglePage="true" paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">
                                                	<f:facet name="first">
														
														<t:graphicImage url="../resources/images/first_btn.gif" id="lblF"></t:graphicImage>
													</f:facet>

													<f:facet name="fastrewind">
														<t:graphicImage url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>
													</f:facet>

													<f:facet name="fastforward">
														<t:graphicImage url="../resources/images/next_btn.gif" id="lblFF"></t:graphicImage>
													</f:facet>

													<f:facet name="last">
														<t:graphicImage url="../resources/images/last_btn.gif" id="lblL"></t:graphicImage>
													</f:facet>

												</t:dataScroller>
									
                                           </t:div>