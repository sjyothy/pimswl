
<script type="text/javascript">

						
</script>
<t:div styleClass="TAB_DETAIL_SECTION" style="width:100%" >
	<t:panelGrid id="contractDetailsTable" cellpadding="1px" width="100%" cellspacing="5px" styleClass="TAB_DETAIL_SECTION_INNER"  columns="4"
	columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
					
             <h:outputLabel styleClass="LABEL"  value="#{msg['commons.referencenumber']}" />
		    <h:inputText maxlength="20"   id="txtContractRefNum"  styleClass="READONLY" readonly="true" binding="#{pages$consultantContractDetailsTab.txtContractNo}" value="#{pages$consultantContractDetailsTab.contractNo}" ></h:inputText>
	       <h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/> 
	        <h:outputLabel id="lblContractType" styleClass="LABEL"  value="#{msg['contract.contractType']}"></h:outputLabel>
	        </h:panelGroup>
	        <h:inputText maxlength="20"   id="txtContractType" styleClass="READONLY" readonly="true" binding="#{pages$consultantContractDetailsTab.txtContractType}" value="#{pages$consultantContractDetailsTab.contractType}" ></h:inputText>
	        
			<h:outputLabel styleClass="LABEL" value="#{msg['serviceContract.tender.number']}"></h:outputLabel>
	        <h:panelGroup>
	          <h:inputText id="txtTenderNumber"  styleClass="READONLY" readonly="true" binding="#{pages$consultantContractDetailsTab.txtTenderNum}" title="#{pages$consultantContractDetailsTab.txtTenderNum}"  value="#{pages$consultantContractDetailsTab.tenderNum}" ></h:inputText>
	          <h:graphicImage  url="../resources/images/app_icons/Search-tenant.png" onclick="javaScript:showSearchTenderPopUp('#{pages$consultantContractDetailsTab.tenderSearchKey_tenderType}','#{pages$consultantContractDetailsTab.tenderSearchKey_WinnerContractorPresent}','#{pages$consultantContractDetailsTab.tenderSearchKey_NotBindedWithContract}');" binding="#{pages$consultantContractDetailsTab.imgSearchTender}"   style ="MARGIN: 0px 0px -4px;" alt="#{msg[commons.searchTender]}" ></h:graphicImage>
	          <h:graphicImage  url="../resources/images/delete_icon.png" onclick="javaScript:document.getElementById('formRequest:cmdDeleteTender').onclick();" binding="#{pages$consultantContractDetailsTab.imgRemoveTender}"   style ="MARGIN: 0px 0px -4px;" alt="#{msg[commons.delete]}" ></h:graphicImage>

     		</h:panelGroup>
     		<h:outputLabel styleClass="LABEL"  value="#{msg['serviceContract.tender.description']}"></h:outputLabel>
            <h:inputText id="txtTenderDesc" styleClass="READONLY" readonly="true"   binding="#{pages$consultantContractDetailsTab.txtTenderDesc}"  value="#{pages$consultantContractDetailsTab.tenderDesc}" title="#{pages$consultantContractDetailsTab.tenderDesc}" ></h:inputText>
	        <h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
	        <h:outputLabel styleClass="LABEL" value="#{msg['serviceContract.contractor.number']}"></h:outputLabel>
	        </h:panelGroup>
	        <h:panelGroup>
	          <h:inputText id="txtContractorNumber"    styleClass="READONLY"readonly="true" binding="#{pages$consultantContractDetailsTab.txtContractorNum}"  title="#{pages$consultantContractDetailsTab.contractorNum}"  value="#{pages$consultantContractDetailsTab.contractorNum}" ></h:inputText>
	          <h:graphicImage  url="../resources/images/app_icons/Search-Person.png" onclick="javaScript:showSearchContractorPopUp('#{pages$consultantContractDetailsTab.contractorScreenQueryStringViewMode}','#{pages$consultantContractDetailsTab.contractorScreenQueryStringPopUpMode}');"  binding="#{pages$consultantContractDetailsTab.imgSearchContractor}" style ="MARGIN: 0px 0px -4px;" alt="#{msg[contract.searchTenant]}" ></h:graphicImage>
            </h:panelGroup>
            <h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
     		<h:outputLabel styleClass="LABEL"  value="#{msg['serviceContract.contractor.name']}"></h:outputLabel>
     		</h:panelGroup>
            <h:inputText id="txtContractorName" styleClass="READONLY" readonly="true"  binding="#{pages$consultantContractDetailsTab.txtContractorName}" value="#{pages$consultantContractDetailsTab.contractorName}" title="#{pages$consultantContractDetailsTab.contractorName}" ></h:inputText>
	        <h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
	        <h:outputLabel  styleClass="LABEL"  value="#{msg['contract.date.Start']}"></h:outputLabel>
	        </h:panelGroup>
			<rich:calendar  id="contractStartDate" value="#{pages$consultantContractDetailsTab.contractStartDate}" 
							popup="true" datePattern="#{pages$consultantContractDetailsTab.dateFormat}"
                            binding="#{pages$consultantContractDetailsTab.startDateCalendar}" 
                            timeZone="#{pages$consultantContractDetailsTab.timeZone}"
                            showApplyButton="false" 
                            enableManualInput="false" cellWidth="24px" 
                            locale="#{pages$consultantContractDetailsTab.locale}"
          >
          </rich:calendar>
          <h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
		  <h:outputLabel   value="#{msg['contract.date.expiry']}"></h:outputLabel>
		  </h:panelGroup>
		  <rich:calendar id="datetimeContractEndDate" value="#{pages$consultantContractDetailsTab.contractEndDate}" 
		              binding="#{pages$consultantContractDetailsTab.endDateCalendar}"
                      popup="true" datePattern="#{pages$consultantContractDetailsTab.dateFormat}" timeZone="#{pages$consultantContractDetailsTab.timeZone}" showApplyButton="false" 
                      enableManualInput="false"  cellWidth="24px" 
                      >
           </rich:calendar>
          <h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/> 
            <h:outputText	value="#{msg['serviceContract.PaymentType']}:"></h:outputText>
          </h:panelGroup>
			<h:selectOneMenu id="selectPaymentType"  value="#{pages$consultantContractDetailsTab.selectPaymentType}" binding="#{pages$consultantContractDetailsTab.cmbSelectPaymentType}" >
					<f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems value="#{pages$ApplicationBean.constructionPaymentTypeList}" />
				<a4j:support event="onchange"  
							 reRender="txtDesignPercentage,txtSupervisionPercentage,txtDesignAmount,txtSupervisionAmount,totalContract" 
							 action="#{pages$consultantContract.onConsultantPaymentType}"	/>	
					
			</h:selectOneMenu>
		    <h:outputLabel styleClass="LABEL"  value="#{msg['consultantContractDetailTab.designPercentage']}"></h:outputLabel>
		    <h:panelGroup>
			    <h:inputText id="txtDesignPercentage" onchange="javascript:calculateDesignAmount();"   styleClass="A_RIGHT_NUM #{pages$consultantContractDetailsTab.readOnlyPercentage}" maxlength="15"    binding="#{pages$consultantContractDetailsTab.txtdesignPercentage}" value="#{pages$consultantContractDetailsTab.designPercentage}">%
			    </h:inputText>
			    
			    <a4j:commandLink id="lnkCalculateDesignAmount" action="#{pages$consultantContract.onCalculateDesignAmount}" reRender="txtDesignAmount,totalContract,txtBankGuaranteeAmount" />
		    </h:panelGroup>
		    
		    <h:outputLabel styleClass="LABEL"  value="#{msg['consultantContractDetailTab.supervisionPercentage']}"></h:outputLabel>
		    <h:panelGroup>
			    <h:inputText id="txtSupervisionPercentage" onchange="javascript:calculateSupervisionAmount();"  styleClass="A_RIGHT_NUM #{pages$consultantContractDetailsTab.readOnlyPercentage}" maxlength="15"    binding="#{pages$consultantContractDetailsTab.txtsupervisionPercentage}" value="#{pages$consultantContractDetailsTab.supervisionPercentage}">%
			    </h:inputText>
			    
			    <a4j:commandLink id="lnkCalculateSupervision" action="#{pages$consultantContract.onCalculateSupervisionAmount}" reRender="txtSupervisionAmount,totalContract,txtBankGuaranteeAmount" />
		    </h:panelGroup>
		    
		    <h:outputLabel styleClass="LABEL"  value="#{msg['consultantContractDetailTab.designAmount']}"></h:outputLabel>
		   <h:panelGroup> 
		     <h:inputText id="txtDesignAmount"  onchange="javascript:calculateTotalAmountDesign();"   styleClass="A_RIGHT_NUM #{pages$consultantContractDetailsTab.readOnlyAmount}" maxlength="15"    binding="#{pages$consultantContractDetailsTab.txtdesignAmount}" value="#{pages$consultantContractDetailsTab.designAmount}">
		      
		     </h:inputText>
		    
		    <a4j:commandLink id="lnkCalculateTotalAmountDesign" action="#{pages$consultantContract.onCalculateTotalAmountBasedDesign}" reRender="totalContract,txtBankGuaranteeAmount" />
		   </h:panelGroup> 
		    
		    
		    <h:outputLabel styleClass="LABEL"  value="#{msg['consultantContractDetailTab.supervisionAmount']}"></h:outputLabel>
		   <h:panelGroup>  
		    <h:inputText id="txtSupervisionAmount" onchange="javascript:calculateTotalAmountSupervision();"      styleClass="A_RIGHT_NUM #{pages$consultantContractDetailsTab.readOnlyAmount}" maxlength="15"    binding="#{pages$consultantContractDetailsTab.txtsupervisionAmount}" value="#{pages$consultantContractDetailsTab.supervisionAmount}">
		      
		    </h:inputText>
		    
		    <a4j:commandLink id="lnkCalculateTotalAmountSupervision" action="#{pages$consultantContract.onCalculateTotalAmountBasedSupervision}" reRender="totalContract,txtBankGuaranteeAmount" />
		  </h:panelGroup>  
           <h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
			<h:outputLabel styleClass="LABEL"  value="#{msg['contract.Amount']}"></h:outputLabel>
			</h:panelGroup>
		    <h:inputText id="totalContract"    styleClass="A_RIGHT_NUM READONLY" maxlength="15" readonly="true" value="#{pages$consultantContractDetailsTab.totalAmount}">
		      <f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="#{pages$consultantContractDetailsTab.numberFormat}"/>
		    </h:inputText>
           
            <h:outputLabel styleClass="LABEL"  value="#{msg['serviceContract.guarantee.percentage']}"></h:outputLabel>
		    <h:inputText
		    	disabled="#{pages$consultantContractDetailsTab.guaranteeFieldReadOnly}"
		    	  id="txtGuaranteePercentage"    styleClass="A_RIGHT_NUM" maxlength="7"  binding="#{pages$consultantContractDetailsTab.txtGuaranteePercentage}" value="#{pages$consultantContractDetailsTab.guaranteePercentage}">%
		   		 <a4j:support event="onchange"
						action="#{pages$consultantContract.calculateGuaranteeAmount}"
						reRender="txtBankGuaranteeAmount" />
		    </h:inputText>
		   
		    <h:outputLabel styleClass="LABEL"  value="#{msg['serviceContract.guarantee.bankAmount']}"></h:outputLabel>
		    <h:inputText id="txtBankGuaranteeAmount"   styleClass="A_RIGHT_NUM READONLY" maxlength="15"  readonly="true" binding="#{pages$consultantContractDetailsTab.txtGuaranteeAmont}"  value="#{pages$consultantContractDetailsTab.guaranteeAmont}"></h:inputText>
		    <h:outputText	value="#{msg['serviceContract.bankName']}:"></h:outputText>
			<h:selectOneMenu 
				disabled="#{pages$consultantContractDetailsTab.guaranteeFieldReadOnly}"
				id="selectBank"  value="#{pages$consultantContractDetailsTab.selectOneBank}" binding="#{pages$consultantContractDetailsTab.cmbBank}" >
					<f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems value="#{pages$consultantContractDetailsTab.banks}" />
			</h:selectOneMenu>
			<h:outputLabel value="#{msg['serviceContract.guarantee.expiryDate']}"></h:outputLabel>
			<rich:calendar id="guaranteeExpiryDate"
			disabled="#{pages$consultantContractDetailsTab.guaranteeFieldReadOnly}"
			value="#{pages$consultantContractDetailsTab.guaranteeExpiry}"
			binding="#{pages$consultantContractDetailsTab.guaranteeExpiryDate}"
			popup="true"
			datePattern="#{pages$consultantContractDetailsTab.dateFormat}"
			timeZone="#{pages$consultantContractDetailsTab.timeZone}"
			showApplyButton="false" enableManualInput="false" cellWidth="24px"
		>
		</rich:calendar>													        
   </t:panelGrid>		              
</t:div>
