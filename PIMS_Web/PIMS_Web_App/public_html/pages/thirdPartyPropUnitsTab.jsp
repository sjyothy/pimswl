<%-- Author: Kamran Ahmed--%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<t:div>
	<%-- Inherited Assets Tab --%>
	<t:panelGrid border="0" width="100%" cellpadding="1" cellspacing="1"
		id="thirdPartyManagedTab">
		<t:panelGroup>
			<%--Column 1,2 Starts--%>
			<t:panelGrid columns="4"
				columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

				<h:outputText value="#{msg['thirdPartyUnit.label.propertyNumber']}" />
				<h:inputText id="propertyNumberId"
					value="#{pages$thirdPartyPropUnitsTab.thirdPartyPropUnit.propertyRefNumber}"
					styleClass="READONLY" disabled="true" />

				<h:outputText value="#{msg['thirdPartyUnit.label.propertyNameEn']}" />
				<h:inputText id="propertyNameEnId"
					value="#{pages$thirdPartyPropUnitsTab.thirdPartyPropUnit.propertyNameEn}"
					styleClass="READONLY" disabled="true" />

				<h:outputText value="#{msg['thirdPartyUnit.label.propertyNameAr']}" />
				<h:inputText id="propertyNameArId"
					value="#{pages$thirdPartyPropUnitsTab.thirdPartyPropUnit.propertyNameAr}"
					styleClass="READONLY" disabled="true" />

				<h:outputText value="#{msg['thirdPartyUnit.label.unitNumber']}" />
				<h:inputText id="unitNumberId"
					value="#{pages$thirdPartyPropUnitsTab.thirdPartyPropUnit.unitNumber}"
					maxlength="50" />

				<h:outputText value="#{msg['thirdPartyUnit.label.floorNumber']}" />
				<h:inputText id="floorNumberId"
					value="#{pages$thirdPartyPropUnitsTab.thirdPartyPropUnit.floorNumber}"
					maxlength="50" />

				<h:outputLink />
				<h:outputLink />
			</t:panelGrid>

			<t:panelGrid columns="4"
				columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
				<h:outputText value="#{msg['thirdPartyUnit.label.description']}" />
				<h:inputTextarea id="descriptionId"
					value="#{pages$thirdPartyPropUnitsTab.thirdPartyPropUnit.description}"
					onchange="removeExtraCharacter(this,250);"
					onblur="removeExtraCharacter(this,250);"
					onclick="removeExtraCharacter(this,250);"
					onkeyup="removeExtraCharacter(this,250);" rows="5" cols="114" />
			</t:panelGrid>
		</t:panelGroup>
		<%--Column 3,4 Ends--%>
	</t:panelGrid>

	<t:div style="height:5px;"></t:div>
	<t:div style="width:100%;">
		<t:div styleClass="A_RIGHT">
			<h:commandButton value="#{msg['commons.saveButton']}"
				action="#{pages$thirdPartyPropUnitsTab.onSave}" styleClass="BUTTON"
				style="width:10%;"></h:commandButton>
			<h:outputText value=" "></h:outputText>
			<h:commandButton value="#{msg['commons.clear']}" styleClass="BUTTON"
				onclick="clearThirdPartyFields(this);" type="reset"
				style="width:10%;"></h:commandButton>
			<h:commandButton value="#{msg['commons.closeButton']}" styleClass="BUTTON" 
			    rendered="#{pages$thirdPartyPropUnitsTab.viewModePopup}"
				onclick="closeWindow();" type="button"
				style="width:10%;"></h:commandButton>
		</t:div>
		<t:div style="height:5px;"></t:div>
		<t:div styleClass="contentDiv" style="width:98%">
			<t:dataTable rows="15"
				binding="#{pages$thirdPartyPropUnitsTab.dtThirdPartyPropUnit}"
				value="#{pages$thirdPartyPropUnitsTab.thirdPartyPropUnits}"
				preserveDataModel="true" preserveSort="false" var="dataItem"
				rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
				width="100%">

				<t:column sortable="true" id="propertyNumber">
					<f:facet name="header">
						<h:outputText
							value="#{msg['thirdPartyUnit.label.propertyNumber']}" />
					</f:facet>
					<h:outputText value="#{dataItem.propertyRefNumber}" />
				</t:column>

				<t:column sortable="true" id="propertyNameEn">
					<f:facet name="header">
						<h:outputText
							value="#{msg['thirdPartyUnit.label.propertyNameEn']}" />
					</f:facet>
					<h:outputText value="#{dataItem.propertyNameEn}" />
				</t:column>

				<t:column sortable="true" id="propertyNameAr">
					<f:facet name="header">
						<h:outputText
							value="#{msg['thirdPartyUnit.label.propertyNameAr']}" />
					</f:facet>
					<h:outputText value="#{dataItem.propertyNameAr}" />
				</t:column>

				<t:column sortable="true" id="unitNumber">
					<f:facet name="header">
						<h:outputText value="#{msg['thirdPartyUnit.label.unitNumber']}" />
					</f:facet>
					<h:outputText value="#{dataItem.unitNumber}" />
				</t:column>

				<t:column sortable="true" id="floorNumber">
					<f:facet name="header">
						<h:outputText value="#{msg['thirdPartyUnit.label.floorNumber']}" />
					</f:facet>
					<h:outputText value="#{dataItem.floorNumber}" />
				</t:column>

				<t:column sortable="true" id="description">
					<f:facet name="header">
						<h:outputText value="#{msg['thirdPartyUnit.label.description']}" />
					</f:facet>
					<h:outputText value="#{dataItem.description}" />
				</t:column>

				<t:column sortable="true" id="createdBy">
					<f:facet name="header">
						<h:outputText value="#{msg['thirdPartyUnit.label.createdBy']}" />
					</f:facet>
					<h:outputText
						value="#{pages$thirdPartyPropUnitsTab.englishLocale?dataItem.createdByEn:dataItem.createdByAr}" />
				</t:column>

				<t:column sortable="true" id="createdOn">
					<f:facet name="header">
						<h:outputText value="#{msg['thirdPartyUnit.label.createdOn']}" />
					</f:facet>
					<h:outputText value="#{dataItem.createdOn}" />
				</t:column>



				<t:column id="actionCol" sortable="false">
					<f:facet name="header">
						<t:outputText
							value="#{msg['mems.inheritanceFile.columnLabel.action']}" />
					</f:facet>

					<t:commandLink action="#{pages$thirdPartyPropUnitsTab.onEdit}"
						rendered="#{!dataItem.selected}">
						<h:graphicImage title="#{msg['commons.edit']}"
							style="cursor:hand;" url="../resources/images/edit-icon.gif" />&nbsp;
				</t:commandLink>

					<t:commandLink rendered="#{!dataItem.selected}"
						onclick="if (!confirm('#{msg['thirdPartyUnit.confirmMsg.sureToDelete']}')) return false;"
						action="#{pages$thirdPartyPropUnitsTab.onDelete}">
						<h:graphicImage title="#{msg['commons.delete']}"
							style="cursor:hand;" url="../resources/images/delete.gif" />&nbsp;
				</t:commandLink>
				</t:column>
			</t:dataTable>
		</t:div>
	</t:div>
</t:div>