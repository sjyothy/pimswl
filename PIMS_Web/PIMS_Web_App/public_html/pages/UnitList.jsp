
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>



<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html style="overflow:hidden;">
<head>
		<title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>	
		
		
		<script language="javascript" type="text/javascript">
  
    var selectedUnits;

    function RowDoubleClick()
	{
       window.opener.populateSelectManyUsers(selectedUnits);
	   window.close();
	  
	}
	 function sendToParent()
	{
	 var unitAreaMin  = document.getElementById("formUnits:txtUnitAreaMin").value;
     var unitAreaMax = document.getElementById("formUnits:txtUnitAreaMax").value;
     var livingMax	= document.getElementById("formUnits:txtLivingMax").value;
	 var bedsMax = document.getElementById("formUnits:txtBedsMax").value;
	 var bathMax =  document.getElementById("formUnits:txtBathMax").value;
	 var rentMin =  document.getElementById("formUnits:txtRentMin").value;
	 var rentMax = document.getElementById("formUnits:txtRentMax").value;
	 var floorMin =  document.getElementById("formUnits:txtFloorMin").value;
	 
	 var objSelectUnitType = document.getElementById("formUnits:selectUnitType");
	 var objSelectUnitUsage = document.getElementById("formUnits:selectUnitUsage");
	 var objSelectUnitSide = document.getElementById("formUnits:selectUnitSide");
	 var selectedUnitHdn = document.getElementById("formUnits:hdnSelectedUnit");
	 
	 
	 
	       
       
       window.opener.populateSelectManyUsers(selectedUnitHdn,unitAreaMin
       ,unitAreaMax,livingMax,bedsMax,bathMax,rentMin,rentMax,floorMin
       ,objSelectUnitType,objSelectUnitUsage,objSelectUnitSide);
		window.close();
	  
	}
	function AddUnits(field,unitNumber)
	{

    
	  if(field.checked)
	  {
		    if(selectedUnits!=null && selectedUnits.length>0 )
		    {
	
		       selectedUnits=selectedUnits+","+unitNumber;
		    }
		     else
		     selectedUnits=unitNumber;
	   }
	   else
	   {
		   var delimiters=",";
		   var unitArr=selectedUnits.split(delimiters);
		   selectedUnits="";
		   for(i=0;i<unitArr.length;i++)
		   {
		   if(unitArr[i]!=unitNumber)
		    {
	       	   if(selectedUnits.length>0 )
		       {
		             selectedUnits=selectedUnits+","+unitArr[i];
	
               }
		       else
		             selectedUnits=unitArr[i];
		     
		    }
		    
		    
		   }
	   
	   }
	     
	   
	
	
	
	}
	function clearWindow()
	{
	        document.getElementById("formUnits:txtUnitNumber").value="";
	        document.getElementById("formUnits:txtRentMin").value="";
		    document.getElementById("formUnits:txtRentMax").value="";
		    document.getElementById("formUnits:txtBedsMin").value="";
		    document.getElementById("formUnits:txtBedsMax").value="";
	
	        var cmbUnitType=document.getElementById("formUnits:selectUnitType");
		     cmbUnitType.selectedIndex = 0;
		    var cmbUnitUsage=document.getElementById("formUnits:selectUnitUsage");
		     cmbUnitUsage.selectedIndex = 0;
		    var cmbCountry=document.getElementById("formUnits:country");
		     cmbCountry.selectedIndex = 0;
		    var cmbState=document.getElementById("formUnits:state");
		     cmbState.selectedIndex = 0;
		    var cmbCity=document.getElementById("formUnits:selectcity");
		     cmbCity.selectedIndex = 0;
	
	}
	
	function closeWindow(){
	
	window.close();
	
	}
    	function Calendar(control,control2,x,y)
		{
		  var ctl2Name="formUnits:"+control2;
		  
		  var ctl2=document.getElementById(ctl2Name);
		  showCalendar(control,ctl2,'dd/mm/yyyy','','',x,y); 
		  return false;
		
		
		}
    </script>

<%

   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1

   response.setHeader("Pragma","no-cache"); //HTTP 1.0

   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

%>



	</head>
<body dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE"> 
    <!-- Header --> 
<table width="100%" cellpadding="0" cellspacing="0"  border="0">
    

<tr width="100%">
    
    <td width="100%" height="616px" valign="top" class="divBackgroundBody">
        <table width="100%" class="greyPanelTable" cellpadding="0" cellspacing="0"  border="0">
       	<tr height="1%">
			<td>
				<table width="99.2%" class="greyPanelTable" cellpadding="0"
					cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
								<h:outputLabel value="#{msg['units.unitsSearch']}" styleClass="HEADER_FONT"/>
						</td>
						<td width="100%">
							&nbsp;
						</td>
					</tr>
        </table>
    
        <table width="100%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
          <tr valign="top">
             <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
             </td> 
	
	  
  <td width="100%" height="554px"  valign="top" nowrap="nowrap">
			
			
			
			
           <div class="SCROLLABLE_SECTION_POPUP" style="height:100%"  >			
			 <h:form id="formUnits" style="width:98%">
		                <div style="padding-left:10px;" styleClass="DETAIL_SECTION">			 
					      <table border="0" class="layoutTable">
									<tr id="trerrorMessage">
   									      <td id="tderrorMessage" colspan="6">
											<h:outputText id="hdnerrorMessage" value="#{pages$UnitList.errorMessages}" escape="false" styleClass="INFO_FONT" />
										  </td>
									</tr>
							</table>
						</div>					
			  <div class= "MARGIN">
				<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
				</table>
				 <div class="DETAIL_SECTION">
 					<h:outputLabel value="#{msg['inquiry.searchcriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
 					<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
      									    	
												<tr>
												   <h:inputHidden id="hdnSelectedUnit" value="#{pages$UnitList.hdnSelectedUnit}"> </h:inputHidden>
													<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.unitType']}:"></h:outputLabel>
													</td>
													<td >
														<h:selectOneMenu  id="selectUnitType" style="width: 122px" value="#{pages$UnitList.selectOneUnitType}" >
															 <f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1"/>
															<f:selectItems value="#{pages$ApplicationBean.unitType}"/>
														</h:selectOneMenu>
													</td>
													<td >
													    <h:outputLabel styleClass="LABEL" value="#{msg['inquiry.unitUsage']}:"></h:outputLabel>
													 </td>   
													 <td>
													     <h:selectOneMenu  id="selectUnitUsage" style="width: 122px" value="#{pages$UnitList.selectOneUnitUsage}" >
															 <f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1"/>
   															<f:selectItems value="#{pages$ApplicationBean.propertyUsageType}"/>
														</h:selectOneMenu>
														
													</td>
													 <td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.unitNumber']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText id="txtUnitNumber" value="#{pages$UnitList.unitNumber}">
												        </h:inputText>
													</td>   										
																																							
												</tr>
												<tr>
												     <td>
												        <h:outputLabel  styleClass="LABEL" value="#{msg['contact.country']}:"></h:outputLabel>
												     </td>   
												        <td>
															<h:selectOneMenu id="country" style="width: 122px;" value="#{pages$UnitList.countryId}"> 
																<f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems value="#{pages$UnitList.countryList}" />
                                                             <a4j:support event="onchange" action="#{pages$UnitList.loadState}" reRender="state,selectcity" />
															</h:selectOneMenu>
														</td>
														<td>	
													     	<h:outputLabel styleClass="LABEL"  value="#{msg['contact.city']}:"></h:outputLabel>
													    </td>
													    <td> 	
														    <h:selectOneMenu id="state"
																style="width: 122px; height: 24px"  required="false"
																value="#{pages$UnitList.stateId}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems value="#{pages$UnitList.stateList}" />
															<a4j:support event="onchange" action="#{pages$UnitList.loadCity}" reRender="selectcity" />
															</h:selectOneMenu>
														</td>
												     
												     <td>
															<h:outputLabel  styleClass="LABEL" value="#{msg['commons.area']}:"></h:outputLabel>
												    </td>
												    <td>
														    <h:selectOneMenu id="selectcity" style="width: 122px;"  value="#{pages$UnitList.cityId}">
														                <f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1"/>
															            <f:selectItems value="#{pages$UnitList.cityList}"/>
														    </h:selectOneMenu>
												
													</td>
											   </tr>
											   <tr>
													<td>
														<h:outputLabel styleClass="LABEL"  value="#{msg['inquiry.rentValueMin']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText id="txtRentMin" styleClass="A_RIGHT_NUM" value="#{pages$UnitList.rentValueMin}">
												        </h:inputText>
													</td>
												
													<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.rentValueMax']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText id="txtRentMax" styleClass="A_RIGHT_NUM" value="#{pages$UnitList.rentValueMax}">
												        </h:inputText>
													</td>
													<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.noOfBedsMin']}:"></h:outputLabel>
												  </td>
												  <td>
												        <h:inputText id="txtBedsMin" styleClass="A_RIGHT_NUM" value="#{pages$UnitList.noOfBedsMin}">
												        </h:inputText>
											     </td>
														    
											   </tr>
											   <tr>
										                     <td>
																	<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.noOfBedsMax']}:"></h:outputLabel>
															</td>
															<td>
															        <h:inputText id="txtBedsMax" styleClass="A_RIGHT_NUM" value="#{pages$UnitList.noOfBedsMax}">
															        </h:inputText>
														    </td>
													 	
												</tr>
												
												
												
												
													
													
												
										
																									
											<tr>
                                                <td colspan="6" class="BUTTON_TD"  >
                              	                       <h:commandButton styleClass="BUTTON" type= "submit" value="#{msg['commons.search']}"  action="#{pages$UnitList.doSearch}" > </h:commandButton>
												       <h:commandButton styleClass="BUTTON" type="button" onclick="javascript:clearWindow();" value="#{msg['commons.reset']}" >  </h:commandButton>
												       <h:commandButton styleClass="BUTTON" type="button" onclick="javascript:closeWindow();"value="#{msg['commons.cancel']}" >	</h:commandButton>

                                		         </td>
                                              </tr>
												
      									    </table>
      									  </div>
      									</div>
                                 		
                                 		<div style="padding:5px;">		    
                   			              <div class="imag">&nbsp;</div>
                   			               <div class="contentDiv"  >
										         <t:dataTable id="test2"
													value="#{pages$UnitList.propertyInquiryDataList}"
													binding="#{pages$UnitList.dataTable}"
													rows="15" width="100%" 
													preserveDataModel="false" preserveSort="false" var="dataItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

													<t:column id="col1"  sortable="true">

														<f:facet name="header" >

															<t:outputText value="#{msg['unit.unitNumber']}" />

														</f:facet>
														<t:outputText value="#{dataItem.unitNumber}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
													</t:column>
                                     	

												<t:column id="col2"  sortable="true" >
													<f:facet name="header">


														<t:outputText value="#{msg['unit.unitDescription']}"  />

													</f:facet>
													<t:outputText value="#{dataItem.unitDesc}" styleClass="A_LEFT" style="white-space: normal;" />
												</t:column>
	

												<t:column id="col3"  >


													<f:facet name="header">

														<t:outputText value= "#{msg['unit.unitArea']}" />

													</f:facet>
													<t:outputText value="#{dataItem.unitArea}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
												</t:column>


	


												<t:column id="col4" >

													<f:facet name="header">
														
														<t:outputText value="#{msg['unit.noOfBed']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.noOfBed}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
													</t:column>
									
										<t:column id="col5" >

													<f:facet name="header">
														
														<t:outputText value="#{msg['unit.noOfLiving']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.noOfLiving}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
													</t:column>
					     
									     <t:column id="col6" >

													<f:facet name="header">
														
														<t:outputText value="#{msg['unit.noOfBath']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.noOfBath}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
													</t:column>
						
										<t:column id="col7" >

													<f:facet name="header">
														
													 <t:outputText value="#{msg['unit.rentValue']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.rentValue}" styleClass="A_RIGHT_NUM" style="white-space: normal;"/>
													</t:column>
						
											

                                     		<t:column id="col9"  >

	                                            <f:facet name="header">
	                                            <t:outputText value="#{msg['commons.select']}" />
	                                            </f:facet>
	                                            <h:selectBooleanCheckbox id="select" value="#{dataItem.selected}"
	                                            onclick="javascript:AddUnits(this,'#{dataItem.unitId}_#{dataItem.unitNumber}');"
	                                            
	                                            />
                                                                            
                                           </t:column>		
                                            
                                        </t:dataTable>										
											</div>
											
											<div class="contentDivFooter" style="width: 98.8%">
											
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$UnitList.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
											<CENTER>
												<t:dataScroller id="scroller" for="test2" paginator="true"
													fastStep="1" paginatorMaxPages="#{pages$UnitList.paginatorMaxPages}" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 
												    pageIndexVar="pageNumber"
												    styleClass="SCH_SCROLLER"
												    paginatorActiveColumnStyle="font-weight:bold;"
												    paginatorRenderLinkForActive="false" 
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

								                    	<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
													
													<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>
													
												</t:dataScroller>
												</CENTER>
												</td></tr>
												</table>
                                           </div>
                                         </div>
											
											<table width="98%" border="0">
													<tr>
													<td>
													&nbsp;&nbsp;
													</td>
													     <td  colspan="6" class="BUTTON_TD" >
															   <h:commandButton styleClass="BUTTON" type= "submit" 
															   value="#{msg['commons.select']}"  
                              	                              action="#{pages$UnitList.btnSelect_Click}" > </h:commandButton>
													     </td>
													
													</tr>
											</table>
	

			</h:form>
		</div>
		  </td>
		</tr>
		<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
		</table>
			</body>
		</f:view>
	
</html>
