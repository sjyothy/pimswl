

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="javascript" type="text/javascript">
function loadUnRegBidder()
{
	
}
	function closeWindow()
	{
	  window.opener.document.getElementById('conductAuctionForm:isResultPopupCalled').value = "Y";
	  window.opener.document.forms[0].submit();
	  window.close();
	}
	var prevField;	
	function selectBidder(field, bidderId, bidderName, socialSec, nationality, emirates, cellNumber)
	{
		if(field.checked == true)
		 {
			if(prevField == null) {
				prevField = field;
			}
			else {
				prevField.checked = false;
				prevField = field;
			}
			document.getElementById('selectWinnerForm:bidderChk').onclick();
			
		}				
	}
	
	function resetBidPrice() {
		document.getElementById('selectWinnerForm:BidPrice').value = "0.0";
	}
</script>


<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
 
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
  <head>     
     <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
    </head>

 <body class="BODY_STYLE"> 
 
 
    <!-- Header --> 
    <table width="100%" cellpadding="0" cellspacing="0"  border="0">
    <tr>
    <td colspan="2">   
    	<%--<jsp:include page="header.jsp"/>--%>     
    </td>
    </tr>

<tr style="width:100%">
    <td width="100%" valign="top" class="divBackgroundBody" colspan="2">
        <table width="100%" class="greyPanelTable" cellpadding="0" cellspacing="0"  border="0">
	        <tr>
				<td class="HEADER_TD">
					<h:outputLabel value="#{msg['bidder.searchbidder']}" styleClass="HEADER_FONT"/>
				</td>
	            <td width="100%">
	        		&nbsp;
	            </td>
			</tr>
        </table>
        <table width="100%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
          <tr valign="top">
             <td height="100%" valign="top" nowrap="nowrap" 
             	background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" 
             	width="1">
             </td>    
             <!--Use this below only for desiging forms Please do not touch other sections -->             
    
             <td valign="top" nowrap="nowrap" height="100%">
             	<div class="SCROLLABLE_SECTION" style="height: 412px; width: 100%; # height: 412px;; # width: 100%;">
             	<h:form id="selectWinnerForm" style="width:98%;">
             	<table width="100%" border="0">	
             		<tr>
             			<td>
             				<div style="padding-left:15px; padding-right:15px;">  
             				<h:outputText id="successmsg" escape="false"
										styleClass="INFO_FONT"
										value="#{pages$SelectWinner.successMessages}" />
									<h:outputText id="errormsg" escape="false"
										styleClass="ERROR_FONT"
										value="#{pages$SelectWinner.errorMessages}" />           			
							</div>
             			</td>
             		</tr>				
					<tr>
						<td>							
							
							<div class="MARGIN">
													
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
								<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
								<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
								<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
								</tr>
							</table>
							<div class="DETAIL_SECTION">
								<h:outputLabel value="#{msg['feeConfiguration.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
								<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
								<tr>
									<td colspan="4">
										
									</td>
								</tr>
								<tr>
									<td>
										<h:outputLabel value="#{msg['bidder.firstName']}" styleClass="LABEL"/>
									</td>
									<td>
										<h:inputText id="bidderName" binding="#{pages$SelectWinner.inputTextBidderName}"/>
									</td>
									<td>
										<h:outputLabel value="#{msg['tenants.visaNumber']}" styleClass="LABEL"/>
									</td>
									<td>
										<h:inputText id="socialSecNo"
										binding="#{pages$SelectWinner.inputTextSocialSecNo}"></h:inputText>
									</td>
								</tr>
								<tr>
								</tr>
								<tr>
									<td colspan="4">
									</td>
								</tr>
								<tr>
									<td>
										<h:outputLabel value="#{msg['tenants.nationality']}" styleClass="LABEL"/>
									</td>
									<td>
										<h:selectOneMenu id="nationality"
											style="width: 192px; height: 24px" required="true"
											value="#{pages$SelectWinner.nationalitySelected}">
											<f:selectItem itemValue="-1"
												itemLabel="#{msg['commons.All']}" />
											<f:selectItems value="#{pages$SelectWinner.nationality}" />
											<a4j:support event="onchange" action="#{pages$SelectWinner.loadState}" reRender="state" />
										</h:selectOneMenu>
									</td>
									<td>
										<h:outputLabel value="#{msg['tenants.emirates']}" styleClass="LABEL"/>
									</td>
									<td>
									    <h:selectOneMenu id="state"
											style="width: 172px; height: 24px"  required="false"
											value="#{pages$SelectWinner.stateId}">
											<f:selectItem itemValue="-1"
												itemLabel="#{msg['commons.All']}" />
											<f:selectItems value="#{pages$SelectWinner.stateList}" />
										</h:selectOneMenu>
									</td>
								</tr>
								<tr>
									<td>
										<h:outputLabel value="#{msg['tenants.cellnumber']}" styleClass="LABEL"/>
									</td>
									<td>
										<h:inputText id="cellNumber"
										binding="#{pages$SelectWinner.inputTextCellNumber}"/>
									</td>
									<td>
										<h:outputLabel value="#{msg['auction.conductAuction.bidderType']}" styleClass="LABEL" rendered="false"/>
									</td>
									<td>
										<h:selectOneMenu id="biddersType" rendered="false"
											style="width: 192px; height: 24px" required="true"
											value="#{pages$SelectWinner.bidderTypeSelected}">
											<f:selectItems
												value="#{pages$SelectWinner.biddersType}" />
										</h:selectOneMenu>
									</td>
								</tr>
								<tr>
									<td colspan="4" class="BUTTON_TD">
										<h:commandButton styleClass="BUTTON" value="#{msg['commons.search']}"
												action="#{pages$SelectWinner.search}"/>
										<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}"
												onclick="javascript:closeWindow();"/>
									</td>
								</tr>
							</table>
							</div>
							</div>																											
						</td>													
					</tr>
					</table>
							<div class="MARGIN">
							<div class="imag">&nbsp;</div>
							<div class="contentDiv" align="center">
							<t:dataTable id="bidderOfRequest" style="width:100%;"
								value="#{pages$SelectWinner.bidderDataList}"													
								binding="#{pages$SelectWinner.dataTable}"													
								rows="5"
								preserveDataModel="false" preserveSort="false" var="dataItem"
								rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
									
								<t:column id="col2" sortable="true" >
									<f:facet name="header">
										<t:outputText value="#{msg['bidder.name']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT" value="#{dataItem.personFullName}"/>
								</t:column>
								
								<t:column id="col4" sortable="true">
									<f:facet name="header">
										<t:outputText  value="#{msg['tenants.cellnumber']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT" value="#{dataItem.cellNumber}" />
								</t:column>
								
								<t:column id="col5" sortable="true">
									<f:facet name="header">
										<t:outputText  value="#{msg['tenants.visaNumber']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT" value="#{dataItem.residenseVisaNumber}" />
								</t:column>
	
								<t:column id="col6" sortable="true">
									<f:facet name="header">
										<t:outputText  value="#{msg['AuctionDepositRefundApproval.DataTable.DepositAmount']}" />
									</f:facet>
									<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.totalDepositAmount}" style="white-space: normal;">
										<f:convertNumber pattern="#{pages$auctionDetails.numberFormat}"/>
									</t:outputText>
								</t:column>

								<t:column id="col7" width="50" >
									<f:facet name="header">
										<t:outputText value="#{msg['commons.select']}" />
									</f:facet>
										<h:commandLink action="#{pages$SelectWinner.unRegisteredBidderSelected}" 
											rendered="#{!dataItem.isRegistered}" >
										<t:graphicImage 
										title="#{msg['auction.selectwinner.tootTip.replaceUnit']}"
										url="../resources/images/app_icons/Add-Partner.png"></t:graphicImage>
										</h:commandLink>
										<h:commandLink 
											rendered="#{dataItem.isRegistered}"
											action="#{pages$SelectWinner.registeredBidderSelected}" >
										<t:graphicImage 
										title="#{msg['auction.selectwinner.tootTip.markWinner']}"
										url="../resources/images/app_icons/enable.png"></t:graphicImage>
										</h:commandLink>
								</t:column>								
								
							</t:dataTable>		
							</div>
							
							<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:98.9%;" >
								<table cellpadding="0" cellspacing="0" width="100%">
									<tr>
									<td class="RECORD_NUM_TD">
										<div class="RECORD_NUM_BG">
											<table cellpadding="0" cellspacing="0">
											<tr><td class="RECORD_NUM_TD">
											<h:outputText value="#{msg['commons.recordsFound']}"/>
											</td><td class="RECORD_NUM_TD">
											<h:outputText value=" : "/>
											</td><td class="RECORD_NUM_TD">
											<h:outputText value="#{pages$SelectWinner.recordSize}"/>
											</td></tr>
											</table>
										</div>
									</td>
									<td class="BUTTON_TD" style="width:50%"> 
										<t:dataScroller id="scroller" for="bidderOfRequest" paginator="true"
											fastStep="1" paginatorMaxPages="10" immediate="false"
											paginatorTableClass="paginator"
											renderFacetsIfSinglePage="true" 
											paginatorTableStyle="grid_paginator" layout="singleTable"
											paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
											paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;" pageIndexVar="pageNumber">
			
											<f:facet name="first">												
												<t:graphicImage url="../resources/images/first_btn.gif" id="lblF"></t:graphicImage>
											</f:facet>
			
											<f:facet name="fastrewind">
												<t:graphicImage url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>
											</f:facet>
			
											<f:facet name="fastforward">
												<t:graphicImage url="../resources/images/next_btn.gif" id="lblFF"></t:graphicImage>
											</f:facet>
			
											<f:facet name="last">
												<t:graphicImage url="../resources/images/last_btn.gif" id="lblL"></t:graphicImage>
											</f:facet>
											<t:div styleClass="PAGE_NUM_BG">
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td>	
															<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
														</td>
														<td>	
															<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
														</td>
													</tr>					
												</table>										
											</t:div>
										</t:dataScroller>
									</td>
									</tr>
								</table>
							</t:div>							
                        </div>
						<div>
							<h:inputHidden id="bidderId" value="#{pages$SelectWinner.hdnBidderId}"></h:inputHidden>
							<h:inputHidden id="bidPrice" value="#{pages$SelectWinner.hdnBidPrice}"></h:inputHidden>
							<h:inputHidden id="auctionId" value="#{pages$SelectWinner.hdnAuctionId}"></h:inputHidden>
							<h:inputHidden id="auctionUnitId" value="#{pages$SelectWinner.hdnAuctionUnitId}"></h:inputHidden>
						</div>
					<table cellpadding="10px" cellspacing="0" width="100%">
						<tr>
							<td>
							<rich:tabPanel id="tabPane"	
					binding="#{pages$SelectWinner.detailTab}"
					style="height:auto;width:100%;" headerSpacing="0">
					<rich:tab 
						id="bidderTab"
						binding="#{pages$SelectWinner.bidderTab}"						
						label="#{msg['auction.selectwinner.tabLabel.WinnerInfo']}">
						<t:panelGrid  columns="4"	cellpadding="1px" width="100%" cellspacing="3px">
								 <h:outputLabel value="#{msg['bidder.firstName']}"  styleClass="LABEL"/>
								 <h:inputText id="selectedName" readonly="true" binding="#{pages$SelectWinner.txtSelectedName}"/>
								 <h:outputLabel value="#{msg['tenants.visaNumber']}" styleClass="LABEL"/>
								 <h:inputText id="selectedSocialSecNo" readonly="true" binding="#{pages$SelectWinner.txtSelectedSocialSecNo}"/>
								 
								 <h:outputLabel value="#{msg['tenants.nationality']}" styleClass="LABEL"/>
								 <h:inputText id="selectedNationality" readonly="true" binding="#{pages$SelectWinner.txtSelectedNationality}"/>
								 <h:outputLabel rendered="false" value="#{msg['tenants.emirates']}" styleClass="LABEL"></h:outputLabel>
								 <h:inputText rendered="false" id="selectedEmirates" readonly="true"></h:inputText>
								 
								 <h:outputLabel value="#{msg['tenants.cellnumber']}" styleClass="LABEL"/>
								 <h:inputText id="selectedCellNumber" readonly="true" binding="#{pages$SelectWinner.txtSelectedCellNumber}"/>
								 
								  <t:panelGroup>
									      	<h:outputLabel styleClass="mandatory" value="#{pages$auctionDetails.asterisk}"></h:outputLabel>
											<h:outputLabel value="#{msg['auction.conductAuction.bidPrice']}" styleClass="LABEL"/>				      
							      </t:panelGroup>
								 <h:inputText id="BidPrice" value="#{pages$SelectWinner.hdnBidPrice}" 
										binding="#{pages$SelectWinner.txtBidPrice}"
										style="text-align:right"/>
								
								 <h:outputLabel 
								 rendered="#{pages$SelectWinner.showOldNewUnit}"
								 value="#{msg['auction.selectwinner.fieldLabel.oldUnitNumber']}" styleClass="LABEL"/>
								 <h:inputText id="OldUnitNumber" 
								 readonly="true"
								 rendered="#{pages$SelectWinner.showOldNewUnit}"
								 value="#{pages$SelectWinner.unitNumberToBeReplaced}"/>
								
								 <h:outputLabel 
								 rendered="#{pages$SelectWinner.showOldNewUnit}"
								 value="#{msg['auction.selectwinner.fieldLabel.newUnitNumber']}" styleClass="LABEL"/>
								 <h:inputText id="newUnitNumber" 
								 readonly="true"
								 rendered="#{pages$SelectWinner.showOldNewUnit}"
								 value="#{pages$SelectWinner.newUnitNumber}" />		
						</t:panelGrid>	
					</rich:tab>	
					<rich:tab 
					id="replaceUnitsTab"
					binding="#{pages$SelectWinner.replaceUnitsTab}"
					label="#{msg['auction.selectwinner.title.ReplaceableUnits']}">
								<t:div styleClass="contentDiv" style="align:center;">
									<t:dataTable id="units" style="width:100%;"
											value="#{pages$SelectWinner.replaceableUnitList}"													
											binding="#{pages$SelectWinner.unitDataTable}"													
											rows="5"
											preserveDataModel="false" preserveSort="false" var="dataItem"
											rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
										<t:column id="colerr" sortable="true" >
									<f:facet name="header">
										<t:outputText value="#{msg['cancelContract.tab.unit.unitno']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT" value="#{dataItem.unitNumber}"/>
								</t:column>
								
								<t:column id="colerrer4" sortable="true">
									<f:facet name="header">
										<t:outputText  value="#{msg['chequeList.propertyCommercialName']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT" value="#{dataItem.propertyCommercialName}" />
								</t:column>
								
								<t:column id="colrerrr5" sortable="true">
									<f:facet name="header">
										<t:outputText  value="#{msg['inquiry.floorNumber']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT" value="#{dataItem.floorNumber}" />
								</t:column>
								<t:column id="colrerrr5ew" sortable="true">
									<f:facet name="header">
										<t:outputText  value="#{msg['paymentConfiguration.Actions']}" />
									</f:facet>
									<h:commandLink 
											onclick="if (!confirm('#{msg['auction.selectwinner.warngMsg.unitWillBeReplaced']}')) return false"
											action="#{pages$SelectWinner.replaceUnit}" >
										<t:graphicImage url="../resources/images/app_icons/Approve-Request.png"></t:graphicImage>
									</h:commandLink>
								</t:column>
							</t:dataTable>
							</t:div>
				<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99.1%;">
				<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.recordsFound']}"/>
						<h:outputText value=" : "/>
						<h:outputText value="#{pages$SelectWinner.unitRecordSize}" />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="scroller1" for="units" paginator="true"  
						fastStep="1" paginatorMaxPages="#{pages$SelectWinner.paginatorMaxPages}" immediate="false"
						paginatorTableClass="paginator"
						renderFacetsIfSinglePage="true" 												
						paginatorTableStyle="grid_paginator" layout="singleTable" 
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" 
						pageIndexVar="pageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}" id="lblF1"></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR1"></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF1"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}" id="lblL1"></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
							<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
		</t:div>
							</rich:tab>
					</rich:tabPanel>
							</td>
						</tr>					
					</table>
					<table cellpadding="10px" cellspacing="0" width="100%">
							<tr>
								<td align="right">
										<h:commandButton 
											action="#{pages$SelectWinner.saveWinner}"
											onclick="if (!confirm('#{msg['auction.selectwinner.warngMsg.doYouWantToSave']}')) return false"
											value="#{msg['commons.saveButton']}"
											styleClass="BUTTON"/>
								</td>
							</tr>
					</table>
                </h:form>
                </div>               
         	</td>
         </tr>
         
         <tr>
         	<td>

         	</td>         	
         </tr>
        </table>
    
    
    
    
    
    </td>
    </tr>
    <tr>
    <td colspan="2">
    </td>
    </tr>
    </table>
    </body>
</html>
</f:view>
