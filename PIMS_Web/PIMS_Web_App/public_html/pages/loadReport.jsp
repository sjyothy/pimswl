<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="com.avanza.core.util.Logger"%>
<%@page import="com.crystaldecisions.report.web.viewer.CrystalReportViewer"%>
<%@page import="com.crystaldecisions.sdk.occa.report.reportsource.IReportSource"%>
<%@page import="java.util.Locale"%>
<%@page import="com.avanza.pims.report.manager.IReportManager"%>
<%@page import="com.avanza.pims.report.manager.DefaultReportManager"%>
<%@page import="com.crystaldecisions.report.web.viewer.CrPrintMode"%>
<%@page import="com.avanza.pims.report.criteria.IReportCriteria"%>
<%@page import="com.avanza.pims.report.constant.ReportConstant"%>

<%
	Logger logger = Logger.getLogger(this.getClass());
	try
	{	
		logger.logInfo(" --- Started --- ");
		IReportCriteria reportCriteria = (IReportCriteria) session.getAttribute( ReportConstant.Keys.REPORT_CRITERIA_KEY );
		IReportManager reportManager = new DefaultReportManager( reportCriteria );
		IReportSource rptSource = reportManager.getReportSource();
		CrystalReportViewer crystalReportPageViewer = new CrystalReportViewer();
		crystalReportPageViewer.setReportSource(rptSource);
		crystalReportPageViewer.setPrintMode( CrPrintMode.ACTIVEX );
        crystalReportPageViewer.setHasToggleGroupTreeButton(false);
        crystalReportPageViewer.setDisplayGroupTree(false);
        crystalReportPageViewer.setEnableDrillDown(false);
        crystalReportPageViewer.setEnableLogonPrompt( false );
        crystalReportPageViewer.setHasPrintButton(true);
        crystalReportPageViewer.setHasPageBottomToolbar(false);
        crystalReportPageViewer.setOwnPage(true);
        crystalReportPageViewer.setOwnForm(true);        
		crystalReportPageViewer.processHttpRequest(request, response, session.getServletContext(), out); 
		logger.logInfo(" --- Successfully Completed --- "); 
	}
	catch ( Exception e )
	{
		logger.LogException(" --- Exception --- ", e);
	}		
%>

<script type="text/javascript">
	function OnPrintClicked()
	{	
		var screen_width = 500;
		var screen_height = 200;
        var popup_width = screen_width;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('printReportManage.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	}

	// document.getElementById('crprint').href = 'javascript:OnPrintClicked();';
</script>
