
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="javascript" type="text/javascript">
	
	
		
		function submitForm(ads){
					
			document.getElementById('NOLForm:hiddenValueNOLType').value = ads.value;
			alert(document.forms[0]);
			document.forms[0].submit();
		}
		
		function   showContractPopup(clStatus)
		{
	     // alert("In show PopUp");
		 var screen_width = 1024;
         var screen_height = 470;

		    
		   window.open('ContractListPopUp.jsf?clStatus='+clStatus,'_blank','width='+(screen_width-220)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
		    
		}
	
		function populateContract(contractNumber,contractId)
		{
     
        document.getElementById("NOLForm:hdnContractId").value=contractId;
	    document.getElementById("NOLForm:hdnContactNumber").value=contractNumber;
	    
     }
    
     
     function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		    
		  //  alert("in populatePerson");
		    
		    document.getElementById("NOLForm:hdnPersonId").value=personId;
		    document.getElementById("NOLForm:hdnCellNo").value=cellNumber;
		    document.getElementById("NOLForm:hdnPersonName").value=personName;
		    document.getElementById("NOLForm:hdnPersonType").value=hdnPersonType;
		    document.getElementById("NOLForm:hdnIsCompany").value=isCompany;
			
	       document.forms[0].submit();
	}
     
     
		
	  function	openPopUp()
		{
		
		    var width = 300;
            var height = 300;
            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + ",menubar=yes,toolbar=yes,scrollbars=yes,resizable=yes,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

            var child1 = window.open("about:blank", "subWind", windowFeatures);

		      child1.focus();
		}

		
		function   showPersonPopup()
		{
		   var screen_width = 1024;
		   var screen_height = 470;
		   window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
						    
		}
	</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />


	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table_ff}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
		</head>




		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['contract.nolScreenTitle']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<!--Use this below only for desiging forms Please do not touch other sections -->


									<td width="100%" height="99%" valign="top" nowrap="nowrap">

										<h:form id="NOLForm" enctype="multipart/form-data">
											<div class="SCROLLABLE_SECTION">

												<table border="0" class="layoutTable"
													style="margin-left: 15px; margin-right: 15px;">
													<tr>
														<td>
															<h:outputText id="successMsg_2"
																value="#{pages$IssueMemsNOL.successMessages}"
																escape="false" styleClass="INFO_FONT" />
															<h:outputText id="errorMsg_1"
																value="#{pages$IssueMemsNOL.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:inputHidden id="hdnRequestId"
																value="#{pages$IssueMemsNOL.hdnRequestId}"></h:inputHidden>
															<h:inputHidden id="hdnPersonId"
																value="#{pages$IssueMemsNOL.hdnPersonId}"></h:inputHidden>
															<h:inputHidden id="hdnPersonType"
																value="#{pages$IssueMemsNOL.hdnPersonType}"></h:inputHidden>
															<h:inputHidden id="hdnPersonName"
																value="#{pages$IssueMemsNOL.hdnPersonName}"></h:inputHidden>
															<h:inputHidden id="hdnCellNo"
																value="#{pages$IssueMemsNOL.hdnCellNo}"></h:inputHidden>
															<h:inputHidden id="hdnIsCompany"
																value="#{pages$IssueMemsNOL.hdnIsCompany}"></h:inputHidden>

														</td>
													</tr>
												</table>

												<table cellpadding="0" cellspacing="6px" width="98%">
													<tr>
														<td class="BUTTON_TD MARGIN" colspan="6">

															<h:commandButton id="idFile"
																value="#{msg['attachment.file']}"
																action="#{pages$IssueMemsNOL.showInheritanceFilePopup}"
																styleClass="BUTTON">
															</h:commandButton>
														</td>
													</tr>
												</table>

												<t:panelGrid cellpadding="1px" width="100%"
													cellspacing="5px" columns="4">
													<!-- Top Fields - Start -->
													<h:outputLabel styleClass="LABEL"
														value="#{msg['collectionProcedure.fileNumber']} :" />
													<h:inputText styleClass="READONLY" readonly="true"
														binding="#{pages$IssueMemsNOL.htmlFileNumber}" />


													<h:outputLabel styleClass="LABEL"
														value="#{msg['report.memspaymentreceiptreport.fileOwner']}:" />
													<h:inputText styleClass="READONLY" readonly="true"
														binding="#{pages$IssueMemsNOL.txtFileOwner}" />


													<h:outputLabel styleClass="LABEL"
														value="#{msg['searchInheritenceFile.fileType']}:" />
													<h:inputText styleClass="READONLY" readonly="true"
														binding="#{pages$IssueMemsNOL.htmlFileType}" />

													<h:outputLabel styleClass="LABEL"
														value="#{msg['commons.status']}:" />
													<h:inputText styleClass="READONLY" readonly="true"
														binding="#{pages$IssueMemsNOL.htmlFileStatus}" />


													<%-- Top Fields - End --%>


													<%-- Send TO GROUP COMBO START--%>


													<h:outputText id="idReview" styleClass="LABEL"
														rendered="#{ (pages$IssueMemsNOL.isApprovalRequired || pages$IssueMemsNOL.isReviewed || pages$IssueMemsNOL.isReviewRequired ) &&  !pages$IssueMemsNOL.isView}"
														value="#{msg['mems.nol.label.reviewReqGrp']}" />
													<t:panelGroup colspan="3">
														<h:selectOneMenu id="idRshGrp"
															rendered="#{ (pages$IssueMemsNOL.isApprovalRequired || pages$IssueMemsNOL.isReviewed || pages$IssueMemsNOL.isReviewRequired ) &&  !pages$IssueMemsNOL.isView}"
															disabled="#{pages$IssueMemsNOL.isReviewRequired}"
															binding="#{pages$IssueMemsNOL.cmbUserGroups}"
															style="width: 200px;">
															<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																itemValue="-1" />
															<f:selectItems
																value="#{pages$IssueMemsNOL.allUserGroups}" />
														</h:selectOneMenu>
													</t:panelGroup>
													<%-- Send TO GROUP COMBO FINISH--%>

													<%-- REVIEW TEXT START--%>
													<h:outputText id="idDoReview" styleClass="LABEL"
														rendered="#{pages$IssueMemsNOL.isReviewRequired && pages$IssueMemsNOL.isUserInGroup &&  !pages$IssueMemsNOL.isView}"
														value="#{msg['mems.nol.label.review']}" />
													<t:panelGroup colspan="3">
														<h:inputTextarea binding="#{pages$IssueMemsNOL.reviewTxt}"
															rendered="#{pages$IssueMemsNOL.isReviewRequired && pages$IssueMemsNOL.isUserInGroup &&  !pages$IssueMemsNOL.isView}"
															style="width:600px;height:50px;">
														</h:inputTextarea>
													</t:panelGroup>
													<%-- REVIEW TEXT FINISH--%>

												</t:panelGrid>




												<div class="MARGIN" style="width: 97%" border="0">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>

													<div class="TAB_PANEL_INNER">
														<rich:tabPanel id="issueNOLTabPanel" headerSpacing="0"
															binding="#{pages$IssueMemsNOL.richPanel}"
															style="width:100%">

															<rich:tab id="applicationTab"
																label="#{msg['commons.tab.applicationDetails']}">
																<%@ include file="ApplicationDetails.jsp"%>
															</rich:tab>


															<rich:tab id="NOLTab"
																label="#{msg['contract.nolTabHeader']}">
																<t:div id="divNOLTab" styleClass="TAB_DETAIL_SECTION">
																	<t:panelGrid id="NOLDetailTab" cellpadding="1px"
																		width="100%" cellspacing="5px" columns="4"
																		styleClass="TAB_DETAIL_SECTION_INNER"
																		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputText styleClass="LABEL"
																				value="#{msg['contract.nolType']}:" />
																		</h:panelGroup>
																		<h:selectOneMenu id="nolTypeSelectOneMenu"
																			disabled="#{!pages$IssueMemsNOL.isNew || pages$IssueMemsNOL.isView}"
																			style="width: 200px;"
																			binding="#{pages$IssueMemsNOL.cmbNolTypes}"
																			valueChangeListener="#{pages$IssueMemsNOL.handleNolTypeChange}"
																			onchange="submit();">
																			<f:selectItem
																				itemLabel="#{msg['commons.pleaseSelect']}"
																				itemValue="-1" />
																			<f:selectItems
																				value="#{pages$IssueMemsNOL.memsNolTypes}" />
																		</h:selectOneMenu>

																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputText styleClass="LABEL"
																				value="#{msg['mems.nol.groups']}" />
																		</h:panelGroup>
																		<h:inputText
																			styleClass="READONLY APPLICANT_DETAILS_INPUT"
																			id="txtGroups" readonly="true"
																			binding="#{pages$IssueMemsNOL.nolTypeGroups}">
																		</h:inputText>

																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputText styleClass="LABEL"
																				value="#{msg['contract.nolReason']}:" />
																		</h:panelGroup>
																		<h:inputTextarea
																			binding="#{pages$IssueMemsNOL.reasonNOL}"
																			disabled="#{!pages$IssueMemsNOL.isNew}"
																			style="width:198px;height:100px;"></h:inputTextarea>
																	</t:panelGrid>
																</t:div>
															</rich:tab>

															<rich:tab id="commentsTab"
																label="#{msg['commons.commentsTabHeading']}">
																<%@ include file="notes/notes.jsp"%>
															</rich:tab>
															<rich:tab id="attachmentTab"
																label="#{msg['commons.attachmentTabHeading']}">
																<%@  include file="attachment/attachment.jsp"%>
															</rich:tab>
															<rich:tab
																label="#{msg['contract.tabHeading.RequestHistory']}"
																title="#{msg['commons.tab.requestHistory']}"
																action="#{pages$IssueMemsNOL.getRequestHistory}">
																<%@ include file="../pages/requestTasks.jsp"%>
															</rich:tab>


														</rich:tabPanel>

													</div>
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																	class="TAB_PANEL_MID" style="width: 100%;" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td height="5px"></td>
														</tr>
														<tr>
															<td class="BUTTON_TD MARGIN" colspan="6">

																<pims:security screen="Pims.MinorMgmt.NOL.Create"
																	action="create">
																	<h:commandButton id="idSaveBtn"
																		value="#{msg['commons.saveButton']}"
																		onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;"
																		styleClass="BUTTON"
																		rendered="#{pages$IssueMemsNOL.isNew &&  !pages$IssueMemsNOL.isView}"
																		action="#{pages$IssueMemsNOL.save}">
																	</h:commandButton>

																	<h:commandButton styleClass="BUTTON" id="idSubmitBtn"
																		value="#{msg['commons.submitButton']}"
																		onclick="if (!confirm('#{msg['mems.common.alertSubmit']}')) return false;"
																		action="#{pages$IssueMemsNOL.submitQuery}"
																		rendered="#{pages$IssueMemsNOL.isNew &&  !pages$IssueMemsNOL.isView}">
																	</h:commandButton>
																</pims:security>

																<pims:security screen="Pims.MinorMgmt.NOL.Approve"
																	action="create">
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.approve']}"
																		action="#{pages$IssueMemsNOL.approve}"
																		onclick="if (!confirm('#{msg['mems.common.alertApprove']}')) return false;"
																		rendered="#{(pages$IssueMemsNOL.isApprovalRequired || pages$IssueMemsNOL.isReviewed) && pages$IssueMemsNOL.isTaskPresent &&     !pages$IssueMemsNOL.isView }">
																	</h:commandButton>

																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.reject']}"
																		action="#{pages$IssueMemsNOL.reject}"
																		onclick="if (!confirm('#{msg['mems.common.alertReject']}')) return false;"
																		rendered="#{(pages$IssueMemsNOL.isApprovalRequired || pages$IssueMemsNOL.isReviewed) &&  pages$IssueMemsNOL.isTaskPresent &&   !pages$IssueMemsNOL.isView}">
																	</h:commandButton>

																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.review']}"
																		action="#{pages$IssueMemsNOL.sendForReview}"
																		onclick="if (!confirm('#{msg['mems.common.alertReview']}')) return false;"
																		rendered="#{(pages$IssueMemsNOL.isApprovalRequired || pages$IssueMemsNOL.isReviewed) &&pages$IssueMemsNOL.isTaskPresent &&     !pages$IssueMemsNOL.isView}">
																	</h:commandButton>
																</pims:security>

																<h:commandButton
																	rendered="#{(pages$IssueMemsNOL.isReviewRequired && pages$IssueMemsNOL.isUserInGroup) && pages$IssueMemsNOL.isTaskPresent &&    !pages$IssueMemsNOL.isView}"
																	styleClass="BUTTON"
																	onclick="if (!confirm('#{msg['mems.common.alertReviewDone']}')) return false;"
																	value="#{msg['commons.done']}"
																	action="#{pages$IssueMemsNOL.done}">
																</h:commandButton>

																<pims:security screen="Pims.MinorMgmt.NOL.Complete"
																	action="create">
																	<h:commandButton
																		rendered="#{pages$IssueMemsNOL.isApproved &&  pages$IssueMemsNOL.isTaskPresent &&   !pages$IssueMemsNOL.isView}"
																		styleClass="BUTTON"
																		onclick="if (!confirm('#{msg['mems.common.alertComplete']}')) return false;"
																		value="#{msg['commons.complete']}"
																		action="#{pages$IssueMemsNOL.complete}">
																	</h:commandButton>
																</pims:security>

																<pims:security screen="Pims.MinorMgmt.NOL.Print"
																	action="create">
																	<h:commandButton
																		rendered="#{!pages$IssueMemsNOL.isNew}"
																		styleClass="BUTTON" value="#{msg['commons.print']}"
																		action="#{pages$IssueMemsNOL.print}">
																	</h:commandButton>
																</pims:security>

															</td>
														</tr>
													</table>

												</div>


											</div>
										</h:form>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td class="footer">
													<h:outputLabel value="#{msg['commons.footer.message']}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>


						</td>
					</tr>
				</table>
			</div>
		</body>

	</html>
</f:view>
