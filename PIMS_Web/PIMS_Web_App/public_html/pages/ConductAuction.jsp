


<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="avanza-security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<script language="javascript" type="text/javascript">
	
	var attendanceIds;	
	var unitIdsToExclude;
	
	function SelectBidder(field,attId)
	{
    	//alert(field.checked); = true print hota hai	   
	   var attFlag = "";
   		if(field.checked)
   			attFlag = "1";
   		else
   			attFlag = "0";
   			
		var newID = ":" + attId;
		
		if(attendanceIds == null)
		{
			attendanceIds = attFlag + ":" + attId;
		}
		else
		{
			if(attendanceIds.indexOf(newID) != -1)
			{
				var delimiters=",";
				var attArr=attendanceIds.split(delimiters);
				attendanceIds="";
				for(i=0;i<attArr.length;i++)
				{
					   var att = newID;
					   if(attFlag == "1")
					   		att = "0"+att;	
					   else
					   		att = "1"+att;
					   		
					   if(attArr[i] == att)
					    {
				       	   if(attendanceIds.length>0)
	       	       	   			attendanceIds = attendanceIds + "," + attFlag + newID;
       	       	   			else
       	       	   				attendanceIds = attFlag + newID;
					    }
					    else
					    {
					    	if(attendanceIds.length>0)
					    		attendanceIds = attendanceIds + ","  + attArr[i];
					    	else
					    		attendanceIds = attArr[i];
					    }		    
				}		
			}
			else
			{
				attendanceIds = attendanceIds + "," + attFlag + ":" + attId;
			}
		}
   	   document.getElementById("conductAuctionForm:attendanceIds").value = attendanceIds;
	}
	
	function excludeUnits(field,unitId)
	{
		var exclusionFlag = "";
   		if(field.checked)
   			exclusionFlag = "1";
   		else
   			exclusionFlag = "0";
   			
		var newID = ":" + unitId;
		
		if(unitIdsToExclude == null)
		{
			unitIdsToExclude = exclusionFlag + ":" + unitId;
		}
		else
		{
			if(unitIdsToExclude.indexOf(newID) != -1)
			{
				var delimiters=",";
				var unitsArr=unitIdsToExclude.split(delimiters);
				unitIdsToExclude="";
				for(i=0;i<unitsArr.length;i++)
				{
					   var unit = newID;
					   if(exclusionFlag == "1")
					   		unit = "0"+unit;	
					   else
					   		unit = "1"+unit;
					   		
					   if(unitsArr[i] == unit)
					    {
				       	   if(unitIdsToExclude.length>0)
	       	       	   			unitIdsToExclude = unitIdsToExclude + "," + exclusionFlag + newID;
       	       	   			else
       	       	   				unitIdsToExclude = exclusionFlag + newID;
					    }
					    else
					    {
					    	if(unitIdsToExclude.length>0)
					    		unitIdsToExclude = unitIdsToExclude + ","  + unitsArr[i];
					    	else
					    		unitIdsToExclude = unitsArr[i];
					    }		    
				}		
			}
			else
			{
				unitIdsToExclude = unitIdsToExclude + "," + exclusionFlag + ":" + unitId;
			}
		}		
   	   document.getElementById("conductAuctionForm:unitIdsToExclude").value = unitIdsToExclude;
	}
	
	function showEditBidPrice()
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
		window.open('EditBidPrice.jsp' ,'_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=yes,status=yes');
	}
	
	function showSelectWinner(auctionId,auctionUnitId,bidPrice)
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
		var queryString = '?aucId='+auctionId+'&aucUnitId='+auctionUnitId+'&bidPrice='+bidPrice;
		window.open('SelectWinner.jsp'+queryString ,'_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=yes,status=yes');
	}

</script>


<f:view>

 <html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
  <head>     
  	<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
		
      
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache,must-revalidate,no-store">
	<meta http-equiv="expires" content="0">

	<link rel="stylesheet" type="text/css" href="../../resources/css/simple_en.css" />
	<link rel="stylesheet" type="text/css" href="../../resources/css/amaf_en.css" />
	<link rel="stylesheet" type="text/css" href="../resources/css/table.css" />
        <title>PIMS</title>
    </head>

    
    <%
    UIViewRoot view = (UIViewRoot)session.getAttribute("view");
    if (view.getLocale().toString().equals("en")){ %>
    <body dir="ltr">
    <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg" />
    <%}else{%>
    <body dir="rtl">
    <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages_ar" var="msg" />
    <%}
    
    %>
    <!-- Header --> 
    <table width="100%" cellpadding="0" cellspacing="0"  border="0">
    <tr>
    <td colspan="2">
        <jsp:include page="header.jsp"/>
    </td>
    </tr>

<tr width="100%">
    <td class="divLeftMenuWithBody" width="17%">
        <jsp:include page="leftmenu.jsp"/>
    </td>
    <td width="83%" valign="top" class="divBackgroundBody">
        <table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0"  border="0">
	        <tr>
	         <%if (view.getLocale().toString().equals("en"))
	             {  %>
	            <td background ="../../resources/images/Grey panel/Grey-Panel-left-1.jpg" height="38" style="background-repeat:no-repeat;" width="100%">
	             <font style="font-family:Trebuchet MS;font-weight:regular;font-size:19px;margin-left:15px;top:30px;">
	             <h:outputLabel value="#{msg['auction.conductAuction']}"></h:outputLabel>
	             </font>
	             </td>                
	             <%}else {%>
	             <td width="100%">
	                 <IMG src="../../resources/images/ar/Detail/Grey Panel/Grey-Panel-right-1.jpg"></img>
	             </td>
	             <%}%>
	            <td width="100%">
	        &nbsp;
	            </td>
			</tr>
        </table>
        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
          <tr valign="top">
             <td height="100%" valign="top" nowrap="nowrap" background ="../../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
             </td>    
             <!--Use this below only for desiging forms Please do not touch other sections -->             
    
             <td width="100%" height="100%" valign="top" nowrap="nowrap">
             	<h:form id="conductAuctionForm">
             	<h:inputHidden id="attendanceIds" value="#{pages$ConductAuction.hiddenAttendanceIds}" ></h:inputHidden>
             	<h:inputHidden id="unitIdsToExclude" value="#{pages$ConductAuction.hiddenUnitsToExclude}"></h:inputHidden>
             	<h:inputHidden id="isResultPopupCalled" value="#{pages$ConductAuction.hdnResultPopupCalled}"></h:inputHidden>
             	<table width="100%">
					<tr>
						<td colspan="6">
							<h:messages id="Msgs">
							<h:outputText id="opMsgs" value="#{pages$ConductAuction.messages}" escape="false"/>
							</h:messages>
						</td>
					</tr>
					<tr>
						<td>
							<h:outputLabel value="#{msg['auction.number']}"></h:outputLabel>
						</td>
						<td align="left">
							<h:inputText id="txtAuctionNo" readonly="true" style="width: 200px; height: 16px" value="#{pages$ConductAuction.auctionNo}"></h:inputText>
						</td>
						<td>
							<h:outputLabel value="#{msg['auction.title']}"></h:outputLabel>
						</td>
						<td>	
							<h:inputText id="txtAuctionTitle" readonly="true" style="width: 200px; height: 16px" value="#{pages$ConductAuction.auctionTitle}"></h:inputText>
						</td>
						<td colspan="2" align="left">
							&nbsp;
						</td>																										
					</tr>
						

					
					<tr>
						<td>
							<h:outputLabel value="#{msg['auction.venue']}"></h:outputLabel>
						</td>
						<td>	
							<h:inputText id="txtAuctionVenue" readonly="true" style="width: 200px; height: 16px" value="#{pages$ConductAuction.auctionVenueName}"></h:inputText>
						</td>
						<td>
							<h:outputLabel value="#{msg['auction.date']}"></h:outputLabel>
						</td>
						<td>	
							<h:inputText id="auctionDate" style="width: 200px; height: 16px" readonly="true" value="#{pages$ConductAuction.auctionDate}"></h:inputText>
						</td>
						<td colspan="2">
							&nbsp;																											
						</td>													
					</tr>
					
					<tr>
						<td colspan="6">
							&nbsp;																											
						</td>													
					</tr>
					
					<tr>
						<td width="100%" colspan="6">
							
							<t:panelTabbedPane width="100%" serverSideTabSwitch="false">
									<t:panelTab label="#{msg['auction.conductAuction.attendance']}">
										<f:verbatim><div class="contentDiv" style="width: 100%">
											<%/* Bidders' Attendance Grid */%>
											<t:dataTable id="Attendance" 
												value="#{pages$ConductAuction.dataListBidder}"
												binding="#{pages$ConductAuction.dataTableBidder}"
												rows="10" 
												preserveDataModel="true" preserveSort="true" var="dataItemBidder"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
					
												<t:column id="col1" >
													<f:facet name="header">
														<t:outputText value="#{msg['bidder.name']}" />
													</f:facet>
													<t:outputText value="#{dataItemBidder.name}" />
												</t:column>
					
												<t:column id="col2">
													<f:facet name="header">
														<t:outputText value="#{msg['customer.passportNumber']}" />
													</f:facet>
													<t:outputText value="#{dataItemBidder.passportNumber}" />
												</t:column>
					
												<t:column id="col3">
													<f:facet name="header">
														<t:outputText value="#{msg['customer.socialSecNumber']}" />
													</f:facet>
													<t:outputText value="#{dataItemBidder.socialSecNumber}" />
												</t:column>
													
												<t:column id="col4">
													<f:facet name="header">
														<t:outputText value="#{msg['customer.residenseVisaNumber']}" />
													</f:facet>
													<t:outputText value="#{dataItemBidder.residenseVisaNumber}" />
												</t:column>						
					
												<t:column id="col5">
													<f:facet name="header">
														<t:outputText value="#{msg['customer.personalSecCardNo']}" />
													</f:facet>
													<t:outputText value="#{dataItemBidder.personalSecCardNo}" />
												</t:column>
												
												<t:column id="col6">
													<f:facet name="header">
														<t:outputText value="#{msg['customer.drivingLicenseNumber']}" />
													</f:facet>
													<t:outputText value="#{dataItemBidder.drivingLicenseNumber}" />
												</t:column>
												
												<t:column id="col7">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.action']}" />
													</f:facet>
													<h:selectBooleanCheckbox id="select" value="#{dataItemBidder.attendance}"
													onclick="javascript:SelectBidder(this,'#{dataItemBidder.bidderAuctionRegId}');"/>
												</t:column>
											</t:dataTable>
										</div></f:verbatim>
									</t:panelTab>
									
									<t:panelTab label="#{msg['auction.conductAuction.unitToExclude']}">
										<f:verbatim><div class="contentDiv" style="width: 100%">
											<%/* Auction units inclusion/exclusion Grid */%>											
											<t:dataTable id="UnitExclusion" 
												value="#{pages$ConductAuction.dataListUnitsToExclude}"
												binding="#{pages$ConductAuction.dataTableUnitsToExclude}"
												rows="10" 
												preserveDataModel="true" preserveSort="true" var="dataItemUnitsToExclude"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
					
													<t:column id="colPropertyRefNo" width="100" >
														<f:facet name="header">
															<t:outputText value="#{msg['property.refNumber']}" />
														</f:facet>
														<t:outputText value="#{dataItemUnitsToExclude.propertyRefNumber}" />
													</t:column>
						
													<t:column id="colPropertyCommName" width="150" >
														<f:facet name="header">
															<t:outputText value="#{msg['property.commercialName']}" />
														</f:facet>
														<t:outputText value="#{dataItemUnitsToExclude.commercialName}" />
													</t:column>
						
													<t:column id="colUnitNo" width="100" >
														<f:facet name="header">
															<t:outputText value="#{msg['unit.number']}" />
														</f:facet>
														<t:outputText value="#{dataItemUnitsToExclude.unitNumber}" />
													</t:column>
						
													<t:column id="colUnitType" width="100" >
														<f:facet name="header">
															<t:outputText value="#{msg['unit.type']}" />
														</f:facet>
														<t:outputText value="#{dataItemUnitsToExclude.unitTypeEn}" />
													</t:column>
													
													<t:column id="colDepositAmount" width="100" >
														<f:facet name="header">
															<t:outputText value="#{msg['unit.depositAmount']}" />
														</f:facet>
														<t:outputText value="#{dataItemUnitsToExclude.depositAmount}" />
													</t:column>
													
													<t:column id="colSelect" width="50" >
														<f:facet name="header">
															<t:outputText value="#{msg['commons.action']}" />
														</f:facet>
														<h:selectBooleanCheckbox  id="select" value="#{dataItemUnitsToExclude.excluded}"
														onclick="javascript:excludeUnits(this,'#{dataItemUnitsToExclude.auctionUnitId}');"/>
													</t:column>
											</t:dataTable>
										</div></f:verbatim>
									</t:panelTab>
									
									<t:panelTab label="#{msg['auction.conductAuction.markWinners']}">
										<div class="contentDiv" style="width: 100%">
											<t:dataTable id="MarkWinners"
												value="#{pages$ConductAuction.dataListAuctionUnitRequests}"
												binding="#{pages$ConductAuction.dataTableAuctionUnitRequests}" rows="10"
												preserveDataModel="true" preserveSort="false" var="dataItemAuctionUnitRequests"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="false"
												width="100%">

													<t:column id="colBidderName" width="100" >
														<f:facet name="header">
															<t:outputText value="#{msg['bidder.name']}" />
														</f:facet>
														<t:outputText value="#{dataItemAuctionUnitRequests.bidderName}" />
													</t:column>
													
													<t:column id="colBidderSocialSecNo" width="100" >
														<f:facet name="header">
															<t:outputText value="#{msg['customer.socialSecNumber']}" />
														</f:facet>
														<t:outputText value="#{dataItemAuctionUnitRequests.bidderSocialSecNo}" />
													</t:column>
													
													<t:column id="colBidderPassportNo" width="100" >
														<f:facet name="header">
															<t:outputText value="#{msg['customer.passportNumber']}" />
														</f:facet>
														<t:outputText value="#{dataItemAuctionUnitRequests.passportNo}" />
													</t:column>
													
													<t:column id="colUnitNo" width="100" >
														<f:facet name="header">
															<t:outputText value="#{msg['unit.number']}" />
														</f:facet>
														<t:outputText value="#{dataItemAuctionUnitRequests.unitNo}" />
													</t:column>
													
													<t:column id="colUnitType" width="100" >
														<f:facet name="header">
															<t:outputText value="#{msg['unit.type']}" />
														</f:facet>
														<t:outputText value="#{dataItemAuctionUnitRequests.unitTypeEn}" />
													</t:column>
													
													<t:column id="colBidPrice" width="100" >
														<f:facet name="header">
															<t:outputText value="#{msg['auction.conductAuction.bidPrice']}" />
														</f:facet>
														<t:outputText value="#{dataItemAuctionUnitRequests.bidPrice}" />
													</t:column>
													
													<t:column id="colAuctionWin" width="100" >
														<f:facet name="header">
															<t:outputText value="#{msg['auction.conductAuction.auctionWon']}" />
														</f:facet>
														<t:selectBooleanCheckbox value="#{dataItemAuctionUnitRequests.auctionWin}" readonly="true" />
													</t:column>
													
													<t:column id="col10" sortable="false" width="100">
														<f:facet name="header">
															<t:outputText value="#{msg['commons.action']}" />
														</f:facet>
														<t:commandLink
															action="#{pages$ConductAuction.editRequest}"
															value="#{msg['auction.conductAuction.editBidWinner']}"
															rendered="true" />
													</t:column>
											</t:dataTable>
										</div>										
									</t:panelTab>									
							</t:panelTabbedPane>
							
						</td>					
					</tr>
					
					<tr>
						<td colspan="6" align="right">
							<h:commandButton id="btnSave" type="submit" styleClass="BUTTON"
							value="#{msg['commons.saveButton']}"
							action="#{pages$ConductAuction.save}"></h:commandButton>
							<h:commandButton id="btnComplete" type="submit" styleClass="BUTTON"
							value="#{msg['auction.conductAuction.completeAuction']}"
							action="#{pages$ConductAuction.completeAuction}"></h:commandButton>
						</td>
					</tr>
				</table>
                </h:form>
                                   
         	</td>
         </tr>
         
         <tr>
         	<td>

         	</td>         	
         </tr>
        </table>
    
    
    
    
    
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
       <tr><td class="footer"><h:outputLabel value="#{msg['commons.footer.message']}" /></td></tr>
    </table>
        </td>
    </tr>
    </table>
    </body>
</html>
</f:view>
