<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
  <h:panelGrid rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}" styleClass="BUTTON_TD" width="100%" columns="4">
											<h:panelGroup>	
												<h:outputLabel styleClass="mandatory" value="*"/>	
												<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.profit.value']}:" />
											</h:panelGroup>
											<h:inputText id="profitValue" value="#{pages$portfolioManage.portfolioProfitView.strProfitValue}" style="width: 205px;" maxlength="15" />
											
											<h:panelGroup>	
													<h:outputLabel styleClass="mandatory" value="*"/>	
													<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.profit.date.from']}:"></h:outputLabel>
											</h:panelGroup>
											<rich:calendar     value="#{pages$portfolioManage.portfolioProfitView.profitFromDate}"
															   inputStyle="width: 186px; height: 18px"
						                        			   locale="#{pages$portfolioManage.locale}" 
						                        			   popup="true" 
						                        			   datePattern="#{pages$portfolioManage.dateFormat}" 
						                        			   showApplyButton="false" 
						                        			   enableManualInput="false" 
						                        			   cellWidth="24px" cellHeight="22px" 
						                        			   style="width:186px; height:16px"/>
						                        			   
											<h:panelGroup>	
													<h:outputLabel styleClass="mandatory" value="*"/>	
													<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.profit.date.to']}:"></h:outputLabel>
											</h:panelGroup>
											<rich:calendar     value="#{pages$portfolioManage.portfolioProfitView.profitToDate}"
															   inputStyle="width: 186px; height: 18px"
						                        			   locale="#{pages$portfolioManage.locale}" 
						                        			   popup="true" 
						                        			   datePattern="#{pages$portfolioManage.dateFormat}" 
						                        			   showApplyButton="false" 
						                        			   enableManualInput="false" 
						                        			   cellWidth="24px" cellHeight="22px" 
						                        			   style="width:186px; height:16px"/>
																											
											<h:outputText value=""></h:outputText>
											<h:commandButton styleClass="BUTTON"
															value="#{msg['commons.Add']}"
															action="#{pages$portfolioManage.onProfitAdd}"
															style="width: 120px;" />
										</h:panelGrid>
										
										
										<t:div styleClass="contentDiv" style="width:99%">																
	                                       <t:dataTable id="profitDataTable"  
												rows="#{pages$portfolioManage.paginatorRows}"
												value="#{pages$portfolioManage.portfolioProfitViewList}"
												binding="#{pages$portfolioManage.profitDataTable}"																	
												preserveDataModel="false" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
												
												<t:column width="30%" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['portfolio.manage.tab.portfolio.profit.value']}" />
													</f:facet>
													<t:outputText value="#{dataItem.profitValue}" rendered="#{dataItem.isDeleted == 0}" />
												</t:column>
																													
												<t:column width="30%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['portfolio.manage.tab.portfolio.profit.date.from']}" />
													</f:facet>
													<t:outputText value="#{dataItem.profitFromDate}" rendered="#{dataItem.isDeleted == 0}" />																		
												</t:column>
												
												<t:column width="30%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['portfolio.manage.tab.portfolio.profit.date.to']}" />
													</f:facet>
													<t:outputText value="#{dataItem.profitToDate}" rendered="#{dataItem.isDeleted == 0}" />																		
												</t:column>
												
												<t:column width="10%" sortable="false" rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.action']}" />
													</f:facet>
													<a4j:commandLink onclick="if (!confirm('#{msg['profit.confirm.delete']}')) return" action="#{pages$portfolioManage.onProfitDelete}" rendered="#{dataItem.isDeleted == 0}" reRender="profitDataTable,profitGridInfo12">
														<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />
													</a4j:commandLink>																															
												</t:column>
											  </t:dataTable>										
											</t:div>
									
                                          		<t:div id="profitGridInfo" styleClass="contentDivFooter" style="width:100%">
														<t:panelGrid id="rqtkRecNumTable7"  columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
															<t:div id="rqtkRecNumDiv6" styleClass="RECORD_NUM_BG">
																<t:panelGrid id="rqtkRecNumTbl6" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
																	<h:outputText value="#{msg['commons.recordsFound']}"/>
																	<h:outputText value=" : "/>
																	<h:outputText value="#{pages$portfolioManage.profitRecordSize}"/>																						
																</t:panelGrid>
															</t:div>
															  
														      <CENTER>
															  	<t:dataScroller id="rqtkscroller7" for="expectedRateDataTable" paginator="true"
																				fastStep="1"  
																				paginatorMaxPages="#{pages$portfolioManage.paginatorMaxPages}" 
																				immediate="false"
																				paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true" 
																			    pageIndexVar="pageNumber"
																			    styleClass="SCH_SCROLLER"
																			    paginatorActiveColumnStyle="font-weight:bold;"
																			    paginatorRenderLinkForActive="false" 
																				paginatorTableStyle="grid_paginator" layout="singleTable"
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">
			
																    <f:facet  name="first">
																		<t:graphicImage url="../#{path.scroller_first}" id="lblFRequestHistory7"></t:graphicImage>
																	</f:facet>
				
																	<f:facet name="fastrewind">
																		<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFRRequestHistory7"></t:graphicImage>
																	</f:facet>
				
																	<f:facet name="fastforward">
																		<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFRequestHistory7"></t:graphicImage>
																	</f:facet>
				
																	<f:facet name="last">
																		<t:graphicImage url="../#{path.scroller_last}" id="lblLRequestHistory7"></t:graphicImage>
																	</f:facet>
				
			                                                      	<t:div id="rqtkPageNumDiv7" styleClass="PAGE_NUM_BG">
																	   	<t:panelGrid id="rqtkPageNumTable6" styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0" >
																	 		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																			<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																		</t:panelGrid>																							
																	</t:div>
																 </t:dataScroller>
															 	</CENTER>																		      
												        </t:panelGrid>
			                          			 </t:div>   