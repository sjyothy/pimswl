
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="javascript" type="text/javascript">
	function showPropertyList()
	{
	   var screen_width = screen.width;
       var screen_height = screen.height;
       var popup_width = screen_width-250;
       var popup_height = screen_height-280;
       var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
	   window.open('propertyListPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');
	   //window.document.forms[0].submit();	   		
	}
	
	function checkChanged()
	{
		alert('check changed!');	
	}
	
	var totalDepositAmount=0;
	var aucUnitNo;
	var amountToReceive = 0;
	function AddUnits(field,unitId,auctionUnitNo,depositAmount)
	{
	  totalDepositAmount = parseFloat(document.getElementById("auctionRequestForm:totalDeposit").value);
	  aucUnitNo = document.getElementById("auctionRequestForm:auctionUnitsOfRequest").value;
	  document.getElementById("auctionRequestForm:opMsgs").value = "";
	  if(field.checked)
	  {
		    if(aucUnitNo!=null && aucUnitNo.length>0 )
		    {					
		       aucUnitNo=aucUnitNo+","+auctionUnitNo + ":" + unitId;		       
		    }
			else
			{
				aucUnitNo=auctionUnitNo + ":" + unitId; 
			}
			totalDepositAmount = parseFloat(totalDepositAmount) + parseFloat(depositAmount);
   	   }
   	   else
   	   {
	   	   var delimiters=",";
		   var unitArr=aucUnitNo.split(delimiters);
		   aucUnitNo="";
		   for(i=0;i<unitArr.length;i++)
		   {
			   if(unitArr[i]!=auctionUnitNo + ":" + unitId)
			   {
		       	   if(aucUnitNo.length>0 )
			       {
			             aucUnitNo=aucUnitNo+","+unitArr[i];
		
	               }
			       else
			       {
			             aucUnitNo=unitArr[i];
		           }
			   }		    
		   }
		   totalDepositAmount = parseFloat(totalDepositAmount) - parseFloat(depositAmount); 
   	   }
   	   document.getElementById("auctionRequestForm:inputTextTotalDepositAmount").value = totalDepositAmount.toFixed(2);
   	   document.getElementById("auctionRequestForm:totalDeposit").value = totalDepositAmount;
   	   document.getElementById("auctionRequestForm:auctionUnitsOfRequest").value = aucUnitNo;   	   
	}
	
	function showPaymentPopup()
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
		window.open('receivePayment.jsf?parentHidden=auctionRequestForm:isPayPopupClosed','_blank','width='+(screen_width-10)+',height='+(screen_height-180)+',left=0,top=40,scrollbars=yes,status=yes');		
	}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html>
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"	CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
		</head>

		<body dir="${sessionScope.CurrentLocale.dir}"
			lang="${sessionScope.CurrentLocale.languageCode}">


			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<c:choose>
									<c:when
										test="${sessionScope.CurrentLocale.languageCode eq 'en'}">
										<td
											background="../resources/images/Grey panel/Grey-Panel-left-1.jpg"
											height="38" style="background-repeat: no-repeat;"
											width="100%">
											<font
												style="font-family: Trebuchet MS; font-weight: regular; font-size: 19px; margin-left: 15px; top: 30px;">
												<h:outputLabel value="Unit Details"></h:outputLabel> </font>
											<!--<IMG src="../resources/images/Grey panel/Grey-Panel-left-1.jpg"/>-->
										</td>
									</c:when>
									<c:otherwise>
										<td width="100%">
											<IMG
												src="../resources/images/ar/Detail/Grey Panel/Grey-Panel-right-1.jpg"></img>
										</td>
									</c:otherwise>
								</c:choose>

								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>

								<td height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height: 495px;">
										<h:form id="auctionRequestForm">
											<table width="100%">
												<tr>
													<td colspan="6">														
															<h:outputText id="opMsgs" 
																value="#{pages$UnitDetails.messages}" escape="false" styleClass="INFO_FONT"/>
															<h:outputText id="errorMsgs" 
																value="#{pages$UnitDetails.errorMsgs}" escape="false" styleClass="ERROR_FONT"/>	
													</td>
												</tr>
												<tr>
													<td colspan="6">
														<div class="MARGIN" style="width:95%">
															<table cellpadding="0" cellspacing="0">
																<tr>
																<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
																<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
																<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
																</tr>
															</table>
															<div class="DETAIL_SECTION">
															<h:outputLabel value="#{msg['receiveProperty.propertyDetails']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
																<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
																	<tr>
																		<td>
																			<h:outputLabel value="#{msg['property.propertyNumber']}"></h:outputLabel>
																		</td>
																		<td colspan="2">
																			<h:inputText id="propNumber" style="width: 186px"
																				disabled="true"
																				value="#{pages$UnitDetails.property.propertyNumber}"></h:inputText>
																			<c:if test="${pages$UnitDetails.isAddMode}">
																				<h:commandLink
																					onclick="showPropertyList();">
																					<h:graphicImage id="populateIcon" 
																					title="#{msg['property.search']}" 
																					url="../resources/images/app_icons/property.png"/>
																				</h:commandLink>
																			</c:if>	
																		</td>
																		<td colspan="1">
																			<h:commandButton rendered="false" styleClass="BUTTON"
																				style="width:100px"
																				actionListener="#{pages$UnitDetails.showPropertyList}"
																				value="Search Property" id="btnSearchProperty">
																			</h:commandButton>
																		</td>

																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel value="#{msg['commons.typeCol']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="propertyType" style="width: 186px"
																				disabled="true"
																				value="#{pages$UnitDetails.property.propertyTypeEn}"></h:inputText>
																		</td>
																		<td>
																			<h:outputLabel value="#{msg['property.endowedName']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="endowedName" style="width: 186px"
																				disabled="true"
																				value="#{pages$UnitDetails.property.endowedName}"></h:inputText>
																		</td>

																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel value="#{msg['property.category']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="propertyCategory"
																				style="width: 186px" disabled="true"
																				value="#{pages$UnitDetails.property.categoryTypeEn}"></h:inputText>
																		</td>
																		<td>
																			<h:outputLabel value="#{msg['property.commercialName']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="commercialName" style="width: 186px"
																				disabled="true"
																				value="#{pages$UnitDetails.property.commercialName}"></h:inputText>
																		</td>

																	</tr>
																</table>
															</div>
														</div>
<%-------*************** --%>
														<div class="MARGIN" style="width:95%">
															<table cellpadding="0" cellspacing="0">
																<tr>
																<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
																<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
																<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
																</tr>
															</table>
															<div class="DETAIL_SECTION">
															<h:outputLabel value="#{msg['generateFloors.SectionHeader']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
																<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
																	<tr>
																		<td>
																			<h:outputLabel value="#{msg['inquiry.floorNumber']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="floors" 
																			value="#{pages$UnitDetails.floorId}"
																			valueChangeListener="#{pages$UnitDetails.floorValueChange}"
																			onchange="submit();"
																			style="width: 192px; height: 24px">
																				<f:selectItems value="#{pages$UnitDetails.propertyFloors}"/>
																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:outputLabel value="#{msg['generateFloors.FloorType']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="floorType" style="width: 186px"
																				disabled="true"
																				value="#{pages$UnitDetails.floorType}"></h:inputText>
																		</td>
																	</tr>
																</table>
															</div>
														</div>
<%-------*************** --%>
														<div class="MARGIN" style="width:95%">
															<table cellpadding="0" cellspacing="0">
																<tr>
																<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
																<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
																<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
																</tr>
															</table>
															<div class="DETAIL_SECTION">
															<h:outputLabel value="#{msg['generateUnits.SectionHeader']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
																<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
																	<tr>
																		<td>
																			<h:outputLabel value="#{msg['commons.number']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="unitNumber"
																				value="#{pages$UnitDetails.unitView.unitNumber}"
																				style="width: 184px"></h:inputText>
																		</td>
																		<td>
																			<h:outputLabel value="#{msg['commons.typeCol']}" style="width: 186px"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="unitTypes" required="false"
																				value="#{pages$UnitDetails.unitTypeId}">

																				<f:selectItems
																					value="#{pages$ApplicationBean.unitType}" />
																			</h:selectOneMenu>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel value="#{msg['contract.UnitUsusageType']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="unitUsageTypes" required="false"
																				value="#{pages$UnitDetails.unitUsageTypeId}">

																				<f:selectItems
																					value="#{pages$ApplicationBean.unitUsageTypeList}" />
																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:outputLabel value="#{msg['unit.investmentType']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="investmentType" required="false"
																				value="#{pages$UnitDetails.investmentTypeId}">

																				<f:selectItems
																					value="#{pages$ApplicationBean.unitInvestmentTypeList}" />
																			</h:selectOneMenu>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel value="#{msg['generateUnits.UnitSideType']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="unitSideTypes" required="false"
																				value="#{pages$UnitDetails.unitSideTypeId}">

																				<f:selectItems
																					value="#{pages$ApplicationBean.unitSideType}" />
																			</h:selectOneMenu>

																		</td>
																		<td>
																			<h:outputLabel value="#{msg['inquiry.Person.unitArea']} (Sq. Ft)"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="unitArea"
																				value="#{pages$UnitDetails.unitArea}"
																				styleClass="A_RIGHT_NUM" style="width:184px"></h:inputText>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel value="#{msg['inquiry.rentvalue']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="unitRent"
																				value="#{pages$UnitDetails.rentValue}"
																				styleClass="A_RIGHT_NUM" style="width:184px">
																			</h:inputText>

																		</td>
																		<td>
																			<h:outputLabel value="#{msg['receiveProperty.rentProcess']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="rentProcess" required="false"
																				value="#{pages$UnitDetails.rentProcessId}">

																				<f:selectItems
																					value="#{pages$ApplicationBean.rentProcessList}" />
																			</h:selectOneMenu>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel value="#{msg['receiveProperty.auctionOpeningPrice']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="auctionOpeningPrice"
																				value="#{pages$UnitDetails.requiredPrice}"
																				styleClass="A_RIGHT_NUM" style="width:184px"></h:inputText>

																		</td>
																		<td>
																			<h:outputLabel value="#{msg['payment.accountNo']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="accountNumber"
																				value="#{pages$UnitDetails.unitView.accountNumber}"
																				style="width: 184px"></h:inputText>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel value="#{msg['receiveProperty.gracePeriod']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="gracePeriod"
																				value="#{pages$UnitDetails.gracePeriod}"
																				styleClass="A_RIGHT_NUM" style="width:184px"></h:inputText>

																		</td>
																		<td>
																			<h:outputLabel value="#{msg['receiveProperty.gracePeriodType']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="gracePeriodTypes" required="false" 
																				value="#{pages$UnitDetails.gracePeriodTypeId}">
																				<f:selectItems
																					value="#{pages$ApplicationBean.gracePeriodTypeList}" />
																			</h:selectOneMenu>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel value="#{msg['unit.bedRooms']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="bedRooms"
																				value="#{pages$UnitDetails.noOfBed}"
																				styleClass="A_RIGHT_NUM" style="width:184px"></h:inputText>

																		</td>
																		<td>
																			<h:outputLabel value="#{msg['unit.bathRooms']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="bathRooms"
																				value="#{pages$UnitDetails.noOfBath}"
																				styleClass="A_RIGHT_NUM" style="width:184px"></h:inputText>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel value="#{msg['unit.livingRooms']}"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="livingRooms"
																				value="#{pages$UnitDetails.noOfLiving}"
																				styleClass="A_RIGHT_NUM" style="width:184px"></h:inputText>

																		</td>
																	</tr>
																</table>
															</div>
														</div>

													</td>
												</tr>

											</table>

											<div>
												<table width="97%">
													<tr>
														<td class="BUTTON_TD">

															<h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}"
																id="btnSave" action="#{pages$UnitDetails.persistUnit}">
															</h:commandButton>

															<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}"
																id="btnCancel" action="#{pages$UnitDetails.cancel}">
															</h:commandButton>

														</td>
													</tr>
												</table>
											</div>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</body>
	</html>
</f:view>

