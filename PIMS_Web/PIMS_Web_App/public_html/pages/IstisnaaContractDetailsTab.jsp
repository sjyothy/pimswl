<t:div id="contractDetailsTab" styleClass="TAB_DETAIL_SECTION" style="width:100%" >
	<t:panelGrid id="contractDetailsTable" cellpadding="1px" width="100%" cellspacing="5px" styleClass="TAB_DETAIL_SECTION_INNER"  columns="4"
	>
					
            <h:outputLabel styleClass="LABEL"  value="#{msg['commons.referencenumber']}:" />
		    <h:inputText maxlength="20"  style="width:85%;" styleClass="READONLY"   id="txtContractRefNum" readonly="true" binding="#{pages$IstisnaaContractDetailsTab.txtContractNo}" value="#{pages$IstisnaaContractDetailsTab.contractNo}" ></h:inputText>
	        
	        <h:outputLabel id="lblContractType" styleClass="LABEL"  value="#{msg['contract.contractType']}:"></h:outputLabel>
	        <h:inputText maxlength="20"  style="width:85%;" styleClass="READONLY"  id="txtContractType" readonly="true" binding="#{pages$IstisnaaContractDetailsTab.txtContractType}" value="#{pages$IstisnaaContractDetailsTab.contractType}" ></h:inputText>

	      <t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
		    <h:outputLabel styleClass="LABEL" value="#{msg['istisnaaContract.lbl.ownerNumber']}:"></h:outputLabel>
		  </t:panelGroup>  
	        
	        <t:panelGroup >
	          <h:inputText id="txtinvestorNumber" style="width:85%;" styleClass="READONLY"   readonly="true" binding="#{pages$IstisnaaContractDetailsTab.txtContractorNum}"  title="#{pages$IstisnaaContractDetailsTab.contractorNum}"  value="#{pages$IstisnaaContractDetailsTab.contractorNum}" ></h:inputText>
	          <h:graphicImage  binding="#{pages$IstisnaaContractDetailsTab.imgInvestorSearch}"  url="../resources/images/app_icons/Search-tenant.png" onclick="javaScript:showPopup();"  style ="MARGIN: 0px 0px -4px;" alt="#{msg[contract.searchTenant]}" ></h:graphicImage>
	          <h:graphicImage  id="imgViewInvestor" binding="#{pages$IstisnaaContractDetailsTab.imgViewInvestor}" onclick="javaScript:showPersonReadOnlyPopup();" title="#{msg['commons.view']}" url="../resources/images/app_icons/Tenant.png" style ="MARGIN: 0px 0px -4px; CURSOR: hand;"  />
     		</t:panelGroup>
     		<h:outputLabel styleClass="LABEL"  value="#{msg['istisnaaContract.lbl.ownerName']}:"></h:outputLabel>
            <h:inputText id="txtinvestorName" style="width:85%;" readonly="true" styleClass="READONLY"  binding="#{pages$IstisnaaContractDetailsTab.txtContractorName}" value="#{pages$IstisnaaContractDetailsTab.contractorName}" title="#{pages$IstisnaaContractDetailsTab.contractorName}" ></h:inputText>
           	
           	
           	<t:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
		        <h:outputLabel  styleClass="LABEL"  value="#{msg['contract.contractDate']}:"></h:outputLabel>
			</t:panelGroup>
			<rich:calendar  id="contractDate" value="#{pages$IstisnaaContractDetailsTab.contractDate}" 
							popup="true" datePattern="#{pages$IstisnaaContractDetailsTab.dateFormat}"
                            binding="#{pages$IstisnaaContractDetailsTab.contractDateCalendar}" 
                            timeZone="#{pages$IstisnaaContractDetailsTab.timeZone}"
                            showApplyButton="false" 
                            enableManualInput="false" cellWidth="24px" 
                            locale="#{pages$IstisnaaContractDetailsTab.locale}">
          <a4j:support event="onchanged" action="#{pages$IstisnaaContractDetailsTab.calculateContractStartDate}" reRender="contractStartDate,datetimeContractEndDate" />
          </rich:calendar>
		     <h:outputLabel   value="#{msg['botContractDetails.ContractHejjriDate']}:"></h:outputLabel>
		    <h:inputText  id="txtContractHejjriDate"     styleClass="A_RIGHT_NUM" maxlength="12"  ></h:inputText>

            <t:panelGroup>
			   <h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
		       <h:outputLabel styleClass="LABEL"  value="#{msg['botContractDetails.ContractPeriod']}:"></h:outputLabel>
			</t:panelGroup>
		    <t:panelGrid columns="2" columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
		            <h:inputText  id="txtContractPeriod" style="width:100%;" styleClass="A_RIGHT_NUM" maxlength="4" value="#{pages$IstisnaaContractDetailsTab.txtContractPeriod}"  >
		            <a4j:support event="onchange" action="#{pages$IstisnaaContractDetailsTab.calculateContractExpiryDate}" reRender="datetimeContractEndDate" />
		            </h:inputText>			
					<h:selectOneMenu id="selectContractPeriod"  style="width:80px;" value="#{pages$IstisnaaContractDetailsTab.selectOneContractPeriod}" binding="#{pages$IstisnaaContractDetailsTab.cmbContractPeriod}" >
					 
					 <f:selectItems value="#{pages$ApplicationBean.gracePeriodTypeList}" />
					 <a4j:support event="onchange" action="#{pages$IstisnaaContractDetailsTab.calculateContractExpiryDate}" reRender="datetimeContractEndDate" />
					</h:selectOneMenu>
			</t:panelGrid>
			<t:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
        		<h:outputLabel styleClass="LABEL"  value="#{msg['botContractDetails.GracePeriod']}:"></h:outputLabel>
			</t:panelGroup>
		    <t:panelGrid columns="2" columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
		            <h:inputText  id="txtGracePeriod"   style="width:100%;"  styleClass="A_RIGHT_NUM" maxlength="4" value="#{pages$IstisnaaContractDetailsTab.txtGracePeriod}">
		            <a4j:support event="onchange" action="#{pages$IstisnaaContractDetailsTab.calculateContractStartDate}" reRender="contractStartDate,datetimeContractEndDate" />
		            </h:inputText>			
					<h:selectOneMenu id="selectGracePeriod"   style="width:80px;" value="#{pages$IstisnaaContractDetailsTab.selectOneGracePeriod}" binding="#{pages$IstisnaaContractDetailsTab.cmbGracePeriod}" >
					 
					 <f:selectItems value="#{pages$ApplicationBean.gracePeriodTypeList}" />
					 <a4j:support event="onchange" action="#{pages$IstisnaaContractDetailsTab.calculateContractStartDate}" reRender="contractStartDate,datetimeContractEndDate" />
					</h:selectOneMenu>
			</t:panelGrid>
		    
		            
			
            <t:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
		        <h:outputLabel  styleClass="LABEL"  value="#{msg['contract.date.Start']}:"></h:outputLabel>
			</t:panelGroup>
			<rich:calendar  id="contractStartDate" value="#{pages$IstisnaaContractDetailsTab.contractStartDate}" 
							popup="true" datePattern="#{pages$IstisnaaContractDetailsTab.dateFormat}"
                            binding="#{pages$IstisnaaContractDetailsTab.startDateCalendar}" 
                            timeZone="#{pages$IstisnaaContractDetailsTab.timeZone}"
                            showApplyButton="false" 
                            enableManualInput="false" cellWidth="24px" 
                            locale="#{pages$IstisnaaContractDetailsTab.locale}">
          <a4j:support event="onchanged" action="#{pages$IstisnaaContractDetailsTab.calculateContractExpiryDate}" reRender="datetimeContractEndDate" />
          </rich:calendar>
          <t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
		    <h:outputLabel   value="#{msg['contract.date.expiry']}:"></h:outputLabel>
		  </t:panelGroup>
		  <rich:calendar id="datetimeContractEndDate" value="#{pages$IstisnaaContractDetailsTab.contractEndDate}" 
		              binding="#{pages$IstisnaaContractDetailsTab.endDateCalendar}" disabled="true"
                      popup="true" datePattern="#{pages$IstisnaaContractDetailsTab.dateFormat}" timeZone="#{pages$IstisnaaContractDetailsTab.timeZone}" showApplyButton="false" 
                      enableManualInput="false"  cellWidth="24px" >
           </rich:calendar>
            <h:outputLabel  value="#{msg['istisnaaContract.lbl.istisnaaMethod']}:"></h:outputLabel>
           <h:selectOneMenu id="selectIstisnaaMehthod"  style="width:140px;" value="#{pages$IstisnaaContractDetailsTab.selectOneIstisnaaMehthod}" binding="#{pages$IstisnaaContractDetailsTab.cmbIstisnaaMehthod}" >
			 <f:selectItem  itemLabel="#{msg['istisnaaContract.istisnaaMethodFixed']}" itemValue="0"/>
			 <f:selectItem  itemLabel="#{msg['istisnaaContract.istisnaaMethodPercentage']}" itemValue="1"/>
			 <a4j:support   event="onchange" action="#{pages$IstisnaaContractDetailsTab.calculateShares}" 
			                reRender="tabPanel" />
			</h:selectOneMenu>
			
           <t:panelGroup rendered="false">
				<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
		        <h:outputLabel styleClass="LABEL"  value="#{msg['botContractDetails.RentAmount']}:"></h:outputLabel>
			</t:panelGroup>
		    <h:inputText rendered="false" id="totalContract"    styleClass="A_RIGHT_NUM" maxlength="15" binding="#{pages$IstisnaaContractDetailsTab.txtTotalAmount}" value="#{pages$IstisnaaContractDetailsTab.totalAmount}"></h:inputText>
             
             
	           <t:panelGroup id="lblGrpamafShare" rendered="#{pages$IstisnaaContractDetailsTab.isIstisnaaMethodPercentage}">
				  <h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			      <h:outputLabel styleClass="LABEL"  value="#{msg['istisnaaContract.lbl.firstPartyShare']}:"></h:outputLabel>
			   </t:panelGroup>
              
			 <h:inputText id="amafShare"     rendered="#{pages$IstisnaaContractDetailsTab.isIstisnaaMethodPercentage}" styleClass="A_RIGHT_NUM" maxlength="15"  value="#{pages$IstisnaaContractDetailsTab.amafShare}">
               <a4j:support event="onchange" action="#{pages$IstisnaaContractDetailsTab.calculateSecondPartyShares}" reRender="secondPartyShare" />
               </h:inputText>
			
	           <t:panelGroup id="lblGrpSecondPartyShare">
				 <h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			     <h:outputLabel styleClass="LABEL"  value="#{msg['istisnaaContract.lbl.secondPartyShare']}:"></h:outputLabel>
			   </t:panelGroup>
               <h:inputText id="secondPartyShare"    styleClass="A_RIGHT_NUM" maxlength="15"  value="#{pages$IstisnaaContractDetailsTab.secondPartyShare}">
               <a4j:support event="onchange" action="#{pages$IstisnaaContractDetailsTab.calculateFirstPartyShares}" reRender="amafShare" />
               </h:inputText>
   </t:panelGrid>		              
</t:div>
