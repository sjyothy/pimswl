<%-- 
  - Author: Anil Verani
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Construction Tenders
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	    
	    function resetValues()
      	{
      	    document.getElementById("searchFrm:tenderNumber").value="";
			document.getElementById("searchFrm:tenderTypeCombo").selectedIndex=0;
      	    $('searchFrm:issueDateFrom').component.resetSelectedDate();
			$('searchFrm:issueDateTo').component.resetSelectedDate();
			$('searchFrm:endDateFrom').component.resetSelectedDate();
			$('searchFrm:endDateTo').component.resetSelectedDate();
			document.getElementById("searchFrm:propertyName").value="";
			document.getElementById("searchFrm:tenderDescription").value="";
			
        }
        
        function sendToParent(tenderId,tenderNumber,tenderDescription)
        {
		    window.opener.populateTender(tenderId,tenderNumber,tenderDescription);
		    window.close();
		}

        function submitForm()
	    {
          document.getElementById('searchFrm').submit();
          document.getElementById("searchFrm:country").selectedIndex=0;
	    }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			 <meta http-equiv="pragma" content="no-cache">
			 <meta http-equiv="cache-control" content="no-cache">
			 <meta http-equiv="expires" content="0">
			 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
	</head>

	<body class="BODY_STYLE">
	     <div class="containerDiv">
			<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
			%>
	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
						<c:when test="${!pages$constructionTenderSearch.isViewModePopUp}">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</c:when>
					</c:choose>
				</tr>

				<tr width="100%">
					<c:choose>
						<c:when test="${!pages$constructionTenderSearch.isViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
										<h:outputLabel value="#{msg['constructionTender.search.heading']}" styleClass="HEADER_FONT"/>
										<h:inputHidden id="hdnTenderType" value="#{pages$constructionTenderSearch.searchFilterView.tenderTypeId}"/>
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td width="100%" height="466px" valign="top" >
									<div class="SCROLLABLE_SECTION  AUC_SCH_SS" style="height:400px;">
									<h:form id="searchFrm" style="WIDTH: 97.6%;">
										<table border="0" class="layoutTable">
											<tr>
												<td>
													<h:outputText value="#{pages$constructionTenderSearch.errorMessages}"  escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$constructionTenderSearch.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
										<div class="MARGIN"> 
										<table cellpadding="0" cellspacing="0" width="100%">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
											<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
										<div class="DETAIL_SECTION">
											<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
											<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
											<tr>
											<td width="97%">
											<table width="100%">
												<tr>
													<td  width="25%">
														<h:outputLabel styleClass="LABEL" value="#{msg['tender.number']}:"></h:outputLabel>
													</td>
													<td  width="25%">
															<h:inputText id="tenderNumber"
																value="#{pages$constructionTenderSearch.searchFilterView.tenderNumber}"
																 maxlength="20">
															</h:inputText>
													 </td>
													<td  width="25%">
														<h:outputLabel styleClass="LABEL" value="#{msg['serviceContract.serviceType']}:"></h:outputLabel>
													</td>
													<td  width="25%">
														<h:selectOneMenu id="constructionServiceTypeCombo" binding="#{pages$constructionTenderSearch.constructionServiceTypeCombo}"  required="false" value="#{pages$constructionTenderSearch.searchFilterView.constructionServiceTypeId}">
															<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
															<f:selectItems value="#{pages$constructionTenderSearch.constructionServiceTypeList}" />
														</h:selectOneMenu>
													</td>		
												</tr>
												
												<tr>	
													<td >
														<h:outputLabel styleClass="LABEL" value="#{msg['constructionTender.search.tender.status']}:"></h:outputLabel>
													</td>
													<td >
														<h:selectOneMenu id="tenderStatusCombo" binding="#{pages$constructionTenderSearch.tenderStatusCombo}"  required="false" value="#{pages$constructionTenderSearch.searchFilterView.tenderStatusId}">
															<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
															<f:selectItems value="#{pages$constructionTenderSearch.tenderStatusList}" />
														</h:selectOneMenu>
													</td>
													<td >
														<h:outputLabel styleClass="LABEL" value="#{msg['tenderManagement.startFromDate']}:"></h:outputLabel>
													</td>
													<td >
														<rich:calendar id="issueDateFrom" inputStyle="width:170px; height: 14px" value="#{pages$constructionTenderSearch.searchFilterView.tenderIssueDateFrom}"
								                        locale="#{pages$constructionTenderSearch.locale}" popup="true" datePattern="#{pages$constructionTenderSearch.dateFormat}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px" style="width:186px; height:16px"/>
													</td>
												</tr>
												
												<tr>
													
													<td >
														<h:outputLabel styleClass="LABEL" value="#{msg['tenderManagement.startToDate']}:" />
													</td>
													<td >
														<rich:calendar id="issueDateTo" inputStyle="width:170px; height: 14px" value="#{pages$constructionTenderSearch.searchFilterView.tenderIssueDateTo}"
								                        locale="#{pages$constructionTenderSearch.locale}" popup="true" datePattern="#{pages$constructionTenderSearch.dateFormat}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px" style="width:186px; height:16px"/>
								                    </td>
								                    <td >
														<h:outputLabel styleClass="LABEL" value="#{msg['tenderManagement.endFromDate']}:"></h:outputLabel>
													</td>
													<td >
														<rich:calendar id="endDateFrom" inputStyle="width:170px; height: 14px" value="#{pages$constructionTenderSearch.searchFilterView.tenderEndDateFrom}"
								                        locale="#{pages$constructionTenderSearch.locale}" popup="true" datePattern="#{pages$constructionTenderSearch.dateFormat}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px" style="width:186px; height:16px"/>
													</td>
												</tr>
												<tr>
													
													<td >
														<h:outputLabel styleClass="LABEL" value="#{msg['tenderManagement.endToDate']}:" />
													</td>
													<td >
														<rich:calendar id="endDateTo" inputStyle="width:170px; height: 14px" value="#{pages$constructionTenderSearch.searchFilterView.tenderEndDateTo}"
								                        locale="#{pages$constructionTenderSearch.locale}" popup="true" datePattern="#{pages$constructionTenderSearch.dateFormat}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px" style="width:186px; height:16px"/>
								                    </td>
								                    <td >
														<h:outputLabel styleClass="LABEL" value="#{msg['constructionTender.search.project.number']}:"></h:outputLabel>
													</td>
													<td >
														<h:inputText id="propertyName"
															value="#{pages$constructionTenderSearch.searchFilterView.projectNumber}"
															></h:inputText>
													</td>
												</tr>
												<tr>
													<td >
														<h:outputLabel styleClass="LABEL" value="#{msg['constructionTender.search.project.name']}:"></h:outputLabel>
													</td>
													<td >
														<h:inputText id="tenderDescription"
															value="#{pages$constructionTenderSearch.searchFilterView.projectName}"
															></h:inputText>
													</td>
												</tr>
												<tr>
													<td class="BUTTON_TD" colspan="4">
															<pims:security screen="Pims.Tendering.SearchTender.SearchButton" action="create">
						     									<h:commandButton styleClass="BUTTON" value="#{msg['commons.search']}" action="#{pages$constructionTenderSearch.searchTenders}" style="width: 80px" tabindex="7"></h:commandButton>
						     								</pims:security>					     									
					     									<h:commandButton styleClass="BUTTON" value="#{msg['commons.clear']}" onclick="javascript:resetValues();" style="width: 80px" tabindex="7"></h:commandButton>
															<h:commandButton styleClass="BUTTON" value="#{msg['tenderManagement.buttons.addTender']}" action="#{pages$constructionTenderSearch.onAddTender}" style="width: 80px" tabindex="7" rendered="#{!pages$constructionTenderSearch.isViewModePopUp}"></h:commandButton>
													</td>
												</tr>
												</table>
												</td>
												<td width="3%">
												</td>
												</tr>
											</table>
										</div>	
										</div>
										<div style="padding-bottom:7px;padding-left:10px;padding-right:7px;padding-top:7px;">
										<div class="imag">&nbsp;</div>
										<div class="contentDiv" style="width: 99%">
											<t:dataTable id="dt1"
												value="#{pages$constructionTenderSearch.dataList}"
												binding="#{pages$constructionTenderSearch.dataTable}" 
												rows="#{pages$constructionTenderSearch.paginatorRows}"
												preserveDataModel="false" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												width="100%">
												<t:column id="tenderNumberCol" width="12%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['tender.number.gridHeader']}" />
													</f:facet>
													<t:outputText value="#{dataItem.tenderNumber}" />
												</t:column>
												<t:column id="tenderTypeCol" width="12%" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['commons.typeCol']}" />
													</f:facet>
													<t:outputText value="#{dataItem.tenderType}" />
												</t:column>
												<t:column id="issueDateCol" width="15%" sortable="true">
													<f:facet  name="header">
														<t:outputText value="#{msg['commons.issueDate']}" />
													</f:facet>
													<t:outputText value="#{dataItem.tenderIssueDateStr}" />
												</t:column>
												<t:column id="endDateCol" width="14%" sortable="true">
													<f:facet  name="header">
														<t:outputText value="#{msg['commons.endDate']}" />
													</f:facet>
													<t:outputText value="#{dataItem.tenderEndDateStr}" />
												</t:column>
												<t:column id="projectNameCol" width="15%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['constructionTender.search.project.name']}" />
													</f:facet>
													<t:outputText value="#{dataItem.projectName}" />
												</t:column>
												<t:column id="tenderStatusCol" width="12%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.status']}" />
													</f:facet>
													<t:outputText value="#{dataItem.tenderStatus}" />
												</t:column>
												<t:column id="actionCol" sortable="false" width="20%" rendered="#{!pages$constructionTenderSearch.isViewModePopUp}">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.action']}" />
													</f:facet>
													<pims:security screen="Pims.Tendering.SearchTender.ViewTenderGridAction" action="create">
														<t:commandLink
															action="#{pages$constructionTenderSearch.onView}"
															rendered="#{dataItem.showView}" >
															<h:graphicImage title="#{msg['commons.view']}" url="../resources/images/app_icons/view_tender.png" />&nbsp;
														</t:commandLink>
													</pims:security>
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													<pims:security screen="Pims.Tendering.SearchTender.EditTenderGridAction" action="create">
														<t:commandLink
															action="#{pages$constructionTenderSearch.onEdit}"
															rendered="#{dataItem.showEdit}" >
															<h:graphicImage title="#{msg['commons.edit']}" url="../resources/images/app_icons/edit_tender.png" />&nbsp;
														</t:commandLink>
													</pims:security>
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													<pims:security screen="Pims.Tendering.SearchTender.CancelTenderGridAction" action="create">
														<t:commandLink
															action="#{pages$constructionTenderSearch.onCancel}"
															onclick="if (!confirm('#{msg['extendApplication.search.confirm.cancel']}')) return"
															rendered="#{dataItem.showCancel}" >
															<h:graphicImage title="#{msg['commons.cancel']}" url="../resources/images/app_icons/cancel_tender.png" />&nbsp;
														</t:commandLink>
													</pims:security>
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													<pims:security screen="Pims.Tendering.SearchTender.ReOpenTenderGridAction" action="create">
														<t:commandLink 
															action="#{pages$constructionTenderSearch.onReopen}"
															onclick="if (!confirm('#{msg['extendApplication.search.confirm.reOpen']}')) return"
															rendered="#{dataItem.showReopen}" >
														<h:graphicImage title="#{msg['tenderManagement.actions.reopen']}" url="../resources/images/app_icons/reopen_tender.png" />&nbsp;
														</t:commandLink>
													</pims:security>
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													<pims:security screen="Pims.Tendering.SearchTender.ReceiveProposalGridAction" action="create">
														<t:commandLink 
															action="#{pages$constructionTenderSearch.onReceiveProposal}"
															rendered="#{dataItem.showReceiveProposal}" >
														<h:graphicImage title="#{msg['tenderManagement.actions.receiveProsposal']}" url="../resources/images/app_icons/receiveProposal_tender.png" />&nbsp;
														</t:commandLink>
													</pims:security>
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													<pims:security screen="Pims.Tendering.SearchTender.OpenProposalGridAction" action="create">
														<t:commandLink 
															action="#{pages$constructionTenderSearch.onOpenProposal}"
															rendered="#{dataItem.showOpenProposal}" >
														<h:graphicImage title="#{msg['tenderManagement.actions.openProsposal']}" url="../resources/images/app_icons/openProposal_tender.png" />&nbsp;
														</t:commandLink>
													</pims:security>
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													<pims:security screen="Pims.Tendering.SearchTender.PurchaseGridAction" action="create">
														<t:commandLink 
															action="#{pages$constructionTenderSearch.onPurchaseTender}"
															rendered="#{dataItem.showPurchaseTender}" >
														<h:graphicImage title="#{msg['tenderManagement.actions.purchaseTender']}" url="../resources/images/app_icons/purchase_tender.png" />&nbsp;
														</t:commandLink>
													</pims:security>
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													<pims:security screen="Pims.Tendering.SearchTender.ManageTenderGridAction" action="create">
														<t:commandLink 
															action="#{pages$constructionTenderSearch.onManage}"
															rendered="#{dataItem.showManage}" >
														<h:graphicImage title="#{msg['commons.manage']}" url="../resources/images/app_icons/manage_tender.png" />&nbsp;
														</t:commandLink>
													</pims:security>
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													<pims:security screen="Pims.Tendering.SearchTender.DeleteTenderGridAction" action="create">
														<t:commandLink 
															onclick="if (!confirm('#{msg['extendApplication.search.confirm.delete']}')) return"
															action="#{pages$constructionTenderSearch.onDelete}"
															rendered="#{dataItem.showDelete}" >
														<h:graphicImage id="deleteAppIcon" title="#{msg['commons.delete']}" url="../resources/images/app_icons/delete_tender.png" />&nbsp;
														</t:commandLink>
													</pims:security>
												</t:column>
												<pims:security screen="Pims.Tendering.SearchTender.SelectTenderGridColumn" action="create">
													<t:column id="col8" sortable="true" rendered="#{pages$constructionTenderSearch.isViewModePopUp}">
														<f:facet name="header">
															<t:outputText value="#{msg['commons.select']}" />
														</f:facet>
														<h:commandLink id="selectLink" onclick="javascript:sendToParent('#{dataItem.tenderId}','#{dataItem.tenderNumber}','#{dataItem.tenderDescription}');">
															<h:graphicImage id="selectIcon" title="#{msg['commons.select']}" url="../resources/images/select-icon.gif" />&nbsp;
														</h:commandLink>
													</t:column>
												</pims:security>
											</t:dataTable>
										</div>
										<div class="contentDivFooter" style="width: 100%">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{pages$constructionTenderSearch.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">
																<CENTER>
																	<t:dataScroller id="scroller" for="dt1"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$constructionTenderSearch.paginatorMaxPages}"
																		immediate="false" paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		pageIndexVar="pageNumber" styleClass="SCH_SCROLLER"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblF" />
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR" />
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF" />
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL" />
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.pageNumber}" />
																					</td>
																				</tr>
																			</table>
																		</t:div>
																	</t:dataScroller>
																</center>
															</td>
														</tr>
													</table>
												</div>
                                           </div>
                                        </div>
									</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!pages$constructionTenderSearch.isViewModePopUp}">
							<td colspan="2">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</tr>
								</table>
							</td>
						</c:when>
					</c:choose>
				</tr>
			</table>
		</div>	 
	</body>
</html>
</f:view>