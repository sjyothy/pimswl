<t:div id="tabHealthAspectChronic" style="width:100%;">
	<t:panelGrid id="tabHealthAspectChronicGrid" cellpadding="3px"
		width="100%" cellspacing="2px" columns="1"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.HealthAspect.ChronicDisease']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">
		<h:commandButton id="btnAddHealthAspectChronicb" styleClass="BUTTON"
			type="button" value="#{msg['commons.Add']}"
			onclick="javaScript:openFamilyVillageHealthAspectChronicPopup('#{pages$tabFamilyVillageHealthAspect.personId}');">
		</h:commandButton>
	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableHealthAspectsChronic" rows="5" width="100%"
			value="#{pages$tabFamilyVillageHealthAspect.dataListHealthAspectsChronic}"
			binding="#{pages$tabFamilyVillageHealthAspect.dataTableHealthAspectsChronic}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="HealthAspectChronicAspectsCreatedBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="HealthAspectChronicAspectsCreatedByOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageHealthAspect.englishLocale? 
																				dataItem.createdByEn:
																				dataItem.createdByAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="HealthAspectChronicAspectsCreatedOn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageHealthAspect.dateFormat}"
						timeZone="#{pages$tabFamilyVillageHealthAspect.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="HealthAspectChronicAspectsdiseaseTypeEn"
				sortable="true">
				<f:facet name="header">
					<t:outputText id="HealthAspectChronicAspectsdiseaseTypeEnOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.HealthAspect.ChronicDisease']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageHealthAspect.englishLocale? 
																				dataItem.diseaseTypeEn:
																				dataItem.diseaseTypeAr}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>

			<t:column id="HealthAspectsFromDate" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.fromDate']}" />
				</f:facet>
				<h:outputText value="#{dataItem.fromDate}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageHealthAspect.dateFormat}"
						timeZone="#{pages$tabFamilyVillageHealthAspect.timeZone}" />
				</h:outputText>
			</t:column>

			<t:column id="HealthAspectChronicAspectsDetails" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.description']}" />
				</f:facet>
				<h:outputText value="#{dataItem.description}">

				</h:outputText>
			</t:column>


		</t:dataTable>
	</t:div>
	<t:div id="HealthAspectChronicAspectsDivScroller"
		styleClass="contentDivFooter" style="width:99.1%">


		<t:panelGrid id="HealthAspectChronicAspectsRecNumTable" columns="2"
			cellpadding="0" cellspacing="0" width="100%"
			columnClasses="RECORD_NUM_TD,BUTTON_TD">
			<t:div id="HealthAspectChronicAspectsRecNumDiv"
				styleClass="RECORD_NUM_BG">
				<t:panelGrid id="HealthAspectChronicAspectsRecNumTbl" columns="3"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
					cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
					<h:outputText value="#{msg['commons.recordsFound']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$tabFamilyVillageHealthAspect.recordSizeHealthAspectsChronic}" />
				</t:panelGrid>

			</t:div>

			<CENTER>
				<t:dataScroller id="HealthAspectChronicAspectsScroller"
					for="dataTableHealthAspectsChronic" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$tabFamilyVillageHealthAspect.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="HealthAspectChronicAspectslblFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="HealthAspectChronicAspectslblFRRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="HealthAspectChronicAspectslblFFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="HealthAspectChronicAspectslblLRequestHistory"></t:graphicImage>
					</f:facet>

					<t:div id="HealthAspectChronicAspectsPageNumDiv"
						styleClass="PAGE_NUM_BG">
						<t:panelGrid id="HealthAspectChronicAspectsPageNumTable"
							styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
							cellspacing="0">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:panelGrid>

					</t:div>
				</t:dataScroller>
			</CENTER>

		</t:panelGrid>
	</t:div>

	<t:panelGrid id="tabHealthAspectAcuteGrid" cellpadding="3px"
		width="100%" cellspacing="2px" columns="1"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.HealthAspect.AcuteDisease']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">
		<h:commandButton id="btnAddHealthAspectAcuteb" styleClass="BUTTON"
			type="button" value="#{msg['commons.Add']}"
			onclick="javaScript:openFamilyVillageHealthAspectAcutePopup('#{pages$tabFamilyVillageHealthAspect.personId}');">
		</h:commandButton>
	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableHealthAspectsAcute" rows="5" width="100%"
			value="#{pages$tabFamilyVillageHealthAspect.dataListHealthAspectsAcute}"
			binding="#{pages$tabFamilyVillageHealthAspect.dataTableHealthAspectsAcute}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="HealthAspectAcuteAspectsCreatedBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="HealthAspectAcuteAspectsCreatedByOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageHealthAspect.englishLocale? 
																				dataItem.createdByEn:
																				dataItem.createdByAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="HealthAspectAcuteAspectsCreatedOn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageHealthAspect.dateFormat}"
						timeZone="#{pages$tabFamilyVillageHealthAspect.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="HealthAspectAcuteAspectsdiseaseTypeEn" sortable="true">
				<f:facet name="header">
					<t:outputText id="HealthAspectAcuteAspectsdiseaseTypeEnOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.HealthAspect.AcuteDisease']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageHealthAspect.englishLocale? 
																				dataItem.diseaseTypeEn:
																				dataItem.diseaseTypeAr}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>

			<t:column id="HealthAspectsAcuteFromDate" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.fromDate']}" />
				</f:facet>
				<h:outputText value="#{dataItem.fromDate}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageHealthAspect.dateFormat}"
						timeZone="#{pages$tabFamilyVillageHealthAspect.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="HealthAspectAcuteToDate" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.toDate']}" />
				</f:facet>
				<h:outputText value="#{dataItem.toDate}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageHealthAspect.dateFormat}"
						timeZone="#{pages$tabFamilyVillageHealthAspect.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="HealthAspectAcuteAspectsDetails" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.description']}" />
				</f:facet>
				<h:outputText value="#{dataItem.description}">

				</h:outputText>
			</t:column>


		</t:dataTable>
	</t:div>
	<t:div id="HealthAspectAcuteAspectsDivScroller"
		styleClass="contentDivFooter" style="width:99.1%">


		<t:panelGrid id="HealthAspectAcuteAspectsRecNumTable" columns="2"
			cellpadding="0" cellspacing="0" width="100%"
			columnClasses="RECORD_NUM_TD,BUTTON_TD">
			<t:div id="HealthAspectAcuteAspectsRecNumDiv"
				styleClass="RECORD_NUM_BG">
				<t:panelGrid id="HealthAspectAcuteAspectsRecNumTbl" columns="3"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
					cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
					<h:outputText value="#{msg['commons.recordsFound']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$tabFamilyVillageHealthAspect.recordSizeHealthAspectsAcute}" />
				</t:panelGrid>

			</t:div>

			<CENTER>
				<t:dataScroller id="HealthAspectAcuteAspectsScroller"
					for="dataTableHealthAspectsAcute" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$tabFamilyVillageHealthAspect.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="HealthAspectAcuteAspectslblFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="HealthAspectAcuteAspectslblFRRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="HealthAspectAcuteAspectslblFFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="HealthAspectAcuteAspectslblLRequestHistory"></t:graphicImage>
					</f:facet>

					<t:div id="HealthAspectAcuteAspectsPageNumDiv"
						styleClass="PAGE_NUM_BG">
						<t:panelGrid id="HealthAspectAcuteAspectsPageNumTable"
							styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
							cellspacing="0">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:panelGrid>

					</t:div>
				</t:dataScroller>
			</CENTER>

		</t:panelGrid>
	</t:div>


	<t:panelGrid id="tabHealthAspectMedicalHistoryGrid" cellpadding="3px"
		width="100%" cellspacing="2px" columns="1"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.HealthAspect.MedicalHistory']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">
		<h:commandButton id="btnAddHealthAspectMedicalHistoryb"
			styleClass="BUTTON" type="button" value="#{msg['commons.Add']}"
			onclick="javaScript:openFamilyVillageMedicalHistoryPopup('#{pages$tabFamilyVillageHealthAspect.personId}');">
		</h:commandButton>
	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableMedicalHistory" rows="5" width="100%"
			value="#{pages$tabFamilyVillageHealthAspect.dataListMedicalHistory}"
			binding="#{pages$tabFamilyVillageHealthAspect.dataTableMedicalHistory}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="HealthAspectMedicalHistoryAspectsCreatedBy"
				sortable="true">
				<f:facet name="header">
					<t:outputText id="HealthAspectMedicalHistoryAspectsCreatedByOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageHealthAspect.englishLocale? 
																				dataItem.createdByEn:
																				dataItem.createdByAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="HealthAspectMedicalHistoryAspectsCreatedOn"
				sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageHealthAspect.dateFormat}"
						timeZone="#{pages$tabFamilyVillageHealthAspect.timeZone}" />
				</h:outputText>
			</t:column>

			<t:column id="HealthAspectMedicalHistoryAspectsdiseaseTypeEn"
				sortable="true">
				<f:facet name="header">
					<t:outputText id="HealthAspectMedicalHistoryAspectsTypeEnOT"
						value="#{msg['commons.typeCol']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageHealthAspect.englishLocale? 
																				dataItem.medicalHistoryTypeEn:
																				dataItem.medicalHistoryTypeAr}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>

			<t:column id="HealthAspectMedicalHistoryAnaesthesiaGiven"
				sortable="true">
				<f:facet name="header">
					<t:outputText id="HealthAspectMedicalHistoryAnaesthesiaGivenEnOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.HealthAspect.MedicalHistory.AnaesthesiaGiven']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" styleClass="A_LEFT"
					value="#{dataItem.anaesthesiaGiven==1?msg['commons.yes']:msg['commons.no']}" />
			</t:column>

			<t:column id="HealthAspectMedicalHistoryFromDate" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.fromDate']}" />
				</f:facet>
				<h:outputText value="#{dataItem.fromDate}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageHealthAspect.dateFormat}"
						timeZone="#{pages$tabFamilyVillageHealthAspect.timeZone}" />
				</h:outputText>
			</t:column>

			<t:column id="HealthAspectMedicalHistoryToDate" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.toDate']}" />
				</f:facet>
				<h:outputText value="#{dataItem.toDate}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageHealthAspect.dateFormat}"
						timeZone="#{pages$tabFamilyVillageHealthAspect.timeZone}" />
				</h:outputText>
			</t:column>

			<t:column id="HealthAspectMedicalHistoryAspectsDetails"
				sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.description']}" />
				</f:facet>
				<h:outputText value="#{dataItem.description}">

				</h:outputText>
			</t:column>


		</t:dataTable>
	</t:div>
	<t:div id="HealthAspectMedicalHistoryAspectsDivScroller"
		styleClass="contentDivFooter" style="width:99.1%">


		<t:panelGrid id="HealthAspectMedicalHistoryAspectsRecNumTable"
			columns="2" cellpadding="0" cellspacing="0" width="100%"
			columnClasses="RECORD_NUM_TD,BUTTON_TD">
			<t:div id="HealthAspectMedicalHistoryAspectsRecNumDiv"
				styleClass="RECORD_NUM_BG">
				<t:panelGrid id="HealthAspectMedicalHistoryAspectsRecNumTbl"
					columns="3"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
					cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
					<h:outputText value="#{msg['commons.recordsFound']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$tabFamilyVillageHealthAspect.recordSizeMedicalHistory}" />
				</t:panelGrid>

			</t:div>

			<CENTER>
				<t:dataScroller id="HealthAspectMedicalHistoryAspectsScroller"
					for="dataTableMedicalHistory" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$tabFamilyVillageHealthAspect.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="HealthAspectMedicalHistoryAspectslblFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="HealthAspectMedicalHistoryAspectslblFRRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="HealthAspectMedicalHistoryAspectslblFFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="HealthAspectMedicalHistoryAspectslblLRequestHistory"></t:graphicImage>
					</f:facet>

					<t:div id="HealthAspectMedicalHistoryAspectsPageNumDiv"
						styleClass="PAGE_NUM_BG">
						<t:panelGrid id="HealthAspectMedicalHistoryAspectsPageNumTable"
							styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
							cellspacing="0">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:panelGrid>

					</t:div>
				</t:dataScroller>
			</CENTER>

		</t:panelGrid>
	</t:div>

</t:div>
