	<h:panelGrid columns="2" columnClasses="presidentDiscussionColumn"
			rendered="#{pages$auctionDetails.auctionUnitsTabActive}">	
			<t:dataTable id="dt1"
				value="#{pages$auctionDetails.auctionUnitDataList}"
				binding="#{pages$auctionDetails.dataTable}"
				rows="7"
				preserveDataModel="true" preserveSort="false" var="dataItem"
				rowClasses="row1,row2" rules="all" renderedIfEmpty="false" width="100%">
			
				<t:column id="propertyNumberCol" sortable="true" >
					<f:facet name="header">
						<t:outputText value="#{msg['property.number']}" />
					</f:facet>
					<t:outputText value="#{dataItem.propertyNumber}" />
				</t:column>
			<t:column id="propertyCommercialNameCol" sortable="true" >
				<f:facet name="header">
					<t:outputText value="#{msg['property.commercialName']}" />
				</f:facet>
				<t:outputText value="#{dataItem.propertyCommercialName}" />
			</t:column>
			<t:column id="unitNumberCol" sortable="true" >
				<f:facet name="header">
					<t:outputText value="#{msg['unit.number']}" />
				</f:facet>
				<t:outputText value="#{dataItem.unitNumber}" />
			</t:column>
			<t:column id="unitTypeEnCol" sortable="true" rendered="#{pages$auctionDetails.isEnglishLocale}">
				<f:facet name="header">
					<t:outputText value="#{msg['unit.type']}" />
				</f:facet>
				<t:outputText value="#{dataItem.unitTypeEn}" />
			</t:column>
			<t:column id="unitTypeArCol" sortable="true" rendered="#{pages$auctionDetails.isArabicLocale}">
				<f:facet name="header">
					<t:outputText value="#{msg['unit.type']}" />
				</f:facet>
				<t:outputText value="#{dataItem.unitTypeAr}" />
			</t:column>
			<t:column id="unitUsageEnCol" sortable="true" rendered="#{pages$auctionDetails.isEnglishLocale}">
				<f:facet name="header">
					<t:outputText value="#{msg['inquiry.unitUsage']}" />
				</f:facet>
				<t:outputText value="#{dataItem.unitUsageTypeEn}" />
			</t:column>
			<t:column id="unitUsageTypeArCol" sortable="true" rendered="#{pages$auctionDetails.isArabicLocale}">
				<f:facet name="header">
					<t:outputText value="#{msg['inquiry.unitUsage']}" />
				</f:facet>
				<t:outputText value="#{dataItem.unitUsageTypeAr}" />
			</t:column>
			<t:column id="rentValueCol" sortable="true" >
				<f:facet name="header">
					<t:outputText value="#{msg['unit.rentValue']}" />
				</f:facet>
				<t:outputText value="#{dataItem.rentValue}" />
			</t:column>
			<t:column id="openingPriceCol" sortable="true" >
				<f:facet name="header">
					<t:outputText value="#{msg['auctionUnit.openingPrice']}" />
				</f:facet>
				<t:outputText value="#{dataItem.openingPrice}" />
			</t:column>
			<t:column id="depositAmountCol" sortable="true" >
				<f:facet name="header">
					<t:outputText value="#{msg['unit.depositAmount']}" />
				</f:facet>
				<t:outputText value="#{dataItem.depositAmount}" />
			</t:column>
			<t:column id="editCol" sortable="false" rendered="#{!pages$auctionDetails.auctionViewMode}">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.edit']}" />
			</f:facet>
				<t:commandLink actionListener="#{pages$auctionDetails.editAuctionUnit}"
								value="#{msg['commons.edit']}"
								/>
			</t:column>
		</t:dataTable>					

		<t:dataScroller id="scroller" for="dt1" paginator="true" fastStep="1"
			paginatorMaxPages="2" immediate="true" styleClass="scroller"
			paginatorTableClass="paginator" renderFacetsIfSinglePage="false"
			paginatorTableStyle="grid_paginator" layout="singleTable"
			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
			paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">
	
			<f:facet name="first">
				<t:outputLabel value="First" id="lblF" />
			</f:facet>
			<f:facet name="last">
				<t:outputLabel value="Last" id="lblL" />
			</f:facet>
			<f:facet name="fastforward">
				<t:outputLabel value="FFwd" id="lblFF" />
			</f:facet>
			<f:facet name="fastrewind">
				<t:outputLabel value="FRev" id="lblFR" />
			</f:facet>
		</t:dataScroller>
	</h:panelGrid>