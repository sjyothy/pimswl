<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript"> 
function calculateMonthlyIncome(control){
removeNonNumeric(control);
var nodes = document.getElementById('detailsFrm:lastMonthlyIncomeTableId').getElementsByTagName('input');
	document.getElementById('detailsFrm:lastMonthlyIncomeTableId:0:totalMonthlyIncomeId').value ="";
	var total = 0 ;
	for(index = 0; index < nodes.length ; index++){
		if(nodes[index].value != null && nodes[index].value.length > 0 && isNumeric(nodes[index].value)){
			total = total + parseFloat(nodes[index].value);
		}
	}
	document.getElementById('detailsFrm:lastMonthlyIncomeTableId:0:totalMonthlyIncomeId').value = total;
}

function calculateAnnualIncome(control){
removeNonNumeric(control);
var nodes = document.getElementById('detailsFrm:lastAnnualIncomeTableId').getElementsByTagName('input');
document.getElementById('detailsFrm:lastAnnualIncomeTableId:0:totalAnnualIncomeId').value ="";
	var total = 0 ;
	for(index = 0; index < nodes.length ; index++){
		if(nodes[index].value != null && nodes[index].value.length > 0 && isNumeric(nodes[index].value)){
			total = total + parseFloat(nodes[index].value);
		}
	}
	document.getElementById('detailsFrm:lastAnnualIncomeTableId:0:totalAnnualIncomeId').value = total;
}

function calculateMonthlyExpense(control){
removeNonNumeric(control);
document.getElementById('detailsFrm:lastMonthlyExpenseTableId:0:totalMonthlyExpenseId').value ="";
	var nodes = document.getElementById('detailsFrm:lastMonthlyExpenseTableId').getElementsByTagName('input');
	var total = 0 ;
	for(index = 0; index < nodes.length ; index++){
		if(nodes[index].value != null && nodes[index].value.length > 0 && isNumeric(nodes[index].value)){
			total = total + parseFloat(nodes[index].value);
		}
	}
	document.getElementById('detailsFrm:lastMonthlyExpenseTableId:0:totalMonthlyExpenseId').value = total;
}


	
	
	function openIncomeThresholdPopUp()
	{
	   var screen_width = 900;
	   var screen_height = 330;
	   var screen_top = screen.top;
	     window.open('incomeThresholdPopup.jsf','_blank','width='+(screen_width)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">



			<!-- Header -->
			<table width="100%" height="100%" cellpadding="0" cellspacing="0"
				border="0">

				<tr width="100%">


					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99.2%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['pageHeading.researchFormBene']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION"
										style="height: 420px; width: 100%; # height: 420px; # width: 100%;">
										<h:form id="detailsFrm" enctype="multipart/form-data"
											style="WIDTH: 94%;">


											<table border="0" class="layoutTable"
												style="margin-left: 15px; margin-right: 15px;">
												<tr>
													<td>
														<h:messages></h:messages>
														<h:outputText
															value="#{pages$ResearchFormBeneficiary.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />
														<h:outputText
															value="#{pages$ResearchFormBeneficiary.successMessages}"
															escape="false" styleClass="INFO_FONT" />
													</td>
												</tr>
											</table>



											<t:panelGrid cellpadding="1px" width="100%" cellspacing="5px"
												columns="4">
												
												
												<h:outputLabel styleClass="LABEL"
													value="#{msg['commons.Name']}:"></h:outputLabel>
												<h:outputText
													value="#{pages$ResearchFormBeneficiary.view.personName}" />

												<h:outputLabel styleClass="LABEL"
													rendered="#{!pages$ResearchFormBeneficiary.contextFamilyVillage
															   }"
													value="#{msg['ResearchFormBenef.incomeCategory']}:"></h:outputLabel>

												<h:panelGroup>

													<h:selectOneMenu id="incomeCate"
														rendered="#{!pages$ResearchFormBeneficiary.contextFamilyVillage
															   }"
														value="#{pages$ResearchFormBeneficiary.view.incomeCategory}">
														<f:selectItem itemValue="-1"
															itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
														<f:selectItems
															value="#{pages$ResearchFormBeneficiary.incomeCategoryList}" />
													</h:selectOneMenu>

													<t:commandLink
														rendered="#{!pages$ResearchFormBeneficiary.contextFamilyVillage
															   }"
														onclick="javaScript:openIncomeThresholdPopUp()">
														<h:graphicImage
															title="#{msg['researchFrom.msg.incomeCategoryThreshold']}"
															style="cursor:hand;"
															url="../resources/images/app_icons/Change-tenant.png" />
													</t:commandLink>
												</h:panelGroup>
												<h:outputLabel styleClass="LABEL"
													rendered="#{!pages$ResearchFormBeneficiary.contextFamilyVillage
															   }"
													value="#{msg['generalAspects.hobby']}:"></h:outputLabel>
												<h:selectOneMenu id="hobbey"
													rendered="#{!pages$ResearchFormBeneficiary.contextFamilyVillage
															   }"
													value="#{pages$ResearchFormBeneficiary.view.hobbyId}">
													<f:selectItem itemValue="-1"
														itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
													<f:selectItems value="#{pages$ApplicationBean.allHobbies}" />
												</h:selectOneMenu>

												<h:outputLabel styleClass="LABEL"
													rendered="#{!pages$ResearchFormBeneficiary.openedForOwner && 
																!pages$ResearchFormBeneficiary.contextFamilyVillage
															   }"
													value="#{msg['generalAspects.orphanCategory']}:"></h:outputLabel>
												<h:selectOneMenu id="orphanCategory"
													rendered="#{!pages$ResearchFormBeneficiary.openedForOwner &&
																	!pages$ResearchFormBeneficiary.contextFamilyVillage
															   }"
													value="#{pages$ResearchFormBeneficiary.view.orphanCategoryId}">
													<f:selectItem itemValue="-1"
														itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
													<f:selectItems
														value="#{pages$ApplicationBean.allOrphanCategories}" />
												</h:selectOneMenu>



												<h:outputLabel styleClass="LABEL"
													rendered="#{!pages$ResearchFormBeneficiary.contextFamilyVillage
															   }"
													value="#{msg['ResearchFormBenef.isStudent']}:"></h:outputLabel>
												<h:selectBooleanCheckbox id="isStudent"
													rendered="#{!pages$ResearchFormBeneficiary.contextFamilyVillage
															   }"
													value="#{pages$ResearchFormBeneficiary.view.student}">
													<f:converter converterId="javax.faces.Boolean" />
												</h:selectBooleanCheckbox>

												<h:outputLabel styleClass="LABEL"
													value="#{msg['ResearchFormBenef.applyToAllBeneficiaries']}:"
													rendered="#{! (empty pages$ResearchFormBeneficiary.view.inhFileId) && pages$ResearchFormBeneficiary.view.inhFileId > 0 && pages$ResearchFormBeneficiary.copyBeneficiaryListSize > 0}"></h:outputLabel>
												<h:selectBooleanCheckbox id="applyToAllBeneficiaries"
													value="#{pages$ResearchFormBeneficiary.view.applyToAllBeneficiaries}"
													rendered="#{! (empty pages$ResearchFormBeneficiary.view.inhFileId) && pages$ResearchFormBeneficiary.view.inhFileId > 0 && pages$ResearchFormBeneficiary.copyBeneficiaryListSize > 0}">
													<f:converter converterId="javax.faces.Boolean" />
												</h:selectBooleanCheckbox>

												<h:outputLabel style="font-weight:normal;"
													rendered="#{! (empty pages$ResearchFormBeneficiary.view.inhFileId) && pages$ResearchFormBeneficiary.view.inhFileId > 0 && pages$ResearchFormBeneficiary.copyBeneficiaryListSize > 0}"
													styleClass="TABLE_LABEL"
													value="#{msg['researchFormBenef.copyToBeneficiary']} :"></h:outputLabel>
												<t:div
													style="overflow-x:hidden;overflow-y:auto;height:50px;background:white;"
													rendered="#{! (empty pages$ResearchFormBeneficiary.view.inhFileId) && pages$ResearchFormBeneficiary.view.inhFileId > 0 && pages$ResearchFormBeneficiary.copyBeneficiaryListSize > 0}">
													<h:selectManyCheckbox id="copyBeneficiary"
														style="border-style: solid; border-color: #a2a2a2; background:white;font-weight:400 "
														layout="pageDirection"
														value="#{pages$ResearchFormBeneficiary.view.copyToBeneficiary}">

														<f:selectItems
															value="#{pages$ResearchFormBeneficiary.copyBeneficiaryList}" />
													</h:selectManyCheckbox>
												</t:div>

											</t:panelGrid>
											<br />
											<t:div styleClass="BUTTON_TD">
												<h:commandButton styleClass="BUTTON" id="btnSaveUp"
													value="#{msg['commons.saveButton']}"
													onclick="if (!confirm('#{msg['confirmMsg.areuSureToSave']}')) return false;"
													action="#{pages$ResearchFormBeneficiary.onSave}">
												</h:commandButton>

												<h:commandButton styleClass="BUTTON" id="btnCloseButton"
													value="#{msg['commons.closeButton']}"
													onclick="window.close();">
												</h:commandButton>
											</t:div>
											<div class="TAB_PANEL_INNER">
												<rich:tabPanel
													binding="#{pages$ResearchFormBeneficiary.tabPanel}"
													style="MIN-HEIGHT: 200px;#MIN-HEIGHT: 200px;width: 100%;margin-left:5px;margin-top:5px;
												        margin-right:5px;margin-bottom:5px;">
													<rich:tab id="accomAspects"
														
														label="#{msg['researchFormBenef.accomodationAspects']}">
														<t:div
															style="min-height:200px;overflow-y:scroll;overflow-x:hidden;width:99%;
																			
																			margin-left: 5px;
																			margin-right: 5px;">
															<t:panelGrid id="TAB_PANEL_INNER_GRID_1" border="0"
																width="80%" cellpadding="1" cellspacing="1">
																<t:panelGroup id="TAB_PANEL_INNER_GROUP_1">

																	<t:panelGrid columns="4" id="TAB_PANEL_INNER_GRID_2"
																		columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2">


																		<h:outputLabel style="font-weight:normal;"
																			styleClass="TABLE_LABEL"
																			value="#{msg['ResearchFormBenef.beneficiaryOfProp']} :"></h:outputLabel>
																		<t:panelGroup colspan="3">
																			<h:selectOneMenu id="benProp"
																				value="#{pages$ResearchFormBeneficiary.view.accomdationAspects.beneficiaryOfPropertyId}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ResearchFormBeneficiary.beneficiaryPropList}" />
																			</h:selectOneMenu>
																		</t:panelGroup>
																		<h:outputLabel style="font-weight:normal;"
																			value="#{msg['ResearchFormBenef.accType']} :"></h:outputLabel>
																		<h:selectOneMenu id="accType"
																			value="#{pages$ResearchFormBeneficiary.view.accomdationAspects.accomodationTypeId}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
																			<f:selectItems
																				value="#{pages$ResearchFormBeneficiary.accTypeList}" />
																		</h:selectOneMenu>

																		<h:outputLabel style="font-weight:normal;"
																			value="#{msg['fieldName.accTypeDesc']} :"></h:outputLabel>
																		<h:inputTextarea
																			onkeyup="javaScript:removeExtraCharacter(this,50)"
																			onkeypress="javaScript:removeExtraCharacter(this,50)"
																			onkeydown="javaScript:removeExtraCharacter(this,50)"
																			onblur="javaScript:removeExtraCharacter(this,50)"
																			onchange="javaScript:removeExtraCharacter(this,50)"
																			id="accDescTxtBox"
																			value="#{pages$ResearchFormBeneficiary.view.accomdationAspects.accomodationTypeDesc}"
																			style="width: 185px;" />


																		<h:outputLabel style="font-weight:normal;"
																			value="#{msg['ResearchFormBenef.accOwnership']} :"></h:outputLabel>
																		<h:selectOneMenu id="accOwnership"
																			value="#{pages$ResearchFormBeneficiary.view.accomdationAspects.accomodationOwnershipId}"
																			disabled="false">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
																			<f:selectItems
																				value="#{pages$ResearchFormBeneficiary.accOwnershipList}" />
																		</h:selectOneMenu>

																		<h:outputLabel style="font-weight:normal;"
																			value="#{msg['fieldName.accOwnDesc']} :"></h:outputLabel>


																		<h:inputTextarea
																			onkeyup="javaScript:removeExtraCharacter(this,50)"
																			onkeypress="javaScript:removeExtraCharacter(this,50)"
																			onkeydown="javaScript:removeExtraCharacter(this,50)"
																			onblur="javaScript:removeExtraCharacter(this,50)"
																			onchange="javaScript:removeExtraCharacter(this,50)"
																			style="width: 185px;" id="accOwnDesc"
																			value="#{pages$ResearchFormBeneficiary.view.accomdationAspects.accomodationOwnershipDesc}" />

																		<h:outputLabel style="font-weight:normal;"
																			value="#{msg['ResearchFormBenef.resType']} :"></h:outputLabel>
																		<h:selectOneMenu id="resType"
																			value="#{pages$ResearchFormBeneficiary.view.accomdationAspects.resTypeId}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
																			<f:selectItems
																				value="#{pages$ResearchFormBeneficiary.resTypeList}" />
																		</h:selectOneMenu>

																		<h:outputLabel style="font-weight:normal;"
																			value="#{msg['fieldName.resTypeDesc']} :" />
																		<h:inputTextarea
																			onkeyup="javaScript:removeExtraCharacter(this,50)"
																			onkeypress="javaScript:removeExtraCharacter(this,50)"
																			onkeydown="javaScript:removeExtraCharacter(this,50)"
																			onblur="javaScript:removeExtraCharacter(this,50)"
																			onchange="javaScript:removeExtraCharacter(this,50)"
																			style="width: 185px;" id="resTypeDesc"
																			value="#{pages$ResearchFormBeneficiary.view.accomdationAspects.resTypeDesc}" />

																		<h:outputLabel style="font-weight:normal;"
																			styleClass="TABLE_LABEL"
																			value="#{msg['ResearchFormBenef.conditionOfHome']} :"></h:outputLabel>
																		<h:selectOneMenu id="homeCondition"
																			value="#{pages$ResearchFormBeneficiary.view.accomdationAspects.conditionOfHomeId}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
																			<f:selectItems
																				value="#{pages$ResearchFormBeneficiary.homeConditionList}" />
																		</h:selectOneMenu>


																		<h:outputLabel style="font-weight:normal;"
																			value="#{msg['fieldName.homecondDesc']} :"></h:outputLabel>
																		<h:inputTextarea
																			onkeyup="javaScript:removeExtraCharacter(this,50)"
																			onkeypress="javaScript:removeExtraCharacter(this,50)"
																			onkeydown="javaScript:removeExtraCharacter(this,50)"
																			onblur="javaScript:removeExtraCharacter(this,50)"
																			onchange="javaScript:removeExtraCharacter(this,50)"
																			style="width: 185px;" id="homecondDesc"
																			value="#{pages$ResearchFormBeneficiary.view.accomdationAspects.conditionOfHomeDesc}" />


																		<h:outputLabel style="font-weight:normal;"
																			styleClass="TABLE_LABEL"
																			value="#{msg['ResearchFormBenef.reasonForRentingFlat']} :"></h:outputLabel>
																		<h:selectOneMenu id="reasonForRenting"
																			value="#{pages$ResearchFormBeneficiary.view.accomdationAspects.reasonForRentId}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
																			<f:selectItems
																				value="#{pages$ResearchFormBeneficiary.rentingReasonList}" />
																		</h:selectOneMenu>


																		<h:outputLabel style="font-weight:normal;"
																			value="#{msg['fieldName.rentingReasonDesc']} :"></h:outputLabel>
																		<h:inputTextarea
																			onkeyup="javaScript:removeExtraCharacter(this,50)"
																			onkeypress="javaScript:removeExtraCharacter(this,50)"
																			onkeydown="javaScript:removeExtraCharacter(this,50)"
																			onblur="javaScript:removeExtraCharacter(this,50)"
																			onchange="javaScript:removeExtraCharacter(this,50)"
																			style="width: 185px;" id="reasonForRentDesc"
																			value="#{pages$ResearchFormBeneficiary.view.accomdationAspects.reasonForRentDesc}" />
																		<%-- 

																	<h:outputLabel style="font-weight:normal;"
																		styleClass="TABLE_LABEL"
																		value="#{msg['ResearchFormBenef.typeOfHome']} :"></h:outputLabel>
																	<t:div 
																		style="overflow-y:auto;height: 100px; overflow-x: hidden; border-width: 1px; border-style: solid; border-color: #a2a2a2;">
																		<h:panelGrid
																			style="border-width: 1px; border-style: solid; border-color: #a2a2a2; background: white; overflow-x: auto; overflow-y: scroll; width: 80%; height: 60px;font-weight:400 ">
																			<h:selectManyCheckbox id="typeOfHome"
																				layout="pageDirection"
																				value="#{pages$ResearchFormBeneficiary.view.accomdationAspects.typeOfHome}">

																				<f:selectItems
																					value="#{pages$ResearchFormBeneficiary.homeTypeList}" />
																			</h:selectManyCheckbox>
																		</h:panelGrid>
																	</t:div>
																	<h:outputLabel style="font-weight:normal;"
																		value="#{msg['fieldName.homeTypeDesc']} :"></h:outputLabel>
																	<h:inputTextarea
																		onkeyup="javaScript:removeExtraCharacter(this,50)"
																		onkeypress="javaScript:removeExtraCharacter(this,50)"
																		onkeydown="javaScript:removeExtraCharacter(this,50)"
																		onblur="javaScript:removeExtraCharacter(this,50)"
																		onchange="javaScript:removeExtraCharacter(this,50)"
																		style="width: 185px;" id="typeOfHomeDesc"
																		value="#{pages$ResearchFormBeneficiary.view.accomdationAspects.typeOfHomeDesc}" />
--%>
																	</t:panelGrid>
																</t:panelGroup>

															</t:panelGrid>
															<t:div styleClass="contentDiv"
																style="width: 95%;overflow-y:hidden;overflow-x:hidden;">
																<t:dataTable id="historyGrid"
																	binding="#{pages$ResearchFormBeneficiary.accomodationAspectsDT}"
																	value="#{pages$ResearchFormBeneficiary.view.accomdationAspectsList}"
																	width="100%"
																	rows="#{pages$ResearchFormBeneficiary.paginatorRows}"
																	preserveDataModel="false" preserveSort="false"
																	var="dataItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true">
																	<t:column id="date" width="10%" sortable="false"
																		style="white-space: normal;">

																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.date']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.createdOn}"
																			style="white-space: normal;" styleClass="A_LEFT" />

																	</t:column>
																	<t:column id="beneficiaryOfProperty" width="10%"
																		sortable="false" style="white-space: normal;">

																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['ResearchFormBenef.beneficiaryOfProp']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$ResearchFormBeneficiary.englishLocale?dataItem.beneificiaryOfPropertyGridDispEn:dataItem.beneificiaryOfPropertyGridDispAr}"
																			style="white-space: normal;" styleClass="A_LEFT" />
																	</t:column>
																	<%--
																<t:column id="homeType" width="10%" defaultSorted="true"
																	style="white-space: normal;">

																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['ResearchFormBenef.typeOfHome']}" />

																	</f:facet>
																	<t:outputText
																		value="#{pages$ResearchFormBeneficiary.englishLocale?dataItem.typeOfHomeGridDispEn:dataItem.typeOfHomeGridDispAr}"
																		style="white-space: normal;" />

																</t:column>
--%>
																	<t:column id="accountType" width="10%"
																		defaultSorted="true" style="white-space: normal;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['ResearchFormBenef.conditionOfHome']} " />
																		</f:facet>
																		<t:outputText
																			value="#{pages$ResearchFormBeneficiary.englishLocale?dataItem.conditionOfHomeGridDispEn:dataItem.conditionOfHomeGridDispAr}"
																			style="white-space: normal;" styleClass="A_LEFT" />
																	</t:column>

																	<t:column id="accountOwnership" width="10%"
																		sortable="false" style="white-space: normal;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['ResearchFormBenef.accOwnership']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$ResearchFormBeneficiary.englishLocale?dataItem.accomodationOwnershipGridDispEn:dataItem.accomodationOwnershipGridDispAr}"
																			style="white-space: normal;" styleClass="A_LEFT" />
																	</t:column>


																	<t:column id="accomodationType" width="10%"
																		sortable="false" style="white-space: normal;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['ResearchFormBenef.accType']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$ResearchFormBeneficiary.englishLocale?dataItem.accomodationTypeGridDispEn:dataItem.accomodationTypeGridDispAr}"
																			style="white-space: normal;" styleClass="A_LEFT" />
																	</t:column>

																	<t:column id="resTypeDesc" width="10%" sortable="false"
																		style="white-space: normal;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['ResearchFormBenef.resType']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$ResearchFormBeneficiary.englishLocale?dataItem.resTypeGridDispEn:dataItem.resTypeGridDispAr}"
																			style="white-space: normal;" styleClass="A_LEFT" />
																	</t:column>

																	<t:column id="rentingReason" width="10%"
																		sortable="false" style="white-space: normal;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['ResearchFormBenef.reasonForRentingFlat']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$ResearchFormBeneficiary.englishLocale?dataItem.rentingReasonGridDispEn:dataItem.rentingReasonGridDispAr}"
																			style="white-space: normal;" styleClass="A_LEFT" />
																	</t:column>
																	<t:column id="deleteAction" sortable="false"
																		style="white-space: normal;">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<t:commandLink
																			onclick="if (!confirm('#{msg['migrationProject.confirmMsg.sureToDelete']}')) return false;"
																			action="#{pages$ResearchFormBeneficiary.deleteAccomodationAspects}">
																			<h:graphicImage id="deleteGuardian"
																				title="#{msg['commons.delete']}"
																				style="cursor:hand;"
																				url="../resources/images/delete.gif" />&nbsp;
																	</t:commandLink>
																	</t:column>

																</t:dataTable>
															</t:div>

														</t:div>
													</rich:tab>
													<rich:tab id="tabEducationalAspects"
														title="#{msg['educationAspects.tabName']}"
														label="#{msg['educationAspects.tabName']}">
														<%@include file="educationAspectsTab.jsp"%>
													</rich:tab>
													<rich:tab id="tabHealthAspects"
														title="#{msg['healthAspects.tabName']}"
														label="#{msg['healthAspects.tabName']}">
														<%@include file="healthAspectsTab.jsp"%>
													</rich:tab>


												</rich:tabPanel>
											</div>

											<t:div styleClass="BUTTON_TD">
												<h:commandButton styleClass="BUTTON" id="btnSave"
													value="#{msg['commons.saveButton']}"
													onclick="if (!confirm('#{msg['confirmMsg.areuSureToSave']}')) return false;"
													action="#{pages$ResearchFormBeneficiary.onSave}">
												</h:commandButton>

												<h:commandButton styleClass="BUTTON"
													value="#{msg['commons.closeButton']}"
													onclick="window.close();">
												</h:commandButton>
											</t:div>
										</h:form>
									</div>

								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>

		</body>
	</html>
</f:view>