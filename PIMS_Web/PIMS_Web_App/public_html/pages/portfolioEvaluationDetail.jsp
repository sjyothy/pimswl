<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
  <h:panelGrid rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}" styleClass="BUTTON_TD" width="100%" columns="4">
											<h:panelGroup rendered="false">	
												<h:outputLabel styleClass="mandatory" value="*"/>	
												<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.Evaluation.Type']}:" />
											</h:panelGroup>
											<h:panelGroup rendered="false">	
												<h:selectOneMenu id ="evaluationType" value="#{pages$portfolioManage.portfolioEvaluationView.strEvaluationTypeId}" onchange="evalucationTypeChange();" style="width: 192px;">
													<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
													<f:selectItems value="#{pages$ApplicationBean.portfolioEvaluationType}" />																	
												</h:selectOneMenu>
											    <a4j:commandLink id="lnkEvaluationTypeChange" action="#{pages$portfolioManage.onEvaluationTypeChanged}" reRender="evaluationValue,unitValue,numberOfUnit" />	
											</h:panelGroup>
											
											<h:panelGroup>	
													<h:outputLabel styleClass="mandatory" value="*"/>	
													<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.Evaluation.Value']}:"></h:outputLabel>
											</h:panelGroup>
											<h:inputText id="evaluationValue" value="#{pages$portfolioManage.portfolioEvaluationView.strEvaluationValue}" styleClass="#{pages$portfolioManage.readOnlyEvaluationValue}" disabled="#{pages$portfolioManage.disableEvaluationValue}" style="width: 186px;" maxlength="15" />
						                        			   
											<h:panelGroup>	
													<h:outputLabel styleClass="mandatory" value="*"/>	
													<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.Evaluation.Unit.Value']}:"></h:outputLabel>
											</h:panelGroup>
											<h:panelGroup>
											 <h:inputText id="unitValue"
											 styleClass="#{pages$portfolioManage.readOnlyUnitValue}" disabled="#{pages$portfolioManage.disableUnitValue}" 
											 value="#{pages$portfolioManage.portfolioEvaluationView.strUnitValue}" style="width: 186px;" maxlength="10" />
											 <a4j:commandLink id="calcEvaluationValue" action="#{pages$portfolioManage.onCalcEvaluationValue}" reRender="evaluationValue" />
											</h:panelGroup>
											
											<h:panelGroup>	
													<h:outputLabel styleClass="mandatory" value="*"/>	
													<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.Evaluation.Number.of.unit']}:"></h:outputLabel>
											</h:panelGroup>
											<h:panelGroup>
											<h:inputText id="numberOfUnit" value="#{pages$portfolioManage.portfolioEvaluationView.strNumberOfUnit}"
											styleClass="#{pages$portfolioManage.readOnlyUnitValue}" disabled="#{pages$portfolioManage.disableUnitValue}" 
											style="width: 186px;" maxlength="10" />
											<a4j:commandLink id="calcEvaluationValue1" action="#{pages$portfolioManage.onCalcEvaluationValue}" reRender="evaluationValue" />
											</h:panelGroup>
											<h:panelGroup>	
												<h:outputLabel styleClass="mandatory" value="*"/>	
												<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.Evaluation.Quarter']}:" />
											</h:panelGroup>
											<h:panelGroup>	
												<h:selectOneMenu id ="evaluationQuarter"  value="#{pages$portfolioManage.portfolioEvaluationView.strEvaluationQuarterId}" style="width: 192px;">
													<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
													<f:selectItems value="#{pages$ApplicationBean.portfolioEvaluationQuarter}" />																	
												</h:selectOneMenu>
											</h:panelGroup>
											
											<h:panelGroup>	
													<h:outputLabel styleClass="mandatory" value="*"/>	
													<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.Evaluation.Year']}:"></h:outputLabel>
											</h:panelGroup>
											<h:inputText id="evaluationYear" value="#{pages$portfolioManage.portfolioEvaluationView.strEvaluationYear}" style="width: 186px;" maxlength="5" />
											
												<h:outputText value=""></h:outputText>
												<h:outputText value=""></h:outputText>
												<h:outputText value=""></h:outputText>
											<h:commandButton styleClass="BUTTON"
															value="#{msg['commons.Add']}"
															action="#{pages$portfolioManage.onEvaluationAdd}"
															style="width: 120px;" />
											
										</h:panelGrid>
										
										
										<t:div styleClass="contentDiv" style="width:99%">																
	                                       <t:dataTable id="evaluationDataTable"  
												rows="#{pages$portfolioManage.paginatorRows}"
												value="#{pages$portfolioManage.portfolioEvaluationViewList}"
												binding="#{pages$portfolioManage.evaluationDataTable}"																	
												preserveDataModel="false" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
												
												<t:column width="30%" sortable="true" rendered="false">
													<f:facet name="header">
														<t:outputText value="#{msg['portfolio.manage.tab.portfolio.Evaluation.Type']}" />
													</f:facet>
													<t:outputText value="#{pages$portfolioManage.isEnglishLocale ? dataItem.evaluationTypeEn : dataItem.evaluationTypeAr}" rendered="#{dataItem.isDeleted == 0}" />
												</t:column>
																													
												<t:column width="30%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['portfolio.manage.tab.portfolio.Evaluation.Value']}" />
													</f:facet>
													<t:outputText value="#{dataItem.evaluationValue}" rendered="#{dataItem.isDeleted == 0}" />																		
												</t:column>
												
												<t:column width="30%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['portfolio.manage.tab.portfolio.Evaluation.Unit.Value']}" />
													</f:facet>
													<t:outputText value="#{dataItem.unitValue}" rendered="#{dataItem.isDeleted == 0}" />																		
												</t:column>
												
												<t:column width="30%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['portfolio.manage.tab.portfolio.Evaluation.Number.of.unit']}" />
													</f:facet>
													<t:outputText value="#{dataItem.numberOfUnit}" rendered="#{dataItem.isDeleted == 0}" />																		
												</t:column>
												
												<t:column width="30%" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['portfolio.manage.tab.portfolio.Evaluation.Quarter']}" />
													</f:facet>
													<t:outputText value="#{pages$portfolioManage.isEnglishLocale ? dataItem.evaluationQuarterEn : dataItem.evaluationQuarterAr}" rendered="#{dataItem.isDeleted == 0}" />
												</t:column>
												
												<t:column width="30%" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['portfolio.manage.tab.portfolio.Evaluation.Year']}" />
													</f:facet>
													<t:outputText value="#{dataItem.evaluationYear}" rendered="#{dataItem.isDeleted == 0}" />
												</t:column>
												
												<t:column width="10%" sortable="false" rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.action']}" />
													</f:facet>
													<a4j:commandLink onclick="if (!confirm('#{msg['evaluation.confirm.delete']}')) return" action="#{pages$portfolioManage.onEvaluationDelete}" rendered="#{dataItem.isDeleted == 0}" reRender="evaluationDataTable,evaluationGridInfo">
														<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />
													</a4j:commandLink>																															
												</t:column>
											  </t:dataTable>										
											</t:div>
									
                                          		<t:div id="evaluationGridInfo" styleClass="contentDivFooter" style="width:100%">
														<t:panelGrid id="rqtkRecNumTable8"  columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
															<t:div id="rqtkRecNumDiv8" styleClass="RECORD_NUM_BG">
																<t:panelGrid id="rqtkRecNumTbl8" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
																	<h:outputText value="#{msg['commons.recordsFound']}"/>
																	<h:outputText value=" : "/>
																	<h:outputText value="#{pages$portfolioManage.evaluationRecordSize}"/>																						
																</t:panelGrid>
															</t:div>
															  
														      <CENTER>
															  	<t:dataScroller id="rqtkscroller8" for="expectedRateDataTable" paginator="true"
																				fastStep="1"  
																				paginatorMaxPages="#{pages$portfolioManage.paginatorMaxPages}" 
																				immediate="false"
																				paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true" 
																			    pageIndexVar="pageNumber"
																			    styleClass="SCH_SCROLLER"
																			    paginatorActiveColumnStyle="font-weight:bold;"
																			    paginatorRenderLinkForActive="false" 
																				paginatorTableStyle="grid_paginator" layout="singleTable"
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">
			
																    <f:facet  name="first">
																		<t:graphicImage url="../#{path.scroller_first}" id="lblFRequestHistory8"></t:graphicImage>
																	</f:facet>
				
																	<f:facet name="fastrewind">
																		<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFRRequestHistory8"></t:graphicImage>
																	</f:facet>
				
																	<f:facet name="fastforward">
																		<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFRequestHistory8"></t:graphicImage>
																	</f:facet>
				
																	<f:facet name="last">
																		<t:graphicImage url="../#{path.scroller_last}" id="lblLRequestHistory8"></t:graphicImage>
																	</f:facet>
				
			                                                      	<t:div id="rqtkPageNumDiv8" styleClass="PAGE_NUM_BG">
																	   	<t:panelGrid id="rqtkPageNumTable8" styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0" >
																	 		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																			<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																		</t:panelGrid>																							
																	</t:div>
																 </t:dataScroller>
															 	</CENTER>																		      
												        </t:panelGrid>
			                          			 </t:div>   