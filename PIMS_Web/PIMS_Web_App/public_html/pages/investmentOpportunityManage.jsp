<%-- 
  - Author: Anil Verani
  - Date: 27/10/2009
  - Copyright Notice:
  - @(#)
  - Description: Used for Adding, Updating and Viewing Investment Opportunity 
  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		function receiveSelectedPortfolio()
	{
		document.getElementById('frm:lnkReceiveSelectedPortfolio').onclick(); 
	}
        
        function openPortfolioSearchPopup()
	{
		var screen_width = 990;
		var screen_height = 475;
        var popup_width = screen_width;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('portfolioSearch.jsf?mode=InvestmentOpportunity','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
	
	function openEvaluationManagePopup()
	{
		var screen_width = 960;
		var screen_height = 375;
        var popup_width = screen_width;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('opportunityEvaluationManage.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
	
	function receiveEvaluation()
	{ 
		document.getElementById('frm:lnkReceiveEvaluation').onclick();
	}
	
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<c:choose>
							<c:when test="${true}">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</c:when>
						</c:choose>
					</tr>

					<tr>
						<c:choose>
							<c:when test="${true}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table class="greyPanelTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel
											value="#{msg['investment.opportunity.heading']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<td width="100%" height="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION">

											<h:form id="frm" style="width:97%"
												enctype="multipart/form-data">
												<table border="0" class="layoutTable">
													<tr>

														<td>
															<h:outputText escape="false" styleClass="ERROR_FONT"
																value="#{pages$investmentOpportunityManage.errorMessages}" />
															<h:outputText escape="false" styleClass="INFO_FONT"
																value="#{pages$investmentOpportunityManage.successMessages}" />
														</td>
													</tr>
												</table>

												<t:div
													rendered="#{pages$investmentOpportunityManage.isApprovalMode}"
													style="width:97%; margin:5px;">
													<t:panelGrid cellpadding="1px" width="100%"
														cellspacing="5px" columns="4">
														<h:outputLabel styleClass="LABEL"
															value="#{msg['commons.approval.date']}: " />
														<rich:calendar inputStyle="width: 186px; height: 18px"
															locale="#{pages$investmentOpportunityManage.locale}"
															popup="true"
															datePattern="#{pages$investmentOpportunityManage.dateFormat}"
															showApplyButton="false" enableManualInput="false"
															cellWidth="24px" cellHeight="22px" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['commons.comments']}: " />
														<t:inputTextarea
															value="#{pages$investmentOpportunityManage.txtRemarks}"
															styleClass="TEXTAREA" rows="3"
															style="width: 200px; height:30px;" />

														<t:div style="height:10px;" />
													</t:panelGrid>
													<table border="0" width="100%">
														<tr>
															<td class="BUTTON_TD">
																<h:commandButton value="#{msg['commons.approve']}"
																	action="#{pages$investmentOpportunityManage.onApprove}"
																	styleClass="BUTTON"></h:commandButton>
																<h:commandButton value="#{msg['commons.reject']}"
																	action="#{pages$investmentOpportunityManage.onReject}"
																	styleClass="BUTTON"></h:commandButton>
															</td>
														</tr>
													</table>
												</t:div>

												<div class="MARGIN" style="width: 98%">

													<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>

													<rich:tabPanel id="tabPanel" style="width:100%;height:235px;"
														headerSpacing="0">

														<!-- Opportunity Details Tab - Start -->
														<rich:tab label="#{msg['opportunity.details']}">
															<t:div style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid styleClass="TAB_DETAIL_SECTION_INNER"
																	cellpadding="1px" width="100%" cellspacing="5px"
																	columns="4">

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory"
																			value="#{msg['commons.mandatory']}" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['opportunity.number']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText
																		value="#{pages$investmentOpportunityManage.investmentOpportunityView.opportunityNumber}"
																		style="width: 186px;" maxlength="20"
																		styleClass="READONLY" readonly="true" />

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory"
																			value="#{msg['commons.mandatory']}" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['opportunity.status']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:selectOneMenu readonly="true" styleClass="READONLY"
																		value="#{pages$investmentOpportunityManage.investmentOpportunityView.opportunityStatusId}"
																		style="width: 192px;">
																		<f:selectItems
																			value="#{pages$ApplicationBean.opportunityStatusList}" />
																	</h:selectOneMenu>

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory"
																			value="#{msg['commons.mandatory']}" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['opportunity.title']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText
																		readonly="#{pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isApprovalMode}"
																		styleClass="#{pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isApprovalMode ? 'READONLY' : ''}"
																		value="#{pages$investmentOpportunityManage.investmentOpportunityView.opportunityTitle}"
																		style="width: 186px;" maxlength="50" />

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory"
																			value="#{msg['commons.mandatory']}" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['opportunity.owner']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText
																		readonly="#{pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isApprovalMode}"
																		styleClass="#{pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isApprovalMode ? 'READONLY' : ''}"
																		value="#{pages$investmentOpportunityManage.investmentOpportunityView.opportunityOwner}"
																		style="width: 186px;" maxlength="50" />

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory"
																			value="#{msg['commons.mandatory']}" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['opportunity.type']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:selectOneMenu
																		readonly="#{pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isApprovalMode}"
																		styleClass="#{pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isApprovalMode ? 'READONLY' : ''}"
																		value="#{pages$investmentOpportunityManage.investmentOpportunityView.opportunityTypeId}"
																		style="width: 192px;">
																		<f:selectItem itemValue=""
																			itemLabel="#{msg['commons.select']}" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.opportunityTypeList}" />
																	</h:selectOneMenu>

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory"
																			value="#{msg['commons.mandatory']}" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['opportunity.subject']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:selectOneMenu
																		readonly="#{pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isApprovalMode}"
																		styleClass="#{pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isApprovalMode ? 'READONLY' : ''}"
																		value="#{pages$investmentOpportunityManage.investmentOpportunityView.opportunitySubjectId}"
																		style="width: 192px;">
																		<f:selectItem itemValue=""
																			itemLabel="#{msg['commons.select']}" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.opportunitySubjectList}" />
																	</h:selectOneMenu>


																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory"
																			value="#{msg['commons.mandatory']}" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['opportunity.date']}:"></h:outputLabel>
																	</h:panelGroup>
																	<rich:calendar
																		disabled="#{pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isApprovalMode}"
																		inputClass="#{pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isApprovalMode ? 'READONLY' : ''}"
																		value="#{pages$investmentOpportunityManage.investmentOpportunityView.opportunityDate}"
																		inputStyle="width: 186px; height: 18px"
																		locale="#{pages$investmentOpportunityManage.locale}"
																		popup="true"
																		datePattern="#{pages$investmentOpportunityManage.dateFormat}"
																		showApplyButton="false" enableManualInput="false"
																		cellWidth="24px" cellHeight="22px"
																		style="width:186px; height:16px" />

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory"
																			value="#{msg['commons.mandatory']}" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['opportunity.description']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputTextarea
																		readonly="#{pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isApprovalMode}"
																		styleClass="#{pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isApprovalMode ? 'READONLY' : ''}"
																		rows="3"
																		value="#{pages$investmentOpportunityManage.investmentOpportunityView.opportunityDescription}"
																		style="width: 186px;" />
																	<h:panelGroup>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.number']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:panelGroup>
																		<h:inputText
																			value="#{pages$investmentOpportunityManage.portfolioNumber}"
																			readonly="true"
																			styleClass="#{pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isApprovalMode ? 'READONLY' : ''}"
																			style="width: 186px;" maxlength="50" />
																		<h:commandLink rendered="#{!(pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isApprovalMode)}"
																		 action="#{pages$investmentOpportunityManage.onSearchPortfolio}">
																			<h:graphicImage id="populateIcon"
																				style="padding-left:5px;"
																				title="#{msg['commons.populate']}"
																				url="../resources/images/magnifier.gif" />
																		</h:commandLink>					
																		<a4j:commandLink id="lnkReceiveSelectedPortfolio"
																		action="#{pages$investmentOpportunityManage.onReceiveSelectedPortfolio}"
																		reRender="tabPanel" />
																	</h:panelGroup>												
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Opportunity Details Tab - End -->

														<!-- Evaluation Details Tab - Start -->
														<rich:tab
															rendered="#{pages$investmentOpportunityManage.isEvaluationMode || pages$investmentOpportunityManage.isViewMode || pages$investmentOpportunityManage.isApprovalMode}"
															label="#{msg['opportunity.evaluation.details']}">
															<h:panelGrid
																rendered="#{pages$investmentOpportunityManage.isEvaluationMode}"
																styleClass="BUTTON_TD" width="100%">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['opportunity.evaluation.add.evaluation']}"
																	action="#{pages$investmentOpportunityManage.onAddEvaluation}"
																	style="width: 100px;" />
																	
																	
																<a4j:commandLink id="lnkReceiveEvaluation"
																	action="#{pages$investmentOpportunityManage.onReceiveEvaluation}"
																	reRender="dataTable,gridInfo" />
															</h:panelGrid>

															<t:div styleClass="contentDiv" style="width: 99%">
																<t:dataTable id="dataTable"
																	rows="#{pages$investmentOpportunityManage.paginatorRows}"
																	value="#{pages$investmentOpportunityManage.opportunityEvaluationViewList}"
																	binding="#{pages$investmentOpportunityManage.dataTable}"
																	preserveDataModel="false" preserveSort="false"
																	var="dataItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true" width="100%">

																	<t:column width="12%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['evaluation.type']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$investmentOpportunityManage.isEnglishLocale ? dataItem.evaluationTypeEn : dataItem.evaluationTypeAr}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="10%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['evaluation.date']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.createdOn}"
																			rendered="#{dataItem.isDeleted == 0}">
																			<f:convertDateTime
																				timeZone="#{pages$investmentOpportunityManage.timeZone}"
																				pattern="#{pages$investmentOpportunityManage.dateFormat}" />
																		</t:outputText>
																	</t:column>

																	<t:column width="10%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['evaluation.details']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.evaluationDetails}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="10%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['evaluation.recommendation']}" />
																		</f:facet>
																		<t:outputText
																			value="#{dataItem.evaluationRecommendations}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column
																		rendered="#{pages$investmentOpportunityManage.isEvaluationMode}"
																		sortable="false" width="15%">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<a4j:commandLink
																			onclick="if (!confirm('#{msg['evaluation.confirm.delete']}')) return"
																			action="#{pages$investmentOpportunityManage.onDeleteEvaluation}"
																			rendered="#{dataItem.isDeleted == 0}"
																			reRender="dataTable,gridInfo">
																			<h:graphicImage title="#{msg['commons.delete']}"
																				url="../resources/images/delete.gif" />&nbsp;
																		</a4j:commandLink>
																	</t:column>

																</t:dataTable>
															</t:div>

															<t:div id="gridInfo" styleClass="contentDivFooter"
																style="width:100%">
																<t:panelGrid id="recNumTable" columns="2"
																	cellpadding="0" cellspacing="0" width="100%"
																	columnClasses="RECORD_NUM_TD,BUTTON_TD">
																	<t:div id="recNumDiv" styleClass="RECORD_NUM_BG">
																		<t:panelGrid id="recNumTbl" columns="3"
																			columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
																			cellpadding="0" cellspacing="0"
																			style="width:182px;#width:150px;">
																			<h:outputText value="#{msg['commons.recordsFound']}" />
																			<h:outputText value=" : " />
																			<h:outputText
																				value="#{pages$investmentOpportunityManage.evaluationRecordSize}" />
																		</t:panelGrid>
																	</t:div>

																	<CENTER>
																		<t:dataScroller id="rscroller" for="dataTable"
																			paginator="true" fastStep="1"
																			paginatorMaxPages="#{pages$investmentOpportunityManage.paginatorMaxPages}"
																			immediate="false" paginatorTableClass="paginator"
																			renderFacetsIfSinglePage="true"
																			pageIndexVar="pageNumber" styleClass="SCH_SCROLLER"
																			paginatorActiveColumnStyle="font-weight:bold;"
																			paginatorRenderLinkForActive="false"
																			paginatorTableStyle="grid_paginator"
																			layout="singleTable"
																			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

																			<f:facet name="first">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblFEval"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastrewind">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFREval"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastforward">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}"
																					id="lblFFEval"></t:graphicImage>
																			</f:facet>

																			<f:facet name="last">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblLEval"></t:graphicImage>
																			</f:facet>

																			<t:div id="pageNumDiv" styleClass="PAGE_NUM_BG">
																				<t:panelGrid id="pageNumTable"
																					styleClass="PAGE_NUM_BG_TABLE" columns="2"
																					cellpadding="0" cellspacing="0">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</t:panelGrid>
																			</t:div>
																		</t:dataScroller>
																	</CENTER>

																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Evaluation Details Tab - End -->

																											<!-- REQUEST HISTORY Tab - End -->
														<!-- Attachments Tab - Start -->
														<rich:tab label="#{msg['contract.attachmentTabHeader']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<!-- Attachments Tab - End -->

														<!-- Comments Tab - Start -->
														<rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														<!-- Comments Tab - End -->
														<rich:tab
															label="#{msg['contract.tabHeading.RequestHistory']}"
															title="#{msg['commons.tab.requestHistory']}"
															action="#{pages$investmentOpportunityManage.tabActivityLog_Click}">
															<%@ include file="../pages/requestTasks.jsp"%>
														</rich:tab>

													</rich:tabPanel>

													<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT" /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID" style="width:100%;" /></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>

													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td colspan="6" class="BUTTON_TD">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$investmentOpportunityManage.onSave}"
																	rendered="#{pages$investmentOpportunityManage.isAddMode || pages$investmentOpportunityManage.isEditMode}">
																</h:commandButton>

																<h:commandButton style="width: 120px;"
																	styleClass="BUTTON"
																	value="#{msg['commons.send.evaluation']}"
																	action="#{pages$investmentOpportunityManage.onSendForEvaluation}"
																	rendered="#{pages$investmentOpportunityManage.isEditMode}">
																</h:commandButton>

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['btn.save.evaluation']}"
																	action="#{pages$investmentOpportunityManage.onSaveEvaluation}"
																	rendered="#{pages$investmentOpportunityManage.isEvaluationMode}"
																	style="width: 100px;">
																</h:commandButton>

																<h:commandButton style="width: 120px;"
																	styleClass="BUTTON"
																	value="#{msg['commons.sendForApproval']}"
																	action="#{pages$investmentOpportunityManage.onSendForApproval}"
																	rendered="#{pages$investmentOpportunityManage.isEvaluationMode}">
																</h:commandButton>

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	action="#{pages$investmentOpportunityManage.onCancel}">
																</h:commandButton>
															</td>
														</tr>
													</table>
												</div>

											</h:form>
										</div>


										</div>
									</td>
								</tr>
							</table>

						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<c:choose>
										<c:when test="${true}">
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</c:when>
									</c:choose>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>
