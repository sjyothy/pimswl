
<script type="text/javascript">
</script>

<t:div style="width:100%" styleClass="TAB_DETAIL_SECTION">


	<t:panelGrid  cellpadding="1px"
		width="100%" cellspacing="5px" columns="4"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:outputLabel styleClass="LABEL"
			value="#{msg['appointmentSearch.lbl.appointmentWith']}:" />
		<h:inputText styleClass="READONLY"
			maxlength="20"  readonly="true"
			value="#{pages$appointmentRequest.appointmentDetailVO.appointmentWith}"
			></h:inputText>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['appointmentSearch.lbl.createdOn']}:" />
		<h:inputText styleClass="READONLY"
			maxlength="20"  readonly="true"
			value="#{pages$appointmentRequest.appointmentDetailVO.createdOn}"
			></h:inputText>	
		<h:outputLabel styleClass="LABEL"
			value="#{msg['appointmentSearch.lbl.appointmentDate']}:" />
			<rich:calendar  id="appointmentDateId" styleClass="READONLY" 
							value="#{pages$appointmentRequest.appointmentDetailVO.appointmentDate}"
							locale="#{pages$appointmentSearch.locale}"
							popup="true" datePattern="dd/MM/yyyy"
							showApplyButton="false" enableManualInput="false"/>
							
			<h:outputLabel styleClass="LABEL"
			value="#{msg['appointmentSearch.lbl.appointmentDate']}:" />
			<rich:calendar  id="appointmentTimeId" styleClass="READONLY" 
							value="#{pages$appointmentRequest.appointmentDetailVO.appointmentDate}"
							locale="#{pages$appointmentSearch.locale}"
							popup="true" datePattern="hh:mm"
							showApplyButton="false" enableManualInput="false"/>
			<h:outputLabel styleClass="LABEL"
			value="#{msg['appointmentSearch.lbl.duration']}:" />
			<h:inputText styleClass="READONLY APPLICANT_DETAILS_INPUT"
			maxlength="20"  readonly="true"
			value="#{pages$appointmentRequest.appointmentDetailVO.duration}"
			></h:inputText>	
			
			<h:outputLabel styleClass="LABEL"
			value="#{msg['appointmentSearch.lbl.createdBy']}:" />
		<h:inputText styleClass="READONLY APPLICANT_DETAILS_INPUT"
			maxlength="20"  readonly="true"
			value="#{pages$appointmentRequest.appointmentDetailVO.createdBy}"
			></h:inputText>	
			
			<h:outputLabel styleClass="LABEL"
			value="#{msg['appointmentSearch.lbl.email']}:" />
		<h:inputText styleClass="READONLY "
			maxlength="20"  readonly="true"
			value="#{pages$appointmentRequest.appointmentDetailVO.contactEmail}"
			></h:inputText>
			
			<h:outputLabel styleClass="LABEL"
			value="#{msg['appointmentSearch.lbl.phone']}:" />
		<h:inputText styleClass="READONLY "
			maxlength="20"  readonly="true"
			value="#{pages$appointmentRequest.appointmentDetailVO.contactNumber}"
			></h:inputText>
			
			<h:outputLabel styleClass="LABEL"
			value="#{msg['appointmentSearch.lbl.appointmentTitle']}:" />
		<h:inputTextarea styleClass="READONLY " 
			 readonly="true" style="width:80%;height:80px"
			value="#{pages$appointmentRequest.appointmentDetailVO.title}"
			></h:inputTextarea>
			
			
			<h:outputLabel styleClass="LABEL"
			value="#{msg['appointmentSearch.lbl.appointmentDsc']}:" />
		<h:inputTextarea styleClass="READONLY"
			  readonly="true" style="width:80%;height:80px"
			value="#{pages$appointmentRequest.appointmentDetailVO.description}"
			></h:inputTextarea>
					
	</t:panelGrid>
</t:div>
