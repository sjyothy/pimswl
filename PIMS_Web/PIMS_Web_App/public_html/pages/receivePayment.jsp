

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="javascript" type="text/javascript">
	
		function closeWindow(parentVar)
		{
			if(parentVar.length > 0)
			{
				window.opener.document.getElementById(parentVar).value = "Y";
	      	}
	      	window.opener.document.forms[0].submit();
	      	window.close();	      	
		}
		function clearFields(control){

			document.getElementById(control).value = "";
		}

</script>



<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">

			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

			<script type="text/javascript">
		function disableBTAddButton()
		{
			document.getElementById('_id0:btn_addBankTransfer').disabled	= "true";
		}
		function disableControl( cntrl )
		{
		  
		  document.getElementById('_id0:btnOk').disabled = "true";
		  document.getElementById('_id0:lnkOk').onclick();
		} 
		function setValuesToHidden(){
		if(document.getElementById('_id0:f5') !=null)
		document.getElementById('_id0:gbtu5').value  = document.getElementById('_id0:f5').value;
		if(document.getElementById('_id0:f4') !=null)
		document.getElementById('_id0:gbtu4').value = document.getElementById('_id0:f4').value;
				if(document.getElementById('_id0:f3') !=null)
		document.getElementById('_id0:gbtu3').value =document.getElementById('_id0:f3').value;
				if(document.getElementById('_id0:f2') !=null)
		document.getElementById('_id0:gbtu2').value =document.getElementById('_id0:f2').value;
				if(document.getElementById('_id0:f1') !=null)
		document.getElementById('_id0:gbtu1').value =document.getElementById('_id0:f1').value;
						if(document.getElementById('_id0:f7') !=null)
		document.getElementById('_id0:gbtu7').value =document.getElementById('_id0:f7').value;	
		document.getElementById('_id0:gbtu6').value	= "true";					
        document.getElementById('_id0:btn_addCheque').disabled	= "true";
		document.forms[0].submit();
		}	
		 
         function onMouseOver(payNums) {            
            var payNum = payNums.split(",");
            for(var i= 0;i< payNum.length;i++)
             var chkBox =document.getElementById(payNum[i]);
             chkBox.checked=true;
             
            }
           
        
        function onMouseOver(payNums) {        	
            var payNum = payNums.split(",");
            var tbl =document.getElementById('_id0:ttbb');
             var trs = tbl.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
              for (var i = 0; i < trs.length; i++) {
                for(var j= 0;j< payNum.length;j++)
	            {
	              if(trs[i].getAttribute("id")==payNum[j])
	               {  
	                 
		             var chkBox = document.getElementById("chk_PaySch["+i+"]");
		             
		             chkBox.checked=true;
		           }
		        }
	           }
	     
	     
            }
           
           function onMouseClick(payNums) {           		
           		var payNum = payNums.split(",");
            	var tbl =document.getElementById('_id0:ttbb');
             	var trs = tbl.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
              	for (var i = 0; i < trs.length; i++) 
              	{	
	            	trs[i].style.backgroundColor = '';
	            	
                	for(var j= 0;j< payNum.length;j++)
	            	{ 
		              if(trs[i].getAttribute("id")== payNum[j])
		              {  
		              	trs[i].style.backgroundColor = '#f9a63c';
			          }			          
		        	}
	           	}
           }
            
            function onMouseOut(payNums) {
        
            var payNum = payNums.split(",");
            var tbl =document.getElementById('_id0:ttbb');
             var trs = tbl.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
              for (var i = 0; i < trs.length; i++) {
                for(var j= 0;j< payNum.length;j++)
	            {
	              if(trs[i].getAttribute("id")==payNum[j])
	               {  
		             var chkBox = document.getElementById("chk_PaySch["+i+"]");
		             
		             chkBox.checked=false;
		           }
		        }
	           }
	     
	     
            }
           
        




		</script>
		</head>
		<body>

			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>

					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['receivePayment.title']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height: 410px">
										<h:form id="_id0" style="width:98%;">
											<h:inputHidden id="gbtu6"
												binding="#{pages$receivePayment.add}"></h:inputHidden>
											<h:inputHidden id="gbtu7"
												binding="#{pages$receivePayment.cashAmountHidden}"></h:inputHidden>
											<h:inputHidden id="gbtu5"
												binding="#{pages$receivePayment.accountNoHidden}"></h:inputHidden>
											<h:inputHidden id="gbtu4"
												binding="#{pages$receivePayment.chequeNoHidden}"></h:inputHidden>
											<h:inputHidden id="gbtu3"
												binding="#{pages$receivePayment.chequeDateHidden}"></h:inputHidden>
											<h:inputHidden id="gbtu2"
												binding="#{pages$receivePayment.transactionAmountHidden}"></h:inputHidden>
											<h:inputHidden id="gbtu1"
												binding="#{pages$receivePayment.bankIdHidden}"></h:inputHidden>
											<h:inputHidden id="gbtu8"
												binding="#{pages$receivePayment.hdnParentVarName}"></h:inputHidden>
											<h:inputHidden id="cashHid"
												value="#{pages$receivePayment.totalCashAmountToBePaid}"></h:inputHidden>
											<h:inputHidden id="chqHid"
												value="#{pages$receivePayment.totalChqAmountToBePaid}"></h:inputHidden>
											<h:inputHidden id="todayHid"
												value="#{pages$receivePayment.todayDateString}"></h:inputHidden>
											<h:inputHidden id="chqhid"
												value="#{pages$receivePayment.chqType}"></h:inputHidden>
											<h:outputText id="ccc" escape="false" styleClass="ERROR_FONT"
												value="#{pages$receivePayment.errorMessages}" />
											<div class="MARGIN">
												<table style="width: 100%" border="0">

													<tr>
														<td>
															<DIV>

																<DIV>

																	<table width="98%">
																		<tr>
																			<td width="25%">
																				<h:outputLabel styleClass="A_RIGHT"
																					value="#{msg['collectPayment.collectionDate']} :" />
																			</td>
																			<td width="25%">
																				<h:outputLabel styleClass="A_LEFT"
																					value="#{pages$receivePayment.todayDateString}" />
																			</td>
																			<td>
																				<h:outputLabel styleClass="A_LEFT"
																					value="#{msg['collectPayment.TotalAmount']} :" />
																			</td>
																			<td>
																				<h:outputText styleClass="A_LEFT"
																					value="#{pages$receivePayment.totalChqAmountToBePaid + pages$receivePayment.totalCashAmountToBePaid+ pages$receivePayment.totalBankTransferAmountToBePaid}" />
																			</td>
																		</tr>

																	</table>
																	<BR />
																	<BR />
																</DIV>

																<t:div id="divtt" styleClass="contentDiv"
																	style="width:100%">

																	<t:dataTable id="ttbb" width="100%"
																		value="#{pages$receivePayment.paymentSchedules}"
																		binding="#{pages$receivePayment.dataTablePaymentSchedule}"
																		preserveDataModel="false" preserveSort="false"
																		var="paymentScheduleDataItem" rowClasses="row1,row2"
																		renderedIfEmpty="true"
																		rowId="#{paymentScheduleDataItem.paymentNumber}">
																		<t:column id="select" style="width:5%;">
																			<f:facet name="header">
																				<t:outputText id="selectTxt"
																					value="#{msg['commons.select']}" />
																			</f:facet>
																			<t:selectBooleanCheckbox forceId="true"
																				id="chk_PaySch"
																				disabled="#{paymentScheduleDataItem.paymentModeId== pages$receivePayment.paymentModeCashId || paymentScheduleDataItem.disabled || paymentScheduleDataItem.addedToPaymentReceiptDetail}"
																				value="#{paymentScheduleDataItem.selected}">
																				<a4j:support event="onclick"
																					action="#{pages$receivePayment.chkPaySc_Changed}"
																					reRender="ttbb,f2,transferAmount,transferDate,div_addCheque,div_addBankTransfer" />
																			</t:selectBooleanCheckbox>

																		</t:column>

																		<t:column id="col_PaymentNumber">
																			<f:facet name="header">
																				<t:outputText id="ac20"
																					value="#{msg['paymentSchedule.paymentNumber']}" />
																			</f:facet>
																			<t:outputText id="ac19" styleClass="A_LEFT"
																				value="#{paymentScheduleDataItem.paymentNumber}" />
																		</t:column>



																		<t:column id="colTypeEnt"
																			rendered="#{pages$receivePayment.isEnglishLocale}">


																			<f:facet name="header">

																				<t:outputText id="ac17"
																					value="#{msg['paymentSchedule.paymentType']}" />

																			</f:facet>
																			<t:outputText id="ac16" styleClass="A_LEFT" title=""
																				value="#{paymentScheduleDataItem.typeEn}" />
																		</t:column>
																		<t:column id="colTypeArt"
																			rendered="#{pages$receivePayment.isArabicLocale}">


																			<f:facet name="header">

																				<t:outputText id="ac15"
																					value="#{msg['paymentSchedule.paymentType']}" />

																			</f:facet>
																			<t:outputText id="ac14" styleClass="A_LEFT" title=""
																				value="#{paymentScheduleDataItem.typeAr}" />
																		</t:column>

																		<t:column id="colDesc">


																			<f:facet name="header">

																				<t:outputText id="descHead"
																					value="#{msg['renewContract.description']}" />

																			</f:facet>
																			<t:outputText id="descText" styleClass="A_LEFT"
																				title=""
																				value="#{paymentScheduleDataItem.description}" />
																		</t:column>


																		<t:column id="colDueOnt">


																			<f:facet name="header">

																				<t:outputText id="ac13"
																					value="#{msg['paymentSchedule.paymentDueOn']}" />

																			</f:facet>
																			<t:outputText id="ac12" styleClass="A_LEFT"
																				value="#{paymentScheduleDataItem.paymentDueOn}">
																				<f:convertDateTime
																					pattern="#{pages$receivePayment.dateFormat}"
																					timeZone="#{pages$receivePayment.timeZone}" />
																			</t:outputText>
																		</t:column>

																		<t:column id="colStatusEnt"
																			rendered="#{pages$receivePayment.isEnglishLocale}">


																			<f:facet name="header">

																				<t:outputText id="ac11"
																					value="#{msg['commons.status']}" />

																			</f:facet>
																			<t:outputText id="ac10" styleClass="A_LEFT" title=""
																				value="#{paymentScheduleDataItem.statusEn}" />
																		</t:column>
																		<t:column id="colStatusArt"
																			rendered="#{pages$receivePayment.isArabicLocale}">


																			<f:facet name="header">

																				<t:outputText id="ac9"
																					value="#{msg['commons.status']}" />

																			</f:facet>
																			<t:outputText id="ac8" styleClass="A_LEFT" title=""
																				value="#{paymentScheduleDataItem.statusAr}" />
																		</t:column>

																		<t:column id="colModeEnt"
																			rendered="#{pages$receivePayment.isEnglishLocale}">


																			<f:facet name="header">

																				<t:outputText id="ac7"
																					value="#{msg['paymentSchedule.paymentMode']}" />

																			</f:facet>
																			<t:outputText id="ac6" styleClass="A_LEFT" title=""
																				value="#{paymentScheduleDataItem.paymentModeEn}" />
																		</t:column>
																		<t:column id="colModeArt"
																			rendered="#{pages$receivePayment.isArabicLocale}">


																			<f:facet name="header">

																				<t:outputText id="ac5"
																					value="#{msg['paymentSchedule.paymentMode']}" />

																			</f:facet>
																			<t:outputText id="ac4" styleClass="A_LEFT" title=""
																				value="#{paymentScheduleDataItem.paymentModeAr}" />
																		</t:column>


																		<t:column id="colAmountt">


																			<f:facet name="header">

																				<t:outputText id="ac1"
																					value="#{msg['paymentSchedule.amount']}" />

																			</f:facet>
																			<t:outputText id="ac2" styleClass="A_RIGHT_NUM"
																				value="#{paymentScheduleDataItem.amount}" />
																		</t:column>





																	</t:dataTable>



																</t:div>
																<br />
																<br />
															</DIV>


														
															<div class="TAB_PANEL_INNER">
																<rich:tabPanel id="richTab"
																	binding="#{pages$receivePayment.paymentsTabPanel}"
																	valueChangeListener="#{pages$receivePayment.tabChanged}"
																	headerSpacing="0" width="100%" style="width:100%">
																	<!-- Cash Tab -  Start-->
																	<rich:tab binding="#{pages$receivePayment.cashTab}"
																		id="Cash" label="#{msg['payment.cash']}">
																		<h:panelGrid columns="4" border="0" id="pg1">
																			<h:outputLabel styleClass="A_LEFT"
																				value="#{msg['payments.totalCash']} :" />
																			<h:inputText id="f7" readonly="true"
																				styleClass="A_RIGHT_NUM READONLY"
																				binding="#{pages$receivePayment.cashAmount}"></h:inputText>

																			<h:outputText value="#{msg['payment.exchangeRate']}:"
																				rendered="false">
																			</h:outputText>
																			<h:inputText
																				binding="#{pages$receivePayment.exchangeRate}"
																				style="text-align:right" rendered="false"></h:inputText>
																		</h:panelGrid>

																	</rich:tab>



																	<!-- Cash Tab - End -->
																	<!-- Cheque Tab - Start-->
																	<rich:tab binding="#{pages$receivePayment.chequeTab}"
																		id="Cheque" label="#{msg['payment.cheque']}"
																		rendered="#{pages$receivePayment.chequeTabRendered}">

																		<t:div styleClass="DETAIL_SECTION" style="width:100%;">

																			<h:panelGrid id="pg2" columns="4" border="0"
																				width="100%" cellpadding="1px" cellspacing="5px"
																				styleClass="DETAIL_SECTION_INNER">
																				<h:outputText style="width:25%;" styleClass="A_LEFT"
																					value="#{msg['payments.totalChq']} :" />
																				<h:inputText readonly="true"
																					styleClass="A_RIGHT_NUM READONLY"
																					value="#{pages$receivePayment.totalChqAmountToBePaid}" />
																				<h:outputText style="width:25%;" styleClass="A_LEFT"
																					value="#{msg['collectPayment.chequeAmountCollected']} :" />
																				<h:inputText readonly="true"
																					styleClass="A_RIGHT_NUM READONLY"
																					value="#{pages$receivePayment.chequeAmountReceived}" />

																				<h:outputText value="#{msg['payment.chequeNo']}:">
																				</h:outputText>

																				<h:inputText id="f4"
																					binding="#{pages$receivePayment.chequeNo}"></h:inputText>
																				<h:outputText value="#{msg['payment.chequeDate']}:">
																				</h:outputText>
																				<rich:calendar id="chequeDate" disabled="true"
																					value="#{pages$receivePayment.chequeDateDt}"
																					locale="#{pages$receivePayment.locale}"
																					popup="true"
																					datePattern="#{pages$receivePayment.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					cellWidth="24px" cellHeight="22px"
																					style="width:186px; height:16px" />


																				<h:outputText value="#{msg['payment.amount']}:">
																				</h:outputText>

																				<h:inputText id="f2"
																					styleClass="A_RIGHT_NUM READONLY" readonly="true"
																					binding="#{pages$receivePayment.transactionAmount}"></h:inputText>
																				<h:outputText value="#{msg['commons.typeCol']}:">
																				</h:outputText>
																				<h:selectOneMenu id="typeCombo"
																					value="#{pages$receivePayment.chqType}">
																					<f:selectItems
																						value="#{pages$ApplicationBean.chequeType}" />
																				</h:selectOneMenu>

																				<h:outputText value="#{msg['payment.accountNo']}:">
																				</h:outputText>
																				<h:inputText id="f5"
																					binding="#{pages$receivePayment.accountNo}"></h:inputText>
																				<h:outputText value="#{msg['payment.bank']}:">
																				</h:outputText>

																				<h:selectOneMenu id="f1"
																					value="#{pages$receivePayment.selectOneBank}"
																					binding="#{pages$receivePayment.banksMenu}">

																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																					<f:selectItems
																						value="#{pages$receivePayment.banks}" />
																				</h:selectOneMenu>



																			</h:panelGrid>

																		</t:div>

																		<t:div id="div_addCheque" styleClass="BUTTON_TD">
																			<h:commandButton id="btn_addCheque" type="button"
																				styleClass="BUTTON"
																				binding="#{pages$receivePayment.buttonSave}"
																				action="#{pages$receivePayment.add}"
																				onclick="javascript:setValuesToHidden();" />
																		</t:div>

																		<t:div>

																			<t:div styleClass="MARGIN">

																				<t:div styleClass="contentDiv" style="width:98.3%">
																					<t:dataTable id="test2" width="100%"
																						styleClass="grid"
																						value="#{pages$receivePayment.paymentTransactionList}"
																						binding="#{pages$receivePayment.dataTable}"
																						rows="10" preserveDataModel="true"
																						preserveSort="false" var="dataItem"
																						rowOnMouseOver="javascript:onMouseOver('#{dataItem.csPaymentScheduleNum}');"
																						rowOnMouseOut="javascript:onMouseOut('#{dataItem.csPaymentScheduleNum}');"
																						rowOnClick="javascript:onMouseClick('#{dataItem.csPaymentScheduleNum}');"
																						rowClasses="row1,row2" rules="all"
																						renderedIfEmpty="true">

																						<t:column id="col1En" width="20%"
																							rendered="#{pages$receivePayment.isEnglishLocale}">
																							<f:facet name="header">
																								<t:outputText value="#{msg['payment.bank']}" />
																							</f:facet>
																							<t:outputText value="#{dataItem.bankEn}" />
																						</t:column>
																						<t:column id="col1Ar" width="20%"
																							rendered="#{pages$receivePayment.isArabicLocale}">
																							<f:facet name="header">
																								<t:outputText value="#{msg['payment.bank']}" />
																							</f:facet>
																							<t:outputText value="#{dataItem.bankAr}" />
																						</t:column>
																						<t:column id="colAccNo" width="20%">
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['payment.accountNo']}" />
																							</f:facet>
																							<t:outputText value="#{dataItem.accountNo}" />
																						</t:column>
																						<t:column id="col2" width="20%">
																							<f:facet name="header">
																								<t:outputText value="#{msg['payment.chequeNo']}" />
																							</f:facet>
																							<t:outputText value="#{dataItem.methodRefNo}" />
																						</t:column>


																						<t:column id="col3" width="20%">


																							<f:facet name="header">

																								<t:outputText
																									value="#{msg['payment.chequeDate']}" />

																							</f:facet>
																							<t:outputText value="#{dataItem.methodRefDate}">
																								<f:convertDateTime
																									pattern="#{pages$receivePayment.dateFormat}"
																									timeZone="#{pages$receivePayment.timeZone}" />
																							</t:outputText>
																						</t:column>
																						<t:column id="col4" width="20%">


																							<f:facet name="header">

																								<t:outputText value="#{msg['payment.amount']}" />

																							</f:facet>
																							<t:outputText value="#{dataItem.amount}" />
																						</t:column>

																						<t:column id="col9">

																							<f:facet name="header">
																								<h:outputLabel value="#{msg['commons.delete']}" />
																							</f:facet>
																							<h:commandLink
																								action="#{pages$receivePayment.deleteDataItem}">�
																	<h:graphicImage id="deleteCmdLink"
																									title="#{msg['commons.delete']}"
																									url="../resources/images/delete_icon.png" />
																							</h:commandLink>

																						</t:column>
																					</t:dataTable>
																				</t:div>

																				<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																					style="width:99.9%;">
																				</t:div>
																			</t:div>
																		</t:div>
																	</rich:tab>
																	<!-- Cheque Tab - End -->
																	<!-- Bank Transfer Tab -  Start-->
																	<rich:tab id="BankTransfer"
																		binding="#{pages$receivePayment.bankTransferTab}"
																		label="#{msg['payments.tabTitle.bankTransfer']}">

																		<t:div styleClass="DETAIL_SECTION" style="width:100%;">
																			<h:panelGrid id="pg3" columns="4" border="0"
																				width="100%" cellpadding="1px" cellspacing="5px"
																				styleClass="DETAIL_SECTION_INNER">
																				<h:outputLabel
																					value="#{msg['payments.fieldName.transferDate']} :" />
																				<rich:calendar id="transferDate"
																					binding="#{pages$receivePayment.clndrTransferDate}"
																					value="#{pages$receivePayment.transferDate}"
																					locale="#{pages$receivePayment.locale}"
																					popup="true"
																					datePattern="#{pages$receivePayment.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					cellWidth="24px" cellHeight="22px"
																					style="width:186px; height:16px" />
																				<h:panelGroup>
																					<h:outputLabel styleClass="mandatory" value="*" />
																					<h:outputLabel
																						value="#{msg['payments.fieldName.transferNumber']} :" />
																				</h:panelGroup>
																				<h:inputText
																					binding="#{pages$receivePayment.txtTransferNumber}"
																					value="#{pages$receivePayment.transferNumber}" />

																				<h:panelGroup>
																					<h:outputLabel styleClass="mandatory" value="*" />
																					<h:outputLabel
																						value="#{msg['bouncedChequesList.bankNameCol']} :" />
																				</h:panelGroup>
																				<h:selectOneMenu id="bankId"
																					value="#{pages$receivePayment.selectOneBank}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																					<f:selectItems
																						value="#{pages$receivePayment.banks}" />
																				</h:selectOneMenu>

																				<h:panelGroup>
																					<h:outputLabel styleClass="mandatory" value="*" />
																					<h:outputLabel
																						value="#{msg['receiveProperty.unitAccountNumber']} :" />
																				</h:panelGroup>
																				<h:inputText
																					binding="#{pages$receivePayment.txtAccountNumber}"
																					value="#{pages$receivePayment.accountNumber}" />

																				<h:panelGroup>
																					<h:outputLabel styleClass="mandatory" value="*" />
																					<h:outputLabel
																						value="#{msg['payments.fieldName.transferAmount']} :" />
																				</h:panelGroup>
																				<h:inputText styleClass="A_RIGHT_NUM READONLY"
																					readonly="true"
																					binding="#{pages$receivePayment.txtTransferAmount}"
																					id="transferAmount"
																					value="#{pages$receivePayment.transferAmount}" />
																			</h:panelGrid>
																		</t:div>
																		<t:div id="div_addBankTransfer" styleClass="BUTTON_TD">
																			<h:commandButton id="btn_addBankTransfer"
																				styleClass="BUTTON" value="#{msg['attachment.add']}"
																				binding="#{pages$receivePayment.buttonSaveBankTransfer}"
																				action="#{pages$receivePayment.addBankTransfer}" />
																		</t:div>
																		<t:div>
																			<t:div styleClass="MARGIN">
																				<t:div styleClass="contentDiv" style="width:98.3%">
																					<t:dataTable id="bankTransfer" width="100%"
																						styleClass="grid"
																						value="#{pages$receivePayment.bankTransferPaymentList}"
																						binding="#{pages$receivePayment.bankTransferDataTable}"
																						rows="10" preserveDataModel="true"
																						preserveSort="false" var="dataItem2"
																						rowOnMouseOver="javascript:onMouseOver('#{dataItem2.csPaymentScheduleNum}');"
																						rowOnMouseOut="javascript:onMouseOut('#{dataItem2.csPaymentScheduleNum}');"
																						rowOnClick="javascript:onMouseClick('#{dataItem2.csPaymentScheduleNum}');"
																						rowClasses="row1,row2" rules="all"
																						renderedIfEmpty="true">
																						<t:column id="col1Enrer" width="20%"
																							rendered="#{pages$receivePayment.isEnglishLocale}">
																							<f:facet name="header">
																								<t:outputText value="#{msg['payment.bank']}" />
																							</f:facet>
																							<t:outputText value="#{dataItem2.bankEn}" />
																						</t:column>
																						<t:column id="col1Arer" width="20%"
																							rendered="#{pages$receivePayment.isArabicLocale}">
																							<f:facet name="header">
																								<t:outputText value="#{msg['payment.bank']}" />
																							</f:facet>
																							<t:outputText value="#{dataItem2.bankAr}" />
																						</t:column>
																						<t:column id="colAccNorer" width="20%">
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['payment.accountNo']}" />
																							</f:facet>
																							<t:outputText value="#{dataItem2.accountNo}" />
																						</t:column>
																						<t:column id="col2rer" width="20%">
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['payments.fieldName.transferNumber']}" />
																							</f:facet>
																							<t:outputText value="#{dataItem2.methodRefNo}" />
																						</t:column>
																						<t:column id="col3erre" width="20%">
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['payments.fieldName.transferDate']}" />
																							</f:facet>
																							<t:outputText value="#{dataItem2.methodRefDate}">
																								<f:convertDateTime
																									pattern="#{pages$receivePayment.dateFormat}"
																									timeZone="#{pages$receivePayment.timeZone}" />
																							</t:outputText>
																						</t:column>
																						<t:column id="col92">

																							<f:facet name="header">
																								<h:outputLabel value="#{msg['commons.delete']}" />
																							</f:facet>
																							<h:commandLink
																								action="#{pages$receivePayment.deleteBankTransferDataItem}">�
																	<h:graphicImage id="deleteCmdLink1"
																									title="#{msg['commons.delete']}"
																									url="../resources/images/delete_icon.png" />
																							</h:commandLink>

																						</t:column>
																					</t:dataTable>
																				</t:div>
																			</t:div>
																		</t:div>
																	</rich:tab>
																	<!-- Bank Transfer Tab- End -->
																	<!-- Attachments Tab - Start -->
																	<rich:tab id="attachmentTab"
																		label="#{msg['contract.attachmentTabHeader']}"
																		disabled="#{!pages$receivePayment.paymentPossible}">
																		<%@  include file="attachment/attachment.jsp"%>
																	</rich:tab>
																	<!-- Attachments Tab - End -->
																</rich:tabPanel>

																

															</div>

														</td>
													</tr>

												</table>
											</div>


											<DIV class="BUTTON_TD">
												<h:commandButton styleClass="BUTTON" id="btnOk"
													binding="#{pages$receivePayment.buttonOk}"
													onclick="javascript:disableControl();">
												</h:commandButton>
												<h:commandLink id="lnkOk"
													action="#{pages$receivePayment.done}"></h:commandLink>
												<h:commandButton styleClass="BUTTON"
													binding="#{pages$receivePayment.buttonCancel}"
													action="#{pages$receivePayment.reset}">
												</h:commandButton>



											</DIV>
									</div>
									</div>

									</h:form>
									</div>
									</f:view>
		</body>
	</html>