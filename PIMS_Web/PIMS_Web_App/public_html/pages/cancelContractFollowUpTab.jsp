
<t:panelGrid id="tbl_Action" width="100%" border="0" columns="4"
	binding="#{pages$cancelContractFollowUpTab.tbl_Action}" rendered="#{pages$CancelContract.followUpDone}">
	<h:outputLabel styleClass="LABEL" value="#{msg['maintenanceRequest.remarks']}"/>
	<t:panelGroup colspan="3">
		<t:inputTextarea style="width: 190px" id="txt_remarks"
			value="#{pages$cancelContractFollowUpTab.txtRemarks}" />
	</t:panelGroup>



</t:panelGrid>
<t:panelGrid id="tbl_ActionBtn" cellpadding="1px" cellspacing="3px"
	width="100%" border="0" columns="1" columnClasses="BUTTON_TD" rendered="#{pages$CancelContract.followUpDone}">
	<t:panelGroup>
		<h:commandButton type="submit" styleClass="BUTTON" style="width:auto;"
			action="#{pages$cancelContractFollowUpTab.btnSaveFollowUp_Click}"
			value="#{msg['maintenanceRequest.saveFollowUp']}" />
	</t:panelGroup>
</t:panelGrid>

<t:div id="followUpsDiv" styleClass="contentDiv" style="width:98%">
	<t:dataTable id="followUpDataTable"
		rows="#{pages$cancelContractFollowUpTab.paginatorRows}"
		style="width:100%" value="#{pages$cancelContractFollowUpTab.list}"
		binding="#{pages$cancelContractFollowUpTab.requestHistoryDataTable}"
		preserveDataModel="false" preserveSort="false" var="followUpDataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
		<t:column id="lastFollowupDateCol" style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.date']}" />
			</f:facet>
			<t:outputText value="#{followUpDataItem.followupDate}"
				styleClass="A_LEFT" style="white-space: normal;">
				<f:convertDateTime
					pattern="#{pages$cancelContractFollowUpTab.dateFormat}"
					timeZone="#{pages$cancelContractFollowUpTab.timeZone}" />
			</t:outputText>
		</t:column>

		
			<t:column id="followUpcolUser">
				<f:facet name="header">
					<t:outputText value="#{msg['maintenanceRequest.followUpBy']}" />
				</f:facet>
				<t:outputText id ="createdByFullName" styleClass="A_LEFT"
					value="#{pages$cancelContractFollowUpTab.isEnglishLocale? followUpDataItem.createdByFullNameEn:followUpDataItem.createdByFullNameAr}" />
			</t:column>
<t:column>
			<f:facet name="header">
				<t:outputText value="#{msg['maintenanceRequest.remarks']}" />
			</f:facet>
			<t:outputText value="#{followUpDataItem.remarks}" styleClass="A_LEFT" />

		</t:column>



	</t:dataTable>
</t:div>

<t:div id="followUpDivScroller" styleClass="contentDivFooter"
	style="width:99.1%">


	<t:panelGrid id="followUpRecNumTable" columns="2" cellpadding="0"
		cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD">
		<t:div id="followUpRecNumDiv" styleClass="RECORD_NUM_BG">
			<t:panelGrid id="followUpRecNumTbl" columns="3"
				columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
				cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
				<h:outputText value="#{msg['commons.recordsFound']}" />
				<h:outputText value=" : " />
				<h:outputText value="#{pages$cancelContractFollowUpTab.recordSize}" />
			</t:panelGrid>

		</t:div>

		<CENTER>
			<t:dataScroller id="followUpscroller" for="followUpDataTable"
				paginator="true" fastStep="1"
				paginatorMaxPages="#{pages$cancelContractFollowUpTab.paginatorMaxPages}"
				immediate="false" paginatorTableClass="paginator"
				renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
				styleClass="SCH_SCROLLER"
				paginatorActiveColumnStyle="font-weight:bold;"
				paginatorRenderLinkForActive="false"
				paginatorTableStyle="grid_paginator" layout="singleTable"
				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

				<f:facet name="first">
					<t:graphicImage url="../#{path.scroller_first}"
						id="lblFcancelContractFollowUpTab"></t:graphicImage>
				</f:facet>

				<f:facet name="fastrewind">
					<t:graphicImage url="../#{path.scroller_fastRewind}"
						id="lblFRcancelContractFollowUpTab"></t:graphicImage>
				</f:facet>

				<f:facet name="fastforward">
					<t:graphicImage url="../#{path.scroller_fastForward}"
						id="lblFFcancelContractFollowUpTab"></t:graphicImage>
				</f:facet>

				<f:facet name="last">
					<t:graphicImage url="../#{path.scroller_last}"
						id="lblLcancelContractFollowUpTab"></t:graphicImage>
				</f:facet>

				<t:div id="followUpPageNumDiv" styleClass="PAGE_NUM_BG">
					<t:panelGrid id="followUpPageNumTable"
						styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
						cellspacing="0">
						<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}" />
						<h:outputText styleClass="PAGE_NUM"
							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
							value="#{requestScope.pageNumber}" />
					</t:panelGrid>

				</t:div>
			</t:dataScroller>
		</CENTER>

	</t:panelGrid>
</t:div>
