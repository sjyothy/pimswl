<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)\
  - Description: Used for Searching Violations
  --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%-- 
		<% UIViewRoot viewRoot = (UIViewRoot)session.getAttribute("view");
           if (viewRoot.getLocale().toString().equals("en"))
           { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>
        <%
            } 
            else
            { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_ar.js"></script>
        <%
            } 
        %>
--%>
<script language="JavaScript" type="text/javascript">

	    function resetValues()
      	{
      	    document.getElementById("searchFrm:auctionStatuses").selectedIndex=0;
		    document.getElementById("searchFrm:auctionNumber").value="";
			document.getElementById("searchFrm:auctionTitle").value="";
			document.getElementById("searchFrm:auctionDate").value="";
			document.getElementById("searchFrm:auctionCreationDate").value="";
			document.getElementById("searchFrm:auctionStartTime").value="";
			document.getElementById("searchFrm:auctionEndTime").value="";
			document.getElementById("searchFrm:venueName").value="";
        }
	    

</script>
<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
 
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is Violation search page">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>						


			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			
 	</head>

	<body>
			<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
			%>
	
		
	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				

				<tr width="100%">
				
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" 
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="99%" valign="top" nowrap="nowrap">
									<div style="height:450;overflow:auto;" >
									<h:form id="searchFrm">
											<table width="100%">
										        <tr>
												     <td>
													&nbsp;	
													
													</td>
													
													
										    </tr>
										  
										</table>
										<br>
										<br><br>
										<div class="imag">&nbsp;</div>
										<div class="contentDiv" align="center">
											<t:dataTable id="dt1" styleClass="grid" 
												value="#{pages$ViolationActionPopUp.dataList}"
												binding="#{pages$ViolationActionPopUp.tbl_violationSearch}" rows="10"
												preserveDataModel="true" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												width="100%">


									            <t:column id="actionDate" sortable="true" >
													<f:facet  name="header">
														<t:outputText value="#{msg['inspection.type']}" />
													</f:facet>
													<t:outputText value="#{dataItem.actionDate}" />
												</t:column>
												<t:column id="typeEn" sortable="true" rendered="#{pages$ViolationActionPopUp.isEnglishLocale}">
													<f:facet  name="header">
														<t:outputText value="#{msg['inspection.type']}" />
													</f:facet>
													<t:outputText value="#{dataItem.actionEn}" />
												</t:column>
												<t:column id="typeAr" sortable="true" rendered="#{pages$ViolationActionPopUp.isArabicLocale}">
													<f:facet  name="header">
														<t:outputText value="#{msg['inspection.type']}" />
													</f:facet>
													<t:outputText value="#{dataItem.actionAr}" />
												</t:column>
											<t:column id="createdBy" sortable="true" >
													<f:facet  name="header">
														<t:outputText value="#{msg['inspection.type']}" />
													</f:facet>
													<t:outputText value="#{dataItem.createdBy}" />
												</t:column>
											</t:dataTable>
										</div>
										<div class="contentDivFooter">

												<t:dataScroller id="scroller" for="dt1" paginator="true"
													fastStep="1" paginatorMaxPages="2" immediate="false"
													styleClass="scroller" paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

													<f:facet name="first">
														
														<t:graphicImage url="../resources/images/first_btn.gif" id="lblF"></t:graphicImage>
													</f:facet>

													<f:facet name="fastrewind">
														<t:graphicImage url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>
													</f:facet>

													<f:facet name="fastforward">
														<t:graphicImage url="../resources/images/next_btn.gif" id="lblFF"></t:graphicImage>
													</f:facet>

													<f:facet name="last">
														<t:graphicImage url="../resources/images/last_btn.gif" id="lblL"></t:graphicImage>
													</f:facet>

												</t:dataScroller>
									
                                           </div>
									</h:form>
									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
  				  <td colspan="2">
  				  <table width="100%" cellpadding="0" cellspacing="0" border="0">
  				     <tr><td class="footer"><h:outputLabel value="#{msg['commons.footer.message']}" /></td></tr>
 				   </table>
        </td>
    </tr>
			</table>
		
	</body>
</html>
</f:view>