<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Requests
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function submitForm()
  	{
           document.getElementById('searchFrm').submit();
	}
	    function openLoadReportPopup(pageName) 
	{	
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
	    function resetValues()
      	{
      	
      		document.getElementById("searchFrm:complaintNum").value="";
      		document.getElementById("searchFrm:originalRequestNum").value="";
      	    document.getElementById("searchFrm:requestStatuses").selectedIndex=0;
      	    document.getElementById("searchFrm:requestTypes").selectedIndex=0;
		    document.getElementById("searchFrm:contractNumber").value="";
			document.getElementById("searchFrm:tenantNumber").value="";
			document.getElementById("searchFrm:requestNumber").value="";
			$('searchFrm:requestDateTo').component.resetSelectedDate();
			$('searchFrm:requestDateFrom').component.resetSelectedDate();
			document.getElementById("searchFrm:state").selectedIndex=0;
      	    document.getElementById("searchFrm:country").selectedIndex=0;
      	    document.getElementById("searchFrm:selectTenantType").selectedIndex=0;
      	    document.getElementById("searchFrm:selectContractType").selectedIndex=0;

		    document.getElementById("searchFrm:tenantName").value="";
		    document.getElementById("searchFrm:hdnTenantId").value="";
			document.getElementById("searchFrm:contractValue").value="";
			document.getElementById("searchFrm:requestNumber").value="";
			document.getElementById("searchFrm:requestOriginationCmb").value="";
			document.getElementById("searchFrm:fileNo").value="";
			document.getElementById("searchFrm:fileOwner").value="";
			document.getElementById("searchFrm:selectOneDisbursementReasonId").selectedIndex=0;
			document.getElementById("searchFrm:selectOneDisbursementReasonTypeId").selectedIndex=0;
			document.getElementById("searchFrm:endowmentFileNameTxt").value="";
			document.getElementById("searchFrm:endowmentFilenumTxt").value="";
			document.getElementById("searchFrm:endowmentNameTxt").value="";
			document.getElementById("searchFrm:endowmentNumTxt").value="";
			document.getElementById("searchFrm:masrafNameTxt").value="";
			
			

        }
        function showPopup(personType)
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   //window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
	    window.open('SearchPerson.jsf?persontype='+personType+'&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		    
		   
			document.getElementById("searchFrm:hdntenantId").value=personId;
			 document.getElementById("searchFrm:tenantName").value=personName;
			 
	         
	       
	}
        
         function submitForm()
	   {
          document.getElementById('searchFrm').submit();
          document.getElementById("searchFrm:country").selectedIndex=0;
	   }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<body class="BODY_STYLE">
			<div class="containerDiv">

				<%
					response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
						response.setHeader("Pragma", "no-cache"); //HTTP 1.0
						response.setDateHeader("Expires", 0); //prevents caching at the proxy server
				%>

				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table class="greyPanelTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['request.searchHeading']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<h:form id="searchFrm" style="WIDTH: 100%;">
											<h:inputHidden id="hdntenantId"
												value="#{pages$requestSearch.hdnTenantId}" />

											<h:inputHidden id="hdnApplicantId"
												value="#{pages$requestSearch.hdnApplicantId}" />

											<div class="SCROLLABLE_SECTION">

												<table border="0" class="layoutTable" style="width: 95%;">
													<tr>
														<td>
															<h:outputText
																value="#{pages$requestSearch.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
												<div class="MARGIN" style="width: 95%;">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="font-size: 0px;">
																<IMG
																	src="../<h:outputText value="#{path.img_section_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="font-size: 0px;">
																<IMG
																	src="../<h:outputText value="#{path.img_section_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="font-size: 0px;">
																<IMG
																	src="../<h:outputText value="#{path.img_section_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="DETAIL_SECTION" style="width: 99.8%;">
														<h:outputLabel value="#{msg['commons.searchCriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER">
															<tr>
																<td width="97%">
																	<table width="100%">
																		<tr>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['request.number']}:"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="requestNumber"
																					value="#{pages$requestSearch.requestView.requestNumber}"
																					maxlength="20"></h:inputText>
																			</td>
																			<td width="25%">
																				<h:outputLabel
																					value="#{msg['requestOrigination.Combo']}:"
																					styleClass="LABEL"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:selectManyMenu id="requestOriginationCmb"
																					tabindex="5"
																					value="#{pages$requestSearch.requestOriginationList}"
																				     style="height:70px" >
																					
																					<f:selectItem itemValue="0"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.requestOrigination}" />
																				</h:selectManyMenu >
																			</td>

																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['request.fromDate']}:"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="requestDateFrom"
																					binding="#{pages$requestSearch.htmlCalendarFromDate}"
																					locale="#{pages$requestSearch.locale}" popup="true"
																					datePattern="dd/MM/yyyy" showApplyButton="false"
																					enableManualInput="false"
																					inputStyle="width:170px; height:14px" />
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['request.toDate']}:" />
																			</td>
																			<td>
																				<%--			<h:inputText id="requestDateTo"
															value="#{pages$requestSearch.toDate}"
															></h:inputText>--%>
																				<rich:calendar id="requestDateTo"
																					binding="#{pages$requestSearch.htmlCalendarToDate}"
																					locale="#{pages$requestSearch.locale}" popup="true"
																					enableManualInput="false" datePattern="dd/MM/yyyy"
																					showApplyButton="false"
																					inputStyle="width:170px; height:14px" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['request.ProcessedFrom']}:"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="requestlastTrxDateFrom"
																					binding="#{pages$requestSearch.lastTrxDateFrom}"
																					locale="#{pages$requestSearch.locale}" popup="true"
																					datePattern="dd/MM/yyyy" showApplyButton="false"
																					enableManualInput="false"
																					inputStyle="width:170px; height:14px" />
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['request.ProcessedTo']}:" />
																			</td>
																			<td>

																				<rich:calendar id="requestlastTrxDateTo"
																					binding="#{pages$requestSearch.lastTrxDateTo}"
																					locale="#{pages$requestSearch.locale}" popup="true"
																					enableManualInput="false" datePattern="dd/MM/yyyy"
																					showApplyButton="false"
																					inputStyle="width:170px; height:14px" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['request.type.module']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="pimsModules"
																					binding="#{pages$requestSearch.requestTypeModuleCombo}"
																					valueChangeListener="#{pages$requestSearch.requestTypeModuleChange}"
																					onchange="javaScript:submitForm();" 
																					>
																					<f:selectItem itemValue="0"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItem itemValue="property"
																						itemLabel="#{msg['pims.module.property']}" />
																					<f:selectItem itemValue="minor"
																						itemLabel="#{msg['pims.module.minor']}" />
																					<f:selectItem itemValue="endowment"
																						itemLabel="#{msg['pims.module.endowment']}" />
																				</h:selectOneMenu>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['request.type']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="requestTypes"
																					binding="#{pages$requestSearch.requestTypeCombo}"
																					required="false"
																					value="#{pages$requestSearch.requestTypeId}">
																					<f:selectItem itemValue="0"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$requestSearch.requestTypes}" />
																				</h:selectOneMenu>
																			</td>
																		</tr>
																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['applicationDetails.applicantName']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:panelGroup>
																					<h:inputText id="tenantName"
																						value="#{pages$requestSearch.applicantName}"></h:inputText>
																					<h:graphicImage
																						url="../resources/images/app_icons/Search-tenant.png"
																						onclick="showPopup('#{pages$requestSearch.personTenant}');"
																						style="MARGIN: 0px 0px -4px; CURSOR: hand;"
																						alt="#{msg[contract.searchTenant]}"></h:graphicImage>
																				</h:panelGroup>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['request.status']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="requestStatuses"
																					binding="#{pages$requestSearch.requestStatusCombo}"
																					value="#{pages$requestSearch.statusId}">
																					<f:selectItem itemValue="0"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.requestStatusList}" />
																				</h:selectOneMenu>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.contractNumber']}:"></h:outputLabel>
																			</td>

																			<td>
																				<h:inputText id="contractNumber"
																					value="#{pages$requestSearch.requestView.contractView.contractNumber}"
																					maxlength="20">
																				</h:inputText>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.replaceCheque.contractType']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="selectContractType"
																					value="#{pages$requestSearch.selectOneContractType}">
																					<f:selectItem
																						itemLabel="#{msg['commons.pleaseSelect']}"
																						itemValue="0" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.contractType}" />
																				</h:selectOneMenu>

																			</td>

																		</tr>

																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.TenantsNumber']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="tenantNumber" value=""
																					maxlength="20"></h:inputText>
																			</td>

																			<td>
																				<h:outputLabel
																					value="#{msg['tenants.tenantsType']}:"
																					styleClass="LABEL"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="selectTenantType"
																					style="width: 192px;"
																					value="#{pages$requestSearch.selectOneTenantType}">
																					<f:selectItem
																						itemLabel="#{msg['commons.pleaseSelect']}"
																						itemValue="0" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.tenantType}" />
																				</h:selectOneMenu>

																			</td>

																		</tr>
																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.emirate']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="country" tabindex="2"
																					value="#{pages$requestSearch.selectOneState}"
																					onchange="javascript:submitForm();"
																					valueChangeListener="#{pages$requestSearch.loadStates}">
																					<f:selectItem itemValue="0"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$requestSearch.countries}" />
																				</h:selectOneMenu>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.city']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="state" tabindex="5"
																					value="#{pages$requestSearch.selectOneCity}">
																					<f:selectItem itemValue="0"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$requestSearch.states}" />
																				</h:selectOneMenu>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.createdBy']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="requestCreatedBy"
																					value="#{pages$requestSearch.requestCreatedBy}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.pleaseSelect']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.userNameList}" />
																				</h:selectOneMenu>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['request.ProcessedBy']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="requestProcessedBy"
																					value="#{pages$requestSearch.requestProcessedBy}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.pleaseSelect']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.userNameList}" />
																				</h:selectOneMenu>
																			</td>
																		</tr>
																		<tr>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['searchInheritenceFile.fileNo']}:"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="fileNo"
																					value="#{pages$requestSearch.inheritanceFileRefNo}"
																					maxlength="20"></h:inputText>
																			</td>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['searchInheritenceFile.filePerson']}:"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="fileOwner"
																					value="#{pages$requestSearch.fileOwner}"></h:inputText>
																			</td>

																		</tr>
																		<tr>

																			<td>

																				<h:outputLabel id="lblReasonType" styleClass="LABEL"
																					value="#{msg['commons.typeCol']}" />
																			</td>
																			<td>
																				<h:selectOneMenu
																					id="selectOneDisbursementReasonTypeId"
																					style="width: 200px;"
																					value="#{pages$requestSearch.selectOneDisbursementReasonTypeId}">
																					<f:selectItem itemLabel="#{msg['commons.All']}"
																						itemValue="-1" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.disbursementReasonsType}" />

																				</h:selectOneMenu>

																			</td>
																			<td>

																				<h:outputLabel id="lblReason" styleClass="LABEL"
																					value="#{msg['commons.disbursementReason']}" />
																			</td>
																			<td>
																				<h:selectOneMenu id="selectOneDisbursementReasonId"
																					style="width: 200px;"
																					value="#{pages$requestSearch.selectOneDisbursementReasonId}">
																					<f:selectItem
																						itemLabel="#{msg['commons.pleaseSelect']}"
																						itemValue="-1" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.disbursementReasons}" />

																				</h:selectOneMenu>
																			</td>
																		</tr>
																		<tr>


																			<td>
																				<h:outputLabel id="collectedBycField"
																					styleClass="LABEL"
																					value="#{msg['donationRequest.lbl.collectedBy']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="selectCollectedBy"
																					value="#{pages$requestSearch.selectOnedonationCollectedBy}">
																					<f:selectItem itemLabel="#{msg['commons.All']}"
																						itemValue="-1" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.donationCollectedByUser}" />
																				</h:selectOneMenu>
																			</td>

																			<td>
																				<h:outputLabel id="masrafName" styleClass="LABEL"
																					value="#{msg['commons.Masraf']}:"></h:outputLabel>
																			</td>
																			<td>

																				<h:inputText id="masrafNameTxt"
																					value="#{pages$requestSearch.masrafName}"></h:inputText>
																			</td>


																		</tr>
																		<tr>

																			<td>
																				<h:outputLabel id="endowmentFileName"
																					styleClass="LABEL"
																					value="#{msg['request.search.endFileName']}:"></h:outputLabel>
																			</td>
																			<td>

																				<h:inputText id="endowmentFileNameTxt"
																					value="#{pages$requestSearch.endowmentFileName}"></h:inputText>
																			</td>

																			<td>
																				<h:outputLabel id="endowmentFilenum"
																					styleClass="LABEL"
																					value="#{msg['request.search.endFileNum']}:"></h:outputLabel>
																			</td>
																			<td>

																				<h:inputText id="endowmentFilenumTxt"
																					value="#{pages$requestSearch.endowmentFileNum}"></h:inputText>
																			</td>


																		</tr>

																		<tr>

																			<td>
																				<h:outputLabel id="endowmentName" styleClass="LABEL"
																					value="#{msg['revenueFollowup.lbl.endowmentName']}:"></h:outputLabel>
																			</td>
																			<td>

																				<h:inputText id="endowmentNameTxt"
																					value="#{pages$requestSearch.endowmentName}"></h:inputText>
																			</td>

																			<td>
																				<h:outputLabel id="endowmentnum" styleClass="LABEL"
																					value="#{msg['endowment.lbl.num']}:"></h:outputLabel>
																			</td>
																			<td>

																				<h:inputText id="endowmentNumTxt"
																					value="#{pages$requestSearch.endowmentNum}"></h:inputText>
																			</td>


																		</tr>
																		<tr>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.originalReqNum']}:"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="originalRequestNum"
																					value="#{pages$requestSearch.requestView.originalRequestNum}"
																					maxlength="20"></h:inputText>
																			</td>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.complaintNum']}:"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="complaintNum"
																					value="#{pages$requestSearch.complaintNum}"
																					maxlength="20"></h:inputText>
																			</td>

																		</tr>
																		<div class="MARGIN" />
																		<tr>
																			<td class="BUTTON_TD" colspan="4">
																				<pims:security
																					screen="Pims.Home.SearchRequest.Search"
																					action="create">
																					<h:commandButton styleClass="BUTTON"
																						value="#{msg['commons.search']}"
																						action="#{pages$requestSearch.searchRequests}"
																						style="width: 75px" tabindex="7"></h:commandButton>
																				</pims:security>
																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.clear']}"
																					onclick="javascript:resetValues();"
																					style="width: 75px" tabindex="7"></h:commandButton>
																			</td>
																		</tr>
																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>
															</tr>

														</table>

													</div>
												</div>
												<div
													style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;"
													style="width:96%;">
													<div class="imag">
														&nbsp;
													</div>
													<div class="contentDiv">
														<t:dataTable id="dt1"
															value="#{pages$requestSearch.dataList}"
															binding="#{pages$requestSearch.dataTable}"
															rows="#{pages$requestSearch.paginatorRows}"
															preserveDataModel="false" preserveSort="false"
															var="dataItem" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true" width="100%">

															<t:column id="requestNumberCol" width="100"
																sortable="true" style="white-space: normal;">
																<f:facet name="header">


																	<t:commandSortHeader columnName="requestNumberCol"
																		actionListener="#{pages$requestSearch.sort}"
																		value="#{msg['request.numberCol']}" arrow="true">
																		<f:attribute name="sortField" value="requestNumber" />
																	</t:commandSortHeader>


																</f:facet>
																<t:outputText value="#{dataItem.requestNumber}" />
															</t:column>
															<t:column id="tenantNameCol" width="100" sortable="false"
																style="white-space: normal;">
																<f:facet name="header">
																	<t:outputText
																		value="#{msg['applicationDetails.applicantName']}" />
																</f:facet>
																<t:outputText
																	value="#{dataItem.applicantView.personFullName}" />
															</t:column>
															<t:column id="contractNumberCol" width="100"
																sortable="false" style="white-space: normal;">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.refNum']}" />
																</f:facet>
																<t:outputText value="#{dataItem.refNumber}" />
															</t:column>
															<t:column id="requestDateCol" width="75" sortable="true"
																style="white-space: normal;">
																<f:facet name="header">
																	<t:commandSortHeader columnName="requestDateCol"
																		actionListener="#{pages$requestSearch.sort}"
																		value="#{msg['commons.date']}" arrow="true">
																		<f:attribute name="sortField" value="createdOn" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.createdOn}">
																	<f:convertDateTime
																		timeZone="#{pages$requestSearch.localTimeZone}"
																		pattern="#{pages$requestSearch.shortDateFormat}" />
																</t:outputText>
															</t:column>
															<t:column id="requestStatusCol" sortable="true"
																style="white-space: normal;">
																<f:facet name="header">
																	<t:commandSortHeader columnName="requestStatusCol"
																		actionListener="#{pages$requestSearch.sort}"
																		value="#{msg['commons.status']}" arrow="true">
																		<f:attribute name="sortField" value="statusId" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText
																	value="#{pages$requestSearch.isEnglishLocale? dataItem.statusEn:dataItem.statusAr}" />
															</t:column>
															<t:column id="requestGroup" sortable="true"
																style="white-space: normal;">
																<f:facet name="header">
																	<t:outputText value="#{msg['member.groupName']}" />
																</f:facet>
																<t:outputText
																	value="#{pages$requestSearch.isEnglishLocale?dataItem.groupsNameEn:dataItem.groupsNameAr}" />
															</t:column>

															<t:column id="requestTypeCol"
																style="white-space: normal;">
																<f:facet name="header">
																	<t:outputText value="#{msg['request.type']}" />
																</f:facet>
																<t:outputText
																	value="#{pages$requestSearch.isEnglishLocale? dataItem.requestTypeEn:dataItem.requestTypeAr}" />
															</t:column>

															<t:column id="createdByCol" style="white-space: normal;">
																<f:facet name="header">
																	<t:commandSortHeader columnName="createdByCol"
																		actionListener="#{pages$requestSearch.sort}"
																		value="#{msg['commons.createdBy']}" arrow="true">
																		<f:attribute name="sortField" value="createdBy" />
																	</t:commandSortHeader>

																</f:facet>
																<t:outputText
																	value="#{pages$requestSearch.isEnglishLocale? dataItem.createdByFullName : dataItem.createdByFullNameAr}" />
															</t:column>
															<t:column id="claimedByCol" style="white-space: normal;">
																<f:facet name="header">
																	<t:outputText
																		value="#{msg['TaskList.DataTable.ClaimedBy']}" />
																</f:facet>
																<t:outputText
																	value="#{pages$requestSearch.isEnglishLocale? dataItem.taskAcquiredByNameEn:dataItem.taskAcquiredByNameAr}" />
															</t:column>

															<pims:security screen="Pims.Home.SearchRequest.Actions"
																action="create">
																<t:column id="actionCol" sortable="false" width="80">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.action']}" />
																	</f:facet>
																	<t:commandLink
																		action="#{pages$requestSearch.fetchHumanTask}"
																		rendered="true">
																		<h:graphicImage id="taskIcon"
																			title="#{msg['requestSearch.fetchPendingTask']}"
																			url="../resources/images/app_icons/Claim.png" />&nbsp;
														</t:commandLink>
																	<t:outputLabel value="  " rendered="true"></t:outputLabel>
																	<t:commandLink
																		action="#{pages$requestSearch.approvalRequiredAction}"
																		rendered="#{dataItem.approvalRequiredMode}">
																		<h:graphicImage rendered="false"
																			id="approvalRequiredIcon"
																			title="#{msg['request.approvalRequired']}"
																			url="../resources/images/app_icons/Approve-Request.png" />&nbsp;
														</t:commandLink>
																	<t:outputLabel value="  " rendered="true"></t:outputLabel>
																	<t:commandLink
																		action="#{pages$requestSearch.technicalCommentsRequiredAction}"
																		rendered="#{dataItem.technicalCommentsRequiredMode}">
																		<h:graphicImage id="technicalCommentsRequiredIcon"
																			title="#{msg['request.technicalCommentsRequired']}"
																			url="../resources/images/app_icons/Provide-Tecnical-Comments.png" />&nbsp;
														</t:commandLink>
																	<t:outputLabel value="  " rendered="true"></t:outputLabel>
																	<t:commandLink id="detailsAction"
																		action="#{pages$requestSearch.detailsAction}"
																		rendered="true">
																		<h:graphicImage id="detailsIcon"
																			title="#{msg['commons.details']}"
																			url="../resources/images/app_icons/Request-detail.png" />&nbsp;
														</t:commandLink>
																	<t:outputLabel value="  " rendered="true"></t:outputLabel>
																	<t:commandLink
																		action="#{pages$requestSearch.printReport}"
																		rendered="true">
																		<h:graphicImage id="printIcon"
																			title="#{msg['commons.print']}"
																			url="../resources/images/app_icons/print.gif" />&nbsp;
														</t:commandLink>
																</t:column>
															</pims:security>
														</t:dataTable>
													</div>
													<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
														style="width:99%;#width:99%;">
														<table cellpadding="0" cellspacing="0" width="99%">
															<tr>
																<td class="RECORD_NUM_TD">
																	<div class="RECORD_NUM_BG">
																		<table cellpadding="0" cellspacing="0"
																			style="width: 182px; # width: 150px;">
																			<tr>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText value=" : " />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{pages$requestSearch.recordSize}" />
																				</td>
																			</tr>
																		</table>
																	</div>
																</td>
																<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																	align="right">

																	<t:div styleClass="PAGE_NUM_BG" style="#width:20%">
																		<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																		<table cellpadding="0" cellspacing="0">
																			<tr>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																				</td>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{pages$requestSearch.currentPage}" />
																				</td>
																			</tr>
																		</table>
																	</t:div>
																	<TABLE border="0" class="SCH_SCROLLER">
																		<tr>
																			<td>
																				<t:commandLink
																					action="#{pages$requestSearch.pageFirst}"
																					disabled="#{pages$requestSearch.firstRow == 0}">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$requestSearch.pagePrevious}"
																					disabled="#{pages$requestSearch.firstRow == 0}">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:dataList value="#{pages$requestSearch.pages}"
																					var="page">
																					<h:commandLink value="#{page}"
																						actionListener="#{pages$requestSearch.page}"
																						rendered="#{page != pages$requestSearch.currentPage}" />
																					<h:outputText value="<b>#{page}</b>" escape="false"
																						rendered="#{page == pages$requestSearch.currentPage}" />
																				</t:dataList>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$requestSearch.pageNext}"
																					disabled="#{pages$requestSearch.firstRow + pages$requestSearch.rowsPerPage >= pages$requestSearch.totalRows}">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$requestSearch.pageLast}"
																					disabled="#{pages$requestSearch.firstRow + pages$requestSearch.rowsPerPage >= pages$requestSearch.totalRows}">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblL"></t:graphicImage>
																				</t:commandLink>
																			</td>
																		</tr>
																	</TABLE>

																</td>
															</tr>
														</table>
													</t:div>
												</div>
											</div>
											</div>
										</h:form>

									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

		</body>
	</html>
</f:view>