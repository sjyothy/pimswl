

<t:div style="width:95%;padding-top:5px;padding-botton:5px;">
	<%--
	<t:div styleClass="BUTTON_TD">
		<h:commandButton styleClass="BUTTON" type="button"
			rendered="#{
						pages$endowmentPrograms.showSaveButton || 
						pages$endowmentPrograms.showResubmitButton 
					   }"
			value="#{msg['commons.saveButton']}" onclick="openSearchBoxPopup();">
		</h:commandButton>

	</t:div>
--%>
	<t:div styleClass="contentDiv"
		style="width:95%;">
		<t:dataTable id="programBoxesGrid"
		    binding="#{pages$endowmentPrograms.dataTableBox}"
			value="#{pages$endowmentPrograms.donationBoxes}"
			preserveDataModel="true" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
			width="100%">


			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['donationBox.label.boxNumber']}" />
				</f:facet>
				<h:outputText style="white-space: normal;" id="boxNum"
					value="#{dataItem.boxNum}" />
			</t:column>

			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['donationBox.label.place']}" />
				</f:facet>

				<h:outputText style="white-space: normal;" id="boxPlaceSubTypeAr"
					value="#{dataItem.boxPlaceSubTypeAr}" />
			</t:column>

			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['donationBox.label.community']}" />
				</f:facet>

				<h:outputText style="white-space: normal;" id="communityAr"
					value="#{dataItem.communityAr}" />
			</t:column>

			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['donationBox.label.address']}" />
				</f:facet>

				<h:outputText style="white-space: normal;" id="boxaddress"
					value="#{dataItem.address}" />
			</t:column>
			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['donationBox.label.contactInfo']}" />
				</f:facet>

				<h:outputText style="white-space: normal;" id="boxcontactInfo"
					value="#{dataItem.contactPersonNameNum}" />
			</t:column>
			<%-- 
			<t:column id="actionGrid" width="5%" style="white-space: normal;"
				sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.action']}" />
				</f:facet>
				<t:commandLink
					rendered="#{
								pages$endowmentPrograms.showSaveButton || 
								pages$endowmentPrograms.showResubmitButton 
							   }"
					onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;"
					action="#{pages$endowmentPrograms.onDeleteBox}">
					<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
						url="../resources/images/delete_icon.png" />
				</t:commandLink>


			</t:column>
--%>
		</t:dataTable>
	</t:div>
</t:div>