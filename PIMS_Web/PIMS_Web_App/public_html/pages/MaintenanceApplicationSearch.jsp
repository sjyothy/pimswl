<%-- 
  - Author: Ahmed Faraz
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Requests
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<%-- 
		<% UIViewRoot viewRoot = (UIViewRoot)session.getAttribute("view");
           if (viewRoot.getLocale().toString().equals("en"))
           { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>
        <%
            } 
            else
            { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_ar.js"></script>
        <%
            } 
        %>
--%>
<script language="JavaScript" type="text/javascript">

	    function resetValues()
      	{
      	    document.getElementById("searchFrm:requestStatuses").selectedIndex=0;
      	    document.getElementById("searchFrm:requestTypes").selectedIndex=0;
		    document.getElementById("searchFrm:contractNumber").value="";
			document.getElementById("searchFrm:tenantNumber").value="";
			document.getElementById("searchFrm:requestNumber").value="";
			$('searchFrm:requestDateTo').component.resetSelectedDate();
			$('searchFrm:requestDateFrom').component.resetSelectedDate();
			document.getElementById("searchFrm:state").selectedIndex=0;
      	    document.getElementById("searchFrm:country").selectedIndex=0;
      	    document.getElementById("searchFrm:selectTenantType").selectedIndex=0;
      	    document.getElementById("searchFrm:selectContractType").selectedIndex=0;

		    document.getElementById("searchFrm:tenantName").value="";
			document.getElementById("searchFrm:contractValue").value="";
			document.getElementById("searchFrm:requestNumber").value="";
		
        }
        
       function submitForm()
	   {
          document.getElementById('searchFrm').submit();
          document.getElementById("searchFrm:country").selectedIndex=0;
	   }
	   
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="Maintenance Request Search"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td width="100%" height="466px" valign="top">
								<h:form id="searchFrm" style="WIDTH: 97.6%;">
									<div class="SCROLLABLE_SECTION  AUC_SCH_SS">
										
											<table border="0" class="layoutTable">
												<tr>
													<td>
														<h:outputText value="#{pages$MaintenanceApplicationSearch.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />
													</td>
												</tr>
											</table>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td>
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td>
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['request.number']}:"></h:outputLabel>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:inputText id="requestNumber"
																	value="#{pages$MaintenanceApplicationSearch.requestView.requestNumber}"
																	style="width: 186px;" maxlength="20"></h:inputText>
															</td>
														</tr>
														<tr>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['request.fromDate']}:"></h:outputLabel>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<rich:calendar id="requestDateFrom"
																	value="#{pages$MaintenanceApplicationSearch.fromDate}"
																	locale="#{pages$MaintenanceApplicationSearch.locale}" popup="true"
																	datePattern="dd/MM/yyyy" showApplyButton="false"
																	enableManualInput="false" cellWidth="24px"
																	cellHeight="22px" style="width:186px; height:16px" />
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['request.toDate']}:" />
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<%--			<h:inputText id="requestDateTo"
															value="#{pages$requestSearch.toDate}"
															style="width: 186px;"></h:inputText>--%>
																<rich:calendar id="requestDateTo"
																	value="#{pages$MaintenanceApplicationSearch.toDate}"
																	locale="#{pages$MaintenanceApplicationSearch.locale}" popup="true"
																	enableManualInput="false" datePattern="dd/MM/yyyy"
																	showApplyButton="false" cellWidth="24px"
																	cellHeight="22px" style="width:186px; height:16px" />
															</td>
														</tr>

														<tr>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['request.type']}:"></h:outputLabel>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:selectOneMenu id="requestTypes" readonly="true"
																	binding="#{pages$MaintenanceApplicationSearch.requestTypeCombo}"
																	style="width: 192px;" required="false"
																	value="#{pages$MaintenanceApplicationSearch.requestTypeId}">
																	<f:selectItem itemValue="0"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$MaintenanceApplicationSearch.requestTypes}" />
																</h:selectOneMenu>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['request.status']}:"></h:outputLabel>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:selectOneMenu id="requestStatuses"
																	binding="#{pages$MaintenanceApplicationSearch.requestStatusCombo}"
																	style="width: 192px;" required="false"
																	value="#{pages$MaintenanceApplicationSearch.statusId}">
																	<f:selectItem itemValue="0"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.requestStatusList}" />
																</h:selectOneMenu>
															</td>
														</tr>
														<tr>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.contractNumber']}:"></h:outputLabel>
															</td>

															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:inputText id="contractNumber"
																	value="#{pages$MaintenanceApplicationSearch.requestView.contractView.contractNumber}"
																	style="width: 186px;" maxlength="20">
																</h:inputText>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.replaceCheque.contractType']}:"></h:outputLabel>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:selectOneMenu id="selectContractType"
																	style="width: 192px;"
																	value="#{pages$MaintenanceApplicationSearch.selectOneContractType}">
																	<f:selectItem
																		itemLabel="#{msg['commons.pleaseSelect']}"
																		itemValue="0" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.contractType}" />
																</h:selectOneMenu>

															</td>

														</tr>
														<tr>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.replaceCheque.totalContractValue']}:"></h:outputLabel>
															</td>

															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:inputText id="contractValue"
																	value="#{pages$MaintenanceApplicationSearch.requestView.contractView.rentAmount}"
																	style="width: 186px;" maxlength="20">
																</h:inputText>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenants.name']}:"></h:outputLabel>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:inputText id="tenantName"
																	value="#{pages$MaintenanceApplicationSearch.requestView.tenantsView.firstName}"
																	style="width: 186px;" maxlength="20"></h:inputText>
															</td>
														</tr>
														<%--
														<tr>

															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.TenantsNumber']}:"></h:outputLabel>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																			<h:inputText id="tenantNumber"
														value="#{pages$requestSearch.requestView.tenantsView.tenantNumber}"
														style="width: 186px;" maxlength="20"></h:inputText>
															</td>

															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 9px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel value="#{msg['tenants.tenantsType']}:"></h:outputLabel>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:selectOneMenu id="selectTenantType"
																	style="width: 192px;"
																	value="#{pages$MaintenanceApplicationSearch.selectOneTenantType}">
																	<f:selectItem
																		itemLabel="#{msg['commons.pleaseSelect']}"
																		itemValue="0" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.tenantType}" />
																</h:selectOneMenu>

															</td>

														</tr>
														
														<tr>

															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 10px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel
																	value="#{msg['receiveProperty.emirate']}:"></h:outputLabel>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:selectOneMenu id="country" tabindex="2"
																	style="width: 192px;"
																	value="#{pages$MaintenanceApplicationSearch.selectOneState}"
																	onchange="javascript:submitForm();"
																	valueChangeListener="#{pages$MaintenanceApplicationSearch.loadStates}">
																	<f:selectItem itemValue="0"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems value="#{pages$MaintenanceApplicationSearch.countries}" />
																</h:selectOneMenu>
															</td>


															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 10px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:outputLabel value="#{msg['receiveProperty.city']}:"></h:outputLabel>
															</td>
															<td
																style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																<h:selectOneMenu id="state" tabindex="5"
																	style="width: 192px;"
																	value="#{pages$MaintenanceApplicationSearch.selectOneCity}">
																	<f:selectItem itemValue="0"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems value="#{pages$MaintenanceApplicationSearch.states}" />
																</h:selectOneMenu>
															</td>



														</tr>

--%>

														<tr>
															<td class="BUTTON_TD JUG_BUTTON_TD" colspan="4">
																<table cellpadding="1px" cellspacing="1px">
																	<tr>
																		<td>
																			<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.clear']}"
																				onclick="javascript:resetValues();"
																				style="width: 75px" tabindex="7"></h:commandButton>
																		</td>
																		<td>
																			<pims:security
																				screen="Pims.Home.SearchRequest.Search"
																				action="create">
																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.search']}"
																					action="#{pages$MaintenanceApplicationSearch.searchRequests}"
																					style="width: 75px" tabindex="7"></h:commandButton>
																			</pims:security>
																		</td>
																		<td>
																			<pims:security
																				screen="Pims.Home.SearchRequest.Search"
																				action="create">
																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.add']}"
																					action="#{pages$MaintenanceApplicationSearch.gotoAddRequest}"
																					style="width: 75px" tabindex="7"></h:commandButton>
																			</pims:security>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>

												</div>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv">
													<t:dataTable id="dt1"
														value="#{pages$MaintenanceApplicationSearch.dataList}"
														binding="#{pages$MaintenanceApplicationSearch.dataTable}"
														rows="#{pages$MaintenanceApplicationSearch.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%">

														<t:column id="requestNumberCol" width="100"
															sortable="true">
															<f:facet name="header">
																<t:outputText id="ot1" value="#{msg['request.numberCol']}" />
															</f:facet>
															<t:outputText  id="ot2" value="#{dataItem.requestNumber}" />
														</t:column>
														<t:column id="requestDateCol" width="75" sortable="true">
															<f:facet name="header">
																<t:outputText  id="ot3" value="#{msg['commons.date']}" />
															</f:facet>
															<t:outputText  id="ot4" value="#{dataItem.requestDateString}" />
														</t:column>
														<t:column id="tenantNameCol" width="100" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['tenants.name']}" />
															</f:facet>
															<t:outputText value="#{dataItem.tenantsView.firstName}" />
														</t:column>
														<t:column id="contractNumberCol" width="100"
															sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.contractNumber']}" />
															</f:facet>
															<t:outputText
																value="#{dataItem.contractView.contractNumber}" />
														</t:column>
														
														
														<t:column id="requestTypeCol" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['request.type']}" />
															</f:facet>
															<t:outputText value="#{dataItem.requestType}" />
														</t:column>
														<t:column id="createdByCol" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.createdBy']}" />
															</f:facet>
															<t:outputText value="#{dataItem.createdBy}" />
														</t:column>
														<t:column id="requestStatusCol" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.status']}" />
															</f:facet>
															<t:outputText value="#{dataItem.status}" />
														</t:column>
														
															<t:column id="actionCol" sortable="false" width="60">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.action']}" />
																</f:facet>
																<t:commandLink
																	action="#{pages$MaintenanceApplicationSearch.approvalRequiredAction}"
																	rendered="#{dataItem.approvalRequiredMode}">
																	<h:graphicImage id="approvalRequiredIcon"
																		title="#{msg['request.approvalRequired']}"
																		url="../resources/images/app_icons/Approve-Request.png" />&nbsp;
														</t:commandLink>
																<t:outputLabel value="  " rendered="true"></t:outputLabel>
																<t:commandLink
																	action="#{pages$MaintenanceApplicationSearch.technicalCommentsRequiredAction}"
																	rendered="#{dataItem.technicalCommentsRequiredMode}">
																	<h:graphicImage id="technicalCommentsRequiredIcon"
																		title="#{msg['request.technicalCommentsRequired']}"
																		url="../resources/images/app_icons/Provide-Tecnical-Comments.png" />&nbsp;
														</t:commandLink>
																<t:outputLabel value="  " rendered="true"></t:outputLabel>
																<t:commandLink
																	action="#{pages$MaintenanceApplicationSearch.detailsAction}"
																	rendered="true">
																	<h:graphicImage id="detailsIcon"
																		title="#{msg['commons.details']}"
																		url="../resources/images/app_icons/Request-detail.png" />&nbsp;
														</t:commandLink>
															</t:column>
														
													</t:dataTable>
												</div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:100%;">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$MaintenanceApplicationSearch.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 50%">
																<t:dataScroller id="scroller" for="dt1" paginator="true"
																	fastStep="1"
																	paginatorMaxPages="#{pages$MaintenanceApplicationSearch.paginatorMaxPages}"
																	immediate="false" paginatorTableClass="paginator"
																	renderFacetsIfSinglePage="true"
																	paginatorTableStyle="grid_paginator"
																	layout="singleTable"
																	paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																	paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																	pageIndexVar="pageNumber" styleClass="SCH_SCROLLER">

																	<f:facet name="first">
																		<t:graphicImage url="../#{path.scroller_first}"
																			id="lblF"></t:graphicImage>
																	</f:facet>
																	<f:facet name="fastrewind">
																		<t:graphicImage url="../#{path.scroller_fastRewind}"
																			id="lblFR"></t:graphicImage>
																	</f:facet>
																	<f:facet name="fastforward">
																		<t:graphicImage url="../#{path.scroller_fastForward}"
																			id="lblFF"></t:graphicImage>
																	</f:facet>
																	<f:facet name="last">
																		<t:graphicImage url="../#{path.scroller_last}"
																			id="lblL"></t:graphicImage>
																	</f:facet>
																	<div class="PAGE_NUM_BG" style="width: 100%;">
																		<table cellpadding="0" cellspacing="0">
																			<tr>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																				</td>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</td>
																			</tr>
																		</table>
																		<div>
																</t:dataScroller>

															</td>
														</tr>
													</table>
												</t:div>
											</div>
									</div>

									</h:form>
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</body>
	</html>
</f:view>