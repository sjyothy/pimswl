<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">

	function openLoadReportPopup(pageName) 	{	
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />


			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />





		</head>

		<body>
			<div style="width: 1024px;">


				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>


					</tr>
					<tr width="100%">

						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['listReport.landBuildingTemplate.reportSearchPage.heading']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg">
									</td>
									<td width="100%" height="99%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION" style="height: 450px;">

											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$reportLandBuildingTemplate.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>


											<h:form id="searchFrm">

												<div class="MARGIN" style="width: 96%">
													<table cellpadding="0" cellspacing="0">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td style="FONT-SIZE: 0px" width="100%">
																<IMG
																	src="../<h:outputText value="#{path.img_section_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="DETAIL_SECTION" style="width: 99.6%">
														<h:outputLabel value="#{msg['commons.report.criteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="0px" cellspacing="0px"
															class="DETAIL_SECTION_INNER" border="0">
															<tr>
																<td width="97%">
																	<table width="100%">
																		<tr>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.property.Name']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="txPropertyName"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtPropertyName}"
																					styleClass="INPUT_TEXT" maxlength="250"></h:inputText>
																			</td>
																			
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['inspection.propertyRefNum']} :"></h:outputLabel>
																			</td>

																			<td>


																				<h:inputText id="txtPropertyNumber"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtPropertyNumber}"
																					styleClass="INPUT_TEXT" maxlength="20"></h:inputText>

																			</td>
																	

																		</tr>
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['property.type']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:selectOneMenu id="cboPropertyType"
																					required="false"
																					value="#{pages$reportLandBuildingTemplate.criteria.cboPropertyType}">

																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyTypeList}" />
																				</h:selectOneMenu>
																			</td>

																			
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['property.status']} :" />
																			</td>
																			<td>
																				<h:selectOneMenu id="cboPropertyStatus" required="false"
																					value="#{pages$reportLandBuildingTemplate.criteria.cboPropertyStatus}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyStatusList}" />
																				</h:selectOneMenu>
																			</td>

																		</tr>

																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.property.usageType']} :" />

																			</td>
																			<td>
																				<h:selectOneMenu id="cboPropertyUsage"
																					required="false"
																					value="#{pages$reportLandBuildingTemplate.criteria.cboPropertyUsage}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyUsageList}" />
																				</h:selectOneMenu>

																			</td>

																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.contractNumber']} :"></h:outputLabel>
																			</td>
																			<td>

																				<h:inputText id="txtContractNumber"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtContractNumber}"
																					maxlength="20"></h:inputText>
																			</td>

																		</tr>


																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL" value="#{msg['commons.fromDate']} :"></h:outputLabel>
																			</td>
																			<td>




																				<rich:calendar id="fromDate" popup="true"
																					datePattern="#{pages$reportLandBuildingTemplate.dateFormat}"
																					locale="#{pages$reportLandBuildingTemplate.locale}"
																					value="#{pages$reportLandBuildingTemplate.criteria.clndrFromDate}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width: 170px; height: 14px"></rich:calendar>
																			</td>


																			<td>

																				<h:outputLabel styleClass="LABEL" value="#{msg['commons.toDate']} :"></h:outputLabel>
																			</td>
																			<td>

																				<rich:calendar id="toDate"
																					datePattern="#{pages$reportLandBuildingTemplate.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$reportLandBuildingTemplate.locale}"
																					value="#{pages$reportLandBuildingTemplate.criteria.cldrToDate}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px">

																				</rich:calendar>
																			</td>

																		</tr>
																		<tr>
																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.landBuildingTemplate.community']} :" />

																			</td>
																			<td>
																				<h:selectOneMenu id="cboPropertyCommunity"
																					required="false"
																					value="#{pages$reportLandBuildingTemplate.criteria.cboPropertyCommunity}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems value="#{view.attributes['city']}" />
																				</h:selectOneMenu>
																			</td>

																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['property.landNumber']} :"></h:outputLabel>
																			</td>
																			<td>

																				<h:inputText id="landNmuberInput"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtLandNumber}" maxlength="30"></h:inputText>
																			</td>

																		</tr>
																		<tr>
																		
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.landBuildingTemplate.ownerName']} :"></h:outputLabel>


																			</td>
																			<td>


																				<h:inputText id="txtOwnerName"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtOwnerName}"
																					maxlength="20"></h:inputText>
																			</td>
																		
																		
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.landBuildingTemplate.ownerNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtOwnerNo"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtOwnerNo}"
																					maxlength="20"></h:inputText>
																			</td>
																		
																			
																	

																		</tr>
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.contractAmountFrom']} :"></h:outputLabel>
																			</td>

																			<td>


																				<h:inputText id="txtContractAmountFrom"
																					value="#{pages$reportLandBuildingTemplate.criteria.contractRentAmountFrom}"
																					styleClass="INPUT_TEXT" maxlength="20"></h:inputText>

																			</td>


																			<td>


																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.contractAmountTo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtContractAmountTo"
																					value="#{pages$reportLandBuildingTemplate.criteria.contractRentAmountTo}"
																					maxlength="20"></h:inputText>
																			</td>
																		</tr>


																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.landBuildingTemplate.nationality']} :" />

																			</td>
																			<td>
																				<h:selectOneMenu id="cboOwnerNationality"
																					required="false"
																					value="#{pages$reportLandBuildingTemplate.criteria.cboOwnerNationality}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.countryList}" />
																				</h:selectOneMenu>
																			</td>



																			<td>


																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.landBuildingTemplate.visaNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtOwnerVisaNo"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtOwnerVisaNo}"
																					maxlength="30"></h:inputText>
																			</td>


																		</tr>

																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.landBuildingTemplate.tradeLNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtOwnerTradeLNo"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtOwnerTradeLNo}"
																					maxlength="10"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.landBuildingTemplate.phoneNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtOwnerPhoneNo"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtOwnerPhoneNo}"
																					maxlength="30"></h:inputText>
																			</td>


																		</tr>


																		
																		<tr>
								
																			
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.address']} :" />
																			</td>
																			<td>
																				<h:inputText id="txtPropertyAddress"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtPropertyAddress}"
																					maxlength="250"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.landBuildingTemplate.nidNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtOwnerNidNo"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtOwnerNidNo}"
																					maxlength="20"></h:inputText>
																			</td>


																		</tr>


																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.landBuildingTemplate.dmNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtPropertyDmNo"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtPropertyDmNo}"
																					maxlength="7"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.landBuildingTemplate.propertyDewaAccNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtPropertyDewaAccNo"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtPropertyDewaAccNo}"
																					maxlength="15"></h:inputText>
																			</td>


																		</tr>
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyTotalUnitsMin']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtNoOfUnitsFrom"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtNoOfUnitsFrom}"
																					maxlength="12"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyTotalUnitsMax']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtNoOfUnitsTo"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtNoOfUnitsTo}"
																					maxlength="12"></h:inputText>
																			</td>

																		</tr>
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyTotalFloorsMin']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtNoOfFloorsFrom"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtNoOfFloorsFrom}"
																					maxlength="12"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyTotalFloorsMax']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtNoOfFloorsTo"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtNoOfFloorsTo}"
																					maxlength="12"></h:inputText>
																			</td>

																		</tr>
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyCommercialAreaMin']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtCommercialAreaFrom"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtCommercialAreaFrom}"
																					maxlength="12"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyCommercialAreaMax']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtCommercialAreaTo"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtCommercialAreaTo}"
																					maxlength="12"></h:inputText>
																			</td>

																		</tr>
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyBuiltAreaMin']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtPropertyBuiltAreaSqFtFrom"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtPropertyBuiltAreaSqFtFrom}"
																					maxlength="12"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyBuiltAreaMax']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtPropertyBuiltAreaSqFtTo"
																					value="#{pages$reportLandBuildingTemplate.criteria.txtPropertyBuiltAreaSqFtTo}"
																					maxlength="12"></h:inputText>
																			</td>

																		</tr>

																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.ownershipType']} :" />

																			</td>
																			<td>
																				<h:selectOneMenu id="cboPropertyOwnershipType"
																					required="false"
																					value="#{pages$reportLandBuildingTemplate.criteria.cboPropertyOwnershipType}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyOwnershipType}" />
																				</h:selectOneMenu>
																			</td>






																		</tr>


																		<tr>
																			<td class="BUTTON_TD" colspan="4">
																				<h:commandButton styleClass="BUTTON"
																					id="submitButton"
																					action="#{pages$reportLandBuildingTemplate.cmdView_Click}"
																					immediate="false" value="#{msg['commons.view']}"
																					tabindex="7"></h:commandButton>
																			</td>
																		</tr>

																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>
															</tr>

														</table>


													</div>
												</div>



											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

		</body>
	</html>
</f:view>
