


<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="Cache-Control"
			content="no-cache,must-revalidate,no-store">
		<meta http-equiv="expires" content="0">

		<link rel="stylesheet" type="text/css"
			href="../../resources/css/simple_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../../resources/css/amaf_en.css" />
		<title>PIMS</title>
<script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>
		<script language="javascript" type="text/javascript">
  
       
    </script>
		



	</head>
	<body>

		<f:view>

			<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
				var="msg" />

			<%
				UIViewRoot view = (UIViewRoot) session.getAttribute("view");
					if (view.getLocale().toString().equals("en")) {
			%>
			<body dir="ltr">
				<%
					} else {
				%>
			
			<body dir="rtl">
				<%
					}
				%>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<%
										if (view.getLocale().toString().equals("en")) {
									%>
									<td
										background="../resources/images/Grey panel/Grey-Panel-left-1.jpg"
										height="38" style="background-repeat: no-repeat;" width="100%">
										<font style="font-family: Trebuchet MS; font-weight: regular; font-size: 19px; margin-left: 15px; top: 30px;">
											Tenant's Add
									  </font>
										<!--<IMG src="../resources/images/Grey panel/Grey-Panel-left-1.jpg"/>-->
										<%
											} else {
										%>
									</td>
																		
								     <td width="100%">
										<IMG
											src="../resources/images/ar/Detail/Grey Panel/Grey-Panel-right-1.jpg"></img>
										<%
											}
										%>
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap" 	background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
									
									</td>

									<td width="500px" height="100%" valign="top" nowrap="nowrap">
										<h:form>
											<table>
												<tr>
													<td colspan="6">
														<h:messages></h:messages>
													</td>
												</tr>
												<tr>
													<td>
														<h:outputLabel value="#{msg['commons.referencenumber']}"></h:outputLabel>
													</td>
													<td colspan="5" >
												        <h:inputText  >
												        
												        </h:inputText>
													</td>
												</tr>
												<tr>
													<td >
														<h:outputLabel value="#{msg['tenants.firstname']}"></h:outputLabel>
													</td>
													<td >
												        <h:inputText>
												        
												        </h:inputText>
													</td>
													<td >
														<h:outputLabel value="#{msg['tenants.middlename']}"></h:outputLabel>
													</td>
													<td >
												        <h:inputText>
												        
												        </h:inputText>
													</td>
													<td >
														<h:outputLabel value="#{msg['tenants.lastname']}"></h:outputLabel>
													</td>
													<td >
												        <h:inputText >
												        
												        </h:inputText>
													</td>
												</tr>
												<tr>
													<td >
														<h:outputLabel value="#{msg['tenants.gender']}"></h:outputLabel>
													</td>
													<td >
													    
												        <h:selectOneMenu id="selectGender">
												          <f:selectItem id="selectGenderMale"   itemLabel="#{msg['tenants.male']}"   itemValue="1"/>
												          <f:selectItem id="selectGenderFemale" itemLabel="#{msg['tenants.female']}" itemValue="2"/>
												        </h:selectOneMenu>
													</td>
													<td >
														<h:outputLabel value="#{msg['tenants.dateofbirth']}"></h:outputLabel>
													</td>
													<td >
													<h:inputText id="datetimeDOB"  style="width: 92px; height: 20px"/>
												       <h:graphicImage 
												          style="width: 16;height: 16;border: 0"  
												          url="../resources/images/calendar.gif" 
												          onclick="showCalendar(this,datetimeDOB,'dd/mm/yyyy','',200,360); return false;"
												       />


													</td>
													<td >
														<h:outputLabel value="#{msg['tenants.maritalstatus']}"></h:outputLabel>
													</td>
													<td >
												        <h:selectOneMenu id="selectMaritalStatus">
												          <f:selectItem id="selectMaritalStatusMarried"   itemLabel="#{msg['tenants.married']}"   itemValue="1"/>
												          <f:selectItem id="selectMaritalStatusUnmarried" itemLabel="#{msg['tenants.unmarried']}" itemValue="2"/>
												        </h:selectOneMenu>
													</td>
												</tr>
												




											</table>

																					</h:form>
			</body>
		</f:view>
</html>
