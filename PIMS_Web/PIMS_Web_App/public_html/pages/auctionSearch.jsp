<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Auction
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	    function resetValues()
      	{
      		document.getElementById("searchFrm:auctionNumber").value="";
			document.getElementById("searchFrm:auctionTitle").value="";
      	    $('searchFrm:auctionDate').component.resetSelectedDate();
      	    $('searchFrm:auctionCreationDate').component.resetSelectedDate();
      	    $('searchFrm:auctionRegStartDateFrom').component.resetSelectedDate();
      	    $('searchFrm:auctionRegStartDateTo').component.resetSelectedDate();
      	    $('searchFrm:auctionRegEndDateFrom').component.resetSelectedDate();
      	    $('searchFrm:auctionRegEndDateTo').component.resetSelectedDate();
      	    
      	    document.getElementById("searchFrm:auctionStatuses").selectedIndex=0;
      	    document.getElementById("searchFrm:venueName").value="";
      	    document.getElementById("searchFrm:auctionStartTimeMM").selectedIndex=0;
      	    document.getElementById("searchFrm:auctionStartTimeHH").selectedIndex=0;
      	    document.getElementById("searchFrm:auctionEndTimeMM").selectedIndex=0;
      	    document.getElementById("searchFrm:auctionEndTimeHH").selectedIndex=0;
			document.getElementById("searchFrm:auctionStartTime").value="";
			document.getElementById("searchFrm:auctionEndTime").value="";
			
        }
        function RowDoubleClick(auctionId, auctionNumber, auctionDate, auctionTitle, venueName)
		{ 		      
			 
		      /*
		      window.opener.document.getElementById("auctionRequestForm:auctionID").value = auctionId;
			  window.opener.document.getElementById("auctionRequestForm:aNo").value = auctionNumber;
			  window.opener.document.getElementById("auctionRequestForm:aDate").value = auctionDate;
			  window.opener.document.getElementById("auctionRequestForm:aTitle").value = auctionTitle;
			  window.opener.document.getElementById("auctionRequestForm:aVenue").value = venueName;
			  window.opener.document.getElementById("auctionRequestForm:auctionUnitsOfRequest").value = "";
			  window.opener.resetSelectedUnits();
			  */
			
		  
		  window.opener.populateAuction( auctionId, auctionNumber, auctionDate, auctionTitle, venueName );
		  window.close();
		  
		}
		
		function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	 function openLoadReportPopup(pageName) 
	{	
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
	function closeWindow()
		{
		  window.close();
		}
        
        	  function	openPopUp()
		{
		
		    var width = 300;
            var height = 300;
            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + ",menubar=yes,toolbar=yes,scrollbars=yes,resizable=yes,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

            var child1 = window.open("about:blank", "subWind", windowFeatures);

		      child1.focus();
		}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<%
					response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
						response.setHeader("Pragma", "no-cache"); //HTTP 1.0
						response.setDateHeader("Expires", 0); //prevents caching at the proxy server
				%>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">


					<c:choose>
						<c:when test="${!pages$auctionSearch.isViewModePopUp}">
							<tr
								style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>


					<tr width="100%">

						<c:choose>
							<c:when test="${!pages$auctionSearch.isViewModePopUp}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>

						<td width="83%" height="470px" valign="top"
							class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['auction.searchHeading']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" height="100%">
										<c:choose>
											<c:when test="${!pages$auctionSearch.isViewModePopUp}">
												<div class="SCROLLABLE_SECTION AUC_SCH_SS"
													style="height: 95%; width: 100%; # height: 95%; # width: 100%;">
											</c:when>
										</c:choose>
										<c:choose>
											<c:when test="${pages$auctionSearch.isViewModePopUp}">
												<div class="SCROLLABLE_SECTION AUC_SCH_SS"
													style="height: 70%; width: 100%; # height: 88%; # width: 90%;">
											</c:when>
										</c:choose>
										<h:form id="searchFrm"
											style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
											<t:div styleClass="MESSAGE">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText value="#{pages$auctionSearch.infoMessage}"
																escape="false" styleClass="INFO_FONT" />
															<h:outputText
																value="#{pages$auctionSearch.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</t:div>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">
																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['auction.number']}:"></h:outputLabel>
																		</td>

																		<td width="25%">
																			<h:inputText id="auctionNumber"
																				value="#{pages$auctionSearch.auctionView.auctionNumber}"
																				maxlength="20"></h:inputText>

																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['auction.title']}:"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText id="auctionTitle"
																				value="#{pages$auctionSearch.auctionView.auctionTitle}"
																				maxlength="100"></h:inputText>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['auction.date']}:"></h:outputLabel>
																		</td>
																		<td>
																			<rich:calendar id="auctionDate"
																				value="#{pages$auctionSearch.auctionView.auctionDateVal}"
																				locale="#{pages$auctionSearch.locale}" popup="true"
																				datePattern="#{pages$auctionSearch.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:170px; height:14px;" />
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['auction.creationDate']}:" />
																		</td>
																		<td>
																			<rich:calendar id="auctionCreationDate"
																				value="#{pages$auctionSearch.auctionView.createdOn}"
																				locale="#{pages$auctionSearch.locale}" popup="true"
																				datePattern="#{pages$auctionSearch.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:170px; height:14px;" />
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['auction.requestStartDateFrom']}:"></h:outputLabel>
																		</td>
																		<td>
																			<rich:calendar id="auctionRegStartDateFrom"
																				binding="#{pages$auctionSearch.htmlCalendarRequestStartDateFrom}"
																				locale="#{pages$auctionSearch.locale}" popup="true"
																				datePattern="#{pages$auctionSearch.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:170px; height:14px;" />
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['auction.requestStartDateTo']}:" />
																		</td>
																		<td>
																			<rich:calendar id="auctionRegStartDateTo"
																				binding="#{pages$auctionSearch.htmlCalendarRequestStartDateTo}"
																				locale="#{pages$auctionSearch.locale}" popup="true"
																				datePattern="#{pages$auctionSearch.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:170px; height:14px;" />
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['auction.requestEndDateFrom']}:"></h:outputLabel>
																		</td>
																		<td>
																			<rich:calendar id="auctionRegEndDateFrom"
																				binding="#{pages$auctionSearch.htmlCalendarRequestEndDateFrom}"
																				locale="#{pages$auctionSearch.locale}" popup="true"
																				datePattern="#{pages$auctionSearch.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:170px; height:14px;" />
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['auction.requestEndDateTo']}:" />
																		</td>
																		<td>
																			<rich:calendar id="auctionRegEndDateTo"
																				binding="#{pages$auctionSearch.htmlCalendarRequestEndDateTo}"
																				locale="#{pages$auctionSearch.locale}" popup="true"
																				datePattern="#{pages$auctionSearch.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:170px; height:14px;" />
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['auction.startTime']}:"></h:outputLabel>
																		</td>
																		<td>
																			<table>
																				<tr>
																					<td>
																						<h:selectOneMenu id="auctionStartTimeHH"
																							style="width: 40px" required="false"
																							value="#{pages$auctionSearch.hhStart}">
																							<f:selectItem itemValue="" itemLabel="hh" />
																							<f:selectItems
																								value="#{pages$ApplicationBean.hours}" />
																						</h:selectOneMenu>
																					</td>
																					<td>
																						<h:selectOneMenu id="auctionStartTimeMM"
																							style="width: 40px" required="false"
																							value="#{pages$auctionSearch.mmStart}">
																							<f:selectItem itemValue="" itemLabel="mm" />
																							<f:selectItems
																								value="#{pages$ApplicationBean.minutes}" />
																						</h:selectOneMenu>
																					</td>
																				</tr>
																			</table>
																			<%-- 	<h:inputText id="auctionStartTime"
														value="#{pages$auctionSearch.auctionView.auctionStartTime}"
														 maxlength="5"></h:inputText> --%>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['auction.endTime']}:" />
																		</td>
																		<td>
																			<table>
																				<tr>
																					<td>
																						<h:selectOneMenu id="auctionEndTimeHH"
																							style="width: 40px;" required="false"
																							value="#{pages$auctionSearch.hhEnd}">
																							<f:selectItem itemValue="" itemLabel="hh" />
																							<f:selectItems
																								value="#{pages$ApplicationBean.hours}" />
																						</h:selectOneMenu>
																					</td>
																					<td>
																						<h:selectOneMenu id="auctionEndTimeMM"
																							style="width: 40px;" required="false"
																							value="#{pages$auctionSearch.mmEnd}">
																							<f:selectItem itemValue="" itemLabel="mm" />
																							<f:selectItems
																								value="#{pages$ApplicationBean.minutes}" />
																						</h:selectOneMenu>
																					</td>
																				</tr>
																			</table>

																			<%-- 	<h:inputText id="auctionEndTime"
														value="#{pages$auctionSearch.auctionView.auctionEndTime}"
														 maxlength="5"></h:inputText> --%>
																		</td>

																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['auction.venue']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="venueName"
																				value="#{pages$auctionSearch.auctionView.auctionVenue.venueName}"
																				maxlength="50"></h:inputText>
																		</td>
																		<td>
																			<c:choose>

																				<c:when
																					test="${!pages$auctionSearch.isViewModePopUp}">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['auction.status']}:"></h:outputLabel>
																				</c:when>
																			</c:choose>
																		</td>

																		<td>

																			<c:choose>

																				<c:when
																					test="${!pages$auctionSearch.isViewModePopUp}">
																					<h:selectOneMenu id="auctionStatuses"
																						required="false"
																						value="#{pages$auctionSearch.auctionView.auctionStatusId}">
																						<f:selectItem itemValue="0"
																							itemLabel="#{msg['commons.All']}" />
																						<f:selectItems
																							value="#{pages$ApplicationBean.auctionStatusList}" />
																					</h:selectOneMenu>
																				</c:when>
																			</c:choose>

																		</td>
																	</tr>
																	<tr>
																		<c:choose>
																			<c:when test="${pages$auctionSearch.isViewModePopUp}">
																				<td class="BUTTON_TD" colspan="4" />
																			</c:when>
																			<c:otherwise>
																				<td class="BUTTON_TD" colspan="4" />
																			</c:otherwise>
																		</c:choose>
																		<table cellpadding="1px" cellspacing="1px">
																			<tr>



																				<td colspan="2">
																					<pims:security
																						screen="Pims.AuctionManagement.Auction.SearchAuctions"
																						action="create">
																						<h:commandButton styleClass="BUTTON"
																							value="#{msg['commons.search']}"
																							action="#{pages$auctionSearch.searchAuctions}"
																							style="width: 75px" tabindex="7">
																						</h:commandButton>
																					</pims:security>
																					<h:commandButton styleClass="BUTTON"
																						value="#{msg['commons.clear']}"
																						onclick="javascript:resetValues();"
																						style="width: 75px" tabindex="7"></h:commandButton>
																					<h:commandButton styleClass="BUTTON"
																						value="#{msg['commons.cancel']}"
																						style="width: 75px" tabindex="7"
																						rendered="#{pages$auctionSearch.isViewModePopUp}"
																						onclick="javascript:closeWindow();"></h:commandButton>

																				</td>


																			</tr>
																		</table>
																		</td>
																	</tr>
																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>

													</table>
												</div>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" style="width: 100%; # width: 99%;">
													<t:dataTable id="dt1"
														value="#{pages$auctionSearch.auctionDataList}"
														binding="#{pages$auctionSearch.dataTable}"
														rows="#{pages$auctionSearch.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%">

														<t:column id="auctionNumberCol" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="auctionNumberCol"
																	actionListener="#{pages$auctionSearch.sort}"
																	value="#{msg['auction.numberCol']}" arrow="true">
																	<f:attribute name="sortField" value="auctionNumber" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.auctionNumber}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="auctionTitleCol" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="auctionTitleCol"
																	actionListener="#{pages$auctionSearch.sort}"
																	value="#{msg['auction.titleCol']}" arrow="true">
																	<f:attribute name="sortField" value="auctionTitle" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.auctionTitle}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="auctionCreationDateCol" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="auctionCreationDateCol"
																	actionListener="#{pages$auctionSearch.sort}"
																	value="#{msg['auction.creationDateCol']}" arrow="true">
																	<f:attribute name="sortField" value="requestStartDate" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText style="white-space: normal;"
																value="#{dataItem.auctionCreationDateString}" />
														</t:column>
														<t:column id="auctionDateCol" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="auctionDateCol"
																	actionListener="#{pages$auctionSearch.sort}"
																	value="#{msg['auction.date']}" arrow="true">
																	<f:attribute name="sortField" value="requestEndDate" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.auctionDate}"
																style="white-space: normal;" />
														</t:column>
														<t:column id="auctionStartTimeCol" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="auctionStartTimeCol"
																	actionListener="#{pages$auctionSearch.sort}"
																	value="#{msg['auction.startTimeCol']}" arrow="true">
																	<f:attribute name="sortField"
																		value="auctionStartDatetime" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.auctionStartTime}"
																style="white-space: normal;" />
														</t:column>
														<t:column id="auctionEndTimeCol" rendered="false"
															sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="auctionEndTimeCol"
																	actionListener="#{pages$auctionSearch.sort}"
																	value="#{msg['auction.endTimeCol']}" arrow="true">
																	<f:attribute name="sortField"
																		value="auctionEndDatetime" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.auctionEndTime}" />
														</t:column>
														<t:column id="venueNameCol" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="venueNameCol"
																	actionListener="#{pages$auctionSearch.sort}"
																	value="#{msg['auction.venueCol']}" arrow="true">
																	<f:attribute name="sortField" value="venueName" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.auctionVenue.venueName}"
																styleClass="A_LEFT" style="white-space: normal;" />
														</t:column>
														<t:column id="auctionStatusEnCol"
															rendered="#{pages$auctionSearch.isEnglishLocale}"
															sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="auctionStatusEnCol"
																	actionListener="#{pages$auctionSearch.sort}"
																	value="#{msg['auction.statusCol']}" arrow="true">
																	<f:attribute name="sortField" value="statusId" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.auctionStatus}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="auctionStatusArCol"
															rendered="#{pages$auctionSearch.isArabicLocale}"
															sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="auctionStatusArCol"
																	actionListener="#{pages$auctionSearch.sort}"
																	value="#{msg['auction.statusCol']}" arrow="true">
																	<f:attribute name="sortField" value="statusId" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.auctionStatusAr}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="actionCol" sortable="false" width="100"
															style="TEXT-ALIGN: center;">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>
															<pims:security
																screen="Pims.AuctionManagement.Auction.View"
																action="create">
																<t:commandLink
																	action="#{pages$auctionSearch.auctionDetailsViewMode}"
																	rendered="#{dataItem.auctionViewMode && !pages$auctionSearch.isViewModePopUp}">&nbsp;
															<h:graphicImage id="viewIcon"
																		title="#{msg['commons.view']}"
																		url="../resources/images/app_icons/View-Auction.png" />&nbsp;
														</t:commandLink>
															</pims:security>
															<t:outputLabel value="  "></t:outputLabel>
															<pims:security
																screen="Pims.AuctionManagement.Auction.Actions"
																action="create">
																<t:commandLink
																	action="#{pages$auctionSearch.auctionDetailsEditMode}"
																	rendered="#{dataItem.auctionEditMode && !pages$auctionSearch.isViewModePopUp}">
																	<h:graphicImage id="editIcon"
																		title="#{msg['commons.edit']}"
																		url="../resources/images/app_icons/Edit-Auction.png" />&nbsp;
																</t:commandLink>
																<t:commandLink
																	action="#{pages$auctionSearch.printAdvertisment}">
																	<h:graphicImage id="printIcon"
																		title="#{msg['commons.print']}"
																		url="../resources/images/app_icons/print.jpg" />&nbsp;
														</t:commandLink>
																<t:commandLink
																	onclick="if (!confirm('#{msg['auction.messages.confirmCancel']}')) return"
																	action="#{pages$auctionSearch.cancelAuction}"
																	rendered="#{dataItem.auctionCancelMode && !pages$auctionSearch.isViewModePopUp}">
																	<h:graphicImage id="cancelIcon"
																		title="#{msg['commons.cancel']}"
																		url="../resources/images/app_icons/Edit-Auction.png" />&nbsp;
																</t:commandLink>
															</pims:security>
															<t:outputLabel value="  "></t:outputLabel>

															<pims:security
																screen="Pims.AuctionManagement.Auction.Actions"
																action="create">
																<t:commandLink
																	action="#{pages$auctionSearch.auctionDetailsConductMode}"
																	rendered="#{dataItem.auctionConductMode && !pages$auctionSearch.isViewModePopUp}">&nbsp;
															<h:graphicImage id="conductIcon"
																		title="#{msg['auction.conduct']}"
																		url="../resources/images/app_icons/Auction.png" />&nbsp;
														</t:commandLink>
															</pims:security>
															<pims:security
																screen="Pims.AuctionManagement.Auction.Refund" action="create">

																<t:commandLink
																	action="#{pages$auctionSearch.auctionDetailsRefundMode}"
																	rendered="#{dataItem.auctionRefundMode && !pages$auctionSearch.isViewModePopUp}">&nbsp;
															<h:graphicImage id="refundIcon"
																		title="#{msg['auction.refund.heading']}"
																		url="../resources/images/app_icons/Refund-Auction-Deposits.png" />&nbsp;
														</t:commandLink>
															</pims:security>
															<t:column id="col10" sortable="false" width="30">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.select']}" />
																</f:facet>

																<t:commandLink
																	actionListener="#{pages$auctionSearch.auctionDetailsViewMode}"
																	rendered="#{pages$auctionSearch.isViewModePopUp}"
																	onclick='javascript:RowDoubleClick(#{dataItem.auctionId}, "#{dataItem.auctionNumber}", "#{dataItem.auctionDate}", "#{dataItem.auctionTitle}", "#{dataItem.auctionVenue.venueName}"); return false;'>
																	<t:graphicImage id="selectIcon"
																		title="#{msg['commons.select']}"
																		url="../resources/images/app_icons/Select-Auction.png" />
																</t:commandLink>
															<t:commandLink
																	action="#{pages$auctionSearch.printAuctionReport}">&nbsp;
															<h:graphicImage id="printReport"
																		title="#{msg['report.title.auctionBidderReport']}"
																		url="../resources/images/app_icons/Status.png" />&nbsp;
														</t:commandLink>	

															</t:column>
														</t:column>
													</t:dataTable>
												</div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:100%;#width:100%;">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$auctionSearch.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">

																<t:div styleClass="PAGE_NUM_BG" style="#width:20%">
																	<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																			</td>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{pages$auctionSearch.currentPage}" />
																			</td>
																		</tr>
																	</table>
																</t:div>
																<TABLE border="0" class="SCH_SCROLLER">
																	<tr>
																		<td>
																			<t:commandLink
																				action="#{pages$auctionSearch.pageFirst}"
																				disabled="#{pages$auctionSearch.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$auctionSearch.pagePrevious}"
																				disabled="#{pages$auctionSearch.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFR"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:dataList value="#{pages$auctionSearch.pages}"
																				var="page">
																				<h:commandLink value="#{page}"
																					actionListener="#{pages$auctionSearch.page}"
																					rendered="#{page != pages$auctionSearch.currentPage}" />
																				<h:outputText value="<b>#{page}</b>" escape="false"
																					rendered="#{page == pages$auctionSearch.currentPage}" />
																			</t:dataList>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$auctionSearch.pageNext}"
																				disabled="#{pages$auctionSearch.firstRow + pages$auctionSearch.rowsPerPage >= pages$auctionSearch.totalRows}">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$auctionSearch.pageLast}"
																				disabled="#{pages$auctionSearch.firstRow + pages$auctionSearch.rowsPerPage >= pages$auctionSearch.totalRows}">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblL"></t:graphicImage>
																			</t:commandLink>
																		</td>
																	</tr>
																</TABLE>
															</td>
														</tr>
													</table>
												</t:div>
											</div>

										</h:form>

										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<c:choose>
								<c:when test="${!pages$auctionSearch.isViewModePopUp}">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
								</c:when>
							</c:choose>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>