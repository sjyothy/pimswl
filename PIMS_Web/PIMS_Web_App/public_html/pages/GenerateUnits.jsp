<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="javascript" type="text/javascript">
	function closeWindow()
	{
	  //window.opener.document.getElementById('conductAuctionForm:isResultPopupCalled').value = "Y";
	  window.opener.document.forms[0].submit();
	  window.close();
	}
	function numberRangeChanged()
	{
	  
	  var numberOfUnits =  document.getElementById('GenerateUnitsForm:noOfUnits').value;	  
	  var unitFrom =  document.getElementById('GenerateUnitsForm:unitNumberFrom').value;	  
	  if(numberOfUnits != null && numberOfUnits.length >0 && unitFrom != null && unitFrom.length >0)
	  {	  
	   var unitto = parseFloat(numberOfUnits) + parseFloat(unitFrom) - 1;	  
	   document.getElementById('GenerateUnitsForm:unitNumberTo').value = unitto;
	   document.getElementById('GenerateUnitsForm:hdnUnitNumberTo').value = unitto;
	  }
	  else
	  {
	    document.getElementById('GenerateUnitsForm:unitNumberTo').value = null;
	    document.getElementById('GenerateUnitsForm:hdnUnitNumberTo').value = null;
	  }
	}
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
		</head>

		<body class="BODY_STYLE">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<%--<jsp:include page="header.jsp"/>--%>
					</td>
				</tr>

				<tr style="width: 100%">
					<td width="100%" valign="top" class="divBackgroundBody" colspan="2">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['generateUnits.PageHeader']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<div>
							<table width="717px" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">

								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<!--Use this below only for desiging forms Please do not touch other sections -->

									<td valign="top" nowrap="nowrap" height="100%">

										<h:form id="GenerateUnitsForm" style="height:300px;">
											<div class="SCROLLABLE_SECTION" style="height:300px;">
												<table width="100%" border="0">
													<tr>
														<td>
															<h:outputText id="opMsgs"
															value="#{pages$ReceiveProperty.messages}" escape="false" />
														</td>
													</tr>
													<tr>
														<td>
															<h:messages id="errMsgs">
																<h:outputText id="opErrorMsgs" styleClass="ERROR_FONT"
																	value="#{pages$GenerateUnits.errorMessages}"
																	escape="false" />
															</h:messages>
														</td>
													</tr>
													<tr>
														<td>

															<div class="MARGIN">

																<table cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td style="font-size:0pt;">
																			<IMG
																				src="../<h:outputText value="#{path.img_section_left}"/>"
																				class="TAB_PANEL_LEFT" />
																		</td>
																		<td width="100%" style="font-size:0pt;">
																			<IMG
																				src="../<h:outputText value="#{path.img_section_mid}"/>"
																				class="TAB_PANEL_MID" />
																		</td>
																		<td style="font-size:0pt;">
																			<IMG
																				src="../<h:outputText value="#{path.img_section_right}"/>"
																				class="TAB_PANEL_RIGHT" />
																		</td>
																	</tr>
																</table>
																<div class="DETAIL_SECTION" style="width:99.5%;">
																	<h:outputLabel
																		value="#{msg['generateUnits.SectionHeader']}"
																		styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
																<div class="DETAIL_SECTION_INNER" style="width:99.5%;">
																	<t:panelGrid width="90%" columns="4" 	id="unitGrid">
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.floorNumber']}"></h:outputLabel>
																		</t:panelGroup>
																		<h:selectOneMenu id="floorsMenu" style="WIDTH: 202px;"
																			binding="#{pages$ReceiveProperty.floorNumberMenu}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																			<f:selectItems
																				value="#{pages$ReceiveProperty.floorsList}" />

																		</h:selectOneMenu>

																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unitPrefix']}"></h:outputLabel>

																		<h:inputText id="unitPrefix"
																			binding="#{pages$ReceiveProperty.unitPrefix}" />
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unitNo']}"></h:outputLabel>
																		</t:panelGroup>
																		<h:inputText id="unitNo"
																			binding="#{pages$ReceiveProperty.unitNo}" />

																		<h:outputLabel styleClass="LABEL" rendered="false"
																			value="#{msg['receiveProperty.unitNumber']}"></h:outputLabel>

																		<h:inputText id="unitNumber" rendered="false"
																			binding="#{pages$ReceiveProperty.unitNumber}" />

																		<h:outputLabel styleClass="LABEL" rendered="false"
																			value="#{msg['receiveProperty.unitStatus']}"></h:outputLabel>

																		<h:inputText id="unitStatus" readonly="true"
																			styleClass="READONLY"
																			binding="#{pages$ReceiveProperty.unitStatus}"
																			rendered="false" />

																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unitType']}"></h:outputLabel>
																		</t:panelGroup>
																		<h:selectOneMenu id="unitTypeMenu"
																			style="WIDTH: 202px;"
																			binding="#{pages$ReceiveProperty.unitTypeMenu}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																			<f:selectItems
																				value="#{pages$ReceiveProperty.unitTypeList}" />

																		</h:selectOneMenu>
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unitSide']}"></h:outputLabel>
																		</t:panelGroup>
																		<h:selectOneMenu id="unitSideMenu"
																			style="WIDTH: 202px;"
																			binding="#{pages$ReceiveProperty.unitSideMenu}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																			<f:selectItems
																				value="#{pages$ReceiveProperty.unitSideList}" />

																		</h:selectOneMenu>

																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unitUsageType']}"></h:outputLabel>
																		</t:panelGroup>
																		<h:selectOneMenu id="unitUsageTypeMenu"
																			style="WIDTH: 202px;"
																			binding="#{pages$ReceiveProperty.unitUsageTypeMenu}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																			<f:selectItems
																				value="#{pages$ReceiveProperty.unitUsageTypeList}" />

																		</h:selectOneMenu>
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unitInvestmentType']}"></h:outputLabel>
																		</t:panelGroup>
																		<h:selectOneMenu id="unitInvestmentTypeMenu"
																			style="WIDTH: 202px;"
																			binding="#{pages$ReceiveProperty.unitInvestmentTypeMenu}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																			<f:selectItems
																				value="#{pages$ReceiveProperty.unitInvestmentTypeList}" />

																		</h:selectOneMenu>

																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unitDescription']}"></h:outputLabel>

																		<h:inputText id="unitDescription"
																			binding="#{pages$ReceiveProperty.unitDescription}" />







																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.areaInSquare']}"></h:outputLabel>
																		<h:inputText id="areaInSquare"
																			style="text-align: right;"
																			binding="#{pages$ReceiveProperty.areaInSquare}" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.bedRooms']}"></h:outputLabel>
																		<h:inputText id="bedRooms" style="text-align: right;"
																			binding="#{pages$ReceiveProperty.bedRooms}" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.livingRooms']}"></h:outputLabel>
																		<h:inputText id="livingRooms"
																			style="text-align: right;"
																			binding="#{pages$ReceiveProperty.livingRooms}" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.bathRooms']}"></h:outputLabel>
																		<h:inputText id="bathRooms" style="text-align: right;"
																			binding="#{pages$ReceiveProperty.bathRooms}" />



																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unitEWUtilityNo']}"></h:outputLabel>
																		<h:inputText id="unitEWUtilityNumber"
																			binding="#{pages$ReceiveProperty.unitEWUtilityNumber}" />
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.rentValueForUnits']}"></h:outputLabel>
																		</t:panelGroup>
																		<h:inputText id="rentValueForUnits"
																			style="text-align: right;"
																			binding="#{pages$ReceiveProperty.rentValueForUnits}" />

																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unitRemarks']}"></h:outputLabel>
																		<h:inputText id="unitRemarks"
																			binding="#{pages$ReceiveProperty.unitRemarks}" />

																	</t:panelGrid>

																	<t:panelGrid   width="35%">

																		<h:selectBooleanCheckbox
																			style="width: 15%;height: 22px; border: 0px;"
																			binding="#{pages$ReceiveProperty.includeFloorNumber}"
																			value="#{pages$ReceiveProperty.includeFloorNumberVar}">
																			<h:outputText
																				value="#{msg['receiveProperty.unit.addWithFloors']}" />
																		</h:selectBooleanCheckbox>


																	</t:panelGrid>
																	<t:panelGrid  id="unitbuttongrid" width="97.5%"
																		columnClasses="BUTTON_TD" columns="1">

																		<h:commandButton styleClass="BUTTON"
																	
																			value="#{msg['commons.Add']}"
																			action="#{pages$ReceiveProperty.generatePropertyUnits}" />
																	</t:panelGrid>
</div>
																	<t:div style="text-align:center;">
																		<t:div styleClass="contentDiv" style="width:95%">
																			<t:dataTable id="units" rows="15" width="100%"
																				value="#{pages$ReceiveProperty.unitDataList}"
																				binding="#{pages$ReceiveProperty.unitTable}"
																				preserveDataModel="true" preserveSort="false"
																				var="unitDataItem" rowClasses="row1,row2"
																				rules="all" renderedIfEmpty="true">
																				<t:column id="col_unitNo">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['receiveProperty.unitNumberHeading']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{unitDataItem.unitNumber}" />
																				</t:column>

																				<t:column id="col_unitStatus">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['receiveProperty.unitStatusHeading']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{unitDataItem.statusEn}"
																						rendered="#{pages$ReceiveProperty.isEnglishLocale}" />
																					<t:outputText styleClass="A_LEFT"
																						value="#{unitDataItem.statusAr}"
																						rendered="#{pages$ReceiveProperty.isArabicLocale}" />


																				</t:column>
																				<t:column id="col_unitRent">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['receiveProperty.unitRentHeading']}" />
																					</f:facet>
																					<t:outputText styleClass="A_RIGHT"
																						style="width: 20%;"
																						value="#{unitDataItem.rentValue}" />
																				</t:column>
																				<t:column id="col_unitType">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['receiveProperty.unitTypeHeading']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{unitDataItem.unitTypeEn}"
																						rendered="#{pages$ReceiveProperty.isEnglishLocale}" />
																					<t:outputText styleClass="A_LEFT"
																						rendered="#{pages$ReceiveProperty.isArabicLocale}"
																						value="#{unitDataItem.unitTypeAr}" />
																				</t:column>
																				<t:column
																					>
																					<f:facet name="header">
																						<t:outputText id="lbl_edit2"
																							value="#{msg['commons.edit']}" />
																					</f:facet>
																				
																					<h:commandLink
																						action="#{pages$ReceiveProperty.editUnit}">
																						<h:graphicImage id="edit_Icon42"
																							title="#{msg['commons.edit']}"
																							url="../resources/images/conduct.gif" />
																					</h:commandLink>
																				

																					<h:commandLink id="deleteIconRCVUnit"
																						action="#{pages$ReceiveProperty.deleteUnit}">
																						<h:graphicImage id="delete_IconRCVUnit"
																							title="#{msg['commons.delete']}"
																							url="../resources/images/delete_icon.png" />
																					</h:commandLink>

																				</t:column>

																			</t:dataTable>
																		</t:div>
																		<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																			style="width:96%;">
																			<t:panelGrid cellpadding="0" cellspacing="0"
																				width="100%" columns="2"
																				columnClasses="RECORD_NUM_TD">

																				<t:div styleClass="RECORD_NUM_BG">
																					<t:panelGrid cellpadding="0" cellspacing="0"
																						columns="3" columnClasses="RECORD_NUM_TD">

																						<h:outputText
																							value="#{msg['commons.recordsFound']}" />

																						<h:outputText value=" : "
																							styleClass="RECORD_NUM_TD" />

																						<h:outputText
																							value="#{pages$ReceiveProperty.unitRecordSize}"
																							styleClass="RECORD_NUM_TD" />

																					</t:panelGrid>
																				</t:div>

																				<t:dataScroller id="unitScroller" for="units"
																					paginator="true" fastStep="1"
																					paginatorMaxPages="10" immediate="false"
																					styleClass="SCH_SCROLLER"
																					paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true"
																					paginatorTableStyle="grid_paginator"
																					layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																					paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																					pageIndexVar="unitPageNumber">

																					<f:facet name="first">
																						<t:graphicImage url="../#{path.scroller_first}"
																							id="lblFunit"></t:graphicImage>
																					</f:facet>
																					<f:facet name="fastrewind">
																						<t:graphicImage
																							url="../#{path.scroller_fastRewind}"
																							id="lblFRunit"></t:graphicImage>
																					</f:facet>
																					<f:facet name="fastforward">
																						<t:graphicImage
																							url="../#{path.scroller_fastForward}"
																							id="lblFFunit"></t:graphicImage>
																					</f:facet>
																					<f:facet name="last">
																						<t:graphicImage url="../#{path.scroller_last}"
																							id="lblLunit"></t:graphicImage>
																					</f:facet>
																					<t:div styleClass="PAGE_NUM_BG">
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.unitPageNumber}" />
																					</t:div>
																				</t:dataScroller>

																			</t:panelGrid>
																		</t:div>
																	</t:div>
																	<div>
																		<h:inputHidden id="hdnPropertyId"
																			binding="#{pages$GenerateUnits.hdnTextPropertyId}"></h:inputHidden>
																		<h:inputHidden id="hdnPropertyName"
																			binding="#{pages$GenerateUnits.hdnTextPropertyName}"></h:inputHidden>
																		<h:inputHidden id="hdnInvestmentPuposeId"
																			binding="#{pages$GenerateUnits.hdnTextInvestmentPurposeId}"></h:inputHidden>
																		<h:inputHidden id="hdnUsageTypeId"
																			binding="#{pages$GenerateUnits.hdnTextUsageTypeId}"></h:inputHidden>
																		<h:inputHidden id="hdnTypeId"
																			binding="#{pages$GenerateUnits.hdnTextTypeId}"></h:inputHidden>
																		<h:inputHidden id="hdnUnitNumberTo"
																			binding="#{pages$GenerateUnits.hdnTextUnitNumberTo}"></h:inputHidden>
																	</div>
																</div>
										</h:form>

									</td>
								</tr>

								<tr>
									<td>

									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
					</td>
				</tr>
			</table>
		</body>
	</html>
</f:view>
