<%-- 
  - Author:Muhammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for distributing revenues amongst beneficiaries.
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		
	            
	function onChkChange()
    {
      document.getElementById('frm:disablingDiv').style.display='none';
    }
    function onSelectionChanged(changedChkBox)
	{
	    document.getElementById('frm:disablingDiv').style.display='block';
		changedChkBox.nextSibling.onclick();
	}
	function closeWindowSubmit()
	{
		window.opener.populateDistributionDetails(); 
	  	window.close();	  
	}	
	function closeWindow()
		{
		  window.close();
		}
        
        function	openPopUp()
		{
		
		    var width = 300;
            var height = 300;
            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + ",menubar=yes,toolbar=yes,scrollbars=yes,resizable=yes,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

            var child1 = window.open("about:blank", "subWind", windowFeatures);

		      child1.focus();
		}
		
		
		
			
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">


			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">




				<tr width="100%">


					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['commons.distributePopUp']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="frm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText id ="errorMessages"
																value="#{pages$openFileDistributePopUp.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$openFileDistributePopUp.successMessages}"
																escape="false" styleClass="INFO_FONT" />

														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width: 99.8%;">
													<h:outputLabel value="#{msg['commons.distributePopUp']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px" border="0"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">

																	<tr>

																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['collectionProcedure.description']} :"></h:outputLabel>

																		</td>
																		<td>
																			<h:inputTextarea id="description"
																				value="#{pages$openFileDistributePopUp.openFileDistribute.reason}"
																				onkeyup="removeExtraCharacter(this,100)"
																				onkeypress="removeExtraCharacter(this,100)"
																				onkeydown="removeExtraCharacter(this,100)"
																				onblur="removeExtraCharacter(this,100)"
																				onchange="removeExtraCharacter(this,100)"
																				style="width:570px;">
																			</h:inputTextarea>
																		</td>

																	</tr>
																	<tr>

																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.distributePopUp']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:selectOneMenu id="distCrit"
																				binding="#{pages$openFileDistributePopUp.distCritSelectMenu}"
																				tabindex="3">

																				<f:selectItems
																					value="#{pages$openFileDistributePopUp.distributionCriteriaList}" />

																				<a4j:support id="as"
																					action="#{pages$openFileDistributePopUp.onCriteriaChanged}"
																					event="onchange"
																					reRender="test2,errortxt,errorMessages,parentDivs,buttonDiv,btnDone" />



																			</h:selectOneMenu>
																		</td>


																	</tr>
																</table>
																<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
																<t:div styleClass="contentDiv" id="parentDivs"
																	style="width:95%">
																	<t:dataTable id="test2" width="100%" styleClass="grid"
																		binding="#{pages$openFileDistributePopUp.dataTable}"
																		value="#{pages$openFileDistributePopUp.dataList}"
																		rows="400" preserveDataModel="true"
																		preserveSort="false" var="dataItem"
																		rowClasses="row1,row2">

																		<t:column id="selectCheckBox" width="10%"
																			sortable="false" style="white-space: normal;">
																			<f:facet name="header">
																				<t:outputText value="#{msg['commons.select']}" />
																			</f:facet>
																			<h:selectBooleanCheckbox id="select"
																				rendered="#{dataItem.checkBoxRender==0}"
																				value="#{dataItem.selected}"
																				onclick="onSelectionChanged(this);" />
																			<a4j:commandLink id="lnkUnitSelectionChanged"
																				onbeforedomupdate="javascript:onChkChange();"
																				action="#{pages$openFileDistributePopUp.onChkChange}"
																				
																				reRender="test2,errortxt,errorMessages,parentDivs,buttonDiv,btnDone" />
																		</t:column>

																		<t:column id="beneficiary" width="10%"
																			sortable="false" style="white-space: normal;">

																			<f:facet name="header">
																				<t:outputText
																					value="#{msg['distribution.beneficiary']}" />
																			</f:facet>
																			<t:outputText id="beneficiaryText"
																				value="#{dataItem.beneficiaryName}" />
																		</t:column>
																		<t:column id="costCenter" width="10%" sortable="false"
																			style="white-space: normal;">
																			<f:facet name="header">
																				<t:outputText
																					value="#{msg['settlement.label.costCenter']}" />
																			</f:facet>
																			<t:outputText id="costCenterText"
																				value="#{dataItem.costCenter}" />
																		</t:column>

																		<t:column id="shariaPercent" width="10%"
																			sortable="false" style="white-space: normal;">
																			<f:facet name="header">
																				<t:outputText
																					value="#{msg['distribution.shariaPercent']}" />
																			</f:facet>
																			<t:outputText id="ShariaText"
																				value="#{dataItem.fileShareRatioString}" />
																		</t:column>


																		<t:column id="assetNumber" width="10%"
																			sortable="false" style="white-space: normal;">
																			<f:facet name="header">
																				<t:outputText
																					value="#{msg['distribution.assetPercent']}" />
																			</f:facet>
																			<t:outputText id="AssetNumTxt"
																				value="#{dataItem.assetShareRatioString}" />
																		</t:column>

																		<t:column id="amount" width="10%" sortable="false"
																			style="white-space: normal;">
																			<f:facet name="header">
																				<t:outputText value="#{msg['distribution.amout']}" />
																			</f:facet>
																			<t:outputText id="AmountTxt"
																				value="#{dataItem.amount}" />
																		</t:column>





																	</t:dataTable>

																</t:div>
																<br />
																<t:div id="buttonDiv" styleClass="BUTTON_TD">
																	<h:commandButton styleClass="BUTTON" id="btnDone"
																	    binding="#{pages$openFileDistributePopUp.btnDone}"
																		value="#{msg['commons.done']}"
																		action="#{pages$openFileDistributePopUp.onDone}"
																		style="width: auto">
																	</h:commandButton>

																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['generateFloors.Close']}"
																		onclick="javascript:window.close();"
																		style="width: auto">
																	</h:commandButton>
																</t:div>
													</table>
								</td>
							</tr>


						</table>

						</div>

						</div>



						</div>


						</div>
						</h:form>


					</td>
				</tr>
			</table>
			</td>
			</tr>
			</table>



		</body>
	</html>
</f:view>