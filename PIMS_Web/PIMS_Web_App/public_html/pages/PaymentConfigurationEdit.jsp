<%-- 
  - Author: Munir Chagani
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Editing an Auction Unit Deposit Amount
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		function closeWindowSubmit()
		{
			window.opener.document.forms[0].submit();
		  	window.close();	  
		}
		function windowSubmit()
		{
			document.getElementById('detailsFrm').submit();
		}
</script>
<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
		 <title>PIMS</title>
		 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
		 <meta http-equiv="pragma" content="no-cache">
		 <meta http-equiv="cache-control" content="no-cache">
		 <meta http-equiv="expires" content="0">
		 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 <script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
	</head>

	<body class="BODY_STYLE">
			<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
			%>
	
				<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="100%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
											<h:outputLabel value="#{msg['paymentConfiguration.PageTitle']}" styleClass="HEADER_FONT"/>
									</td>
								</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" 
							cellspacing="0" border="0">
								<tr valign="top">
								<td width="100%" height="100%" valign="top" >
								<div style="display:block;height:700px;width:100%">
										<h:form id="detailsFrm" style="width:98%">
										<div style="height:35px; width:100%; padding-left:9px;">
										<table border="0" class="layoutTable">
											<tr>
												<td>
													<h:outputText value="#{pages$PaymentConfigurationEdit.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$PaymentConfigurationEdit.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
										</div>
										<t:panelGrid rendered="false">
										<t:div style="width:100%;padding-left:10px;">
										
										</t:div>
										</t:panelGrid>
									<div class="MARGIN" style="padding-top:10px;width:100%;"> 
										<table cellpadding="0" cellspacing="0" style="#width:100%;">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
											<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
									
									<div class="DETAIL_SECTION" style="#width:100%;">
										<h:outputLabel value="#{msg['paymentConfiguration.GroupTitle']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
										<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" style="width:100%;">
										
											<tr>
												<td style="PADDING: 5px; PADDING-TOP: 5px;width:20%">
													<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel   value= "#{msg['paymentConfiguration.procedureType']}:" styleClass="LABEL"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; PADDING-TOP: 5px;width:30%">
													<h:selectOneMenu id="cmbProcedureType" tabindex="0"  readonly="true" 
																	onchange="javascript:windowSubmit();"
																	binding="#{pages$PaymentConfigurationEdit.cmbProcedureType}">
																		
																															
																	<f:selectItems
																		value="#{pages$ApplicationBean.procedureType}" />
																</h:selectOneMenu>
												</td>
												<td style="PADDING: 5px; PADDING-TOP: 5px;width:20%">
													<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL" value= "#{msg['paymentConfiguration.paymentType']}:"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; PADDING-TOP: 5px;width:30%">
													<h:selectOneMenu id="cmbPaymentType" tabindex="1" 
																	

																	binding="#{pages$PaymentConfigurationEdit.cmbPaymentType}">																	
																	<f:selectItems
																		value="#{pages$PaymentConfigurationEdit.paymentTypeList}" />
																</h:selectOneMenu>
												</td>
											</tr>								
											
											
											<tr>
												<td style="PADDING: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL" value= "#{msg['paymentConfiguration.descriptionEn']}:"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; PADDING-TOP: 5px;width:192px">
													<h:inputText id="descriptionEn" tabindex="2" maxlength="200" binding="#{pages$PaymentConfigurationEdit.paymentDescriptionEn}"  >
													</h:inputText>
												</td>
												<td style="PADDING: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL" value= "#{msg['paymentConfiguration.descriptionAr']}:"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; PADDING-TOP: 5px;width:192px">
													<h:inputText id="descriptionAr" tabindex="3" maxlength="200"  binding="#{pages$PaymentConfigurationEdit.paymentDescriptionAr}" >
													</h:inputText>
												</td>
											</tr>
											<tr>
												<td style="PADDING: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="LABEL" value= "#{msg['paymentConfiguration.condition']}:"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; PADDING-TOP: 5px;">
													<h:selectOneMenu    id="cmbCondition" 
														                tabindex="4"
																		style="width:250px;" 
																		binding="#{pages$PaymentConfigurationEdit.cmbCondition}">
																	<f:selectItem itemLabel="#{msg['commons.None']}"
																		itemValue="-1" />																
																	<f:selectItems
																		value="#{pages$ApplicationBean.paymentConfigConditionList}" />
																</h:selectOneMenu>
												</td>
												<td style="PADDING: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['paymentConfiguration.paymentSubType']}:"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; PADDING-TOP: 5px;">
													<h:selectOneMenu id="cmbPaymentSubType" tabindex="5"
																	readonly="#{pages$PaymentConfigurationEdit.isNOLSelected}" 
																	binding="#{pages$PaymentConfigurationEdit.cmbPaymentSubType}">
																	<f:selectItem itemLabel="#{msg['commons.None']}"
																		itemValue="-1" />																	
																	<f:selectItems
																		value="#{pages$PaymentConfigurationEdit.paymentSubTypeList}" />
																</h:selectOneMenu>
												</td>
											</tr>
											<tr>
												<td style="PADDING: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['paymentConfiguration.paymentNature']}:"></h:outputLabel>
												</td>
												<td colspan="1" style="PADDING: 5px; PADDING-TOP: 25px;" >
													  <h:selectOneRadio id="paymentNatureRadio" tabindex="6" layout="pageDirection" binding="#{pages$PaymentConfigurationEdit.radioPaymentNature}" valueChangeListener="#{pages$PaymentConfigurationEdit.radioValueChanged}" onclick="javascript:windowSubmit();">
													  	<f:selectItems value="#{pages$PaymentConfigurationEdit.paymentNatureList}"/>
													  </h:selectOneRadio>
												</td>
												<td colspan="1" style="PADDING-TOP: 25px;">
													<h:inputText maxlength="20" binding="#{pages$PaymentConfigurationEdit.paymentAmount}" tabindex="7" styleClass="#{pages$auctionUnitDepositEdit.readonlyStyleClassPerc} A_RIGHT_NUM"></h:inputText>
												</td>
											</tr>											
											<tr>
												<td>
													<h:outputLabel styleClass="LABEL" value=""></h:outputLabel>
												</td>
												<td>
													<h:outputLabel styleClass="LABEL RADIO_SUB" value="#{msg['paymentConfiguration.basedOn']}:"></h:outputLabel>
												</td>
												<td>
													<h:selectOneMenu id="basedOnCombo"  tabindex="8"
															readonly="#{pages$PaymentConfigurationEdit.isFixed}"											 
														binding="#{pages$PaymentConfigurationEdit.cmbBasedOnType}">
														<f:selectItem itemLabel="#{msg['commons.None']}"
																		itemValue="-1" />
														<f:selectItems	value="#{pages$ApplicationBean.basedOnType}" />
													</h:selectOneMenu>
												</td>
											</tr>
											<tr>
												<td>
													<h:outputLabel rendered="false" styleClass="LABEL" value=""></h:outputLabel>
												</td>
												<td>
													<h:outputLabel styleClass="LABEL RADIO_SUB" value="#{msg['paymentConfiguration.minAmount']}:" ></h:outputLabel>
												</td>
												<td>
													<h:inputText  tabindex="9"  binding="#{pages$PaymentConfigurationEdit.paymentMinAmount}" readonly="#{pages$PaymentConfigurationEdit.isFixed}" styleClass="#{pages$PaymentConfigurationEdit.percentAmountOnCss}">
													</h:inputText>
												</td>
											</tr>
											<tr>
												<td>
													<h:outputLabel rendered="false" styleClass="LABEL" value=""></h:outputLabel>
												</td>
												<td>
													<h:outputLabel styleClass="LABEL RADIO_SUB" value="#{msg['paymentConfiguration.maxAmount']}:" ></h:outputLabel>
												</td>
												<td>
													<h:inputText  tabindex="10" binding="#{pages$PaymentConfigurationEdit.paymentMaxAmount}" readonly="#{pages$PaymentConfigurationEdit.isFixed}" styleClass="#{pages$PaymentConfigurationEdit.percentAmountOnCss}">
													</h:inputText>
												</td>
											</tr>
											<tr>
												<td class="BUTTON_TD AUC_DEP_JUG_BUTTON_TD" colspan="4">
													<table>
													<tr>		
														<td>
														
															<h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}"
																action="#{pages$PaymentConfigurationEdit.savePaymentConfiguration}"
																tabindex="16"/>
														
														</td>
																										
														<td>
															<h:commandButton styleClass="BUTTON"  value="#{msg['commons.cancel']}"
																actionListener="#{pages$PaymentConfigurationEdit.cancel}"
																tabindex="15"/>
														</td>
														
													</tr>
												</table>
												</td>
											</tr>
											</table>
											</div>
											</div>
											<h:inputHidden  binding="#{pages$PaymentConfigurationEdit.paymentConfigurationId}" >
											</h:inputHidden>
										</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>        		
		</table>
	</body>
</html>
</f:view>
