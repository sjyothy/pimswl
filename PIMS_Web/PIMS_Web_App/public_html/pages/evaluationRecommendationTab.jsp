<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<t:div rendered="true" style="width:100%">
<t:panelGrid id="evaluationRecommendationTable"
		rendered="#{pages$evaluationApplicationDetails.pageModeHOS && !pages$evaluationApplicationDetails.HOSDone}"
		styleClass="BUTTON_TD" width="100%" columns="4">
		
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['portfolio.manage.tab.portfolio.Evaluation.Type']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu value="#{pages$evaluationApplicationDetails.evaluationRecommendationView.strTypeId}"	styleClass="SELECT_MENU">
		<f:selectItems value="#{pages$ApplicationBean.evaluationRecommendationTypes}" />
	    </h:selectOneMenu>
																	
		
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['evaluation.recommendation']}:"></h:outputLabel>
		</h:panelGroup>
		<t:inputTextarea styleClass="TEXTAREA"
			rows="12"
			style="width: 190px;"
			value="#{pages$evaluationApplicationDetails.evaluationRecommendationView.text}" 
			/>
		
		<h:outputText value=""></h:outputText>															
		<h:outputText value=""></h:outputText>
		<h:outputText value=""></h:outputText>
																
		<h:commandButton
		 styleClass="BUTTON"
		 style="width:auto;"
		value="#{msg['tenderManagement.addProposal.heading']}"
		actionListener="#{pages$evaluationApplicationDetails.addEvaluationRecommendation}"
		/>
</t:panelGrid>
</t:div>
<t:div styleClass="contentDiv" style="width:99%">																
	                                       <t:dataTable id="evaluationRecommendationDataTable"  
												rows="#{pages$evaluationApplicationDetails.paginatorRows}"
												value="#{pages$evaluationApplicationDetails.evaluationRecommendationList}"
												binding="#{pages$evaluationApplicationDetails.evaluationRecommendationDataTable}"																	
												preserveDataModel="false" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
											
											
												<t:column width="30%" sortable="true" sortPropertyName="typeAr">
													<f:facet name="header">
														<t:outputText value="#{msg['portfolio.manage.tab.portfolio.Evaluation.Type']}" />
													</f:facet>
													<t:outputText value="#{pages$evaluationApplicationDetails.isEnglishLocale ? dataItem.typeEn : dataItem.typeAr}" rendered="#{dataItem.isDeleted == 0}" styleClass="A_LEFT" style="white-space: normal;"/>
												</t:column>
												
												<t:column width="30%" sortable="true" sortPropertyName="text">
													<f:facet name="header">
														<t:outputText value="#{msg['evaluation.recommendation']}"  />
													</f:facet>
													<t:outputText value="#{dataItem.text}" rendered="#{dataItem.isDeleted == 0}" styleClass="A_LEFT" style="white-space: normal;"/>																		
												</t:column>
												
												<t:column width="30%" sortable="true" sortPropertyName="createdOn">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.date']}" />
															</f:facet>
															<t:outputText value="#{dataItem.createdOn}"
																rendered="#{dataItem.isDeleted == 0}">
																<f:convertDateTime
																	timeZone="#{pages$evaluationApplicationDetails.timeZone}"
																	pattern="#{pages$evaluationApplicationDetails.evaluationDateFormat}" />
															</t:outputText>
												</t:column>
												
												<t:column width="10%" sortable="false" rendered="true">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.action']}" />
													</f:facet>
													<a4j:commandLink onclick="if (!confirm('#{msg['confirm.recomendatio.delete']}')) return" 
													actionListener="#{pages$evaluationApplicationDetails.deleteEvaluationRecommendation}" 
													rendered="#{dataItem.isDeleted == 0 && pages$evaluationApplicationDetails.pageModeHOS}" 
													reRender="evaluationRecommendationDataTable,evaluationRecommendationGridInfo">
														<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />
													</a4j:commandLink>																															
												</t:column>
											</t:dataTable>	
</t:div>
<t:div
 	id="evaluationRecommendationGridInfo"
	styleClass="contentDivFooter AUCTION_SCH_RF" style="width:100%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']} :"/>
						<h:outputText value="#{pages$evaluationApplicationDetails.recordSize}"/>
						<h:outputText  />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>

											
																							
											<t:dataScroller id="scrollerEvaluationRecommendation" for="evaluationRecommendationDataTable" paginator="true"
												fastStep="1" paginatorMaxPages="15" immediate="false"
												paginatorTableClass="paginator"
												
												renderFacetsIfSinglePage="true" 
												paginatorTableStyle="grid_paginator" layout="singleTable" 
												paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
												styleClass="SCH_SCROLLER"
												pageIndexVar="pageNumber"
												paginatorActiveColumnStyle="font-weight:bold;"
												rendered="true"
												paginatorRenderLinkForActive="false">

												        <f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblFEvaluationRecommendation"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFREvaluationRecommendation"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFEvaluationRecommendation"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblLEvaluationRecommendation"></t:graphicImage>
														</f:facet>
														<t:div styleClass="PAGE_NUM_BG">
												    	<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="font-size:10;" value="#{msg['commons.page']}:"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:10;" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
														</table>
															
														</t:div>
														
											</t:dataScroller>
											</t:panelGroup>
											</t:panelGrid>
											</t:div>
											
												
