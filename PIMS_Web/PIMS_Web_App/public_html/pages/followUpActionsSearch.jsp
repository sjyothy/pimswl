<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>


<script language="javascript" type="text/javascript">
	function openManageFollowUpActionsPopup() 
	{
	          
        	  var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/2+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('followUpActionsManage.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
	}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden">

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<script language="javascript" type="text/javascript">


    function refresh()
    {
      document.getElementById("form:refresh").onclick();

    }

	
	function openManageFollowUpActionsPopup() 
	{
	          
        	  var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('followUpActionsManage.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
	}
	
</script>


			
			
			
			

		</head>
		<body class="BODY_STYLE">
			      <div class="containerDiv">
			<table cellpadding="0" cellspacing="0" border="0">
			<tr>
							 <td colspan="2">
						        <jsp:include page="header.jsp"/>
						     </td>
	            </tr>
				<tr>
					  <td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					  </td>
				    
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['followupaction.search']}"
										styleClass="HEADER_FONT" style="padding:10px" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
							
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" />
								<td valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" >
										<h:form id="form">
											<div class="MARGIN" style="width: 95%">
											<table  id="layoutTable" border="0" class="layoutTable" width="100%" style="margin-left:15px;margin-right:15px;" >
											<tr>
													<td >
													    <h:outputText    id="successmsg"      escape="false" styleClass="INFO_FONT"  value="#{pages$followUpActionsSearch.successMessages}"/>
 														<h:outputText    id="errormsg"        escape="false" styleClass="ERROR_FONT" value="#{pages$followUpActionsSearch.errorMessages}"/>
						                                <a4j:commandLink id="refresh"  reRender="layoutTable,successmsg,errormsg,contentDiv,contentdivFooter,scroller,dt1" action="#{pages$followUpActionsSearch.onRefresh}"/>
													</td>
												</tr>
											</table>
												<table cellpadding="0" cellspacing="0" >
													<tr>
														<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
														<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>

												<div class="DETAIL_SECTION" >
													<h:outputLabel value="#{msg['commons.search']}" styleClass="DETAIL_SECTION_LABEL" />
													<table cellpadding="1px" cellspacing="3px" class="DETAIL_SECTION_INNER">

														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL" value="#{msg['followupaction.followUpActionEn']} :" />
															</td>
															<td width="25%">
																<h:inputText id="descEn" style="width:85%" binding="#{pages$followUpActionsSearch.txt_followUpActionDescEn}" />
															</td>
															<td width="25%">
																<h:outputLabel styleClass="LABEL" value="#{msg['followupaction.followUpActionAr']} :" />
															</td>
															<td width="25%">
																<h:inputText id="descAr" style="width:85%" binding="#{pages$followUpActionsSearch.txt_followUpActionDescAr}" />
															</td>
														</tr>


														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL" value="#{msg['followupaction.followUpStatus']} :" />
															</td>
															<td width="25%">
																<h:selectOneMenu id="selectUnitType" binding="#{pages$followUpActionsSearch.cmb_followUpStatus}" style="width:87%">
																	<f:selectItem id="selectAllUnitType" itemValue="-1" itemLabel="#{msg['commons.All']}" />
																	<f:selectItems value="#{pages$ApplicationBean.followUpStatus}" />
																</h:selectOneMenu>
															</td>
														</tr>
                                                        <div></div>        
														<tr>
															<td colspan="4">
																<DIV class="BUTTON_TD" style="padding-bottom: 4px;">
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.search']}"
																		action="#{pages$followUpActionsSearch.btnSearch_Click}" />
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.clear']}"
																		action="#{pages$followUpActionsSearch.btnClear_Click}" />
																	<h:commandButton type="button" styleClass="BUTTON"
																		value="#{msg['commons.add']}"
																		onclick="javaScript:openManageFollowUpActionsPopup();" />
																		
																</DIV>
															</td>
														</tr>
													</table>
												</div>
											</div>

											<div style="padding: 5px;">
												<div class="imag">
													&nbsp;
												</div>
												<t:div id="contentDiv" styleClass="contentDiv" style="width: 95%;align:center;">

													<t:dataTable id="dt1" preserveDataModel="false" preserveSort="false" var="dataItem" 
													    rules="all" renderedIfEmpty="true" width="100%"
														rows="#{pages$followUpActionsSearch.paginatorRows}"
														value="#{pages$followUpActionsSearch.list}"
														binding="#{pages$followUpActionsSearch.dataTable}">

														<t:column id="colDescEn" sortable="true" style="text-align:left;white-space:normal;" >
															<f:facet name="header">
																<t:outputText value="#{msg['followupaction.followUpActionEn']}" />
															</f:facet>
															<t:outputText style="text-align:left;white-space:normal;" 
															value="#{dataItem.descriptionEn}" />
														</t:column>
														<t:column id="colDescAr" sortable="true" 	style="text-align:right;white-space:normal;" >
															<f:facet name="header">
																<t:outputText value="#{msg['followupaction.followUpActionAr']}" />
															</f:facet>
															<t:outputText style="text-align:right;white-space:normal;" 
															value="#{dataItem.descriptionAr}" />
														</t:column>

														<t:column id="colStatusEn" sortable="true" style="white-space: normal;"
															rendered="#{pages$followUpActionsSearch.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText value="#{msg['followupaction.followUpStatus']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT" style="white-space: normal;" value="#{dataItem.followUpStatusEn}" />
														</t:column>
														
														<t:column id="colStatusAr" style="padding-right:5px;white-space: normal;"
															sortable="true"
															rendered="#{!pages$followUpActionsSearch.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText value="#{msg['followupaction.followUpStatus']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT" style="white-space: normal;" value="#{dataItem.followUpStatusAr}" />
														</t:column>

														<t:column id="col6"  style="white-space:normal; text-align:center">
															<f:facet name="header">
																<t:outputText style="text-align:center" value="#{msg['commons.action']}" />
															</f:facet>
														    <h:commandLink  action="#{pages$followUpActionsSearch.imgEdit_Click}">
													           <h:graphicImage id="editIcon" 
													           title="#{msg['followupaction.edit']}" 
													           url="../resources/images/edit-icon.gif" />&nbsp;
													        </h:commandLink>
															
														</t:column>
													</t:dataTable>
												</t:div>
												<t:div styleClass="contentDivFooter" style="width:96%">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$followUpActionsSearch.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">
																<CENTER>
																	<t:dataScroller id="scroller" for="dt1"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$followUpActionsSearch.paginatorMaxPages}"
																		paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																		styleClass="SCH_SCROLLER" pageIndexVar="pageNumber"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false">

																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblF" />
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR" />
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF" />
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL" />
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.pageNumber}" />
																					</td>
																				</tr>
																			</table>
																		</t:div>
																	</t:dataScroller>
																</CENTER>
															</td>
														</tr>
													</table>
												</t:div>
											</div>
										</h:form>
									</div>
								</td>
							</tr>

						</table>
					</td>
				</tr>
			    
			    				
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
			</table>
			      </div>
			
		</body>

	</html>
</f:view>