<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Assets
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	    function resetValues()
      	{
      	  
      		document.getElementById("searchFrm:txtFloorNo").value="";
      		document.getElementById("searchFrm:txtPropertyNumber").value="";
      		document.getElementById("searchFrm:txtEndowedName").value="";
      		document.getElementById("searchFrm:txtEndowedNumber").value="";
			document.getElementById("searchFrm:txtUnitNo").value="";
			document.getElementById("searchFrm:txtUnitCostCenter").value="";        	
			document.getElementById("searchFrm:txtBedRooms").value="";
			document.getElementById("searchFrm:txtDewaNumber").value="";
			document.getElementById("searchFrm:txtRentAmountFrom").value="";
			document.getElementById("searchFrm:txtRentAmountTo").value="";
			document.getElementById("searchFrm:txtPropertyName").value="";    
      	    document.getElementById("searchFrm:selectUnitUsage").selectedIndex=0;
      	    document.getElementById("searchFrm:selectUnitType").selectedIndex=0;
      	    document.getElementById("searchFrm:selectInvestmentType").selectedIndex=0;
      	    document.getElementById("searchFrm:selectUnitSide").selectedIndex=0;
      	    document.getElementById("searchFrm:selectPropertyOwnerShip").selectedIndex=0;
      	    document.getElementById("searchFrm:selectStatus").selectedIndex=0;
			document.getElementById("searchFrm:state").selectedIndex=0;
			document.getElementById("searchFrm:txtUnitDesc").value="";
        }        
		
		function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	function closeWindow()
		{
		  window.close();
		}
        
      
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			


			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


			


				<tr width="100%">

				

					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['availableBankTransfer.header']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="searchFrm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText value="#{pages$AvailableBankTransferBackingBean.errorMessages}"
																		escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
														<%--<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT" /></td>																																																										
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
														<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT" /></td>--%>	
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width: 99.8%;">
													<%--<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>--%>
													<t:div styleClass="contentDiv" style="width:95%">
<t:dataTable id="assetGrid" width="100%" styleClass="grid"
	value="#{pages$AvailableBankTransfers.assetDataList}" rows="10"
	preserveDataModel="true" preserveSort="false" var="dataItem"
	rowClasses="row1,row2">




	<t:column id="assetNumber" width="10%" sortable="false"
		style="white-space: normal;">
		<f:facet name="header">
			<t:outputText value="#{msg['searchAssets.assetNumber']}" />
		</f:facet>
		<t:outputText value="#{dataItem.assetNumber}" />
	</t:column>
	
	<%--<t:column id="assetTypeEn" width="10%" sortable="false"
		style="white-space: normal;" rendered = "#{pages$AvailableBankTransfers.isEnglishLocale}">
		<f:facet name="header">
			<t:outputText value="#{msg['searchAssets.assetType']}" />
		</f:facet>
		<t:outputText value="#{dataItem.assetTypeView.assetTypeNameEn}" />
	</t:column>
	<t:column id="assetTypeAr" width="10%" sortable="false"
		style="white-space: normal;" rendered = "#{!pages$AvailableBankTransfers.isEnglishLocale}">
		<f:facet name="header">
			<t:outputText value="#{msg['searchAssets.assetType']}" />
		</f:facet>
		<t:outputText value="#{dataItem.assetTypeView.assetTypeNameAr}" />
	</t:column>--%>
	<t:column id="bankNameEn" width="10%" sortable="false"
		style="white-space: normal;" rendered = "#{pages$AvailableBankTransfers.isEnglishLocale}">
		<f:facet name="header">
			<t:outputText value="#{msg['addMissingMigratedPayments.fieldName.bankName']}" />
		</f:facet>
		<t:outputText value="#{dataItem.bankView.bankEn}" />
	</t:column>
	<t:column id="bankNameAr" width="10%" sortable="false"
		style="white-space: normal;" rendered = "#{!pages$AvailableBankTransfers.isEnglishLocale}">
		<f:facet name="header">
			<t:outputText value="#{msg['addMissingMigratedPayments.fieldName.bankName']}" />
		</f:facet>
		<t:outputText value="#{dataItem.bankView.bankAr}" />
	</t:column>
	
	<t:column id="bankTransferNumber" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['availableBankTransferTab.bankTransferNumber']}" />
		</f:facet>
		<t:outputText value="#{dataItem.fieldNumber}" />
	</t:column>
	
	<t:column id="expTransDate" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['availableBankTransferTab.expTransferDate']}" />
		</f:facet>
		<t:outputText value="#{dataItem.dueDate}" />
	</t:column>
	
	

	<t:column id="assetNameEn" width="10%" sortable="false"
		style="white-space: normal;">
		<f:facet name="header">
			<t:outputText value="#{msg['searchAssets.assetNameEn']}" />
		</f:facet>
		<t:outputText value="#{dataItem.assetNameEn}" />
	</t:column>
	<t:column id="assetNameAr" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['searchAssets.assetNameAr']}" />
		</f:facet>
		<t:outputText value="#{dataItem.assetNameAr}" />
	</t:column>
	<t:column id="assetDesc" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['searchAssets.assetDesc']}" />
		</f:facet>
		<t:outputText value="#{dataItem.description}" />
	</t:column>
	<t:column id="sharePercent" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['availableBankTransferTab.sharePercent']}" />
		</f:facet>
		<t:outputText value="#{dataItem.sharePercent}" />
	</t:column>
	<t:column id="amount" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['chequeList.amount']}" />
		</f:facet>
		<t:outputText value="#{dataItem.amount}" />
	</t:column>
	<t:column id="shareAmount" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['availableBankTransferTab.shareAmount']}" />
		</f:facet>
		<t:outputText value="#{dataItem.shareAmount}" />
	</t:column>

	



</t:dataTable>
</t:div>
												</div>
											</div>
											
											<%--<table width="96.6%" border="0">
												<tr>
													<td class="BUTTON_TD JUG_BUTTON_TD_US">

														<h:commandButton type="submit"
															rendered="#{pages$paymentDetailsPopUp.isPageModeSelectManyPopUp}"
															styleClass="BUTTON"
															actionListener="#{pages$paymentDetailsPopUp.sendManyAssetsInfoToParent}"
															value="#{msg['commons.select']}" />
													</td>

												</tr>


											</table>--%>
										</div>


										</div>
									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>
			
			</table>
			

		</body>
	</html>
</f:view>