<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>Test JSP Title</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
			<script type="text/javascript" src="../../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>
		<BODY>
					<h:form id="tabForm">
						<div>
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
							</tr>
						</table>
						<div class="TAB_PANEL_INNER">
							<rich:tabPanel id="richTab" 
								valueChangeListener="#{pages$receivePayment.tabChanged}" headerSpacing="0">
															


														<rich:tab id="Cash" label="#{msg['payment.cash']}">
															<h:panelGrid columns="5" border="1">

																<h:outputText value="#{msg['payment.amount']}"></h:outputText>
																<h:inputText id="f7"
																	binding="#{pages$receivePayment.cashAmount}"
																	style="text-align:right"></h:inputText>

																<h:outputText value="#{msg['payment.exchangeRate']}"></h:outputText>
																<h:inputText
																	binding="#{pages$receivePayment.exchangeRate}"
																	style="text-align:right"></h:inputText>
															</h:panelGrid>



														</rich:tab>
														<rich:tab id="Cheque" label="#{msg['payment.cheque']}">

															<h:panelGrid columns="4">

																<h:outputText value="#{msg['payment.amount']}"></h:outputText>
																<h:inputText style="text-align:right" id="f2"
																	binding="#{pages$receivePayment.transactionAmount}"></h:inputText>
																<h:outputText value="#{msg['payment.bank']}"></h:outputText>

																<h:selectOneMenu id="f1"
																	binding="#{pages$receivePayment.banksMenu}">
																	<f:selectItems value="#{pages$receivePayment.banks}" />
																</h:selectOneMenu>
																<h:outputText value="#{msg['payment.chequeDate']}"></h:outputText>
																<h:inputText id="f3"
																	binding="#{pages$receivePayment.chequeDate}"
																	readonly="true"></h:inputText>



																<h:graphicImage style="width: 16;height: 16;border: 0"
																	id="image1" url="../resources/images/calendar.gif"
																	onclick="javascript:Calendar(this,'_id0:f3');" />
																<h:graphicImage id="image2" style="width: 16;height: 16;border: 0"
																	url="../resources/images/delete.gif"
																	onclick="javascript:clearFields('_id0:f3');" />

																<h:outputText value="#{msg['payment.accountNo']}">
																</h:outputText>
																<h:inputText id="f5"
																	binding="#{pages$receivePayment.accountNo}"></h:inputText>
																<h:outputText value="#{msg['payment.chequeNo']}"></h:outputText>

																<h:inputText id="f4"
																	binding="#{pages$receivePayment.chequeNo}"></h:inputText>


															</h:panelGrid>


														</rich:tab>

													</rich:tabPanel>
							</div>
						</div>
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
							</tr>
						</table>
					
					</h:form>
		</BODY>
	</html>
</f:view>