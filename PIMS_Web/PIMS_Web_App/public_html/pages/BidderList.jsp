
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>	
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

		
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>		
		    

<html style="overflow:hidden;">
<head>
	<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>	
		
		
        
    <script language="javascript" type="text/javascript">
	function closeWindow()
	{
	  window.close();
	}
	//function RowDoubleClick(bidderID, bidderFName, bidderLName, bidderSocialSecNo, bidderPassportNo)
	function RowDoubleClick()
	{
    //  window.opener.document.getElementById("auctionRequestForm:bidderID").value = bidderID;
	//  window.opener.document.getElementById("auctionRequestForm:bidderNameInputText").value = bidderFName + ' ' + bidderLName;	  
	//  window.opener.document.getElementById("auctionRequestForm:bidderName").value = bidderFName + ' ' + bidderLName;
	//  window.opener.document.getElementById("auctionRequestForm:bidderSocialSec").value = bidderSocialSecNo;
	//  window.opener.document.getElementById("auctionRequestForm:bidderPassport").value = bidderPassportNo;	  
    //  window.opener.document.getElementById("auctionRequestForm:auctionUnitsOfRequest").value = "";
    
    //  window.opener.document.getElementById("formBidder:bidderID").value = bidderID;
	//  window.opener.document.getElementById("formBidder:txtfirstName").value = bidderFName + ' ' + bidderLName;	  
	//  window.opener.document.getElementById("formBidder:txtlastName").value = bidderFName + ' ' + bidderLName;
	//  window.opener.document.getElementById("formBidder:txtsocialSecNumber").value = bidderSocialSecNo;
	//  window.opener.document.getElementById("formBidder:txtpassportNumber").value = bidderPassportNo;	  
     // window.opener.document.getElementById("formBidder:auctionUnitsOfRequest").value = "";
    //  window.opener.document.forms[0].submit();
    //  closeWindow();
  //alert("In show PopUp");
 var screen_width = screen.width;
 var screen_height = screen.height;
 window.open('AddBidder.jsp?','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes');
	}
 function clearWindow()
		{  
		    
		    document.getElementById("formSearchBidder:socialSecNo").value="";
		    document.getElementById("formSearchBidder:firstname").value="";
		    document.getElementById("formSearchBidder:passport").value="";
		    document.getElementById("formSearchBidder:lastname").value="";
		    
		}    	
	
</script>
</head>

<body dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE"> 

    <!-- Header --> 
<table width="100%" cellpadding="0" cellspacing="0"  border="0">
    <tr>
    <td colspan="2">
        <jsp:include page="header.jsp"/>
    </td>
    </tr>

<tr width="100%">
    <td class="divLeftMenuWithBody" width="17%">
        <jsp:include page="leftmenu.jsp"/>
    </td>
    <td width="83%" valign="top" class="divBackgroundBody" height="505px">
        <table width="99.2%" class="greyPanelTable" cellpadding="0" cellspacing="0"  border="0">
        <tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['bidder.searchbidder']}" styleClass="HEADER_FONT"/>
						</td>
						<td width="100%">
							&nbsp;
						</td>
					</tr>
        </table>
    
        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
          <tr valign="top" height="100%">
             <td height="100%" valign="top" nowrap="nowrap" background ="../../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
             </td> 
	  
  <td width="100%" height="460px" valign="top" >
             	
            <div style="padding-bottom:7px;padding-left:10px;padding-right:0;padding-top:7px;" >	
             	<h:form id="formSearchBidder" style="width:97%">
             <div style="height:25px;">	
             	<table border="0" class="layoutTable">
												<tr>
      									    	      <td colspan="6">
														<h:outputText value="#{pages$BidderList.errorMessages}" escape="false" styleClass="INFO_FONT" style="padding:10px;"/>
													  </td>
												</tr>
									</table>
			</div>						
             	
             	<div class= "MARGIN">
				<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
				</table>
				 <div class="DETAIL_SECTION">
             		<h:outputLabel value="#{msg['inquiry.searchcriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>				
							<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" width="100%" >
								
								<tr>
									<td style="width:20%">
										<h:outputLabel styleClass="LABEL" value="#{msg['customer.socialSecNumber']}:"></h:outputLabel>
									</td>
									<td style="width:30%">
										<h:inputText style="width:150px;" id="socialSecNo"   styleClass="A_LEFT_SMALL"
										binding="#{pages$BidderList.inputTextSocialSecNo}"></h:inputText>
									</td>
									
									<td style="width:20%" >
										<h:outputLabel  styleClass="LABEL" value="#{msg['bidder.firstName']}:"></h:outputLabel>
									</td>
									<td style="width:30%">
										<h:inputText style="width:150px;" id="firstname"  styleClass="A_LEFT_SMALL"
										binding="#{pages$BidderList.inputTextFirstName}"></h:inputText>
									</td>
																</tr>
								<tr>
									<td style="width:20%">
										<h:outputLabel styleClass="LABEL" value="#{msg['commons.passport.number']}:"></h:outputLabel>
									</td>
									<td style="width:30%">
										<h:inputText style="width:150px;" id="passport"   styleClass="A_LEFT_SMALL"
										binding="#{pages$BidderList.inputTextPassport}"></h:inputText>
									</td>
									<td style="width:20%">
										<h:outputLabel styleClass="LABEL"  value="#{msg['tenants.lastname']}:"></h:outputLabel>
									</td>
									<td style="width:30%">
										<h:inputText style="width:150px;" id="lastname"  styleClass="A_LEFT_SMALL"
										binding="#{pages$BidderList.inputTextLastName}"></h:inputText>
									</td>
								
								</tr>
								<tr></tr><tr></tr>
								
								<tr >
								
									<td colspan="5" class="BUTTON_TD">
									<pims:security screen="Pims.AuctionManagement.Bidder.BidderList" action="create">
									    <h:commandButton styleClass="BUTTON" type= "submit" 
									    value="#{msg['commons.Add']}" 
									    action="#{pages$BidderList.btnAddBidder_Click}" 
									    >	</h:commandButton>
									 </pims:security>   
										
										<h:commandButton styleClass="BUTTON" value="#{msg['commons.search']}"
												action="#{pages$BidderList.search}"></h:commandButton>
		                                <h:commandButton styleClass="BUTTON" type="button"
												                           value="#{msg['commons.reset']}"
												                           onclick="javascript:clearWindow();" 
												                             
												                           
												           />  
										
									</td>

								</tr>
							</table>		
							</div>
							</div>
																																
					
					
					
						<div style="padding-bottom:7px;padding-left:10px;padding-right:0;padding-top:7px;">		    
                   		 <div class="imag">&nbsp;</div>
							<div class="contentDiv"  style="width:97%">
							<t:dataTable id="bidderGrid" 
								value="#{pages$BidderList.bidderDataList}"													
								binding="#{pages$BidderList.dataTable}"													
								rows="5"
								preserveDataModel="false" preserveSort="false" var="dataItem"
								rules="all" renderedIfEmpty="true" width="100%">
									
								<t:column id="col2" sortable="true" >
									<f:facet name="header">
										<t:outputText value="#{msg['bidder.firstName']}" />
									</f:facet>
									<h:commandLink styleClass="A_LEFT"  action="#{pages$BidderList.cmdBidderName_Click}" value="#{dataItem.firstName}"
									style="white-space: normal;" />
									
									<t:outputText />
								</t:column>
								
								<t:column id="col3" sortable="true"  >
									<f:facet name="header">
										<t:outputText value="#{msg['customer.lastname']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT"  value="#{dataItem.lastName}" 
									style="white-space: normal;"/>
								</t:column>
	
								<t:column id="col4" >
									<f:facet name="header">
										<t:outputText value="#{msg['commons.passport.number']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT"  value="#{dataItem.passportNumber}" 
									style="white-space: normal;"/>
								</t:column>
								
								<t:column id="col5" >
									<f:facet name="header">
										<t:outputText value="#{msg['customer.socialSecNumber']}" />
									</f:facet>
									<t:outputText  styleClass="A_LEFT" value="#{dataItem.socialSecNumber}"
									style="white-space: normal;" />
								</t:column>
							<pims:security screen="Pims.AuctionManagement.Bidder.BidderList" action="create">	
								<t:column id="col6" >
									<f:facet name="header">
										<t:outputText value="#{msg['commons.delete']}" />
									</f:facet>
								   <t:commandLink   actionListener="#{pages$BidderList.cmdDelete_Click}">&nbsp;
									 <h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}" url="../resources/images/delete.gif"/>&nbsp;
								  </t:commandLink>
									<t:outputText />
								</t:column>
							</pims:security>
							<pims:security screen="Pims.AuctionManagement.Bidder.BidderList" action="create">	
								<t:column id="col7" >
									<f:facet name="header">
										<t:outputText value="#{msg['commons.status']}" />
									</f:facet>
									
								  <t:commandLink  rendered="#{dataItem.enable}"    actionListener="#{pages$BidderList.cmdStatus_Click}">&nbsp;
									 <h:graphicImage id="enableIcon" title="#{msg['commons.clickDisable']}" url="../resources/images/app_icons/enable.png" />&nbsp;
								  </t:commandLink>
								  
								  <t:commandLink  rendered="#{dataItem.disable}"  actionListener="#{pages$BidderList.cmdStatus_Click}">&nbsp;
									 <h:graphicImage id="disableIcon" title="#{msg['commons.clickEnable']}" url="../resources/images/app_icons/disable.png" />&nbsp;
								  </t:commandLink>
									<t:outputText />
								</t:column>
							</pims:security>	
							</t:dataTable>		
							</div>
							<div class="contentDivFooter" style="width: 99%">
							 <table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$BidderList.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
											<CENTER>
												<t:dataScroller id="scroller" for="bidderGrid" paginator="true"
													fastStep="1" paginatorMaxPages="#{pages$BidderList.paginatorMaxPages}" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 
												    pageIndexVar="pageNumber"
												    styleClass="SCH_SCROLLER"
												    paginatorActiveColumnStyle="font-weight:bold;"
												    paginatorRenderLinkForActive="false" 
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

								                    	<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
													
													<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>
													
												</t:dataScroller>
												</CENTER>
												</td></tr>
												</table>
                                           </div>
					                </div>  
				
                </h:form>
               </div>
               </td>
               </tr>
               </table>
               </td>
               </tr>
               <tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
               </table>
               
    </body>
</html>
</f:view>
