<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

	<t:div style="width:100%;">
		<t:panelGrid columns="1" cellpadding="0" cellspacing="0">
			<t:div styleClass="BUTTON_TD" style="height:24px;">
				<t:commandButton styleClass="BUTTON" value="#{msg['notes.add']}" actionListener="#{notesController.openAddNotePopup}" rendered="#{notesController.canAddNote}" style="padding-right: 1px;" />
				</br>
			</t:div>
			<t:div styleClass="contentDiv" style="width:98.8%;">
				<t:dataTable id="notesTable" renderedIfEmpty="true" width="100%" cellpadding="0" cellspacing="0" var="dataItem" binding="#{notesController.dataTable}" value="#{notesController.notesDisplayList}"
				 	preserveDataModel="false" preserveSort="false" rowClasses="row1,row2" rules="all" rows="#{notesController.paginatorRows}">
					<t:column width="200" id="notesTableuser">
						<f:facet name="header">
							<t:outputText value="#{msg['notes.user']}" id="notesTableuserHeadin"/>
						</f:facet>
						<t:outputText id="notesTableuservalue" value="#{notesController.isEnglishLocale? dataItem.createdByFullName:dataItem.createdByFullNameAr}" styleClass="A_LEFT"/>
					</t:column>
					<t:column width="130" id="notesTabledate">
						<f:facet name="header">
							<t:outputText value="#{msg['notes.date']}" id="notesTabledatehdn" />
						</f:facet>
						<t:outputText value="#{dataItem.createdOn}" id="notesTabledatevale">
							<f:convertDateTime pattern="#{notesController.longDateFormat}" 
											   timeZone="#{notesController.timeZone}"/>
						</t:outputText>
					</t:column>
					<t:column width="400" id="notesTabledesc">
						<f:facet name="header">
							<t:outputText value="#{msg['notes.description']}" id="notesTabledeschdn"/>
						</f:facet>
						<t:outputText value="#{dataItem.description}" styleClass="A_LEFT" id="notesTabledesctxt" style="white-space: normal;"/>
					</t:column>
				</t:dataTable>
			</t:div>
			<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99.8%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']}"/>
						<h:outputText value=" : "/>
						<h:outputText value="#{notesController.recordSize}"/>
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="notesScrollers" for="notesTable" paginator="true"  
						fastStep="1" paginatorMaxPages="#{pages$notesController.paginatorMaxPages}" immediate="false"
						paginatorTableClass="paginator"
						renderFacetsIfSinglePage="true" 												
						paginatorTableStyle="grid_paginator" layout="singleTable" 
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" 
						pageIndexVar="pageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}" ></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}" ></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
							<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
		</t:div>
		</t:panelGrid>
	</t:div>