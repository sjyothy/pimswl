					<t:div styleClass="contentDiv" style="border:0px none;">
						<t:dataTable id="viewNotesTable" renderedIfEmpty="false" columnClasses="notes_col,notes_col,notes_col" cellpadding="0" cellspacing="0" var="notedata" value="#{notesController.notesDisplayList}" preserveDataModel="false" preserveSort="false">
							<t:column>
								<f:facet name="header"><h:outputText value="#{msg['notes.user']}" styleClass="filename_th"/></f:facet>
								<t:outputText value="#{notedata.createdBy}" styleClass=""/>
							</t:column>
							<t:column>
								<f:facet name="header"><h:outputText value="#{msg['notes.date']}" styleClass="filename_th"/></f:facet>
								<t:outputText value="#{notedata.createdOn}" styleClass=""/>
							</t:column>
							<t:column>
								<f:facet name="header"><h:outputText value="#{msg['notes.description']}" styleClass="filename_th"/></f:facet>
								<t:outputText value="#{notedata.description}" styleClass="NOTES_DESC"/>
							</t:column>
						</t:dataTable>
					</t:div>
