					<t:div styleClass="contentDiv" style="border:0px none;">
						<t:panelGrid columns="1" cellpadding="0" cellspacing="0">
							<t:panelGrid columns="3" cellpadding="0" cellspacing="2" columnClasses="notes_col,notes_col,notes_col">
								<h:outputText value="#{msg['notes.description']}" styleClass="filename_th"/>
								<t:inputTextarea styleClass="TEXTAREA" value="#{notesController.notes.description}"/>
								<t:commandButton value="#{msg['notes.add']}" actionListener="#{notesController.addNotes}" styleClass="BUTTON" />
							</t:panelGrid>
							<t:dataTable id="notesTable" renderedIfEmpty="false" columnClasses="notes_col,notes_col,notes_col" width="100%" cellpadding="0" cellspacing="0" var="notedata" binding="#{notesController.dataTable}" value="#{notesController.notesDisplayList}" preserveDataModel="false" preserveSort="false">
								<t:column>
									<f:facet name="header">
										<h:outputText value="#{msg['notes.user']}" styleClass="filename_th"/>
									</f:facet>
									<t:outputText value="#{notedata.createdBy}" styleClass=""/>
								</t:column>
								<t:column>
									<f:facet name="header">
										<h:outputText value="#{msg['notes.date']}" styleClass="filename_th"/>
									</f:facet>
									<t:outputText value="#{notedata.createdOn}" styleClass=""/>
						
								</t:column>
								<t:column>
									<f:facet name="header">
										<h:outputText value="#{msg['notes.description']}" styleClass="filename_th" />
									</f:facet>
									<t:outputText value="#{notedata.description}" styleClass="NOTES_DESC"/>
								</t:column>
							</t:dataTable>
						</t:panelGrid>
					</t:div>