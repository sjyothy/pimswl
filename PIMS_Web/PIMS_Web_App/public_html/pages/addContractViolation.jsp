<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript" src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript"> 

function populateContract(contractNumber,contractId,tenantId)
	{
     
        document.getElementById("cLRForm:hdnContractId").value=contractId;
	    document.getElementById("cLRForm:hdnContactNumber").value=contractNumber;
	    document.getElementById("cLRForm:hdnTenantId").value=contractId;
     }
     
    function   showContractPopup(clStatus)
		{
	     // alert("In show PopUp");
		 var screen_width = 1024;
         var screen_height = 470;

		    
		   window.open('ContractListPopUp.jsf?clStatus='+clStatus,'_blank','width='+(screen_width-220)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
		    
		}  
		
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
	    
	    //alert("in populatePerson");
	    
	    document.getElementById("cLRForm:hdnPersonId").value=personId;
	    document.getElementById("cLRForm:hdnCellNo").value=cellNumber;
	    document.getElementById("cLRForm:hdnPersonName").value=personName;
	    document.getElementById("cLRForm:hdnPersonType").value=hdnPersonType;
	    document.getElementById("cLRForm:hdnIsCompany").value=isCompany;
		
        document.forms[0].submit();
	}	

	function  showPersonPopup()
						{
				   var screen_width = 1024;
				   var screen_height = 470;
				   window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
						    
						}
						
	  function showAddViolationPopup(){
			   var screen_width = 1024;
			   var screen_height = 500;
			   var popup_width = screen_width-150;
		       var popup_height = screen_height;
		       var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		       var popup = window.open('AddViolationPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,location=no,directories=no,status=no,resizable=yes,titlebar=no,dialog=yes');
			
			}	
								
	  function showBlacklistPopup()
	   {
			   var screen_width = 1024-400;
			   var screen_height = 500-200;
			   var popup_width = screen_width-150;
		       var popup_height = screen_height;
		       var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		       var popup = window.open('blacklistPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,location=no,directories=no,status=no,resizable=yes,titlebar=no,dialog=yes');
		}					
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">

				<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['contract.screenName.contractViolation']}" styleClass="HEADER_FONT"/>
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
								<div class="SCROLLABLE_SECTION" style="height:450px;#height:450px;">
									<h:form id="cLRForm" enctype="multipart/form-data" style="width:98%">
										<h:inputHidden id="hdnContractId" value="#{pages$addContractViolation.hdnContractId}"></h:inputHidden>
										<h:inputHidden id="hdnContactNumber" value="#{pages$addContractViolation.hdnContractNumber}"></h:inputHidden>
										<h:inputHidden id="hdnTenantId" value="#{pages$addContractViolation.hdnTenantId}"></h:inputHidden>
										
						    
					
						           <div >
										<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
											<tr>
												<td>
													<h:outputText id="successMsg_2" value="#{pages$addContractViolation.successMessages}" escape="false"  styleClass="INFO_FONT"/>
													<h:outputText id="errorMsg_1" value="#{pages$addContractViolation.errorMessages}"  escape="false" styleClass="ERROR_FONT"/>
												</td>
											</tr>
										</table>
									</div>
											
										 
										
										<table class="TAB_PANEL_MARGIN" width="100%" border="0">

											<tr>
												<td colspan="2">
												<div >
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
													<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
										</table>
													
											
									<rich:tabPanel binding="#{pages$addContractViolation.richTabPanel}" style="HEIGHT: 330px;width: 100%">
										<rich:tab id = "contractDetailTab" label="#{msg['contract.Contract.Details']}">
											<t:div id="divContract" styleClass="TAB_DETAIL_SECTION"> 
												<t:panelGrid cellpadding="1px" width="100%" cellspacing="3px" styleClass="TAB_DETAIL_SECTION_INNER"  columns="4">
										          <t:panelGroup >
											         <t:panelGrid columns="2" cellpadding="1" cellspacing="5">
												     							
												      <h:outputText styleClass="LABEL" value="#{msg['contract.contractNumber']}" />
												        <t:panelGroup>
											         
														   <h:inputText styleClass="READONLY " readonly="true" binding="#{pages$addContractViolation.contractNoText}" style="width: 186px; height: 18px"></h:inputText>
														   <h:commandLink  binding="#{pages$addContractViolation.populateContract}"  onclick="javascript:showContractPopup('#{pages$addContractViolation.clStatus}')" >
														   <h:graphicImage id="populateIcon" style="padding-left:5px;" title="#{msg['commons.populate']}" url="../resources/images/magnifier.gif" />
														  </h:commandLink>
														   <h:commandLink   action="#{pages$addContractViolation.btnContract_Click}" >
														    <h:graphicImage style="padding-left:4px;" title="#{msg['commons.view']}" url="../resources/images/app_icons/Lease-contract.png"/>
														  </h:commandLink>
													   </t:panelGroup>
													  
													  														
													  <h:outputLabel styleClass="LABEL" value="#{msg['contract.tenantName']}" />
													  
													 <t:panelGroup> 
													  <h:inputText styleClass="READONLY " readonly="true" binding="#{pages$addContractViolation.tenantNameText}" style="width: 186px; height: 18px"></h:inputText>
													  <h:commandLink    action="#{pages$addContractViolation.btnTenant_Click}" >
														    <h:graphicImage style="padding-left:4px;" title="#{msg['commons.view']}" url="../resources/images/app_icons/Tenant.png"/>
													 </h:commandLink>
													</t:panelGroup>	  
													  
													  <h:outputLabel styleClass="LABEL" value="#{msg['contract.startDate']}" />
													  <h:inputText styleClass="READONLY A_LEFT" readonly="true" binding="#{pages$addContractViolation.contractStartDateText}" style="width: 186px; height: 18px"></h:inputText>
													  
													  <h:outputLabel styleClass="LABEL" value="#{msg['cancelContract.tab.unit.propertyname']}" />
													  <h:inputText styleClass="READONLY A_LEFT" readonly="true" binding="#{pages$addContractViolation.txtpropertyName}" style="width: 186px; height: 18px"></h:inputText>
													  
													  <h:outputLabel styleClass="LABEL" value="#{msg['unit.unitNumber']}" />
													  <h:inputText styleClass="READONLY A_LEFT"   readonly="true" binding="#{pages$addContractViolation.txtunitRefNum}" style="width: 186px; height: 18px"></h:inputText>
													  
													  <h:outputLabel styleClass="LABEL" value="#{msg['unit.rentValue']}" />
													  <h:inputText styleClass="READONLY A_RIGHT_NUM"  readonly="true" binding="#{pages$addContractViolation.txtUnitRentValue}" style="width: 186px; height: 18px"></h:inputText>
													  
												 </t:panelGrid>	  
												</t:panelGroup>
												
												  
											   <t:div style="padding-bottom:21px;#padding-bottom:25px;"> 	
												<t:panelGroup >
												 <t:panelGrid columns="2" cellpadding="1" cellspacing="5">						
												      
												      <h:outputLabel styleClass="LABEL" value="#{msg['contract.contractType']}" />
													  <h:inputText styleClass="READONLY A_LEFT"  readonly="true" binding="#{pages$addContractViolation.contractTypeText}" style="width: 186px; height: 18px"></h:inputText>
													  
													  <h:outputLabel styleClass="LABEL" value="#{msg['transferContract.oldTenant.Number']}" />
    												  <h:inputText styleClass="READONLY A_LEFT"  style="width: 186px; height: 18px" readonly="true" binding="#{pages$addContractViolation.tenantNumberType}"></h:inputText>
												        
													  <h:outputLabel styleClass="LABEL" value="#{msg['contract.endDate']}" />
													  <h:inputText styleClass="READONLY A_LEFT" readonly="true" binding="#{pages$addContractViolation.contractEndDateText}" style="width: 186px; height: 18px"></h:inputText>
																	
													  <h:outputLabel styleClass="LABEL" value="#{msg['property.type']}" />
													  <h:inputText styleClass="READONLY A_LEFT"  readonly="true" binding="#{pages$addContractViolation.txtpropertyType}" style="width: 186px; height: 18px"></h:inputText>
													  
													  <h:outputLabel styleClass="LABEL" value="#{msg['commons.replaceCheque.unitType']}" />
													  <h:inputText styleClass="READONLY A_LEFT" readonly="true" binding="#{pages$addContractViolation.txtUnitType}" style="width: 186px; height: 18px"></h:inputText>
													  
													
													 </t:panelGrid>
												</t:panelGroup>
											 </t:div>
										  </t:panelGrid> 
										</t:div>																						
								</rich:tab>		
								<rich:tab id="resultsTabVioaltions"
																	label="#{msg['violation.violations']}">
																	<t:panelGrid id="violationButtonGrid" width="100%"
																		columnClasses="BUTTON_TD" columns="3">
																		<h:outputText id="empty112" escape="false"
																			value="&nbsp;"></h:outputText>
																		<h:outputText id="empty212" escape="false"
																			value="&nbsp;"></h:outputText>
																		<t:panelGrid id="violationButtonGridSub" width="100%"
																			columnClasses="BUTTON_TD" columns="1">
																			<h:commandButton styleClass="BUTTON"
																				style="text-align:right;width:85px"
																				value="#{msg['violation.addViolation']}"
																				binding="#{pages$addContractViolation.addViolationButton}"
																				action="#{pages$addContractViolation.showAddViolationPopup}" />
																		</t:panelGrid>
																	</t:panelGrid>
																	<t:div style="text-align:center; width:100%">
																		<t:div styleClass="contentDiv"
																			style="text-align:center; width:98%">

																			<t:dataTable id="dtViolation" rows="5"
																				preserveDataModel="false" preserveSort="false"
																				var="dataItemVI" rules="all" renderedIfEmpty="true"
																				width="100%" 
																				value="#{pages$addContractViolation.violationViewDataList}"
																				binding="#{pages$addContractViolation.dataTableViolation}">

																				<t:column id="colViolUnitNumber"  rendered="false"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText id="otxtUnitNumbet"
																							value="#{msg['unit.unitNumber']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						style="WHITE-SPACE: normal"
																						value="#{dataItemVI.unitNumber}" />
																				</t:column>
																				<t:column id="colVioContract" 
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['contract.contractNumber']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						style="WHITE-SPACE: normal"
																						value="#{dataItemVI.contractNumber}" />
																				</t:column>
																				
																				<t:column id="colViolationDate" 
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText id="otxtVolDate"
																							value="#{msg['violation.Date']}" />
																					</f:facet>
																					<t:outputText id="otxtVolDateValue"
																						styleClass="A_LEFT"
																						value="#{dataItemVI.violationDateString}" />

																				</t:column>

																				<t:column id="colViolCategory" 
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText id="otxtVolCategory"
																							value="#{msg['violation.category']}" />
																					</f:facet>
																					<t:outputText id="otxtCatValueEn"
																						styleClass="A_LEFT" style="WHITE-SPACE: normal"
																						value="#{dataItemVI.categoryEn}"
																						rendered="#{pages$addContractViolation.isEnglishLocale}" />
																					<t:outputText id="otxtCatValueAr"
																						styleClass="A_LEFT" style="WHITE-SPACE: normal"
																						value="#{dataItemVI.categoryAr}"
																						rendered="#{pages$addContractViolation.isArabicLocale}" />

																				</t:column>
																				<t:column id="colViolType" 
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText id="otxtVoilType"
																							value="#{msg['violation.type']}" />
																					</f:facet>
																					<t:outputText id="otxtVoilTypeValueEn"
																						styleClass="A_LEFT" style="WHITE-SPACE: normal"
																						value="#{dataItemVI.typeEn}"
																						rendered="#{pages$addContractViolation.isEnglishLocale}" />
																					<t:outputText id="otxtVoilTypeValueAr"
																						styleClass="A_LEFT" style="WHITE-SPACE: normal"
																						value="#{dataItemVI.typeAr}"
																						rendered="#{pages$addContractViolation.isArabicLocale}" />
																				</t:column>
																				<t:column id="colViolTenant"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['contract.tenantName']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						style="WHITE-SPACE: normal"
																						value="#{dataItemVI.tenantName}" />
																				</t:column>

																				<t:column id="colVioStatus" 
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['violation.status']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						style="WHITE-SPACE: normal"
																						value="#{dataItemVI.statusEn}"
																						rendered="#{pages$addContractViolation.isEnglishLocale}" />
																					<t:outputText styleClass="A_LEFT"
																						style="WHITE-SPACE: normal"
																						value="#{dataItemVI.statusAr}"
																						rendered="#{pages$addContractViolation.isArabicLocale}" />

																				</t:column>
																				<t:column id="colViolationAction" sortable="false" rendered="#{pages$addContractViolation.enableDisableCommandLink}">
																					<f:facet name="header">
																						<t:outputText value="#{msg['commons.action']}" />
																					</f:facet>


																					<t:commandLink
																						action="#{pages$addContractViolation.btnEditViolation_Click}">
																						<h:graphicImage style="margin-left:5px;"
																							title="#{msg['commons.edit']}"
																							url="../resources/images/edit-icon.gif" />
																					</t:commandLink>

																					<t:commandLink
																						action="#{pages$addContractViolation.btnAddViolationAction_Click}">
																						<h:graphicImage style="margin-left:5px;"
																							title="#{msg['violation.violationAction']}"
																							url="../resources/images/app_icons/Action.png" 
																							rendered ="#{ ! empty dataItemVI.statusKey  && dataItemVI.statusKey!='CLOSED' }"
																							/>
																					</t:commandLink>
																					
																					<t:commandLink
																						action="#{pages$addContractViolation.btnDeleteViolation_Click}">
																						<h:graphicImage style="margin-left:5px;"
																							title="#{msg['violation.violationDelete']}"
																							url="../resources/images/delete.gif" 
																							rendered ="#{ ! empty dataItemVI.statusKey  && dataItemVI.statusKey!='CLOSED' }"
																							/>
																					</t:commandLink>
																					<h:commandLink 
																					action="#{pages$addContractViolation.btnBlackList_Click}"  >
																					<h:graphicImage style="margin-left:13px;" rendered ="#{ ! empty dataItemVI.statusKey  && dataItemVI.statusKey!='CLOSED' }"
																						title="#{msg['violationAction.blackList']}" url="../resources/images/blackListIcon.gif" />
																					</h:commandLink>	
																					

																				</t:column>

																			</t:dataTable>
																		</t:div>
																		<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99.1%;">
																			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
																				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																					<t:div styleClass="JUG_NUM_REC_ATT">
																						<h:outputText value="#{msg['commons.records']}"/>
																						<h:outputText value=" : "/>
																						<h:outputText value="#{pages$addContractViolation.recordSize}" />
																					</t:div>
																				</t:panelGroup>
																				<t:panelGroup>
																					<t:dataScroller id="ViolationScroller" for="dtViolation" paginator="true"  
																						fastStep="1" paginatorMaxPages="5" immediate="false"
																						paginatorTableClass="paginator"
																						renderFacetsIfSinglePage="true" 												
																						paginatorTableStyle="grid_paginator" layout="singleTable" 
																						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																						paginatorActiveColumnStyle="font-weight:bold;"
																						paginatorRenderLinkForActive="false" 
																						pageIndexVar="pageNumber"
																						styleClass="JUG_SCROLLER">
																						<f:facet name="first">
																							<t:graphicImage url="../#{path.scroller_first}" id="lblF1"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastrewind">
																							<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR1"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastforward">
																							<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF1"></t:graphicImage>
																						</f:facet>
																						<f:facet name="last">
																							<t:graphicImage url="../#{path.scroller_last}" id="lblL1"></t:graphicImage>
																						</f:facet>
																						<t:div styleClass="PAGE_NUM_BG" rendered="true">
																							<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																							<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																						</t:div>
																					</t:dataScroller>
																				</t:panelGroup>
																			</t:panelGrid>
																		</t:div>
																	</t:div>
																</rich:tab>
								
																<rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}">
								          								    <%@  include file="attachment/attachment.jsp"%>
																</rich:tab>
																<rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}">
											              					<%@ include file="notes/notes.jsp"%>
																</rich:tab>
																		</rich:tabPanel>
											
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
													<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>
												</div>
												</td>
											</tr>
											<tr>
												<td class="BUTTON_TD MARGIN" colspan="8">												
													
													<h:commandButton styleClass="BUTTON"  
													binding="#{pages$addContractViolation.saveButton}"
													action="#{pages$addContractViolation.doSave}"></h:commandButton>

													<h:commandButton styleClass="BUTTON" style="width: 75px" 
													value="#{msg['commons.cancel']}"
													action="#{pages$addContractViolation.doCancel}"></h:commandButton>												
												  </td>
											  </tr>
										  </table>
									  </h:form>
									</div>	
								</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>

				</table>
			</div>
		</body>
	</html>
</f:view>