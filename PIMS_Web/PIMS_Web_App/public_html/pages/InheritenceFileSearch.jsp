<%-- 
  - Author: imran.mirza
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching a inheritence file
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
		function obtainFileBlockingNotes()
		{
		  document.getElementById("searchFrm:obtainFileBlockingNotes").onclick();
		  
		}
	
	
	    function resetValues()
      	{
      	  
      		document.getElementById("searchFrm:fileNumber").value="";
      		document.getElementById("searchFrm:createdOn").value="";
      		document.getElementById("searchFrm:createdBy").value="";
      		document.getElementById("searchFrm:researcher").value="";
			document.getElementById("searchFrm:fileType").selectedIndex=0;
			document.getElementById("searchFrm:fileStatus").selectedIndex=0;      	
			document.getElementById("searchFrm:benificiary").value="";
			document.getElementById("searchFrm:filePerson").value="";
			document.getElementById("searchFrm:assetNo").value="";
			document.getElementById("searchFrm:assetType").selectedIndex=0;
			document.getElementById("searchFrm:assetNameEn").value="";    
      	    document.getElementById("searchFrm:assetNameAr").value="";  
      	    document.getElementById("searchFrm:assetDesc").value="";  
      	    document.getElementById("searchFrm:sponsor").value="";  
      	    document.getElementById("searchFrm:guardian").value="";    	  
        }        
		
		function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	function closeWindow()
		{
		  window.close();
		}
        
        	  function	openPopUp()
		{
		
		    var width = 300;
            var height = 300;
            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + ",menubar=yes,toolbar=yes,scrollbars=yes,resizable=yes,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

            var child1 = window.open("about:blank", "subWind", windowFeatures);

		      child1.focus();
		}
		
		
			function GenerateUnitsPopup()
		{
		      var screen_width = 1024;
		      var screen_height = 450;
		      var popup_width = screen_width-250;
		      var popup_height = screen_height;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('editUnitPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<c:choose>
				<c:when test="${!pages$InheritenceFileSearch.isViewModePopUp}">
					<div class="containerDiv">
				</c:when>
			</c:choose>

			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<c:choose>
					<c:when test="${!pages$InheritenceFileSearch.isViewModePopUp}">
						<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>
					</c:when>
				</c:choose>


				<tr width="100%">

					<c:choose>
						<c:when test="${!pages$InheritenceFileSearch.isViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>

					<td width="83%" height="470px" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['searchInheritenceFile.title']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="searchFrm" enctype="multipart/form-data">

										<h:commandLink id="obtainFileBlockingNotes"
											action="#{pages$InheritenceFileSearch.blockFile}">
										</h:commandLink>
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$InheritenceFileSearch.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$InheritenceFileSearch.successMessages}"
																escape="false" styleClass="INFO_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width: 99.8%;">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px" border="0"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">
																	<tr>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchInheritenceFile.fileNo']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:inputText id="fileNumber"
																				binding="#{pages$InheritenceFileSearch.textInheritenceFileNumber}"
																				tabindex="10">
																			</h:inputText>
																		</td>

																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchInheritenceFile.fileType']}:"></h:outputLabel>

																		</td>
																		<td>
																			<h:selectOneMenu id="fileType"
																				binding="#{pages$InheritenceFileSearch.listInheritenceFileType}">

																				<f:selectItems
																					value="#{pages$InheritenceFileSearch.fileTypeList}" />
																			</h:selectOneMenu>
																		</td>

																	</tr>
																	<tr>

																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['inheritanceFileSearch.createdOnFrom']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<rich:calendar id="createdOnFromId"
																				binding="#{pages$InheritenceFileSearch.clndrCreatedOnFrom}"
																				locale="#{pages$InheritenceFileSearch.locale}"
																				popup="true"
																				datePattern="#{pages$InheritenceFileSearch.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:185px;height:17px" />

																		</td>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['inheritanceFileSearch.createdOnTo']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<rich:calendar id="createdOnToId"
																				binding="#{pages$InheritenceFileSearch.clndrCreatedOnTo}"
																				locale="#{pages$InheritenceFileSearch.locale}"
																				popup="true"
																				datePattern="#{pages$InheritenceFileSearch.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:185px;height:17px" />

																		</td>

																	</tr>

																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.createdBy']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="createdBy"
																				binding="#{pages$InheritenceFileSearch.listCreatedBy}">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$InheritenceFileSearch.createdByList}" />
																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchInheritenceFile.researcher']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="researcher"
																				binding="#{pages$InheritenceFileSearch.listResearchedBy}">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$InheritenceFileSearch.createdByList}" />
																			</h:selectOneMenu>
																		</td>

																	</tr>
																	<tr>

																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.status']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="fileStatus"
																				binding="#{pages$InheritenceFileSearch.listInheritenceFileStatus}">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.inheritenceFileStatusList}" />
																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchInheritenceFile.filePerson']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="filePerson"
																				binding="#{pages$InheritenceFileSearch.textFilePerson}"
																				tabindex="1">
																			</h:inputText>
																		</td>

																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchInheritenceFile.benificiary']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="benificiary"
																				binding="#{pages$InheritenceFileSearch.textBenificiary}">
																			</h:inputText>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['SearchBeneficiary.nurse']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="nurse"
																				binding="#{pages$InheritenceFileSearch.textNurse}" />
																		</td>

																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchInheritenceFile.assetNo']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="assetNo"
																				binding="#{pages$InheritenceFileSearch.textAssetNo}"
																				tabindex="1" />
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchInheritenceFile.assetType']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="assetType"
																				binding="#{pages$InheritenceFileSearch.listAssetType}"
																				tabindex="3">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.fileAssetTypeList}" />
																			</h:selectOneMenu>

																		</td>
																	</tr>

																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchInheritenceFile.assetNameEn']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="assetNameEn"
																				binding="#{pages$InheritenceFileSearch.textAssetNameEn}"
																				tabindex="1" />
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchInheritenceFile.assetNameAr']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="assetNameAr"
																				styleClass="A_RIGHT_NUM"
																				binding="#{pages$InheritenceFileSearch.textAssetNameAr}"
																				tabindex="1" />
																		</td>
																	</tr>

																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchInheritenceFile.assetDesc']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="assetDesc"
																				binding="#{pages$InheritenceFileSearch.textAssetDesc}"
																				tabindex="6">
																			</h:inputText>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchInheritenceFile.sponsor']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="sponsor"
																				binding="#{pages$InheritenceFileSearch.textSponsor}"
																				tabindex="7">
																			</h:inputText>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchInheritenceFile.guardian']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="guardian"
																				binding="#{pages$InheritenceFileSearch.textGuardian}"
																				tabindex="8" />
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['inheritanceFile.fieldLabel.newCostCenter']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="newCC"
																				binding="#{pages$InheritenceFileSearch.textNewCC}"
																				tabindex="8" />
																		</td>

																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['inheritanceFile.fieldLabel.oldCostCenter']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="oldCC"
																				binding="#{pages$InheritenceFileSearch.textOldCC}"
																				tabindex="8" />
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['inheritanceFile.fieldLabel.benCostCenter']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="benCC"
																				binding="#{pages$InheritenceFileSearch.textBenCC}"
																				tabindex="8" />
																		</td>
																	</tr>
																	<tr>

																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['fileSearch.fieldLabel.migrated']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="isMigrated"
																				binding="#{pages$InheritenceFileSearch.migrated}">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItem
																					itemLabel="#{msg['fileSearch.comboValue.migrated']}"
																					itemValue="1" />
																				<f:selectItem
																					itemLabel="#{msg['fileSearch.comboValue.notmigrated']}"
																					itemValue="2" />
																			</h:selectOneMenu>
																		</td>

																	</tr>

																	<tr>
																		<td class="BUTTON_TD" colspan="4">
																			<h:commandButton styleClass="BUTTON" type="submit"
																				value="#{msg['commons.search']}"
																				action="#{pages$InheritenceFileSearch.doSearch}" />

																			<pims:security
																				screen="Pims.MinorMgmt.InheritanceFile.Save"
																				action="view">
																				<h:commandButton styleClass="BUTTON"
																					action="#{pages$InheritenceFileSearch.createFile}"
																					value="#{msg['mems.fileSearch.buttonLabel.createFile']}" />
																			</pims:security>
																			<h:commandButton styleClass="BUTTON" type="button"
																				onclick="javascript:resetValues();"
																				value="#{msg['commons.clear']}" />

																			<h:commandButton styleClass="BUTTON" type="button"
																				rendered="#{pages$InheritenceFileSearch.isViewModePopUp}"
																				value="#{msg['commons.cancel']}"
																				onclick="javascript:window.close();">
																			</h:commandButton>
																		</td>

																	</tr>
																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>

														</tr>



													</table>
												</div>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px; width: 95%;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" style="width: 99%">
													<t:dataTable id="test2"
														value="#{pages$InheritenceFileSearch.inheritenceFileDataList}"
														binding="#{pages$InheritenceFileSearch.dataTable}"
														width="100%"
														rows="#{pages$InheritenceFileSearch.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true">

														<t:column id="col1" width="10%" defaultSorted="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:commandSortHeader columnName="fileNumber"
																	actionListener="#{pages$InheritenceFileSearch.sort}"
																	value="#{msg['searchInheritenceFile.fileNo']}"
																	arrow="true">
																	<f:attribute name="sortField" value="fileNumber" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.fileNumber}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="filePerson" width="10%" defaultSorted="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:commandSortHeader columnName="firstName"
																	actionListener="#{pages$InheritenceFileSearch.sort}"
																	value="#{msg['SearchBeneficiary.filePerson']}"
																	arrow="true">
																	<f:attribute name="sortField" value="firstName" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.filePerson}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="costCenter" width="10%" sortable="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:commandSortHeader columnName="fileType"
																	actionListener="#{pages$InheritenceFileSearch.sort}"
																	value="#{msg['searchInheritenceFile.fileType']}"
																	arrow="true">
																	<f:attribute name="sortField" value="fileType" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText
																value="#{pages$InheritenceFileSearch.isArabicLocale?dataItem.fileTypeAr:dataItem.fileTypeEn}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column id="col7" width="10%"
															style="white-space: normal;"
															rendered="#{pages$InheritenceFileSearch.isEnglishLocale}"
															sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="status"
																	actionListener="#{pages$InheritenceFileSearch.sort}"
																	value="#{msg['commons.status']}" arrow="true">
																	<f:attribute name="sortField" value="status" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.statusEn}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="statusArCol" width="10%"
															style="white-space: normal;"
															rendered="#{pages$InheritenceFileSearch.isArabicLocale}"
															sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="status"
																	actionListener="#{pages$InheritenceFileSearch.sort}"
																	value="#{msg['commons.status']}" arrow="true">
																	<f:attribute name="sortField" value="status" />
																</t:commandSortHeader>

															</f:facet>
															<t:outputText value="#{dataItem.statusAr}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>


														<t:column id="col2" width="7%" sortable="true"
															style="white-space: normal;">
															<f:facet name="header">
																<t:commandSortHeader columnName="researcher"
																	actionListener="#{pages$InheritenceFileSearch.sort}"
																	value="#{msg['searchInheritenceFile.researcher']}"
																	arrow="true">
																	<f:attribute name="sortField" value="researcher" />
																</t:commandSortHeader>

															</f:facet>
															<t:outputText value="#{dataItem.researcherAr}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column id="col3" width="10%" sortable="true"
															style="white-space: normal;">
															<f:facet name="header">
																<t:commandSortHeader columnName="createdOn"
																	actionListener="#{pages$InheritenceFileSearch.sort}"
																	value="#{msg['commons.createdOn']}" arrow="true">
																	<f:attribute name="sortField" value="createdOn" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.createdOn}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="propertyCommercialNameCol" width="15%"
															style="white-space: normal;" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="createdBy"
																	actionListener="#{pages$InheritenceFileSearch.sort}"
																	value="#{msg['commons.createdBy']}" arrow="true">
																	<f:attribute name="sortField" value="createdBy" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.createdBy}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column width="10%" sortable="false"
															style="white-space: normal;">
															<f:facet name="header">
																<t:commandSortHeader columnName="costCenter"
																	actionListener="#{pages$InheritenceFileSearch.sort}"
																	value="#{msg['inheritanceFile.fieldLabel.newCostCenter']}"
																	arrow="true">
																	<f:attribute name="sortField" value="costCenter" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.costCenter}" />
														</t:column>
														<t:column width="10%" sortable="false"
															style="white-space: normal;">
															<f:facet name="header">
																<t:commandSortHeader columnName="oldCostCenter"
																	actionListener="#{pages$InheritenceFileSearch.sort}"
																	value="#{msg['inheritanceFile.fieldLabel.oldCostCenter']}"
																	arrow="true">
																	<f:attribute name="sortField" value="oldCostCenter" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.oldCostCenter}" />
														</t:column>
														<t:column id="col8" sortable="false" width="10%"
															style="white-space: normal;"
															rendered="#{pages$InheritenceFileSearch.isPageModeSelectOnePopUp && !dataItem.blocked}">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.select']}" />
															</f:facet>
															<t:commandLink
																actionListener="#{pages$InheritenceFileSearch.sendFileInfoToParent}">
																<h:graphicImage style="width: 16;height: 16;border: 0"
																	alt="#{msg['commons.select']}"
																	url="../resources/images/select-icon.gif"></h:graphicImage>
															</t:commandLink>
														</t:column>
														<t:column id="col9" width="10%" sortable="false"
															style="white-space: normal;"
															rendered="#{pages$InheritenceFileSearch.isPageModeSelectManyPopUp && !dataItem.blocked}">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.select']}" />
															</f:facet>
															<h:selectBooleanCheckbox id="select"
																value="#{dataItem.selected}" />
														</t:column>
														<t:column id="editCol" width="5%"
															style="white-space: normal;"
															rendered="#{!pages$InheritenceFileSearch.isPageModeSelectManyPopUp && !pages$InheritenceFileSearch.isPageModeSelectOnePopUp}"
															sortable="false">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.edit']}" />
															</f:facet>
															<t:commandLink id="nol"
																action="#{pages$InheritenceFileSearch.editFile}"
																rendered="#{!pages$InheritenceFileSearch.isPageModeSelectManyPopUp}">
																<h:graphicImage id="nolIcon"
																	title="#{msg['commons.edit']}"
																	url="../resources/images/edit-icon.gif" />
															</t:commandLink>

															<pims:security screen="Pims.MinorMgmt.TakharujManagement"
																action="view">
																<t:commandLink
																	action="#{pages$InheritenceFileSearch.onCreateTakharujRequest}">
																	<h:graphicImage
																		rendered="#{dataItem.domainDataKeyEn=='FINAL_LIMITATION_DONE' && !dataItem.blocked}"
																		title="#{msg['takharuj.request.create']}"
																		url="../resources/images/detail-icon.gif" />
																</t:commandLink>
															</pims:security>

															<pims:security screen="Pims.MinorMgmt.NOL.Create" action="view">
																<t:commandLink
																	action="#{pages$InheritenceFileSearch.issueNOL}"
																	rendered="#{!pages$InheritenceFileSearch.isPageModeSelectManyPopUp && !dataItem.blocked &&
																				  
																				 dataItem.domainDataKeyEn!='CANCELLED'  &&
																				 dataItem.domainDataKeyEn!='CLOSED' && dataItem.domainDataKeyEn!='UNDER_CONVERSION' && dataItem.domainDataKeyEn!='CONVERTED'
																				 
																	}">
																	<h:graphicImage id="editIcon2"
																		title="#{msg['contract.screenName.issueNOL']}"
																		url="../resources/images/app_icons/No-Objection-letter.png" />
																</t:commandLink>
															</pims:security>

															<pims:security screen="Pims.MinorMgmt.CollectionRequest"
																action="view">
																<t:commandLink id="collectionProc"
																	action="#{pages$InheritenceFileSearch.openCollectionProc}"
																	rendered="#{!pages$InheritenceFileSearch.isPageModeSelectManyPopUp && !dataItem.blocked
																	
																				&& dataItem.domainDataKeyEn!='NEW'&& 
																				 dataItem.domainDataKeyEn!='REJECTED_RESUBMIT'  &&
																				 dataItem.domainDataKeyEn!='APPROVAL_REQUIRED'  && 
																				 dataItem.domainDataKeyEn!='CANCELLED'  &&
																				 dataItem.domainDataKeyEn!='CLOSED' && dataItem.domainDataKeyEn!='UNDER_CONVERSION' && dataItem.domainDataKeyEn!='CONVERTED'
																				 
																				 
																				 
																	}">
																	<h:graphicImage id="collectionProcedureIcon"
																		title="#{msg['collectionProcedure.header']}"
																		url="../resources/images/app_icons/Add-Paid-Facility.png" />
																</t:commandLink>
															</pims:security>


															<pims:security
																screen="Pims.MinorMgmt.MatureSharePaymentRequest"
																action="view">
																<t:commandLink
																	action="#{pages$InheritenceFileSearch.issuePaymentRequest}"
																	rendered="#{!pages$InheritenceFileSearch.isPageModeSelectManyPopUp && 
																				!dataItem.blocked &&
																				 dataItem.domainDataKeyEn!='NEW'&& 
																				 dataItem.domainDataKeyEn!='REJECTED_RESUBMIT'  &&
																				 dataItem.domainDataKeyEn!='APPROVAL_REQUIRED'  && 
																				 dataItem.domainDataKeyEn!='CANCELLED'  &&
																				 dataItem.domainDataKeyEn!='CLOSED' && dataItem.domainDataKeyEn!='UNDER_CONVERSION' && dataItem.domainDataKeyEn!='CONVERTED'
																				}">
																	<h:graphicImage id="paymentReq"
																		title="#{msg['mems.payment.label.title']}"
																		url="../resources/images/app_icons/Pay-Fine.png" />
																</t:commandLink>
															</pims:security>

															<pims:security screen="Pims.MinorMgmt.InvestmentRequest"
																action="view">
																<t:commandLink
																	action="#{pages$InheritenceFileSearch.issueInvestmentRequest}"
																	rendered="#{!pages$InheritenceFileSearch.isPageModeSelectManyPopUp && 
																				!dataItem.blocked &&
																				 dataItem.domainDataKeyEn!='NEW'&& 
																				 dataItem.domainDataKeyEn!='REJECTED_RESUBMIT'  &&
																				 dataItem.domainDataKeyEn!='APPROVAL_REQUIRED'  && 
																				 dataItem.domainDataKeyEn!='CANCELLED'  &&
																				 dataItem.domainDataKeyEn!='CLOSED' && dataItem.domainDataKeyEn!='UNDER_CONVERSION' && dataItem.domainDataKeyEn!='CONVERTED'
																				
																				}">
																	<h:graphicImage id="invtReq"
																		title="#{msg['mems.investment.label.title']}"
																		url="../resources/images/app_icons/Lease-contract.png" />
																</t:commandLink>
															</pims:security>



															<pims:security
																screen="Pims.MinorMgmt.DisbursementRequest.Create"
																action="view">
																<t:commandLink
																	action="#{pages$InheritenceFileSearch.issueNormalPaymentRequest}"
																	rendered="#{!pages$InheritenceFileSearch.isPageModeSelectManyPopUp && 
																				 !dataItem.blocked &&
																				 dataItem.domainDataKeyEn!='NEW'&& 
																				 dataItem.domainDataKeyEn!='REJECTED_RESUBMIT'  &&
																				 dataItem.domainDataKeyEn!='APPROVAL_REQUIRED'  && 
																				 dataItem.domainDataKeyEn!='CANCELLED'  &&
																				 dataItem.domainDataKeyEn!='CLOSED' && dataItem.domainDataKeyEn!='UNDER_CONVERSION' && dataItem.domainDataKeyEn!='CONVERTED'
																				    
																				}">
																	<h:graphicImage id="normalPaymentReq"
																		title="#{msg['mems.normaldisb.label.heading']}"
																		url="../resources/images/app_icons/replaceCheque.png" />
																</t:commandLink>
															</pims:security>
															<pims:security
																screen="Pims.MinorMgmt.MinorMaintenanceRequest"
																action="view">
																<t:commandLink
																	action="#{pages$InheritenceFileSearch.onMinorMaintenanceRequest}">
																	<h:graphicImage
																		rendered="#{dataItem.domainDataKeyEn=='FINAL_LIMITATION_DONE' && !dataItem.blocked}"
																		title="#{msg['BPM.WorkList.MEMSMinorMaintenanceRequest.RequestTitle']}"
																		url="../resources/images/app_icons/Maintenance-Request-Managem.png" />
																</t:commandLink>
															</pims:security>
															<pims:security
																screen="Pims.MinorMgmt.InheritanceFile.Save"
																action="view">
																<t:commandLink
																	rendered="#{!dataItem.blocked && dataItem.domainDataKeyEn!='CANCELLED' && 
																				 dataItem.domainDataKeyEn!='CLOSED' && 
																				 dataItem.domainDataKeyEn!='UNDER_CONVERSION' && dataItem.domainDataKeyEn!='CONVERTED'
																				}"
																	onclick="if (!confirm('#{msg['warning.sureToBlock']}')) return false;"
																	action="#{pages$InheritenceFileSearch.openFileBlockingComments}">
																	<h:graphicImage
																		style="MARGIN: 1px 1px -5px;cursor:hand"
																		title="#{msg['iconTitle.block']}"
																		url="../resources/images/app_icons/file_block.gif" />
																</t:commandLink>
																<t:commandLink
																	rendered="#{dataItem.blocked && sessionScope['Current_User'].loginId=='pims_admin' &&
																				 dataItem.domainDataKeyEn!='CLOSED' && 
																				 dataItem.domainDataKeyEn!='CANCELLED' &&
																				 dataItem.domainDataKeyEn!='UNDER_CONVERSION' && dataItem.domainDataKeyEn!='CONVERTED'
																	}"
																	onclick="if (!confirm('#{msg['warning.sureToUnBlock']}')) return false;"
																	action="#{pages$InheritenceFileSearch.openFileUnblockingComments}">

																	<h:graphicImage
																		style="MARGIN: 1px 1px -5px;cursor:hand"
																		title="#{msg['iconTitle.unBlock']}"
																		url="../resources/images/app_icons/file_allow.gif" />
																</t:commandLink>
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.ModifyEstateLimitation.Save"
																action="view">

																<t:commandLink
																	action="#{pages$InheritenceFileSearch.onModifyEstateLimitationRequest}">
																	<h:graphicImage
																		title="#{msg['modifyEstateLimitation.lbl.ModifyEstateLimitation']}"
																		alt="#{msg['modifyEstateLimitation.lbl.ModifyEstateLimitation']}"
																		url="../resources/images/app_icons/Amend-Contract.png" /> />
																</t:commandLink>

															</pims:security>

															<pims:security screen="Pims.EndowMgmt.EndowmentFile.Save"
																action="view">
																<t:commandLink
																	action="#{pages$InheritenceFileSearch.onTransferToEndowmentFile}">
																	<h:graphicImage
																		rendered="#{dataItem.domainDataKeyEn=='FINAL_LIMITATION_DONE' && 
																					!dataItem.blocked
																					}"
																		title="#{msg['inheritanceFile.lbl.transferToEndowment']}"
																		url="../resources/images/app_icons/Transfer-Contract.png" />
																</t:commandLink>
															</pims:security>
															<t:commandLink
																action="#{pages$InheritenceFileSearch.onBlockingRequest}"
																rendered="#{	
																			!dataItem.blocked &&
																			 dataItem.domainDataKeyEn!='CLOSED' && 
																			 dataItem.domainDataKeyEn!='CANCELLED' &&
																			 dataItem.domainDataKeyEn!='UNDER_CONVERSION' && dataItem.domainDataKeyEn!='CONVERTED'
																			}">

																<h:graphicImage style="MARGIN: 1px 1px -5px;cursor:hand"
																	title="#{msg['blocking.lbl.heading']}"
																	url="../resources/images/app_icons/blacklist_tenant.png" />
															</t:commandLink>
															<pims:security
																screen="Pims.MinorMgmt.DisbursementRequest"
																action="view">
																<t:commandLink
																	action="#{pages$InheritenceFileSearch.onMinorStatusChangeRequest}"
																	rendered="#{!pages$InheritenceFileSearch.isPageModeSelectManyPopUp && 
																				 !dataItem.blocked &&
																				 dataItem.domainDataKeyEn!='NEW'&& 
																				 dataItem.domainDataKeyEn!='REJECTED_RESUBMIT'  &&
																				 dataItem.domainDataKeyEn!='APPROVAL_REQUIRED'  && 
																				 dataItem.domainDataKeyEn!='CANCELLED'  &&
																				 dataItem.domainDataKeyEn!='CLOSED' && dataItem.domainDataKeyEn!='UNDER_CONVERSION' && dataItem.domainDataKeyEn!='CONVERTED'
																				    
																				}">
																	<h:graphicImage id="minorStatusChange"
																		title="#{msg['minorStatusChange.heading']}"
																		url="../resources/images/app_icons/Change-tenant.png" />
																</t:commandLink>
															</pims:security>
															<t:commandLink
																action="#{pages$InheritenceFileSearch.printFileDetailsReport}">
																<h:graphicImage style="MARGIN: 1px 1px -5px;cursor:hand"
																	title="#{msg['reportIconToolTip.printFileDetails']}"
																	url="../resources/images/app_icons/print.gif" />
															</t:commandLink>

															<t:commandLink
																action="#{pages$InheritenceFileSearch.printInheritanceDetailsReport}">
																<h:graphicImage style="MARGIN: 1px 1px -5px;cursor:hand"
																	title="#{msg['reportTooltip.inheritanceDtlReport']}"
																	url="../resources/images/app_icons/print.gif" />
															</t:commandLink>

															<t:commandLink
																action="#{pages$InheritenceFileSearch.openStatmntOfAccountNew}">
																<h:graphicImage style="MARGIN: 1px 1px -5px;cursor:hand"
																	title="#{msg['report.tooltip.PrintStOfAcc']}"
																	url="../resources/images/app_icons/print.gif" />
															</t:commandLink>
															
															<t:commandLink
																action="#{pages$InheritenceFileSearch.onRequestsOnInheritanceFile}">
																<h:graphicImage style="MARGIN: 1px 1px -5px;cursor:hand"
																	title="#{msg['report.tooltip.RequestsonInheritanceFiles']}"
																	url="../resources/images/app_icons/print.gif" />
															</t:commandLink>

														</t:column>
													</t:dataTable>
												</div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:100%;#width:100%;">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$InheritenceFileSearch.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">

																<t:div styleClass="PAGE_NUM_BG" style="#width:20%">
																	<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																			</td>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{pages$InheritenceFileSearch.currentPage}" />
																			</td>
																		</tr>
																	</table>
																</t:div>
																<TABLE border="0" class="SCH_SCROLLER">
																	<tr>
																		<td>
																			<t:commandLink
																				action="#{pages$InheritenceFileSearch.pageFirst}"
																				disabled="#{pages$InheritenceFileSearch.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$InheritenceFileSearch.pagePrevious}"
																				disabled="#{pages$InheritenceFileSearch.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFR"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:dataList
																				value="#{pages$InheritenceFileSearch.pages}"
																				var="page">
																				<h:commandLink value="#{page}"
																					actionListener="#{pages$InheritenceFileSearch.page}"
																					rendered="#{page != pages$InheritenceFileSearch.currentPage}" />
																				<h:outputText value="<b>#{page}</b>" escape="false"
																					rendered="#{page == pages$InheritenceFileSearch.currentPage}" />
																			</t:dataList>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$InheritenceFileSearch.pageNext}"
																				disabled="#{pages$InheritenceFileSearch.firstRow + pages$InheritenceFileSearch.rowsPerPage >= pages$InheritenceFileSearch.totalRows}">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$InheritenceFileSearch.pageLast}"
																				disabled="#{pages$InheritenceFileSearch.firstRow + pages$InheritenceFileSearch.rowsPerPage >= pages$InheritenceFileSearch.totalRows}">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblL"></t:graphicImage>
																			</t:commandLink>
																		</td>
																	</tr>
																</TABLE>
															</td>
														</tr>
													</table>
												</t:div>
											</div>
											<table width="96.6%" border="0">
												<tr>
													<td class="BUTTON_TD JUG_BUTTON_TD_US">

														<h:commandButton id="btnSelect" type="submit"
															rendered="#{pages$InheritenceFileSearch.isPageModeSelectManyPopUp}"
															styleClass="BUTTON"
															actionListener="#{pages$InheritenceFileSearch.sendManyFilesInfoToParent}"
															value="#{msg['commons.select']}" />
													</td>

												</tr>


											</table>
										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
					<td colspan="2">
						<c:choose>
							<c:when test="${!pages$InheritenceFileSearch.isViewModePopUp}">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</tr>
								</table>
							</c:when>
						</c:choose>
					</td>
				</tr>
			</table>
			<c:choose>
				<c:when test="${!pages$InheritenceFileSearch.isViewModePopUp}">
					</div>
				</c:when>
			</c:choose>

		</body>
	</html>
</f:view>