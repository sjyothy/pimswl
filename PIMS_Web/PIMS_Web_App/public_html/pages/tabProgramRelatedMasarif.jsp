<%-- Author: Kamran Ahmed--%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<script language="JavaScript" type="text/javascript"> 



	function onComboBeneficiaryClick()
	{
		var cbo = document.getElementById("memsFollowupForm:cboBeneficiaryPerson");
		var id = cbo.options[cbo.selectedIndex].value;
	    if( id>0)
	    {
	       openBeneficiaryPopup(id);
	    }	
				
	}
</script>





<t:div styleClass="contentDiv"
	style="width:95%;">
	<t:dataTable id="socialResearchGrid"
		value="#{pages$endowmentPrograms.programRelatedMasarif}"
		preserveDataModel="true" preserveSort="false" var="dataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">


		<t:column sortable="true">
			<f:facet name="header">
				<h:outputText style="white-space: normal;"
					value="#{msg['masraf.label.masrafName']}" />
			</f:facet>
			<h:outputText style="white-space: normal;"
				value="#{dataItem.masraf.masrafName}" />
		</t:column>

		<t:column sortable="true">
			<f:facet name="header">
				<h:outputText style="white-space: normal;"
					value="#{msg['masraf.label.masrafBank']}" />
			</f:facet>

			<h:outputText style="white-space: normal;"
				value="#{dataItem.masraf.bank.bankAr}" />
		</t:column>
		<t:column sortable="true">
			<f:facet name="header">
				<h:outputText style="white-space: normal;"
					value="#{msg['commons.percentage']}" />
			</f:facet>

			<h:inputText style="width:100%;white-space: normal;"
				readonly="#{!pages$endowmentPrograms.showSaveButton && 
							!pages$endowmentPrograms.showResubmitButton 
					       }"
				value="#{dataItem.percentageString}"
				onkeyup="removeNonInteger(this)" onkeypress="removeNonInteger(this)"
				onkeydown="removeNonInteger(this)" onblur="removeNonInteger(this)"
				onchange="removeNonInteger(this)" />
		</t:column>


	</t:dataTable>
</t:div>
