<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript"> 

		function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		
		function populateContractor(contractorId, contractorNumber, contractorNameEn, contractorNameAr, licenseNumber, licenseSource, officeNumber){
			document.getElementById("tenderInquiryFrm:hdnContractorId").value=contractorId;
			document.getElementById("tenderInquiryFrm:hdnContractorNumber").value=contractorNumber;
			document.getElementById("tenderInquiryFrm:hdnContractorNameEn").value=contractorNameEn;
			document.getElementById("tenderInquiryFrm:hdnContractorNameAr").value=contractorNameAr;
			
		    document.forms[0].submit();
		}
		
		function populateTender(tenderId,tenderNumber,tenderDescription)
		{
			 document.getElementById("tenderInquiryFrm:hdnTenderId").value=tenderId;
			 document.getElementById("tenderInquiryFrm:hdnTenderNumber").value=tenderNumber;
			 document.getElementById("tenderInquiryFrm:hdnTenderDescription").value=tenderDescription;
		     
		     document.forms[0].submit();
		}
		
		function submitForm()
		{
			window.document.forms[0].submit();
		}	
	 
		function showContractorPopup()
		{
			var screen_width = screen.width;
			var screen_height = screen.height;
			var popup_width = screen_width-180;
			var popup_height = screen_height-265;
			var leftPos = (screen_width-popup_width)/2 -5, topPos = (screen_height-popup_height)/2 - 20;
			var popup = window.open('contractorSearch.jsf?viewMode=popup','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
			popup.focus();
		}
		
	    function showSearchTenderPopUp()
		{
			var screen_width = screen.width;
			var screen_height = screen.height;
			var popup_width = screen_width-180;
			var popup_height = screen_height-240;
			var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
			
			var popup = window.open('tenderSearch.jsf?VIEW_MODE=popup','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
			popup.focus();
		}	

       function closeWindow() 
       { 
	      window.close();
	   }
	  function showTenderInquiryRepliesAll()
	        {
			   var screen_width = 1024;
			   var screen_height = 586;
			   
	            window.open('tenderInquiryReplayAll.jsf','_blank','width='+(screen_width-400)+',height='+(screen_height-250)+',left=40,top=40,scrollbars=yes,status=yes');
	        }
	   
	   	function  receiveReplies()
	{
	  document.getElementById('tenderInquiryFrm:lnkReceiveReplies').onclick();
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 <script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>


	
</head>
	<body class="BODY_STYLE">
	  <div class="containerDiv">
	<%	
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header --> 
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<c:choose>
	 <c:when test="${pages$tenderInquiryReplies.hideHeader}">
		 <tr>
			<td colspan="2">
				<jsp:include page="header.jsp" />
			</td>
		</tr>
	</c:when>
   </c:choose>	
		<tr width="100%">
		<c:choose>
	      <c:when test="${pages$tenderInquiryReplies.hideHeader}">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
		  </c:when>
		</c:choose>	
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['tenderInquiryAdd.heading']}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>
				<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%">
					<tr valign="top">
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
						<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
							<div class="SCROLLABLE_SECTION" style="height:475px;width:100%;#height:466px;">
									<h:form id="tenderInquiryFrm" enctype="multipart/form-data" style="WIDTH: 96%;">
	
									<h:inputHidden id="hdnContractorId" value="#{pages$tenderInquiryReplies.hdnContractorId}"></h:inputHidden>
									<h:inputHidden id="hdnContractorNumber" value="#{pages$tenderInquiryReplies.hdnContractorNumber}"></h:inputHidden>
									<h:inputHidden id="hdnContractorNameEn" value="#{pages$tenderInquiryReplies.hdnContractorNameEn}"></h:inputHidden>
									<h:inputHidden id="hdnContractorNameAr" value="#{pages$tenderInquiryReplies.hdnContractorNameAr}"></h:inputHidden>
									<h:inputHidden id="hdnTenderId" value="#{pages$tenderInquiryReplies.hdnTenderId}"></h:inputHidden>
									<h:inputHidden id="hdnTenderNumber" value="#{pages$tenderInquiryReplies.hdnTenderNumber}"></h:inputHidden>
									<h:inputHidden id="hdnTenderDescription" value="#{pages$tenderInquiryReplies.hdnTenderDescription}"></h:inputHidden>
									<h:inputHidden id="hdnApplicationDate" value="#{pages$tenderInquiryReplies.applicationDate}"></h:inputHidden>
									<h:inputHidden id="hdnRequestId" value="#{pages$tenderInquiryReplies.hdnRequestId}"></h:inputHidden>
									
									
								 
								<div >
										<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
											<tr>
												<td>
													<h:outputText value="#{pages$tenderInquiryReplies.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$tenderInquiryReplies.infoMessages}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
								 </div>
								
										<div class="TAB_DETAIL_SECTION">
											<table cellpadding="1px" width="100%" cellspacing="5px"  styleClass="TAB_DETAIL_SECTION_INNER"  columns="4">
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL" value="#{msg['tenderInquiryAdd.inquiryNumber']}: #{pages$tenderInquiryReplies.asterisk}" />
															</td>
															<td>
																<h:inputText  maxlength="20" id="txtApplicationNum" readonly="true"  value="#{pages$tenderInquiryReplies.applicationNumber}" styleClass="READONLY A_LEFT INPUT_TEXT"></h:inputText>
															</td>

															<td >
																<h:outputLabel styleClass="LABEL" value="#{msg['tenderInquiryAdd.inquiryStatus']}: #{pages$tenderInquiryReplies.asterisk}"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="txtApplicationStatus" styleClass="READONLY A_LEFT INPUT_TEXT" readonly="true" maxlength="20" value="#{pages$tenderInquiryReplies.applicationStatus}" />
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL" value="#{msg['tenderInquiryAdd.inquiryDate']}: #{pages$tenderInquiryReplies.asterisk}"/>
															</td>
															<td >
													          <h:inputText id="txtApplicationDate" styleClass="READONLY A_LEFT INPUT_TEXT"   readonly="true" value="#{pages$tenderInquiryReplies.applicationDate}"/>
												            </td>

															<td >
																<h:outputLabel  styleClass="LABEL"  value="#{msg['application.source']}: #{pages$tenderInquiryReplies.asterisk}"/>
															</td>
															<td>
																<h:inputText id="txtApplicationSource" styleClass="READONLY A_LEFT INPUT_TEXT" readonly="true" maxlength="20" value="#{pages$tenderInquiryReplies.applicationSource}" />
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL" value="#{msg['contractor.name']}: #{pages$tenderInquiryReplies.asterisk}"></h:outputLabel>
															</td>
															<td >
																<h:inputText id="txtContractorName" readonly="true" styleClass="READONLY A_LEFT INPUT_TEXT" value="#{pages$tenderInquiryReplies.contractorName}"></h:inputText>
															</td>

															<td >
																<h:outputLabel id="lblAppType" value="#{msg['tenderInquiryAdd.inquiryContractorNumber']}: #{pages$tenderInquiryReplies.asterisk}"></h:outputLabel>
															</td>
															<td >
																<h:inputText id="txtContractorType" styleClass="READONLY A_LEFT INPUT_TEXT" maxlength="15"  readonly="true" value="#{pages$tenderInquiryReplies.contractorNo}" ></h:inputText>
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL" value="#{msg['tender.number']}: #{pages$tenderInquiryReplies.asterisk}"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="txtTenderNumber" styleClass="READONLY A_LEFT INPUT_TEXT"  maxlength="20"  readonly="true" value="#{pages$tenderInquiryReplies.tenderNumber}" >
																</h:inputText>															
															</td>

															<td >
																<h:outputLabel  styleClass="LABEL"  value="#{msg['tender.description']}: #{pages$tenderInquiryReplies.asterisk}"></h:outputLabel>
															</td>
															<td >
																<h:inputText id="txtTenderDesc" readonly="true" styleClass="READONLY A_LEFT INPUT_TEXT" value="#{pages$tenderInquiryReplies.tenderDes}" rendered="true"></h:inputText>
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"  value="#{msg['tenderInquiryAdd.inquiryDetails']}: #{pages$tenderInquiryReplies.asterisk}"></h:outputLabel>
															</td>
															<td >
																<t:inputTextarea styleClass="TEXTAREA READONLY A_LEFT INPUT_TEXT" readonly="true" rendered="true" value="#{pages$tenderInquiryReplies.reasons}"  rows="6" />
															</td>
														</tr>

														<tr>
															<td colspan="6" class="BUTTON_TD">
																<h:commandButton styleClass="BUTTON" type="submit"
																	value="#{msg['commons.cancel']}"
																	rendered="#{!pages$tenderInquiryReplies.showCancelButton}"
																	action="#{pages$tenderInquiryReplies.cancel}"/>
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	rendered="#{pages$tenderInquiryReplies.showCancelButton}"
																	onclick="closeWindow();"/>	
																<h:commandButton rendered="#{pages$tenderInquiryReplies.hideButton}" type="submit" styleClass="BUTTON" style="width:auto;"
																	value="#{msg['common.replayRequest']}"
																	action="#{pages$tenderInquiryReplies.replied}" />
																<h:commandButton rendered="#{pages$tenderInquiryReplies.hideButton}" type="submit" styleClass="BUTTON" style="width:auto;"
																	value="#{msg['common.cancelRequest']}"
																	action="#{pages$tenderInquiryReplies.cancelRequest}" />
																		
															</td>
														</tr>
													</table>
												</div>
									           
					<div>
						<div class="AUC_DET_PAD">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
						<div class="TAB_PANEL_INNER"> 
							<rich:tabPanel  id="tabPanel" switchType="server" style="HEIGHT: 200px;#HEIGHT: 200px;width: 100%" >
								<rich:tab id="InquiryDetailsTab" label="#{msg['tenderInquiryAdd.inquiryRepliesTabHeading']}">
								   <h:panelGrid  styleClass="BUTTON_TD" width="100%">
																<h:commandButton style="width:auto;" styleClass="BUTTON"
																		value="#{msg['tenderInquiry.btn.addReplies']}"
																		action="#{pages$tenderInquiryReplies.onAddReplies}"
																		rendered="#{pages$tenderInquiryReplies.hideReplyButton}">
																</h:commandButton>																
																 <a4j:commandLink id="lnkReceiveReplies" action="#{pages$tenderInquiryReplies.receiveReplies}"   reRender="dataTable2,gridInfo2" /> 
																															
															</h:panelGrid>
																													
															<t:div styleClass="contentDiv" style="width:99%">																
						                                       <t:dataTable id="dataTable2"  
																	rows="#{pages$tenderInquiryReplies.paginatorRows}"
																	value="#{pages$tenderInquiryReplies.requestRepliesViewList}"
																	binding="#{pages$tenderInquiryReplies.requestRepliesDataTable}"																	
																	preserveDataModel="false" preserveSort="false" var="dataItem"
																	rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
																																		
																	<t:column id="assetNameArCol" sortable="true" style="width:70%" >
																		<f:facet  name="header">
																			<t:outputText  value="#{msg['tenderInquiry.Col.Reply']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.answer}" rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	<t:column id="AssetNameEnCol" sortable="true"  style="width:25%" >
																		<f:facet  name="header">
																			<t:outputText value="#{msg['tenderInquiry.Col.Date']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.createdOn}" rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	<t:column id="actions"  style="width:5%" rendered="#{pages$tenderInquiryReplies.hideReplyButton}">
																		<f:facet  name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		
																	    <t:outputText escape="false" value="&nbsp;" rendered="#{dataItem.isDeleted == 0}"></t:outputText>
																		<t:commandLink   action="#{pages$tenderInquiryReplies.cmdDeleteReplies}"  rendered="#{dataItem.isDeleted == 0}">
																		   <h:graphicImage title="#{msg['commons.delete']}" 
																				                alt="#{msg['commons.delete']}"
																				                 url="../resources/images/delete.gif"
																				                />
																	    </t:commandLink>
																	</t:column>
																  </t:dataTable>										
																</t:div>
														
				                                           		<t:div id="gridInfo2" styleClass="contentDivFooter" style="width:100%">
																			<t:panelGrid id="rqtkRecNumTable2"  columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
																				<t:div id="rqtkRecNumDiv2"  styleClass="RECORD_NUM_BG">
																					<t:panelGrid id="rqtkRecNumTbl2" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
																						<h:outputText value="#{msg['commons.recordsFound']}"/>
																						<h:outputText value=" : "/>
																						<h:outputText value="#{pages$tenderInquiryReplies.repliesRecordSize}"/>																						
																					</t:panelGrid>
																				</t:div>
																				  
																			      <CENTER>
																				   <t:dataScroller id="rqtkscroller2" for="dataTable2" paginator="true"
																					fastStep="1"  paginatorMaxPages="#{pages$tenderInquiryReplies.paginatorMaxPages}" immediate="false"
																					paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true" 
																				    pageIndexVar="pageNumber"
																				    styleClass="SCH_SCROLLER"
																				    paginatorActiveColumnStyle="font-weight:bold;"
																				    paginatorRenderLinkForActive="false" 
																					paginatorTableStyle="grid_paginator" layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">
								
																					    <f:facet  name="first">
																							<t:graphicImage    url="../#{path.scroller_first}"  id="lblFRequestHistory2"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastrewind">
																							<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRRequestHistory2"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastforward">
																							<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFRequestHistory2"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="last">
																							<t:graphicImage  url="../#{path.scroller_last}"  id="lblLRequestHistory2"></t:graphicImage>
																						</f:facet>
									
								                                                      <t:div id="rqtkPageNumDiv2"   styleClass="PAGE_NUM_BG">
																					   <t:panelGrid id="rqtkPageNumTable2"  styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0" >
																					 					<h:outputText  styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																										<h:outputText   styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																						</t:panelGrid>
																							
																						</t:div>
																				 </t:dataScroller>
																				 </CENTER>
																		      
																	        </t:panelGrid>
								                          			 </t:div>
								                          			  <h:panelGrid  styleClass="BUTTON_TD" width="100%">
																     <h:commandButton rendered="#{pages$tenderInquiryReplies.hideReplyButton}" type="submit" styleClass="BUTTON" 
																	  value="#{msg['commons.saveButton']}"
																	  action="#{pages$tenderInquiryReplies.btnSave_Click}" /> 																
															         </h:panelGrid>
								</rich:tab>
							    <rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}">
									<%@  include file="attachment/attachment.jsp"%>
								</rich:tab>
								<rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}">
									<%@ include file="notes/notes.jsp"%>
								</rich:tab>
							</rich:tabPanel>
							</div>
										<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT" /></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID" style="width:100%;" /></td>
										<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT" /></td>
										</tr>
										</table>
						           </div>
								</div>
							</h:form>
					      </div> 
						</td>			
					</tr>
				</table>
			</td>
		</tr>
		<c:choose>
	         <c:when test="${pages$tenderInquiryReplies.hideHeader}">
		         <tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</c:when>
		</c:choose>		
	  </table>
	</div>
</body>
</html>
</f:view>