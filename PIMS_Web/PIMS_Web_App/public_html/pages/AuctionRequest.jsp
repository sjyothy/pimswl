


<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="javascript" type="text/javascript">
	function showPopupBidderSearch()
	{
	   var screen_width = screen.width;
       var screen_height = screen.height;
       var popup_width = screen_width-350;
       var popup_height = screen_height-220;
       var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
	   window.open('BidderLookup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');
	   //window.open('BidderLookup.jsf','_blank','width='+(screen_width-350)+',height='+(screen_height-180)+',left='+screen_width-((screen_width/3)*2)+',top=40,scrollbars=yes,status=yes');		
	}
	function showPopupAuctionSearch()
	{
	   var screen_width = screen.width;
       var screen_height = screen.height;
       var popup_width = screen_width-10;
       var popup_height = screen_height-260;
       var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
       window.open('RequestAuctionSearch.jsf','_blank','width='+popup_width+',height='+popup_height+',left=0,top=40,scrollbars=yes,status=yes');
	   //var screen_width = screen.width;
	   //var screen_height = screen.height;
	   //window.open('RequestAuctionSearch.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-180)+',left=0,top=40,scrollbars=yes,status=yes');
	}
	
	function checkChanged()
	{
		alert('check changed!');	
	}
	
	var totalDepositAmount=0;
	var aucUnitNo;
	var amountToReceive = 0;
	function AddUnits(field,unitId,auctionUnitNo,depositAmount)
	{
	  totalDepositAmount = parseFloat(document.getElementById("auctionRequestForm:totalDeposit").value);
	  aucUnitNo = document.getElementById("auctionRequestForm:auctionUnitsOfRequest").value;
	  document.getElementById("auctionRequestForm:opMsgs").value = "";
	  if(field.checked)
	  {
		    if(aucUnitNo!=null && aucUnitNo.length>0 )
		    {					
		       aucUnitNo=aucUnitNo+","+auctionUnitNo + ":" + unitId;		       
		    }
			else
			{
				aucUnitNo=auctionUnitNo + ":" + unitId; 
			}
			totalDepositAmount = parseFloat(totalDepositAmount) + parseFloat(depositAmount);
   	   }
   	   else
   	   {
	   	   var delimiters=",";
		   var unitArr=aucUnitNo.split(delimiters);
		   aucUnitNo="";
		   for(i=0;i<unitArr.length;i++)
		   {
			   if(unitArr[i]!=auctionUnitNo + ":" + unitId)
			   {
		       	   if(aucUnitNo.length>0 )
			       {
			             aucUnitNo=aucUnitNo+","+unitArr[i];
		
	               }
			       else
			       {
			             aucUnitNo=unitArr[i];
		           }
			   }		    
		   }
		   totalDepositAmount = parseFloat(totalDepositAmount) - parseFloat(depositAmount); 
   	   }
   	   document.getElementById("auctionRequestForm:inputTextTotalDepositAmount").value = totalDepositAmount.toFixed(2);
   	   document.getElementById("auctionRequestForm:totalDeposit").value = totalDepositAmount;
   	   document.getElementById("auctionRequestForm:auctionUnitsOfRequest").value = aucUnitNo;   	   
	}
	
	function showPaymentPopup()
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
		window.open('receivePayment.jsf?parentHidden=auctionRequestForm:isPayPopupClosed','_blank','width='+(screen_width-10)+',height='+(screen_height-270)+',left=0,top=40,scrollbars=yes,status=yes');		
	}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
		     <title>PIMS</title>
			 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			 <meta http-equiv="pragma" content="no-cache">
			 <meta http-equiv="cache-control" content="no-cache">
			 <meta http-equiv="expires" content="0">
			 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
	</head>
	
	<body>
	

	<!-- Header -->
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="2">
				<jsp:include page="header.jsp" />
			</td>
		</tr>

		<tr width="100%">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99%" class="greyPanelTable" cellpadding="0"
					cellspacing="0" border="0">
					<tr>
				        <c:choose>
							<c:when test="${sessionScope.CurrentLocale.languageCode eq 'en'}">
								 <td background ="../resources/images/Grey panel/Grey-Panel-left-1.jpg" height="38" style="background-repeat:no-repeat;" width="100%">
					             <font style="font-family:Trebuchet MS;font-weight:regular;font-size:19px;margin-left:15px;top:30px;">
					             <h:outputLabel value="#{msg['auction.registerAuction']}"></h:outputLabel>
					             </font>
				                <!--<IMG src="../resources/images/Grey panel/Grey-Panel-left-1.jpg"/>-->
							</c:when>
							<c:otherwise>
								<td width="100%">
				                 <IMG src="../resources/images/ar/Detail/Grey Panel/Grey-Panel-right-1.jpg"></img>
							</c:otherwise>
						</c:choose>
				           </td>   
							<td width="100%">
								&nbsp;
							</td>
						</tr>
					</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>

									<td height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:460px;">
										<h:form id="auctionRequestForm">
											<table width="100%">
												<tr>
													<td colspan="6">
														<h:messages id="Msgs">
														<h:outputText id="opMsgs" value="#{pages$AuctionRequest.messages}" escape="false" styleClass="INFO_FONT"/>
														</h:messages>																												
													</td>
												</tr>
												<tr>
													<td colspan="6">
													<div class="MARGIN">
													
													<table cellpadding="0" cellspacing="0" style="width:98%">
														<tr>
														<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
														<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
														</tr>
													</table>
													<div class="DETAIL_SECTION" style="width:98%">
														<h:outputLabel value="#{msg['bidder.details']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
														
															<tr>
																<td width="25%">
																	<h:outputLabel value="#{msg['bidder.name']}"></h:outputLabel>
																</td>
																<td colspan="3">
																	<h:inputHidden id="bidderID" value="#{pages$AuctionRequest.hiddenBidderId}"></h:inputHidden>
																	<h:inputHidden id="bidderName" value="#{pages$AuctionRequest.hiddenBidderName}"></h:inputHidden>
																	<h:inputHidden id="auctionID" value="#{pages$AuctionRequest.auctionId}"></h:inputHidden>
																	<h:inputHidden id="aNo" value="#{pages$AuctionRequest.auctionNo}"></h:inputHidden>
																	<h:inputHidden id="aDate" value="#{pages$AuctionRequest.auctionDate}"></h:inputHidden>
																	<h:inputHidden id="aTitle" value="#{pages$AuctionRequest.auctionTitle}"></h:inputHidden>
																	<h:inputHidden id="aVenue" value="#{pages$AuctionRequest.auctionVenueName}"></h:inputHidden>
																	<h:inputHidden id="bidderSocialSec" value="#{pages$AuctionRequest.hiddenSocialSecNo}"></h:inputHidden>
																	<h:inputHidden id="bidderPassport" value="#{pages$AuctionRequest.hiddenPassportNo}"></h:inputHidden>
																	<h:inputHidden id="isPayPopupClosed" value="#{pages$AuctionRequest.isPaymentPopupClosed}"></h:inputHidden>
																	<h:inputHidden id="reqReprinted" value="#{pages$AuctionRequest.requestReprinted}"></h:inputHidden>
																	<h:inputHidden id="totalDeposit" value="#{pages$AuctionRequest.hiddenTotalDeposit}"></h:inputHidden> 
 																	<h:inputHidden id="auctionUnitsOfRequest" value="#{pages$AuctionRequest.hiddenAuctionUnitsOfRequest}"></h:inputHidden> 																	
																	<h:inputText id="bidderNameInputText" style="width: 200px; height: 16px" readonly="true" 
																	value="#{pages$AuctionRequest.hiddenBidderName}"></h:inputText>

																	<%--<h:commandButton id="searchCommandButtonBidder" 
																	styleClass="BUTTON" 
																	type="button" 
																	value="#{msg['commons.search']}" 
																	onclick="javascript:showPopupBidderSearch();"
																	rendered="#{pages$AuctionRequest.showSearchBtn}"></h:commandButton>--%>
																	<t:commandLink
																		rendered="#{pages$AuctionRequest.showSearchBtn}"
																		onclick="javascript:showPopupBidderSearch();">
																		&nbsp;
																		<h:graphicImage id="searchCommandButtonBidder" 
																		title="#{msg['commons.search']}" 
																		url="../resources/images/magnifier.gif"/>
																	</t:commandLink>
																</td>																										
															</tr>
															<tr>
																<td width="25%">
																	<h:outputLabel value="#{msg['tenants.socialSecNo']}"></h:outputLabel>
																</td>
																<td>	
																	<h:inputText id="socialSecNo" style="width: 200px; height: 16px" readonly="true" 
																	value="#{pages$AuctionRequest.hiddenSocialSecNo}"></h:inputText>
																</td>
																<td>
																	<h:outputLabel value="#{msg['commons.passport.number']}"></h:outputLabel>
																</td>
																<td>	
																	<h:inputText id="passport" style="width: 200px; height: 16px" readonly="true" 
																	value="#{pages$AuctionRequest.hiddenPassportNo}"></h:inputText>
																</td>
																
															</tr>
															</table>
															</div>
															</div>
													<div class="MARGIN">
													
													<table cellpadding="0" cellspacing="0" style="width:98%">
														<tr>
														<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
														<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
														</tr>
													</table>
													<div class="DETAIL_SECTION" style="width:98%">
														<h:outputLabel value="#{msg['auction.details']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
														
															<tr>
																<td width="25%">
																	<h:outputLabel value="#{msg['auction.number']}"></h:outputLabel>
																</td>
																<td colspan="3">
																	<h:inputText id="auctionNo" style="width: 200px; height: 16px" readonly="true" value="#{pages$AuctionRequest.auctionNo}"></h:inputText>
																	
																	<%--<h:commandButton id="searchCommandButtonAuction"  
																	styleClass="BUTTON" 
																	type="button" 
																	value="#{msg['commons.search']}" 
																	onclick="javascript:showPopupAuctionSearch();"
																	rendered="#{pages$AuctionRequest.showSearchBtn}"></h:commandButton>--%>
																	<t:commandLink
																		rendered="#{pages$AuctionRequest.showSearchBtn}"
																		onclick="javascript:showPopupAuctionSearch();">
																		&nbsp;
																		<h:graphicImage id="searchCommandButtonAuction" 
																		title="#{msg['commons.search']}" 
																		url="../resources/images/magnifier.gif"/>
																	</t:commandLink>
																</td>													
															</tr>																														
															<tr>
																<td width="25%">
																	<h:outputLabel value="#{msg['auction.venue']}"></h:outputLabel>
																</td>
																<td>
																	<h:inputText id="auctionVenue" style="width: 200px; height: 16px" readonly="true" value="#{pages$AuctionRequest.auctionVenueName}"></h:inputText>
																</td>
																<td colspan="2">
																	&nbsp;
																</td>
															</tr>
															<tr>
																<td width="25%">
																	<h:outputLabel value="#{msg['auction.title']}"></h:outputLabel>
																</td>
																<td>	
																	<h:inputText id="auctionTitle" style="width: 200px; height: 16px" readonly="true" value="#{pages$AuctionRequest.auctionTitle}"></h:inputText>
																</td>
																<td>
																	<h:outputLabel value="#{msg['auction.date']}"></h:outputLabel>
																</td>
																<td>	
																	<h:inputText id="auctionDate" style="width: 200px; height: 16px" readonly="true" value="#{pages$AuctionRequest.auctionDate}"></h:inputText>
																</td>
															</tr>
														</table>
														</div></div>
													</td>
												</tr>
												
											</table>	 											 
											<br>
											<div class="MARGIN">
											<div class="imag">&nbsp;</div>
											<div class="contentDiv" style="width:96%">												
												<t:dataTable id="auctionUnitsDataTable" styleClass="grid"
													value="#{pages$AuctionRequest.auctionDataList}"
													binding="#{pages$AuctionRequest.dataTable}"
													rows="3"
													preserveDataModel="true" preserveSort="true" var="dataItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
						
													<t:column id="colPropertyRefNo">
														<f:facet name="header">
															<t:outputText value="#{msg['property.refNumber']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.propertyRefNumber}" />
													</t:column>
						
													<t:column id="colPropertyCommName">
														<f:facet name="header">
															<t:outputText value="#{msg['property.commercialName']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.commercialName}" />
													</t:column>
						
													<t:column id="colUnitNo" >
														<f:facet name="header">
															<t:outputText value="#{msg['unit.number']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.unitNumber}" />
													</t:column>
						
													<c:if test="${sessionScope.CurrentLocale.dir == \"ltr\"}">
													<t:column id="colUnitTypeEn">
														<f:facet name="header">
															<t:outputText value="#{msg['unit.type']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.unitTypeEn}" />
													</t:column>
													</c:if>
													
													<c:if test="${sessionScope.CurrentLocale.dir == \"rtl\"}">
													<t:column id="colUnitTypeAr">
														<f:facet name="header">
															<t:outputText value="#{msg['unit.type']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.unitTypeAr}" />
													</t:column>
													</c:if>
													
													<t:column id="colDepositAmount">
														<f:facet name="header">
															<t:outputText value="#{msg['unit.depositAmount']}" />
														</f:facet>
														<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.depositAmount}" />
													</t:column>
													
													<t:column id="colSelect" rendered="#{pages$AuctionRequest.hideSelectBooleanCheckBox}">
														<f:facet name="header">
															<t:outputText   value="#{msg['commons.select']}"/>
														</f:facet>
														<h:selectBooleanCheckbox  style="text-align:center" value="#{dataItem.isSelected}" 
														onclick="javascript:AddUnits(this,'#{dataItem.unitId}','#{dataItem.auctionUnitId}','#{dataItem.depositAmount}');"/>
													</t:column>
												</t:dataTable>
											</div>
											<div class="contentDivFooter" style="width:98%;">
												<t:dataScroller id="regScroller" for="auctionUnitsDataTable" paginator="true"
													fastStep="1" paginatorMaxPages="2" immediate="false"
													styleClass="scroller" paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 												
													paginatorTableStyle="grid_paginator" layout="singleTable" 
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">
				
													<f:facet name="first">
														
														<t:graphicImage url="../resources/images/first_btn.gif" id="AttendancelblF"></t:graphicImage>
													</f:facet>
				
													<f:facet name="fastrewind">
														<t:graphicImage url="../resources/images/previous_btn.gif" id="AttendancelblFR"></t:graphicImage>
													</f:facet>
				
													<f:facet name="fastforward">
														<t:graphicImage url="../resources/images/next_btn.gif" id="AttendancelblFF"></t:graphicImage>
													</f:facet>
				
													<f:facet name="last">
														<t:graphicImage url="../resources/images/last_btn.gif" id="AttendancelblL"></t:graphicImage>
													</f:facet>
				
												</t:dataScroller>
											</div>
 											</div>												
												<br>																								
												<div class="MARGIN">
												<table width="97%">
												<tr>
												<td>
													<h:outputLabel style="font-weight: bold;" value="#{msg['auction.totalDepositAmount']}"></h:outputLabel>
														&nbsp;
														<h:inputText id="inputTextTotalDepositAmount" 
														readonly="true" 
														style="text-align:right"
														value="#{pages$AuctionRequest.hiddenTotalDeposit}"></h:inputText>
												</td>
												<td class="BUTTON_TD">
													
														<h:commandButton styleClass="BUTTON" value="#{msg['auction.reprintReceipt']}"
														id="buttonReprintReceipt"
														style="width: 100px"
														rendered="#{pages$AuctionRequest.allowRePrint}"
														action="#{pages$AuctionRequest.rePrintReceipt}">
														</h:commandButton>
															
														<h:commandButton styleClass="BUTTON" value="#{msg['auction.pay']}"
														id="buttonPay"
														rendered="#{pages$AuctionRequest.allowPay}"
														actionListener="#{pages$AuctionRequest.requestPayment}">
														</h:commandButton>
														
														<h:inputHidden id="IsRequestSaved" value="#{pages$AuctionRequest.requestSaved}"></h:inputHidden>
														<h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" 
														id="buttonSave" 
														type="submit" 
														action="#{pages$AuctionRequest.save}"
														rendered="#{pages$AuctionRequest.allowSave}">
														</h:commandButton>
														
														<h:commandButton styleClass="BUTTON" value="#{msg['request.newRequest']}" 
														id="buttonNew" 
														type="submit"
														style="width:auto;"
														action="#{pages$AuctionRequest.newRequest}">
														</h:commandButton>														
												</td>
												</tr>	
												</table>
												</div>
											
										</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
				    <td colspan="2">
				    <table width="100%" cellpadding="0" cellspacing="0" border="0">
				       <tr><td class="footer"><h:outputLabel value="#{msg['commons.footer.message']}" /></td></tr>
				    </table>
				    </td>
				    </tr>
				</table>	
			</body>
		</f:view>
</html>
