
<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
	<t:dataTable id="dataTableProgramHistory" rows="15" width="100%"
		value="#{pages$donationBox.listProgramHistory}"
		preserveDataModel="false" preserveSort="false" var="dataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

		<t:column id="endProgramC" sortable="true">
			<f:facet name="header">
				<t:outputText id="endProgOT"
					value="#{msg['endowmentProgram.lbl.name']}" />
			</f:facet>
			<t:commandLink
				onclick="javaScript:openManageEndowmentPrograms('#{dataItem.program.endowmentProgramId}');"
				value="#{dataItem.program.progName}"
				style="white-space: normal;" styleClass="A_LEFT" />

		</t:column>
		<t:column id="replacedOn" sortable="true">
			<f:facet name="header">
				<t:outputText id="changedOnOT" value="#{msg['commons.changedOn']}" >
				<f:convertDateTime timeZone="#{pages$donationBox.timeZone}" pattern="#{pages$donationBox.dateFormat}" />
				</t:outputText>
			</f:facet>
			<t:outputText id="payNoField" styleClass="A_LEFT"
				value="#{dataItem.createdOn}" />
		</t:column>
		<t:column id="replacedBy" sortable="true">
			<f:facet name="header">
				<t:outputText id="changedByOT" value="#{msg['commons.changedBy']}" />
			</f:facet>
			<t:outputText id="createdByNamePH" styleClass="A_LEFT"
				value="#{dataItem.createdByName}" />
		</t:column>


	</t:dataTable>
</t:div>


