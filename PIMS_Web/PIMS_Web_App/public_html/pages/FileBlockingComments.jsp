<%-- 
  - Author: Muhammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Adding Blocking Comments
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	function sendNotesToParent()
	{
		window.opener.obtainFileBlockingNotes();
		window.close();
				
	}		
		function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	function closeWindow()
		{
		  window.close();
		}
	function validate() {
      maxlength=500;
       if(document.getElementById("searchFrm:txtAreaComments").value.length>=maxlength) 
       {
          document.getElementById("searchFrm:txtAreaComments").value=
          document.getElementById("searchFrm:txtAreaComments").value.substr(0,maxlength);  
      }
    }
        
      
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			


			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


			


				<tr width="100%">

				

					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['attachment.comments']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>
									<div>
										<h:outputText value="#{pages$FileBlockingComments.errorMessages}"
											escape="false" styleClass="ERROR_FONT" />
									</div>
									<h:form id="searchFrm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION">
											<div class="MARGIN" style="width: 95%;">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
														<%--<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT" /></td>																																																										
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
														<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT" /></td>--%>	
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width: 99.8%;">
													<%--<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>--%>
													<table cellpadding="1px" cellspacing="2px" border="0"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">

																	<tr>

																		<td width="auto" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['attachment.comments']} : "></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:inputTextarea id="txtAreaComments"
																				binding="#{pages$FileBlockingComments.txtAreaComments}"
																				style="width:450px;height:100px;"
																				onblur="javaScript:validate();"
																				onkeyup="javaScript:validate();"
																				onmouseup="javaScript:validate();"
																				onmousedown="javaScript:validate();"
																				onchange="javaScript:validate();"
																				onkeypress="javaScript:validate();"
																				
																				tabindex="3">						



																			</h:inputTextarea>
																		</td>
																		
																		

																	</tr>
																	<tr>
																		
																		
																		
																		
																		<%--<f:selectItems
																					value="#{pages$paymentDetailsPopUp.disbursementSourceList}" /> --%>


																	</tr>
																	<tr>
																		
																		
																		
																		
																		
																		
																	</tr>

																	<tr>
																		<td class="BUTTON_TD" colspan="4">

																			<h:commandButton styleClass="BUTTON" type="submit"
																				value="#{msg['commons.done']}"
																				action="#{pages$FileBlockingComments.sendNotesToParent}" />
																				<h:commandButton styleClass="BUTTON" type="submit" value="#{msg['bouncedChequesList.close']}"
																				 onclick="window.close()"/>
																				

																		</td>

																	</tr>
																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>

													</table>
												</div>
											</div>
											
											<%--<table width="96.6%" border="0">
												<tr>
													<td class="BUTTON_TD JUG_BUTTON_TD_US">

														<h:commandButton type="submit"
															rendered="#{pages$paymentDetailsPopUp.isPageModeSelectManyPopUp}"
															styleClass="BUTTON"
															actionListener="#{pages$paymentDetailsPopUp.sendManyAssetsInfoToParent}"
															value="#{msg['commons.select']}" />
													</td>

												</tr>


											</table>--%>
										</div>


										</div>
									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>
			
			</table>
			

		</body>
	</html>
</f:view>