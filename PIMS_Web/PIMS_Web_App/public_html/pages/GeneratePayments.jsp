<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



    
<f:view>
        <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
       <f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
 
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>						
            <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
			


			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			
 	</head>
 	        
		<script language="javascript" type="text/javascript">
	function clearWindow()
	{
            //document.getElementById("formPaymentSchedule:dateTimePaymentDueOn").value="";
			//document.getElementById("formPaymentSchedule:txtAmount").value="";
			//var cmbPaymentMode=document.getElementById("formPaymentSchedule:selectPaymentMode");
			
			//cmbPaymentMode.selectedIndex = 0;
			//  $('formPaymentSchedule:dateTimePaymentDueOn').component.resetSelectedDate();
	        
	}
	function addToContract()

      {

      

        window.opener.document.forms[0].submit();

        

        window.close();

      }
	
    </script>
 	

				<!-- Header -->
	
		<body class="BODY_STYLE">
	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<% System.out.println("- ONE"); %>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr><td class="HEADER_TD">
										<h:outputLabel value="#{msg['contract.paymentSchedule.GeneratePayments']}" styleClass="HEADER_FONT"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
					        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
                                 <tr valign="top">
                                  <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
                                   </td>    
									<td width="100%" height="85%" valign="top" nowrap="nowrap">
										<h:form id="formPaymentSchedule" style="width:100%"  enctype="multipart/form-data">
									     <div  class="SCROLLABLE_SECTION"  style="width:100%"  >
											<table border="0" class="layoutTable" >
											
												<tr>
												
													<td colspan="1">
												
														<h:outputText  styleClass="ERROR_FONT" escape="false" value="#{pages$GeneratePayments.errorMessages}"/>
												
													</td>
												</tr>
											</table>

										<div class="MARGIN" style="width:100%;" >
											<table cellpadding="0" cellspacing="0" width="95.3%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
											</table>
										<div class="DETAIL_SECTION" style="width:95%" >
										<h:outputLabel value="#{msg['paymentSchedule.paymentDetails']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="1px" class="DETAIL_SECTION_INNER" width="100%">
													
                                                        <tr>
															<td width="25%"><h:inputHidden value="#{pages$GeneratePayments.amount}"></h:inputHidden></td>
															<td width="25%"><h:inputHidden id="hdnDateTimeDueOn" value="#{pages$GeneratePayments.today}"></h:inputHidden></td>
														</tr>
														<tr>
															<td width="25%"><h:outputLabel value="#{msg['contract.TotalContractValue']}">:</h:outputLabel></td>
															<td width="25%"><h:inputText disabled="true" style="width:85%" styleClass="A_RIGHT_NUM READONLY" readonly="true" maxlength="15" id="txtTotalContractValue" value="#{pages$GeneratePayments.amount}">
															        <f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
																</h:inputText>
															</td>
	                                                        <td ><h:outputLabel value="#{msg['generatePayments.startDate']}">:</h:outputLabel></td>
															<td><rich:calendar id="dateTimePaymentDueOn" value="#{pages$GeneratePayments.startDate}" popup="true" showApplyButton="false" datePattern="#{pages$GeneratePayments.dateformat}" enableManualInput="false" cellWidth="24px" inputStyle="width: 85%" /></td>
														</tr>
														<tr>
														   <td width="25%"> <h:outputLabel styleClass="LABEL" value="#{msg['contract.date.Start']}">:</h:outputLabel></td>
														   <td width="25%"><h:inputText disabled="true" style="width:85%" styleClass="READONLY" readonly="true" maxlength="15" id="contractStartDate" value="#{pages$GeneratePayments.contractStartDate}" >
														   <f:convertDateTime pattern="#{pages$GeneratePayments.dateformat}" timeZone="#{pages$GeneratePayments.timeZone}"/>  </h:inputText> 
														   </td>
														   <td width="25%"><h:outputLabel value="#{msg['contract.date.expiry']}">:</h:outputLabel></td>
														   <td width="25%"><h:inputText disabled="true" style="width:85%" styleClass="READONLY" readonly="true" maxlength="15" id="contractEndDate" value="#{pages$GeneratePayments.contractEndDate}" >
														   <f:convertDateTime pattern="#{pages$GeneratePayments.dateformat}" timeZone="#{pages$GeneratePayments.timeZone}"/></h:inputText>
														   </td>
														</tr>
														<tr>
															<td><h:outputLabel value="#{msg['generatePayments.numberOfCash']}">:</h:outputLabel></td>
															<td><h:inputText style="width: 85%" styleClass="A_LEFT_NUM" maxlength="15" id="noOfCash" value="#{pages$GeneratePayments.numberOfCashPayments}"/></td>
															<td><h:outputLabel value="#{msg['renewContractNumberOfCheques']}">:</h:outputLabel></td>
															<td><h:inputText  style="width: 85%" styleClass="A_LEFT_NUM" maxlength="15" id="noOfChq" value="#{pages$GeneratePayments.numberOfChqPayemts}"/></td>
														</tr>

														
														<tr>
															
															
															<td colspan="4" class="BUTTON_TD">
															 <h:commandButton type="submit" styleClass="BUTTON" action="#{pages$GeneratePayments.generatePayments}" value="#{msg['contract.paymentSchedule.GeneratePayments']}" style="width: 151px"/>
															 <a4j:commandButton id ="btnReset" type="submit" reRender="noOfChq,noOfCash,dateTimePaymentDueOn,test3,btnSendToParent" styleClass="BUTTON" value="#{msg['commons.reset']}" style="width: 151px" 
															    action="#{pages$GeneratePayments.btnReset_Click}" />
															  <h:commandButton id ="btnSendToParent1" style="width: 151px" styleClass="BUTTON" value="#{msg['paidFacilities.sendToContract']}"  rendered="#{pages$GeneratePayments.isAddToContractBtnShow}" action="#{pages$GeneratePayments.sendToContract}"/>
															
															</td>
														</tr>
													</table>


												</div>
                                          </div>
                                          <br/>
                                          <div id="paddingDiv"  style="margin:10px">
								         <table id="imageTable" cellpadding="0" cellspacing="0" width="97.1%" style ="margin-right:4px;">
													<tr>
													<td><IMG id="image1" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_left")%>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG id="image2" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_mid")%>" class="TAB_PANEL_MID"/></td>
													<td><IMG id="image3" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_right")%>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>
						                 <t:div id="dataListDiv" styleClass="contentDiv"  style="width:96.1%"  >
												<t:dataTable id="test3"
													rows="12" width="100%"
													value="#{pages$GeneratePayments.paymentSchedules}"
													binding="#{pages$GeneratePayments.dataTable}" 
													preserveDataModel="true" preserveSort="true" var="dataItem" 
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												>
												<t:column id="colDueOn">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentDueOn']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.paymentDueOn}" >
														 <f:convertDateTime pattern="#{pages$GeneratePayments.dateformat}" 
													timeZone="#{pages$GeneratePayments.timeZone}"/>
													</t:outputText>
												</t:column>
												<t:column id="colTypeEn" rendered="#{pages$GeneratePayments.isEnglishLocale}">


													<f:facet name="header">

														<t:outputText  value="#{msg['paymentSchedule.paymentType']}" />

													</f:facet>
													<t:outputText     styleClass="A_LEFT"  title="" value="#{dataItem.typeEn}" />
												</t:column>
												<t:column id="colTypeAr" rendered="#{pages$GeneratePayments.isArabicLocale}">


													<f:facet name="header">

														<t:outputText  value="#{msg['paymentSchedule.paymentType']}" />

													</f:facet>
													<t:outputText     styleClass="A_LEFT"  title="" value="#{dataItem.typeAr}" />
												</t:column>
												<t:column id="colModeEn" rendered="#{pages$GeneratePayments.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentMode']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.paymentModeEn}" />
												</t:column>
												<t:column id="colModeAr" rendered="#{pages$GeneratePayments.isArabicLocale}">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentMode']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.paymentModeAr}" />
												</t:column>
												<t:column id="colAmount" >
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.amount']}" />
													</f:facet>
													<t:outputText styleClass="A_RIGHT_NUM" title="" value="#{dataItem.amount}" >
													<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
													</t:outputText>
												</t:column>
													 
											  </t:dataTable>										
											</t:div>
											  <t:div id="pagingDivPaySch" styleClass="contentDivFooter" style="width:97.1%;" >

												<t:dataScroller id="scrollerPaySch" for="test3" paginator="true"
													fastStep="1" paginatorMaxPages="15" immediate="false" 
													styleClass="scroller" paginatorTableClass="paginator" 
													renderFacetsIfSinglePage="true" paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">
                                                	<f:facet  name="first">
														
														<t:graphicImage  url="../#{path.scroller_first}" id="lblFPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="fastrewind">
														<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="fastforward">
														<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="last">
														<t:graphicImage   url="../#{path.scroller_last}"  id="lblLPaySch"></t:graphicImage>
													</f:facet>

												</t:dataScroller>
									
                                           </t:div>
						                 
						                  </div> 
						                   <table width="96.0%" >
											<tr>
											    <td colspan="6" class="BUTTON_TD"> 
											      <h:commandButton id ="btnSendToParent" style="width: 151px" styleClass="BUTTON" value="#{msg['paidFacilities.sendToContract']}"  rendered="#{pages$GeneratePayments.isAddToContractBtnShow}" action="#{pages$GeneratePayments.sendToContract}"/>
											</td>
											</tr>
											</table> 
                   </div>
			    </h:form>
			 
									</td>
									</tr>
									</table>
			
			</td>
    </tr>
    
    </table>
			
		</body>
	</html>
</f:view>

