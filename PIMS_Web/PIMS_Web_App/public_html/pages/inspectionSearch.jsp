<%-- 
  - Author: Munir Chagani
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Inspections
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	 function resetValues()
      	       {
      	       
      	        document.getElementById("searchFrm:txtInspectionNumber").value="";
      	        document.getElementById("searchFrm:cmbInspectionStatus").selectedIndex = 0;
      	        $('searchFrm:inspectionFrom').component.resetSelectedDate();
      	        $('searchFrm:inspectionTo').component.resetSelectedDate();
      	        document.getElementById("searchFrm:cmbInspectionType").selectedIndex = 0;
      	        document.getElementById("searchFrm:txtInspectorName").value="";
      	        document.getElementById("searchFrm:txtPropertyName").value="";
      	        document.getElementById("searchFrm:cmbPropertyType").selectedIndex = 0;
      	        document.getElementById("searchFrm:txtContractNumber").value="";
      	        document.getElementById("searchFrm:cmbContractType").selectedIndex = 0;
      	        document.getElementById("searchFrm:txtTenantName").value="";
      	        document.getElementById("searchFrm:cmbTenantType").selectedIndex = 0;
      	        document.getElementById("searchFrm:txtUnitNumber").value="";      	        
      	        
               }
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

</head>
	<body class="BODY_STYLE">
	<div class="containerDiv">
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


			<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>


				<tr width="100%">

					<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
					</td>

					<td width="83%" height="470px" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['inspectionSearch.header']}" styleClass="HEADER_FONT"/>

								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
								<div class="SCROLLABLE_SECTION AUC_SCH_SS"
										style="height: 70%; width: 100%; # height: 440px; # width: 100%;">
								
									<h:form id="searchFrm" style="WIDTH: 96%;height:450px;">
											<div class="MESSAGE">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$inspectionSearch.infoMessages}"
																escape="false" styleClass="INFO_FONT" />
															<h:outputText
																value="#{pages$inspectionSearch.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
												<div class="DETAIL_SECTION" style="width:99.6%;">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER" >
														<tr><td width="97%">
														<table width="100%">
														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['inspection.visitNumber']}: "></h:outputLabel>
															</td>
															<td width="25%">														
																<h:inputText id="txtInspectionNumber" 
																binding="#{pages$inspectionSearch.htmlInspectionNumber}"
																	tabindex="1">
																</h:inputText>
															</td>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['inspection.visitStatus']}: "></h:outputLabel>
															</td>
															<td width="25%">															
																<h:selectOneMenu id="cmbInspectionStatus"  tabindex="2"
																binding="#{pages$inspectionSearch.htmlSelectOneInspectionStatus}"
																	>
																	<f:selectItem itemLabel="#{msg['commons.All']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.inspectionStatus}" />
																</h:selectOneMenu>
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['inspection.date.from']}">:</h:outputLabel>
															</td>
															<td >															
																<rich:calendar id="inspectionFrom" popup="true"
																  binding="#{pages$inspectionSearch.htmlCalendarInspectionFrom}"
																	datePattern="#{pages$inspectionSearch.dateFormat}"
																	showApplyButton="false"
																	locale="#{pages$inspectionSearch.locale}"
																	enableManualInput="false" inputStyle="width:170px; height:14px;"/>
															</td>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.to']}">:</h:outputLabel>
															</td>
															<td >															
																<rich:calendar id="inspectionTo" popup="true" 
																   binding="#{pages$inspectionSearch.htmlCalendarInspectionTo}"
																	datePattern="#{pages$inspectionSearch.dateFormat}"
																	showApplyButton="false"
																	locale="#{pages$inspectionSearch.locale}"
																	enableManualInput="false" inputStyle="width:170px; height:14px;" />
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['inspection.type']}:"></h:outputLabel>
															</td>
															<td >
															
																<h:selectOneMenu id="cmbInspectionType" tabindex="5"
																   binding="#{pages$inspectionSearch.htmlSelectOneInspectionType}"
																	>
																	<f:selectItem itemLabel="#{msg['commons.All']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.inspectionType}" />
																</h:selectOneMenu>
															</td>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['inspector.name']}:"></h:outputLabel>
															</td>
															<td >
															
																<h:inputText id="txtInspectorName" tabindex="6"
																    binding="#{pages$inspectionSearch.htmlInspectorName}"
																	 >
																</h:inputText>
															</td>
														</tr>


														<tr>
															<td >
															
																<h:outputLabel styleClass="LABEL" 
																value="#{msg['contract.property.Name']}:"></h:outputLabel>
															</td>
															<td >
																<h:inputText id="txtPropertyName" tabindex="7"
																binding="#{pages$inspectionSearch.htmlPropertyName}"
																	 >
																</h:inputText>
															</td>
															<td >
															<h:outputLabel styleClass="LABEL" 
																value="#{msg['property.type']}:"></h:outputLabel>
															</td>
															<td >
																<h:selectOneMenu id="cmbPropertyType" tabindex="8"
																binding="#{pages$inspectionSearch.htmlSelectOnePropertyType}"
																	>
																	<f:selectItem itemLabel="#{msg['commons.All']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.propertyTypeList}" />
																</h:selectOneMenu>
															</td>
														</tr>

														<tr>
															<td >
																<h:outputLabel styleClass="LABEL" 
																value="#{msg['commons.Contract.Number']}:"></h:outputLabel>
															</td>
															<td >
																<h:inputText id="txtContractNumber" tabindex="9"
																binding="#{pages$inspectionSearch.htmlContractNumber}"
																	 >
																</h:inputText>
															</td>
															<td >
																<h:outputLabel styleClass="LABEL" value="#{msg['contract.contractType']}:"></h:outputLabel>
															</td>
															<td >
																<h:selectOneMenu id="cmbContractType" tabindex="10"
																binding="#{pages$inspectionSearch.htmlSelectOneContractType}"
																	>
																	<f:selectItem itemLabel="#{msg['commons.All']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.contractType}" />
																</h:selectOneMenu>
															</td>
														</tr>

														<tr>
															<td >
																<h:outputLabel styleClass="LABEL" value="#{msg['contract.tenantName']}:"></h:outputLabel>
															</td>
															<td >
																<h:inputText id="txtTenantName"  tabindex="11"
																binding="#{pages$inspectionSearch.htmlTenantName}"
																	>
																</h:inputText>
															</td>
															<td >
															<h:outputLabel styleClass="LABEL" value="#{msg['tenants.tenantsType']}:"></h:outputLabel>
															</td>
															<td >
																<h:selectOneMenu id="cmbTenantType" tabindex="12"
																binding="#{pages$inspectionSearch.htmlSelectOneTenantType}"
																	>
																	<f:selectItem itemLabel="#{msg['commons.All']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.personKindList}" />
																</h:selectOneMenu>
															</td>
														</tr>

														<tr>
															<td >
																<h:outputLabel styleClass="LABEL" value="#{msg['unit.unitNumber']}:"></h:outputLabel>
															</td>
															<td >
															<h:inputText id="txtUnitNumber"  tabindex="13"
															binding="#{pages$inspectionSearch.htmlUnitNumber}"
																	>
																</h:inputText>
															</td>
															<td >
															
															</td>
															<td >

															</td>
														</tr>

														<tr>
															<td colspan="4" CLASS ="BUTTON_TD">
															 <h:commandButton styleClass="BUTTON" value="#{msg['commons.search']}" action="#{pages$inspectionSearch.btnSearch_Click}" style="width: 75px"/> 
															 <h:commandButton type="BUTTON" styleClass="BUTTON" value="#{msg['commons.clear']}" 
															                   onclick="javascript:resetValues();" 
															                   style="width: 75px"/> 
															<pims:security screen="Pims.InspectionManagement.Inspection.AddInspection" action="create">
															    <h:commandButton styleClass="BUTTON" value="#{msg['inspection.addNewVisitButton']}"
															                        action="#{pages$inspectionSearch.btnAdd_Click}" 
															                        style="width: 155px"/>
															 </pims:security>
															 </td>
														</tr>
														</table>
													</td>
													<td width="3%">
														&nbsp;
													</td>
												</tr>
														
													</table>

												</div>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" style="width: 98.6%;">
													<t:dataTable id="dt1"  
												value="#{pages$inspectionSearch.dataList}"
												binding="#{pages$inspectionSearch.tbl_InspectionSearch}" 
												rows="#{pages$inspectionSearch.paginatorRows}"
													preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%">

												<t:column id="inspectionNo"  sortable="true" width="30%">
													<f:facet name="header">
														<t:outputText value="#{msg['inspection.inspectionNo']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.inspectionNumber}" />
												</t:column>
												<t:column id="inspectionDate"  sortable="true" width="85">
													<f:facet name="header">
														<t:outputText value="#{msg['inspection.date']}" />														
														
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.inspectionDate}" >
													<f:convertDateTime
																	pattern="#{pages$inspectionSearch.dateFormat}"
																	timeZone="#{pages$inspectionSearch.timeZone}" />
														</t:outputText>
													
												</t:column>
												
													<t:column id="inspectionTypeEn" sortable="true" width="45%"  rendered="#{pages$inspectionSearch.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['inspection.type']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.typeEn}" />
												</t:column>
												<t:column id="inspectionTypeAr" sortable="true" width="45%" rendered="#{pages$inspectionSearch.isArabicLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['inspection.type']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.typeAr}" />
												</t:column>
												<t:column id="inspectionStatusEn" sortable="true" width="45%"  rendered="#{pages$inspectionSearch.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['inspection.status']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.statusEn}" />
												</t:column>
												<t:column id="inspectionStatusAr" sortable="true" width="45%" rendered="#{pages$inspectionSearch.isArabicLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['inspection.status']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.statusAr}" />
												</t:column>
												
														<t:column id="edit" width="50">
														<f:facet name="header">
															<t:outputText value="#{msg['commons.action']}" />
														</f:facet>
														<h:commandLink 
															action="#{pages$inspectionSearch.cmdDetails_Click}" >
															<h:graphicImage title="#{msg['commons.details']}" 
															                alt="#{msg['commons.details']}"
															                url="../resources/images/detail-icon.gif"
															                />
												        </h:commandLink>

													</t:column>
												
												
											</t:dataTable>

												</div>
												<t:div styleClass="contentDivFooter"
													style="width:99.6%;">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$inspectionSearch.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">
																<CENTER>
																	<t:dataScroller id="scroller" for="dt1"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$inspectionSearch.paginatorMaxPages}"
																		immediate="false" paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false"
																		pageIndexVar="pageNumber" styleClass="SCH_SCROLLER">
																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL"></t:graphicImage>
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
																			<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.pageNumber}" />
																					</td>
																				</tr>
																			</table>

																		</t:div>
																	</t:dataScroller>
																</CENTER>

															</td>
														</tr>
													</table>
												</t:div>
											</div>
									</div>

									</h:form>

									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
					<td colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
</html>
</f:view>