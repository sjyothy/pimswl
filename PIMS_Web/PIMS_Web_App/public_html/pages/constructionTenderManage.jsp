<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	function showSearchContractorPopUp()
	{    
//	     var screen_width = screen.width;
	//     var screen_height = screen.height;	     
//	    window.open('contractorSearch.jsf?pageMode=MODE_SELECT_MANY_POPUP','_blank','width='+(screen_width-10)+',height='+(screen_height-270)+',left=0,top=40,scrollbars=no,status=yes');	  
		var screen_width = 1024;
		var screen_height = 450;
		window.open('contractorSearch.jsf?pageMode=MODE_SELECT_MANY_POPUP','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');

	    
	}
	
	function populateContractor(contractorId, contractorNumber, contractorNameEn, contractorNameAr, licenseNumber, licenseSource, officeNumber)
	{
		document.getElementById("frm:hdnContractorId").value = contractorId;		
		document.forms[0].submit();
	}
	
	function showEditTenderProposalPopup() 
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('EditProposalDetails.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	}
	
	function showViewTenderProposalPopup() 
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('ViewProposalDetails.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	}
	
	function showAddProposalRecommendationPopup()
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('AddProposalRecommendation.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	}
	 function showTenderInquiryReplies()
	        {
			   var screen_width = 1024;
			   var screen_height = 586;
			   
	            window.open('tenderInquiryReplies.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-40)+',left=0,top=40,scrollbars=yes,status=yes');
	        }
    function showTanderInquiryAddPopUp()
	        {
			   var screen_width = 1024;
			   var screen_height = 586;
			   
	            window.open('tenderInquiryAdd.jsf?pageMode=MODE_POPUP','_blank','width='+(screen_width-10)+',height='+(screen_height-40)+',left=0,top=40,scrollbars=yes,status=yes');
	        }
	        
	        function showUploadPopup()
		{
		      var screen_width = 1024;
		      var screen_height = 450;
		      var popup_width = screen_width-530;
		      var popup_height = screen_height-150;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		  function closeWindow()
       {
         window.close();
       }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>

		<body class="BODY_STYLE">
			<div class="containerDiv">
				<%	
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<c:choose>
						<c:when test="${!pages$constructionTenderManage.viewModePopUp}">
							<tr>
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>
					<tr width="100%">
						<c:choose>
							<c:when test="${!pages$constructionTenderManage.viewModePopUp}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel
											value="#{msg['constructionTender.manage.pageTitle']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" height="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 475px; width: 100%; # height: 466px;">
											<h:form id="frm" enctype="multipart/form-data"
												style="WIDTH: 96%;">
												<h:inputHidden id="hdnContractorId"
													value="#{pages$constructionTenderManage.hdnContractorId}" />
												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:outputText
																	value="#{pages$constructionTenderManage.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText
																	value="#{pages$constructionTenderManage.infoMessages}"
																	escape="false" styleClass="INFO_FONT" />
															</td>
														</tr>
													</table>
												</div>
												<div>

													<t:div
														binding="#{pages$constructionTenderManage.divApproveReject}"
														style="width:98%;">
														<t:div>
															<t:panelGrid cellpadding="1px" cellspacing="2px"
																columns="4" styleClass="TAB_DETAIL_SECTION" width="100%">
																<h:outputLabel
																	value="#{msg['tenderManagement.approveTender.actionDate']}:" />
																<rich:calendar id="calActionDate"
																	binding="#{pages$constructionTenderManage.calActionDate}"
																	value="#{pages$constructionTenderManage.note.createdOn}"
																	datePattern="#{pages$constructionTenderManage.dateFormat}"
																	locale="#{pages$constructionTenderManage.locale}"
																	popup="true" showApplyButton="false"
																	enableManualInput="false" />
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel
																		value="#{msg['tenderManagement.approveTender.comments']}:" />
																</h:panelGroup>
																<h:inputTextarea id="txtComments"
																	binding="#{pages$constructionTenderManage.txtComments}" />
															</t:panelGrid>
														</t:div>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px;padding-bottom:10px;">
															<t:commandButton id="btnApprove" styleClass="BUTTON"
																binding="#{pages$constructionTenderManage.btnApprove}"
																value="#{msg['tenderManagement.approveTender.approve']}"
																action="#{pages$constructionTenderManage.onApprove}">
															</t:commandButton>

															<t:commandButton id="btnReject" styleClass="BUTTON"
																binding="#{pages$constructionTenderManage.btnReject}"
																value="#{msg['tenderManagement.approveTender.reject']}"
																action="#{pages$constructionTenderManage.onReject}">
															</t:commandButton>
														</t:div>
													</t:div>

													<div class="AUC_DET_PAD">
														
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$constructionTenderManage.tabPanel}"
																id="tabPanel" switchType="server"
																style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%">

																<rich:tab
																	binding="#{pages$constructionTenderManage.tenderDetailsTab}"
																	id="tenderDetailsTab"
																	label="#{msg['commons.tab.tenderDetails']}">
																	<%@  include file="constructionTenderDetails.jsp"%>
																</rich:tab>

																<rich:tab
																	binding="#{pages$constructionTenderManage.contractorsTab}"
																	id="contractorsTab"
																	label="#{msg['commons.tab.contractors']}">
																	<t:panelGrid id="contractDetailsButtonGrid"
																		width="99.4%" columnClasses="BUTTON_TD" columns="1">
																		<t:commandButton id="btnAddContractor"
																			binding="#{pages$constructionTenderManage.btnAddContractor}"
																			value="#{msg['constructionTender.addContractor']}"
																			styleClass="BUTTON" style="width: 100px"
																			onclick="javascript:showSearchContractorPopUp(); return false;" />
																	</t:panelGrid>
																	<%@ include file="Contractors.jsp"%>
																</rich:tab>

																<rich:tab
																	binding="#{pages$constructionTenderManage.sessionDetailsTab}"
																	id="sessionDetailsTab"
																	label="#{msg['commons.tab.sessionDetails']}">
																	<t:div id="divSessionDetail">
																		<t:panelGrid columns="4" cellpadding="1"
																			styleClass="TAB_DETAIL_SECTION" cellspacing="5"
																			style="width:100%;">

																			<h:outputLabel
																				value="#{msg['tenderManagement.openTender.sessionDate']}:" />
																			<rich:calendar id="calSessionDate"
																				binding="#{pages$constructionTenderManage.calSessionDate}"
																				value="#{pages$constructionTenderManage.tenderSessionView.tenderSessionDate}"
																				datePattern="#{pages$constructionTenderManage.dateFormat}"
																				locale="#{pages$constructionTenderManage.locale}"
																				popup="true" showApplyButton="false"
																				enableManualInput="false" />

																			<h:outputLabel
																				value="#{msg['tenderManagement.openTender.sessionTime']}:" />
																			<h:panelGroup>
																				<h:selectOneMenu id="ddnSessionTimeHH"
																					binding="#{pages$constructionTenderManage.ddnSessionTimeHH}"
																					style="VERTICAL-ALIGN: middle; width: 40px; height: 24px"
																					value="#{pages$constructionTenderManage.tenderSessionView.tenderSessionTimeHH}">
																					<f:selectItem itemValue="" itemLabel="HH" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.hours}" />
																				</h:selectOneMenu>
																				<h:outputText value=" : " />
																				<h:selectOneMenu id="ddnSessionTimeMM"
																					style="VERTICAL-ALIGN: middle; width: 40px; height: 24px"
																					binding="#{pages$constructionTenderManage.ddnSessionTimeMM}"
																					value="#{pages$constructionTenderManage.tenderSessionView.tenderSessionTimeMM}">
																					<f:selectItem itemValue="" itemLabel="MM" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.minutes}" />
																				</h:selectOneMenu>
																			</h:panelGroup>

																			<h:outputLabel
																				value="#{msg['tenderManagement.openTender.sessionLocation']}:" />
																			<h:inputText id="txtSessionLocation"
																				binding="#{pages$constructionTenderManage.txtSessionLocation}"
																				value="#{pages$constructionTenderManage.tenderSessionView.tenderSessionLocation}" />


																			<h:outputLabel
																				value="#{msg['tenderManagement.openTender.sessionRemarks']}:" />
																			<h:inputTextarea id="txtSessionRemarks"
																				binding="#{pages$constructionTenderManage.txtSessionRemarks}"
																				value="#{pages$constructionTenderManage.tenderSessionView.tenderSessionRemarks}" />

																		</t:panelGrid>
																	</t:div>
																</rich:tab>

																<rich:tab
																	binding="#{pages$constructionTenderManage.proposalsTab}"
																	id="proposalsTab"
																	label="#{msg['commons.tab.proposals']}">
																	<t:div style="width:100%;">
																		<t:panelGrid width="100%" columns="1" cellpadding="0"
																			cellspacing="0">
																			<t:div styleClass="contentDiv" style="width:99%">
																				<t:dataTable id="proposalDataTable"
																					renderedIfEmpty="true" var="tenderProposal"
																					binding="#{pages$constructionTenderManage.proposalDataTable}"
																					value="#{pages$constructionTenderManage.proposalDataList}"
																					preserveDataModel="false" preserveSort="false"
																					rowClasses="row1,row2" rules="all"
																					rows="#{pages$constructionTenderManage.paginatorRows}"
																					width="100%">
																					<t:column width="10%" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['tenderManagement.openProposal.proposalNumber']}" />
																						</f:facet>
																						<t:outputText
																							value="#{tenderProposal.proposalNumber}" />
																					</t:column>
																					<t:column width="12%" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['tenderManagement.openProposal.proposalDate']}" />
																						</f:facet>
																						<t:outputText
																							value="#{tenderProposal.proposalDate}" />
																					</t:column>
																					<t:column width="15%" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['tenderManagement.openProposal.proposalContractor']}" />
																						</f:facet>
																						<t:outputText
																							value="#{pages$constructionTenderManage.isEnglishLocale ? tenderProposal.contractorNameEn : tenderProposal.contractorNameAr}" />
																					</t:column>
																					<t:column width="12%" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['tenderManagement.openProposal.proposalAmount']}" />
																						</f:facet>
																						<t:outputText
																							value="#{tenderProposal.proposalAmount}" />
																					</t:column>
																					<t:column width="20%" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['tenderManagement.openProposal.recommendation']}" />
																						</f:facet>
																						<t:outputText
																							value="#{tenderProposal.recommendation}" />
																					</t:column>
																					<t:column width="10%" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['tenderManagement.openProposal.proposalStatus']}" />
																						</f:facet>
																						<t:outputText
																							value="#{pages$constructionTenderManage.isEnglishLocale ? tenderProposal.statusEn : tenderProposal.statusAr}" />
																					</t:column>
																					<t:column width="21%"
																						binding="#{pages$constructionTenderManage.proposalActionCol}"
																						sortable="false">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['tenderManagement.openProposal.proposalActions']}" />
																						</f:facet>
																						<h:commandLink id="editLink" rendered="false"
																							action="#{pages$constructionTenderManage.editLink_action}">
																							<h:graphicImage id="editIcon"
																								title="#{msg['commons.edit']}"
																								url="../resources/images/edit-icon.gif" />
																						</h:commandLink>
																						<t:outputLabel value=" " rendered="true"></t:outputLabel>
																						<h:commandLink id="viewLink" rendered="false"
																							action="#{pages$constructionTenderManage.viewLink_action}">
																							<h:graphicImage id="viewIcon"
																								title="#{msg['commons.view']}"
																								url="../resources/images/app_icons/View-Contract.png" />
																						</h:commandLink>
																						<t:outputLabel value=" " rendered="true"></t:outputLabel>
																						<h:commandLink id="addLink" rendered="false"
																							action="#{pages$constructionTenderManage.addLink_action}">
																							<h:graphicImage id="addIcon"
																								title="#{msg['commons.add']}"
																								url="../resources/images/detail-icon.gif" />
																						</h:commandLink>
																						<t:outputLabel value=" " rendered="true"></t:outputLabel>
																						<h:commandLink id="winnerLink"
																							rendered="#{tenderProposal.showWinner}"
																							onclick="if (!confirm('#{msg['winnerContractor.msg']}')) return false"
																							action="#{pages$constructionTenderManage.winnerLink_action}">
																							<h:graphicImage id="winnerIcon"
																								title="#{msg['bidder.markWinner']}"
																								url="../resources/images/app_icons/Dclare-Auction-Winner.png" />
																						</h:commandLink>
																						<t:outputLabel value=" " rendered="true"></t:outputLabel>
																						<h:commandLink id="cancelLink" rendered="false"
																							action="#{pages$constructionTenderManage.cancelLink_action}">
																							<h:graphicImage id="cancelIcon"
																								title="#{msg['commons.cancel']}"
																								url="../resources/images/app_icons/Cancel-Contract.png" />
																						</h:commandLink>
																						<t:outputLabel value=" " rendered="true"></t:outputLabel>
																						<h:commandLink id="deleteLink" rendered="false"
																							action="#{pages$constructionTenderManage.deleteLink_action}">
																							<h:graphicImage id="deleteIcon"
																								title="#{msg['commons.delete']}"
																								url="../resources/images/delete.gif" />
																						</h:commandLink>
																						<t:outputLabel value=" " rendered="true"></t:outputLabel>
																						<h:commandLink id="rejectLink" rendered="false"
																							action="#{pages$constructionTenderManage.rejectLink_action}">
																							<h:graphicImage id="rejectIcon"
																								title="#{msg['commons.reject']}"
																								url="../resources/images/app_icons/Renew-Contract.png" />
																						</h:commandLink>
																					</t:column>
																				</t:dataTable>

																			</t:div>
																			<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																				style="width:100%;">
																				<t:panelGrid columns="2" border="0" width="100%"
																					cellpadding="1" cellspacing="1">
																					<t:panelGroup
																						styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																						<t:div styleClass="JUG_NUM_REC_ATT">
																							<h:outputText value="#{msg['commons.records']}" />
																							<h:outputText value=" : " />
																							<h:outputText
																								value="#{pages$constructionTenderManage.recordSize}" />
																						</t:div>
																					</t:panelGroup>
																					<t:panelGroup>
																						<t:dataScroller id="proposalScroller"
																							for="proposalDataTable" paginator="true"
																							fastStep="1"
																							paginatorMaxPages="#{pages$constructionTenderManage.paginatorMaxPages}"
																							immediate="false" paginatorTableClass="paginator"
																							renderFacetsIfSinglePage="true"
																							paginatorTableStyle="grid_paginator"
																							layout="singleTable"
																							paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																							paginatorActiveColumnStyle="font-weight:bold;"
																							paginatorRenderLinkForActive="false"
																							pageIndexVar="pageNumber"
																							styleClass="JUG_SCROLLER">
																							<f:facet name="first">
																								<t:graphicImage url="../#{path.scroller_first}"
																									id="lblF1"></t:graphicImage>
																							</f:facet>
																							<f:facet name="fastrewind">
																								<t:graphicImage
																									url="../#{path.scroller_fastRewind}"
																									id="lblFR1"></t:graphicImage>
																							</f:facet>
																							<f:facet name="fastforward">
																								<t:graphicImage
																									url="../#{path.scroller_fastForward}"
																									id="lblFF1"></t:graphicImage>
																							</f:facet>
																							<f:facet name="last">
																								<t:graphicImage url="../#{path.scroller_last}"
																									id="lblL1"></t:graphicImage>
																							</f:facet>
																							<t:div styleClass="PAGE_NUM_BG" rendered="true">
																								<h:outputText styleClass="PAGE_NUM"
																									value="#{msg['commons.page']}" />
																								<h:outputText styleClass="PAGE_NUM"
																									style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																									value="#{requestScope.pageNumber}" />
																							</t:div>
																						</t:dataScroller>

																					</t:panelGroup>

																				</t:panelGrid>

																			</t:div>
																			<t:div style="padding:2px; text-align:right;">
																				<t:commandButton styleClass="BUTTON"
																					style="width:120px;"
																					value="#{msg['tenderProposal.DeselectWinners']}"
																					action="#{pages$constructionTenderManage.onDeselectWinners}"
																					binding="#{pages$constructionTenderManage.btnDeselect}"
																					onclick="return confirm('#{msg['tenderProposal.UnmarkAllConfirmation']}');" />
																			</t:div>

																		</t:panelGrid>
																	</t:div>
																</rich:tab>

																<rich:tab
																	binding="#{pages$constructionTenderManage.inquiryAppTab}"
																	id="inquiryAppTab" label="#{msg['tender.inquiryApp']}">
																	<t:div style="width:100%;">
																		<t:panelGrid columns="1" cellpadding="0"
																			cellspacing="0" width="100%">
																			<t:div styleClass="contentDiv" style="width:99%;">
																				<t:dataTable id="dtInquiry"
																					value="#{pages$constructionTenderManage.tenderInquiryList}"
																					binding="#{pages$constructionTenderManage.inquiryDataTable}"
																					rows="#{pages$constructionTenderManage.paginatorRows}"
																					preserveDataModel="false" preserveSort="false"
																					var="TenderInquiry" rowClasses="row1,row2"
																					rules="all" renderedIfEmpty="true" width="100%">

																					<t:column id="colInquiryNumber" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['tenderInquirySearch.inquiryNumber']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{TenderInquiry.requestNumber}" />
																					</t:column>

																					<t:column id="colInquiryDate" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['tenderInquirySearch.inquiryDate']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{TenderInquiry.requestDate}">
																							<f:convertDateTime
																								pattern="#{pages$constructionTenderManage.dateFormat}"
																								timeZone="#{pages$constructionTenderManage.timeZone}" />
																						</t:outputText>
																					</t:column>

																					<t:column id="colSourceType" rendered="false"
																						sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['tenderInquirySearch.sourceType']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{TenderInquiry.requestSourceEn}"
																							rendered="#{pages$constructionTenderManage.isEnglishLocale}" />
																						<t:outputText styleClass="A_LEFT"
																							value="#{TenderInquiry.requestSourceAr}"
																							rendered="#{!pages$constructionTenderManage.isEnglishLocale}" />
																					</t:column>

																					<t:column id="colContractorNumber" rendered="false"
																						sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['ContractorSearch.contractorNumber']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{TenderInquiry.contractorNumber}" />
																					</t:column>

																					<t:column id="colContractorName" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['ContractorSearch.contractorName']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{TenderInquiry.contractorNameEn}"
																							rendered="#{pages$constructionTenderManage.isEnglishLocale}" />
																						<t:outputText styleClass="A_LEFT"
																							value="#{TenderInquiry.contractorNameAr}"
																							rendered="#{!pages$constructionTenderManage.isEnglishLocale}" />
																					</t:column>
																					<t:column id="colTenderNumber" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['tenderInquirySearch.TenderNo']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{TenderInquiry.tenderNumber}" />
																					</t:column>

																					<t:column id="colTenderStatus" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['tenderInquirySearch.inquiryStatus']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{TenderInquiry.statusEn}"
																							rendered="#{pages$constructionTenderManage.isEnglishLocale}" />
																						<t:outputText styleClass="A_LEFT"
																							value="#{TenderInquiry.statusAr}"
																							rendered="#{!pages$constructionTenderManage.isEnglishLocale}" />
																					</t:column>
																					<t:column id="colTenderAction"
																						binding="#{pages$constructionTenderManage.actionInquiryAppCol}"
																						sortable="false">
																						<f:facet name="header">
																							<t:outputText value="#{msg['commons.action']}" />
																						</f:facet>

																						<h:commandLink
																							action="#{pages$constructionTenderManage.btnManage_Click}">
																							<h:graphicImage style="margin-left:5px;"
																								title="#{msg['commons.Manage']}"
																								url="../resources/images/detail-icon.gif" />
																						</h:commandLink>

																						<h:commandLink
																							action="#{pages$constructionTenderManage.btnDeleteInquiry_Click}">
																							<h:graphicImage style="margin-left:5px;"
																								title="#{msg['commons.delete']}"
																								url="../resources/images/delete.gif" />
																						</h:commandLink>
																						<h:commandLink
																							action="#{pages$constructionTenderManage.btnViewInquiry_Click}">
																							<h:graphicImage style="margin-left:5px;"
																								title="#{msg['commons.view']}"
																								url="../resources/images/app_icons/Contrect.png" />
																						</h:commandLink>
																					</t:column>

																				</t:dataTable>
																			</t:div>
																			<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																				style="width:100%;">
																				<t:panelGrid columns="2" border="0" width="100%"
																					cellpadding="1" cellspacing="1">
																					<t:panelGroup
																						styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																						<t:div styleClass="JUG_NUM_REC_ATT">
																							<h:outputText value="#{msg['commons.records']}" />
																							<h:outputText value=" : " />
																							<h:outputText
																								value="#{pages$constructionTenderManage.inquiryAppRecordSize}" />
																						</t:div>
																					</t:panelGroup>
																					<t:panelGroup>
																						<t:dataScroller id="Inquiryscroller"
																							for="dtInquiry" paginator="true" fastStep="1"
																							paginatorMaxPages="#{pages$constructionTenderManage.paginatorMaxPages}"
																							immediate="false" paginatorTableClass="paginator"
																							renderFacetsIfSinglePage="true"
																							paginatorTableStyle="grid_paginator"
																							layout="singleTable"
																							paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																							paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																							pageIndexVar="pageNumber"
																							styleClass="SCH_SCROLLER">

																							<f:facet name="first">
																								<t:graphicImage url="../#{path.scroller_first}"
																									id="lblFextend"></t:graphicImage>
																							</f:facet>
																							<f:facet name="fastrewind">
																								<t:graphicImage
																									url="../#{path.scroller_fastRewind}"
																									id="lblFRextend"></t:graphicImage>
																							</f:facet>
																							<f:facet name="fastforward">
																								<t:graphicImage
																									url="../#{path.scroller_fastForward}"
																									id="lblFFexte"></t:graphicImage>
																							</f:facet>
																							<f:facet name="last">
																								<t:graphicImage url="../#{path.scroller_last}"
																									id="lblLextend"></t:graphicImage>
																							</f:facet>
																							<t:div styleClass="PAGE_NUM_BG" rendered="true">
																								<h:outputText styleClass="PAGE_NUM"
																									value="#{msg['commons.page']}" />
																								<h:outputText styleClass="PAGE_NUM"
																									style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																									value="#{requestScope.pageNumber}" />
																							</t:div>
																						</t:dataScroller>
																					</t:panelGroup>
																				</t:panelGrid>
																			</t:div>
																		</t:panelGrid>
																	</t:div>
																</rich:tab>

																<rich:tab
																	binding="#{pages$constructionTenderManage.extendsAppTab}"
																	id="extendsAppTab"
																	label="#{msg['constructionTender.extendApp']}">
																	<t:div style="width:100%;">
																		<t:panelGrid columns="1" cellpadding="0"
																			cellspacing="0" width="100%">
																			<t:div styleClass="contentDiv" style="width:99%;">
																				<t:dataTable id="dt1"
																					value="#{pages$constructionTenderManage.extendedDataList}"
																					binding="#{pages$constructionTenderManage.extendedDataTable}"
																					rows="#{pages$constructionTenderManage.paginatorRows}"
																					preserveDataModel="false" preserveSort="false"
																					var="dataItem" rowClasses="row1,row2" rules="all"
																					renderedIfEmpty="true" width="100%">

																					<t:column id="requestNumberCol" width="15%"
																						sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['application.number.gridHeader']}" />
																						</f:facet>
																						<t:outputText value="#{dataItem.requestNumber}" />
																					</t:column>
																					<t:column id="requestDateCol" width="14%"
																						sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['application.date.gridHeader']}" />
																						</f:facet>
																						<t:outputText value="#{dataItem.requestDateStr}" />
																					</t:column>
																					<t:column id="contractorNameCol" width="20%"
																						sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['contractor.name.gridHeader']}" />
																						</f:facet>
																						<t:outputText value="#{dataItem.contractorName}" />
																					</t:column>
																					<t:column id="tenderNumberCol" width="12%"
																						sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['tender.number.gridHeader']}" />
																						</f:facet>
																						<t:outputText value="#{dataItem.tenderNumber}" />
																					</t:column>
																					<t:column id="tenderTypeCol" width="12%"
																						sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['tender.type.gridHeader']}" />
																						</f:facet>
																						<t:outputText value="#{dataItem.tenderType}" />
																					</t:column>
																					<t:column id="applicationStatusCol" width="12%"
																						sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['application.status.gridHeader']}" />
																						</f:facet>
																						<t:outputText
																							value="#{dataItem.applicationStatus}" />
																					</t:column>
																					<t:column id="actionCol"
																						binding="#{pages$constructionTenderManage.actionextendsAppCol}"
																						sortable="false" width="15%">
																						<f:facet name="header">
																							<t:outputText value="#{msg['commons.action']}" />
																						</f:facet>
																						<t:commandLink
																							action="#{pages$constructionTenderManage.onView}"
																							rendered="#{dataItem.showView}">
																							<h:graphicImage id="viewIcon"
																								title="#{msg['extendApplication.search.toolTips.view']}"
																								url="../resources/images/app_icons/Approve-Request.png" />&nbsp;
																											</t:commandLink>
																						<t:outputLabel value=" " rendered="true"></t:outputLabel>
																						<t:commandLink
																							action="#{pages$constructionTenderManage.onFollowUp}"
																							rendered="#{dataItem.showFollowUp}">
																							<h:graphicImage id="followUpIcon"
																								title="#{msg['extendApplication.search.toolTips.followUp']}"
																								url="../resources/images/app_icons/Provide-Tecnical-Comments.png" />&nbsp;
																											</t:commandLink>
																						<t:outputLabel value=" " rendered="true"></t:outputLabel>
																						<t:commandLink
																							action="#{pages$constructionTenderManage.onManage}"
																							rendered="#{dataItem.showManage}">
																							<h:graphicImage id="manageIcon"
																								title="#{msg['extendApplication.search.toolTips.manage']}"
																								url="../resources/images/app_icons/Request-detail.png" />&nbsp;
																											</t:commandLink>
																						<t:outputLabel value=" " rendered="true"></t:outputLabel>
																						<t:commandLink
																							action="#{pages$constructionTenderManage.onPublish}"
																							rendered="#{dataItem.showPublish}">
																							<h:graphicImage id="publishIcon"
																								title="#{msg['extendApplication.search.toolTips.publish']}"
																								url="../resources/images/app_icons/Request-detail.png" />&nbsp;
																											</t:commandLink>
																						<t:outputLabel value=" " rendered="true"></t:outputLabel>
																						<t:commandLink
																							onclick="if (!confirm('#{msg['extendApplication.search.confirm.delete']}')) return"
																							action="#{pages$constructionTenderManage.onDelete}"
																							rendered="#{dataItem.showDelete}">
																							<h:graphicImage id="deleteAppIcon"
																								title="#{msg['extendApplication.search.toolTips.delete']}"
																								url="../resources/images/app_icons/Request-detail.png" />&nbsp;
																											</t:commandLink>
																					</t:column>
																				</t:dataTable>
																			</t:div>
																			<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																				style="width:100%;">
																				<t:panelGrid columns="2" border="0" width="100%"
																					cellpadding="1" cellspacing="1">
																					<t:panelGroup
																						styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																						<t:div styleClass="JUG_NUM_REC_ATT">
																							<h:outputText value="#{msg['commons.records']}" />
																							<h:outputText value=" : " />
																							<h:outputText
																								value="#{pages$constructionTenderManage.extendAppRecordSize}" />
																						</t:div>
																					</t:panelGroup>
																					<t:panelGroup>
																						<t:dataScroller id="extendscroller" for="dt1"
																							paginator="true" fastStep="1"
																							paginatorMaxPages="#{pages$constructionTenderManage.paginatorMaxPages}"
																							immediate="false" paginatorTableClass="paginator"
																							renderFacetsIfSinglePage="true"
																							paginatorTableStyle="grid_paginator"
																							layout="singleTable"
																							paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																							paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																							pageIndexVar="pageNumber"
																							styleClass="SCH_SCROLLER">

																							<f:facet name="first">
																								<t:graphicImage url="../#{path.scroller_first}"
																									id="lblFinquiry"></t:graphicImage>
																							</f:facet>
																							<f:facet name="fastrewind">
																								<t:graphicImage
																									url="../#{path.scroller_fastRewind}"
																									id="lblFRinquiry"></t:graphicImage>
																							</f:facet>
																							<f:facet name="fastforward">
																								<t:graphicImage
																									url="../#{path.scroller_fastForward}"
																									id="lblFFinquiry"></t:graphicImage>
																							</f:facet>
																							<f:facet name="last">
																								<t:graphicImage url="../#{path.scroller_last}"
																									id="lblLinquiry"></t:graphicImage>
																							</f:facet>
																							<t:div styleClass="PAGE_NUM_BG" rendered="true">
																								<h:outputText styleClass="PAGE_NUM"
																									value="#{msg['commons.page']}" />
																								<h:outputText styleClass="PAGE_NUM"
																									style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																									value="#{requestScope.pageNumber}" />
																							</t:div>
																						</t:dataScroller>
																					</t:panelGroup>
																				</t:panelGrid>
																			</t:div>
																		</t:panelGrid>
																	</t:div>
																</rich:tab>

																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>

																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>

																<rich:tab id="activityLog"
																	label="#{msg['tenders.activityLog']}"
																	action="#{pages$constructionTenderManage.requestHistoryTabClick}"
																	rendered="#{!empty pages$constructionTenderManage.tenderView && 
							                      !empty pages$constructionTenderManage.tenderView.tenderId
							                     }">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														

														<t:div styleClass="BUTTON_TD" style="padding-top:10px;">
															<t:commandButton styleClass="BUTTON"
																value="#{msg['commons.saveButton']}"
																action="#{pages$constructionTenderManage.onSave}"
																binding="#{pages$constructionTenderManage.btnSave}" />
															<t:commandButton styleClass="BUTTON"
																value="#{msg['commons.sendButton']}"
																action="#{pages$constructionTenderManage.onSend}"
																binding="#{pages$constructionTenderManage.btnSend}" />
															<t:commandButton styleClass="BUTTON"
																value="#{msg['commons.cancel']}"
																action="#{pages$constructionTenderManage.onCancel}"
																rendered="#{!pages$constructionTenderManage.viewModePopUp}" />
															<h:commandButton id="cancelButton2" styleClass="BUTTON"
																type="button"
																rendered="#{pages$constructionTenderManage.viewModePopUp}"
																value="#{msg['commons.cancel']}" onclick="closeWindow()">
															</h:commandButton>
														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<c:choose>
									<c:when test="${!pages$constructionTenderManage.viewModePopUp}">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</c:when>
								</c:choose>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>