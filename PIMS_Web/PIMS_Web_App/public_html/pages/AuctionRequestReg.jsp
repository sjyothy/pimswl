<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>

<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=windows-1252" />
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="Cache-Control"
			content="no-cache,must-revalidate,no-store">
		<meta http-equiv="expires" content="0">

		<link rel="stylesheet" type="text/css"
			href="../resources/css/simple_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../resources/css/amaf_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../resources/css/table.css" />
		<title>PIMS</title>

	</head>
	<body>

		<f:view>
			<%
				UIViewRoot view = (UIViewRoot) session.getAttribute("view");
				if (view.getLocale().toString().equals("en")) {
			%>
			<body dir="ltr">
			<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg" />
				<%
				} else {
				%>
			
			<body dir="rtl">
			<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages_ar" var="msg" />
				<%
				}
				%>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<%
									if (view.getLocale().toString().equals("en")) {
									%>
									<td
										background="../../resources/images/Grey panel/Grey-Panel-left-1.jpg"
										height="38" style="background-repeat:no-repeat;" width="100%">
										<font
											style="font-family:Trebuchet MS;font-weight:regular;font-size:19px;margin-left:15px;top:30px;">
											<h:outputLabel value="#{msg['auction.registerAuction']}"></h:outputLabel>											
											</font>
										<!--<IMG src="../resources/images/Grey panel/Grey-Panel-left-1.jpg"/>-->
										<%
										} else {
										%>
									
									<td width="100%">
										<IMG
											src="../../resources/images/ar/Detail/Grey Panel/Grey-Panel-right-1.jpg"></img>
										<%
										}
										%>
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>

									<td width="500px" height="100%" valign="top" nowrap="nowrap">
										<h:form>
											<table style="width: 600px" >
												<tr>
													<td colspan="6">
														<h:messages></h:messages>
													</td>
												</tr>
												<tr>
													<td>
														<h:outputLabel value="#{msg['bidder.name']}"></h:outputLabel>
													</td>
													<td>
														<h:inputText style="width: 200px; height: 16px"></h:inputText>
													</td>
													<td colspan="4" align="left">
														<h:commandButton styleClass="BUTTON" value="#{msg['commons.search']}"></h:commandButton>
													</td>													
												</tr>
													
												<tr>
													<td>
														<h:outputLabel value="#{msg['auction.number']}"></h:outputLabel>
													</td>
													<td>	
														<h:selectOneMenu id="approvedAuctions">
															<f:selectItems value="#{pages$AucRequestReg.appAuctions}"/>
														</h:selectOneMenu>
													</td>
													<td colspan="4">
																																								
													</td>													
												</tr>
												
												<tr>
													<td colspan="6">
											<div class="contentDiv" style="width: 100%">

												<t:dataTable id="test2"
													value="#{pages$BidderLookup.bidderDataList}"
													binding="#{pages$BidderLookup.dataTable}"
													rows="15"
													preserveDataModel="true" preserveSort="true" var="dataItem"
													rules="all" renderedIfEmpty="true">
						
													<t:column id="col1" width="100" >
														<f:facet name="header">
															<t:outputText value="Property Ref No" />
														</f:facet>
														<t:outputText value="#{dataItem.firstName}" />
													</t:column>
						
													<t:column id="col2" width="100" >
														<f:facet name="header">
															<t:outputText value="Commercial Name" />
														</f:facet>
														<t:outputText value="#{dataItem.lastName}" />
													</t:column>
						
													<t:column id="col3" width="100" >
														<f:facet name="header">
															<t:outputText value="Unit No" />
														</f:facet>
														<t:outputText value="#{dataItem.passportNumber}" />
													</t:column>
						
													<t:column id="col4" width="100" >
														<f:facet name="header">
															<t:outputText value="Unit Type" />
														</f:facet>
														<t:outputText value="#{dataItem.passportNumber}" />
													</t:column>
													
													<t:column id="col5" width="100" >
														<f:facet name="header">
															<t:outputText value="Deposit Amount" />
														</f:facet>
														<t:outputText value="#{dataItem.passportNumber}" />
													</t:column>
													
													<t:column id="col6" width="100" >
														<f:facet name="header">
															<t:outputText value="Select" />
														</f:facet>
														<h:selectBooleanCheckbox value="select"
															 />
													</t:column>
												</t:dataTable>										
											</div>
													</td>
												</tr>
												<tr>
													<td colspan="6">
														<h:outputLabel value="Total Deposit Amount"></h:outputLabel>
														&nbsp;
														<h:inputText readonly="true" style="width: 186px; height: 16px"></h:inputText>
													</td>
												</tr>
												
												<tr align="right">
													<td colspan="6">
														<h:commandButton styleClass="BUTTON" value="Cancel"
															></h:commandButton>&nbsp;
														<h:commandButton styleClass="BUTTON" value="Save"
															></h:commandButton>
													</td>

												</tr>
											</table>
											

										</h:form>
			</body>
		</f:view>
</html>
