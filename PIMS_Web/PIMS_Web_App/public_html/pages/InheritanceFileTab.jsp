<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%-- Please remove above taglibs after completing your tab contents--%>
<t:div styleClass="contentDiv" style="width:95%">
	<t:dataTable id="test2" width="100%" styleClass="grid"
	    binding="#{pages$InheritanceFileTab.dataTable}"
		value="#{pages$InheritanceFileTab.inheritenceFileDataList}" rows="10"
		preserveDataModel="true" preserveSort="false" var="dataItem"
		rowClasses="row1,row2">

		<t:column id="col1En" width="10%" sortable="false"
			style="white-space: normal;">

			<f:facet name="header">
				<t:outputText value="#{msg['columns.grid.fileNumber']}" />
			</f:facet>
			<t:commandLink id="colfileNumber"
				action="#{pages$InheritanceFileTab.openFilePopUp}" styleClass="A_LEFT"
				value="#{dataItem.fileNumber}" />
		</t:column>


		<t:column id="fileStatusEn" width="10%" sortable="false"
			style="white-space: normal;"
			rendered="#{pages$InheritanceFileTab.isEnglishLocale}">
			<f:facet name="header">
				<t:outputText value="#{msg['columns.grid.fileStatus']}" />
			</f:facet>
			<t:outputText value="#{dataItem.fileStatusEn}" />
		</t:column>

		<t:column id="fileStatusAr" width="10%" sortable="false"
			style="white-space: normal;"
			rendered="#{pages$InheritanceFileTab.isArabicLocale}">
			<f:facet name="header">
				<t:outputText value="#{msg['columns.grid.fileStatus']}" />
			</f:facet>
			<t:outputText value="#{dataItem.fileStatusAr}" />
		</t:column>

		<t:column id="fileTypeEn" width="10%" sortable="false"
			style="white-space: normal;"
			rendered="#{pages$InheritanceFileTab.isEnglishLocale}">
			<f:facet name="header">
				<t:outputText value="#{msg['columns.grid.fileType']}" />
			</f:facet>
			<t:outputText value="#{dataItem.fileTypeEn}" />
		</t:column>

		<t:column id="fileTypeAr" width="10%" sortable="false"
			style="white-space: normal;"
			rendered="#{pages$InheritanceFileTab.isArabicLocale}">
			<f:facet name="header">
				<t:outputText value="#{msg['columns.grid.fileType']}" />
			</f:facet>
			<t:outputText value="#{dataItem.fileTypeAr}" />
		</t:column>
		<t:column id="newOldCostCenter" width="10%" sortable="false"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['property.fieldLabel.oldNewCostCenterFile']}" />
			</f:facet>
			<t:outputText value="#{dataItem.oldNewCostCenter}" />
		</t:column>
		<t:column id="beneNewOldCostCenter" width="10%" sortable="false"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['property.fieldLabel.oldNewCostCenterBene']}" />
			</f:facet>
			<t:outputText value="#{dataItem.beneOldNewCostCenter}" />
		</t:column>

		<t:column id="filePerson" width="10%" sortable="false"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['columns.grid.filePerson']}" />
			</f:facet>
			<t:outputText value="#{dataItem.filePerson}" />
		</t:column>


		<t:column id="relationShip" width="10%" sortable="false"
			rendered="#{pages$InheritanceFileTab.pageMode!='PAGE_MODE_ASSET'}"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['columns.grid.relationship']}" />
			</f:facet>
			<t:outputText value="#{dataItem.relationship}" />
		</t:column>

		<t:column id="researcher" width="10%" sortable="false"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['columns.grid.researcher']}" />
			</f:facet>
			<t:outputText value="#{dataItem.researcher}" />
		</t:column>

		<t:column id="share" width="10%" sortable="false"
			rendered="#{pages$InheritanceFileTab.pageMode!='PAGE_MODE_ASSET'}"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['columns.grid.share']}" />
			</f:facet>
			<t:outputText value="#{dataItem.share}" />
		</t:column>



	</t:dataTable>
</t:div>
<%--Column 3,4 Ends--%>
