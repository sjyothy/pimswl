<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>

		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>

			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['procedureGroupsMapping.heading']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height: 500px;">
										<h:form id="venueSearchFrm">
											<table border="0" class="layoutTable">
												<tr>
													<td>
														<h:outputText
															value="#{pages$ProcedureGroupsMapping.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />
														<h:outputText
															value="#{pages$ProcedureGroupsMapping.infoMessages}"
															escape="false" styleClass="INFO_FONT" />
													</td>
												</tr>
											</table>
											<div
												style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; MARGIN: 0px; WIDTH: 95%; PADDING-TOP: 10px">

												<div class="MARGIN">
													<div>
														<table cellpadding="1px" cellspacing="2px">
															<tr>

																<td class="PADDING: 5px; PADDING-TOP: 5px;width:25%;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['user.userGroups']}:" />
																</td>

																<td style="PADDING: 5px; PADDING-TOP: 5px; width: 25%;">
																	<h:selectOneMenu id="selectProcedureType"
																		style="width:192px;"
																		value="#{pages$ProcedureGroupsMapping.selectOneUserGroup}">
																		<f:selectItems
																			value="#{pages$ProcedureGroupsMapping.selectUserGroups}" />
																	</h:selectOneMenu>

																</td>
																<td style="PADDING: 5px; PADDING-TOP: 5px; width: 5%;">
																	<h:commandButton id="btnAddGroup" type="submit"
																		styleClass="BUTTON"
																		binding="#{pages$ProcedureGroupsMapping.btnAddGroup}"
																		value="#{msg['procedureGroupsMapping.addGroup']}"
																		action="#{pages$ProcedureGroupsMapping.addGroup}"
																		style="width:auto;" />
																</td>
																<td style="PADDING: 5px; PADDING-TOP: 5px; width: 45%">

																</td>
															</tr>
														</table>
													</div>
												</div>


												<div class="contentDiv" style="width: 97.8%">
													<t:dataTable id="dt1"
														value="#{pages$ProcedureGroupsMapping.procedureUserGroups}"
														binding="#{pages$ProcedureGroupsMapping.groupsDataTable}"
														rows="#{pages$ProcedureGroupsMapping.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="userGroup" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%">
														<t:column id="col2" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['member.groupName']}" />
															</f:facet>
															<t:outputText value="#{userGroup.userGroupId}"
																styleClass="A_LEFT" />
														</t:column>
														<t:column id="col9">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>
															<h:commandLink id="deleteLink"
																action="#{pages$ProcedureGroupsMapping.deleteGroup}">
																<h:graphicImage id="deleteIcon"
																	title="#{msg['commons.delete']}"
																	url="../resources/images/delete.gif" />&nbsp;
															</h:commandLink>
														</t:column>
													</t:dataTable>
												</div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:100%;">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$ProcedureGroupsMapping.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 50%">
																<t:dataScroller id="scroller" for="dt1" paginator="true"
																	fastStep="1"
																	paginatorMaxPages="#{pages$ProcedureGroupsMapping.paginatorMaxPages}"
																	immediate="false" paginatorTableClass="paginator"
																	renderFacetsIfSinglePage="true"
																	paginatorTableStyle="grid_paginator"
																	layout="singleTable" pageIndexVar="pageNumber"
																	paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																	paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																	styleClass="SCH_SCROLLER">

																	<f:facet name="first">
																		<t:graphicImage
																			url="../resources/images/first_btn.gif" id="lblF"></t:graphicImage>
																	</f:facet>
																	<f:facet name="fastrewind">
																		<t:graphicImage
																			url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>
																	</f:facet>
																	<f:facet name="fastforward">
																		<t:graphicImage url="../resources/images/next_btn.gif"
																			id="lblFF"></t:graphicImage>
																	</f:facet>
																	<f:facet name="last">
																		<t:graphicImage url="../resources/images/last_btn.gif"
																			id="lblL"></t:graphicImage>
																	</f:facet>
																	<div class="PAGE_NUM_BG">
																		<table cellpadding="0" cellspacing="0">
																			<tr>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																				</td>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</td>
																			</tr>
																		</table>
																		<div>
																</t:dataScroller>
															</td>
														</tr>
													</table>
												</t:div>
											</div>

											<div class="MARGIN">
												<table style="width: 98%">
													<tr>
														<td class="BUTTON_TD">
															<h:commandButton id="btnCancel" type="submit"
																styleClass="BUTTON" value="#{msg['commons.cancel']}"
																onclick="javascript:window.close();" style="width:auto;" />
															<h:commandButton id="btnSave" type="submit"
																styleClass="BUTTON" value="#{msg['commons.saveButton']}"
																action="#{pages$ProcedureGroupsMapping.saveGroups}"
																style="width:auto;" />
														</td>
													</tr>
												</table>
											</div>
									</div>
									</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</body>
	</html>
</f:view>