<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
	}	
	function   showFilePersonPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('SearchPerson.jsf?persontype=FILE_PERSON&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
	function showFilePersonDetailsIcon()
	{
	    
		return document.getElementById("detailsFrm:hdnFilePersonId").length > 0
	}	
	function onMessageFromEndowmentFileBen()
	{
	     disableInputs();
	     document.getElementById("detailsFrm:onMessageFromEndowmentFileBen").onclick();
	}
	function onMessageFromEndowmentFileAsso()
	{
	     disableInputs();
	     document.getElementById("detailsFrm:onMessageFromEndowmentFileAsso").onclick();
	}
	function onMessageFromEvaluateEndowmentPopup()
	{
	 //    disableInputs();
	     document.getElementById("detailsFrm:onMessageFromEvaluateEndowmentPopup").onclick();
	}	
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		disableInputs();	
		if( hdnPersonType=='APPLICANT' ) 
		{			    			 
			 document.getElementById("detailsFrm:hdnPersonId").value=personId;
			 document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
			 document.getElementById("detailsFrm:hdnPersonName").value=personName;
			 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
			 document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;
		}
		else if(hdnPersonType=='FILE_PERSON')
	    {
	    	 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
	    	 document.getElementById("detailsFrm:hdnFilePersonId").value=personId;
	    	 document.getElementById("detailsFrm:hdnFilePersonName").value=personName;
	    	 document.getElementById("detailsFrm:txtFilePersonName").value=personName;
	    	 
	    } 
					
	     document.getElementById("detailsFrm:onMessageFromSearchPerson").onclick();
	} 
	function   openEndowmentFileBenPopup()
	{
	   var screen_width   = 0.55 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   window.open('endowmentFileBenPopup.jsf?','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	    
	}
	
	function   openThirdPartyUnitPopup()
	{
	   var screen_width   = 0.55 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   
	   window.open('thirdPartyPropUnitsPopup.jsf?','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	    
	}
	
	
	function   openEndowmentFileAssoPopup()
	{
	   var screen_width   = 0.60 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   
	   window.open('endowmentFileAssoPopup.jsf?','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              ); 
	}
	function   openFollowupPopup()
	{
	   var screen_width = 900;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('memsFollowup.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width)+',height='+(screen_height)+
	     			 ',left=120,top=150,scrollbars=yes,status=yes,resizeable=yes');
	    
	}

	
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{pages$endowmentFile.pageTitle}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>

																<h:outputText id="errorId"
																	value="#{pages$endowmentFile.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$endowmentFile.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$endowmentFile.pageMode}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$endowmentFile.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$endowmentFile.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$endowmentFile.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$endowmentFile.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$endowmentFile.hdnIsCompany}"></h:inputHidden>
																<h:inputHidden id="hdnFilePersonId"
																	value="#{pages$endowmentFile.hdnFilePersonId}"></h:inputHidden>
																<h:inputHidden id="hdnFilePersonName"
																	value="#{pages$endowmentFile.hdnFilePersonName}"></h:inputHidden>

																<h:commandLink id="onMessageFromSearchPerson"
																	action="#{pages$endowmentFile.onMessageFromSearchPerson}" />

																<h:commandLink id="onMessageFromEndowmentFileBen"
																	action="#{pages$endowmentFile.onMessageFromEndowmentFileBen}" />


																<h:commandLink id="onMessageFromEndowmentFileAsso"
																	action="#{pages$endowmentFile.onMessageFromAddEndowmentPopup}" />

																<h:commandLink id="onMessageFromEvaluateEndowmentPopup"
																	action="#{pages$endowmentFile.onMessageFromEvaluateEndowmentPopup}" />

															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<t:div rendered="true" style="width:100%;">
														<t:panelGrid cellpadding="1px" width="100%"
															cellspacing="5px" columns="4">

															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentFile.lbl.num']}:" />
															<h:inputText id="txtFileNumber" readonly="true"
																value="#{pages$endowmentFile.endowmentFile.fileNumber}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.status']}:" />
															<h:inputText id="txtFileStatus"
																value="#{pages$endowmentFile.englishLocale? 
																	     pages$endowmentFile.endowmentFile.status.dataDescEn:
																	     pages$endowmentFile.endowmentFile.status.dataDescAr
																	    }"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.createdBy']}:" />
															<h:inputText id="txtFileCreatedB" readonly="true"
																value="#{pages$endowmentFile.endowmentFile.createdByName}"
																styleClass="READONLY" />


															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.createdOn']}:" />
															<h:inputText id="txtFileCreatedOn" readonly="true"
																value="#{pages$endowmentFile.endowmentFile.formattedCreatedOn}"
																styleClass="READONLY" />

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" />
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.Name']}:" />
															</h:panelGroup>
															<h:inputText id="txtFileName"
																value="#{pages$endowmentFile.endowmentFile.fileName}"
																/>

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" />
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['endowmentfile.lbl.filePerson']}:" />
															</h:panelGroup>
															<t:panelGroup colspan="2">
																<h:inputText id="txtFilePersonName"
																	value="#{pages$endowmentFile.hdnFilePersonName}"
																	styleClass="READONLY" />
																<h:graphicImage title="#{msg['commons.search']}"
																	style="MARGIN: 1px 1px -5px;cursor:hand;"
																	onclick="javaScript:showFilePersonPopup();"
																	url="../resources/images/app_icons/Search-tenant.png">

																	<h:graphicImage id="imgViewPersonApplicat"
																		title="#{msg['commons.details']}"
																		rendered="javaScript:showFilePersonDetailsIcon();"
																		onclick="javascript:showPersonReadOnlyPopup('#{pages$ApplicationDetails.applicationId}')"
																		style="MARGIN: 0px 0px -4px; CURSOR: hand;"
																		url="../resources/images/app_icons/Tenant.png"></h:graphicImage>
																</h:graphicImage>
															</t:panelGroup>
                                                                                                                        <h:outputLabel styleClass="LABEL"
																rendered="#{pages$endowmentFile.endowmentFile.inheritanceFile ne null && pages$endowmentFile.endowmentFile.inheritanceFile.inheritanceFileId ne null }"
																value="#{msg['endowmentFile.lbl.inheritanceFileNumber']}:" />
															<t:panelGroup colspan="2"
																rendered="#{pages$endowmentFile.endowmentFile.inheritanceFile ne null && pages$endowmentFile.endowmentFile.inheritanceFile.inheritanceFileId ne null }">
																<h:inputText id="inheritanceFileNumber" readonly="true"
																	value="#{pages$endowmentFile.endowmentFile.inheritanceFile.fileNumber}"
																	styleClass="READONLY" />
																<h:commandLink
																	action="#{pages$endowmentFile.onShowInheritanceFilePopup}">
																	<h:graphicImage
																		title="#{msg['inheritanceFile.tabHeader']}"
																		style="MARGIN: 1px 1px -5px;cursor:hand;"
																		url="../resources/images/app_icons/No-Objection-letter.png" />
																</h:commandLink>
															</t:panelGroup>
														</t:panelGrid>
													</t:div>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel binding="#{pages$endowmentFile.tabPanel}"
																style="width: 100%">

																<rich:tab id="applicationTab"
																	label="#{msg['commons.tab.applicationDetails']}">
																	<%@ include file="ApplicationDetails.jsp"%>
																</rich:tab>

																<rich:tab id="endowmentTab"
																	binding="#{pages$endowmentFile.tabEndowments}"
																	action="#{pages$endowmentFile.onEndowmentTab}"
																	label="#{msg['endowmentFile.tab.Endowments']}">
																	<%@ include file="tabEndowmentDetails.jsp"%>
																</rich:tab>
																<rich:tab id="registrationTab"
																	rendered="#{pages$endowmentFile.showRegistrationTab}"
																	action="#{pages$endowmentFile.onOfficialCorrespondenceTab}"
																	label="#{msg['endowmentFile.tab.officialCorrespondenceTab']}">
																	<%@ include
																		file="endowmentsOfficialCorrespondenceTab.jsp"%>
																</rich:tab>

																<rich:tab id="commentsTab" action="#{pages$endowmentFile.onAttachmentsCommentsClick}"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>

																<rich:tab id="attachmentTab" action="#{pages$endowmentFile.onAttachmentsCommentsClick}"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>


																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$endowmentFile.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">
															<pims:security screen="Pims.EndowMgmt.EndowmentFile.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentFile.showSaveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$endowmentFile.onSave}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentFile.showSubmitButton}"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSubmit"
																	action="#{pages$endowmentFile.onSubmit}" />
															</pims:security>
															<pims:security
																screen="Pims.EndowMgmt.EndowmentFile.Register"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentFile.showRegistration}"
																	value="#{msg['commons.register']}"
																	onclick="if (!confirm('#{msg['endowmentFile.msg.alertRegisterEndowments']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkRegister"
																	action="#{pages$endowmentFile.onRegister}" />
															</pims:security>

															<pims:security
																screen="Pims.EndowMgmt.EndowmentFile.Approve"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentFile.showApprove}"
																	value="#{msg['commons.approve']}"
																	onclick="if (!confirm('#{msg['endowmentFile.msg.alertApproveEndowments']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkApprove"
																	action="#{pages$endowmentFile.onApprove}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentFile.showApprove}"
																	value="#{msg['commons.reject']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkReject"
																	action="#{pages$endowmentFile.onReject}" />
															</pims:security>
															<pims:security
																screen="Pims.EndowMgmt.EndowmentFile.Evaluate"
																action="view">


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentFile.showEvaluation}"
																	value="#{msg['commons.evaluate']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkEvaluate"
																	action="#{pages$endowmentFile.onEvaluateDone}" />

															</pims:security>
															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$endowmentFile.showComplete}"
																value="#{msg['commons.complete']}"
																onclick="if (!confirm('#{msg['endowmentFile.msg.alertEndowmentFileCompletion']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkComplete"
																action="#{pages$endowmentFile.onComplete}" />

														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>