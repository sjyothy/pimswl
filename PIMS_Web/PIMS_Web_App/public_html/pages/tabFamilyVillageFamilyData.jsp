
<t:div id="tabBDetailsdf" style="width:100%;">
	<t:panelGrid id="tabdsBDetailsab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="firstname" styleClass="LABEL"
				value="#{msg['commons.Name']}:"></h:outputLabel>
		</h:panelGroup>
		<h:inputText maxlength="500"
			value="#{pages$tabFamilyVillageFamilyData.pageView.familyMember.firstName}" />
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.relationship']}" />
		</h:panelGroup>
		<h:selectOneMenu id="relationshipIdBeneficiary"
			styleClass="SELECT_MENU"
			value="#{pages$tabFamilyVillageFamilyData.pageView.relationship.relationshipIdString}">
			<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
				itemValue="-1" />
			<f:selectItems value="#{pages$ApplicationBean.relationList}" />
		</h:selectOneMenu>


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['customer.dateOfBirth']}:" />
		</h:panelGroup>
		<rich:calendar id="DateOfBirthFamly"
			value="#{pages$tabFamilyVillageFamilyData.pageView.familyMember.dateOfBirth}"
			popup="true"
			datePattern="#{pages$tabFamilyVillageFamilyData.dateFormat}"
			showApplyButton="false"
			locale="#{pages$tabFamilyVillageFamilyData.locale}"
			enableManualInput="false" inputStyle="width: 170px; height: 14px" />

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL" value="#{msg['customer.gender']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="selectGenderfamilymember"
			value="#{pages$tabFamilyVillageFamilyData.pageView.familyMember.gender}">
			<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
				itemValue="-1" />
			<f:selectItem itemLabel="#{msg['tenants.gender.male']}" itemValue="M" />
			<f:selectItem itemLabel="#{msg['tenants.gender.female']}"
				itemValue="F" />
		</h:selectOneMenu>

		<h:panelGroup id="lblDeathDate">

			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.deathDate']}" />
		</h:panelGroup>
		<rich:calendar id="clndrDeathDate"
			value="#{pages$tabFamilyVillageFamilyData.pageView.familyMember.dateOfDeath}"
			locale="#{pages$tabFamilyVillageFamilyData.locale}" popup="true"
			datePattern="#{pages$tabFamilyVillageFamilyData.dateFormat}"
			showApplyButton="false" enableManualInput="false"
			inputStyle="width: 170px; height: 14px" />

		<h:outputText
			value="#{msg['mems.inheritanceFile.fieldLabel.maritalStatus']}" />
		<h:selectOneMenu id="cboMaritialStatusBeneficiary"
			value="#{pages$tabFamilyVillageFamilyData.pageView.familyMember.maritialStatusIdString}">
			<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
				itemValue="-1" />
			<f:selectItems value="#{pages$ApplicationBean.maritialStatus}" />
		</h:selectOneMenu>


		<h:outputLabel value="#{msg['educationAspects.level']} :"
			style="font-weight:normal;" styleClass="TABLE_LABEL" />
		<h:selectOneMenu id="level"
			value="#{pages$tabFamilyVillageFamilyData.pageView.familyMember.educationAspects.educationLevelId}">
			<f:selectItem itemValue="-1"
				itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
			<f:selectItems value="#{pages$ApplicationBean.allEducationLevel}" />
		</h:selectOneMenu>

		<h:outputLabel value="#{msg['educationAspects.educationLevelDesc']} :"
			style="font-weight:normal;" styleClass="TABLE_LABEL" />
		<h:inputTextarea id="eduLevelDesc"
			onkeyup="javaScript:removeExtraCharacter(this,50)"
			onkeypress="javaScript:removeExtraCharacter(this,50)"
			onkeydown="javaScript:removeExtraCharacter(this,50)"
			onblur="javaScript:removeExtraCharacter(this,50)"
			onchange="javaScript:removeExtraCharacter(this,50)"
			value="#{pages$tabFamilyVillageFamilyData.pageView.familyMember.educationAspects.educationLevelDesc}" />

		<h:outputText
			value="#{msg['familyVillageManageBeneficiary.lbl.marriagesCount']}" />

		<h:inputText id="marrigesCount" maxlength="2"
			onkeypress="removeNonInteger(this)"
			onkeydown="removeNonInteger(this)" onblur="removeNonInteger(this)"
			onchange="removeNonInteger(this)"
			value="#{pages$tabFamilyVillageFamilyData.pageView.familyMember.marriagesCount}" />


		<h:outputText
			value="#{msg['familyVillageManageBeneficiary.lbl.salary']}" />

		<h:inputText id="salary" maxlength="8"
			onkeypress="removeNonInteger(this)"
			onkeydown="removeNonInteger(this)" onblur="removeNonInteger(this)"
			onchange="removeNonInteger(this)"
			value="#{pages$tabFamilyVillageFamilyData.pageView.familyMember.salary}" />


	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;">

		<h:commandButton styleClass="BUTTON" value="#{msg['commons.Add']}"
			action="#{pages$tabFamilyVillageFamilyData.onAdd}">
		</h:commandButton>
	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableFamilyDetails" rows="15" width="100%"
			value="#{pages$tabFamilyVillageFamilyData.dataList}"
			binding="#{pages$tabFamilyVillageFamilyData.dataTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="beneficiaryNamedtc" sortable="true">
				<f:facet name="header">
					<t:outputText id="beneficiaryNameOT" value="#{msg['commons.Name']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageFamilyData.englishLocale? 
																				dataItem.familyMember.personFullName:
																				dataItem.familyMember.personFullName}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="relationshipCol" sortable="true">
				<f:facet name="header">
					<h:outputText
						value="#{msg['mems.inheritanceFile.columnLabel.relationship']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageFamilyData.englishLocale? dataItem.relationship.relationshipDescEn :
				dataItem.relationship.relationshipDescAr
				}" />
			</t:column>
			<t:column id="dateofbirthcil" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['customer.dateOfBirth']}" />
				</f:facet>
				<h:outputText value="#{dataItem.familyMember.dateOfBirth}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageFamilyData.dateFormat}"
						timeZone="#{pages$tabFamilyVillageFamilyData.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="educationLevelEn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['educationAspects.level']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageFamilyData.englishLocale? dataItem.familyMember.educationAspects.educationLevelEn :
																			dataItem.familyMember.educationAspects.educationLevelAr 
				}" />
			</t:column>
			<t:column id="gendercol" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['customer.gender']}" />
				</f:facet>
				<h:outputText value="#{dataItem.familyMember.gender}">

				</h:outputText>
			</t:column>
			<t:column id="salarycol" sortable="true">
				<f:facet name="header">
					<h:outputText
						value="#{msg['familyVillageManageBeneficiary.lbl.salary']}" />
				</f:facet>
				<h:outputText value="#{dataItem.familyMember.salary}">

				</h:outputText>
			</t:column>

			<t:column id="action" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAction" value="#{msg['commons.action']}" />
				</f:facet>
				<%-- 
				<t:commandLink
					action="#{pages$tabFamilyVillageFamilyData.onOpenResearcherForm}">
					<h:graphicImage id="researchForm"
						title="#{msg['beneficiaryDetails.tooltip.openResearchForm']}"
						style="cursor:hand;margin-left:2px;margin-right:2px"
						url="../resources/images/detail-icon.gif" />&nbsp;
				</t:commandLink>
--%>
				<h:commandLink id="lnkDeleteBeneficiary"
					onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableInputs();"
					action="#{pages$tabFamilyVillageFamilyData.onDelete}">
					<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
						style="cursor:hand;margin-left:2px;margin-right:2px"
						url="../resources/images/delete.gif" width="18px;" />
				</h:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>


</t:div>


