

<h:panelGrid columns="2" columnClasses="presidentDiscussionColumn"
   rendered="#{pages$inspectProperty.floorCurrent}">

<t:dataTable id="test2" width="100%"
	value="#{pages$inspectProperty.floorList}"
	binding="#{pages$inspectProperty.dataTable}" rows="15"
	preserveDataModel="false" preserveSort="false" var="dataItem"
	rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

	<t:column id="col1" width="20">

		<f:facet name="header">

			<t:outputText value="Floor Number" />

		</f:facet>
		<t:outputText value="#{dataItem.floorNumber}" />
	</t:column>


	<t:column id="col2" sortable="true" width="20%">
		<f:facet name="header">


			<t:outputText value="Floor Type English" />

		</f:facet>
		<t:outputText value="#{dataItem.floorTypeEn}" />
	</t:column>


	<t:column id="col3" sortable="true">


		<f:facet name="header">

			<t:outputText value="Floor Type Arabic" />

		</f:facet>
		<t:outputText value="#{dataItem.floorTypeAr}" />
	</t:column>
		
							<t:column id="col4">

													<f:facet name="header">
														<t:outputText value="Edit" />
													</f:facet>
													<h:commandLink value="Edit"
														action="#{pages$inspectProperty.editFloor}" />

												</t:column>
</t:dataTable>

	<t:dataScroller id="scroller" for="test2" paginator="true" fastStep="1"
		paginatorMaxPages="2" immediate="true" styleClass="scroller"
		paginatorTableClass="paginator" renderFacetsIfSinglePage="false"
		paginatorTableStyle="grid_paginator" layout="singleTable"
		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
		paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

		<f:facet name="first">

			<t:outputLabel value="First" id="lblF" />

		</f:facet>

		<f:facet name="last">

			<t:outputLabel value="Last" id="lblL" />

		</f:facet>

		<f:facet name="fastforward">

			<t:outputLabel value="FFwd" id="lblFF" />

		</f:facet>

		<f:facet name="fastrewind">

			<t:outputLabel value="FRev" id="lblFR" />

		</f:facet>

	</t:dataScroller>
	</h:panelGrid>