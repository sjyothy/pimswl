<%-- 
  - Author: Muhammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for adding payment details
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	

		function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	function closeWindow()
		{
		  window.close();
		}
        
      
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">



			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">





				<tr width="100%">



					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['paymentDetailsPopUp.header']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="searchFrm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION" style="height: 420px;">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$paymentDetailsPopUp.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">
												
												<div class="DETAIL_SECTION" style="width: 99.8%;">

													<table id="detailTable" cellpadding="1px" cellspacing="2px"
														border="0" class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">

																	<tr>

																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['paymentDetailsPopUp.payMode']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:selectOneMenu id="paymentMode"
																				binding="#{pages$paymentDetailsPopUp.paymentModeSelectMenu}"
																				tabindex="3">
																				<f:selectItems
																					value="#{pages$paymentDetailsPopUp.paymentModeList}" />
																				<a4j:support event="onchange"
																					reRender="detailTable,accountNumber,bank"
																					action="#{pages$paymentDetailsPopUp.onPaidToChanged}" />



																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				rendered="#{pages$paymentDetailsPopUp.isContextCollection}"
																				value="#{msg['paymentDetailsPopUp.paidTo']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="disbursementSource"
																				rendered="#{pages$paymentDetailsPopUp.isContextCollection}"
																				binding="#{pages$paymentDetailsPopUp.paidToSelectMenu}"
																				tabindex="3">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['researchFormBenef.others']}" />
																				<f:selectItems
																					value="#{pages$paymentDetailsPopUp.paidToList}" />
																				<a4j:support event="onchange"
																					reRender="detailTable,accountNumber,bank"
																					action="#{pages$paymentDetailsPopUp.onPaidToChanged}" />

																			</h:selectOneMenu>
																		</td>

																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['paymentDetailsPopUp.referenceNumber']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="referenceNum"
																				binding="#{pages$paymentDetailsPopUp.htmlReferenceNumber}"
																				tabindex="10">
																			</h:inputText>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['paymentDetailsPopUp.bank']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="bank"
																				binding="#{pages$paymentDetailsPopUp.bankSelectMenu}"
																				tabindex="3">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$paymentDetailsPopUp.bankList}" />
																			</h:selectOneMenu>
																		</td>
																		<%--<f:selectItems
																					value="#{pages$paymentDetailsPopUp.disbursementSourceList}" /> --%>


																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['paymentDetailsPopUp.date']}:"></h:outputLabel>

																		</td>

																		<td>
																			<rich:calendar id="createdOnDateId"
																				styleClass="READONLY" disabled="false"
																				binding="#{pages$paymentDetailsPopUp.clndrCreatedOn}"
																				locale="#{pages$paymentDetailsPopUp.locale}"
																				popup="true"
																				datePattern="#{pages$inheritanceFile.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:185px;height:17px" />
																		</td>

																		<td>
																			<h:outputLabel styleClass="LABEL" 
																				value="#{msg['paymentDetailsPopUp.accountNumber']}:"></h:outputLabel>

																		</td>
																		<td>
																			<h:inputText id="accountNumber" maxlength="23"
																				binding="#{pages$paymentDetailsPopUp.htmlAccountNumber}"
																				tabindex="10">
																			</h:inputText>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				id="lblOtherBeneficiary"
																				binding="#{pages$paymentDetailsPopUp.htmlOtherBeneficiaryLbl}"
																				value="#{msg['paymentDetailPopUp.otherBeneficiary']}:"></h:outputLabel>

																		</td>


																		<td>
																			<h:inputText id="otherBeneficiary"
																				binding="#{pages$paymentDetailsPopUp.htmlOtherBeneficiary}">
																			</h:inputText>

																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				id="lblReadOnlyBankName"
																				
																				value="#{msg['paymentDetailsPopUp.bank']}:"></h:outputLabel>

																		</td>


																		<td>
																			<h:inputText id="htmlReadOnlyBankName" readonly="true" styleClass="READONLY"
																				binding="#{pages$paymentDetailsPopUp.htmlReadOnlyBankName}">
																			</h:inputText>

																		</td>

																	</tr>




																	<tr>
																		<td class="BUTTON_TD" colspan="4">

																			<h:commandButton styleClass="BUTTON" type="submit"
																				disabled="#{pages$paymentDetailsPopUp.pageModeConfirm}"
																				value="#{msg['commons.done']}"
																				actionListener="#{pages$paymentDetailsPopUp.sendPaymentInfoToParent}" />

																		</td>

																	</tr>
																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>

													</table>
												</div>
											</div>
										</div>


									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>


		</body>
	</html>
</f:view>