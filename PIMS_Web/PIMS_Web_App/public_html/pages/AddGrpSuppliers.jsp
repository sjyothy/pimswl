<%-- 
  - Author:Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - 
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	function disableAddControl( cntrl )
	{
		  
		  document.getElementById('frm:btnAdd').disabled = "true";
		  document.getElementById('frm:addLink').onclick();
	}
	            
	function onChkChange()
    {
      document.getElementById('frm:disablingDiv').style.display='none';
    }
    function onSelectionChanged(changedChkBox)
	{
	    document.getElementById('frm:disablingDiv').style.display='block';
		document.getElementById('frm:lnkUnitSelectionChanged').onclick();
	}
	</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">


			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">




				<tr width="100%">


					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['grpSupplier.lbl.heading']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>
									<h:form id="frm" enctype="multipart/form-data">
										<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText id="errorMessages"
																value="#{pages$AddGrpSuppliers.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$AddGrpSuppliers.successMessages}"
																escape="false" styleClass="INFO_FONT" />
															<h:messages></h:messages>

															<h:commandLink id="addLink"
																action="#{pages$AddGrpSuppliers.onAdd}" />

															<a4j:commandLink id="lnkUnitSelectionChanged"
																onbeforedomupdate="javascript:onChkChange();"
																action="#{pages$AddGrpSuppliers.onChangeFileCombo}"
																reRender="costCenter" />




														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width: 100%">
													<h:outputLabel value="" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table id="tableForm" cellpadding="1px" width="100%"
														cellspacing="2px" class="DETAIL_SECTION_INNER" columns="4">

														<tr>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['grpSupplier.lbl.name']} :"></h:outputLabel>
															</td>
															<td>
																<h:selectOneMenu id="cmbselectedSupplierName"
																	value="#{pages$AddGrpSuppliers.selectedSupplierName}"
																	tabindex="3">
																	<f:selectItem
																		itemLabel="#{msg['commons.pleaseSelect']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.allGrpSupplierName}" />
																	<a4j:support event="onchange"
																		action="#{pages$AddGrpSuppliers.onSupplierChange}"
																		reRender="cmbselectedSupplierCode,errorMessages" />

																</h:selectOneMenu>


															</td>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['grpSupplier.lbl.code']} :"></h:outputLabel>
															</td>
															<td>
																<h:selectOneMenu id="cmbselectedSupplierCode"
																	value="#{pages$AddGrpSuppliers.selectedSupplierCode}"
																	tabindex="3">

																	<f:selectItems
																		value="#{pages$AddGrpSuppliers.codeList}" />


																</h:selectOneMenu>


															</td>

														</tr>


													</table>
												</div>
												<t:div>&nbsp;</t:div>
												<table class="BUTTON_TD" cellpadding="1px" width="100%"
													cellspacing="3px">
													<tr>
														<td class="BUTTON_TD" colspan="10">

															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.done']}"
																binding="#{pages$AddGrpSuppliers.btnDone}"
																action="#{pages$AddGrpSuppliers.onDone}"
																style="width: 135px">
															</h:commandButton>
														</td>
													</tr>
												</table>


												<br></br>
												<table id="imageTable" cellpadding="0" cellspacing="0"
													width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG id="image1"
																src="../<%=ResourceUtil.getInstance().getPathProperty(
									"img_section_left")%>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td style="FONT-SIZE: 0px" width="100%">
															<IMG id="image2"
																src="../<%=ResourceUtil.getInstance().getPathProperty(
									"img_section_mid")%>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG id="image3"
																src="../<%=ResourceUtil.getInstance().getPathProperty(
									"img_section_right")%>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="contentDiv" style="width: 99%">
													<t:dataTable id="test2"
														value="#{pages$AddGrpSuppliers.dataList}"
														binding="#{pages$AddGrpSuppliers.dataTable}" width="100%"
														rows="10000" preserveDataModel="false"
														preserveSort="false" var="dataItem" rowClasses="row1,row2"
														rules="all" renderedIfEmpty="true">

														<t:column id="checkbox" style="width:auto;">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.select']}" />
															</f:facet>
															<h:selectBooleanCheckbox id="idbox" immediate="true"
																value="#{dataItem.selected}">
															</h:selectBooleanCheckbox>
														</t:column>
														<t:column id="name" width="10%" defaultSorted="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText value="#{msg['commons.Name']}" />

															</f:facet>
															<t:outputText value="#{dataItem.name}"
																style="white-space: normal;" />

														</t:column>

														<t:column id="suppName" width="10%"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText value="#{msg['grpSupplier.lbl.name']}" />

															</f:facet>
															<t:outputText value="#{dataItem.supplierName}"
																style="white-space: normal;" />

														</t:column>

														<t:column id="suppCode" width="10%"
															style="white-space: normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['grpSupplier.lbl.code']}" />
															</f:facet>
															<t:outputText value="#{dataItem.supplierCode}"
																style="white-space: normal;" />
														</t:column>
													</t:dataTable>
												</div>

											</div>
									</h:form>

								</td>
							</tr>


						</table>

						</div>

						</div>



						</div>




					</td>
				</tr>
			</table>
			</td>
			</tr>
			</table>



		</body>
	</html>
</f:view>