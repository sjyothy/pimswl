<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
			<script language="javascript" type="text/javascript">
				function clearWindow() {
					document.getElementById("frmSearchTenderInquiry:txtReqNumber").value="";
					document.getElementById("frmSearchTenderInquiry:txtContractorNameEn").value="";	
					document.getElementById("frmSearchTenderInquiry:txtContractorNameAr").value="";
					document.getElementById("frmSearchTenderInquiry:txtTenderNumber").value="";
					document.getElementById("frmSearchTenderInquiry:txtTenderDescription").value="";
					
					
					document.getElementById("frmSearchTenderInquiry:selectRequestStatus").value="-1";
					document.getElementById("frmSearchTenderInquiry:selectRequestSource").value="-1";
					document.getElementById("frmSearchTenderInquiry:selectContractorType").value="-1";
					
					
					 $('frmSearchTenderInquiry:requestDate').component.resetSelectedDate();
				}
				
				function closeWindow() {
					window.opener="x";
				  	window.close();
				}
				
				function sendToParent(contractorId, contractorNumber, contractorNameEn, contractorNameAr, licenseNumber, licenseSource, officeNumber) {
				    window.opener.populateContractor(contractorId, contractorNumber, contractorNameEn, 
				    								contractorNameAr, licenseNumber, licenseSource, officeNumber);
				    window.close();
				}
					
			    function closeWindow() {
					window.close();
				}
				
		function showTanderInquiryAddPopUp()
	        {
			   var screen_width = 1024;
			   var screen_height = 500;
			   
	            window.open('tenderInquiryAdd.jsf?pageMode=MODE_POPUP','_blank','width='+(screen_width-10)+',height='+(screen_height-40)+',left=0,top=40,scrollbars=no,status=yes');
	        }
	        
	   function showTenderInquiryReplies()
	        {
			   var screen_width = 1024;
			   var screen_height = 500;
			   
	            window.open('tenderInquiryReplies.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height)+',left=0,top=40,scrollbars=no,status=yes');
	        }
	        
	   function showTenderInquiryRepliesAll()
	        {
			   var screen_width = 1024;
			   var screen_height = 586;
			   
	            window.open('tenderInquiryReplayAll.jsf','_blank','width='+(screen_width-400)+',height='+(screen_height-250)+',left=40,top=40,scrollbars=yes,status=yes');
	        }
			</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"	CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>" />		


		</head>

		<body class="BODY_STYLE">
		   <div class="containerDiv">
				<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
			%>
	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['tenderInquirySearch.heading']}" styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>						
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="3" />
								<td width="100%" height="450px" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION">
										<h:form id="frmSearchTenderInquiry" style="width:97%">
											<div >
												<table border="0" class="layoutTable">
													<tr>
														<td colspan="6">
															<h:outputText
																value="#{pages$tenderInquirySearch.errorMessages}"
																escape="false" styleClass="INFO_FONT"
																style="padding:10px;" />
														</td>
													</tr>
												</table>
											</div>
										<div class="MARGIN"> 
										<table cellpadding="0" cellspacing="0" width="100%">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
											<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
										<div class="DETAIL_SECTION">
											<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
											<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenderInquirySearch.inquiryNumber']}:" />
															</td>
															<td>
																<h:inputText styleClass="A_LEFT INPUT_TEXT" 
																	id="txtReqNumber" 
																	value="#{pages$tenderInquirySearch.requestNumber}" />
															</td>

															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenderInquirySearch.inquiryStatus']}:" />
															</td>
															<td>
																<h:selectOneMenu id="selectRequestStatus"
																	styleClass="SELECT_MENU"
																	value="#{pages$tenderInquirySearch.requestStatusId}">
																	<f:selectItem id="selectAllStatusType" itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$tenderInquirySearch.requestStatusList}" />
																</h:selectOneMenu>
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenderInquirySearch.inquiryDate']}:" />
															</td>
															<td >
													          <rich:calendar id="requestDate"  value="#{pages$tenderInquirySearch.requestDate}"
							                                  locale="#{pages$tenderInquirySearch.locale}" popup="true" datePattern="#{pages$tenderInquirySearch.dateFormat}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px" />
												            </td>

															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenderInquirySearch.sourceType']}:" />
															</td>
															<td>
																<h:selectOneMenu id="selectRequestSource"
																	styleClass="SELECT_MENU"
																	value="#{pages$tenderInquirySearch.sourceTypeId}">
																	<f:selectItem id="selectAllSourceType" itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.applicationSourceList}" />
																</h:selectOneMenu>
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL" 
																	value="#{msg['tenderInquirySearch.contractorNameEn']}:" />
															</td>
															<td >
																<h:inputText styleClass="A_LEFT INPUT_TEXT" 
																	id="txtContractorNameEn"
																	value="#{pages$tenderInquirySearch.contractorNameEn}" />
															</td>

															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenderInquirySearch.contractorNameAr']}:" />
															</td>
															<td >
																<h:inputText styleClass="A_LEFT INPUT_TEXT" 
																	id="txtContractorNameAr" 
																	value="#{pages$tenderInquirySearch.contractorNameAr}" />
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenderInquirySearch.contractorType']}:" />
															</td>
															<td>
																<h:selectOneMenu id="selectContractorType"
																	styleClass="SELECT_MENU"
																	value="#{pages$tenderInquirySearch.contractorType}">
																	<f:selectItem id="selectAllContractorType" itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$tenderInquirySearch.contractorTypeList}" />
																</h:selectOneMenu>
															</td>

															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenderInquirySearch.TenderNo']}:" />
															</td>
															<td width="30%">
																<h:inputText styleClass="A_LEFT INPUT_TEXT" 
																	id="txtTenderNumber" 
																	value="#{pages$tenderInquirySearch.tenderNumber}" />
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenderInquirySearch.TenderDes']}:" />
															</td>
															<td width="30%">
																<h:inputText styleClass="A_LEFT INPUT_TEXT" 
																	id="txtTenderDescription"
																	value="#{pages$tenderInquirySearch.tenderDescription}" />
															</td>
														</tr>

														<tr>
															<td colspan="6" class="BUTTON_TD">
																<h:commandButton type="submit" styleClass="BUTTON"
																	value="#{msg['commons.search']}"
																	action="#{pages$tenderInquirySearch.searchTenders}" />
																<h:commandButton styleClass="BUTTON" type="button"
																	value="#{msg['commons.reset']}"
																	onclick="javascript:clearWindow();" />	
																<h:commandButton type="submit" styleClass="BUTTON"
																	value="#{msg['tenderInquiry.btn.addInquiry']}"
																	action="#{pages$tenderInquirySearch.btnAddInquiry_Click}" />	
															</td>
														</tr>
													</table>
												</div>
											</div>

											<div style="padding: 5px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" style="width:99%">
													<t:dataTable id="dtContractors" rows="#{pages$tenderInquirySearch.paginatorRows}" width="100%"
														value="#{pages$tenderInquirySearch.tenderInquiryViewList}"
														binding="#{pages$tenderInquirySearch.dataTable}"
														preserveDataModel="false" preserveSort="false"
														var="TenderInquiry" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true"
														columnClasses="A_LEFT_GRID,A_LEFT_GRID,A_LEFT_GRID,A_LEFT_GRID,A_LEFT_GRID">
                                                        
                                                        <t:column id="col8" style="width:5%" rendered="false" >
				                                            <f:facet name="header">
				                                            <t:outputText value="#{msg['commons.select']}" />
				                                            </f:facet>
															<h:selectBooleanCheckbox id="select"
																value="#{TenderInquiry.selected}" />
													    </t:column>
														<t:column id="col1" defaultSorted="true" sortable="true" style="width:10%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['tenderInquirySearch.inquiryNumber']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{TenderInquiry.requestNumber}" />
														</t:column>
														
														<t:column id="col2" sortable="true" style="width:10%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['tenderInquirySearch.inquiryDate']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{TenderInquiry.requestDate}" >
																<f:convertDateTime pattern="#{pages$tenderInquirySearch.dateFormat}" timeZone="#{pages$tenderInquirySearch.timeZone}" />
																</t:outputText>
														</t:column>
														
														<t:column id="col3" sortable="true" style="width:10%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['tenderInquirySearch.sourceType']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{TenderInquiry.requestSourceEn}"
																rendered="#{pages$tenderInquirySearch.isEnglishLocale}" />
															<t:outputText styleClass="A_LEFT"
																value="#{TenderInquiry.requestSourceAr}"
																rendered="#{!pages$tenderInquirySearch.isEnglishLocale}" />
														</t:column>
                                                         
                                                         <t:column id="col4" sortable="true" style="width:15%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['ContractorSearch.contractorNumber']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{TenderInquiry.contractorNumber}" />
														</t:column>
														
														<t:column id="col5" sortable="true" style="width:15%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['ContractorSearch.contractorName']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{TenderInquiry.contractorNameEn}"
																rendered="#{pages$tenderInquirySearch.isEnglishLocale}" />
															<t:outputText styleClass="A_LEFT"
																value="#{TenderInquiry.contractorNameAr}"
																rendered="#{!pages$tenderInquirySearch.isEnglishLocale}" />
														</t:column>
                                                          <t:column id="col6" sortable="true" style="width:10%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['tenderInquirySearch.TenderNo']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{TenderInquiry.tenderNumber}" />
														</t:column>
														
														<t:column id="col7" sortable="true" style="width:10%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['tenderInquirySearch.inquiryStatus']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{TenderInquiry.statusEn}"
																rendered="#{pages$tenderInquirySearch.isEnglishLocale}" />
															<t:outputText styleClass="A_LEFT"
																value="#{TenderInquiry.statusAr}"
																rendered="#{!pages$tenderInquirySearch.isEnglishLocale}" />
														</t:column>
														<t:column id="colAction" sortable="false" style="width:15%">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>
															
															<h:commandLink 
															    rendered="#{TenderInquiry.showEdit}" 
																action="#{pages$tenderInquirySearch.btnManage_Click}">
																<h:graphicImage style="margin-left:5px;"
																	title="#{msg['commons.Manage']}"
																	url="../resources/images/detail-icon.gif" />
															</h:commandLink>
                                                             
                                                             <h:commandLink
                                                                rendered="#{TenderInquiry.showEdit}"
																action="#{pages$tenderInquirySearch.btnDeleteInquiry_Click}">
																<h:graphicImage style="margin-left:5px;"
																  title="#{msg['commons.delete']}"
																  url="../resources/images/delete.gif" />
															</h:commandLink>
															
															
															<h:commandLink
																action="#{pages$tenderInquirySearch.btnViewInquiry_Click}">
																<h:graphicImage style="margin-left:5px;"
																  title="#{msg['commons.view']}"
																  url="../resources/images/app_icons/Contrect.png" />
															</h:commandLink>
														</t:column>
																
											 </t:dataTable>
										</div>

												<div class="contentDivFooter" style="width: 100%">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{pages$tenderInquirySearch.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">
																<CENTER>
																	<t:dataScroller id="scroller" for="dtContractors"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$tenderInquirySearch.paginatorMaxPages}"
																		immediate="false" paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		pageIndexVar="pageNumber" styleClass="SCH_SCROLLER"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblF" />
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR" />
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF" />
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL" />
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.pageNumber}" />
																					</td>
																				</tr>
																			</table>
																		</t:div>
																	</t:dataScroller>
																</center>
															</td>
														</tr>
													</table>
												</div>
											</div>
										 <table>
											<tr>
															<td colspan="6" class="BUTTON_TD">
												
																<h:commandButton type="submit" styleClass="BUTTON"
																    rendered="false"
																	value="#{msg['tenderInquiry.btn.sendReply']}"
																	action="#{pages$tenderInquirySearch.replayAll}" />	
															</td>
											</tr>
										</table>	
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		  </div>
		</body>
	</html>
</f:view>