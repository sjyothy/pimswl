

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />


	<html style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

			<script language="javascript" type="text/javascript">
		
		
		
		
		function clearWindow()
	{
	       
            document.getElementById("formVenue:txtVenueName").value="";
		    document.getElementById("formVenue:txtaddress1").value="";
		    document.getElementById("formVenue:txtaddress2").value="";
            document.getElementById("formVenue:txtemail").value="";
            document.getElementById("formVenue:txtfax").value="";
            document.getElementById("formVenue:txtofficePhone").value="";
            document.getElementById("formVenue:txthomePhone").value="";
            document.getElementById("formVenue:txtpostCode").value="";
            document.getElementById("formVenue:txtstreet").value="";
		    
		    var cmbcity=document.getElementById("formVenue:selectcity");
		    cmbcity.selectedIndex = 0;
		    
		    var cmbstate=document.getElementById("formVenue:selectstate");
		    cmbstate.selectedIndex = 0;
		    
		    var cmbcountry=document.getElementById("formVenue:selectcountry");
		    cmbcountry.selectedIndex = 0; 
		    
        	    
		    }
		    
		       function submitForm()
	   {
          document.getElementById('formVenue').submit();
          document.getElementById("formVenue:country").selectedIndex=0;
          document.getElementById("formVenue:state").selectedIndex=0;
	   }
		    
		    function submitFormState(){
       
		        
		    var cmbState=document.getElementById("formVenue:selectstate");
		    var j = 0;
		    j=cmbState.selectedIndex;
		    document.getElementById("formVenue:hdnCityValue").value=cmbState.options[j].value ;
		    document.forms[0].submit();
		   
		    
		    }
		
    </script>
		</head>

		<body dir="${sessionScope.CurrentLocale.dir}"
			lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">
			<div class="containerDiv">

			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" class="divBackgroundBody" height="470px"
						style="VERTICAL-ALIGN: top">
						<table cellpadding="0" width="100%" cellspacing="0" height="100%">
							<tr height="1%">
								<td>
									<table width="99.3%" class="greyPanelTable" cellpadding="0"
										cellspacing="0" border="0">
										<tr>
											<td class="HEADER_TD">
												<h:outputLabel value="#{msg['auctionVenue.header']}"
													styleClass="HEADER_FONT" />
											</td>
											<td width="100%">
												&nbsp;
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr height="99%" valign="top">
								<td>
									<table width="99.2%" class="greyPanelMiddleTable" cellpadding="0"
										cellspacing="0" border="0" height="100%" style="height: 98%;">
										<tr valign="top">
											<td height="100%" valign="top" nowrap="nowrap"
												background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
												width="1">
											</td>

											<td width="100%" height="460px" valign="top" nowrap="nowrap">
												<div>
													<h:form id="formVenue">

														<table border="0" class="layoutTable">

															<tr>
																<td colspan="6">

																	<h:outputText value="#{pages$AddAuctionVenue.errorMessages}" escape="false" styleClass="ERROR_FONT" style="padding:10px;"/>
											                        <h:outputText value="#{pages$AddAuctionVenue.successMessages}" escape="false" styleClass="INFO_FONT" style="padding:10px;"/>
																	<h:inputHidden id="hdnVenueId"
																		value="#{pages$AddAuctionVenue.venueId}" />	
																	<h:inputHidden id="hdnContactInfoId"
																		value="#{pages$AddAuctionVenue.contactInfoId}" />
																	<h:inputHidden id="hdnpageMode"
																		value="#{pages$AddAuctionVenue.pageMode}" />
																</td>
															</tr>
														</table>

														<div class="MARGIN">
															<table cellpadding="0" cellspacing="0" width="100%">
																<tr>
																<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
																<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
																<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
																</tr>
															</table>
															<div class="DETAIL_SECTION">
																<h:outputLabel
																	value="#{msg['auctionVenue.venueInformation']}"
																	styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
																<table cellpadding="1px" cellspacing="2px"
																	class="DETAIL_SECTION_INNER">
																	<tr>
																		<td>
																			<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel
																				value="#{msg['auctionVenue.venueName']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText styleClass="A_LEFT" style="width:160px"
																				maxlength="50" id="txtVenueName"
																				value="#{pages$AddAuctionVenue.venueName}">
																			</h:inputText>
																		</td>
																	</tr>

																	<tr>
																		<td>
																			<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['contact.address1']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText styleClass="A_LEFT" style="width:160px"
																				maxlength="150" id="txtaddress1"
																				value="#{pages$AddAuctionVenue.address1}">

																			</h:inputText>
																		</td>
																		<td>
																			<h:outputLabel value="#{msg['contact.address2']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText styleClass="A_LEFT" style="width:160px"
																				maxlength="150" id="txtaddress2"
																				value="#{pages$AddAuctionVenue.address2}">

																			</h:inputText>
																		</td>
																		
																	</tr>

																	<tr>
																	  <td>
																			<h:outputLabel value="#{msg['contact.street']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText styleClass="A_LEFT" style="width:160px"
																				maxlength="150" id="txtstreet"
																				value="#{pages$AddAuctionVenue.street}">

																			</h:inputText>
																		</td>
																	
																		<td>
																			<h:outputLabel value="#{msg['contact.postcode']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText styleClass="A_RIGHT_NUM"
																				style="width:160px" maxlength="10" id="txtpostCode"
																				value="#{pages$AddAuctionVenue.postCode}">

																			</h:inputText>
																		</td>
							
							


																	</tr>

																	<tr>
																												<td >
																		<h:outputLabel value="#{msg['contact.country']}:"></h:outputLabel>
																		</td>
																		<td >
																		<h:selectOneMenu id="country" tabindex="2" style="width:165px" 
																		value="#{pages$AddAuctionVenue.selectOneCountry}" 
																		onchange="javascript:submitForm();" 
																		valueChangeListener="#{pages$AddAuctionVenue.loadStates}" >
																		<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																		<f:selectItems value="#{pages$AddAuctionVenue.countries}" />
																		</h:selectOneMenu>
																		</td>
													
																		<td >
																		<h:outputLabel value="#{msg['contact.state']}:"></h:outputLabel>
																		</td>
																		<td >
																		<h:selectOneMenu id="state"  tabindex="5" style="width:165px" 
																		value="#{pages$AddAuctionVenue.selectOneState}" 
																		onchange="javascript:submitForm();" 
																		valueChangeListener="#{pages$AddAuctionVenue.loadCities}">
																		<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																		<f:selectItems value="#{pages$AddAuctionVenue.states}" />
																		</h:selectOneMenu>
																		</td>

																	
																	
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel value="#{msg['contact.city']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="selectcity" style="width:165px"
																				value="#{pages$AddAuctionVenue.selectOneCity}">
																			<f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1" />
																			<f:selectItems value="#{pages$AddAuctionVenue.cities}" />
																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:outputLabel value="#{msg['contact.homephone']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText styleClass="A_RIGHT_NUM"
																				style="width:160px" maxlength="15" id="txthomePhone"
																				value="#{pages$AddAuctionVenue.homePhone}">

																			</h:inputText>
																		</td>
																	</tr>
																	
																	<tr>
																	<td>
																			<h:outputLabel value="#{msg['contact.officephone']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText styleClass="A_RIGHT_NUM"
																				style="width:160px" maxlength="15" id="txtofficePhone"
																				value="#{pages$AddAuctionVenue.officePhone}">

																			</h:inputText>
																		</td>
																	
																		<td>
																			<h:outputLabel value="#{msg['contact.fax']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText styleClass="A_RIGHT_NUM"
																				style="width:160px" maxlength="15" id="txtfax"
																				value="#{pages$AddAuctionVenue.fax}">

																			</h:inputText>
																		</td>
																																			
																	</tr>
																	<tr>
																	<td>
																			<h:outputLabel value="#{msg['contact.email']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText styleClass="A_LEFT" style="width:160px"
																				maxlength="50" id="txtemail"
																				value="#{pages$AddAuctionVenue.email}">

																			</h:inputText>
																		</td>
																	</tr>
																	

																	<h:inputHidden id="hdnStateValue"
																		binding="#{pages$AddAuctionVenue.hiddenStateValue}" />
																	<h:inputHidden id="hdnCityValue"
																		binding="#{pages$AddAuctionVenue.hiddenCityValue}" />



																</table>
															</div>
														</div>
														<table width="100%" border="0">
															<tr>

																<td colspan="6" class="BUTTON_TD">
                                                              <pims:security screen="Pims.AuctionManagement.AuctionVenue.AddAuctionVenue" action="create">
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.saveButton']}" type="submit"
																		action="#{pages$AddAuctionVenue.btnAdd_Click}">
																	</h:commandButton>
															  </pims:security>	
																	<h:commandButton styleClass="BUTTON" type="button"
																		onclick="javascript:clearWindow();"
																		value="#{msg['commons.reset']}">
																	</h:commandButton>
																	<h:commandButton styleClass="BUTTON" type="submit"
																		value="#{msg['commons.cancel']}"
																		action="#{pages$AddAuctionVenue.btnBack_Click}">
																	</h:commandButton>

																</td>
															</tr>
														</table>




													</h:form>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>

						</table>
					</td>
				</tr>
			</table>
			<tr>
				<td colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="footer">
								<h:outputLabel value="#{msg['commons.footer.message']}" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</div>
		</body>
</f:view>

</html>
