
<t:div>
	<t:panelGrid id="ddTab" cellpadding="5px" width="100%"
		rendered="#{
						!(	pages$unBlockingRequest.statusApproved || 
							pages$unBlockingRequest.statusCompleted
							)  
						  }"
		cellspacing="10px" columns="4"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="ReasonField" styleClass="LABEL"
				value="#{msg['unblocking.lbl.unblockReason']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="selectOneReason"
			value="#{pages$unBlockingRequest.selectOneReason}">

			<f:selectItems value="#{pages$ApplicationBean.blockingReasons}" />
		</h:selectOneMenu>

		<h:commandButton styleClass="BUTTON" value="#{msg['commons.apply']}"
			action="#{pages$unBlockingRequest.onAddUnblockingReason}" />

	</t:panelGrid>

</t:div>

<t:div id="divDetails" styleClass="contentDiv"
	style="width:98%;margin-top: 5px;">

	<t:dataTable id="dataTableUnblocking" rows="15" width="100%"
		value="#{pages$unBlockingRequest.requestDetailList}"
		binding="#{pages$unBlockingRequest.dataTable}"
		preserveDataModel="false" preserveSort="false" var="dataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

		<t:column id="isSelected" sortable="true" width="2%">
			<f:facet name="header">

			</f:facet>
			<h:selectBooleanCheckbox id="selectdUnblocking"
				rendered="#{
							!( pages$unBlockingRequest.statusApproved || 
								pages$unBlockingRequest.statusCompleted
							)  
						  }"
				value="#{dataItem.selected}">
			</h:selectBooleanCheckbox>
		</t:column>


		<t:column id="isComplete" sortable="true">
			<f:facet name="header">
				<t:outputText id="isCompleteHeading"
					value="#{msg['UnBlockingRequest.lbl.isComplete']}" />
			</f:facet>
			<h:selectBooleanCheckbox id="selectUnblocking"
				disabled="#{
							pages$unBlockingRequest.statusApproved || 
							pages$unBlockingRequest.statusCompleted  
						  }"
				onclick="onSelectionChanged(this);"
				value="#{dataItem.completeBlocking}">

			</h:selectBooleanCheckbox>
			<h:commandLink id="lnkUnitSelectionChanged"
				action="#{pages$unBlockingRequest.onCompletedChange}" />
		</t:column>
		<t:column id="refNumcolUnblocking" sortable="true">
			<f:facet name="header">
				<t:outputText id="refNumHeadingUnblocking"
					value="#{msg['commons.refNum']}" />
			</f:facet>
			<t:outputText id="refNumUnblocking" styleClass="A_LEFT"
				value="#{dataItem.oldBlockingDetails.refNum}" />
		</t:column>

		<t:column id="fullNamecolUnblocking" sortable="true">
			<f:facet name="header">
				<t:outputText id="fullNameHeadingUnblocking"
					value="#{msg['commons.Name']}" />
			</f:facet>
			<t:outputText id="fullNameUnblocking" styleClass="A_LEFT"
				value="#{dataItem.oldBlockingDetails.person.fullName}" />
		</t:column>

		<t:column id="statusColUnblocking" sortable="true">
			<f:facet name="header">
				<t:outputText id="statusAmntUnblocking"
					value="#{msg['commons.status']}" />
			</f:facet>
			<t:outputText id="statusUnblocking" styleClass="A_LEFT"
				value="#{pages$unBlockingRequest.englishLocale? dataItem.oldBlockingDetails.status.dataDescEn:dataItem.oldBlockingDetails.status.dataDescAr}" />
		</t:column>
		<t:column id="REASONCol" sortable="true">
			<f:facet name="header">
				<t:outputText id="REASONOTT" value="#{msg['commons.reason']}" />
			</f:facet>
			<t:outputText id="REASONTT" styleClass="A_LEFT"
				value="#{pages$unBlockingRequest.englishLocale? dataItem.oldBlockingDetails.reason.reasonEn:dataItem.oldBlockingDetails.reason.reasonAr}" />
		</t:column>
		<t:column id="UNREASONCol" sortable="true">
			<f:facet name="header">
				<t:outputText id="UNREASONOTT"
					value="#{msg['unblocking.lbl.unblockReason']}" />
			</f:facet>
			<t:outputText id="UNREASONTT" styleClass="A_LEFT"
				value="#{pages$unBlockingRequest.englishLocale? dataItem.oldBlockingDetails.unblockReason.reasonEn:dataItem.oldBlockingDetails.unblockReason.reasonAr}" />
		</t:column>
		<t:column id="desc" sortable="true" style="white-space: normal;">
			<f:facet name="header">
				<t:outputText id="hdTo" value="#{msg['commons.description']}" />
			</f:facet>
			<div style="width: 70%; white-space: normal;">
				<t:outputText id="p_w" styleClass="A_LEFT"
					style="white-space: normal;"
					value="#{dataItem.oldBlockingDetails.description}" />
			</div>
		</t:column>
		<t:column id="amntCol" sortable="true">
			<f:facet name="header">
				<t:outputText id="hdAmnt" value="#{msg['blocking.lbl.amount']}" />
			</f:facet>
			<t:outputText id="amountt" styleClass="A_LEFT"
				value="#{dataItem.oldBlockingDetails.amountStr}" />
		</t:column>
		<t:column id="unblockamntCol" sortable="true"
			rendered="#{
								pages$unBlockingRequest.statusApproved || 
								pages$unBlockingRequest.statusCompleted ||
								pages$unBlockingRequest.statusReviewRequired  
							  }">
			<f:facet name="header">
				<t:outputText id="unblockhdAmnt"
					value="#{msg['UnBlockingRequest.lbl.unblockAmount']}" />
			</f:facet>
			<t:outputText id="unblocamountt" styleClass="A_LEFT"
				value="#{dataItem.annualContractRentString}" />

		</t:column>
		<t:column id="unblockamntColtxtinput" sortable="true"
			rendered="#{     
								!pages$unBlockingRequest.statusApproved &&
								!pages$unBlockingRequest.statusCompleted &&
								!pages$unBlockingRequest.statusReviewRequired  
				      }">
			<f:facet name="header">
				<t:outputText id="unblockhdAmnttxtinput"
					value="#{msg['UnBlockingRequest.lbl.unblockAmount']}" />
			</f:facet>
			<t:inputText id="unblocamountttxtinput" maxlength="14"
				styleClass="#{ dataItem.completeBlocking ?'READONLY':'A_LEFT'}"
				style="width:90%;" readonly="#{ dataItem.completeBlocking }"
				value="#{dataItem.annualContractRentString}"
				onkeyup="removeNonNumeric(this)" onkeypress="removeNonNumeric(this)"
				onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
				onchange="removeNonNumeric(this)" />

		</t:column>
		<t:column id="actionUnblocking" sortable="true">
			<f:facet name="header">
				<t:outputText id="hdActionUnblocking"
					value="#{msg['commons.action']}" />
			</f:facet>

			<h:commandLink id="lnkDeleteUnblocking"
				onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableInputs();"
				rendered="#{pages$unBlockingRequest.showSaveButton || pages$unBlockingRequest.showApproveButton}"
				action="#{pages$unBlockingRequest.onDelete}">
				<h:graphicImage id="deleteIconUnblocking"
					title="#{msg['commons.delete']}"
					url="../resources/images/delete.gif" width="18px;" />
			</h:commandLink>
		</t:column>
	</t:dataTable>
</t:div>


