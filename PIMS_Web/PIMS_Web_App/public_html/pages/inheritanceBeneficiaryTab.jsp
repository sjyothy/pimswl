<%-- Author: Kamran Ahmed--%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<t:div style="height:200px;overflow-y:scroll;width:100%;">

	<t:panelGrid border="0" width="95%" cellpadding="1" cellspacing="1"
		rendered="#{pages$inheritanceFile.showBeneDetailsInputs}"
		id="inheritanceBeneficiaryTabControls">
		<t:panelGroup>
			<%--Column 1,2 Starts--%>
			<t:panelGrid columns="4"
				columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.beneficiary']}" />
				</h:panelGroup>
				<h:panelGroup>
					<h:inputText readonly="true"
						value="#{pages$inheritanceBeneficiaryTab.beneficiary.personFullName}" />
					<h:graphicImage title="#{msg['commons.search']}"
						rendered="#{pages$inheritanceFile.showBeneficiarySearch}"
						style="MARGIN: 1px 1px -5px;cursor:hand"
						onclick="beneficiaryLinkClick();"
						url="../resources/images/app_icons/Search-tenant.png">
					</h:graphicImage>
					<h:commandLink id="lnkBeneficiary"
						action="#{pages$inheritanceBeneficiaryTab.showBeneficiaryPopup}"></h:commandLink>
				</h:panelGroup>
				
				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.passportName']}" />
				</h:panelGroup>
				<h:inputText maxlength="100"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.passportName}" />

				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.relationship']}" />
				</h:panelGroup>
				<h:selectOneMenu id="relationshipIdBeneficiary"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					styleClass="SELECT_MENU"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.relationshipString}">
					<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
						itemValue="-1" />
					<f:selectItems value="#{pages$ApplicationBean.relationList}" />
				</h:selectOneMenu>

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.relationshipDesc']}" />
				<h:inputText
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.relationshipDesc}"
					maxlength="60"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}" />


				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.gender']}" />
				</h:panelGroup>
				<h:selectOneMenu id="selectGenderBeneficiary"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					styleClass="SELECT_MENU"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.gender}">
					<f:selectItem itemLabel="#{msg['tenants.gender.male']}"
						itemValue="M" />
					<f:selectItem itemLabel="#{msg['tenants.gender.female']}"
						itemValue="F" />
				</h:selectOneMenu>

				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.nationality']}" />
				</h:panelGroup>
				<h:selectOneMenu id="nationalityBeneficiary" immediate="false"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.nationalityIdString}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems value="#{pages$ApplicationBean.countryList}" />
				</h:selectOneMenu>


				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.nationalId']}" />
				<h:inputText maxlength="15"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.personalSecCardNo}" />

				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.birthDate']}" />
				</h:panelGroup>
				<rich:calendar
					disabled="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.dateOfBirth}"
					locale="#{pages$inheritanceFileBasicInfoTab.locale}" popup="true"
					datePattern="#{pages$inheritanceFileBasicInfoTab.dateFormat}"
					showApplyButton="false" enableManualInput="false" />

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.birthPlace']}" />
				<h:selectOneMenu id="birthPlaceIdBeneficiary" immediate="false"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.birthPlaceId}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems value="#{pages$ApplicationBean.countryList}" />
				</h:selectOneMenu>

				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.resCountry']}" />
				</h:panelGroup>
				<h:selectOneMenu id="resCountryIdBeneficiary"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.countryId}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems value="#{pages$ApplicationBean.countryList}" />
					<a4j:support id="loadResidenceCity" event="onchange"
						reRender="resCityIdBeneficiary"
						action="#{pages$inheritanceBeneficiaryTab.countryChanged}" />
				</h:selectOneMenu>

				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.resCity']}" />
				</h:panelGroup>
				<h:selectOneMenu id="resCityIdBeneficiary" immediate="false"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.cityId}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems value="#{pages$inheritanceBeneficiaryTab.cityList}" />
				</h:selectOneMenu>
				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.address']}" />
				</h:panelGroup>
				<h:inputText maxlength="150"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.address1}" />


				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.contactNumber']}" />
				<h:inputText maxlength="15"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.homePhone}" />

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.poBox']}" />
				<h:inputText maxlength="20"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.postCode}" />



				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.email']}" />
				<h:inputText maxlength="50"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.email}" />

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.motherTongue']}" />
				<h:inputText maxlength="30"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.motherTongue}" />

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.passportNumber']}" />
				<h:inputText maxlength="100"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.passportNumber}" />

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.passportPlace']}" />
				<h:selectOneMenu id="passportPlaceIdBeneficiary" immediate="false"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.passportIssuePlaceIdString}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems value="#{pages$ApplicationBean.countryList}" />

					<a4j:support event="onchange" reRender="benefpassportPlaceCity"
						action="#{pages$inheritanceBeneficiaryTab.passportCountryChanged}" />
				</h:selectOneMenu>
				<h:outputText value="#{msg['person.passportIssueCity']}" />
				<h:selectOneMenu id="benefpassportPlaceCity" immediate="false"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.passportIssueCityString}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems
						value="#{pages$inheritanceBeneficiaryTab.benenficiaryPassportCityList}" />
				</h:selectOneMenu>


				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.maritalStatus']}" />
				<h:selectOneMenu id="cboMaritialStatusBeneficiary"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.maritialStatusIdString}">
					<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
						itemValue="-1" />
					<f:selectItems value="#{pages$ApplicationBean.maritialStatus}" />
				</h:selectOneMenu>

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.jobDescription']}" />
				<h:inputText maxlength="50"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.jobDescription}" />

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.placeOfWork']}" />
				<h:inputText maxlength="50"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.workingCompany}" />
				<%--New Fields --%>
				<h:outputText
					value="#{msg['financialAccConfiguration.bankRemitanceAccountName']}" />
				<h:inputText maxlength="23"
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.accountNumber}" />

				<h:outputText value="#{msg['grp.BankName']}" />
				<h:selectOneMenu
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.bankId}">
					<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
						itemValue="-1" />
					<f:selectItems value="#{pages$ApplicationBean.bankList}" />
				</h:selectOneMenu>

				<h:outputText value="#{msg['label.familyNumber']}" />
				<h:inputText
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.familyNumber}" />

				<h:outputText
					value="#{msg['migrationProject.fieldName.townNumber']}" />
				<h:inputText
					readonly="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.townNumber}" />

				<h:outputText value="#{msg['sessionDetailsTab.maturityDate']}" />
				<rich:calendar
					disabled="#{pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.maturityDate}"
					locale="#{pages$inheritanceBeneficiaryTab.locale}" popup="true"
					datePattern="#{pages$inheritanceBeneficiaryTab.dateFormat}"
					showApplyButton="false" enableManualInput="false" />
				<%--New Fields --%>

				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['inheritanceFile.label.beneficiaryShareValue']}" />
				</h:panelGroup>
				<h:inputText maxlength="50" onkeyup="removeNonInteger(this)"
					onkeypress="removeNonInteger(this)"
					onkeydown="removeNonInteger(this)" onblur="removeNonInteger(this)"
					onchange="removeNonInteger(this)"
					readonly="#{pages$inheritanceBeneficiaryTab.pageModeView ||
							pages$inheritanceBeneficiaryTab.readonlyBeneficiay}"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.sharePercentage}" />


				<h:outputLabel
					value="#{msg['inheritanceFile.fieldLabel.costCenter']} :"
					styleClass="LABEL" />
				<h:inputText id="beneficiaryOldCostCenter"
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.costCenter}" />



				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.isStudent']}" />
				<h:selectBooleanCheckbox
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.student}"
					readonly="true"></h:selectBooleanCheckbox>

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.isApplicant']}" />

				<h:selectBooleanCheckbox
					value="#{pages$inheritanceBeneficiaryTab.beneficiary.applicant}"></h:selectBooleanCheckbox>

			</t:panelGrid>
		</t:panelGroup>
		<%--Column 3,4 Ends--%>
	</t:panelGrid>
	<%--</t:collapsiblePanel> --%>
	<t:div rendered="#{pages$inheritanceFile.showBeneDetailsInputs}"
		style="width:95%;background-color:#CCCCCC; height:1px; cell-padding:0px; cell-spacing:0px"></t:div>
	<t:panelGrid border="0" width="95%" cellpadding="1" cellspacing="1"
		rendered="#{pages$inheritanceFile.showBeneDetailsInputs}"
		id="inheritanceBeneficiaryTabControls2">
		<t:panelGroup>
			<t:panelGrid columns="4"
				columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.nurse']}" />
				<h:panelGroup>
					<h:inputText readonly="true"
						value="#{pages$inheritanceBeneficiaryTab.nurse.personFullName}" />
					<h:graphicImage title="#{msg['commons.search']}"
						rendered="#{pages$inheritanceFile.showNurseSearch}"
						style="MARGIN: 1px 1px -5px;cursor:hand"
						onclick="showNursePopup();"
						url="../resources/images/app_icons/Search-tenant.png">
					</h:graphicImage>
					<t:commandLink
						rendered="#{!empty pages$inheritanceBeneficiaryTab.nurse.personId}"
						onclick="if (!confirm('#{msg['migrationProject.confirmMsg.sureToDelete']}')) return false;"
						action="#{pages$inheritanceBeneficiaryTab.deleteNurseFromBeneDetail}">
						<h:graphicImage id="deleteNurse" title="#{msg['commons.delete']}"
							style="cursor:hand;" url="../resources/images/delete.gif" />&nbsp;
				</t:commandLink>
				</h:panelGroup>

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.relationship']}" />
				<h:selectOneMenu id="relationshipIdNurse" styleClass="SELECT_MENU"
					readonly="#{pages$inheritanceBeneficiaryTab.pageModeView}"
					value="#{pages$inheritanceBeneficiaryTab.nurse.relationshipString}">
					<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
						itemValue="-1" />
					<f:selectItems value="#{pages$ApplicationBean.relationList}" />
				</h:selectOneMenu>

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.relationshipDesc']}" />
				<h:inputText
					value="#{pages$inheritanceBeneficiaryTab.nurse.relationshipDesc}" />
				<h:outputLabel></h:outputLabel>
				<h:outputLabel></h:outputLabel>
				<%-- Remove this section if you want to put the nurse and guradian detail in separat block and uncomment the CODE_GUARDIAN --%>

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.guardian']}" />
				<h:panelGroup>
					<h:inputText readonly="true"
						value="#{pages$inheritanceBeneficiaryTab.guardian.personFullName}" />
					<h:graphicImage title="#{msg['commons.search']}"
						rendered="#{pages$inheritanceFile.showGuardianSearch}"
						style="MARGIN: 1px 1px -5px;cursor:hand"
						onclick="showGuardianPopup();"
						url="../resources/images/app_icons/Search-tenant.png">
					</h:graphicImage>
					<t:commandLink
						rendered="#{!empty pages$inheritanceBeneficiaryTab.guardian.personId}"
						onclick="if (!confirm('#{msg['migrationProject.confirmMsg.sureToDelete']}')) return false;"
						action="#{pages$inheritanceBeneficiaryTab.deleteGuardianFromBeneDetail}">
						<h:graphicImage id="deleteGuardian"
							title="#{msg['commons.delete']}" style="cursor:hand;"
							url="../resources/images/delete.gif" />&nbsp;
				</t:commandLink>
				</h:panelGroup>

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.relationship']}" />
				<h:selectOneMenu id="relationshipIdGuardian"
					styleClass="SELECT_MENU"
					readonly="#{pages$inheritanceBeneficiaryTab.pageModeView}"
					value="#{pages$inheritanceBeneficiaryTab.guardian.relationshipString}">
					<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
						itemValue="-1" />
					<f:selectItems value="#{pages$ApplicationBean.relationList}" />
				</h:selectOneMenu>

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.relationshipDesc']}" />
				<h:inputText
					value="#{pages$inheritanceBeneficiaryTab.guardian.relationshipDesc}" />

			</t:panelGrid>
		</t:panelGroup>
	</t:panelGrid>

	<t:div style="height:5px;"></t:div>
	<t:div style="width:95%;" id="beneficiaryDetailsDiv">
		<t:div styleClass="A_RIGHT">
			<h:commandButton id="btnAdd"
				rendered="#{
							pages$inheritanceFile.showAddBeneficiaryButton && 
							!(pages$inheritanceFile.viewModePopUp || pages$inheritanceFile.pageModeView)
						   }"
				value="#{msg['mems.inheritanceFile.buttonLabel.add']}"
				onclick="javaScript:onProcessStart();"
				action="#{pages$inheritanceBeneficiaryTab.addBeneficiaryDetails}"
				styleClass="BUTTON" style="width:10%;"></h:commandButton>
			<%-- 
			<h:outputText
				value="#{pages$inheritanceBeneficiaryTab.errorMessages}"
				escape="false" styleClass="ERROR_FONT" />
			--%>
		</t:div>
		<t:div style="height:5px;"></t:div>
		<t:div styleClass="contentDiv" style="width:95%">
			<t:dataTable id="beneficiaryDetailsGrid" rows="200"
				value="#{pages$inheritanceBeneficiaryTab.beneficiaryDetailsList}"
				binding="#{pages$inheritanceBeneficiaryTab.beneficiaryDetailsDataTable}"
				preserveDataModel="false" preserveSort="false" var="dataItem"
				rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
				width="100%">
				<t:column id="beneficiaryCol" sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['mems.inheritanceFile.columnLabel.beneficiary']}" />
					</f:facet>
					<t:commandLink
						actionListener="#{pages$inheritanceBeneficiaryTab.openManageBeneficiaryPopUp}"
						value="#{dataItem.beneficiaryName}"
						rendered="#{!dataItem.deleted}" style="white-space: normal;"
						styleClass="A_LEFT" />
				</t:column>
				<t:column id="relationshipCol" sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['mems.inheritanceFile.columnLabel.relationship']}" />
					</f:facet>
					<h:outputText value="#{dataItem.relationshipString}"
						rendered="#{!dataItem.deleted}" />
				</t:column>
				<t:column id="isMinorCol" sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['mems.inheritanceFile.columnLabel.isMinor']}" />
					</f:facet>
					<h:outputText value="#{dataItem.minorString}"
						rendered="#{!dataItem.deleted}" />
				</t:column>
				<t:column id="matdate" sortable="true">
					<f:facet name="header">
						<h:outputText value="#{msg['sessionDetailsTab.maturityDate']}" />
					</f:facet>
					<h:outputText value="#{dataItem.beneficiary.maturityDate}"
						rendered="#{!dataItem.deleted}">
						<f:convertDateTime
							pattern="#{pages$inheritanceBeneficiaryTab.dateFormat}"
							timeZone="#{pages$inheritanceBeneficiaryTab.timeZone}" />
					</h:outputText>
				</t:column>

				<t:column id="expectedMaturityDate" sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['mems.inheritanceFile.fieldLabel.expectedMaturityDate']}" />
					</f:facet>
					<h:outputText value="#{dataItem.beneficiary.expMaturityDate}"
						rendered="#{!dataItem.deleted}">
						<f:convertDateTime
							pattern="#{pages$inheritanceBeneficiaryTab.dateFormat}"
							timeZone="#{pages$inheritanceBeneficiaryTab.timeZone}" />
					</h:outputText>
				</t:column>

				<t:column id="guardianCol" sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['mems.inheritanceFile.columnLabel.guardian']}" />
					</f:facet>
					<h:outputText value="#{dataItem.guardianName}"
						rendered="#{!dataItem.deleted}" />
				</t:column>

				<t:column id="nurseCol" sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['mems.inheritanceFile.columnLabel.nurse']}" />
					</f:facet>
					<h:outputText value="#{dataItem.nurseName}"
						rendered="#{!dataItem.deleted}" />
				</t:column>

				<t:column id="shareCol" sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['mems.inheritanceFile.columnLabel.share']}" />
					</f:facet>
					<h:outputText value="#{dataItem.sharePercentage}"
						rendered="#{!dataItem.deleted}" />
				</t:column>
				<t:column id="isApplicant" sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['mems.inheritanceFile.fieldLabel.isApplicant']}" />
					</f:facet>
					<h:outputText
						value="#{ !empty dataItem.isApplicant && dataItem.isApplicant =='1'? msg['commons.true']:
																									   msg['commons.false']
					
										  }"
						rendered="#{!dataItem.deleted}" />
				</t:column>
				<t:column id="newCCCol" sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['property.fieldLabel.oldNewCostCenter']}" />
					</f:facet>
					<h:outputText value="#{dataItem.oldNewCostCenter}"
						rendered="#{!dataItem.deleted}" />
				</t:column>

				<t:column id="actioCol" sortable="false"
					rendered="#{pages$inheritanceFile.showBeneDetailsInputs}">
					<f:facet name="header">
						<t:outputText
							value="#{msg['mems.inheritanceFile.columnLabel.action']}" />
					</f:facet>

					<t:commandLink id="beneficiaryGridEditLink"
						rendered="#{!dataItem.deleted && !dataItem.selected && pages$inheritanceFile.showAddBeneficiaryButton}"
						action="#{pages$inheritanceBeneficiaryTab.editBeneficiaryDetails}">
						<h:graphicImage id="edit" title="#{msg['commons.edit']}"
							style="cursor:hand;" url="../resources/images/edit-icon.gif" />&nbsp;
				</t:commandLink>

					<t:commandLink
						rendered="#{!dataItem.deleted && (dataItem.inheritanceBeneficiaryId ne null)}"
						action="#{pages$inheritanceBeneficiaryTab.openResearcherForm}">
						<h:graphicImage id="researchForm"
							title="#{msg['beneficiaryDetails.tooltip.openResearchForm']}"
							style="cursor:hand;" url="../resources/images/detail-icon.gif" />&nbsp;
				</t:commandLink>

					<t:commandLink
						rendered="#{!dataItem.deleted && (dataItem.inheritanceBeneficiaryId ne null)}"
						action="#{pages$inheritanceBeneficiaryTab.openFinancialAspectsPopup}">
						<h:graphicImage id="financialDetails"
							title="#{msg['beneficiaryDetails.tooltip.financial']}"
							style="cursor:hand;"
							url="../resources/images/app_icons/calculator.jpg" />&nbsp;
				</t:commandLink>

					<t:commandLink
						rendered="#{
								  dataItem.inheritanceBeneficiaryId ne null   &&
								  !empty dataItem.minor && 
								  dataItem.minor ne null &&
								  !dataItem.minor
							   }"
						action="#{pages$inheritanceBeneficiaryTab.onMinorFromAdult}">
						<h:graphicImage id="imgMinorFromAdult"
							title="#{msg['common.person.MinorFromAdult']}"
							style="cursor:hand;"
							url="../resources/images/app_icons/Change-tenant.png" />&nbsp;
				</t:commandLink>

					<t:commandLink
						rendered="#{  
								  dataItem.inheritanceBeneficiaryId ne null   &&
								  !empty dataItem.minor && 
								  dataItem.minor ne null &&
								  dataItem.minor 
							   }"
						action="#{pages$inheritanceBeneficiaryTab.onAdultFromMinor}">
						<h:graphicImage id="imgAdultFromMinor"
							title="#{msg['common.person.AdultFromMinor']}"
							style="cursor:hand;"
							url="../resources/images/app_icons/Change-tenant.png" />&nbsp;
				</t:commandLink>

					<t:commandLink
						rendered="#{!dataItem.deleted && pages$inheritanceFile.showPrintStatement}"
						action="#{pages$inheritanceBeneficiaryTab.openStatmntOfAccountNew}">
						<h:graphicImage id="printReport"
							title="#{msg['report.tooltip.PrintStOfAcc']}"
							style="cursor:hand;"
							url="../resources/images/app_icons/print.gif" />&nbsp;
				</t:commandLink>

					<t:commandLink
						rendered="#{!dataItem.deleted && !dataItem.selected && pages$inheritanceFile.showAddBeneficiaryButton}"
						onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToDeleteBene']}')) return false;"
						action="#{pages$inheritanceBeneficiaryTab.deleteBeneficiaryDetails}">
						<h:graphicImage id="delete" title="#{msg['commons.delete']}"
							style="cursor:hand;" url="../resources/images/delete.gif" />&nbsp;
				</t:commandLink>

				</t:column>
			</t:dataTable>
		</t:div>
	</t:div>
</t:div>