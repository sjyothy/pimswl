
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
	<SCRIPT>	
function closeWindow()
	{
	  //window.opener.document.getElementById('conductAuctionForm:isResultPopupCalled').value = "Y";
	  window.opener.document.forms[0].submit();
	  window.close();
	}
       </SCRIPT>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
<title>PIMS</title>
			 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>	
		
<style>
		span{
			PADDING-RIGHT: 5px; 
			PADDING-LEFT: 5px;
		}
		</style>
		<script language="javascript" type="text/javascript">
	function clearWindow()
	{
     //  alert("In clear");
        
            document.getElementById("formSearchPerson:txtfirstname").value="";
			document.getElementById("formSearchPerson:txtlastname").value="";	
			document.getElementById("formSearchPerson:txtpassportNumber").value="";
			document.getElementById("formSearchPerson:txtresidenseVisaNumber").value="";
			//document.getElementById("formSearchPerson:txtpersonalSecCardNo").value="";
			//document.getElementById("formSearchPerson:txtdrivingLicenseNumber").value="";
			document.getElementById("formSearchPerson:txtsocialSecNumber").value="";
			//var cmbRequestType=document.getElementById("formSearchPerson:selectPersonType");
			//cmbRequestType.selectedIndex = 0;
			//var cmbRequestType1=document.getElementById("formSearchPerson:selectnationality");
			//cmbRequestType1.selectedIndex = 0;
	   //   cmbRequestType.selectedIndex = 0;
	  //      alert(cmbRequestType);
			
	}
	

	
</script>



	</head>
	<body dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE"> 
    <!-- Header --> 
<table width="100%" cellpadding="0" cellspacing="0"  border="0">
   

<tr width="100%">
    
    <td width="100%" valign="top" class="divBackgroundBody" height="610px">
        <table width="100%" class="greyPanelTable" cellpadding="0" cellspacing="0"  border="0">
       	<tr height="1%">
			<td>
				<table width="100%" class="greyPanelTable" cellpadding="0"
					cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
								<h:outputLabel value="#{msg['person.personList']}" styleClass="HEADER_FONT"/>
						</td>
						<td width="100%">
							&nbsp;
						</td>
					</tr>
        
        </table>
    
        <table width="100%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
          <tr valign="top">
             <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
             </td> 
	  
  <td width="100%" height="576px" valign="top" nowrap="nowrap">

								
								
								
					<div  >		
								 <h:form id="formSearchPerson" style="width:98%">
								 <h:inputHidden id="personTypeHid" value="#{pages$SearchPersonPopup.personTypeId}" />
								 <div class= "MARGIN">
											<table cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																	<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
																	<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
																	<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
																	</tr>
											</table>
				 <div class="DETAIL_SECTION">
 					<h:outputLabel value="#{msg['inquiry.searchcriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
								    <table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">

												<tr>
												    <td style="display:none"> 
														<h:outputLabel styleClass="LABEL"   value="#{msg['customer.personType']}"></h:outputLabel>
													</td>
													<td  style="display:none" >
												        <h:selectOneMenu id="selectPersonType" value=" ">
												          <f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1"/>
			                                              <f:selectItem itemLabel="#{msg['person.Mr']}" itemValue="1"/>
												          <f:selectItem itemLabel="#{msg['person.Mrs']}" itemValue="2"/>
												          <f:selectItem itemLabel="#{msg['person.Miss']}"   itemValue="3"/>
												          
												        </h:selectOneMenu>
													</td>
													
													
													
													</tr>
												
												<tr>
												
												    <td width="20%">
															<h:outputLabel styleClass="LABEL"  value="#{msg['customer.firstname']}:" ></h:outputLabel>
													</td>
													<td width="30%">
															<h:inputText style="width:85%;"  id="txtfirstname"
																 value="#{pages$SearchPerson.firstName}">
															</h:inputText>
													</td>
												
													  <td width="20%">
															<h:outputLabel styleClass="LABEL"  value="#{msg['customer.lastname']}:" ></h:outputLabel>
													</td>
													<td width="30%">
															<h:inputText style="width:85%;" id="txtlastname"
																 value="#{pages$SearchPerson.lastName}">
															</h:inputText>
													</td>
													
													
												</tr>
												<tr>
												
												    <td width="20%">
															<h:outputLabel styleClass="LABEL"  value="#{msg['customer.socialSecNumber']}:" ></h:outputLabel>
													</td>
													<td width="30%"> 
															<h:inputText  style="width:85%;" id="txtsocialSecNumber" 
																 value="#{pages$SearchPerson.socialSecNumber}">
															</h:inputText>
													</td>  
													
													 <td width="20%">
															<h:outputLabel styleClass="LABEL"  value="#{msg['customer.passportNumber']}:" ></h:outputLabel>
													</td>
													<td width="30%">
															<h:inputText style="width:85%;" id="txtpassportNumber"
																 value= "#{pages$SearchPerson.passportNumber}" >
															</h:inputText>
													</td>  
													
												</tr>
												<tr>

															<td width="20%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.residenseVisaNumber']}:"></h:outputLabel>
															</td>
															<td width="30%">
															<h:inputText  style="width:85%;"id="txtresidenseVisaNumber"
																 value="#{pages$SearchPerson.residenseVisaNumber}" >
															</h:inputText>
													</td>
													 
													
												</tr>
												
												 <tr>
												 
                                              <td colspan="6" class="BUTTON_TD" > 
                                           			<h:commandButton type="submit" styleClass="BUTTON" value="#{msg['commons.search']}" action="#{pages$SearchPersonPopup.doSearch}" ></h:commandButton>
                                           			 

										              <h:commandButton styleClass="BUTTON" type="button"
												                           value="#{msg['commons.reset']}"
												                           onclick="javascript:clearWindow();" 
												                             /> 

                                		      </td>
                                          </tr>
								    </table>
								    </div>
								    </div>
								    
								
								       <div style="padding:5px;">		    
                   			              <div class="imag">&nbsp;</div>
								    		<div class="contentDiv" >
												<t:dataTable id="test3"
													rows="5" width="99%"
														value="#{pages$SearchPersonPopup.propertyInquiryDataList}"
													binding="#{pages$SearchPersonPopup.dataTable}"
													
													
													preserveDataModel="true" preserveSort="true" var="dataItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												>
												<t:column id="col2" sortable="true" >


													<f:facet name="header" >

														<t:outputText value="#{msg['customer.firstname']}" />

													</f:facet>
													<t:outputText styleClass="A_LEFT"  value="#{dataItem.firstName} #{dataItem.middleName} #{dataItem.lastName}" />
												</t:column>
												<t:column id="col4" sortable="true">


													<f:facet name="header">

														<t:outputText  value="#{msg['customer.passportNumber']}" />

													</f:facet>
													<t:outputText styleClass="A_LEFT"  title="" value="#{dataItem.passportNumber}" />
												</t:column>
												
												<t:column id="col5" sortable="true">


													<f:facet name="header">

														<t:outputText  value="#{msg['customer.residenseVisaNumber']}" />

													</f:facet>
													<t:outputText styleClass="A_LEFT"  title="" value="#{dataItem.residenseVisaNumber}" />
												</t:column>
																								
												<t:column id="col6" sortable="true">


													<f:facet name="header">

														<t:outputText  value="#{msg['customer.drivingLicenseNumber']}" />

													</f:facet>
													<t:outputText styleClass="A_LEFT"  title="" value="#{dataItem.drivingLicenseNumber}" />
												</t:column>
																								
												<t:column id="col7" sortable="true">


													<f:facet name="header">

														<t:outputText  value="#{msg['customer.socialSecNumber']}" />

													</f:facet>
													<t:outputText styleClass="A_LEFT"  title="" value="#{dataItem.socialSecNumber}" />
												</t:column>
												
											    <t:column id="col8" sortable="true"  >
													<f:facet name="header" >
														<t:outputText value="#{msg['commons.select']}" />
                                                    </f:facet>
                                                    		<t:commandLink 
																	action="#{pages$SearchPersonPopup.editDataItem}"
																	>
														  <h:graphicImage  style="width: 16;height: 16;border: 0" 
					                                         			                alt="#{msg['commons.select']}"
																						url="../resources/images/select-icon.gif"
																				      
																				      
																		     ></h:graphicImage>
														
			</t:commandLink>


												</t:column>
												                                                                                                                                          
												   
	                                               
	                                               
												
												
												</t:dataTable>										
											</div>
											
											<div class="contentDivFooter" style="width: 100.3%">
												<t:dataScroller id="scroller" for="test3" paginator="true"
													fastStep="1" paginatorMaxPages="2" immediate="false"
													 paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 
												
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblFowner"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFRowner"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFowner"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblLowner"></t:graphicImage>
														</f:facet>		
												</t:dataScroller>
									
                                           </div>
											
										
								 </h:form>
								 </div>
							  </td>
							</tr>
					      </table>  
					    </td>
					 </tr>
				 <tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				 </table> 
			
									
			</body>
		</f:view>
</html>
