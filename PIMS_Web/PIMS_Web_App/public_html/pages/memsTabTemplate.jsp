<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%-- Please remove above taglibs after completing your tab contents--%>
<t:panelGrid  border="0" width="100%" cellpadding="1"
	cellspacing="1">
	<t:panelGroup><%--Column 1,2,3,4 Starts--%>
		<t:panelGrid columns="4">
		
			<h:outputLabel value="Label#1" styleClass="LABEL"/> 
			<h:outputLabel value="Label#2" styleClass="LABEL"/> 
			
			<h:outputLabel value="Label#3" styleClass="LABEL"/>
			<h:outputLabel value="Label#4" styleClass="LABEL"/>
			
			<h:outputLabel value="Label#5" styleClass="LABEL"/>
			<h:outputLabel value="Label#6" styleClass="LABEL"/>

	
		
			<h:outputLabel value="Label#7" styleClass="LABEL"/> 
			<h:outputLabel value="Label#8" styleClass="LABEL"/> 
			
			<h:outputLabel value="Label#9" styleClass="LABEL"/>
			<h:outputLabel value="Label#10" styleClass="LABEL"/>
			
			<h:outputLabel value="Label#11" styleClass="LABEL"/>
			<h:outputLabel value="Label#12" styleClass="LABEL"/>
			
		</t:panelGrid>
	</t:panelGroup><%--Column 1,2,3,4 Ends--%>
</t:panelGrid>