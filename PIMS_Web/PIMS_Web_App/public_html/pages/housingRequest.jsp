<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	function performTabClick(elementid)
	{
			
		disableInputs();
		
		document.getElementById("detailsFrm:"+elementid).onclick();
		
		return 
		
	}
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
	}	
	
	function onMessageFromHousingReason()
	{
	     disableInputs();
	     document.getElementById("detailsFrm:onMessageFromHousingReason").onclick();
	}
	
	
		
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		disableInputs();	
		if( hdnPersonType=='APPLICANT' ) 
		{			    			 
			 document.getElementById("detailsFrm:hdnPersonId").value=personId;
			 document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
			 document.getElementById("detailsFrm:hdnPersonName").value=personName;
			 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
			 document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;
		}
				
	     document.getElementById("detailsFrm:onMessageFromSearchPerson").onclick();
	} 
	
	function onMessageFromSearchFamilyVillageFiles()
	{
		document.getElementById("detailsFrm:onMessageFromSearchFile").onclick();
	
	}
	
		
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2"><jsp:include page="header.jsp" /></td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%"><jsp:include
								page="leftmenu.jsp" /></td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{pages$housingRequest.pageTitle}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>
																<h:outputText id="errorId"
																	value="#{pages$housingRequest.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$housingRequest.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$housingRequest.pageMode}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$housingRequest.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$housingRequest.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$housingRequest.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$housingRequest.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$housingRequest.hdnIsCompany}"></h:inputHidden>

																<h:commandLink id="onMessageFromSearchPerson"
																	action="#{pages$housingRequest.onMessageFromSearchPerson}" />

																<h:commandLink id="onMessageFromHousingReason"
																	action="#{pages$housingRequest.onMessageFromHousingRejectionReason}" />


															</td>
														</tr>
													</table>
													<table cellpadding="1px" width="100%" cellspacing="5px">
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notification.requestnumber']}:" />
															</td>
															<td>
																<h:inputText styleClass="READONLY" readonly="true"
																	value="#{pages$housingRequest.requestView.requestNumber}"></h:inputText>
															</td>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['request.date']}:"></h:outputLabel>
															</td>
															<td>
																<h:inputText styleClass="READONLY" readonly="true"
																	value="#{pages$housingRequest.requestView.requestDate}"></h:inputText>
															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.status']}:"></h:outputLabel>
															</td>
															<td>
																<h:inputText styleClass="READONLY" readonly="true"
																	value="#{pages$housingRequest.englishLocale?pages$housingRequest.requestView.statusEn:pages$housingRequest.requestView.statusAr}"></h:inputText>
															</td>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['SearchBeneficiary.researcher']}:"></h:outputLabel>
															</td>
															<td>
																<h:selectOneMenu
																	value="#{pages$housingRequest.requestView.socialResearcherId}"
																	tabindex="3">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.pleaseSelect']}" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.socialResearcherGroup}" />
																</h:selectOneMenu>
															</td>
															<td>
															</td>
															<td>
															</td>
														</tr>

														<tr>
															<td>
																<h:outputText id="idSendTo" styleClass="LABEL"
																	rendered="#{pages$housingRequest.showApproveRejectButton }"
																	value="#{msg['mems.investment.label.sendTo']}" />
															</td>
															<td colspan="3">
																<h:selectOneMenu id="idUserGrps"
																	rendered="#{pages$housingRequest.showApproveRejectButton }"
																	binding="#{pages$housingRequest.cmbReviewGroup}"
																	style="width: 200px;">
																	<f:selectItem
																		itemLabel="#{msg['commons.pleaseSelect']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.userGroups}" />
																</h:selectOneMenu>
															</td>
														</tr>


														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	rendered="#{pages$housingRequest.showApproveRejectButton || pages$housingRequest.showCompleteButton}"
																	value="#{msg['maintenanceRequest.remarks']}" />
															</td>
															<td colspan="3">
																<t:inputTextarea style="width: 90%;" id="txt_remarks"
																	rendered="#{pages$housingRequest.showApproveRejectButton || pages$housingRequest.showCompleteButton}"
																	value="#{pages$housingRequest.txtRemarks}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</td>
														</tr>
														<tr>
															<td>
																<t:outputLabel styleClass="LABEL"
																	rendered="#{pages$housingRequest.requestView.statusId== '90018'}"
																	value="#{msg['lbl.header.rejectReason']}:">
																</t:outputLabel>
															</td>
															<td>
																<h:inputText styleClass="READONLY" readonly="true"
																	rendered="#{pages$housingRequest.requestView.statusId== '90018'}"
																	value="#{pages$housingRequest.englishLocale?pages$housingRequest.requestView.housingRequestRejectionReasonEn:pages$housingRequest.requestView.housingRequestRejectionReasonAr}"></h:inputText>
															</td>
															<td>

																<t:outputLabel styleClass="LABEL"
																	rendered="#{pages$housingRequest.requestView.statusId== '90018'}"
																	value="#{msg['lbl.requestReasonDesc']}:  "
																	style="padding-right:5px;padding-left:5px;margin-right:26px;"></t:outputLabel>
															</td>
															<td>
																<t:inputTextarea styleClass="TEXTAREA READONLY"
																	rendered="#{pages$housingRequest.requestView.statusId== '90018'}"
																	readonly="true"
																	value="#{pages$housingRequest.requestView.housingRequestRejectionReasonDescription}"
																	rows="4" />
															</td>

														</tr>


													</table>


													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">




														<div class="TAB_PANEL_INNER">
															<rich:tabPanel binding="#{pages$housingRequest.tabPanel}"
																style="width: 100%">

																<rich:tab id="applicationTab"
																	label="#{msg['commons.tab.applicationDetails']}">
																	<%@ include file="ApplicantDetails.jsp"%>
																</rich:tab>

																<rich:tab id="inheritanceBeneficiaryTabId"
																	label="#{msg['mems.inheritanceFile.tabHeading.beneficiary']}">


																	<t:div
																		style="height:240px;overflow-y:scroll;width:100%;">


																		<t:panelGrid styleClass="TAB_DETAIL_SECTION_INNER"
																			cellpadding="1px" width="100%" cellspacing="5px"
																			columns="4">
																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputText
																					value="#{msg['SearchBeneficiary.name']}" />
																			</h:panelGroup>
																			<h:inputText
																				value="#{pages$housingRequest.beneficiary.firstName}" />

																			<h:outputText value="#{msg['commons.dateofbirth']}" />
																			<rich:calendar
																				value="#{pages$housingRequest.beneficiary.dateOfBirth}"
																				locale="#{pages$housingRequest.locale}" popup="true"
																				datePattern="#{pages$housingRequest.dateFormat}"
																				showApplyButton="false" enableManualInput="false" />



																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['customer.gender']}:"></h:outputLabel>


																			<h:selectOneMenu id="selectGender"
																				value="#{pages$housingRequest.beneficiary.gender}">
																				<f:selectItem
																					itemLabel="#{msg['tenants.gender.male']}"
																					itemValue="M" />
																				<f:selectItem
																					itemLabel="#{msg['tenants.gender.female']}"
																					itemValue="F" />
																			</h:selectOneMenu>

																			<h:outputText value="#{msg['educationAspects.instituteName']}" />
																			<h:inputText
																				value="#{pages$housingRequest.beneficiary.companyName}" />

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['customer.nationality']}:"></h:outputLabel>

																			<h:selectOneMenu immediate="false"
																				style="WIDTH: 157px"
																				value="#{pages$housingRequest.beneficiary.nationalityIdString}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$housingRequest.countryList}" />
																			</h:selectOneMenu>

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['lbl.requestReason']}:"></h:outputLabel>

																			<div class="SCROLLABLE_SECTION"
																				style="height: 100px; width: 298px; overflow-x: hidden; border-width: 1px; border-style: solid; border-color: #a2a2a2;">
																				<h:panelGroup
																					style="border-width: 1px; border-style: solid; border-color: #a2a2a2; background: white; overflow-x: auto; overflow-y: scroll; width: 90%; height: 60px; ">

																					<h:selectOneRadio layout="pageDirection"
																						value="#{pages$housingRequest.beneficiary.housingReasonId}">
																						<f:selectItems
																							value="#{pages$ApplicationBean.housingRequestReason}" />
																					</h:selectOneRadio>
																				</h:panelGroup>
																			</div>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['lbl.requestReasonDesc']}:"></h:outputLabel>
																			<h:inputTextarea rows="6" style="width: 320px;"
																				value="#{pages$housingRequest.beneficiary.housingReasonDescription}" />

																		</t:panelGrid>


																	</t:div>

																</rich:tab>


																<rich:tab id="fileTab"
																	label="#{msg['inheritanceFile.tabHeader']}"
																	
																	rendered="#{pages$housingRequest.showCompleteButton}"
																	>
																	<t:div
																		style="height:240px;overflow-y:scroll;width:100%;">
																		<h:commandLink id="onMessageFromSearchFile"
																			action="#{pages$housingRequest.onMessageFromSearchFile}" />


																		<t:panelGrid styleClass="TAB_DETAIL_SECTION_INNER"
																			cellpadding="1px" width="100%" cellspacing="5px"
																			columns="4">

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['mems.inheritanceFile.fieldLabel.fileNumber']}" />
																			<h:panelGroup>
																				<h:inputText styleClass="READONLY"
																					style="width:170px;" readonly="true"
																					value="#{pages$housingRequest.inheritanceFile.fileNumber}"></h:inputText>
																				<h:graphicImage
																					url="../resources/images/magnifier.gif"
																					onclick="javaScript:openSearchFamilyVillageFiles( 'MODE_SELECT_ONE_POPUP');"
																					style="MARGIN: 0px 0px -4px;"
																					title="#{msg['commons.tooltip.searchInheritanceFile']}"
																					alt="#{msg['commons.tooltip.searchInheritanceFile']}"></h:graphicImage>

																			</h:panelGroup>

																			<h:outputText value="#{msg['lbl.villaNumber']}" />
																			<h:inputText readonly="true" styleClass="READONLY"
																				value="#{pages$housingRequest.inheritanceFile.familyVillageVillaView.villaNumber}" />


																			<h:outputText
																				value="#{msg['familyVillagefile.lbl.category']}" />
																			<h:inputText readonly="true" styleClass="READONLY"
																				value="#{pages$housingRequest.englishLocale ? pages$housingRequest.inheritanceFile.familyVillageVillaView.villaCategoryAr : pages$housingRequest.inheritanceFile.familyVillageVillaView.villaCategoryEn}" />


																		</t:panelGrid>
																	</t:div>

																</rich:tab>



																<rich:tab id="reviewTab"
																	label="#{msg['commons.tab.reviewDetails']}">
																	<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
																</rich:tab>


																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>
																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>
																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$housingRequest.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

																<rich:tab id="commentsTabId"
																	label="#{msg['transferContract.comments']}">


																	<t:div
																		style="height:240px;overflow-y:scroll;width:100%;">


																		<t:panelGrid styleClass="TAB_DETAIL_SECTION_INNER"
																			cellpadding="1px" width="100%" cellspacing="5px"
																			columns="2">

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['lbl.director.comments']}:"></h:outputLabel>
																			<h:inputTextarea rows="6" style="width: 320px;"
																				value="#{pages$housingRequest.requestView.directorComments}" />


																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['lbl.researcher.comments']}:"></h:outputLabel>
																			<h:inputTextarea rows="6" style="width: 320px;"
																				value="#{pages$housingRequest.requestView.researcherComments}" />

																		</t:panelGrid>


																	</t:div>

																</rich:tab>




															</rich:tabPanel>
														</div>

														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">
															<pims:security
																screen="Pims.EndowMgmt.DonationRequest.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$housingRequest.showSaveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$housingRequest.onSave}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$housingRequest.showSubmitButton}"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink
																	action="#{pages$housingRequest.onSubmitted}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$housingRequest.showResubmitForApprovalButton}"
																	value="#{msg['socialResearch.btn.resubmit']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink
																	action="#{pages$housingRequest.onReSubmitted}" />




																<h:commandButton styleClass="BUTTON"	
																	rendered="#{pages$housingRequest.showCompleteButton}"
																	value="#{msg['commons.completeButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink
																	action="#{pages$housingRequest.onComplete}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$housingRequest.showPostBackButton}"
																	value="#{msg['commons.sendBack']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink
																	action="#{pages$housingRequest.onPostBack}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$housingRequest.showApproveRejectButton && pages$housingRequest.userTask ne null}"
																	value="#{msg['commons.approveButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink
																	action="#{pages$housingRequest.onApprove}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$housingRequest.showApproveRejectButton && 
																				pages$housingRequest.userTask ne null
																			    }"
																	value="#{msg['commons.sendBack']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink
																	action="#{pages$housingRequest.onSendBackFromApproval}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$housingRequest.showApproveRejectButton && pages$housingRequest.userTask ne null}"
																	value="#{msg['commons.rejectButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else showAddRejectReasonPopup();">
																</h:commandButton>


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$housingRequest.showApproveRejectButton && pages$housingRequest.userTask ne null}"
																	value="#{msg['commons.review']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink
																	action="#{pages$housingRequest.onReviewRequired}" />



															</pims:security>

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$housingRequest.pageMode == 'REVIEW_REQUIRED' && pages$housingRequest.userTask ne null}"
																value="#{msg['commons.done']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink
																action="#{pages$housingRequest.onReviewed}" />

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$housingRequest.showPrintButton}"
																value="#{msg['commons.print']}"
																action="#{pages$housingRequest.onPrintReceipt}" />

														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>