
<t:div id="tabDDetails" style="width:100%;">
	<t:panelGrid id="ddTab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:outputLabel styleClass="LABEL" value="#{msg['endowment.lbl.num']}:" />
		<h:inputText id="txtendowmentNum" readonly="true"
			value="#{pages$endowmentManage.endowment.endowmentNum}"
			styleClass="READONLY" />

		<h:outputLabel styleClass="LABEL"
			value="#{msg['endowment.lbl.name']}:" />
		<h:inputText id="txtendowmentName" readonly="true"
			value="#{pages$endowmentManage.endowment.endowmentName}"
			styleClass="READONLY" />

		<h:outputLabel styleClass="LABEL"
			value="#{msg['endowment.lbl.typeCol']}:" />
		<h:selectOneMenu disabled="true"
			value="#{pages$endowmentManage.endowment.statusId}"
			styleClass="SELECT_MENU">

			<f:selectItems
				value="#{pages$ApplicationBean.endowmentAssetTypesList}" />
		</h:selectOneMenu>
		
		<h:outputLabel styleClass="LABEL"
			value="#{msg['endowment.lbl.typeCol']}:" />
		<h:selectOneMenu disabled="true"
			value="#{pages$endowmentManage.endowment.assetTypeIdStr}"
			styleClass="SELECT_MENU">

			<f:selectItems
				value="#{pages$ApplicationBean.endowmentAssetTypesList}" />
		</h:selectOneMenu>
		
		<h:outputLabel styleClass="LABEL"
			value="#{msg['commons.typeStatus']}:" />
		<h:selectOneMenu disabled="true"
			value="#{pages$endowmentManage.endowment.typeStatusId}"
			styleClass="SELECT_MENU">

			<f:selectItems
				value="#{pages$ApplicationBean.endowmentStatusList}" />
		</h:selectOneMenu>
		
		

		<h:outputLabel styleClass="LABEL"
			value="#{msg['endowment.lbl.onAgreement']}" />
		<h:selectBooleanCheckbox id="chkOnAgreement"
			binding="#{pages$endowmentManage.chkOnAgreement }" />


		<h:outputLabel styleClass="LABEL"
			value="#{msg['inheritanceFile.inheritedAssetTab.label.isManagerAmaf']}" />
		<h:selectBooleanCheckbox id="isManagerAmafId" disabled="true"
			styleClass="SELECT_MENU"
			binding="#{pages$endowmentManage.chkIsManagerAmaf}">

		</h:selectBooleanCheckbox>

		<h:outputLabel
			value="#{msg['inheritanceFile.inheritedAssetTab.label.manager']}" />
		<t:panelGroup>
			<h:inputText id="managerName" style="width:84%;"
				value="#{pages$endowmentManage.hdnPersonName}" styleClass="READONLY" />
			<h:graphicImage title="#{msg['commons.view']}"
				rendered="#{pages$endowmentManage.endowment.isAmafManager!='1'}"
				url="../resources/images/app_icons/Tenant.png"
				style="MARGIN: 0px 0px -4px; CURSOR: hand;"
				onclick="javaScript:showPersonReadOnlyPopup('#{pages$endowmentManage.hdnPersonId}');">
			</h:graphicImage>
		</t:panelGroup>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['endowmentFileAsso.lbl.expRevenue']}" />
		</td>
		<td>
			<h:inputText
				value="#{pages$endowmentManage.endowment.expectedRevenueString}"
				onkeyup="removeNonNumeric(this)" onkeypress="removeNonNumeric(this)"
				onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
				onchange="removeNonNumeric(this)">

			</h:inputText>

			<h:outputLabel styleClass="LABEL"
				value="#{msg['endowmentFileAsso.lbl.generatesRevenue']}" />
			<h:selectOneMenu
				value="#{pages$endowmentManage.endowment.expRevFreqString}"
				styleClass="SELECT_MENU">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$ApplicationBean.paymentPeriodList}" />
			</h:selectOneMenu>

			<h:outputLabel styleClass="LABEL"
				value="#{msg['endowmentfile.lbl.revenuePercentage']}" />
			<h:inputText value="#{pages$endowmentManage.endowment.revenuePercentageString}"
				onkeyup="removeNonNumeric(this)" onkeypress="removeNonNumeric(this)"
				onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
				onchange="removeNonNumeric(this)">
			</h:inputText>
			
			<h:outputLabel styleClass="LABEL"
				value="#{msg['endowmentfile.lbl.reconstructionPercentage']}" />
			<h:inputText value="#{pages$endowmentManage.endowment.reconstructionPercentageString}"
				onkeyup="removeNonNumeric(this)" onkeypress="removeNonNumeric(this)"
				onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
				onchange="removeNonNumeric(this)">
			</h:inputText>
			
			<h:outputLabel styleClass="LABEL"
				value="#{msg['endowmentfile.lbl.marketValue']}" />
			<h:inputText value="#{pages$endowmentManage.endowment.marketValStr}"
				onkeyup="removeNonNumeric(this)" onkeypress="removeNonNumeric(this)"
				onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
				onchange="removeNonNumeric(this)">

			</h:inputText>

			<h:outputLabel styleClass="LABEL"
				value="#{msg['endowment.lbl.maincategory']}:" />
			<h:selectOneMenu
				value="#{pages$endowmentManage.endowment.mainCategoryIdStr}"
				styleClass="SELECT_MENU">
				<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
					itemValue="-1" />
				<f:selectItems
					value="#{pages$ApplicationBean.endowmentMainCategory}" />

			</h:selectOneMenu>

			<h:outputLabel styleClass="LABEL"
				value="#{msg['endowment.lbl.endowFor']}" />
			<h:inputText maxlength="250"
				value="#{pages$endowmentManage.endowment.endowFor}" />
	</t:panelGrid>

	<t:panelGrid border="0" width="95%" cellpadding="1" cellspacing="1"
		id="ExtraDetailWithEachAssetType">

		<t:div style="width:100%;background-color:#636163;"
			rendered="#{pages$endowmentManage.landProperties}">
			<h:outputText value="#{msg['inhFile.label.LandnProp']}"
				styleClass="GROUP_LABEL"></h:outputText>
		</t:div>

		<h:panelGrid columns="4" id="landAndPropertiesPanel"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%"
			binding="#{pages$endowmentManage.gridLandProperties}">

			<h:outputLabel styleClass="LABEL"
				rendered="#{! empty pages$endowmentManage.endowment.propertyView && ! empty pages$endowmentManage.endowment.propertyView.propertyId}"
				value="#{msg['property.propertyNumber']}:"></h:outputLabel>
			<h:inputText styleClass="READONLY"
				rendered="#{! empty pages$endowmentManage.endowment.propertyView && ! empty pages$endowmentManage.endowment.propertyView.propertyId}"
				id="txtpropertyNumber" style="width:78%;" readonly="true"
				value="#{pages$endowmentManage.endowment.propertyView.propertyNumber}"></h:inputText>

			<h:outputLabel styleClass="LABEL"
				rendered="#{! empty pages$endowmentManage.endowment.propertyView && ! empty pages$endowmentManage.endowment.propertyView.propertyId}"
				value="#{msg['commons.status']}:"></h:outputLabel>
			<h:inputText styleClass="READONLY"
				rendered="#{! empty pages$endowmentManage.endowment.propertyView && ! empty pages$endowmentManage.endowment.propertyView.propertyId}"
				id="txtpropertyStatus" style="width:78%;" readonly="true"
				value="#{pages$endowmentManage.endowment.propertyView.statusTypeAr}"></h:inputText>


			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.land']}" />
			<h:inputText maxlength="200" id="txtLandNumber"
				value="#{pages$endowmentManage.endowment.fielDetials1}" />

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.devolution']}" />
			<h:inputText maxlength="200"
				value="#{pages$endowmentManage.endowment.fielDetials2}" />

			<h:outputLabel value="#{msg['receiveProperty.addressLineOne']}" />
			<h:inputText maxlength="200"
				value="#{pages$endowmentManage.endowment.fielDetials3}" />

			<h:outputLabel value="#{msg['botContractDetails.LandArea']}" />
			<h:inputText maxlength="200"
				value="#{pages$endowmentManage.endowment.fielDetials4}"
				onkeyup="removeNonNumeric(this)" onkeypress="removeNonNumeric(this)"
				onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
				onchange="removeNonNumeric(this)" />

		</h:panelGrid>
		<t:div style="width:100%;background-color:#636163;"
			rendered="#{pages$endowmentManage.cheque}">
			<h:outputText value="#{msg['inhFile.label.cheque']}"
				styleClass="GROUP_LABEL"></h:outputText>
		</t:div>
		
		<t:panelGrid columns="4" id="chequePanel"
													rendered="#{pages$endowmentManage.cheque}"
													columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
													width="100%"
													binding="#{pages$endowmentFileAssoPopup.gridCheque}">

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
													<h:inputText onkeyup="removeNonNumeric(this)"
														onkeypress="removeNonNumeric(this)"
														onkeydown="removeNonNumeric(this)"
														onblur="removeNonNumeric(this)"
														onchange="removeNonNumeric(this)"
														value="#{pages$endowmentManage.endowment.fielDetials1}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.bank']}" />
													<h:selectOneMenu
														value="#{pages$endowmentManage.bankIdStr}"
														styleClass="SELECT_MENU">
														<f:selectItem
															itemLabel="#{msg['commons.combo.PleaseSelect']}"
															itemValue="-1" />
														<f:selectItems value="#{pages$ApplicationBean.bankList}" />
													</h:selectOneMenu>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.chequeNmbr']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentManage.endowment.fielDetials2}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.dueDate']}" />
													<rich:calendar
														value="#{pages$endowmentManage.endowment.dueDate}"
														locale="#{pages$endowmentFileAssoPopup.locale}"
														popup="true"
														datePattern="#{pages$endowmentFileAssoPopup.dateFormat}"
														showApplyButton="false" enableManualInput="false"
														inputStyle="width:185px;height:17px" />

												</t:panelGrid>
		
		
		<t:div style="width:100%;background-color:#636163;"
			rendered="#{pages$endowmentManage.stockshares}">
			<h:outputText value="#{msg['inhFile.label.stocknShare']}"
				styleClass="GROUP_LABEL"></h:outputText>
		</t:div>
		<h:panelGrid columns="4" id="stockAndSharesPanel"
			binding="#{pages$endowmentManage.gridStockshares}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.company']}" />
			<h:inputText maxlength="150"
				value="#{pages$endowmentManage.endowment.fielDetials1}"></h:inputText>

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.nofShare']}" />
			<h:inputText maxlength="50" onkeyup="removeNonNumeric(this)"
				onkeypress="removeNonNumeric(this)"
				onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
				onchange="removeNonNumeric(this)"
				value="#{pages$endowmentManage.endowment.fielDetials2}"></h:inputText>

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.listingParty']}" />
			<h:inputText maxlength="150"
				value="#{pages$endowmentManage.endowment.fielDetials3}"></h:inputText>

		</h:panelGrid>
		<t:div style="width:100%;background-color:#636163;"
			rendered="#{pages$endowmentManage.licenses}">
			<h:outputText value="#{msg['inhFile.label.license']}"
				styleClass="GROUP_LABEL"></h:outputText>
		</t:div>
		<h:panelGrid columns="4" id="licensePanel"
			binding="#{pages$endowmentManage.gridLicenses}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.licenseName']}" />
			<h:inputText maxlength="150"
				value="#{pages$endowmentManage.endowment.fielDetials1}"></h:inputText>

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.licenseNmber']}" />
			<h:inputText maxlength="150"
				value="#{pages$endowmentManage.endowment.fielDetials2}"></h:inputText>

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.licenseStatus']}" />
			<h:inputText maxlength="150"
				value="#{pages$endowmentManage.endowment.fielDetials3}"></h:inputText>

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.rentValue']}" />
			<h:inputText onkeyup="removeNonNumeric(this)"
				onkeypress="removeNonNumeric(this)"
				onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
				onchange="removeNonNumeric(this)"
				value="#{pages$endowmentManage.endowment.fielDetials4}"></h:inputText>
		</h:panelGrid>
		<t:div style="width:100%;background-color:#636163;"
			rendered="#{pages$endowmentManage.cash}">
			<h:outputText value="#{msg['inhFile.label.cash']}"
				styleClass="GROUP_LABEL"></h:outputText>
		</t:div>
		<t:panelGrid columns="4" id="cashPanel"
			rendered="#{pages$endowmentManage.cash}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%"
			binding="#{pages$endowmentManage.gridCash}">

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
			<h:inputText onkeyup="removeNonNumeric(this)"
				onkeypress="removeNonNumeric(this)"
				onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
				onchange="removeNonNumeric(this)"
				value="#{pages$endowmentManage.endowment.fielDetials1}"></h:inputText>
		</t:panelGrid>

		<t:div style="width:100%;background-color:#636163;"
			rendered="#{pages$endowmentManage.animals}">

			<h:outputText value="#{msg['inhFile.label.animal']}"
				styleClass="GROUP_LABEL"></h:outputText>
		</t:div>

		<h:panelGrid columns="4" id="animalsPanel"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%"
			binding="#{pages$endowmentManage.gridAnimals}">

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.nofAnimal']}" />
			<h:inputText maxlength="150" onkeyup="removeNonInteger(this)"
				onkeypress="removeNonInteger(this)"
				onkeydown="removeNonInteger(this)" onblur="removeNonInteger(this)"
				onchange="removeNonInteger(this)"
				value="#{pages$endowmentManage.endowment.fielDetials1}"></h:inputText>

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.animalType']}" />
			<h:inputText maxlength="150"
				value="#{pages$endowmentManage.endowment.fielDetials2}"></h:inputText>
		</h:panelGrid>
		<t:div style="width:100%;background-color:#636163;"
			rendered="#{pages$endowmentManage.vehicles}">
			<h:outputText value="#{msg['inhFile.label.vehicles']}"
				styleClass="GROUP_LABEL"></h:outputText>
		</t:div>
		<h:panelGrid columns="4" id="vehiclePanel"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%"
			binding="#{pages$endowmentManage.gridVehicles}">

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.govDept']}" />
			<h:selectOneMenu
				value="#{pages$endowmentManage.endowment.fielDetials1}"
				styleClass="SELECT_MENU">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$ApplicationBean.govtDepttList}" />
			</h:selectOneMenu>

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.plateNmber']}" />
			<h:inputText maxlength="150"
				value="#{pages$endowmentManage.endowment.fielDetials2}"></h:inputText>

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.vehicleCat']}" />
			<h:inputText maxlength="150"
				value="#{pages$endowmentManage.endowment.fielDetials3}"></h:inputText>

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.vehicleType']}" />
			<h:inputText maxlength="150"
				value="#{pages$endowmentManage.endowment.fielDetials4}"></h:inputText>

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.vehicleStatus']}" />
			<h:inputText maxlength="150"
				value="#{pages$endowmentManage.endowment.fielDetials5}"></h:inputText>
		</h:panelGrid>
		<t:div style="width:100%;background-color:#636163;"
			rendered="#{pages$endowmentManage.transportation}">
			<h:outputText value="#{msg['inhFile.label.transportation']}"
				styleClass="GROUP_LABEL"></h:outputText>
		</t:div>
		<h:panelGrid columns="4" id="transportationPanel"
			binding="#{pages$endowmentManage.gridTransportation}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.plateNmber']}" />
			<h:inputText maxlength="150"
				value="#{pages$endowmentManage.endowment.fielDetials1}"></h:inputText>

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.income']}" />
			<h:inputText maxlength="50" onkeyup="removeNonNumeric(this)"
				onkeypress="removeNonNumeric(this)"
				onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
				onchange="removeNonNumeric(this)"
				value="#{pages$endowmentManage.endowment.fielDetials2}"></h:inputText>

			<h:outputLabel
				value="#{msg['mems.inheritanceFile.fieldLabel.transferParty']}" />
			<h:inputText maxlength="150"
				value="#{pages$endowmentManage.endowment.fielDetials3}"></h:inputText>
		</h:panelGrid>
	</t:panelGrid>

</t:div>
