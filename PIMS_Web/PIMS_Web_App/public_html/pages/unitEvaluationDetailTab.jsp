<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<script type="text/javascript">
</script>
<script type="text/javascript">
</script>
<t:div  style="width:100%">
	<t:panelGrid width="100%" columns="4">
		<h:outputLabel styleClass="LABEL"
			value="#{msg['receiveProperty.propertyReferenceNumber']}:">
		</h:outputLabel>
		<h:inputText id="txtPropertyNumber" 
		styleClass="READONLY"
		readonly="true"
		value="#{pages$unitEvaluationDetailTab.propertyNumber}" >
		</h:inputText>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['property.buildingName']}:">
		</h:outputLabel>
		<h:inputText id="txtPropertyName" 
		styleClass="READONLY"
		readonly="true"
		value="#{pages$unitEvaluationDetailTab.propertyName}" 
		>
		</h:inputText>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['receiveProperty.endowedName']}:">
		</h:outputLabel>
		<h:inputText id="txtEndowedName"
		styleClass="READONLY"
		readonly="true" 
		value="#{pages$unitEvaluationDetailTab.endowedName}" 
		>
		</h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['unit.floorNumber']}:">
		</h:outputLabel>
		<h:inputText id="txtFloorNo"
		styleClass="READONLY"
		readonly="true" 
		value="#{pages$unitEvaluationDetailTab.floorNumber}" 
		>
		</h:inputText>
		<h:panelGroup>
		<h:outputLabel styleClass="LABEL" value="#{msg['unit.unitNumber']}:">
		</h:outputLabel>
		</h:panelGroup>
		<h:panelGroup>
		<h:inputText id="txtUnitNo"
		styleClass="READONLY"
		readonly="true" 
		value="#{pages$unitEvaluationDetailTab.unitNumber}" 
		>
		</h:inputText>
		<h:commandLink action="#{pages$unitEvaluationDetailTab.openUnitsPopUp}"
			rendered="#{!pages$unitEvaluationDetailTab.popUp && !pages$unitEvaluationDetailTab.requestAvailable}"
			title="#{msg['commons.search']}">
			<h:graphicImage id="imgSearchUnit" style="MARGIN: 0px 0px -4px"
				url="../resources/images/magnifier.gif"></h:graphicImage>
		</h:commandLink>
		</h:panelGroup>
		<h:outputLabel styleClass="LABEL" value="#{msg['unit.unitUsage']}:">
		</h:outputLabel>
		<h:inputText id="txtUnitUsage"
		styleClass="READONLY"
		readonly="true"		 
		value="#{pages$unitEvaluationDetailTab.unitUsage}" 
		>
		</h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['unit.unitType']}:">
		</h:outputLabel>
		<h:inputText id="txtUnitType"
		styleClass="READONLY"
		readonly="true" 
		value="#{pages$unitEvaluationDetailTab.unitType}" 
		>
		</h:inputText>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['unit.InvestmentType']}:">
		</h:outputLabel>
		<h:inputText id="txtInvestmentType"
		styleClass="READONLY"
		readonly="true" 
		value="#{pages$unitEvaluationDetailTab.investmentType}" 
		>
		</h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['unit.unitSide']}:">
		</h:outputLabel>
		<h:inputText id="txtUnitSide"
		styleClass="READONLY"
		readonly="true" 
		value="#{pages$unitEvaluationDetailTab.unitSide}" 
		>
		</h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['unit.BedRooms']}:">
		</h:outputLabel>
		<h:inputText id="txtBedRooms"
		styleClass="READONLY"
		readonly="true" 
		value="#{pages$unitEvaluationDetailTab.noOfBeds}" 
		>
		</h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['unit.DewaNumber']}:"></h:outputLabel>
		<h:inputText id="txtDewaNumber"
		styleClass="READONLY"
		readonly="true" 
		value="#{pages$unitEvaluationDetailTab.dewaAccNo}" 
		>
		</h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['unit.unitStatus']}:">
		</h:outputLabel>
		<h:inputText id="txtStatus" 
		styleClass="READONLY"
		readonly="true"
		value="#{pages$unitEvaluationDetailTab.unitStatus}" 
		>
		</h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['commons.Emirate']}:">
		</h:outputLabel>
		<h:inputText id="txtState"
		styleClass="READONLY"
		readonly="true"
		value="#{pages$unitEvaluationDetailTab.unitState}" 
		 >
		</h:inputText>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['unit.unitDescription']}:">
		</h:outputLabel>
		<h:inputText id="txtUnitDesc"
		styleClass="READONLY"
		readonly="true" 
		value="#{pages$unitEvaluationDetailTab.unitDesc}" >
		</h:inputText>
		
		<h:outputLabel styleClass="LABEL"
			value="#{msg['cancelContract.tab.unit.rent']}:">
		</h:outputLabel>
		<h:inputText id="txtUnitRentAmount"
		styleClass="READONLY"
		readonly="true" 
		value="#{pages$unitEvaluationDetailTab.unitRentAmount}" >
		</h:inputText>
	</t:panelGrid>
</t:div>
