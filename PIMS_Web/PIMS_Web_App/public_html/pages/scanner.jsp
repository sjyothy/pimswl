<%-- - Author: Wasif Kirmani - Copyright Notice: Avanza Solutions - Description: for submitting Transfer Contract
     Application--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- TAG LIBS START--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/";
%>
<f:view>
    <body>
        <h:form id="ScanDocumentForm" enctype="multipart/form-data">
            <%-- LOADING RESOURCES START--%>
            <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
            <f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
            <%-- LOADING RESOURCES END--%>
            <html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
                <%-- HEAD START--%>
                <head>
                    <title></title>
                    <meta http-equiv="content-type"
                          content="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
                    <meta http-equiv="pragma" content="no-cache"/>
                    <meta http-equiv="cache-control" content="no-cache"/>
                    <meta http-equiv="expires" content="0"/>
                    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
                    <meta http-equiv="description" content="This is a template page"/>
                    <link rel="stylesheet" type="text/css"
						href="../<h:outputFormat value="#{path.css_simple}"/>" />
					<link rel="stylesheet" type="text/css"
						href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
					<link rel="stylesheet" type="text/css"
						href="../<h:outputFormat value="#{path.css_table}"/>" />
					<link rel="stylesheet" type="text/css"
						href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
					<link rel="stylesheet" type="text/css"
						href="../<h:outputFormat value="#{path.css_amaf}"/>" />
					<link rel="stylesheet" type="text/css"
						href="../<h:outputFormat value="#{path.css_calendar}"/>" />
                </head>
                <script language="JavaScript" type="text/javascript">
                  var Pages = new Array()
                  var ImageCount = 0;
                  var CurrentImage = 1;
                  var Success;

                  function Initialisation() {
                  }

                  function nextScan() {
                  }

                  function SelectClick() {
                      document.ScanDocumentForm.csxi.Clear ( );                         
                      document.ScanDocumentForm.csxi.ScaleToGray = false;
                      document.ScanDocumentForm.csxi.SelectTwainDevice();
                      if (document.ScanDocumentForm.csxi.TwainConnected) {
                          document.ScanDocumentForm.scanbutton.disabled = false;
                          document.ScanDocumentForm.csxi.twainmultiimage = true;
                          document.ScanDocumentForm.csxi.useadf = true;
                          document.ScanDocumentForm.csxi.twainimagestoread = 0;
                          ScanClick();
                      }

                  }

                  function ScanClick() {
                      ImageCount = 0;
                      CurrentImage = 1;

                      if (document.ScanDocumentForm.advOptions.checked) {
                          document.ScanDocumentForm.csxi.usetwaininterface = true;
                      }
                      else {
                          document.ScanDocumentForm.csxi.usetwaininterface = false;
                          document.ScanDocumentForm.csxi.showTwainProgress = true;

                      }

                      document.ScanDocumentForm.csxi.Acquire();
                      
                      if (document.ScanDocumentForm.csxi.ImageHeight != 0) {
                          AutoZoomClick();
                      }
                      
                      return false;
                  }

                  function csxiacquire() {
                      try {
                          
                          document.ScanDocumentForm.csxi.ClearPDF();
                          document.ScanDocumentForm.csxi.redraw();
                          document.ScanDocumentForm.csxi.addtopdf(0);
                          ImageCount += 1;
                          CurrentImage = ImageCount;
                          Pages[ImageCount] = document.ScanDocumentForm.csxi.WriteBinary(0);
                      }
                      catch (err) {
                          alert(err.message);
                      }
                  }
                  function closeWindowSubmit() {
                      window.opener.opener.document.forms[0].submit();
                      window.opener.close();
                      window.close();
                  }
                  function UploadClick() {
                      try {
                                  for (var i = 1;i <= ImageCount;i++) {
                                      document.ScanDocumentForm.csxi.ReadBinary(0, Pages[i]);
                                      document.ScanDocumentForm.csxi.Redraw();
                                      document.ScanDocumentForm.csxi.AddToPDF(0);
                                  }
                                  // Success =document.ScanDocumentForm.csxi.writepdf("C:\\demo_pdf.pdf");
                                  Success = document.ScanDocumentForm.csxi.PostImage('<%=basePath%>pages/scanner.jsf', 'scan.pdf', '', 8);
//                                  alert(document.ScanDocumentForm.csxi.PostReturnFile);
                                  if (Success) {
                                      window.opener.document.forms[0].submit();
                                      window.close();      
                                  }
                                  else {
                                      alert('<h:outputText value="#{msg['Tool.Scan.Upload.Fail']}"/>');
                                  }
                      }
                      catch (err) {
                          alert(err.message);
                      }
                      return Success;
                  }

                  function RectClick() {
                      document.ScanDocumentForm.csxi.MouseSelectRectangle();
                      document.ScanDocumentForm.cropbutton.disabled = false;
                      Pages[CurrentImage] = document.ScanDocumentForm.csxi.WriteBinary(0);
                  }

                  function CropClick() {
                      document.ScanDocumentForm.csxi.CropToSelection();
                      document.ScanDocumentForm.cropbutton.disabled = true;
                      Pages[CurrentImage] = document.ScanDocumentForm.csxi.WriteBinary(0);
                  }

                  function LeftClick() {
                      document.ScanDocumentForm.csxi.Rotate(90);
                      document.ScanDocumentForm.csxi.Redraw();
                      Pages[CurrentImage] = document.ScanDocumentForm.csxi.WriteBinary(0);
                  }

                  function RightClick() {
                      document.ScanDocumentForm.csxi.Rotate(270);
                      Pages[CurrentImage] = document.ScanDocumentForm.csxi.WriteBinary(0);
                  }

                  function ZoomInClick() {
                      document.ScanDocumentForm.csxi.AutoZoom = false;
                      if (document.ScanDocumentForm.csxi.Zoom + 10 <= 1000) {
                          document.ScanDocumentForm.csxi.Zoom = document.ScanDocumentForm.csxi.Zoom + 10;
                      }
                  }

                  function ZoomOutClick() {
                      document.ScanDocumentForm.csxi.AutoZoom = false;
                      if (document.ScanDocumentForm.csxi.Zoom - 10 >= 5) {
                          document.ScanDocumentForm.csxi.Zoom = document.ScanDocumentForm.csxi.Zoom - 10;
                      }
                  }

                  function AutoZoomClick() {
                      document.ScanDocumentForm.csxi.AutoZoom = true;
                  }

                  function ActualSizeClick() {
                      document.ScanDocumentForm.csxi.AutoZoom = false;
                      document.ScanDocumentForm.csxi.Zoom = 100;
                  }

                  function PrevClick() {
                      if (CurrentImage > 1) {
                          CurrentImage -= 1;
                          document.ScanDocumentForm.csxi.ReadBinary(0, Pages[CurrentImage]);
                          document.ScanDocumentForm.csxi.Redraw();
                          document.getElementById('scanPages').innerText = "Page " + CurrentImage + " of " + ImageCount;
                      }

                      if (CurrentImage == 1) {
                          document.ScanDocumentForm.previouspagebutton.disabled = true;
                      }

                      document.ScanDocumentForm.nextpagebutton.disabled = false;

                  }

                  function NextClick() {
                      if (CurrentImage < ImageCount) {
                          CurrentImage += 1;
                          document.ScanDocumentForm.csxi.ReadBinary(0, Pages[CurrentImage]);
                          document.ScanDocumentForm.csxi.Redraw();
                          document.getElementById('scanPages').innerText = "Page " + CurrentImage + " of " + ImageCount;
                      }
                      if (CurrentImage == ImageCount) {
                          document.ScanDocumentForm.nextpagebutton.disabled = true;
                      }

                      document.ScanDocumentForm.previouspagebutton.disabled = false;

                  }
                </script>
                <script language="Javascript" for="csxi" event="onacquire()">
                  csxiacquire()
                </script>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="DetailFormHeader" colspan="1">
                            <h:outputText id="scaner" value="#{msg['Tool.Scan.scanHeader']}"></h:outputText>
                        </td>
                    </tr>
                     
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                     
                    <tr>
                        <td>
                            <table width="100%" cellpadding="5" cellspacing="0" border="0">
                                <tbody>
                                    <tr>
                                        <td colspan="2">
                                            <object classid="clsid:5220cb21-c88d-11cf-b347-00aa00a28331">
                                                <param name="LPKPath" value="csximage.lpk"/>
                                            </object>
                                             
                                            <object id="csxi" classid="clsid:62E57FC5-1CCD-11D7-8344-00C1261173F0"
                                                    codebase="csXImage.cab" width="780" height="280">
                                                <param name="ScaleToGray" value="false"/>
                                            </object>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ReverseAlign" width="55%">
                                            <input type="checkbox" id="advOptions" class="checkbox"/>
                                             &nbsp;&nbsp; 
                                            <span class="labels">
                                                <h:outputText value="#{msg['Tool.Scan.advanceOptions']}"/></span>
                                        </td>
                                        <td class="ReverseAlign" width="45%">&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="ReverseAlign">
                                            <button style="width: 15%; border-bottom: 0px; border-left: 0px; border-right: 0px; border-top: 0px;"
                                                    id="scanbutton" onclick="SelectClick();" tabindex="2"
                                                    class="BUTTON">
                                                <h:outputText value="#{msg['Tool.Scan']}"></h:outputText>
                                            </button>
                                             &nbsp; 
                                            <button style="width: 15%; border-bottom: 0px; border-left: 0px; border-right: 0px; border-top: 0px;"
                                                    id="zoominbutton" onclick="ZoomInClick();return false;" tabindex="7"
                                                    class="BUTTON">
                                                <h:outputText value="#{msg['Tool.Scan.ZoomIn']}"/>
                                            </button>
                                             &nbsp; 
                                            <t:commandButton style="width:15%;" id="zoomoutbutton"
                                                             onclick="ZoomOutClick();return false;" tabindex="7"
                                                             styleClass="BUTTON" value="#{msg['Tool.Scan.ZoomOut']}"/>
                                             &nbsp; 
                                            <t:commandButton style="width:15%;" id="screenfitbutton"
                                                             onclick="AutoZoomClick();return false;" tabindex="7"
                                                             styleClass="BUTTON" value="#{msg['Tool.Scan.ScreenFit']}"/>
                                             &nbsp; 
                                            <t:commandButton style="width:15%;" id="actualsizebutton"
                                                             onclick="ActualSizeClick();return false;" tabindex="7"
                                                             styleClass="BUTTON"
                                                             value="#{msg['Tool.Scan.ActualSize']}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="ReverseAlign">
                                            <button style="width: 15%; border-bottom: 0px; border-left: 0px; border-right: 0px; border-top: 0px;"
                                                    id="rectbutton" onclick="RectClick();" tabindex="4" class="BUTTON">
                                                <h:outputText value="#{msg['Tool.Scan.SelectArea']}"></h:outputText>
                                            </button>
                                             &nbsp; 
                                            <button style="width: 15%; border-bottom: 0px; border-left: 0px; border-right: 0px; border-top: 0px;"
                                                    id="cropbutton" onclick="CropClick();" tabindex="5" class="BUTTON">
                                                <h:outputText value="#{msg['Tool.Scan.Crop']}"></h:outputText>
                                            </button>
                                             &nbsp; 
                                            <t:commandButton style="width:15%;" id="leftbutton"
                                                             onclick="LeftClick();return false;" tabindex="6"
                                                             styleClass="BUTTON"
                                                             value="#{msg['Tool.Scan.RotateLeft']}"/>
                                             &nbsp; 
                                            <t:commandButton style="width:15%;" id="rightbutton"
                                                             onclick="RightClick();return false;" tabindex="7"
                                                             styleClass="BUTTON"
                                                             value="#{msg['Tool.Scan.RotateRight']}"/>
                                             &nbsp; 
                                            <t:commandButton onclick="return UploadClick();"
                                                             value="#{msg['Tool.Scan.UploadImage']}" style="width:15%;"
                                                             id="uploadbutton" tabindex="3" styleClass="BUTTON"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </html>
        </h:form>
        <script type="text/javascript">
              Initialisation();
        </script>
    </body>
</f:view>