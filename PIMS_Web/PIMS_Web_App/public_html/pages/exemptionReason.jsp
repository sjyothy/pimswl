<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>Exemption Reason</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
		 	<script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>			
		</head>
		<BODY class="BODY_STYLE">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0" style="margin-left:1px; margin-right:1px;">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['settlement.title.exemption']}" styleClass="HEADER_FONT"/>
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap" 	background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION_POPUP" style="height:400px;overflow:hidden;">
										<h:form id="attachmentForm" enctype="multipart/form-data">
											<CENTER>
												<%@ include file="notes/addNotes.jsp"%>
												<t:div>&nbsp;</t:div>
												<h:commandButton id="saveExemptionReason" style="width:60px;" styleClass="BUTTON" value="#{msg['commons.saveButton']}" actionListener="#{pages$ExemptionReasonBean.saveExemptionReason}"/>
												<h:commandButton id="closeWindow" style="width:60px;" styleClass="BUTTON" value="#{msg['cancelContract.close']}" onclick="javascript: self.close(); return false;"/>
											</CENTER>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</BODY>
	</html>
</f:view>