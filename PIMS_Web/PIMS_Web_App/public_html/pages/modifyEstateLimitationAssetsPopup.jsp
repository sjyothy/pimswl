<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	function sendDataToParent()
	{
		
		window.opener.onMessageFromModifyAssetPopup();
		
	  		  
	}	
	function closeWindow()
	{
		 window.close();
	}
	        
	function disableButtons(control)
	{
			
			var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
			
			control.nextSibling.nextSibling.onclick();
			
			return 
		
	}
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		    document.getElementById("frm:hdnPersonId").value = personId;
		    document.getElementById("frm:hdnPersonName").value = personName;
		    document.getElementById("frm:onMessageFromSearchPerson").onclick();
	}
    function   showManagerSearchPopup()
	{
		   var screen_width = 1024;
		   var screen_height = 470;
		   var screen_top = screen.top;
		     window.open('SearchPerson.jsf?viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
		    
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">

				<tr>
					<td valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel
										value="#{msg['modifyEstateLimitationAsset.hdng.ModifyAsset']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="frm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION" style="width: 95%;">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:messages styleClass="ERROR_FONT"></h:messages>
															<h:outputText id="errorMessages"
																value="#{pages$modifyEstateLimitationAssetsPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$modifyEstateLimitationAssetsPopup.successMessages}"
																escape="false" styleClass="INFO_FONT" />
															<h:inputHidden id="hdnPersonId"
																value="#{pages$modifyEstateLimitationAssetsPopup.hdnPersonId}" />

															<h:inputHidden id="hdnPersonName"
																value="#{pages$modifyEstateLimitationAssetsPopup.hdnPersonName}" />

															<h:commandLink id="onMessageFromSearchPerson"
																action="#{pages$modifyEstateLimitationAssetsPopup.onMessageFromSearchPerson}" />
														</td>
													</tr>
												</table>
											</div>

											<div class="POPUP_DETAIL_SECTION" style="width: 95%;">


												<h:panelGrid border="0" width="95%" cellpadding="1"
													cellspacing="1" id="basicAssetsInfoGrid">
													<t:panelGroup>
														<%--Column 1,2 Starts--%>
														<h:panelGrid columns="4"
															columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
															width="100%">

															<h:outputText value="#{msg['searchAssets.assetNumber']}" />
															<h:inputText readonly="true" styleClass="READONLY"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.assetNumber}" />


															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" />
																<h:outputText value="#{msg['searchAssets.assetType']}" />
															</h:panelGroup>

															<%--value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.assetTypeView.assetTypeIdString}"
																	This will not work here, it returns the default value i.e= -1 --%>
															<h:selectOneMenu
																disabled="#{pages$modifyEstateLimitationAssetsPopup.pageMode == 'edit'}"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.assetTypeView.assetTypeIdString}"
																styleClass="SELECT_MENU"
																valueChangeListener="#{pages$modifyEstateLimitationAssetsPopup.onAssetTypeChange}"
																onchange="submit();">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.assetTypesList}" />

															</h:selectOneMenu>

															<h:outputLabel value="#{msg['searchAssets.assetNameEn']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.assetNameEn}"></h:inputText>

															<h:outputLabel value="#{msg['searchAssets.assetNameAr']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.assetNameAr}"></h:inputText>


															<h:outputLabel value="#{msg['searchAssets.assetDesc']}" />
															<t:panelGroup colspan="3" style="width: 100%">
																<t:inputTextarea style="width: 90%;"
																	value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.description}"/>
															</t:panelGroup>
															
															<h:outputLabel value="" />
															<h:outputLabel value="" />
															
															<h:outputLabel
																value="#{msg['inheritanceFile.inheritedAssetTab.label.isManagerAmaf']}" />
															<h:selectBooleanCheckbox id="isManagerAmafId"
																styleClass="SELECT_MENU"
																disabled="#{pages$modifyEstateLimitationAssetsPopup.pageMode == 'view'}"
																binding="#{pages$modifyEstateLimitationAssetsPopup.chkIsManagerAmaf}">
																<a4j:support
																	action="#{pages$modifyEstateLimitationAssetsPopup.onManagerAmafClick}"
																	event="onclick"
																	reRender="basicAssetsInfoGrid,managerId" />
															</h:selectBooleanCheckbox>

															<h:outputLabel
																value="#{msg['inheritanceFile.inheritedAssetTab.label.manager']}" />
															<h:panelGroup>
																<h:inputText readonly="true"
																	value="#{pages$modifyEstateLimitationAssetsPopup.hdnPersonName}"
																	styleClass="READONLY" />

																<h:graphicImage
																	rendered="#{pages$modifyEstateLimitationAssetsPopup.pageMode != 'view' }"
																	title="#{msg['inheritanceFile.inheritedAssetTab.tooltip.searchManager']}"
																	style="MARGIN: 1px 1px -3px;cursor:hand;"
																	onclick="showManagerSearchPopup();"
																	url="../resources/images/app_icons/Search-tenant.png" />
															</h:panelGroup>

															<h:outputLabel id="lblAssetTotalshare"
																value="#{msg['inheritanceFile.label.totalAssetShare']}" />
															<h:inputText id="txtAssetTotalShare"
																onkeyup="removeNonInteger(this)"
																onkeypress="removeNonInteger(this)"
																onkeydown="removeNonInteger(this)"
																onblur="removeNonInteger(this)"
																onchange="removeNonInteger(this)"
																value="#{pages$modifyEstateLimitationAssetsPopup.inheritedAssetView.totalShareValueString }" />
														</h:panelGrid>

													</t:panelGroup>

												</h:panelGrid>

												<h:panelGrid border="0" width="95%" cellpadding="1"
													cellspacing="1" id="ExtraDetailWithEachAssetType">

													<t:panelGroup>

														<t:div style="width:100%;background-color:#636163;"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.landProperties}">
															<h:outputText value="#{msg['inhFile.label.LandnProp']}"
																styleClass="GROUP_LABEL"></h:outputText>
														</t:div>
														<h:panelGrid columns="4" id="landAndPropertiesPanel"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.landProperties}"
															columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
															width="100%">

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.govDept']}" />
															<h:selectOneMenu
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.govtDepttView.govtDepttIdString}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.govtDepttList}" />
															</h:selectOneMenu>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.land']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldNumber}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.region']}" />
															<h:inputText maxlength="30"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.regionName}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.devolution']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.devolution}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.landStatus']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldStatus}"></h:inputText>
														</h:panelGrid>
														<t:div style="width:100%;background-color:#636163;"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.animals}">
															<h:outputText value="#{msg['inhFile.label.animal']}"
																styleClass="GROUP_LABEL"></h:outputText>
														</t:div>
														<h:panelGrid columns="4" id="animalsPanel"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.animals}"
															columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
															width="100%">

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.nofAnimal']}" />
															<h:inputText maxlength="150"
																onkeyup="removeNonInteger(this)"
																onkeypress="removeNonInteger(this)"
																onkeydown="removeNonInteger(this)"
																onblur="removeNonInteger(this)"
																onchange="removeNonInteger(this)"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldNumber}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.animalType']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldType}"></h:inputText>
														</h:panelGrid>
														<t:div style="width:100%;background-color:#636163;"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.vehicles}">
															<h:outputText value="#{msg['inhFile.label.vehicles']}"
																styleClass="GROUP_LABEL"></h:outputText>
														</t:div>
														<h:panelGrid columns="4" id="vehiclePanel"
															columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
															width="100%"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.vehicles}">

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.govDept']}" />
															<h:selectOneMenu
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.govtDepttView.govtDepttIdString}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.govtDepttList}" />
															</h:selectOneMenu>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.plateNmber']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldNumber}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.vehicleCat']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.vehicleCategory}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.vehicleType']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldType}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.vehicleStatus']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldStatus}"></h:inputText>
														</h:panelGrid>
														<t:div style="width:100%;background-color:#636163;"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.transportation}">
															<h:outputText
																value="#{msg['inhFile.label.transportation']}"
																styleClass="GROUP_LABEL"></h:outputText>
														</t:div>
														<h:panelGrid columns="4" id="transportationPanel"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.transportation}"
															columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
															width="100%">

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.plateNmber']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldNumber}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.income']}" />
															<h:inputText maxlength="50"
																onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.amountString}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.transferParty']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldParty}"></h:inputText>
														</h:panelGrid>
														<t:div style="width:100%;background-color:#636163;"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.stockshares}">
															<h:outputText value="#{msg['inhFile.label.stocknShare']}"
																styleClass="GROUP_LABEL"></h:outputText>
														</t:div>
														<h:panelGrid columns="4" id="stockAndSharesPanel"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.stockshares}"
															columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
															width="100%">

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.company']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.company}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.nofShare']}" />
															<h:inputText maxlength="50"
																onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldNumber}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.listingParty']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldParty}"></h:inputText>

														</h:panelGrid>
														<t:div style="width:100%;background-color:#636163;"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.licenses}">
															<h:outputText value="#{msg['inhFile.label.license']}"
																styleClass="GROUP_LABEL"></h:outputText>
														</t:div>
														<h:panelGrid columns="4" id="licensePanel"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.licenses}"
															columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
															width="100%">

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.licenseName']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.licenseName}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.licenseNmber']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldNumber}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.licenseStatus']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldStatus}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.rentValue']}" />
															<h:inputText onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.amountString}"></h:inputText>
														</h:panelGrid>
														<t:div style="width:100%;background-color:#636163;"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.loanLiabilities}">
															<h:outputText
																value="#{msg['inhFile.label.loanliability']}"
																styleClass="GROUP_LABEL"></h:outputText>
														</t:div>
														<h:panelGrid columns="4" id="loanAndLiabilities"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.loanLiabilities}"
															columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
															width="100%">
															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.party']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldParty}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
															<h:inputText onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.amountString}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.dueDate']}" />
															<rich:calendar
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.dueDate}"
																locale="#{pages$modifyEstateLimitationAssetsPopup.locale}"
																popup="true"
																datePattern="#{pages$modifyEstateLimitationAssetsPopup.dateFormat}"
																showApplyButton="false" enableManualInput="false"
																inputStyle="width:185px;height:17px" />
														</h:panelGrid>
														<t:div style="width:100%;background-color:#636163;"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.jewellery}">
															<h:outputText value="#{msg['inhFile.label.jewellery']}"
																styleClass="GROUP_LABEL"></h:outputText>
														</t:div>
														<h:panelGrid columns="4" id="jewelleryPanel"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.jewellery}"
															columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
															width="100%">

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.jewelType']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldType}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.wightInGms']}" />
															<h:inputText onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.weightString}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.nofJewel']}" />
															<h:inputText maxlength="50"
																onkeyup="removeNonInteger(this)"
																onkeypress="removeNonInteger(this)"
																onkeydown="removeNonInteger(this)"
																onblur="removeNonInteger(this)"
																onchange="removeNonInteger(this)"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldNumber}"></h:inputText>
														</h:panelGrid>
														<t:div style="width:100%;background-color:#636163;"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.cash}">
															<h:outputText value="#{msg['inhFile.label.cash']}"
																styleClass="GROUP_LABEL"></h:outputText>
														</t:div>
														<h:panelGrid columns="4" id="cashPanel"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.cash}"
															columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
															width="100%">

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
															<h:inputText onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.amountString}"></h:inputText>
														</h:panelGrid>

														<t:div style="width:100%;background-color:#636163;"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.bankTransfer}">
															<h:outputText
																value="#{msg['inhFile.label.bankTransfer']}"
																styleClass="GROUP_LABEL"></h:outputText>
														</t:div>
														<h:panelGrid columns="4" id="bankTransferPanel"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.bankTransfer}"
															columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
															width="100%">

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.transferNmber']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldNumber}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
															<h:inputText onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.amountString}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.bank']}" />
															<h:selectOneMenu
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.bankView.bankIdString}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$ApplicationBean.bankList}" />
															</h:selectOneMenu>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.expTransferDate']}" />
															<rich:calendar
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.dueDate}"
																locale="#{pages$modifyEstateLimitationAssetsPopup.locale}"
																popup="true"
																datePattern="#{pages$modifyEstateLimitationAssetsPopup.dateFormat}"
																showApplyButton="false" enableManualInput="false"
																inputStyle="width:185px;height:17px" />


														</h:panelGrid>

														<t:div style="width:100%;background-color:#636163;"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.cheque}">
															<h:outputText value="#{msg['inhFile.label.cheque']}"
																styleClass="GROUP_LABEL"></h:outputText>
														</t:div>
														<h:panelGrid columns="4" id="chequePanel"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.cheque}"
															columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
															width="100%">

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
															<h:inputText onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.amountString}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.bank']}" />
															<h:selectOneMenu
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.bankView.bankIdString}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$ApplicationBean.bankList}" />
															</h:selectOneMenu>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.chequeNmbr']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.fieldNumber}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.dueDate']}" />
															<rich:calendar
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.dueDate}"
																locale="#{pages$modifyEstateLimitationAssetsPopup.locale}"
																popup="true"
																datePattern="#{pages$modifyEstateLimitationAssetsPopup.dateFormat}"
																showApplyButton="false" enableManualInput="false"
																inputStyle="width:185px;height:17px" />

														</h:panelGrid>
														<t:div style="width:100%;background-color:#636163;"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.pension}">
															<h:outputText value="#{msg['inhFile.label.Pension']}"
																styleClass="GROUP_LABEL"></h:outputText>
														</t:div>
														<h:panelGrid columns="4" id="pensionPanel"
															rendered="#{pages$modifyEstateLimitationAssetsPopup.pension}"
															columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
															width="100%">

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
															<h:inputText onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.amountString}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.govDept']}" />
															<h:selectOneMenu
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.govtDepttView.govtDepttIdString}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.govtDepttList}" />
															</h:selectOneMenu>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.bank']}" />
															<h:selectOneMenu
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.bankView.bankIdString}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$ApplicationBean.bankList}" />
															</h:selectOneMenu>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.accountName']}" />
															<h:inputText maxlength="150"
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.accountName}"></h:inputText>

															<h:outputLabel
																value="#{msg['mems.inheritanceFile.fieldLabel.amafAccount']}" />
															<h:selectBooleanCheckbox
																value="#{pages$modifyEstateLimitationAssetsPopup.assetMemsView.isAmafAccountBool}"></h:selectBooleanCheckbox>
														</h:panelGrid>

													</t:panelGroup>
												</h:panelGrid>
												<t:div styleClass="contentDiv" id="parentDivs"
													style="width:95%;margin-top:10px;"
													rendered="#{
																pages$modifyEstateLimitationAssetsPopup.pageMode == 'add'||
																pages$modifyEstateLimitationAssetsPopup.pageMode == 'edit'||
																pages$modifyEstateLimitationAssetsPopup.pageMode == 'view'
															   }">
													<t:dataTable id="beneficiaryAssetsGrid" rows="300"
														binding="#{pages$modifyEstateLimitationAssetsPopup.dataTableShares}"
														value="#{pages$modifyEstateLimitationAssetsPopup.listShares}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%">

														<t:column sortable="true">
															<f:facet name="header">
																<h:outputText value="#{msg['commons.Name']}" />
															</f:facet>
															<h:outputText value="#{dataItem.beneficiaryName}" />
														</t:column>
														<t:column sortable="true">
															<f:facet name="header">
																<h:outputText
																	value="#{msg['settlement.label.costCenter']}" />
															</f:facet>
															<h:outputText value="#{dataItem.costCenter}" />
														</t:column>
														<t:column sortable="true">
															<f:facet name="header">
																<h:outputText
																	value="#{msg['distribution.shariaPercent']}" />
															</f:facet>
															<h:outputText value="#{dataItem.shariaShare}" />
														</t:column>
														<t:column sortable="true">
															<f:facet name="header">
																<h:outputText
																	value="#{msg['distribution.assetPercent']}" />
															</f:facet>
															<h:outputText value="#{dataItem.assetShareOld}" />
														</t:column>
														<t:column sortable="true">
															<f:facet name="header">
																<h:outputText
																	value="#{msg['modifyEstateLimitationAsset.newAssetPercentage']}" />
															</f:facet>
															<h:inputText
																rendered="#{pages$modifyEstateLimitationAssetsPopup.pageMode != 'view'}"
																value="#{dataItem.assetShareNew}" />
															<h:outputText
																rendered="#{pages$modifyEstateLimitationAssetsPopup.pageMode == 'view'}"
																value="#{dataItem.assetShareNew}" />
														</t:column>

													</t:dataTable>
												</t:div>

												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td height="5px"></td>
													</tr>
													<tr>
														<td class="BUTTON_TD MARGIN" colspan="6">
															<h:commandButton id="idSaveBtn"
																rendered="#{pages$modifyEstateLimitationAssetsPopup.pageMode=='edit'}"
																value="#{msg['commons.saveButton']}" styleClass="BUTTON"
																action="#{pages$modifyEstateLimitationAssetsPopup.onSave}">
															</h:commandButton>
															<h:commandButton id="idAddBtn"
																rendered="#{pages$modifyEstateLimitationAssetsPopup.pageMode=='add'}"
																value="#{msg['commons.Add']}" styleClass="BUTTON"
																action="#{pages$modifyEstateLimitationAssetsPopup.onAdd}">
															</h:commandButton>
															<h:commandButton id="idCloseBtn"
																value="#{msg['commons.closeButton']}"
																styleClass="BUTTON"
																action="#{pages$modifyEstateLimitationAssetsPopup.onClose}">
															</h:commandButton>
														</td>
													</tr>
												</table>
											</div>
									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>
		</body>
	</html>
</f:view>