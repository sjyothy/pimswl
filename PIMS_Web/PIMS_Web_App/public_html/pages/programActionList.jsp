
<script language="JavaScript" type="text/javascript">	
</script>

<t:panelGrid id="actionListGrdFields"
	styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%"
	cellspacing="5px" columns="4">

	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['socialProgram.lbl.ActionList']}" />
	</h:panelGroup>
	<h:inputTextarea
		value="#{pages$programActionList.programActionList.description}"
		style="width: 185px;" />



</t:panelGrid>
<t:panelGrid id="actionListGrdActions" styleClass="BUTTON_TD"
	cellpadding="1px" width="100%" cellspacing="5px">
	<h:panelGroup>
		<h:commandButton styleClass="BUTTON"
			value="#{msg['commons.saveButton']}"
			action="#{pages$programActionList.onSave}" />
	</h:panelGroup>
</t:panelGrid>


<t:div styleClass="contentDiv" style="width:98%"
	id="actionListContentDiv">
	<t:dataTable id="actionListsDataTable" rows="100"
		value="#{pages$programActionList.list}"
		binding="#{pages$programActionList.dataTable}"
		preserveDataModel="false" preserveSort="false" var="dataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">



		<t:column id="actionListdate" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText
					value="#{msg['socialProgram.ActionListGrid.lbl.Date']}"
					style="white-space: normal;" />
			</f:facet>

			<t:outputText style="white-space: normal;"
				value="#{dataItem.createdOn}"
				rendered="#{dataItem.isDeleted != '1' }">
				<f:convertDateTime pattern="#{pages$programActionList.dateFormat}"
					timeZone="#{pages$programActionList.timeZone}" />
			</t:outputText>
		</t:column>
		<t:column id="actionListCreatedBy" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText
					value="#{msg['socialProgram.ActionListGrid.lbl.CreatedBy']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				value="#{dataItem.createdEn}"
				rendered="#{dataItem.isDeleted != '1' }" />
		</t:column>
		<t:column id="actionListdesc" width="55%" sortable="true">
			<f:facet name="header">
				<t:outputText
					value="#{msg['socialProgram.ActionListGrid.lbl.Desc']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText value="#{dataItem.description}"
				style="white-space: normal;"
				rendered="#{dataItem.isDeleted != '1' }" />
		</t:column>
		<t:column id="programActionColumn" width="15%">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.action']}" />
			</f:facet>
			<h:graphicImage id="imgDeleteAction"
				rendered="#{dataItem.isDeleted != '1' }"
				title="#{msg['commons.delete']}"
				url="../resources/images/delete_icon.png"
				onclick="if (!confirm('#{msg['commons.confirmDelete']}')) return false; else onDeleted(this);">
			</h:graphicImage>
			<a4j:commandLink id="onActionDeleted"
				reRender="hiddenFields,actionListContentDiv,actionListsDataTable,actionListDataTableFooter,successmsg,errormsg"
				onbeforedomupdate="javaScript:onRemoveDisablingDiv(); "
				action="#{pages$programActionList.onDelete}" />
		</t:column>
	</t:dataTable>
</t:div>
