<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	
	function performClick(control)
	{
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++)
		{
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		       )
		    {
		        inputs[i].disabled = true;
		    }
		}
	}
		
	function onMessageFromEndowmentPrograms()
	{
	  disableInputs();
	  document.getElementById("detailsFrm:onMessageFromEndowmentPrograms").onclick();
	}
	
	function onSelectionChanged(changedChkBox)
	{
		document.getElementById('detailsFrm:disablingDiv').style.display='block';
		document.getElementById('detailsFrm:lnkCommunityChanged').onclick();
	}
	function onChkChange()
	{
	    document.getElementById('detailsFrm:disablingDiv').style.display='none';
	}
	function onSystemGeneratedChanged(checkbox)
	{
		if(checkbox.checked)
		{
			document.getElementById('detailsFrm:txtNumber').value='';
			//document.getElementById('detailsFrm:txtNumber').readOnly=true;
		    document.getElementById('detailsFrm:txtNumber').className='READONLY';
		}
		else
		{
			//document.getElementById('detailsFrm:txtNumber').readOnly=false;
		    document.getElementById('detailsFrm:txtNumber').className='';
		}
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{pages$donationBox.pageTitle}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">
												<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>
																<h:outputText id="errorId"
																	value="#{pages$donationBox.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$donationBox.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$donationBox.pageMode}"></h:inputHidden>
																<h:commandLink id="onMessageFromEndowmentPrograms"
																	action="#{pages$donationBox.onMessageFromEndowmentProgramSearch}" />


																<a4j:commandLink id="lnkCommunityChanged"
																	onbeforedomupdate="javascript:onChkChange();"
																	action="#{pages$donationBox.onCommunityChanged}"
																	reRender="basicInfoFields,errorId,successId,txtNumberGrp" />
															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<t:div rendered="true" style="width:100%;">
														<t:panelGrid cellpadding="1px" width="100%"
															id="basicInfoFields" cellspacing="5px" columns="4">

															<h:outputLabel styleClass="LABEL"
																value="#{msg['donationBox.lbl.boxNum']}:" />
															<h:panelGroup id="txtNumberGrp">
																<h:inputText id="txtNumber" maxlength="20"
																	binding="#{pages$donationBox.txtBoxNumber}"
																	value="#{pages$donationBox.donationBox.boxNumber}" />
																<h:selectBooleanCheckbox id="systemGenerated"
																	title="#{msg['donationBox.lbl.systemGenerated']}"
																	onclick="javascript:onSystemGeneratedChanged(this);"
																	rendered="#{empty pages$donationBox.donationBox.boxId  }"
																	value="#{pages$donationBox.donationBox.generateBySystem}" />
															</h:panelGroup>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['donationBox.lbl.boxCategory']}:" />
															<h:selectOneMenu id="cmbBoxCategory"
																value="#{pages$donationBox.donationBox.boxCategory}"
																tabindex="3">
																<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.boxCategory}" />
															</h:selectOneMenu>


															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.status']}:" />
															<h:inputText id="txtStatus"
																value="#{pages$donationBox.englishLocale? 
																	     pages$donationBox.donationBox.statusEn:
																	     pages$donationBox.donationBox.statusAr
																	    }"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['donationBox.lbl.registerDate']}:" />
															<h:inputText id="txtregistrationDate" readonly="true"
																value="#{pages$donationBox.donationBox.registrationDate}"
																styleClass="READONLY" />
<%-- 
															<h:outputLabel styleClass="LABEL"
																value="#{msg['donationBox.label.place']}:" />
															<h:selectOneMenu id="cmbPlace"
																value="#{pages$donationBox.donationBox.place}"
																tabindex="3">
																<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.donationBoxSubPlaces}" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['donationBox.label.community']}"></h:outputLabel>

															<h:selectOneMenu id="communityId"
																onchange="onSelectionChanged(this);"
																value="#{pages$donationBox.donationBox.community}">

																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItem itemValue="-2"
																	itemLabel="#{msg['researchFormBenef.others']}" />

																<f:selectItems
																	value="#{pages$ApplicationBean.dubaiCommunity}" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['researchFormBenef.others']}:" />
															<h:inputText id="txtOtherDesc" maxlength="50"
																binding="#{pages$donationBox.txtOtherDesc}"
																value="#{pages$donationBox.donationBox.otherDesc}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['donationBox.lbl.address']}" />
															<t:panelGroup colspan="3" style="width: 100%">
																<t:inputTextarea style="width: 90%;" id="txtAddress"
																	value="#{pages$donationBox.donationBox.address}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</t:panelGroup>
 --%>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['donationBox.lbl.respEmp']}:" />
															<h:selectOneMenu id="cmbResponsibleEmp"
																value="#{pages$donationBox.donationBox.responsibleEmp}"
																tabindex="3">
																<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.secUserList}" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['donationBox.lbl.contactPerson']}:" />
															<h:inputText id="txtContactPerson" maxlength="50"
																value="#{pages$donationBox.donationBox.contacnPerson}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['donationBox.lbl.contactNum']}:" />
															<h:inputText id="txtName" maxlength="14"
																value="#{pages$donationBox.donationBox.contactNumber}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.description']}" />
															<t:panelGroup colspan="3" style="width: 100%">
																<t:inputTextarea style="width: 90%;" id="txt_desc"
																	value="#{pages$donationBox.donationBox.description}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</t:panelGroup>


															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" />
																<h:outputLabel id="lblBenef" styleClass="LABEL"
																	value="#{msg['donationRequest.lbl.endowmentProgram']}" />
															</h:panelGroup>
															<h:panelGroup>
																<h:inputText id="program" maxlength="20" readonly="true"
																	styleClass="READONLY"
																	value="#{pages$donationBox.donationBox.endowmentProgramView.progName}" />
																<h:graphicImage id="endowmentProgramsSearchpopup"
																	rendered="#{pages$donationBox.pageMode!='PAGE_MODE_CANCELLED'}"
																	style="margin-right:5px;"
																	title="#{msg['endowmentProgram.title.search']}"
																	url="../resources/images/app_icons/Settle-Contract.png"
																	onclick="javaScript:openEndowmentProgramsPopup();" />

															</h:panelGroup>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['donationBox.lbl.distributedBy']}:" />
															<h:inputText id="txtDistributor" styleClass="READONLY"
																readonly="true"
																value="#{pages$donationBox.donationBox.distributor}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['donationBox.lbl.distributedOn']}:" />
															<h:inputText id="txtDistributedOn" styleClass="READONLY"
																readonly="true"
																value="#{pages$donationBox.donationBox.distributedOnStr}" />


														</t:panelGrid>
													</t:div>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel binding="#{pages$donationBox.tabPanel}"
																style="width: 100%">

																<rich:tab id="distributionHistory"
																	label="#{msg['donationBox.lbl.distributionHistory']}">
																	<%@ include file="tabBoxDistributionHistory.jsp"%>
																</rich:tab>
																
																<rich:tab id="programHistory"
																	rendered="#{pages$donationBox.showProgramHistory}"
																	label="#{msg['donationRequest.tab.programHistory']}">
																	<%@ include file="tabBoxProgramHistory.jsp"%>
																</rich:tab>

																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>

																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>

																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$donationBox.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">


															<pims:security screen="Pims.EndowMgmt.DonationBox.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$donationBox.showSaveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$donationBox.onSave}" />
															</pims:security>
															<pims:security
																screen="Pims.EndowMgmt.DonationBox.Register"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$donationBox.showRegistrationButton}"
																	value="#{msg['commons.register']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSubmit"
																	action="#{pages$donationBox.onRegister}" />

															</pims:security>



														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>