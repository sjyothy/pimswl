<script language="JavaScript" type="text/javascript">
		function disableButton(control)
		{
			
			var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
			control.nextSibling.onclick();
			
			return 
		
		}
</script>
<t:div id="tabNormalDetails" style="width:100%;">
	<t:panelGrid id="ddTab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblPaymenType" styleClass="LABEL"
				value="#{msg['mems.normaldisb.label.paymentType']}" />
		</h:panelGroup>
		<h:selectOneMenu id="cmbPymtId"
			binding="#{pages$NormalDisbursement.cmbPaymentTypes}"
			style="width: 200px;">
			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems value="#{pages$NormalDisbursement.paymentTypes}" />
			<a4j:support event="onchange"
				reRender="paymentSrc,layoutTable,errorId"
				action="#{pages$NormalDisbursement.handlePaymentTypeChange}" />
		</h:selectOneMenu>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="amnt" styleClass="LABEL"
				value="#{msg['commons.amount']}:"></h:outputLabel>
		</h:panelGroup>
		<h:inputText id="inAmnt"
			binding="#{pages$NormalDisbursement.txtAmount}" maxlength="20" />

		<h:outputLabel id="lblPaymentSrc" styleClass="LABEL"
			value="#{msg['mems.normaldisb.label.paymentSrc']}" />
		<h:selectOneMenu id="paymentSrc"
			binding="#{pages$NormalDisbursement.cmbPaymentSource}"
			style="width: 200px;">
			<f:selectItems value="#{pages$NormalDisbursement.paymentSource}" />
			<a4j:support event="onchange"
				reRender="disbSrcId,cmbDisbSrc,inBalance,cmbPaidTo,vndor,inVendor,blockinBalance,availableBalance,layoutTable,errorId"
				action="#{pages$NormalDisbursement.handlePaymentSourceChange}" />
		</h:selectOneMenu>

		<h:outputLabel id="lblBenef" styleClass="LABEL"
			value="#{msg['mems.normaldisb.label.beneficiary']}"></h:outputLabel>

		<h:selectOneMenu id="cmbBeneficiary"
			binding="#{pages$NormalDisbursement.cmbBeneficiaryList}"
			readonly="#{pages$NormalDisbursement.isCompleted ||
						pages$NormalDisbursement.isReviewRequired ||
			            pages$NormalDisbursement.isDisbursementRequired|| 
			            !pages$NormalDisbursement.isFileAssociated}"
			style="width: 200px;">
			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItem itemLabel="--" itemValue="all" />
			<f:selectItems value="#{pages$NormalDisbursement.beneficiaryList}" />
			<a4j:support event="onchange"
				reRender="accountBalance,blockinBalance,availableBalance,requestedAmount,noOfBenef,layoutTable,errorId"
				action="#{pages$NormalDisbursement.handleBeneficiaryChange}" />
		</h:selectOneMenu>
		<h:outputLabel id="labelNoofBenef" styleClass="LABEL"
			value="#{msg['mems.normaldisb.label.noOfBenef']}:">
		</h:outputLabel>
		<h:inputText id="noOfBenef" readonly="true" styleClass="READONLY"
			binding="#{pages$NormalDisbursement.noOfBenef}">
		</h:inputText>
		<h:outputLabel id="labelBalance" styleClass="LABEL"
			value="#{msg['mems.normaldisb.label.balance']}:">
		</h:outputLabel>
		<h:inputText id="accountBalance" readonly="true" styleClass="READONLY"
			binding="#{pages$NormalDisbursement.accountBalance}">
		</h:inputText>
		<h:outputLabel id="lblBlockBalance" styleClass="LABEL"
			value="#{msg['blocking.lbl.blockedamount']}"></h:outputLabel>
		<h:panelGroup>
			<h:inputText id="blockinBalance" styleClass="READONLY"
				readonly="true"
				binding="#{pages$NormalDisbursement.blockingBalance}" maxlength="20" />
			<h:commandLink id="lnkBlockingDetails"
				action="#{pages$NormalDisbursement.onOpenBlockingDetailsPopup}">
				<h:graphicImage id="openBlockingDetailsPopup"
					style="margin-right:5px;"
					title="#{msg['blocking.lbl.blockingDetails']}"
					url="../resources/images/app_icons/manage_tender.png" />
			</h:commandLink>
		</h:panelGroup>

		<h:outputLabel id="labelrequestedAmount" styleClass="LABEL"
			value="#{msg['commons.requestedAmount']}:">
		</h:outputLabel>
		<h:panelGroup>
			<h:inputText id="requestedAmount" readonly="true"
				styleClass="READONLY"
				binding="#{pages$NormalDisbursement.requestedAmount}">
			</h:inputText>
			<h:commandLink id="lnkDisbDetails"
				action="#{pages$NormalDisbursement.onOpenBeneficiaryDisbursementDetailsPopup}">
				<h:graphicImage id="onOpenBeneficiaryDisbursementDetailsPopup"
					style="margin-right:5px;"
					title="#{msg['commons.disbursementDetails']}"
					url="../resources/images/app_icons/manage_tender.png" />
			</h:commandLink>
		</h:panelGroup>

		<h:outputLabel id="labelAVBalance" styleClass="LABEL"
			value="#{msg['commons.availableBalance']}:">
		</h:outputLabel>
		<h:inputText id="availableBalance" readonly="true"
			styleClass="READONLY"
			binding="#{pages$NormalDisbursement.availableBalance}">
		</h:inputText>

	</t:panelGrid>
	<t:panelGrid id="descTab" cellpadding="5px" width="100%"
		columnClasses="APP_DETAIL_TWO_COL_C1, APP_DETAIL_TWO_COL_C2"
		cellspacing="10px" columns="2" styleClass="TAB_DETAIL_SECTION_INNER">
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblDescription" styleClass="LABEL"
				style="width: 25%" value="#{msg['commons.description']}" />
		</h:panelGroup>
		<h:inputTextarea id="invDetails"
			binding="#{pages$NormalDisbursement.txtDescription}"
			style="width: 97%; height: 70px;" />
	</t:panelGrid>
	<t:panelGrid id="mainReasonGrid" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblMainReason" styleClass="LABEL"
				value="#{msg['commons.mainReason']}" />
		</h:panelGroup>
		<h:selectOneMenu id="cmbMainReasonId"
			binding="#{pages$NormalDisbursement.cmbMainReason}"
			value="#{pages$NormalDisbursement.selectOneMainReason}"
			style="width: 200px;">
			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems
				value="#{pages$ApplicationBean.disbursementMainReason}" />
			<a4j:support event="onchange"
				reRender="resTab,cmbReasonId,cmbItems,layoutTable,errorId"
				action="#{pages$NormalDisbursement.handleMainReasonChange}" />
		</h:selectOneMenu>
	</t:panelGrid>
	<t:panelGrid id="resTab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblReason" styleClass="LABEL"
				value="#{msg['commons.reasons']}" />
		</h:panelGroup>
		<h:selectOneMenu id="cmbReasonId"
			binding="#{pages$NormalDisbursement.cmbReasonTypes}"
			style="width: 200px;">
			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems value="#{pages$NormalDisbursement.reasonTypes}" />
			<a4j:support event="onchange" reRender="cmbItems,layoutTable,errorId"
				action="#{pages$NormalDisbursement.handleReasonChange}" />
		</h:selectOneMenu>

		<h:outputLabel id="lblItems" styleClass="LABEL"
			value="#{msg['mems.normaldisb.label.items']}"></h:outputLabel>

		<h:selectOneMenu id="cmbItems"
			binding="#{pages$NormalDisbursement.cmbItemTypes}"
			style="width: 200px;">
			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems value="#{pages$NormalDisbursement.itemTypes}" />
		</h:selectOneMenu>
	</t:panelGrid>

	<t:panelGrid id="disbSrcId" cellpadding="5px" width="100%"
		rendered="#{pages$NormalDisbursement.isApprovalRequired|| pages$NormalDisbursement.isFinanceRejected || pages$NormalDisbursement.isReviewed}"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblDisSrc" styleClass="LABEL"
				value="#{msg['commons.source']}" />
		</h:panelGroup>
		<h:selectOneMenu id="cmbDisbSrc"
			binding="#{pages$NormalDisbursement.cmbDisbursementSource}"
			style="width: 200px;">
			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems
				value="#{pages$NormalDisbursement.disbursementSources}" />
		</h:selectOneMenu>

		<h:outputLabel id="lblBalance" styleClass="LABEL"
			value="#{msg['mems.normaldisb.label.balance']}"></h:outputLabel>
		<h:inputText id="inBalance" styleClass="READONLY" readonly="true"
			binding="#{pages$NormalDisbursement.txtBalance}" maxlength="20" />


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblPaidTo" styleClass="LABEL"
				value="#{msg['mems.normaldisb.label.paidTo']}" />
		</h:panelGroup>
		<h:selectOneMenu id="cmbPaidTo"
			binding="#{pages$NormalDisbursement.cmbPaidTo}" style="width: 200px;">
			<f:selectItem itemLabel="#{msg['mems.normaldisb.label.beneficiary']}"
				itemValue="1" />
			<f:selectItem itemLabel="#{msg['mems.normaldisb.label.vendor']}"
				itemValue="0" />
			<a4j:support event="onchange" reRender="vndor,layoutTable,errorId"
				action="#{pages$NormalDisbursement.handlePaidToChange}" />
		</h:selectOneMenu>

		<h:outputLabel id="lblVendor" styleClass="LABEL"
			value="#{msg['mems.normaldisb.label.vendor']}"></h:outputLabel>
		<h:panelGroup id="vndor">
			<h:inputText id="inVendor" styleClass="READONLY" readonly="true"
				binding="#{pages$NormalDisbursement.txtVendor}" maxlength="20" />
			<h:graphicImage id="imgVendor" title="#{msg['commons.search']}"
				rendered="false" binding="#{pages$NormalDisbursement.imgVendor}"
				onclick="showVendorPopup();"
				style="MARGIN: 0px 0px -4px; cursor:hand;horizontal-align:right;"
				url="../resources/images/app_icons/Search-tenant.png">
			</h:graphicImage>
		</h:panelGroup>

	</t:panelGrid>


	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;">
		<h:commandButton styleClass="BUTTON"
			rendered="#{pages$NormalDisbursement.isNewStatus ||
			            pages$NormalDisbursement.isApprovalRequired || 
			            pages$NormalDisbursement.isReviewed ||
			            pages$NormalDisbursement.isApproved ||
			            pages$NormalDisbursement.isFinanceRejected || 
			            pages$NormalDisbursement.isCaringRejected}"
			onclick="disableButton(this);" value="#{msg['commons.saveButton']}">
		</h:commandButton>
		<h:commandLink id="lnkSaveBeneficiary"
			action="#{pages$NormalDisbursement.saveBeneficiary}" />
		<!--  when the request is required approval -->
		<%--
		<h:commandButton styleClass="BUTTON"
			rendered="#{pages$NormalDisbursement.isApprovalRequired || pages$NormalDisbursement.isReviewed}"
			action="#{pages$NormalDisbursement.saveBeneficiary}"
			value="#{msg['commons.saveButton']}">
		</h:commandButton>
		--%>
		<!--  when the request is approved -->
		<%-- 
		<h:commandButton styleClass="BUTTON"
			rendered="#{pages$NormalDisbursement.isApproved}"
			action="#{pages$NormalDisbursement.saveBeneficiaryWhenApproved}"
			value="#{msg['commons.saveButton']}">
		</h:commandButton>
		--%>
	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;">
		<t:dataTable id="disbursementDT" rows="15" width="100%"
			value="#{pages$NormalDisbursement.disbursementDetails}"
			binding="#{pages$NormalDisbursement.disbursementTable}"
			preserveDataModel="false" preserveSort="false" var="unitDataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
			<t:column id="checkbox" style="width:auto;white-space: normal;">
				<f:facet name="header">
				</f:facet>
				<h:selectBooleanCheckbox id="idbox" immediate="true"
					value="#{unitDataItem.selected}">
				</h:selectBooleanCheckbox>
			</t:column>
			<t:column id="refNumTd" sortable="true"
				style="white-space: normal;width:3%;">
				<f:facet name="header">
					<t:outputText id="lblRefNumTd" value="#{msg['commons.refNum']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="txtRefNumTd"
					styleClass="A_LEFT" value="#{unitDataItem.refNum}" />
			</t:column>
			<t:column id="idPmtType" sortable="true"
				style="white-space: normal;width:2%;">
				<f:facet name="header">
					<t:outputText id="hdPmtType" style="white-space: normal;"
						value="#{msg['mems.normaldisb.label.paymentType']}" />
				</f:facet>
				<t:outputText id="hdInsPmtType" styleClass="A_LEFT"
					value="#{pages$NormalDisbursement.isEnglishLocale?unitDataItem.paymentTypeEn:unitDataItem.paymentTypeAr}" />
			</t:column>
			<t:column id="idCurStatus" sortable="true"
				style="white-space: normal;width:3%;"
				rendered="#{ pages$NormalDisbursement.isApprovalRequired || 
						           pages$NormalDisbursement.isReviewed || 
						           pages$NormalDisbursement.isCaringRejected || 
						           pages$NormalDisbursement.isFinanceRejected ||
						           pages$NormalDisbursement.isApproved ||
						           pages$NormalDisbursement.isCompleted ||
						           pages$NormalDisbursement.isDisbursementRequired
						         }">
				<f:facet name="header">
					<t:outputText id="hdCurStatus" value="#{msg['commons.status']}" />
				</f:facet>
				<t:outputText id="hdInsCurStatus" styleClass="A_LEFT"
					value="#{pages$NormalDisbursement.isEnglishLocale?unitDataItem.statusEn:unitDataItem.statusAr}" />
			</t:column>
			<t:column id="beneficiary" sortable="true"
				style="white-space: normal;width:10%;">
				<f:facet name="header">
					<t:outputText id="hdBenef"
						value="#{msg['mems.normaldisb.label.beneficiary']}" />
				</f:facet>
				<t:commandLink
					actionListener="#{pages$NormalDisbursement.openManageBeneficiaryPopUp}"
					value="#{unitDataItem.beneficiariesName}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="amnt" sortable="true"
				style="white-space: normal;width:4%;">
				<f:facet name="header">
					<t:outputText id="hdAmnt" value="#{msg['commons.amount']}" />
				</f:facet>
				<t:outputText id="amountt" styleClass="A_LEFT"
					value="#{unitDataItem.amount}" />
			</t:column>
			<t:column id="pmtSrc" sortable="true"
				style="white-space: normal;width:6%;">
				<f:facet name="header">
					<t:outputText id="hdPmtSrc"
						value="#{msg['mems.normaldisb.label.paymentSrc']}" />
				</f:facet>
				<t:outputText id="paymentSrc" styleClass="A_LEFT"
					value="#{pages$NormalDisbursement.isEnglishLocale?unitDataItem.srcTypeEn:unitDataItem.srcTypeAr}" />
			</t:column>
			<t:column id="desc" sortable="true" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText id="hdTo" value="#{msg['commons.description']}" />
				</f:facet>
				<div style="width: 70%; white-space: normal;">
					<t:outputText id="p_w" styleClass="A_LEFT"
						style="white-space: normal;" value="#{unitDataItem.description}" />
				</div>
			</t:column>
			<t:column id="idReason" sortable="true"
				style="white-space: normal;width:8%;">
				<f:facet name="header">
					<t:outputText id="hdReason" value="#{msg['commons.reasons']}" />
				</f:facet>
				<t:outputText id="idInsReason" styleClass="A_LEFT"
					value="#{pages$NormalDisbursement.isEnglishLocale?unitDataItem.reasonEn:unitDataItem.reasonAr}" />
			</t:column>
			<t:column id="idItems" sortable="true"
				style="white-space: normal;width:8%;">
				<f:facet name="header">
					<t:outputText id="hdItems"
						value="#{msg['mems.normaldisb.label.items']}" />
				</f:facet>
				<t:outputText id="hdInsItems" styleClass="A_LEFT"
					value="#{pages$NormalDisbursement.isEnglishLocale?unitDataItem.itemEn:unitDataItem.itemAr}" />
			</t:column>
			<t:column id="idPaidTo" sortable="true"
				style="white-space: normal;width:5%;">
				<f:facet name="header">
					<t:outputText id="hdPaidTo"
						value="#{msg['mems.normaldisb.label.paidTo']}" />
				</f:facet>
				<div style="width: 70%; white-space: normal;">
					<t:outputText id="hdInsPaidTo" styleClass="A_LEFT"
						value="#{'0'==unitDataItem.paidTo?unitDataItem.vendorName:msg['mems.normaldisb.label.msg.paidToBenef']}" />
				</div>
			</t:column>
			<t:column id="idDisbSrcName" sortable="true"
				style="white-space: normal;width:6%;">
				<f:facet name="header">
					<t:outputText id="hdDisbSrcName"
						value="#{msg['mems.normaldisb.label.disbSrc']}" />
				</f:facet>
				<t:outputText id="hdInsDisbSrcName" styleClass="A_LEFT"
					value="#{pages$NormalDisbursement.isEnglishLocale?unitDataItem.disburseSrcNameEn:unitDataItem.disburseSrcNameAr}" />
			</t:column>
			<t:column id="idActions" sortable="true" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText id="hdActions" value="#{msg['commons.action']}" />
				</f:facet>
				<h:commandLink
					action="#{pages$NormalDisbursement.deleteBeneficiary}"
					rendered="#{!( pages$NormalDisbursement.isCompleted ||
								   pages$NormalDisbursement.isReviewRequired ||
			            		   pages$NormalDisbursement.isDisbursementRequired
			            		 ) && 1!=unitDataItem.isDisbursed
			            		}"
					onclick="if (!confirm('#{msg['mems.common.alertDelete']}')) return false;">
					<h:graphicImage id="deleteIconDisbursement"
						title="#{msg['commons.delete']}"
						url="../resources/images/delete.gif" width="18px;" />
				</h:commandLink>
				<h:commandLink action="#{pages$NormalDisbursement.editBeneficiary}">
					<h:graphicImage id="editIcon" title="#{msg['commons.edit']}"
						url="../resources/images/edit-icon.gif" width="18px;" />
				</h:commandLink>
				<!-- Disabling,after discussing with Omar as its making complicating scenario -->
				<%-- 
				<h:commandLink
					action="#{pages$NormalDisbursement.addPaymentScheduleFromPopup}"
					rendered="#{!( pages$NormalDisbursement.isNewStatus ||
					               pages$NormalDisbursement.isCompleted ||
								   pages$NormalDisbursement.isReviewRequired ||
			            		   pages$NormalDisbursement.isDisbursementRequired
			            		 )     
						           &&
						           1!=unitDataItem.isDisbursed
						           }">
					<h:graphicImage id="addSchedule"
						title="#{msg['mems.normaldisb.label.addPaymentSchedule']}"
						url="../resources/images/app_icons/Add-Payment-schedule.png"
						width="18px;" />
				</h:commandLink>
				--%>
				<h:commandLink
					action="#{pages$NormalDisbursement.addBeneficiaryFromPopup}"
					rendered="#{ 1==unitDataItem.isMultiple && 
					            !( pages$NormalDisbursement.isNewStatus || 
					               pages$NormalDisbursement.isReviewRequired 
			            		   
			            		 ) && 1!=unitDataItem.isDisbursed
			            		}">
					<h:graphicImage id="addBene"
						title="#{msg['mems.normaldisb.label.addBeneficiary']}"
						url="../resources/images/app_icons/Add-Person.png" width="18px;" />
				</h:commandLink>
				<h:commandLink
					action="#{pages$NormalDisbursement.openAssociateCostCenters}"
					rendered="#{ unitDataItem.showAssoCostCentersPopUp && 
								(pages$NormalDisbursement.isApproved || pages$NormalDisbursement.isFinanceRejected) && 
								0==unitDataItem.isMultiple && 1!=unitDataItem.isDisbursed 
							   }">
					<h:graphicImage id="associateCostCenterIcon"
						title="#{msg['commons.cmdLink.openAssociateCostCenter']}"
						url="../resources/images/app_icons/Add-Fee-sub-type.png"
						width="18px;" />
				</h:commandLink>
				<h:commandLink action="#{pages$NormalDisbursement.editDetails}"
					rendered="#{pages$NormalDisbursement.isRenderCaringApprove || pages$NormalDisbursement.isRenderResearcherApprove || pages$NormalDisbursement.isCompleted}">
					<h:graphicImage id="pymtIcon"
						title="#{msg['mems.payment.request.label.paymentDetails']}"
						url="../resources/images/app_icons/Pay-Fine.png" width="18px;" />
				</h:commandLink>

				<h:commandLink action="#{pages$NormalDisbursement.onOpenEmailPopup}"
					rendered="#{ 0 == unitDataItem.paidTo && !empty unitDataItem.vendorId   &&
					            !( pages$NormalDisbursement.isNewStatus || 
					               pages$NormalDisbursement.isReviewRequired  ||
			            		   pages$NormalDisbursement.isCompleted
			            		 ) && 1!=unitDataItem.isDisbursed
			            		}">
					<h:graphicImage id="sendEmail" title="#{msg['commons.send']}"
						url="../resources/images/app_icons/email.jpg" width="18px;" />
				</h:commandLink>

			</t:column>
		</t:dataTable>
	</t:div>

	<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99%;">
		<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
			cellspacing="1">
			<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
				<t:div styleClass="JUG_NUM_REC_ATT">
					<h:outputText value="#{msg['commons.records']}" />
					<h:outputText value=" : " />
					<h:outputText value="#{pages$NormalDisbursement.totalRows}" />
				</t:div>
			</t:panelGroup>
			<t:panelGroup>
				<t:dataScroller id="shareScrollers" for="disbursementDT"
					paginator="true" fastStep="1" immediate="false"
					paginatorTableClass="paginator" renderFacetsIfSinglePage="true"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false" pageIndexVar="pageNumber"
					styleClass="JUG_SCROLLER">
					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
					</f:facet>
					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
					</f:facet>
					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
					</f:facet>
					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
					</f:facet>

					<t:div>
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<h:outputText styleClass="PAGE_NUM"
										style="font-size:9;color:black;"
										value="#{msg['mems.normaldisb.label.totalAmount']}:" />
								</td>
								<td>
									<h:outputText styleClass="PAGE_NUM"
										style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:9;font-weight:bold;color:black"
										value="#{pages$NormalDisbursement.totalDisbursementCount}" />
								</td>
							</tr>
						</table>
					</t:div>
				</t:dataScroller>
			</t:panelGroup>
		</t:panelGrid>
	</t:div>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;"
		rendered="#{pages$NormalDisbursement.isRenderResearcherApprove}">
		<h:commandButton styleClass="BUTTON"
			action="#{pages$NormalDisbursement.acceptDisbursement}"
			value="#{msg['mems.normaldisb.label.accept']}"
			onclick="disableButton(this);">
		</h:commandButton>
		<h:commandLink id="lnkAcceptDisbursements"
			action="#{pages$NormalDisbursement.acceptDisbursement}" />
		<h:commandButton styleClass="BUTTON"
			value="#{msg['mems.normaldisb.label.reject']}"
			onclick="disableButton(this);">
		</h:commandButton>
		<h:commandLink id="lnkRejectDisbursements"
			action="#{pages$NormalDisbursement.rejectDisbursement}" />

		<h:commandButton styleClass="BUTTON" value="#{msg['commons.delete']}"
			onclick="if (!confirm('#{msg['mems.common.alertDelete']}')) return false; else disableButton(this); return true;">
		</h:commandButton>
		<h:commandLink id="lnkDeleteDisbursements"
			action="#{pages$NormalDisbursement.deleteSelected}" />
	</t:div>
</t:div>


