<%-- 
  - Author: Syed Hammad Afridi
  - Date:
  - Copyright Notice:
  - @(#)\
  - Description: Used for Searching an Auction
  --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

		function resetValues()
		{
			document.getElementById("searchFrm:txtPropertyNumber").value="";
			document.getElementById("searchFrm:commercialName").value="";
			document.getElementById("searchFrm:country").selectedIndex=0;
			document.getElementById("searchFrm:state").selectedIndex=0;
			document.getElementById("searchFrm:propertyType").selectedIndex=0;
			document.getElementById("searchFrm:propertyStatus").selectedIndex=0;
			document.getElementById("searchFrm:propertyUsage").selectedIndex=0;
			document.getElementById("searchFrm:unitRefNo").value="";
		}

		function submitForm()
		{
			document.getElementById('searchFrm').submit();
		}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg" />
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>	

		</head>

		<body>
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
				response.setHeader("Pragma", "no-cache"); //HTTP 1.0
				response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
					</td>
				</tr>

				<tr width="100%">
					<td width="100%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['property.search']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="99%" valign="top" nowrap="nowrap">
									<div style="height:450;overflow:auto;">
										<h:form id="searchFrm" style="width:99%">
										 <div class="MARGIN">

                                                      <table cellpadding="0" cellspacing="0" >

                                                            <tr>
                                                            <td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
                                                            <td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
                                                            <td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
                                                            </tr>

                                                      </table>

                                                      <div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
                                                      <table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">

												<tr>

													<td colspan="1">
														<h:outputLabel styleClass="LABEL"
															value="#{msg['inspection.propertyRefNum']}:"></h:outputLabel>
													</td>

													<td colspan="1">


														<h:inputText id="txtPropertyNumber"  
															value="#{pages$propertyList.propertyBean.propertyNumber}"
															style="width: 186px; height: 16px" maxlength="20"></h:inputText>

													</td>
													<td colspan="1">
														&nbsp;
													</td>
													<td colspan="1">
														&nbsp;
													</td>


													<td colspan="1">

														<h:outputLabel styleClass="LABEL" value="#{msg['contract.property.Name']}:"></h:outputLabel>
													</td>

													<td colspan="1">
														<h:inputText id="commercialName"
															value="#{pages$propertyList.propertyBean.commercialName}"
															style="width: 186px; height: 16px" maxlength="250"></h:inputText>

													</td>
													<td colspan="1">
													</td>
												</tr>
												<tr>
													<td colspan="1">
														<h:outputLabel styleClass="LABEL" value="#{msg['contact.country']}:"></h:outputLabel>
													</td>
													<td colspan="1" width="10%">
														<h:selectOneMenu id="country"  valueChangeListener="#{pages$propertyList.loadState}"  
														onchange="submitForm();" style="width: 192px; height: 24px" required="false" immediate="false"
															value="#{pages$propertyList.selectedCountryId}">
															<f:selectItem itemValue="0" itemLabel="All" />
															<f:selectItems
																value="#{pages$propertyList.countryList}" />
														</h:selectOneMenu>

													</td>
													<td colspan="1">



													</td>

													<td colspan="1">
														 
													</td>

													<td colspan="1">

														<h:outputLabel styleClass="LABEL" value="#{msg['contact.state']}:" />
													</td>
													<td colspan="1" width="10%">
															<h:selectOneMenu id="state"  
															style="width: 192px; height: 24px" required="false"
															value="#{pages$propertyList.stateId}">
															<f:selectItem itemValue="0" itemLabel="All" />
															<f:selectItems
																value="#{pages$propertyList.stateList}" />
														</h:selectOneMenu>

													</td>
													<td colspan="1">



													</td>
												</tr>
												<tr>

													<td colspan="1">
														<h:outputLabel styleClass="LABEL" value="#{msg['commons.status']}:"></h:outputLabel>
													</td>
												<td colspan="1">
														<h:selectOneMenu id="propertyStatus"
															style="width: 192px; height: 24px" required="false"
															value="#{pages$propertyList.statusId}">
															<f:selectItem itemValue="0" itemLabel="All" />
															<f:selectItems
																value="#{pages$ApplicationBean.propertyStatusList}" />
														</h:selectOneMenu>


													</td>
													<td colspan="1">
														&nbsp;
													</td>
													<td colspan="1">
														&nbsp;
													</td>
													<td colspan="1">

														<h:outputLabel styleClass="LABEL" value="#{msg['contract.unitRefNo']}:" />
													</td>
													<td colspan="1">
														<h:inputText id="unitRefNo"
															value="#{pages$propertyList.propertyBean.unitRefNo}"
															style="width: 186px; height: 16px"></h:inputText>
													</td>
													<td colspan="1">
													</td>
												</tr>
												<tr>

													<td colspan="1">

														<h:outputLabel styleClass="LABEL" value="#{msg['property.type']}:"></h:outputLabel>
													</td>

													<td colspan="1">
													<h:selectOneMenu id="propertyType"
															style="width: 192px; height: 24px" required="false"
															value="#{pages$propertyList.typeId}">
															
															<f:selectItem itemValue="0" itemLabel="All" />
															<f:selectItems
																value="#{pages$ApplicationBean.propertyTypeList}" />
														</h:selectOneMenu>

													</td>
													<td colspan="1">
														&nbsp;
													</td>
													<td colspan="1">
														&nbsp;
													</td>

<td colspan="1">

														<h:outputLabel styleClass="LABEL" value="#{msg['property.usage']}:" />
													</td>
													<td colspan="1" width="10%">
														<h:selectOneMenu id="propertyUsage"
															style="width: 192px; height: 24px" required="false"
															value="#{pages$propertyList.usageId}">
															<f:selectItem itemValue="0" itemLabel="All" />
															<f:selectItems
																value="#{pages$ApplicationBean.propertyUsageList}" />
														</h:selectOneMenu>

													</td>
												</tr>
												<tr>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td class="BUTTON_TD" colspan="7">
														<h:commandButton styleClass="BUTTON" id="submitButton"
															action="#{pages$propertyList.searchProperty}" immediate="false"
															value="#{msg['commons.search']}"
															style="width: 75px" tabindex="7"></h:commandButton>
														<h:commandButton styleClass="BUTTON"
															value="#{msg['commons.clear']}"
															onclick="javascript:resetValues();" style="width: 75px"
															tabindex="7"></h:commandButton>
													</td>
												</tr>
											</table>
	                                       </div>
                                           </div>
                                           <br><br>
											<div class="MARGIN">
											
											<div class="imag">
												&nbsp;
											</div>
											<div class="contentDiv" align="center">
												<t:dataTable id="dt1" styleClass="grid"
													value="#{pages$propertyList.propertyViewList}"
													binding="#{pages$propertyList.dataTable}" rows="3"
													preserveDataModel="true" preserveSort="false"
													var="dataItem" rowClasses="row1,row2" rules="all"
													renderedIfEmpty="true" width="100%">
													<t:column id="propertyNoCol"  sortable="true" style="width:90;">
														<f:facet name="header">
															<t:outputText value="#{msg['inspection.propertyRefNum']}" />
														</f:facet>
														<t:outputText value="#{dataItem.propertyNumber}" />
													</t:column>
													<t:column id="propertyNameCol" sortable="true" style="width:120;">
														<f:facet name="header">
															<t:outputText value="#{msg['contract.property.Name']}" />
														</f:facet>
														<t:outputText value="#{dataItem.commercialName}" />
													</t:column>
													<t:column id="propertyTypeCol" width="100" sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['property.type']}" />
														</f:facet>
														<t:outputText value="#{dataItem.propertyTypeEn}" />
													</t:column>
													<t:column id="usageTypeCol" width="100" sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['property.usage']}" />
														</f:facet>
														<t:outputText value="#{dataItem.usageTypeEn}" />
													</t:column>
													<t:column id="statusTypeCol" width="110" sortable="true">
														<f:facet name="header">
															<t:outputText value="Status Type" />
														</f:facet>
														<t:outputText value="#{dataItem.statusTypeEn}" />
													</t:column>
													<t:column id="selectCol" width="40">
														<f:facet name="header">
															<t:outputText value="#{msg['commons.select']}" />
														</f:facet>
														<t:commandLink
															action="#{pages$propertyList.selectProperty}">&nbsp;
															<h:graphicImage id="selectCmdLink" 
															title="#{msg['commons.select']}" 
															url="../resources/images/select-icon.gif"/>
														</t:commandLink>
													</t:column>													
													
												</t:dataTable>
											</div>
											<div class="contentDivFooter">

												<t:dataScroller id="scroller" for="dt1" paginator="true"
													fastStep="1" paginatorMaxPages="2" immediate="false"
													styleClass="scroller" paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true"
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

													<f:facet name="first">

														<t:graphicImage url="../resources/images/first_btn.gif"
															id="lblF"></t:graphicImage>
													</f:facet>

													<f:facet name="fastrewind">
														<t:graphicImage url="../resources/images/previous_btn.gif"
															id="lblFR"></t:graphicImage>
													</f:facet>

													<f:facet name="fastforward">
														<t:graphicImage url="../resources/images/next_btn.gif"
															id="lblFF"></t:graphicImage>
													</f:facet>

													<f:facet name="last">
														<t:graphicImage url="../resources/images/last_btn.gif"
															id="lblL"></t:graphicImage>
													</f:facet>

												</t:dataScroller>

											</div>
										</h:form>
									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
			</table>

		</body>
	</html>
</f:view>
