<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>


<script language="javascript" type="text/javascript">
	function openLoadReportPopup(pageName) 
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}	

	function resetViolationType() {
		document.getElementById("formTenantViolations:cmbViolationType").selectedIndex=0;
	}

</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden">

	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache"/>
			<meta http-equiv="cache-control" content="no-cache"/>
			<meta http-equiv="expires" content="0"/>
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
			<meta http-equiv="description" content="This is my page"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>	
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
	 								
			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>
		



</head>
	<body class="BODY_STYLE">
		<div style="width:1024px;">
			
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td  colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>	<td class="HEADER_TD">
										<h:outputLabel value="#{msg['commons.report.name.tenantViolations.jsp']}"  styleClass="HEADER_FONT" style="padding:10px"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						
					
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" 
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									>
								</td>
								<td width="0%" height="99%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:450px;">
									<h:form id="formTenantViolations">

									<div class="MARGIN" style="width:95%">
									<table cellpadding="0" cellspacing="0"  >
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>

									<div class="DETAIL_SECTION" style="width:99.6%;">
									<h:outputLabel value="#{msg['commons.report.criteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
										<table cellpadding="0px" cellspacing="0px"
												class="DETAIL_SECTION_INNER">

												<tr>
													<td><h:outputLabel styleClass="LABEL"
															value="#{msg['chequeList.tenantName']}"></h:outputLabel>
														:
													</td>
													<td><h:inputText id="tenantName" styleClass="INPUT_TEXT"
															value="#{pages$TenantViolations.reportCriteria.tenantName}"></h:inputText>
													</td>
													<td><h:outputLabel styleClass="LABEL"
															value="#{msg['contract.property.Name']}"></h:outputLabel>
														:
													</td>
													<td><h:inputText id="propertyName" styleClass="INPUT_TEXT"
															value="#{pages$TenantViolations.reportCriteria.propertyName}"></h:inputText>
													</td>
												</tr>
												<tr>
													<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['receiveProperty.ownershipType']}:"></h:outputLabel>
													</td>
													<td>			
														<h:selectOneMenu id="owner" styleClass="SELECT_MENU"
															value="#{pages$TenantViolations.reportCriteria.propertyOwnershipType}">
															<f:selectItem itemValue="-1"
																itemLabel="#{msg['commons.pleaseSelect']}" />

															<f:selectItems
																value="#{pages$ApplicationBean.propertyOwnershipType}" />
														</h:selectOneMenu>
													</td>
													<td><h:outputLabel styleClass="LABEL"
															value="#{msg['unit.number']}"></h:outputLabel>
														:
													</td>
													<td><h:inputText id="unitRefNo" styleClass="INPUT_TEXT"
															value="#{pages$TenantViolations.reportCriteria.unitNumber}"></h:inputText>
													</td>
												</tr>
												<tr>													
													<td>
														
														
													<h:outputLabel styleClass="LABEL" value="#{msg['commons.Contract.Number']}" />:</td>
													<td>
														<h:inputText id="contNum"  styleClass="INPUT_TEXT" 
															value="#{pages$TenantViolations.reportCriteria.contractNumber}"></h:inputText>
													</td>
													<td >
														
													<h:outputLabel styleClass="LABEL" value="#{msg['tenants.id']} :"></h:outputLabel></td>
													<td >
														
													<h:inputText id="txtTenantId" value="#{pages$TenantViolations.reportCriteria.tenantId}" styleClass="INPUT_TEXT" maxlength="20"></h:inputText></td>
												</tr>
												<tr>													
													<td><h:outputLabel styleClass="LABEL" value="#{msg['violation.Date']}"> :</h:outputLabel>
														
													</td>
													<td> 
														
													<rich:calendar id="violationDate" datePattern="#{pages$LeaseContractsSummary.dateFormat}" value="#{pages$TenantViolations.reportCriteria.violationDate}" inputStyle="width: 186px; height: 18px" enableManualInput="false" cellWidth="24px" cellHeight="22px"></rich:calendar></td>
													<td>
														
													<h:outputLabel styleClass="LABEL" value="#{msg['violation.violationCategory']}">:</h:outputLabel></td>
													<td><h:selectOneMenu id="cmbViolationCategory" 
															value="#{pages$TenantViolations.reportCriteria.violationCategory}" 
														styleClass="SELECT_MENU"
																binding="#{pages$ViolationSearch.htmlSelectOneViolationCategory}"
																	valueChangeListener="#{pages$ViolationSearch.violationCategorySelected}"
																	onchange="resetViolationType();submit()"

														>        
															<f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1" />
															<f:selectItems value="#{pages$ApplicationBean.violationCategoryList}" />
														</h:selectOneMenu>
														
													</td>


												</tr>
												<tr>

													<td>
														<h:outputLabel styleClass="LABEL"
															value="#{msg['violation.violationType']}">:</h:outputLabel>
													</td>
													<td>
														<h:selectOneMenu id="cmbViolationType" 
															value="#{pages$TenantViolations.reportCriteria.violationType}"
															styleClass="SELECT_MENU">
															<f:selectItem itemLabel="#{msg['commons.All']}"
																itemValue="-1" />
															<f:selectItems
																value="#{pages$ViolationSearch.violationTypes}" />
														</h:selectOneMenu>
													</td>



																<td>
														<h:outputLabel styleClass="LABEL"
															value="#{msg['violation.violationActions']}">:</h:outputLabel>
													</td>
													<td>
														<h:selectOneMenu id="cmbViolationActions" tabindex="4"
															value="#{pages$TenantViolations.reportCriteria.violationAction}"
															styleClass="SELECT_MENU">
															<f:selectItem itemLabel="#{msg['commons.All']}"
																itemValue="-1" />
															<f:selectItems
																value="#{pages$ApplicationBean.violationActionList}" />
														</h:selectOneMenu>
													</td>
													<tr>
													<td><h:outputLabel styleClass="LABEL" value="#{msg['violation.violationStatus']}">:</h:outputLabel></td>
													<td><h:selectOneMenu id="cmbViolationStatus" value="#{pages$TenantViolations.reportCriteria.violationStatus}" styleClass="SELECT_MENU">
															<f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1" />
															<f:selectItems value="#{pages$ApplicationBean.inspectionViolationStatus}" />
														</h:selectOneMenu></td>
													<td></td> 
													<td></td>
													</tr>
												</tr>														
													<tr>
														<td colspan="4">
															<DIV class="A_RIGHT" style="padding-bottom:4px;">
 																	<h:commandButton id="btnPrint" type="submit"
																		styleClass="BUTTON"
																		action="#{pages$TenantViolations.btnPrint_Click}"
																		binding="#{pages$TenantViolations.btnPrint}"
																		value="#{msg['commons.view']}" />
															</DIV>
														</td>
													</tr>	
												</table>
											</div>
											</div>
										</h:form>
									</div>
								</td></tr>
							</table>
						</td>
						</tr>
						<tr>
						    <td colspan="2">
						    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
						       		<tr><td class="footer"><h:outputLabel value= "#{msg['commons.footer.message']}" />
						       				</td></tr>
					    		</table>
					        </td>
					    </tr> 
					</table>
					</div>
			</body>
		</html>
	</f:view> 