<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%-- Please remove above taglibs after completing your tab contents--%>
<t:div styleClass="contentDiv" style="width:95%">
	<t:dataTable id="RetainedProfitDetailsDataTable" width="100%"
		styleClass="grid"
		binding="#{pages$RetainedProfitDetailsTab.dataTable}"
		value="#{pages$RetainedProfitDetailsTab.retainedProfitLists}"
		rows="10" preserveDataModel="true" preserveSort="false" var="dataItem"
		rowClasses="row1,row2">

		<t:column id="retainedProfitsRefNum" width="10%" sortable="false"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.refNum']}" />
			</f:facet>
			<t:outputText value="#{dataItem.refNum}" />
		</t:column>
		<t:column id="retainedProfitsYear" width="10%" sortable="false"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['portfolioDetailsReport.year']}" />
			</f:facet>
			<t:outputText value="#{dataItem.refNum}" />
		</t:column>

		<t:column id="retainedProfitsStatus" width="10%" sortable="false"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.status']}" />
			</f:facet>
			<t:outputText value="#{dataItem.status.dataDescAr}" />
		</t:column>
		<t:column id="retainedProfitsAmount" width="10%" sortable="false"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.amount']}" />
			</f:facet>
			<t:outputText value="#{dataItem.amount}" />
		</t:column>



	</t:dataTable>

</t:div>
<t:div>
	<table cellpadding="0" cellspacing="5" width="100%">
		<tr>
			<td>
				<h:outputText styleClass="PAGE_NUM" style="font-size:9;color:black;"
					value="#{msg['commons.retainedUnReleasedProfits']}:" />
			</td>
			<td>
				<h:outputText styleClass="PAGE_NUM"
					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:9;font-weight:bold;color:black"
					value="#{pages$RetainedProfitDetailsTab.sumTotalRetainedActiveProfits}" />
			</td>
			<td>
				<h:outputText styleClass="PAGE_NUM" style="font-size:9;color:black;"
					value="#{msg['commons.retainedReleasedProfits']}:" />
			</td>
			<td>
				<h:outputText styleClass="PAGE_NUM"
					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:9;font-weight:bold;color:black"
					value="#{pages$RetainedProfitDetailsTab.sumTotalRetainedReleasedProfits}" />
			</td>
		</tr>
		<tr>
			<td>
				<h:outputText styleClass="PAGE_NUM" style="font-size:9;color:black;"
					value="#{msg['commons.total']}:" />
			</td>
			<td>
				<h:outputText styleClass="PAGE_NUM"
					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:9;font-weight:bold;color:black"
					value="#{pages$RetainedProfitDetailsTab.sumTotalRetainedProfits}" />
			</td>
		</tr>
	</table>
</t:div>

