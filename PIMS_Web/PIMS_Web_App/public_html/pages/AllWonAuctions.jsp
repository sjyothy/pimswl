
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

    
<f:view>
        <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
       <f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
 
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>						
            <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
			


			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			
 	</head>
 	        
		<script language="javascript" type="text/javascript">
	function clearWindow()
	{
	
            document.getElementById("formAllWonAuctions:bidderNamwInput").value="";
			document.getElementById("formAllWonAuctions:unitNumberInput").value="";
			document.getElementById("formAllWonAuctions:txtAuctionNumber").value="";
			$('formAllWonAuctions:AuctionEndDate').component.resetSelectedDate();
			$('formAllWonAuctions:AuctionStartDate').component.resetSelectedDate();
	        
	}
	
	function addToContract()

      {

      

        window.opener.document.forms[0].submit();

        

        window.close();

      }
	
    </script>
 	

				<!-- Header -->
	
		<body class="BODY_STYLE">
	     <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr >
					
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
							<tr><td class="HEADER_TD">
										&nbsp;&nbsp;<h:outputLabel value="#{msg['allWonAuction']}" styleClass="HEADER_FONT"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
					        <table width="99%" class="greyPanelMiddleTable" style="align:center;"  cellpadding="0" cellspacing="0" border="0" >
                                 <tr valign="top">
                                  <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
                                   </td>    
									<td width="100%"  valign="top" nowrap="nowrap" style="align:center;" >
										<h:form id="formAllWonAuctions" style="width:98%;" styleClass="align:center;" enctype="multipart/form-data">
									     <div  class="SCROLLABLE_SECTION">		
										
										<div class="MARGIN" style="width:95%;align:center;" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="99%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
											</table>
										<div class="DETAIL_SECTION" style="width:99.7%" >
										<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
										<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER" border="0" >
										    
											<tr>
											<td width="97%">
												<table width="100%">
												<tr>
													<td width="25%">
														<h:outputLabel  styleClass="LABEL"  value="#{msg['auction.number']}:"></h:outputLabel>
													</td>
													<td width="25%">
														<h:inputText   id="txtAuctionNumber" maxlength="20"
															value="#{pages$AllWonAuctionsBackingBean.auctionNumber}"></h:inputText>
													</td>
													<td width="25%">
														<h:outputLabel  styleClass="LABEL"  value="#{msg['unit.number']}:"></h:outputLabel>
													</td>
													<td width="25%">
														<h:inputText   id="unitNumberInput" maxlength="10"
															value="#{pages$AllWonAuctionsBackingBean.unitNumber}"></h:inputText>
													</td>
												</tr>
												<tr>
													<td width="25%">
														<h:outputLabel  styleClass="LABEL"  value="#{msg['auction.startDate']}" />
														:
													</td>
													<td width="25%">
															  <rich:calendar id="AuctionStartDate" 
															     value="#{pages$AllWonAuctionsBackingBean.auctionStartDate}"  
														           popup="true" showApplyButton="false"
														           datePattern="#{pages$AllWonAuctionsBackingBean.dateformat}"  
							                                        
							                                       locale="#{pages$AllWonAuctionsBackingBean.locale}"
							                                       inputStyle="width: 170px; height: 14px"  
							                                />
													</td>
													<td width="25%">
														<h:outputLabel  styleClass="LABEL"  value="#{msg['auction.endDate']}"></h:outputLabel>
														:
													</td>
													<td width="25%">
												             <rich:calendar id="AuctionEndDate" 
															     value="#{pages$AllWonAuctionsBackingBean.auctionEndDate}"  
														           popup="true" showApplyButton="false"
														           datePattern="#{pages$AllWonAuctionsBackingBean.dateformat}"  
							                                        
							                                       locale="#{pages$AllWonAuctionsBackingBean.locale}"
							                                       inputStyle="width: 170px; height: 14px"  
							                                />
													</td>
												</tr>

												<tr>
													<td width="25%">
														<h:outputLabel  styleClass="LABEL"  value="#{msg['bidder.firstName']}"></h:outputLabel>
														:
													</td>
													<td width="25%">
														<h:inputText  id="bidderNamwInput" maxlength="50"
															value="#{pages$AllWonAuctionsBackingBean.bidderName}"></h:inputText>


													</td>

													<td width="25%">
														&nbsp;
													</td>
													<td width="25%">
														&nbsp;
													</td>

												</tr>
												<tr >

													<td colspan="4" class="BUTTON_TD">
														<h:commandButton styleClass="BUTTON"
															value="#{msg['commons.search']}"
															action="#{pages$AllWonAuctionsBackingBean.searchAllWonAuctions}">
														</h:commandButton>

														<h:commandButton styleClass="BUTTON" type="button"
												                           value="#{msg['commons.clear']}"
												                          
												                           onclick="javascript:clearWindow();" 
												                             />
														<h:commandButton styleClass="BUTTON"
															value="#{msg['commons.cancel']}"
															onclick="javascript:window.close();"></h:commandButton>
															
													</td>

												</tr>
												
												</table>
												</td>
															<td width="3%">
																&nbsp;
															</td>
												
												</tr>

										</table>
										
											
                                          </div>
                                          </div>
                                          <br>
                                          <div id="paddingDiv"  style="width:95%;padding:5px;">
								          <t:div styleClass="imag">
								                  
								          </t:div>	
								          <div id="dataListDiv" class="contentDiv"  style="width:100%;"  >
						                	<t:dataTable id="test2"
													value="#{pages$AllWonAuctionsBackingBean.auctionUnitViews}"
													binding="#{pages$AllWonAuctionsBackingBean.dataTable}"
													rows="#{pages$AllWonAuctionsBackingBean.paginatorRows}"
													preserveDataModel="false" preserveSort="false" var="dataItem"
													rules="all" renderedIfEmpty="true" width="100%">
						
													<t:column id="col1">
														<f:facet name="header">
															<t:outputText value="#{msg['unit.number']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.unitNumber}"/>
													</t:column>
													
													<t:column id="col2">
														<f:facet name="header">
															<t:outputText value="#{msg['auction.number']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.auctionNumber}"/>
													</t:column>
													
													<t:column id="colStarttDate" sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['bidder.firstName']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT"  value="#{dataItem.bidderName}" />
													</t:column>
													
													<t:column id="col3" sortable="true" >
														<f:facet name="header">
															<t:outputText value="#{msg['unit.rentValue']}" />
														</f:facet>
														<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.rentValue}" />
													</t:column>
														<t:column id="colAuctionRent" sortable="true" >
														<f:facet name="header">
															<t:outputText value="#{msg['unit.OpeningPrice']}" />
														</f:facet>
														<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.openingPrice}" />
													</t:column>
													<t:column id="colBidPrice" sortable="true" >
														<f:facet name="header">
															<t:outputText value="#{msg['auction.conductAuction.bidPrice']}" />
														</f:facet>
														<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.bidPrice}" />
													</t:column>
						
													<t:column id="colStatusEn" sortable="true"  rendered="#{pages$AllWonAuctionsBackingBean.isEnglishLocale}">
														<f:facet name="header">
															<t:outputText value="#{msg['unit.type']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.unitTypeEn}" />
													</t:column>
												   <t:column id="colStatusAr"  sortable="true"  rendered="#{pages$AllWonAuctionsBackingBean.isArabicLocale}">
														<f:facet name="header">
															<t:outputText value="#{msg['unit.type']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.unitTypeAr}" />
													</t:column>
													
													<t:column id="colUnitUsusageEn" sortable="true"  rendered="#{pages$AllWonAuctionsBackingBean.isEnglishLocale}">
														<f:facet name="header">
															<t:outputText value="#{msg['inquiry.unitUsage']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.unitUsageTypeEn}" />
													</t:column>
												   <t:column id="colUnitUsusageAr" sortable="true"  rendered="#{pages$AllWonAuctionsBackingBean.isArabicLocale}">
														<f:facet name="header">
															<t:outputText value="#{msg['inquiry.unitUsage']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.unitUsageTypeAr}" />
													</t:column>
													
													
													<t:column id="StartDatecol" sortable="true" >
														<f:facet name="header">
															<t:outputText value="#{msg['auction.startDatetime']}" />
														</f:facet>
														<t:outputText value="#{dataItem.auctionStartDate}" />
													</t:column>
													
													<t:column id="EndDatecol" sortable="true" >
														<f:facet name="header">
															<t:outputText value="#{msg['auction.endDatetime']}" />
														</f:facet>
														<t:outputText value="#{dataItem.auctionEndDate}" />
													</t:column>
						
													<t:column id="col6" width="50" >
														<f:facet name="header">
															<t:outputText value="#{msg['commons.action']}" />
														</f:facet>
														<h:commandLink 
															actionListener="#{pages$AllWonAuctionsBackingBean.sendAuctionUnitInfoToParent}">
															<h:graphicImage url="../resources/images/select-icon.gif"/>
														</h:commandLink>
														
													</t:column>
												</t:dataTable>										
											
 </div>

                                               <div class="contentDivFooter" style="width:100.8%;">
                                                <table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$AllWonAuctionsBackingBean.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:50%" align="right">
											     <CENTER>
										
                                                                        <t:dataScroller id="scroller" for="test2" paginator="true"
                                                                              fastStep="1" paginatorMaxPages="#{pages$AllWonAuctionsBackingBean.paginatorMaxPages}" immediate="false"
                                                                              paginatorTableClass="paginator"
                                                                              renderFacetsIfSinglePage="true" 
                                                                              pageIndexVar="pageNumber"
                                                                              paginatorTableStyle="grid_paginator" layout="singleTable"
                                                                              paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
                                                                              paginatorActiveColumnStyle="font-weight:bold;"
                                                                              paginatorRenderLinkForActive="false"
                                                                               styleClass="JUG_SCROLLER"
                                                                              
                                                                              >
                                                                              
										                                      					<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
								
																					<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
													</t:div>
                                                                        </t:dataScroller>
                                                                        </CENTER>
											</td></tr>
											</table>

						                 
						                  </div>
						                   
						               </div>
			    
			 </div>
			 </h:form>
									</td>
									</tr>
									</table>
			
			</td>
    </tr>
    
    </table>
			</div>
		</body>
	</html>
</f:view>

