
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />


			<script language="javascript" type="text/javascript">
  
    
    </script>

			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1

					response.setHeader("Pragma", "no-cache"); //HTTP 1.0

					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>



		</head>
		<body dir="${sessionScope.CurrentLocale.dir}"
			lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">
			<!-- Header -->
			<h:form id="frmFileUpload">

				<table width="100%" border="0">
					<tr align="left">
						<h:inputHidden id = "docIdHidden" binding="#{pages$FileUploadTest.docId}">
						</h:inputHidden>
						<td class="BUTTON_TD">
							<t:inputFileUpload id="fileupload" size="60" storage="file"
								binding="#{pages$FileUploadTest.fileUploadCtrl}">
							</t:inputFileUpload>

							<h:commandLink id="lnkImgUpload"  
							onclick="document.forms['frmFileUpload'].encoding = 'multipart/form-data';"							
								action="#{pages$FileUploadTest.UploadFile}" value="Upload File"  />
								
								<h:commandLink id="lnkImgDownload" target="_blank"														
								action="#{pages$FileUploadTest.Download}" value="Download File"  />




						</td>
					</tr>
				</table>

			</h:form>


		</body>
</f:view>

</html>
