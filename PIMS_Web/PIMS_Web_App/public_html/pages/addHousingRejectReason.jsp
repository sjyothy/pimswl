<%-- 
  - Author: Imran Ali
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for adding a housing reject 
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<script language="JavaScript" type="text/javascript">
		function closeWindowSubmit()
		{
			
			window.opener.document.forms[0].submit();
		  	window.close();	  
		}	
		
		
		  function sendMessageToParent()
        {
          	window.opener.onMessageFromHousingReason();
        	window.close();
          
        }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>

		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>

			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="100%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['lbl.header.rejectReason']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td width="100%" height="100%" valign="top">
									<div style="display: block; height: 700px; width: 100%">
										<h:form>
											<t:div styleClass="MESSAGE">
												<t:panelGrid columns="1"
													style="margin-left:5px;margin-right:5px;">
													<h:outputText
														value="#{pages$housingRejectReasonBean.errorMessage}"
														escape="false" styleClass="ERROR_FONT" />
												</t:panelGrid>
											</t:div>

											<t:panelGrid columns="2" cellpadding="2" cellspacing="0"
												border="0" style="margin-bottom:20px;">


												<t:outputLabel styleClass="LABEL"
													value="#{msg['lbl.header.rejectReason']}:">
												</t:outputLabel>
												<h:selectOneRadio layout="pageDirection"
													value="#{pages$housingRejectReasonBean.rejectReasonId}">
													<f:selectItems
														value="#{pages$ApplicationBean.housingRejectReason}" />
												</h:selectOneRadio>



												<t:outputLabel styleClass="LABEL"
													value="#{msg['lbl.requestReasonDesc']}:  "
													style="padding-right:5px;padding-left:5px;margin-right:26px;"></t:outputLabel>
												<t:inputTextarea styleClass="TEXTAREA"
													value="#{pages$housingRejectReasonBean.rejectReason}"
													rows="6" style="width: 320px;" />




												<t:outputLabel></t:outputLabel>
												<t:panelGroup>
													<t:div styleClass="BUTTON_TD">
														<t:commandButton styleClass="BUTTON"
															value="#{msg['commons.rejectButton']}"
															action="#{pages$housingRejectReasonBean.onReject}" />
														<t:commandButton styleClass="BUTTON"
															value="#{msg['commons.cancel']}"
															onclick="javascript:window.close();" />
													</t:div>
												</t:panelGroup>
											</t:panelGrid>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				</td>
				</tr>
			</table>
		</body>
	</html>
</f:view>
