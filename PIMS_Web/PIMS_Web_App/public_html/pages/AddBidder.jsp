

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

		
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>	


<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}"
 style="overflow:hidden;" >
	<head>
		<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
	
		<script language="javascript" type="text/javascript">
		
		
		
		
		function clearWindow()
	{
	       
	        document.getElementById("formBidder:txtfirstName").value="";
		    document.getElementById("formBidder:txtmiddleName").value="";
		    document.getElementById("formBidder:txtlastName").value="";
		    document.getElementById("formBidder:dateOfBirth").value="";
		    document.getElementById("formBidder:txtcellNo").value="";
		    document.getElementById("formBidder:txtpassportNumber").value="";
		    document.getElementById("formBidder:datePassportIssueDate").value="";
		    document.getElementById("formBidder:datePassportExpiryDate").value="";
		    document.getElementById("formBidder:txtresidenseVisaNumber").value="";
		    document.getElementById("formBidder:dateVisaExpiryDate").value="";
		    document.getElementById("formBidder:txtpersonalSecCardNo").value="";
		    document.getElementById("formBidder:txtdrivingLicenseNumber").value="";
		    document.getElementById("formBidder:txtsocialSecNumber").value="";
		    document.getElementById("formBidder:txtaddress1").value="";
		    document.getElementById("formBidder:txtaddress2").value="";
            document.getElementById("formBidder:txtemail").value="";
            document.getElementById("formBidder:txtfax").value="";
            document.getElementById("formBidder:txtofficePhone").value="";
            document.getElementById("formBidder:txthomePhone").value="";
            document.getElementById("formBidder:txtpostCode").value="";
            document.getElementById("formBidder:txtstreet").value="";
            
            var cmbTitle=document.getElementById("formBidder:selectTitle");
		     cmbTitle.selectedIndex = 0;
		    
		    var cmbGender =document.getElementById("formBidder:selectGender");
		     cmbGender.selectedIndex = 0; 
		    
		    var cmbcity=document.getElementById("formBidder:selectcity");
		     cmbcity.selectedIndex = 0;
		    
		    var cmbstate=document.getElementById("formBidder:selectstate");
		     cmbstate.selectedIndex = 0;
		    
		    var cmbcountry=document.getElementById("formBidder:selectcountry");
		     cmbcountry.selectedIndex = 0; 
		    
               var cmbnationality=document.getElementById("formBidder:selectnationality");
		     cmbnationality.selectedIndex = 0; 		    
		    }
		    
		          function submitForm()
	   {
          document.getElementById('formBidder').submit();
          document.getElementById("formBidder:selectcountry").selectedIndex=0;
          document.getElementById("formBidder:selectstate").selectedIndex=0;
	   }
		    
		    function submitFormState(){
       
		        
		    var cmbState=document.getElementById("formBidder:selectstate");
		    var j = 0;
		    j=cmbState.selectedIndex;
		    document.getElementById("formBidder:hdnCityValue").value=cmbState.options[j].value ;
		    document.forms[0].submit();
		   
		    
		    }
		
    </script>
    			<%

   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1

   response.setHeader("Pragma","no-cache"); //HTTP 1.0

   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

%>
</head>
	
	<body  class="BODY_STYLE">
  
    <!-- Header --> 
<table width="100%" cellpadding="0" cellspacing="0"  border="0">
    <tr>
    <td colspan="2">
        <jsp:include page="header.jsp"/>
    </td>
    </tr>

<tr width="100%">
    <td class="divLeftMenuWithBody" width="17%">
        <jsp:include page="leftmenu.jsp"/>
    </td>
    <td width="83%" valign="top" class="divBackgroundBody" height="505px">
        <table width="99.2%" class="greyPanelTable" cellpadding="0" cellspacing="0"  border="0">
        <tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['bidder.bidderInformation']}" styleClass="HEADER_FONT"/>
						</td>
						<td width="100%">
							&nbsp;
						</td>
					</tr>
        </table>
    
        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
          <tr valign="top" height="100%">
             <td height="100%" valign="top" nowrap="nowrap" background ="../../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
             </td> 
	  
  <td width="100%" height="460px" valign="top" >

		
		<div class="SCROLLABLE_SECTION" style="padding-bottom:7px;padding-left:10px;padding-right:7;padding-top:7px;">	
			<h:form id="formBidder" style="width:95%" >
			
			                          <div style = "height:25px;">
											<table border="0" class="layoutTable">
												 	
												<tr>
      									    	      <td colspan="6">
														<h:outputText value="#{pages$AddBidder.errorMessages}" escape="false" styleClass="ERROR_FONT" style="padding:10px;"/>
											            <h:outputText value="#{pages$AddBidder.successMessages}" escape="false" styleClass="INFO_FONT" style="padding:10px;"/>
														<h:inputHidden id="hdnBidderId" value="#{pages$AddBidder.bidderId}"/>
														<h:inputHidden id="hdnContactInfoId" value="#{pages$AddBidder.contactInfoId}"/>
														<h:inputHidden id="hdnpageMode" value="#{pages$AddBidder.pageMode}"/>
													  </td>
												</tr>
											 
											 </table>
									</div>		 
								
								<div class="MARGIN">
		       						<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
				                <div class="DETAIL_SECTION">
			                       <h:outputLabel value="#{msg['bidder.bidderDetail']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>	
											<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">	
												
												<tr>
													<td>
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['customer.title']}:"></h:outputLabel>														
													</td>
													
													<td>
															
													    <h:selectOneMenu  id="selectTitle" style="width: 140px" value="#{pages$AddBidder.selectOneTitle}" >
													       <f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$ApplicationBean.title}"/>
														</h:selectOneMenu>
													
													</td>
													<td >
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['customer.firstname']}:"></h:outputLabel>
													</td>
													
													<td >
												        <h:inputText styleClass="A_LEFT"  style="width:134px;" maxlength="50" id = "txtfirstName"  value="#{pages$AddBidder.firstName}">
												        
												        </h:inputText>
													</td>
												
												</tr>
											    
											    <tr>
											    	<td >
														<h:outputLabel value="#{msg['customer.middlename']}:"></h:outputLabel>
													</td>
		 											
													<td >
												        <h:inputText styleClass="A_LEFT"  style="width:134px;" maxlength="10" id="txtmiddleName" value="#{pages$AddBidder.middleName}">
												        
												        </h:inputText>
													</td>
											    	<td >
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['customer.lastname']}:"></h:outputLabel>
													</td>
													
													<td >
												        <h:inputText styleClass="A_LEFT"  style="width:134px;"  maxlength="50" id="txtlastName"  value="#{pages$AddBidder.lastName}">
												        
												        </h:inputText>
													</td>
											    
											    
													
												</tr>
											<tr>
											<td ><h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['customer.gender']}:"></h:outputLabel>
														
													</td>
													
													<td><h:selectOneMenu id="selectGender" style="width: 140px" value="#{pages$AddBidder.gender1}">
													    <f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
														<f:selectItem itemLabel="#{msg['tenants.gender.male']}" itemValue="M" />
														<f:selectItem itemLabel="#{msg['tenants.gender.female']}" itemValue="F" />	
														</h:selectOneMenu>
												        
													</td>
													<td ><h:outputLabel value="#{msg['customer.dateOfBirth']}:"></h:outputLabel>
														
														
													</td>
													
													<td>
													
													 <rich:calendar  id="DateOfBirth" value="#{pages$AddBidder.dateOfBirth}" 
							                          popup="true" datePattern="#{pages$AddBidder.dateFormat}" showApplyButton="false"
							                          locale="#{pages$AddBidder.locale}" 
							                          enableManualInput="false" cellWidth="24px" 
							                          />
												        
													</td>
										        	
												</tr>
												<tr>
												<td>
													<h:outputLabel value="#{msg['customer.cell']}:"/>
													</td>
													
													<td>
													<h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"  maxlength="15" id="txtcellNo" value="#{pages$AddBidder.cellNo}">
												        </h:inputText>
													</td>
													
													<td ><h:outputLabel value="#{msg['customer.nationality']}:"></h:outputLabel>
													</td>
													<td><h:selectOneMenu id="selectnationality" style="width: 140px"  value="#{pages$AddBidder.selectOneNationality}">
														<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$AddBidder.nationality}"/>
														</h:selectOneMenu>
												        
													
																						
												</tr>
												<tr>
												
													
	 												
														<td ><h:outputLabel  value="#{msg['customer.passportNumber']}:"></h:outputLabel>
														
													</td>
													<td>
												        
													<h:inputText styleClass="A_RIGHT_NUM" style="width:134px"   maxlength="15" id="txtpassportNumber" value="#{pages$AddBidder.passportNumber}">
												        
												        </h:inputText></td>
											
	 												
													<td >
														
													<h:outputLabel value="#{msg['customer.passportIssueDate']}:"></h:outputLabel></td>
													<td>
													
													<rich:calendar  id="datePassportIssueDate" value="#{pages$AddBidder.passportIssueDate}" 
							                          popup="true" datePattern="#{pages$AddBidder.dateFormat}" showApplyButton="false"
							                          locale="#{pages$AddBidder.locale}" 
							                          enableManualInput="false" cellWidth="24px" 
							                          />
												        
													</td>
	 												
						
																									
												</tr>
											
															
												<tr>
														<td><h:outputLabel value="#{msg['customer.passportExpiryDate']}:"></h:outputLabel>
														
													</td>
													<td>
														<rich:calendar  id="datePassportExpiryDate" value="#{pages$AddBidder.passportExpiryDate}" 
							                          popup="true" datePattern="#{pages$AddBidder.dateFormat}" showApplyButton="false"
							                          locale="#{pages$AddBidder.locale}" 
							                          enableManualInput="false" cellWidth="24px" 
							                          />
     													
													<td ><h:outputLabel value="#{msg['customer.residenseVisaNumber']}:"></h:outputLabel>
														
													</td>
													<td ><h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"   maxlength="15" id="txtresidenseVisaNumber" value="#{pages$AddBidder.residenseVisaNumber}">
												        
												        </h:inputText>
															
													</td>
													
													
													 
							
											     	</tr>
											     <tr>
													     <td ><h:outputLabel value="#{msg['customer.residenseVisaExpDate']}:"></h:outputLabel>
																
															</td>
															<td>
															<rich:calendar  id="dateVisaExpiryDate" value="#{pages$AddBidder.residenseVidaExpDate}" 
									                          popup="true" datePattern="#{pages$AddBidder.dateFormat}" showApplyButton="false"
									                          locale="#{pages$AddBidder.locale}" 
									                          enableManualInput="false" cellWidth="24px" 
									                          />
									                         </td>
													      <td >
																
															<h:outputLabel value="#{msg['customer.drivingLicenseNumber']}:"></h:outputLabel></td>
																
															<td><h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"   maxlength="15" id="txtdrivingLicenseNumber" value="#{pages$AddBidder.drivingLicenseNumber}">
														        
														        </h:inputText>
														        
															</td>
													
											     </tr>
											     <tr>
											     <td ><h:outputLabel value="#{msg['customer.personalSecCardNo']}:"></h:outputLabel>
														
													</td>
													<td ><h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"  maxlength="15" id="txtpersonalSecCardNo" value="#{pages$AddBidder.personalSecCardNo}">
												        
												        </h:inputText>
															
													</td>								
					
											          <td >
														
													<h:outputLabel value="#{msg['customer.socialSecNumber']}:"></h:outputLabel></td>
													<td>
												        
													<h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"   maxlength="15" id="txtsocialSecNumber" value="#{pages$AddBidder.socialSecNumber}">
												        
												        </h:inputText></td>
											     </tr>			
												  </table>		
											    </div>
											</div>	
                                             
                                             
                                 <div class="MARGIN">
									<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
									<div class="DETAIL_SECTION">
										<h:outputLabel value="#{msg['contact.contactinformation']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
                                           <table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
												<tr >
													<td>
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['contact.address1']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT"  style="width:134px;" maxlength="150" id = "txtaddress1" value="#{pages$AddBidder.address1}">
												        
												        </h:inputText>
													</td>
													<td>
														<h:outputLabel value="#{msg['contact.address2']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT"  style="width:134px;" maxlength="150" id = "txtaddress2" value="#{pages$AddBidder.address2}">
												        
												        </h:inputText>
													</td>
													
													
												</tr>
                                                 
                                                 <tr>
                                                 <td>
														<h:outputLabel value="#{msg['contact.street']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT"  style="width:134px;"  maxlength="150" id = "txtstreet"   value="#{pages$AddBidder.street}">
												        
												        </h:inputText>
													</td>
                                                 <td>
														<h:outputLabel value="#{msg['contact.postcode']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"   maxlength="10" id= "txtpostCode"  value="#{pages$AddBidder.postCode}">
												        
												        </h:inputText>
													</td>
                                                 													
													
													
													
													
												</tr>
												
												<tr>
												<td>
													<h:outputLabel value="#{msg['contact.country']}:"></h:outputLabel>
													
													<td><h:selectOneMenu id="selectcountry" valueChangeListener="#{pages$AddBidder.loadStates}" 
													onchange="javascript:submitForm();"  style="width: 140px"  value="#{pages$AddBidder.selectOneCountry}">
													
													<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$AddBidder.countries}" />
															
														</h:selectOneMenu>
														
													</td>
													<td>
														<h:outputLabel value="#{msg['contact.state']}:"></h:outputLabel>
													</td>
													<td>
														<h:selectOneMenu id="selectstate"  valueChangeListener="#{pages$AddBidder.loadCities}"  
														onchange="javascript:submitForm();" style="width: 140px" value="#{pages$AddBidder.selectOneState}">
														
														<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$AddBidder.states}"/>
															
														</h:selectOneMenu>	
													</td>
												
												
													
												</tr>
												<tr>
												<td>
														<h:outputLabel value="#{msg['contact.city']}:"></h:outputLabel>
													</td>
												        <td>
														<h:selectOneMenu id="selectcity" style="width: 140px"  value="#{pages$AddBidder.selectOneCity}">
														<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$AddBidder.cities}"/>
														</h:selectOneMenu>	
														</td>
													<td>
														<h:outputLabel value="#{msg['contact.homephone']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"  maxlength="15" id = "txthomePhone" value="#{pages$AddBidder.homePhone}">
												        
												        </h:inputText>
													</td>
													
												</tr>
												<tr>
												<td>
														<h:outputLabel value="#{msg['contact.officephone']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"  maxlength="15" id  ="txtofficePhone" value="#{pages$AddBidder.officePhone}">
												        
												        </h:inputText>
													</td>
												<td>
														<h:outputLabel value="#{msg['contact.fax']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"   maxlength="15" id = "txtfax"  value="#{pages$AddBidder.fax}">
												        
												        </h:inputText>
													</td>
													
												
												</tr>
												<tr>
												      <td>
														<h:outputLabel value="#{msg['contact.email']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT"  style="width:134px;" maxlength="50" id = "txtemail"  value="#{pages$AddBidder.email}">
												        
												        </h:inputText>
													</td>
												
												</tr>
												<h:inputHidden id="hdnStateValue" binding="#{pages$AddBidder.hiddenStateValue}"  />
												<h:inputHidden id="hdnCityValue" binding="#{pages$AddBidder.hiddenCityValue}"  />
											
											
                                		        
                                               </table>
                                              </div>
                                            </div>
                                            <table width="100%" border="0">
                                            <tr>
                                 				
										       <td colspan="6" class="BUTTON_TD">
                                                   <pims:security screen="Pims.AuctionManagement.Bidder.AddBidder" action="create">     
                                      				   <h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" type= "submit"   action="#{pages$AddBidder.btnAdd_Click}" >  </h:commandButton>
                                      				</pims:security>   
												       <h:commandButton styleClass="BUTTON" type="button" onclick="javascript:clearWindow();" value="#{msg['commons.reset']}" >  </h:commandButton>
												       <h:commandButton styleClass="BUTTON" type= "submit" value="#{msg['commons.cancel']}" action="#{pages$AddBidder.btnBack_Click}" >	</h:commandButton>
												       
                                		        </td>
                                		        </tr>
                                            </table>

 										
												          
               
			</h:form>
			</div>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			
			</table>
			</td>
			</tr>
			</table>
			<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</body>
		</f:view>
	
</html>
