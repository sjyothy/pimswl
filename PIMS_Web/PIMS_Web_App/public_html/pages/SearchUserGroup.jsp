  
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">        
        
	    function resetValues()
      	{
      		document.getElementById("searchFrm:txtGroupNamePri").value="";
      	    document.getElementById("searchFrm:txtGroupNameSec").value="";      	    
        }
        
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

</head>
	<body class="BODY_STYLE">
	<div class="containerDiv">
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header --> 
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="2">
				<jsp:include page="header.jsp" />
			</td>
		</tr>
		<tr >
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['userGroup.SearchGroup']}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>
				<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%">
					<tr valign="top">
						<td width="100%" height="100%">
							<div class="SCROLLABLE_SECTION AUC_SCH_SS">
									<h:form id="searchFrm" style="WIDTH: 97.6%;">
									<t:div styleClass="MESSAGE">
										<table border="0" class="layoutTable">
											<tr>
												<td>
													<h:outputText value="#{pages$SearchUserGroup.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$SearchUserGroup.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
									</t:div>
								<div class="MARGIN"> 
									<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
									<div class="DETAIL_SECTION">
										<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
										<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
										
															<tr>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['userGroup.groupName']}: "></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="txtGroupNamePri"
																		styleClass="INPUT_TEXT"
																		binding="#{pages$SearchUserGroup.groupNamePriTextBox}"
																		tabindex="1">
																	</h:inputText>
																</td>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['userGroup.groupNameArabic']}: "></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="txtGroupNameSec"
																		styleClass="INPUT_TEXT"
																		binding="#{pages$SearchUserGroup.groupNameSecTextBox}"
																		tabindex="1">
																	</h:inputText>
																</td>
															</tr>															

															<tr>
												<td class="BUTTON_TD" colspan="10" >

																	
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.search']}"
																		action="#{pages$SearchUserGroup.searchGroup}"></h:commandButton>
																	<h:commandButton styleClass="BUTTON" type="button" value="#{msg['commons.reset']}" 
																		onclick="javascript:resetValues();" style="width: 75px" ></h:commandButton>
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.add']}" action="add"></h:commandButton>

												</td>
											</tr>
										</table>
								</div>	
								</div>
								 <div style="padding-bottom:7px;padding-left:10px;padding-right:7px;padding-top:7px;">   
                                      <div class="imag">&nbsp;</div>
										<div class="contentDiv">
														<t:dataTable id="dt1" 
															value="#{pages$SearchUserGroup.secGroupDataList}"
															binding="#{pages$SearchUserGroup.dataTable}" 
															rows="#{pages$SearchUserGroup.paginatorRows}" 
															preserveDataModel="false" preserveSort="false" var="dataItem"
															rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
															width="100%">
															
															<t:column id="col2" sortable="true" 
																style="padding-left:2px;text-align:left;">
																<f:facet name="header">
																	<t:outputText value="#{msg['userGroup.groupName']}" />
																</f:facet>
																<t:outputText value="#{dataItem.primaryName}" />
															</t:column>


															<t:column id="col3" sortable="true" 
																style="padding-right:2px;text-align:right;">
																<f:facet name="header">
																	<t:outputText
																		value="#{msg['userGroup.groupNameArabic']}" />
																</f:facet>
																<t:outputText value="#{dataItem.secondaryName}" />
															</t:column>
															
															<t:column id="col5" width="12%">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.action']}" />
																</f:facet>
																<h:commandLink
																	action="#{pages$SearchUserGroup.setPermission}">
																	<h:graphicImage title="#{msg['userGroup.permission']}"
																		url="../resources/images/app_icons/userrights.gif" />
																</h:commandLink>
																<t:outputLabel value=" " rendered="true"></t:outputLabel>
																<h:commandLink
																	action="#{pages$SearchUserGroup.editDataItem}">
																	<h:graphicImage title="#{msg['commons.edit']}"
																		url="../resources/images/edit-icon.gif" />
																</h:commandLink>
																<t:outputLabel value=" " rendered="true"></t:outputLabel>
																<h:commandLink
																	action="#{pages$SearchUserGroup.deleteGroup}">
																	<c:choose>
																		<c:when
																			test="${dataItem.userGroupId eq 'AMAF Rent Committee'}">
																			<h:graphicImage
																				url="../resources/images/app_icons/Delete_Disable.png" />
																		</c:when>
																		<c:otherwise>
																			<h:graphicImage title="#{msg['commons.delete']}"
																				url="../resources/images/delete_icon.png" />
																		</c:otherwise>
																	</c:choose>
																</h:commandLink>
															</t:column>															
														
														</t:dataTable>
													</div>
										<t:div styleClass="contentDivFooter" style="width:99%;" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$SearchUserGroup.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
												<CENTER>
													<t:dataScroller id="scroller" for="dt1" paginator="true"  
														fastStep="1" paginatorMaxPages="#{pages$SearchUserGroup.paginatorMaxPages}" immediate="false"
														paginatorTableClass="paginator"
														renderFacetsIfSinglePage="true" 												
														paginatorTableStyle="grid_paginator" layout="singleTable" 
														paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
														paginatorActiveColumnStyle="font-weight:bold;"
														paginatorRenderLinkForActive="false" 
														pageIndexVar="pageNumber"
														styleClass="SCH_SCROLLER">
														<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
														<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>
													</t:dataScroller>
													</CENTER>
													
												</td></tr>
											</table>
									</t:div>
									</div>	
									</div>
								
							</h:form>
							
					  </div> 
						</td>			
					</tr>
				</table>
			</td>
		</tr>
		<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
	</table>
</div>
</body>
</html>
</f:view>