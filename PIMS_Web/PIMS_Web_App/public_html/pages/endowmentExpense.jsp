<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	function performTabClick(elementid)
	{
			
		disableInputs();
		
		document.getElementById("detailsFrm:"+elementid).onclick();
		
		return 
		
	}
	function performClick(control)
	{
			
		disableInputs();
		
		document.getElementById("detailsFrm:"+control).onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
	}	
	
	function populateUnit(unitId,unitNum,usuageTypeId,unitRentAmount,unitUsuageType,propertyCommercialName,unitAddress,unitStatusId,unitStatus)
	{
	    document.forms[0].submit();
	
	}
    function showUnitSearchPopUp()
	{
		var screen_width = 1024;
		var screen_height = 460;
		window.open('UnitSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP&context=MaintenanceRequest','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=10,top=150,scrollbars=yes,status=yes');
	}
	
	function populateProperty(propertyId)
	{
	  
	  document.getElementById("detailsFrm:hdnPropertyId").value=propertyId;
	  document.getElementById("detailsFrm:onMessageFromSearchProperty").onclick();
	}
	function showPropertySearchPopUp()
	{
		var screen_width = 1025;
		var screen_height = 470;
		var popup_width = screen_width-20;
		var popup_height = screen_height;
		var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		//window.open('propertyList.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
		var popup = window.open('propertyList.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		popup.focus();
	}
	
	 
	
	
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<c:choose>
						<c:when test="${pages$endowmentManage.pageMode != 'POPUP'}">
							<tr
								style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>
					<tr width="100%">
						<c:choose>
							<c:when test="${pages$endowmentManage.pageMode != 'POPUP'}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{pages$endowmentExpense.pageTitle}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>

																<h:outputText id="errorId"
																	value="#{pages$endowmentExpense.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$endowmentExpense.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$endowmentExpense.pageMode}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$endowmentExpense.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$endowmentExpense.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$endowmentExpense.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$endowmentExpense.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$endowmentExpense.hdnIsCompany}"></h:inputHidden>
																<h:inputHidden id="hdnPropertyId"
																	value="#{pages$endowmentExpense.hdnPropertyId}" />
																<h:commandLink id="onMessageFromSearchProperty"
																	action="#{pages$endowmentExpense.onMessageFromSearchProperty}" />

															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<t:div rendered="true" style="width:100%;">
														<t:panelGrid cellpadding="1px" width="100%"
															cellspacing="5px" columns="4">

															<h:outputText id="idSendTo" styleClass="LABEL"
																rendered="#{
																				pages$endowmentExpense.showFinance|| 
																	 			pages$endowmentExpense.showApprove}"
																value="#{msg['mems.investment.label.sendTo']}:" />
															<h:selectOneMenu id="idUserGrps"
																rendered="#{pages$endowmentExpense.showFinance|| pages$endowmentExpense.showApprove}"
																binding="#{pages$endowmentExpense.cmbReviewGroup}"
																style="width: 200px;">
																<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.userGroups}" />
															</h:selectOneMenu>

															<h:outputLabel />
															<h:outputLabel />
															
															<h:outputLabel styleClass="LABEL"
																value="#{msg['maintenanceRequest.remarks']}:"
																rendered="#{
																				pages$endowmentExpense.showComplete || 
																				pages$endowmentExpense.showResubmitButton||
																	 			pages$endowmentExpense.showFinance|| 
																	 			pages$endowmentExpense.showApprove
																	}" />
															<t:panelGroup colspan="3" style="width: 100%">
																<t:inputTextarea style="width: 750px;" id="txt_remarks" rows="3"
																	rendered="#{
																				pages$endowmentExpense.showComplete || 
																				pages$endowmentExpense.showResubmitButton||
																	 			pages$endowmentExpense.showFinance|| 
																	 			pages$endowmentExpense.showApprove
																	}"
																	value="#{pages$endowmentExpense.txtRemarks}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</t:panelGroup>
														</t:panelGrid>
													</t:div>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">

														<div class="TAB_PANEL_INNER">
															<rich:tabPanel id="tabpanel"
																binding="#{pages$endowmentExpense.tabPanel}"
																style="width: 100%">

																<rich:tab id="applicationTab"
																	label="#{msg['commons.tab.applicationDetails']}">
																	<%@ include file="ApplicationDetails.jsp"%>
																</rich:tab>
																<rich:tab id="detailsTab"
																	label="#{msg['commons.details']}">

																	<t:div id="tabDDetails" style="width:100%;">
																		<t:panelGrid id="ddTab" cellpadding="5px" width="100%"
																			cellspacing="10px" columns="4"
																			styleClass="TAB_DETAIL_SECTION_INNER"
																			columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel id="lblBenef" styleClass="LABEL"
																					value="#{msg['revenueFollowup.lbl.endowment']}" />
																			</h:panelGroup>
																			<h:panelGroup>
																				<h:inputText id="endowment" maxlength="20"
																					readonly="true" styleClass="READONLY"
																					value="#{pages$endowmentExpense.pageView.endowment.endowmentNum}" />
																			</h:panelGroup>

																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['property.propertyNumber']}:"></h:outputLabel>
																			</h:panelGroup>

																			<h:panelGroup>
																				<h:inputText styleClass="READONLY"
																					id="txtpropertyNumber" style="width:170px;"
																					readonly="true"
																					value="#{ pages$endowmentExpense.pageView.propertyView.propertyNumber}"></h:inputText>
																				<h:graphicImage id="imgsearchproperty"
																					binding="#{pages$endowmentExpense.imgPropertySearch}"
																					url="../resources/images/app_icons/Search-tenant.png"
																					onclick="javaScript:showPropertySearchPopUp();"
																					style="MARGIN: 0px 0px -4px;"
																					title="#{msg['maintenanceRequest.searchProperties']}"
																					alt="#{msg['maintenanceRequest.searchProperties']}"></h:graphicImage>
																			</h:panelGroup>

																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.unitNumber']}:" />
																			</h:panelGroup>
																			<h:panelGroup>
																				<h:inputText maxlength="20" styleClass="READONLY"
																					id="txtunitNumber" style="width:170px;"
																					readonly="true"
																					value="#{pages$endowmentExpense.pageView.unitView.unitNumber}"></h:inputText>
																				<h:graphicImage
																					binding="#{pages$endowmentExpense.imgUnitSearch}"
																					url="../resources/images/app_icons/Search-tenant.png"
																					onclick="javaScript:showUnitSearchPopUp();"
																					style="MARGIN: 0px 0px -4px;"
																					title="#{msg['maintenanceRequest.searchUnits']}"
																					alt="#{msg['maintenanceRequest.searchUnits']}"></h:graphicImage>

																			</h:panelGroup>

																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel id="endExpType" styleClass="LABEL"
																					value="#{msg['endowmentExpense.lbl.endowmentExpenseType']}:"></h:outputLabel>
																			</h:panelGroup>
																			<h:selectOneMenu id="cmbendowmentExpenseTypes"
																				disabled="#{!pages$endowmentExpense.showSaveButton && 
																				    		!pages$endowmentExpense.showResubmitButton 
																				           }"
																				value="#{pages$endowmentExpense.pageView.endowmentExpenseTypeId}"
																				tabindex="3">
																				<f:selectItem
																					itemLabel="#{msg['commons.pleaseSelect']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.allEndowmentExpTypes}" />
																			</h:selectOneMenu>


																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel id="lblDescriptionamnt"
																					styleClass="LABEL"
																					value="#{msg['commons.description']}:"></h:outputLabel>
																			</h:panelGroup>
																			<t:panelGroup colspan="7">
																				<t:inputTextarea id="txtDescription"
																					style="width:600px;"
																					readonly="#{!pages$endowmentExpense.showSaveButton && 
																					    		!pages$endowmentExpense.showResubmitButton 
																					        }"
																					value="#{pages$endowmentExpense.pageView.description }" />

																			</t:panelGroup>

																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel id="lblperiodFrom" styleClass="LABEL"
																					value="#{msg['endowmentExpense.lbl.periodFrom']}:"></h:outputLabel>
																			</h:panelGroup>
																			<rich:calendar id="periodFromcal"
																				value="#{pages$endowmentExpense.pageView.periodFrom}"
																				popup="true" datePattern="dd/MM/yyyy"
																				showApplyButton="false"
																				locale="#{pages$endowmentExpense.locale}"
																				enableManualInput="false"
																				inputStyle="width: 170px; height: 14px" />

																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel id="lblperiodTo" styleClass="LABEL"
																					value="#{msg['endowmentExpense.lbl.periodTo']}:"></h:outputLabel>
																			</h:panelGroup>
																			<rich:calendar id="periodTocal"
																				value="#{pages$endowmentExpense.pageView.periodTo}"
																				popup="true" datePattern="dd/MM/yyyy"
																				showApplyButton="false"
																				locale="#{pages$endowmentExpense.locale}"
																				enableManualInput="false"
																				inputStyle="width: 170px; height: 14px" />

																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel id="benef" styleClass="LABEL"
																					value="#{msg['commons.Beneficiary']}:"></h:outputLabel>
																			</h:panelGroup>
																			<h:inputText id="txtBenef" maxlength="250"
																				readonly="#{!pages$endowmentExpense.showSaveButton && 
																				    		!pages$endowmentExpense.showResubmitButton 
																				           }"
																				value="#{pages$endowmentExpense.pageView.beneficiaryName}" />

																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel id="amnt" styleClass="LABEL"
																					value="#{msg['commons.amount']}:"></h:outputLabel>
																			</h:panelGroup>
																			<h:inputText id="txtAmount"
																				readonly="#{!pages$endowmentExpense.showSaveButton && 
																				    		!pages$endowmentExpense.showResubmitButton 
																				           }"
																				value="#{pages$endowmentExpense.pageView.amountString}"
																				onkeyup="removeNonNumeric(this)"
																				onkeypress="removeNonNumeric(this)"
																				onkeydown="removeNonNumeric(this)"
																				onblur="removeNonNumeric(this)"
																				onchange="removeNonNumeric(this)" />


																		</t:panelGrid>
																	</t:div>
																	<t:div styleClass="BUTTON_TD"
																		style="padding-top:10px; padding-right:21px;">

																		<h:commandButton styleClass="BUTTON"
																			rendered="#{pages$endowmentExpense.showSaveButton ||  
												 										pages$endowmentExpense.showResubmitButton 
																   				    	}"
																			value="#{msg['commons.Add']}"
																			onclick="performTabClick('lnkAdd');">
																			<h:commandLink id="lnkAdd"
																				action="#{pages$endowmentExpense.onAdd}" />
																		</h:commandButton>


																	</t:div>
																	<t:div styleClass="contentDiv"
																		style="width:98%;margin-top: 5px;">
																		<t:dataTable id="dataTableExpenses" rows="15"
																			width="100%"
																			value="#{pages$endowmentExpense.dataList}"
																			binding="#{pages$endowmentExpense.dataTable}"
																			preserveDataModel="false" preserveSort="false"
																			var="dataItem" rowClasses="row1,row2" rules="all"
																			renderedIfEmpty="true">
																			<t:column id="checkbox" style="width:auto;">
																				<f:facet name="header">
																					<t:outputText value="#{msg['commons.select']}" />
																				</f:facet>
																				<h:selectBooleanCheckbox id="idbox" immediate="true"
																					rendered="#{
																								 pages$endowmentExpense.showFinance && 
																								 dataItem.status == 211002
																								}"
																					value="#{dataItem.selected}">
																				</h:selectBooleanCheckbox>
																			</t:column>
																			<t:column id="payNumber" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="payNum"
																						value="#{msg['chequeList.paymentNumber']}" />
																				</f:facet>
																				<t:outputText id="payNoField" styleClass="A_LEFT"
																					value="#{!empty dataItem.paymentSchedules ?  
																								dataItem.paymentSchedules[0].paymentNumber:''}" />
																			</t:column>
																			<t:column id="typeCol" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="typeCoOT"
																						value="#{msg['commons.typeCol']}" />
																				</f:facet>
																				<t:outputText id="typeO" styleClass="A_LEFT"
																					value="#{pages$endowmentExpense.englishLocale? dataItem.endowmentExpenseTypeEn:dataItem.endowmentExpenseTypeAr}" />
																			</t:column>
																			<t:column id="expEndowmentName" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="expEndowmentNameOT"
																						value="#{msg['officialCorrespondence.lbl.endowment']}" />
																				</f:facet>
																				<t:outputText id="expEndowmentNameField"
																					styleClass="A_LEFT"
																					value="#{! empty dataItem.endowment ?dataItem.endowment.endowmentNum :''}-
																						   #{! empty dataItem.endowment ?dataItem.endowment.endowmentName :''}
																						   " />
																			</t:column>

																			<t:column id="expPropertyName" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="expPropertyNameOT"
																						value="#{msg['contractList.property.commercialName']}" />
																				</f:facet>
																				<t:outputText id="expPropertyField"
																					styleClass="A_LEFT"
																					value="#{! empty dataItem.propertyView?dataItem.propertyView.propertyNumber:''}" />
																			</t:column>
																			<t:column id="expUnitName" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="expUnitNameOT"
																						value="#{msg['cancelContract.tab.unit.unitno']}" />
																				</f:facet>
																				<t:outputText id="expUnitField" styleClass="A_LEFT"
																					value="#{! empty dataItem.unitView?dataItem.unitView.unitNumber:''}" />
																			</t:column>
																			<t:column id="expbeneficiaryName" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="expNbeneficiaryNameOT"
																						value="#{msg['commons.Beneficiary']}" />
																				</f:facet>
																				<t:outputText id="expbeneficiaryNameField"
																					styleClass="A_LEFT"
																					value="#{dataItem.beneficiaryName}" />
																			</t:column>


																			<t:column id="statusCol" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="statusAmnt"
																						value="#{msg['commons.status']}" />
																				</f:facet>
																				<t:outputText id="status" styleClass="A_LEFT"
																					value="#{pages$endowmentExpense.englishLocale? dataItem.statusEn:dataItem.statusAr}" />
																			</t:column>


																			<t:column id="periodFrom" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="periodFromCOloutput"
																						value="#{msg['endowmentExpense.lbl.periodFrom']}" />
																				</f:facet>
																				<t:outputText id="periodFromDate"
																					styleClass="A_LEFT" value="#{dataItem.periodFrom}"
																					style="white-space: normal;">
																					<f:convertDateTime
																						pattern="#{pages$endowmentExpense.dateFormat}"
																						timeZone="#{pages$endowmentExpense.timeZone}" />
																				</t:outputText>
																			</t:column>
																			<t:column id="periodTo" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="periodToCOloutput"
																						value="#{msg['endowmentExpense.lbl.periodTo']}" />
																				</f:facet>
																				<t:outputText id="periodToDate" styleClass="A_LEFT"
																					value="#{dataItem.periodTo}"
																					style="white-space: normal;">
																					<f:convertDateTime
																						pattern="#{pages$endowmentExpense.dateFormat}"
																						timeZone="#{pages$endowmentExpense.timeZone}" />
																				</t:outputText>
																			</t:column>
																			<t:column id="paymentDescription" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="hdpaymentDescription"
																						value="#{msg['commons.description']}" />
																				</f:facet>
																				<t:outputText id="paymentDescriptionField"
																					styleClass="A_LEFT" value="#{dataItem.description}" />
																			</t:column>

																			<t:column id="amntCol" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="hdAmnt"
																						value="#{msg['mems.payment.request.label.Amount']}" />
																				</f:facet>
																				<t:outputText id="amountt" styleClass="A_LEFT"
																					value="#{dataItem.amountString}" />
																			</t:column>

																			<t:column id="action" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="hdAmoun"
																						value="#{msg['commons.action']}" />
																				</f:facet>

																				<h:commandLink id="lnkDelete"
																					onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableInputs();"
																					rendered="#{pages$endowmentExpense.showSaveButton || pages$endowmentExpense.showResubmitButton}"
																					action="#{pages$endowmentExpense.onDelete}">
																					<h:graphicImage id="deleteIcon"
																						title="#{msg['commons.delete']}"
																						url="../resources/images/delete.gif" width="18px;" />
																				</h:commandLink>
																			</t:column>
																		</t:dataTable>
																	</t:div>
																	<pims:security
																		screen="Pims.EndowMgmt.EndowmentExpense.Finance"
																		action="view">
																		<t:div styleClass="BUTTON_TD">

																			<h:commandButton styleClass="BUTTON"
																				rendered="#{pages$endowmentExpense.showDisburse}"
																				value="#{msg['mems.finance.label.disburseButton']}"
																				onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}'))
																								 return false;else performClick('lnkDisbrse');" />

																			<h:commandLink id="lnkDisbrse"
																				action="#{pages$endowmentExpense.onDisburse}"
																				style="width: auto;">
																			</h:commandLink>


																		</t:div>
																	</pims:security>
																</rich:tab>
																<rich:tab id="reviewTab"
																	label="#{msg['commons.tab.reviewDetails']}">
																	<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
																</rich:tab>
																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>
																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>
																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$endowmentExpense.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>

														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">
															<pims:security
																screen="Pims.EndowMgmt.EndowmentExpense.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentExpense.showSaveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="performClick('lnkSave');">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$endowmentExpense.onSave}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentExpense.showSubmitButton}"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick('lnkSubmit');">
																</h:commandButton>
																<h:commandLink id="lnkSubmit"
																	action="#{pages$endowmentExpense.onSubmit}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentExpense.showResubmitButton}"
																	value="#{msg['socialResearch.btn.resubmit']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick('lnkResubmit');">
																</h:commandButton>
																<h:commandLink id="lnkResubmit"
																	action="#{pages$endowmentExpense.onResubmitted}" />

															</pims:security>
															<pims:security
																screen="Pims.EndowMgmt.EndowmentExpense.Approve"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentExpense.showApprove}"
																	value="#{msg['commons.reject']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick('lnkReject');">
																</h:commandButton>
																<h:commandLink id="lnkReject"
																	action="#{pages$endowmentExpense.onRejected}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentExpense.showApprove}"
																	value="#{msg['commons.review']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick('lnkReviewReqFromApprover');">
																</h:commandButton>
																<h:commandLink id="lnkReviewReqFromApprover"
																	action="#{pages$endowmentExpense.onReviewRequired}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentExpense.showApprove}"
																	value="#{msg['commons.sendBack']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick('lnkSendBackFromApprover');">
																</h:commandButton>
																<h:commandLink id="lnkSendBackFromApprover"
																	action="#{pages$endowmentExpense.onSendBackFromApprover}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentExpense.showApprove}"
																	value="#{msg['commons.approveButton']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick('lnkApprove');">
																</h:commandButton>
																<h:commandLink id="lnkApprove"
																	action="#{pages$endowmentExpense.onApprove}" />


															</pims:security>
															<pims:security
																screen="Pims.EndowMgmt.EndowmentExpense.Finance"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentExpense.showFinance}"
																	value="#{msg['commons.auditButton']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick('lnkAudit');">
																</h:commandButton>
																<h:commandLink id="lnkAudit"
																	action="#{pages$endowmentExpense.onSendForAudit}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentExpense.showFinance}"
																	value="#{msg['commons.review']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick('lnkReviewReqFromFinance');">
																</h:commandButton>
																<h:commandLink id="lnkReviewReqFromFinance"
																	action="#{pages$endowmentExpense.onReviewRequired}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentExpense.showFinance}"
																	value="#{msg['commons.sendBack']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick('lnkSendBackFromFinance');">
																</h:commandButton>
																<h:commandLink id="lnkSendBackFromFinance"
																	action="#{pages$endowmentExpense.onSendBackFromFinance}" />

																<h:commandButton styleClass="BUTTON"
																	style="min-width:75px;width:auto;"
																	rendered="#{pages$endowmentExpense.showFinance}"
																	value="#{msg['commons.sendForCompletion']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick('lnkSendForComplete');">
																</h:commandButton>
																<h:commandLink id="lnkSendForComplete"
																	action="#{pages$endowmentExpense.onSendForComplete}" />
															</pims:security>
															<pims:security
																screen="Pims.EndowMgmt.EndowmentExpense.Complete"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentExpense.showComplete}"
																	value="#{msg['commons.complete']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick('lnkComplete');">
																</h:commandButton>
																<h:commandLink id="lnkComplete"
																	action="#{pages$endowmentExpense.onComplete}" />
															</pims:security>
															<pims:security
																screen="Pims.EndowMgmt.EndowmentExpense.Audit"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentExpense.showAudit}"
																	value="#{msg['commons.done']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick('lnkAudited');">
																</h:commandButton>
																<h:commandLink id="lnkAudited"
																	action="#{pages$endowmentExpense.onAudited}" />
															</pims:security>
															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$endowmentExpense.showReview}"
																value="#{msg['commons.done']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick('lnkReviewDone');">
															</h:commandButton>
															<h:commandLink id="lnkReviewDone"
																action="#{pages$endowmentExpense.onReviewed}" />
														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

				</table>
			</div>

		</body>
	</html>
</f:view>