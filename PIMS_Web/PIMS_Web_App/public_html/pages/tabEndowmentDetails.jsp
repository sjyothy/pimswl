
<t:div id="tabDDetails" style="width:100%;">

	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;">

		<h:commandButton styleClass="BUTTON" id="openAddPopup"
			binding="#{pages$endowmentFile.btnAddEndowment}"
			action="#{pages$tabEndowmentDetails.onAdd}"
			value="#{msg['commons.Add']}">
		</h:commandButton>
		<pims:security
			screen="Pims.EndowMgmt.EndowmentFile.SaveAfterCompletion"
			action="view">

			<h:commandButton styleClass="BUTTON" id="openAddPopupAfterSave"
				binding="#{pages$endowmentFile.btnAddEndowmentAfterCompletion}"
				action="#{pages$tabEndowmentDetails.onAdd}"
				value="#{msg['commons.Add']}" />

		</pims:security>

	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="prdc" rows="15" width="100%"
			value="#{pages$tabEndowmentDetails.dataList}"
			binding="#{pages$tabEndowmentDetails.dataTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
			<t:column id="endNumber" sortable="true" defaultSorted="true">
				<f:facet name="header">
					<t:outputText id="endNum" value="#{msg['endowment.lbl.num']}" />
				</f:facet>
				<t:commandLink
					onclick="openEndowmentManagePopup('#{dataItem.endowment.endowmentId}')"
					value="#{dataItem.endowment.endowmentNum}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="endowName" sortable="true">
				<f:facet name="header">
					<t:outputText id="endname" value="#{msg['endowment.lbl.name']}" />
				</f:facet>
				<t:outputText id="namee" styleClass="A_LEFT"
					value="#{dataItem.endowment.endowmentName}" />
			</t:column>

			<t:column id="endowCostCenter" sortable="true">
				<f:facet name="header">
					<t:outputText id="endCostCenter" value="#{msg['unit.costCenter']}" />
				</f:facet>
				<t:outputText id="costCentere" styleClass="A_LEFT"
					value="#{dataItem.endowment.costCenter}" />
			</t:column>
			<t:column id="colenTypeName" sortable="true">
				<f:facet name="header">
					<t:outputText id="endTypeName" value="#{msg['commons.typeCol']}" />
				</f:facet>
				<t:outputText id="endTypeNamee" styleClass="A_LEFT"
					value="#{pages$tabEndowmentDetails.englishLocale?dataItem.endowment.assetType.assetTypeNameEn:
																	 dataItem.endowment.assetType.assetTypeNameAr
							}" />
			</t:column>
			<t:column id="propertyNumber">
				<f:facet name="header">
					<t:outputText value="#{msg['property.number']}" />
				</f:facet>
				<t:outputText value="#{dataItem.endowment.propertyNumbers}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>

			<t:column id="endowmentMainCategory" sortable="true">
				<f:facet name="header">
					
						<t:outputText id="maincategory"
						value="#{msg['endowment.lbl.maincategory']}" />
				</f:facet>
				<t:outputText value="#{dataItem.endowment.endowmentMainCategoryAr}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			
			<t:column sortable="true">
				<f:facet name="header">
					tabEndowmentDetails.englishLocale?dataItem.endowment.assetType.assetTypeNameEn:
																	 dataItem.endowment.assetType.assetTypeNameAr
						<t:outputText 
						value="#{msg['commons.typeStatus']}" />
				</f:facet>
				<t:outputText value="#{englishLocale?dataItem.endowment.endowmentStatusType.dataDescEn:
										dataItem.endowment.endowmentStatusType.dataDescAr}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			
			<t:column id="colcategory" sortable="true">
				<f:facet name="header">
					<t:outputText id="endcategory"
						value="#{msg['endowment.lbl.category']}" />
				</f:facet>
				<t:outputText id="endcategorye" styleClass="A_LEFT"
					value="#{dataItem.endowmentCategory.categoryNameAr}" />
			</t:column>
			<t:column id="colpurpose" sortable="true">
				<f:facet name="header">
					<t:outputText id="endpurpose"
						value="#{msg['endowment.lbl.purpose']}" />
				</f:facet>
				<t:outputText id="endpurposee" styleClass="A_LEFT"
					value="#{dataItem.endowmentPurpose.purposeNameAr}" />
			</t:column>
			<t:column id="colManager" sortable="false">
				<f:facet name="header">
					<t:outputText id="endManager"
						value="#{msg['contract.person.Manager']}" />
				</f:facet>
				<t:outputText id="endManagere" styleClass="A_LEFT"
					rendered="#{dataItem.endowment.isAmafManager!= null && dataItem.endowment.isAmafManager=='1'}"
					value="#{msg['inheritanceFile.inheritedAssetTab.managerAmaf']}" />
				<t:commandLink
					rendered="#{dataItem.endowment.isAmafManager!= null && dataItem.endowment.isAmafManager=='0' &&
					            dataItem.endowment.manager.personId != null }"
					onclick="javascript:showPersonReadOnlyPopup(#{dataItem.endowment.manager.personId }); 
					         retrun "
					value="#{dataItem.endowment.manager.fullName}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<%-- 
			<t:column id="colownerShare" sortable="true">
				<f:facet name="header">
					<t:outputText id="ownerShare"
						value="#{msg['endowment.lbl.ownerShare']}" />
				</f:facet>
				<t:outputText id="ownerSharee" styleClass="A_LEFT"
					value="#{dataItem.ownerShare}" />
			</t:column>

			<t:column id="colShareToEndow" sortable="true">
				<f:facet name="header">
					<t:outputText id="shareToEndow"
						value="#{msg['endowment.lbl.shareToEndow']}" />
				</f:facet>
				<t:outputText id="shareToEndowe" styleClass="A_LEFT"
					value="#{dataItem.shareToEndowString}" />
			</t:column>
			--%>
			<t:column id="colEndFileStatus" sortable="true">
				<f:facet name="header">
					<t:outputText id="endFileStatus" value="#{msg['commons.status']}" />
				</f:facet>
				<t:outputText id="endFileStatuse" styleClass="A_LEFT"
					value="#{pages$tabEndowmentDetails.englishLocale?dataItem.status.dataDescEn:
																	 dataItem.status.dataDescAr
							}" />
			</t:column>
			<t:column id="colAction" sortable="false">
				<f:facet name="header">
					<t:outputText id="hdAmoun" value="#{msg['commons.action']}" />
				</f:facet>

				<h:commandLink action="#{pages$tabEndowmentDetails.onEdit}"
					rendered="#{pages$endowmentFile.showEditEndowmentLink}">

					<h:graphicImage id="editIcon" title="#{msg['commons.edit']}"
						url="../resources/images/edit-icon.gif" width="18px;"
						style="margin-right:5px;" />
				</h:commandLink>

				<t:commandLink
					rendered="#{pages$endowmentFile.showAddBeneficiariesLink}"
					action="#{pages$tabEndowmentDetails.onAddBeneficiary}">
					<h:graphicImage id="addIcon" style="margin-right:5px;"
						title="#{msg['commons.addBeneficiary']}"
						url="../resources/images/app_icons/Add-New-Tenant.png" />
				</t:commandLink>

				<t:commandLink
					rendered="#{dataItem.endowment.showAddThirdPartyPopupLink  && pages$endowmentFile.showThirdPartyLink}"
					action="#{pages$tabEndowmentDetails.onAddThirdPartyUnits}">
					<h:graphicImage id="addUnits" style="margin-right:5px;"
						title="#{msg['endowment.AddThirdPartyUnits']}"
						url="../resources/images/app_icons/property.png" />
				</t:commandLink>

				<t:commandLink
					rendered="#{dataItem.endowment.showReceivePropertyLink && 
					            pages$endowmentFile.showReceivePropertyLink}"
					action="#{pages$tabEndowmentDetails.onReceiveProperty}">
					<h:graphicImage id="receiveProperty" style="margin-right:5px;"
						title="#{msg['endowment.ReceiveProperty']}"
						url="../resources/images/app_icons/property.png" />
				</t:commandLink>

				<t:commandLink
					rendered="#{dataItem.endowment.showCollectPaymentLink && 
					            pages$endowmentFile.showCollectPaymentLink}"
					action="#{pages$tabEndowmentDetails.onCollectPayment}">
					<h:graphicImage id="collectPayment" style="margin-right:5px;"
						title="#{msg['commons.collectAppFee']}"
						url="../resources/images/app_icons/Add-fee.png" />
				</t:commandLink>
				<t:commandLink
					rendered="#{dataItem.endowment.showAddPortfolioLink && 
					            pages$endowmentFile.showPortfolioLink}"
					action="#{pages$tabEndowmentDetails.onPortfolio}">
					<h:graphicImage id="portfolio" style="margin-right:5px;"
						title="#{msg['endowment.portfolio']}"
						url="../resources/images/app_icons/Tendering.png" />
				</t:commandLink>

				<t:commandLink
					rendered="#{dataItem.showEvaluateEndowment && pages$endowmentFile.showEvaluateEndowmentLink}"
					action="#{pages$tabEndowmentDetails.onEvaluateEndowments}">
					<h:graphicImage id="evaluateEndowment" style="margin-right:5px;"
						title="#{msg['endowmentFileAsso.lbl.evaluateEndowment']}"
						url="../resources/images/app_icons/Settle-Contract.png" />
				</t:commandLink>

				<t:commandLink
					rendered="#{pages$endowmentFile.showDeleteEndowmentLink}"
					onclick="if (!confirm('#{msg['endowment.msg.confirmEndowmentDeletion']}')) return false;"
					action="#{pages$tabEndowmentDetails.onDelete}">
					<h:graphicImage id="deleteEndowments"
						title="#{msg['commons.delete']}"
						style="margin-right:10px;cursor:hand;"
						url="../resources/images/delete.gif" />
				</t:commandLink>

			</t:column>
		</t:dataTable>
	</t:div>
	<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99%;">
		<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
			cellspacing="1">
			<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
				<t:div styleClass="JUG_NUM_REC_ATT">
					<h:outputText value="#{msg['commons.records']}" />
					<h:outputText value=" : " />
					<h:outputText value="#{pages$tabEndowmentDetails.totalRows}" />
				</t:div>
			</t:panelGroup>
			<t:panelGroup>
				<t:dataScroller id="shareScrollers" for="prdc" paginator="true"
					fastStep="1" immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" paginatorMaxPages="5"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false" pageIndexVar="pageNumber"
					styleClass="JUG_SCROLLER">
					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
					</f:facet>
					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
					</f:facet>
					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
					</f:facet>
					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
					</f:facet>

				</t:dataScroller>


			</t:panelGroup>
		</t:panelGrid>
	</t:div>
</t:div>


