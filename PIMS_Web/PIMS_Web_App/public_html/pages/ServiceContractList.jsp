<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden">

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />

			<style>
.rich-calendar-input {
	width: 75%;
}
</style>
			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

			<script language="JavaScript" type="text/javascript"
				src="../resources/jscripts/popcalendar_en.js" />
			
			
			
			
			<script language="javascript" type="text/javascript">

	function closeWindow() {
	  window.opener="x";
	  window.close();
	}
	
	function RowDoubleClick(bidderName, bidderId) {
	    window.opener.document.getElementById("auctionRequestForm:bidderNameInputText").value=bidderName;
	    window.opener.document.getElementById("auctionRequestForm:bidderId").value=bidderId;
		window.close();
	}
	
	function submitForm() {
		document.forms[0].submit();
	}
	
</script>
		</head>
		<body class="BODY_STYLE">
		<c:choose>
				<c:when test="${!pages$ServiceContractList.isViewModePopUp}">
			      <div class="containerDiv">
			    </c:when>
		</c:choose>
			<table cellpadding="0" cellspacing="0" border="0">
			<tr>
						 <c:choose>
							<c:when test="${!pages$ServiceContractList.isViewModePopUp}">
							 <td colspan="2">
						        <jsp:include page="header.jsp"/>
						     </td>
						    </c:when>
		                 </c:choose>
	            </tr>
				<tr>
				    <c:choose>
					 <c:when test="${!pages$ServiceContractList.isViewModePopUp}">
					  <td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					  </td>
				     </c:when>
		            </c:choose>
				    
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['contract.header']}"
										styleClass="HEADER_FONT" style="padding:10px" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
							
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" />
								<td valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" >
										<h:form id="formContractList">
											<div class="MARGIN" style="width: 95%">
											<table border="0" class="layoutTable" width="100%" style="margin-left:15px;margin-right:15px;" >
											<tr>
													<td >
													
														<h:outputText  id="errormsg"     escape="false" styleClass="ERROR_FONT" value="#{pages$ServiceContractList.errorMessages}"/>
						
													</td>
												</tr>
											</table>
												<table cellpadding="0" cellspacing="0" >
													<tr>
														<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
														<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>

												<div class="DETAIL_SECTION" >
													<h:outputLabel value="#{msg['commons.search']}" styleClass="DETAIL_SECTION_LABEL" />
													<table cellpadding="1px" cellspacing="3px"
														class="DETAIL_SECTION_INNER">
														<tr>
														<td width="97%">
														<table width="100%">
														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.Contract.Number']} :" />
															</td>

															<td width="25%">
																<h:inputText id="contractNum" 
																	value="#{pages$ServiceContractList.contractNumber}" />
															</td>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.contractStatus']} :" />
															</td>

															<td width="25%">
																<h:selectOneMenu id="contractStatus"
																	value="#{pages$ServiceContractList.contractStatus}">
																	<f:selectItem id="selectAll" itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ServiceContractList.statusList}" />
																</h:selectOneMenu>
															</td>
														</tr>

														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['serviceContract.contractorNumber']} :" />
															</td>

															<td width="25%">
																<h:inputText id="contractorNum"  
																	value="#{pages$ServiceContractList.contractorNumber}" />
															</td>


															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.contractType']} :" />
															</td>

															<td width="25%">
																<h:selectOneMenu id="contractType"
																	value="#{pages$ServiceContractList.selectOneContractType}"
																	binding="#{pages$ServiceContractList.cmbContractType}">
																	<f:selectItem id="selectAllContractTypes"
																		itemValue="-1" itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ServiceContractList.serviceContractTypeList}" />
																</h:selectOneMenu>
															</td>
															
														</tr>

														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.property.Name']} :" />
															</td>

															<td width="25%">
																<h:inputText id="propertyName"  
																    binding="#{pages$ServiceContractList.txtPropertyName}" 
																	value  ="#{pages$ServiceContractList.propertyName}" />
															</td>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['receiveProperty.ownershipType']} :" />
															</td>

															<td width="25%">
																<h:selectOneMenu id="ownershipType"
																	value="#{pages$ServiceContractList.ownershipType}" >
																	<f:selectItem id="selectAllOwnershipTypes"
																		itemValue="-1" itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.propertyOwnershipType}" />
																</h:selectOneMenu>
															</td>
														</tr>
														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['unit.unitNumber']} :" />
															</td>

															<td width="25%">
																<h:inputText id="unitNumber"  
																    binding="#{pages$ServiceContractList.txtUnitNumber}" 
																	value  ="#{pages$ServiceContractList.unitNumber}" />
															</td>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['inquiry.unitType']} :" />
															</td>

															<td width="25%">
																<h:selectOneMenu id="selectUnitType"
																	value="#{pages$ServiceContractList.unitType}" >
																	<f:selectItem id="selectAllUnitType" itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.unitTypeList}" />
																</h:selectOneMenu>
															</td>
														</tr>
														<tr>
														<td></td>
														</tr>
        
														<tr>
															<td colspan="4">
																<DIV class="BUTTON_TD" style="padding-bottom: 4px;">
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.search']}"
																		action="#{pages$ServiceContractList.searchContracts}" />
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.clear']}"
																		action="#{pages$ServiceContractList.reset}" />
																</DIV>
															</td>
														</tr>
														</table>
														</td>
														<td width="3%">
														&nbsp;
														</td>
														</tr>
													</table>
												</div>
											</div>

											<div style="padding: 10px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" align="center" style="width: 97%">

													<t:dataTable id="dt1" preserveDataModel="false"
														rowClasses="row1,row2"
														preserveSort="false" var="dataItem" rules="all"
														renderedIfEmpty="true" width="100%"
														rows="#{pages$ServiceContractList.paginatorRows}"
														value="#{pages$ServiceContractList.contractList}"
														binding="#{pages$ServiceContractList.dataTable}">

														<t:column id="colContractNumber" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.contractNumber']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.contractNumber}" />
														</t:column>
														<t:column id="colContractType" sortable="true" 	rendered="#{pages$ServiceContractList.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.contractType']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.contractTypeEn}" />
														</t:column>
														<t:column id="colContractTypeAr" sortable="true" 	rendered="#{!pages$ServiceContractList.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.contractType']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.contractTypeAr}" />
														</t:column>

														<t:column id="colContractorName" sortable="true">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['blacklistSearch.contractorName']}" />
															</f:facet>
															<!-- TODO: contractorNameEn/contractorNameAr has to be inserted in ContractView -->
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.contractorNameEn}" />
														</t:column>

														<t:column id="colStarttDate" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.startDate']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.startDate}">
																<f:convertDateTime
																	pattern="#{pages$ServiceContractList.dateFormat}"
																	timeZone="#{pages$ServiceContractList.timeZone}" />
															</t:outputText>
														</t:column>

														<t:column id="col3" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.endDate']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.endDate}">
																<f:convertDateTime
																	pattern="#{pages$ServiceContractList.dateFormat}"
																	timeZone="#{pages$ServiceContractList.timeZone}" />
															</t:outputText>
														</t:column>

														<t:column id="colStatusEn" sortable="true"
															rendered="#{pages$ServiceContractList.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.contractStatus']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.statusEn}" />
														</t:column>
														<t:column id="colStatusAr" style="padding-right:5px"
															sortable="true"
															rendered="#{!pages$ServiceContractList.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.contractStatus']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.statusAr}" />
														</t:column>

														<t:column id="col6" width="170px"
															style="whitespace:nowrap; text-align:right">
															<f:facet name="header">
																<t:outputText style="text-align:center"
																	value="#{msg['commons.action']}" />
															</f:facet>
															<t:commandLink  rendered="#{pages$ServiceContractList.isViewModePopUp && pages$ServiceContractList.isSingleSelectMode}" action="#{pages$ServiceContractList.cmdSelectContract_Click}" >
														        <h:graphicImage   style="width: 16;height: 16;border: 0" alt="#{msg['commons.select']}" url="../resources/images/select-icon.gif"/>
														    </t:commandLink>
															<t:commandLink  rendered="#{!pages$ServiceContractList.isViewModePopUp}" action="#{pages$ServiceContractList.imgEditContract_Click}" >
														       <h:graphicImage title="#{msg['commons.view']}" url="../resources/images/app_icons/Lease-contract.png"/>
														    </t:commandLink>
														    <t:commandLink rendered="#{!pages$ServiceContractList.isViewModePopUp}" action="#{pages$ServiceContractList.imgManageContract_Click}">
													           <h:graphicImage id="manageIcon" title="#{msg['contract.search.toolTips.manage']}" url="../resources/images/app_icons/Request-detail.png" />&nbsp;
													        </t:commandLink>
															
														</t:column>
													</t:dataTable>
												</div>
												<t:div styleClass="contentDivFooter" style="width:98%">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$ServiceContractList.recordSize}"/>
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">
																<CENTER>
																	<t:dataScroller id="scroller" for="dt1"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$ServiceContractList.paginatorMaxPages}"
																		immediate="false" paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																		styleClass="SCH_SCROLLER" pageIndexVar="pageNumber"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false">

																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblF" />
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR" />
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF" />
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL" />
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.pageNumber}" />
																					</td>
																				</tr>
																			</table>
																		</t:div>
																	</t:dataScroller>
																</CENTER>
															</td>
														</tr>
													</table>
												</t:div>
											</div>
										</h:form>
									</div>
								</td>
							</tr>

						</table>
					</td>
				</tr>
				<c:choose>
				<c:when test="${!pages$ServiceContractList.isViewModePopUp}">
			    
			    				
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				</c:when>
		</c:choose>
				
			</table>
			<c:choose>
				<c:when test="${!pages$ServiceContractList.isViewModePopUp}">
			      </div>
			    </c:when>
		</c:choose>
			
		</body>

	</html>
</f:view>