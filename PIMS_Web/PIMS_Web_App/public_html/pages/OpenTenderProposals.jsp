<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="javascript" type="text/javascript">
	
	function showEditTenderProposalPopup() 
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('EditProposalDetails.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	}
	
	function showViewTenderProposalPopup() 
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('ViewProposalDetails.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	}
	
	function SearchMemberPopup(){
   		var screen_width = screen.width;
	   	var screen_height = screen.height;
	   	var screen_top = screen.width/6;
	   	var screen_left = screen.height/5;
	   	window.open('SearchMemberPopup.jsf','_blank','width='+(screen_width-280)+',height='+(screen_height-350)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}

	function showAddProposalRecommendationPopup()
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('AddProposalRecommendation.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	}

</script>


<f:view>
<script>
	function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		
		function affectExternalUserControls(chk){
			if(chk.checked){
				document.getElementById("OpenTenderProposalsForm:lblExternalUserName").disabled = false;
				document.getElementById("OpenTenderProposalsForm:txtExternalUserName").disabled = false;
				document.getElementById("OpenTenderProposalsForm:btnAddExternalUser").disabled = false;
			}
			else {
				document.getElementById("OpenTenderProposalsForm:lblExternalUserName").disabled = true;
				document.getElementById("OpenTenderProposalsForm:txtExternalUserName").disabled = true;
				document.getElementById("OpenTenderProposalsForm:btnAddExternalUser").disabled = true;
			}
		}
</script>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

			<style>
		.rich-calendar-input{
		width:150px;
		}
		</style>

		</head>

		<!-- Header -->

		<body class="BODY_STYLE">
    				<div class="containerDiv">

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['openTenderProposals.heading']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:465px">

										<h:form id="OpenTenderProposalsForm"
											styleClass="OpenTenderProposalsForm"
											enctype="multipart/form-data" style="width:99%">
										
											<div >
												<table border="0" class="layoutTable"
													style="margin-left:10px;margin-right:10px;">
													<tr>
														<td>
															<h:outputText
																value="#{pages$OpenTenderProposals.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$OpenTenderProposals.successMessages}"
																escape="false" styleClass="INFO_FONT" />
														</td>
													</tr>
												</table>
											</div>

											<div class="MARGIN" style="width:98%">




												<table cellpadding="0" cellspacing="0" width="98%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>

												<t:div styleClass="TAB_PANEL_INNER">

													<rich:tabPanel style="width:98%;height:235px;"
														headerSpacing="0"
														binding="#{pages$OpenTenderProposals.tabPanel}">
														<rich:tab id="tenderDetailsTab"
															label="#{msg['commons.tab.tenderDetails']}">
															<t:div id="divTenderDetail" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid columns="4" cellpadding="1"
																	styleClass="TAB_DETAIL_SECTION_INNER" cellspacing="5" width="100%">

																	<h:outputLabel
																		value="#{msg['tenderManagement.tenderNumber']}:"></h:outputLabel>
																	<h:inputText id="tenderNumber" readonly="true"
																			style="width: 170px; height: 18px"
																			binding="#{pages$OpenTenderProposals.tenderNumber}"
																			value="#{pages$OpenTenderProposals.tenderDetailView.tenderNumber}" />

																	<h:outputLabel
																		value="#{msg['tenderManagement.tenderStatus']}:"></h:outputLabel>
																	<h:selectOneMenu id="tenderStatus" 
																		readonly="true"
																		style="width: 170px; height: 18px"																		 
																		value="#{pages$OpenTenderProposals.tenderDetailView.tenderStatusId}">
																		<f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																		<f:selectItems value="#{pages$ApplicationBean.tenderStatusList}" />
																	</h:selectOneMenu>

																	<h:outputLabel
																		value="#{msg['tenderManagement.tenderType']}:"></h:outputLabel>
																	<h:selectOneMenu id="tenderType"
																		readonly="true"
																		style="width: 170px; height: 18px"
																		binding="#{pages$OpenTenderProposals.tenderType}"
																		value="#{pages$OpenTenderProposals.tenderDetailView.tenderTypeId}">
																		<f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																		<f:selectItems value="#{pages$ApplicationBean.tenderTypeList}" />
																	</h:selectOneMenu>
																	
																	<h:outputLabel
																		value="#{msg['tenderManagement.tenderDescription']}:"></h:outputLabel>
																	<h:inputText id="tenderDescription" readonly="true"
																		style="width: 170px; height: 18px"
																		binding="#{pages$OpenTenderProposals.tenderDescription}" 
																		value="#{pages$OpenTenderProposals.tenderDetailView.tenderRemarks}" />

																	<h:outputLabel
																		value="#{msg['tenderManagement.estimatedCost']}:"></h:outputLabel>
																	<h:inputText id="estimatedCost" readonly="true"
																		style="width: 170px; height: 18px"
																		binding="#{pages$OpenTenderProposals.estimatedCost}" 
																		value="#{pages$OpenTenderProposals.tenderDetailView.tenderExpectedCost}" />
																		
																	<h:outputLabel
																		value="#{msg['tenderManagement.tenderDocSellingPrice']}:"></h:outputLabel>
																	<h:inputText id="tenderDocSellingPrice" readonly="true"
																		style="width: 170px; height: 18px"
																		binding="#{pages$OpenTenderProposals.tenderDocSellingPrice}" 
																		value="#{pages$OpenTenderProposals.tenderDetailView.tenderPrice}" />
																		
																	<h:outputLabel
																		value="#{msg['tenderManagement.startDate']}:"></h:outputLabel>
																	<h:panelGroup>
																		<rich:calendar disabled="true" id="startDate" rendered="true"
																			value="#{pages$OpenTenderProposals.tenderDetailView.tenderIssueDate}"
																			datePattern="#{pages$OpenTenderProposals.dateFormat}"
																			locale="#{pages$OpenTenderProposals.locale}" popup="true"
																			showApplyButton="false" enableManualInput="false" inputStyle="width: 70px" />
																		
																		<h:selectOneMenu rendered="false" id="startTimeHH" style="VERTICAL-ALIGN: middle; width: 40px; height: 24px" required="false"
																			binding="#{pages$OpenTenderProposals.startDateHoursMenu}"
																			value="#{pages$OpenTenderProposals.hhStart}">
																			<f:selectItem itemValue="" itemLabel="hh" />
																			<f:selectItems value="#{pages$ApplicationBean.hours}" />
																		</h:selectOneMenu>
																		
																		<h:selectOneMenu rendered="false" id="startTimeMM" style="VERTICAL-ALIGN: middle; width: 40px; height: 24px" required="false"
																			binding="#{pages$OpenTenderProposals.startDateMinsMenu}"
																			value="#{pages$OpenTenderProposals.mmStart}">
																			<f:selectItem itemValue="" itemLabel="mm" />
																			<f:selectItems value="#{pages$ApplicationBean.minutes}" />
																		</h:selectOneMenu> 
																	</h:panelGroup>
																	
																	<h:outputLabel
																		value="#{msg['tenderManagement.endDate']}:"></h:outputLabel>
																	<h:panelGroup>
																		<rich:calendar disabled="true" id="endDate" rendered="true"
																			value="#{pages$OpenTenderProposals.tenderDetailView.tenderEndDate}"
																			datePattern="#{pages$OpenTenderProposals.dateFormat}"
																			locale="#{pages$OpenTenderProposals.locale}" popup="true"
																			showApplyButton="false" enableManualInput="false" inputStyle="width: 70px" />
																			
																		<h:selectOneMenu rendered="false" id="endTimeHH" style="VERTICAL-ALIGN: middle; width: 40px; height: 24px" required="false"
																			binding="#{pages$OpenTenderProposals.endDateHoursMenu}"
																			value="#{pages$OpenTenderProposals.hhEnd}">
																			<f:selectItem itemValue="" itemLabel="hh" />
																			<f:selectItems value="#{pages$ApplicationBean.hours}" />
																		</h:selectOneMenu>
																		
																		<h:selectOneMenu rendered="false" id="endTimeMM" style="VERTICAL-ALIGN: middle; width: 40px; height: 24px" required="false"
																			binding="#{pages$OpenTenderProposals.endDateMinsMenu}"
																			value="#{pages$OpenTenderProposals.mmEnd}">
																			<f:selectItem itemValue="" itemLabel="mm" />
																			<f:selectItems value="#{pages$ApplicationBean.minutes}" />
																		</h:selectOneMenu>
																	</h:panelGroup>

																</t:panelGrid>
															</t:div>
														</rich:tab>
														
														
														
														
														
														
														
														
														
															<rich:tab 
																style="width:95%;white-space:nowrap;"
																label="#{msg['receiveProperty.tab.receivingTeam']}">
																
																<t:div style="text-align:center; padding:5px;">
																	<t:div style="width:95%; text-align:left;">
																		<h:selectBooleanCheckbox id="chkAllowExternalAdd" onclick="return affectExternalUserControls(this);"></h:selectBooleanCheckbox><h:outputLabel id="idsx" value="#{msg['teamMember.check.label']}" for="chkAllowExternalAdd"></h:outputLabel>
																	</t:div>
																		
																	<t:panelGrid id="receivingButtonGrid2" width="100%"
																	columnClasses="BUTTON_TD" columns="4">
																	
																	<h:outputLabel id="lblExternalUserName" value="#{msg['teamMember.label.UserName']}"></h:outputLabel>
																	<h:inputText id="txtExternalUserName" value="#{pages$OpenTenderProposals.externalUserName}"></h:inputText>
																	
																	<h:commandButton styleClass="BUTTON" style="width:150px" id="btnAddExternalUser"
																		value="#{msg['teamMember.button.label']}"
																		action="#{pages$OpenTenderProposals.addExternalTeamMember}" />
																		
																	<h:commandButton styleClass="BUTTON"
																	   style="width:150px;"
																		binding="#{pages$OpenTenderProposals.addReceiveingTeam}"
																		value="#{msg['receiveProperty.AddMember']}"
																		action="#{pages$OpenTenderProposals.showReceivingSearchPopup}" />
																		
																	</t:panelGrid>
																</t:div>
																
																
																
																<t:div style="text-align:center;">
																	<t:div styleClass="contentDiv" style="width:94%;">
																		<t:dataTable id="test8" rows="5" width="100%"
																			value="#{pages$OpenTenderProposals.receivingDataList}"
																			binding="#{pages$OpenTenderProposals.receivingTeamGrid}"
																			preserveDataModel="false" preserveSort="false"
																			var="dataItemRCV" rowClasses="row1,row2" rules="all"
																			renderedIfEmpty="true">




																			<t:column id="dataItemRCV2" sortable="true">


																				<f:facet name="header">

																					<t:outputText value="#{msg['member.fullName']}" />

																				</f:facet>
																				<t:outputText styleClass="A_LEFT"
																					value="#{dataItemRCV.fullNamePrimary}"  rendered="#{dataItemRCV.teamMemberIsDeleted == 0}"/>
																			</t:column>
																			<t:column id="dataItemRCV4" sortable="true">


																				<f:facet name="header">

																					<t:outputText value="#{msg['member.loginName']}" />

																				</f:facet>
																				<t:outputText styleClass="A_LEFT" title=""
																					value="#{dataItemRCV.userName}"    rendered="#{dataItemRCV.teamMemberIsDeleted == 0}"/>
																			</t:column>
																			<t:column id="dataItemRCV5" sortable="true">


																				<f:facet name="header">

																					<t:outputText value="#{msg['member.groupName']}" />

																				</f:facet>
																				<t:outputText styleClass="A_LEFT" title=""
																					value="#{dataItemRCV.groupNamePrimary}"   rendered="#{dataItemRCV.teamMemberIsDeleted == 0}"/>
																			</t:column>

																			<t:column>
																				<f:facet name="header">
																					<t:outputText id="lbl_delete_RCV"
																						value="#{msg['commons.delete']}" />
																				</f:facet>
																				<h:commandLink id="deleteIconRCV" rendered="#{dataItemRCV.teamMemberIsDeleted == 0}"
																					action="#{pages$OpenTenderProposals.cmdReceivingTeamMemberDelete_Click}">
																					<h:graphicImage id="delete_IconRCV"
																						title="#{msg['commons.delete']}"
																						url="../resources/images/delete_icon.png" />
																				</h:commandLink>

																			</t:column>


																		</t:dataTable>
																	</t:div>



																	<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																		style="width:95%;">
																		<t:panelGrid cellpadding="0" cellspacing="0"
																			width="100%" columns="2"
																			columnClasses="RECORD_NUM_TD">

																			<t:div styleClass="RECORD_NUM_BG">
																				<t:panelGrid cellpadding="0" cellspacing="0"
																					columns="3" columnClasses="RECORD_NUM_TD">

																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />

																					<h:outputText value=" : "
																						styleClass="RECORD_NUM_TD" />

																					<h:outputText
																						value="#{pages$OpenTenderProposals.receivingTeamRecordSize}"
																						styleClass="RECORD_NUM_TD" />

																				</t:panelGrid>
																			</t:div>

																			 <t:dataScroller id="extendscroller" for="test8" paginator="true"
																											fastStep="1"  paginatorMaxPages="2" immediate="false"
																											paginatorTableClass="paginator"
																											renderFacetsIfSinglePage="true" 								
																										    paginatorTableStyle="grid_paginator" layout="singleTable"
																												paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																												paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;" 
																											pageIndexVar="pageNumber"
																											styleClass="SCH_SCROLLER" >
														
																											<f:facet name="first">
																													<t:graphicImage url="../#{path.scroller_first}" id="lblFinquiry"></t:graphicImage>
																												</f:facet>
																												<f:facet name="fastrewind">
																													<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFRinquiry"></t:graphicImage>
																												</f:facet>
																												<f:facet name="fastforward">
																													<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFinquiry"></t:graphicImage>
																												</f:facet>
																												<f:facet name="last">
																													<t:graphicImage url="../#{path.scroller_last}" id="lblLinquiry"></t:graphicImage>
																												</f:facet>
																											<t:div styleClass="PAGE_NUM_BG" rendered="true">
																					<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																					<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																				</t:div>
																			</t:dataScroller>

																		</t:panelGrid>
																	</t:div>

																</t:div>
															</rich:tab>
														<rich:tab id="sessionDetailsTab"
															label="#{msg['commons.tab.sessionDetails']}">
															<t:div id="divSessionDetail" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid columns="4" cellpadding="1"
																	styleClass="TAB_DETAIL_SECTION_INNER" cellspacing="5" width="100%">

																	<h:outputLabel
																		value="#{msg['tenderManagement.openTender.sessionDate']}:">
																	</h:outputLabel>
																	<rich:calendar id="sessionDate" rendered="true"																			
																			value="#{pages$OpenTenderProposals.tenderSessionView.tenderSessionDate}"
																			datePattern="#{pages$OpenTenderProposals.dateFormat}"
																			locale="#{pages$OpenTenderProposals.locale}" popup="true"
																			showApplyButton="false" enableManualInput="false" />
																			
																	<h:outputLabel
																		value="#{msg['tenderManagement.openTender.sessionTime']}:">
																	</h:outputLabel>
																	<h:panelGroup>
																		<h:selectOneMenu id="sessionTimeHH" style="VERTICAL-ALIGN: middle; width: 40px; height: 24px" required="false"
																				binding="#{pages$OpenTenderProposals.sessionTimeHourMenu}"
																				value="#{pages$OpenTenderProposals.tenderSessionView.tenderSessionTimeHH}">
																				<f:selectItem itemValue="" itemLabel="HH" />
																				<f:selectItems value="#{pages$ApplicationBean.hours}" />
																		</h:selectOneMenu>
																		<h:outputText value=" : " />
																		<h:selectOneMenu id="sessionTimeMM" style="VERTICAL-ALIGN: middle; width: 40px; height: 24px" required="false"
																				binding="#{pages$OpenTenderProposals.sessionTimeMinsMenu}"
																				value="#{pages$OpenTenderProposals.tenderSessionView.tenderSessionTimeMM}">
																				<f:selectItem itemValue="" itemLabel="MM" />
																				<f:selectItems value="#{pages$ApplicationBean.minutes}" />
																		</h:selectOneMenu>
																	</h:panelGroup>
																	
																	<h:outputLabel
																		value="#{msg['tenderManagement.openTender.sessionLocation']}:">
																	</h:outputLabel>
																	<h:inputText
																				id="sessionLocation"
																				binding="#{pages$OpenTenderProposals.sessionLocation}"
																				value="#{pages$OpenTenderProposals.tenderSessionView.tenderSessionLocation}"
																				style="width: 170px; height: 18px">
																	</h:inputText>
			 														
																	<h:outputLabel
																		value="#{msg['tenderManagement.openTender.sessionRemarks']}:">
																	</h:outputLabel>
																	<h:inputTextarea
																					id="sessionRemarks"
																					binding="#{pages$OpenTenderProposals.sessionRemarks}"
																					value="#{pages$OpenTenderProposals.tenderSessionView.tenderSessionRemarks}"
																					style="width: 170px;">
																	</h:inputTextarea>
																	
																</t:panelGrid>
															</t:div>
														</rich:tab>
														
														<rich:tab 
																id="proposalsTab"
																label="#{msg['commons.tab.proposals']}">
															<t:div style="width:100%;">
																<t:panelGrid width="100%" columns="1" cellpadding="0" cellspacing="0">																	
																	<t:div styleClass="contentDiv">
																		<t:dataTable id="proposalDataTable" 
																					renderedIfEmpty="true"
																					var="tenderProposal" 
																					binding="#{pages$OpenTenderProposals.proposalDataTable}" 
																					value="#{pages$OpenTenderProposals.proposalDataList}"
																		 			preserveDataModel="false" preserveSort="false" rowClasses="row1,row2" rules="all" 
																		 			rows="#{pages$OpenTenderProposals.paginatorRows}"
																		 			width="100%">
																			<t:column width="10%" sortable="true">
																				<f:facet name="header">
																					<t:outputText value="#{msg['tenderManagement.openProposal.proposalNumber']}"/>
																				</f:facet>
																				<t:outputText value="#{tenderProposal.proposalNumber}" />																				
																			</t:column>
																			<t:column width="12%" sortable="true">
																				<f:facet name="header">
																					<t:outputText value="#{msg['tenderManagement.openProposal.proposalDate']}"/>
																				</f:facet>
																				<t:outputText value="#{tenderProposal.proposalDate}" />																				
																			</t:column>
																			<t:column width="15%" sortable="true">
																				<f:facet name="header">
																					<t:outputText value="#{msg['tenderManagement.openProposal.proposalContractor']}"/>
																				</f:facet>
																				<t:outputText value="#{pages$OpenTenderProposals.isEnglishLocale ? tenderProposal.contractorNameEn : tenderProposal.contractorNameAr}" />																				
																			</t:column>																			
																			<t:column width="14%" sortable="true">
																				<f:facet name="header">
																					<t:outputText value="#{msg['tenderManagement.openProposal.proposalAmount']}"/>
																				</f:facet>
																				<t:outputText value="#{tenderProposal.proposalAmount}" />																				
																			</t:column>
																			<t:column width="20%" sortable="true">
																				<f:facet name="header">
																					<t:outputText value="#{msg['tenderManagement.openProposal.recommendation']}"/>
																				</f:facet>
																				<t:outputText value="#{tenderProposal.recommendation}" />																				
																			</t:column>
																			<t:column width="10%" sortable="true">
																				<f:facet name="header">
																					<t:outputText value="#{msg['tenderManagement.openProposal.proposalStatus']}"/>
																				</f:facet>
																				<t:outputText value="#{pages$OpenTenderProposals.isEnglishLocale ? tenderProposal.statusEn : tenderProposal.statusAr}" />																				
																			</t:column>																			
																			<t:column width="19%">
																				<f:facet name="header">
																					<t:outputText value="#{msg['tenderManagement.openProposal.proposalActions']}"/>																					
																				</f:facet>			
																				
																				<t:commandLink id="lnkAddAttachment" rendered="#{!tenderProposal.proposalUploaded  && pages$OpenTenderProposals.canAttachFile}" actionListener="#{pages$OpenTenderProposals.openUploadPopup}" value="" style="padding-left:1px;padding-right:1px;">
																					<h:graphicImage id="fileAddIcon"  title="#{msg['attachment.grid.attachFileCol']}" url="../resources/images/app_icons/addAttachment.png" />
																				</t:commandLink>
																				<t:commandLink rendered="#{ tenderProposal.proposalUploaded && pages$OpenTenderProposals.canAttachFile}" actionListener="#{pages$OpenTenderProposals.removeAttachment}" value="" style="padding-left:1px;padding-right:1px;" onclick="if (!confirm('#{msg['attachment.messages.confirmRemove']}')) return">
																					<h:graphicImage id="fileRemoveIcon"  title="#{msg['commons.deleteAttachment']}" url="../resources/images/delete.gif" />
																				</t:commandLink>
																				<t:commandLink id="lnkDownload" rendered="#{tenderProposal.proposalUploaded  && pages$OpenTenderProposals.canAttachFile}" action="#{pages$OpenTenderProposals.download}" value="" style="padding-left:1px;padding-right:1px;">
																					<h:graphicImage id="fileDownloadIcon"  title="#{msg['attachment.buttons.download']}" url="../resources/images/app_icons/downloadAttachment.png" />
																				</t:commandLink>	
																																				
																				<h:commandLink id="editLink" rendered="#{tenderProposal.showEdit}"
																					action="#{pages$OpenTenderProposals.editLink_action}">
																					<h:graphicImage id="editIcon"
																						title="#{msg['commons.edit']}"
																						url="../resources/images/edit-icon.gif" />
																				</h:commandLink>
																				<t:outputLabel value=" " rendered="true"></t:outputLabel>
																				<h:commandLink id="viewLink" rendered="#{tenderProposal.showView}"
																					action="#{pages$OpenTenderProposals.viewLink_action}">
																					<h:graphicImage id="viewIcon"
																						title="#{msg['commons.view']}"
																						url="../resources/images/app_icons/View-Contract.png" />
																				</h:commandLink>
																				<t:outputLabel value=" " rendered="true"></t:outputLabel>
																				<h:commandLink id="addLink" rendered="#{tenderProposal.showAdd}"
																					action="#{pages$OpenTenderProposals.addLink_action}">
																					<h:graphicImage id="addIcon"
																						title="#{msg['commons.add']}"
																						url="../resources/images/detail-icon.gif" />
																				</h:commandLink>
																				<t:outputLabel value=" " rendered="true"></t:outputLabel>																				
																				<h:commandLink id="cancelLink" rendered="false"
																					action="#{pages$OpenTenderProposals.cancelLink_action}">
																					<h:graphicImage id="cancelIcon"
																						title="#{msg['commons.cancel']}"
																						url="../resources/images/app_icons/Cancel-Contract.png" />
																				</h:commandLink>
																				<t:outputLabel value=" " rendered="true"></t:outputLabel>
																				<h:commandLink id="deleteLink" rendered="#{tenderProposal.showDelete}"
																					action="#{pages$OpenTenderProposals.deleteLink_action}">
																					<h:graphicImage id="deleteIcon"
																						title="#{msg['commons.delete']}"
																						url="../resources/images/delete.gif" />
																				</h:commandLink>
																				<t:outputLabel value=" " rendered="true"></t:outputLabel>
																				<h:commandLink id="rejectLink" rendered="#{tenderProposal.showReject}"
																					action="#{pages$OpenTenderProposals.rejectLink_action}">
																					<h:graphicImage id="rejectIcon"
																						title="#{msg['commons.reject']}"
																						url="../resources/images/app_icons/Renew-Contract.png" />
																				</h:commandLink>
																			</t:column>
																		</t:dataTable>
																	</t:div>
																	<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99%;">
																	<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
																		<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																			<t:div styleClass="JUG_NUM_REC_ATT">
																				<h:outputText value="#{msg['commons.records']}"/>
																				<h:outputText value=" : "/>
																				<h:outputText value="#{pages$OpenTenderProposals.recordSize}" />
																			</t:div>
																		</t:panelGroup>
																		<t:panelGroup>
																			<t:dataScroller id="proposalScroller" for="proposalDataTable" paginator="true"  
																				fastStep="1" paginatorMaxPages="#{pages$OpenTenderProposals.paginatorMaxPages}" immediate="false"
																				paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true" 												
																				paginatorTableStyle="grid_paginator" layout="singleTable" 
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																				paginatorActiveColumnStyle="font-weight:bold;"
																				paginatorRenderLinkForActive="false" 
																				pageIndexVar="pageNumber"
																				styleClass="JUG_SCROLLER">
																				<f:facet name="first">
																					<t:graphicImage url="../#{path.scroller_first}" id="lblF1"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastrewind">
																					<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR1"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastforward">
																					<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF1"></t:graphicImage>
																				</f:facet>
																				<f:facet name="last">
																					<t:graphicImage url="../#{path.scroller_last}" id="lblL1"></t:graphicImage>
																				</f:facet>
																				<t:div styleClass="PAGE_NUM_BG" rendered="true">
																					<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																					<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																				</t:div>
																			</t:dataScroller>
																		</t:panelGroup>
																	</t:panelGrid>
																</t:div>
																</t:panelGrid>
															</t:div>															
														</rich:tab>

																	
														
														

														<rich:tab id="attachmentTab"
															label="#{msg['commons.attachmentTabHeading']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>

														<rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>









													</rich:tabPanel>

												</t:div>
												<table cellpadding="0" cellspacing="0" width="98%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<table cellpadding="1px" cellspacing="3px" width="100%">



													<tr>
														<td colspan="6" class="BUTTON_TD">
														<pims:security screen="Pims.ServicesMgmt.OpenTenderProposals.SaveButton" action="create">
															<h:commandButton id="saveButton"
																			 styleClass="BUTTON"
																			 value="#{msg['commons.saveButton']}"
																			 action="#{pages$OpenTenderProposals.saveAction}">
															</h:commandButton>
															</pims:security>
															
															<h:commandButton id="cancelButton"
																			 styleClass="BUTTON"
																			 value="#{msg['commons.cancel']}"
																			 action="#{pages$OpenTenderProposals.cancelAction}">
															</h:commandButton>
														</td>

													</tr>


												</table>
											</div>
									</div>
									</h:form>

									</div>
								</td>
							</tr>
						</table>


					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
  		  </div>
			<script type="text/javascript">
				document.getElementById("OpenTenderProposalsForm:lblExternalUserName").disabled = true;
				document.getElementById("OpenTenderProposalsForm:txtExternalUserName").disabled = true;
				document.getElementById("OpenTenderProposalsForm:btnAddExternalUser").disabled = true;
				document.getElementById("OpenTenderProposalsForm:chkAllowExternalAdd").checked = false;
				document.getElementById("OpenTenderProposalsForm:txtExternalUserName").value = "";
			</script>
		</body>
	</html>
</f:view>