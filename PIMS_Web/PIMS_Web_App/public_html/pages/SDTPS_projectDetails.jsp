<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for managing a Project and its followups
  --%>

<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript"> 

		function showMilestonePopup()
		{
			var screen_width = 1024;
			var screen_height = 450;
			var popup_width = screen_width-244;
			var popup_height = screen_height-120;
			var leftPos = (screen_width-popup_width)/2 -5, topPos = (screen_height-popup_height)/2 - 20;
			var popup = window.open('SDTPS_addMilestone.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
			popup.focus();
		}
		
		function showUploadPopup()
		{
		      var screen_width = 1024;
		      var screen_height = 450;
		      var popup_width = screen_width-530;
		      var popup_height = screen_height-150;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		
		function populateStudy(studyId){
			document.getElementById("detailsFrm:studyId").value=studyId;
			document.getElementById("detailsFrm:loadStudy").value='true';
		    document.forms[0].submit();
		}
		
		function receiveSelectedStudies() 
		{ 
            document.forms[0].submit();
		}

		
		
		function populateProperty(propertyId){
			document.getElementById("detailsFrm:propertyId").value=propertyId;
			document.getElementById("detailsFrm:loadProperty").value='true';
		    document.forms[0].submit();
		}
		
		function openStudySearchPopup()
        {
	         var screen_width = 990;
             var screen_height = 475;
             var popup_width = screen_width;
    		 var popup_height = screen_height;
             var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
             window.open('studySearch.jsf?context=ProjectDetails','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
       }
				
		function openStudyViewPopup() 
		{
			var screen_width = 980;
			var screen_height = 350;
	        var popup_width = screen_width;
	        var popup_height = screen_height + 30;
	        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
	        window.open('studyManage.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		}
	   function submitForm()
	   {	
		 window.document.forms[0].submit();
	   }	
		
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 <script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>


	
</head>
	<body class="BODY_STYLE">
	<div class="containerDiv" style="width:100%;">
	
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header --> 
	<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
		<c:choose>
			<c:when test="${!pages$projectDetails.popupViewMode}">
				<tr style="height:84px;width:100%;#height:84px;#width:100%;">
					<td colspan="2">
						<jsp:include page="SDTPS_header.jsp" />
					</td>
				</tr>
			</c:when>
		</c:choose>
		
		<tr width="100%">
			<c:choose>
				<c:when test="${!pages$projectDetails.popupViewMode}">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
				</c:when>
			</c:choose>
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99.2%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{pages$projectDetails.heading}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>
				<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%">
					<tr valign="top">
								<td width="100%" valign="top" nowrap="nowrap">
						<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
							<div class="SCROLLABLE_SECTION" style="height:470px;width:100%;#height:470px;#width:100%;">
									<h:form id="detailsFrm" enctype="multipart/form-data" style="WIDTH: 97.6%;">
									
								<div>
									<div styleClass="MESSAGE"> 
									<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
										<tr>
											<td>
												<h:outputText value="#{pages$projectDetails.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
												<h:outputText value="#{pages$projectDetails.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
											</td>
										</tr>
									</table>
									</div>
									<h:inputHidden id="studyId" value="#{pages$projectDetails.hiddenStudyId}"></h:inputHidden>
									<h:inputHidden id="loadStudy" value="#{pages$projectDetails.hiddenLoadStudy}"></h:inputHidden>	
									
									<h:inputHidden id="propertyId" value="#{pages$projectDetails.hiddenPropertyId}"></h:inputHidden>
									<h:inputHidden id="loadProperty" value="#{pages$projectDetails.hiddenLoadProperty}"></h:inputHidden>
					
							<t:div style="width:97%; margin:13px;" rendered="#{pages$projectDetails.showFollowUpActionPanel}">
								<t:panelGrid id="followUpActionGrid" cellpadding="1px" width="100%" cellspacing="5px" columns="4">
											
											<h:outputLabel styleClass="LABEL" value="#{msg['project.details.completionPercent']}: "/>
											<h:inputText id="completionPercent" styleClass="INPUT_TEXT READONLY" value="#{pages$projectDetails.completionPercent}" readonly="true"></h:inputText>
											<h:outputLabel styleClass="LABEL" value="#{msg['project.details.lastAchievedMilestone']}: "/>
											<h:inputText id="lasAchievedMileston" styleClass="INPUT_TEXT READONLY" value="#{pages$projectDetails.lastAchievedMilestone}" readonly="true"></h:inputText>
				
											<t:panelGroup>
						                    	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
												<h:outputLabel styleClass="LABEL" value="#{msg['project.details.newCompletionPercent']}: "/>
											</t:panelGroup>
											<h:inputText id="newCompletionPercent" styleClass="INPUT_TEXT  #{pages$projectDetails.readonlyStyleClassFollowUp}" value="#{pages$projectDetails.newCompletionPercent}" readonly="#{pages$projectDetails.followUpReadonlyMode}"></h:inputText>							
											<t:panelGroup>
						                    	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
												<h:outputLabel styleClass="LABEL" value="#{msg['project.details.newAchievedMilestone']}: "/>
											</t:panelGroup>
											<h:selectOneMenu id="milestoneCombo" styleClass="SELECT_MENU  #{pages$projectDetails.readonlyStyleClassFollowUp}" required="false" readonly="#{pages$projectDetails.followUpReadonlyMode}"
														value="#{pages$projectDetails.newMilestoneId}">
														<f:selectItem itemValue="0" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
														<f:selectItems value="#{pages$projectDetails.projectMilstoneSelectList}" />
											</h:selectOneMenu>

											<t:panelGroup>
						                    	<h:outputLabel rendered="false" styleClass="mandatory" value="*"></h:outputLabel>
												<h:outputLabel id="followUpDateLabel" styleClass="LABEL" value="#{msg['project.details.followUpDate']}: "/>
											</t:panelGroup>
					                   		<rich:calendar id="followUpDate" styleClass="#{pages$projectDetails.readonlyStyleClassFollowUp}" value="#{pages$projectDetails.followUpDate}"  locale="#{pages$projectDetails.locale}" popup="true" datePattern="#{pages$projectDetails.dateFormat}" disabled="#{pages$projectDetails.followUpReadonlyMode || true}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px"/>												
											<t:panelGroup>
						                    	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
									        	<h:outputLabel id="followUpRemarksLabel" styleClass="LABEL"  value="#{msg['commons.remarks']}: "></h:outputLabel>
									        </t:panelGroup>
									        <t:inputTextarea styleClass="TEXTAREA #{pages$projectDetails.readonlyStyleClassFollowUp}" value="#{pages$projectDetails.followUpRemarks}" rows="6" style="width: 200px; height:30px;" binding="#{pages$projectDetails.followUpremarksField}"/>

											<t:panelGroup>
						                    	<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$projectDetails.actualStartDateReadOnly}"></h:outputLabel>
												<h:outputLabel id="actualStartDateLabel" styleClass="LABEL" value="#{msg['project.details.actualStartDate']}: "/>
											</t:panelGroup>
					                   		<rich:calendar id="actualStartDate" styleClass="#{pages$projectDetails.readonlyStyleClassFollowUp}" value="#{pages$projectDetails.actualStartDate}"  locale="#{pages$projectDetails.locale}" popup="true" datePattern="#{pages$projectDetails.dateFormat}" disabled="#{pages$projectDetails.followUpReadonlyMode || pages$projectDetails.actualStartDateReadOnly}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px"/>
						                    

											<t:div style="height:10px;"/>
						   	</t:panelGrid>
						</t:div>
						<t:div  style="width:97%; margin:10px;" rendered="#{pages$projectDetails.showActionPanel}">
							<t:panelGrid id="actionGrid" cellpadding="1px" width="100%" cellspacing="5px" columns="2">
											
											<t:panelGroup rendered="#{pages$projectDetails.showHandOverActionPanel}">
												<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
												<h:outputLabel id="handOverDateLabel" styleClass="LABEL" value="#{msg['project.details.handOverDate']}: "/>
							                    <rich:calendar id="handOverDate" value="#{pages$projectDetails.handOverDate}"  locale="#{pages$projectDetails.locale}" popup="true" datePattern="#{pages$projectDetails.dateFormat}" disabled="#{pages$projectDetails.handOverReadonlyMode}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px"/>
						                    </t:panelGroup>
						                    <t:panelGroup rendered="#{pages$projectDetails.showInitialReceivingActionPanel}">
						                    	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
												<h:outputLabel id="initialDateLabel" styleClass="LABEL PROJ_FU_LABEL1" value="#{msg['project.details.initialReceivingDate']}: "/>
						                   		<rich:calendar id="initialDate" value="#{pages$projectDetails.initialReceivingDate}"  locale="#{pages$projectDetails.locale}" popup="true" datePattern="#{pages$projectDetails.dateFormat}" disabled="#{pages$projectDetails.initialReceivingReadonlyMode}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px"/>
						                    </t:panelGroup>
						                    <t:panelGroup rendered="#{pages$projectDetails.showFinalReceivingActionPanel}">
						                    	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>	
												<h:outputLabel id="finalDateLabel" styleClass="LABEL" value="#{msg['project.details.finalReceivingDate']}: "/>
						                   		<rich:calendar id="finalDate" value="#{pages$projectDetails.finalReceivingDate}"  locale="#{pages$projectDetails.locale}" popup="true" datePattern="#{pages$projectDetails.dateFormat}" disabled="#{pages$projectDetails.finalReceivingReadonlyMode}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px"/>												
						                    </t:panelGroup>
						                    <t:panelGroup>
						                    	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
										        <h:outputLabel id="remarksLabel" styleClass="LABEL"  value="#{msg['commons.remarks']}: "></h:outputLabel>
										        <t:inputTextarea styleClass="TEXTAREA" value="#{pages$projectDetails.remarks}" rows="6" style="width: 200px; height:30px;" binding="#{pages$projectDetails.remarksField}"/>
											</t:panelGroup>
											<t:panelGroup rendered="#{pages$projectDetails.showInitialReceivingActionPanel}">
						                    	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
												<h:outputLabel id="actualEndDateLabel" styleClass="LABEL" value="#{msg['project.details.actualEndDate']}: "/>
						                   		<rich:calendar id="actualEndDate" value="#{pages$projectDetails.actualEndDate}"  locale="#{pages$projectDetails.locale}" popup="true" datePattern="#{pages$projectDetails.dateFormat}" disabled="#{pages$projectDetails.initialReceivingReadonlyMode || pages$projectDetails.actualEndDateReadOnly}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px"/>
						                    </t:panelGroup>
											<t:div style="height:10px;"/>
						   	</t:panelGrid>
						</t:div>
					
					<div class="AUC_DET_PAD">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
						<div class="TAB_PANEL_INNER"> 
							<rich:tabPanel binding="#{pages$projectDetails.tabPanel}" id="tabPanel" switchType="server" style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%" >
								<rich:tab id="projectDetailsTab" label="#{msg['projectDetailsInsShort']}" rendered="#{pages$projectDetails.showProjectDetailsTab}">
									<jsp:include page="SDTPS_projectTab.jsp"/>
								</rich:tab>
								<rich:tab id="projectMilestonesTab" label="#{msg['projectMilestoineInShort']}" rendered="#{pages$projectDetails.showProjectMilestonesTab}">
									<%@ include file="SDTPS_projectMilestonesTab.jsp"%>
								</rich:tab>
								<rich:tab id="projectFollowupHistoryTab" label="#{msg['projectFollowUpHistoryInShort']}" rendered="#{pages$projectDetails.showFollowUpActionPanel}">
									<%@ include file="SDTPS_projectFollowupDetailsTab.jsp"%>
								</rich:tab>
								<rich:tab id="projectPaymentTab" action="#{pages$projectPaymentsTab.showUpdatedPayment}" label="#{msg['projectPaymentsInShort']}" rendered="#{pages$projectDetails.showProjectPaymentTab}">
									<%@ include file="../pages/SDTPS_projectPaymentsTab.jsp"%>
								</rich:tab>
								
								<rich:tab label="#{msg['commons.history']}" action="#{pages$projectDetails.requestHistoryTabClick}" rendered="#{pages$projectDetails.showHistory}">
	                                <%@ include file="../pages/requestTasks.jsp"%>
								</rich:tab>	
								<rich:tab id="attachmentTab" label="#{msg['ProjectAttachmentInShort']}">
									 <%@ include file="attachment/attachment.jsp"%>
								</rich:tab>
								<rich:tab id="commentsTab" label="#{msg['ProjectCommentsInShort']}">
									<%@ include file="notes/notes.jsp"%>
								</rich:tab>	
							</rich:tabPanel>
													
							</div>
							<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT" /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID" style="width:100%;" /></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
						<t:div styleClass="BUTTON_TD" style="padding-top:10px;">
							<pims:security screen="Pims.ConstructionMgmt.ProjectDetails.SaveProject" action="create">
						 		<t:commandButton styleClass="BUTTON" value="#{msg['cancelContract.submit']}" onclick="if (!confirm('#{msg['project.details.messages.confirmProjectSubmit']}')) return false" action="#{pages$projectDetails.submitProject}" tabindex="13" rendered="#{pages$projectDetails.draftMode}"/>
						 		<t:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" action="#{pages$projectDetails.saveProject}" tabindex="13" rendered="#{pages$projectDetails.showSaveProject || pages$projectDetails.draftMode}"/>
							</pims:security>
							<pims:security screen="Pims.ConstructionMgmt.ProjectDetails.SaveLocationHandover" action="create">
						 		<t:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" onclick="if (!confirm('#{msg['project.details.messages.confirmProjectLocationSubmit']}')) return false" action="#{pages$projectDetails.saveHandOver}" tabindex="14" rendered="#{pages$projectDetails.showSaveHandOver}"/>
							</pims:security>
							<pims:security screen="Pims.ConstructionMgmt.ProjectDetails.SaveFollowUp" action="create">
						 		<t:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" onclick="if (!confirm('#{msg['project.details.messages.confirmProjectFollowUp']}')) return false" action="#{pages$projectDetails.saveFollowUp}" tabindex="15" rendered="#{pages$projectDetails.showSaveFollowUp}"/>
							</pims:security>
							<pims:security screen="Pims.ConstructionMgmt.ProjectDetails.CompleteFollowUp" action="create">
						 		<t:commandButton styleClass="BUTTON" value="#{msg['commons.completeButton']}" onclick="if (!confirm('#{msg['project.details.messages.confirmProjectFollowUpComplete']}')) return false" action="#{pages$projectDetails.completeFollowUp}" tabindex="16" rendered="#{pages$projectDetails.showCompleteFollowUp}"/>
							</pims:security>
							<pims:security screen="Pims.ConstructionMgmt.ProjectDetails.SaveInitialAcceptance" action="create">
						 		<t:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" onclick="if (!confirm('#{msg['project.details.messages.confirmInitialAcceptance']}')) return false" action="#{pages$projectDetails.saveInitialReceiving}" tabindex="16" rendered="#{pages$projectDetails.showSaveInitial}"/>
							</pims:security>
							<pims:security screen="Pims.ConstructionMgmt.ProjectDetails.SaveFinalAcceptance" action="create">
						 		<t:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" onclick="if (!confirm('#{msg['project.details.messages.confirmFinalAcceptance']}')) return false" action="#{pages$projectDetails.saveFinalReceiving}" tabindex="17" rendered="#{pages$projectDetails.showSaveFinal}"/>
							</pims:security>
							<pims:security screen="Pims.ConstructionMgmt.ProjectDetails.Cancel" action="create">
								<t:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}" onclick="if (!confirm('#{msg['commons.messages.cancelConfirm']}')) return false" action="#{pages$projectDetails.cancel}" tabindex="12" rendered="#{pages$projectDetails.showCancel && !pages$projectDetails.projectDetailsReadonlyMode}" > </t:commandButton>
							</pims:security>
							<pims:security screen="Pims.ConstructionMgmt.ProjectDetails.Cancel" action="create">
								<t:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}" onclick="if (!confirm('#{msg['commons.messages.backConfirm']}')) return false" action="#{pages$projectDetails.cancel}" tabindex="12" rendered="#{pages$projectDetails.showCancel && pages$projectDetails.projectDetailsReadonlyMode}" > </t:commandButton>
							</pims:security>
							 
						</t:div>
						</div>
								</div>
							</h:form>
					  </div> 
						</td>			
					</tr>
				</table>
			</td>
		</tr>
		  <c:choose>
			<c:when test="${!pages$projectDetails.popupViewMode}">
				<tr style="height:10px;width:100%;#height:10px;#width:100%;">
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</c:when>
		</c:choose>
	</table>
	</div>
</body>
</html>
</f:view>