
<t:div id="tabBVillaDetails" style="width:100%;">
	<t:panelGrid id="tabBVIllaDetailsab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">


		<h:outputLabel id="fileNoVillalbl" styleClass="LABEL"
			value="#{msg['searchInheritenceFile.fileNo']}:" />
		<h:inputText id="fileNoVilla" styleClass="READONLY" readonly="true"
			value="#{pages$tabFamilyVillageBeneficiaryVilla.pageView.inheritanceFileView.fileNumber}" />
		<h:panelGroup id="villnumpg">

			<h:outputLabel id="villaNumber" styleClass="LABEL"
				value="#{msg['familyVillagefile.lbl.villaNumber']}:"></h:outputLabel>
		</h:panelGroup >
		<h:inputText id="villaNumberVilla" maxlength="20" styleClass="READONLY" readonly="true"
			value="#{pages$tabFamilyVillageBeneficiaryVilla.pageView.inheritanceFileView.familyVillageVillaView.villaNumber}" />

		<h:outputLabel styleClass="LABEL"  id="joiningDateVilla" 
			value="#{msg['familyVillageManageBeneficiary.lbl.joiningDate']}:" />

		<rich:calendar id="DateOfJoining" value="#{pages$tabFamilyVillageBeneficiaryVilla.pageView.dateOfJoining}"
			popup="true" disabled="true"
			
			datePattern="#{pages$tabFamilyVillageBeneficiaryVilla.dateFormat}" showApplyButton="false"
			locale="#{pages$tabFamilyVillageBeneficiaryVilla.locale}" enableManualInput="false"
			inputStyle="width: 170px; height: 14px" />

		<h:panelGroup id="villacatepg">

			<h:outputLabel id="selectOneVillaCategoryll" styleClass="LABEL"
				value="#{msg['familyVillagefile.lbl.category']}:"></h:outputLabel>
		</h:panelGroup>
		<h:inputText  id="villaCategory"  styleClass="READONLY" readonly="true"
			value="#{pages$tabFamilyVillageBeneficiaryVilla.englishLocale? pages$tabFamilyVillageBeneficiaryVilla.pageView.inheritanceFileView.familyVillageVillaView.villaCategoryEn:
															   pages$tabFamilyVillageBeneficiaryVilla.pageView.inheritanceFileView.familyVillageVillaView.villaCategoryAr
			}" />


		<h:panelGroup id="addlblpg">

			<h:outputLabel id="addresslbl" styleClass="LABEL"
				value="#{msg['familyVillagefile.lbl.address']}:"></h:outputLabel>
		</h:panelGroup>
		<t:panelGroup id="addressvaluePg" colspan="3" style="width: 100%">
			<t:inputTextarea style="width: 90%;" id="txtAddress" styleClass="READONLY" readonly="true"
				value="#{pages$tabFamilyVillageBeneficiaryVilla.pageView.inheritanceFileView.familyVillageVillaView.villaAddress}"
				onblur="javaScript:validate();" onkeyup="javaScript:validate();"
				onmouseup="javaScript:validate();"
				onmousedown="javaScript:validate();"
				onchange="javaScript:validate();"
				onkeypress="javaScript:validate();" />
		</t:panelGroup>

	</t:panelGrid>


</t:div>


