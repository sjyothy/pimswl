<%-- 
  - Author: Syed Hammad Afridi
  - Date:
  - Copyright Notice:
  - @(#)\
  - Description: Used for Searching an Auction
  --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%-- 
		<% UIViewRoot viewRoot = (UIViewRoot)session.getAttribute("view");
           if (viewRoot.getLocale().toString().equals("en"))
           { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>
        <%
            } 
            else
            { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_ar.js"></script>
        <%
            } 
        %>
--%>
<script language="JavaScript" type="text/javascript">

	       function resetValues()
      		{
      		document.getElementById("searchFrm:txtPropertyNumber").value="";
			document.getElementById("searchFrm:commercialName").value="";
      	    document.getElementById("searchFrm:country").selectedIndex=0;
		    document.getElementById("searchFrm:state").selectedIndex=0;
      	   	document.getElementById("searchFrm:propertyType").selectedIndex=0;
      	    document.getElementById("searchFrm:propertyStatus").selectedIndex=0;
      	    document.getElementById("searchFrm:ownerShip").selectedIndex=0;
        	document.getElementById("searchFrm:propertyUsage").selectedIndex=0;
      	    document.getElementById("searchFrm:unitRefNo").value="";
      	    document.getElementById("searchFrm:costCenter").value="";
      	  }
	    
	    
  function submitForm()
  		   {
           document.getElementById('searchFrm').submit();
		   }
		   
		   function GenerateFloorsPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 220;
	   var screen_left = 170;
	   window.open('GenerateFloors.jsf','_blank','width='+(screen_width-300)+',height='+(screen_height-100)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=no,status=yes');
	}
		
	function GenerateUnitsPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 220;
	   var screen_left = 170;
	   window.open('GenerateUnits.jsf','_blank','width='+(screen_width-300)+',height='+(screen_height-100)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=no,status=yes');
	}
	
	function sendToParent(propertyId) {
	    window.opener.populateProperty(propertyId);
	    window.close();
	}
	
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />


			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />

			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

		</head>

		<body class="BODY_STYLE">
			<div class="containerDiv">
				<%
					response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
						response.setHeader("Pragma", "no-cache"); //HTTP 1.0
						response.setDateHeader("Expires", 0); //prevents caching at the proxy server
				%>
				<div style="width: 1024px;">
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<c:choose>
								<c:when test="${!pages$propertyList.isViewModePopUp}">
									<td colspan="2">
										<jsp:include page="header.jsp" />
									</td>
								</c:when>
							</c:choose>

						</tr>
						<tr>
							<c:choose>
								<c:when test="${!pages$propertyList.isViewModePopUp}">
									<td class="divLeftMenuWithBody" width="17%">
										<jsp:include page="leftmenu.jsp" />
									</td>
								</c:when>
							</c:choose>
							<td width="83%" valign="top" class="divBackgroundBody">

								<table width="99%" class="greyPanelTable" cellpadding="0"
									cellspacing="0" border="0">
									<tr>
										<td class="HEADER_TD">
											<h:outputLabel value="#{msg['property.search']}"
												styleClass="HEADER_FONT" />
										</td>
										<td width="100%">
											&nbsp;
										</td>
									</tr>
								</table>

								<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
									cellspacing="0" border="0">
									<tr valign="top">
										<td height="100%" valign="top" nowrap="nowrap"
											background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
											width="1">
										</td>
										<td width="0%" height="99%" valign="top" nowrap="nowrap">

											<h:form id="searchFrm" style="WIDTH: 98%;">
												<div style="height: 440px; overflow-y: scroll;">

													<div class="MARGIN" style="width: 95%">

														<div class="DETAIL_SECTION" style="width: 99.8%;">
															<h:outputLabel value="#{msg['commons.searchCriteria']}"
																styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
															<table cellpadding="1px" cellspacing="2px"
																class="DETAIL_SECTION_INNER" border="0">
																<tr>
																	<td width="97%">
																		<table width="100%">
																			<tr>
																				<td width="25%">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['inspection.propertyRefNum']} :"></h:outputLabel>
																				</td>

																				<td width="25%">


																					<h:inputText id="txtPropertyNumber"
																						value="#{pages$propertyList.propertyBean.propertyNumber}"
																						maxlength="20"></h:inputText>

																				</td>
																				<td width="25%">

																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['contract.property.Name']} :"></h:outputLabel>
																				</td>

																				<td width="25%">
																					<h:inputText id="commercialName"
																						value="#{pages$propertyList.propertyBean.commercialName}"
																						maxlength="250"></h:inputText>
																				</td>

																			</tr>
																			<tr>

																				<td colspan="1" width="25%">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['property.endowedName']} :"></h:outputLabel>
																				</td>
																				<td colspan="1" width="25%">
																					<h:inputText id="endowedName"
																						value="#{pages$propertyList.propertyBean.endowedName}"
																						maxlength="250"></h:inputText>
																				</td>
																				<td colspan="1">

																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['property.type']} :"></h:outputLabel>
																				</td>

																				<td colspan="1">
																					<h:selectOneMenu id="propertyType" required="false"
																						value="#{pages$propertyList.typeId}">

																						<f:selectItem itemValue="0"
																							itemLabel="#{msg['commons.All']}" />
																						<f:selectItems
																							value="#{pages$ApplicationBean.propertyTypeList}" />
																					</h:selectOneMenu>

																				</td>
																			</tr>
																			<tr>
																				<td colspan="1">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['contact.country']} :"></h:outputLabel>
																				</td>
																				<td colspan="1" width="10%">
																					<h:selectOneMenu id="country"
																						valueChangeListener="#{pages$propertyList.loadState}"
																						onchange="submitForm();" required="false"
																						immediate="false"
																						value="#{pages$propertyList.selectedCountryId}">
																						<f:selectItem itemValue="0"
																							itemLabel="#{msg['commons.All']}" />
																						<f:selectItems
																							value="#{view.attributes['country']}" />
																					</h:selectOneMenu>

																				</td>

																				<td colspan="1">

																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['contact.state']} :" />
																				</td>
																				<td colspan="1" width="10%">
																					<h:selectOneMenu id="state" required="false"
																						valueChangeListener="#{pages$propertyList.loadCity}"
																						onchange="submitForm();" immediate="false"
																						value="#{pages$propertyList.stateId}">
																						<f:selectItem itemValue="0"
																							itemLabel="#{msg['commons.All']}" />
																						<f:selectItems value="#{view.attributes['state']}" />
																					</h:selectOneMenu>

																				</td>

																			</tr>

																			<tr>

																				<td colspan="1">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['commons.area']} :"></h:outputLabel>
																				</td>
																				<td colspan="1">
																					<h:selectOneMenu id="areaCombo" required="false"
																						value="#{pages$propertyList.cityId}">

																						<f:selectItem itemValue="0"
																							itemLabel="#{msg['commons.All']}" />
																						<f:selectItems value="#{view.attributes['city']}" />
																					</h:selectOneMenu>
																				</td>

																				<td colspan="1">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['contact.street']} :" />
																				</td>
																				<td colspan="1">
																					<h:inputText id="streetInput"></h:inputText>
																				</td>

																			</tr>


																			<tr>

																				<td colspan="1">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['commons.status']} :"></h:outputLabel>
																				</td>
																				<td colspan="1">
																					<h:selectOneMenu id="propertyStatus"
																						required="false"
																						value="#{pages$propertyList.statusId}">
																						<f:selectItem itemValue="0"
																							itemLabel="#{msg['commons.All']}" />
																						<f:selectItems
																							value="#{pages$ApplicationBean.propertyStatusList}" />
																					</h:selectOneMenu>


																				</td>


																				<td colspan="1">

																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['contract.unitRefNo']} :" />
																				</td>
																				<td colspan="1">
																					<h:inputText id="unitRefNo"
																						value="#{pages$propertyList.propertyBean.unitRefNo}"></h:inputText>
																				</td>

																			</tr>
																			<tr>
																				<td colspan="1">

																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['property.usage']} :" />
																				</td>
																				<td colspan="1" width="10%">
																					<h:selectOneMenu id="propertyUsage"
																						required="false"
																						value="#{pages$propertyList.usageId}">
																						<f:selectItem itemValue="0"
																							itemLabel="#{msg['commons.All']}" />
																						<f:selectItems
																							value="#{pages$ApplicationBean.propertyUsageList}" />
																					</h:selectOneMenu>
																				</td>

																				<td colspan="1">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['property.investmentPurpose']} :" />
																				</td>
																				<td colspan="1" width="10%">
																					<h:selectOneMenu id="propertyPurpose"
																						required="false">
																						<f:selectItem itemValue="0"
																							itemLabel="#{msg['commons.All']}" />
																						<f:selectItems
																							value="#{pages$ApplicationBean.investmentPurposeList}" />
																					</h:selectOneMenu>
																				</td>

																			</tr>
																			<tr>
																				<td colspan="1">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['receiveProperty.ownershipType']} :"></h:outputLabel>
																				</td>

																				<td colspan="1">
																					<h:selectOneMenu id="ownerShip" required="false"
																						binding="#{pages$propertyList.cmbOwnerShipType}"
																						value="#{pages$propertyList.ownerShipTypeId}">

																						<f:selectItem itemValue="0"
																							itemLabel="#{msg['commons.All']}" />
																						<f:selectItems
																							value="#{pages$ApplicationBean.propertyOwnershipType}" />
																					</h:selectOneMenu>

																				</td>

																				<td colspan="1">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['commons.report.costCenter']} :"></h:outputLabel>
																				</td>
																				<td colspan="1">
																					<h:inputText id="costCenter"
																						value="#{pages$propertyList.propertyBean.costCenter}"></h:inputText>
																				</td>
																			</tr>
																			<tr>
																			<td colspan="1">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['botContractDetails.LandNumber']} :"></h:outputLabel>
																			</td>
																			<td colspan="1">
																				<h:inputText id="landNumber"
																					value="#{pages$propertyList.propertyBean.landNumber}"></h:inputText>
																			</td>
																			</tr>

																			<tr>
																				<td class="BUTTON_TD" colspan="4">

																					<h:commandButton styleClass="BUTTON"
																						id="submitButton"
																						action="#{pages$propertyList.searchProperty}"
																						immediate="false" value="#{msg['commons.search']}"
																						style="width: 75px" tabindex="7"></h:commandButton>
																					<h:commandButton styleClass="BUTTON"
																						value="#{msg['commons.clear']}"
																						onclick="javascript:resetValues();"
																						style="width: 75px" tabindex="8"></h:commandButton>
																					<h:commandButton
																						rendered="#{!pages$propertyList.isViewModePopUp}"
																						styleClass="BUTTON" id="addPropertyButton"
																						action="#{pages$propertyList.addProperty}"
																						immediate="false"
																						value="#{msg['commons.addProperty']}"
																						style="width: 85px" tabindex="9"></h:commandButton>

																					<h:commandButton type="submit"
																						rendered="#{pages$propertyList.isPageModeSelectManyPopUp && pages$propertyList.recordSize>0}"
																						styleClass="BUTTON"
																						action="#{pages$propertyList.sendManyPropertyToParent}"
																						value="#{msg['commons.select']}" />
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td width="3%">
																		&nbsp;
																	</td>
																</tr>
															</table>

														</div>
													</div>

													<div class="MARGIN" style="width: 93.8%; # width: 95.5%;">

														<div class="imag">
															&nbsp;
														</div>
														<div class="contentDiv" align="center"
															style="width: 98%; # width: 99%;">
															<t:dataTable id="dt1"
																value="#{pages$propertyList.propertyViewList}"
																binding="#{pages$propertyList.dataTable}"
																rows="#{pages$propertyList.paginatorRows}"
																var="dataItem" rowClasses="row1,row2" rules="all"
																renderedIfEmpty="true" width="100%">
																<t:column id="propertyNoCol" defaultSorted="true"
																	style="width:90;">
																	<f:facet name="header">
																		<t:commandSortHeader columnName="propertyNoCol"
																			actionListener="#{pages$propertyList.sort}"
																			value="#{msg['inspection.propertyRefNum']}"
																			arrow="true">
																			<f:attribute name="sortField" value="propertyNumber" />
																		</t:commandSortHeader>
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		style="white-space: normal;"
																		value="#{dataItem.propertyNumber}" />
																</t:column>
																<t:column id="propertyNameCol" style="width:120;">
																	<f:facet name="header">
																		<t:commandSortHeader columnName="propertyNameCol"
																			actionListener="#{pages$propertyList.sort}"
																			value="#{msg['contract.property.Name']}" arrow="true">
																			<f:attribute name="sortField" value="commercialName" />
																		</t:commandSortHeader>
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		style="white-space: normal;"
																		value="#{dataItem.commercialName}" />
																</t:column>
																<t:column id="propertyNameEnCol" style="width:120;">
																	<f:facet name="header">
																		<t:commandSortHeader columnName="propertyNameCol"
																			actionListener="#{pages$propertyList.sort}"
																			value="#{msg['receiveProperty.lbl.nameEn']}"
																			arrow="true">
																			<f:attribute name="sortField" value="propertyNameEn" />
																		</t:commandSortHeader>
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		style="white-space: normal;"
																		value="#{dataItem.propertyNameEn}" />
																</t:column>


																<t:column id="propertyOwnerShipCol" style="width:90;">
																	<f:facet name="header">
																		<t:commandSortHeader columnName="propertyOwnerShipCol"
																			actionListener="#{pages$propertyList.sort}"
																			value="#{msg['receiveProperty.ownershipType']}"
																			arrow="true">
																			<f:attribute name="sortField" value="categoryId" />
																		</t:commandSortHeader>
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		style="white-space: normal;"
																		value="#{pages$propertyList.isEnglishLocale?dataItem.categoryTypeEn:dataItem.categoryTypeAr}" />
																</t:column>
																<t:column id="propertyTypeCol" width="100">
																	<f:facet name="header">
																		<t:commandSortHeader columnName="propertyTypeCol"
																			actionListener="#{pages$propertyList.sort}"
																			value="#{msg['property.type']}" arrow="true">
																			<f:attribute name="sortField" value="typeId" />
																		</t:commandSortHeader>

																	</f:facet>
																	<t:outputText
																		rendered="#{pages$propertyList.isEnglishLocale}"
																		style="white-space: normal;" styleClass="A_LEFT"
																		value="#{dataItem.propertyTypeEn}" />
																	<t:outputText
																		rendered="#{pages$propertyList.isArabicLocale}"
																		style="white-space: normal;" styleClass="A_LEFT"
																		value="#{dataItem.propertyTypeAr}" />
																</t:column>
																<t:column id="usageTypeCol" width="100">
																	<f:facet name="header">
																		<t:commandSortHeader columnName="usageTypeCol"
																			actionListener="#{pages$propertyList.sort}"
																			value="#{msg['property.usage']}" arrow="true">
																			<f:attribute name="sortField" value="usageTypeId" />
																		</t:commandSortHeader>

																	</f:facet>
																	<t:outputText
																		rendered="#{pages$propertyList.isEnglishLocale}"
																		style="white-space: normal;" styleClass="A_LEFT"
																		value="#{dataItem.usageTypeEn}" />
																	<t:outputText
																		rendered="#{pages$propertyList.isArabicLocale}"
																		style="white-space: normal;" styleClass="A_LEFT"
																		value="#{dataItem.usageTypeAr}" />

																</t:column>
																<t:column id="accountNumberCOle" style="width:90;">
																	<f:facet name="header">
																		<t:commandSortHeader columnName="accountNumber"
																			actionListener="#{pages$propertyList.sort}"
																			value="#{msg['settlement.label.costCenter']}"
																			arrow="true">
																			<f:attribute name="sortField" value="accountNumber" />
																		</t:commandSortHeader>
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		style="white-space: normal;"
																		value="#{dataItem.costCenter}" />
																</t:column>
																<t:column id="statusCol" width="110">
																	<f:facet name="header">
																		<t:commandSortHeader columnName="statusCol"
																			actionListener="#{pages$propertyList.sort}"
																			value="#{msg['commons.status']}" arrow="true">
																			<f:attribute name="sortField" value="statusId" />
																		</t:commandSortHeader>

																	</f:facet>
																	<t:outputText
																		rendered="#{pages$propertyList.isEnglishLocale}"
																		style="white-space: normal;" styleClass="A_LEFT"
																		value="#{dataItem.statusTypeEn}" />
																	<t:outputText
																		rendered="#{pages$propertyList.isArabicLocale}"
																		style="white-space: normal;" styleClass="A_LEFT"
																		value="#{dataItem.statusTypeAr}" />


																</t:column>
																<t:column id="col8" sortable="false" width="10%"
																	rendered="#{pages$propertyList.isPageModeSelectOnePopUp}">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.select']}" />
																	</f:facet>
																	<t:commandLink
																		onclick="javascript:sendToParent('#{dataItem.propertyId}');">
																		<h:graphicImage style="width: 16;height: 16;border: 0"
																			alt="#{msg['commons.select']}"
																			url="../resources/images/select-icon.gif" />
																	</t:commandLink>
																</t:column>
																<t:column id="col9" width="10%" sortable="false"
																	rendered="#{pages$propertyList.isPageModeSelectManyPopUp}">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.select']}" />
																	</f:facet>
																	<h:selectBooleanCheckbox id="select"
																		value="#{dataItem.selected}" />
																</t:column>
																<t:column id="actionCol" width="80"
																	rendered="#{!pages$propertyList.isPageModeSelectManyPopUp && !pages$propertyList.isPageModeSelectOnePopUp}">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.action']}" />
																	</f:facet>

																	<h:commandLink action="#{pages$propertyList.edit}">
																		<h:graphicImage style="margin-left:6px;"
																			title="#{msg['commons.edit']}"
																			url="../resources/images/edit-icon.gif" />
																	</h:commandLink>
																	<h:commandLink
																		action="#{pages$propertyList.openPropertyDetailsToAddRentAmount}">
																		<h:graphicImage style="margin-left:6px;"
																			title="#{msg['receiveProperty.changeRentAmount']}"
																			url="../resources/images/app_icons/Add-fee.png" />
																	</h:commandLink>
																</t:column>

															</t:dataTable>
														</div>
														<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
															style="width:100%;#width:100%;">
															<table cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<td class="RECORD_NUM_TD">
																		<div class="RECORD_NUM_BG">
																			<table cellpadding="0" cellspacing="0"
																				style="width: 182px; # width: 150px;">
																				<tr>
																					<td class="RECORD_NUM_TD">
																						<h:outputText
																							value="#{msg['commons.recordsFound']}" />
																					</td>
																					<td class="RECORD_NUM_TD">
																						<h:outputText value=" : " />
																					</td>
																					<td class="RECORD_NUM_TD">
																						<h:outputText
																							value="#{pages$propertyList.recordSize}" />
																					</td>
																				</tr>
																			</table>
																		</div>
																	</td>
																	<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																		align="right">

																		<t:div styleClass="PAGE_NUM_BG" style="#width:20%">
																			<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{pages$propertyList.currentPage}" />
																					</td>
																				</tr>
																			</table>

																		</t:div>

																		<TABLE border="0" class="SCH_SCROLLER">
																			<tr>
																				<td>
																					<t:commandLink
																						action="#{pages$propertyList.pageFirst}"
																						disabled="#{pages$propertyList.firstRow == 0}">
																						<t:graphicImage url="../#{path.scroller_first}"
																							id="lblF"></t:graphicImage>
																					</t:commandLink>
																				</td>
																				<td>
																					<t:commandLink
																						action="#{pages$propertyList.pagePrevious}"
																						disabled="#{pages$propertyList.firstRow == 0}">
																						<t:graphicImage
																							url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																					</t:commandLink>
																				</td>
																				<td>
																					<t:dataList value="#{pages$propertyList.pages}"
																						var="page">
																						<h:commandLink value="#{page}"
																							actionListener="#{pages$propertyList.page}"
																							rendered="#{page != pages$propertyList.currentPage}" />
																						<h:outputText value="<b>#{page}</b>"
																							escape="false"
																							rendered="#{page == pages$propertyList.currentPage}" />
																					</t:dataList>
																				</td>
																				<td>
																					<t:commandLink
																						action="#{pages$propertyList.pageNext}"
																						disabled="#{pages$propertyList.firstRow + pages$propertyList.rowsPerPage >= pages$propertyList.totalRows}">
																						<t:graphicImage
																							url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																					</t:commandLink>
																				</td>
																				<td>
																					<t:commandLink
																						action="#{pages$propertyList.pageLast}"
																						disabled="#{pages$propertyList.firstRow + pages$propertyList.rowsPerPage >= pages$propertyList.totalRows}">
																						<t:graphicImage url="../#{path.scroller_last}"
																							id="lblL"></t:graphicImage>
																					</t:commandLink>
																				</td>
																			</tr>
																		</TABLE>





																	</td>
																</tr>
															</table>
														</t:div>

													</div>
												</div>
											</h:form>

										</td>
									</tr>
								</table>

							</td>
						</tr>
						<c:choose>
							<c:when test="${!pages$propertyList.isViewModePopUp}">
								<tr>
									<td colspan="2">
										<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td class="footer">
													<h:outputLabel value="#{msg['commons.footer.message']}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</c:when>
						</c:choose>
					</table>
				</div>
			</div>
		</body>
	</html>
</f:view>
