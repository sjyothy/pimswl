<%-- 
  - Author:Muhammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for distributing revenues amongst beneficiaries.
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>

<script language="javaScript" type="text/javascript">
	function calculateFirstPayment(control )
	{
	  
	  
	  
	  removeNonNumeric(control);
	  var cntrlFirstpayment = document.getElementById("frm:oldAmount" );
	  var cntrlOriginalpayment = document.getElementById("frm:originalAmount" );
	  
	  var orignalAmt = parseFloat( cntrlOriginalpayment.value ) ;
	  var secondPaymentAmt = parseFloat( control.value );
	  
	  if(secondPaymentAmt==NaN || orignalAmt  <= secondPaymentAmt )
	  {
	   secondPaymentAmt = 0; 
	   control.innerText = secondPaymentAmt ;
	   
	  }
	  var newFirstPaymentAmt= parseFloat( orignalAmt - secondPaymentAmt); 
	  if ( parseFloat( newFirstPaymentAmt+secondPaymentAmt ) == orignalAmt)
	  {
	  		cntrlFirstpayment.innerText = newFirstPaymentAmt;
	  }
	  
	  
	}
	function closeWindowSubmit()
	{
		window.opener.onMessageFromSplitPopup(); 
	  	window.close();
	  	
	}	
	function closeWindow()
	{
		  window.close();
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">


		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>

		<body class="BODY_STYLE" onload="document.getElementById("frm:newAmount").focus();">

			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel
										value="#{msg['collectionProcedure.lbl.SplitPayment']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="frm" enctype="multipart/form-data">
										<div>
											<table border="0" class="layoutTable">
												<tr>
													<td>
														<h:outputText
															value="#{pages$SplitCollectionPayments.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />
														<h:outputText
															value="#{pages$SplitCollectionPayments.successMessages}"
															escape="false" styleClass="INFO_FONT" />
														<h:inputHidden id="originalAmount"
															value="#{pages$SplitCollectionPayments.originalAmount}" />

													</td>
												</tr>
											</table>
										</div>

										<div class="SCROLLABLE_SECTION">
											<div class="MARGIN" style="width: 95%;">

												<div style="width: 99.8%;font-weight:bold;">

													<table cellpadding="1px" cellspacing="2px" border="0"
														>
														<table width="100%">

															<tr>

																<td>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['collectionProcedure.lbl.FirstAmount']}"></h:outputLabel>

																</td>
																<td>

																	<h:inputText id="oldAmount" styleClass="READONLY"
																		value="#{pages$SplitCollectionPayments.openFileDistribute.originalAmount}"
																		onkeyup="removeNonNumeric(this)"
																		onkeypress="removeNonNumeric(this)"
																		onkeydown="removeNonNumeric(this)"
																		onblur="removeNonNumeric(this)"
																		onchange="removeNonNumeric(this)" />


																</td>

															</tr>
															<tr>

																<td>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['collectionProcedure.lbl.SecondAmount']}"></h:outputLabel>

																</td>
																<td>
																	<h:inputText id="newAmount"
																		value="#{pages$SplitCollectionPayments.openFileDistribute.amount}"
																		onkeyup="calculateFirstPayment(this);"
																		onkeypress="calculateFirstPayment(this);"
																		onkeydown="calculateFirstPayment(this);"
																		onblur="calculateFirstPayment(this);"
																		onchange="calculateFirstPayment(this);" />


																</td>

															</tr>

														</table>
														<br />
															<t:div styleClass="BUTTON_TD">
															<h:commandButton styleClass="BUTTON"
																value="#{msg['collectionProcedure.lbl.SplitPayment']}"
																action="#{pages$SplitCollectionPayments.onSplit}"
																style="width: auto">
															</h:commandButton>

															<h:commandButton styleClass="BUTTON"
																value="#{msg['generateFloors.Close']}"
																onclick="javascript:window.close();" style="width: auto">
															</h:commandButton>
														</t:div>
														
													</table>


												</div>

											</div>



										</div>


										</div>
									</h:form>

								</td>
							</tr>


						</table>



					</td>
				</tr>
			</table>
			</td>
			</tr>
			</table>



		</body>
	</html>
</f:view>