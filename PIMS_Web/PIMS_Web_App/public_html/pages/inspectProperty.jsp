<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="Cache-Control"
			content="no-cache,must-revalidate,no-store">
		<meta http-equiv="expires" content="0">


		<link rel="stylesheet" type="text/css"
			href="../resources/css/simple_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../resources/css/amaf_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../resources/css/table.css" />
	
		<title>PIMS</title>
	</head>
	<body>

		<f:view>
			<body>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr>
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td
										background="../resources/images/Grey panel/Grey-Panel-left-1.jpg"
										height="38" style="background-repeat: no-repeat;" width="100%">
										<font
											style="font-family: Trebuchet MS; font-weight: regular; font-size: 19px; margin-left: 15px; top: 30px;">Property
											Inspection</font>
										<!--<IMG src="../resources/images/Grey panel/Grey-Panel-left-1.jpg"/>-->
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<td width="500px" height="100%" valign="top" nowrap="nowrap">

										<h:form enctype="multipart/form-data">
											<table style="width: 100%">
												<tr>
													<td width="25%">
														<h:outputText value="Property Reference Number :"></h:outputText>
													</td>
													<td width="25%">
														<h:inputText readonly="true"
															binding="#{pages$inspectProperty.propertyReferenceNumber}"></h:inputText>
													</td>
													<td width="25%">
														<h:outputText value="Financial Account Number :"></h:outputText>
													</td>
													<td width="25%">
														<h:inputText readonly="true"
															binding="#{pages$inspectProperty.financialAccofuntNumber}" />
													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Type :"></h:outputText>
													</td>
													<td>
														<h:inputText readonly="true"
															binding="#{pages$inspectProperty.propertyType}">


														</h:inputText>
													</td>
													<td>
														<h:outputText value="Department Code :"></h:outputText>

													</td>
													<td>
														<h:inputText readonly="true"
															binding="#{pages$inspectProperty.departmentCode}"></h:inputText>

													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Category :"></h:outputText>
													</td>
													<td>
														<h:inputText readonly="true"
															binding="#{pages$inspectProperty.propertyCategory}">
														</h:inputText>
													</td>
													<td>

														<h:outputText value="Project Number :"></h:outputText>
													</td>
													<td>
														<h:inputText readonly="true"
															binding="#{pages$inspectProperty.projectNumber}"></h:inputText>


													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Usage :"></h:outputText>
													</td>
													<td>	<h:inputText readonly="true"
															binding="#{pages$inspectProperty.propertyUsageList}"></h:inputText>
													
																											</td>
													<td>
														<h:outputText value="Land Area :"></h:outputText>

													</td>
													<td>

														<h:inputText readonly="true"
															binding="#{pages$inspectProperty.landArea}"></h:inputText>
													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Status :"></h:outputText>
													</td>
													<td>
														<h:inputText readonly="true"
															binding="#{pages$inspectProperty.propertyStatus}">
														</h:inputText>
													</td>
													<td>

														<h:outputText value="Built in Area :"></h:outputText>
													</td>
													<td>
														<h:inputText readonly="true"
															binding="#{pages$inspectProperty.builtInArea}"></h:inputText>

													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Investment Purpose :"></h:outputText>
													</td>
													<td>
														<h:inputText readonly="true"
															binding="#{pages$inspectProperty.investmentPurpose}">
														</h:inputText>
													</td>
													<td>

														<h:outputText value="Number of Units :"></h:outputText>
													</td>
													<td>
														<h:inputText readonly="true"
															binding="#{pages$inspectProperty.numberOfUnits}"></h:inputText>

													</td>
												</tr>
												<tr>
													<td>
														<h:outputText value="Coordinates :"></h:outputText>

													</td>
													<td>


														<h:inputText readonly="true"
															binding="#{pages$inspectProperty.coOrdinates}"></h:inputText>
													</td>
													<td>
														<h:outputText value="Location Chart :"></h:outputText>
													</td>
													<td>
														<h:inputText readonly="true"
															binding="#{pages$inspectProperty.locationChart}"></h:inputText>
													</td>

												</tr>
												<tr>
													<td>
														<h:outputText value="Number of Floors :"></h:outputText>
													</td>
													<td>
														<h:inputText readonly="true"
															binding="#{pages$inspectProperty.numberOfFloors}"></h:inputText>
													</td>
													<td>

														<h:outputText value="File"></h:outputText>
													</td>
													<td>
														<t:inputFileUpload id="fileupload" accept="image/*"
															value="#{pages$inspectProperty._upFile}" storage="file"
															styleClass="fileUploadInput" required="true"
															maxlength="200000" />
														<h:commandButton value="Upload" styleClass="BUTTON"
															action="#{pages$inspectProperty.upload}">
														</h:commandButton>

													</td>
												</tr>
												<tr>
													<td>

														<h:outputText value="Inspection Comments:"></h:outputText>
													</td>
													<td>
														<h:inputTextarea
															binding="#{pages$inspectProperty.inspectionComments}"
															 style="width: 206px; height: 44px"></h:inputTextarea>
													<td>

													Rent Value</td>

													<td><h:inputText binding="#{pages$inspectProperty.propertyRentValue}"></h:inputText></td>
													<td>

													</td>
													<td>

													</td>
												</tr>


												<tr>
													<td></td>
													<td></td>
													<td></td>
													<td>
														<h:commandButton value="Inspect"
															action="#{pages$inspectProperty.inspected}"
															immediate="true" type="submit" styleClass="BUTTON"></h:commandButton>
															<h:commandButton value="Save"
															action="#{pages$inspectProperty.save}"
															immediate="true" type="submit" styleClass="BUTTON"></h:commandButton>
														<h:commandButton value="Cancel" styleClass="BUTTON"></h:commandButton>
													</td>
												</tr>
												<tr>
													<td colspan="4">










														
													</td>
												</tr>
											</table>
											
											<h:panelGrid styleClass="tabbedPane"
															columnClasses="displayPanel">
															<%-- Tabs --%>

															<f:facet name="header">
																<h:panelGrid  columns="3" styleClass="tabbedPaneHeader">

																	<h:commandLink tabindex="1" title="FLOORS" value="FLOOR"
																		styleClass="#{pages$inspectProperty.floorsStyle}"
																		type="submit" immediate="true"  action="#{pages$inspectProperty.floorsAct}">


																	</h:commandLink>
																	<h:commandLink tabindex="2" title="UNITS" value="Units"
																		styleClass="#{pages$inspectProperty.unitsStyle}"
																		type="submit" immediate="true" action="#{pages$inspectProperty.unitsAct}" >
																		


																	</h:commandLink>



																</h:panelGrid>
															</f:facet>

															<%-- Tabbed pane content --%>
														
																<%@ include file="floorTab.jsp"%>


																<%@ include file="unitsTab.jsp"%>
														
														</h:panelGrid>
										</h:form>

									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</body>
		</f:view>
</html>
