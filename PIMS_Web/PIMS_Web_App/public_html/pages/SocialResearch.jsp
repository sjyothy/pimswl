<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
		function disableButtons(control)
		{
			disableInputs();
		
			control.nextSibling.nextSibling.onclick();
			return 
		
		}
		function disableInputs()
		{
			
			var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) 
			{
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
		}
		
		function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
        
		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		function populatePaymentDetails()
		{
		  disableInputs();
		  document.getElementById("formRequest:testing").onclick();
		  
		}
		function updateRecommendation()
		{
		  disableInputs();
		  document.getElementById("formRequest:updateRecommendation").onclick();
		  
		}
		function   openFollowupPopup()
		{
		   var screen_width = 900;
		   var screen_height = 470;
		   var screen_top = screen.top;
		     window.open('memsFollowup.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes,resizeable=yes');
		    
		}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />


	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />


		</head>

		<script language="javascript" type="text/javascript">
	function validate() {
      maxlength=500;
       
       
       if(document.getElementById("formRequest:txt_remarks").value.length>=maxlength) {
          document.getElementById("formRequest:txt_remarks").value=
                                        document.getElementById("formRequest:txt_remarks").value.substr(0,maxlength);  
       
      }
    }
            </script>


		<!-- Header -->

		<body class="BODY_STYLE">
			<div class="containerDiv">

				<table width="99%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>

					</tr>
					<tr>

						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="83%" height="84%" valign="top"
							class="divBackgroundBody">
							<h:form id="formRequest" style="width:99.7%;height:95%"
								enctype="multipart/form-data">
								<h:commandLink id="testing"
									action="#{pages$SocialResearch.obtainChildParams}">
								</h:commandLink>
								<h:commandLink id="updateRecommendation"
									action="#{pages$SocialResearch.updateRecommendation}">
								</h:commandLink>


								<table class="greyPanelTable" cellpadding="0" cellspacing="0"
									border="0">
									<tr>
										<td class="HEADER_TD" style="width: 70%;">
											<h:outputLabel value="Social Researh"
												styleClass="HEADER_FONT" />
											<h:messages></h:messages>
										</td>
										<td>
											&nbsp;
										</td>


									</tr>
								</table>
								<table height="95%" width="99%" class="greyPanelMiddleTable"
									cellpadding="0" cellspacing="0" border="0">
									<tr valign="top">
										<td height="100%" valign="top" nowrap="nowrap"
											background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
											width="1">
										</td>
										<td height="100%" valign="top" nowrap="nowrap">
											<div class="SCROLLABLE_SECTION">
												<t:panelGrid id="hiddenFields" border="0"
													styleClass="layoutTable" width="94%"
													style="margin-left:15px;margin-right:15px;" columns="1">
													<h:outputText id="successmsg" escape="false"
														styleClass="INFO_FONT"
														value="#{pages$SocialResearch.successMessages}" />
													<h:outputText id="errormsg" escape="false"
														styleClass="ERROR_FONT"
														value="#{pages$SocialResearch.errorMessages}" />
												</t:panelGrid>
												<div class="MARGIN" style="width: 94%; margin-bottom: 0px;">
													<t:div styleClass="BUTTON_TD">
														<h:commandButton id="btnViewFile" type="submit"
															styleClass="BUTTON"
															action="#{pages$SocialResearch.onOpenFile}"
															value="#{msg['socialResearch.btn.file']}"
															style="width:10%;" />
													</t:div>

													<t:panelGrid id="unitInfoTable" cellpadding="1px"
														width="100%" cellspacing="3px"
														styleClass="TAB_DETAIL_SECTION_INNER" columns="4"
														columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">

														<h:outputLabel styleClass="LABEL"
															value="#{msg['socialResearch.ResearchNum']}:" />
														<h:inputText maxlength="20" styleClass="READONLY"
															id="txtNumber" readonly="true"
															value="#{pages$SocialResearch.researchView.researchNumber}"
															style="width:78%;"></h:inputText>
														<h:outputLabel styleClass="LABEL"
															value="#{msg['socialResearch.FileNum']}:"></h:outputLabel>
														<h:inputText id="txtResearchType" styleClass="READONLY"
															style="width:78%;" readonly="true" maxlength="20"
															value="#{pages$SocialResearch.researchView.researchTypeEn}" />


													</t:panelGrid>
													<br></br>






													<rich:tabPanel id="tabPanel" style="width:100%;"
														headerSpacing="0">


														<rich:tab id="tabRecDetails"
															label="#{msg['socialResearch.tab.RecommendationDetails']}"
															title="#{msg['socialResearch.tab.RecommendationDetails']}">
															<t:div>
																<t:panelGrid columns="3"
																	binding="#{pages$SocialResearch.tbl_Action}"
																	columnClasses="COLUMN1,COLUMN2,COLUMN1" width="100%">

																	<h:outputLabel styleClass="LABEL" style="width: 20%;"
																		value="#{msg['PeriodicDisbursements.disbursementSource']}:" />

																	<h:selectOneMenu id="selectDisbursementSrc"
																		styleClass="SELECT_MENU"
																		value="#{pages$SocialResearch.disburseSrcId}">
																		<f:selectItem itemValue="-1"
																			itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.disbursementSourceList}" />

																	</h:selectOneMenu>
																	<h:commandButton id="btnAddDisburseSource"
																		style="width:auto;" type="submit" styleClass="BUTTON"
																		onclick="if (!confirm('#{msg['confirmMsg.doUWantToAddDisSrc']}')) return false; else disableButtons(this);"
																		binding="#{pages$SocialResearch.btnAddDisburseSrc}"
																		value="#{msg['socialResearch.btn.addDisburseSrc']}" />
																	 	<h:commandLink id="lnkAddDisburseSrc" action="#{pages$SocialResearch.onAddDisbursementSource}" />
																</t:panelGrid>
															</t:div>
															<t:div></t:div>
															<t:div id="recommendationDataTableDiv"
																styleClass="contentDiv" style="width:100%">
																<t:dataTable id="dataTable"
																	rows="#{pages$SocialResearch.paginatorRows}"
																	value="#{pages$SocialResearch.recommendationsList}"
																	binding="#{pages$SocialResearch.dataTable}"
																	preserveDataModel="false" preserveSort="false"
																	var="dataItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true" width="100%">
																	<t:column id="colSelected"
																		rendered="#{pages$SocialResearch.showCheckBox}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.select']}" />
																		</f:facet>
																		<t:selectBooleanCheckbox
																			rendered="#{dataItem.statusKey == 'DRAFT' ||  
																			             ( pages$SocialResearch.pageMode =='PAGE_MODE_DISBURSE_PAYMENTS' && dataItem.statusKey == 'DISBURSEMENT_REQUIRED' )}"
																			value="#{dataItem.selected}" />

																	</t:column>
																	<t:column id="col1" width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['recommenationDetils.caringType']}"
																				style="white-space: normal;" />
																		</f:facet>

																		<t:outputText style="white-space: normal;"
																			value="#{pages$SocialResearch.englishLocale?dataItem.caringType.caringTypeNameEn:dataItem.caringType.caringTypeNameAr}" />
																	</t:column>
																	<t:column id="colRcmdn" width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['recommenationDetils.recommendationNo']}"
																				style="white-space: normal;" />
																		</f:facet>

																		<t:outputText style="white-space: normal;"
																			value="#{dataItem.recommendationNumber}" />
																	</t:column>
																	<t:column id="colBenef" width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['recommenationDetils.beneficiary']}"
																				style="white-space: normal;" />
																		</f:facet>

																		<t:commandLink
																			actionListener="#{pages$SocialResearch.openManageBeneficiaryPopUp}"
																			style="white-space: normal;"
																			value="#{dataItem.person.personFullName}" />
																	</t:column>
																	<t:column id="col2" width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['recommenationDetils.status']}"
																				style="white-space: normal;" />
																		</f:facet>
																		<t:outputText style="white-space: normal;"
																			value="#{pages$SocialResearch.englishLocale?dataItem.statusEn:dataItem.statusAr}" />
																	</t:column>
																	<t:column id="col3" width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['recommenationDetils.DisbursementReq']}"
																				style="white-space: normal;" />
																		</f:facet>
																		<t:outputText
																			value="#{dataItem.disbursementRequired=='1'?msg['label.Disbursement.required']:msg['label.Disbursement.notRequired']}"
																			style="white-space: normal;" />
																	</t:column>

																	<t:column id="col4" width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['recommenationDetils.DisbursementList']}"
																				style="white-space: normal;" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$SocialResearch.englishLocale?dataItem.disbursementListTypeEn:dataItem.disbursementListTypeAr}"
																			style="white-space: normal;" />
																	</t:column>



																	<t:column id="col5" width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['disType.urgent']}"
																				style="white-space: normal;" />
																		</f:facet>
																		<t:outputText
																			value="#{dataItem.isPeriodic == '1'?msg['commons.no']:(dataItem.isPeriodic == '0'?msg['commons.yes']:msg['commons.no'])}"
																			style="white-space: normal;" />
																	</t:column>

																	<t:column id="col6" width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['recommenationDetils.period']}"
																				style="white-space: normal;" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$SocialResearch.englishLocale?dataItem.durationFreqEn:dataItem.durationFreqAr}"
																			style="white-space: normal;" />
																	</t:column>
																	<t:column id="col7" width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['recommenationDetils.fromDate']}"
																				style="white-space: normal;" />
																		</f:facet>
																		<t:outputText value="#{dataItem.startDate}"
																			style="white-space: normal;" />
																	</t:column>
																	<t:column id="col8" width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['recommenationDetils.ToDate']}"
																				style="white-space: normal;" />
																		</f:facet>
																		<t:outputText value="#{dataItem.endDate}"
																			style="white-space: normal;" />
																	</t:column>
																	<t:column id="col9" width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['recommenationDetils.disburseSrc']}"
																				style="white-space: normal;" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$SocialResearch.englishLocale?dataItem.disburseSrc.srcNameEn:dataItem.disburseSrc.srcNameAr}"
																			style="white-space: normal;" />
																	</t:column>
																	<t:column id="colAmont" width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['recommenationDetils.amount']}"
																				style="white-space: normal;" />
																		</f:facet>
																		<t:outputText value="#{dataItem.amount}"
																			style="white-space: normal;" />
																	</t:column>
																	<t:column id="col11" width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['recommenationDetils.comments']}"
																				style="white-space: normal;" />
																		</f:facet>
																		<t:outputText value="#{dataItem.comments}"
																			style="white-space: normal;" />
																	</t:column>
																	<t:column id="actioCol" sortable="false">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['mems.inheritanceFile.columnLabel.action']}" />
																		</f:facet>

																		<t:commandLink
																			action="#{pages$SocialResearch.onAddRecommendationsFollowup}">
																			<h:graphicImage
																				rendered="#{pages$SocialResearch.pageMode =='PAGE_MODE_EXECUTE_RECOMMENDATIONS'}"
																				title="#{msg['memsFollowupTab.addFollowup']}"
																				style="cursor:hand;"
																				url="../resources/images/app_icons/application_form_edit.png" />&nbsp;
																		</t:commandLink>
																		<t:commandLink id="nol"
																			action="#{pages$SocialResearch.editRecommendation}"
																			rendered="#{pages$SocialResearch.pageMode=='PAGE_MODE_RESUBMIT' || pages$SocialResearch.pageMode=='PAGE_MODE_EXECUTE_RECOMMENDATIONS'}">
																			<h:graphicImage id="nolIcon"
																				title="#{msg['commons.edit']}"
																				url="../resources/images/edit-icon.gif" />
																		</t:commandLink>
																		<t:commandLink
																			actionListener="#{pages$SocialResearch.openPaymentsPopUp}"
																			rendered="#{dataItem.isPeriodic=='0' && dataItem.disbursementRequired=='1'}">
																			<h:graphicImage id="collIcon"
																				title="#{msg['chequeList.chequeList']}"
																				url="../resources/images/app_icons/Add-fee.png" />
																		</t:commandLink>

																	</t:column>


																</t:dataTable>
															</t:div>
															<t:div style="width:10px;"></t:div>
															<t:div styleClass="BUTTON_TD" style="width:100%;">
															<pims:security
																	screen="Pims.MinorMgmt.SocialResearch.ExecuteRecommendation"
																	action="create">
																
																<h:commandButton id="btnCloseRecommendation"
																	type="submit" styleClass="BUTTON" style="width:auto;"
																	binding="#{pages$SocialResearch.btnCloseRecommendation}"
																	onclick="if (!confirm('#{msg['confirmMsg.doUWantTOCloseRecomm']}')) return false;else disableButtons(this);"
																	value="#{msg['socialResearch.btn.closeRecommendation']}" />
																	<h:commandLink id="lnkCloseRecommendationsOne" />
																	<h:commandLink id="lnkCloseRecommendations" action="#{pages$SocialResearch.onCloseRecommendations}" />
																<h:outputText value=" "></h:outputText>
																<h:commandButton id="btnRejectRecommendation"
																	type="submit" styleClass="BUTTON" style="width:180px;"
																	binding="#{pages$SocialResearch.btnRejectRecommendation}"
																	onclick="if (!confirm('#{msg['confirmMsg.doUWantTORejectRecomm']}')) return false; else disableButtons(this);"
																	value="#{msg['socialResearch.btn.reject']}" />
																	<h:commandLink id="lnkRejectRecommendationOne" />
																	<h:commandLink id="lnkRejectRecommendation" action="#{pages$SocialResearch.onRejectRecommendation}" />
																<h:outputText value=" "></h:outputText>
																<h:commandButton id="btnSendToFinance" type="submit"
																	styleClass="BUTTON" style="width:180px;"
																	onclick="if (!confirm('#{msg['confirmMsg.doUWantToSendToFinance']}')) return false;else disableButtons(this);"
																	
																	binding="#{pages$SocialResearch.btnSendToFinance}"
																	value="#{msg['socialResearch.btn.sendToFinance']}" />
																	<h:commandLink id="lnkSendtoFinanceOne" />
																	<h:commandLink id="lnkSendtoFinance" action="#{pages$SocialResearch.onSendToFinance}" />
															</pims:security>
															<pims:security
																	screen="Pims.MinorMgmt.SocialResearch.Disburse"
																	action="create">
																<h:commandButton id="onDisburseDone" type="submit"
																	styleClass="BUTTON" style="width:auto;"
																	binding="#{pages$SocialResearch.btnDisburseDone}"
																	value="#{msg['socialResearch.btn.DisburseDone']}"
																	onclick="if (!confirm('#{msg['confirmMsg.doUWantToDisburse']}')) return false;else disableButtons(this);"
																	 />
																	 <h:commandLink id="lnkDisburseOne" />
																	<h:commandLink id="lnkDisburse" action="#{pages$SocialResearch.onDisburse}" />
															</pims:security>
															</t:div>
														</rich:tab>
													   <rich:tab id="attachmentTab"
																	title="#{msg['commons.attachmentTabHeading']}"
																	label="#{msg['mems.inheritanceFile.tabHeadingShort.attch']}">
																	<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>

														<rich:tab id="commentsTab"
																	title="#{msg['commons.commentsTabHeading']}"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
														</rich:tab>
													</rich:tabPanel>
													<table cellpadding="1px" cellspacing="3px" width="100%">
														<tr>
															<td colspan="12" class="BUTTON_TD">
															<pims:security
																	screen="Pims.MinorMgmt.SocialResearch.Resubmit"
																	action="create">
																<h:commandButton id="onResubmit" type="submit"
																	styleClass="BUTTON" style="width:10%;"
																	binding="#{pages$SocialResearch.btnResubmit}"
																	value="#{msg['socialResearch.btn.resubmit']}"
																	onclick="if (!confirm('#{msg['confirmMsg.doUWantToResbmitRecom']}')) return false;else disableButtons(this);"
																	/>
																	<h:commandLink id="lnkResubmit" action="#{pages$SocialResearch.onResubmit}" />
														 	</pims:security>
														 	<pims:security
																	screen="Pims.MinorMgmt.SocialResearch.ApproveReject"
																	action="create">
															
																<h:commandButton id="onApprove" type="submit"
																	styleClass="BUTTON" style="width:10%;"
																	onclick="if (!confirm('#{msg['confirmMsg.doUWantToApproveRecom']}')) return false;else disableButtons(this);"
																	binding="#{pages$SocialResearch.btnApprove}"
																	value="#{msg['socialResearch.btn.approve']}" />

																	<h:commandLink id="lnkApprove" action="#{pages$SocialResearch.onApprove}" />
																	
																<h:commandButton id="onReject" type="submit"
																	styleClass="BUTTON" style="width:10%;"
																	value="#{msg['socialResearch.btn.reject']}"
																	binding="#{pages$SocialResearch.btnReject}"
																	onclick="if (!confirm('#{msg['confirmMsg.doUWantToRejectRecom']}')) return false;else disableButtons(this);"
																	 />
																	<h:commandLink id="lnkReject" action="#{pages$SocialResearch.onReject}" />
															</pims:security>
															<pims:security
																	screen="Pims.MinorMgmt.SocialResearch.Complete"
																	action="create">
															
																<h:commandButton id="onComplete" type="submit"
																	styleClass="BUTTON" style="width:10%;"
																	binding="#{pages$SocialResearch.btnComplete}"
																	value="#{msg['socialResearch.btn.complete']}"
																	onclick="if (!confirm('#{msg['confirmMsg.doUWantToCompleteRecom']}')) return false;else disableButtons(this);"
																	/>
																	<h:commandLink id="lnkComplete" action="#{pages$SocialResearch.onComplete}" />
																	
															</pims:security>
															</td>
														</tr>
													</table>
												</div>
											</div>





											</div>
										</td>
									</tr>
								</table>
							</h:form>
						</td>
					</tr>
					<tr>
						<td colspan="2">

							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel id="footerLabel"
											value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>

