<t:div styleClass="A_RIGHT">
	<pims:security screen="Pims.MinorMgmt.Unblock.Save" action="view">
		<h:commandButton styleClass="BUTTON"
			value="#{msg['UnBlockingRequest.title.heading']}"
			action="#{pages$tabFileBlockingDetails.onNavigateToUnblocking}" />
	</pims:security>
</t:div>
<t:div style="height:5px;"></t:div>
<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
	<t:dataTable id="dataTablePayments" rows="15" width="100%"
		value="#{pages$tabFileBlockingDetails.blockingList}"
		binding="#{pages$tabFileBlockingDetails.dataTable}"
		preserveDataModel="false" preserveSort="false" var="dataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

		<t:column id="check" sortable="true">
			<f:facet name="header">
				<t:outputText id="checkColInhblockin" />
			</f:facet>
			<h:selectBooleanCheckbox id="select"
				rendered="#{dataItem.showCheckBox}" value="#{dataItem.selected}" />
		</t:column>
		<t:column id="refNumcolblocking" sortable="true">
			<f:facet name="header">
				<t:outputText id="refNumHeadingblocking"
					value="#{msg['commons.refNum']}" />
			</f:facet>
			<t:outputText id="refNumblocking" styleClass="A_LEFT"
				value="#{dataItem.refNum}" />
		</t:column>
		<t:column id="fullNamecol" sortable="true">
			<f:facet name="header">
				<t:outputText id="fullNameHeading" value="#{msg['commons.Name']}" />
			</f:facet>
			<t:outputText id="fullName" styleClass="A_LEFT"
				value="#{dataItem.person.fullName}" />
		</t:column>

		<t:column id="statusCol" sortable="true">
			<f:facet name="header">
				<t:outputText id="statusAmnt" value="#{msg['commons.status']}" />
			</f:facet>
			<t:outputText id="status" styleClass="A_LEFT"
				value="#{pages$tabFileBlockingDetails.englishLocale? dataItem.status.dataDescEn:dataItem.status.dataDescAr}" />
		</t:column>

		<t:column id="amntCol" sortable="true">
			<f:facet name="header">
				<t:outputText id="hdAmnt" value="#{msg['blocking.lbl.amount']}" />
			</f:facet>
			<t:outputText id="amountt" styleClass="A_LEFT"
				value="#{dataItem.amount}">
				<f:convertNumber maxIntegerDigits="15" maxFractionDigits="2"
					pattern="##############0.00" />
			</t:outputText>
		</t:column>

	</t:dataTable>
</t:div>
<t:div 
		id="memsblcokingViewDataTableFooter" styleClass="contentDivFooter"
		style="width:95%">
		<t:panelGrid id="blcokingfooterTable" columns="2" cellpadding="0"
			cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD">


			<CENTER>
				<t:dataScroller id="blcokingScrollerscroller"
					for="dataTablePayments" paginator="true" fastStep="1"
					paginatorMaxPages="15"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="lblblcokingFirst"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="lblblcokingF"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="lblblcokingFF"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}" id="lblblcokingL"></t:graphicImage>
					</f:facet>


				</t:dataScroller>
			</CENTER>
		</t:panelGrid>
	</t:div>


