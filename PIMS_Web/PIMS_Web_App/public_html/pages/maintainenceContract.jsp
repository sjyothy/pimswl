


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
        
		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
</script>
    
<f:view>
        <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
       <f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
 
    
    <html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>						
            <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
			
			
 	</head>
     
<script language="javascript" type="text/javascript">
	function calculateGuaranteeAmount()
    {
      			var percent = parseFloat(document.getElementById("formRequest:txtGuaranteePercentage").value);
      			var contractAmount = parseFloat(document.getElementById("formRequest:totalContract").value);
      			var guranteeAmount = percent*contractAmount /100;
      			document.getElementById("formRequest:txtBankGuaranteeAmount").value="";
      			if(!isNaN( guranteeAmount ))
      			{
      			 document.getElementById("formRequest:txtBankGuaranteeAmount").value=guranteeAmount ;
      			}
      			
      
    }
        function showSearchTenderPopUp(tenderTypeKey,winnerContractorPresent,notBindedWithContract)
	{
	     
	     var  screen_width = 1024;
	     var screen_height = 450;
	     window.open('tenderSearch.jsf?VIEW_MODE=popup&context=MaintenanceContract','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=20,top=150,scrollbars=yes,status=yes');

	  
	}
	function showSearchContractorPopUp(viewMode,popUp)
	{
	     
	     var  screen_width = 1024;
	     var screen_height = 450;
	     
	     window.open('contractorSearch.jsf?'+viewMode+'='+popUp,'_blank','width='+(screen_width-80)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	  
	}
	function populateContractor(contractorId, contractorNumber, contractorNameEn, contractorNameAr, licenseNumber, licenseSource, officeNumber){
			document.getElementById("formRequest:hdnContractorId").value=contractorId;
			document.getElementById("formRequest:txtContractorNumber").value=contractorNumber;
		    document.forms[0].submit();
		}
	function populateTender(tenderId,tenderNumber,tenderDescription)
	{
	  document.getElementById("formRequest:txtTenderNumber").value=tenderNumber;
	  document.getElementById("formRequest:hdnTenderId").value=tenderId;
	  document.forms[0].submit();
	}
	
	
      
    </script>
 	 

				<!-- Header -->
	
		<body class="BODY_STYLE" >
		<div class="containerDiv">
	     
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
					<c:when test="${!pages$maintainenceContract.isViewModePopUp}">
					 <td colspan="2">
				        <jsp:include page="header.jsp"/>
				     </td>
				     </c:when>
		        </c:choose>
	            </tr>
				<tr>
				    <c:choose>
					 <c:when test="${!pages$maintainenceContract.isViewModePopUp}">
					  <td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					  </td>
				     </c:when>
		            </c:choose>
					<td width="83%"  height="45PX" valign="top" class="divBackgroundBody">
						<h:form id="formRequest" style="width:99.7%"  enctype="multipart/form-data">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
							    <td class="HEADER_TD" style="width:70%;" >
										 <h:outputLabel value="#{pages$maintainenceContract.pageTitle}"  styleClass="HEADER_FONT"/>
								</td>
						        <td >
									&nbsp;
								</td>
								
								
							</tr>
						</table>
					        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
                                 <tr valign="top">
                                  <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
                                   </td>    
									<td  height="450PX" valign="top" nowrap="nowrap">
									<div  class="SCROLLABLE_SECTION"  >
									  
											<table border="0" class="layoutTable" width="94%" style="margin-left:15px;margin-right:15px;" >
											<tr>
													<td >
													    <h:outputText  id="successmsg"   escape="false" styleClass="INFO_FONT"  value="#{pages$maintainenceContract.successMessages}"/>
														<h:outputText  id="errormsg"     escape="false" styleClass="ERROR_FONT" value="#{pages$maintainenceContract.errorMessages}"/>
														<h:inputHidden id="hdnContractId" value="#{pages$maintainenceContract.contractId}" />
														<h:inputHidden id="hdnContractCreatedOn" value="#{pages$maintainenceContract.contractCreatedOn}" />
														<h:inputHidden id="hdnContractCreatedBy" value="#{pages$maintainenceContract.contractCreatedBy}" />
														<h:inputHidden id="hdnRequestId" value="#{pages$maintainenceContract.requestId}" />
														<h:inputHidden id="hdnContractorId" value="#{pages$maintainenceContract.hdnContractorId}"/>
														<h:inputHidden id="hdnTenderId" value="#{pages$maintainenceContract.hdnTenderId}"/>
														<h:inputHidden id="hdnSearchTenderType" value="#{pages$maintainenceContract.tenderTypeMaintenance}"/>
														<h:commandLink id="cmdDeleteTender" action="#{pages$maintainenceContract.deleteTenderRelatedInfo}"/>
													</td>
												</tr>
											</table>
										
										<div class="MARGIN" style="width:95%;margin-bottom:0px;" >
   
                                         <t:panelGrid id ="tbl_Action" width="100%" border="0" columns="1"  binding="#{pages$maintainenceContract.tbl_Action}"  >
                                          <t:panelGrid id ="tbl_Remarks" cellpadding="1px" cellspacing="3px" width="100%" border="0" columns="2"  >
                                           <t:outputLabel  value="#{msg['commons.remarks']}:" />
                                          
                                          <t:inputTextarea id="txt_remarks" cols="100" value="#{pages$maintainenceContract.txtRemarks}" />
                                          </t:panelGrid>
                                          <t:panelGrid id ="tbl_ActionBtn" cellpadding="1px" cellspacing="3px" width="100%" border="0" columns="1"  columnClasses="BUTTON_TD">
                                          
                                          <t:panelGroup>
                                                 <pims:security screen="Pims.ServicesMgmt.MaintenanceContracts.ReviewRequired" action="view">
                                                     <h:commandButton id= "btnReviewReq" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$maintainenceContract.btnReviewReq_Click}"
			                                           			binding="#{pages$maintainenceContract.btnReviewReq}"  
			                                           			value="#{msg['commons.sendButton']}" />
                                                 </pims:security> 	 			                                           			
			                                     <pims:security screen="Pims.ServicesMgmt.MaintenanceContracts.ApproveReject" action="view">      			
			                                          <h:commandButton id= "btnApprove" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$maintainenceContract.btnApprove_Click}"
			                                           			binding="#{pages$maintainenceContract.btnApprove}"  
			                                           			value="#{msg['commons.approve']}" />
			                                          <h:commandButton id= "btnReject" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$maintainenceContract.btnReject_Click}"
			                                           			binding="#{pages$maintainenceContract.btnReject}"  
			                                           			value="#{msg['commons.reject']}" />
			                                     </pims:security> 			
			                                     <pims:security screen="Pims.ServicesMgmt.MaintenanceContracts.Review" action="view">
			                                          <h:commandButton id= "btnReview" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$maintainenceContract.btnReview_Click}"
			                                           			binding="#{pages$maintainenceContract.btnReview}"  
			                                           			value="#{msg['commons.reviewButton']}" />
			                                     </pims:security>
			                                     <pims:security screen="Pims.ServicesMgmt.MaintenanceContracts.Complete" action="view">
			                                         <h:commandButton id= "btnComplete" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$maintainenceContract.btnComplete_Click}"
			                                           			binding="#{pages$maintainenceContract.btnComplete}"  
			                                           			value="#{msg['commons.complete']}" />
			                                       </pims:security> 			
			                                       <pims:security screen="Pims.ServicesMgmt.MaintenanceContracts.Print" action="view">
			                                         <h:commandButton id= "btnPrint" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$maintainenceContract.btnPrint_Click}"
			                                           			binding="#{pages$maintainenceContract.btnPrint}"
			                                           			value="#{msg['commons.print']}" />
			                                       </pims:security>			
                                         </t:panelGroup>
                                         </t:panelGrid>
                                       </t:panelGrid>
											
										<table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
										</table>	
										
						 				  <rich:tabPanel id="tabPanel" binding="#{pages$maintainenceContract.tabPanel}" style="width:100%;height:320px;" headerSpacing="0">
												    <rich:tab id="tabContractDetails"  title="#{msg['contract.tab.BasicInfo']}"  label="#{msg['contract.tab.BasicInfo']}" >
												            <%@ include file="../pages/ServiceContractDetailsTab.jsp"%>
												    </rich:tab>
												    <rich:tab id="scopeOfWorkTab" label="#{msg['commons.tab.scopeOfWork']}" title="#{msg['commons.tab.scopeOfWork']}"  action="#{pages$maintainenceContract.tabWorkScope_Click}">
															<%@  include file="ScopeOfWork.jsp"%>
													</rich:tab>
												    <rich:tab id="tabProperties" label="#{msg['serviceContract.tab.propertyDetails']}"  title="#{msg['serviceContract.tab.propertyDetails']}"  action="#{pages$maintainenceContract.tabPropertyDetails_Click}">
				                                   		    <%@ include file="../pages/ServiceContractPropertyDetails.jsp"%>
				                                   	</rich:tab>
				                                   	 <rich:tab  id="tabPaymentSchedule" binding="#{pages$maintainenceContract.tabPaymentTerms}" action="#{pages$maintainenceContract.tabPaymentTerms_Click}"   title="#{msg['contract.paymentTerms']}" label="#{msg['contract.paymentTerms']}" >
				                                       <%@ include file="../pages/serviceContractPaySchTab.jsp"%>
				                                   	</rich:tab>
			                                   		<rich:tab label="#{msg['contract.tabHeading.RequestHistory']}"  title="#{msg['commons.tab.requestHistory']}"  action="#{pages$maintainenceContract.tabAuditTrail_Click}">
								                                     <%@ include file="../pages/requestTasks.jsp"%>
													 </rich:tab>
													 <rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}" action= "#{pages$maintainenceContract.tabAttachmentsComments_Click}">
																		<%@  include file="attachment/attachment.jsp"%>
													 </rich:tab>
										             <rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}" action= "#{pages$maintainenceContract.tabAttachmentsComments_Click}">
																	<%@ include file="notes/notes.jsp"%>
													 </rich:tab>
												
				                         </rich:tabPanel>
				                                    	
				                         </div>
                                    
                                   
                                        <table cellpadding="0" cellspacing="0"  style="width:95%;margin-left:10px;">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
										
											<br/>
											
										
										<table cellpadding="1px" cellspacing="3px" width="97%" >
											<tr>
											
											    <td colspan="12" class="BUTTON_TD">
											    <pims:security screen="Pims.ServicesMgmt.MaintenanceContracts.Persist" action="view">
                                                    <h:commandButton type="submit"  styleClass="BUTTON" 
                                           			action="#{pages$maintainenceContract.btnSave_Click}"
                                           			binding="#{pages$maintainenceContract.btnSave}"  
                                           			value="#{msg['commons.saveButton']}" />
                                           		</pims:security>
                                           		<pims:security screen="Pims.ServicesMgmt.MaintenanceContracts.SendForApproval" action="view">
                                           		 <h:commandButton id= "btnSendForApproval" type="submit"  styleClass="BUTTON" 
                                           			action="#{pages$maintainenceContract.btnSendForApproval_Click}"
                                           			binding="#{pages$maintainenceContract.btnSend_For_Approval}"  
                                           			value="#{msg['commons.sendButton']}" />
                                           		</pims:security>	
										             
										        </td>
		                                   </tr>
		                                </table>
                                          
                                   </div>
                            </div>
									</td>
									</tr>
									</table>
			</h:form>
			</td>
    </tr>
    <tr>
			<td colspan="2">
			
			    <table width="100%" cellpadding="0" cellspacing="0" border="0">
			       <tr><td class="footer"><h:outputLabel value="#{msg['commons.footer.message']}" /></td></tr>
			    </table>
			</td>
    </tr>
    </table>
			</div>
		</body>
	</html>
</f:view>

