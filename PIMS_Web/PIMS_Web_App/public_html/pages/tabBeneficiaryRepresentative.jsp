
<t:div id="tabBDetailsdfRepresentative"
	style="height:200px;overflow-y:scroll;width:100%;">
	<t:panelGrid id="tabdsBDetailsabRepresentative" cellpadding="5px"
		width="100%" cellspacing="10px" columns="4"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="firstnameRepresentative" styleClass="LABEL"
				value="#{msg['person.representative']}:"></h:outputLabel>
		</h:panelGroup>

		<h:panelGroup>

			<h:inputText maxlength="500" readonly="true" id="representativeName"
				value="#{pages$tabBeneficiaryRepresentative.hdnrepresentativePersonName}" />
			<h:graphicImage title="#{msg['commons.search']}"
				style="MARGIN: 1px 1px -5px;cursor:hand"
				onclick="showRepresentativePopup();"
				url="../resources/images/app_icons/Search-tenant.png">
			</h:graphicImage>

			<h:inputHidden id="hdnrepresentativePersonId"
				value="#{pages$tabBeneficiaryRepresentative.hdnrepresentativePersonId}" />
			<h:inputHidden id="hdnrepresentativePersonName"
				value="#{pages$tabBeneficiaryRepresentative.hdnrepresentativePersonName}" />


		</h:panelGroup>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputText value="#{msg['commons.Beneficiary']}" />
		</h:panelGroup>
		<h:selectOneMenu id="representativeBeneficary"
			value="#{pages$tabBeneficiaryRepresentative.selectOneInheritanceBeneficiaryId}">
			<f:selectItem itemValue="-1"
				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
			<f:selectItems
				value="#{pages$tabBeneficiaryRepresentative.inheritanceBeneficiariesList}" />
		</h:selectOneMenu>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.relationship']}" />
		</h:panelGroup>
		<h:selectOneMenu id="relationshipIdBeneficiaryRepresentative"
			styleClass="SELECT_MENU"
			value="#{pages$tabBeneficiaryRepresentative.relationshipIdString}">
			<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
				itemValue="-1" />
			<f:selectItems value="#{pages$ApplicationBean.relationList}" />
		</h:selectOneMenu>


		<h:outputLabel
			value="#{msg['beneficiaryGuardianTab.RelationshipDesc']} :"
			style="font-weight:normal;" styleClass="TABLE_LABEL" />
		<h:inputTextarea id="relationshipDescBeneficiaryRepresentative"
			onkeyup="javaScript:removeExtraCharacter(this,50)"
			onkeypress="javaScript:removeExtraCharacter(this,50)"
			onkeydown="javaScript:removeExtraCharacter(this,50)"
			onblur="javaScript:removeExtraCharacter(this,50)"
			onchange="javaScript:removeExtraCharacter(this,50)"
			value="#{pages$tabBeneficiaryRepresentative.relationshipDesc}" />
	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;">
		<h:commandButton styleClass="BUTTON" value="#{msg['commons.Add']}"
			action="#{pages$tabBeneficiaryRepresentative.onAdd}">
		</h:commandButton>

	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableRepresentativeBeneficiary" rows="15"
			width="100%" value="#{pages$tabBeneficiaryRepresentative.dataList}"
			binding="#{pages$tabBeneficiaryRepresentative.dataTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="NamedtcRepresentative" sortable="true">
				<f:facet name="header">
					<t:outputText id="NameOTRepresentative"
						value="#{msg['person.representative']}" />
				</f:facet>
				<t:commandLink id="s_b_rep"
					onclick="javaScript:showPersonReadOnlyPopup(#{dataItem.representative.personId});"
					styleClass="A_LEFT" style="white-space: normal;"
					value="#{dataItem.representative.fullName}" />


			</t:column>

			<t:column id="beneficiaryNamedtcRepresentative" sortable="true">
				<f:facet name="header">
					<t:outputText id="beneficiaryNameOTRepresentative"
						value="#{msg['searchInheritenceFile.benificiary']}" />
				</f:facet>
				<t:commandLink id="s_b"
					onclick="javaScript:openBeneficiaryPopup(#{dataItem.inheritanceBeneficiary.beneficiary.personId});"
					styleClass="A_LEFT" style="white-space: normal;"
					value="#{dataItem.inheritanceBeneficiary.beneficiary.fullName}" />
			</t:column>
			<t:column id="relationshipColRepresentative" sortable="true">
				<f:facet name="header">
					<h:outputText
						value="#{msg['mems.inheritanceFile.columnLabel.relationship']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabBeneficiaryRepresentative.englishLocale? dataItem.relationship.relationshipDescEn :
				dataItem.relationship.relationshipDescAr
				}" />
			</t:column>
			<t:column id="NamedtcRepresentativerelationdesc" sortable="true">
				<f:facet name="header">
					<t:outputText id="NamedtcRepresentativerelationdescot"
						value="#{msg['beneficiaryGuardianTab.RelationshipDesc']}" />
				</f:facet>
				<h:outputText value="#{dataItem.relationshipDesc}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="colUsercreatedbyrepresentative">
				<f:facet name="header">
					<t:outputText value="#{msg['socialProgram.lbl.CreatedBy']}"
						id="colUsercreatedbyrepresentativeot" />
				</f:facet>
				<t:outputText styleClass="A_LEFT"
					value="#{dataItem.createdBy}"
					id="txtcolUsercreatedbyrepresentative" />
			</t:column>
			<t:column id="colCreatedOnrep" style="width:20%">
				<f:facet name="header">
					<t:outputText value="#{msg['socialProgram.lbl.sponsorCreatedOn']}" id="colCreatedOnrepot" />
				</f:facet>
				<t:outputText styleClass="A_LEFT" value="#{dataItem.createdOn}"
					id="colCreatedOnreplbl">
					<f:convertDateTime timeZone="#{pages$tabBeneficiaryRepresentative.timeZone}"
						pattern="#{pages$tabBeneficiaryRepresentative.dateFormat}" />
				</t:outputText>

			</t:column>
			<t:column id="actionRepresentative" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdActionRepresentative"
						value="#{msg['commons.action']}" />
				</f:facet>
				<h:commandLink id="lnkDeleteBeneficiaryRepresentative"
					onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;"
					action="#{pages$tabBeneficiaryRepresentative.onDelete}">
					<h:graphicImage id="deleteIconRepresentative"
						title="#{msg['commons.delete']}"
						style="cursor:hand;margin-left:2px;margin-right:2px"
						url="../resources/images/delete.gif" width="18px;" />
				</h:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>


</t:div>


