<h:panelGrid columns="5" rendered="#{pages$receivePayment.cashCurrent}">
	<f:facet name="header">

	</f:facet>
	<h:outputText value="#{msg['payment.amount']}" ></h:outputText>
	<h:inputText id="f7" binding="#{pages$receivePayment.cashAmount}"
		style="text-align:right; height: 16px"></h:inputText>

	<h:outputText value="#{msg['payment.exchangeRate']}"></h:outputText>
	<h:inputText binding="#{pages$receivePayment.exchangeRate}"
		style="text-align:right; height: 16px"></h:inputText>
</h:panelGrid>
