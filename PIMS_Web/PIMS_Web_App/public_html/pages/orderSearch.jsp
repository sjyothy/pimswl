<%-- 
  - Author: Anil Verani
  - Date: 30/07/2009
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Orders
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

	function closeAndPassSelected()
	{
		window.opener.receiveSelectedOrders();
		window.close();		
	}

	function clearValues() 
	{
		
	}

</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
			 <title>PIMS</title>
			 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			 <meta http-equiv="pragma" content="no-cache">
			 <meta http-equiv="cache-control" content="no-cache">
			 <meta http-equiv="expires" content="0">
			 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
	</head>

	<body class="BODY_STYLE">
			<%
			   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
			   response.setHeader("Pragma","no-cache"); //HTTP 1.0
			   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
			%>
		<div class="containerDiv">	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
						<c:when test="${pages$orderSearch.isSearchMode}">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>					
						</c:when>
					</c:choose>
				</tr>

				<tr width="100%">
					<c:choose>
						<c:when test="${pages$orderSearch.isSearchMode}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>						
						</c:when>
					</c:choose>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['order.search.heading']}" styleClass="HEADER_FONT"/>																				
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td width="100%" height="466px" valign="top" >
									<div class="SCROLLABLE_SECTION  AUC_SCH_SS">
									<h:form id="searchFrm" style="WIDTH: 97.6%;">
										<table border="0" class="layoutTable">
											<tr>
												<td>
													<h:outputText value="#{pages$orderSearch.errorMessages}"  escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$orderSearch.successMessages}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
										<div class="MARGIN"> 
										<table cellpadding="0" cellspacing="0" width="100%">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
											<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
										<div class="DETAIL_SECTION">
											<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
											<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
												<tr>
													<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
														<h:outputLabel styleClass="LABEL" value="#{msg['order.transaction.no']}:"></h:outputLabel>
													</td>
													<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
														<h:inputText value="#{pages$orderSearch.orderView.transactionRefNo}" style="width: 186px;" maxlength="20" />														
													</td>													
													<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
														<h:outputLabel styleClass="LABEL" value="#{msg['order.status']}:"></h:outputLabel>
													</td>
													<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
														<h:selectOneMenu value="#{pages$orderSearch.orderView.orderStatusId}" style="width: 192px;">
															<f:selectItem itemValue="" itemLabel="#{msg['commons.All']}" />
															<f:selectItems value="#{pages$ApplicationBean.orderStatusList}" />
														</h:selectOneMenu>
													</td>
												</tr>
												
												<tr>
													<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
														<h:outputLabel styleClass="LABEL" value="#{msg['order.type']}:"></h:outputLabel>
													</td>
													<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
														<h:selectOneMenu value="#{pages$orderSearch.orderView.orderTypeId}" style="width: 192px;">
															<f:selectItem itemValue="" itemLabel="#{msg['commons.All']}" />
															<f:selectItems value="#{pages$ApplicationBean.orderTypeList}" />
														</h:selectOneMenu>
													</td>
													
													<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
														<h:outputLabel styleClass="LABEL" value="#{msg['order.transaction.log']}:"></h:outputLabel>
													</td>
													<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
														<h:selectOneMenu value="#{pages$orderSearch.orderView.transactionLogId}" style="width: 192px;">
															<f:selectItem itemValue="" itemLabel="#{msg['commons.All']}" />
															<f:selectItems value="#{pages$ApplicationBean.transactionLogList}" />
														</h:selectOneMenu>
													</td>
												</tr>
												
												
												<tr>
													<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
														<h:outputLabel styleClass="LABEL" value="#{msg['order.transaction.date.from']}:"></h:outputLabel>
													</td>
													<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
														<rich:calendar value="#{pages$orderSearch.orderView.transactionDateFrom}"
																	   inputStyle="width: 186px; height: 18px"
											                           locale="#{pages$orderSearch.locale}" 
											                           popup="true" 
											                           datePattern="#{pages$orderSearch.dateFormat}" 
											                           showApplyButton="false" 
											                           enableManualInput="false" 
											                           cellWidth="24px" cellHeight="22px" 
											                           style="width:186px; height:16px" />
													</td>
													
													<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
														<h:outputLabel styleClass="LABEL" value="#{msg['order.transaction.date.to']}:"></h:outputLabel>
													</td>
													<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
														<rich:calendar value="#{pages$orderSearch.orderView.transactionDateTo}"
																	   inputStyle="width: 186px; height: 18px"
											                           locale="#{pages$orderSearch.locale}" 
											                           popup="true" 
											                           datePattern="#{pages$orderSearch.dateFormat}" 
											                           showApplyButton="false" 
											                           enableManualInput="false" 
											                           cellWidth="24px" cellHeight="22px" 
											                           style="width:186px; height:16px" />
													</td>
												</tr>
												
												<tr>
													<td class="BUTTON_TD JUG_BUTTON_TD" colspan="4">
														<table cellpadding="1px" cellspacing="1px">
															<tr>
																<td>
																	<h:commandButton styleClass="BUTTON" value="#{msg['commons.search']}" action="#{pages$orderSearch.onSearch}" style="width: 80px" tabindex="7"></h:commandButton>							     									
																</td>
																<td>
							     									<h:commandButton styleClass="BUTTON" value="#{msg['commons.clear']}" onclick="javascript:clearValues(); return false;" style="width: 80px" tabindex="7"></h:commandButton>
																</td>
																<td>
							     									<h:commandButton styleClass="BUTTON" value="#{msg['order.add.btn.buying']}" action="#{pages$orderSearch.onAddBuyingOrder}" rendered="#{pages$orderSearch.isSearchMode}" style="width: 120px" tabindex="7"></h:commandButton>
																</td>
																<td>
							     									<h:commandButton styleClass="BUTTON" value="#{msg['order.add.btn.selling']}" action="#{pages$orderSearch.onAddSellingOrder}" rendered="#{pages$orderSearch.isSearchMode}" style="width: 120px" tabindex="7"></h:commandButton>
																</td>
															</tr>
														</table>	
													</td>
												</tr>
											</table>
										</div>	
										</div>
										<div style="padding-bottom:7px;padding-left:10px;padding-right:7px;padding-top:7px;">
										<div class="imag">&nbsp;</div>
										<div class="contentDiv">
											<t:dataTable 
												binding="#{pages$orderSearch.dataTable}"
												id="dt1"
												value="#{pages$orderSearch.orderViewList}"												 
												rows="#{pages$orderSearch.paginatorRows}"
												preserveDataModel="false" preserveSort="false" 
												var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												width="100%">
												
												<t:column width="18%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['order.transaction.no']}" />
													</f:facet>
													<t:outputText value="#{dataItem.transactionRefNo}" />
												</t:column>
												
												<t:column width="18%" sortable="true">
													<f:facet  name="header">
														<t:outputText value="#{msg['order.type']}" />
													</f:facet>
													<t:outputText value="#{pages$orderSearch.isEnglishLocale ? dataItem.orderTypeEn : dataItem.orderTypeAr}" />
												</t:column>
												
												<t:column width="18%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['order.transaction.date']}" />
													</f:facet>
													<t:outputText value="#{dataItem.transactionDate}">
														<f:convertDateTime timeZone="#{pages$orderSearch.timeZone}" pattern="#{pages$orderSearch.dateFormat}" />
													</t:outputText>
												</t:column>
												
												<t:column width="18%" sortable="true">
													<f:facet  name="header">
														<t:outputText value="#{msg['order.transaction.log']}" />
													</f:facet>
													<t:outputText value="#{pages$orderSearch.isEnglishLocale ? dataItem.transactionLogEn : dataItem.transactionLogAr}" />
												</t:column>					
																						
												<t:column width="18%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.status']}" />
													</f:facet>
													<t:outputText value="#{pages$orderSearch.isEnglishLocale ? dataItem.orderStatusEn : dataItem.orderStatusAr}" />
												</t:column>
												
												<t:column rendered="#{pages$orderSearch.isMultiSelectPopupMode}" sortable="false" width="10%">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.select']}" />
													</f:facet>
													<h:selectBooleanCheckbox value="#{dataItem.isSelected}" />
												</t:column>
												
												<t:column rendered="#{pages$orderSearch.isSingleSelectPopupMode}" sortable="false" width="10%">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.select']}" />
													</f:facet>
													<t:commandLink action="#{pages$orderSearch.onSingleSelect}">															
														<h:graphicImage title="#{msg['commons.select']}" url="../resources/images/select-icon.gif" />&nbsp;
													</t:commandLink>
												</t:column>
												
												<t:column rendered="#{pages$orderSearch.isSearchMode}" sortable="false" width="10%">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.action']}" />
													</f:facet>
													
													<t:commandLink action="#{pages$orderSearch.onView}">
															<h:graphicImage title="#{msg['commons.view']}" url="../resources/images/detail-icon.gif" />
													</t:commandLink>													
													
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													
													<t:commandLink action="#{pages$orderSearch.onEdit}">
															<h:graphicImage title="#{msg['commons.edit']}" url="../resources/images/edit-icon.gif" />
													</t:commandLink>										
													
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													
													<t:commandLink onclick="if (!confirm('#{msg['order.confirm.delete']}')) return" action="#{pages$orderSearch.onDelete}">
														<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />
													</t:commandLink>													
												</t:column>
												
											</t:dataTable>
										</div>
										<t:div styleClass="contentDivFooter" style="width:99%">
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$orderSearch.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">
												 		
												<t:dataScroller id="scroller" for="dt1" paginator="true"
													fastStep="1" paginatorMaxPages="#{pages$orderSearch.paginatorMaxPages}" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 								
												    paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;" 
													pageIndexVar="pageNumber"
													styleClass="SCH_SCROLLER" >
														<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
														<t:div styleClass="PAGE_NUM_BG">
															<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
														</t:div>
												</t:dataScroller>
													
                                        		</td></tr>
											</table>
											</t:div>
											
											<div>
												<table width="100%">
													<tr>
														<td class="BUTTON_TD" colspan="4">															
															<h:commandButton action="#{pages$orderSearch.onMultiSelect}" rendered="#{pages$orderSearch.isMultiSelectPopupMode}" value="#{msg['commons.select']}" styleClass="BUTTON" />															
														</td>
													</tr>
												</table>
											</div>
											 
                                           </div>
                                           </div>
                                           
									</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${pages$orderSearch.isSearchMode}">					
							<td colspan="2">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</tr>
								</table>
							</td>
						</c:when>
					</c:choose>					
				</tr>
			</table>
		</div>
	</body>
</html>
</f:view>