
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
		
	            
	function onChkChange()
    {
      document.getElementById('frm:disablingDiv').style.display='none';
    }
    function onSelectionChanged(changedChkBox)
	{
	    document.getElementById('frm:disablingDiv').style.display='block';
		changedChkBox.nextSibling.onclick();
	}
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	function closeWindow()
		{
		  window.close();
		}
        
        function	openPopUp()
		{
		
		    var width = 300;
            var height = 300;
            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + ",menubar=yes,toolbar=yes,scrollbars=yes,resizable=yes,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

            var child1 = window.open("about:blank", "subWind", windowFeatures);

		      child1.focus();
		}
		
		
		
			
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<td width="100%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"  style="width:100%;"
							border="0">
							<tr width="100%">
								<td class="HEADER_TD" width="100%">
									<h:outputLabel value="#{msg['commons.distributePopUp']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="100%" style="margin-left: 1px;" height="100%"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="frm" enctype="multipart/form-data" style="height:100%;">
										<div class="SCROLLABLE_SECTION" >
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText id="errorMessages"
																value="#{pages$distributePopUp.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText id="successMessages"
																value="#{pages$distributePopUp.successMessages}"
																escape="false" styleClass="INFO_FONT" />
															<h:inputHidden id="rowId"
																value="#{pages$distributePopUp.rowId}" />

														</td>
													</tr>
												</table>
											</div>


											<div class="MARGIN" style="width: 95%;">

												<div class="DETAIL_SECTION" style="width: 99.8%;">
													<h:outputLabel value="#{msg['commons.distributePopUp']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px" border="0"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">

																	<tr>

																		<td>
																			<h:outputLabel id="lblTrxNo"
																				value="#{msg['collectionProcedure.TrxNumber']}:" />
																		</td>
																		<td>
																			<h:inputText id="trxNumber" styleClass="READONLY"
																				readonly="true"
																				value="#{pages$distributePopUp.collectionTrxView.trxNumber}" />

																		</td>
																		<td>
																			<h:outputLabel id="lblTrxMethod"
																				value="#{msg['collectionProcedure.collectedAs']}:" />
																		</td>
																		<td>
																			<h:inputText id="trxMethod" styleClass="READONLY"
																				readonly="true"
																				value="#{pages$distributePopUp.englishLocale ?pages$distributePopUp.paymentDetailsView.paymentMethodEn:pages$distributePopUp.paymentDetailsView.paymentMethodAr}" />

																		</td>
																	</tr>

																	<tr>
																		<td>
																			<h:outputLabel id="lblTrxAmount"
																				value="#{msg['collectionProcedure.amount']}:" />
																		</td>
																		<td>
																			<h:inputText id="trxAmount" styleClass="READONLY"
																				readonly="true"
																				value="#{pages$distributePopUp.paymentDetailsView.amount}" />

																		</td>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.distributePopUp']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:selectOneMenu id="distCrit"
																				disabled="#{!pages$distributePopUp.pageModeConfirm}"
																				binding="#{pages$distributePopUp.distCritSelectMenu}"
																				tabindex="3">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$distributePopUp.distCritList}" />

																				<a4j:support id="as"
																					action="#{pages$distributePopUp.distributionCritChanged}"
																					event="onchange"
																					reRender="test2,dataTableGeneralAccount,parentDivs,errorMessages,successMessages,topButtonsDiv,btnDiv2,btnDone,btnDone2" />



																			</h:selectOneMenu>
																		</td>


																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</div>
												<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
												<t:div styleClass="BUTTON_TD" id="topButtonsDiv">
													<h:commandButton id="btnDone" styleClass="BUTTON"
														binding="#{pages$distributePopUp.btnDone}"
														rendered="#{pages$distributePopUp.pageModeConfirm}"
														value="#{msg['commons.done']}"
														actionListener="#{pages$distributePopUp.btnDone_Click}"
														style="width: auto" />


													<h:commandButton styleClass="BUTTON"
														value="#{msg['generateFloors.Close']}"
														onclick="javascript:window.close();" style="width: auto">
													</h:commandButton>
												</t:div>

												<t:div styleClass="contentDiv" id="parentDivs"
													style="width:95%">
													<t:dataTable id="test2" width="100%" styleClass="grid"
														binding="#{pages$distributePopUp.dataTable}"
														value="#{pages$distributePopUp.assetBeneficiaryDataList}"
														rows="200" preserveDataModel="true" preserveSort="false"
														var="dataItem" rowClasses="row1,row2"
														rowId="#{dataItem.inheritedBeneficiaryId}">
														>

														<t:column id="selectCheckBox" width="10%" sortable="false"
															style="white-space: normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.select']}" />
															</f:facet>
															<h:selectBooleanCheckbox id="select"
																rendered="#{dataItem.checkBoxRender==0}"
																readonly="#{!pages$distributePopUp.pageModeConfirm}"
																value="#{dataItem.selected}"
																onclick="onSelectionChanged(this);" />
															<a4j:commandLink id="lnkUnitSelectionChanged"
																onbeforedomupdate="javascript:onChkChange();"
																action="#{pages$distributePopUp.onChkChange}"
																reRender="test2,layoutTable,errorMessages,successMessages,topButtonsDiv,btnDiv2,btnDone,btnDone2" />
														</t:column>

														<t:column id="beneficiary" width="10%" sortable="false"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText value="#{msg['distribution.beneficiary']}" />
															</f:facet>
															<t:commandLink id="s_b"
																onclick="javaScript:openBeneficiaryPopup(#{dataItem.beneficiaryId});"
																styleClass="A_LEFT" value="#{dataItem.beneficiaryName}" />
														</t:column>
														<t:column id="fileNumber" width="10%" sortable="false"
															style="white-space: normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['distribution.fileNumber']}" />
															</f:facet>

															<t:commandLink id="colfileNumber"
																action="#{pages$distributePopUp.openFilePopUp}"
																styleClass="A_LEFT"
																value="#{dataItem.inheritanceFileNumber}" />
														</t:column>
														<t:column id="colFileStatus" width="10%" sortable="false"
															style="white-space: normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.status']}" />
															</f:facet>
															<t:outputText
																value="#{pages$distributePopUp.englishLocale?dataItem.inheritanceFileStatusEn:dataItem.inheritanceFileStatusAr}" />
														</t:column>
														<t:column id="totalFileShare" width="10%" sortable="false"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText
																	value="#{msg['distribution.totalFileShare']}" />
															</f:facet>
															<t:outputText value="#{dataItem.totalFileShares}" />
														</t:column>
														<t:column id="shariaPercent" width="10%" sortable="false"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText
																	value="#{msg['distribution.shariaPercent']}" />
															</f:facet>
															<t:outputText value="#{dataItem.shariaPercent}" />
														</t:column>


														<t:column id="assetTotalShares" width="10%"
															sortable="false" style="white-space: normal;">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['distribution.totalAssetShares']}" />
															</f:facet>
															<t:outputText value="#{dataItem.totalAssetShares}" />
														</t:column>

														<t:column id="assetNumber" width="10%" sortable="false"
															style="white-space: normal;">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['distribution.assetPercent']}" />
															</f:facet>
															<t:outputText value="#{dataItem.assetPercent}" />
														</t:column>

														<t:column id="amount" width="10%" sortable="false"
															style="white-space: normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['distribution.amout']}" />
															</f:facet>
															<t:outputText value="#{dataItem.amount}" />
														</t:column>
													</t:dataTable>
													<t:dataTable id="dataTableGeneralAccount" width="100%"
														styleClass="grid"
														binding="#{pages$distributePopUp.dataTableGeneralAccount}"
														value="#{pages$distributePopUp.generalAccountDataList}"
														rendered="#{pages$distributePopUp.isGeneralAccountSelected}"
														rows="10" preserveDataModel="true" preserveSort="false"
														var="dataItem" rowClasses="row1,row2">

														<t:column id="selectCheckBox" width="10%" sortable="false"
															style="white-space: normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.select']}" />
															</f:facet>
															<h:selectBooleanCheckbox id="select"
																rendered="#{dataItem.checkBoxRender==0}"
																value="#{dataItem.selected}" />
														</t:column>

														<t:column id="fileOwnerName" width="10%" sortable="false"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText
																	value="#{msg['distribution.fileOwnerName']}" />
															</f:facet>
															<t:outputText value="#{dataItem.fileOwnerName}" />
														</t:column>


														<t:column id="fileNumber" width="10%" sortable="false"
															style="white-space: normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['distribution.fileNumber']}" />
															</f:facet>
															<t:outputText value="#{dataItem.inheritanceFileNumber}" />
														</t:column>

														<t:column id="amount" width="10%" sortable="false"
															style="white-space: normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['distribution.amout']}" />
															</f:facet>
															<t:inputText value="#{dataItem.amount}" />
														</t:column>
													</t:dataTable>

												</t:div>
												<t:div id="btnDiv2" styleClass="BUTTON_TD">
													<h:commandButton id="btnDone2" styleClass="BUTTON"
														value="#{msg['commons.done']}"
														binding="#{pages$distributePopUp.btnDone2}"
														rendered="#{pages$distributePopUp.pageModeConfirm}"
														actionListener="#{pages$distributePopUp.btnDone_Click}"
														style="width: auto">
													</h:commandButton>

													<h:commandButton id="btnClose2" styleClass="BUTTON"
														value="#{msg['generateFloors.Close']}"
														onclick="javascript:window.close();" style="width: auto">
													</h:commandButton>
												</t:div>
											</div>
										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<script language="javascript" type="text/javascript">
	
		     var finId = document.getElementById('frm:rowId').value;
		     
		     if(finId.length>0)
		     {
	           	var tbl =document.getElementById('frm:test2');
	            var trs = tbl.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
	            for (var i = 0; i < trs.length; i++) 
	            {
	               trs[i].style.backgroundColor = '';
	                if(finId.indexOf( trs[i].getAttribute("id") ) !== -1 )
		              	trs[i].style.backgroundColor = '#f9a60c';
	           	}
           	}
	
	</script>
		</body>
	</html>
</f:view>