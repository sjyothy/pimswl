<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="javascript" type="text/javascript">

	function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
	function   showContractorsPopup()
						{
						   var screen_width = screen.width;
						   var screen_height = screen.height;
						   window.open('SearchPerson.jsf?persontype=CONTRACTOR&viewMode=popup','_blank','width='+(screen_width-10)+',height='+(screen_height-270)+',left=0,top=40,scrollbars=yes,status=yes');
						    
						}

 		function showPopup()
		{
		   var screen_width = screen.width;
		   var screen_height = screen.height;
		   window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
		}
		
		function populateTenant(TenantId,TenantNumber,TypeId,tenantNames)
        {
            
        }
        
       function closeWindow()
       {
         window.close();
       }

</script>


<f:view>
	<script>
	 function submitForm(){
        	document.forms[0].submit();
        }
	
		function showSearchContractorPopUp()
	{
	     
	     var  screen_width = screen.width;
	     var screen_height = screen.height;
	     
	     window.open('contractorSearch.jsf?pageMode=MODE_SELECT_MANY_POPUP','_blank','width='+(screen_width-10)+',height='+(screen_height-270)+',left=0,top=40,scrollbars=no,status=yes');
	  
	}
	function populateContractor(contractorId, contractorNumber, contractorNameEn, contractorNameAr, licenseNumber, licenseSource, officeNumber)
	{
	
			document.getElementById("TenderManagementForm:hdnContractorId").value=contractorId;
			document.getElementById("TenderManagementForm:hdnTDPersonId").value=contractorId;
			document.forms[0].submit();
		}
	
			  function showPropertyPopup()
				 {
					var screen_width = 1024;
					var screen_height = 465;
					window.open('propertyList.jsf?pageMode=MODE_SELECT_MANY_POPUP','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
				 }			
			function   showUnitPopup()
			{
				var screen_width = 1024;
				var screen_height = 450;
				window.open('UnitSearch.jsf?pageMode=MODE_SELECT_MANY_POPUP','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');


			}
						
	function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
</script>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

			<style>
.rich-calendar-input {
	width: 150px;
}
</style>

		</head>

		<!-- Header -->

		<body class="BODY_STYLE">
			<div class="containerDiv">

				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<c:choose>
						<c:when test="${!pages$TenderManagement.viewModePopUp}">
							<tr>
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>

					<tr width="100%">
						<c:choose>
							<c:when test="${!pages$TenderManagement.viewModePopUp}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['tenderManagement.heading']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<td width="100%" height="99%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION" style="height: 465px;">

											<h:form id="TenderManagementForm"
												styleClass="TenderManagementForm"
												enctype="multipart/form-data">
												<h:inputHidden id="hdnTDPersonId"
													value="#{pages$Contractors.hdnTDPersonId}"></h:inputHidden>
												<h:inputHidden id="hdnTDPersonType"
													value="#{pages$Contractors.hdnTDPersonType}"></h:inputHidden>
												<h:inputHidden id="hdnTDPersonName"
													value="#{pages$Contractors.hdnTDPersonName}"></h:inputHidden>
												<h:inputHidden id="hdnTDCellNo"
													value="#{pages$Contractors.hdnTDCellNo}"></h:inputHidden>
												<h:inputHidden id="hdnTDIsCompany"
													value="#{pages$Contractors.hdnTDIsCompany}"></h:inputHidden>
												<h:inputHidden id="hdnTDTenderID"
													binding="#{pages$TenderManagement.hdnTenderId}"></h:inputHidden>
												<h:inputHidden id="hdnContractorId"
													value="#{pages$TenderManagement.hdnContractorId}"></h:inputHidden>

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 10px; margin-right: 10px;">
														<tr>
															<td>
																<h:outputText
																	value="#{pages$TenderManagement.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText
																	value="#{pages$TenderManagement.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
															</td>
														</tr>
													</table>
												</div>
												<c:choose>
													<c:when test="${pages$TenderManagement.viewModeApproval}">

														<div class="MARGIN" style="width: 95%">
															<div class="TAB_DETAIL_SECTION" style="width: 98%">



																<table cellpadding="1px" cellspacing="2px"
																	class="TAB_DETAIL_SECTION_INNER" width="100%">
																	<tr>
																		<td style="width: 20%">

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['tenderManagement.approveTender.actionDate']}:">
																			</h:outputLabel>

																		</td>
																		<td style="width: 30%">
																			<rich:calendar id="auctionDate"
																				value="#{pages$TenderManagement.actionDate}"
																				locale="#{pages$TenderManagement.locale}"
																				popup="true"
																				datePattern="#{pages$TenderManagement.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				cellWidth="24px" cellHeight="22px"
																				style="width:186px; height:16px" />
																		</td>

																		<td style="width: 20%">
																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['tenderManagement.approveTender.comments']}:"></h:outputLabel>
																			</h:panelGroup>
																		</td>
																		<td style="width: 30%">
																			<h:inputTextarea id="commentsText"
																				binding="#{pages$TenderManagement.commentsText}"></h:inputTextarea>
																		</td>
																	</tr>

																	<tr>
																	</tr>
																	<tr>
																	</tr>

																	<tr>
																		<td colspan="4" class="BUTTON_TD">
																			<pims:security
																				screen="Pims.ServicesMgmt.Tender.ApproveButton"
																				action="create">
																				<h:commandButton id="approveButton"
																					binding="#{pages$TenderManagement.approveButton}"
																					styleClass="BUTTON"
																					value="#{msg['tenderManagement.approveTender.approve']}"
																					action="#{pages$TenderManagement.approveButton_action}">
																				</h:commandButton>
																			</pims:security>
																			<pims:security
																				screen="Pims.ServicesMgmt.Tender.RejectButton"
																				action="create">

																				<h:commandButton id="rejectButton"
																					binding="#{pages$TenderManagement.rejectButton}"
																					styleClass="BUTTON"
																					value="#{msg['tenderManagement.approveTender.reject']}"
																					action="#{pages$TenderManagement.rejectButton_action}">
																				</h:commandButton>
																			</pims:security>
																		</td>
																	</tr>
																</table>
															</div>
														</div>
													</c:when>
												</c:choose>
												<div class="MARGIN" style="width: 98%">


													<t:div styleClass="TAB_PANEL_INNER">

														<rich:tabPanel style="width:98%;height:235px;"
															headerSpacing="0"
															binding="#{pages$TenderManagement.tabPanel}">
															<rich:tab id="tenderDetailsTab"
																label="#{msg['commons.tab.tenderDetails']}">
																<t:div id="divTenderDetail">
																	<t:panelGrid columns="4" cellpadding="1"
																		style="width: 100%;" cellspacing="5">

																		<h:outputLabel
																			value="#{msg['tenderManagement.tenderNumber']}:"></h:outputLabel>
																		<t:panelGroup>

																			<h:inputText id="tenderNumber" styleClass="READONLY"
																				readonly="true"
																				binding="#{pages$TenderManagement.tenderNumber}" />
																		</t:panelGroup>

																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel
																				value="#{msg['tenderManagement.tenderStatus']}:"></h:outputLabel>
																		</h:panelGroup>

																		<h:inputText id="tenderStatus" readonly="true"
																			styleClass="READONLY"
																			binding="#{pages$TenderManagement.tenderStatue}" />

																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel value="#{msg['tender.type']}:"></h:outputLabel>
																		</h:panelGroup>
																		<h:inputText id="servicetenderType" readonly="true"
																			styleClass="READONLY"
																			binding="#{pages$TenderManagement.serviceTenderType}" />

																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel
																				value="#{msg['serviceContract.serviceType']}:"></h:outputLabel>
																		</h:panelGroup>

																		<h:selectOneMenu id="tenderType"
																			disabled="#{!pages$TenderManagement.canDelete}"
																			binding="#{pages$TenderManagement.tenderType}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																			<f:selectItems
																				value="#{pages$ApplicationBean.tenderTypeList}" />

																		</h:selectOneMenu>

																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel
																				value="#{msg['tenderManagement.startDate']}:"></h:outputLabel>
																		</h:panelGroup>
																		<rich:calendar id="startDate" rendered="true"
																			disabled="#{!pages$TenderManagement.canDelete}"
																			value="#{pages$TenderManagement.startDate}"
																			datePattern="#{pages$TenderManagement.dateFormat}"
																			locale="#{pages$TenderManagement.locale}"
																			popup="true" showApplyButton="false"
																			enableManualInput="false"
																			inputStyle="width:170px; height:14px;" />
																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel
																				value="#{msg['tenderManagement.endDate']}:"></h:outputLabel>
																		</h:panelGroup>

																		<rich:calendar id="endDate" rendered="true"
																			disabled="#{!pages$TenderManagement.canDelete}"
																			value="#{pages$TenderManagement.endDate}"
																			datePattern="#{pages$TenderManagement.dateFormat}"
																			locale="#{pages$TenderManagement.locale}"
																			popup="true" showApplyButton="false"
																			enableManualInput="false"
																			inputStyle="width:170px; height:14px;" />

																		<h:outputLabel
																			value="#{msg['tenderManagement.tenderDescription']}:"></h:outputLabel>
																		<h:inputText id="tenderDescription"
																			disabled="#{!pages$TenderManagement.canDelete}"
																			binding="#{pages$TenderManagement.tenderDescription}" />

																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel
																				value="#{msg['maitenanceContract.lbl.estimatedCost']}:"></h:outputLabel>
																		</h:panelGroup>
																		<h:inputText id="expectedCost"
																			disabled="#{!pages$TenderManagement.canDelete}"
																			binding="#{pages$TenderManagement.expectedCost}" />
																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel
																				value="#{msg['constructionTender.search.docSellingPrice']}:"></h:outputLabel>
																		</h:panelGroup>

																		<h:inputText id="trenderPrice"
																			disabled="#{!pages$TenderManagement.canDelete}"
																			binding="#{pages$TenderManagement.tenderPrice}" />
																	</t:panelGrid>
																</t:div>
															</rich:tab>
															<rich:tab id="propertyDetailsTab"
																label="#{msg['commons.propertyDetailsTabHeading']}">
																<t:panelGrid id="propertyDetailsButtonGrid"
																	width="99.8%" columnClasses="BUTTON_TD" columns="1">
																	<t:panelGroup>
																		<h:commandButton styleClass="BUTTON"
																			style="width:90px;"
																			binding="#{pages$TenderManagement.addUnit}"
																			value="#{msg['tenderManagement.AddUnit']}"
																			action="#{pages$TenderManagement.showUnitsPopup}" />
																		<h:commandButton styleClass="BUTTON"
																			style="width:100px;"
																			binding="#{pages$TenderManagement.addProperty}"
																			value="#{msg['tenderManagement.AddProperty']}"
																			action="#{pages$TenderManagement.showPropertyPopup}" />
																	</t:panelGroup>
																</t:panelGrid>
																<t:div style="text-align:center;">
																	<t:div styleClass="contentDiv" style="width:98.2%">
																		<t:dataTable id="test8" rows="5" width="100%"
																			value="#{pages$TenderManagement.propertyDataList}"
																			binding="#{pages$TenderManagement.propertyGrid}"
																			preserveDataModel="true" preserveSort="true"
																			var="dataItemProperty" rowClasses="row1,row2"
																			rules="all" renderedIfEmpty="true">

																			<t:column id="dataItemProperty2" sortable="true">
																				<f:facet name="header">
																					<t:outputText
																						value="#{msg['property.propertyNumber']}" />
																				</f:facet>
																				<t:outputText styleClass="A_LEFT" id="a1"
																					value="#{dataItemProperty.propertyNumber}"
																					rendered="#{dataItemProperty.tenderPropertyIsDeleted == 0}" />
																			</t:column>
																			<t:column id="dataItemProperty4" sortable="true">
																				<f:facet name="header">
																					<t:outputText
																						value="#{msg['property.commercialName']}" />
																				</f:facet>
																				<t:outputText styleClass="A_LEFT" title="" id="a2"
																					value="#{dataItemProperty.commercialName}"
																					rendered="#{dataItemProperty.tenderPropertyIsDeleted == 0}" />
																			</t:column>
																			<t:column id="dataItemProperty5" sortable="true">
																				<f:facet name="header">
																					<t:outputText value="#{msg['property.type']}" />
																				</f:facet>
																				<t:outputText styleClass="A_LEFT" title="" id="a3"
																					value="#{dataItemProperty.propertyTypeEn}"
																					rendered="#{dataItemProperty.tenderPropertyIsDeleted == 0}" />
																			</t:column>
																			<t:column id="dataItemProperty6" sortable="true">
																				<f:facet name="header">
																					<t:outputText value="#{msg['property.unitNumber']}" />
																				</f:facet>
																				<t:outputText styleClass="A_LEFT" title="" id="a34"
																					value="#{dataItemProperty.unitRefNo}"
																					rendered="#{dataItemProperty.tenderPropertyIsDeleted == 0}" />
																			</t:column>
																			<t:column id="dataItemProperty7" sortable="true">
																				<f:facet name="header">
																					<t:outputText value="#{msg['property.emirates']}" />
																				</f:facet>
																				<t:outputText styleClass="A_LEFT" title="" id="a14"
																					value="#{dataItemProperty.emirateNameEn}"
																					rendered="#{dataItemProperty.tenderPropertyIsDeleted == 0}" />
																			</t:column>
																			<t:column id="dataItemProperty8" sortable="true">
																				<f:facet name="header">
																					<t:outputText value="#{msg['property.area']}" />
																				</f:facet>
																				<t:outputText styleClass="A_LEFT" title="" id="a15"
																					value="#{dataItemProperty.stateEn}"
																					rendered="#{dataItemProperty.tenderPropertyIsDeleted == 0}" />
																			</t:column>
																			<t:column
																				rendered="#{pages$TenderManagement.canDeleteTenderProperty}">
																				<f:facet name="header">
																					<t:outputText id="lbl_delete_Property"
																						value="#{msg['commons.delete']}" />
																				</f:facet>
																				<h:commandLink id="deleteIconProperty"
																					rendered="#{dataItemProperty.tenderPropertyIsDeleted == 0}"
																					action="#{pages$TenderManagement.cmdPropertyDelete_Click}">
																					<h:graphicImage id="delete_IconProperty"
																						title="#{msg['commons.delete']}"
																						url="../resources/images/delete_icon.png" />
																				</h:commandLink>
																			</t:column>
																		</t:dataTable>
																	</t:div>
																	<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																		style="width:99.2%;">
																		<t:panelGrid cellpadding="0" cellspacing="0"
																			width="100%" columns="2"
																			columnClasses="RECORD_NUM_TD">
																			<t:div styleClass="RECORD_NUM_BG">
																				<t:panelGrid cellpadding="0" cellspacing="0"
																					columns="3" columnClasses="RECORD_NUM_TD">
																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />
																					<h:outputText value=" : "
																						styleClass="RECORD_NUM_TD" />
																					<h:outputText
																						value="#{pages$TenderManagement.propertyRecordSize}"
																						styleClass="RECORD_NUM_TD" />
																				</t:panelGrid>
																			</t:div>
																			<t:dataScroller id="extendscroller" for="test8"
																				paginator="true" fastStep="1" paginatorMaxPages="2"
																				immediate="false" paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true"
																				paginatorTableStyle="grid_paginator"
																				layout="singleTable"
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																				paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																				pageIndexVar="pageNumber" styleClass="SCH_SCROLLER">

																				<f:facet name="first">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblFinquiry"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastrewind">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}"
																						id="lblFRinquiry"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastforward">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}"
																						id="lblFFinquiry"></t:graphicImage>
																				</f:facet>
																				<f:facet name="last">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblLinquiry"></t:graphicImage>
																				</f:facet>
																				<t:div styleClass="PAGE_NUM_BG" rendered="true">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</t:div>
																			</t:dataScroller>

																		</t:panelGrid>
																	</t:div>

																</t:div>
															</rich:tab>
															<rich:tab id="contractorsTab"
																label="#{msg['commons.tab.contractors']}">

																<t:panelGrid id="contractDetailsButtonGrid"
																	width="99.5	%" columnClasses="BUTTON_TD" columns="1">
																	<h:commandButton
																		value="#{msg['commons.addContractor']}"
																		styleClass="BUTTON" style="width:95px;"
																		binding="#{pages$TenderManagement.addContractor}"
																		onclick="javascript:showSearchContractorPopUp();"></h:commandButton>
																</t:panelGrid>
																<%@  include file="Contractors.jsp"%>
															</rich:tab>
															<rich:tab id="scopeOfWorkTab"
																label="#{msg['commons.tab.scopeOfWork']}">
																<%@  include file="ScopeOfWork.jsp"%>
															</rich:tab>
															<rich:tab
																binding="#{pages$TenderManagement.inquiryAppTab}"
																id="inquiryAppTab"
																label="#{msg['constructionTender.inquiryApp']}">
																<t:div style="width:100%;">
																	<t:panelGrid columns="1" cellpadding="0"
																		cellspacing="0" width="100%">
																		<t:div styleClass="contentDiv" style="width:99%;">
																			<t:dataTable id="dtInquiry"
																				value="#{pages$TenderManagement.tenderInquiryList}"
																				binding="#{pages$TenderManagement.inquiryDataTable}"
																				rows="5" preserveDataModel="false"
																				preserveSort="false" var="TenderInquiry"
																				rowClasses="row1,row2" rules="all"
																				renderedIfEmpty="true" width="100%">

																				<t:column id="colInquiryNumber" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderInquirySearch.inquiryNumber']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.requestNumber}" />
																				</t:column>

																				<t:column id="colInquiryDate" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderInquirySearch.inquiryDate']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.requestDate}">
																						<f:convertDateTime
																							pattern="#{pages$TenderManagement.dateFormat}"
																							timeZone="#{pages$TenderManagement.timeZone}" />
																					</t:outputText>
																				</t:column>

																				<t:column id="colSourceType" rendered="false"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderInquirySearch.sourceType']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.requestSourceEn}"
																						rendered="#{pages$TenderManagement.isEnglishLocale}" />
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.requestSourceAr}"
																						rendered="#{!pages$TenderManagement.isEnglishLocale}" />
																				</t:column>

																				<t:column id="colContractorNumber" rendered="false"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['ContractorSearch.contractorNumber']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.contractorNumber}" />
																				</t:column>

																				<t:column id="colContractorName" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['ContractorSearch.contractorName']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.contractorNameEn}"
																						rendered="#{pages$TenderManagement.isEnglishLocale}" />
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.contractorNameAr}"
																						rendered="#{!pages$TenderManagement.isEnglishLocale}" />
																				</t:column>
																				<t:column id="colTenderNumber" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderInquirySearch.TenderNo']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.tenderNumber}" />
																				</t:column>

																				<t:column id="colTenderStatus" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderInquirySearch.inquiryStatus']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.statusEn}"
																						rendered="#{pages$TenderManagement.isEnglishLocale}" />
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.statusAr}"
																						rendered="#{!pages$TenderManagement.isEnglishLocale}" />
																				</t:column>
																			</t:dataTable>
																		</t:div>
																		<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																			style="width:100%;">
																			<t:panelGrid columns="2" border="0" width="100%"
																				cellpadding="1" cellspacing="1">
																				<t:panelGroup
																					styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																					<t:div styleClass="JUG_NUM_REC_ATT">
																						<h:outputText value="#{msg['commons.records']}" />
																						<h:outputText value=" : " />
																						<h:outputText
																							value="#{pages$TenderManagement.inquiryAppRecordSize}" />
																					</t:div>
																				</t:panelGroup>
																				<t:panelGroup>
																					<t:dataScroller id="Inquiryscroller"
																						for="dtInquiry" paginator="true" fastStep="1"
																						paginatorMaxPages="#{pages$TenderManagement.paginatorMaxPages}"
																						immediate="false" paginatorTableClass="paginator"
																						renderFacetsIfSinglePage="true"
																						paginatorTableStyle="grid_paginator"
																						layout="singleTable"
																						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																						paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																						pageIndexVar="pageNumber"
																						styleClass="SCH_SCROLLER">

																						<f:facet name="first">
																							<t:graphicImage url="../#{path.scroller_first}"
																								id="lblFextend"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastrewind">
																							<t:graphicImage
																								url="../#{path.scroller_fastRewind}"
																								id="lblFRextend"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastforward">
																							<t:graphicImage
																								url="../#{path.scroller_fastForward}"
																								id="lblFFexte"></t:graphicImage>
																						</f:facet>
																						<f:facet name="last">
																							<t:graphicImage url="../#{path.scroller_last}"
																								id="lblLextend"></t:graphicImage>
																						</f:facet>
																						<t:div styleClass="PAGE_NUM_BG" rendered="true">
																							<h:outputText styleClass="PAGE_NUM"
																								value="#{msg['commons.page']}" />
																							<h:outputText styleClass="PAGE_NUM"
																								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																								value="#{requestScope.pageNumber}" />
																						</t:div>
																					</t:dataScroller>
																				</t:panelGroup>
																			</t:panelGrid>
																		</t:div>
																	</t:panelGrid>
																</t:div>
															</rich:tab>
															<rich:tab
																binding="#{pages$TenderManagement.extendsAppTab}"
																id="extendsAppTab"
																label="#{msg['constructionTender.extendApp']}">
																<t:div style="width:100%;">
																	<t:panelGrid columns="1" cellpadding="0"
																		cellspacing="0" width="100%">
																		<t:div styleClass="contentDiv" style="width:99%;">
																			<t:dataTable id="dt1"
																				value="#{pages$TenderManagement.extendedDataList}"
																				binding="#{pages$TenderManagement.extendedDataTable}"
																				rows="#{pages$TenderManagement.paginatorRows}"
																				preserveDataModel="false" preserveSort="false"
																				var="dataItem" rowClasses="row1,row2" rules="all"
																				renderedIfEmpty="true" width="100%">
																				<t:column id="requestNumberCol" width="15%"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['application.number.gridHeader']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.requestNumber}" />
																				</t:column>
																				<t:column id="requestDateCol" width="14%"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['application.date.gridHeader']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.requestDateStr}" />
																				</t:column>
																				<t:column id="contractorNameCol" width="20%"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['contractor.name.gridHeader']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.contractorName}" />
																				</t:column>
																				<t:column id="tenderNumberCol" width="12%"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tender.number.gridHeader']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.tenderNumber}" />
																				</t:column>
																				<t:column id="tenderTypeCol" width="12%"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tender.type.gridHeader']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.tenderType}" />
																				</t:column>
																				<t:column id="applicationStatusCol" width="12%"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['application.status.gridHeader']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.applicationStatus}" />
																				</t:column>
																			</t:dataTable>
																		</t:div>
																		<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																			style="width:100%;">
																			<t:panelGrid columns="2" border="0" width="100%"
																				cellpadding="1" cellspacing="1">
																				<t:panelGroup
																					styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																					<t:div styleClass="JUG_NUM_REC_ATT">
																						<h:outputText value="#{msg['commons.records']}" />
																						<h:outputText value=" : " />
																						<h:outputText
																							value="#{pages$TenderManagement.extendAppRecordSize}" />
																					</t:div>
																				</t:panelGroup>
																				<t:panelGroup>
																					<t:dataScroller id="extendscroller113" for="dt1"
																						paginator="true" fastStep="1"
																						paginatorMaxPages="#{pages$TenderManagement.paginatorMaxPages}"
																						immediate="false" paginatorTableClass="paginator"
																						renderFacetsIfSinglePage="true"
																						paginatorTableStyle="grid_paginator"
																						layout="singleTable"
																						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																						paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																						pageIndexVar="pageNumber"
																						styleClass="SCH_SCROLLER">

																						<f:facet name="first">
																							<t:graphicImage url="../#{path.scroller_first}"
																								id="lblFinquirydasd"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastrewind">
																							<t:graphicImage
																								url="../#{path.scroller_fastRewind}"
																								id="lblFRinquirydascxc"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastforward">
																							<t:graphicImage
																								url="../#{path.scroller_fastForward}"
																								id="lblFFinquirywewx"></t:graphicImage>
																						</f:facet>
																						<f:facet name="last">
																							<t:graphicImage url="../#{path.scroller_last}"
																								id="lblLinquirydsdscc"></t:graphicImage>
																						</f:facet>
																						<t:div styleClass="PAGE_NUM_BG" rendered="true">
																							<h:outputText styleClass="PAGE_NUM"
																								value="#{msg['commons.page']}" />
																							<h:outputText styleClass="PAGE_NUM"
																								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																								value="#{requestScope.pageNumber}" />
																						</t:div>
																					</t:dataScroller>
																				</t:panelGroup>
																			</t:panelGrid>
																		</t:div>
																	</t:panelGrid>
																</t:div>
															</rich:tab>
															<rich:tab id="attachmentTab"
																label="#{msg['commons.attachmentTabHeading']}">
																<%@  include file="attachment/attachment.jsp"%>
															</rich:tab>
															<rich:tab id="commentsTab"
																label="#{msg['commons.commentsTabHeading']}">
																<%@ include file="notes/notes.jsp"%>
															</rich:tab>
															<rich:tab id="activityLog"
																label="#{msg['commons.history']}"
																action="#{pages$TenderManagement.requestHistoryTabClick}"
																rendered="#{! empty pages$TenderManagement.tenderView && ! empty pages$TenderManagement.tenderView.tenderId }">
																<%@ include file="../pages/requestTasks.jsp"%>
															</rich:tab>
														</rich:tabPanel>
													</t:div>
												

													<table width="98%">
														<tr>
															<td colspan="4" class="BUTTON_TD">
																<pims:security
																	screen="Pims.ServicesMgmt.Tender.SaveButton"
																	action="create">
																	<h:commandButton id="saveButton"
																		binding="#{pages$TenderManagement.saveButton}"
																		styleClass="BUTTON"
																		value="#{msg['tenderManagement.save']}"
																		action="#{pages$TenderManagement.saveAction}">
																	</h:commandButton>
																</pims:security>
																<pims:security
																	screen="Pims.ServicesMgmt.Tender.SendButton"
																	action="create">
																	<h:commandButton id="sendButton"
																		binding="#{pages$TenderManagement.sendButton}"
																		styleClass="BUTTON"
																		value="#{msg['tenderManagement.send']}"
																		action="#{pages$TenderManagement.submitAction}">
																	</h:commandButton>
																</pims:security>
																<h:commandButton id="undoWinnerMarked"
																	binding="#{pages$TenderManagement.undoWinnerMarked}"
																	styleClass="BUTTON" style="width:120px;"
																	action="#{pages$TenderManagement.undoWinnerMarkedAction}"
																	value="#{msg['tenderProposal.DeselectWinners']}"
																	onclick="return confirm('#{msg['tenderProposal.UnmarkAllConfirmation']}');">
																</h:commandButton>
																<h:commandButton id="cancelButton" styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	rendered="#{!pages$TenderManagement.viewModePopUp}"
																	action="#{pages$TenderManagement.cancel}">
																</h:commandButton>
																<h:commandButton id="cancelButton2" styleClass="BUTTON"
																	type="button"
																	rendered="#{pages$TenderManagement.viewModePopUp}"
																	value="#{msg['commons.cancel']}"
																	onclick="closeWindow()">
																</h:commandButton>
															</td>
														</tr>
													</table>
												</div>
										</div>
										</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<c:choose>
									<c:when test="${!pages$TenderManagement.viewModePopUp}">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</c:when>
								</c:choose>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>
