<%-- 
  - Author: Anil Verani
  - Date: 10/07/2009
  - Copyright Notice:
  - @(#)
  - Description: Used for Adding, Updating and Viewing Recommendation 
  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

	function closeWindow() 
	{
		window.close();
	}
	
	function callOpenerFn()
	{
		window.opener.receiveRecommendation();
	}
	
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>					
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['recommendation.manage.heading']}" styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" >

										<h:form id="frm" style="width:97%" enctype="multipart/form-data">
											
											<table border="0" class="layoutTable">
												<tr>

													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$recommendationManage.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$recommendationManage.successMessages}" />
													</td>
												</tr>
											</table>

											<div class="MARGIN" style="width: 98%">
												
												<table cellpadding="0" cellspacing="0" width="100%">
												
													<tr>
														<td width="5%">&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td width="5%">&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td width="5%">&nbsp;</td>
													</tr>
												
													<tr>
														<td width="5%">&nbsp;</td>
														<td>
															<h:outputLabel styleClass="LABEL" value="#{msg['study.manage.tab.recommendations.date']}:" />
														</td>
														<td>
															<rich:calendar value="#{pages$recommendationManage.recommendationView.recommendationDate}"
																				   inputStyle="width: 170px; height: 14px"
											                        			   locale="#{pages$recommendationManage.locale}" 
											                        			   popup="true" 
											                        			   datePattern="#{pages$recommendationManage.dateFormat}" 
											                        			   showApplyButton="false" 
											                        			   enableManualInput="false" />
														</td>
														<td width="5%">&nbsp;</td>
														<td>
															<h:outputLabel styleClass="LABEL" value="#{msg['study.manage.tab.recommendations.details']}:" />
														</td>
														<td>
															<h:inputTextarea value="#{pages$recommendationManage.recommendationView.remark}" styleClass="TEXTAREA"/>
														</td>
														<td width="5%">&nbsp;</td>
													</tr>
													
													<tr>
														<td width="5%">&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td width="5%">&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td width="5%">&nbsp;</td>
													</tr>
												</table>
												
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
													<td width="5%">&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td width="5%">&nbsp;</td>
														<td>&nbsp;</td>
														<td colspan="5" class="BUTTON_TD">
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$recommendationManage.onSave}">
															</h:commandButton>															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	onclick="closeWindow();">
															</h:commandButton>																														
														</td>
														<td width="9%">&nbsp;</td>
													</tr>
												</table>
											</div>								
								
									    </h:form>
									</div>
								

									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>				
			</table>
           </div>
		</body>
	</html>
</f:view>
