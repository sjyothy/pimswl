<%-- 
  - Author: Anil Verani
  - Date: 06/04/2010  
  - Description: Used for Adding, Updating and Viewing Orders 
--%>
  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg" />
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr>
					<td class="divLeftMenuWithBody" width="17%">					
						<jsp:include page="leftmenu.jsp" />
					</td>
					
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['assetSearch.assetSubSector']}" styleClass="HEADER_FONT" />									
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" >

										<h:form id="frm" style="width:97%" enctype="multipart/form-data">
											<table border="0" class="layoutTable">
												<tr>
													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$subSectorManage.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$subSectorManage.successMessages}" />
													</td>
												</tr>
											</table>
											
											<div class="MARGIN" style="width: 98%">
												
												<table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
														<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
										        </table>
												
												<rich:tabPanel>
												
														<!-- Sub Sector - Start -->
														<rich:tab label="#{msg['assetSearch.assetSubSector']}">
															<t:div style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid id="pnlGrdCntrls" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4">																																										
																	
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['subSector.name.en']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText readonly="#{pages$subSectorManage.isViewMode}" styleClass="#{pages$subSectorManage.isViewMode ? 'READONLY' : ''}" value="#{pages$subSectorManage.subSectorView.sectorNameEn}" style="width: 186px;" maxlength="50" />
																	
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['subSector.name.ar']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText readonly="#{pages$subSectorManage.isViewMode}" styleClass="#{pages$subSectorManage.isViewMode ? 'READONLY' : ''}" value="#{pages$subSectorManage.subSectorView.sectorNameAr}" style="width: 186px;" maxlength="50" />
																	
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['paymentConfiguration.descriptionEn']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputTextarea readonly="#{pages$subSectorManage.isViewMode}" styleClass="#{pages$subSectorManage.isViewMode ? 'READONLY' : ''}" value="#{pages$subSectorManage.subSectorView.descriptionEn}" style="width: 186px;" />
																	
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['paymentConfiguration.descriptionAr']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputTextarea readonly="#{pages$subSectorManage.isViewMode}" styleClass="#{pages$subSectorManage.isViewMode ? 'READONLY' : ''}" value="#{pages$subSectorManage.subSectorView.descriptionAr}" style="width: 186px;" />
																	
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['assetSearch.assetSector']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:selectOneMenu readonly="#{pages$subSectorManage.isViewMode}" styleClass="#{pages$subSectorManage.isViewMode ? 'READONLY' : ''}" value="#{pages$subSectorManage.subSectorView.parentSectorId}" style="width: 192px;">
																		<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
																		<f:selectItems value="#{pages$ApplicationBean.sectorList}" />
																	</h:selectOneMenu>
																	
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Sub Sector Tab - End -->
																														
												</rich:tabPanel>
												
												<table cellpadding="0" cellspacing="0"  style="width:100%;">
													<tr>
														<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
														<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>
												
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td colspan="6" class="BUTTON_TD">
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$subSectorManage.onSave}"
																	rendered="#{pages$subSectorManage.isAddMode || pages$subSectorManage.isEditMode}">																	
															</h:commandButton>
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	action="#{pages$subSectorManage.onCancel}">
															</h:commandButton>
															
														</td>
													</tr>
												</table>
											</div>								
								
									    </h:form>
									</div>
								

									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
           </div>
		</body>
	</html>
</f:view>
