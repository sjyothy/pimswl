<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript"> 
	
	    function checkBoxClicked()
		{
			document.getElementById("cancelContractForm:checkLink").onclick();
		}
	    function onEvacFinesExempted()
	    {
		 window.open('exemptionReason.jsf?entityId=' + $F('requestId') + '&noteowner=EXEMPTION_REASON', 'Exemption', 'width=500,height=400,toolbar=0,resizable=0,status=0,location=0');
	    }
		function receivePayments(){
			var screen_width = 1024;
			var screen_height = 450;
			var popup_width = screen_width-100;
			var popup_height = screen_height;
			var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
			      
			var popup = window.open('receivePayment.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
			popup.focus();
		}

		function onPaymentButton(){
			window.open('receivePayment.jsf', 'ReceivePayment', 'toolbar=0,resizable=0,status=0,location=0');
		}
		
		function populateContract(contractNumber,contractId,tenantId){
	        document.getElementById("cancelContractForm:hdnContractId").value=contractNumber;
		    document.getElementById("cancelContractForm:hdnContactNumber").value=contractNumber;
		    document.getElementById("cancelContractForm:hdnTenantId").value=contractId;
     	}
     
     	function showContractPopup(clStatus){
		   var screen_width = screen.width;
		   var screen_height = screen.height;
		   window.open('ContractListPopUp.jsf?clStatus='+clStatus,'_blank','width='+(screen_width-10)+',height='+(screen_height-140)+',left=0,top=40,scrollbars=yes,status=yes');
		}    
		
		function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany){
		    document.getElementById("cancelContractForm:hdnPersonId").value=personId;
		    document.getElementById("cancelContractForm:hdnCellNo").value=cellNumber;
		    document.getElementById("cancelContractForm:hdnPersonName").value=personName;
		    document.getElementById("cancelContractForm:hdnPersonType").value=hdnPersonType;
		    document.getElementById("cancelContractForm:hdnIsCompany").value=isCompany;
            document.forms[0].submit();
		}	
	

		function showPersonPopup(){
			   var screen_width = 1024;
			   var screen_height = 470;
			   window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
			     
		}
		
		function	openPopUp()
		{
		
		    var width = 300;
            var height = 300;
            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + ",menubar=yes,toolbar=yes,scrollbars=yes,resizable=yes,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

            var child1 = window.open("about:blank", "subWind", windowFeatures);

		      child1.focus();
		}
		function calculateEvacFines()
        {
         document.getElementById("cancelContractForm:lnkCalcEvacFines").onclick();          
        }
        

</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache" />
			<meta http-equiv="cache-control" content="no-cache" />
			<meta http-equiv="expires" content="0" />
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">

			<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
			<c:choose>
				<c:when test="${!pages$CancelContract.isPagePopUp}">
					<div class="containerDiv">
						<h:inputHidden id="requestId"
							value="#{pages$CancelContract.requestId}" />
				</c:when>
			</c:choose>


			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<c:choose>
					<c:when test="${!pages$CancelContract.isPagePopUp}">
						<tr>
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>
					</c:when>
				</c:choose>


				<tr>
					<c:choose>
						<c:when test="${!pages$CancelContract.isPagePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>

					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['cancelContract.title']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="width: 100%;">
										<h:form id="cancelContractForm" enctype="multipart/form-data"
											style="WIDTH: 97.6%;">
											<h:inputHidden id="hdnPersonId"
												value="#{pages$CancelContract.hdnPersonId}"></h:inputHidden>
											<h:inputHidden id="hdnPersonType"
												value="#{pages$CancelContract.hdnPersonType}"></h:inputHidden>
											<h:inputHidden id="hdnPersonName"
												value="#{pages$CancelContract.hdnPersonName}"></h:inputHidden>
											<h:inputHidden id="hdnCellNo"
												value="#{pages$CancelContract.hdnCellNo}"></h:inputHidden>
											<h:inputHidden id="hdnIsCompany"
												value="#{pages$CancelContract.hdnIsCompany}"></h:inputHidden>


											<div style="height: 45px">
												<table border="0" class="layoutTable"
													style="margin-left: 15px; margin-right: 15px;">
													<tr>
														<td>
															<h:outputText
																value="#{pages$CancelContract.errorMessages}"
																styleClass="ERROR_FONT" style="padding:10px;" />
															<h:outputText
																value="#{pages$CancelContract.successMessages}"
																styleClass="INFO_FONT" style="padding:10px;" />
														</td>
													</tr>
												</table>
											</div>
											<div>
												<div class="AUC_DET_PAD">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="font-size: 0px;">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="font-size: 0px;">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="font-size: 0px;">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="TAB_PANEL_INNER">
														<rich:tabPanel binding="#{pages$CancelContract.tabPanel}"
															id="cancelContractTabPanel" switchType="server"
															style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%">
															<!-- Application Details Tab - Start -->
															<rich:tab id="applicationTab"
																label="#{msg['contract.tabHeading.ApplicationDetails']}">
																<%@ include file="ApplicationDetails.jsp"%>
															</rich:tab>
															<!-- Application Details Tab - End -->
															<!-- Contract Details Tab - Start -->
															<rich:tab id="contractDetailsTab"
																label="#{msg['contract.Contract.Details']}">
																<t:div id="divContract" styleClass="TAB_DETAIL_SECTION">
																	<t:panelGrid cellpadding="1" width="100%"
																		cellspacing="1" styleClass="TAB_DETAIL_SECTION_INNER"
																		columns="4">
																		<t:panelGroup>
																			<t:panelGrid columns="2" cellpadding="1"
																				cellspacing="1">
																				<h:outputText styleClass="LABEL"
																					value="#{msg['contract.contractNumber']}:" />
																				<t:panelGroup>
																					<h:inputText styleClass="READONLY"
																						id="txtContractNo"
																						value="#{pages$CancelContract.contractNoText}"
																						style="width: 186px; height: 18px" readonly="true"></h:inputText>
																					<h:commandLink
																						action="#{pages$CancelContract.btnContract_Click}">
																						<h:graphicImage style="padding-left:4px;"
																							title="#{msg['commons.view']}"
																							url="../resources/images/app_icons/Lease-contract.png" />
																					</h:commandLink>
																				</t:panelGroup>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.tenantName']}:" />
																				<t:panelGroup>
																					<h:inputText styleClass="READONLY"
																						id="txtTenantName" readonly="true"
																						value="#{pages$CancelContract.tenantNameText}"
																						style="width: 186px; height: 18px"></h:inputText>
																					<h:commandLink
																						action="#{pages$CancelContract.btnTenant_Click}">
																						<h:graphicImage style="padding-left:4px;"
																							title="#{msg['commons.view']}"
																							url="../resources/images/app_icons/Tenant.png" />
																					</h:commandLink>
																				</t:panelGroup>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.startDate']}:" />
																				<h:inputText styleClass="READONLY"
																					id="txtContractStartDate" readonly="true"
																					value="#{pages$CancelContract.contractStartDateText}"
																					style="width: 186px; height: 18px"></h:inputText>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['cancelContract.tab.unit.propertyname']}:" />
																				<h:inputText styleClass="READONLY"
																					id="txtPropertyName" readonly="true"
																					value="#{pages$CancelContract.txtpropertyName}"
																					style="width: 186px; height: 18px"></h:inputText>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.unitNumber']}:" />
																				<h:inputText styleClass="READONLY"
																					id="txtUnitRefNum" readonly="true"
																					value="#{pages$CancelContract.txtunitRefNum}"
																					style="width: 186px; height: 18px"></h:inputText>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.rentValue']}:" />
																				<h:inputText styleClass="READONLY"
																					id="txtUnitRentVal" readonly="true"
																					value="#{pages$CancelContract.totalContractValText}"
																					style="width: 186px; height: 18px"></h:inputText>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.label.replaceUnit']}:" />
																				<h:selectBooleanCheckbox styleClass="READONLY"
																					disabled="#{ (pages$CancelContract.pageMode != 'EVACUATION_NEW' &&	pages$CancelContract.pageMode != 'EVACUATION_REQ' && pages$CancelContract.pageMode != 'MODE_EVC_REQ' && pages$CancelContract.pageMode != 'MODE_APPROVAL_REQ') || (pages$CancelContract.fromRequestSearch &&  pages$CancelContract.pageMode == 'EVACUATION_NEW')}"
																					onclick="checkBoxClicked();"
																					value="#{pages$CancelContract.exemptFine}">
																				</h:selectBooleanCheckbox>
																				<t:commandLink id="checkLink"
																					action="#{pages$CancelContract.onCheckBoxClick}" />
																			</t:panelGrid>
																		</t:panelGroup>

																		<t:div>
																			<t:panelGroup>
																				<t:panelGrid columns="2" cellpadding="1"
																					cellspacing="1">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['contract.contractType']}:" />
																					<h:inputText styleClass="READONLY"
																						id="txtContractType" readonly="true"
																						value="#{pages$CancelContract.contractTypeText}"
																						style="width: 186px; height: 18px"></h:inputText>

																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['transferContract.oldTenant.Number']}:" />
																					<h:inputText styleClass="READONLY"
																						id="txtOldTenantNum"
																						style="width: 186px; height: 18px" readonly="true"
																						value="#{pages$CancelContract.tenantType}"></h:inputText>

																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['contract.endDate']}:" />
																					<h:inputText styleClass="READONLY"
																						id="txtContractEndDate" readonly="true"
																						value="#{pages$CancelContract.contractEndDateText}"
																						style="width: 186px; height: 18px"></h:inputText>

																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['property.type']}:" />
																					<h:inputText styleClass="READONLY"
																						id="txtPropertyType" readonly="true"
																						value="#{pages$CancelContract.txtpropertyType}"
																						style="width: 186px; height: 18px"></h:inputText>

																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['commons.replaceCheque.unitType']}:" />
																					<h:inputText styleClass="READONLY" id="txtUnitType"
																						readonly="true"
																						value="#{pages$CancelContract.txtUnitType}"
																						style="width: 186px; height: 18px"></h:inputText>
																					<h:panelGroup>
																						<h:outputLabel styleClass="mandatory" value="*" />
																						<h:outputLabel styleClass="LABEL"
																							value="#{msg['settlement.label.evacdate']}:" />
																					</h:panelGroup>
																					<t:panelGrid cellpadding="0" cellspacing="0"
																						columns="2">
																						<h:panelGroup>

																							<rich:calendar id="evacdate"
																								inputStyle="width: 186px; height: 18px"
																								onchanged="javascript:calculateEvacFines();"
																								value="#{pages$CancelContract.evacDate}"
																								datePattern="#{pages$CancelContract.dateFormat}"
																								timeZone="#{pages$CancelContract.localTimeZone}"
																								disabled="#{(pages$CancelContract.fromRequestSearch &&  pages$CancelContract.pageMode == 'EVACUATION_NEW') || (pages$CancelContract.pageMode != 'EVACUATION_NEW' &&  
														pages$CancelContract.pageMode != 'EVACUATION_REQ' && pages$CancelContract.pageMode != 'MODE_EVC_REQ' && pages$CancelContract.pageMode != 'MODE_APPROVAL_REQ') }" />
																							<t:commandLink
																								actionListener="#{pages$CancelContract.onViewSettlementDetails}"
																								rendered="#{pages$CancelContract.showSettlementDetails && false}">
																								<h:graphicImage
																									title="#{msg['cancelContract.viewSettlementDetails']}"
																									url="../resources/images/app_icons/Payment-Management.png" />
																							</t:commandLink>
																							<a4j:commandLink id="lnkCalcEvacFines"
																								action="#{pages$CancelContract.onCalculateEvacFine}"
																								reRender="txtTotalEvacFines" />
																						</h:panelGroup>
																					</t:panelGrid>


																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['settlement.label.evacfinesApplied']}:"
																						rendered="#{ (pages$CancelContract.exempted == 0) &&  !( pages$CancelContract.pageMode == 'EVACUATION_NEW' || pages$CancelContract.pageMode == 'EVACUATION_REQ') }" />
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['settlement.label.evacfinesExempted']}:"
																						rendered="#{ (pages$CancelContract.exempted == 1) && !( pages$CancelContract.pageMode == 'EVACUATION_NEW' || pages$CancelContract.pageMode == 'EVACUATION_REQ') }" />
																					<t:panelGrid cellpadding="0" cellspacing="0"
																						columns="4">
																						<h:inputText styleClass="READONLY"
																							id="txtTotalEvacFines" readonly="true"
																							value="#{pages$CancelContract.txttotalEvacFines}"
																							style="width: 186px; height: 18px"
																							rendered="#{ !( pages$CancelContract.pageMode == 'EVACUATION_NEW' || pages$CancelContract.pageMode == 'EVACUATION_REQ') }"></h:inputText>
																						<t:panelGroup
																							rendered="#{ ( pages$CancelContract.pageMode == 'MODE_APPROVAL_REQ') }">
																							<h:commandLink
																								onclick="javascript:onEvacFinesExempted();">
																								<h:graphicImage
																									title="#{msg['tenderManagement.approveTender.comments']}"
																									url="../resources/images/app_icons/No-Objection-letter.png">
																								</h:graphicImage>
																							</h:commandLink>
																						</t:panelGroup>
																						<t:panelGroup
																							rendered="#{(pages$CancelContract.exempted == 0) && ( pages$CancelContract.pageMode == 'MODE_APPROVAL_REQ') }">
																							<h:commandLink
																								actionListener="#{pages$CancelContract.onEvacFinesExempted}">
																								<h:graphicImage
																									title="#{msg['settlement.label.exempt']}"
																									url="../resources/images/app_icons/No-Objection-letter.png">
																								</h:graphicImage>
																							</h:commandLink>
																						</t:panelGroup>
																						<t:panelGroup
																							rendered="#{ (pages$CancelContract.exempted == 1) && ( pages$CancelContract.pageMode == 'MODE_APPROVAL_REQ') }">
																							<h:commandLink
																								actionListener="#{pages$CancelContract.onEvacFinesApplied}">
																								<h:graphicImage
																									title="#{msg['settlement.label.apply']}"
																									url="../resources/images/app_icons/No-Objection-letter.png">
																								</h:graphicImage>
																							</h:commandLink>
																						</t:panelGroup>
																					</t:panelGrid>

																				</t:panelGrid>
																			</t:panelGroup>
																		</t:div>
																	</t:panelGrid>
																</t:div>
															</rich:tab>
															<!-- Contract Details Tab - End -->
															<!-- Inspection Tab - Start -->
															<rich:tab id="inspectionTab"
																label="#{msg['cancelContract.tab.inspection']}"
																rendered="#{pages$CancelContract.showInspection}">
																<t:div style="height:280px;overflow-y:scroll;">
																	<t:panelGrid columns="4"
																		columnClasses="GRID_C1, GRID_C2, GRID_C1, GRID_C2"
																		width="97%"
																		rendered="#{!pages$CancelContract.readOnly && pages$CancelContract.pageMode == 'INSPECTION_REQ'}">
																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="* " />
																			<h:outputLabel styleClass="TABLE_LABEL"
																				value="#{msg['cancelContract.inspection.date']}:" />
																		</h:panelGroup>
																		<rich:calendar id="violationdate"
																			value="#{pages$CancelContract.violationBean.date}"
																			datePattern="dd/MM/yyyy" />
																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="* " />
																			<h:outputLabel styleClass="TABLE_LABEL"
																				value="#{msg['cancelContract.inspection.category']}:" />
																		</h:panelGroup>
																		<h:selectOneMenu id="category"
																			value="#{pages$CancelContract.violationBean.category}"
																			style="width:144px;">
																			<f:selectItem
																				itemLabel="#{msg['commons.combo.PleaseSelect']}"
																				itemValue="" />
																			<f:selectItems
																				value="#{pages$ApplicationBean.violationCategoryList}" />
																			<a4j:support
																				action="#{pages$CancelContract.loadViolationTypesForCategory}"
																				event="onchange" reRender="type" />
																		</h:selectOneMenu>
																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="* " />

																			<h:outputLabel styleClass="TABLE_LABEL"
																				value="#{msg['cancelContract.inspection.type']}:" />
																		</h:panelGroup>
																		<h:selectOneMenu id="type"
																			value="#{pages$CancelContract.violationBean.type}"
																			style="width:290px;">
																			<f:selectItem
																				itemLabel="#{msg['commons.combo.PleaseSelect']}"
																				itemValue="" />
																			<f:selectItems
																				value="#{pages$CancelContract.violationTypes}" />
																		</h:selectOneMenu>
																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="* " />

																			<h:outputLabel styleClass="TABLE_LABEL"
																				value="#{msg['cancelContract.inspection.damageamount']}:" />
																		</h:panelGroup>
																		<h:inputText id="damageamount"
																			styleClass="A_RIGHT_NUM"
																			value="#{pages$CancelContract.violationBean.damageAmount}"
																			style="width:140px;">
																		</h:inputText>
																		<h:outputLabel styleClass="TABLE_LABEL"
																			value="#{msg['cancelContract.inspection.description']}:" />
																		<h:inputTextarea id="description" styleClass="A_LEFT"
																			value="#{pages$CancelContract.violationBean.description}"
																			style="width:140px;" rows="3" />
																		<t:div>&nbsp;</t:div>
																		<t:column style="vertical-align:bottom;">
																			<t:panelGrid cellpadding="0" cellspacing="0"
																				columns="1">
																				<h:commandButton id="addInspection"
																					styleClass="BUTTON"
																					actionListener="#{pages$CancelContract.onAddInspection}"
																					value="#{msg['commons.Add']}"
																					rendered="#{!pages$CancelContract.editViolationRequest}" />
																				<h:commandButton id="editInspection"
																					styleClass="BUTTON"
																					actionListener="#{pages$CancelContract.onUpdateInspection}"
																					value="#{msg['commons.edit']}"
																					rendered="#{pages$CancelContract.editViolationRequest}">
																					<f:param name="inspectionid"
																						value="#{pages$CancelContract.violationBean.id}" />
																				</h:commandButton>
																			</t:panelGrid>
																		</t:column>
																	</t:panelGrid>
																	<t:panelGrid cellpadding="0" cellspacing="0"
																		columns="1" rowClasses="C111,A_RIGHT"
																		style="width:97%;">
																		<t:div styleClass="contentDiv" style="width:97%;">
																			<t:dataTable var="inspectionItem"
																				value="#{pages$CancelContract.violationListBean}"
																				width="100%">
																				<t:column width="35%">
																					<f:facet name="header">
																						<h:outputLabel styleClass="TABLE_LABEL"
																							value="#{msg['cancelContract.inspection.description']}" />
																					</f:facet>
																					<t:outputText value="#{inspectionItem.description}"
																						styleClass="A_LEFT" />
																				</t:column>
																				<t:column width="20%">
																					<f:facet name="header">
																						<h:outputLabel styleClass="TABLE_LABEL"
																							value="#{msg['inspection.type']}" />
																					</f:facet>
																					<t:outputText value="#{inspectionItem.typeDesc}" />
																				</t:column>
																				<t:column width="15%">
																					<f:facet name="header">
																						<h:outputLabel styleClass="TABLE_LABEL"
																							value="#{msg['cancelContract.inspection.grid.category']}" />
																					</f:facet>
																					<t:outputText
																						value="#{inspectionItem.categoryDesc}" />
																				</t:column>
																				<t:column width="15%">
																					<f:facet name="header">
																						<h:outputLabel styleClass="TABLE_LABEL"
																							value="#{msg['renewContract.fine']}" />
																					</f:facet>
																					<t:outputText
																						value="#{inspectionItem.damageAmount}"
																						styleClass="A_RIGHT_NUM">
																						<f:convertNumber minFractionDigits="2"
																							maxFractionDigits="2" pattern="##,###,##0.00" />
																					</t:outputText>
																				</t:column>
																				<t:column width="15%">
																					<f:facet name="header">
																						<h:outputLabel styleClass="TABLE_LABEL"
																							value="#{msg['commons.status']}" />
																					</f:facet>
																					<t:outputText value="#{inspectionItem.status}" />
																				</t:column>
																				<t:column width="5%"
																					rendered="#{pages$CancelContract.pageMode == 'INSPECTION_REQ'}">
																					<f:facet name="header">
																						<h:outputLabel styleClass="TABLE_LABEL"
																							value="#{msg['commons.action']}" />
																					</f:facet>
																					<t:commandLink
																						actionListener="#{pages$CancelContract.onDeleteViolation}"
																						value="" title="#{msg['commons.delete']}"
																						onclick="if (!confirm('#{msg['settlement.message.deletefine']}')) return false;">
																						<f:param name="inspectionid"
																							value="#{inspectionItem.id}" />
																						<h:graphicImage id="deleteViolation"
																							title="#{msg['commons.delete']}"
																							url="../resources/images/delete.gif" />
																					</t:commandLink>
																					<t:commandLink
																						actionListener="#{pages$CancelContract.onEditViolation}"
																						value="" title="#{msg['commons.edit']}">
																						<f:param name="inspectionid"
																							value="#{inspectionItem.id}" />
																						<h:graphicImage id="editViolation"
																							title="#{msg['commons.edit']}"
																							url="../resources/images/edit-icon.gif" />
																					</t:commandLink>
																				</t:column>

																			</t:dataTable>
																		</t:div>
																	</t:panelGrid>
																</t:div>
															</rich:tab>
															<!-- Inspection Tab - End -->

															<!-- Follow Up Tab - Start -->
															<rich:tab id="cancelContractFollowUpTab"
																rendered="#{pages$CancelContract.showFollowUpTab}"
																label="#{msg['cancelContract.tab.followUp']}"
																action="#{pages$CancelContract.tabFollowUp_Click}">
																<%@ include file="cancelContractFollowUpTab.jsp"%>
															</rich:tab>
															<!-- Follow Up Tab  - End -->

															<!-- Attachment and Comments Tabs - Start -->
															<rich:tab id="attachmentTab"
																label="#{msg['commons.attachmentTabHeading']}">
																<%@  include file="attachment/attachment.jsp"%>
															</rich:tab>
															<rich:tab id="commentsTab"
																label="#{msg['commons.commentsTabHeading']}">
																<%@ include file="notes/notes.jsp"%>
															</rich:tab>
															<!-- Attachment and Comments Tabs - End -->
															<!-- Request History Tab - Start -->
															<rich:tab id="requestHistoryTab"
																rendered="#{pages$CancelContract.showRequestHistory}"
																label="#{msg['contract.requestHistory']}"
																action="#{pages$CancelContract.tabRequestHistory_Click}">
																<%@ include file="../pages/requestTasks.jsp"%>
															</rich:tab>
															<!-- Request History Tab - End -->
															<!-- Payments Tab - Start -->
															<rich:tab id="paymentsTab"
																label="#{msg['cancelContract.tab.payments']}"
																rendered="#{pages$CancelContract.showPayments}">
																<t:div style="height:280px;overflow-y:scroll;">
																	<t:div styleClass="A_RIGHT" style="padding:10px">
																		<h:commandButton id="addPaymentSchedules"
																			styleClass="BUTTON" style="width:110px;"
																			rendered="#{pages$CancelContract.showPayment && !pages$CancelContract.readOnly}"
																			action="#{pages$CancelContract.onAddPaymentsSchedule}"
																			value="#{msg['contract.AddpaymentSchedule']}" />
																	</t:div>
																	<t:div styleClass="contentDiv" style="width:95%">
																		<t:dataTable var="paymentItem"
																			value="#{pages$CancelContract.payments}"
																			binding="#{pages$CancelContract.dataTablePaymentSchedule}"
																			width="100%">
																			<t:column width="5%"
																				rendered="#{pages$CancelContract.showPayment && !pages$CancelContract.readOnly}">
																				<f:facet name="header">
																					<h:outputText value="#{msg['commons.select']}" />
																				</f:facet>
																				<h:selectBooleanCheckbox
																					value="#{paymentItem.selected}"
																					rendered="#{pages$CancelContract.showPayment && !pages$CancelContract.readOnly && paymentItem.paymentScheduleView.isReceived == 'N' &&
																					            paymentItem.paymentScheduleView.statusId != 36019}" />
																			</t:column>

																			<t:column>
																				<f:facet name="header">
																					<h:outputText
																						value="#{msg['paymentSchedule.paymentNumber']}" />
																				</f:facet>
																				<h:outputText styleClass="A_LEFT"
																					value="#{paymentItem.paymentScheduleView.paymentNumber}"
																					style="white-space=normal;" />
																			</t:column>

																			<t:column>
																				<f:facet name="header">
																					<h:outputText
																						value="#{msg['paymentSchedule.ReceiptNumber']}" />
																				</f:facet>
																				<h:outputText styleClass="A_LEFT"
																					value="#{paymentItem.paymentScheduleView.paymentReceiptNumber}"
																					style="white-space=normal;" />
																			</t:column>

																			<t:column>
																				<f:facet name="header">
																					<h:outputText
																						value="#{msg['cancelContract.payment.paymenttype']}" />
																				</f:facet>
																				<h:outputText value="#{paymentItem.paymentType}"
																					style="white-space=normal;" />
																			</t:column>
																			<t:column>
																				<f:facet name="header">
																					<h:outputText
																						value="#{msg['cancelContract.payment.paymentdate']}" />
																				</f:facet>
																				<h:outputText value="#{paymentItem.paymentDate}"
																					style="white-space=normal;" />
																			</t:column>
																			<t:column>
																				<f:facet name="header">
																					<h:outputText
																						value="#{msg['cancelContract.payment.paymentmethod']}" />
																				</f:facet>
																				<h:outputText value="#{paymentItem.paymentMethod}"
																					style="white-space=normal;" />
																			</t:column>
																			<t:column>
																				<f:facet name="header">
																					<h:outputText value="#{msg['commons.amount']}" />
																				</f:facet>
																				<h:outputText styleClass="A_RIGHT_NUM"
																					value="#{paymentItem.amount}"
																					style="white-space=normal;" />
																			</t:column>
																			<t:column>
																				<f:facet name="header">
																					<h:outputText value="#{msg['commons.status']}" />
																				</f:facet>
																				<h:outputText value="#{paymentItem.status}"
																					style="white-space=normal;" />
																			</t:column>
																			<t:column>
																				<f:facet name="header">
																					<h:outputText
																						value="#{msg['cancelContract.payment.Payee']}" />
																				</f:facet>
																				<h:outputText
																					value="#{paymentItem.paymentScheduleView.payeeAMAF ==0 ? msg['cancelContract.payment.PayeeTenant'] : msg['cancelContract.payment.PayeeAMAF']}"
																					style="white-space=normal;" />
																			</t:column>
																			<t:column>
																				<f:facet name="header">
																					<t:outputText value="#{msg['commons.action']}" />
																				</f:facet>
																				<h:commandLink
																					action="#{pages$CancelContract.btnEditPaymentSchedule_Click}">
																					<h:graphicImage id="editPayments"
																						title="#{msg['commons.edit']}"
																						url="../resources/images/edit-icon.gif"
																						rendered="#{paymentItem.paymentScheduleView.isReceived == 'N' && 
																		            (!pages$CancelContract.fromRequestSearch &&  
																		              pages$CancelContract.pageMode != 'EVACUATION_NEW')
																		           }" />
																				</h:commandLink>
																				<h:commandLink
																					action="#{pages$CancelContract.onSplitPayment}">
																					<h:graphicImage id="splitPayments"
																						title="#{msg['replaceChequeLabel.btnSplit']}"
																						url="../resources/images/app_icons/Add-fee.png"
																						rendered="#{paymentItem.paymentScheduleView.isReceived == 'N' && paymentItem.paymentScheduleView.statusId != 36019 &&
																				            ( !pages$CancelContract.fromRequestSearch &&  
																				               pages$CancelContract.pageMode != 'EVACUATION_NEW'
																				            )
																				           }" />
																				</h:commandLink>
																				<h:commandLink
																					action="#{pages$CancelContract.btnViewPaymentDetails_Click}">
																					<h:graphicImage id="viewPayments"
																						title="#{msg['commons.group.permissions.view']}"
																						url="../resources/images/detail-icon.gif" />
																				</h:commandLink>

																			</t:column>
																		</t:dataTable>
																	</t:div>
																</t:div>
															</rich:tab>
															<!-- Payments Tab - End -->
														</rich:tabPanel>

													</div>
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="font-size: 0px;">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td style="font-size: 0px;" width="100%">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																	class="TAB_PANEL_MID" style="width: 100%;" />
															</td>
															<td style="font-size: 0px;">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<t:div styleClass="BUTTON_TD" style="padding-top:10px;">

														<pims:security
															screen="Pims.Contract.Cancel.ProcessRequest"
															action="view">
															<h:commandButton id="submitReqButton"
																style="width:110px;"
																onclick="if (!confirm('#{msg['commons.messages.confirmSubmit']}')) return false"
																action="#{pages$CancelContract.onSubmitRequest}"
																value="#{msg['cancelContract.submit']}"
																binding="#{pages$CancelContract.submitBtn}"
																styleClass="BUTTON"
																rendered="#{pages$CancelContract.showSubmit && !pages$CancelContract.readOnly}" />
														</pims:security>

														<h:commandButton id="saveReqButton" style="width:110px;"
															action="#{pages$CancelContract.onSaveRequest}"
															value="#{msg['commons.saveButton']}" styleClass="BUTTON"
															rendered="#{pages$CancelContract.showSave && !pages$CancelContract.readOnly}" />

														<pims:security
															screen="Pims.Contract.Cancel.ApproveReject"
															action="view">
															<h:commandButton id="approveReqButton"
																style="width:110px;"
																action="#{pages$CancelContract.onApproveRequest}"
																value="#{msg['commons.approve']}" styleClass="BUTTON"
																rendered="#{pages$CancelContract.showApproveButton && !pages$CancelContract.readOnly}" />

															<h:commandButton id="rejectReqButton"
																style="width:110px;"
																action="#{pages$CancelContract.onRejectRequest}"
																value="#{msg['commons.reject']}" styleClass="BUTTON"
																rendered="#{pages$CancelContract.showRejectButton && !pages$CancelContract.readOnly}" />

															<h:commandButton id="sendInspectionButton"
																style="width:110px;"
																action="#{pages$CancelContract.onSendForInspection}"
																value="#{msg['commons.sendForInspection']}"
																styleClass="BUTTON"
																rendered="#{pages$CancelContract.showSendForInspectionButton && !pages$CancelContract.readOnly}" />

														</pims:security>

														<pims:security screen="Pims.Contract.Cancel.CancelRequest"
															action="view">
															<h:commandButton id="cancelButton" style="width:110px;"
																onclick="if (!confirm('#{msg['commons.messages.confirmCancelRequest']}')) return false"
																action="#{pages$CancelContract.onCancelRequest}"
																value="#{msg['common.cancelRequest']}"
																styleClass="BUTTON"
																rendered="#{pages$CancelContract.showCancelRequest && !pages$CancelContract.readOnly}" />
														</pims:security>
														<pims:security
															screen="Pims.Contract.Cancel.CompleteRequest"
															action="view">
															<h:commandButton id="sendForCollectPayment"
																style="width:110px;"
																onclick="if (!confirm('#{msg['commons.messages.confirmSendForCollectPayment']}')) return false"
																action="#{pages$CancelContract.onSendForCollectPayment}"
																value="#{msg['common.sendForCollectPayment']}"
																styleClass="BUTTON"
																rendered="#{pages$CancelContract.showSendForCollectPayment && !pages$CancelContract.readOnly}" />
															<h:commandButton id="sendForFollowUpButton"
																style="width:110px;"
																onclick="if (!confirm('#{msg['commons.messages.confirmSendForFollowUp']}')) return false"
																action="#{pages$CancelContract.onSendForFollowUp}"
																value="#{msg['common.sendForFollowUp']}"
																styleClass="BUTTON"
																rendered="#{pages$CancelContract.showSendForFollowUp && !pages$CancelContract.readOnly}" />
																
															<h:commandButton id="terminateEjari" style="width:110px;"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false"
																action="#{pages$CancelContract.onTerminateInEjari}"
																value="#{msg['cancelContract.btn.TerminateEjari']}"
																styleClass="BUTTON"
																binding="#{pages$CancelContract.terminateInEjariBtn}" />
																
															<h:commandButton id="completeButton" style="width:110px;"
																onclick="if (!confirm('#{msg['commons.messages.confirmCompleteRequest']}')) return false"
																action="#{pages$CancelContract.onCompleteRequest}"
																value="#{msg['common.completeRequest']}"
																styleClass="BUTTON"
																rendered="#{pages$CancelContract.showComplete && !pages$CancelContract.readOnly}" />
															<h:commandButton id="paymentButton" style="width:110px;"
																actionListener="#{pages$CancelContract.onPayment}"
																value="#{msg['cancelContract.payment']}"
																styleClass="BUTTON"
																rendered="#{pages$CancelContract.showPayment && !pages$CancelContract.readOnly}"
																binding="#{pages$CancelContract.btnCollectPayment}" />
														</pims:security>

														<h:commandButton id="followUpDone" style="width:110px;"
															action="#{pages$CancelContract.onFollowUpDone}"
															value="#{msg['cancelContract.reviewDone']}"
															styleClass="BUTTON"
															rendered="#{pages$CancelContract.followUpDone && !pages$CancelContract.readOnly}" />
														<pims:security screen="Pims.Contract.Cancel.IssueNOL"
															action="view">
															<h:commandButton id="issueNOL" style="width:110px;"
																action="#{pages$CancelContract.onNOL}"
																value="#{msg['cancelContract.nol']}" styleClass="BUTTON"
																rendered="#{pages$CancelContract.showNOL && !pages$CancelContract.readOnly}" />
														</pims:security>
														<h:commandButton styleClass="BUTTON" style="width:110px;"
															binding="#{pages$CancelContract.printCL}"
															value="#{msg['commons.btn.printCL']}"
															action="#{pages$CancelContract.onPrintCL}"></h:commandButton>
														<h:commandButton id="printSett" style="width:150px;"
															styleClass="BUTTON"
															action="#{pages$CancelContract.printReport}"
															value="#{msg['cancelContract.btn.reprintSettlement']}"
															rendered="#{ pages$CancelContract.pageMode == 'PAYMENT_COLLECT_REQ' || pages$CancelContract.pageMode == 'MODE_COMPLETED_REQ' || pages$CancelContract.pageMode == 'MODE_AFTER_SETTLEMENT' || pages$CancelContract.pageMode == 'FOLLOW_UP_REQ'  || pages$CancelContract.showSettlementButton=='1'}" />

														<h:commandButton id="closeButton" style="width:110px;"
															action="#{pages$CancelContract.onClose}"
															onclick="if (!confirm('#{msg['commons.messages.confirmClose']}')) return false"
															value="#{msg['cancelContract.close']}"
															styleClass="BUTTON"
															rendered="#{pages$CancelContract.showClose && !pages$CancelContract.isPagePopUp}" />
													</t:div>
												</div>
											</div>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<c:choose>
					<c:when test="${!pages$CancelContract.isPagePopUp}">
						<tr>
							<td colspan="2">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</c:when>
				</c:choose>

			</table>
			</div>
		</body>
	</html>
</f:view>