<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

		
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 <script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>


	
</head>
	<body class="BODY_STYLE">
	<div class="containerDiv">
	
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header --> 
	<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
		<tr style="height:84px;width:100%;#height:84px;#width:100%;">
			<td colspan="2">
				<jsp:include page="header.jsp" />
			</td>
		</tr>
		<tr width="100%">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99.2%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['userGroup.CreateGroup']}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>
				<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%">
					<tr valign="top">
								<td width="100%" valign="top" nowrap="nowrap">
						<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
							<div class="SCROLLABLE_SECTION" style="height:470px;width:100%;#height:470px;#width:100%;">
									<h:form id="detailsFrm" enctype="multipart/form-data" style="WIDTH: 97.6%;">
									
								<div>
									<div styleClass="MESSAGE"> 
									<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
										<tr>
											<td>
												<h:outputText value="#{pages$userGroup.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
												<h:outputText value="#{pages$userGroup.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
											</td>
										</tr>
									</table>
									</div>									
					
					<t:div  style="width:97%; margin:10px;" >
							
											<table cellpadding="1px" cellspacing="2px" >
										
															<tr>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
																	<h:outputLabel  styleClass="LABEL" value="#{msg['userGroup.groupId']}: "/>
																</td>
																<td class="DET_SEC_TD">
																	<h:selectOneMenu id="selectOneGroupId"
																	value="#{pages$userGroup.userGroupId}" 	
																	binding="#{pages$userGroup.selectOneMenuUserGroupId}"	
																	styleClass="SELECT_MENU" tabindex="1" >
																	<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1"/>
																	<f:selectItems value="#{pages$userGroup.lDAPGroups}"/>
																	</h:selectOneMenu>	
																</td>
																<td class="DET_SEC_TD">
																	
																</td>
																<td class="DET_SEC_TD">
																	
																</td>
															</tr>	
															<tr>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
										       						 <h:outputLabel  styleClass="LABEL"  value="#{msg['userGroup.groupName']}: "></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="txtGroupNameSec"
																		styleClass="INPUT_TEXT"
																		value="#{pages$userGroup.userGroupDbImpl.primaryName}" 																		
																		tabindex="2">
																	</h:inputText>
																</td>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
										        					<h:outputLabel  styleClass="LABEL"  value="#{msg['userGroup.groupNameArabic']}: "></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<t:inputText id="txtGroupSec" tabindex="3"
																	value="#{pages$userGroup.userGroupDbImpl.secondaryName}" 	
															        styleClass="INPUT_TEXT" 
															        />
																</td>
															</tr>																	
										</table>	
					</t:div>
													<t:div styleClass="BUTTON_TD" style="padding-top:10px;">
														<h:commandButton styleClass="BUTTON"
															value="#{msg['commons.saveButton']}"
															action="#{pages$userGroup.saveGroup}" type="submit"
															>
														</h:commandButton>
														&nbsp;
														<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}" 
															action="#{pages$userGroup.cancel}"></h:commandButton>
								</t:div>
												</div>
								</div>
							</h:form>
					  </div> 
						</td>			
					</tr>
				</table>
			</td>
		</tr>
		  
		<tr style="height:10px;width:100%;#height:10px;#width:100%;">
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
	</table>
	</div>
</body>
</html>
</f:view>