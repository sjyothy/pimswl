<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}
	function showAttachmentsPopup() 
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-500;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	}
		
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>

		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
				response.setHeader("Pragma", "no-cache"); //HTTP 1.0
				response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>

			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['tenderManagement.editProposal.heading']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height: 500px;">
										<h:form id="editProposalDetailsFrm">
											<table border="0" class="layoutTable">
												<tr>
													<td>
														<h:outputText
															value="#{pages$EditProposalDetails.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />
														<h:outputText
															value="#{pages$EditProposalDetails.infoMessages}"
															escape="false" styleClass="INFO_FONT" />
													</td>
												</tr>
											</table>
											<div
												style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; MARGIN: 0px; WIDTH: 95%; PADDING-TOP: 10px">

												<div class="MARGIN">
													<table style="width:100%"><tr><td>														
														<h:outputLabel styleClass="LABEL" style="width:150px;"
															value="#{msg['tenderManagement.editProposal.proposalNumber']}:" />
															</td><td>
														<h:inputText id="txtProposalNumber"
															value="#{pages$EditProposalDetails.tenderProposalView.proposalNumber}"
															readonly="true" style="width:200px;" />
</td><td>
														<h:outputLabel styleClass="LABEL" style="width:150px;"
															value="#{msg['tenderManagement.editProposal.proposalContractor']}:" />
					</td><td>									<h:inputText id="txtProposalContractor"
															value="#{pages$EditProposalDetails.isEnglishLocale ? pages$EditProposalDetails.tenderProposalView.contractorNameEn : pages$EditProposalDetails.tenderProposalView.contractorNameAr}"
															readonly="true" style="width:200px;" />
							</td>		
												</tr><tr><td>
														<h:outputLabel styleClass="LABEL" style="width:150px;"
															value="#{msg['tenderManagement.editProposal.proposalAmount']}:" />
</td><td>														<h:inputText id="txtProposalAmount"
															value="#{pages$EditProposalDetails.tenderProposalView.proposalAmount}"
															style="width:200px;" />
		</td>													
														
			<td>												<h:outputLabel styleClass="LABEL" style="width:150px;"
																value="#{msg['tenderManagement.editProposal.proposalPeriod']}:" />
				</td><td>										<h:panelGroup>
															<h:inputText id="txtProposalPeriod"
																value="#{pages$EditProposalDetails.tenderProposalView.proposalPeriod}"
																style="width:70px;" />														
															<h:outputLabel styleClass="LABEL" style="width:20px;"
																value="#{msg['tenderManagement.editProposal.proposalPeriod.In']} " />															
															<h:selectOneMenu id="drpDwnProposalPeriodType"
																value="#{pages$EditProposalDetails.tenderProposalView.proposalPeriodType}"
																style="width:110px;" >
																<f:selectItem itemValue="" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems value="#{pages$EditProposalDetails.tenderProposalPeriodTypeMap}" />																
															</h:selectOneMenu> 
														</h:panelGroup>
						</td>								
</tr><tr><td>
														<h:outputLabel styleClass="LABEL" style="width:150px;"
															value="#{msg['tenderManagement.editProposal.proposalRecommendation']}:" />
			</td><td colspan="3">											
														<h:inputTextarea id="txtProposalRecommendation"
																value="#{pages$EditProposalDetails.tenderProposalView.recommendation}"
																rows="5" style="width:200px;" />
																
								</td>
							</tr>
					</table>						
														
														<!-- <h:inputHidden id="hdnTenderProposalId" 
																value="#{pages$EditProposalDetails.tenderProposalView.tenderProposalId}">
														</h:inputHidden>
														
														<h:inputHidden id="hdnProposalNumber" 
																value="#{pages$EditProposalDetails.tenderProposalView.proposalNumber}">
														</h:inputHidden>
														
														<h:inputHidden id="hdnProposalContractor"
															value="#{pages$EditProposalDetails.isEnglishLocale ? pages$EditProposalDetails.tenderProposalView.contractorNameEn : pages$EditProposalDetails.tenderProposalView.contractorNameAr}">
														</h:inputHidden> --> 
														
													
												</div>
											</div>

											<div class="MARGIN">
												<table style="width: 98%">
													<tr>
														<td class="BUTTON_TD">
															<h:commandButton id="btnCancel" type="submit"
																styleClass="BUTTON" value="#{msg['commons.cancel']}"
																onclick="javascript:window.close();" style="width:100px;" />
															<h:commandButton id="btnSave" type="submit"
																styleClass="BUTTON" value="#{msg['commons.saveButton']}"
																action="#{pages$EditProposalDetails.btnSave_action}"
																style="width:100px;" />
														</td>
													</tr>
												</table>
											</div>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</body>
	</html>
</f:view>