<%-- 
  - Author: Munir Chagani
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Followup
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	    function resetValues()
      	{
      		document.getElementById("searchFrm:followupNumber").value="";
			document.getElementById("searchFrm:chequeNumber").value="";
      	    document.getElementById("searchFrm:propertyName").value="";
      	    document.getElementById("searchFrm:unitNumber").value="";
      	    document.getElementById("searchFrm:contractNumber").value="";
      	    document.getElementById("searchFrm:tenantName").value="";
      	    $('searchFrm:followupDateFrom').component.resetSelectedDate();
      	    $('searchFrm:followupDateTo').component.resetSelectedDate();
      	    
      	    $('searchFrm:paymentDateFrom').component.resetSelectedDate();
      	    $('searchFrm:paymentDateTo').component.resetSelectedDate();
      	    document.getElementById("searchFrm:followupActions").selectedIndex=0;
      	    document.getElementById("searchFrm:banks").selectedIndex=0;
      	    document.getElementById("searchFrm:followupStatus").selectedIndex=0;
      	    
        }
        
        function RowDoubleClick(auctionId, auctionNumber, auctionDate, auctionTitle, venueName)
		{
	      window.opener.document.getElementById("auctionRequestForm:auctionID").value = auctionId;
		  window.opener.document.getElementById("auctionRequestForm:aNo").value = auctionNumber;
		  window.opener.document.getElementById("auctionRequestForm:aDate").value = auctionDate;
		  window.opener.document.getElementById("auctionRequestForm:aTitle").value = auctionTitle;
		  window.opener.document.getElementById("auctionRequestForm:aVenue").value = venueName;
		  window.opener.document.getElementById("auctionRequestForm:auctionUnitsOfRequest").value = "";
		}
		
		function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	function closeWindow()
		{
		  window.close();
		}
        
        	  function	openPopUp()
		{
		
		    var width = 300;
            var height = 300;
            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + ",menubar=yes,toolbar=yes,scrollbars=yes,resizable=yes,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

            var child1 = window.open("about:blank", "subWind", windowFeatures);

		      child1.focus();
		}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

</head>
	<body class="BODY_STYLE">
	<div class="containerDiv">
	
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<c:choose>

					<c:when test="${!pages$followupSearch.isViewModePopUp}">
						<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>
					</c:when>
				</c:choose>


				<tr width="100%">

					<c:choose>
						<c:when test="${!pages$followupSearch.isViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>

					<td width="83%" height="470px" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['followup.search.heading']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
									<div class="SCROLLABLE_SECTION AUC_SCH_SS"
										style="height: 95%; width: 100%; # height: 95%; # width: 100%;">
										<h:form id="searchFrm" style="WIDTH: 96%; height: 428px;">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$followupSearch.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
														<td width="97%">
														<table width="100%">
														
														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['followup.number']} :"></h:outputLabel>
															</td>

															<td width="25%">
																<h:inputText id="followupNumber"
																	binding="#{pages$followupSearch.htmlInputFollowupNumber}"
																	></h:inputText>

															</td>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['followup.action']} :"></h:outputLabel>
															</td>
															<td width="25%">
																<h:selectOneMenu id="followupActions"
																	required="false"																																
																	binding="#{pages$followupSearch.htmlSelectOneFollowupAction}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.allFollowUpActions}" />
																</h:selectOneMenu>
															</td>
															
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['followup.search.fromDate']} :"></h:outputLabel>
															</td>
															<td >
																<rich:calendar id="followupDateFrom"
																	binding="#{pages$followupSearch.htmlCalendarFollowupDateFrom}"
																	locale="#{pages$followupSearch.locale}" popup="true"
																	datePattern="#{pages$followupSearch.dateFormat}"
																	showApplyButton="false" enableManualInput="false"
																	inputStyle="width:170px; height:14px" />
															</td>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['followup.search.toDate']} :"></h:outputLabel>
															</td>
															<td >
																<rich:calendar id="followupDateTo"
																	binding="#{pages$followupSearch.htmlCalendarFollowupDateTo}"
																	locale="#{pages$followupSearch.locale}" popup="true"
																	datePattern="#{pages$followupSearch.dateFormat}"
																	showApplyButton="false" enableManualInput="false"
																	inputStyle="width:170px; height:14px" />
															</td>
														</tr>	

														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['bouncedChequesList.chequeNumberCol']} :"></h:outputLabel>
															</td>

															<td >
																<h:inputText id="chequeNumber"
																	binding="#{pages$followupSearch.htmlInputChequeNumber}"
																	 ></h:inputText>

															</td>
															<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['chequeList.bank']} :"></h:outputLabel>
																</td>
															<td >
																<h:selectOneMenu id="banks"
																	required="false"																																
																	binding="#{pages$followupSearch.htmlSelectOneBankName}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.bankList}" />
																</h:selectOneMenu>
															</td>
															
														</tr>
														<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['followup.search.fromPaymentDate']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="paymentDateFrom"
																					binding="#{pages$followupSearch.htmlCalendarPaymentDateFrom}"
																					locale="#{pages$followupSearch.locale}"
																					popup="true"
																					datePattern="#{pages$followupSearch.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:170px; height:14px" />
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['followup.search.toPaymentDate']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="paymentDateTo"
																					binding="#{pages$followupSearch.htmlCalendarPaymentDateTo}"
																					locale="#{pages$followupSearch.locale}"
																					popup="true"
																					datePattern="#{pages$followupSearch.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:170px; height:14px" />
																			</td>
																		</tr>



														
														
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['property.name']} :"></h:outputLabel>
															</td>

															<td >
																<h:inputText id="propertyName"
																	binding="#{pages$followupSearch.htmlInputPropertyName}"
																	 ></h:inputText>

															</td>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['unit.unitNumber']} :"></h:outputLabel>
															</td>

															<td >
																<h:inputText id="unitNumber"
																	binding="#{pages$followupSearch.htmlInputUnitNumber}"
																	 ></h:inputText>

															</td>
															
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.Contract.Number']} :"></h:outputLabel>
															</td>

															<td >
																<h:inputText id="contractNumber"
																	binding="#{pages$followupSearch.htmlInputContract}"
																	></h:inputText>

															</td>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenants.name']} :"></h:outputLabel>
															</td>

															<td >
																<h:inputText id="tenantName"
																	binding="#{pages$followupSearch.htmlInputTenantName}"
																	></h:inputText>

															</td>															
														</tr>
														<tr>
																	   <td >
																		<h:outputLabel styleClass="LABEL"
																		  value="#{msg['commons.status']} :"></h:outputLabel>
																		</td>
																		<td >
																			<h:selectOneMenu id="followupStatus"
																				required="false"																																
																				binding="#{pages$followupSearch.htmlSelectOneFollowupStatus}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.All']}" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.followUpStatus}" />
																			</h:selectOneMenu>
																		</td>
																		
																	</tr>
														<tr>
															<td class="BUTTON_TD" colspan="4">
																			<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.search']}"
																				action="#{pages$followupSearch.searchFollowups}"
																				style="width: 75px" tabindex="7">
																			</h:commandButton>
																			
																			<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.clear']}"
																				onclick="javascript:resetValues();"
																				style="width: 75px" tabindex="7"></h:commandButton>

																		</td>


																	</tr>
																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>
														
													</table>
												</div>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" style="width: 100%; # width: 100%;">
													<t:dataTable id="dt1"
														value="#{pages$followupSearch.followupsList}"
														binding="#{pages$followupSearch.dataTable}"
														rows="#{pages$followupSearch.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%">

														<t:column id="followUpNumnberCol" defaultSorted="true" >
															<f:facet name="header">
																  <t:commandSortHeader columnName="followUpNumber" 
															      actionListener="#{pages$followupSearch.sort}"
														          value="#{msg['followup.number']}" arrow="true" >
														          <f:attribute name="sortField" value="followUpNumber" />	
												               </t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.followUpNumber}" style="white-space: normal;"
																styleClass="A_LEFT" />
														</t:column>
														<t:column id="followUpStatusCol" >
															<f:facet name="header">
																<t:outputText value="#{msg['commons.status']}" />
																
															</f:facet>
															<t:outputText rendered="#{pages$followupSearch.isEnglishLocale}" value="#{dataItem.statusEn}" 
															              style="white-space: normal;" styleClass="A_LEFT" />
															<t:outputText rendered="#{!pages$followupSearch.isEnglishLocale}" value="#{dataItem.statusAr}" 
															              style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>														
																												
														<t:column id="chequeAmountCol"  >
															<f:facet name="header">
																<t:commandSortHeader columnName="amount" 
																      actionListener="#{pages$followupSearch.sort}"
															          value="#{msg['cheque.amount']}" arrow="true" >
															          <f:attribute name="sortField" value="amount" />	
													             </t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.paymentReceiptDetailView.amount}"
																styleClass="A_RIGHT" style="white-space: normal;">
																<f:convertNumber maxFractionDigits="2" minFractionDigits="2" pattern="#{pages$followupSearch.numberFormat}"/>
															</t:outputText>
														</t:column>
														<t:column id="chequeNumberCol"  style="white-space: normal;">
															<f:facet name="header">
															  <t:commandSortHeader columnName="methodRefNo" 
															      actionListener="#{pages$followupSearch.sort}"
														          value="#{msg['bouncedChequesList.chequeNumberCol']}" arrow="true" >
														          <f:attribute name="sortField" value="methodRefNo" />	
												              </t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.paymentReceiptDetailView.methodRefNo}" 
																styleClass="A_LEFT" style="white-space: normal;"/>
														</t:column>
														<t:column id="chequeDateCol"  style="white-space: normal;">
																<f:facet name="header">
																	<t:commandSortHeader columnName="methodRefDate"
																		actionListener="#{pages$followupSearch.sort}"
																		value="#{msg['payment.chequeDate']}" arrow="true">
																		<f:attribute name="sortField" value="methodRefDate" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText
																	value="#{dataItem.paymentReceiptDetailView.methodRefDate}"
																	styleClass="A_LEFT" style="white-space: normal;">
																	<f:convertDateTime
																		pattern="#{pages$followupSearch.dateFormat}"
																		timeZone="#{pages$followupSearch.timeZone}" />
																</t:outputText>
															</t:column>

														
														<t:column id="chequeStatusCol"      style="white-space: normal;">
															<f:facet name="header">
																<t:commandSortHeader columnName="methodStatusId" 
															      actionListener="#{pages$followupSearch.sort}"
														          value="#{msg['chequeList.chequeStatus']}" arrow="true" >
														          <f:attribute name="sortField" value="methodStatusId" />	
												              </t:commandSortHeader>
															</f:facet>
															<t:outputText rendered="#{pages$followupSearch.isEnglishLocale}" value="#{dataItem.paymentReceiptDetailView.methodStatusEn}"
																styleClass="A_LEFT" style="white-space: normal;"/>
														    <t:outputText rendered="#{!pages$followupSearch.isEnglishLocale}" value="#{dataItem.paymentReceiptDetailView.methodStatusAr}"
																styleClass="A_LEFT" style="white-space: normal;"/>
														</t:column>
														
														<t:column id="lastFollowupActionEnCol" rendered="#{pages$followupSearch.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText value="#{msg['followup.lastAction']}" />
															</f:facet>
															<t:outputText value="#{dataItem.lastAction.followUpActionDescriptionEn}"
																styleClass="A_LEFT" style="white-space: normal;"/>
														</t:column>
														<t:column id="lastFollowupActionArCol" rendered="#{pages$followupSearch.isArabicLocale}">
															<f:facet name="header">
																<t:outputText value="#{msg['followup.lastAction']}" />
															</f:facet>
															<t:outputText value="#{dataItem.lastAction.followUpActionDescriptionAr}"
																styleClass="A_LEFT" style="white-space: normal;"/>
														</t:column>
														<t:column id="lastFollowupDateCol"    style="white-space: normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['followup.lastActionDate']}" />
															</f:facet>
															<t:outputText value="#{dataItem.lastAction.followUpProgressDate}"
																styleClass="A_LEFT" style="white-space: normal;">
																<f:convertDateTime
																	pattern="#{pages$followupSearch.dateFormat}"
																	timeZone="#{pages$followupSearch.timeZone}" />
															</t:outputText>
														</t:column>
														<t:column id="contractNumber" sortable="true"  >
															<f:facet name="header">
															  <t:commandSortHeader columnName="contractNumber" 
															      actionListener="#{pages$followupSearch.sort}"
														          value="#{msg['commons.Contract.Number']}"            arrow="true" >
														          <f:attribute name="sortField" value="contractNumber" />	
												               </t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.contractView.contractNumber}"
																styleClass="A_LEFT" style="white-space: normal;"/>
														</t:column>														
														<t:column id="unitNumberCol" >
															<f:facet name="header">
																<t:outputText value="#{msg['unit.numberCol']}" />
															</f:facet>
															<t:outputText value="#{dataItem.contractView.unitNumber}"
																styleClass="A_LEFT" style="white-space: normal;"/>
														</t:column>
														<t:column id="tenantNameCol" >
															<f:facet name="header">
																<t:outputText value="#{msg['bouncedChequesList.tenantNameCol']}" />
															</f:facet>
															<t:outputText value="#{dataItem.contractView.tenantFirstName}"
																styleClass="A_LEFT" style="white-space: normal;"/>
														</t:column>
														<t:column id="followUpDateCol" >
															<f:facet name="header">
																<t:commandSortHeader columnName="startDate" 
															       actionListener="#{pages$followupSearch.sort}"
														           value="#{msg['followup.date']}" arrow="true" >
														                 <f:attribute name="sortField" value="startDate" />	
												               </t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.startDate}"
																styleClass="A_LEFT" style="white-space: normal;">
																<f:convertDateTime
																	pattern="#{pages$followupSearch.dateFormat}"
																	timeZone="#{pages$followupSearch.timeZone}" />
															</t:outputText>
														</t:column>
														<t:column id="bouncedReason" >
															<f:facet name="header">
															<h:outputLabel value="#{msg['commons.label.bouncedReason']}">
															</h:outputLabel>
															</f:facet>
															<h:outputLabel value="#{dataItem.paymentReceiptDetailView.bouncedReason}">
															</h:outputLabel>
														</t:column>
														<t:column id="actionCol" sortable="false"
															style="TEXT-ALIGN: center;">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.action']}" />
																</f:facet>
																<t:commandLink
																	action="#{pages$followupSearch.followUpDetailsMode}"
																	rendered="true">&nbsp;
																	<h:graphicImage id="viewIcon"
																		title="#{msg['commons.details']}"
																		url="../resources/images/app_icons/Contrect.png" />&nbsp;
																</t:commandLink>
														</t:column>		
												</t:dataTable>
												</div>
													<t:div id = "footerDiV" styleClass="contentDivFooter AUCTION_SCH_RF" style="width:100%;#width:100%;" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td id="recordTD" class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td  class="RECORD_NUM_TD">
														<h:outputText value="#{pages$followupSearch.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
												
													<t:div styleClass="PAGE_NUM_BG" style="#width:20%">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{pages$followupSearch.currentPage}"/>
																	</td>
																</tr>					
															</table>
														</t:div>
																	<TABLE border="0" class="SCH_SCROLLER">
																 	<tr>
																 		<td>   
																			<t:commandLink
																				action="#{pages$followupSearch.pageFirst}"
																				disabled="#{pages$followupSearch.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																		<t:commandLink
																			action="#{pages$followupSearch.pagePrevious}"
																			disabled="#{pages$followupSearch.firstRow == 0}">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR"></t:graphicImage>
																		</t:commandLink>
																	  	</td>
																	  	<td>
																		<t:dataList value="#{pages$followupSearch.pages}"
																			var="page" >
																			<h:commandLink value="#{page}"
																				actionListener="#{pages$followupSearch.page}"
																				rendered="#{page != pages$followupSearch.currentPage}" />
																			<h:outputText value="<b>#{page}</b>" escape="false"
																				rendered="#{page == pages$followupSearch.currentPage}" />
																		</t:dataList>
																		</td>
																		<td>
																		<t:commandLink action="#{pages$followupSearch.pageNext}"
																			disabled="#{pages$followupSearch.firstRow + pages$followupSearch.rowsPerPage >= pages$followupSearch.totalRows}">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF"></t:graphicImage>
																		</t:commandLink>
																		</td>
																		<td>
																		<t:commandLink action="#{pages$followupSearch.pageLast}"
																			disabled="#{pages$followupSearch.firstRow + pages$followupSearch.rowsPerPage >= pages$followupSearch.totalRows}">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL"></t:graphicImage>
																		</t:commandLink>
																		</td>
																		</tr>
																	</TABLE>	
																	</td></tr>
															</table>
													</t:div>											</div>
									</div>

									</h:form>

									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
</html>
</f:view>