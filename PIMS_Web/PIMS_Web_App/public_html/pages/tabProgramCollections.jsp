<t:div style="width:95%;padding-top:5px;padding-botton:5px;">

	<t:div styleClass="contentDiv" style="width:95%;">

		<t:dataTable id="programDonationRequestsGrid"
			binding="#{pages$endowmentPrograms.dataTableProgramCollections}"
			value="#{pages$endowmentPrograms.programCollections}"
			preserveDataModel="true" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
			width="100%">


			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['commons.requestNumber']}" />
				</f:facet>
				<h:outputText style="white-space: normal;" id="requestNumber"
					value="#{dataItem.requestNumber}" />
			</t:column>
			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['paymentSchedule.paymentNumber']}" />
				</f:facet>
				<h:outputText style="white-space: normal;" id="paymentNumber"
					value="#{dataItem.refNumber}" />
			</t:column>
			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['collectionProcedure.createdOn']}" />
				</f:facet>
				<h:outputText style="white-space: normal;" id="requestCreatedOn"
					value="#{dataItem.requestCreatedOn}">
					<f:convertDateTime timeZone="#{pages$endowmentPrograms.timeZone}"
						pattern="#{pages$endowmentPrograms.dateFormat}" />
				</h:outputText>
			</t:column>
			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['commons.status']}" />
				</f:facet>
				<h:outputText style="white-space: normal;" id="trxStatus"
					value="#{dataItem.trxStatusAr}">
					
				</h:outputText>
			</t:column>
			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['commons.amount']}" />
				</f:facet>
				<h:outputText style="white-space: normal;" id="amount"
					value="#{dataItem.amount}">
			
				</h:outputText>
			</t:column>
		</t:dataTable>
	</t:div>
</t:div>