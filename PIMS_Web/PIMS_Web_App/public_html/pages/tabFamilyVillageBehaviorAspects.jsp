
<t:div id="tabBehaviorAspectss" style="width:100%;">

	<t:panelGrid id="tabBehaviorAspectsGeneralGrid" cellpadding="3px"
		width="100%" cellspacing="2px" columns="1"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.BehaviorAspect.General']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">

		<h:commandButton id="btnNavigateToBehaviorMonitoringForm" styleClass="BUTTON" style="width:auto;"
			value="#{msg['researchFamilyVillageBeneficiary.lbl.BehaviorAspect.BehaviorMonitoringForm']}"
			action="#{pages$tabFamilyVillageBehaviorAspects.onNavigateToBehaviorMonitoringForm}">
		</h:commandButton>
		<h:commandButton
			id="btnNavigateToBehaviorAssessmentDuringActiviesForm" style="width:auto;"
			styleClass="BUTTON"
			value="#{msg['researchFamilyVillageBeneficiary.lbl.BehaviorAspect.BehaviorAssessmentDuringActivitiesForm']}"
			action="#{pages$tabFamilyVillageBehaviorAspects.onNavigateToBehaviorAssessmentDuringActiviesForm}">
		</h:commandButton>


	</t:div>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">

		<h:commandButton id="btnAddBehaviorAspectsGeneral" styleClass="BUTTON"
			type="button" value="#{msg['commons.Add']}"
			onclick="javaScript:openFamilyVillageBehaviorAspectsGeneralPopup('#{pages$tabFamilyVillageBehaviorAspects.personId}');">


		</h:commandButton>

	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableBehaviorAspectsGeneral" rows="5"
			width="100%"
			value="#{pages$tabFamilyVillageBehaviorAspects.dataListBehaviorAspectsGeneral}"
			binding="#{pages$tabFamilyVillageBehaviorAspects.dataTableBehaviorAspectsGeneral}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="BehaviorAspectsGeneralAspectsCreatedBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="BehaviorAspectsGeneralAspectsCreatedByOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageBehaviorAspects.englishLocale? 
																				dataItem.createdByEn:
																				dataItem.createdByAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="BehaviorAspectsGeneralAspectsCreatedOn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageBehaviorAspects.dateFormat}"
						timeZone="#{pages$tabFamilyVillageBehaviorAspects.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="BehaviorAspectsGeneralAspectsAngerReasons"
				sortable="true">
				<f:facet name="header">
					<t:outputText id="BehaviorAspectsGeneralAspectsAngerReasonsOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.BehaviorAspect.AngerReasons']}" />
				</f:facet>
				<h:outputText value="#{dataItem.angerReasonsColValue}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="BehaviorAspectsGeneralAspectsAngerManifestation"
				sortable="true">
				<f:facet name="header">
					<t:outputText
						id="BehaviorAspectsGeneralAspectsAngerManifestationOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.BehaviorAspect.AngerManifestation']}" />
				</f:facet>
				<h:outputText value="#{dataItem.angerManifestationColValue}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="BehaviorAspectsGeneralAspectsRoutingChange"
				sortable="true">
				<f:facet name="header">
					<t:outputText
						id="BehaviorAspectsGeneralAspectsBehaviorInRoutingChangeOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.BehaviorAspect.BehaviorInRoutingChange']}" />
				</f:facet>
				<h:outputText value="#{dataItem.behaviorRoutineChangeColValue}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="BehaviorAspectsGeneralAspectsBehaviorInPain"
				sortable="true">
				<f:facet name="header">
					<t:outputText id="BehaviorAspectsGeneralAspectsBehaviorInPainOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.BehaviorAspect.BehaviorInPain']}" />
				</f:facet>
				<h:outputText value="#{dataItem.behaviorInPainColValue}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>



		</t:dataTable>
	</t:div>
	<t:div id="BehaviorAspectsGeneralAspectsDivScroller"
		styleClass="contentDivFooter" style="width:99.1%">


		<t:panelGrid id="BehaviorAspectsGeneralAspectsRecNumTable" columns="2"
			cellpadding="0" cellspacing="0" width="100%"
			columnClasses="RECORD_NUM_TD,BUTTON_TD">
			<t:div id="BehaviorAspectsGeneralAspectsRecNumDiv"
				styleClass="RECORD_NUM_BG">
				<t:panelGrid id="BehaviorAspectsGeneralAspectsRecNumTbl" columns="3"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
					cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
					<h:outputText value="#{msg['commons.recordsFound']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$tabFamilyVillageBehaviorAspects.recordSizeBehaviorAspectsGeneral}" />
				</t:panelGrid>

			</t:div>

			<CENTER>
				<t:dataScroller id="BehaviorAspectsGeneralAspectsScroller"
					for="dataTableBehaviorAspectsGeneral" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$tabFamilyVillageBehaviorAspects.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="BehaviorAspectsGeneralAspectslblFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="BehaviorAspectsGeneralAspectslblFRRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="BehaviorAspectsGeneralAspectslblFFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="BehaviorAspectsGeneralAspectslblLRequestHistory"></t:graphicImage>
					</f:facet>

					<t:div id="BehaviorAspectsGeneralAspectsPageNumDiv"
						styleClass="PAGE_NUM_BG">
						<t:panelGrid id="BehaviorAspectsGeneralAspectsPageNumTable"
							styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
							cellspacing="0">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:panelGrid>

					</t:div>
				</t:dataScroller>
			</CENTER>

		</t:panelGrid>
	</t:div>
	<t:panelGrid id="tabBehaviorAspectsOthersGrid" cellpadding="3px"
		width="100%" cellspacing="2px" columns="1"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.BehaviorAspect.Others']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">

		<h:commandButton id="btnAddBehaviorAspectsOthers" styleClass="BUTTON"
			type="button" value="#{msg['commons.Add']}"
			onclick="javaScript:openFamilyVillageBehaviorAspectsOthersPopup('#{pages$tabFamilyVillageBehaviorAspects.personId}');">


		</h:commandButton>

	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableBehaviorAspectsOthers" rows="5" width="100%"
			value="#{pages$tabFamilyVillageBehaviorAspects.dataListBehaviorAspectsOthers}"
			binding="#{pages$tabFamilyVillageBehaviorAspects.dataTableBehaviorAspectsOthers}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="BehaviorAspectsOthersAspectsCreatedBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="BehaviorAspectsOthersAspectsCreatedByOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageBehaviorAspects.englishLocale? 
																				dataItem.createdByEn:
																				dataItem.createdByAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="BehaviorAspectsOthersAspectsCreatedOn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageBehaviorAspects.dateFormat}"
						timeZone="#{pages$tabFamilyVillageBehaviorAspects.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="BehaviorAspectsOthersAspectsHobbies" sortable="true">
				<f:facet name="header">
					<t:outputText id="BehaviorAspectsOthersAspectsHobbiesOT"
						value="#{msg['generalAspects.hobby']}" />
				</f:facet>
				<h:outputText value="#{dataItem.hobbiesTypeColValue}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="BehaviorAspectsOthersAspectsGoodDealingWith"
				sortable="true">
				<f:facet name="header">
					<t:outputText id="BehaviorAspectsOthersAspectsGoodDealingWithOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.BehaviorAspect.GoodDealingWith']}" />
				</f:facet>
				<h:outputText value="#{dataItem.goodInDealingWithColValue}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="BehaviorAspectsOthersAspectsNotGoodDealingWith"
				sortable="true">
				<f:facet name="header">
					<t:outputText id="BehaviorAspectsOthersAspectsNotGoodDealingWithOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.BehaviorAspect.NotGoodDealingWith']}" />
				</f:facet>
				<h:outputText value="#{dataItem.notGoodInDealingWithColValue}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>


		</t:dataTable>
	</t:div>
	<t:div id="BehaviorAspectsOthersAspectsDivScroller"
		styleClass="contentDivFooter" style="width:99.1%">


		<t:panelGrid id="BehaviorAspectsOthersAspectsRecNumTable" columns="2"
			cellpadding="0" cellspacing="0" width="100%"
			columnClasses="RECORD_NUM_TD,BUTTON_TD">
			<t:div id="BehaviorAspectsOthersAspectsRecNumDiv"
				styleClass="RECORD_NUM_BG">
				<t:panelGrid id="BehaviorAspectsOthersAspectsRecNumTbl" columns="3"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
					cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
					<h:outputText value="#{msg['commons.recordsFound']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$tabFamilyVillageBehaviorAspects.recordSizeBehaviorAspectsOthers}" />
				</t:panelGrid>

			</t:div>

			<CENTER>
				<t:dataScroller id="BehaviorAspectsOthersAspectsScroller"
					for="dataTableBehaviorAspectsOthers" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$tabFamilyVillageBehaviorAspects.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="BehaviorAspectsOthersAspectslblFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="BehaviorAspectsOthersAspectslblFRRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="BehaviorAspectsOthersAspectslblFFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="BehaviorAspectsOthersAspectslblLRequestHistory"></t:graphicImage>
					</f:facet>

					<t:div id="BehaviorAspectsOthersAspectsPageNumDiv"
						styleClass="PAGE_NUM_BG">
						<t:panelGrid id="BehaviorAspectsOthersAspectsPageNumTable"
							styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
							cellspacing="0">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:panelGrid>

					</t:div>
				</t:dataScroller>
			</CENTER>

		</t:panelGrid>
	</t:div>

</t:div>

