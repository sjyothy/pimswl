<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

 function onSelectionChanged(changedChkBox)
 {
     document.getElementById('detailsFrm:disablingDiv').style.display='block';
	 changedChkBox.nextSibling.onclick();
 }
	function performTabClick(elementid)
	{
			
		disableInputs();
		
		document.getElementById("detailsFrm:"+elementid).onclick();
		
		return 
		
	}
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
	}	
	
	
		
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		disableInputs();	
		if( hdnPersonType=='APPLICANT' ) 
		{			    			 
			 document.getElementById("detailsFrm:hdnPersonId").value=personId;
			 document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
			 document.getElementById("detailsFrm:hdnPersonName").value=personName;
			 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
			 document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;
		}
				
	     document.getElementById("detailsFrm:onMessageFromSearchPerson").onclick();
	} 
	
		
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{pages$unBlockingRequest.pageTitle}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">

										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">
												<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>

																<h:outputText id="errorId"
																	value="#{pages$unBlockingRequest.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$unBlockingRequest.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$unBlockingRequest.pageMode}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$unBlockingRequest.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$unBlockingRequest.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$unBlockingRequest.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$unBlockingRequest.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$unBlockingRequest.hdnIsCompany}"></h:inputHidden>


															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<div style="width: 100%;">

														<table cellpadding="1px" cellspacing="5px" width="100%">
															<tr>
																<td>
																	<h:outputText id="idSendTo" styleClass="LABEL"
																		rendered="#{pages$unBlockingRequest.showApproveButton }"
																		value="#{msg['mems.investment.label.sendTo']}" />

																</td>
																<td colspan="3">
																	<h:selectOneMenu id="idUserGrps"
																		rendered="#{pages$unBlockingRequest.showApproveButton }"
																		binding="#{pages$unBlockingRequest.cmbReviewGroup}"
																		style="width: 200px;">
																		<f:selectItem
																			itemLabel="#{msg['commons.pleaseSelect']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.userGroups}" />
																	</h:selectOneMenu>

																</td>
															</tr>
															<tr>
																<td>
																	<h:outputLabel styleClass="LABEL"
																		rendered="#{pages$unBlockingRequest.showApproveButton || pages$unBlockingRequest.showComplete}"
																		value="#{msg['maintenanceRequest.remarks']}" />

																</td>
																<td colspan="3">
																	<t:inputTextarea style="width: 90%;" id="txt_remarks"
																		rendered="#{pages$unBlockingRequest.showApproveButton || pages$unBlockingRequest.showComplete}"
																		value="#{pages$unBlockingRequest.txtRemarks}"
																		onblur="javaScript:validate();"
																		onkeyup="javaScript:validate();"
																		onmouseup="javaScript:validate();"
																		onmousedown="javaScript:validate();"
																		onchange="javaScript:validate();"
																		onkeypress="javaScript:validate();" />
																</td>
															</tr>

														</table>
													</div>

													<div class="TAB_PANEL_INNER">
														<rich:tabPanel
															binding="#{pages$unBlockingRequest.tabPanel}"
															style="width: 100%">

															<rich:tab id="applicationTab"
																label="#{msg['commons.tab.applicationDetails']}">
																<%@ include file="ApplicationDetails.jsp"%>
															</rich:tab>
															<rich:tab id="paymentsTab"
																label="#{msg['UnBlockingRequest.lbl.basicDetails']}">
																<%@ include file="tabUnBlockingDetails.jsp"%>
															</rich:tab>

															<rich:tab id="reviewTab"
																rendered="#{!pages$unBlockingRequest.showSaveButton}"
																label="#{msg['commons.tab.reviewDetails']}">
																<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
															</rich:tab>

															<rich:tab id="commentsTab"
																label="#{msg['commons.commentsTabHeading']}">
																<%@ include file="notes/notes.jsp"%>
															</rich:tab>
															<rich:tab id="attachmentTab"
																label="#{msg['commons.attachmentTabHeading']}">
																<%@  include file="attachment/attachment.jsp"%>
															</rich:tab>
															<rich:tab
																label="#{msg['contract.tabHeading.RequestHistory']}"
																title="#{msg['commons.tab.requestHistory']}"
																action="#{pages$unBlockingRequest.onActivityLogTab}">
																<%@ include file="../pages/requestTasks.jsp"%>
															</rich:tab>

														</rich:tabPanel>
													</div>

													<t:div styleClass="BUTTON_TD"
														style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">

														<pims:security screen="Pims.MinorMgmt.Unblock.Save"
															action="view">

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$unBlockingRequest.showSaveButton}"
																value="#{msg['commons.saveButton']}"
																onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkSave"
																action="#{pages$unBlockingRequest.onSave}" />


															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$unBlockingRequest.showSubmitButton}"
																value="#{msg['commons.submitButton']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkSubmit"
																action="#{pages$unBlockingRequest.onSubmit}" />


															<h:commandButton styleClass="BUTTON"
																									
																rendered="#{pages$unBlockingRequest.showResubmitButton && pages$unBlockingRequest.isTaskPresent}"
																value="#{msg['socialResearch.btn.resubmit']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkResubmit"
																action="#{pages$unBlockingRequest.onResubmit}" />
														</pims:security>


														<pims:security
															screen="Pims.MinorMgmt.Unblock.ApproveReject"
															action="view">

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$unBlockingRequest.showApproveButton && pages$unBlockingRequest.isTaskPresent }"
																value="#{msg['commons.approveButton']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">

															</h:commandButton>
															<h:commandLink id="lnkCollect"
																action="#{pages$unBlockingRequest.onApproved}" />

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$unBlockingRequest.showApproveButton && pages$unBlockingRequest.isTaskPresent }"
																value="#{msg['commons.reject']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkRejected"
																action="#{pages$unBlockingRequest.onRejected}" />

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$unBlockingRequest.showApproveButton && pages$unBlockingRequest.isTaskPresent }"
																value="#{msg['commons.sendBack']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkSendBack"
																action="#{pages$unBlockingRequest.onSendBack}" />


															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$unBlockingRequest.showApproveButton && pages$unBlockingRequest.isTaskPresent }"
																value="#{msg['commons.review']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkReviewReq"
																action="#{pages$unBlockingRequest.onReviewRequired}" />

														</pims:security>

														<h:commandButton styleClass="BUTTON"
															rendered="#{pages$unBlockingRequest.showReviewDone && pages$unBlockingRequest.isTaskPresent }"
															value="#{msg['commons.done']}"
															onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
														</h:commandButton>
														<h:commandLink id="lnkReviewDone"
															action="#{pages$unBlockingRequest.onReviewed && pages$unBlockingRequest.isTaskPresent }" />


														<pims:security screen="Pims.MinorMgmt.Unblock.Complete"
															action="view">

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$unBlockingRequest.showComplete && pages$unBlockingRequest.isTaskPresent }"
																value="#{msg['commons.complete']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkComplete"
																action="#{pages$unBlockingRequest.onComplete}" />

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$unBlockingRequest.showComplete && pages$unBlockingRequest.isTaskPresent }"
																value="#{msg['commons.sendBack']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkRejectBackFromFinance"
																action="#{pages$unBlockingRequest.onRejectedFromFinance}" />
														</pims:security>
													</t:div>

												</div>
										</div>
										</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			+
		</body>
	</html>
</f:view>