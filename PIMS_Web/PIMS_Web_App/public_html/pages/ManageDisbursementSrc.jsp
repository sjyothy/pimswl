<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript">
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
</head>

<body class="BODY_STYLE">
	<div class="containerDiv">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td colspan="2"><jsp:include page="header.jsp" />
				</td>
			</tr>
			<tr width="100%">
				<td class="divLeftMenuWithBody" width="17%">
					<jsp:include page="leftmenu.jsp" />
				</td>
				<td width="83%" valign="top" class="divBackgroundBody">
					<table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="HEADER_TD">
								<h:outputLabel value="#{msg['disburse.lable.main.heading']}"
									styleClass="HEADER_FONT" style="padding:10px" />
							</td>
						</tr>
					</table>
					<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
			        	<tr valign="top">
			        		<td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="3">
			        		</td> 
			        		<td>		        			
			        			<div class="SCROLLABLE_SECTION">
			        				<div id="errMsg">
			        					<table border="0" class="layoutTable"
													style="margin-left: 10px; margin-right: 10px; height: 10px;">
											<tr>
												<td>
													<h:outputText
													value="#{pages$manageDisbursementSrcController.errorMessages}"
													escape="false" styleClass="ERROR_FONT" />
		
													<h:outputText id="oppMsgs"
													value="#{pages$manageDisbursementSrcController.successMessages}"
													styleClass="INFO_FONT" escape="false" />
												</td>
											</tr>
										</table>		        						        				
			        				</div>
			        				
			        				<div class="MARGIN" style="width: 97%;">
			        					<table cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
										<div class="DETAIL_SECTION" style="width: 99.6%;">
											<h:outputLabel value="#{msg['disuburse.heading.detail']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
						        			<t:div id="inhtTab" styleClass="DETAIL_SECTION_INNER" style="width:100%">
						        				<h:form>
						        					<t:panelGrid id="inhtAssetShareTable" cellpadding="1px" width="100%" cellspacing="5px" 		
				 										styleClass="DETAIL_SECTION_INNER" columns="4" columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
				 		
				 										<h:outputLabel styleClass="LABEL" value="#{msg['disuburse.label.sourceNameEn']}:"/> 										
				 										<h:inputText id="txtSrcNameEn" binding="#{pages$manageDisbursementSrcController.sourceNameEn}" style="width:95%;"></h:inputText>
				 		 	
														<h:outputLabel styleClass="LABEL" value="#{msg['disuburse.label.sourceNameAr']}:"/>								
														<h:inputText id="txtSrcNameAr" binding="#{pages$manageDisbursementSrcController.sourceNameAr}" style="width:95%;"></h:inputText>
						
						
														<h:outputLabel styleClass="LABEL" value="#{msg['disburseSrc.lbl.distCodeCombCode']}:"/>
														<h:inputText id="txtGrpNum" binding="#{pages$manageDisbursementSrcController.grpNum}" style="width:95%;"></h:inputText>
														
														<h:outputLabel styleClass="LABEL" value="#{msg['disburseSrc.lbl.acctsPayCombCode']}:"/>
														<h:inputText id="txtacctsPayCombCode" binding="#{pages$manageDisbursementSrcController.acctsPayCombCode}" style="width:95%;"></h:inputText>
														
														<h:outputLabel styleClass="LABEL" value="#{msg['disuburse.label.desc']}:"/>
														<h:inputTextarea id="txtDesc" binding="#{pages$manageDisbursementSrcController.description}" style="width:95%;"></h:inputTextarea>																
													</t:panelGrid>
						        					
						        					<table cellpadding="10" cellspacing="2px" width="97.5%" >
														<tr >
															<td colspan="4" class="BUTTON_TD">
																	<c:choose>
						 												<c:when test="${pages$manageDisbursementSrcController.isEditMode}">
						 													<h:commandButton  id="update"
						 														rendered="#{pages$manageDisbursementSrcController.isEditMode}"
																				styleClass="BUTTON" action="#{pages$manageDisbursementSrcController.manage}" 
																				value="#{msg['commons.Update']}">																			
																			</h:commandButton>					  												
						  												</c:when>
						  												<c:when test="${pages$manageDisbursementSrcController.isAddMode}">
						  														<h:commandButton  id="add" 
						  														rendered="#{pages$manageDisbursementSrcController.isAddMode}"
																				styleClass="BUTTON" action="#{pages$manageDisbursementSrcController.manage}" 
																				value="#{msg['commons.add']}">	
																				</h:commandButton>																	  													
						  												</c:when>
						  												<h:commandButton  id="back"
						 														
																				styleClass="BUTTON" action="#{pages$manageDisbursementSrcController.onBack}" 
																				value="#{msg['commons.back']}">																			
																			</h:commandButton>				
						  											</c:choose>
															</td>
														</tr>	
													</table>
														
						        				</h:form>
						        			</t:div>
						        		</div>					        		
				        			</div>
						        </div>
			        		</td>
			        	</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>
</f:view>