<%-- 
  - Author: Kamran Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Mark a person Blacklist
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%-- JAVA SCRIPT FUNCTIONS START --%>
<script type="text/javascript">
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}" >
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
		</head>
		
		<body class="BODY_STYLE">
		
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				
				<tr width="100%">
					
					<td width="100%" valign="top" class="divBackgroundBody">
						<table  class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['violationAction.blackList']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99.1%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%"  valign="top" nowrap="nowrap">
									<h:form id="searchFrm" >
									<div class="SCROLLABLE_SECTION" >
										
										<t:div styleClass="MESSAGE" style="width:98%;">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText id="ccc" escape="false" styleClass="ERROR_FONT"
																value="#{pages$blacklistPopup.errorMessages}" />
															<h:outputText  escape="false" styleClass="INFO_FONT"
																value="#{pages$blacklistPopup.successMessages}" />
																
														</td>
													</tr>
												</table>
											</t:div>
													
														<div class="MARGIN" style="width:98%;">
														<table cellpadding="0" cellspacing="0" width="99.3%">
																<tr>
																<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
																<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
																<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
																</tr>
														</table>
														<div class="DETAIL_SECTION" style="width:99%" >
														     <h:outputLabel value="" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														 <table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" width="100%">
														 <tr>
														 	<td >
														 		<h:outputLabel
														 			value="#{msg['contract.unit.isBlackListed']}">
														 		</h:outputLabel>
														 	</td>
														 	<td >
														 		<h:selectBooleanCheckbox
														 		readonly="#{pages$blacklistPopup.disabled}"
														 		value="#{pages$blacklistPopup.blacklist}">
														 		</h:selectBooleanCheckbox>
														 	</td>
														 </tr>
														 <tr>
													 		<td >
														 		<h:outputLabel
														 			value="#{msg['person.blacklisted.description']}">
														 		</h:outputLabel>
														 	</td>
														 	<td >
														 		<h:inputTextarea
														 			readonly="#{pages$blacklistPopup.disabled}"
														 			value="#{pages$blacklistPopup.blacklistDesc}">
														 		</h:inputTextarea>
														 	</td>
														 </tr>
												         </table>
															</div>
																<TABLE width="100%">
																	<tr>
																		<td class="BUTTON_TD" colspan="6">	
																				<h:commandButton
																					styleClass="BUTTON"
																					binding="#{pages$blacklistPopup.btnSave}"
																					action="#{pages$blacklistPopup.save}"
																					value="#{msg['commons.saveButton']}"
																					 />
																					 <h:commandButton
																					styleClass="BUTTON"
																					onclick="javascript:window.close();"
																					value="#{msg['bouncedChequesList.close']}"/>
																		</td>
																	</tr>
																</TABLE>
									</div>
									</h:form>
								</td>
							</tr>

						</table>
					</td>
				</tr>
			</table>


</body>
</html>
</f:view>