
<t:div id="tabDDetailsExpsense" style="width:100%;">


	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableExpenses"
			value="#{pages$manageEndowmentRevenueDistribution.dataListExpenses}"
			binding="#{pages$manageEndowmentRevenueDistribution.dataTableExpenses}"
			rows="10000" preserveDataModel="false" preserveSort="false"
			var="dataItem" rowClasses="row1,row2" rules="all"
			renderedIfEmpty="true" width="100%">

			<t:column id="requestNumC" sortable="true">
				<f:facet name="header">
					<t:outputText id="requestNumTxt"
						value="#{msg['commons.requestNumber']}" />
				</f:facet>
				<t:outputText value="#{dataItem.request.requestNumber}"
					style="white-space: normal;" styleClass="A_LEFT"/>
			</t:column>
			<t:column id="CreatedOnExpesnesTxtC" sortable="true">
				<f:facet name="header">
					<t:outputText id="CreatedOnExpesnesTxt"
						value="#{msg['commons.createdOn']}" />
				</f:facet>
				<t:outputText value="#{dataItem.createdOn}"
					style="white-space: normal;" styleClass="A_LEFT">
					<f:convertDateTime
						timeZone="#{pages$manageEndowmentRevenueDistribution.timeZone}"
						pattern="#{pages$manageEndowmentRevenueDistribution.dateFormat}" />
				</t:outputText>
			</t:column>
			<t:column id="statusExpensesC" sortable="true">
				<f:facet name="header">
					<t:outputText id="statusExpensesTxt"
						value="#{msg['commons.status']}" />
				</f:facet>
				<t:outputText
					value="#{pages$manageEndowmentRevenueDistribution.englishLocale? 
															         dataItem.status.dataDescEn:
															         dataItem.status.dataDescAr
							}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="amountExpesnseC" sortable="true">
				<f:facet name="header">
					<t:outputText id="amountExpesnseT" value="#{msg['commons.amount']}" />
				</f:facet>
				<t:outputText value="#{dataItem.amount}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="actionColExp" sortable="false" width="100"
				style="TEXT-ALIGN: center;">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.action']}" />
				</f:facet>

				<h:graphicImage id="manageExpense"
					onclick="javaScript:openManageExpensePopup('#{dataItem.request.requestId}')"
					style="margin-right:5px;" title="#{msg['commons.view']}"
					url="../resources/images/app_icons/manage_tender.png" />
			</t:column>
		</t:dataTable>
	</t:div>
	<t:div id="pagingDivExpenses" styleClass="contentDivFooter"
		style="width:99.2%;">
		<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
			cellspacing="1">
			<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
				<t:div styleClass="JUG_NUM_REC_ATT">
					<h:outputText value="#{msg['commons.records']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$manageEndowmentRevenueDistribution.expensesRecordsSize}" />
				</t:div>
			</t:panelGroup>
			<t:panelGroup>
				<t:dataScroller id="scrollerExpensesList" for="dataTableExpenses"
					paginator="true" fastStep="1" paginatorMaxPages="15"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
					styleClass="SCH_SCROLLER" pageIndexVar="pageNumber"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false">
					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="lblFExpensesList"></t:graphicImage>
					</f:facet>
					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="lblFRExpensesList"></t:graphicImage>
					</f:facet>
					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="lblFFExpensesList"></t:graphicImage>
					</f:facet>
					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="lblLExpensesList"></t:graphicImage>
					</f:facet>
					<t:div>
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<h:outputText styleClass="PAGE_NUM"
										style="font-size:9;color:black;"
										value="#{msg['mems.normaldisb.label.totalAmount']}:" />
								</td>
								<td>
									<h:outputText styleClass="PAGE_NUM"
										style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:9;font-weight:bold;color:black"
										value="#{pages$manageEndowmentRevenueDistribution.totalExpenseAmount}" />
								</td>
							</tr>
						</table>
					</t:div>

				</t:dataScroller>
			</t:panelGroup>
		</t:panelGrid>
	</t:div>
</t:div>


