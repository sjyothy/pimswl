<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++)
		{
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		       )
		    {
		        inputs[i].disabled = true;
		    }
		}
	}
	
	function onMessageFromSearchEndowments()
	{
	  
	  disableInputs();
	  document.getElementById("detailsFrm:onMessageFromSearchEndowments").onclick();
	}
	function populateAsset(assetDesc,nameAr,nameEn,assetType,fileNumber,personName,assetId,status)
	{
	  disableInputs();
	  document.getElementById("detailsFrm:onMessageFromSearchAsset").onclick();
	}
	function   openSearchAssetsPopup()
	{
		
	   var screen_width   = 0.65 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   window.open('SearchAssets.jsf?pageMode=MODE_SELECT_ONE_POPUP&context=revenueFollowup','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	}
		
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<c:choose>
						<c:when test="${!pages$revenueFollowup.viewModePopup}">
							<tr
								style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>


					<tr width="100%">
						<c:choose>
							<c:when test="${!pages$revenueFollowup.viewModePopup}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{pages$revenueFollowup.pageTitle}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>
																<h:outputText id="errorId"
																	value="#{pages$revenueFollowup.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$revenueFollowup.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$revenueFollowup.pageMode}"></h:inputHidden>
																<h:commandLink id="onMessageFromSearchAsset"
																	action="#{pages$revenueFollowup.onMessageFromSearchAsset}" />
																<h:commandLink id="onMessageFromSearchEndowments"
																	action="#{pages$revenueFollowup.onMessageFromSearchEndowments}" />
															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<t:div rendered="true" style="width:100%;">
														<t:panelGrid cellpadding="1px" width="100%"
															cellspacing="5px" columns="4">

															<h:outputLabel styleClass="LABEL"
																value="#{msg['revenueFollowup.lbl.RefNum']}:" />
															<h:inputText id="txtFileNumber" readonly="true"
																value="#{pages$revenueFollowup.pageObject.refNum}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.status']}:" />
															<h:inputText id="txtFileStatus"
																value="#{pages$revenueFollowup.englishLocale? 
																	     pages$revenueFollowup.pageObject.status.dataDescEn:
																	     pages$revenueFollowup.pageObject.status.dataDescAr
																	    }"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.createdBy']}:" />
															<h:inputText id="txtFileCreatedB" readonly="true"
																value="#{pages$revenueFollowup.pageObject.createdByName}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.createdOn']}:" />
															<h:inputText id="txtFileCreatedOn" readonly="true"
																value="#{pages$revenueFollowup.pageObject.formattedCreatedOn}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['revenueFollowup.lbl.followupOn']}:" />
															<rich:calendar id="txtfollowupOn"
																disabled="#{!pages$revenueFollowup.showSaveButton}"
																value="#{pages$revenueFollowup.pageObject.followUpOn}"
																locale="#{pages$revenueFollowup.locale}" popup="true"
																datePattern="#{pages$revenueFollowup.dateFormat}"
																showApplyButton="false" enableManualInput="false"
																cellWidth="24px" cellHeight="22px" style="height:16px" />

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" />
																<h:outputLabel id="lblendowmentAssets"
																	styleClass="LABEL"
																	value="#{msg['revenueFollowup.lbl.endowmentAssets']}" />
															</h:panelGroup>
															<h:panelGroup>
																<h:inputText id="endowmentselectd" maxlength="20"
																	readonly="true" styleClass="READONLY"
																	value="#{pages$revenueFollowup.pageObject.endowment.endowmentNum}" />
																<h:graphicImage id="endowmentSearchpopup"
																	rendered="#{pages$revenueFollowup.showSaveButton}"
																	style="margin-right:5px;"
																	title="#{msg['commons.search']}"
																	url="../resources/images/magnifier.gif"
																	onclick="javaScript:openSearchEndowmentsPopup('revenueFollowup');" />
															</h:panelGroup>

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" />
																<h:outputLabel id="lblBenef" styleClass="LABEL"
																	value="#{msg['revenueFollowup.lbl.minorAssets']}" />
															</h:panelGroup>
															<h:panelGroup>
																<h:inputText id="assetselected" maxlength="20"
																	readonly="true" styleClass="READONLY"
																	value="#{pages$revenueFollowup.pageObject.assetMems.assetNumber}" />
																<h:graphicImage id="endowmentProgramsSearchpopup"
																	rendered="#{pages$revenueFollowup.showSaveButton}"
																	style="margin-right:5px;"
																	title="#{msg['commons.search']}"
																	url="../resources/images/magnifier.gif"
																	onclick="javaScript:openSearchAssetsPopup();" />
															</h:panelGroup>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['revenueFollowup.lbl.units']}:" />
															<h:selectOneMenu id="cmbUnits"
																disabled="#{!pages$revenueFollowup.showSaveButton}"
																value="#{pages$revenueFollowup.pageObject.thirdPartyPropUnitsId}"
																tabindex="3">
																<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$revenueFollowup.thirdPartyUnits}" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['revenueFollowup.lbl.expectedRevenue']}:" />
															<h:inputText id="txtBudget"
																readonly="#{!pages$revenueFollowup.showSaveButton}"
																value="#{pages$revenueFollowup.pageObject.expRevenueStr}"
																onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['revenueFollowup.lbl.contactName']}:" />
															<h:inputText id="contactName"
																readonly="#{!pages$revenueFollowup.showSaveButton}"
																value="#{pages$revenueFollowup.pageObject.contactName}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['person.contactNumber']}:" />
															<h:inputText id="contactNo"
																readonly="#{!pages$revenueFollowup.showSaveButton}"
																value="#{pages$revenueFollowup.pageObject.contactNumber}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.address']}:" />
															<h:inputText id="contactAdd"
																readonly="#{!pages$revenueFollowup.showSaveButton}"
																value="#{pages$revenueFollowup.pageObject.contactAddress}" />


															<h:outputLabel styleClass="LABEL"
																value="#{msg['revenueFollowup.lbl.remarks']}" />
															<t:panelGroup colspan="3" style="width: 100%">
																<t:inputTextarea style="width: 90%;"
																	id="txt_openingRemarks"
																	readonly="#{!pages$revenueFollowup.showSaveButton}"
																	value="#{pages$revenueFollowup.pageObject.openingRemarks}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</t:panelGroup>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['revenueFollowup.lbl.closingRemarks']}"
																rendered="#{pages$revenueFollowup.showClosingRemarks}" />
															<t:panelGroup colspan="3" style="width: 100%">
																<t:inputTextarea style="width: 90%;"
																	id="txt_evalremarks"
																	rendered="#{pages$revenueFollowup.showClosingRemarks}"
																	value="#{pages$revenueFollowup.pageObject.closingRemarks}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</t:panelGroup>

														</t:panelGrid>
													</t:div>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$revenueFollowup.tabPanel}"
																style="width: 100%">

																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>

																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>

																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$revenueFollowup.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">
															<pims:security
																screen="Pims.RevenueFollowupMgmt.RevenueFollowup.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$revenueFollowup.showSaveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$revenueFollowup.onSave}" />

																<h:commandButton styleClass="BUTTON" id="btnClose"
																	rendered="#{pages$revenueFollowup.showClose}"
																	value="#{msg['commons.closeButton']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkClose"
																	action="#{pages$revenueFollowup.onClose}" />


																<h:commandButton styleClass="BUTTON"
																	id="btnNextFollowup" style="width:120px;"
																	binding="#{pages$revenueFollowup.btnNextFollowup}"
																	value="#{msg['revenueFollowup.lbl.initiateNextFollowup']}"
																	onclick="performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkNextFollowup"
																	action="#{pages$revenueFollowup.onInitiateNextRevFollowup}" />

															</pims:security>

														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<c:choose>
								<c:when test="${!pages$revenueFollowup.viewModePopup}">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
								</c:when>
							</c:choose>


						</td>
					</tr>
				</table>
			</div>
			+
		</body>
	</html>
</f:view>