<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html style="overflow:hidden;" dir="${sessionScope.CurrentLocale.dir}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache" />
			<meta http-equiv="cache-control" content="no-cache" />
			<meta http-equiv="expires" content="0" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />


			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table_ff}"/>" type="text/css" rel="stylesheet" />
			<![endif]
			-->

		</head>

		<body class="BODY_STYLE"
			lang="${sessionScope.CurrentLocale.languageCode}">
          <div class="containerDiv">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table  class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel id="otPicklistHeader"
										value="#{msg['admin.picklist.header']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>

						<table id="tbl1213212" width="99%" 
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" >

							<tr valign="top">
								
								<!--Use this below only for desiging forms Please do not touch other sections -->


								<td width="99%" height="100%" valign="top" nowrap="nowrap">
									<div id="divwww1213212" class="SCROLLABLE_SECTION" style="height:450px" >
										<%
											System.out.println("Page Context :"
														+ pageContext.getAttribute("permissionValueId"));
										%>
										<%
											System.out.println("Request      :"
														+ request.getAttribute("permissionValueId"));
										%>
										<%
											System.out.println("Session      :"
														+ pageContext.getAttribute("permissionValueId"));

												String permissionId = (String) request
														.getAttribute("permissionValueId");
										%>

										<h:inputHidden id="selectedPicklistID"
											value="#{pages$PickListController.selectedPicklist}" />
										<h:form id="picklist" style="width: 98%">
											<div id="divErrorMessages" style="height: 25px">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<a4j:outputPanel id="errorMessages">
																<h:messages styleClass="INFO_FONT" />
															</a4j:outputPanel>
														</td>
													</tr>
												</table>
											</div>
											<div id="divMargin" class="MARGIN">
												<table id="tblheadermid" cellpadding="0" cellspacing="0" width="100%">
													<tr style="FONT-SIZE: 0px">
														<td style="FONT-SIZE: 0px">
															<IMG id="imgleft" class="TAB_PANEL_LEFT" 
																src="../<h:outputText value="#{path.img_section_left}"/>"
																/>
														</td>
														<td width="100%" style="FONT-SIZE: 0px" >
															<IMG id="imgmid" class="TAB_PANEL_MID" 
																src="../<h:outputText  value="#{path.img_section_mid}"/>"
																/>
														</td>
														<td style="FONT-SIZE: 0px"> 
															<IMG id="imgright" class="TAB_PANEL_RIGHT" 
																src="../<h:outputText value="#{path.img_section_right}" />"
																 />
														</td>
													</tr>
												</table>
												<t:div id="divDetailSection2" styleClass="DETAIL_SECTION">
												
												
													<h:outputLabel id="outLabelHeader"
														value="#{msg['admin.picklist.header']}"
														styleClass="DETAIL_SECTION_LABEL">
													</h:outputLabel>
													<t:div id="div11123wweeeeee">

														<t:div id="addPanelId"
															>

															<table id="tbl1234" cellpadding="1px" cellspacing="2px"
																width="100%" class="DETAIL_SECTION_INNER">
																<tr>
																	<td width="5%">&nbsp;</td>
																	<td width="20%">
																		<h:outputText id="otadminheader"
																			value="#{msg['admin.picklist.header']}" />
																	</td>
																	<td width="20%">
																		<h:selectOneMenu id="picklistSelectOneMenu"
																			style="width: 200px;"
																			binding="#{pages$PickListController.picklistTypesSelectOneMenu}"
																			valueChangeListener="#{pages$PickListController.selectPicklist}"
																			onchange="submit();">
																			<f:selectItem id="selectNoneType" itemValue="-1"
																				itemLabel="#{msg['commons.none']}" />
																			<f:selectItems
																				value="#{pages$PickListController.picklistTypes}" />
																		</h:selectOneMenu>

																	</td>
																</tr>																
																<tr>
																	<td width="5%">&nbsp;</td>
																	<td width="20%">
																		<h:outputText id="otadmindataen"
																			value="#{msg['admin.picklist.dataen']}"
																			rendered="#{pages$PickListController.dataEn.rendered}" />
																	</td>
																	<td width="20%">
																		<h:inputText id="dataEnID"
																			binding="#{pages$PickListController.dataEn}"
																			value="#{pages$PickListController.strdataEn}"
																			style="text-align: right;"
																			readonly="#{pages$PickListController.flag}"></h:inputText>
																	</td>
																	<td width="10%">&nbsp;</td>
																	<td width="20%">
																		<h:outputText id="otadmindataar"
																			value="#{msg['admin.picklist.dataar']}" 
																			rendered="#{pages$PickListController.dataAr.rendered}" />
																	</td>
																	<td width="20%">
																		<h:inputText id="dataArID"
																			binding="#{pages$PickListController.dataAr}"
																			value="#{pages$PickListController.strdataAr}"
																			style="text-align: right;"
																			readonly="#{pages$PickListController.flag}"></h:inputText>
																	</td>
																	<td width="5%">&nbsp;</td>
																</tr>
																
																<tr>
																	<td colspan="7" class="BUTTON_TD JUG_BUTTON_TD">

																			<h:commandButton id="btnAddType" styleClass="BUTTON"
																				value="#{msg['commons.Add']}"
																				action="#{pages$PickListController.addPicklistData}"
																				style="width: 75px" tabindex="7"
																				binding="#{pages$PickListController.btnAddType}">
																			</h:commandButton>

																			<h:commandButton id="btnSaveType" styleClass="BUTTON"
																				value="#{msg['commons.saveButton']}"
																				actionListener="#{pages$PickListController.savePicklistData}"
																				style="width: 75px" tabindex="7"
																				binding="#{pages$PickListController.btnSaveType}">
																			</h:commandButton>

																	</td>

																</tr>
															</table>

														</t:div>
													</t:div>

												</t:div>
												<t:div id="picklistDataDiv"
													style="padding-bottom:7px;padding-left:0px;padding-right:0px;padding-top:7px;">

													<div class="imag">
														&nbsp;
													</div>

													<t:div id="picklistDataTablediv1" styleClass="contentDiv">
														<t:dataTable id="picklistDataTable"
															binding="#{pages$PickListController.dataTable}"
															var="picklistDataItem"
															value="#{pages$PickListController.selectedPicklistData}"
															style="width: 100%;"
															preserveDataModel="false"
															preserveSort="false"
															rules="all"
															renderedIfEmpty="true"
															rows="#{pages$PickListController.paginatorRows}"
															rowClasses="row1, row2">
															<t:column id="dataPrimaryName"
																style="padding: 0px;border: 0px;text-align: center;">
																<f:facet name="header">
																	<h:outputText id="ot1213skadj"
																		value="#{msg['admin.picklist.dataen']}"
																		styleClass="grid_column_header_font"
																		style="font-size: 11px" />
																</f:facet>

																<h:outputText id="plistPrimary"
																	value="#{picklistDataItem.displayPrimary}"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="dataSecondaryName"
																style="padding: 0px;border: 0px;text-align: center;">
																<f:facet name="header">
																	<h:outputText value="#{msg['admin.picklist.dataar']}" />
																</f:facet>
																<h:outputText id="plistSecondary"
																	value="#{picklistDataItem.displaySecondary}"
																	styleClass="A_LEFT" />
															</t:column>
															
															<t:column id="deleteIdCol111"
																style="padding: 0px;border: 0px;text-align: center;">
																<f:facet name="header">
																	<h:outputText id="ot1213skadjsss"
																		value="#{msg['commons.action']}"
																		styleClass="grid_column_header_font"
																		style="font-size: 11px" />
																</f:facet>

																<pims:security screen="<%=permissionId%>"
																	action="update">
																	<h:commandLink id="editrecordId"
																		action="#{pages$PickListController.editPicklistData}">
																		<h:graphicImage id="gilskdjlkj323423"
																			title="#{msg['commons.edit']}"
																			url="../resources/images/edit-icon.gif" />
																	</h:commandLink>
																
																			</pims:security>
																<h:outputText value=" "></h:outputText>
<%--  Commented :bcz there might be an item which is being referenced in another table--%>
<%--
																<pims:security screen="<%=permissionId%>"
																	action="delete">
																	<h:commandLink id="deleterecordId11"
																		action="#{pages$PickListController.deletePicklistData}">

																		<h:graphicImage id="asd31323"
																			title="#{msg['commons.delete']}"
																			url="../resources/images/delete.gif" />

																	</h:commandLink>
																</pims:security>
--%>
															</t:column>
														</t:dataTable>
													</t:div>
													
													<t:div styleClass="contentDivFooter" style="width:100%">
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$PickListController.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
												<CENTER>
											<t:dataScroller id="scroller" for="picklistDataTable" paginator="true"
												fastStep="1" paginatorMaxPages="#{pages$PickListController.paginatorMaxPages}" immediate="false"
												paginatorTableClass="paginator"
												
												renderFacetsIfSinglePage="true" 
												paginatorTableStyle="grid_paginator" layout="singleTable" 
												paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
												styleClass="SCH_SCROLLER"
												pageIndexVar="pageNumber"
												paginatorActiveColumnStyle="font-weight:bold;"
												paginatorRenderLinkForActive="false">

												<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
												<t:div styleClass="PAGE_NUM_BG">
												
													<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>		
											</t:dataScroller>
											</CENTER>
													
												</td></tr>
											</table>
										</t:div>  
													
													
												</t:div>
											</div>
										</h:form>
									</div>
								</td>

							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>

	</html>
</f:view>