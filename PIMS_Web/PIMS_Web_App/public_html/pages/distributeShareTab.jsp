<%-- Author: Kamran Ahmed--%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<t:div style="height:240px;overflow-y:scroll;width:100%;">
	<script language="javascript" type="text/javascript">
function populateDistributionDetails( )
{
onProcessStart();
	 document.getElementById("inheritanceFileForm:lnkResponseFromDistributionPopup").onclick();
}
function onMessageFromSplitPopup()
{
	 onProcessStart();
	 document.getElementById("inheritanceFileForm:lnkResponseFromSplitPopup").onclick();
}
</script>

	<a4j:commandLink id="lnkResponseFromDistributionPopup"
		oncomplete="onProcessComplete();"
		action="#{pages$distributeShareTab.onMessageFromDistributePopUp}"
		reRender="distributeSharesTabDiv,successMessages" />
	<a4j:commandLink id="lnkResponseFromSplitPopup"
		oncomplete="onProcessComplete();"
		action="#{pages$distributeShareTab.onMessageFromSplitPopUp}"
		reRender="distributeSharesTabDiv,successMessages" />

	<t:div style="height:5px;"></t:div>
	<t:div style="width:95%;" id="ChangeAssetdiv">
		<t:panelGrid border="0" width="95%" cellpadding="1" cellspacing="1"
			columns="4">

			<h:outputLabel styleClass="LABEL"
				value="#{msg['collectionProcedure.assetName']}:"></h:outputLabel>
			<h:selectOneMenu id="distCrit"
				value="#{pages$distributeShareTab.selectOneAssetId }" tabindex="3">

				<f:selectItems value="#{pages$distributeShareTab.iaList}" />


			</h:selectOneMenu>
		</t:panelGrid>
		<t:div styleClass="A_RIGHT" style ="margin-bottom:12px;">
			<h:commandButton id="btnChangeAsset" value="#{msg['commons.change']}"
				action="#{pages$distributeShareTab.onChangeAsset}"
				styleClass="BUTTON" style="width:10%;"></h:commandButton>

			<h:commandButton id="btnOpenDistributedCollections"
				value="#{msg['distributeShares.lbl.distributedCollections']}"
				action="#{pages$distributeShareTab.openDistributedCollectionsPopUp}"
				styleClass="BUTTON" style="width:auto;margin-left:12px;margin-right:12px;"></h:commandButton>

		</t:div>
	</t:div>
	<t:div id="distributeSharesTabDiv" styleClass="contentDiv"
		style="width:95%">
		<t:dataTable id="distributeDataList" preserveDataModel="true"
			preserveSort="false" var="dataItem" rowClasses="row1,row2"
			rules="all" renderedIfEmpty="true"
			value="#{pages$distributeShareTab.distributionList}"
			binding="#{pages$distributeShareTab.dataTable}" width="100%">

			<t:column sortable="true" width="2%">
				<f:facet name="header">
					<h:outputText value="" />
				</f:facet>
				<h:selectBooleanCheckbox value="#{dataItem.selected}" />
			</t:column>

			<t:column sortable="true" width="25%">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.refNum']}" />
				</f:facet>
				<t:outputText style="white-space: normal;"
					value="#{dataItem.refNum}" />
			</t:column>
			<t:column sortable="true" width="5%">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.reasons']}" />
				</f:facet>
				<t:div style="white-space: normal;word-wrap: break-word;width:95%;">

					<t:outputText style="white-space: normal;"
						value="#{dataItem.reason}" />
				</t:div>
			</t:column>
			<t:column sortable="true" width="6%">
				<f:facet name="header">
					<h:outputText
						value="#{msg['ReviewAndConfirmDisbursements.amount']}" />
				</f:facet>
				<t:outputText style="white-space: normal;"
					value="#{dataItem.amount}">
					<f:convertNumber maxIntegerDigits="15" maxFractionDigits="2"
						pattern="##############0.00" />

				</t:outputText>
			</t:column>

			<t:column sortable="true" width="5%">
				<f:facet name="header">
					<h:outputText value="#{msg['fieldsName.collectedOn']}" />
				</f:facet>
				<t:outputText style="white-space: normal;"
					value="#{dataItem.collectedOn}" />
			</t:column>

			<t:column sortable="true" width="5%">
				<f:facet name="header">
					<h:outputText value="#{msg['fieldsName.collectedAs']}" />
				</f:facet>
				<t:outputText style="white-space: normal;"
					value="#{pages$distributeShareTab.englishLocale?dataItem.collectedAsEn:dataItem.collectedAsAr}" />
			</t:column>

			<t:column sortable="true" width="15%">
				<f:facet name="header">
					<h:outputText value="#{msg['assetSearch.assetName']}" />
				</f:facet>
				<t:outputText style="white-space: normal;"
					value="#{pages$distributeShareTab.englishLocale?dataItem.assetNameEn:dataItem.assetNameAr}" />
			</t:column>

			<t:column sortable="true" width="15%">
				<f:facet name="header">
					<h:outputText value="#{msg['searchAssets.assetType']}" />
				</f:facet>
				<t:outputText style="white-space: normal;"
					value="#{pages$distributeShareTab.englishLocale?dataItem.assetTypeEn:dataItem.assetTypeAr}" />
			</t:column>

			<t:column id="actioCol" sortable="false">
				<f:facet name="header">
					<t:outputText
						value="#{msg['mems.inheritanceFile.columnLabel.action']}" />
				</f:facet>
				<t:commandLink
					action="#{pages$distributeShareTab.onOpenDistributePopUp}">
					<h:graphicImage rendered="#{dataItem.showAddBenefIcon=='1'}"
						style="cursor:hand;"
						title="#{msg['commons.distributeAmongBeneficiaries']}"
						url="../resources/images/app_icons/Add-New-Tenant.png" />
				</t:commandLink>
				<t:commandLink
					onclick="if (!confirm('#{msg['confirmMsg.areuSureToDistribute']}')) return false;else return onDistributedCollection(this);">

					<h:graphicImage rendered="#{dataItem.showDistributeIcon=='1'}"
						id="imgDistributeImg" style="cursor:hand;"
						title="#{msg['commons.distribute']}"
						url="../resources/images/app_icons/Add-fee.png" />
				</t:commandLink>
				<t:commandLink id="lnkDistributeImg"
					action="#{pages$distributeShareTab.onDistribute}" />
				<t:commandLink action="#{pages$distributeShareTab.openSplitPopUp}">
					<h:graphicImage rendered="#{dataItem.showDistributeIcon=='1'}"
						style="cursor:hand;"
						title="#{msg['collectionProcedure.lbl.SplitPayment']}"
						url="../resources/images/app_icons/replaceCheque.png" />
				</t:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>
</t:div>