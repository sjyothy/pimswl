<%@page import="com.avanza.core.constants.CoreConstants"%>
<%@page import="com.avanza.core.security.User"%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ page import="javax.faces.component.UIViewRoot,java.util.Date;"%>



<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>

<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
	var="msg" />

<table class="header" cellpadding="0" cellspacing="0" border="0"
	width="100%">
	<tr>
		
		<td width="530px">
			<div class="LOGIN_DATE">
				<%
					User user = (User) request.getSession().getAttribute(
							CoreConstants.CurrentUser);
				%>
				<b> <%=ResourceUtil.getInstance()
							.getProperty("header.logininfo")%> </b>
				<%=user.getFirstName()%>

				&nbsp;&nbsp;&nbsp;&nbsp;
				<b><%=ResourceUtil.getInstance().getProperty("header.date")%></b>
				<%=new Date()%>
			</div>
		</td>
		
	</tr>
</table>
<table class="header1" cellpadding="0" cellspacing="0" border="0"
	width="100%">
	<tr>
		<td>
			<img
				src="../<%=ResourceUtil.getInstance().getPathProperty(
							"img_blackframeleft1")%>"
				style="height: 27px;" />
		</td>
		<td width="982">
			<table cellpadding="0" cellspacing="0" class="HEADER_ICONS">
				<tr>
					<td class="BUTTON_TD">

						<img onclick="javaScript:openReleaseTasksPopup();"
							src="../resources/images/app_icons/Release.png" />

					</td>
					<td width="5%">
						<a href="logout.jsf"> <img
								src="../resources/images/Left Panel/picture_go.png"> </a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
