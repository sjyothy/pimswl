<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->


			<script language="JavaScript" type="text/javascript"
				src="../resources/jscripts/commons.js"></script>


			<script language="JavaScript" type="text/javascript">

			
			function responseFromReadEIDDataScreen(context)
			{
				if( context=='addperson' )
				{
					document.getElementById("formSearchPerson:onMessageFromReadEIDData").onclick();
				}
			}			
			function disableButtons(control)
			{
					
					var inputs = document.getElementsByTagName("INPUT");
					for (var i = 0; i < inputs.length; i++) {
					    if ( inputs[i] != null &&
					         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
					        )
					     {
					        inputs[i].disabled = true;
					    }
					}
					
					document.getElementById("formSearchPerson:lnkgenerateLoginId").onclick();
					//control.nextSibling.onclick();
					
			}
			
	function validate() 
	{
             maxlength=100;
             if(document.getElementById("formSearchPerson:txt_removalReason").value.length>=maxlength) 
             {
              document.getElementById("formSearchPerson:txt_removalReason").value=
              document.getElementById("formSearchPerson:txt_removalReason").value.substr(0,maxlength);  
             }
    }
    function clearWindow()
	{
       
        
            document.getElementById("formSearchPerson:txtfirstname").value="";
			document.getElementById("formSearchPerson:txtlastname").value="";	
			document.getElementById("formSearchPerson:txtpassportNumber").value="";
			document.getElementById("formSearchPerson:txtresidenseVisaNumber").value="";
			document.getElementById("formSearchPerson:txtsocialSecNumber").value="";
		
			
	}
	function addToContract()
	{
	   
	   
	  window.opener.document.forms[0].submit();
	  
	  window.close();
	}
	function sendToParent(PersonName,PersonId)
	{
	var controlName=document.getElementById("formSearchPerson:hdnControlName").value;
	
	var controlForId=document.getElementById("formSearchPerson:hdncontrolForId").value;
	var hdnDisplaycontrol=document.getElementById("formSearchPerson:hdndisplaycontrolName").value;
	window.opener.addSponsorManager(controlName,controlForId,PersonName,PersonId,hdnDisplaycontrol);
	window.close();
	}
	function closeWindow()
	{
	
	  window.opener="x";
	  window.close();
	
	}
	
	  function submitForm()
  		   {
           document.getElementById('formSearchPerson').submit();
		   }
       </SCRIPT>



		</head>
		<body class="BODY_STYLE">
			<c:choose>
				<c:when test="${!pages$AddPerson.isViewModePopUp}">
					<div class="containerDiv">
				</c:when>
			</c:choose>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
						<c:when test="${!pages$AddPerson.isViewModePopUp}">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</c:when>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!pages$AddPerson.isViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['addPerson.Header']}"
										styleClass="HEADER_FONT"
										rendered="#{!pages$AddPerson.isReadonlyMode}" />
									<h:outputLabel value="#{msg['viewPerson.Header']}"
										styleClass="HEADER_FONT"
										rendered="#{pages$AddPerson.isReadonlyMode}" />
								</td>

							</tr>

						</TABLE>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="HEIGHT: 450px">
										<h:form id="formSearchPerson" style="width:96%;">

											<table border="0" class="layoutTable" width="100%">
												<tr>
													<td>
														<h:outputText id="Errors" styleClass="ERROR_FONT"
															escape="false" value="#{pages$AddPerson.errorMessages}" />
														<h:outputText id="MSGs" styleClass="INFO_FONT"
															escape="false" value="#{pages$AddPerson.successMessages}" />
														<h:inputHidden id="pageMode"
															value="#{pages$AddPerson.pageMode}" />
														<h:inputHidden id="persontypeId"
															value="#{pages$AddPerson.personTypeId}" />
														<h:inputHidden id="persontypeReq"
															value="#{pages$AddPerson.requestForPersonType}" />
														<h:inputHidden id="personId"
															value="#{pages$AddPerson.personId}" />
														<h:inputHidden id="contactInfoId"
															value="#{pages$AddPerson.contactInfoId}" />
														<h:inputHidden id="edit" value="#{pages$AddPerson.edit}" />
														<h:inputHidden id="synchedWithEID"
															value="#{pages$AddPerson.synchedWithEID}" />
														<h:inputHidden id="ef_idn_cn"
															value="#{pages$AddPerson.ef_idn_cn}" />
														<h:inputHidden id="ef_mod_data"
															value="#{pages$AddPerson.ef_mod_data}" />
														<h:inputHidden id="ef_non_mod_data"
															value="#{pages$AddPerson.ef_non_mod_data}" />
														<h:inputHidden id="ef_sign_image"
															value="#{pages$AddPerson.ef_sign_image}" />
														<h:inputHidden id="ef_home_address"
															value="#{pages$AddPerson.ef_home_address}" />
														<h:inputHidden id="ef_work_address"
															value="#{pages$AddPerson.ef_work_address}" />
														<h:commandLink id="onMessageFromReadEIDData"
															action="#{pages$AddPerson.onMessageFromReadEIDData}"></h:commandLink>
													</td>
												</tr>
											</table>
											<div class="MARGIN">
												<t:panelGrid columns="4" cellpadding="0px" cellspacing="0px">
													<h:panelGroup>
														<h:outputLabel styleClass="mandatory" value="*" />
														<h:outputLabel styleClass="LABEL"
															value="#{msg['person.Type']}:"></h:outputLabel>
													</h:panelGroup>
													<h:selectOneMenu id="selectPersonKind"
														binding="#{pages$AddPerson.cmb_SelectedPersonKind}"
														value="#{pages$AddPerson.selectedPersonKind}"
														readonly="#{pages$AddPerson.isReadonlyMode}">
														<f:selectItems
															value="#{pages$ApplicationBean.personKindList}" />
														<a4j:support event="onchange"
															reRender="one1,tabPersonIndividual,tabPersonCompany"
															action="#{pages$AddPerson.selectPersonKind_Changed}" />
													</h:selectOneMenu>
													<h:outputLabel rendered="false" styleClass="LABEL"
														value="#{msg['commons.status']}:" />
													<h:selectOneMenu rendered="false" id="selectPersonStatus"
														value="#{pages$AddPerson.statusId}"
														readonly="#{pages$AddPerson.isReadonlyMode}">
														<f:selectItem
															itemLabel="#{msg['commons.combo.PleaseSelect']}"
															itemValue="-1" />
														<f:selectItems
															value="#{pages$ApplicationBean.personStatus}" />
													</h:selectOneMenu>
													<h:outputLabel styleClass="LABEL"
														value="#{msg['addPerson.grpNumber']}:" />
													<h:inputText
														styleClass="A_LEFT #{pages$AddPerson.readOnlyStyleClass}"
														style="width:150px;" maxlength="100" id="txtCustomerNo"
														value="#{pages$AddPerson.grpCustomerNo}"
														readonly="#{pages$AddPerson.isGrpCustomerNumberReadOnly}"></h:inputText>
														<%--  
													<h:outputLabel styleClass="LABEL"
														value="#{msg['addPerson.loginId']}:" />
													<h:inputText
														styleClass="A_LEFT #{pages$AddPerson.readOnlyStyleClass}"
														style="width:150px;" maxlength="100" id="txtloginId"
														value="#{pages$AddPerson.loginId}"
														readonly="true"></h:inputText>		
														 --%>											
													<t:panelGroup>
														<h:outputLabel styleClass="LABEL"
															value="#{msg['contract.unit.isBlackListed']}:" />
														<h:selectBooleanCheckbox
															style="width: 15%;height: 22px; border: 0px;"
															value="#{pages$AddPerson.blackListed}">

														</h:selectBooleanCheckbox>
													</t:panelGroup>

												</t:panelGrid>

												<table width="100%" cellpadding="0" cellspacing="0">

													<tr style="padding-bottom: 5px;">
														<td class="BUTTON_TD">
															<h:commandButton id="btnSavePerson" styleClass="BUTTON"
																value="#{msg['commons.saveButton']}"
																action="#{pages$AddPerson.btnSavePerson_Click}"
																rendered="#{!pages$AddPerson.isReadonlyMode}" />

															<h:commandButton id="btnGenerateGRPCustomerNumber"
																styleClass="BUTTON"
																value="#{msg['addPerson.generateGRP']}"
																style="width: auto;"
																action="#{pages$AddPerson.btnGenerateGRPCustomerNumber_Click}"
																rendered="#{!pages$AddPerson.isReadonlyMode && !pages$AddPerson.isGrpCustomerNumberReadOnly}" />

															<h:commandButton type="button" styleClass="BUTTON"
																style="width:auto;"
																value="#{msg['searchPerson.lbl.scanEmiratesCard']}"
																onclick="javaScript:openReadEIDDataPopup('addperson');"></h:commandButton>



														<!--  	<h:commandButton id="generateLoginId" styleClass="BUTTON"
																style="width: 110px"
																value="#{msg['commons.generateLoginId']}"
																onclick="disableButtons(this);"
																rendered="#{pages$AddPerson.isGenerateLogin}" />-->

															<h:commandLink id="lnkgenerateLoginId"
																action="#{pages$AddPerson.generateLoginId}" />

															<h:commandButton id="btnBack" styleClass="BUTTON"
																value="#{msg['commons.back']}"
																action="#{pages$AddPerson.btnBack_Click}"
																rendered="#{!pages$AddPerson.isReadonlyMode}" />
															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.closeButton']}"
																onclick="javascript:window.close();"
																style="width: 75px;"
																rendered="#{pages$AddPerson.isReadonlyMode && pages$AddPerson.isViewModePopUp}"></h:commandButton>

														</td>
													</tr>

												</table>


												<table id="table_1" cellpadding="0" cellspacing="0"
													width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>

												<rich:tabPanel id="one1" style="height:200px;"
													headerSpacing="0">
													<%--  Individual Tab Start --%>
													<rich:tab id="tabPersonIndividual"
														label="#{msg['person.personinformation']}"
														binding="#{pages$AddPerson.tabPersonIndividual}">
														<t:div style="height:240px;overflow-y:scroll;width:100%;">
															<t:panelGrid columns="4" cellpadding="1px"
																cellspacing="2px" styleClass="TAB_DETAIL_SECTION_INNER">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.title']}:"></h:outputLabel>
																<h:selectOneMenu id="selectTitle"
																	readonly="#{pages$AddPerson.isReadonlyMode}"
																	value="#{pages$AddPerson.titleId}">
																	<f:selectItems value="#{pages$ApplicationBean.title}" />
																</h:selectOneMenu>
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['customer.firstname']}:"></h:outputLabel>
																</h:panelGroup>
																<h:inputText maxlength="500" id="txtfirstName"
																	value="#{pages$AddPerson.firstName}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.middlename']}:"></h:outputLabel>
																<h:inputText maxlength="500" id="txtmiddleName"
																	value="#{pages$AddPerson.middleName}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['customer.lastname']}:"></h:outputLabel>
																</h:panelGroup>
																<h:inputText maxlength="500" id="txtlastName"
																	value="#{pages$AddPerson.lastName}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['person.fullName.en']}:"></h:outputLabel>
																<h:inputText maxlength="500" id="txtfullNameEn"
																	value="#{pages$AddPerson.fullNameEn}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.gender']}:"></h:outputLabel>
																<h:selectOneMenu id="selectGender"
																	value="#{pages$AddPerson.gender}"
																	readonly="#{pages$AddPerson.isReadonlyMode}">
																	<f:selectItem itemLabel="#{msg['tenants.gender.male']}"
																		itemValue="M" />
																	<f:selectItem
																		itemLabel="#{msg['tenants.gender.female']}"
																		itemValue="F" />
																</h:selectOneMenu>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenants.maritalstatus']}:"></h:outputLabel>
																<h:selectOneMenu id="selectmaritialStatus"
																	value="#{pages$AddPerson.maritialStatusId}"
																	readonly="#{pages$AddPerson.isReadonlyMode}">
																	<f:selectItem
																		itemLabel="#{msg['commons.combo.PleaseSelect']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.maritialStatus}" />
																</h:selectOneMenu>


																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.dateOfBirth']}:" />

																<rich:calendar id="DateOfBirth"
																	value="#{pages$AddPerson.dateOfBirth}"
																	disabled="#{pages$AddPerson.isReadonlyMode}"
																	popup="true"
																	datePattern="#{pages$AddPerson.dateFormat}"
																	showApplyButton="false"
																	locale="#{pages$AddPerson.locale}"
																	enableManualInput="false"
																	inputStyle="width: 170px; height: 14px" />
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['customer.cell']}:" />
																</h:panelGroup>
																<h:inputText styleClass="A_RIGHT_NUM" maxlength="15"
																	id="txtcellNo" value="#{pages$AddPerson.cellNumber}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.secondCell']}:" />
																<h:inputText styleClass="A_RIGHT_NUM"
																	style="width:152px;" maxlength="15" id="txtSeccellNo"
																	value="#{pages$AddPerson.secCellNumber}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['customer.nationality']}:"></h:outputLabel>
																</h:panelGroup>
																<h:selectOneMenu id="nationality" immediate="false"
																	style="WIDTH: 157px"
																	value="#{pages$AddPerson.nationalityId}"
																	readonly="#{pages$AddPerson.isReadonlyMode}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems value="#{pages$AddPerson.countryList}" />
																</h:selectOneMenu>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.nationality']}#:"></h:outputLabel>
																<h:inputText maxlength="15" id="txtpersonalSecCardNo"
																	value="#{pages$AddPerson.personalSecCardNo}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['customer.socialSecNumber']}:"></h:outputLabel>
																</h:panelGroup>

																<h:inputText maxlength="15" id="txtsocialSecNumber"
																	value="#{pages$AddPerson.socialSecNumber}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.passport.issueplace']}:"></h:outputLabel>
																<h:selectOneMenu id="passportIssuePlace"
																	immediate="false"
																	value="#{pages$AddPerson.passportIssuePlaceId}"
																	readonly="#{pages$AddPerson.isReadonlyMode}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems value="#{pages$AddPerson.countryList}" />
																	<a4j:support event="onchange"
																		action="#{pages$AddPerson.onPassportCountryChanged}"
																		reRender="passportIssueCity" />
																</h:selectOneMenu>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['person.passportIssueCity']}:"></h:outputLabel>
																<h:selectOneMenu id="passportIssueCity"
																	immediate="false"
																	value="#{pages$AddPerson.passportIssueCity}"
																	readonly="#{pages$AddPerson.isReadonlyMode}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems
																		value="#{view.attributes['passportCityList']}" />

																</h:selectOneMenu>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.passportNumber']}:"></h:outputLabel>
																<h:inputText maxlength="15" id="txtpassportNumber"
																	value="#{pages$AddPerson.passportNumber}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.passportIssueDate']}:"></h:outputLabel>
																<rich:calendar id="passportIssueDate"
																	value="#{pages$AddPerson.passportIssueDate}"
																	disabled="#{pages$AddPerson.isReadonlyMode}"
																	popup="true"
																	datePattern="#{pages$AddPerson.dateFormat}"
																	showApplyButton="false"
																	locale="#{pages$AddPerson.locale}"
																	verticalOffset="-212" enableManualInput="false"
																	inputStyle="width: 170px; height: 14px" />

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.passportExpiryDate']}:"></h:outputLabel>
																<rich:calendar id="passportExpiryDate"
																	value="#{pages$AddPerson.passportExpiryDate}"
																	popup="true"
																	datePattern="#{pages$AddPerson.dateFormat}"
																	showApplyButton="false" verticalOffset="-212"
																	locale="#{pages$AddPerson.locale}"
																	disabled="#{pages$AddPerson.isReadonlyMode}"
																	enableManualInput="false"
																	inputStyle="width: 170px; height: 14px" />
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenants.visa.issueplace']}:"></h:outputLabel>
																<h:selectOneMenu id="visaIssuePlace" immediate="false"
																	value="#{pages$AddPerson.residenseVisaIssuePlaceId}"
																	readonly="#{pages$AddPerson.isReadonlyMode}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems value="#{pages$AddPerson.countryList}" />
																</h:selectOneMenu>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.residenseVisaNumber']}:"></h:outputLabel>
																<h:inputText maxlength="15" id="txtresidenseVisaNumber"
																	value="#{pages$AddPerson.residenseVisaNumber}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.issueDate']}:"></h:outputLabel>
																<rich:calendar id="residenceVisaIssueDate"
																	value="#{pages$AddPerson.residenseVisaIssueDate}"
																	popup="true"
																	datePattern="#{pages$AddPerson.dateFormat}"
																	showApplyButton="false"
																	locale="#{pages$AddPerson.locale}"
																	disabled="#{pages$AddPerson.isReadonlyMode}"
																	enableManualInput="false"
																	inputStyle="width: 170px; height: 14px"
																	verticalOffset="-212" />
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.residenseVisaExpDate']}:"></h:outputLabel>
																<rich:calendar id="residenceVidaExpDate"
																	value="#{pages$AddPerson.residenseVidaExpDate}"
																	popup="true"
																	datePattern="#{pages$AddPerson.dateFormat}"
																	showApplyButton="false"
																	locale="#{pages$AddPerson.locale}"
																	disabled="#{pages$AddPerson.isReadonlyMode}"
																	enableManualInput="false"
																	inputStyle="width: 170px; height: 14px"
																	verticalOffset="-212" />
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.companyName']}:" />
																<h:inputText maxlength="50" id="txtWorkingCompany"
																	value="#{pages$AddPerson.workingCompany}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.designation']}:" />
																<h:inputText maxlength="50" id="txtDesignation"
																	value="#{pages$AddPerson.designation}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
															</t:panelGrid>
														</t:div>
													</rich:tab>


													<%--  Individaul Tab Finish --%>
													<%--  `Tab Start --%>
													<rich:tab id="tabPersonCompany"
														label="#{msg['person.companyInformation']}"
														binding="#{pages$AddPerson.tabPersonCompany}">
														<t:div style="height:240px;overflow-y:scroll;width:100%;">
															<t:panelGrid columns="4" cellpadding="1px"
																cellspacing="2px" styleClass="TAB_DETAIL_SECTION_INNER">
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['customer.companyName']}:"></h:outputLabel>
																</h:panelGroup>
																<h:inputText styleClass="A_LEFT" style="width:150px;"
																	maxlength="250" id="txtCompanyName"
																	value="#{pages$AddPerson.txtCompanyName}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
														
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenants.license.licenseno']}:"></h:outputLabel>
																<h:inputText styleClass="A_LEFT" style="width:152px;"
																	maxlength="50" id="txtLicenseno"
																	value="#{pages$AddPerson.txtLicenseno}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['tenants.license.issueplace']}:"></h:outputLabel>
																</h:panelGroup>
																<h:selectOneMenu id="selectLicenseIssuePlace"
																	style="WIDTH: 157px" immediate="false"
																	value="#{pages$AddPerson.licenseIssuePlaceId}"
																	readonly="#{pages$AddPerson.isReadonlyMode}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems
																		value="#{pages$AddPerson.licenseIssuingEmirateList}" />
																	<a4j:support event="onchange"
																		reRender="one1,tabPersonIndividual,tabPersonCompany,selectLicenseIssuePlace,selecttradeLicenseIssuer"
																		action="#{pages$AddPerson.onLicenseIssueingEmirateChanged}" />
																</h:selectOneMenu>
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['tenants.license.issuer']}:"></h:outputLabel>
																</h:panelGroup>
																<h:selectOneMenu id="selecttradeLicenseIssuer"
																	style="WIDTH: 157px" immediate="false"
																	value="#{pages$AddPerson.tradeLicenseIssuersId}"
																	readonly="#{pages$AddPerson.isReadonlyMode}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems
																		value="#{pages$AddPerson.tradeLicenseIssuersList}" />
																</h:selectOneMenu>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenants.license.issuedate']}:"></h:outputLabel>
																<rich:calendar id="licenseIssueDate"
																	value="#{pages$AddPerson.licenseIssueDate}"
																	popup="true"
																	datePattern="#{pages$AddPerson.dateFormat}"
																	showApplyButton="false"
																	disabled="#{pages$AddPerson.isReadonlyMode}"
																	locale="#{pages$AddPerson.locale}" style="width:133px"
																	enableManualInput="false" cellWidth="24px"
																	inputStyle="width:134px;" />
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['tenants.license.expirydate']}:" />
																</h:panelGroup>
																<rich:calendar id="licenseExpiryDate"
																	value="#{pages$AddPerson.licenseExpiryDate}"
																	popup="true"
																	datePattern="#{pages$AddPerson.dateFormat}"
																	showApplyButton="false"
																	disabled="#{pages$AddPerson.isReadonlyMode}"
																	locale="#{pages$AddPerson.locale}" style="width:133px"
																	enableManualInput="false" cellWidth="24px"
																	inputStyle="width:134px;" />
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contractAdd.businessActivity']}:" />
																<h:panelGrid
																	style="border-width: 1px; border-style: solid; border-color: #a2a2a2; background: white; overflow-x: auto; overflow-y: scroll; width: 88%; height: 60px;">
																	<h:selectManyCheckbox layout="pageDirection"
																		id="selectManyBusinessActivity"
																		value="#{pages$AddPerson.businessActivities}">
																		<f:selectItems
																			value="#{pages$AddPerson.businessActivityList}" />
																	</h:selectManyCheckbox>
																</h:panelGrid>

															</t:panelGrid>
														</t:div>
													</rich:tab>
													<%--  Company Tab Finish --%>
													<%--  Contact Info Tab Start --%>
													<rich:tab label="#{msg['contact.contactinformation']}">
														<t:div style="height:240px;overflow-y:scroll;width:100%;">
															<t:panelGrid>
																<t:inputHidden id="contactInfoCreatedBy"
																	value="#{pages$AddPerson.contactInfoCreatedBy}" />
																<t:inputHidden id="contactInfoCreatedOn"
																	value="#{pages$AddPerson.contactInfoCreatedOn}" />
																<t:inputHidden id="rowId"
																	value="#{pages$AddPerson.contactInfoRowId}" />

															</t:panelGrid>
															<t:panelGrid cellpadding="1px" cellspacing="2px"
																styleClass="TAB_DETAIL_SECTION_INNER" style="width:97%;"
																columns="4"
																rendered="#{!pages$AddPerson.isReadonlyMode}">

																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contact.address1']}:"></h:outputLabel>
																</h:panelGroup>
																<h:inputText maxlength="150" id="txtaddress1"
																	value="#{pages$AddPerson.address1}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.address2']}:"></h:outputLabel>
																<h:inputText maxlength="150" id="txtaddress2"
																	value="#{pages$AddPerson.address2}"
																	readonly="#{pages$AddPerson.isReadonlyMode}" />

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.street']}:"></h:outputLabel>
																<h:inputText maxlength="150" id="txtstreet"
																	value="#{pages$AddPerson.street}"
																	readonly="#{pages$AddPerson.isReadonlyMode}" />
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.postcode']}:"></h:outputLabel>
																<h:inputText styleClass="A_RIGHT_NUM" maxlength="20"
																	id="txtpostCode" value="#{pages$AddPerson.postCode}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.country']}:"></h:outputLabel>
																<h:selectOneMenu id="country"
																	value="#{pages$AddPerson.countryId}"
																	readonly="#{pages$AddPerson.isReadonlyMode}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems
																		value="#{view.attributes['countryList']}" />
																	<a4j:support event="onchange"
																		action="#{pages$AddPerson.loadContactInfoState}"
																		reRender="state,selectcity" />
																</h:selectOneMenu>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.state']}:"></h:outputLabel>
																<h:selectOneMenu id="state"
																	readonly="#{pages$AddPerson.isReadonlyMode}"
																	required="false" value="#{pages$AddPerson.stateId}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems value="#{view.attributes['stateList']}" />
																	<a4j:support event="onchange"
																		action="#{pages$AddPerson.loadContactInfoCity}"
																		reRender="state,selectcity" />
																</h:selectOneMenu>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.city']}:"></h:outputLabel>
																<h:selectOneMenu id="selectcity"
																	value="#{pages$AddPerson.cityId}"
																	readonly="#{pages$AddPerson.isReadonlyMode}">
																	<f:selectItem itemLabel="#{msg['commons.All']}"
																		itemValue="-1" />
																	<f:selectItems value="#{pages$AddPerson.cityList}" />
																</h:selectOneMenu>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.homephone']}:"></h:outputLabel>
																<h:inputText styleClass="A_RIGHT_NUM" maxlength="15"
																	id="txthomePhone" value="#{pages$AddPerson.homePhone}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.officephone']}:"></h:outputLabel>
																<h:inputText styleClass="A_RIGHT_NUM" maxlength="15"
																	id="txtofficePhone"
																	value="#{pages$AddPerson.officePhone}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.fax']}:"></h:outputLabel>
																<h:inputText styleClass="A_RIGHT_NUM" maxlength="15"
																	id="txtfax" value="#{pages$AddPerson.fax}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>


																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contact.email']}:"></h:outputLabel>
																</h:panelGroup>
																<h:inputText maxlength="50" id="txtemail"
																	value="#{pages$AddPerson.email}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>

															</t:panelGrid>

															<t:panelGrid style="width:97%;" styleClass="BUTTON_TD"
																columns="1">
																<h:commandButton id="btnAddContactInfo"
																	styleClass="BUTTON" value="#{msg['commons.Add']}"
																	action="#{pages$AddPerson.btnAddContactInfo_Click}"
																	rendered="#{!pages$AddPerson.isReadonlyMode}" />
															</t:panelGrid>
															<t:div id="contactInfoDiv" styleClass="contentDiv"
																style="align:center;width:96%;">
																<t:dataTable id="contactInfoDataTable" rows="9"
																	width="100%" value="#{pages$AddPerson.contactInfoList}"
																	binding="#{pages$AddPerson.contactInfoDataTable}"
																	preserveDataModel="false" preserveSort="false"
																	var="contactInfoItem" rowClasses="row1,row2"
																	rules="all" renderedIfEmpty="true">
																	<t:column id="colAddress1" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.address']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{contactInfoItem.fullAddress}" />
																	</t:column>
																	<t:column id="colpostCode">
																		<f:facet name="header">
																			<t:outputText value="#{msg['contact.postcode']}" />
																		</f:facet>
																		<t:outputText title="" styleClass="A_LEFT"
																			value="#{contactInfoItem.postCode}" />
																	</t:column>
																	<t:column id="colHomePhone">
																		<f:facet name="header">
																			<t:outputText value="#{msg['contact.homephone']}" />
																		</f:facet>
																		<t:outputText title="" styleClass="A_LEFT"
																			value="#{contactInfoItem.homePhone}" />
																	</t:column>
																	<t:column id="colOffPhone">
																		<f:facet name="header">
																			<t:outputText value="#{msg['contact.officephone']}" />
																		</f:facet>
																		<t:outputText title="" styleClass="A_LEFT"
																			value="#{contactInfoItem.officePhone}" />
																	</t:column>
																	<t:column id="colEmail">
																		<f:facet name="header">
																			<t:outputText value="#{msg['contact.email']}" />
																		</f:facet>
																		<t:outputText title="" styleClass="A_LEFT"
																			value="#{contactInfoItem.email}" />
																	</t:column>
																	<t:column id="colActionContactInfo" sortable="true"
																		rendered="#{!pages$AddPerson.isReadonlyMode}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.select']}" />
																		</f:facet>
																		<a4j:commandLink reRender="contactInfoDataTable"
																			action="#{pages$AddPerson.imgRemoveContactInfo_Click}"
																			rendered="#{!pages$AddPerson.isReadonlyMode}">&nbsp;
																	                 <h:graphicImage
																				id="imgRemoveContactInfo"
																				title="#{msg['commons.delete']}"
																				url="../resources/images/delete_icon.png" />
																		</a4j:commandLink>
																		<h:commandLink
																			action="#{pages$AddPerson.btnPopulateContactInfoFromList_Click}">&nbsp;
																	                 <h:graphicImage
																				id="btnPopulateContactInfoFromList"
																				title="#{msg['commons.edit']}"
																				url="../resources/images/edit-icon.gif" />
																		</h:commandLink>

																	</t:column>

																</t:dataTable>
															</t:div>
														</t:div>
													</rich:tab>

													<%--  Contact Info  Tab Finish --%>
													<%--  Contact Ref Tab Start --%>
													<rich:tab id="tabContactRef"
														binding="#{pages$AddPerson.tabContactReferences}"
														label="#{msg['person.ContactReference']}">
														<t:div style="height:240px;overflow-y:scroll;width:100%;">
															<t:panelGrid id="hdnPanelGrid">

																<t:inputHidden id="crContactInfoCreatedBy"
																	value="#{pages$AddPerson.crContactInfoCreatedBy}" />
																<t:inputHidden id="crContactInfoCreatedOn"
																	value="#{pages$AddPerson.crContactInfoCreatedOn}" />
																<t:inputHidden id="contactRefCreatedBy"
																	value="#{pages$AddPerson.contactRefCreatedBy}" />
																<t:inputHidden id="contactRefCreatedOn"
																	value="#{pages$AddPerson.contactRefCreatedOn}" />
															</t:panelGrid>
															<t:panelGrid id="crDetailTable" cellpadding="1px"
																cellspacing="2px" styleClass="TAB_DETAIL_SECTION_INNER"
																columns="4" style="width:97;">

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.title']}:"></h:outputLabel>
																<h:selectOneMenu id="selectCRTitle"
																	value="#{pages$AddPerson.CRTitleId}"
																	readonly="#{pages$AddPerson.isReadonlyMode}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems value="#{pages$ApplicationBean.title}" />
																</h:selectOneMenu>
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['customer.firstname']}:"></h:outputLabel>
																</h:panelGroup>
																<h:inputText maxlength="500" id="txtCRFirstName"
																	value="#{pages$AddPerson.CRFirstName}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['customer.middlename']}:"></h:outputLabel>
																<h:inputText maxlength="500" id="txtCRMiddleName"
																	value="#{pages$AddPerson.CRMiddleName}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['customer.lastname']}:"></h:outputLabel>
																</h:panelGroup>
																<h:inputText maxlength="500" id="txtCRLastName"
																	value="#{pages$AddPerson.CRLastName}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contact.address1']}:"></h:outputLabel>
																</h:panelGroup>
																<h:inputText maxlength="150" id="txtCRAddress1"
																	value="#{pages$AddPerson.CRAddress1}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.address2']}:"></h:outputLabel>
																<h:inputText maxlength="150" id="txtCRAddress2"
																	value="#{pages$AddPerson.CRAddress2}"
																	readonly="#{pages$AddPerson.isReadonlyMode}" />

																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['customer.cell']}:"></h:outputLabel>
																</h:panelGroup>


																<h:inputText maxlength="15" id="txtCRCellNumber"
																	value="#{pages$AddPerson.CRCellNumber}"
																	readonly="#{pages$AddPerson.isReadonlyMode}" />

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.street']}:"></h:outputLabel>
																<h:inputText maxlength="150" id="txtCRStreet"
																	value="#{pages$AddPerson.CRStreet}"
																	readonly="#{pages$AddPerson.isReadonlyMode}" />
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.postcode']}:"></h:outputLabel>
																<h:inputText styleClass="A_RIGHT_NUM" maxlength="10"
																	id="txtCRPostCode"
																	value="#{pages$AddPerson.CRPostCode}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.country']}:"></h:outputLabel>
																<h:selectOneMenu id="countryCR"
																	value="#{pages$AddPerson.CRCountryId}"
																	readonly="#{pages$AddPerson.isReadonlyMode}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems
																		value="#{view.attributes['countryList']}" />
																	<a4j:support event="onchange"
																		action="#{pages$AddPerson.loadContactRefState}"
																		reRender="stateCR,selectcityCR" />
																</h:selectOneMenu>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.state']}:"></h:outputLabel>
																<h:selectOneMenu id="stateCR" required="false"
																	value="#{pages$AddPerson.CRStateId}"
																	readonly="#{pages$AddPerson.isReadonlyMode}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems value="#{pages$AddPerson.CRStateList}" />

																	<a4j:support event="onchange"
																		action="#{pages$AddPerson.loadContactRefCity}"
																		reRender="selectcityCR" />
																</h:selectOneMenu>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.city']}:"></h:outputLabel>
																<h:selectOneMenu id="selectcityCR"
																	value="#{pages$AddPerson.CRCityId}"
																	readonly="#{pages$AddPerson.isReadonlyMode}">
																	<f:selectItem itemLabel="#{msg['commons.All']}"
																		itemValue="-1" />
																	<f:selectItems value="#{pages$AddPerson.CRCityList}" />
																</h:selectOneMenu>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.homephone']}:"></h:outputLabel>
																<h:inputText styleClass="A_RIGHT_NUM"
																	style="width:187px;" maxlength="15" id="txtCRHomePhone"
																	value="#{pages$AddPerson.CRHomePhone}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.officephone']}:"></h:outputLabel>
																<h:inputText styleClass="A_RIGHT_NUM" maxlength="15"
																	id="txtCROfficePhone"
																	value="#{pages$AddPerson.CROfficePhone}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.fax']}:"></h:outputLabel>
																<h:inputText styleClass="A_RIGHT_NUM" maxlength="15"
																	id="txtCRFax" value="#{pages$AddPerson.CRFax}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.email']}:"></h:outputLabel>
																<h:inputText maxlength="50" id="txtCREmail"
																	value="#{pages$AddPerson.CREmail}"
																	readonly="#{pages$AddPerson.isReadonlyMode}"></h:inputText>

															</t:panelGrid>

															<t:panelGrid style="width:97%;" styleClass="BUTTON_TD"
																columns="1">
																<h:commandButton id="btnAddContactRef"
																	styleClass="BUTTON" value="#{msg['commons.Add']}"
																	action="#{pages$AddPerson.btnAddContactRef_Click}"
																	rendered="#{!pages$AddPerson.isReadonlyMode}" />
															</t:panelGrid>
															<t:div id="contactRefDiv" styleClass="contentDiv"
																style="align:center;width:96%;">
																<t:dataTable id="contactRefDataTable" rows="9"
																	width="100%" value="#{pages$AddPerson.contactRefList}"
																	binding="#{pages$AddPerson.contactRefDataTable}"
																	preserveDataModel="false" preserveSort="false"
																	var="contactRefItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true">
																	<t:column id="colCRName" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.Name']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{contactRefItem.fullName}" />
																	</t:column>
																	<t:column id="colAddress1" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.address']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{contactRefItem.contactInfoView.fullAddress}" />
																	</t:column>
																	<t:column id="colHomePhone">
																		<f:facet name="header">
																			<t:outputText value="#{msg['contact.homephone']}" />
																		</f:facet>
																		<t:outputText title="" styleClass="A_LEFT"
																			value="#{contactRefItem.contactInfoView.homePhone}" />
																	</t:column>
																	<t:column id="colcontactofficephone">
																		<f:facet name="header">
																			<t:outputText value="#{msg['contact.officephone']}" />
																		</f:facet>
																		<t:outputText title="" styleClass="A_LEFT"
																			value="#{contactRefItem.contactInfoView.officePhone}" />
																	</t:column>
																	<t:column id="cell">
																		<f:facet name="header">
																			<t:outputText value="#{msg['customer.cell']}" />
																		</f:facet>
																		<t:outputText title="" styleClass="A_LEFT"
																			value="#{contactRefItem.cellNumber}" />
																	</t:column>
																	<t:column id="colEmail">
																		<f:facet name="header">
																			<t:outputText value="#{msg['contact.email']}" />
																		</f:facet>
																		<t:outputText title="" styleClass="A_LEFT"
																			value="#{contactRefItem.contactInfoView.email}" />
																	</t:column>
																	<t:column id="colActionContactRef" sortable="true"
																		rendered="#{!pages$AddPerson.isReadonlyMode}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.select']}" />
																		</f:facet>
																		<a4j:commandLink reRender="contactRefDataTable"
																			action="#{pages$AddPerson.imgRemoveContactRef_Click}"
																			rendered="#{!pages$AddPerson.isReadonlyMode}">&nbsp;
																	                 <h:graphicImage
																				id="imgRemoveContactRef"
																				title="#{msg['commons.delete']}"
																				url="../resources/images/delete_icon.png" />
																		</a4j:commandLink>
																		<h:commandLink
																			action="#{pages$AddPerson.btnPopulateContactRefFromList_Click}">&nbsp;
																	                 <h:graphicImage
																				id="btnPopulateContactRefFromList"
																				title="#{msg['commons.edit']}"
																				url="../resources/images/edit-icon.gif" />
																		</h:commandLink>

																	</t:column>

																</t:dataTable>
															</t:div>
														</t:div>
													</rich:tab>


													<%--  Contact Ref Tab Finish --%>
													<%--  Attachment Tab Start --%>
													<rich:tab id="attachmentTab"
														label="#{msg['commons.attachmentTabHeading']}">
														<%@  include file="attachment/attachment.jsp"%>
													</rich:tab>
													<%--  Attachment Tab Finish --%>
													<%--  Comment Tab Start --%>
													<rich:tab id="commentsTab"
														label="#{msg['commons.commentsTabHeading']}">
														<%@ include file="notes/notes.jsp"%>
													</rich:tab>
													<%--  Comment Tab Finish --%>
													<%--  Roles Tab Start --%>
													<rich:tab label="#{msg['person.Roles']}"
														rendered="#{!pages$AddPerson.isReadonlyMode}">
														<t:panelGrid columns="4" cellpadding="1px"
															cellspacing="2px" styleClass="TAB_DETAIL_SECTION_INNER"
															rendered="#{!pages$AddPerson.isReadonlyMode}">
															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" />
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['person.Role']}:"></h:outputLabel>
															</h:panelGroup>
															<h:selectOneMenu id="selectPerson"
																binding="#{pages$AddPerson.cmbPersonType}"
																value="#{pages$AddPerson.selectedPersonType}"
																readonly="#{pages$AddPerson.isReadonlyMode}">
																<f:selectItems value="#{pages$AddPerson.personTypeList}" />
																<a4j:support event="onchange"
																	action="#{pages$AddPerson.loadPersonSubType}"
																	reRender="selectPersonSubType" />

															</h:selectOneMenu>
															<%-- 
														<h:outputLabel styleClass="LABEL" value="#{msg['person.Role.SubType']}:"></h:outputLabel>
														 <h:selectOneMenu id="selectPersonSubType" style="width: 157px" binding="#{pages$AddPerson.cmbSubPersonType}" value="#{pages$AddPerson.selectedSubPersonType}" readonly="#{pages$AddPerson.isReadonlyMode}">
																
															<f:selectItems value="#{pages$AddPerson.personSubTypeList}"/>
														</h:selectOneMenu>
														--%>
														</t:panelGrid>
														<t:panelGrid columnClasses="BUTTON_TD" width="98.5%">
															<a4j:commandButton id="btnRole" styleClass="BUTTON"
																value="#{msg['commons.Add']}"
																action="#{pages$AddPerson.addRoleToPerson}"
																reRender="rolesDataTable,one1"
																rendered="#{!pages$AddPerson.isReadonlyMode}" />
														</t:panelGrid>
														<t:div id="roleDiv" styleClass="contentDiv"
															style="align:center;">
															<t:dataTable id="rolesDataTable" rows="9" width="100%"
																value="#{pages$AddPerson.rolesList}"
																binding="#{pages$AddPerson.rolesDataTable}"
																preserveDataModel="false" preserveSort="false"
																var="roleItem" rowClasses="row1,row2" rules="all"
																renderedIfEmpty="true">
																<t:column id="roleName">
																	<f:facet name="header">
																		<t:outputText value="#{msg['person.Role']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{roleItem.typeDesc}" />
																</t:column>
																<%-- 
																				<t:column id="personType" >
																					<f:facet name="header">
																						<t:outputText  value="#{msg['person.Type']}" />
																					</f:facet>
																					<t:outputText title="" styleClass="A_LEFT" value="#{roleItem.subTypeDesc}" />
																				</t:column>
																				--%>
																<t:column id="colAction">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.select']}" />

																	</f:facet>
																	<a4j:commandLink
																		rendered="#{roleItem.assignedPersonTypeId == null && !pages$AddPerson.isReadonlyMode}"
																		reRender="rolesDataTable,one1"
																		action="#{pages$AddPerson.imgDeleteRemoveRoleFromPerson_Click}">&nbsp;
																	                 <h:graphicImage
																			id="imgDeleteRemoveRoleFromPerson"
																			title="#{msg['commons.delete']}"
																			url="../resources/images/delete_icon.png" />
																	</a4j:commandLink>

																</t:column>

															</t:dataTable>
														</t:div>

													</rich:tab>
													<%--  Roles Tab Finish --%>

													<%--  Violation Tab Start --%>
													<rich:tab label="#{msg['violation.violations']}"
														action="#{pages$AddPerson.populateViolations}">
														<t:div style="height:240px;overflow-y:scroll;width:100%;">
															<t:panelGrid width="100%" border="0"
																rendered="#{pages$AddPerson.blackListed}"
																cellspacing="2" cellpadding="1" columns="1">
																<t:inputTextarea id="txt_removalReason" cols="100"
																	value="#{pages$AddPerson.txt_removalReason}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
																<t:panelGrid cellpadding="1px" cellspacing="3px"
																	width="100%" border="0" columns="1"
																	columnClasses="BUTTON_TD">
																	<h:commandButton id="btnRemoveBlackList"
																		styleClass="BUTTON" value="#{msg['commons.Add']}"
																		action="#{pages$AddPerson.btnRemoveBlackList_Click}"
																		rendered="#{!pages$AddPerson.isReadonlyMode}" />
																</t:panelGrid>


															</t:panelGrid>
															<t:panelGrid cellpadding="1px" cellspacing="3px"
																width="100%" border="0" columns="1">
																<h:outputLabel
																	value="#{msg['violation.blackListHistory']}"
																	style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px;" />
															</t:panelGrid>
															<t:div styleClass="contentDiv">
																<t:dataTable id="BlackListdt" width="100%"
																	value="#{pages$AddPerson.blackListViewList}" rows="5"
																	preserveDataModel="false" preserveSort="false"
																	var="dataItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true">

																	<t:column id="collastAddedBy" sortable="true"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['blacklistSearch.addedBy']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			style="WHITE-SPACE: normal;"
																			value="#{pages$AddPerson.isEnglishLocale?dataItem.createdByEn:dataItem.createdByAr}" />
																	</t:column>
																	<t:column id="colAddedOn" sortable="true"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.date']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			style="WHITE-SPACE: normal;"
																			value="#{dataItem.createdOn}">
																			<f:convertDateTime
																				pattern="#{pages$AddPerson.dateFormat}"
																				timeZone="#{pages$AddPerson.timeZone}" />
																		</t:outputText>
																	</t:column>


																	<t:column id="collastReason" sortable="true"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText value="#{msg['blackList.reason']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			style="WHITE-SPACE: normal;"
																			value="#{dataItem.remarks}" />
																	</t:column>

																	<t:column id="colremovedBy" sortable="true"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText value="#{msg['blackList.removedBy']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			style="WHITE-SPACE: normal;"
																			value="#{pages$AddPerson.isEnglishLocale?dataItem.removedByEn:dataItem.removedByAr}" />
																	</t:column>
																	<t:column id="colRemovedOn" sortable="true"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.date']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			style="WHITE-SPACE: normal;"
																			value="#{dataItem.removedOn}">
																			<f:convertDateTime
																				pattern="#{pages$AddPerson.dateFormat}"
																				timeZone="#{pages$AddPerson.timeZone}" />
																		</t:outputText>
																	</t:column>
																	<t:column id="colremovedReason" sortable="true"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText value="#{msg['blackList.reason']}" />

																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			style="WHITE-SPACE: normal;"
																			value="#{dataItem.removingReason}" />
																	</t:column>
																</t:dataTable>
															</t:div>
															<t:panelGrid cellpadding="1px" cellspacing="3px"
																width="100%" border="0" columns="1">
																<h:outputLabel
																	value="#{msg['inspection.action.Violation']}"
																	style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px;" />
															</t:panelGrid>
															<t:div styleClass="contentDiv">
																<t:dataTable id="dt1" width="100%"
																	value="#{pages$AddPerson.violationsList}" rows="5"
																	preserveDataModel="false" preserveSort="false"
																	var="dataItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true">
																	<t:column id="colVioContract" sortable="true"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['contract.contractNumber']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			style="WHITE-SPACE: normal;"
																			value="#{dataItem.contractNumber}" />
																	</t:column>
																	<t:column id="colViolUnitNumber" sortable="true"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText id="otxtUnitNumbet"
																				value="#{msg['unit.unitNumber']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			style="WHITE-SPACE: normal;"
																			value="#{dataItem.unitNumber}" />
																	</t:column>
																	<t:column id="colViolationDate" sortable="true"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText id="otxtVolDate"
																				value="#{msg['violation.Date']}" />
																		</f:facet>
																		<t:outputText id="otxtVolDateValue"
																			styleClass="A_LEFT"
																			value="#{dataItem.violationDateString}" />

																	</t:column>
																	<t:column id="description" sortable="true"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['violationDetails.description']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{dataItem.description}" />
																	</t:column>
																	<t:column id="typeEn" sortable="true"
																		rendered="#{pages$AddPerson.isEnglishLocale}"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText value="#{msg['inspection.type']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{dataItem.typeEn}" />
																	</t:column>
																	<t:column id="typeAr" sortable="true"
																		rendered="#{pages$AddPerson.isArabicLocale}"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText value="#{msg['inspection.type']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{dataItem.typeAr}" />
																	</t:column>
																	<t:column id="categoryEn" sortable="true"
																		rendered="#{pages$AddPerson.isEnglishLocale}"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText value="#{msg['violation.category']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{dataItem.categoryEn}" />
																	</t:column>
																	<t:column id="categoryAr" sortable="true"
																		rendered="#{pages$AddPerson.isArabicLocale}"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText value="#{msg['violation.category']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{dataItem.categoryAr}" />
																	</t:column>
																	<t:column id="StatusEn" sortable="true"
																		rendered="#{pages$AddPerson.isEnglishLocale}"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText value="#{msg['inspection.status']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{dataItem.statusEn}" />
																	</t:column>
																	<t:column id="StatusAr" sortable="true"
																		rendered="#{pages$AddPerson.isArabicLocale}"
																		style="width:15%;">
																		<f:facet name="header">
																			<t:outputText value="#{msg['inspection.status']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{dataItem.statusAr}" />
																	</t:column>

																</t:dataTable>
															</t:div>
														</t:div>

													</rich:tab>
													<%--  Violation Tab Finish --%>

												</rich:tabPanel>
												<table id="table_2" cellpadding="0" cellspacing="0"
													width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
											</div>



											<br />




										</h:form>
									</div>
								</td>
							</tr>


						</table>



					</td>
				</tr>





			</table>

			<c:choose>
				<c:when test="${!pages$AddPerson.isViewModePopUp}">
					</div>
				</c:when>
			</c:choose>
		</body>
	</html>
</f:view>

