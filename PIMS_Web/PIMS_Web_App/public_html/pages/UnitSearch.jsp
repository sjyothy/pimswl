<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Auction
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	    function resetValues()
      	{
      	  
      		document.getElementById("searchFrm:txtFloorNo").value="";
      		document.getElementById("searchFrm:txtPropertyNumber").value="";
      		document.getElementById("searchFrm:txtContractNumber").value="";
      		document.getElementById("searchFrm:txtEndowedName").value="";
      		document.getElementById("searchFrm:txtEndowedNumber").value="";
			document.getElementById("searchFrm:txtUnitNo").value="";
			document.getElementById("searchFrm:txtUnitCostCenter").value="";        	
			document.getElementById("searchFrm:txtBedRooms").value="";
			document.getElementById("searchFrm:txtDewaNumber").value="";
			document.getElementById("searchFrm:txtRentAmountFrom").value="";
			document.getElementById("searchFrm:txtRentAmountTo").value="";
			document.getElementById("searchFrm:txtPropertyName").value="";    
      	    document.getElementById("searchFrm:selectUnitUsage").selectedIndex=0;
      	    document.getElementById("searchFrm:selectUnitType").selectedIndex=0;
      	    document.getElementById("searchFrm:selectInvestmentType").selectedIndex=0;
      	    document.getElementById("searchFrm:selectUnitSide").selectedIndex=0;
      	    document.getElementById("searchFrm:selectPropertyOwnerShip").selectedIndex=0;
      	    document.getElementById("searchFrm:selectStatus").selectedIndex=0;
			document.getElementById("searchFrm:state").selectedIndex=0;
			document.getElementById("searchFrm:txtUnitDesc").value="";
        }        
		
		function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	function closeWindow()
		{
		  window.close();
		}
        
        	  function	openPopUp()
		{
		
		    var width = 300;
            var height = 300;
            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + ",menubar=yes,toolbar=yes,scrollbars=yes,resizable=yes,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

            var child1 = window.open("about:blank", "subWind", windowFeatures);

		      child1.focus();
		}
		
		
			function GenerateUnitsPopup()
		{
		      var screen_width = 1024;
		      var screen_height = 450;
		      var popup_width = screen_width-250;
		      var popup_height = screen_height;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('editUnitPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<c:choose>
					<c:when test="${!pages$UnitSearch.isViewModePopUp}">
                		<div class="containerDiv">
				   </c:when>
		     </c:choose>
		
		
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<c:choose>
					<c:when test="${!pages$UnitSearch.isViewModePopUp}">
						<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>
					</c:when>
				</c:choose>


				<tr width="100%">

					<c:choose>
						<c:when test="${!pages$UnitSearch.isViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>

					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table  class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['unit.SearchUnits']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" >
							<tr valign="top">
								<td>

											<h:form id="searchFrm" enctype="multipart/form-data" >
											<div class="SCROLLABLE_SECTION" >
												<div>
													<table border="0" class="layoutTable">
														<tr>
															<td>
																<h:outputText
																	value="#{pages$UnitSearch.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
															</td>
														</tr>
													</table>
												</div>
												<div class="MARGIN" style="width:95%;">
													<table cellpadding="0" cellspacing="0" width="100%">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
											<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
													<div class="DETAIL_SECTION" style="width:99.8%;">
														<h:outputLabel value="#{msg['commons.searchCriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px" border="0" 	class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">
														
                                                            <tr>
                                                            <td width="25%" colspan="1">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.propertyReferenceNumber']}:"></h:outputLabel>
																</td>
																<td width="25%" colspan="1">
																	<h:inputText id="txtPropertyNumber"
																		binding="#{pages$UnitSearch.htmlPropertyNumber}"
																		tabindex="10">
																	</h:inputText>
																</td>
																<td width="25%" colspan="1">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['property.buildingName']}:"></h:outputLabel>
																</td>
																<td width="25%" colspan="1">
																	<h:inputText id="txtPropertyName"
																		binding="#{pages$UnitSearch.htmlPropertyName}"
																		tabindex="10">
																	</h:inputText>
																</td>
																
															</tr>
															  <tr>
                                                            <td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.endowedName']}:"></h:outputLabel>
																</td>
																<td >
																	<h:inputText id="txtEndowedName"
																		
																		binding="#{pages$UnitSearch.htmlPropertyEndowedName}"
																		tabindex="10">
																	</h:inputText>
																</td>
																<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.endowedMinorNo']}:"></h:outputLabel>
																</td>
																<td >
																	<h:inputText id="txtEndowedNumber"
																		binding="#{pages$UnitSearch.htmlEndowedMinor}"
																		tabindex="10">
																	</h:inputText>
																</td>
																
															</tr>
															<tr>
																<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['property.ownership']}:"></h:outputLabel>
															
																</td>
																<td >
																	<h:selectOneMenu id="selectPropertyOwnerShip"
																		binding="#{pages$UnitSearch.propertyOwnerShip}"
																		>
																		<f:selectItem itemLabel="#{msg['commons.All']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.propertyOwnershipType}" />
																	</h:selectOneMenu>
																</td>
																<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.floorNumber']}:"></h:outputLabel>
																</td>
																<td >
																	<h:inputText id="txtFloorNo"
																		binding="#{pages$UnitSearch.htmlFloorNumber}"
																		>
																	</h:inputText>
																</td>
															</tr>
                                                            <tr>
																<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.unitNumber']}:"></h:outputLabel>
																</td>
																<td >
																	<h:inputText id="txtUnitNo"
																		binding="#{pages$UnitSearch.htmlUnitNumber}"
																		tabindex="1">
																	</h:inputText>
																</td>
																<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.costCenter']}:"></h:outputLabel>
																</td>
																<td >
																	<h:inputText id="txtUnitCostCenter"
																		binding="#{pages$UnitSearch.htmlunitCostCenter}"
																		tabindex="1">
																	</h:inputText>
																</td>
															</tr>
															<tr>
																<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.unitUsage']}:"></h:outputLabel>
																</td>
																<td >
																	<h:selectOneMenu id="selectUnitUsage"
																		binding="#{pages$UnitSearch.unitUsageSelectMenu}"
																		tabindex="2">
																		<f:selectItem itemLabel="#{msg['commons.All']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.unitUsageTypeList}" />
																	</h:selectOneMenu>
																</td>
																<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.unitType']}:"></h:outputLabel>
																</td>
																<td >
																	<h:selectOneMenu id="selectUnitType"
																		binding="#{pages$UnitSearch.unitTypeSelectMenu}"
																		tabindex="3">
																		<f:selectItem itemLabel="#{msg['commons.All']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.unitTypeList}" />
																	</h:selectOneMenu>

																</td>
															</tr>

															<tr>
																<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.InvestmentType']}:"></h:outputLabel>
																</td>
																<td >
																	<h:selectOneMenu id="selectInvestmentType"
																		binding="#{pages$UnitSearch.unitInvestmentSelectMenu}"
																		tabindex="4">
																		<f:selectItem itemLabel="#{msg['commons.All']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.unitInvestmentTypeList}" />
																	</h:selectOneMenu>
																</td>
																<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.unitSide']}:"></h:outputLabel>
																</td>
																<td >
																	<h:selectOneMenu id="selectUnitSide"
																		binding="#{pages$UnitSearch.unitSideSelectMenu}"
																		tabindex="5">
																		<f:selectItem itemLabel="#{msg['commons.All']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.unitSideType}" />
																	</h:selectOneMenu>
																</td>
															</tr>

															<tr>
																<td >
																	<h:outputLabel styleClass="LABEL" value="#{msg['unit.BedRooms']}:"></h:outputLabel>
																</td>
																<td >
																	<h:inputText id="txtBedRooms"
																		binding="#{pages$UnitSearch.htmlBedRooms}"
																		tabindex="6">
																	</h:inputText>
																</td>
																<td >
																	<h:outputLabel styleClass="LABEL" value="#{msg['unit.DewaNumber']}:"></h:outputLabel>
																</td>
																<td >
																	<h:inputText id="txtDewaNumber"
																		binding="#{pages$UnitSearch.htmlDewaNumberNumber}"
																		tabindex="7">
																	</h:inputText>
																</td>
															</tr>
															<tr>
																<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.FromRentAmount']}:"></h:outputLabel>
																</td>
																<td >
																	<h:inputText id="txtRentAmountFrom" styleClass="A_RIGHT_NUM"
																		binding="#{pages$UnitSearch.htmlFromRentAmount}"
																		tabindex="8">
																		<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="#{pages$UnitSearch.numberFormat}"/>
																	</h:inputText>
																</td>
																<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.ToRentAmount']}:"></h:outputLabel>
																</td>
																<td >
																	<h:inputText id="txtRentAmountTo" styleClass="A_RIGHT_NUM"
																		binding="#{pages$UnitSearch.htmlToRentAmount}"
																		tabindex="9">
																		<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="#{pages$UnitSearch.numberFormat}"/>
																	</h:inputText>
																</td>
															</tr>

															<tr>
																
																<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.unitStatus']}:"></h:outputLabel>
																</td>
																<td >
																	<h:selectOneMenu id="selectStatus"
																		binding="#{pages$UnitSearch.unitStatusSelectMenu}"
																		tabindex="10">
																		<f:selectItem itemLabel="#{msg['commons.All']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$UnitSearch.unitStatusList}" />
																	</h:selectOneMenu>
																</td>
																<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.Emirate']}:"></h:outputLabel>
																</td>
																<td >
																	<h:selectOneMenu id="state"

																		binding="#{pages$UnitSearch.unitEmirateSelectMenu}"
																		tabindex="11">
																		<f:selectItem itemValue="-1"
																			itemLabel="#{msg['commons.All']}" />
																		<f:selectItems value="#{pages$UnitSearch.state}" />
																	</h:selectOneMenu>
																</td>
															</tr>
															<tr>
															
																<td>
																	<h:outputLabel styleClass="LABEL" value="#{msg['unit.unitDescription']}:">
																	</h:outputLabel>
																</td>
																 
																<td >
																	<h:inputText id="txtUnitDesc"
																		binding="#{pages$UnitSearch.htmlunitDesc}"
																		tabindex="12">
																	</h:inputText>
																</td>
																
																
																<td>
																	<h:outputLabel styleClass="LABEL" value="#{msg['commons.Contract.Number']}:">
																	</h:outputLabel>
																</td>
																 
																<td >
																	<h:inputText id="txtContractNumber"
																		binding="#{pages$UnitSearch.htmlContractNumber}"
																		tabindex="13">
																	</h:inputText>
																</td>
																
																
																
																
															</tr>

															
															<tr>
																<td class="BUTTON_TD" colspan="4">


																
																	<h:commandButton styleClass="BUTTON" type="submit"
																		value="#{msg['commons.search']}"
																		action="#{pages$UnitSearch.doSearch}" />
																	<h:commandButton styleClass="BUTTON" type="button"
																		onclick="javascript:resetValues();"
																		value="#{msg['commons.clear']}" />
																		<h:commandButton styleClass="BUTTON"
																		rendered="false"
																		value="#{msg['unit.addButton']}"
																		action="#{pages$UnitSearch.addUnit}" />
																	<h:commandButton styleClass="BUTTON" type="button"
																		rendered="#{pages$UnitSearch.isViewModePopUp}"
																		value="#{msg['commons.cancel']}"
																		onclick="javascript:window.close();">
																	</h:commandButton>
																</td>

															</tr>
																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>
															
														</table>
													</div>
												</div>
												<div
													style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;width:95%;">
													<div class="imag">
														&nbsp;
													</div>
													<div class="contentDiv" style="width:99%" >
														<t:dataTable id="test2"
															value="#{pages$UnitSearch.unitDataList}"
															binding="#{pages$UnitSearch.dataTable}" width="100%"
															rows="#{pages$UnitSearch.paginatorRows}"
															preserveDataModel="false" preserveSort="false"
															var="dataItem" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true">

															<t:column id="col1" width="10%" defaultSorted="true"  style="white-space: normal;">

																<f:facet name="header">
																    <t:commandSortHeader columnName="col1" actionListener="#{pages$UnitSearch.sort}"
															          value="#{msg['unit.numberCol']}" arrow="true" >
															          <f:attribute name="sortField" value="unitNumber" />	
															        </t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.unitNumber}" style="white-space: normal;"
																	styleClass="A_LEFT" />
															</t:column>
					                                         <t:column id="costCenter" width="10%" sortable="true" style="white-space: normal;">

																<f:facet name="header">
																    <t:commandSortHeader columnName="costCenter" actionListener="#{pages$UnitSearch.sort}"
															          value="#{msg['unit.costCenter']}" arrow="true" >
															          <f:attribute name="sortField" value="accountNumber" />	
															        </t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.accountNumber}" style="white-space: normal;"
																	styleClass="A_LEFT" />
															</t:column>
															
															

															<t:column id="col2" width="7%" sortable="true" style="white-space: normal;">
																<f:facet name="header">
																     <t:commandSortHeader columnName="col2" actionListener="#{pages$UnitSearch.sort}"
															            value="#{msg['floor.floorNo']}" arrow="true" >
															            <f:attribute name="sortField" value="floorNumber" />	
															        </t:commandSortHeader>
																	
																</f:facet>
																<t:outputText value="#{dataItem.floorView.floorNumber}" style="white-space: normal;"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="col3" width="10%" sortable="true" rendered="false" style="white-space: normal;">
																<f:facet name="header">
																      <t:commandSortHeader columnName="col3" actionListener="#{pages$UnitSearch.sort}"
															            value="#{msg['property.propertyNumberCol']}" arrow="true" >
															            <f:attribute name="sortField" value="propertyNumber" />	
															          </t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.propertyNumber}" style="white-space: normal;"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="propertyCommercialNameCol" width="15%" style="white-space: normal;"
																sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="propertyCommercialNameCol" actionListener="#{pages$UnitSearch.sort}"
															            value="#{msg['receiveProperty.commercialName']}" arrow="true" >
															            <f:attribute name="sortField" value="commercialName" />	
															        </t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.propertyCommercialName}" style="white-space: normal;"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="propertyEndowedNameCol" width="10%" style="white-space: normal;"
																sortable="true">
																<f:facet name="header">
																     <t:commandSortHeader columnName="propertyEndowedNameCol" actionListener="#{pages$UnitSearch.sort}"
															            value="#{msg['receiveProperty.endowedName']}" arrow="true" >
															            <f:attribute name="sortField" value="endowedName" />	
															        </t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.propertyEndowedName}" style="white-space: normal;"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="propertyCategory" width="6%" style="white-space: normal;"
																sortable="true">
																<f:facet name="header">
																     <t:commandSortHeader columnName="propertyCategory" actionListener="#{pages$UnitSearch.sort}"
															            value="#{msg['receiveProperty.ownershipType']}" arrow="true" >
															            <f:attribute name="sortField" value="categoryId" />	
															        </t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{pages$UnitSearch.isEnglishLocale?dataItem.propertyCategoryEn:dataItem.propertyCategoryAr}" style="white-space: normal;"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="unitDescription" width="8%" style="white-space: normal;"
																sortable="true">
																<f:facet name="header">
																      <t:commandSortHeader columnName="unitDescription" actionListener="#{pages$UnitSearch.sort}"
															            value="#{msg['unit.unitDescriptionCol']}" arrow="true" >
															            <f:attribute name="sortField" value="unitRemarks" />	
															         </t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.unitDesc}" style="white-space: normal;"
																	styleClass="A_LEFT" />
															</t:column>															
														
															<t:column id="unitTypeEnCol" width="7%" style="white-space: normal;"
																rendered="#{pages$UnitSearch.isEnglishLocale}"
																sortable="true">
																<f:facet name="header">
																     <t:commandSortHeader columnName="unitTypeEnCol" actionListener="#{pages$UnitSearch.sort}"
															             value="#{msg['commons.typeCol']}"  arrow="true" >
															            <f:attribute name="sortField" value="unitTypeId" />	
															        </t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.unitTypeEn}" style="white-space: normal;"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="unitTypeArCol" width="7%" style="white-space: normal;"
																rendered="#{pages$UnitSearch.isArabicLocale}"
																sortable="true">
																<f:facet name="header">
																     <t:commandSortHeader columnName="unitTypeArCol" actionListener="#{pages$UnitSearch.sort}"
															            value="#{msg['commons.typeCol']}" arrow="true" >
															            <f:attribute name="sortField" value="unitTypeId" />	
															        </t:commandSortHeader>
																	
																</f:facet>
																<t:outputText value="#{dataItem.unitTypeAr}" style="white-space: normal;"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="unitAddress" width="10%" style="white-space: normal;"
																sortable="true">
																<f:facet name="header">
																	<t:outputText value="#{msg['unit.address']}" />
																</f:facet>
																<t:outputText value="#{dataItem.unitAddress}" style="white-space: normal;"
																	styleClass="A_LEFT" />
															</t:column>
															
															
															<t:column id="unitRent" width="10%" sortable="true" style="white-space: normal;">
																<f:facet name="header">
																     <t:commandSortHeader columnName="unitRent" actionListener="#{pages$UnitSearch.sort}"
															            value="#{msg['unit.rentValue']}" arrow="true" >
															            <f:attribute name="sortField" value="rentValue" />	
															        </t:commandSortHeader>
																</f:facet>
																	<t:outputText value="#{dataItem.rentValue}" style="white-space: normal;"
																	styleClass="A_RIGHT_NUM" >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="#{pages$UnitSearch.numberFormat}"/>
																	</t:outputText>
															</t:column>

															<t:column id="col7" width="10%" style="white-space: normal;"
																rendered="#{pages$UnitSearch.isEnglishLocale}"
																sortable="true">
																<f:facet name="header">
																     <t:commandSortHeader columnName="col7" actionListener="#{pages$UnitSearch.sort}"
															            value="#{msg['commons.status']}" arrow="true" >
															            <f:attribute name="sortField" value="statusId" />	
															        </t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.statusEn}" style="white-space: normal;"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="statusArCol" width="10%" style="white-space: normal;"
																rendered="#{pages$UnitSearch.isArabicLocale}"
																sortable="true">
																<f:facet name="header">
																    <t:commandSortHeader columnName="statusArCol" actionListener="#{pages$UnitSearch.sort}"
															            value="#{msg['commons.status']}" arrow="true" >
															            <f:attribute name="sortField" value="statusId" />	
															        </t:commandSortHeader>
																	
																</f:facet>
																<t:outputText value="#{dataItem.statusAr}" style="white-space: normal;"
																	styleClass="A_LEFT" />
															</t:column>
															
															<t:column id="col8" sortable="false" width="10%" style="white-space: normal;"
																rendered="#{pages$UnitSearch.isPageModeSelectOnePopUp}">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.select']}" />
																</f:facet>
																<t:commandLink
																	actionListener="#{pages$UnitSearch.sendUnitInfoToParent}">
																	<h:graphicImage style="width: 16;height: 16;border: 0"
																		alt="#{msg['commons.select']}"
																		url="../resources/images/select-icon.gif"></h:graphicImage>
																</t:commandLink>
															</t:column>
															<t:column id="col9" width="10%" sortable="false" style="white-space: normal;"
																rendered="#{pages$UnitSearch.isPageModeSelectManyPopUp}">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.select']}" />
																</f:facet>
																<h:selectBooleanCheckbox id="select" value="#{dataItem.selected}"
																	/>
															</t:column>
															<t:column id="editCol" width="5%" style="white-space: normal;"
																rendered="#{!pages$UnitSearch.isPageModeSelectManyPopUp && !pages$UnitSearch.isPageModeSelectOnePopUp}"
																sortable="false">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.edit']}" />
																</f:facet>
																<pims:security screen="Pims.PropertyManagement.Unit.EditUnit" action="create">
																<t:commandLink action="#{pages$UnitSearch.editUnit}"
																	rendered="#{!pages$UnitSearch.isPageModeSelectManyPopUp && dataItem.statusReceived}">
																	<h:graphicImage id="editIcon"
																		title="#{msg['commons.edit']}"
																		url="../resources/images/edit-icon.gif" />
																</t:commandLink>
																</pims:security>
															</t:column>
														</t:dataTable>
													</div>
													<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:100%;#width:100%;" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$UnitSearch.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
												
													<t:div styleClass="PAGE_NUM_BG" style="#width:20%">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{pages$UnitSearch.currentPage}"/>
																	</td>
																</tr>					
															</table>
														</t:div>
																<TABLE border="0" class="SCH_SCROLLER">
																 	<tr>
																 		<td>   
																			<t:commandLink
																				action="#{pages$UnitSearch.pageFirst}"
																				disabled="#{pages$UnitSearch.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																		<t:commandLink
																			action="#{pages$UnitSearch.pagePrevious}"
																			disabled="#{pages$UnitSearch.firstRow == 0}">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR"></t:graphicImage>
																		</t:commandLink>
																	  	</td>
																	  	<td>
																		<t:dataList value="#{pages$UnitSearch.pages}"
																			var="page" >
																			<h:commandLink value="#{page}"
																				actionListener="#{pages$UnitSearch.page}"
																				rendered="#{page != pages$UnitSearch.currentPage}" />
																			<h:outputText value="<b>#{page}</b>" escape="false"
																				rendered="#{page == pages$UnitSearch.currentPage}" />
																		</t:dataList>
																		</td>
																		<td>
																		<t:commandLink action="#{pages$UnitSearch.pageNext}"
																			disabled="#{pages$UnitSearch.firstRow + pages$UnitSearch.rowsPerPage >= pages$UnitSearch.totalRows}">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF"></t:graphicImage>
																		</t:commandLink>
																		</td>
																		<td>
																		<t:commandLink action="#{pages$UnitSearch.pageLast}"
																			disabled="#{pages$UnitSearch.firstRow + pages$UnitSearch.rowsPerPage >= pages$UnitSearch.totalRows}">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL"></t:graphicImage>
																		</t:commandLink>
																		</td>
																		</tr>
																	</TABLE>	
																	</td></tr>
															</table>
													</t:div>
												</div>
											<table width="96.6%" border="0">
												<tr>
													<td class="BUTTON_TD JUG_BUTTON_TD_US" >

														<h:commandButton type="submit"
															rendered="#{pages$UnitSearch.isPageModeSelectManyPopUp}"
															styleClass="BUTTON"
															actionListener="#{pages$UnitSearch.sendManyUnitInfoToParent}"
															value="#{msg['commons.select']}" />
													</td>

												</tr>


											</table></div>
										
										
               </div>
										</h:form>

									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
					<td colspan="2">
					<c:choose>
					<c:when test="${!pages$UnitSearch.isViewModePopUp}">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</c:when>
					</c:choose>
					</td>
				</tr>
			</table>
				<c:choose>
					<c:when test="${!pages$UnitSearch.isViewModePopUp}">
                		</div>
				   </c:when>
		     </c:choose>
		
		</body>
	</html>
</f:view>