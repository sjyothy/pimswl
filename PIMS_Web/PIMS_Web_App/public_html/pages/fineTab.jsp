

					<h:panelGrid columns="2" columnClasses="presidentDiscussionColumn"
   						rendered="#{pages$RenewContractBackingBean.currentFine}">

											<t:dataTable id="FineTable"
													value="#{pages$RenewContractBackingBean.fine}"
													rows="20"
													preserveDataModel="true" preserveSort="true" var="dataItem"
													rules="all" renderedIfEmpty="true" width="80%" align="center">
						
													<t:column id="colNameEn" sortable="true" rendered="#{pages$RenewContractBackingBean.isEnglishLocale}">
														<f:facet name="header">
															<t:outputText value="#{msg['renewContract.name']}" />
														</f:facet>
														<t:outputText value="#{dataItem.nameEn}"/>
													</t:column>
													
													<t:column id="coltNameAr" sortable="true" rendered="#{pages$RenewContractBackingBean.isArabicLocale}">
														<f:facet name="header">
															<t:outputText value="#{msg['renewContract.name']}" />
														</f:facet>
														<t:outputText value="#{dataItem.nameAr}"/>
													</t:column>
													
													<t:column id="coContractNumber" sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['renewContract.fineAmount']}" />
														</f:facet>
														<t:outputText value="#{dataItem.fineAmount}"/>
													</t:column>
													
												
												</t:dataTable>

	<t:dataScroller id="scroller" for="FineTable" paginator="true" fastStep="1"
		paginatorMaxPages="2" immediate="true" styleClass="scroller"
		paginatorTableClass="paginator" renderFacetsIfSinglePage="false"
		paginatorTableStyle="grid_paginator" layout="singleTable"
		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
		paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

		<f:facet name="first">

			<t:outputLabel value="First" id="lblF" />

		</f:facet>

		<f:facet name="last">

			<t:outputLabel value="Last" id="lblL" />

		</f:facet>

		<f:facet name="fastforward">

			<t:outputLabel value="FFwd" id="lblFF" />

		</f:facet>

		<f:facet name="fastrewind">

			<t:outputLabel value="FRev" id="lblFR" />

		</f:facet>

	</t:dataScroller>
</h:panelGrid>