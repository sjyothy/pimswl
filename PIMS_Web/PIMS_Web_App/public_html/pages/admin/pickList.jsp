<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg" />
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title><h:outputFormat value="#{msg['admin.picklist.title']}"/></title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache" />
			<meta http-equiv="cache-control" content="no-cache" />
			<meta http-equiv="expires" content="0" />
			<link rel="stylesheet" type="text/css" href="../../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css" href="../../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css" href="../../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css" href="../../<h:outputFormat value="#{path.css_tabPanel}"/>" />
		</head>
		<body class="body_style body_color" marginheight="0px" marginwidth="0px">
			<h:inputHidden value="#{pages$PickListController.init}"/>
			<h:inputHidden value="#{pages$PickListController.selectedPicklist}"/>
			<h:form id="picklist" styleClass="stretch">
				<t:div styleClass="stretch" style="background: #ffffff; height: 98%;width: 99%;border: 1px solid #000000;">
					<t:div style="width: 100%;" styleClass="header_cell_middle">
						<div class="grid_buttons_image_accountlist" style="float: left;margin: 1px 2px;">&nbsp;</div>
					  	<div class="grid_title_div">
					  		<span class="grid_title_text"><h:outputText value="#{msg['admin.picklist.title']}" /> </span>
					    </div>
					    <t:div style="float: right;">
					    	<a4j:commandLink id="deletePicklistData"   
								styleClass="delete_button" style="margin: 0px;float:left;"
								actionListener="#{pages$PickListController.deletePicklistData}" reRender="picklist:errorMessages" limitToList="true">
								<div style="float: left;cursor: pointer;">
									<div class="grid_buttons_image_delete" style="float: left;border: 0px;margin: 2px;">&nbsp;</div>
									<h:outputText value="#{msg['commons.delete']}" styleClass="grid_buttons_text" style="float: left;"/>
								</div>
							</a4j:commandLink>
					    </t:div>
					    <t:div style="float: right;">
					    	<a4j:commandLink id="savePicklistData"   
								styleClass="delete_button" style="margin: 0px;float:left;"
								actionListener="#{pages$PickListController.savePicklistData}" reRender="picklist:errorMessages" limitToList="true">
								<div style="float: left;cursor: pointer;">
									<div class="grid_buttons_image_save" style="float: left;border: 0px;margin: 2px;">&nbsp;</div>
									<h:outputText value="#{msg['commons.saveButton']}" styleClass="grid_buttons_text" style="float: left;"/>
								</div>
							</a4j:commandLink>
					    </t:div>
					    <t:div style="float: right;">
					    	<a4j:commandLink id="addPicklistData"   
								styleClass="delete_button" style="margin: 0px;float:left;"
								actionListener="#{pages$PickListController.addPicklistData}" reRender="picklist:picklistDataTable, picklist:errorMessages" limitToList="true">
								<div style="float: left;cursor: pointer;">
									<div class="grid_buttons_image_add" style="float: left;border: 0px;margin: 2px;">&nbsp;</div>
									<h:outputText value="#{msg['commons.Add']}" styleClass="grid_buttons_text" style="float: left;"/>
								</div>
							</a4j:commandLink>
					    </t:div>
					</t:div>
					<t:div styleClass="picklist_list">
						<t:div styleClass="section_head_style">
							<h:outputLabel id="metaPicklistHeader" value="#{msg['admin.picklist.header']}" style="vertical-align: middle;height: 23px"/>
						</t:div>
						<t:div style="height: 96%;overflow-y: auto;" styleClass="panelGridRow2">
							<rich:dataTable var="metaPicklist" value="#{pages$PickListController.picklistList}" styleClass="panelGridRow2" style="width: 100%;">
								<rich:column id="picklistName" style="padding: 0px;border: 0px;">
									<h:commandLink id="primaryName" value="#{metaPicklist.picklistName}" styleClass="grid_column_text_font" style="font-size: 12px;" actionListener="#{pages$PickListController.selectPicklist}">
										<f:param name="picklistId" value="#{metaPicklist.id}" />
									</h:commandLink>
								</rich:column>
							</rich:dataTable>
						</t:div>
					</t:div>
					<t:div id="picklistDataDiv" styleClass="picklist_data panelGridRow2" rendered="#{pages$PickListController.selectedPicklist.id != null}">
						<t:div styleClass="section_head_style" style="vertical-align: middle;height: 23px">
							<h:outputLabel id="picklistDataHeader" value="#{msg['admin.picklist.detail']}"/>
						</t:div>
						<a4j:outputPanel id="errorMessages">
							<h:messages styleClass="grid_column_text_font"/>
						</a4j:outputPanel>
						<t:div styleClass="Label picklist_nameheader">
							<h:outputLabel id="selectedPicklistNameLabel" styleClass="Label" value="#{msg['admin.picklist.selectname']}"/>
							<h:outputLabel id="selectedPicklistName" value="#{pages$PickListController.selectedPicklist.picklistName}" style="padding-left: 10px;"/>
						</t:div>
						<t:div style="height: 365px; overflow-y: auto;" styleClass="contentDiv">
							<rich:dataTable id="picklistDataTable" var="dataPicklist" value="#{pages$PickListController.picklistData}" styleClass="panelGridRow2" style="width: 100%;" headerClass="panelGridRow2">
								<rich:column id="dataPrimaryName" style="padding: 0px;border: 0px;text-align: center;">
									<f:facet name="header"><h:outputText value="#{msg['admin.picklist.dataen']}" styleClass="grid_column_header_font" style="font-size: 11px"/></f:facet>
									<h:selectBooleanCheckbox value="#{dataPicklist.selected}" styleClass="grid_column_text_font child_chkbox pointer" style="margin-right: 5px;" disabled="#{pages$PickListController.selectedPicklist.isSystem}"/>
									<h:inputText id="plistPrimary" value="#{dataPicklist.displayPrimary}" styleClass="grid_column_text_font" disabled="#{pages$PickListController.selectedPicklist.isSystem}" />
								</rich:column>
								<rich:column id="dataSecondaryName" style="padding: 0px;border: 0px;text-align: center;">
									<f:facet name="header"><h:outputText value="#{msg['admin.picklist.dataar']}" styleClass="grid_column_header_font" style="font-size: 11px"/></f:facet>
									<h:inputText id="plistSecondary" value="#{dataPicklist.displaySecondary}" styleClass="grid_column_text_font" disabled="#{pages$PickListController.selectedPicklist.isSystem}"/>
								</rich:column>
								<rich:column id="dataValue" style="padding: 0px;border: 0px;text-align: center;">
									<f:facet name="header"><h:outputText value="#{msg['admin.picklist.datavalue']}" styleClass="grid_column_header_font" style="font-size: 11px"/></f:facet>
									<h:inputText id="plistValue" value="#{dataPicklist.value}" styleClass="grid_column_text_font" disabled="#{pages$PickListController.selectedPicklist.isSystem}"/>
								</rich:column>
							</rich:dataTable>
						</t:div>
					</t:div>
				</t:div>
			</h:form>
		</body>
	</html>
</f:view>
