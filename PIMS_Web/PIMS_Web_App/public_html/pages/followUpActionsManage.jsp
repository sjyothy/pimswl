<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<script language="javascript" type="text/javascript">


	
	function sendResponse() 
	{
	    
		window.opener.refresh();
		window.close();
	}
	
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden">

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />


		</head>
		<body class="BODY_STYLE">

			<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				
	            </tr>
				<tr>
				    
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['followupaction.manage']}" styleClass="HEADER_FONT" style="padding:10px" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
							
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" />
								<td valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" >
										<h:form id="formContractList">
											<div class="MARGIN" style="width: 95%">
											<table border="0" class="layoutTable" width="100%" style="margin-left:15px;margin-right:15px;" >
											<tr>
													<td >
													
														<h:outputText  id="errormsg"     escape="false" styleClass="ERROR_FONT" value="#{pages$followUpActionsManage.errorMessages}"/>
						
													</td>
												</tr>
											</table>
												<table cellpadding="0" cellspacing="0" >
													<tr>
														<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
														<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>

												<div class="DETAIL_SECTION" >
												<h:outputLabel value="#{msg['followupaction.details']}" styleClass="DETAIL_SECTION_LABEL" />
													<table cellpadding="1px" cellspacing="3px" class="DETAIL_SECTION_INNER">

														<tr>
															<td >
																<h:outputLabel styleClass="LABEL" value="#{msg['followupaction.followUpActionEn']} :" />
															</td>
															<td width="70%">
																<h:inputText id="descEn" maxlength="100" style="width:85%" binding="#{pages$followUpActionsManage.txt_followUpActionDescEn}" />
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL" value="#{msg['followupaction.followUpActionAr']} :" />
															</td>
															<td width="70%">
																<h:inputText id="descAr" style="width:85%" binding="#{pages$followUpActionsManage.txt_followUpActionDescAr}" />
															</td>
														</tr>


														<tr>
															<td >
																<h:outputLabel styleClass="LABEL" value="#{msg['followupaction.followUpStatus']} :" />
															</td>
															<td >
																<h:selectOneMenu id="selectUnitType" binding="#{pages$followUpActionsManage.cmb_followUpStatus}" >
																	<f:selectItems value="#{pages$ApplicationBean.followUpStatus}" />
																</h:selectOneMenu>
															</td>
														</tr>
                                                        <div></div>        
														<tr>
															<td colspan="4">
																<DIV class="BUTTON_TD" style="padding-bottom: 4px;">
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.saveButton']}"
																		action="#{pages$followUpActionsManage.btnSave_Click}" />
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.reset']}"
																		action="#{pages$followUpActionsManage.reset}" />
																</DIV>
															</td>
														</tr>
													</table>
												</div>
											</div>

										</div>
										</h:form>
									</div>
								</td>
							</tr>

						</table>
					</td>
				</tr>
			    
			    				
				
			</table>
			
		</body>

	</html>
</f:view>