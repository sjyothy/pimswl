<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript"> 
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
	        if(hdnPersonType=='APPLICANT')
	        {
			 document.getElementById("inheritanceFileForm:hdnApplicantId").value=personId;
	        }
	        document.forms[0].submit();
	}

</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">

					<c:choose>
						<c:when test="${!pages$ManageBeneficiary.isViewModePopUp}">
							<tr
								style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>
					<tr width="100%">


						<c:choose>
							<c:when test="${!pages$ManageBeneficiary.isViewModePopUp}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['manageBeneficiary.header']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<h:outputText value="#{pages$ManageBeneficiary.errorMessages}"
											escape="false" styleClass="ERROR_FONT" />
										<h:outputText
											value="#{pages$ManageBeneficiary.successMessages}"
											escape="false" styleClass="INFO_FONT" />
										<h:messages></h:messages>
										<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="inheritanceFileForm"
												enctype="multipart/form-data" style="WIDTH: 97.6%;">


												<div class="AUC_DET_PAD">

													<div class="TAB_PANEL_INNER">
														<rich:tabPanel switchType="server"
															style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%">

															<rich:tab
																label="#{msg['mems.inheritanceFile.tabName.basicInfo']}"
																style="height:100px;"
																title="#{msg['mems.inheritanceFile.tabToolTip.basicInfo']}">
																<%@  include file="ManageBeneficiaryBasicInfoTab.jsp"%>
															</rich:tab>

															<rich:tab label="#{msg['inheritanceFile.tabHeader']}">
																<%@  include file="InheritanceFileTab.jsp"%>
															</rich:tab>

															<rich:tab label="#{msg['assets.tabHeader']}">
																<%@  include file="AssetTab.jsp"%>
															</rich:tab>

															<rich:tab
																label="#{msg['PeriodicDisbursements.disbursementList']}">
																<%@  include file="DisbursementListTab.jsp"%>
															</rich:tab>

															<rich:tab
																label="#{msg['socialResearch.tab.RecommendationDetails']}">
																<%@  include file="RecommendationDetailsTab.jsp"%>
															</rich:tab>

															<rich:tab label="#{msg['commons.retainedProfits']}"
																action="#{pages$ManageBeneficiary.onRetainedProfitDetailsTab}">
																<%@  include file="RetainedProfitDetailsTab.jsp"%>
															</rich:tab>


															<rich:tab label="#{msg['commons.commentsTabHeading']}">
																<%@ include file="notes/notes.jsp"%>
															</rich:tab>
															<%--  Attachment Tab Start --%>
															<rich:tab id="attachmentTab"
																label="#{msg['commons.attachmentTabHeading']}">
																<%@  include file="attachment/attachment.jsp"%>
															</rich:tab>
															<%--  Attachment Tab Finish --%>
															<rich:tab
																label="#{msg['mems.inheritanceFile.tabHeadingShort.history']}"
																action="#{pages$ManageBeneficiary.onRequestHistoryTabClick}">
																<%@ include file="../pages/requestTasks.jsp"%>
															</rich:tab>
														</rich:tabPanel>
													</div>

													<t:div styleClass="BUTTON_TD"
														style="padding-top:10px; padding-right:21px;">
														<h:commandButton styleClass="BUTTON"
															value="#{msg['report.heading.statementOfAccount']}"
															style="width: auto"
															action="#{pages$ManageBeneficiaryBasicInfoTab.openStatmntOfAccount}">
														</h:commandButton>
														<h:commandButton styleClass="BUTTON"
															value="#{msg['mems.inheritanceFile.buttonLabel.saveFile']}"
															style="width: auto"
															action="#{pages$ManageBeneficiaryBasicInfoTab.saveFile}">
														</h:commandButton>
														<h:commandButton styleClass="BUTTON"
															value="#{msg['researchFrom.researchButtonLabel']}"
															style="width: auto"
															action="#{pages$ManageBeneficiary.openResearchFrom}">
														</h:commandButton>

														<h:commandButton styleClass="BUTTON"
															value="#{msg['beneficiaryDetails.tooltip.financial']}"
															style="width: auto"
															action="#{pages$ManageBeneficiary.openFinancialAspectsPopup}">
														</h:commandButton>
													</t:div>
												</div>

											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">

								<c:choose>
									<c:when test="${!pages$ManageBeneficiary.isViewModePopUp}">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</c:when>
								</c:choose>

							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>