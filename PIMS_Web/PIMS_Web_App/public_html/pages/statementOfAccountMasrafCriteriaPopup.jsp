<%-- 
  - Author: Jawwad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: this popup wil save the remarks of user while block/unblock the contract.
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<%-- JAVA SCRIPT FUNCTIONS START --%>

<script type="text/javascript">



</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<body class="BODY_STYLE">

			<table width="100%" cellpadding="0" cellspacing="0" border="0">

				<tr width="100%">

					<td width="100%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel
										value="#{msg['commons.statementOfAccountCriteria']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99.1%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" valign="top" nowrap="nowrap">
									<h:form id="searchFrm">
										<div class="SCROLLABLE_SECTION">

											<t:div styleClass="MESSAGE" style="width:98%;">
												<table border="0" class="layoutTable" width="90%"
													style="margin-left: 15px; margin-right: 15px;">
													<tr>
														<td>
															<h:outputText id="errorMsg" escape="false"
																styleClass="ERROR_FONT"
																value="#{pages$statementOfAccountMasrafCriteriaPopup.errorMessages}" />
														</td>
														<td>
															<h:outputText id="successMsg" escape="false"
																styleClass="INFO_FONT"
																value="#{pages$statementOfAccountMasrafCriteriaPopup.successMessages}" />
														</td>

													</tr>
												</table>
											</t:div>

											<div class="MARGIN" style="width: 97%;">

												<div class="DETAIL_SECTION" style="width: 97%;">
													<h:outputLabel value="" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER" width="100%">
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.Masraf']}:"></h:outputLabel>
															</td>
															<td colspan="3">
																<h:inputText readonly="true" styleClass="READONLY"
																	value="#{pages$statementOfAccountMasrafCriteriaPopup.masraf.masrafName}" />
															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.from']}:"></h:outputLabel>
															</td>
															<td>
																<rich:calendar id="fromId"
																	locale="#{pages$statementOfAccountMasrafCriteriaPopup.locale}"
																	value="#{pages$statementOfAccountMasrafCriteriaPopup.fromDate}"
																	popup="true"
																	datePattern="#{pages$statementOfAccountMasrafCriteriaPopup.dateFormat}"
																	showApplyButton="false" enableManualInput="false"
																	inputStyle="width:185px;height:17px" />
															</td>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.to']}:"></h:outputLabel>
															</td>
															<td>
																<rich:calendar id="toid"
																	locale="#{pages$statementOfAccountMasrafCriteriaPopup.locale}"
																	value="#{pages$statementOfAccountMasrafCriteriaPopup.toDate}"
																	popup="true"
																	datePattern="#{pages$statementOfAccountMasrafCriteriaPopup.dateFormat}"
																	showApplyButton="false" enableManualInput="false"
																	inputStyle="width:185px;height:17px" />
															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.showSubMasarif']}:"></h:outputLabel>
															</td>
															<td>
																<h:selectBooleanCheckbox
																	value="#{pages$statementOfAccountMasrafCriteriaPopup.showSubMasarif }" />
															</td>
														</tr>
													</table>
												</div>
												<TABLE width="97%">
													<tr>
														<td class="BUTTON_TD" colspan="6">
															<h:commandButton styleClass="BUTTON" type="submit"
																action="#{pages$statementOfAccountMasrafCriteriaPopup.onOpenStatementOfAccount}"
																value="#{msg['commons.generate']}" />
														</td>
													</tr>
												</TABLE>
											</div>
									</h:form>
								</td>
							</tr>

						</table>
					</td>
				</tr>
			</table>


		</body>
	</html>
</f:view>