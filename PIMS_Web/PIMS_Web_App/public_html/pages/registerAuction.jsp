<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript"> 

		function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}


	function receivePayments(){
		var screen_width = 1025;
		var screen_height = 450;
		var popup_width = screen_width-200;
		var popup_height = screen_height;
		var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		var popup = window.open('receivePayment.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		popup.focus();
	}

	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		 document.getElementById("auctionRequestForm:bidderName").value=personName;		
		 document.getElementById("auctionRequestForm:bidderID").value=personId;	 
	     document.forms[0].submit();	     
	}

	function showPopupBidderSearch()
	{
	   var screen_width = 1024;
       var screen_height = 470;
       var popup_width = screen_width-200;
       var popup_height = screen_height;
       var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
	   window.open('SearchPerson.jsf?persontype=BIDDER&viewMode=popup','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+',scrollbars=yes,resizable=yes,status=yes');
	   		
	}
	function showPopupAuctionSearch()
	{
	   var screen_width = 1024;
       var screen_height = 450;
       var popup_width = screen_width-128;
       var popup_height = screen_height;
       var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
       window.open('auctionSearch.jsf?viewMode=popup','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=40,scrollbars=yes,status=yes');
       //resetSelectedUnits();
	   //var screen_width = screen.width;
	   //var screen_height = screen.height;
	   //window.open('RequestAuctionSearch.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-180)+',left=0,top=40,scrollbars=yes,status=yes');
	}
	
	function checkChanged()
	{
		//alert('check changed!');	
	}

	var totalDepositAmount=0;
	var totalSelected=0;
	var aucUnitNo;
	var amountToReceive = 0;
	function AddUnits(field,unitId,auctionUnitNo,depositAmount)
	{
		//alert('unit id : ' + unitId);
		//alert('auction unit id : ' + auctionUnitNo);
		//alert('deposit amount : ' + depositAmount);
	  totalDepositAmount = parseFloat(document.getElementById("auctionRequestForm:totalDeposit").value);
	  //alert('totalDepositAmount : ' + totalDepositAmount);
	  totalSelected = parseInt(document.getElementById("auctionRequestForm:totalSelected").value);
	  //alert('totalSelected : ' + totalSelected);
	  aucUnitNo = document.getElementById("auctionRequestForm:auctionUnitsOfRequest").value;
	  //alert('before : ' + aucUnitNo);
	  if(field.checked)
	  {
	  		//alert('check = true');
		    if(aucUnitNo!=null && aucUnitNo.length>0 )
		    {				
		       aucUnitNo=aucUnitNo+","+auctionUnitNo + ":" + unitId;		       
		    }
			else
			{
				aucUnitNo=auctionUnitNo + ":" + unitId; 
			}
			
			totalDepositAmount = parseFloat(totalDepositAmount) + parseFloat(depositAmount);
			totalSelected = totalSelected + 1;
   	   }
   	   else
   	   {
   	   	//alert('check = false');
	   	   var delimiters=",";
		   var unitArr=aucUnitNo.split(delimiters);
		   aucUnitNo="";
		   for(i=0;i<unitArr.length;i++)
		   {
			   if(unitArr[i]!=auctionUnitNo + ":" + unitId)
			   {
		       	   if(aucUnitNo.length>0 )
			       {
			             aucUnitNo=aucUnitNo+","+unitArr[i];
		
	               }
			       else
			       {
			             aucUnitNo=unitArr[i];
		           }
			   }		    
		   }
		   totalDepositAmount = parseFloat(totalDepositAmount) - parseFloat(depositAmount);
		   totalSelected = totalSelected - 1;
   	   }
   	   
   	   //alert('totalDepositAmount' + totalDepositAmount);
	   //alert('totalSelected' + totalSelected);
   	   
   	   //alert('after ' + aucUnitNo);
   	   
   	   document.getElementById("auctionRequestForm:inputTextTotalDepositAmount").value = totalDepositAmount.toFixed(2);
   	   document.getElementById("auctionRequestForm:inputTextTotalSelected").value = totalSelected;
   	   document.getElementById("auctionRequestForm:totalDeposit").value = totalDepositAmount;
   	   document.getElementById("auctionRequestForm:totalSelected").value = totalSelected;
   	   document.getElementById("auctionRequestForm:auctionUnitsOfRequest").value = aucUnitNo;
	   document.forms[0].submit();
	}
	
	function showPaymentPopup()
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
		window.open('receivePayment.jsf?parentHidden=auctionRequestForm:isPayPopupClosed','_blank','width='+(screen_width-10)+',height='+(screen_height-270)+',left=0,top=40,scrollbars=yes,status=yes');		
	}
	
	function resetSelectedUnits(){
		document.getElementById("auctionRequestForm:inputTextTotalDepositAmount").value = totalDepositAmount.toFixed(2);
   	    document.getElementById("auctionRequestForm:inputTextTotalSelected").value = totalSelected;
   	    document.getElementById("auctionRequestForm:totalDeposit").value = totalDepositAmount;
   	    document.getElementById("auctionRequestForm:totalSelected").value = totalSelected;
	}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 <script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>


	
</head>
	<body class="BODY_STYLE">
	<div class="containerDiv">
	
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header --> 
	<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
		<tr style="height:84px;width:100%;#height:84px;#width:100%;">
			<td colspan="2">
				<jsp:include page="header.jsp" />
			</td>
		</tr>
		<tr width="100%">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99.2%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['auction.registerAuction']}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>
				<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%">
					<tr valign="top">
								<td width="100%" valign="top" nowrap="nowrap">
						<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
							<div class="SCROLLABLE_SECTION" style="height:470px;width:100%;#height:470px;#width:100%;">
									<h:form id="auctionRequestForm" enctype="multipart/form-data" style="WIDTH: 97.6%;">
											
											<h:inputHidden id="bidderID" value="#{pages$registerAuction.hiddenBidderId}"></h:inputHidden>
											<h:inputHidden id="bidderName" value="#{pages$registerAuction.hiddenBidderName}"></h:inputHidden>
											<h:inputHidden id="auctionID" value="#{pages$registerAuction.auctionId}"></h:inputHidden>
											<h:inputHidden id="aNo" value="#{pages$registerAuction.auctionNo}"></h:inputHidden>
											<h:inputHidden id="aDate" value="#{pages$registerAuction.auctionDate}"></h:inputHidden>
											<h:inputHidden id="aRegStartDate" value="#{pages$registerAuction.regStartDate}"></h:inputHidden>
											<h:inputHidden id="aRegEndDate" value="#{pages$registerAuction.regEndDate}"></h:inputHidden>
											<h:inputHidden id="aTitle" value="#{pages$registerAuction.auctionTitle}"></h:inputHidden>
											<h:inputHidden id="aVenue" value="#{pages$registerAuction.auctionVenueName}"></h:inputHidden>
											<h:inputHidden id="bidderSocialSec" value="#{pages$registerAuction.hiddenSocialSecNo}"></h:inputHidden>
											<h:inputHidden id="bidderCellNo" value="#{pages$registerAuction.hiddenCellNumber}"></h:inputHidden>
											<h:inputHidden id="bidderNationality" value="#{pages$registerAuction.hiddenNationality}"></h:inputHidden>
											<h:inputHidden id="bidderEmail" value="#{pages$registerAuction.hiddenEmail}"></h:inputHidden>
											<h:inputHidden id="bidderPassport" value="#{pages$registerAuction.hiddenPassportNo}"></h:inputHidden>
											<h:inputHidden id="isPayPopupClosed" value="#{pages$registerAuction.isPaymentPopupClosed}"></h:inputHidden>
											<h:inputHidden id="reqReprinted" value="#{pages$registerAuction.requestReprinted}"></h:inputHidden>
											<h:inputHidden id="totalDeposit" value="#{pages$registerAuction.hiddenTotalDeposit}"></h:inputHidden> 
											<h:inputHidden id="totalSelected" value="#{pages$registerAuction.hiddenTotalSelected}"></h:inputHidden>
											<h:inputHidden id="auctionUnitsOfRequest" value="#{pages$registerAuction.hiddenAuctionUnitsOfRequest}"></h:inputHidden>
								
								<div> 
									<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
											<tr>
												<td>
													<h:outputText value="#{pages$registerAuction.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$registerAuction.infoMessages}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
									<div class="AUC_DET_PAD">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
						<div class="TAB_PANEL_INNER"> 
							<rich:tabPanel binding="#{pages$registerAuction.tabPanel}" id="registerAuctionTabPanel" switchType="server" style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%" >
								<rich:tab id="bidderInfoTab" label="#{msg['auction.registration.tabs.bidderInfo']}" >
									<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
										<t:panelGroup>
											<t:panelGrid columns="2">
												  <t:panelGroup>
												  <h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL" value="#{msg['bidder.name']}:"/>												  
												  </t:panelGroup>
												  <t:panelGroup>
													  <h:inputText id="bidderNameField" styleClass="INPUT_TEXT READONLY" readonly="true" value="#{pages$registerAuction.hiddenBidderName}" tabindex="1"  maxlength="20"></h:inputText>
												      <t:commandLink rendered="#{!pages$registerAuction.done}" actionListener="#{pages$registerAuction.setCount}" onclick="javascript:showPopupBidderSearch();" style="padding-left:2px; padding-right:2px;">
															<h:graphicImage id="searchCommandButtonBidder" title="#{msg['bidder.searchbidder']}" url="../resources/images/app_icons/searchBidder.png"/>
													  </t:commandLink>
												  </t:panelGroup>																	

											      <h:outputLabel styleClass="LABEL" value="#{msg['bidder.SSN']}: "></h:outputLabel> 
												  <h:inputText id="idNumberField" styleClass="INPUT_TEXT READONLY" readonly="true" value="#{pages$registerAuction.hiddenSocialSecNo}" tabindex="4"></h:inputText>
												  
												  <h:outputLabel styleClass="LABEL" value="#{msg['bidder.cellnumber']}: "></h:outputLabel>
											      <h:inputText id="cellNoField" styleClass="INPUT_TEXT READONLY" readonly="true" value="#{pages$registerAuction.hiddenCellNumber}" tabindex="6"></h:inputText>
											</t:panelGrid>
										</t:panelGroup>
										<t:panelGroup>
											<t:panelGrid columns="2">
											  <h:outputLabel styleClass="LABEL" value="#{msg['bidder.nationality']}: "></h:outputLabel> 
											  <h:inputText id="nationalityField" styleClass="INPUT_TEXT READONLY" readonly="true" value="#{pages$registerAuction.hiddenNationality}" tabindex="3"></h:inputText>
											  
											  <h:outputLabel styleClass="LABEL" value="#{msg['bidder.email']}: "></h:outputLabel>
										      <h:inputText id="emailField" styleClass="INPUT_TEXT READONLY" readonly="true" value="#{pages$registerAuction.hiddenEmail}" tabindex="5"></h:inputText>
										      <t:div style="height: 20px;" />
											</t:panelGrid>
										</t:panelGroup>
									</t:panelGrid>
								</rich:tab>
								<rich:tab id="auctionTab" label="#{msg['auction.registration.tabs.Auction']}" >
									 	<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
										<t:panelGroup>
											<t:panelGrid columns="2">
												  <t:panelGroup>
												  <h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL" value="#{msg['auction.number']}:"/>
												  
												  </t:panelGroup>
												  <t:panelGroup>
													  <h:inputText id="auctionNumberField" styleClass="INPUT_TEXT READONLY" readonly="true" value="#{pages$registerAuction.auctionNo}" tabindex="1"  maxlength="20"></h:inputText>
												      <t:commandLink rendered="#{!pages$registerAuction.done}" onclick="javascript:showPopupAuctionSearch();" style="padding-left:2px; padding-right:2px;">
															<h:graphicImage id="searchCommandButtonAuction" title="#{msg['auction.searchAuction']}" url="../resources/images/app_icons/Search-Auction.png"/>
													  </t:commandLink>
												  </t:panelGroup>																	
												  
												  <h:outputLabel styleClass="LABEL" value="#{msg['auction.requestStartDate']}: "></h:outputLabel>
											      <h:inputText id="regStartDateField" styleClass="INPUT_TEXT READONLY" readonly="true" value="#{pages$registerAuction.regStartDate}" tabindex="6"></h:inputText>
											</t:panelGrid>
										</t:panelGroup>
										<t:panelGroup>
											<t:panelGrid columns="2">
											  <h:outputLabel styleClass="LABEL" value="#{msg['auction.title']}: "></h:outputLabel> 
											  <h:inputText id="auctionDescField" styleClass="INPUT_TEXT READONLY" readonly="true" value="#{pages$registerAuction.auctionTitle}" tabindex="3"></h:inputText>
											  
											  <h:outputLabel styleClass="LABEL" value="#{msg['auction.requestEndDate']}: "></h:outputLabel>
											  <h:inputText id="regEndDateField" styleClass="INPUT_TEXT READONLY" readonly="true" value="#{pages$registerAuction.regEndDate}" tabindex="6"></h:inputText>
											</t:panelGrid>
										</t:panelGroup>
									</t:panelGrid>
										<t:div style="width:100%;">
										<t:div styleClass="contentDiv" style="width:98.2%; height:140px;">
											<t:dataTable id="dt1"
												value="#{pages$registerAuction.auctionDataList}"
												binding="#{pages$registerAuction.dataTable}"
												rows="5"
												preserveDataModel="false" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
												<t:column id="colPropertyRefNo" sortable="true" width="18%">
													<f:facet name="header">
														<t:outputText value="#{msg['property.refNumber']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.propertyRefNumber}" />
												</t:column>
												<t:column id="colPropertyCommName" sortable="true" width="18%">
													<f:facet name="header">
														<t:outputText value="#{msg['property.name']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.commercialName}" />
												</t:column>
												<t:column id="colUnitNo" sortable="true" width="18%">
													<f:facet name="header">
														<t:outputText value="#{msg['unit.number']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.unitNumber}" />
												</t:column>
												<t:column id="colUnitDesc" sortable="true" width="18%">
													<f:facet name="header">
														<t:outputText value="#{msg['unit.unitDescription']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.unitDesc}" />
												</t:column>
												
												<t:column id="colUnitTypeEn" width="18%" rendered="#{pages$registerAuction.isEnglishLocale}" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['unit.type']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.unitTypeEn}" />
												</t:column>
												<t:column id="colUnitTypeAr" width="18%" rendered="#{pages$registerAuction.isArabicLocale}" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['unit.type']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.unitTypeAr}" />
												</t:column>
												<t:column id="colDepositAmount" width="18%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['auctionUnit.depositColumn']}" />
													</f:facet>
													<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.depositAmount}">
														<f:convertNumber maxFractionDigits="2" minFractionDigits="2" pattern="#{pages$registerAuction.numberFormat}"/>
													</t:outputText>
												</t:column>
												<t:column id="colSelect" width="10%" rendered="#{!pages$registerAuction.done}">
													<f:facet name="header">
														<t:outputText   value="#{msg['commons.select']}"/>
													</f:facet>
													<h:selectBooleanCheckbox id="checkBoxes" style="text-align:center" value="#{dataItem.isSelected}" 
														onclick="javascript:AddUnits(this,'#{dataItem.unitId}','#{dataItem.auctionUnitId}','#{dataItem.depositAmount}');"/>
												</t:column>
											</t:dataTable>
										</t:div>
										<t:div styleClass="contentDivFooter" style="width:99.2%;">
										<t:dataScroller id="regScroller" for="dt1" paginator="true"
											fastStep="1" paginatorMaxPages="5" immediate="false"
											styleClass="JUG_SCROLLER_AUC_REG" paginatorTableClass="paginator"
											renderFacetsIfSinglePage="true" 												
											paginatorTableStyle="grid_paginator" layout="singleTable" 
											paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
											paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">
									
											<f:facet name="first">
												<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
											</f:facet>
											<f:facet name="fastrewind">
												<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
											</f:facet>
											<f:facet name="fastforward">
												<t:graphicImage url="../#{path.scroller_fastForward}" ></t:graphicImage>
											</f:facet>
											<f:facet name="last">
												<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
											</f:facet>

										</t:dataScroller>
									</t:div>
								</t:div>
								<t:div>
									<t:panelGrid columns="1">
										<t:panelGroup>
											<h:outputLabel styleClass="LABEL" style="padding-right: 5px;" value="#{msg['auction.totalDepositAmount']} : " ></h:outputLabel>
											<h:inputText id="inputTextTotalDepositAmount" styleClass="INPUT_TEXT A_RIGHT_NUM" readonly="true" value="#{pages$registerAuction.hiddenTotalDeposit}" style="padding-left: 2px; padding-right: 2px; display:inline;"></h:inputText>
										</t:panelGroup>
										<t:panelGroup>
											<h:outputLabel styleClass="LABEL REG_AUC_SEL_LAB" value="#{msg['auction.registration.totalSelectedUnits']} : "></h:outputLabel>
											<h:inputText id="inputTextTotalSelected" styleClass="INPUT_TEXT A_RIGHT_NUM" readonly="true" value="#{pages$registerAuction.hiddenTotalSelected}" style="padding-left: 2px; padding-right: 2px; display:inline;"></h:inputText>
										</t:panelGroup>
									</t:panelGrid>
								</t:div>
							</rich:tab>
							    <rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}">
									<%@ include file="attachment/attachment.jsp"%>
								</rich:tab>
								<rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}">
									<%@ include file="notes/notes.jsp"%>
								</rich:tab>
								<rich:tab id="paymentsTab" label="#{msg['auction.registration.tabs.payments']}" action="#{pages$registerAuction.tabPaymentSchedule_Click}">
                                   			 <t:div styleClass="contentDiv" style="width:97.1%"  >
                                                 <t:dataTable id="tbl_PaymentSchedule" rows="7" width="100%"
													value="#{pages$registerAuction.paymentScheduleDataList}"
													binding="#{pages$registerAuction.paymentScheduleDataTable}"
													preserveDataModel="true"  var="paymentScheduleDataItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
													
												 <t:column id="col_PaymentNumber" style="width:15%;" sortable="true" rendered="#{pages$registerAuction.isPaymentCollected}">
													<f:facet name="header" >
														<t:outputText value="#{msg['paymentSchedule.paymentNumber']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{paymentScheduleDataItem.paymentNumber}" />
												</t:column>
												<t:column id="colTypeEn" rendered="#{pages$registerAuction.isEnglishLocale}" sortable="true">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentType']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{paymentScheduleDataItem.typeEn}" />
												</t:column>
												<t:column id="colTypeAr" rendered="#{pages$registerAuction.isArabicLocale}" sortable="true">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentType']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{paymentScheduleDataItem.typeAr}" />
												</t:column>
												<t:column id="colDueOn" rendered="false" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['paymentSchedule.paymentDueOn']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{paymentScheduleDataItem.paymentDueOnString}" />
												</t:column>
                                                <t:column id="colPaymentDate" rendered="false" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['paymentSchedule.paymentDate']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT"  value="#{paymentScheduleDataItem.paymentDate}" />
												</t:column>
												<t:column id="colModeEn" rendered="#{pages$registerAuction.isEnglishLocale}" sortable="true">
													<f:facet name="header" >
														<t:outputText value="#{msg['paymentSchedule.paymentMode']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{paymentScheduleDataItem.paymentModeEn}" />
												</t:column>
												<t:column id="colModeAr" rendered="#{pages$registerAuction.isArabicLocale}" sortable="true">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentMode']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{paymentScheduleDataItem.paymentModeAr}" />
												</t:column>
												<t:column id="colStatusEn" rendered="#{pages$registerAuction.isEnglishLocale}" sortable="true">
													<f:facet name="header" >
														<t:outputText value="#{msg['paymentSchedule.paymentStatus']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{paymentScheduleDataItem.statusEn}" />
												</t:column>
												<t:column id="colStatusAr" rendered="#{pages$registerAuction.isArabicLocale}" sortable="true">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentStatus']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{paymentScheduleDataItem.statusAr}" />
												</t:column>
												<t:column id="colReceiptNumber" style="width:15%;" rendered="#{pages$registerAuction.isPaymentCollected && false}" sortable="true">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.ReceiptNumber']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{paymentScheduleDataItem.paymentReceiptNumber}" />
												</t:column>
												<t:column id="colAmount" sortable="true">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.amount']}" />
													</f:facet>
													<t:outputText styleClass="A_RIGHT_NUM" value="#{paymentScheduleDataItem.amount}" />
												</t:column>
												 <t:column id="actionCol"  sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.action']}" />
													</f:facet>
													<h:commandLink action="#{pages$registerAuction.btnEditPaymentSchedule_Click}">
													 <h:graphicImage id="editPayments" 
																		title="#{msg['commons.edit']}" 
																		url="../resources/images/edit-icon.gif"
																		rendered="#{paymentScheduleDataItem.isReceived == 'N'}"
																		/>
													</h:commandLink>
													<h:commandLink rendered="false" action="#{pages$registerAuction.openReceivePaymentsPopUp}">&nbsp;
													    <h:graphicImage id="receivePayments" 
																		title="#{msg['contract.paymentSchedule.receivePayment']}" 
																		url="../resources/images/app_icons/Pay-Fine.png"
																		rendered="#{paymentScheduleDataItem.isReceived == 'N'  && paymentScheduleDataItem.statusId==pages$registerAuction.paymentSchedulePendingStausId}"
																		/>
													</h:commandLink>
													<a4j:commandLink rendered="false" reRender="tbl_PaymentSchedule" action="#{pages$registerAuction.btnDelPaymentSchedule_Click}">&nbsp;
												       <h:graphicImage id="deletePaySc" 
																title="#{msg['commons.delete']}" 
																rendered="#{(paymentScheduleDataItem.isReceived == 'N' && paymentScheduleDataItem.typeId==pages$registerAuction.rentIdFromDomainData)}"
																url="../resources/images/delete_icon.png"/>														
													</a4j:commandLink>             
												</t:column>
											  </t:dataTable>		
										    </t:div>
										    <t:div id="pagingDivPaySch" styleClass="contentDivFooter" style="width:98.2%;">

												<t:dataScroller id="scrollerPaySch" for="tbl_PaymentSchedule" paginator="true"
													fastStep="1" paginatorMaxPages="15" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 
													pageIndexVar="paySchedPageNumber"
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-weight:bold;"
													paginatorRenderLinkForActive="false" 
														
														styleClass="SCH_SCROLLER">
                                                	<f:facet  name="first">
														<t:graphicImage  url="../#{path.scroller_first}"  id="lblFPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="fastrewind">
														<t:graphicImage  url="../#{path.scroller_fastRewind}"  id="lblFRPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="fastforward">
														<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="last">
														<t:graphicImage  url="../#{path.scroller_last}" id="lblLPaySch"></t:graphicImage>
													</f:facet>
												</t:dataScroller>
                                           </t:div>
                                           <t:div styleClass="BUTTON_TD">
												<h:commandButton id="receivePaymentButton" styleClass="BUTTON" value="#{msg['auction.registration.collectPaymentButton']}" rendered="#{!pages$registerAuction.isPaymentCollected && false}"
																action="#{pages$registerAuction.openReceivePaymentsPopUp}" style="width: 150px;">
												</h:commandButton>	
                                           </t:div>
                                   		</rich:tab>
							</rich:tabPanel>
													
							</div>
							<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT" /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID" style="width:100%;" /></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
						<t:div styleClass="BUTTON_TD" style="padding-top:10px;">
							<h:commandButton id="buttonReprintReceipt" styleClass="BUTTON" value="#{msg['auction.reprintReceipt']}" style="width: 100px" 
											rendered="false" action="#{pages$registerAuction.rePrintReceipt}">
							</h:commandButton>
					       	<h:inputHidden id="IsRequestSaved" value="#{pages$registerAuction.requestSaved}"></h:inputHidden>
					       	
							
							<h:commandButton id="buttonSave" styleClass="BUTTON" value="#{msg['auction.registration.registerButton']}" onclick="if (!confirm('#{msg['auction.registration.registerConfirm']}')) return false"
			        	 					action="#{pages$registerAuction.registerUnits}" rendered="#{!pages$registerAuction.done}" style="width: 120px">
			       			</h:commandButton>
						</t:div>
						</div>
								</div>
							</h:form>
					  </div> 
						</td>			
					</tr>
				</table>
			</td>
		</tr>
		  
		<tr style="height:10px;width:100%;#height:10px;#width:100%;">
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
	</table>
	</div>
</body>
</html>
</f:view>