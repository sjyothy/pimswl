


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
        
		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
</script>
    
<f:view>
        <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
       <f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
 
    
    <html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>						
            <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
			
			
 	</head>
     
<script language="javascript" type="text/javascript">

//	function calculateGuaranteeAmount()
//    {
//		var percent = parseFloat(document.getElementById("formRequest:txtGuaranteePercentage").value);
//		var contractAmount = parseFloat(document.getElementById("formRequest:totalContract").value);
//		var guranteeAmount = percent*contractAmount /100;
//		document.getElementById("formRequest:txtBankGuaranteeAmount").value="";
//		if(!isNaN( guranteeAmount ))
//		{
//		 document.getElementById("formRequest:txtBankGuaranteeAmount").value=guranteeAmount ;
//		}
//    }
    
    function calculateProjectAmount()
    {
     document.getElementById("formRequest:lnkCalculatePayment").onclick();
    }
    
    function calculatePercentageChange()
    {
     document.getElementById("formRequest:lnkPercentageChange").onclick();
    }
    function showSearchProjectPopUp(pageModeKey,selectOnePopKey,AllowedStatusKey,AllowedStatusValue,AllowedTypeKey,AllowedTypeValue)
    {
	     var  screen_width = 1024;
	     var screen_height = 450;
	     
	     window.open('projectSearch.jsf?'+pageModeKey+'='+selectOnePopKey+'&context=ConstructionContract','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=20,top=150,scrollbars=yes,status=yes');    
    
    }
    function populateProject(projectId,projectName,projectNumber,estimatedCost)
    {
    		document.getElementById("formRequest:hdnProjectId").value=projectId;
		    document.forms[0].submit();
    }
    function showSearchTenderPopUp(tenderTypeKey,winnerContractorPresent,notBindedWithContract)
	{
	     
	     var  screen_width = 1024;
	     var screen_height = 450;
	     window.open('constructionTenderSearch.jsf?VIEW_MODE=popup&context=ConstructionContract','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=20,top=150,scrollbars=yes,status=yes');

	  
	}
	function showSearchContractorPopUp(viewMode,popUp)
	{
	     
	     var  screen_width = 1024;
	     var screen_height = 450;
	     
	     window.open('contractorSearch.jsf?'+viewMode+'='+popUp+'&contractorTypeId=1','_blank','width='+(screen_width-10)+',height='+(screen_height)+',left=0,top=40,scrollbars=no,status=yes');
	  
	}
	function populateContractor(contractorId, contractorNumber, contractorNameEn, contractorNameAr, licenseNumber, licenseSource, officeNumber){
			document.getElementById("formRequest:hdnContractorId").value=contractorId;
			document.getElementById("formRequest:txtContractorNumber").value=contractorNumber;
		    document.forms[0].submit();
		}
	function populateTender(tenderId,tenderNumber,tenderDescription)
	{
	  document.getElementById("formRequest:txtTenderNumber").value=tenderNumber;
	  document.getElementById("formRequest:hdnTenderId").value=tenderId;
	  document.forms[0].submit();
	}
	
	
      
    </script>
 	 

				<!-- Header -->
	
		<body class="BODY_STYLE" >
	     <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
					<c:when test="${!pages$constructionContract.isViewModePopUp}">
					 <td colspan="2">
				        <jsp:include page="header.jsp"/>
				     </td>
				     </c:when>
		        </c:choose>
	            </tr>
				<tr>
				    <c:choose>
					 <c:when test="${!pages$constructionContract.isViewModePopUp}">
					  <td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					  </td>
				     </c:when>
		            </c:choose>
					<td width="83%"  height="45PX" valign="top" class="divBackgroundBody">
						<h:form id="formRequest" style="width:99.7%"  enctype="multipart/form-data">
						<table  class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
							<tr>
							    <td class="HEADER_TD" style="width:70%;" >
										 <h:outputLabel value="#{pages$constructionContract.pageTitle}"  styleClass="HEADER_FONT"/>
								</td>
						        <td >
									&nbsp;
								</td>
								
								
							</tr>
						</table>
					        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
                                 <tr valign="top">
                                  <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
                                   </td>    
									<td  height="450PX" valign="top" nowrap="nowrap">
									<div  class="SCROLLABLE_SECTION"  >
									  
											<table border="0" class="layoutTable" width="94%" style="margin-left:15px;margin-right:15px;" >
											<tr>
													<td >
													    <h:outputText  id="successmsg"   escape="false" styleClass="INFO_FONT"  value="#{pages$constructionContract.successMessages}"/>
														<h:outputText  id="errormsg"     escape="false" styleClass="ERROR_FONT" value="#{pages$constructionContract.errorMessages}"/>
														<h:inputHidden id="hdnContractId" value="#{pages$constructionContract.contractId}" />
														<h:inputHidden id="hdnContractCreatedOn" value="#{pages$constructionContract.contractCreatedOn}" />
														<h:inputHidden id="hdnContractCreatedBy" value="#{pages$constructionContract.contractCreatedBy}" />
														<h:inputHidden id="hdnRequestId" value="#{pages$constructionContract.requestId}" />
														<h:inputHidden id="hdnContractorId" value="#{pages$constructionContract.hdnContractorId}"/>
														<h:inputHidden id="hdnTenderId" value="#{pages$constructionContract.hdnTenderId}"/>
														<h:inputHidden id="hdnSearchTenderType" value="#{pages$constructionContract.tenderTypeConstruction}"/>
														<h:inputHidden id="hdnProjectId" value="#{pages$constructionContract.hdnProjectId}"/>
                                                        <h:commandLink id="cmdDeleteTender" action="#{pages$constructionContract.imgRemoveTender_Click}"/>														
													</td>
												</tr>
											</table>
										
										<div class="MARGIN" style="width:94%;margin-bottom:0px;" >
   
                                         <t:panelGrid id ="tbl_Action" width="95%" border="0" columns="1"  binding="#{pages$constructionContract.tbl_Action}"  >
                                         
                                          <t:panelGrid id ="tbl_Remarks" cellpadding="1px" cellspacing="3px" width="100%" border="0" columns="2"  >
                                          <h:panelGroup>
				                           <h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
                                             <t:outputLabel  value="#{msg['commons.remarks']}:" />
                                           </h:panelGroup>
                                         
                                          <t:inputTextarea id="txt_remarks" cols="100" value="#{pages$constructionContract.txtRemarks}" />
                                         </t:panelGrid>
                                          <t:panelGrid id ="tbl_ActionBtn" cellpadding="1px" cellspacing="3px" width="100%" border="0" columns="1"  columnClasses="BUTTON_TD">
                                          <t:panelGroup>
                                                 <pims:security screen="Pims.ConstructionMgmt.ConstructionContract.ReviewRequired" action="view">
                                                     <h:commandButton id= "btnReviewReq" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$constructionContract.btnReviewReq_Click}"
			                                           			binding="#{pages$constructionContract.btnReviewReq}"  
			                                           			value="#{msg['commons.review']}" />
                                                 </pims:security> 	 			                                           			
			                                     <pims:security screen="Pims.ConstructionMgmt.ConstructionContract.ApproveReject" action="view">      			
			                                          <h:commandButton id= "btnApprove" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$constructionContract.btnApprove_Click}"
			                                           			binding="#{pages$constructionContract.btnApprove}"  
			                                           			value="#{msg['commons.approve']}" />
			                                          <h:commandButton id= "btnReject" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$constructionContract.btnReject_Click}"
			                                           			binding="#{pages$constructionContract.btnReject}"  
			                                           			value="#{msg['commons.reject']}" />
			                                     </pims:security> 			
			                                     <pims:security screen="Pims.ConstructionMgmt.ConstructionContract.Review" action="view">
			                                          <h:commandButton id= "btnReview" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$constructionContract.btnReview_Click}"
			                                           			binding="#{pages$constructionContract.btnReview}"  
			                                           			value="#{msg['commons.reviewButton']}" />
			                                     </pims:security>
			                                     <pims:security screen="Pims.ConstructionMgmt.ConstructionContract.Complete" action="view">
			                                         <h:commandButton id= "btnComplete" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$constructionContract.btnComplete_Click}"
			                                           			binding="#{pages$constructionContract.btnComplete}"  
			                                           			value="#{msg['commons.complete']}" />
			                                       </pims:security> 			
			                                       <pims:security screen="Pims.ConstructionMgmt.ConstructionContract.Print" action="view">
			                                         <h:commandButton id= "btnPrint" type="submit"  styleClass="BUTTON" 
			                                           			action="#{pages$constructionContract.btnPrint_Click}"
			                                           			binding="#{pages$constructionContract.btnPrint}"
			                                           			value="#{msg['commons.print']}" />
			                                       </pims:security>			
                                         </t:panelGroup>
                                         </t:panelGrid>
                                       </t:panelGrid>
											
										<table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
										</table>	
						 				  <rich:tabPanel id="tabPanel"  binding="#{pages$constructionContract.tabPanel}" style="width:100%;height:320px;" headerSpacing="0">
						 				           <rich:tab id="tabContractDetails" action="#{pages$constructionContract.onCalculatePayment}"  label="#{msg['contract.tab.BasicInfo']}"  title="#{msg['contract.tab.BasicInfo']}"  >
									                           <%@ include file="../pages/constructionContractDetailsTab.jsp"%>
													</rich:tab>
													<rich:tab   id="tabProject" binding="#{pages$constructionContract.tabProjectDetails}" action="#{pages$constructionContract.tabProjectDetails_Click}"   title="#{msg['constructionContract.tab.projectDetails']}" label="#{msg['constructionContract.tab.projectDetails']}" >
				                                            <%@ include file="../pages/projectDetailsTab.jsp"%>
				                                   	</rich:tab>
													<rich:tab  id="tabPaymentSchedule" binding="#{pages$constructionContract.tabPaymentTerms}" action="#{pages$constructionContract.tabPaymentTerms_Click}"   title="#{msg['contract.paymentTerms']}" label="#{msg['contract.paymentTerms']}" >
				                                            <%@ include file="../pages/serviceContractPaySchTab.jsp"%>
				                                   	</rich:tab>
												    <rich:tab label="#{msg['contract.tabHeading.RequestHistory']}"  title="#{msg['commons.tab.requestHistory']}"  action="#{pages$constructionContract.tabAuditTrail_Click}">
									                           <%@ include file="../pages/requestTasks.jsp"%>
													</rich:tab>
													<rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}" action= "#{pages$constructionContract.tabAttachmentsComments_Click}">
															   <%@  include file="attachment/attachment.jsp"%>
													</rich:tab>
											        <rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}" action= "#{pages$constructionContract.tabAttachmentsComments_Click}">
															   <%@ include file="notes/notes.jsp"%>
													</rich:tab>
												
				                            </rich:tabPanel>
				                           </div>
                                    
                                   
                                        <table cellpadding="0" cellspacing="0"  style="width:94.2%;margin-left:10px;">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
										
											<br/>
											
										
										<table cellpadding="1px" cellspacing="3px" width="95%" >
											<tr>
											
											    <td colspan="12" class="BUTTON_TD">
											    <pims:security screen="Pims.ConstructionMgmt.ConstructionContract.Persist" action="view">
                                                    <h:commandButton type="submit"  styleClass="BUTTON" 
                                           			action="#{pages$constructionContract.btnSave_Click}"
                                           			binding="#{pages$constructionContract.btnSave}"  
                                           			value="#{msg['commons.saveButton']}" />
                                           		</pims:security>
                                           		<pims:security screen="Pims.ConstructionMgmt.ConstructionContract.SendForApproval" action="view">
                                           		 <h:commandButton id= "btnSendForApproval" type="submit"  styleClass="BUTTON" 
                                           			action="#{pages$constructionContract.btnSendForApproval_Click}"
                                           			binding="#{pages$constructionContract.btnSend_For_Approval}"  
                                           			value="#{msg['commons.sendButton']}" />
                                           		</pims:security>	
										             
										        </td>
		                                   </tr>
		                                </table>
                                          
                                   </div>
                            </div>
									</td>
									</tr>
									</table>
			</h:form>
			</td>
    </tr>
    <tr>
			<td colspan="2">
			
			    <table width="100%" cellpadding="0" cellspacing="0" border="0">
			       <tr><td class="footer"><h:outputLabel value="#{msg['commons.footer.message']}" /></td></tr>
			    </table>
			</td>
    </tr>
    </table>
			</div>
		</body>
	</html>
</f:view>

