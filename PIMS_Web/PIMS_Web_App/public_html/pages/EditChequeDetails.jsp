<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">




<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />



			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

		</head>

		<script language="javascript" type="text/javascript">
	
	function closeWindow()
	{
	  window.opener.fromEditCheque();		
	  window.close();
	}	
    </script>


		<!-- Header -->

		<body class="BODY_STYLE">

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">

					<td width="60%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['payments.msg.editChequeDetails']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="99%" valign="top" nowrap="nowrap">


									<h:form id="formPaymentSchedule" style="width:92%"
										enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION">
											<table border="0" class="layoutTable">

												<tr>

													<td colspan="3">
														<h:outputText id="successmsg" escape="false"
															styleClass="INFO_FONT"
															value="#{pages$EditChequeDetails.successMessages}" />
														<h:outputText id="errormsg" escape="false"
															styleClass="ERROR_FONT"
															value="#{pages$EditChequeDetails.errorMessages}" />

													</td>
												</tr>
											</table>

											<div class="MARGIN" style="width: 100%">
												<table cellpadding="0" cellspacing="0" width="90.3%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width: 90%">
													<h:outputLabel
														value="#{msg['paymentSchedule.paymentDetails']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="1px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.replaceCheque.oldChequeDate']}">:</h:outputLabel>
															</td>
															<td>
																<rich:calendar id="clndrChequeDate"
																	value="#{pages$EditChequeDetails.chequeDate}"
																	popup="true" showApplyButton="false"
																	datePattern="#{pages$EditChequeDetails.dateFormat}"
																	locale="#{pages$EditChequeDetails.locale}"
																	enableManualInput="false" cellWidth="24px" />
															</td>
														</tr>
														<tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['paymentSchedule.paymentMode']}">:</h:outputLabel>
															</td>
															<td>
																<h:inputText id="paymentMode"
																	value="#{pages$EditChequeDetails.selectedPaymentMode}"
																	readonly="true" styleClass="READONLY">
																</h:inputText>
															</td>
														</tr>
														<tr>
															<td>
																&nbsp;&nbsp;
															</td>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['paymentSchedule.amount']}">:</h:outputLabel>
															</td>
															<td>
																<h:inputText id="amount"
																	styleClass="A_RIGHT_NUM READONLY"
																	value="#{pages$EditChequeDetails.amount}"
																	readonly="true">

																</h:inputText>
															</td>

														</tr>
														<tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['bouncedChequesList.bankNameCol']}">:</h:outputLabel>
															</td>
															<td>
																<h:selectOneMenu id="bankList"
																	value="#{pages$EditChequeDetails.selectedBankId}">
																	<f:selectItems
																		value="#{pages$ApplicationBean.bankList}" />
																</h:selectOneMenu>
															</td>
														</tr>
														<tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['bouncedChequesList.chequeNumberCol']}">:</h:outputLabel>
															</td>
															<td>
																<h:inputText id="ChequeNumbec"
																	value="#{pages$EditChequeDetails.chequeNumber}" />
															</td>
														</tr>
														<tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['chequeList.chequeType']} :"></h:outputLabel>
															</td>
															<td>
																<h:selectOneMenu styleClass="SELECT_MENU"
																	id="cboChequeType"
																	value="#{pages$EditChequeDetails.chequeType}">
																	<f:selectItems
																		value="#{pages$ApplicationBean.chequeType}" />
																</h:selectOneMenu>
															</td>
														</tr>
													</table>

												</div>
												<table width="90%">
													<tr>

														<td colspan="6" class="BUTTON_TD">
															<h:commandButton styleClass="BUTTON"
																action="#{pages$EditChequeDetails.btnSave_Click}"
																value="#{msg['AuctionDepositRefundApproval.Buttons.Save']}" />

															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.closeButton']}"
																onclick="closeWindow();"
																style="width: 75px; margin-right: 2px;margin-left: 2px;" />

														</td>
													</tr>
												</table>
											</div>
										</div>
									</h:form>



								</td>
							</tr>
						</table>

					</td>
				</tr>

			</table>

		</body>
	</html>
</f:view>

