


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />


			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->


			<script language="javascript" type="text/javascript">
    function clearWindow()
	{
       
        
            document.getElementById("formSearchPerson:txtfirstname").value="";
			document.getElementById("formSearchPerson:txtlastname").value="";	
			document.getElementById("formSearchPerson:txtpassportNumber").value="";
			document.getElementById("formSearchPerson:txtresidenseVisaNumber").value="";
			document.getElementById("formSearchPerson:txtsocialSecNumber").value="";
		
			
	}
	function addToContract()
	{
	   
	   
	  window.opener.document.forms[0].submit();
	  
	  window.close();
	}
	function sendToParent(PersonName,PersonId)
	{
	var controlName=document.getElementById("formSearchPerson:hdnControlName").value;
	
	var controlForId=document.getElementById("formSearchPerson:hdncontrolForId").value;
	var hdnDisplaycontrol=document.getElementById("formSearchPerson:hdndisplaycontrolName").value;
	window.opener.addSponsorManager(controlName,controlForId,PersonName,PersonId,hdnDisplaycontrol);
	window.close();
	}
	function closeWindow()
	{
	
	  window.opener="x";
	  window.close();
	
	}
	
       </SCRIPT>



		</head>
		<table width="100%" cellpadding="0" cellspacing="0" border="0">


			<tr width="100%">

				<td width="83%" valign="top" class="divBackgroundBody">
					<table width="99%" class="greyPanelTable" cellpadding="0"
						cellspacing="0" border="0">
						<tr>
							<td class="HEADER_TD">
								<h:outputLabel value="#{msg['commons.searchPerson']}"
									styleClass="HEADER_FONT" />
							</td>
							<td width="100%">
								&nbsp;
							</td>
						</tr>
					</table>
					<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
						cellspacing="0" border="0">
						<tr valign="top">
							<td height="100%" valign="top" nowrap="nowrap"
								background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
								width="1">
							</td>
							<td width="100%" height="100%" valign="top" nowrap="nowrap">
								<div style="height: 450; overflow: auto;">
									<h:form id="formSearchPerson">
										<table id="SearchCriteria" width="100%">
											<tr>
												<td colspan="6">
													<h:inputHidden id="hdnPersonType"
														value="#{pages$PersonSearch.requestForPersonType}" />
													<h:inputHidden id="hdnControlName"
														value="#{pages$PersonSearch.controlName}" />
													<h:inputHidden id="hdncontrolForId"
														value="#{pages$Person.controlForId}" />
													<h:inputHidden id="hdndisplaycontrolName"
														value="#{pages$Person.hdndisplaycontrolName}" />
													<h:messages></h:messages>
												</td>
											</tr>
											<tr>
												<td>
													&nbsp;&nbsp;
												</td>
												<td style="display: none">
													<h:outputLabel value="#{msg['customer.personType']}"></h:outputLabel>
												</td>
												<td width="35%" style="display: none">
													<h:selectOneMenu id="selectPersonType" value=" ">
														<f:selectItem itemLabel="#{msg['commons.All']}"
															itemValue="-1" />
													</h:selectOneMenu>
												</td>



											</tr>

											<tr>
												<td>
													&nbsp;&nbsp;
												</td>
												<td width="20%">
													<h:outputLabel value="#{msg['customer.firstname']}"></h:outputLabel>
												</td>
												<td>
													<h:inputText maxlength="15" id="txtfirstname"
														value="#{pages$Person.firstName}">
													</h:inputText>
												</td>
												<td>
													&nbsp;&nbsp;
												</td>
												<td>
													&nbsp;&nbsp;
												</td>
												<td>
													<h:outputLabel value="#{msg['customer.lastname']}"></h:outputLabel>
												</td>
											
												<td>
													<h:inputText maxlength="15" id="txtlastname"
														value="#{pages$Person.lastName}">
													</h:inputText>
												</td>


											</tr>
											<tr>
												<td>
													&nbsp;&nbsp;
												</td>
												<td>
													<h:outputLabel value="#{msg['customer.socialSecNumber']}"></h:outputLabel>
												</td>
												<td>
													<h:inputText maxlength="15" id="txtsocialSecNumber"
														value="#{pages$Person.socialSecNumber}">
													</h:inputText>
												</td>
												<td>
													&nbsp;&nbsp;
												</td>
											
												<td>
													&nbsp;&nbsp;
												</td>
												<td>
													<h:outputLabel value="#{msg['customer.passportNumber']}"></h:outputLabel>
												</td>
												<td>
													<h:inputText maxlength="15" id="txtpassportNumber"
														value="#{pages$Person.passportNumber}">
													</h:inputText>
												</td>

											</tr>
											<tr>
												<td>
													&nbsp;&nbsp;
												</td>
												<td>
													<h:outputLabel
														value="#{msg['customer.residenseVisaNumber']}"></h:outputLabel>
												</td>
												<td>
													<h:inputText maxlength="15" id="txtresidenseVisaNumber"
														value="#{pages$Person.residenseVisaNumber}">
													</h:inputText>
												</td>

											</tr>

											<tr>
												<td>
													&nbsp;&nbsp;
												</td>
												<td colspan="6">
													<h:commandButton type="submit" styleClass="BUTTON"
														value="#{msg['commons.search']}"
														action="#{pages$Person.doSearch}"></h:commandButton>

													<h:commandButton type="submit" styleClass="BUTTON"
														action="#{pages$Person.btnAdd_Click}"
														value="#{msg['commons.Add']}" />

													<h:commandButton styleClass="BUTTON" type="button"
														value="#{msg['commons.reset']}" style="width: 54px"
														onclick="javascript:clearWindow();" />

												</td>

											</tr>
										</table>

										<t:div styleClass="contentDiv" style="width:95%">
											<t:dataTable id="test3" rows="15" width="100%"
												styleClass="grid"
												value="#{pages$Person.propertyInquiryDataList}"
												binding="#{pages$Person.dataTable}" preserveDataModel="true"
												preserveSort="false" var="dataItem" rowClasses="row1,row2"
												rules="all" renderedIfEmpty="false">

												<t:column id="col2" sortable="true">


													<f:facet name="header">

														<t:outputText value="#{msg['customer.firstname']}" />

													</f:facet>
													<t:outputText
														value="#{dataItem.firstName} #{dataItem.middleName} #{dataItem.lastName}" />
												</t:column>
												<t:column id="col4" sortable="true">


													<f:facet name="header">

														<t:outputText value="#{msg['customer.passportNumber']}" />

													</f:facet>
													<t:outputText title="" value="#{dataItem.passportNumber}" />
												</t:column>

												<t:column id="col5" sortable="true">


													<f:facet name="header">

														<t:outputText
															value="#{msg['customer.residenseVisaNumber']}" />

													</f:facet>
													<t:outputText title=""
														value="#{dataItem.residenseVisaNumber}" />
												</t:column>

												<t:column id="col6" sortable="true">


													<f:facet name="header">

														<t:outputText
															value="#{msg['customer.drivingLicenseNumber']}" />

													</f:facet>
													<t:outputText title=""
														value="#{dataItem.drivingLicenseNumber}" />
												</t:column>

												<t:column id="col7" sortable="true">


													<f:facet name="header">

														<t:outputText value="#{msg['customer.socialSecNumber']}" />

													</f:facet>
													<t:outputText title="" value="#{dataItem.socialSecNumber}" />
												</t:column>

												<t:column id="col8">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.referencenumber']}" />
													</f:facet>
													<t:commandButton styleClass="BUTTON"
														rendered="#{pages$Person.isSelectOnePerson}"
														onclick="javascript:sendToParent('#{dataItem.firstName} #{dataItem.middleName} #{dataItem.lastName}','#{dataItem.personId}');"
														value="#{msg['commons.select']}" />
													<t:commandButton styleClass="BUTTON"
														action="#{pages$Person.btnAddPersonToProspectiveSession_Click}"
														rendered="#{pages$Person.isSelectManyPerson}"
														value="#{msg['commons.select']}" />
												</t:column>
											</t:dataTable>
										</t:div>
										<table width="100%">
											<tr>
												<td>
													&nbsp;&nbsp;&nbsp;
												</td>
												<td align="center">
													<h:commandButton styleClass="BUTTON" style="width:179px"
														rendered="#{pages$Person.showProspectiveGrid}"
														value="#{msg['paidFacilities.sendToContract']}"
														onclick="javascript:addToContract();" />
												</td>
											</tr>
										</table>
										<t:div styleClass="contentDiv" style="width:95%"
											rendered="#{pages$Person.showProspectiveGrid}">

											<t:dataTable id="prospective" rows="10" width="100%"
												value="#{pages$Person.prospectivePersonDataList}"
												binding="#{pages$Person.propspectivePersonDataTable}"
												preserveDataModel="true" preserveSort="false"
												var="prospectiveDataItem" rowClasses="row1,row2" rules="all"
												renderedIfEmpty="false">
												<t:column id="col_PersonName">
													<f:facet name="header">
														<t:outputText value="#{msg['customer.firstname']}" />
													</f:facet>
													<t:outputText
														value="#{prospectiveDataItem.firstName} #{prospectiveDataItem.middleName} #{prospectiveDataItem.lastName}" />
												</t:column>
												<t:column id="col_Passport">


													<f:facet name="header">

														<t:outputText id="lbl_Passport"
															value="#{msg['customer.passportNumber']}" />

													</f:facet>
													<t:outputText id="txt_passport" title=""
														value="#{prospectiveDataItem.passportNumber}" />
												</t:column>


												<t:column id="col_Visa">


													<f:facet name="header">

														<t:outputText id="lbl_visa"
															value="#{msg['customer.residenseVisaNumber']}" />

													</f:facet>
													<t:outputText id="txt_visa" title=""
														value="#{prospectiveDataItem.residenseVisaNumber}" />
												</t:column>

												<t:column id="col_DrivingLicense">


													<f:facet name="header">

														<t:outputText id="lbl_driving"
															value="#{msg['customer.drivingLicenseNumber']}" />

													</f:facet>
													<t:outputText id="txt_driving" title=""
														value="#{prospectiveDataItem.drivingLicenseNumber}" />
												</t:column>

												<t:column id="col_SSN">


													<f:facet name="header">

														<t:outputText id="lbl_ssn"
															value="#{msg['customer.socialSecNumber']}" />

													</f:facet>
													<t:outputText id="txt_ssn" title=""
														value="#{prospectiveDataItem.socialSecNumber}" />
												</t:column>

											</t:dataTable>
										</t:div>

									</h:form>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								&nbsp;
							</td>
						</tr>

					</table>



				</td>
			</tr>





		</table>


		</body>
	</html>
</f:view>

