<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


    
<f:view>
        <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
       <f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
 
    <html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>						
            <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			
 	</head>
 	        
    <script language="javascript" type="text/javascript">
		function showPopup()
		{
		   var screen_width = screen.width;
		   var screen_height = screen.height;
		   window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width-10)+',height='+(screen_height-350)+',left=0,top=40,scrollbars=yes,status=yes');
		    
		}
	 function populateTenant(TenantId,TenantNumber,TypeId,tenantNames)
           {
               
               document.getElementById("changeTenantForm:hdnNewTenantId").value=TenantId;
               document.getElementById("changeTenantForm:hdnNewTenantName").value=tenantNames;
               document.getElementById("changeTenantForm:txtNewTenantName").value=tenantNames;
           
           }
       function   showContractPopup()
		{
		//alert("In show PopUp");
		   var screen_width = screen.width;
		   var screen_height = screen.height;
		    
		   window.open('ContractListPopUp.jsf?','_blank','width='+(screen_width-10)+',height='+(screen_height-140)+',left=0,top=40,scrollbars=yes,status=yes');
		    
		}    
     
     function populateContract(contractNumber,contractId,tenantId){
     
        document.getElementById("ContractFollowUpForm:txtContractNumber").value=contractNumber;
	    document.getElementById("ContractFollowUpForm:txtContractNumberhdn").value=contractNumber;
	    document.getElementById("ContractFollowUpForm:txtContractId").value=contractId;
	    document.getElementById("ContractFollowUpForm:txtTenantIdHdn").value=tenantId;
	    document.getElementById("ContractFollowUpForm:renderInfohdn").value='true';
     
     }
    
    function clearWindow()
      	{
      	    document.getElementById("ContractFollowUpForm:txtContractNumber").value="";

			
			$('ContractFollowUpForm:startDate').component.resetSelectedDate();
			$('ContractFollowUpForm:endDate').component.resetSelectedDate();
			
			document.getElementById("ContractFollowUpForm:selectFollowUpType").selectedIndex= 0;
      	    document.getElementById("ContractFollowUpForm:selectFollowUpStatusType").selectedIndex= 0;

        }


    </script>
 	

				<!-- Header -->
	
		<body class="BODY_STYLE">
	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr><td class="HEADER_TD">
										<h:outputLabel value="#{msg['contractFollowUp.Heading']}" styleClass="HEADER_FONT"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
					        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
                                 <tr valign="top">
                                  <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
                                   </td>    
									<td width="100%" height="100%" valign="top" nowrap="nowrap">
							<div style="padding-bottom:7px;padding-left:10px;padding-right:0;padding-top:7px;">
    								 <h:form id="ContractFollowUpForm" style="width:97%"  enctype="multipart/form-data">
    								  <div style = "height:25px;">
											<table border="0" class="layoutTable" >
											
												<tr>
												
													<td >
													  <h:messages></h:messages>
											          <h:outputText   escape="false" value="#{pages$ContractFollowUp.errorMessages}" styleClass="ERROR_FONT" style="padding:10px;"/>
											          <h:outputText   escape="false" value="#{pages$ContractFollowUp.successMessages}" styleClass="INFO_FONT" style="padding:10px;"/>
											          <h:inputHidden  id="txtContractId"   value="#{pages$ContractFollowUp.contractId}"/>
											          <h:inputHidden  id="txtContractNumberhdn" value="#{pages$ContractFollowUp.contractNumber}"/>
											          <h:inputHidden  id="txtTenantIdHdn" value="#{pages$ContractFollowUp.tenantId}"/>
											          <h:inputHidden  id="renderInfohdn" value="#{pages$ContractFollowUp.renderInfo}"/>
													
													</td>
												</tr>
											</table>
										</div>	
										
									  <div class= "MARGIN">
				                      <table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
				                       </table>
				 <div class="DETAIL_SECTION">
             		<h:outputLabel value="#{msg['contractFollowUp.basicInformation']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>				
							<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" width="100%" >
											<tr>
											        <td>
											        &nbsp;
											      
											        </td>
											        <td colspan="6" class="BUTTON_TD">
											         <pims:security screen="Pims.LeaseContractManagement.ContractFollowUp.ContractFollowUp" action="create">                                                  
											               <h:commandButton id="btnTenantBtn" styleClass="BUTTON"  type="submit"  
														             action="#{pages$ContractFollowUp.btnTenant_Click}"
														             value="#{msg['contractFollowUp.tenant']}"
														             
														             binding="#{pages$ContractFollowUp.btnRenderedTenant}"
														   />
													</pims:security>
													<pims:security screen="Pims.LeaseContractManagement.ContractFollowUp.ContractFollowUp" action="create">	   
														   <h:commandButton id="btnContract"  styleClass="BUTTON"  type="submit"  
														             action="#{pages$ContractFollowUp.btnContract_Click}"
														             value="#{msg['contractFollowUp.contract']}"
														             binding="#{pages$ContractFollowUp.btnRenderedContract}"
														   />
													</pims:security>	   
											        </td>
										        										        
										        </tr>	 
      								   	
												
												<tr>
												  
												<td id="td1" >
														<h:outputLabel id="label1" value="#{msg['chequeList.contractNo']}:"></h:outputLabel>
													</td>
													<td id="td2" >
												        <h:inputText  id="txtContractNumber" disabled="true" style="width:50%" styleClass="A_RIGHT_NUM" value="#{pages$ContractFollowUp.contractNumber}">
												        
												        </h:inputText>
													</td>
													<td>
												<pims:security screen="Pims.LeaseContractManagement.ContractFollowUp.ContractFollowUp" action="create">	
													<h:commandButton styleClass="BUTTON" type="button" style="width:128px"
												                           value="#{msg['contractFollowUp.searchContract']}"
												                           onclick="showContractPopup();"     
												           />   
												</pims:security>           
													</td>
												    <td>
														<h:outputLabel styleClass="LABEL" value="Follow Up Type"></h:outputLabel>
													</td>
													<td >
														<h:selectOneMenu  id="selectFollowUpType" style="width: 136px" value="#{pages$ContractFollowUp.followUpTypeId}" >
															 <f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$ApplicationBean.followUpType}"/>
														</h:selectOneMenu>
													</td>   
													
												</tr>
									    <tr id="tr1">
												
												
										<td  id="td3">
										<h:outputLabel id="label2" value="#{msg['chequeList.dateFrom']}:"></h:outputLabel>
										</td>
										
										<td id="td4">
										<rich:calendar  id="startDate" value="#{pages$ContractFollowUp.startDate}" 
										popup="true"  showApplyButton="false" datePattern="#{pages$ContractFollowUp.dateFormat}"
										locale="#{pages$ContractFollowUp.locale}"
										enableManualInput="false" cellWidth="24px" 
										/>
										</td>
										<td>
										</td>
										
										<td id="td5">
										<h:outputLabel id="label3" value="#{msg['chequeList.dateTo']}:"></h:outputLabel>
										</td>
      									<td id="td6">
										<rich:calendar  id="endDate" value="#{pages$ContractFollowUp.endDate}" 
										popup="true"  showApplyButton="false" datePattern="#{pages$ContractFollowUp.dateFormat}"
										locale="#{pages$ContractFollowUp.locale}"
										enableManualInput="false" cellWidth="24px" 
										/>
										</td>
      									
      									</tr>
      									<tr>
      									            <td>
														<h:outputLabel styleClass="LABEL" value="Status"></h:outputLabel>
													</td>
													<td >
														<h:selectOneMenu  id="selectFollowUpStatusType" style="width: 136px" value="#{pages$ContractFollowUp.followUpStatusId}" >
															 <f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$ApplicationBean.followUpStatus}"/>
														</h:selectOneMenu>
													</td>   
      									</tr>
																									
											<tr id="td7">
                                                <td id="td8" colspan="6"  class="BUTTON_TD">
                              	                    <pims:security screen="Pims.LeaseContractManagement.ContractFollowUp.ContractFollowUp" action="create">  
                              	                       <h:commandButton id ="btnSub" styleClass="BUTTON" type= "submit"
                              	                        value="#{msg['contractFollowUp.submit']}"
                              	                        action="#{pages$ContractFollowUp.btnAdd_Click}" > </h:commandButton>
                              	                    </pims:security>    
												       <h:commandButton id="btnReset" styleClass="BUTTON" type="button" 
												       onclick="javascript:clearWindow();" 
												       value="#{msg['commons.reset']}" >  </h:commandButton>
                                		        </td>
                                           </tr>
									       
						
									       				
      									 </table>
      									 </div>
                                 		</div>		    
                                        
                                        
					<div class= "MARGIN" style="padding-bottom:7px;padding-right:0;padding-top:7px;">	
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
							</tr>
						</table>
						 <div class="TAB_PANEL_INNER">									   
  		                    <rich:tabPanel    style="width:100%;height:235px;" headerSpacing="0">

									<rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}">
										<%@ include file="notes/addNotes.jsp"%>
									</rich:tab>
									<rich:tab id="attachmentTab" label="#{msg['contract.attachmentTabHeader']}">
		                                     <%@ include file="../pages/attachment/addAttachment.jsp"%>
									</rich:tab>
							
							</rich:tabPanel>
                         </div>
                          <table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
							</tr>
						   </table>
						 </div>
                     
					</h:form>
				</div>					
								

									</td>
									</tr>
                                   								
									</table>
									
			
			</td>
    </tr>
    
		    <tr>
					<td colspan="2">
					    <table width="100%" cellpadding="0" cellspacing="0" border="0">
					       <tr><td class="footer"><h:outputLabel value="#{msg['commons.footer.message']}" /></td></tr>
					    </table>
					</td>
		    </tr>
    
    </table>
    
			
		</body>
	</html>
</f:view>

