<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

		function openStudySearchPopup()
		{
			var screen_width = 990;
			var screen_height = 475;
	        var popup_width = screen_width;
	        var popup_height = screen_height;
	        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
	        window.open('studySearch.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
		}
		
		function receiveSelectedStudies()
		{				
			document.getElementById('detailsFrm:lnkReceiveSelectedStuides').onclick();
		} 

		function receiveSelectedPortfolio()
		{ 
		  document.forms[0].submit();
		}
		function showPortfolioPopup()
		{
			var screen_width = 990;
			var screen_height = 475;
			var popup_width = screen_width;
			var popup_height = screen_height;
			var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
			var popup = window.open('portfolioSearch.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
			popup.focus();
		}
		
		function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		
		function populateStudy(studyId){
			document.getElementById("detailsFrm:studyId").value=studyId;
			document.getElementById("detailsFrm:loadStudy").value='true';
		    document.forms[0].submit();
		}
		
		
		function populateProperty(propertyId){
			document.getElementById("detailsFrm:propertyId").value=propertyId;
			document.getElementById("detailsFrm:loadProperty").value='true';
		    document.forms[0].submit();
		}
		
		function openStudySearchPopup()
        {
	         var screen_width = 990;
             var screen_height = 475;
             var popup_width = screen_width;
    		 var popup_height = screen_height;
             var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
             window.open('studySearch.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
       }
				
		function openStudyViewPopup() 
		{
			var screen_width = 980;
			var screen_height = 350;
	        var popup_width = screen_width;
	        var popup_height = screen_height + 30;
	        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
	        window.open('studyManage.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		}
	   function submitForm()
	   {	
		 window.document.forms[0].submit();
	   }	
		
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 <script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>


	
</head>
	<body class="BODY_STYLE">
	<div class="containerDiv">
	
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header --> 
	<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
		<tr style="height:84px;width:100%;#height:84px;#width:100%;">
			<td colspan="2">
				<jsp:include page="header.jsp" />
			</td>
		</tr>
		<tr width="100%">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99.2%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{pages$strategicPlanDetails.heading}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>
				<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%">
					<tr valign="top">
								<td width="100%" valign="top" nowrap="nowrap">
						<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
							<div class="SCROLLABLE_SECTION" style="height:470px;width:100%;#height:470px;#width:100%;">
									<h:form id="detailsFrm" enctype="multipart/form-data" style="WIDTH: 97.6%;">
									
								<div>
									<div styleClass="MESSAGE"> 
									<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
										<tr>
											<td>
												<h:outputText value="#{pages$strategicPlanDetails.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
												<h:outputText value="#{pages$strategicPlanDetails.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
											</td>
										</tr>
									</table>
									</div>									
					
					<t:div  style="width:97%; margin:10px;" rendered="#{pages$strategicPlanDetails.showApprovalStrategicPlanPanel}">
							<t:panelGrid id="approvalGrid" cellpadding="1px" width="100%" cellspacing="5px" columns="2">
											
											<t:panelGroup>
						                    	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
												<h:outputLabel  styleClass="LABEL" value="#{msg['strategicPlan.details.approvalGrid.actionDate']}: "/>
						                   		<rich:calendar id="actionDate" value="#{pages$strategicPlanDetails.strategicPlanView.strategicPlanApproveRejectDate}"						                   		
						                   		 locale="#{pages$strategicPlanDetails.locale}" popup="true" datePattern="#{pages$strategicPlanDetails.dateFormat}"
						                   		 disabled="true"
						                   		 showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px"/>
						                    </t:panelGroup>
						                    
											<t:panelGroup>
						                    	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
										        <h:outputLabel  styleClass="LABEL"  value="#{msg['strategicPlan.details.approvalGrid.actionRemarks']}: "></h:outputLabel>
										        <t:inputTextarea id="txtApprovalRemakrs"  rows="6" 
										        styleClass="INPUT_TEXT_AREA" 
										        value="#{pages$strategicPlanDetails.strategicPlanView.strategicPlanApproveRejectRemarks}"/>
											</t:panelGroup>
											
											<t:div style="height:10px;"/>
						   	</t:panelGrid>
					</t:div>
					<t:div  style="width:97%; margin:10px;" rendered="#{pages$strategicPlanDetails.showReviewPlanPanel}">
							<t:panelGrid id="reviewGrid" cellpadding="1px" width="100%" cellspacing="5px" columns="2">
											
											<t:panelGroup>
						                    	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
												<h:outputLabel  styleClass="LABEL" value="#{msg['strategicPlan.details.reviewGrid.reviewDate']}: "/>
						                   		<rich:calendar id="reviewDate" value="#{pages$strategicPlanDetails.strategicPlanView.strategicPlanReviewDate}"
						                   		disabled="true"
						                   		 locale="#{pages$strategicPlanDetails.locale}" popup="true" datePattern="#{pages$strategicPlanDetails.dateFormat}"
						                   		 showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px"/>
						                    </t:panelGroup>
						                    
											<t:panelGroup>
						                    	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
										        <h:outputLabel  styleClass="LABEL"  value="#{msg['strategicPlan.details.reviewGrid.reviewRemarks']}: "></h:outputLabel>
										        <t:inputTextarea id="txtreviewRemakrs"  rows="6" 
										        styleClass="INPUT_TEXT_AREA" 
										        value="#{pages$strategicPlanDetails.strategicPlanView.strategicPlanReviewRemarks}"/>
											</t:panelGroup>
											
											<t:div style="height:10px;"/>
						   	</t:panelGrid>
					</t:div>
					
					<t:div  style="width:97%; margin:10px;" rendered="#{pages$strategicPlanDetails.showCloseStrategicPlanPanel}">
							<t:panelGrid id="closePlanGrid" cellpadding="1px" width="100%" cellspacing="5px" columns="2">
											
											<t:panelGroup>
						                    	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
												<h:outputLabel  styleClass="LABEL" value="#{msg['strategicPlan.details.closePlanGrid.closingDate']}: "/>
						                   		<rich:calendar id="closingDate"
						                   		disabled="true" 
						                   		value="#{pages$strategicPlanDetails.strategicPlanView.strategicPlanClosingDate}"
						                   		 locale="#{pages$strategicPlanDetails.locale}" popup="true" datePattern="#{pages$strategicPlanDetails.dateFormat}"
						                   		 showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px"/>
						                    </t:panelGroup>
						                    
											<t:panelGroup>
						                    	<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
										        <h:outputLabel  styleClass="LABEL"  value="#{msg['strategicPlan.details.closePlanGrid.closingRemarks']}: "></h:outputLabel>
										        <t:inputTextarea id="txtclosingRemakrs"  rows="6" 
										        styleClass="INPUT_TEXT_AREA" 
										        value="#{pages$strategicPlanDetails.strategicPlanView.strategicPlanClosingRemarks}" />
											</t:panelGroup>
											
											<t:div style="height:10px;"/>
						   	</t:panelGrid>
					</t:div>
					
					<div class="AUC_DET_PAD">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
						<div class="TAB_PANEL_INNER"> 
							<rich:tabPanel binding="#{pages$strategicPlanDetails.tabPanel}" id="tabPanel" switchType="server" style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%" >
								<rich:tab id="strategicPlanDetailsTab" label="#{msg['strategicPlan.details.tabs.planDetails']}" >
																	<t:div style="width:100%">
																		<t:panelGrid id="strategicPlanDetailsTable"
																			cellpadding="1px" width="100%" cellspacing="5px"
																			columns="4">
																			
																			<h:panelGroup>																				
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['strategicPlan.details.tabs.planDetails.planNumber']}: "></h:outputLabel>
																			</h:panelGroup>
																			<h:inputText
																				styleClass="INPUT_TEXT READONLY" readonly="true" tabindex="1"
																				value="#{pages$strategicPlanDetails.strategicPlanView.strategicPlanNumber}"
																				id="txtPlanNumber"></h:inputText>
																			<h:panelGroup>
																				
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['strategicPlan.details.tabs.planDetails.planStatus']}: "></h:outputLabel>
																			</h:panelGroup>
																			<h:inputText
																				styleClass="INPUT_TEXT READONLY"  readonly="true" tabindex="2"
																				value="#{pages$strategicPlanDetails.strategicPlanView.statusEn}"
																				id="txtStatus">
																			</h:inputText>

																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory"
																					value="#{pages$strategicPlanDetails.asterisk}"></h:outputLabel>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['strategicPlan.details.tabs.planDetails.planNameEn']}: "></h:outputLabel>
																			</h:panelGroup>
																			<h:inputText styleClass="#{pages$strategicPlanDetails.readonlyStyleClass} INPUT_TEXT" tabindex="3"
																			binding="#{pages$strategicPlanDetails.htmlInputPlanNameEn}"
																			value="#{pages$strategicPlanDetails.strategicPlanView.strategicPlanNameEn}"
																				id="txtPlanNameEn"></h:inputText>

																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory"
																					value="#{pages$strategicPlanDetails.asterisk}"></h:outputLabel>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['strategicPlan.details.tabs.planDetails.planNameAr']}: "></h:outputLabel>
																			</h:panelGroup>
																			<h:inputText styleClass="#{pages$strategicPlanDetails.readonlyStyleClass} INPUT_TEXT" tabindex="4"
																			binding="#{pages$strategicPlanDetails.htmlInputPlanNameAr}"
																			value="#{pages$strategicPlanDetails.strategicPlanView.strategicPlanNameAr}"
																				id="txtPlanNameAr"></h:inputText>
																				
																			<h:panelGroup>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['strategicPlan.details.tabs.planDetails.planDescription']}: "></h:outputLabel>
																			</h:panelGroup>
																			<t:inputTextarea id="txtPlanDescription" tabindex="5"
																			binding="#{pages$strategicPlanDetails.htmlInputPlanDescription}"
																			value="#{pages$strategicPlanDetails.strategicPlanView.strategicPlanDescription}"
																				styleClass="#{pages$strategicPlanDetails.readonlyStyleClass} INPUT_TEXT_AREA"
																				/>
																				
																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory"
																					value="#{pages$strategicPlanDetails.asterisk}"></h:outputLabel>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['strategicPlan.details.tabs.planDetails.creationDate']}: "></h:outputLabel>
																			</h:panelGroup>
																			<rich:calendar id="creationtDate" datePattern="#{pages$strategicPlanDetails.dateFormat}"
																				styleClass="#{pages$strategicPlanView.readonlyStyleClass}"
																				binding="#{pages$strategicPlanDetails.htmlCalendarCreationDate}"
																			value="#{pages$strategicPlanDetails.strategicPlanView.strategicPlanCreationDate}"
																				
																				showApplyButton="false" enableManualInput="false"
																				cellWidth="22px" cellHeight="24px" />
																				
																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="#{pages$strategicPlanDetails.asterisk}"></h:outputLabel>
																				<h:outputLabel styleClass="LABEL" value="#{msg['commons.endDate']}: "></h:outputLabel>
																			</h:panelGroup>
																			<rich:calendar datePattern="#{pages$strategicPlanDetails.dateFormat}"
																						   styleClass="#{pages$strategicPlanView.readonlyStyleClass}"
																				           binding="#{pages$strategicPlanDetails.htmlCalendarEndDate}"
																			               value="#{pages$strategicPlanDetails.strategicPlanView.strategicPlanEndDate}"																				
																						   showApplyButton="false" enableManualInput="false"
																						   cellWidth="22px" cellHeight="24px" />
																			
																		</t:panelGrid>
																	</t:div>
																</rich:tab>
								<rich:tab id="portfoliosTab" label="#{msg['strategicPlan.details.tabs.portfolios']}">
									<t:div style="width:100%;">
		<t:panelGrid columns="1" cellpadding="0" cellspacing="0" style="width:100%">
			<t:div styleClass="BUTTON_TD">
		<t:commandButton id="btnAddPortfolio" styleClass="BUTTON"
		binding="#{pages$strategicPlanDetails.addPortfolioButton}" 
		value="#{msg['strategicPlan.details.tabs.portfolios.buttons.addPortfolio']}" 
		actionListener="#{pages$strategicPlanDetails.showPortfolioPopup}"  style="width: 100px;margin: 3px;" />
			</t:div>
			<t:div styleClass="contentDiv" style="width:98%;">
				<t:dataTable id="dtStrategicPlanPortfolios" renderedIfEmpty="true" width="100%" cellpadding="0" cellspacing="0" var="dataItem" binding="#{pages$strategicPlanDetails.dataTable}" value="#{pages$strategicPlanDetails.strategicPlanView.strategicPlanPortfolioViewList}"
				 	preserveDataModel="false" preserveSort="false" rowClasses="row1,row2" rules="all" rows="9">
					
					<t:column sortable="true"  id="portfolioNumberCol">
						<f:facet name="header">
							<t:outputText value="#{msg['strategicPlan.details.tabs.portfolios.grid.portfolioNumber']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.portfolio.portfolioNumber}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" id="portfolioNameArb">
						<f:facet name="header">
							<t:outputText value="#{msg['strategicPlan.details.tabs.portfolios.grid.portfolioNameAr']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.portfolio.portfolioNameAr}" styleClass="A_RIGHT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" id="portfolioNameEnCol">
						<f:facet name="header">
							<t:outputText value="#{msg['strategicPlan.details.tabs.portfolios.grid.portfolioNameEn']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.portfolio.portfolioNameEn}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>		
					<t:column sortable="true" id="marketSectorCol">
						<f:facet name="header">
							<t:outputText value="#{msg['strategicPlan.details.tabs.portfolios.grid.marketSector']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.portfolio.marketSector}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>		
					<t:column sortable="true" id="accountColEn" rendered="#{pages$strategicPlanDetails.isEnglishLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['strategicPlan.details.tabs.portfolios.grid.account']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.portfolio.accountTypeEn}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" id="accountColAr" rendered="#{pages$strategicPlanDetails.isArabicLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['strategicPlan.details.tabs.portfolios.grid.account']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.portfolio.accountTypeAr}" styleClass="A_RIGHT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" id="typeColEn" rendered="#{pages$strategicPlanDetails.isEnglishLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['strategicPlan.details.tabs.portfolios.grid.type']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.portfolio.portfolioTypeEn}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" id="typeColAr" rendered="#{pages$strategicPlanDetails.isArabicLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['strategicPlan.details.tabs.portfolios.grid.type']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.portfolio.portfolioTypeAr}" styleClass="A_RIGHT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" id="statusColEn" rendered="#{pages$strategicPlanDetails.isEnglishLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['strategicPlan.details.tabs.portfolios.grid.status']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.portfolio.statusEn}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" id="statusColAr" rendered="#{pages$strategicPlanDetails.isArabicLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['strategicPlan.details.tabs.portfolios.grid.status']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.portfolio.statusEn}" styleClass="A_RIGHT" style="white-space: normal;"/>
					</t:column>
					
					<t:column width="15" rendered="#{ pages$strategicPlanDetails.showSaveButton || pages$strategicPlanDetails.showSendForApprovalButton}" >
						<f:facet name="header">
							<t:outputText value="#{msg['commons.action']}"/>
						</f:facet>			
						<t:commandLink actionListener="#{pages$strategicPlanDetails.deletePortfolio}"
										   onclick="if (!confirm('#{msg['strategicPlan.details.tabs.portfolios.grid.actions.delete.confirm']}')) return">
								<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete_icon.png" />
							</t:commandLink>
					</t:column>
				</t:dataTable>
			</t:div>
			<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99.1%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']}"/>
						<h:outputText value=" : "/>
						<h:outputText value="#{pages$strategicPlanDetails.recordSize}" />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="dtStrategicPlanPortfoliosScroller" for="dtStrategicPlanPortfolios" paginator="true"  
						fastStep="1" paginatorMaxPages="5" immediate="false"
						paginatorTableClass="paginator"
						renderFacetsIfSinglePage="true" 												
						paginatorTableStyle="grid_paginator" layout="singleTable" 
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" 
						pageIndexVar="pageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
							<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
		</t:div>
		</t:panelGrid>
	</t:div>
								</rich:tab>
								
														<!-- Related Studies Tab - Start -->
														<rich:tab label="#{msg['study.manage.tab.relatedStudies.heading']}">														
															
															<h:panelGrid styleClass="BUTTON_TD" width="100%" rendered="#{pages$strategicPlanDetails.showSaveButton || pages$strategicPlanDetails.showSendForApprovalButton}">
																<h:commandButton styleClass="BUTTON"
																		value="#{msg['study.manage.tab.relatedStudies.btn.addStudies']}"
																		action="#{pages$strategicPlanDetails.onAddStudies}">
																</h:commandButton>
																<a4j:commandLink id="lnkReceiveSelectedStuides" action="#{pages$strategicPlanDetails.receiveSelectedStudies}" reRender="dataTable,gridInfo" />
															</h:panelGrid>
																													
															<t:div styleClass="contentDiv" style="width:99%">																
						                                       <t:dataTable id="dataTable"  
																	rows="#{pages$strategicPlanDetails.paginatorRows}"
																	value="#{pages$strategicPlanDetails.strategicPlanStudyViewList}"
																	binding="#{pages$strategicPlanDetails.strategicPlanStudyDataTable}"																	
																	preserveDataModel="false" preserveSort="false" var="dataItem"
																	rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
																																		
																	<t:column width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['study.number']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.studyDetailView.studyNumber}" rendered="#{dataItem.isDeleted == 0}" />																		
																	</t:column>
																	<t:column width="10%" sortable="true">
																		<f:facet  name="header">
																			<t:outputText value="#{msg['study.startDate']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.studyDetailView.startDate}" rendered="#{dataItem.isDeleted == 0}">
																			<f:convertDateTime timeZone="#{pages$strategicPlanDetails.timeZone}" pattern="#{pages$strategicPlanDetails.dateFormat}" />
																		</t:outputText>
																	</t:column>
																	<t:column width="15%" sortable="true">
																		<f:facet  name="header">
																			<t:outputText value="#{msg['study.endDate']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.studyDetailView.endDate}" rendered="#{dataItem.isDeleted == 0}">
																			<f:convertDateTime timeZone="#{pages$strategicPlanDetails.timeZone}" pattern="#{pages$strategicPlanDetails.dateFormat}" />
																		</t:outputText>
																	</t:column>
																	<t:column width="12%" sortable="true" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.typeCol']}" />
																		</f:facet>
																		<t:outputText value="#{pages$strategicPlanDetails.isEnglishLocale ? dataItem.studyDetailView.studyTypeEn : dataItem.studyDetailView.studyTypeAr}" rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>												
																	<t:column width="13%" sortable="true" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['TaskList.DataTable.Subject']}" />
																		</f:facet>
																		<t:outputText value="#{pages$strategicPlanDetails.isEnglishLocale ? dataItem.studyDetailView.studySubjectEn : dataItem.studyDetailView.studySubjectAr}" rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>												
																	<t:column width="10%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['bidder.title']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.studyDetailView.studyTitle}" rendered="#{dataItem.isDeleted == 0}" />																		
																	</t:column>												
																	<t:column width="10%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.status']}" />
																		</f:facet>
																		<t:outputText value="#{pages$strategicPlanDetails.isEnglishLocale ? dataItem.studyDetailView.statusEn : dataItem.studyDetailView.statusAr}" rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	
																	<t:column sortable="false" width="15%" rendered="#{pages$strategicPlanDetails.showSaveButton || pages$strategicPlanDetails.showSendForApprovalButton}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>																		
																		
																		<a4j:commandLink onclick="if (!confirm('#{msg['study.confirm.delete']}')) return" action="#{pages$strategicPlanDetails.onStudyDelete}" rendered="#{dataItem.isDeleted == 0}" reRender="dataTable,gridInfo">
																			<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
																		</a4j:commandLink>																															
																	</t:column>
																	
																  </t:dataTable>										
																</t:div>
														
				                                           		<t:div id="gridInfo" styleClass="contentDivFooter" style="width:100%">
																			<t:panelGrid id="rqtkRecNumTable"  columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
																				<t:div id="rqtkRecNumDiv"  styleClass="RECORD_NUM_BG">
																					<t:panelGrid id="rqtkRecNumTbl" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
																						<h:outputText value="#{msg['commons.recordsFound']}"/>
																						<h:outputText value=" : "/>
																						<h:outputText value="#{pages$strategicPlanDetails.strategicPlanStudyViewListRecordSize}"/>																						
																					</t:panelGrid>
																				</t:div>
																				  
																			      <CENTER>
																				   <t:dataScroller id="rqtkscroller" for="dataTable" paginator="true"
																					fastStep="1"  paginatorMaxPages="#{pages$strategicPlanDetails.paginatorMaxPages}" immediate="false"
																					paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true" 
																				    pageIndexVar="pageNumber"
																				    styleClass="SCH_SCROLLER"
																				    paginatorActiveColumnStyle="font-weight:bold;"
																				    paginatorRenderLinkForActive="false" 
																					paginatorTableStyle="grid_paginator" layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">
								
																					    <f:facet  name="first">
																							<t:graphicImage    url="../#{path.scroller_first}"  id="lblFRequestHistory"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastrewind">
																							<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRRequestHistory"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastforward">
																							<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFRequestHistory"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="last">
																							<t:graphicImage  url="../#{path.scroller_last}"  id="lblLRequestHistory"></t:graphicImage>
																						</f:facet>
									
								                                                      <t:div id="rqtkPageNumDiv"   styleClass="PAGE_NUM_BG">
																					   <t:panelGrid id="rqtkPageNumTable"  styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0" >
																					 					<h:outputText  styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																										<h:outputText   styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																						</t:panelGrid>
																							
																						</t:div>
																				 </t:dataScroller>
																				 </CENTER>
																		      
																	        </t:panelGrid>
								                          			 </t:div>														
															</rich:tab>
														<!-- Related Studies Tab - End -->
								
							    <rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}">
									<%@ include file="attachment/attachment.jsp"%>
								</rich:tab>
								<rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}">
									<%@ include file="notes/notes.jsp"%>
								</rich:tab>																			
							</rich:tabPanel>
													
							</div>
							<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT" /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID" style="width:100%;" /></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
						<t:div styleClass="BUTTON_TD" style="padding-top:10px;">
						
						
						<pims:security screen="Pims.InvestmentManagement.StrategicPlanDetails.SavePlan" action="create">
							<t:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" rendered="#{pages$strategicPlanDetails.showSaveButton}"
							binding="#{pages$strategicPlanDetails.saveButton}" 
						 		action="#{pages$strategicPlanDetails.saveStrategicPlan}" tabindex="13" />		
						 	</pims:security>
						 	<pims:security screen="Pims.InvestmentManagement.StrategicPlanDetails.SendForApproval" action="create">
						 	<t:commandButton styleClass="BUTTON"  value="#{msg['commons.sendForApproval']}" style="width: 140px;"
						 	binding="#{pages$strategicPlanDetails.sendForApprovalButton}"
						 	rendered="#{pages$strategicPlanDetails.showSendForApprovalButton}"
						 	onclick="if (!confirm('#{msg['commons.messages.sendForApprovalConfirm']}')) return false" 
						 		action="#{pages$strategicPlanDetails.sendStrategicPlanForApproval}" tabindex="13" />					 		
						 	</pims:security>
						 	<pims:security screen="Pims.InvestmentManagement.StrategicPlanDetails.ApproveRejectPlan" action="create">
						 	<t:commandButton styleClass="BUTTON" value="#{msg['commons.approveButton']}" rendered="#{pages$strategicPlanDetails.showApprovalStrategicPlanPanel}"
						 	binding="#{pages$strategicPlanDetails.approveButton}"
						 		action="#{pages$strategicPlanDetails.approveStrategicPlan}" tabindex="13" />
						 	</pims:security>
						 	<pims:security screen="Pims.InvestmentManagement.StrategicPlanDetails.ApproveRejectPlan" action="create">
						 	<t:commandButton styleClass="BUTTON" value="#{msg['commons.rejectButton']}" rendered="#{pages$strategicPlanDetails.showApprovalStrategicPlanPanel}"
						 	binding="#{pages$strategicPlanDetails.rejectButton}"
						 		action="#{pages$strategicPlanDetails.rejectStrategicPlan}" tabindex="13" />
						 	</pims:security>
						 	<pims:security screen="Pims.InvestmentManagement.StrategicPlanDetails.ActivatePlan" action="create">
						 	<t:commandButton styleClass="BUTTON" value="#{msg['commons.activate']}"  rendered="#{pages$strategicPlanDetails.showReviewPlanPanel}"
						 	binding="#{pages$strategicPlanDetails.activateButton}"
						 		action="#{pages$strategicPlanDetails.reviewStrategicPlan}" tabindex="13" />		
						 	</pims:security>
						 	<pims:security screen="Pims.InvestmentManagement.StrategicPlanDetails.ClosePlan" action="create">
						 	<t:commandButton styleClass="BUTTON" value="#{msg['strategicPlan.buttons.closeButton']}" style="width: 100px;"  
						 	binding="#{pages$strategicPlanDetails.closeButton}"
						 		rendered="#{pages$strategicPlanDetails.showCloseStrategicPlanPanel}"
						 		action="#{pages$strategicPlanDetails.closeStrategicPlan}" tabindex="13" />		
							</pims:security>
							<t:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}" 
								onclick="if (!confirm('#{msg['commons.messages.cancelConfirm']}')) return false" 
								rendered="#{!pages$strategicPlanDetails.planDetailsReadonlyMode}"  
								action="#{pages$strategicPlanDetails.cancel}" tabindex="12"  > </t:commandButton>
							
							<t:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}" 
								onclick="if (!confirm('#{msg['commons.messages.backConfirm']}')) return false" action="#{pages$strategicPlanDetails.cancel}" 
								tabindex="12" rendered="#{pages$strategicPlanDetails.planDetailsReadonlyMode}" > 
							</t:commandButton>
							
							
						</t:div>
						</div>
								</div>
							</h:form>
					  </div> 
						</td>			
					</tr>
				</table>
			</td>
		</tr>
		  
		<tr style="height:10px;width:100%;#height:10px;#width:100%;">
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
	</table>
	</div>
</body>
</html>
</f:view>