
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<SCRIPT>	
function closeWindow()
	{
	  //window.opener.document.getElementById('conductAuctionForm:isResultPopupCalled').value = "Y";
	  window.opener.document.forms[0].submit();
	  window.close();
	}
       </SCRIPT>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />

			<style>
span {
	PADDING-RIGHT: 5px;
	PADDING-LEFT: 5px;
}
</style>
			<script language="javascript" type="text/javascript">
	function clearWindow()
	{
        
            document.getElementById("formDuplicationUnits:txtFloorPrefix").value="";
			document.getElementById("formDuplicationUnits:txtStartNo").value="";	
			document.getElementById("formDuplicationUnits:txtEndNo").value="";
			window.close();
			
	}
	

	
</script>



		</head>
		<body dir="${sessionScope.CurrentLocale.dir}"
			lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<tr width="100%">

					<td width="100%" valign="top" class="divBackgroundBody"
						height="610px">
						<table width="100%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr height="1%">
								<td>
									<table width="100%" class="greyPanelTable" cellpadding="0"
										cellspacing="0" border="0">
										<tr>
											<td class="HEADER_TD">
												<h:outputLabel
													value="#{msg['receiveProperty.unit.duplicateUnits']}"
													styleClass="HEADER_FONT" />
											</td>
											<td width="100%">
												&nbsp;
											</td>
										</tr>

									</table>

									<table width="100%" class="greyPanelMiddleTable"
										cellpadding="0" cellspacing="0" border="0">
										<tr valign="top">
											<td height="100%" valign="top" nowrap="nowrap"
												background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
												width="1">
											</td>

											<td width="100%" valign="top" nowrap="nowrap">
												<h:form id="formDuplicationUnits">
													<div class="SCROLLABLE_SECTION">
														<h:inputHidden
															binding="#{pages$DuplicateUnits.propertyId}"></h:inputHidden>
														<div>
															<table border="0" class="layoutTable"
																style="margin-left: 10px; margin-right: 10px;">
																<tr>
																	<td>
																		<h:outputText value="#{pages$DuplicateUnits.messages}"
																			escape="false" styleClass="ERROR_FONT" />

																	</td>
																</tr>
															</table>
														</div>



														<div class="MARGIN">
															<table id="unitInfoTable" cellpadding="1px" width="100%"
																cellspacing="3px" columns="4">
																<tr>
																	<td>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['floor.floorNo']}:"></h:outputLabel>
																	</td>
																	<td>
																		<h:inputText id="txtfloorNumber" styleClass="READONLY"
																			readonly="true" maxlength="20"
																			value="#{pages$DuplicateUnits.unitViewFromParent.floorNumber}" />
																	</td>
																	<td>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['unit.unitNumber']}:" />
																	</td>
																	<td>
																		<h:inputText maxlength="20" styleClass="READONLY"
																			id="txtunitNumber" readonly="true"
																			value="#{pages$DuplicateUnits.unitViewFromParent.unitNumber}"></h:inputText>
																	</td>

																</tr>
																<tr>
																	<td>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['unit.costCenter']}:"></h:outputLabel>
																	</td>
																	<td>
																		<h:inputText id="txtCostCenter" styleClass="READONLY"
																			readonly="true" maxlength="20"
																			value="#{pages$DuplicateUnits.unitViewFromParent.accountNumber}" />
																	</td>
																	<td>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['unit.unitDescription']}:"></h:outputLabel>
																	</td>
																	<td>
																		<h:inputText id="txtUnitDesc" styleClass="READONLY"
																			readonly="true" maxlength="20"
																			value="#{pages$DuplicateUnits.unitViewFromParent.unitDesc}" />
																	</td>

																</tr>
																<tr>
																	<td>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['unit.usage']}:"></h:outputLabel>
																	</td>
																	<td>
																		<h:inputText styleClass="READONLY" id="txtunitusageEn"
																			readonly="true"
																			value="#{pages$DuplicateUnits.isEnglishLocale ?pages$DuplicateUnits.unitViewFromParent.usageTypeEn:pages$DuplicateUnits.unitViewFromParent.usageTypeAr}"></h:inputText>
																	</td>
																	<td>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unitType']}:"></h:outputLabel>
																	</td>
																	<td>
																		<h:inputText styleClass="READONLY" id="txtunitTypeEn"
																			readonly="true"
																			value="#{pages$DuplicateUnits.isEnglishLocale ?pages$DuplicateUnits.unitViewFromParent.unitTypeEn:pages$DuplicateUnits.unitViewFromParent.unitTypeAr}"></h:inputText>
																	</td>

																</tr>
																<tr>
																	<td>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unitSide']}:"></h:outputLabel>
																	</td>
																	<td>
																		<h:inputText styleClass="READONLY" id="txtunitSideEn"
																			readonly="true"
																			value="#{pages$DuplicateUnits.isEnglishLocale ?pages$DuplicateUnits.unitViewFromParent.unitSideTypeEn:pages$DuplicateUnits.unitViewFromParent.unitSideTypeAr}"></h:inputText>
																	</td>
																	<td>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unitInvestmentType']}:"></h:outputLabel>
																	</td>
																	<td>
																		<h:inputText styleClass="READONLY"
																			id="txtunitInvestmentType" readonly="true"
																			value="#{pages$DuplicateUnits.isEnglishLocale ?pages$DuplicateUnits.unitViewFromParent.investmentTypeEn:pages$DuplicateUnits.unitViewFromParent.investmentTypeAr}"></h:inputText>
																	</td>

																</tr>
																<tr>

																	<td>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['listReport.unitTemplate.unitAreaSqFt']}:" />
																	</td>
																	<td>
																		<h:inputText styleClass="READONLY A_RIGHT_NUM"
																			id="txtUnitAreaSqFt" readonly="true"
																			value="#{pages$DuplicateUnits.unitViewFromParent.unitArea}">
																			<f:convertNumber minFractionDigits="2"
																				maxFractionDigits="2" pattern="##,###,###.##" />
																		</h:inputText>
																	</td>
																	<td>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unitRentHeading']}:" />
																	</td>
																	<td>
																		<h:inputText styleClass="READONLY A_RIGHT_NUM"
																			id="txtofferedRent" readonly="true"
																			value="#{pages$DuplicateUnits.unitViewFromParent.rentValue}">
																			<f:convertNumber minFractionDigits="2"
																				maxFractionDigits="2" pattern="##,###,###.##" />
																		</h:inputText>
																	</td>
																</tr>




																<tr>

																	<td>
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unit.generationDirection']}:"></h:outputLabel>
																		</t:panelGroup>
																	</td>
																	<td>
																		<h:selectOneRadio layout="lineDirection"
																			binding="#{pages$DuplicateUnits.generationFloor}">
																			<f:selectItem
																				itemLabel="#{msg['receiveProperty.unit.sameFloor']}"
																				itemValue="1" />
																			<f:selectItem
																				itemLabel="#{msg['receiveProperty.unit.otherFloor']}"
																				itemValue="2" />
																		</h:selectOneRadio>
																	</td>
																	<td colspan="2">
																		<h:selectBooleanCheckbox
																			binding="#{pages$DuplicateUnits.includeFloorNumberVar}"
																			value="#{pages$DuplicateUnits.includeFloorNumber}">
																			<h:outputText
																				value="#{msg['receiveProperty.unit.addWithFloors']}" />
																		</h:selectBooleanCheckbox>
																	</td>
																</tr>
																<tr>

																	<td>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unit.floorList']}:"></h:outputLabel>

																	</td>
																	<td>
																		<h:selectOneMenu id="floorsList"
																			binding="#{pages$DuplicateUnits.floorMenu}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />

																			<f:selectItems
																				value="#{pages$DuplicateUnits.floorItems}" />
																		</h:selectOneMenu>
																	</td>
																	<td>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unit.floorList']}:"></h:outputLabel>

																	</td>
																	<td>
																		<h:selectOneMenu id="floorsListTo"
																			binding="#{pages$DuplicateUnits.floorMenuTo}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />

																			<f:selectItems
																				value="#{pages$DuplicateUnits.floorItems}" />
																		</h:selectOneMenu>
																	</td>


																</tr>
																<tr>
																	<td>
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unit.FromNumber']}:"></h:outputLabel>
																		</t:panelGroup>
																	</td>
																	<td>
																		<h:inputText id="txtStartNo"
																			binding="#{pages$DuplicateUnits.startNumber}">
																		</h:inputText>
																	</td>



																	<td>
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unit.ToNumber']}:"></h:outputLabel>
																		</t:panelGroup>
																	</td>
																	<td>
																		<h:inputText id="txtEndNo"
																			binding="#{pages$DuplicateUnits.endNumber}">
																		</h:inputText>
																	</td>



																</tr>
																<tr>
																	<td>

																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unit.specificUnitNumbers']}:"></h:outputLabel>

																	</td>
																	<td colspan="3">
																		<h:inputText id="txtCsvUnitNumbers"
																			binding="#{pages$DuplicateUnits.csvUnitNumbers}">
																		</h:inputText>
																	</td>

																</tr>

																<tr>

																	<td colspan="6" class="BUTTON_TD">
																		<h:commandButton type="submit" styleClass="BUTTON"
																			style="width: 100px;"
																			value="#{msg['receiveProperty.unit.generateUnits']}"
																			action="#{pages$DuplicateUnits.duplicateUnit}"></h:commandButton>


																		<h:commandButton styleClass="BUTTON" type="submit"
																			action="#{pages$DuplicateUnits.cancelButton}"
																			value="#{msg['commons.cancel']}" />

																	</td>
																</tr>

															</table>
														</div>
													</div>




												</h:form>

											</td>
										</tr>
									</table>
								</td>
							</tr>

						</table>
					</td>
				</tr>

			</table>
		</body>
	</html>
</f:view>

