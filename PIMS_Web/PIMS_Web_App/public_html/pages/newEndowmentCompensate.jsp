<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	function   openBuildFromPopup(endowmentId)
	{
	
	   var screen_width   = 0.75 * screen.width ;
	   var screen_height  = screen.height-220;
	   var screen_left    = screen.width /6.5;
	   var screen_top     = screen.height/6;
	   window.open('endowmentBuildFromPopup.jsf?pageMode=POPUP&BuildFrom=1&endowmentId='+endowmentId,'_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+	
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	}
	function onMessageFromSearchEndowments()
	{
		document.getElementById("detailsFrm:onMessageFromSearchEndowments").onclick();
	
	}
		
	function performClick(control)
	{
		disableInputs();
		control.nextSibling.onclick();
		return; 
	}
	
</script>

<t:div id="tabECDetails" style="width:100%;">
	<t:div styleClass="BUTTON_TD" style="height:24px;">
		<t:commandButton styleClass="BUTTON"
			value="#{msg['commons.view']}"
			onclick="javaScript:openBuildFromPopup('#{ pages$endowmentManage.endowment.endowmentId}');"
			
			style="padding-right: 1px;" />
		</br>
	</t:div>


	<t:div style="width:100%;">
		<h:commandLink id="onMessageFromSearchEndowments"
			action="#{pages$endowmentManage.onMessageFromSearchEndowments}" />



		<t:panelGrid id="endCompTab" cellpadding="5px" width="100%"
			cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
			columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
			<h:outputLabel id="lblEndowment" styleClass="LABEL"
				value="#{msg['endowment.lbl.name']}" />



			<h:panelGroup>
				<h:inputText id="endowmentName" styleClass="READONLY"
					style="width:170px;"
					value="#{pages$endowmentManage.compensateVO.newEndowment.endowmentName}" />
				<h:inputHidden
					value="#{pages$endowmentManage.compensateVO.newEndowment.endowmentId}" />
				<h:inputHidden
					value="#{pages$endowmentManage.compensateVO.newEndowment.costCenter}" />
				<h:graphicImage id="endowmentPopup" style="MARGIN: 0px 0px -4px;"
					rendered="#{
								 empty pages$endowmentManage.compensateVO || 
								 empty pages$endowmentManage.compensateVO.id 
								 
								
							  }"
					title="#{msg['endowment.search.title']}"
					url="../resources/images/magnifier.gif"
					onclick="javaScript:openSearchEndowmentsPopup('endowmentManage');" />
			</h:panelGroup>
			<h:outputLabel styleClass="LABEL" value="#{msg['endowment.lbl.num']}" />

			<h:inputText id="endowmentNumber" styleClass="READONLY"
				style="width:170px;"
				value="#{pages$endowmentManage.compensateVO.newEndowment.endowmentNum}" />

			<h:outputLabel styleClass="LABEL"
				value="#{msg['endowment.lbl.compensatedpercentage']}" />
			<h:inputText id="percentage"
				value="#{pages$endowmentManage.compensateVO.percentage}"
				maxlength="20" onkeyup="removeNonNumeric(this)"
				onkeypress="removeNonNumeric(this)"
				onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
				onchange="removeNonNumeric(this)"></h:inputText>

			<h:outputLabel styleClass="LABEL" value="#{msg['commons.amount']}" />
			<h:inputText id="compensationAmount"
				value="#{pages$endowmentManage.compensateVO.compensationAmount}"
				maxlength="20" onkeyup="removeNonNumeric(this)"
				onkeypress="removeNonNumeric(this)"
				onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
				onchange="removeNonNumeric(this)"></h:inputText>


			<h:outputLabel styleClass="LABEL"
				value="#{msg['endowment.lbl.totalPercentage']}" />
			<h:inputText id="newEndowmentTotalPercentage" styleClass="READONLY"
				value="#{pages$endowmentManage.compensateVO.totalNewEndowmentPercantage}"
				maxlength="20" onkeyup="removeNonNumeric(this)"
				onkeypress="removeNonNumeric(this)"
				onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
				onchange="removeNonNumeric(this)"></h:inputText>

			<h:outputLabel styleClass="LABEL"
				value="#{msg['endowment.lbl.newEndowmentpercentage']}" />
			<h:inputText id="newEndowmentPercentage"
				value="#{pages$endowmentManage.compensateVO.newEndowmentPercentage}"
				maxlength="20" onkeyup="removeNonNumeric(this)"
				onkeypress="removeNonNumeric(this)"
				onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
				onchange="removeNonNumeric(this)"></h:inputText>


		</t:panelGrid>




		<t:div styleClass="BUTTON_TD" style="height:24px;">
			<t:commandButton styleClass="BUTTON"
				value="#{msg['commons.buttonLabel.add']}"
				action="#{pages$endowmentManage.onSaveCompensate}"
				style="padding-right: 1px;" />
			</br>
		</t:div>








	</t:div>


</t:div>
<t:div id="dataTableCompensateIdss" style="width:100%;">
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">

		<t:dataTable id="dataTableCompensate" width="100%"
			rows="#{pages$docTypeList.paginatorRows}"
			value="#{pages$endowmentManage.compensateDataList}"
			binding="#{pages$endowmentManage.dataTableEndowmentCompensate}"
			preserveSort="false" var="dataItem" rowClasses="row1,row2"
			rules="all" renderedIfEmpty="true">
			<t:column sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['endowment.lbl.num']}" />
				</f:facet>
				<t:commandLink
					onclick="openEndowmentManagePopup('#{dataItem.newEndowment.endowmentId}')"
					value="#{dataItem.newEndowment.endowmentNum}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['endowment.lbl.name']}" />
				</f:facet>
				<t:outputText styleClass="A_LEFT"
					value="#{dataItem.newEndowment.endowmentName}" />
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['endowment.lbl.compensatedpercentage']}" />
				</f:facet>
				<t:outputText styleClass="A_LEFT" value="#{dataItem.percentage}">
					<f:convertNumber maxIntegerDigits="15" maxFractionDigits="3"
						pattern="##############0.000" />
				</t:outputText>
			</t:column>
			<t:column sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.amount']}" />
				</f:facet>
				<t:outputText styleClass="A_LEFT"
					value="#{dataItem.compensationAmount}">
					<f:convertNumber maxIntegerDigits="15" maxFractionDigits="2"
						pattern="##############0.00" />
				</t:outputText>
			</t:column>
			<t:column sortable="true">
				<f:facet name="header">
					<t:outputText
						value="#{msg['endowment.lbl.newEndowmentpercentage']}" />
				</f:facet>
				<t:outputText styleClass="A_LEFT"
					value="#{dataItem.newEndowmentPercentage}">
					<f:convertNumber maxIntegerDigits="15" maxFractionDigits="3"
						pattern="##############0.000" />
				</t:outputText>
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.report.costCenter']}" />
				</f:facet>
				<t:outputText styleClass="A_LEFT"
					value="#{dataItem.newEndowment.costCenter}" />
			</t:column>


			<t:column id="action" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.action']}" />
				</f:facet>

				<h:commandLink action="#{pages$endowmentManage.onEditCompensate}">
					<h:graphicImage style="cursor:hand;" title="#{msg['commons.edit']}"
						url="../resources/images/edit-icon.gif" width="18px;" />
				</h:commandLink>

				<h:commandLink
					onclick="if (!confirm('#{msg['mems.common.alertDelete']}')) return false;else true"
					action="#{pages$endowmentManage.onDeleteCompensate}">
					<h:graphicImage style="cursor:hand;"
						title="#{msg['endowment.lbl.deleteCompensate']}"
						url="../resources/images/delete.gif" width="18px;" />
				</h:commandLink>

			</t:column>
		</t:dataTable>
	</t:div>

	<t:div styleClass="contentDivFooter" style="width:99.2%;">
		<t:panelGrid columns="3" border="0" width="100%" cellpadding="1"
			cellspacing="1">
			<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
				<t:div styleClass="JUG_NUM_REC_ATT">
					<h:outputText value="#{msg['commons.records']}" />
					<h:outputText value=" : " />
					<h:outputText value="#{pages$endowmentManage.recordSize}" />
				</t:div>
			</t:panelGroup>

			<t:panelGroup>
				<t:div style="font-weight: bold;">
					<h:outputText value="#{msg['endowment.lbl.totalPercentage']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$endowmentManage.compensateVO.totalPercantage}" />
					<h:outputText value="%" />

					<h:outputText value="    " />

					<h:outputText value="#{msg['mems.normaldisb.label.totalAmount']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$endowmentManage.compensateVO.totalAmount}">
						<f:convertNumber maxIntegerDigits="15" maxFractionDigits="3"
							pattern="##############0.000" />
					</h:outputText>

				</t:div>

			</t:panelGroup>
			<t:panelGroup>
				<t:dataScroller for="dataTablePayments" paginator="true"
					fastStep="1" paginatorMaxPages="15" immediate="false"
					paginatorTableClass="paginator" renderFacetsIfSinglePage="true"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
					styleClass="SCH_SCROLLER" pageIndexVar="pageNumber"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false">
					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
					</f:facet>
					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
					</f:facet>
					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
					</f:facet>
					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
					</f:facet>


				</t:dataScroller>
			</t:panelGroup>
		</t:panelGrid>
	</t:div>


</t:div>