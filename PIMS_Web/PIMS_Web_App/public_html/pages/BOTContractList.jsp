<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden">

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />

			


		</head>
		<script language="javascript" type="text/javascript">

	
			function clear() {
		       
			    document.getElementById("formContractList:contractNum").value='';
			    document.getElementById("formContractList:contractStatus").value='-1';
			    document.getElementById("formContractList:contractorNum").value='';
			    document.getElementById("formContractList:contractorNAME").value='';
			    document.getElementById("formContractList:projectName").value='';
				
			}
			

	
        </script>
		<body class="BODY_STYLE">
		
		<div class="containerDiv">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['botContract.searchTitle']}"
										styleClass="HEADER_FONT" style="padding:10px" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
							
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" />
								<td width="0%" height="99%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION">
										<h:form id="formContractList">
											<div class="MARGIN" style="width: 95%">
											<table border="0" class="layoutTable" width="100%" style="margin-left:15px;margin-right:15px;" >
											<tr>
													<td >
													
														<h:outputText  id="errormsg"     escape="false" styleClass="ERROR_FONT" value="#{pages$BOTContractList.errorMessages}"/>
						
													</td>
												</tr>
											</table>
												<table cellpadding="0" cellspacing="0" width="99.2%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
													<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
									            </table>

												<div class="DETAIL_SECTION" style="width:99%;">
													<h:outputLabel value="#{msg['commons.search']}"
														styleClass="DETAIL_SECTION_LABEL" />
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">

														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.Contract.Number']} :" />
															</td>

															<td width="25%">
																<h:inputText id="contractNum" style="width:85%"
																	value="#{pages$BOTContractList.contractNumber}" />
															</td>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.contractStatus']} :" />
															</td>

															<td width="25%">
																<h:selectOneMenu id="contractStatus"
																	value="#{pages$BOTContractList.contractStatus}"
																	style="width:87%">
																	<f:selectItem id="selectAll" itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$BOTContractList.statusList}" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.contractStatus}" />
																</h:selectOneMenu>
															</td>
														</tr>

														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['botContractDetails.InvestorNumber']} :" />
															</td>

															<td width="25%">
																<h:inputText id="contractorNum" style="width:85%"
																	value="#{pages$BOTContractList.contractorNumber}" />
															</td>
																														<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['botContractDetails.InvestorName']} :" />
															</td>

															<td width="25%">
																<h:inputText id="contractorNAME" style="width:85%"
																	value="#{pages$BOTContractList.contractorName}" />
															</td>
                                                              
																													</tr>

														
														<tr>
														
														<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="Contract Type :" />
															</td>

															<td width="25%">
																<h:inputText id="contractType" style="width:85%"
																	value="#{pages$BOTContractList.contractType}"
																	 />
															</td>
														
														<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['project.projectName']} :" />
															</td>

															<td width="25%">
																<h:inputText id="projectName" style="width:85%"
																	value="#{pages$BOTContractList.projectName}" />
															</td>
														</tr>
														<tr>
															<td colspan="4">
																<div class="BUTTON_TD" style="padding-bottom: 4px;">
																	<h:commandButton  styleClass="BUTTON"
																		value="#{msg['commons.search']}"
																		action="#{pages$BOTContractList.searchContracts}" />
																	<h:commandButton type="reset" styleClass="BUTTON" value="#{msg['commons.clear']}" 
															                   onclick="clear();" />
															        <h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.Add']}" action="#{pages$BOTContractList.btnAddBOT}" />
																</div>
															</td>
														</tr>
													</table>
												</div>
											</div>

											<div style="padding: 5px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" align="center" style="width: 95%">

													<t:dataTable id="dt1" preserveDataModel="false"
													rowClasses="row1,row2"
														preserveSort="false" var="dataItem" rules="all"
														renderedIfEmpty="true" width="100%"
														rows="#{pages$BOTContractList.paginatorRows}"
														value="#{pages$BOTContractList.contractList}"
														binding="#{pages$BOTContractList.dataTable}">

														<t:column id="colContractNumber" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.contractNumber']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.contractNumber}" />
														</t:column>

														<t:column id="colStarttDate" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.startDate']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.startDate}">
																<f:convertDateTime
																	pattern="#{pages$BOTContractList.dateFormat}"
																	timeZone="#{pages$BOTContractList.timeZone}" />
															</t:outputText>
														</t:column>

														<t:column id="col3" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.endDate']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.endDate}">
																<f:convertDateTime
																	pattern="#{pages$BOTContractList.dateFormat}"
																	timeZone="#{pages$BOTContractList.timeZone}" />
															</t:outputText>
														</t:column>
														<t:column id="colStatusEn" sortable="true"
															rendered="#{pages$BOTContractList.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.contractStatus']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.statusEn}" />
														</t:column>
														<t:column id="colStatusAr" style="padding-right:5px"
															sortable="true"
															rendered="#{!pages$BOTContractList.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.contractStatus']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.statusAr}" />
														</t:column>
														
														<t:column id="colProjectName" style="padding-right:5px"
															sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['project.projectName']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.projectView.projectName}" />
														</t:column>
														
														<t:column id="colContractorNameeN" sortable="true" rendered="#{pages$BOTContractList.isEnglishLocale}"> 
															<f:facet name="header">
																<t:outputText
																	value="#{msg['blacklistSearch.contractorName']}" />
															</f:facet>
															
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.contractorNameEn}" />
														</t:column>
														<t:column id="colContractorNameAR" sortable="true" rendered="#{!pages$BOTContractList.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['blacklistSearch.contractorName']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.contractorNameAr}" />
														</t:column>
														<t:column id="col6" width="170px"
															style="whitespace:nowrap; text-align:right">
															<f:facet name="header">
																<t:outputText style="text-align:center"
																	value="#{msg['commons.action']}" />
															</f:facet>
															
															<t:commandLink action="#{pages$BOTContractList.imgEditContract_Click}" >
														       <h:graphicImage title="#{msg['commons.view']}" url="../resources/images/app_icons/Lease-contract.png"/>
														    </t:commandLink>
														    <t:commandLink action="#{pages$BOTContractList.imgManageContract_Click}">
													           <h:graphicImage id="manageIcon" title="#{msg['contract.search.toolTips.manage']}" url="../resources/images/app_icons/Request-detail.png" />&nbsp;
													        </t:commandLink>
															
														</t:column>
													</t:dataTable>
												</div>
												<t:div styleClass="contentDivFooter" style="width:96%">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$BOTContractList.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">
																<CENTER>
																	<t:dataScroller id="scroller" for="dt1"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$BOTContractList.paginatorMaxPages}"
																		immediate="false" paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																		styleClass="SCH_SCROLLER" pageIndexVar="pageNumber"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false">

																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblF" />
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR" />
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF" />
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL" />
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.pageNumber}" />
																					</td>
																				</tr>
																			</table>
																		</t:div>
																	</t:dataScroller>
																</CENTER>
															</td>
														</tr>
													</table>
												</t:div>
											</div>
										</h:form>
									</div>
								</td>
							</tr>

						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>

	</html>
</f:view>