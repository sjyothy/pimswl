
<t:div id="tabDDDetails" style="width:100%;">


	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="prcdc" rows="150" width="100%"
			value="#{pages$endowmentManage.listEndFileAsso}"
			binding="#{pages$endowmentManage.dataTableFileAsso}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">


			<t:column id="endNumber" sortable="true" defaultSorted="true">
				<f:facet name="header">
					<t:outputText id="endNum" value="#{msg['endowment.lbl.num']}" />
				</f:facet>
				<t:commandLink
					actionListener="#{pages$endowmentManage.openEndowmentPopUp}"
					value="#{dataItem.endowmentFile.fileNumber}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>


			<t:column id="endowCostCenter" sortable="true">
				<f:facet name="header">
					<t:outputText id="endCostCenter" value="#{msg['unit.costCenter']}" />
				</f:facet>
				<t:outputText id="costCentere" styleClass="A_LEFT"
					value="#{dataItem.endowment.costCenter}" />
			</t:column>

			<t:column id="colcategory" sortable="true">
				<f:facet name="header">
					<t:outputText id="endcategory"
						value="#{msg['endowment.lbl.category']}" />
				</f:facet>
				<t:outputText id="endcategorye" styleClass="A_LEFT"
					value="#{dataItem.endowmentCategory.categoryNameAr}" />
			</t:column>
			<t:column id="colpurpose" sortable="true">
				<f:facet name="header">
					<t:outputText id="endpurpose"
						value="#{msg['endowment.lbl.purpose']}" />
				</f:facet>
				<t:outputText id="endpurposee" styleClass="A_LEFT"
					value="#{dataItem.endowmentPurpose.purposeNameAr}" />
			</t:column>
			<t:column id="colManager" sortable="false">
				<f:facet name="header">
					<t:outputText id="endManager"
						value="#{msg['contract.person.Manager']}" />
				</f:facet>
				<t:outputText id="endManagere" styleClass="A_LEFT"
					rendered="#{dataItem.endowment.isAmafManager!= null && dataItem.endowment.isAmafManager=='1'}"
					value="#{msg['inheritanceFile.inheritedAssetTab.managerAmaf']}" />
				<t:commandLink
					rendered="#{dataItem.endowment.isAmafManager!= null && dataItem.endowment.isAmafManager=='0' &&
					            dataItem.endowment.manager.personId != null }"
					onclick="javascript:showPersonReadOnlyPopup(#{dataItem.endowment.manager.personId }); 
					         retrun "
					value="#{dataItem.endowment.manager.fullName}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<%-- 
			<t:column id="colownerShare" sortable="true">
				<f:facet name="header">
					<t:outputText id="ownerShare"
						value="#{msg['endowment.lbl.ownerShare']}" />
				</f:facet>
				<t:outputText id="ownerSharee" styleClass="A_LEFT"
					value="#{dataItem.ownerShare}" />
			</t:column>

			<t:column id="colShareToEndow" sortable="true">
				<f:facet name="header">
					<t:outputText id="shareToEndow"
						value="#{msg['endowment.lbl.shareToEndow']}" />
				</f:facet>
				<t:outputText id="shareToEndowe" styleClass="A_LEFT"
					value="#{dataItem.shareToEndowString}" />
			</t:column>
			--%>
			<t:column id="colEndFileStatus" sortable="true">
				<f:facet name="header">
					<t:outputText id="endFileStatus" value="#{msg['commons.status']}" />
				</f:facet>
				<t:outputText id="endFileStatuse" styleClass="A_LEFT"
					value="#{pages$endowmentManage.englishLocale?dataItem.status.dataDescEn:
																	 dataItem.status.dataDescAr
							}" />
			</t:column>

		</t:dataTable>
	</t:div>

</t:div>


