<%-- 
  - Author: Anil Verani
  - Date: 15/07/2009
  - Description: Used for Adding, Updating and Viewing Portfolio 
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">



	function openShowInvestmentUnitHistoryPopup(pageName) 	{	
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}



	function openOrderManagePopup() {
		var screen_width = 980;
		var screen_height = 380;
        var popup_width = screen_width;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('orderManage.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
	
	function receiveOrderView()	{	 
		document.getElementById('frm:lnkReceiveOrderView').onclick();
	}
	
	function openAssetClassSearchPopup(){
		var screen_width = 980;
		var screen_height = 470;
        var popup_width = screen_width;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('assetClassesSearch.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
	
	function receiveSelectedAssetClass()
	{
    	document.getElementById('frm:lnkReceiveAssetClasses').onclick();
	}
	
	function openAssetSearchPopup()	{
		var screen_width = 980;
		var screen_height = 470;
        var popup_width = screen_width;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('assetSearch.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
	
	function receiveSelectedAsset()
	{
    	document.getElementById('frm:lnkAssetReceive').onclick();
	}
	function evalucationTypeChange()
	{
    	document.getElementById('frm:lnkEvaluationTypeChange').onclick();
	}
	function calcEvalucationValue()
	{
    	document.getElementById('frm:calcEvaluationValue').onclick();
	}
	function calcEvalucationValue1()
	{
    	document.getElementById('frm:calcEvaluationValue1').onclick();
	}			
	function showUploadPopup()
	{
	      var screen_width = 1024;
	      var screen_height = 450;
	      var popup_width = screen_width-530;
	      var popup_height = screen_height-150;
	      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
	      
	      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	      popup.focus();
	}

	function showAddNotePopup()
	{
	      var screen_width = screen.width;
	      var screen_height = screen.height;
	      var popup_width = screen_width/3+120;
	      var popup_height = screen_height/3-80;
	      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
	      
	      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	      popup.focus();
	}
	
	function onExpectedRateChanged()
	{
		document.getElementById('frm:lnkChangePeriod').onclick();
		document.getElementById('frm:strExpectedRateId').disabled=true;
		document.getElementById('frm:strExpectedRateId').setAttribute("className","READONLY");
	}
	
	
	function onExpectedRateChangedCompleteRequest() {
		document.getElementById('frm:strExpectedRateId').disabled=false;
		document.getElementById('frm:strExpectedRateId').removeAttribute("className","READONLY");
	}
	
	function calcTotalInvestCost()
    {
		var unitPrice = parseFloat(document.getElementById("frm:txtUnitPrice").value);
		var totalUnit = parseFloat(document.getElementById("frm:txtTotalUnit").value);
		var additionalExpenses = parseFloat(document.getElementById("frm:txtAdditionalExpenses").value);
		var portfolioFees = parseFloat(document.getElementById("frm:txtPortfolioFees").value);
		
		var exchangeRate = document.getElementById("frm:txtExchangeRate").value;

		if(!isNaN(exchangeRate)) { // exchange rate is provided
			unitPrice = unitPrice * exchangeRate;
			additionalExpenses = additionalExpenses * exchangeRate;
			portfolioFees = portfolioFees * exchangeRate;
		}
		
		
		var totalUnitPrice = (totalUnit*unitPrice);
		var totalInvestmentCost = totalUnitPrice + additionalExpenses + portfolioFees;
		
		
		if (totalUnitPrice==0) {
			var principalAmount=parseFloat(document.getElementById("frm:txtPrincipalAmount").value);
			if (!isNaN(principalAmount)){
				totalInvestmentCost = totalInvestmentCost + principalAmount;
			}
		}
		 
		totalInvestmentCost = Math.round(totalInvestmentCost);

		document.getElementById("frm:txtTotalInvestmentCost").value="";
		document.getElementById("frm:hdnTotalInvestmentCost").value="";
		
		if(!isNaN( totalInvestmentCost ))
		{
		 document.getElementById("frm:txtTotalInvestmentCost").value=totalInvestmentCost ;
		 document.getElementById("frm:hdnTotalInvestmentCost").value=totalInvestmentCost ;
		}
    }
    
    
    /* 
    If the selected currency is AED then set 1 as the exchange rate
    */
    function onCurrencySelect(currencies){
    	var selectedCurrency = currencies.options[currencies.selectedIndex];
    	
    	var selectedCurrencyValue=selectedCurrency.value;
    	var selectedCurrencyText=selectedCurrency.text;
    	
    	var defaultCurrencyValue="3"; // AED Value
    	var defaultCurrencyText="AED";
    	var defaultExchangeRate = 1; // default exhange rate
    	
    	if (selectedCurrencyText==defaultCurrencyText || selectedCurrencyValue==defaultCurrencyValue) { // loose check
    		document.getElementById("frm:txtExchangeRate").value=defaultExchangeRate;
    	}
    
    }
    
    /* 
    Copy the portfolio name text field's value to read only fields above the tab
    */
    function onValueChange(control)
    {
    	if( control.id == 'frm:inputPortfolioNameEn')
    	{
    	  document.getElementById("frm:readOnlyPortfolioNameEn").value = control.value;
    	}
    	else if( control.id == 'frm:inputPortfolioNameAr')
    	{
    	  document.getElementById("frm:readOnlyPortfolioNameAr").value = control.value;
    	}
    
    }
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr>
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table class="greyPanelTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['portfolio.manage.heading']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<td width="100%" height="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION">

											<h:form id="frm" style="width:97%"
												enctype="multipart/form-data">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText escape="false" styleClass="ERROR_FONT"
																value="#{pages$portfolioManage.errorMessages}" />
															<h:outputText escape="false" styleClass="INFO_FONT"
																value="#{pages$portfolioManage.successMessages}" />
															<a4j:commandLink id="lnkReceiveOrderView"
																action="#{pages$portfolioManage.onOrderReceive}"
																reRender="buyingOrderDataTable,buyingOrderGridInfo,sellingOrderDataTable,sellingOrderGridInfo" />
														</td>
													</tr>
												</table>
												<t:panelGrid cellpadding="1px" width="100%"
													rendered="#{! empty pages$portfolioManage.portfolioView }"
													cellspacing="5px" columns="4">



													<h:outputLabel styleClass="LABEL"
														value="#{msg['portfolio.manage.tab.portfolio.name.en']}:"
														style="font-weight:bold;"></h:outputLabel>
													<h:inputText id="readOnlyPortfolioNameEn" readonly="true"
														styleClass="READONLY"
														value="#{pages$portfolioManage.portfolioView.readOnlyPortfolioNameEn}"
														style="font-weight:bold;" />


													<h:outputLabel styleClass="LABEL "
														value="#{msg['portfolio.manage.tab.portfolio.name.ar']}:"
														style="font-weight:bold;"></h:outputLabel>
													<h:inputText id="readOnlyPortfolioNameAr" readonly="true"
														styleClass="READONLY"
														value="#{pages$portfolioManage.portfolioView.readOnlyPortfolioNameAr}"
														style="font-weight:bold;" />



												</t:panelGrid>

												<t:div rendered="false" style="width:97%; ">


													<t:panelGrid cellpadding="1px" width="100%"
														cellspacing="5px" columns="4">

														<h:outputLabel styleClass="LABEL"
															value="#{msg['tenderManagement.approveTender.actionDate']}: " />
														<rich:calendar inputStyle="width: 186px; height: 18px"
															locale="#{pages$portfolioManage.locale}" popup="true"
															datePattern="#{pages$portfolioManage.dateFormat}"
															showApplyButton="false" enableManualInput="false"
															cellWidth="24px" cellHeight="22px" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['commons.comments']}: " />
														<t:inputTextarea styleClass="TEXTAREA" rows="6"
															style="width: 200px; height:30px;" />


													</t:panelGrid>
													<t:panelGrid cellpadding="1px" width="100%"
														cellspacing="5px" columns="4">
														<table border="0" width="100%">
															<tr>
																<td class="BUTTON_TD">
																	<h:commandButton value="#{msg['commons.approve']}"
																		action="#{pages$portfolioManage.onTaskApprove}"
																		styleClass="BUTTON"></h:commandButton>
																	<h:commandButton value="#{msg['commons.reject']}"
																		action="#{pages$portfolioManage.onTaskReject}"
																		styleClass="BUTTON"></h:commandButton>
																	<h:commandButton value="#{msg['commons.cancel']}"
																		action="#{pages$portfolioManage.onTaskCancel}"
																		styleClass="BUTTON"></h:commandButton>
																</td>
															</tr>
														</table>
													</t:panelGrid>
												</t:div>

												<t:div rendered="false" style="width:97%; ">
													<t:panelGrid cellpadding="1px" width="100%"
														cellspacing="5px" columns="4">
														<h:outputLabel styleClass="LABEL"
															value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.closingDate']}: " />
														<rich:calendar inputStyle="width: 186px; height: 18px"
															locale="#{pages$portfolioManage.locale}" popup="true"
															datePattern="#{pages$portfolioManage.dateFormat}"
															showApplyButton="false" enableManualInput="false"
															cellWidth="24px" cellHeight="22px" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['commons.comments']}: " />
														<t:inputTextarea styleClass="TEXTAREA" rows="6"
															style="width: 200px; height:30px;" />


													</t:panelGrid>
													<t:panelGrid cellpadding="1px" width="100%"
														cellspacing="5px" columns="4">
														<table border="0" width="100%">
															<tr>
																<td class="BUTTON_TD">
																	<h:commandButton value="#{msg['commons.ok']}"
																		action="#{pages$portfolioManage.onTaskClose}"
																		styleClass="BUTTON"></h:commandButton>
																	<h:commandButton value="#{msg['commons.cancel']}"
																		action="#{pages$portfolioManage.onTaskCancel}"
																		styleClass="BUTTON"></h:commandButton>
																</td>
															</tr>
														</table>
													</t:panelGrid>
												</t:div>

												<div class="MARGIN" style="width: 98%">

													<table id="table_1" cellpadding="0" cellspacing="0"
														width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>

													<rich:tabPanel>

														<!-- Portfolio Tab - Start -->
														<rich:tab
															label="#{msg['portfolio.manage.tab.portfolio.heading']}">
															<t:div style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid styleClass="TAB_DETAIL_SECTION_INNER"
																	cellpadding="1px" width="100%" cellspacing="5px"
																	columns="4">



																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['portfolio.manage.tab.portfolio.number']}:"></h:outputLabel>
																	<h:inputText
																		value="#{pages$portfolioManage.portfolioView.portfolioNumber}"
																		style="width: 186px;" maxlength="20"
																		styleClass="READONLY" readonly="true" />

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['portfolio.manage.tab.portfolio.status']}:"></h:outputLabel>
																	<h:selectOneMenu readonly="true" styleClass="READONLY"
																		value="#{pages$portfolioManage.portfolioView.statusId}"
																		style="width: 192px;">
																		<f:selectItem itemValue=""
																			itemLabel="#{msg['commons.select']}" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.portfolioStatusList}" />
																	</h:selectOneMenu>

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.type']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:selectOneMenu
																		value="#{pages$portfolioManage.portfolioView.portfolioTypeId}"
																		style="width: 192px;">
																		<f:selectItem itemValue="-1"
																			itemLabel="#{msg['commons.select']}" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.portfolioTypeList}" />
																	</h:selectOneMenu>
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.date']}:"></h:outputLabel>
																	</h:panelGroup>
																	<rich:calendar
																		value="#{pages$portfolioManage.portfolioView.creationDate}"
																		inputStyle="width: 186px; height: 18px"
																		locale="#{pages$portfolioManage.locale}" popup="true"
																		datePattern="#{pages$portfolioManage.dateFormat}"
																		showApplyButton="false" enableManualInput="false"
																		cellWidth="24px" cellHeight="22px"
																		style="width:186px; height:16px" />
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.name.en']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText id="inputPortfolioNameEn"
																		value="#{pages$portfolioManage.portfolioView.portfolioNameEn}"
																		style="width: 186px;" maxlength="50"
																		onblur="javaScript:onValueChange(this);"
																		onkeyup="javaScript:onValueChange(this);"
																		onmouseup="javaScript:onValueChange(this);"
																		onmousedown="javaScript:onValueChange(this);"
																		onchange="javaScript:onValueChange(this);"
																		onkeypress="javaScript:onValueChange(this);" />
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.name.ar']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText id="inputPortfolioNameAr"
																		value="#{pages$portfolioManage.portfolioView.portfolioNameAr}"
																		style="width: 186px;" maxlength="50"
																		onblur="javaScript:onValueChange(this);"
																		onkeyup="javaScript:onValueChange(this);"
																		onmouseup="javaScript:onValueChange(this);"
																		onmousedown="javaScript:onValueChange(this);"
																		onchange="javaScript:onValueChange(this);"
																		onkeypress="javaScript:onValueChange(this);" />


																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.desc.en']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputTextarea style="width: 186px;"
																		value="#{pages$portfolioManage.portfolioView.portfolioDescEn}" />
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.desc.ar']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputTextarea style="width: 186px;"
																		value="#{pages$portfolioManage.portfolioView.portfolioDescAr}" />

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.sector']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText
																		value="#{pages$portfolioManage.portfolioView.marketSector}"
																		style="width: 186px;" maxlength="100" />

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['assetSearch.currency']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:selectOneMenu id="txtCurrencyId"
																		value="#{pages$portfolioManage.portfolioView.currencyId}"
																		styleClass="SELECT_MENU"
																		onchange="onCurrencySelect(this),calcTotalInvestCost()">
																		<f:selectItem itemLabel="#{msg['commons.All']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.currencyList}" />
																	</h:selectOneMenu>

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['payment.exchangeRate']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText id="txtExchangeRate"
																		value="#{pages$portfolioManage.portfolioView.exchangeRate}"
																		style="width: 186px;" maxlength="10"
																		onchange="calcTotalInvestCost()" />

																	<h:panelGroup>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.original.amount']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText
																		value="#{pages$portfolioManage.portfolioView.originalAmount}"
																		style="width: 186px;" maxlength="10" />

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.principal.amount']}:"></h:outputLabel>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['commons.currencyInAED']}" />
																	</h:panelGroup>
																	<h:inputText id="txtPrincipalAmount"
																		value="#{pages$portfolioManage.portfolioView.principalAmount}"
																		onchange="calcTotalInvestCost()" style="width: 186px;"
																		maxlength="10" />


																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.unit.price']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText id="txtUnitPrice"
																		value="#{pages$portfolioManage.portfolioView.unitPrice}"
																		onchange="calcTotalInvestCost()" style="width: 186px;"
																		maxlength="10" />

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.total.unit']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:panelGroup>
																		<h:inputText id="txtTotalUnit"
																			value="#{pages$portfolioManage.portfolioView.totalUnits}"
																			onchange="calcTotalInvestCost()"
																			style="width: 186px;" maxlength="10" />
																		<h:commandLink rendered="true"
																			action="#{pages$portfolioManage.showInvestmentUnitHistory}">
																			<h:graphicImage
																				title="#{msg['portfolio.investmentUnitHistory']}"
																				url="../resources/images/detail-icon.gif" />
																		</h:commandLink>
																	</h:panelGroup>


																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.fees']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText id="txtPortfolioFees"
																		onchange="calcTotalInvestCost()"
																		value="#{pages$portfolioManage.portfolioView.portfolioFees}"
																		style="width: 186px;" maxlength="10" />


																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.additional.expenses']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText id="txtAdditionalExpenses"
																		value="#{pages$portfolioManage.portfolioView.additionalExpenses}"
																		onchange="calcTotalInvestCost()" style="width: 186px;"
																		maxlength="10" />


																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['portfolio.manage.tab.portfolio.total.investment.cost']}:"></h:outputLabel>
																	<h:panelGroup>
																		<h:inputText id="txtTotalInvestmentCost"
																			readonly="true"
																			value="#{pages$portfolioManage.portfolioView.totalInvestmentCost}"
																			styleClass="READONLY A_RIGHT_NUM"
																			style="width: 186px;" maxlength="15">
																			<f:convertNumber pattern="##,###,###" />
																		</h:inputText>
																		<h:inputHidden id="hdnTotalInvestmentCost"
																			value="#{pages$portfolioManage.portfolioView.totalInvestmentCost}"></h:inputHidden>
																	</h:panelGroup>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['portfolio.manage.tab.portfolio.total.expense']}:"></h:outputLabel>
																	<h:inputText readonly="true" styleClass="READONLY"
																		style="width: 186px;" />

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['portfolio.manage.tab.portfolio.total.return']}:"></h:outputLabel>
																	<h:inputText readonly="true" styleClass="READONLY"
																		style="width: 186px;" />

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['portfolio.manage.tab.portfolio.total.fees']}:"></h:outputLabel>
																	<h:inputText value="#{pages$portfolioManage.totalFees}"
																		readonly="true" styleClass="READONLY"
																		style="width: 186px;" />

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['portfolio.manage.tab.portfolio.realized.return']}:"></h:outputLabel>
																	<h:inputText
																		value="#{pages$portfolioManage.portfolioView.realizedReturn}"
																		readonly="true" styleClass="READONLY"
																		style="width: 186px;" />

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['portfolio.manage.tab.portfolio.unrealized.return']}:"></h:outputLabel>
																	<h:inputText
																		value="#{pages$portfolioManage.portfolioView.unrealizedReturn}"
																		readonly="true" styleClass="READONLY"
																		style="width: 186px;" />

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['commons.startDate']}:"></h:outputLabel>
																	</h:panelGroup>
																	<rich:calendar
																		value="#{pages$portfolioManage.portfolioView.fundsDurationStartDate}"
																		inputStyle="width: 186px; height: 18px"
																		locale="#{pages$portfolioManage.locale}" popup="true"
																		datePattern="#{pages$portfolioManage.dateFormat}"
																		showApplyButton="false" enableManualInput="false"
																		cellWidth="24px" cellHeight="22px"
																		style="width:186px; height:16px" />

																	<h:panelGroup>

																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['commons.endDate']}:"></h:outputLabel>
																	</h:panelGroup>
																	<rich:calendar
																		value="#{pages$portfolioManage.portfolioView.fundsDurationEndDate}"
																		inputStyle="width: 186px; height: 18px"
																		locale="#{pages$portfolioManage.locale}" popup="true"
																		datePattern="#{pages$portfolioManage.dateFormat}"
																		showApplyButton="false" enableManualInput="false"
																		cellWidth="24px" cellHeight="22px"
																		style="width:186px; height:16px" />

																	<h:outputLabel id="lblEndowmentnum"
																		value="#{msg['endowment.lbl.num']}:"></h:outputLabel>

																	<h:panelGroup id="grpEndowment"
																		rendered="#{
										 											  !empty pages$portfolioManage.portfolioView.endowmentId 
										 										   }">
																		<h:inputText styleClass="READONLY" readonly="true"
																			style="width:187px;"
																			value="#{pages$portfolioManage.portfolioView.endowmentRef}" />
																		<%-- 
																			<h:graphicImage
																				alt="#{msg['receiveProperty.tooltip.searchEndowments']}"
																				binding="#{pages$ReceiveProperty.imgSearchEndowment}"
																				url="../resources/images/app_icons/View_Icon.png"
																				style="MARGIN: 1px 1px -5px;cursor:hand;"
																				onclick="showAssetSearchPopup();"></h:graphicImage>
																			--%>
																		<h:graphicImage
																			alt="#{msg['receiveProperty.tooltip.viewEndowments']}"
																			url="../resources/images/app_icons/view_tender.png"
																			style="MARGIN: 1px 1px -5px;cursor:hand;"
																			onclick="showAssetDetailsPopup();"></h:graphicImage>
																	</h:panelGroup>

																	<h:panelGroup style="display: none;">
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.account']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:panelGroup style="display: none;">
																		<h:selectOneMenu
																			value="#{pages$portfolioManage.portfolioView.accountTypeId}"
																			style="width: 192px;">
																			<%-- <f:selectItem itemValue="-1" itemLabel="#{msg['commons.select']}" /> --%>
																			<f:selectItems
																				value="#{pages$ApplicationBean.accountTypeList}" />
																		</h:selectOneMenu>
																	</h:panelGroup>

																	<h:panelGroup style="display: none;">
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['portfolio.manage.tab.portfolio.rate.return']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:panelGroup style="display: none;">
																		<h:inputText id="budgetRateOfReturn"
																			value="#{pages$portfolioManage.portfolioView.budgetRateOfReturn}"
																			style="width: 186px;" maxlength="10" />
																	</h:panelGroup>
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Portfolio Tab - End -->


														<!-- Account Type Tab - Start -->
														<rich:tab
															label="#{msg['portfolio.manage.tab.portfolio.account']}">

															<h:panelGrid
																rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}"
																styleClass="BUTTON_TD" width="100%" columns="4">
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['portfolio.manage.tab.portfolio.account']}:" />
																</h:panelGroup>
																<h:selectOneMenu
																	value="#{pages$portfolioManage.portfolioAccountTypeView.strSelectedAccountTypeId}"
																	style="width: 192px;">
																	<f:selectItem itemValue=""
																		itemLabel="#{msg['commons.select']}" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.accountTypeList}" />
																</h:selectOneMenu>

																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['paymentSchedule.percentage']}:" />
																</h:panelGroup>
																<h:inputText
																	value="#{pages$portfolioManage.portfolioAccountTypeView.strSelectedAccountPercentage}"
																	style="width: 186px;" maxlength="5" />

																<h:outputText value=""></h:outputText>
																<h:outputText value=""></h:outputText>
																<h:outputText value=""></h:outputText>
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.Add']}"
																	action="#{pages$portfolioManage.onAccountTypeAdd}"
																	style="width: 120px;" />
															</h:panelGrid>

															<t:div styleClass="contentDiv" style="width:99%">
																<t:dataTable id="accountTypeDataTable"
																	rows="#{pages$portfolioManage.paginatorRows}"
																	value="#{pages$portfolioManage.portfolioAccountTypeViewList}"
																	binding="#{pages$portfolioManage.accountTypeDataTable}"
																	preserveDataModel="false" preserveSort="false"
																	var="dataItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true" width="100%">

																	<t:column width="45%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['portfolio.manage.tab.portfolio.account']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$portfolioManage.isEnglishLocale ? dataItem.accountTypeView.accountTypeEn : dataItem.accountTypeView.accountTypeAr}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="45%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.percentage']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.accountPercentage}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="10%" sortable="false"
																		rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<a4j:commandLink
																			onclick="if (!confirm('#{msg['accountType.confirm.delete']}')) return"
																			action="#{pages$portfolioManage.onAccountTypeDelete}"
																			rendered="#{dataItem.isDeleted == 0}"
																			reRender="accountTypeDataTable,accountTypeGridInfo">
																			<h:graphicImage title="#{msg['commons.delete']}"
																				url="../resources/images/delete.gif" />
																		</a4j:commandLink>
																	</t:column>
																</t:dataTable>
															</t:div>

															<t:div id="accountTypeGridInfo"
																styleClass="contentDivFooter" style="width:100%">
																<t:panelGrid id="rqtkRecNumTable4" columns="2"
																	cellpadding="0" cellspacing="0" width="100%"
																	columnClasses="RECORD_NUM_TD,BUTTON_TD">
																	<t:div id="rqtkRecNumDiv4" styleClass="RECORD_NUM_BG">
																		<t:panelGrid id="rqtkRecNumTbl4" columns="3"
																			columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
																			cellpadding="0" cellspacing="0"
																			style="width:182px;#width:150px;">
																			<h:outputText value="#{msg['commons.recordsFound']}" />
																			<h:outputText value=" : " />
																			<h:outputText
																				value="#{pages$portfolioManage.accountTypeRecordSize}" />
																		</t:panelGrid>
																	</t:div>

																	<CENTER>
																		<t:dataScroller id="rqtkscroller4"
																			for="accountTypeDataTable" paginator="true"
																			fastStep="1"
																			paginatorMaxPages="#{pages$portfolioManage.paginatorMaxPages}"
																			immediate="false" paginatorTableClass="paginator"
																			renderFacetsIfSinglePage="true"
																			pageIndexVar="pageNumber" styleClass="SCH_SCROLLER"
																			paginatorActiveColumnStyle="font-weight:bold;"
																			paginatorRenderLinkForActive="false"
																			paginatorTableStyle="grid_paginator"
																			layout="singleTable"
																			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

																			<f:facet name="first">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblFRequestHistory4"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastrewind">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFRRequestHistory4"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastforward">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}"
																					id="lblFFRequestHistory4"></t:graphicImage>
																			</f:facet>

																			<f:facet name="last">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblLRequestHistory4"></t:graphicImage>
																			</f:facet>

																			<t:div id="rqtkPageNumDiv4" styleClass="PAGE_NUM_BG">
																				<t:panelGrid id="rqtkPageNumTable4"
																					styleClass="PAGE_NUM_BG_TABLE" columns="2"
																					cellpadding="0" cellspacing="0">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</t:panelGrid>
																			</t:div>
																		</t:dataScroller>
																	</CENTER>
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Account Type Tab - End -->

														<!-- Expected Rate Tab - Start -->
														<rich:tab
															label="#{msg['portfolio.manage.tab.portfolio.expected.rate']}">
															<jsp:include page="portfolioExpectedRate.jsp"></jsp:include>
														</rich:tab>
														<!-- Expected Rate Tab - End -->

														<!-- Profit Tab - Start -->
														<rich:tab
															label="#{msg['portfolio.manage.tab.portfolio.portfolio.profit']}">
															<jsp:include page="portfolioProfitDetail.jsp"></jsp:include>
														</rich:tab>
														<!-- Profit Tab  - End -->

														<!-- Evaluation Tab - Start -->
														<rich:tab
															label="#{msg['portfolio.manage.tab.portfolio.portfolio.evaluation']}">
															<jsp:include page="portfolioEvaluationDetail.jsp"></jsp:include>
														</rich:tab>
														<!-- Evaluation Tab - End -->

														<!-- Asset Classes Tab - Start -->
														<rich:tab
															label="#{msg['portfolio.manage.tab.assetClasses.heading']}">

															<h:panelGrid
																rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}"
																styleClass="BUTTON_TD" width="100%">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['portfolio.manage.tab.assetClasses.btn.add']}"
																	action="#{pages$portfolioManage.onAssetClassesAdd}"
																	style="width: 120px;">
																</h:commandButton>
																<a4j:commandLink id="lnkReceiveAssetClasses"
																	action="#{pages$portfolioManage.onAssetClassesReceive}"
																	reRender="assetClassesDataTable,assetClassesGridInfo" />
															</h:panelGrid>

															<t:div styleClass="contentDiv" style="width:99%">
																<t:dataTable id="assetClassesDataTable"
																	rows="#{pages$portfolioManage.paginatorRows}"
																	value="#{pages$portfolioManage.portfolioAssetClassViewList}"
																	binding="#{pages$portfolioManage.assetClassesDataTable}"
																	preserveDataModel="false" preserveSort="false"
																	var="dataItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true" width="100%">

																	<t:column width="20%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['assetClassesSearch.assetClassNumber']}" />
																		</f:facet>
																		<t:outputText
																			value="#{dataItem.assetClassView.assetClassNumber}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	<t:column width="20%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['assetClass.name']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$portfolioManage.isEnglishLocale ? dataItem.assetClassView.assetClassNameEn : dataItem.assetClassView.assetClassNameAr}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	<t:column width="20%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['assetClassesSearch.parentClassNumber']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$portfolioManage.isEnglishLocale ? dataItem.assetClassView.parentAssetClassNameEn : dataItem.assetClassView.parentAssetClassNameAr}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	<t:column width="30%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['assetClassesSearch.assetClassDescription']}" />
																		</f:facet>
																		<t:outputText
																			value="#{dataItem.assetClassView.description}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	<t:column width="10%" sortable="false"
																		rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<a4j:commandLink
																			onclick="if (!confirm('#{msg['assetClass.confirm.delete']}')) return"
																			action="#{pages$portfolioManage.onAssetClassesDelete}"
																			rendered="#{dataItem.isDeleted == 0}"
																			reRender="assetClassesDataTable,assetClassesGridInfo">
																			<h:graphicImage title="#{msg['commons.delete']}"
																				url="../resources/images/delete.gif" />
																		</a4j:commandLink>
																	</t:column>

																</t:dataTable>
															</t:div>

															<t:div id="assetClassesGridInfo"
																styleClass="contentDivFooter" style="width:100%">
																<t:panelGrid id="rqtkRecNumTable0" columns="2"
																	cellpadding="0" cellspacing="0" width="100%"
																	columnClasses="RECORD_NUM_TD,BUTTON_TD">
																	<t:div id="rqtkRecNumDiv0" styleClass="RECORD_NUM_BG">
																		<t:panelGrid id="rqtkRecNumTbl0" columns="3"
																			columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
																			cellpadding="0" cellspacing="0"
																			style="width:182px;#width:150px;">
																			<h:outputText value="#{msg['commons.recordsFound']}" />
																			<h:outputText value=" : " />
																			<h:outputText
																				value="#{pages$portfolioManage.assetClassesRecordSize}" />
																		</t:panelGrid>
																	</t:div>

																	<CENTER>
																		<t:dataScroller id="rqtkscroller0"
																			for="assetClassesDataTable" paginator="true"
																			fastStep="1"
																			paginatorMaxPages="#{pages$portfolioManage.paginatorMaxPages}"
																			immediate="false" paginatorTableClass="paginator"
																			renderFacetsIfSinglePage="true"
																			pageIndexVar="pageNumber" styleClass="SCH_SCROLLER"
																			paginatorActiveColumnStyle="font-weight:bold;"
																			paginatorRenderLinkForActive="false"
																			paginatorTableStyle="grid_paginator"
																			layout="singleTable"
																			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

																			<f:facet name="first">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblFRequestHistory0"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastrewind">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFRRequestHistory0"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastforward">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}"
																					id="lblFFRequestHistory0"></t:graphicImage>
																			</f:facet>

																			<f:facet name="last">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblLRequestHistory0"></t:graphicImage>
																			</f:facet>

																			<t:div id="rqtkPageNumDiv0" styleClass="PAGE_NUM_BG">
																				<t:panelGrid id="rqtkPageNumTable0"
																					styleClass="PAGE_NUM_BG_TABLE" columns="2"
																					cellpadding="0" cellspacing="0">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</t:panelGrid>
																			</t:div>
																		</t:dataScroller>
																	</CENTER>
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Asset Classes Tab - End -->


														<!-- Assets Tab - Start -->
														<rich:tab
															label="#{msg['portfolio.manage.tab.assets.heading']}">

															<h:panelGrid
																rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}"
																styleClass="BUTTON_TD" width="100%">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['portfolio.manage.tab.assets.btn.add']}"
																	action="#{pages$portfolioManage.onAssetAdd}"
																	style="width: 120px;">
																</h:commandButton>
																<a4j:commandLink id="lnkAssetReceive"
																	action="#{pages$portfolioManage.onAssetReceive}"
																	reRender="assetsDataTable,assetsGridInfo" />
															</h:panelGrid>

															<t:div styleClass="contentDiv" style="width:99%">
																<t:dataTable id="assetsDataTable"
																	rows="#{pages$portfolioManage.paginatorRows}"
																	value="#{pages$portfolioManage.portfolioAssetViewList}"
																	binding="#{pages$portfolioManage.assetDataTable}"
																	preserveDataModel="false" preserveSort="false"
																	var="dataItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true" width="100%">

																	<t:column width="20%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['assetManage.assetNumber']}" />
																		</f:facet>
																		<t:outputText
																			value="#{dataItem.assetView.assetNumber}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	<t:column width="20%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['assetSearch.assetName']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$portfolioManage.isEnglishLocale ? dataItem.assetView.assetNameEn : dataItem.assetView.assetNameAr}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	<t:column width="20%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['assetSearch.assetStatus']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$portfolioManage.isEnglishLocale ? dataItem.assetView.assetStatusEn : dataItem.assetView.assetStatusAr}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	<t:column width="30%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['assetSearch.description']}" />
																		</f:facet>
																		<t:outputText
																			value="#{dataItem.assetView.description}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	<t:column width="10%" sortable="false"
																		rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<a4j:commandLink
																			onclick="if (!confirm('#{msg['asset.confirm.delete']}')) return"
																			action="#{pages$portfolioManage.onAssetDelete}"
																			rendered="#{dataItem.isDeleted == 0}"
																			reRender="assetsDataTable,assetsGridInfo">
																			<h:graphicImage title="#{msg['commons.delete']}"
																				url="../resources/images/delete.gif" />
																		</a4j:commandLink>
																	</t:column>

																</t:dataTable>
															</t:div>

															<t:div id="assetsGridInfo" styleClass="contentDivFooter"
																style="width:100%">
																<t:panelGrid id="rqtkRecNumTable3" columns="2"
																	cellpadding="0" cellspacing="0" width="100%"
																	columnClasses="RECORD_NUM_TD,BUTTON_TD">
																	<t:div id="rqtkRecNumDiv3" styleClass="RECORD_NUM_BG">
																		<t:panelGrid id="rqtkRecNumTbl3" columns="3"
																			columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
																			cellpadding="0" cellspacing="0"
																			style="width:182px;#width:150px;">
																			<h:outputText value="#{msg['commons.recordsFound']}" />
																			<h:outputText value=" : " />
																			<h:outputText
																				value="#{pages$portfolioManage.assetRecordSize}" />
																		</t:panelGrid>
																	</t:div>

																	<CENTER>
																		<t:dataScroller id="rqtkscroller3"
																			for="assetsDataTable" paginator="true" fastStep="1"
																			paginatorMaxPages="#{pages$portfolioManage.paginatorMaxPages}"
																			immediate="false" paginatorTableClass="paginator"
																			renderFacetsIfSinglePage="true"
																			pageIndexVar="pageNumber" styleClass="SCH_SCROLLER"
																			paginatorActiveColumnStyle="font-weight:bold;"
																			paginatorRenderLinkForActive="false"
																			paginatorTableStyle="grid_paginator"
																			layout="singleTable"
																			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;">

																			<f:facet name="first">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblFRequestHistory3"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastrewind">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFRRequestHistory3"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastforward">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}"
																					id="lblFFRequestHistory3"></t:graphicImage>
																			</f:facet>

																			<f:facet name="last">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblLRequestHistory3"></t:graphicImage>
																			</f:facet>

																			<t:div id="rqtkPageNumDiv3" styleClass="PAGE_NUM_BG">
																				<t:panelGrid id="rqtkPageNumTable3"
																					styleClass="PAGE_NUM_BG_TABLE" columns="2"
																					cellpadding="0" cellspacing="0">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</t:panelGrid>
																			</t:div>
																		</t:dataScroller>
																	</CENTER>
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Assets Tab - End -->


														<!-- Buying Orders Tab - Start -->
														<rich:tab
															label="#{msg['portfolio.manage.tab.order.buying.heading']}"
															rendered="#{pages$portfolioManage.isPortfolioApproved}">
															<h:panelGrid styleClass="BUTTON_TD" width="100%">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['order.add.btn.buying']}"
																	action="#{pages$portfolioManage.onOrderAddBuying}"
																	style="width: 120px;"
																	rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}">
																</h:commandButton>
															</h:panelGrid>

															<t:div styleClass="contentDiv" style="width:99%">
																<t:dataTable id="buyingOrderDataTable"
																	rows="#{pages$portfolioManage.paginatorRows}"
																	value="#{pages$portfolioManage.buyingOrderViewList}"
																	binding="#{pages$portfolioManage.buyingOrderDataTable}"
																	preserveDataModel="false" preserveSort="false"
																	var="dataItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true" width="100%">

																	<t:column width="18%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['order.transaction.no']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.transactionRefNo}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="5%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.date']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.transactionDate}"
																			rendered="#{dataItem.isDeleted == 0}">
																			<f:convertDateTime
																				timeZone="#{pages$portfolioManage.timeZone}"
																				pattern="#{pages$portfolioManage.dateFormat}" />
																		</t:outputText>
																	</t:column>

																	<t:column width="18%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['order.current.value.nav']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.currentValueNav}"
																			rendered="#{dataItem.isDeleted == 0}">
																			<f:convertNumber minFractionDigits="2"
																				maxFractionDigits="2" pattern="##,###,##0.00" />
																		</t:outputText>
																	</t:column>

																	<t:column width="24%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['order.total.amount.investment']}" />
																		</f:facet>
																		<t:outputText
																			value="#{dataItem.totalAmountInvestReturn}"
																			rendered="#{dataItem.isDeleted == 0}">
																			<f:convertNumber minFractionDigits="2"
																				maxFractionDigits="2" pattern="##,###,##0.00" />
																		</t:outputText>
																	</t:column>

																	<t:column width="16%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['order.total.units.bought']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.totalUnitsBoughtSold}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="8%" sortable="true" rendered="false">
																		<f:facet name="header">
																			<t:outputText value="#{msg['order.total.units']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.totalUnits}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="9%" sortable="true" rendered="false">
																		<f:facet name="header">
																			<t:outputText value="#{msg['order.total.worth']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.totalAmount}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="9%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.status']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$portfolioManage.isEnglishLocale ? dataItem.orderStatusEn : dataItem.orderStatusAr}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column
																		rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}"
																		sortable="false" width="10%">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<t:commandLink
																			action="#{pages$portfolioManage.onOrderView}"
																			rendered="#{dataItem.isDeleted == 0}">
																			<f:param name="orderType" value="buying" />
																			<h:graphicImage title="#{msg['commons.view']}"
																				url="../resources/images/app_icons/Approve-Request.png" />&nbsp;
																					</t:commandLink>
																		<t:commandLink
																			action="#{pages$portfolioManage.onOrderEdit}"
																			rendered="#{dataItem.isDeleted == 0}">
																			<f:param name="orderType" value="buying" />
																			<h:graphicImage title="#{msg['commons.edit']}"
																				url="../resources/images/edit-icon.gif" />&nbsp;
																					</t:commandLink>
																		<a4j:commandLink
																			onclick="if (!confirm('#{msg['order.confirm.delete']}')) return"
																			action="#{pages$portfolioManage.onOrderDelete}"
																			reRender="buyingOrderDataTable,buyingOrderGridInfo"
																			rendered="#{dataItem.isDeleted == 0}">
																			<f:param name="orderType" value="buying" />
																			<h:graphicImage title="#{msg['commons.delete']}"
																				url="../resources/images/delete.gif" />&nbsp;
																					</a4j:commandLink>
																	</t:column>

																</t:dataTable>
															</t:div>

															<t:div id="buyingOrderGridInfo"
																styleClass="contentDivFooter" style="width:100%">
																<t:panelGrid id="rqtkRecNumTable" columns="2"
																	cellpadding="0" cellspacing="0" width="100%"
																	columnClasses="RECORD_NUM_TD,BUTTON_TD">
																	<t:div id="rqtkRecNumDiv" styleClass="RECORD_NUM_BG">
																		<t:panelGrid id="rqtkRecNumTbl" columns="3"
																			columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
																			cellpadding="0" cellspacing="0"
																			style="width:182px;#width:150px;">
																			<h:outputText value="#{msg['commons.recordsFound']}" />
																			<h:outputText value=" : " />
																			<h:outputText
																				value="#{pages$portfolioManage.buyingOrderListRecordSize}" />
																		</t:panelGrid>
																	</t:div>

																	<CENTER>
																		<t:dataScroller id="rqtkscroller"
																			for="buyingOrderDataTable" paginator="true"
																			fastStep="1"
																			paginatorMaxPages="#{pages$portfolioManage.paginatorMaxPages}"
																			immediate="false" paginatorTableClass="paginator"
																			renderFacetsIfSinglePage="true"
																			pageIndexVar="pageNumber" styleClass="SCH_SCROLLER"
																			paginatorActiveColumnStyle="font-weight:bold;"
																			paginatorRenderLinkForActive="false"
																			paginatorTableStyle="grid_paginator"
																			layout="singleTable"
																			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

																			<f:facet name="first">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblFRequestHistory"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastrewind">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFRRequestHistory"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastforward">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}"
																					id="lblFFRequestHistory"></t:graphicImage>
																			</f:facet>

																			<f:facet name="last">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblLRequestHistory"></t:graphicImage>
																			</f:facet>

																			<t:div id="rqtkPageNumDiv" styleClass="PAGE_NUM_BG">
																				<t:panelGrid id="rqtkPageNumTable"
																					styleClass="PAGE_NUM_BG_TABLE" columns="2"
																					cellpadding="0" cellspacing="0">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</t:panelGrid>

																			</t:div>
																		</t:dataScroller>
																	</CENTER>

																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Buying Orders Tab - End -->

														<!-- Selling Orders Tab - Start -->
														<rich:tab
															label="#{msg['portfolio.manage.tab.order.selling.heading']}"
															rendered="#{pages$portfolioManage.isPortfolioApproved}">
															<h:panelGrid styleClass="BUTTON_TD" width="100%">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['order.add.btn.selling']}"
																	action="#{pages$portfolioManage.onOrderAddSelling}"
																	style="width: 120px;"
																	rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}">
																	<f:param name="orderType" value="selling" />
																</h:commandButton>
															</h:panelGrid>

															<t:div styleClass="contentDiv" style="width:99%">
																<t:dataTable id="sellingOrderDataTable"
																	rows="#{pages$portfolioManage.paginatorRows}"
																	value="#{pages$portfolioManage.sellingOrderViewList}"
																	binding="#{pages$portfolioManage.sellingOrderDataTable}"
																	preserveDataModel="false" preserveSort="false"
																	var="dataItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true" width="100%">

																	<t:column width="18%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['order.transaction.no']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.transactionRefNo}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="5%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.date']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.transactionDate}"
																			rendered="#{dataItem.isDeleted == 0}">
																			<f:convertDateTime
																				timeZone="#{pages$portfolioManage.timeZone}"
																				pattern="#{pages$portfolioManage.dateFormat}" />
																		</t:outputText>
																	</t:column>

																	<t:column width="18%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['order.current.value.nav']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.currentValueNav}"
																			rendered="#{dataItem.isDeleted == 0}">
																			<f:convertNumber minFractionDigits="2"
																				maxFractionDigits="2" pattern="##,###,##0.00" />
																		</t:outputText>
																	</t:column>

																	<t:column width="24%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['order.total.amount.returned']}" />
																		</f:facet>
																		<t:outputText
																			value="#{dataItem.totalAmountInvestReturn}"
																			rendered="#{dataItem.isDeleted == 0}">
																			<f:convertNumber minFractionDigits="2"
																				maxFractionDigits="2" pattern="##,###,##0.00" />
																		</t:outputText>
																	</t:column>

																	<t:column width="16%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['order.total.units.sold']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.totalUnitsBoughtSold}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="8%" sortable="true" rendered="false">
																		<f:facet name="header">
																			<t:outputText value="#{msg['order.total.units']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.totalUnits}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="9%" sortable="true" rendered="false">
																		<f:facet name="header">
																			<t:outputText value="#{msg['order.total.worth']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.totalAmount}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="9%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.status']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$portfolioManage.isEnglishLocale ? dataItem.orderStatusEn : dataItem.orderStatusAr}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column
																		rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}"
																		sortable="false" width="15%">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<t:commandLink
																			action="#{pages$portfolioManage.onOrderView}"
																			rendered="#{dataItem.isDeleted == 0}">
																			<f:param name="orderType" value="selling" />
																			<h:graphicImage title="#{msg['commons.view']}"
																				url="../resources/images/app_icons/Approve-Request.png" />&nbsp;
																					</t:commandLink>
																		<t:commandLink
																			action="#{pages$portfolioManage.onOrderEdit}"
																			rendered="#{dataItem.isDeleted == 0}">
																			<f:param name="orderType" value="selling" />
																			<h:graphicImage title="#{msg['commons.edit']}"
																				url="../resources/images/edit-icon.gif" />&nbsp;
																					</t:commandLink>
																		<a4j:commandLink
																			onclick="if (!confirm('#{msg['order.confirm.delete']}')) return"
																			action="#{pages$portfolioManage.onOrderDelete}"
																			reRender="sellingOrderDataTable,sellingOrderGridInfo"
																			rendered="#{dataItem.isDeleted == 0}">
																			<f:param name="orderType" value="selling" />
																			<h:graphicImage title="#{msg['commons.delete']}"
																				url="../resources/images/delete.gif" />&nbsp;
																					</a4j:commandLink>
																	</t:column>

																</t:dataTable>
															</t:div>

															<t:div id="sellingOrderGridInfo"
																styleClass="contentDivFooter" style="width:100%">
																<t:panelGrid id="rqtkRecNumTable2" columns="2"
																	cellpadding="0" cellspacing="0" width="100%"
																	columnClasses="RECORD_NUM_TD,BUTTON_TD">
																	<t:div id="rqtkRecNumDiv2" styleClass="RECORD_NUM_BG">
																		<t:panelGrid id="rqtkRecNumTbl2" columns="3"
																			columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
																			cellpadding="0" cellspacing="0"
																			style="width:182px;#width:150px;">
																			<h:outputText value="#{msg['commons.recordsFound']}" />
																			<h:outputText value=" : " />
																			<h:outputText
																				value="#{pages$portfolioManage.sellingOrderListRecordSize}" />
																		</t:panelGrid>
																	</t:div>

																	<CENTER>
																		<t:dataScroller id="rqtkscroller2"
																			for="sellingOrderDataTable" paginator="true"
																			fastStep="1"
																			paginatorMaxPages="#{pages$portfolioManage.paginatorMaxPages}"
																			immediate="false" paginatorTableClass="paginator"
																			renderFacetsIfSinglePage="true"
																			pageIndexVar="pageNumber" styleClass="SCH_SCROLLER"
																			paginatorActiveColumnStyle="font-weight:bold;"
																			paginatorRenderLinkForActive="false"
																			paginatorTableStyle="grid_paginator"
																			layout="singleTable"
																			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

																			<f:facet name="first">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblFRequestHistory2"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastrewind">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFRRequestHistory2"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastforward">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}"
																					id="lblFFRequestHistory2"></t:graphicImage>
																			</f:facet>

																			<f:facet name="last">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblLRequestHistory2"></t:graphicImage>
																			</f:facet>

																			<t:div id="rqtkPageNumDiv2" styleClass="PAGE_NUM_BG">
																				<t:panelGrid id="rqtkPageNumTable2"
																					styleClass="PAGE_NUM_BG_TABLE" columns="2"
																					cellpadding="0" cellspacing="0">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</t:panelGrid>

																			</t:div>
																		</t:dataScroller>
																	</CENTER>

																</t:panelGrid>
															</t:div>


														</rich:tab>
														<!-- Selling Orders Tab - End -->

														<!-- Attachments Tab - Start -->
														<rich:tab label="#{msg['contract.attachmentTabHeader']}">
															<%@ include file="attachment/attachment.jsp"%>
														</rich:tab>
														<!-- Attachments Tab - End -->

														<!-- Comments Tab - Start -->
														<rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														<!-- Comments Tab - End -->

													</rich:tabPanel>

													<table cellpadding="0" cellspacing="0" style="width: 100%;">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>

													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td colspan="6" class="BUTTON_TD">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['AuctionDepositRefundApproval.Buttons.Approve']}"
																	action="#{pages$portfolioManage.onApprove}"
																	rendered="#{pages$portfolioManage.isStatusNew && pages$portfolioManage.isEditMode}">
																</h:commandButton>
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$portfolioManage.onSave}"
																	rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}">
																</h:commandButton>



																<h:commandButton styleClass="BUTTON"
																	rendered="#{!pages$portfolioManage.showBackButton}"
																	value="#{msg['commons.cancel']}"
																	action="#{pages$portfolioManage.onCancel}">
																</h:commandButton>
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.back']}"
																	action="#{pages$portfolioManage.onBack}"
																	rendered="#{pages$portfolioManage.showBackButton}">
																</h:commandButton>
															</td>
														</tr>
													</table>
												</div>

											</h:form>
										</div>


										</div>
									</td>
								</tr>
							</table>

						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>

<script type="text/javascript">
	if ( document.getElementById('frm:budgetRateOfReturn') != null )
		document.getElementById('frm:budgetRateOfReturn').value = '0.00';
</script>
