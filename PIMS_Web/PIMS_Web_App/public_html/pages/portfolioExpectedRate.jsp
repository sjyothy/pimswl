<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>				
															<h:panelGrid rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}" styleClass="BUTTON_TD" width="100%" columns="4" id="expectedRatePanelGrid">
																<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.expected.rate']}:" />
																</h:panelGroup>
																<h:panelGroup>	
																	<h:selectOneMenu onchange="onExpectedRateChanged();" value="#{pages$portfolioManage.portfolioExpectedRateView.strExpectedRateId}" style="width: 192px;" id="strExpectedRateId">
																		<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
																		<f:selectItems value="#{pages$ApplicationBean.expectedRateList}" />																	
																	</h:selectOneMenu>
																	<a4j:commandLink id="lnkChangePeriod" action="#{pages$portfolioManage.onExpectedRateChanged}" reRender="expectedRatePanelGrid" 
																	oncomplete="onExpectedRateChangedCompleteRequest();"
																	/>
																</h:panelGroup>
																
																<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel styleClass="LABEL" value="#{msg['commons.value']}:" />
																</h:panelGroup>
																<h:inputText id="expectedRateValue" value="#{pages$portfolioManage.portfolioExpectedRateView.strExpectedRateValue}" style="width: 186px;" maxlength="5"
																styleClass="#{! pages$portfolioManage.portfolioExpectedRateView.hasValue ? 'READONLY' : ''}" 
																readonly="#{! pages$portfolioManage.portfolioExpectedRateView.hasValue}"
																 />
																
																<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.expected.rate.period']}:" />
																</h:panelGroup>
																<h:inputText id="expectedRatePeriod" styleClass="#{! pages$portfolioManage.portfolioExpectedRateView.hasPeriod ? 'READONLY' : ''}" readonly="#{! pages$portfolioManage.portfolioExpectedRateView.hasPeriod}" value="#{pages$portfolioManage.portfolioExpectedRateView.strExpectedRatePeriod}" style="width: 186px;" maxlength="3" />																
																
																<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.expected.rate.times']}:" />
																</h:panelGroup>
																<h:inputText id="expectedRateTimes"  style="width: 186px;" maxlength="5" 
																styleClass="#{! pages$portfolioManage.portfolioExpectedRateView.hasTimes ? 'READONLY' : ''}" 
																readonly="#{! pages$portfolioManage.portfolioExpectedRateView.hasTimes}"
																value="#{pages$portfolioManage.portfolioExpectedRateView.strExpectedRateTimes}"
																/>	
																
																<h:panelGroup>	
																	<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.expected.rate.note']}:" />
																</h:panelGroup>
																<h:inputTextarea
																cols="34"
																id="expectedRateNote"
																value="#{pages$portfolioManage.portfolioExpectedRateView.strExpectedRateNote}"
																styleClass="#{! pages$portfolioManage.portfolioExpectedRateView.hasNote ? 'READONLY' : ''}" 
																readonly="#{! pages$portfolioManage.portfolioExpectedRateView.hasNote}"
																/>
																
																<h:outputText value=""></h:outputText>
																<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.Add']}"
																				action="#{pages$portfolioManage.onExpectedRateAdd}"
																				style="width: 120px;" />
																				
																			
																				
																				
															</h:panelGrid>
															
															
															<t:div styleClass="contentDiv" style="width:99%">																
						                                       <t:dataTable id="expectedRateDataTable"  
																	rows="#{pages$portfolioManage.paginatorRows}"
																	value="#{pages$portfolioManage.portfolioExpectedRateViewList}"
																	binding="#{pages$portfolioManage.expectedRateDataTable}"																	
																	preserveDataModel="false" preserveSort="false" var="dataItem"
																	rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
																	
																	<t:column width="30%" sortable="true" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['portfolio.manage.tab.portfolio.expected.rate']}" />
																		</f:facet>
																		<t:outputText value="#{pages$portfolioManage.isEnglishLocale ? dataItem.expectedRateView.expectedRateEn : dataItem.expectedRateView.expectedRateAr}" rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																																		
																	<t:column width="10%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.value']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.expectedRateValue}" rendered="#{dataItem.isDeleted == 0}" />																		
																	</t:column>
																	
																	<t:column width="10%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['portfolio.expected.rate.period']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.expectedRatePeriod}" rendered="#{dataItem.isDeleted == 0}" />																		
																	</t:column>
																	
																	<t:column width="10%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['portfolio.expected.rate.times']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.expectedRateTimes}" rendered="#{dataItem.isDeleted == 0}" />																		
																	</t:column>
																	
																	<t:column width="35%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['portfolio.expected.rate.note']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.expectedRateNote}" rendered="#{dataItem.isDeleted == 0}" />																		
																	</t:column>
																	
																	<t:column width="5%" sortable="false" rendered="#{pages$portfolioManage.isAddMode || pages$portfolioManage.isEditMode}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<a4j:commandLink onclick="if (!confirm('#{msg['expectedRate.confirm.delete']}')) return" action="#{pages$portfolioManage.onExpectedRateDelete}" rendered="#{dataItem.isDeleted == 0}" reRender="expectedRateDataTable,expectedRateGridInfo">
																			<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />
																		</a4j:commandLink>																															
																	</t:column>
																  </t:dataTable>										
																</t:div>
														
				                                           		<t:div id="expectedRateGridInfo" styleClass="contentDivFooter" style="width:100%">
																			<t:panelGrid id="rqtkRecNumTable5"  columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
																				<t:div id="rqtkRecNumDiv5" styleClass="RECORD_NUM_BG">
																					<t:panelGrid id="rqtkRecNumTbl5" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
																						<h:outputText value="#{msg['commons.recordsFound']}"/>
																						<h:outputText value=" : "/>
																						<h:outputText value="#{pages$portfolioManage.expectedRateRecordSize}"/>																						
																					</t:panelGrid>
																				</t:div>
																				  
																			      <CENTER>
																				  	<t:dataScroller id="rqtkscroller5" for="expectedRateDataTable" paginator="true"
																									fastStep="1"  
																									paginatorMaxPages="#{pages$portfolioManage.paginatorMaxPages}" 
																									immediate="false"
																									paginatorTableClass="paginator"
																									renderFacetsIfSinglePage="true" 
																								    pageIndexVar="pageNumber"
																								    styleClass="SCH_SCROLLER"
																								    paginatorActiveColumnStyle="font-weight:bold;"
																								    paginatorRenderLinkForActive="false" 
																									paginatorTableStyle="grid_paginator" layout="singleTable"
																									paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">
								
																					    <f:facet  name="first">
																							<t:graphicImage url="../#{path.scroller_first}" id="lblFRequestHistory5"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastrewind">
																							<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFRRequestHistory5"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastforward">
																							<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFRequestHistory5"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="last">
																							<t:graphicImage url="../#{path.scroller_last}" id="lblLRequestHistory5"></t:graphicImage>
																						</f:facet>
									
								                                                      	<t:div id="rqtkPageNumDiv5" styleClass="PAGE_NUM_BG">
																						   	<t:panelGrid id="rqtkPageNumTable5" styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0" >
																						 		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																								<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																							</t:panelGrid>																							
																						</t:div>
																					 </t:dataScroller>
																				 	</CENTER>																		      
																	        </t:panelGrid>
								                          			 </t:div>
												