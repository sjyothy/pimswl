<h:panelGrid columns="2" columnClasses="presidentDiscussionColumn"
			rendered="#{pages$auctionDetails.advertisementTabActive}">
	<table width="3">
		<tr align="center">
		  	<td align="right" colspan="1">
		    	<h:outputLabel value="#{msg['advertisement.publicationDate']}:"></h:outputLabel>
		  	</td>
			<td colspan="1">
		        <h:inputText id="advertisementDate" readonly="#{!pages$auctionDetails.advertisementSaveMode}"  value="#{pages$auctionDetails.advertisementView.advertisementDate}" tabindex="3" style="width: 186px; height: 16px"></h:inputText>
			</td>
			<td colspan="1">
			<h:panelGroup rendered="#{!pages$auctionDetails.advertisementSaveMode}">
			<h:graphicImage style="width: 16;height: 16;border: 0" url="../resources/images/calendar.gif" onclick="javascript:Calendar(this,'advertisementDate',300,500);"/>
			</h:panelGroup>
			</td>
		</tr>
		
		<tr align="center">
			<td colspan="3">
			<h:commandButton type="submit" value="#{msg['commons.saveButton']}"
				action="#{pages$auctionDetails.saveAdvertisement}"
				style="width: 75px" tabindex="7" rendered="#{pages$auctionDetails.advertisementSaveMode}"/>&nbsp;
			
			<h:commandButton type="submit" value="#{msg['commons.approve']}"
				action="#{pages$auctionDetails.approveAdvertisement}"
				style="width: 75px" tabindex="9" rendered="#{pages$auctionDetails.canApproveAdvertisement}"/>&nbsp;
			<h:commandButton type="submit"
				value="#{msg['commons.reject']}"
				action="#{pages$auctionDetails.rejectAdvertisement}"
				style="width: 75px" tabindex="9" rendered="#{pages$auctionDetails.canApproveAdvertisement}" />
			</td>
			
		</tr>
		<tr align="center">
			<td colspan="3">
			<h:commandButton type="submit" value="#{msg['advertisement.requestPublication']}"
				action="#{pages$auctionDetails.requestForPublication}"
				style="width: 75px" tabindex="8" rendered="#{pages$auctionDetails.canRequestForPublication}" />&nbsp;
			<h:commandButton type="submit" value="#{msg['advertisement.publish']}"
				action="#{pages$auctionDetails.publishAdvertisement}"
				style="width: 75px" tabindex="9" rendered="#{pages$auctionDetails.canPublishAdvertisement}"/>&nbsp;
			</td>
		</tr>
		</table>
</h:panelGrid>