<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="javascript" type="text/javascript">
	function closeWindow()
	{
	  //window.opener.document.getElementById('conductAuctionForm:isResultPopupCalled').value = "Y";
	  window.opener.document.forms[0].submit();
	  window.close();
	}
	function numberRangeChanged()
	{
	  var floorto=0;
	  var numberOfFloors =  document.getElementById('GenerateFloorsForm:noOfFloors').value;	  
	  var floorFrom =  document.getElementById('GenerateFloorsForm:floorNumberFrom').value;	  
	  if(numberOfFloors != null && numberOfFloors.length >0 && floorFrom != null && floorFrom.length >0)
	  {	  
	  if(numberOfFloors!=0)
	  {
	  	    floorto = parseFloat(numberOfFloors) + parseFloat(floorFrom) - 1;	  
	   }
	   
	   document.getElementById('GenerateFloorsForm:floorNumberTo').value = floorto;
	   document.getElementById('GenerateFloorsForm:hdnFloorNumberTo').value = floorto;
	  }
	  else
	  {
	    document.getElementById('GenerateFloorsForm:floorNumberTo').value = null;
	    document.getElementById('GenerateFloorsForm:hdnFloorNumberTo').value = null;
	  }
	}
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
		</head>

		<body class="BODY_STYLE">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<%--<jsp:include page="header.jsp"/>--%>
					</td>
				</tr>

				<tr style="width: 100%">
					<td width="100%" valign="top" class="divBackgroundBody" colspan="2">
						<table  class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['generateFloors.PageHeader']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<div>
						<table width="717px"   class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<!--Use this below only for desiging forms Please do not touch other sections -->

								<td valign="top" nowrap="nowrap" height="100%">

									<h:form id="GenerateFloorsForm" >
									<div class="SCROLLABLE_SECTION" >
											<table width="100%" border="0">
											<tr>
												<td>
									
													<h:messages id="succMsgs">
														<h:outputText id="opMsgs" styleClass="INFO_FONT"
															value="#{pages$GenerateFloors.messages}" escape="false" />
													</h:messages>
												</td>
											</tr>
											<tr>
												<td>
													<h:messages id="errsMsgs">
														<h:outputText id="opErrorMsgs" styleClass="ERROR_FONT"
															value="#{pages$GenerateFloors.errorMessages}" escape="false" />
													</h:messages></td>
											</tr></table>

													<div class="MARGIN">

														<table cellpadding="0" cellspacing="0" width="100%" style="font-size:0pt;">
															<tr>
																<td style="font-size:0pt;">
																	<IMG
																		src="../<h:outputText value="#{path.img_section_left}"/>"
																		class="TAB_PANEL_LEFT" />
																</td>
																<td width="100%" style="font-size:0pt;">
																	<IMG
																		src="../<h:outputText value="#{path.img_section_mid}"/>"
																		class="TAB_PANEL_MID" />
																</td>
																<td style="font-size:0pt;">
																	<IMG
																		src="../<h:outputText value="#{path.img_section_right}"/>"
																		class="TAB_PANEL_RIGHT" />
																</td>
															</tr>
														</table>
														<div class="DETAIL_SECTION" style="width:99.5%;">
															<h:outputLabel
																value="#{msg['generateFloors.SectionHeader']}"
																styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
															<table cellpadding="1px" cellspacing="2px"
																class="DETAIL_SECTION_INNER">
																<tr>
																	<td colspan="4">

																	</td>
																</tr>
																<tr>
																	<td>
																		<h:outputLabel
																			value="#{msg['generateFloors.PropertyName']}"
																			styleClass="LABEL"></h:outputLabel>
																	</td>
																	<td>
																		<h:inputText id="propertyName" disabled="true" style="width:186px"
																			binding="#{pages$GenerateFloors.inputTextPropertyName}"></h:inputText>

																	</td>
																	<td>
																	
																		<h:outputLabel
																			value="#{msg['generateFloors.NoOfFloors']}"
																			styleClass="LABEL"></h:outputLabel>
																	</td>
																	<td>
																		<h:inputText id="noOfFloors" styleClass="A_RIGHT_NUM" style="width:184px"
																			onchange="javascript:numberRangeChanged();"
																			binding="#{pages$GenerateFloors.inputTextNumberOfFloors}"></h:inputText>
																	</td>
																</tr>
																<tr>
																	<td>
																		<h:outputLabel
																			value="#{msg['generateFloors.FloorNamePrefix']}"
																			styleClass="LABEL"></h:outputLabel>
																	</td>
																	<td>
																		<h:inputText id="floorNamePrefix" style="width:186px"
																			binding="#{pages$GenerateFloors.inputTextFloorNamePrefix}"></h:inputText>
																	</td>
																	<td>
																		<h:outputLabel
																			value="#{msg['generateFloors.FloorNumberPrefix']}"
																			styleClass="LABEL"></h:outputLabel>
																	</td>
																	<td>
																		<h:inputText id="floorNumberPrefix" style="width:186px"
																			binding="#{pages$GenerateFloors.inputTextFloorNumberPrefix}"></h:inputText>
																	</td>
																</tr>
																<tr>
																	<td>
																		<h:outputLabel
																			value="#{msg['generateFloors.FloorNumFrom']}"
																			styleClass="LABEL"></h:outputLabel>
																	</td>
																	<td>
																		<h:inputText id="floorNumberFrom" styleClass="A_RIGHT_NUM" style="width:184px"
																			binding="#{pages$GenerateFloors.inputTextFloorNumberFrom}"
																			onchange="javascript:numberRangeChanged();"></h:inputText>
																	</td>
																	<td>
																		<h:outputLabel
																			value="#{msg['generateFloors.FloorNumTo']}"
																			styleClass="LABEL"></h:outputLabel>
																	</td>
																	<td>
																		<h:inputText id="floorNumberTo" disabled="true" styleClass="A_RIGHT_NUM" style="width:184px"
																			binding="#{pages$GenerateFloors.inputTextFloorNumberTo}"></h:inputText>
																	</td>
																</tr>

																<tr>
																	<td>
																		<h:outputLabel
																			value="#{msg['generateFloors.FloorType']}"
																			styleClass="LABEL"></h:outputLabel>
																	</td>
																	<td>
																		<h:selectOneMenu id="floorTypes" required="false"
																			value="#{pages$GenerateFloors.floorTypeId}">

																			<f:selectItems
																				value="#{pages$ApplicationBean.floorTypesList}" />
																		</h:selectOneMenu>
																	</td>
																	<td>

																	</td>
																	<td>

																	</td>
																</tr>

																<tr>
																	<td colspan="4">
																	</td>
																</tr>
																<tr>
																	<td colspan="2">

																	</td>

																	<td colspan="2" class="BUTTON_TD">
																		<h:commandButton styleClass="BUTTON" value="#{msg['generateFloors.Generate']}"
																			action="#{pages$GenerateFloors.generatePropertyFloors}"></h:commandButton>

																		<h:commandButton styleClass="BUTTON" value="#{msg['generateFloors.Close']}"
																			onclick="javascript:closeWindow();"></h:commandButton>
																	</td>
																</tr>
															</table>
														</div>
													</div>
												</td>
											</tr>
										</table>

										<div>
											<h:inputHidden id="hdnPropertyId"
												binding="#{pages$GenerateFloors.hdnTextPropertyId}"></h:inputHidden>
											<h:inputHidden id="hdnPropertyName"
												binding="#{pages$GenerateFloors.hdnTextPropertyName}"></h:inputHidden>
											<h:inputHidden id="hdnFloorNumberTo"
												binding="#{pages$GenerateFloors.hdnTextFloorNumberTo}"></h:inputHidden>
									</div>
									</h:form>

								</td>
							</tr>

							<tr>
								<td>

								</td>
							</tr>
						</table>
				
					</td>
				</tr>
				<tr>
					<td colspan="2">
					</td>
				</tr>
			</table>
		</body>
	</html>
</f:view>
