
<t:div id="tabInvestment" style="width:100%;">
	<t:panelGrid id="disbTab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblBenef" styleClass="LABEL"
				value="#{msg['mems.investment.label.beneficiary']}:" />
		</h:panelGroup>
		<h:selectOneMenu id="benefList"
			binding="#{pages$InvestmentRequest.cmbBeneficiaryList}"
			style="width: 200px;"
			styleClass="#{pages$InvestmentRequest.beneficiaryCmbCss}"
			readonly="#{!pages$InvestmentRequest.isNewStatus || !pages$InvestmentRequest.isFileAssociated}">
			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems value="#{pages$InvestmentRequest.beneficiaryList}" />
			<a4j:support event="onchange" reRender="inBalance,blockinBalance,availableBalance,requestedAmount,errorMessages"
				action="#{pages$InvestmentRequest.handleBeneficiaryChange}" />
		</h:selectOneMenu>


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="amnt" styleClass="LABEL"
				value="#{msg['commons.amount']}:"></h:outputLabel>
		</h:panelGroup>
		<h:inputText id="inAmnt" maxlength="20"
			styleClass="#{pages$InvestmentRequest.readOnlyClass}"
			readonly="#{pages$InvestmentRequest.readOnlyMode}"
			binding="#{pages$InvestmentRequest.txtAmount}" />

		<h:outputLabel id="lblComment" styleClass="LABEL"
			value="#{msg['commons.description']}:">
		</h:outputLabel>

		<h:inputTextarea id="in" style="width: 200px;"
			styleClass="#{pages$InvestmentRequest.readOnlyClass}"
			readonly="#{pages$InvestmentRequest.readOnlyMode}"
			binding="#{pages$InvestmentRequest.txtDescription}">
		</h:inputTextarea>

		<h:outputLabel id="lblBalance" styleClass="LABEL"
			value="#{msg['mems.normaldisb.label.balance']}:">
		</h:outputLabel>
		<t:panelGroup>
			<h:inputText id="inBalance" readonly="true" styleClass="READONLY"
				binding="#{pages$InvestmentRequest.txtBalance}">
			</h:inputText>
			<%-- 
				<t:commandLink actionListener="#{pages$InvestmentRequest.openAvailableBankTransfersPopUp}"															>
																	<h:graphicImage id="editIcon"
																		title="#{msg['commons.edit']}"
																		url="../resources/images/edit-icon.gif" />
				
				</t:commandLink>
			--%>
		</t:panelGroup>
		<h:outputLabel id="lblBlockBalance" styleClass="LABEL"
			value="#{msg['blocking.lbl.amount']}"></h:outputLabel>
		<h:panelGroup>
			<h:inputText id="blockinBalance" styleClass="READONLY"
				readonly="true" binding="#{pages$InvestmentRequest.blockingBalance}"
				maxlength="20" />
			<h:commandLink id="lnkBlockingDetails"
				action="#{pages$InvestmentRequest.onOpenBlockingDetailsPopup}">
				<h:graphicImage id="openBlockingDetailsPopup"
					style="margin-right:5px;"
					title="#{msg['blocking.lbl.blockingDetails']}"
					url="../resources/images/app_icons/manage_tender.png" />
			</h:commandLink>
		</h:panelGroup>


		<h:outputLabel id="labelrequestedAmount" styleClass="LABEL"
			value="#{msg['commons.requestedAmount']}:">
		</h:outputLabel>
		<h:panelGroup>
			<h:inputText id="requestedAmount" readonly="true"
				styleClass="READONLY"
				binding="#{pages$InvestmentRequest.requestedAmount}">
			</h:inputText>
			<h:commandLink id="lnkDisbDetails"
				action="#{pages$InvestmentRequest.onOpenBeneficiaryDisbursementDetailsPopup}">
				<h:graphicImage id="onOpenBeneficiaryDisbursementDetailsPopup"
					style="margin-right:5px;"
					title="#{msg['commons.disbursementDetails']}"
					url="../resources/images/app_icons/manage_tender.png" />
			</h:commandLink>
		</h:panelGroup>

		<h:outputLabel id="labelAVBalance" styleClass="LABEL"
			value="#{msg['commons.availableBalance']}:">
		</h:outputLabel>
		<h:inputText id="availableBalance" readonly="true"
			styleClass="READONLY"
			binding="#{pages$InvestmentRequest.availableBalance}">
		</h:inputText>


	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;">
		<pims:security screen="Pims.MinorMgmt.InvestmentRequest.Create"
			action="create">
			<h:commandButton styleClass="BUTTON"
				rendered="#{pages$InvestmentRequest.isNewStatus}"
				action="#{pages$InvestmentRequest.saveBeneficiary}"
				value="#{msg['commons.saveButton']}">
			</h:commandButton>
		</pims:security>
		<pims:security screen="Pims.MinorMgmt.InvestmentRequest.Approve"
			action="create">
			<h:commandButton styleClass="BUTTON"
				rendered="#{pages$InvestmentRequest.isApprovalRequired}"
				action="#{pages$InvestmentRequest.saveBeneficiary}"
				value="#{msg['commons.saveButton']}">
			</h:commandButton>
		</pims:security>
	</t:div>
	<t:panelGrid columns="1" cellpadding="0" cellspacing="0" width="100%">
		<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
			<t:dataTable id="prdc" rows="15" width="100%"
				value="#{pages$InvestmentRequest.disbursementDetails}"
				binding="#{pages$InvestmentRequest.disbursementTable}"
				preserveDataModel="false" preserveSort="false" var="unitDataItem"
				rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
				<t:column id="beneficiary" sortable="true">
					<f:facet name="header">
						<t:outputText id="hdBenef"
							value="#{msg['mems.investment.label.beneficiaries']}" />
					</f:facet>
					<t:commandLink
						actionListener="#{pages$InvestmentRequest.openManageBeneficiaryPopUp}"
						value="#{unitDataItem.beneficiariesName}"
						style="white-space: normal;" styleClass="A_LEFT" />
				</t:column>
				<t:column id="amnt" sortable="true">
					<f:facet name="header">
						<t:outputText id="hdAmnt" value="#{msg['commons.amount']}" />
					</f:facet>
					<t:outputText id="amountt" styleClass="A_LEFT"
						value="#{unitDataItem.amount}" />
				</t:column>

				<t:column id="desc" sortable="true">
					<f:facet name="header">
						<t:outputText id="hdTo"
							value="#{msg['mems.payment.request.lable.Desc']}" />
					</f:facet>
					<t:outputText id="p_w" styleClass="A_LEFT"
						value="#{unitDataItem.description}" />
				</t:column>
				<t:column id="status" sortable="true">
					<f:facet name="header">
						<t:outputText id="statusOutput"
							value="#{msg['chequeList.status']}" />
					</f:facet>
					<t:outputText id="p_w1" styleClass="A_LEFT"
						value="#{pages$InvestmentRequest.isEnglishLocale?unitDataItem.statusEn:unitDataItem.statusAr}" />
				</t:column>

				<t:column id="actionId" sortable="true">
					<f:facet name="header">
						<t:outputText id="hdAmoun" value="#{msg['commons.action']}" />
					</f:facet>

					<h:commandLink
						action="#{pages$InvestmentRequest.openAssociateCostCenters}"
						rendered="#{unitDataItem.showAssoCostCentersPopUp && (pages$InvestmentRequest.isApprovalRequired || pages$InvestmentRequest.isReviewed)}">
						<h:graphicImage id="associateCostCenterIcon"
							title="#{msg['commons.cmdLink.openAssociateCostCenter']}"
							url="../resources/images/app_icons/Add-Fee-sub-type.png"
							width="18px;" />
					</h:commandLink>
					<h:commandLink
						action="#{pages$InvestmentRequest.deleteBeneficiary}"
						rendered="#{pages$InvestmentRequest.isNewStatus}">
						<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
							url="../resources/images/delete.gif" width="18px;" />
					</h:commandLink>
					<h:commandLink action="#{pages$InvestmentRequest.editBeneficiary}">
						<h:graphicImage id="editIcon" title="#{msg['commons.edit']}"
							url="../resources/images/edit-icon.gif" width="18px;" />
					</h:commandLink>
					<pims:security screen="Pims.MinorMgmt.InvestmentRequest.Approve"
						action="create">
						<h:commandLink
							action="#{pages$InvestmentRequest.editPaymentDetails}"
							rendered="#{pages$InvestmentRequest.isApprovalRequired || pages$InvestmentRequest.isReviewed}">
							<h:graphicImage id="pymtIcon"
								title="#{msg['mems.payment.request.label.paymentDetails']}"
								url="../resources/images/app_icons/Pay-Fine.png" width="18px;" />
						</h:commandLink>
					</pims:security>
				</t:column>
			</t:dataTable>
		</t:div>
		<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
			style="width:99.1%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
				cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']}" />
						<h:outputText value=" : " />
						<h:outputText value="#{pages$InvestmentRequest.totalRows}" />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="shareScrollers" for="prdc" paginator="true"
						fastStep="1" immediate="false" paginatorTableClass="paginator"
						renderFacetsIfSinglePage="true"
						paginatorTableStyle="grid_paginator" layout="singleTable"
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" pageIndexVar="pageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
		</t:div>
	</t:panelGrid>
</t:div>


