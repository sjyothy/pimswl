<%-- 
  - Author: Wasif A Kirmani
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Financial Configuration
  --%>
  
<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script src="../resources/jscripts/ComboItemTootTip.js" language="javaScript"/> 
<script language="JavaScript" type="text/javascript">
	    
	    function resetValues()
      	{
      	    document.getElementById("searchFrm:tenderNumber").value="";
			document.getElementById("searchFrm:tenderTypeCombo").selectedIndex=0;
      	    $('searchFrm:issueDateFrom').component.resetSelectedDate();
			$('searchFrm:issueDateTo').component.resetSelectedDate();
			$('searchFrm:endDateFrom').component.resetSelectedDate();
			$('searchFrm:endDateTo').component.resetSelectedDate();
			document.getElementById("searchFrm:propertyName").value="";
			document.getElementById("searchFrm:tenderDescription").value="";
			
        }
        
        
        
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<body class="BODY_STYLE">
		<div class="containerDiv">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>

				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['financialAccConfiguration.search.heading']}"
										styleClass="HEADER_FONT" />

								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td width="100%" height="466px" valign="top">
									<div class="SCROLLABLE_SECTION  AUC_SCH_SS">
										<h:form id="searchFrm" style="WIDTH: 97.6%;">
										<h:inputHidden id="isPosted" binding="#{pages$searchFinancialConfiguration.parentHidden}"/>
											<table border="0" class="layoutTable">
												<tr>
													<td>
														<h:outputText value="#{pages$searchFinancialConfiguration.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />
														<h:outputText value="#{pages$searchFinancialConfiguration.successMessages}"
															escape="false" styleClass="INFO_FONT" />
													</td>
												</tr>
											</table>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="font-size: 0px;">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td style="font-size: 0px;" width="100%">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="font-size: 0px;">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr><td width="100%">
														<table width="97%">
														<tr>
															<td
																width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['financialAccConfiguration.bankAccName']}:"></h:outputLabel>
															</td>
															<td
																width="25%">
																<h:inputText id="batchNumber"
																	binding="#{pages$searchFinancialConfiguration.bankRemitanceAccountName}"
																	style="width: 186px;" maxlength="20">
																</h:inputText>
															</td>
															
															<td
																width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['financialAccConfiguration.bankRemitanceAccountName']}:"></h:outputLabel>
																
															</td>
															<td
																width="25%">
																<h:inputText id="GRPAccountNumber"
																		style="width: 186px;"
																	binding="#{pages$searchFinancialConfiguration.GRPAccountNumber}"
																	 maxlength="20">
																</h:inputText>
															</td>
</tr><tr>
															<td
																width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['grp.paymentType']}:"></h:outputLabel>
															</td>
															<td
																width="25%">
																<h:selectOneMenu id="paymentTypeMenu"
																	binding="#{pages$searchFinancialConfiguration.paymentTypeMenu}"
																	 required="false">
																	<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$searchFinancialConfiguration.paymentTypeList}" />
																</h:selectOneMenu>
															</td>
		
															<td
																width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['grp.ownerShipMenu']}:"></h:outputLabel>
															</td>
															<td
																width="25%">
																<h:selectOneMenu id="ownerShipMenu"
																	binding="#{pages$searchFinancialConfiguration.ownerShipMenu}"
																	 required="false">
																	<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.propertyOwnershipType}" />
																</h:selectOneMenu>
															</td>

															
														</tr>
														<tr>
															<td
																width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['cancelContract.payment.paymentmethod']}:"></h:outputLabel>
															</td>
															<td
																width="25%">
																<h:selectOneMenu id="PaymentMethodMenu"
																	binding="#{pages$searchFinancialConfiguration.paymentMethod}"
																	 required="false">
																	<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$searchFinancialConfiguration.paymentMethodList}" />
																</h:selectOneMenu>
															</td>
														<td
																width="25%">
																<h:outputLabel styleClass="LABEL" value="#{msg['grp.TrxType']}:"></h:outputLabel>
														</td>
														<td
																width="25%">
																<h:selectOneMenu   id="XXTransactionTypeMenu"
																	binding="#{pages$searchFinancialConfiguration.trxTypeMenu}"
																	 required="false">
																	<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}"  />
																	<f:selectItems value="#{pages$ApplicationBean.xxAmafTransactionTypes}" />
																</h:selectOneMenu>
															</td>

															
														</tr>
														
												</div>
														<tr>
															<td class="BUTTON_TD" colspan="4">
																		
															
																			<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.search']}"
																				action="#{pages$searchFinancialConfiguration.searchTransactions}"
																				style="width: 75px" />
																			<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.clear']}"
																				action="#{pages$searchFinancialConfiguration.btnClear_Click}"
																				style="width: 75px" />
                                                            				<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.add']}"
																				action="#{pages$searchFinancialConfiguration.btnAdd_Click}"
																				style="width: 75px" />
														</td>
													</tr>
														</table>
														</td>
																	<td width="3%">
																		&nbsp;
																	</td>
														
														</tr>
															
													</table>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
												</div>
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" style="width: 99%;">
													<t:dataTable id="dt1"
														value="#{pages$searchFinancialConfiguration.configurationDataList}"
														binding="#{pages$searchFinancialConfiguration.configurationGrid}"
														rows="#{pages$searchFinancialConfiguration.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%">
														
														<t:column id="tenderDescCol" width="20%" sortable="true" style=";white-space:normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['grp.ownership']}" />
															</f:facet>
															<t:outputText value="#{dataItem.ownerShipTypeEn}" rendered = "#{pages$searchFinancialConfiguration.isEnglishLocale}"/>
															<t:outputText value="#{dataItem.ownerShipTypeAr}" rendered = "#{pages$searchFinancialConfiguration.isArabicLocale}"/> 
														</t:column>
														<t:column id="tenderTypeCol" width="12%" sortable="true" style=";white-space:normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['grp.paymentType']}" />
															</f:facet>
															<t:outputText value="#{dataItem.paymentTypeEn}" rendered = "#{pages$searchFinancialConfiguration.isEnglishLocale}"/>
															<t:outputText value="#{dataItem.paymentTypeAr}" rendered = "#{pages$searchFinancialConfiguration.isArabicLocale}"/>
														</t:column>
														<t:column id="paymentMethod" width="12%" sortable="true" style=";white-space:normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['financialAccConfiguration.paymentMethod']}" />
															</f:facet>
															<t:outputText value="#{dataItem.paymentMethodDescription}" />
														
														</t:column>
														
														<t:column id="trxTypeDesc" width="12%" sortable="true" style=";white-space:normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['grp.TrxType']}" />
															</f:facet>
															<t:outputText value="#{dataItem.trxTypeDescription}" />
															
														</t:column>
														<t:column id="issueDateCol" width="15%" sortable="true" style=";white-space:normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['financialAccConfiguration.bankNo']}" />
															</f:facet>
															<t:outputText value="#{dataItem.grpAccountNumber}" />
														</t:column>
														<t:column id="bankAccName" width="15%" sortable="true" style=";white-space:normal;">
															<f:facet name="header">
															
																<t:outputText value="#{msg['financialAccConfiguration.bankName']}" />
															</f:facet>
															<t:outputText value="#{dataItem.bankRemitanceAccountName}" />
														</t:column>
														

													
														<t:column id="action" style=";white-space:normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['paymentConfiguration.Actions']}" />
															</f:facet>
															<h:commandLink  action="#{pages$searchFinancialConfiguration.imgEdit_Click}">
													           <h:graphicImage id="editIcon" title="#{msg['commons.edit']}" url="../resources/images/edit-icon.gif" />&nbsp;
													        </h:commandLink>
														  <h:commandLink  action="#{pages$searchFinancialConfiguration.cmdDelete_Click}">
													           <h:graphicImage id="delIcon" title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
													        </h:commandLink>
														</t:column>


												</t:dataTable>
												</div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width: 100%;">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{pages$searchFinancialConfiguration.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td >
																<t:dataScroller id="scroller" for="dt1" paginator="true"
																	fastStep="1"
																	paginatorMaxPages="#{pages$searchFinancialConfiguration.paginatorMaxPages}"
																	immediate="false" paginatorTableClass="paginator"
																	renderFacetsIfSinglePage="true"
																	paginatorTableStyle="grid_paginator"
																	layout="singleTable"
																	paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																	paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																	pageIndexVar="pageNumber" styleClass="SCH_SCROLLER" >

																	<f:facet name="first">
																		<t:graphicImage url="../#{path.scroller_first}"
																			id="lblF"></t:graphicImage>
																	</f:facet>
																	<f:facet name="fastrewind">
																		<t:graphicImage url="../#{path.scroller_fastRewind}"
																			id="lblFR"></t:graphicImage>
																	</f:facet>
																	<f:facet name="fastforward">
																		<t:graphicImage url="../#{path.scroller_fastForward}"
																			id="lblFF"></t:graphicImage>
																	</f:facet>
																	<f:facet name="last">
																		<t:graphicImage url="../#{path.scroller_last}"
																			id="lblL"></t:graphicImage>
																	</f:facet>
																	
																	<t:div styleClass="PAGE_NUM_BG" >
																		<table cellpadding="0" cellspacing="0">
																			<tr>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																				</td>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</td>
																			</tr>
																		</table>
																	</t:div>
																	
																</t:dataScroller>

															</td>
														</tr>
													</table>
												</t:div>
											</div>

									</div>
									</div>
									</h:form>

								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>

					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>

				</tr>
			</table>
			</div>
		</body>
	</html>
</f:view>

<script language="JavaScript" type="text/javascript">
	showToolTip();
</script>