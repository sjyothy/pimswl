
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<script language="javascript" type="text/javascript">
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden">

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />

			<style>
.rich-calendar-input {
	width: 75%;
}
</style>
			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

			<script language="JavaScript" type="text/javascript"
				src="../resources/jscripts/popcalendar_en.js"></script>


			<script language="javascript" type="text/javascript">
</SCRIPT>

		</head>
		<body class="BODY_STYLE">
			<div style="width: 1024px;">

				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['addMissingMigratedPayments.headerTitle.addPayments']}"
											styleClass="HEADER_FONT" style="padding:10px" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>


							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg">
									</td>
									<td width="0%" height="99%" valign="top" nowrap="nowrap">
										<h:form id="formContractList">
											<div class="SCROLLABLE_SECTION" style="width: 842px;">
												<table border="0" class="layoutTable" width="90%"
													style="margin-left: 15px; margin-right: 15px;">
													<tr>
														<td>
															<h:outputText id="errorMsg" escape="false"
																styleClass="ERROR_FONT"
																value="#{pages$addMissingPayments.errorMessages}" />
														</td>
														<td>
															<h:outputText id="successMsg" escape="false"
																styleClass="INFO_FONT"
																value="#{pages$addMissingPayments.successMessages}"/>
														</td>
													
													</tr>
												</table>
												<div class="MARGIN" style="width: 95.5%">
													<table cellpadding="0" cellspacing="0">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>

													<div class="DETAIL_SECTION"  style="width:99.8%;">
													<h:outputLabel value="#{msg['addMissingMigratedPayments.subTitle.requiredFileds']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													
													<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" border="0" >
														<tr>
															<td>
															<h:panelGrid columns="4" width="100%" id="paymentControls">
																<h:panelGroup >
																<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
																<h:outputLabel value="#{msg['addMissingMigratedPayments.fieldName.paymentType']} :"/>
																</h:panelGroup>
																 <h:selectOneMenu  id="selectPaymentType" 
																	 binding="#{pages$addMissingPayments.htmlPaymentType}">
															       <f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" 
																			     value="#{pages$addMissingPayments.paymentType}"	
																			     itemValue="-1"/>
															       <f:selectItems value="#{pages$ApplicationBean.paymentTypeList}"/>
																</h:selectOneMenu>
																
																<h:panelGroup>
																<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
																<h:outputLabel value="#{msg['addMissingMigratedPayments.fieldName.paymentMode']} :"/>
																</h:panelGroup>
																<h:selectOneMenu  id="selectPaymentMethod"  binding="#{pages$addMissingPayments.htmlPaymentMode}">
															       <f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" 
															       			     value="#{pages$addMissingPayments.paymentMode}"	
															       			     itemValue="-1"/>
															       <f:selectItems value="#{pages$ApplicationBean.paymentMethodList}"/>
															       <a4j:support event="onchange" action="#{pages$addMissingPayments.paymentModeChanged}" 
															       reRender="lblBankList,bankList,paymentControls" />
																</h:selectOneMenu>
																
																<h:panelGroup>
																<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
																<h:outputLabel value="#{msg['addMissingMigratedPayments.fieldName.paymentDueDate']} :"/>
																</h:panelGroup>
																<rich:calendar  id="dueDate" 
																  value="#{pages$addMissingPayments.paymentDueDate}"	 
										                          popup="true" datePattern="#{pages$addMissingPayments.dateFormat}" 
										                          showApplyButton="false"
										                          locale="#{pages$chequeList.locale}" 
										                          enableManualInput="false" inputStyle="width: 170px; height: 14px"/>
																
																<h:panelGroup>
																<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
																<h:outputLabel value="#{msg['addMissingMigratedPayments.fieldName.paymentStatus']} :"/>
																</h:panelGroup>
																<h:selectOneMenu  id="selectPaymentStatus"
																 binding="#{pages$addMissingPayments.htmlPaymentStatus}">
															       <f:selectItem 
															       value="#{pages$addMissingPayments.paymentStatus}"
															       itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
															       <f:selectItems value="#{pages$ApplicationBean.paymentScheduleStatusList}"/>
																</h:selectOneMenu>
																
																<h:panelGroup>
																<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
																<h:outputLabel value="#{msg['addMissingMigratedPayments.fieldName.amount']} :"/>
																</h:panelGroup>
																<h:inputText value="#{pages$addMissingPayments.paymentAmount}"/>
																
																<h:panelGroup>
																<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
																<h:outputLabel value="#{msg['addMissingMigratedPayments.fieldName.receiptNumber']} :"/>
																</h:panelGroup>
																<h:inputText value="#{pages$addMissingPayments.receiptNumber}"/>
																
																<h:outputLabel value="#{msg['addMissingMigratedPayments.fieldName.grpReceiptId']} :"/>
																<h:inputText value="#{pages$addMissingPayments.grpReceiptId}"/>
																
																<h:panelGroup rendered="#{pages$addMissingPayments.cheque || pages$addMissingPayments.bankTransfer }">
																<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
																<h:outputLabel id="lblChequeTransferNumber" 
																value="#{msg['addMissingMigratedPayments.fieldName.chequeNTransfer']} :"  />
																</h:panelGroup>
																<h:inputText id="chequeTransferNumber" 
																rendered="#{pages$addMissingPayments.cheque || pages$addMissingPayments.bankTransfer }"
																value="#{pages$addMissingPayments.chequeOrTransferNumber}" />
																
																<h:panelGroup rendered="#{pages$addMissingPayments.cheque || pages$addMissingPayments.bankTransfer }">
																<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
																<h:outputLabel value="#{msg['addMissingMigratedPayments.fieldName.bankName']} :" id="lblBankList" />
																</h:panelGroup>
																<h:selectOneMenu  id="bankList" 
																binding="#{pages$addMissingPayments.htmlBankName}"
																rendered="#{pages$addMissingPayments.cheque || pages$addMissingPayments.bankTransfer }">
															       <f:selectItem 
															       value="#{pages$addMissingPayments.bankName}"
															       itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
															       <f:selectItems value="#{pages$ApplicationBean.bankList}"/>
																</h:selectOneMenu>
																
																<h:outputLabel value="#{msg['addMissingMigratedPayments.fieldName.accountNumber']} :" id="lblAccountNumber" rendered="#{pages$addMissingPayments.cheque}"/>
																<h:inputText  id="accountNumber" value="#{pages$addMissingPayments.accountNumber}" rendered="#{pages$addMissingPayments.cheque}"/>
																
																<h:panelGroup rendered="#{pages$addMissingPayments.cheque}">
																<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
																<h:outputLabel value="#{msg['addMissingMigratedPayments.fieldName.chequeType']} :" id="lblChequeType" />
																</h:panelGroup>
																<h:selectOneMenu id="chequeType"
																binding="#{pages$addMissingPayments.htmlChequeType}"
																value="#{pages$addMissingPayments.chequeType}" rendered="#{pages$addMissingPayments.cheque}">
																     <f:selectItems value="#{pages$ApplicationBean.chequeType}" />
																</h:selectOneMenu>
																
																<h:panelGroup>
																<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
																<h:outputLabel value="#{msg['addMissingMigratedPayments.fieldName.description']} :"/>
																</h:panelGroup>
																<h:inputTextarea value="#{pages$addMissingPayments.description}"/>
																
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.trxNumber']}:"></h:outputLabel>
																	<h:inputText id="grpTrxNumber" maxlength="40"
																		value="#{pages$addMissingPayments.trxNumber}">
																	</h:inputText>
																
														</h:panelGrid>
														</td>
														</tr>
														<tr>
															<td colspan="4">
															<DIV class="BUTTON_TD">
															
																<h:commandButton styleClass="BUTTON" 
																	value="#{msg['addMissingMigratedPayments.buttonName.clear']}"
																	action="#{pages$addMissingPayments.clearFields}" 
																	style="width:auto;"/>
																<h:commandButton styleClass="BUTTON" 
																	value="#{msg['addMissingMigratedPayments.buttonName.viewContract']}"
																	action="#{pages$addMissingPayments.btnViewContractClick}" 
																	style="width:auto;"/>
																<h:commandButton styleClass="BUTTON" 
																	value="#{msg['addMissingMigratedPayments.buttonName.addPayment']}"
																	action="#{pages$addMissingPayments.btnAddPaymentContractClick}" 
																	style="width:auto;"/>
															</DIV>
															</td>	
														</tr>
													</table>
													<t:div>&nbsp;</t:div>
													<t:div styleClass="contentDiv"	style="width:99.2%;align:center;">
													 <table>
															<t:dataTable id="dt1" preserveSort="false" var="dataItem"
																rules="all" renderedIfEmpty="true" width="100%"
																rowClasses="row1,row2"
																rows="#{pages$addMissingPayments.paginatorRows}"
																value="#{pages$addMissingPayments.bounceChequeList}"
																binding="#{pages$addMissingPayments.dataTable}">

																	<t:column style="width:auto;" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['addMissingMigratedPayments.fieldName.paymentType']}" />
																		</f:facet>
																		<h:outputText id="ID1" value="#{pages$addMissingPayments.isEnglishLocale?dataItem.paymentTypeDescriptionEn:dataItem.paymentTypeDescriptionAr}">
																		</h:outputText>
																	</t:column>
																	
															      <t:column style="width:auto;" > 
																		<f:facet name="header">
																			<t:outputText value="#{msg['addMissingMigratedPayments.fieldName.paymentMode']}" />
																		</f:facet>
																		<h:outputText id="ID2" value="#{pages$addMissingPayments.isEnglishLocale?dataItem.paymentModeEn:dataItem.paymentModeAr}">
																		</h:outputText>
																	</t:column>
																		
																	<t:column style="width:auto;" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['addMissingMigratedPayments.fieldName.paymentDueDate']}" />
																		</f:facet>
																		<h:outputText id="ID3" value="#{dataItem.paymentDueOn}">
																		</h:outputText>
																	</t:column>
																	
																	<t:column style="width:auto;" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['addMissingMigratedPayments.fieldName.paymentStatus']}" />
																		</f:facet>
																		<h:outputText id="ID4" value="#{pages$addMissingPayments.isEnglishLocale?dataItem.statusEn:dataItem.statusAr}">
																		</h:outputText>
																	</t:column>
																	
																	<t:column style="width:auto;" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['addMissingMigratedPayments.fieldName.amount']}" />
																		</f:facet>
																		<<h:outputText id="ID5" value="#{dataItem.amount}">
																		</h:outputText>
																	</t:column>
																	
																	<t:column style="width:auto;" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['addMissingMigratedPayments.fieldName.receiptNumber']}" />
																		</f:facet>
																		<h:outputText id="ID6" value="#{dataItem.receiptNumber}">
																		</h:outputText>
																	</t:column>
																	
																	<t:column style="width:auto;" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['addMissingMigratedPayments.fieldName.grpReceiptId']}" />
																		</f:facet>
																		<h:outputText id="ID7" value="#{dataItem.grpReceiptId}">
																		</h:outputText>
																	</t:column>
																	
																	<t:column style="width:auto;"  >
																		<f:facet name="header">
																			<t:outputText value="#{msg['addMissingMigratedPayments.fieldName.description']}" />
																		</f:facet>
																		<h:outputText id="ID8" value="#{dataItem.description}">
																		</h:outputText>
																	</t:column>
																	
																	<t:column style="width:auto;" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['addMissingMigratedPayments.fieldName.bankName']}" />
																		</f:facet>
																		<h:outputText  id="ID9" value="#{pages$addMissingPayments.isEnglishLocale?dataItem.bankNameEn:dataItem.bankNameAr}">
																		</h:outputText>
																	</t:column>
																	
																	<t:column style="width:auto;">
																		<f:facet name="header">
																			<t:outputText value="#{msg['addMissingMigratedPayments.fieldName.chequeNTransfer']}" />
																		</f:facet>
																		<h:outputText  id="ID10" value="#{dataItem.chequeNumber}">
																		</h:outputText>
																	</t:column>
																	
																	<t:column style="width:auto;" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['addMissingMigratedPayments.fieldName.chequeType']}" />
																		</f:facet>
																		<h:outputText id="ID11" value="#{pages$addMissingPayments.isEnglishLocale?dataItem.chequeTypeEn:dataItem.chequeTypeAr}">
																		</h:outputText>
																	</t:column>
																	
																	<t:column style="width:auto;" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['addMissingMigratedPayments.fieldName.accountNumber']}" />
																		</f:facet>
																		<h:outputText id="ID12" value="#{dataItem.accountNumber}">
																		</h:outputText>
																	</t:column>
															</t:dataTable>
														</table>
													</t:div>
											</div>
										</h:form>
									</td>
								</tr>

							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>

	</html>
</f:view>
