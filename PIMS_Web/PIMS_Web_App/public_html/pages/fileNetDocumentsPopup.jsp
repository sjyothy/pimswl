<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for listing disbursement details of person 
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">

			<div class="SCROLLABLE_SECTION">

				<%
					response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
						response.setHeader("Pragma", "no-cache"); //HTTP 1.0
						response.setDateHeader("Expires", 0); //prevents caching at the proxy server
				%>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr width="100%">

						<td width="83%" height="100%" valign="top"
							class="divBackgroundBody">
							<table class="greyPanelTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel
											value="#{msg['attachment.buttons.archivedDocs']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>

							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr valign="top">
									<td>

										<h:form id="searchFrm" enctype="multipart/form-data" style ="min-height:410px;">

											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$fileNetDocumentsPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">
												<t:div styleClass="contentDiv" style="width:98%;">
													<t:dataTable id="attachmentTable" renderedIfEmpty="true"
														width="100%" cellpadding="0" cellspacing="0"
														var="dataItem"
														binding="#{pages$fileNetDocumentsPopup.dataTable}"
														value="#{pages$fileNetDocumentsPopup.list}"
														preserveDataModel="false" preserveSort="false"
														rowClasses="row1,row2" rules="all"
														rows="#{pages$fileNetDocumentsPopup.paginatorRows}">


														<t:column id="attachmentTabledoctype" width="3%"
															style="white-space: normal;"
															rendered="#{pages$fileNetDocumentsPopup.englishLocale}"
															sortable="true">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['attachment.grid.docTypeCol']}"
																	id="attachmentTabledoctypehdn" />
															</f:facet>
															<t:outputText value="#{dataItem.docTypeEn}"
																style="white-space: normal;" styleClass="A_LEFT"
																id="attachmentTabledoctypetxt" />

														</t:column>
														<t:column width="15%" style="white-space: normal;"
															rendered="#{!pages$fileNetDocumentsPopup.englishLocale}"
															sortable="true" id="attachmentTabledoctypear">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['attachment.grid.docTypeCol']}"
																	id="attachmentTabledoctypearheading" />
															</f:facet>
															<t:outputText value="#{dataItem.docTypeAr}"
																style="white-space: normal;" styleClass="A_LEFT"
																id="attachmentTabledoctypepathar" />

														</t:column>

														<t:column width="30%" sortable="true"
															style="white-space: normal;" id="attachmentTabledocname">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['attachment.grid.docNameCol']}"
																	id="attachmentTabledocnamehdn" />
															</f:facet>
															<t:outputText value="#{dataItem.documentTitle}"
																style="white-space: normal;" styleClass="A_LEFT"
																id="attachmentTabledocnametxt" />
														</t:column>

														<t:column id="colCreatedOn" style="width:15%;white-space: normal;"
															sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.date']}"
																	id="requestTasksDataTablecolcreatedonhn" />
															</f:facet>
															<t:outputText styleClass="A_LEFT" 
																style="white-space: normal;"
																value="#{dataItem.createdOn}"
																id="archiveDocCreatedonlbl">
																<f:convertDateTime
																	timeZone="#{pages$fileNetDocumentsPopup.timeZone}"
																	pattern="dd - MMM - yyyy H:m:s" />
															</t:outputText>

														</t:column>

														<t:column width="15%" sortable="true"
															id="attachmentTabledoccomment">
															<f:facet name="header">
																<h:outputText
																	value="#{msg['attachment.grid.commentsCol']}"
																	id="attachmentTabledoccommenthdn" />
															</f:facet>
															<t:outputText value="#{dataItem.comments}"
																style="white-space: normal;" styleClass="A_LEFT"
																id="attachmentTabledoccommenttxt" />
														</t:column>
														<t:column width="15%" sortable="false"
															style="white-space: normal;" id="attachmentFileNetDocId">
															<f:facet name="header">

																<h:outputText
																	value="#{msg['attachment.grid.fileNetDocId']}"
																	id="attachmentFileNetDocIdLbl" />

															</f:facet>
															<t:outputText value="#{dataItem.fileNetDocId}"
																style="white-space: normal;" styleClass="A_LEFT"
																id="attachmentFileNetDocIdtxt" />
														</t:column>
														<t:column width="5%" id="attachmentTabledocaction">
															<f:facet name="header"></f:facet>

															<t:commandLink id="lnkDownload"
																action="#{pages$fileNetDocumentsPopup.download}"
																value="" style="padding-left:1px;padding-right:1px;">
																<h:graphicImage id="fileDownloadIcon"
																	title="#{msg['attachment.buttons.download']}"
																	url="../resources/images/app_icons/downloadAttachment.png" />
															</t:commandLink>
														</t:column>

													</t:dataTable>
												</t:div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:99.1%;">
													<t:panelGrid columns="2" border="0" width="100%"
														cellpadding="1" cellspacing="1">
														<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
															<t:div styleClass="JUG_NUM_REC_ATT">
																<h:outputText value="#{msg['commons.records']}" />
																<h:outputText value=" : " />
																<h:outputText
																	value="#{pages$fileNetDocumentsPopup.recordSize}" />
															</t:div>
														</t:panelGroup>
														<t:panelGroup>
															<t:dataScroller id="attachmentScroller"
																for="attachmentTable" paginator="true" fastStep="1"
																paginatorMaxPages="#{pages$fileNetDocumentsPopup.paginatorMaxPages}"
																immediate="false" paginatorTableClass="paginator"
																renderFacetsIfSinglePage="true"
																paginatorTableStyle="grid_paginator"
																layout="singleTable"
																paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																paginatorActiveColumnStyle="font-weight:bold;"
																paginatorRenderLinkForActive="false"
																pageIndexVar="pageNumber" styleClass="JUG_SCROLLER">
																<f:facet name="first">
																	<t:graphicImage url="../#{path.scroller_first}"
																		id="lblF"></t:graphicImage>
																</f:facet>
																<f:facet name="fastrewind">
																	<t:graphicImage url="../#{path.scroller_fastRewind}"
																		id="lblFR"></t:graphicImage>
																</f:facet>
																<f:facet name="fastforward">
																	<t:graphicImage url="../#{path.scroller_fastForward}"
																		id="lblFF"></t:graphicImage>
																</f:facet>
																<f:facet name="last">
																	<t:graphicImage url="../#{path.scroller_last}"
																		id="lblL"></t:graphicImage>
																</f:facet>
																<t:div styleClass="PAGE_NUM_BG" rendered="true">
																	<h:outputText styleClass="PAGE_NUM"
																		value="#{msg['commons.page']}" />
																	<h:outputText styleClass="PAGE_NUM"
																		style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																		value="#{requestScope.pageNumber}" />
																</t:div>
															</t:dataScroller>
														</t:panelGroup>
													</t:panelGrid>
												</t:div>
											</div>
										</h:form>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

		</body>
	</html>
</f:view>