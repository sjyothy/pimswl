<%-- Author: Kamran Ahmed--%>
<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

var getGrpNumber = function (propertyName) {
    var jsonString = document.getElementById('masarifForm:grpJson').value;
    jsonObj = stringToJSONObject(jsonString);
    return jsonObj[propertyName];
};
var getBankName = function (propertyName) {
    var jsonString = document.getElementById('masarifForm:bankJson').value;
  jsonObj = stringToJSONObject(jsonString);
    return jsonObj[propertyName];
};

function selectBankAndGrp(){
	var selectedMasrafId = document.getElementById('masarifForm:parentMasraf').value;
	var  grpNumber = getGrpNumber(selectedMasrafId);
	var bankName = getBankName(selectedMasrafId);
	if(grpNumber != null)
		document.getElementById('masarifForm:parentGRP').value = grpNumber;
	else	
		document.getElementById('masarifForm:parentGRP').value = "";
	if(bankName != null)	
		document.getElementById('masarifForm:parentBank').value = bankName;
	else
	document.getElementById('masarifForm:parentBank').value = "";	
}

</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">

					<c:choose>
						<c:when test="${!pages$manageMasarif.popup}">
							<tr
								style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>

					<tr width="100%">
						<c:choose>
							<c:when test="${!pages$manageMasarif.popup}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['masraf.headerTitle.masarif']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="masarifForm" enctype="multipart/form-data"
												style="WIDTH:100%;">
												<h:inputHidden id="grpJson"
													value="#{pages$manageMasarif.parentGRPJson}" />
												<h:inputHidden id="bankJson"
													value="#{pages$manageMasarif.parentBankJson}" />
												<div>
													<table id="layoutTable" border="0" class="layoutTable"
														style="margin-left: 15px; width: 96%;">
														<tr>
															<td>
																<h:outputText id="errorMesssages"
																	value="#{pages$manageMasarif.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText
																	value="#{pages$manageMasarif.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
															</td>
														</tr>
													</table>
												</div>

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px; width: 90%;">
														<tr>
															<td>
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*"
																		rendered="#{!pages$manageMasarif.readOnly}" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['masraf.label.masrafName']}"></h:outputLabel>
																</h:panelGroup>
															</td>
															<td>
																<h:inputText maxlength="75"
																	styleClass="#{pages$manageMasarif.styleClass}"
																	readonly="#{pages$manageMasarif.readOnly}"
																	value="#{pages$manageMasarif.masarifView.masrafName}"></h:inputText>
															</td>
															<td>
																<h:panelGroup>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.BankName']}"></h:outputLabel>
																</h:panelGroup>
															</td>
															<td>
																<h:selectOneMenu
																	styleClass="#{pages$manageMasarif.styleClass}"
																	readonly="#{pages$manageMasarif.readOnly}"
																	value="#{pages$manageMasarif.masarifView.bankId}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.bankList}" />
																</h:selectOneMenu>
															</td>

														</tr>
														<tr>
															<td>
																<h:panelGroup>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['financialAccConfiguration.bankRemitanceAccountName']}"></h:outputLabel>
																</h:panelGroup>
															</td>
															<td>
																<h:inputText
																	styleClass="#{pages$manageMasarif.styleClass}"
																	readonly="#{pages$manageMasarif.readOnly}"
																	maxlength="100"
																	value="#{pages$manageMasarif.masarifView.bankAccountNumber}"></h:inputText>
															</td>
															<td>
																<h:panelGroup>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['accountStatus.GRPAccountNumber']}"></h:outputLabel>
																</h:panelGroup>
															</td>
															<td>
																<h:inputText
																	styleClass="#{pages$manageMasarif.styleClass}"
																	readonly="#{pages$manageMasarif.readOnly}"
																	maxlength="100"
																	value="#{pages$manageMasarif.masarifView.grpAccNumber}"></h:inputText>
															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['masraf.label.parentMasraf']}"></h:outputLabel>
															</td>
															<td>
																<h:selectOneMenu id="parentMasraf"
																	readonly="#{!pages$manageMasarif.enableParentMasraf || pages$manageMasarif.readOnly}"
																	styleClass="#{pages$manageMasarif.parentStyleClass}"
																	onchange="selectBankAndGrp();"
																	value="#{pages$manageMasarif.masarifView.parentId}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems
																		value="#{pages$manageMasarif.allParents}" />
																</h:selectOneMenu>
															</td>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['masraf.label.parentBank']}"></h:outputLabel>
															</td>
															<td>
																<h:inputText readonly="true" styleClass="READONLY"
																	id="parentBank"
																	value="#{pages$manageMasarif.masarifView.parentBankName}" />
															</td>
														</tr>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['masraf.label.parentGRPNumber']}"></h:outputLabel>
														</td>
														<td>
															<h:inputText styleClass="READONLY" readonly="true"
																id="parentGRP"
																value="#{pages$manageMasarif.masarifView.parentGrp}"></h:inputText>
														</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['thirdPartyUnit.label.description']}"></h:outputLabel>
															</td>
															<td colspan="3">
																<h:inputTextarea
																	styleClass="#{pages$manageMasarif.styleClass}" rows="8"
																	readonly="#{pages$manageMasarif.readOnly}"
																	style="width:92%;height:20%"
																	onchange="removeExtraCharacter(this,250);"
																	onblur="removeExtraCharacter(this,250);"
																	onclick="removeExtraCharacter(this,250);"
																	onkeyup="removeExtraCharacter(this,250);"
																	value="#{pages$manageMasarif.masarifView.description}"></h:inputTextarea>
															</td>
														</tr>
													</table>
													<t:div styleClass="BUTTON_TD"
														style="padding-top:10px; padding-right:21px;width:90%;">
														<h:commandButton styleClass="BUTTON"
															rendered="#{!(pages$manageMasarif.readOnly || pages$manageMasarif.popup)}"
															action="#{pages$manageMasarif.onSave}"
															value="#{msg['commons.saveButton']}" style="width: auto;">
														</h:commandButton>
														
														<h:commandButton styleClass="BUTTON"
															
															action="#{pages$manageMasarif.onOpenStatementOfAccountCriteriaPopup}"
															value="#{msg['report.heading.statementOfAccount']}" style="width: auto;">
														</h:commandButton>
														<h:commandButton styleClass="BUTTON"
															rendered="#{!pages$manageMasarif.popup}"
															action="#{pages$manageMasarif.onBack}"
															value="#{msg['commons.back']}" style="width: auto;">
														</h:commandButton>

													</t:div>
													<DIV style="height: 10px"></DIV>
													<div class="AUC_DET_PAD">
														
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel switchType="server" style="width:90%;">
																<rich:tab
																	label="#{msg['masraf.label.tab.childMasarif']}">
																	<%@ include file="childMasarifTab.jsp"%>
																</rich:tab>

																<rich:tab label="#{msg['endowmentFile.tab.Endowments']}"
																	action="#{pages$manageMasarif.onEndowmentsTab}">

																	<t:div styleClass="contentDiv"
																		style="width:99%;margin-top: 5px;">
																		<t:dataTable id="prdEndowments" rows="15" width="100%"
																			value="#{pages$manageMasarif.dataListEndowemntFileAsso}"
																			binding="#{pages$manageMasarif.dataTableFileAsso}"
																			preserveDataModel="false" preserveSort="false"
																			var="dataItem" rowClasses="row1,row2" rules="all"
																			renderedIfEmpty="true">

																			<t:column id="endNumber" sortable="true"
																				defaultSorted="true">
																				<f:facet name="header">
																					<t:outputText id="endNum"
																						value="#{msg['endowment.lbl.num']}" />
																				</f:facet>
																				<t:commandLink
																					onclick="openEndowmentManagePopup('#{dataItem.endowment.endowmentId}')"
																					value="#{dataItem.endowment.endowmentNum}"
																					style="white-space: normal;" styleClass="A_LEFT" />
																			</t:column>
																			<t:column id="endowName" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="endname"
																						value="#{msg['endowment.lbl.name']}" />
																				</f:facet>
																				<t:outputText id="namee" styleClass="A_LEFT"
																					value="#{dataItem.endowment.endowmentName}" />
																			</t:column>

																			<t:column id="endowCostCenter" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="endCostCenter"
																						value="#{msg['unit.costCenter']}" />
																				</f:facet>
																				<t:outputText id="costCentere" styleClass="A_LEFT"
																					value="#{dataItem.endowment.costCenter}" />
																			</t:column>
																			<t:column id="colenTypeName" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="endTypeName"
																						value="#{msg['commons.typeCol']}" />
																				</f:facet>
																				<t:outputText id="endTypeNamee" styleClass="A_LEFT"
																					value="#{pages$manageMasarif.englishLocale?
																													dataItem.endowment.assetType.assetTypeNameEn:
																	 												dataItem.endowment.assetType.assetTypeNameAr
																					}" />
																			</t:column>
																			<t:column id="colcategory" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="endcategory"
																						value="#{msg['endowment.lbl.category']}" />
																				</f:facet>
																				<t:outputText id="endcategorye" styleClass="A_LEFT"
																					value="#{dataItem.endowmentCategory.categoryNameAr}" />
																			</t:column>
																			<t:column id="colpurpose" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="endpurpose"
																						value="#{msg['endowment.lbl.purpose']}" />
																				</f:facet>
																				<t:outputText id="endpurposee" styleClass="A_LEFT"
																					value="#{dataItem.endowmentPurpose.purposeNameAr}" />
																			</t:column>
																			<t:column id="colManager" sortable="false">
																				<f:facet name="header">
																					<t:outputText id="endManager"
																						value="#{msg['contract.person.Manager']}" />
																				</f:facet>
																				<t:outputText id="endManagere" styleClass="A_LEFT"
																					rendered="#{dataItem.endowment.isAmafManager!= null && dataItem.endowment.isAmafManager=='1'}"
																					value="#{msg['inheritanceFile.inheritedAssetTab.managerAmaf']}" />
																				<t:commandLink
																					rendered="#{
																						dataItem.endowment.isAmafManager!= null && 
																						dataItem.endowment.isAmafManager=='0' &&
					            														dataItem.endowment.manager.personId != null 
					            														}"
																					onclick="javascript:showPersonReadOnlyPopup(#{dataItem.endowment.manager.personId });retrun "
																					value="#{dataItem.endowment.manager.fullName}"
																					style="white-space: normal;" styleClass="A_LEFT" />
																			</t:column>

																			<t:column id="colEndFileStatus" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="endFileStatus"
																						value="#{msg['commons.status']}" />
																				</f:facet>
																				<t:outputText id="endFileStatuse"
																					styleClass="A_LEFT"
																					value="#{pages$manageMasarif.englishLocale?
																														 dataItem.status.dataDescEn:
																	 													 dataItem.status.dataDescAr
																	 		  		}" />
																			</t:column>
																			<t:column id="percentageBenef" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="percentageValueBenef"
																						value="#{msg['commons.percentage']}" />
																				</f:facet>
																				<t:outputText
																					rendered="#{dataItem.isDeleted == '0' }"
																					value="#{dataItem.beneficiary.sharePercentageFileString}"
																					style="white-space: normal;" styleClass="A_LEFT" />
																			</t:column>
																			<t:column id="amountValueBenef" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="amountStringBenef"
																						value="#{msg['commons.amount']}" />
																				</f:facet>
																				<t:outputText
																					value="#{dataItem.beneficiary.amountString}"
																					rendered="#{dataItem.isDeleted == '0' }"
																					style="white-space: normal;" styleClass="A_LEFT" />
																			</t:column>
																			<t:column id="commentsBenef">
																				<f:facet name="header">
																					<t:outputText id="CommentsStringBenef"
																						value="#{msg['commons.comments']}" />
																				</f:facet>
																				<t:outputText
																					value="#{dataItem.beneficiary.comments}"
																					rendered="#{dataItem.isDeleted == '0' }"
																					style="white-space: normal;" styleClass="A_LEFT" />
																			</t:column>
																			<t:column id="revenuePeriodBenef">
																				<f:facet name="header">
																					<t:outputText id="revenuePeriodStringBenef"
																						value="#{msg['endowmentFileBen.lbl.revenuePeriod']}" />
																				</f:facet>
																				<t:outputText
																					value="#{dataItem.beneficiary.revenuePeriod}"
																					rendered="#{dataItem.isDeleted == '0' }"
																					style="white-space: normal;" styleClass="A_LEFT" />
																			</t:column>


																		</t:dataTable>
																	</t:div>
																	<t:div styleClass="contentDivFooter"
																		style="width:100.2%">
																		<t:panelGrid columns="2" border="0" width="100%"
																			cellpadding="1" cellspacing="1">
																			<t:panelGroup
																				styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																				<t:div styleClass="JUG_NUM_REC_ATT">
																					<h:outputText value="#{msg['commons.records']}" />
																					<h:outputText value=" : " />
																					<h:outputText
																						value="#{pages$manageMasarif.totalEndowments}" />
																				</t:div>
																			</t:panelGroup>
																			<t:panelGroup>
																				<t:dataScroller id="scroller" for="prdEndowments"
																					paginator="true" fastStep="1" paginatorMaxPages="5"
																					immediate="false" paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true"
																					paginatorTableStyle="grid_paginator"
																					layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																					styleClass="SCH_SCROLLER" pageIndexVar="pageNumber"
																					paginatorActiveColumnStyle="font-weight:bold;"
																					paginatorRenderLinkForActive="false">

																					<f:facet name="first">
																						<t:graphicImage url="../#{path.scroller_first}"
																							id="lblF"></t:graphicImage>
																					</f:facet>
																					<f:facet name="fastrewind">
																						<t:graphicImage
																							url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																					</f:facet>
																					<f:facet name="fastforward">
																						<t:graphicImage
																							url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																					</f:facet>
																					<f:facet name="last">
																						<t:graphicImage url="../#{path.scroller_last}"
																							id="lblL"></t:graphicImage>
																					</f:facet>
																					<t:div styleClass="PAGE_NUM_BG">

																						<table cellpadding="0" cellspacing="0">
																							<tr>
																								<td>
																									<h:outputText styleClass="PAGE_NUM"
																										value="#{msg['commons.page']}" />
																								</td>
																								<td>
																									<h:outputText styleClass="PAGE_NUM"
																										style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																										value="#{requestScope.pageNumber}" />
																								</td>
																							</tr>
																						</table>

																					</t:div>
																				</t:dataScroller>
																			</t:panelGroup>
																			</t:panelGrid>	
																			
																	</t:div>




																</rich:tab>


																<rich:tab
																	rendered="#{pages$manageMasarif.showActivitylogTab}"
																	label="#{msg['mems.inheritanceFile.tabHeadingShort.history']}"
																	action="#{pages$manageMasarif.requestHistoryTabClick}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>
															</rich:tabPanel>
														</div>

														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;width:90%;">

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$manageMasarif.popup}"
																value="#{msg['commons.closeButton']}" style="width: 10%"
																onclick="window.close();">
															</h:commandButton>

														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<c:choose>
						<c:when test="${!pages$manageMasarif.popup}">
							<tr
								style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
								<td colspan="2">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>

											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>

										</tr>
									</table>
								</td>
							</tr>
						</c:when>
					</c:choose>
				</table>
			</div>
		</body>
	</html>
</f:view>