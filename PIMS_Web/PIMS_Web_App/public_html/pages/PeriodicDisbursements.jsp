<%-- 
  - Author: Muhammad Ahmed 
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Assets
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
 
 	function resetValues()
    {
      	  
      		document.getElementById("searchFrm:amountTo").value="";
      		document.getElementById("searchFrm:amountFrom").value="";
      		document.getElementById("searchFrm:beneficiary").value="";
      		document.getElementById("searchFrm:toBeneficiary").value="";
      		document.getElementById("searchFrm:toBank").value="";
      		document.getElementById("searchFrm:toAccNum").value="";
			//document.getElementById("searchFrm:status").selectedIndex=0;
			$('searchFrm:startFrom').component.resetSelectedDate();    	  
			$('searchFrm:expectedMaturityDateFrom').component.resetSelectedDate();
			$('searchFrm:expectedMaturityDateTo').component.resetSelectedDate();
			$('searchFrm:startTo').component.resetSelectedDate();
			$('searchFrm:endFrom').component.resetSelectedDate();
			$('searchFrm:endTo').component.resetSelectedDate();
    }
 	function enableButtons( )
	{
		var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = false;
		    }
		}
		
		
	}
	function disableAddAllDisbursementActionRelatedButtons(control)
	{
		var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
		document.getElementById("searchFrm:onAddAllToDisbursementList").onclick();
		
	}
	function disableButtons(control)
	{
		var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
		
		control.nextSibling.nextSibling.onclick();
		
	}
		
	function openPopupPaymentMode()
	{
		var screen_width = 1024;
        var screen_height = 500;
        var popup_width = screen_width-200;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        window.open('paymentDetailsPopUp.jsf?pageMode=MODE_SELECT_MANY_POPUP','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');
	}
	    
	
		function populatePaymentDetails()
		{
		  document.getElementById("searchFrm:testing").onclick();
		  
		}
       	function	openPopUp()
		{
		
		    var width = 300;
            var height = 300;
            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + ",menubar=yes,toolbar=yes,scrollbars=yes,resizable=yes,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

            var child1 = window.open("about:blank", "subWind", windowFeatures);

		      child1.focus();
		}
		
		
			
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<c:choose>
				<c:when test="${!pages$PeriodicDisbursements.isViewModePopUp}">
					<div class="containerDiv">
				</c:when>
			</c:choose>


			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<c:choose>
					<c:when test="${!pages$PeriodicDisbursements.isViewModePopUp}">
						<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>
					</c:when>
				</c:choose>


				<tr width="100%">

					<c:choose>
						<c:when test="${!pages$PeriodicDisbursements.isViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>

					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['PeriodicDisbursements.header']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="searchFrm" enctype="multipart/form-data">
										<div>
											<table border="0" class="layoutTable">
												<tr>
													<td>
														<h:outputText id="errortxt"
															value="#{pages$PeriodicDisbursements.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />
														<h:outputText id="successTxt" styleClass="INFO_FONT"
															escape="false"
															value="#{pages$PeriodicDisbursements.successMessages}" />
														<h:inputHidden id="hdnContractId"
															value="#{pages$PeriodicDisbursements.disbursementListCount}" />
													</td>
												</tr>
											</table>
										</div>
										<h:commandLink id="testing"
											action="#{pages$PeriodicDisbursements.obtainChildParams}">
										</h:commandLink>

										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText escape="false" styleClass="ERROR_FONT" />
															<h:messages></h:messages>
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width: 99.8%;">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px" border="0"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">

																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['PeriodicDisbursements.disbursementList']}:"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:selectOneMenu id="disbursementList"
																				binding="#{pages$PeriodicDisbursements.disbursementTypeSelectMenu}"
																				tabindex="3"
																				onchange="javaScript:disableButtons(this);">
																				<%-- 
																				<f:selectItem
																					itemLabel="#{msg['PeriodicDisbursements.listMesssage']}"
																					itemValue="-1" />
																				--%>
																				<f:selectItems
																					value="#{pages$PeriodicDisbursements.disbursementTypeList}" />


																			</h:selectOneMenu>
																			<h:commandLink id="as"
																				action="#{pages$PeriodicDisbursements.disbursementTypeChanged}" />
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['PeriodicDisbursements.status']}:"></h:outputLabel>
																		</td>

																		<td>
																			<h:selectOneMenu id="status"
																				value="#{pages$PeriodicDisbursements.status}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.pleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$PeriodicDisbursements.periodicDisbursementStausList}" />
																			</h:selectOneMenu>

																		</td>
																	</tr>


																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['PeriodicDisbursements.fromStartDate']}:"></h:outputLabel>
																		</td>
																		<td>
																			<rich:calendar id="startFrom"
																				binding="#{pages$PeriodicDisbursements.calStartFrom}"
																				locale="#{pages$PeriodicDisbursements.locale}"
																				popup="true" datePattern="dd/MM/yyyy"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:170px; height:14px" />
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['PeriodicDisbursements.ToStartDate']}:" />
																		</td>
																		<td>

																			<rich:calendar id="startTo"
																				binding="#{pages$PeriodicDisbursements.calStartTo}"
																				locale="#{pages$PeriodicDisbursements.locale}"
																				popup="true" datePattern="dd/MM/yyyy"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:170px; height:14px" />
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['PeriodicDisbursements.fromEndDate']}:"></h:outputLabel>
																		</td>
																		<td>
																			<rich:calendar id="endFrom"
																				binding="#{pages$PeriodicDisbursements.calEndFrom}"
																				locale="#{pages$PeriodicDisbursements.locale}"
																				popup="true" datePattern="dd/MM/yyyy"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:170px; height:14px" />
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['PeriodicDisbursements.ToEndDate']}:" />
																		</td>
																		<td>

																			<rich:calendar id="endTo"
																				binding="#{pages$PeriodicDisbursements.calEndTo}"
																				locale="#{pages$PeriodicDisbursements.locale}"
																				popup="true" datePattern="dd/MM/yyyy"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:170px; height:14px" />
																		</td>
																	</tr>
																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['PeriodicDisbursements.FromAmount']}:"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText id="amountFrom"
																				onkeyup="removeNonInteger(this)"
																				onkeypress="removeNonInteger(this)"
																				onkeydown="removeNonInteger(this)"
																				onblur="removeNonInteger(this)"
																				onchange="removeNonInteger(this)"
																				value="#{pages$PeriodicDisbursements.amountFrom}" />
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['PeriodicDisbursements.ToAmount']}:"></h:outputLabel>
																		</td>

																		<td>
																			<h:inputText id="amountTo"
																				onkeyup="removeNonInteger(this)"
																				onkeypress="removeNonInteger(this)"
																				onkeydown="removeNonInteger(this)"
																				onblur="removeNonInteger(this)"
																				onchange="removeNonInteger(this)"
																				value="#{pages$PeriodicDisbursements.amountTo}" />

																		</td>
																	</tr>


																	<%--
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['PeriodicDisbursements.disbursementSource']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="disbursementSource"
																				binding="#{pages$PeriodicDisbursements.disbursementSourceSelectMenu}"
																				tabindex="3" disabled="true">

																				<f:selectItems
																					value="#{pages$PeriodicDisbursements.disbursementSourceList}" />


																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['PeriodicDisbursements.disbursementAmount']}:"></h:outputLabel>

																		</td>
																		<td>
																			<h:inputText id="amount"
																				binding="#{pages$PeriodicDisbursements.htmlAmount}"
																				readonly="true" tabindex="10">
																			</h:inputText>
																		</td>


																	</tr>
--%>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['PeriodicDisbursements.fromExpMaturityDate']}:"></h:outputLabel>
																		</td>
																		<td>
																			<rich:calendar id="expectedMaturityDateFrom"
																				binding="#{pages$PeriodicDisbursements.expectedMaturityDateFrom}"
																				locale="#{pages$PeriodicDisbursements.locale}"
																				popup="true" datePattern="dd/MM/yyyy"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:170px; height:14px" />
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.to']}:" />
																		</td>
																		<td>

																			<rich:calendar id="expectedMaturityDateTo"
																				binding="#{pages$PeriodicDisbursements.expectedMaturityDateTo}"
																				locale="#{pages$PeriodicDisbursements.locale}"
																				popup="true" datePattern="dd/MM/yyyy"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:170px; height:14px" />
																		</td>
																	</tr>

																	<tr>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.Beneficiary']}:"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText id="beneficiary"
																				binding="#{pages$PeriodicDisbursements.htmlBeneficiary}" />
																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['paymentDetailsPopUp.paidTo']}:"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText id="toBeneficiary"
																				binding="#{pages$PeriodicDisbursements.htmlToBeneficiary}" />
																		</td>
																		
																	</tr>
																	<tr>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.replaceCheque.bank']}:"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText id="toBank"
																				binding="#{pages$PeriodicDisbursements.htmlToBank}" />
																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['feeConfiguration.accountNo']}:"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText id="toAccNum"
																				binding="#{pages$PeriodicDisbursements.htmlToAccountNum}" />
																		</td>
																	</tr>
																	<tr>
																		<td class="BUTTON_TD" colspan="5">
																			<h:commandButton styleClass="BUTTON" type="submit"
																				value="#{msg['commons.search']}"
																				action="#{pages$PeriodicDisbursements.doSearch}" />

																			<h:commandButton styleClass="BUTTON" rendered="false"
																				value="#{msg['unit.addButton']}" />
																			<h:commandButton styleClass="BUTTON" type="button"
																				onclick="javascript:resetValues();"
																				value="#{msg['commons.clear']}" />
																		</td>

																	</tr>
																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>

													</table>
												</div>
											</div>
											<div class="BUTTON_TD" style="margin-right: 30px;">

												<h:commandButton id="sendToFinance"
													onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else javaScript:disableAddAllDisbursementActionRelatedButtons(this) ;"
													styleClass="BUTTON" type="submit"
													value="#{msg['PeriodicDisbursements.buttons.sendToFinance']}"
													style="width: auto" />
												<h:commandLink id="onAddAllToDisbursementList"
													action="#{pages$PeriodicDisbursements.onAddAllToDisbursementList}" />
													
													
												<h:commandButton styleClass="BUTTON" type="submit"
													onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;"
													actionListener="#{pages$PeriodicDisbursements.sendToResearchers}"
													value="#{msg['PeriodicDisbursements.buttons.sendToResearcher']}"
													style="width: auto" />

											</div>

											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px; width: 95%;">

												<div class="contentDiv" style="width: 100%">
													<t:dataTable id="test2"
														value="#{pages$PeriodicDisbursements.dataList}"
														binding="#{pages$PeriodicDisbursements.dataTable}"
														width="100%"
														rows="#{pages$PeriodicDisbursements.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true">

														<t:column id="selectMany" width="3%" sortable="false"
															style="white-space: normal;">
															<f:facet name="header">

															</f:facet>
															<h:selectBooleanCheckbox id="select"
																rendered="#{dataItem.statusDataValue=='ACTIVE' && dataItem.isSentToFinance ==0 && dataItem.isRenewalRequired ==0 && dataItem.isRenewed==0 }"
																value="#{dataItem.selected}" />
														</t:column>
														<t:column id="Status" width="5%"
															style="white-space: normal;" sortable="false">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.status']}" />
															</f:facet>
															<t:outputText
																value="#{pages$PeriodicDisbursements.isEnglishLocale?dataItem.statusEn:dataItem.statusAr}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column id="disbursementListDt" width="5%"
															defaultSorted="true" style="white-space: normal;">

															<f:facet name="header">
																<t:outputText
																	value="#{msg['PeriodicDisbursements.columns.beneficiary']}" />


															</f:facet>

															<t:commandLink
																actionListener="#{pages$PeriodicDisbursements.openManageBeneficiaryPopUp}"
																value="#{dataItem.beneficiaryName}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="birthDate" width="4%" defaultSorted="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText
																	value="#{msg['mems.inheritanceFile.fieldLabel.birthDate']}" />

															</f:facet>
															<t:div
																style="width:80%;white-space: normal;word-wrap: break-word;">
																<t:outputText value="#{dataItem.birthDate}"
																	style="white-space: normal;" styleClass="A_LEFT">
																	<f:convertDateTime
																		timeZone="#{pages$PeriodicDisbursements.timeZone}" />
																</t:outputText>


															</t:div>
														</t:column>

														<t:column id="maturityDate" width="4%" sortable="true"
															defaultSorted="true" style="white-space: normal;">

															<f:facet name="header">
																<t:commandSortHeader columnName="expMaturityDate"
																	actionListener="#{pages$PeriodicDisbursements.sort}"
																	value="#{msg['mems.inheritanceFile.fieldLabel.expectedMaturityDate']}"
																	arrow="true">
																	<f:attribute name="sortField" value="expMaturityDate" />
																</t:commandSortHeader>


															</f:facet>
															<t:div
																style="width:80%;white-space: normal;word-wrap: break-word;">

																<t:outputText value="#{dataItem.expMaturityDate}"
																	rendered="#{!dataItem.expMaturityDateInPast && !dataItem.expMaturityDateInCurrentMonth}"
																	style="white-space: normal;" styleClass="A_LEFT">
																	<f:convertDateTime
																		timeZone="#{pages$PeriodicDisbursements.timeZone}" />
																</t:outputText>
																<t:outputText value="#{dataItem.expMaturityDate }"
																	rendered="#{dataItem.expMaturityDateInPast || dataItem.expMaturityDateInCurrentMonth}"
																	style="white-space: normal;color:red;"
																	styleClass="A_LEFT">
																	<f:convertDateTime
																		timeZone="#{pages$PeriodicDisbursements.timeZone}" />
																</t:outputText>


															</t:div>
														</t:column>
														<t:column id="listTypeEn" width="10%" sortable="false"
															style="white-space: normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['notification.type']}" />
															</f:facet>
															<t:outputText
																value="#{pages$PeriodicDisbursements.isEnglishLocale ?dataItem.listTypeEn:dataItem.listTypeAr}" />
														</t:column>
														<t:column id="researcher" width="10%" sortable="false"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText
																	value="#{msg['PeriodicDisbursements.columns.researcher']}" />
															</f:facet>
															<t:outputText value="#{dataItem.researcherName}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column id="From" width="8%" sortable="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:commandSortHeader columnName="startDate"
																	actionListener="#{pages$PeriodicDisbursements.sort}"
																	value="#{msg['PeriodicDisbursements.columns.from']}"
																	arrow="true">
																	<f:attribute name="startDate" value="startDate" />
																</t:commandSortHeader>
															</f:facet>
															<t:div
																style="width:80%;white-space: normal;word-wrap: break-word;">
																<t:outputText value="#{dataItem.dateFrom}"
																	style="white-space: normal;" styleClass="A_LEFT">
																	<f:convertDateTime
																		timeZone="#{pages$PeriodicDisbursements.timeZone}" />
																</t:outputText>
															</t:div>
														</t:column>

														<t:column id="to" width="8%" sortable="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:commandSortHeader columnName="endDate"
																	actionListener="#{pages$PeriodicDisbursements.sort}"
																	value="#{msg['PeriodicDisbursements.columns.to']}"
																	arrow="true">
																	<f:attribute name="endDate" value="endDate" />
																</t:commandSortHeader>
															</f:facet>
															<t:div
																style="width:80%;white-space: normal;word-wrap: break-word;">
																<t:outputText value="#{dataItem.dateTo}"
																	style="white-space: normal;" styleClass="A_LEFT">
																	<f:convertDateTime
																		timeZone="#{pages$PeriodicDisbursements.timeZone}" />
																</t:outputText>
															</t:div>
														</t:column>

														<t:column id="Amount" width="3%" sortable="false"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText
																	value="#{msg['PeriodicDisbursements.columns.amount']}" />
															</f:facet>
															<t:inputText
																rendered="#{dataItem.statusDataValue=='ACTIVE' }"
																value="#{dataItem.amount}"
																style="width:50px;white-space: normal;" />

														</t:column>

														<t:column id="paidToName" width="10%" sortable="false"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText
																	value="#{msg['paymentDetailsPopUp.paidTo']}" />
															</f:facet>
															<t:inputText value="#{dataItem.paidToName}"
																rendered="#{dataItem.statusDataValue=='ACTIVE' }"
																style="width:80px;white-space: normal;" />
														</t:column>
														<t:column id="paidToBank" width="10%" sortable="false"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText
																	value="#{msg['addMissingMigratedPayments.fieldName.bankName']}" />

															</f:facet>

															<t:inputText value="#{dataItem.paidToBank}"
																rendered="#{dataItem.statusDataValue=='ACTIVE' }"
																style="width:80px;white-space: normal;" />
														</t:column>
														<t:column id="accountNumber" width="5%" sortable="false"
															style="white-space: normal;word-wrap: hard;">

															<f:facet name="header">

																<h:outputText
																	value="#{msg['feeConfiguration.accountNo']}" />

															</f:facet>
															<t:div style="white-space: normal;word-wrap: break-word;">
																<t:inputText value="#{dataItem.accountNumber}"
																	maxlength="23"
																	rendered="#{dataItem.statusDataValue=='ACTIVE' }"
																	style="width:70px;white-space: normal;word-wrap: break-word;" />

															</t:div>
														</t:column>
														<t:column id="comments" width="10%" sortable="false"
															style="white-space: normal;word-wrap: hard;">

															<f:facet name="header">

																<h:outputText
																	value="#{msg['commons.comments']}" />

															</f:facet>
															<t:div style="white-space: normal;word-wrap: break-word;">
																<t:inputTextarea value="#{dataItem.comments}"
																	
																	rendered="#{dataItem.statusDataValue=='ACTIVE' }"
																	style="width:70px;white-space: normal;word-wrap: break-word;" />

															</t:div>
														</t:column>
														

														<t:column id="action" width="3%"
															style="white-space: normal;" sortable="false">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>

															<t:commandLink
																rendered="#{dataItem.isRenewalRequired ==0 && dataItem.isRenewed==0 && 
																             dataItem.statusDataValue=='ACTIVE' &&
																             dataItem.hasNewDisbursement==0
																           }"
																action="#{pages$PeriodicDisbursements.onAddToDisbursementList}">
																<h:graphicImage id="disbListIcon"
																	title="#{msg['periodicDisbursement.addToDisbursementList']}"
																	url="../resources/images/app_icons/Add-Paid-Facility.png" />
															</t:commandLink>
															<t:outputText
																rendered="#{dataItem.hasNewDisbursement==1}"
																value="#{dataItem.newDisbursementRequestNum}" />
														</t:column>
													</t:dataTable>

												</div>

												<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:100%;#width:100%;">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText id="recordSize"
																					value="#{pages$PeriodicDisbursements.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">

																<t:div styleClass="PAGE_NUM_BG" style="#width:20%">
																	<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																			</td>
																			<td>
																				<h:outputText styleClass="PAGE_NUM" id="currentPage"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{pages$PeriodicDisbursements.currentPage}" />
																			</td>
																		</tr>
																	</table>
																</t:div>
																<TABLE border="0" class="SCH_SCROLLER">
																	<tr>
																		<td>
																			<t:commandLink
																				action="#{pages$PeriodicDisbursements.pageFirst}"
																				disabled="#{pages$PeriodicDisbursements.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$PeriodicDisbursements.pagePrevious}"
																				disabled="#{pages$PeriodicDisbursements.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFR"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:dataList
																				value="#{pages$PeriodicDisbursements.pages}"
																				var="page">
																				<h:commandLink value="#{page}"
																					actionListener="#{pages$PeriodicDisbursements.page}"
																					rendered="#{page != pages$PeriodicDisbursements.currentPage}" />
																				<h:outputText value="<b>#{page}</b>" escape="false"
																					rendered="#{page == pages$PeriodicDisbursements.currentPage}" />
																			</t:dataList>
																		</td>
																		<td>
																			<t:commandLink id="pageNext"
																				action="#{pages$PeriodicDisbursements.pageNext}"
																				disabled="#{pages$PeriodicDisbursements.firstRow + pages$PeriodicDisbursements.rowsPerPage >= pages$PeriodicDisbursements.totalRows}">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink id="pageLast"
																				action="#{pages$PeriodicDisbursements.pageLast}"
																				disabled="#{pages$PeriodicDisbursements.firstRow + pages$PeriodicDisbursements.rowsPerPage >= pages$PeriodicDisbursements.totalRows}">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblL"></t:graphicImage>
																			</t:commandLink>
																		</td>
																	</tr>

																</TABLE>

															</td>
														</tr>

													</table>


												</t:div>
												<t:div styleClass="TAB_DETAIL_SECTION" style="width:99%;">
													<t:panelGrid id="summary" cellpadding="1px" width="100%"
														cellspacing="3px" styleClass="TAB_DETAIL_SECTION_INNER"
														columns="4"
														columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">

														<t:outputLabel styleClass="A_LEFT"
															style="width:75%;;font-weight:bold;"
															value="#{msg['periodicDisbursement.disbursementListCount']} :" />


														<t:commandLink styleClass="A_LEFT" id="disListTypeCount"
															style="width:10%;font-weight:bold;"
															disabled="#{pages$PeriodicDisbursements.disbursementListCount==0}"
															value="#{pages$PeriodicDisbursements.disbursementListCount}"
															action="#{pages$PeriodicDisbursements.onNavigateToDisbursementListScreen}" />




													</t:panelGrid>
												</t:div>

											
											</div>


										</div>


										</div>


									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
					<td colspan="2">
						<c:choose>
							<c:when test="${!pages$PeriodicDisbursements.isViewModePopUp}">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</tr>
								</table>
							</c:when>
						</c:choose>
					</td>
				</tr>
			</table>
			<c:choose>
				<c:when test="${!pages$PeriodicDisbursements.isViewModePopUp}">
					</div>
				</c:when>
			</c:choose>

		</body>
	</html>
</f:view>