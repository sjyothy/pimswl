<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%-- 
  - Author: Imran Ali
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used to show income category threshold list
  --%>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">



			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<br/>
		<t:div styleClass="contentDiv" style="width:95%;">
			<t:dataTable  rows="200"
				value="#{pages$incomeThresholdPopup.incomeCategoryThresholdList}"
				preserveDataModel="false" preserveSort="false" var="dataItem"
				rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
				width="100%">
			
				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['ResearchFormBenef.incomeCategory']}" />
					</f:facet>
					<h:outputText value="#{dataItem.dataDesc}" />
				</t:column>
				
				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['researchFrom.columnLabel.incomeCategoryThreshold.rangeTo']}" />
					</f:facet>
					<h:outputText value="#{dataItem.thresholdValueTo}" />
				</t:column>
				
				
				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['researchFrom.columnLabel.incomeCategoryThreshold.rangeFrom']}" />
					</f:facet>
					<h:outputText value="#{dataItem.thresholdValueFrom}" />
				</t:column>
				
			</t:dataTable>
			
		</t:div>
		<br/>
		<t:div  style="width:95%;text-align:center">
		<t:commandButton styleClass="BUTTON" 
													value="#{msg['commons.closeButton']}"
													onclick="window.close();">
												</t:commandButton>
												</t:div>
		</body>
	</html>
</f:view>