<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html style="overflow:hidden;" dir="${sessionScope.CurrentLocale.dir}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache" />
			<meta http-equiv="cache-control" content="no-cache" />
			<meta http-equiv="expires" content="0" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table_ff}"/>" type="text/css" rel="stylesheet" />
			<![endif]
			-->
		</head>
		<SCRIPT type="text/javascript">
		function testing()
		{
		 alert("tedting");
		}
		function renderControls()
		{
			document.getElementById('picklist:dataArID').style.visibility='hidden';
			document.getElementById('picklist:dataEnID').style.visibility='hidden';
			document.getElementById('picklist:otadmindataen').style.visibility='hidden';
			document.getElementById('picklist:otadmindataar').style.visibility='hidden';
			document.getElementById('picklist:contentDivFooter').style.visibility='visible';
			document.getElementById('picklist:contentDivFooterTD').style.visibility='visible';
		}
		</SCRIPT>

		<body class="BODY_STYLE"
			lang="${sessionScope.CurrentLocale.languageCode}">
          <div id="containerDivId" class="containerDiv">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table  class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel id="otPicklistHeader"
										value="#{msg['violation.violationCategorySearch']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>

						<table id="tbl1213212" width="99%" 
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" >

							<tr valign="top">
								
								<!--Use this below only for desiging forms Please do not touch other sections -->


								<td width="99%" height="100%" valign="top" nowrap="nowrap">
									<div id="divwww1213212" class="SCROLLABLE_SECTION" style="height:450px" >
										<h:form id="picklist" style="width: 98%">
												<table border="0" class="layoutTable">
													<tr>
														<td id="msgTD">
																<h:outputText
																	value="#{pages$ViolationCategorySearch.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" id="errorMessages" />
																<h:outputText
																	value="#{pages$ViolationCategorySearch.successMessages}"
																	escape="false" styleClass="INFO_FONT" id="successMessages" />	
															</td>
													</tr>
												</table>
											<div id="divMargin" class="MARGIN">
												<table id="tblheadermid" cellpadding="0" cellspacing="0" width="100%">
													<tr style="FONT-SIZE: 0px">
														<td style="FONT-SIZE: 0px">
															<IMG id="imgleft" class="TAB_PANEL_LEFT" 
																src="../<h:outputText value="#{path.img_section_left}"/>"
																/>
														</td>
														<td width="100%" style="FONT-SIZE: 0px" >
															<IMG id="imgmid" class="TAB_PANEL_MID" 
																src="../<h:outputText  value="#{path.img_section_mid}"/>"
																/>
														</td>
														<td style="FONT-SIZE: 0px"> 
															<IMG id="imgright" class="TAB_PANEL_RIGHT" 
																src="../<h:outputText value="#{path.img_section_right}" />"
																 />
														</td>
													</tr>
												</table>
												<t:div id="divDetailSection2" styleClass="DETAIL_SECTION">
												
												
													<h:outputLabel id="outLabelHeader"
														value="#{msg['violation.violationCategory']}"
														styleClass="DETAIL_SECTION_LABEL">
													</h:outputLabel>
													<t:div id="div11123wweeeeee">

														<t:div id="addPanelId">

															<table id="tbl1234" cellpadding="1px" cellspacing="2px"
																width="100%" class="DETAIL_SECTION_INNER">
																<tr>
																	<td width="5%">&nbsp;</td>
																	<td width="20%">
																		<h:outputText id="otadminheader"
																			value="#{msg['violation.violationCategory']}" />
																	</td>
																	<td width="20%">
																		<h:selectOneMenu id="violationCategorySelectOneMenu"
																			valueChangeListener="#{pages$ViolationCategorySearch.checkTheSelectedCategory}"
																			onchange="submit();"
																			value="#{pages$ViolationCategorySearch.selectedCategory}">
																			<f:selectItem id="selectNoneType" itemValue="-1"
																				itemLabel="#{msg['commons.none']}" />
																			<f:selectItems
																				value="#{pages$ApplicationBean.violationCategoryList}" />
																		</h:selectOneMenu>

																	</td>
																</tr>																
																<tr id="row">
																	<td width="5%">&nbsp;</td>
																	<td width="20%">
																		<h:outputLabel id="otadmindataen"
																			binding="#{pages$ViolationCategorySearch.lblDescEn}"
																			value="#{msg['admin.picklist.dataen']}" />
																	</td>
																	<td width="20%">
																		<h:inputText id="dataEnID" maxlength="50"
																			binding="#{pages$ViolationCategorySearch.txtDescEn}"
																			value="#{pages$ViolationCategorySearch.descriptionEn}"
																			style="INPUT"></h:inputText>
																	</td>
																	<td width="10%">&nbsp;</td>
																	<td width="20%">
																		<h:outputLabel id="otadmindataar"
																			binding="#{pages$ViolationCategorySearch.lblDescAr}"
																			value="#{msg['admin.picklist.dataar']}"/>
																	</td>
																	<td width="20%">
																		<h:inputText id="dataArID" maxlength="50"
																			binding="#{pages$ViolationCategorySearch.txtDescAr}"
																			value="#{pages$ViolationCategorySearch.descriptionAr}"
																			style="INPUT"/>
																	</td>
																	<td width="5%">&nbsp;</td>
																</tr>
																<tr>
																	<td colspan="7" class="BUTTON_TD JUG_BUTTON_TD">
																			<h:commandButton id="btnAddType" styleClass="BUTTON"
																				value="#{msg['commons.Add']}"
																				binding="#{pages$ViolationCategorySearch.btnAdd}"
																				action ="#{pages$ViolationCategorySearch.addViolationCategory}"
																				style="width: 75px" >
																			</h:commandButton>
																			<h:commandButton id="btnSaveType" styleClass="BUTTON"
																				value="#{msg['commons.saveButton']}"
																				binding="#{pages$ViolationCategorySearch.btnSave}"
																				action="#{pages$ViolationCategorySearch.saveViolationType}"
																				style="width: 75px" tabindex="7">
																			</h:commandButton>
																	</td>
																</tr>
															</table>

														</t:div>
													</t:div>

												</t:div>
												<t:div id="picklistDataDiv"
													style="padding-bottom:7px;padding-left:0px;padding-right:0px;padding-top:7px;">

													<div class="imag">
														&nbsp;
													</div>

													<t:div id="picklistDataTablediv1" styleClass="contentDiv">
														<t:dataTable id="violationDataTable"
															binding="#{pages$ViolationCategorySearch.dataTable}"
															var="violationTypeDataItem"
															value="#{pages$ViolationCategorySearch.violationTypeForCategory}"
															style="width: 100%;"
															preserveDataModel="false"
															preserveSort="false"
															rules="all"
															renderedIfEmpty="true"
															rows="#{pages$ViolationCategorySearch.paginatorRows}"
															rowClasses="row1, row2">
															<t:column id="dataPrimaryName"
																style="padding: 0px;border: 0px;text-align: center;">
																<f:facet name="header">
																	<h:outputText id="ot1213skadj"
																		value="#{msg['admin.picklist.dataen']}"
																		styleClass="grid_column_header_font"
																		style="font-size: 11px" />
																</f:facet>

																<h:outputText id="plistPrimary" style="white-space: normal;"
																	value="#{violationTypeDataItem.descriptionEn}"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="dataSecondaryName"
																style="padding: 0px;border: 0px;text-align: center;">
																<f:facet name="header">
																	<h:outputText value="#{msg['admin.picklist.dataar']}" />
																</f:facet>
																<h:outputText id="plistSecondary" style="white-space: normal;"
																	value="#{violationTypeDataItem.descriptionAr}"
																	styleClass="A_LEFT" />
															</t:column>
															
															<t:column id="deleteIdCol111"
																style="padding: 0px;border: 0px;text-align: center;">
																<f:facet name="header">
																	<h:outputText id="ot1213skadjsss"
																		value="#{msg['commons.action']}"
																		styleClass="grid_column_header_font"
																		style="font-size: 11px" />
																</f:facet>
																<h:commandLink id="editrecordId"
																	action="#{pages$ViolationCategorySearch.editViolationType}">
																	<h:graphicImage id="gilskdjlkj323423"
																		title="#{msg['commons.edit']}"
																		url="../resources/images/edit-icon.gif" />
																</h:commandLink>
															</t:column>
														</t:dataTable>
													</t:div>
													
													<t:div  styleClass="contentDivFooter" style="width:100%">
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td id="contentDivFooterTD" class="RECORD_NUM_TD">
													<div id="contentDivFooter" class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$ViolationCategorySearch.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
												<CENTER>
											<t:dataScroller id="scroller" for="violationDataTable" paginator="true"
												fastStep="1" paginatorMaxPages="#{pages$ViolationCategorySearch.paginatorMaxPages}" immediate="false"
												paginatorTableClass="paginator"
												
												renderFacetsIfSinglePage="true" 
												paginatorTableStyle="grid_paginator" layout="singleTable" 
												paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
												styleClass="SCH_SCROLLER"
												pageIndexVar="pageNumber"
												paginatorActiveColumnStyle="font-weight:bold;"
												paginatorRenderLinkForActive="false">

												<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
												<t:div styleClass="PAGE_NUM_BG">
												
													<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>		
											</t:dataScroller>
											</CENTER>
													
												</td>
												</tr>
											</table>
										</t:div>  
													
													
												</t:div>
											</div>
										</h:form>
									</div>
								</td>

							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>

	</html>
</f:view>