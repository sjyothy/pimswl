						<t:panelGrid columns="4"  width="100%" rendered = "#{pages$ScopeOfWork.canAddTenderScopeOfWork}">
						            <h:panelGroup>
									<h:outputLabel styleClass="mandatory" value="*"/><t:outputLabel styleClass="LABEL" value="#{msg['scopeOfWork.workType']}:" style="padding-right:5px;padding-left:5px;margin-right:10px;" />
								</h:panelGroup>
								<h:selectOneMenu id="workTypeMenu"
									binding="#{pages$ScopeOfWork.workTypeMenu}">
									<f:selectItem itemValue="-1"
										itemLabel="#{msg['commons.combo.PleaseSelect']}" />
									<f:selectItems value="#{pages$ScopeOfWork.workTypeItems}" />
								</h:selectOneMenu>
						        <h:panelGroup>
								  <h:outputLabel styleClass="mandatory" value="*"/><t:outputLabel styleClass="LABEL" value="#{msg['scopeOfWork.workDescription']}: " style="padding-right:5px;padding-left:5px;margin-right:10px;"/>
								</h:panelGroup>
								<h:inputTextarea id="scopeWorkDescription" binding="#{pages$ScopeOfWork.scopeWorkDescription}" styleClass="TEXTAREA" />
							           </t:panelGrid>	
						<t:panelGrid id="scopeOfWorkButtonGrid" width="100%"
							         columnClasses="BUTTON_TD" columns="1">
									<h:commandButton id="saveScopeOfWork" styleClass="BUTTON"
									rendered = "#{pages$ScopeOfWork.canAddTenderScopeOfWork}"
									binding = "#{pages$ScopeOfWork.saveScopeOfWork}"
									action="#{pages$ScopeOfWork.saveScopeOfWork_action}"
									value="#{msg['commons.Add']}">
									</h:commandButton>
						
						</t:panelGrid>
						
								
										 	
						<t:div styleClass="contentDiv" style="width:98.6%;">
							<t:dataTable id="scopeOfWorkDataTable"
							renderedIfEmpty="true" width="100%" cellpadding="0" cellspacing="0" 
								binding="#{pages$ScopeOfWork.scopeOfWorkDataTable}"
							value="#{pages$ScopeOfWork.scopeOfWorkList}" var="tenderWorkScopeView"
						preserveDataModel="false" preserveSort="false" rowClasses="row1,row2" rules="all"
						 rows="#{pages$ScopeOfWork.paginatorRows}">
							<t:column>
								<f:facet name="header">
									<t:outputText value="#{msg['scopeOfWork.workType']}" />
								</f:facet>
								<t:outputText
									value="#{tenderWorkScopeView.workTypeView.descriptionEn}"
									styleClass="A_LEFT" rendered = "#{tenderWorkScopeView.isDeleted == 0}" />
							</t:column>
							<t:column>
								<f:facet name="header">
									<t:outputText value="#{msg['scopeOfWork.workDescription']}" />
								</f:facet>
								<t:outputText value="#{tenderWorkScopeView.description}"
									styleClass="A_LEFT" rendered = "#{tenderWorkScopeView.isDeleted == 0}"/>
							</t:column>
							<t:column rendered="#{pages$ScopeOfWork.canDeleteTenderScopeOfWork}">
								<f:facet name="header">
									<t:outputText value="#{msg['commons.action']}" />
								</f:facet>
								<h:commandLink id="deleteLink"  rendered = "#{tenderWorkScopeView.isDeleted == 0}"
									onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;"
									action="#{pages$ScopeOfWork.deleteLink_action}" >
									<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
										url="../resources/images/delete_icon.png"  binding="#{pages$ScopeOfWork.imgdeleteLink}"/>
									</h:commandLink>
								</t:column>
							</t:dataTable>
						</t:div>
						<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99.6%;">
							<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
								cellspacing="1">
								<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
									<t:div styleClass="JUG_NUM_REC_ATT">
										<h:outputText value="#{msg['commons.records']}" />
									<h:outputText value=" : " />
									<h:outputText value="#{pages$ScopeOfWork.recordSize}" />
								</t:div>
							</t:panelGroup>
							<t:panelGroup>
								<t:dataScroller id="scopeOfWorkScroller" for="scopeOfWorkDataTable"
									paginator="true" fastStep="1" paginatorMaxPages="#{pages$ScopeOfWork.paginatorMaxPages}"
									immediate="false" paginatorTableClass="paginator"
									renderFacetsIfSinglePage="true" paginatorTableStyle="grid_paginator"
									layout="singleTable"
									paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
									paginatorActiveColumnStyle="font-weight:bold;"
									paginatorRenderLinkForActive="false" pageIndexVar="pageNumber"
									styleClass="JUG_SCROLLER">
									<f:facet name="first">
										<t:graphicImage url="../#{path.scroller_first}"
											id="lblFScopeOfWork"></t:graphicImage>
									</f:facet>
									<f:facet name="fastrewind">
										<t:graphicImage url="../#{path.scroller_fastRewind}"
											id="lblFRScopeOfWork"></t:graphicImage>
									</f:facet>
									<f:facet name="fastforward">
										<t:graphicImage url="../#{path.scroller_fastForward}"
											id="lblFFScopeOfWork"></t:graphicImage>
									</f:facet>
									<f:facet name="last">
										<t:graphicImage url="../#{path.scroller_last}" id="lblLScopeOfWork"></t:graphicImage>
									</f:facet>
									<t:div styleClass="PAGE_NUM_BG" rendered="true">
										<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}" />
										<h:outputText styleClass="PAGE_NUM"
											style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
											value="#{requestScope.pageNumber}" />
										</t:div>
									</t:dataScroller>
								</t:panelGroup>
							</t:panelGrid>
						</t:div>
						
						
