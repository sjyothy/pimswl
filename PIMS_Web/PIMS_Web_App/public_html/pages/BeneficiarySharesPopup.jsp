
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
		
	 function calculateRemainingShare( enteredValue ){
	
	
	 var arrEnteredValueIdSplit = enteredValue.id.split("enteredValue");
	 var hdnSharedValue =parseFloat( document.getElementById( "hdnSharedValue" +arrEnteredValueIdSplit[1]).value);
	 var currentShareValue = parseFloat(enteredValue.value);
	 var remaining = parseFloat( document.getElementById( "remaining" +arrEnteredValueIdSplit[1]).innerText);
	 if( currentShareValue =="NaN" || currentShareValue < 0)
	 {
	  enteredValue.value = hdnSharedValue;
	 }
	 else if( hdnSharedValue != currentShareValue )
	 {
	   remainingShares = parseFloat(remaining) + parseFloat(hdnSharedValue) - parseFloat(currentShareValue);
	   if(remainingShares >= 0 )
	   {
	     document.getElementById( "remaining" +arrEnteredValueIdSplit[1]).innerText =remainingShares ;
	     document.getElementById( "hdnSharedValue" +arrEnteredValueIdSplit[1]).value = currentShareValue;
	   }
	   else
	   {  enteredValue.value = hdnSharedValue;}
	   
	   
	    
	 }
	
	}              
		function closeWindowSubmit()
	{
		window.opener.populateBeneficiarySharesDetails();
	  	window.close();	  
	}	
	function closeWindow()
		{
		  window.close();
		}
	function renderDoneButton()
	{
	
	 if( document.getElementById('frm:selectBeneficiary').value > -1 )
	 {
	 document.getElementById('frm:btnDone').style.visibility='visible';
	 }
	 else
	 {
	 document.getElementById('frm:btnDone').style.visibility='hidden';
	 }
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE" onload="javaScript:renderDoneButton();">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['commons.BeneficiarySharesPopup']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="frm" enctype="multipart/form-data">
										<t:div id ="layoutDiv">
											<t:panelGrid columns="1" id="layoutTables" border="0" styleClass="layoutTable">
												
														<h:outputText id="errorText"
															value="#{pages$BeneficiarySharesPopup.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />
														<h:outputText id="successText"
															value="#{pages$BeneficiarySharesPopup.successMessages}"
															escape="false" styleClass="INFO_FONT" />

												
											</t:panelGrid>
										</t:div>
										<t:div id ="scrollableDiv" styleClass="SCROLLABLE_SECTION">

											<div id="margin" class="MARGIN" style="width: 95%;">

												<div  class="DETAIL_SECTION" style="width: 99.8%;">
													<h:outputLabel
														value="#{msg['commons.BeneficiarySharesPopup']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px" border="0"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">

																	<tr>

																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['benef.asset.shares.benef']}:">
																			</h:outputLabel>


																		</td>
																		<td>
																			<h:selectOneMenu id="selectBeneficiary"
																				binding="#{pages$BeneficiarySharesPopup.cmbBnfList}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$BeneficiarySharesPopup.beneficiaryList}" />

																				<a4j:support id="as"
																					action="#{pages$BeneficiarySharesPopup.onBeneficiaryChanged}"
																					event="onchange" 
																					onbeforedomupdate="renderDoneButton();"
																					reRender="txtShariaPercent,parentDivs, errorText,successText 
																					          beneficiaryAssetsGrid,layoutDiv,layoutTables" />

																			</h:selectOneMenu>

																		</td>
																	</tr>
																	<tr>

																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['distribution.shariaPercent']}:">
																			</h:outputLabel>


																		</td>
																		<td>
																			<h:inputText id="txtShariaPercent"
																				styleClass="READONLY" readonly="true"
																				binding="#{pages$BeneficiarySharesPopup.txtShariaPercent}" />

																		</td>
																	</tr>


																</table>
															</td>
														</tr>
													</table>
												</div>
												<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
												<div id= "buttonDiv" class="BUTTON_TD" style="margin-top:10px;">
													<h:commandButton id="btnDone" styleClass="BUTTON" 
													    
														value="#{msg['commons.done']}"
														action="#{pages$BeneficiarySharesPopup.onDone}"
														
														style="width: auto;visibility:hidden;" />


													<h:commandButton styleClass="BUTTON"
														value="#{msg['generateFloors.Close']}"
														onclick="javascript:window.close();" style="width: auto">
													</h:commandButton>
												</div>

												<t:div styleClass="contentDiv" id="parentDivs" 
													style="width:95%;margin-top:10px;">
													<t:dataTable id="beneficiaryAssetsGrid" rows="300"
														binding="#{pages$BeneficiarySharesPopup.dataTable}"
														value="#{pages$BeneficiarySharesPopup.list}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%"
														rowId="#{dataItem.myHashCode}"
														>

														<t:column sortable="true">
															<f:facet name="header">
																<h:outputText value="#{msg['assetManage.assetNumber']}" />
															</f:facet>
															<h:outputText value="#{dataItem.inheritedAssetNumber}" />
														</t:column>

														<t:column sortable="false">
															<f:facet name="header">
																<h:outputText value="#{msg['searchAssets.assetNameEn']}" />
															</f:facet>
															<h:outputText value="#{dataItem.inheritedAssetNameEn}" />
														</t:column>
														<t:column sortable="false">
															<f:facet name="header">
																<h:outputText value="#{msg['searchAssets.assetNameAr']}" />
															</f:facet>
															<h:outputText value="#{dataItem.inheritedAssetNameAr}" />
														</t:column>

														<t:column sortable="false">
															<f:facet name="header">
																<h:outputText value="#{msg['searchAssets.assetType']}" />
															</f:facet>
															<h:outputText
																value="#{pages$BeneficiarySharesPopup.englishLocale?dataItem.inheritedAssetTypeEn:dataItem.inheritedAssetTypeAr}" />
														</t:column>
														<t:column sortable="false" >
															<f:facet name="header">
																<h:outputText
																	value="#{msg['inheritanceFile.label.totalAssetShare']}" />
															</f:facet>
															<t:outputText forceId="true" id="totalShares" value="#{dataItem.totalShare}" />
														</t:column>
														<t:column sortable="false" >
															<f:facet name="header">
																<h:outputText
																	value="#{msg['beneficiaryShares.Remaining']}" />
															</f:facet>
															<t:outputText forceId="true" id="remaining" value="#{dataItem.remainingShare}" >
																<f:convertNumber maxIntegerDigits="15"
																	maxFractionDigits="2" pattern="##############0.00" />
															</t:outputText>
														</t:column>
														
														<t:column id="shareValue"  sortable="false" >
															<f:facet name="header">
															    
																<h:outputText
																	value="#{msg['inheritanceFile.label.beneShareValue']}" />
															</f:facet>
															<t:inputHidden forceId="true" id="hdnSharedValue" value="#{dataItem.oldPctWeight}"/>
															<t:inputText forceId="true" id="enteredValue" value="#{dataItem.pctWeight}"
															             onkeyup="javaScript:calculateRemainingShare(this);" />
														</t:column>
														<t:column id="selectTrans" width="5%">
															<f:facet name="header">
																<h:selectBooleanCheckbox id="markUnMarkAll" value="#{pages$BeneficiarySharesPopup.markUnMarkAll}">
																	<a4j:support id="as"
																					action="#{pages$BeneficiarySharesPopup.onCheckChanged}"
																					event="onclick"
																					reRender="parentDivs,beneficiaryAssetsGrid,layoutTable" />
																</h:selectBooleanCheckbox>

															</f:facet>

															<h:selectBooleanCheckbox id="select"
																value="#{dataItem.selectWill}"  />
														</t:column>

													</t:dataTable>
												</t:div>
											</div>
										</t:div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

		</body>
	</html>
</f:view>