<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
	<t:div style="height:320px;overflow-y:scroll;">
																		<t:panelGrid width="97%" id="rerend"
																			 columns="4">

																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.facility']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:selectOneMenu id="facilityMenu"
																				
																				value="#{pages$ReceiveProperty.facilityId}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ReceiveProperty.facilityList}" />
																				<a4j:support event="onchange"
																					action="#{pages$ReceiveProperty.loadFacilityCharges}"
																					reRender="rerend" />
																			</h:selectOneMenu>


																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.facilityType']}:"></h:outputLabel>
																			</t:panelGroup>

																			<h:selectOneMenu id="facilityTypeMenu"
																				
																				value="#{pages$ReceiveProperty.facilityTypeId}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ReceiveProperty.facilityTypeList}" />
																				<a4j:support event="onchange"
																					action="#{pages$ReceiveProperty.updateFacilityCharges}"
																					reRender="rerend" />
																			</h:selectOneMenu>


																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.charges']}:"></h:outputLabel>
																			</t:panelGroup>

																			<h:inputText id="charges" styleClass="A_RIGHT_NUM"
																				binding="#{pages$ReceiveProperty.charges}" />
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.description']}:"></h:outputLabel>


																			<h:inputText id="FacilityDescription"
																				
																				binding="#{pages$ReceiveProperty.facilityDescription}" />


																		</t:panelGrid>
																		<t:panelGrid id="facilitybuttongrid" width="97%"
																			columnClasses="BUTTON_TD" columns="1">
																			<h:commandButton styleClass="BUTTON"
																				binding="#{pages$ReceiveProperty.saveFacilityButton}"
																				value="#{msg['commons.Add']}"
																				action="#{pages$ReceiveProperty.saveFacility}" />

																		</t:panelGrid>










																		<t:div style="text-align:center;">
																			<t:div styleClass="contentDiv" style="width:95%">
																				<t:dataTable id="facilitiesdatagrid" rows="50"
																					width="100%"
																					value="#{pages$ReceiveProperty.facilityDataList}"
																					binding="#{pages$ReceiveProperty.facilityGrid}"
																					preserveDataModel="false" preserveSort="false"
																					var="facilityDataItem" rowClasses="row1,row2"
																					rules="all" renderedIfEmpty="true">
																					<t:column id="col_PersonName" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.facilityName']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{facilityDataItem.facilityName}"
																							rendered="#{pages$ReceiveProperty.isEnglishLocale}" />
																						<t:outputText title=""
																							rendered="#{pages$ReceiveProperty.isArabicLocale}"
																							value="#{facilityDataItem.facilityNameAr}" />
																					</t:column>
																					<t:column id="col_rent">
																						<f:facet name="header">
																							<t:outputText id="lbl_rent"
																								value="#{msg['receiveProperty.rentFacility']}" />

																						</f:facet>
																						<t:outputText id="txt_rent" title=""
																							value="#{facilityDataItem.rent}" />
																					</t:column>
																					<t:column id="col_fac_type">
																						<f:facet name="header">
																							<t:outputText id="lbl_fac_type"
																								value="#{msg['receiveProperty.facilityType']}"
																								rendered="#{pages$ReceiveProperty.isEnglishLocale}" />


																						</f:facet>
																						<t:outputText id="txt_fac_type" title=""
																							rendered="#{pages$ReceiveProperty.isEnglishLocale}"
																							value="#{facilityDataItem.facilityTypeEn}" />
																						<t:outputText id="txt_fac_type_Ar" title=""
																							rendered="#{pages$ReceiveProperty.isArabicLocale}"
																							value="#{facilityDataItem.facilityTypeAr}" />
																					</t:column>
																					<t:column id="col_fac_Desc">
																						<f:facet name="header">
																							<t:outputText id="lbl_fac_Desc"
																								value="#{msg['receiveProperty.description']}" />

																						</f:facet>
																						<t:outputText id="txt_fac_Desc" title=""
																							value="#{facilityDataItem.description}" />
																					</t:column>
																					<t:column
																						rendered="#{pages$ReceiveProperty.deleteAction}">
																						<f:facet name="header">
																							<t:outputText id="lbl_delete"
																								value="#{msg['commons.delete']}" />
																						</f:facet>
																						<t:commandLink id="facilityDeleteIcon"
																							action="#{pages$ReceiveProperty.cmdFacilityDelete_Click}">
																							<h:graphicImage id="delete_Icon"
																								title="#{msg['commons.delete']}"
																								url="../resources/images/delete_icon.png" />
																						</t:commandLink>
																					</t:column>

																				</t:dataTable>
																			</t:div>

																		</t:div>
																	</t:div>