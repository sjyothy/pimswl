<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Selecting an Auction Venue to an Auction
  --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	    
    function resetValues()
     	{
     	document.getElementById("venueSearchFrm:venueName").value="";
      	document.getElementById("venueSearchFrm:country").selectedIndex=0;
	    document.getElementById("venueSearchFrm:state").selectedIndex=0;
	    document.getElementById("venueSearchFrm:postalCode").value="";
		document.getElementById("venueSearchFrm:street").value="";
		document.getElementById("venueSearchFrm:add1").value="";
		document.getElementById("venueSearchFrm:add2").value="";
       }
       
       function submitForm()
	   {
          document.getElementById('venueSearchFrm').submit();
          document.getElementById("venueSearchFrm:state").selectedIndex=0;
	   }

</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>

		<body class="BODY_STYLE">
			<div class="containerDiv">
				<%
					response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
						response.setHeader("Pragma", "no-cache"); //HTTP 1.0
						response.setDateHeader("Expires", 0); //prevents caching at the proxy server
				%>

				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody"
							height="470px">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['auctionVenueList.header']}"
											styleClass="HEADER_FONT" />
									</td>
									
								</tr>
							</table>

							<table width="99%" style="margin-left: 1px;" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" width="100%">
										<div class="SCROLLABLE_SECTION AUC_SCH_SS"
											style="height: 95%; width: 100%; # height: 95%; # width: 100%;">

											<h:form id="venueSearchFrm"
												style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
						                                        	<div class="MESSAGE">
													<table border="0" class="layoutTable">
														<tr>
															<td>
																<h:outputText
																	value="#{pages$auctionVenue.errorMessages}"
																	escape="false" styleClass="ERROR_FONT"
																	style="padding:10px;" />
															</td>
														</tr>
													</table>
												</div>
                                                                                                 <div class="MARGIN">
                                                                                                      <table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
                                                                                                        <div class="DETAIL_SECTION">
														<h:outputLabel value="#{msg['commons.searchCriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER">
															<tr>
                                                                                                                               <td width="97%">
                                                                                                                                  <table width="100%">
                                                                                                                                               <tr>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['venue.name']}:"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="venueName"
																					value="#{pages$auctionVenue.venue.venueName}"
																					maxlength="50" tabindex="1"></h:inputText>
																			</td>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contact.country']}:"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:selectOneMenu id="country" tabindex="2"
																					value="#{pages$auctionVenue.venue.location.countryIdString}"
																					onchange="javascript:submitForm();"
																					valueChangeListener="#{pages$auctionVenue.loadStates}">
																					<f:selectItem itemValue="0"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$auctionVenue.countries}" />
																				</h:selectOneMenu>
																			</td>


																		</tr>
<tr>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contact.state']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu styleClass="SELECT_MENU" id="state"
																					tabindex="3"
																					value="#{pages$auctionVenue.venue.location.stateIdString}">
																					<f:selectItem itemValue="0"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems value="#{pages$auctionVenue.states}" />
																				</h:selectOneMenu>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contact.street']}:"></h:outputLabel>
																			</td>
																			<td>

																				<h:inputText id="street" maxlength="150"
																					value="#{pages$auctionVenue.venue.location.street}"
																					tabindex="4"></h:inputText>
																			</td>

																			<%-- 	
													<td style="PADDING: 5px; padding-bottom:0px;">
												  		<h:outputLabel styleClass="LABEL" value="#{msg['contact.city']}:"></h:outputLabel></td>
													<td style="PADDING: 5px; padding-bottom:0px;">
												        
													<h:selectOneMenu id="city"  tabindex="4" value="#{pages$auctionVenue.venue.location.cityIdString}">
														<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
														<f:selectItems value="#{pages$auctionVenue.cities}" />
													</h:selectOneMenu></td>
											--%>
																		</tr>
<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contact.postcode']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="postalCode" maxlength="10"
																					value="#{pages$auctionVenue.venue.location.postCode}"
																					tabindex="5"></h:inputText>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contact.address1']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="add1" maxlength="150"
																					value="#{pages$auctionVenue.venue.location.address1}"
																					tabindex="6"></h:inputText>
																			</td>

																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contact.address2']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="add2" maxlength="150"
																					value="#{pages$auctionVenue.venue.location.address2}"
																					tabindex="7"></h:inputText>
																			</td>

																		</tr>
<tr>
																			<td colspan="6" class="BUTTON_TD">
																				<%--	<h:commandButton type="submit" value="#{msg['venue.createVenue']}"
															action="#{pages$auctionVenue.createVenue}"
															style="width: 75px" tabindex="7"/>&nbsp;  
													--%>
																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.search']}"
																					action="#{pages$auctionVenue.searchVenue}"
																					style="width: 75px" tabindex="7" />
																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.clear']}"
																					onclick="javascript:resetValues();"
																					style="width: 75px" tabindex="10" />
																				<pims:security
																					screen="Pims.AuctionManagement.AuctionVenue.AuctionVenueList"
																					action="create">
																					<h:commandButton styleClass="BUTTON" type="submit"
																						value="#{msg['commons.Add']}"
																						action="#{pages$auctionVenue.btnAdd_Click}">
																					</h:commandButton>
																				</pims:security>
																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.cancel']}"
																					action="#{pages$auctionVenue.cancel}"
																					onclick="if (!confirm('You have not selected a venue! Are you sure?')) return false"
																					style="width: 75px" tabindex="8" />




																				<%--		<h:commandButton value="#{msg['commons.done']}"
													action="#{pages$auctionVenue.selectVenue}"
													style="width: 75px" tabindex="9"/>
												--%>
																			</td>
																		</tr>




                                                                                                                                   </table>
                                                                                                                                
                                                                                                                                </td>
																<td width="3%">
																	&nbsp;
 																</td>

                                                                                                                          </tr>
                                                                                                                 </table>
                                                                                                        </div>
                                                                                                        <div
														style="PADDING-BOTTOM: 5px; WIDTH: 100%; PADDING-TOP: 10px">
     												          <div class="imag">	&nbsp;</div>
													     <div class="contentDiv" style="width: 99%">
                                                                                                             				<t:dataTable id="dt1"
																value="#{pages$auctionVenue.venueDataList}"
																binding="#{pages$auctionVenue.dataTable}"
																rows="#{pages$auctionVenue.paginatorRows}"
																preserveDataModel="false" preserveSort="false"
																var="dataItem" rowClasses="row1,row2" rules="all"
																renderedIfEmpty="true" width="100%">
																<t:column id="col1" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['venue.name']}" />
																	</f:facet>
																	<t:commandLink id="cmdlink" styleClass="A_LEFT"
																		action="#{pages$auctionVenue.cmdVenueName_Click}"
																		value="#{dataItem.venueName}"
																		style="white-space: normal;" />
																	<t:outputText />
																</t:column>
																<t:column id="col2" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['contact.country']}" />
																	</f:facet>
																	<t:outputText value="#{dataItem.location.country}"
																		styleClass="A_LEFT" style="white-space: normal;" />
																</t:column>
																<t:column id="col3" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['contact.state']}" />
																	</f:facet>
																	<t:outputText value="#{dataItem.location.state}"
																		styleClass="A_LEFT" style="white-space: normal;" />
																</t:column>
																<%-- 	<t:column id="col4" sortable="true">
											<f:facet name="header">
												<t:outputText value="#{msg['contact.city']}" />
											</f:facet>
											<t:outputText value="#{dataItem.location.cityId}" />
										</t:column>
									--%>
																<t:column id="col5" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['contact.street']}" />
																	</f:facet>
																	<t:outputText value="#{dataItem.location.street}"
																		styleClass="A_LEFT" style="white-space: normal;" />
																</t:column>
																<t:column id="col6" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['contact.postcode']}" />
																	</f:facet>
																	<t:outputText value="#{dataItem.location.postCode}"
																		styleClass="A_LEFT" style="white-space: normal;" />
																</t:column>
																<t:column id="col7" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['contact.address1']}" />
																	</f:facet>
																	<t:outputText value="#{dataItem.location.address1}"
																		styleClass="A_LEFT" style="white-space: normal;" />
																</t:column>
																<t:column id="col8" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['contact.address2']}" />
																	</f:facet>
																	<t:outputText value="#{dataItem.location.address2}"
																		styleClass="A_LEFT" style="white-space: normal;" />
																</t:column>
																<pims:security
																	screen="Pims.AuctionManagement.AuctionVenue.AuctionVenueList"
																	action="create">
																	<t:column id="deletebtn">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.delete']}" />
																		</f:facet>
																		<t:commandLink id="sdasd"
																			action="#{pages$auctionVenue.cmdDelete_Click}">&nbsp;
										 <h:graphicImage id="deleteIcon"
																				title="#{msg['commons.delete']}"
																				url="../resources/images/delete.gif" />&nbsp;
										 </t:commandLink>
																		<t:outputText />
																	</t:column>
																</pims:security>
																<pims:security
																	screen="Pims.AuctionManagement.AuctionVenue.AuctionVenueList"
																	action="create">
																	<t:column id="statusbtn">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.status']}" />
																		</f:facet>

																		<t:commandLink id="sadads"
																			rendered="#{dataItem.enable}"
																			action="#{pages$auctionVenue.cmdStatus_Click}">&nbsp;
											 <h:graphicImage id="enableIcon"
																				title="#{msg['commons.clickDisable']}"
																				url="../resources/images/app_icons/enable.png" />&nbsp;
										  </t:commandLink>

																		<t:commandLink id="dasdd"
																			rendered="#{dataItem.disable}"
																			action="#{pages$auctionVenue.cmdStatus_Click}">&nbsp;
											 <h:graphicImage id="disableIcon"
																				title="#{msg['commons.clickEnable']}"
																				url="../resources/images/app_icons/disable.png" />&nbsp;
										  </t:commandLink>
																		<t:outputText />
																	</t:column>
																</pims:security>

															</t:dataTable>

                                                                                                             </div>
<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
															style="width:100%;">
															<table cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<td class="RECORD_NUM_TD">
																		<div class="RECORD_NUM_BG">
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td class="RECORD_NUM_TD">
																						<h:outputText
																							value="#{msg['commons.recordsFound']}" />
																					</td>
																					<td class="RECORD_NUM_TD">
																						<h:outputText value=" : " />
																					</td>
																					<td class="RECORD_NUM_TD">
																						<h:outputText
																							value="#{pages$auctionVenue.recordSize}" />
																					</td>
																				</tr>
																			</table>
																		</div>
																	</td>
																	<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																		align="right">
																		<CENTER>
																			<t:dataScroller id="scroller" for="dt1"
																				paginator="true" fastStep="1"
																				paginatorMaxPages="#{pages$auctionVenue.paginatorMaxPages}"
																				immediate="false" paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true"
																				pageIndexVar="pageNumber" styleClass="SCH_SCROLLER"
																				paginatorActiveColumnStyle="font-weight:bold;"
																				paginatorRenderLinkForActive="false"
																				paginatorTableStyle="grid_paginator"
																				layout="singleTable"
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

																				<f:facet name="first">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblF"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastrewind">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastforward">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																				</f:facet>
																				<f:facet name="last">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblL"></t:graphicImage>
																				</f:facet>

																				<t:div styleClass="PAGE_NUM_BG">
																					<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																					<table cellpadding="0" cellspacing="0">
																						<tr>
																							<td>
																								<h:outputText styleClass="PAGE_NUM"
																									value="#{msg['commons.page']}" />
																							</td>
																							<td>
																								<h:outputText styleClass="PAGE_NUM"
																									style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																									value="#{requestScope.pageNumber}" />
																							</td>
																						</tr>
																					</table>

																				</t:div>
																			</t:dataScroller>
																		</CENTER>
																	</td>
																</tr>
															</table>
														</t:div>

                                                                                                         </div>

												</div>
                                                                                                

                                                                                                

                                                                                       </h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>

