


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script type="text/javascript">
        function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
        
		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		 function showServiceContract()
		 {
			var screen_width = 1024;
			var screen_height = 460;
			window.open('ServiceContractList.jsf?context=MaintenanceRequest&type=MAINTENANCE&viewMode=true&singleselectmode=true&type=MAINTENANCE','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=10,top=150,scrollbars=yes,status=yes');
		 }
		 
		
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />


	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />


		</head>

		<script language="javascript" type="text/javascript">
		function onExtraWorkClick(checkBox)
		{
		    
		   if( checkBox.checked )
		   {
		     
		     document.getElementById("formRequest:estimatedCostLabelDiv").style.display=
		     document.getElementById('formRequest:estimatedCostDiv').style.display='inline';
		   }
		   else 
		   {
		     document.getElementById("formRequest:estimatedCostLabelDiv").style.display=
		                       document.getElementById('formRequest:estimatedCostDiv').style.display='none';
		     document.getElementById("formRequest:txtEstimatedCost").value	= "";
		     //document.getElementById("formRequest:txtEstimatedCost").disabled	=   !document.getElementById("formRequest:txtEstimatedCost").disabled;
		   }
		}
	function validate() {
      maxlength=500;
       
       
       if(document.getElementById("formRequest:txt_remarks").value.length>=maxlength) {
          document.getElementById("formRequest:txt_remarks").value=
                                        document.getElementById("formRequest:txt_remarks").value.substr(0,maxlength);  
       
      }
    }
    function populateContract(contractId) {
      //document.getElementById("formRequest:hdnMaintenanceContractSearchContractContractId").value=contractId;
      //document.getElementById("formRequest:cmdAddMaintenanceContractSearchContract_Click").onclick();
      document.getElementById("formRequest:hdnContractId").value=contractId;
	  document.getElementById("formRequest:cmdAddContract_Click").onclick();
	
    
    }
    function SearchMemberPopup(){
			   var screen_width = 1024;
			   var screen_height = 450;
			   var popup_width = screen_width-150;
		       var popup_height = screen_height-50;
		       var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		       var popup = window.open('SearchMemberPopup.jsf?singleselectmode=true','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
		       popup.focus();
	}
    function populateUnit(unitId,unitNum,usuageTypeId,unitRentAmount,unitUsuageType,propertyCommercialName,unitAddress,unitStatusId,unitStatus)
	{
	    document.forms[0].submit();
	
	}
     function showUnitSearchPopUp()
	 {
		var screen_width = 1024;
		var screen_height = 460;
		window.open('UnitSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP&context=MaintenanceRequest','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=10,top=150,scrollbars=yes,status=yes');
	 }
	 
	 
	 function populateMaintenanceContract(contractId) 
	 {
	  
	  document.getElementById("formRequest:hdnMaintenanceContractSearchContractContractId").value=contractId;
      document.getElementById("formRequest:cmdAddMaintenanceContractSearchContract_Click").onclick();
	 }
	 
	 
	 
	 function populateProperty(propertyId)
	 {
	  
	  document.getElementById("formRequest:hdnPropertyId").value=propertyId;
	  document.getElementById("formRequest:cmdAddProperty_Click").onclick();
	 }
	 function showPropertySearchPopUp()
	 {
		var screen_width = 1024;
		var screen_height = 450;
		window.open('propertyList.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	 }
	 
	 function showContractSearchPopUp()
	 {
		var screen_width = 1024;
		var screen_height = 450;
		window.open('ContractList.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	 }
 
 
	function showSearchContractorPopUp(viewMode,popUp)
	{
	     
	     var  screen_width = 1024;
	     var screen_height = 450;
	     
	     window.open('contractorSearch.jsf?'+viewMode+'='+popUp,'_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	  
	}
	function populateContractor(contractorId, contractorNumber, contractorNameEn, contractorNameAr, licenseNumber, licenseSource, officeNumber){
		    document.getElementById("formRequest:hdnContractorId").value=contractorId;
		    document.getElementById("formRequest:cmdAddPerson_Click").onclick();
		}
	
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
            if(hdnPersonType=='APPLICANT')
	        {
	         
			 document.getElementById("formRequest:hdnApplicantId").value=personId;
	         document.getElementById("formRequest:cmdAddApplicant_Click").onclick();
	       
	        }
		    
	}
	
	
	function showPersonReadOnlyPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 450;
	    window.open('AddPerson.jsf?personId='+document.getElementById("formRequest:hdnContractorId").value+'&viewMode=popup&readOnlyMode=true','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
    </script>


		<!-- Header -->

		<body class="BODY_STYLE">
			<div class="containerDiv">

				<table width="99%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<c:choose>
							<c:when test="${!pages$newMaintenanceRequest.isViewModePopUp}">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</c:when>
						</c:choose>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${!pages$newMaintenanceRequest.isViewModePopUp}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>
						<td width="83%" height="84%" valign="top"
							class="divBackgroundBody">
							<h:form id="formRequest" style="width:99.7%;height:95%"
								enctype="multipart/form-data">
								<table class="greyPanelTable" cellpadding="0" cellspacing="0"
									border="0">
									<tr>
										<td class="HEADER_TD" style="width: 70%;">
											<h:outputLabel
												value="#{pages$newMaintenanceRequest.pageTitle}"
												styleClass="HEADER_FONT" />
										</td>
										<td>
											&nbsp;
										</td>


									</tr>
								</table>
								<table height="95%" width="99%" class="greyPanelMiddleTable"
									cellpadding="0" cellspacing="0" border="0">
									<tr valign="top">
										<td height="100%" valign="top" nowrap="nowrap"
											background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
											width="1">
										</td>
										<td height="100%" valign="top" nowrap="nowrap">
											<div class="SCROLLABLE_SECTION">
												<t:panelGrid id="hiddenFields" border="0"
													styleClass="layoutTable" width="94%"
													style="margin-left:15px;margin-right:15px;" columns="1">
													<h:outputText id="successmsg" escape="false"
														styleClass="INFO_FONT"
														value="#{pages$newMaintenanceRequest.successMessages}" />
													<h:outputText id="errormsg" escape="false"
														styleClass="ERROR_FONT"
														value="#{pages$newMaintenanceRequest.errorMessages}" />
													<h:inputHidden id="hdnCreatedOn"
														value="#{pages$newMaintenanceRequest.contractCreatedOn}" />
													<h:inputHidden id="hdnCreatedBy"
														value="#{pages$newMaintenanceRequest.contractCreatedBy}" />
													<h:inputHidden id="hdnRequestId"
														value="#{pages$newMaintenanceRequest.requestId}" />
													<h:inputHidden id="hdnPropertyId"
														value="#{pages$newMaintenanceRequest.hdnPropertyId}" />
													<h:inputHidden id="hdnContractorId"
														value="#{pages$newMaintenanceRequest.hdnContractorId}" />
													<h:inputHidden id="hdnApplicantId"
														value="#{pages$newMaintenanceRequest.hdnApplicantId}" />
													<h:inputHidden id="hdnContractId"
														value="#{pages$newMaintenanceRequest.hdnContractId}" />
													<h:inputHidden
														id="hdnMaintenanceContractSearchContractContractId"
														value="#{pages$newMaintenanceRequest.hdnMaintenanceContractSearchContractContractId}" />
													<a4j:commandLink id="cmdAddPerson_Click"
														reRender="tbl_Action,hiddenFields"
														action="#{pages$newMaintenanceRequest.populateContractor}" />
													<a4j:commandLink id="cmdAddProperty_Click"
														reRender="hiddenFields,unitInfoTable"
														action="#{pages$newMaintenanceRequest.populatePropertyInfoInTab}" />
													<a4j:commandLink id="cmdAddApplicant_Click"
														reRender="applicationDetailsTable,hiddenFields"
														action="#{pages$newMaintenanceRequest.findApplicantById}" />
													<a4j:commandLink id="cmdAddContract_Click"
														reRender="tbl_Action,hiddenFields"
														action="#{pages$newMaintenanceRequest.populateContract}" />
													<h:commandLink
														id="cmdAddMaintenanceContractSearchContract_Click"
														action="#{pages$newMaintenanceRequest.populateMaintenanceContractSearchContract}" />


												</t:panelGrid>
												<div class="MARGIN" style="width: 94%; margin-bottom: 0px;">

													<t:panelGrid id="tbl_Action" width="100%" border="0"
														columns="4"
														binding="#{pages$newMaintenanceRequest.tbl_Action}">

														<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*" />
															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.Contract.Number']}:" />
														</h:panelGroup>
														<h:panelGroup>
															<h:inputText maxlength="20" style="width:190px"
																styleClass="READONLY" id="txtcontractNumber"
																binding="#{pages$newMaintenanceRequest.txtContractNum}"
																readonly="true"
																value="#{pages$newMaintenanceRequest.contractView.contractNumber}"></h:inputText>
															<h:commandLink
																action="#{pages$newMaintenanceRequest.onMaintenanceContractSearchClick}">
																<h:graphicImage
																	binding="#{pages$newMaintenanceRequest.imgContractSearch}"
																	url="../resources/images/app_icons/Search-tenant.png"
																	style="MARGIN: 0px 0px -4px;"
																	title="#{msg['commons.search']}"
																	alt="#{msg['commons.search']}"></h:graphicImage>
															</h:commandLink>
															<h:commandLink
																action="#{pages$newMaintenanceRequest.onDeleteMaintenanceContract}">
																<h:graphicImage
																	binding="#{pages$newMaintenanceRequest.imgClearContract}"
																	url="../resources/images/delete_icon.png"
																	style="MARGIN: 0px 0px -4px;"
																	title="#{msg['commons.clear']}"
																	alt="#{msg['commons.clear']}"></h:graphicImage>
															</h:commandLink>
														</h:panelGroup>
														<h:outputLabel styleClass="LABEL"
															value="#{msg['maintenanceRequest.requestPriority']}:" />
														<h:selectOneMenu style="width:193px"
															id="RequestPriorityMenu"
															binding="#{pages$newMaintenanceRequest.cmbRequestPriority}"
															value="#{pages$newMaintenanceRequest.selectOneRequestPriority}">
															<f:selectItems
																value="#{pages$newMaintenanceRequest.requestPriority}" />
														</h:selectOneMenu>
														<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*" />
															<h:outputLabel styleClass="LABEL"
																value="#{msg['maintenanceRequest.contractor']}" />
														</h:panelGroup>
														<h:panelGroup>
															<h:inputText id="txtContractorNumber" style="width:190px"
																readonly="true"
																binding="#{pages$newMaintenanceRequest.txtContractorNum}"
																title="#{pages$newMaintenanceRequest.contractor.personFullName}"
																value="#{pages$newMaintenanceRequest.contractor.personFullName}"></h:inputText>
															<h:graphicImage
																url="../resources/images/app_icons/Search-tenant.png"
																binding="#{pages$newMaintenanceRequest.imgSearchContractor}"
																onclick="javaScript:showSearchContractorPopUp('#{pages$newMaintenanceRequest.contractorScreenQueryStringViewMode}','#{pages$newMaintenanceRequest.contractorScreenQueryStringPopUpMode}');"
																style="MARGIN: 0px 0px -4px;"
																title="#{msg[maintenanceRequest.searchContractor]}"
																alt="#{msg[maintenanceRequest.searchContractor]}"></h:graphicImage>
														</h:panelGroup>
														<t:panelGroup colspan="2">&nbsp;</t:panelGroup>


														<h:panelGroup
															rendered="#{pages$newMaintenanceRequest.isPageModeSiteVisitRequired}">
															<h:outputLabel styleClass="mandatory" value="*" />
															<h:outputLabel styleClass="LABEL"
																value="#{msg['maintenanceRequest.visitDate']}" />
														</h:panelGroup>
														<rich:calendar id="txtVisitDate"
															value="#{pages$newMaintenanceRequest.visitDate}"
															rendered="#{pages$newMaintenanceRequest.isPageModeSiteVisitRequired}"
															locale="#{pages$newMaintenanceRequest.locale}"
															popup="true"
															datePattern="#{pages$newMaintenanceRequest.dateFormat}"
															showApplyButton="false" enableManualInput="false"
															cellWidth="24px" cellHeight="22px" style="height:16px" />


														<h:panelGroup
															rendered="#{pages$newMaintenanceRequest.isPageModeSiteVisitRequired}">
															<h:outputLabel styleClass="mandatory" value="*" />
															<h:outputLabel styleClass="LABEL"
																value="#{msg['maintenanceRequest.engineer']}" />
														</h:panelGroup>
														<h:panelGroup
															rendered="#{pages$newMaintenanceRequest.isPageModeSiteVisitRequired}">
															<h:inputText disabled="true" maxlength="100"
																id="txtEngineerName"
																rendered="#{pages$newMaintenanceRequest.isPageModeSiteVisitRequired}"
																style="width: 190px"
																value="#{pages$newMaintenanceRequest.userView.fullNamePrimary}" />
															<h:graphicImage
																binding="#{pages$newMaintenanceRequest.imgSearchEngineer}"
																url="../resources/images/app_icons/Search-tenant.png"
																onclick="javaScript:SearchMemberPopup();"
																style="MARGIN: 0px 0px -4px;"
																title="#{msg['maintenanceRequest.searchEngineer']}"
																alt="#{msg['maintenanceRequest.searchEngineer']}"></h:graphicImage>
														</h:panelGroup>

														<h:outputLabel styleClass="LABEL"
															rendered="#{pages$newMaintenanceRequest.isPageModeSiteVisitRequired}"
															value="#{msg['maintenanceRequest.isExtraWork']}" />
														<h:selectBooleanCheckbox id="isExtraWork"
															rendered="#{pages$newMaintenanceRequest.isPageModeSiteVisitRequired}"
															style="width: 15%;height: 22px; border: 0px;"
															value="#{pages$newMaintenanceRequest.extraWork}"
															onclick="javaScript:onExtraWorkClick(this);" />
														<t:div id="estimatedCostLabelDiv"
															rendered="#{pages$newMaintenanceRequest.isPageModeSiteVisitRequired }"
															style="display:#{pages$newMaintenanceRequest.extraWork?'inline':'none'};">
															<h:panelGroup
																rendered="#{pages$newMaintenanceRequest.isPageModeSiteVisitRequired}">
																<h:outputLabel id="mandatoryEstimatedCost"
																	styleClass="mandatory" value="*" />
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['maintenanceRequest.estimatedCost']}" />
															</h:panelGroup>
														</t:div>
														<t:div id="estimatedCostDiv"
															rendered="#{pages$newMaintenanceRequest.isPageModeSiteVisitRequired }"
															style="display:#{pages$newMaintenanceRequest.extraWork?'inline':'none'};">
															<h:inputText maxlength="20" style="width:190px"
																id="txtEstimatedCost"
																rendered="#{pages$newMaintenanceRequest.isPageModeSiteVisitRequired }"
																value="#{pages$newMaintenanceRequest.estimatedCost}" />
														</t:div>

														<h:outputLabel styleClass="LABEL"
															value="#{msg['maintenanceRequest.actualCost']}"
															rendered="#{pages$newMaintenanceRequest.isPageModeFollowUp}" />
														<h:inputText maxlength="20" style="width: 190px"
															id="txtActualCost"
															rendered="#{pages$newMaintenanceRequest.isPageModeFollowUp}"
															value="#{pages$newMaintenanceRequest.actualCost}" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['commons.customerReview']}"
															binding="#{pages$newMaintenanceRequest.lblCustomerReview}" />
														<h:selectOneMenu id="cmbCustomerReview"
															binding="#{pages$newMaintenanceRequest.cmbCustomerReview}"
															value="#{pages$newMaintenanceRequest.selectOneCmbCustomerReview}">
															<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																itemValue="-1" />
															<f:selectItems
																value="#{pages$ApplicationBean.customerReview}" />
														</h:selectOneMenu>


														<h:outputLabel styleClass="LABEL"
															value="#{msg['maintenanceRequest.remarks']}"
															binding="#{pages$newMaintenanceRequest.lblRemarks}" />
														<t:panelGroup colspan="3" style="width: 100%"
															binding="#{pages$newMaintenanceRequest.grpRemarks}">
															<t:inputTextarea style="width: 100%;" id="txt_remarks"
																binding="#{pages$newMaintenanceRequest.txtboxRemarks}"
																value="#{pages$newMaintenanceRequest.txtRemarks}"
																onblur="javaScript:validate();"
																onkeyup="javaScript:validate();"
																onmouseup="javaScript:validate();"
																onmousedown="javaScript:validate();"
																onchange="javaScript:validate();"
																onkeypress="javaScript:validate();" />
														</t:panelGroup>

														<h:outputLabel styleClass="LABEL"
															binding="#{pages$newMaintenanceRequest.lblFMRemarks}"
															value="#{msg['commons.fmRemarks']}" />
														<t:panelGroup colspan="3" style="width: 100%"
															binding="#{pages$newMaintenanceRequest.grpFMRemarks}">
															<t:inputTextarea style="width: 100%;" id="txtFMRemarks"
																binding="#{pages$newMaintenanceRequest.txtFMRemarks}"
																onblur="javaScript:validate();"
																onkeyup="javaScript:validate();"
																onmouseup="javaScript:validate();"
																onmousedown="javaScript:validate();"
																onchange="javaScript:validate();"
																onkeypress="javaScript:validate();" />
														</t:panelGroup>


														<h:outputLabel styleClass="LABEL"
															binding="#{pages$newMaintenanceRequest.lblServiceProviderRemarks}"
															value="#{msg['commons.serviceProviderRemarks']}" />
														<t:panelGroup colspan="3" style="width: 100%"
															binding="#{pages$newMaintenanceRequest.grpServiceProviderRemarks}">
															<t:inputTextarea style="width: 100%;"
																id="txtServiceProviderRemarks" readonly="true"
																binding="#{pages$newMaintenanceRequest.txtServiceProviderRemarks}"
																onblur="javaScript:validate();"
																onkeyup="javaScript:validate();"
																onmouseup="javaScript:validate();"
																onmousedown="javaScript:validate();"
																onchange="javaScript:validate();"
																onkeypress="javaScript:validate();" />
														</t:panelGroup>

													</t:panelGrid>
													<t:div styleClass="BUTTON_TD"
														style="padding-top:10px; padding-right:21px;padding-bottom: 10x;width:100%;">

														<h:commandButton type="submit" styleClass="BUTTON"
															style="width:auto;"
															action="#{pages$newMaintenanceRequest.onSave}"
															binding="#{pages$newMaintenanceRequest.btnOtherSave}"
															value="#{msg['commons.saveButton']}" />
														<h:commandButton type="submit" styleClass="BUTTON"
															style="width:auto;"
															action="#{pages$newMaintenanceRequest.onSendForSiteVisit}"
															binding="#{pages$newMaintenanceRequest.btnSiteVisitReq}"
															value="#{msg['maintenanceRequest.siteVisitReq']}" />

														<h:commandButton type="submit" styleClass="BUTTON"
															style="width:auto;"
															action="#{pages$newMaintenanceRequest.onResendToContractor}"
															binding="#{pages$newMaintenanceRequest.btnResendToContractor}"
															value="#{msg['maintenancerequest.lbl.resentToContractor']}" />
														<h:commandButton type="submit" styleClass="BUTTON"
															style="width:auto;"
															action="#{pages$newMaintenanceRequest.onApprove}"
															binding="#{pages$newMaintenanceRequest.btnApprove}"
															value="#{msg['commons.sendToContractor']}" />

														<h:commandButton type="submit" styleClass="BUTTON"
															style="width:auto;"
															action="#{pages$newMaintenanceRequest.onReject}"
															binding="#{pages$newMaintenanceRequest.btnReject}"
															value="#{msg['commons.reject']}" />
														<h:commandButton type="submit" styleClass="BUTTON"
															style="width:auto;"
															action="#{pages$newMaintenanceRequest.onSaveSiteVisit}"
															binding="#{pages$newMaintenanceRequest.btnSaveSiteVisit}"
															value="#{msg['maintenanceRequest.saveSiteVisit']}" />
														<h:commandButton type="submit" styleClass="BUTTON"
															style="width:auto;"
															action="#{pages$newMaintenanceRequest.onCompleteSiteVisit}"
															binding="#{pages$newMaintenanceRequest.btnCompleteSiteVisit}"
															value="#{msg['maintenanceRequest.completeSiteVisit']}" />
														<h:commandButton type="submit" styleClass="BUTTON"
															style="width:auto;"
															action="#{pages$newMaintenanceRequest.onSaveFollowUp}"
															binding="#{pages$newMaintenanceRequest.btnSaveFollowUp}"
															value="#{msg['maintenanceRequest.saveFollowUp']}" />

														<h:commandButton type="submit" styleClass="BUTTON"
															style="width:auto;"
															action="#{pages$newMaintenanceRequest.onCustomerRejected}"
															binding="#{pages$newMaintenanceRequest.btnCustomerRejected}"
															value="#{msg['comons.sendToFM']}" />

														<h:commandButton type="submit" styleClass="BUTTON"
															style="width:auto;"
															action="#{pages$newMaintenanceRequest.onContractorExecuted}"
															binding="#{pages$newMaintenanceRequest.btnContractorExecuted}"
															value="#{msg['commons.done']}" />

														<h:commandButton type="submit" styleClass="BUTTON"
															style="width:auto;"
															action="#{pages$newMaintenanceRequest.onComplete}"
															binding="#{pages$newMaintenanceRequest.btnComplete}"
															value="#{msg['maintenanceRequest.completeRequest']}" />


													</t:div>



													<rich:tabPanel id="tabPanel"
														binding="#{pages$newMaintenanceRequest.tabPanel}"
														style="width:100%;height:320px;" headerSpacing="0">

														<rich:tab id="appDetailsTab"
															binding="#{pages$newMaintenanceRequest.tabApplicationDetails}"
															title="#{msg['commons.tab.applicationDetails']}"
															label="#{msg['contract.tabHeading.ApplicationDetails']}">

															<%@ include file="ApplicationDetails.jsp"%>

														</rich:tab>
														<rich:tab id="tabMaintenanceInfo"
															binding="#{pages$newMaintenanceRequest.tabMaintenanceInfo}"
															title="#{msg['maintenanceRequest.tab.MaintenanceDetails']}"
															label="#{msg['maintenanceRequest.tab.MaintenanceDetails']}">
															<t:div styleClass="TAB_DETAIL_SECTION" style="width:100%">
																<t:panelGrid id="unitInfoTable" cellpadding="1px"
																	width="100%" cellspacing="3px"
																	styleClass="TAB_DETAIL_SECTION_INNER" columns="4">

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['property.propertyNumber']}:"></h:outputLabel>
																	</h:panelGroup>

																	<h:panelGroup>
																		<h:inputText styleClass="READONLY"
																			id="txtpropertyNumber" style="width:170px;"
																			readonly="true"
																			value="#{pages$newMaintenanceRequest.unitView.propertyNumber}"></h:inputText>
																		<h:graphicImage
																			binding="#{pages$newMaintenanceRequest.imgPropertySearch}"
																			url="../resources/images/app_icons/Search-tenant.png"
																			onclick="javaScript:showPropertySearchPopUp();"
																			style="MARGIN: 0px 0px -4px;"
																			title="#{msg['maintenanceRequest.searchProperties']}"
																			alt="#{msg['maintenanceRequest.searchProperties']}"></h:graphicImage>
																	</h:panelGroup>

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['unit.unitNumber']}:" />
																	</h:panelGroup>
																	<h:panelGroup>
																		<h:inputText maxlength="20" styleClass="READONLY"
																			id="txtunitNumber" style="width:170px;"
																			readonly="true"
																			value="#{pages$newMaintenanceRequest.unitView.unitNumber}"></h:inputText>
																		<h:graphicImage
																			binding="#{pages$newMaintenanceRequest.imgUnitSearch}"
																			url="../resources/images/app_icons/Search-tenant.png"
																			onclick="javaScript:showUnitSearchPopUp();"
																			style="MARGIN: 0px 0px -4px;"
																			title="#{msg['maintenanceRequest.searchUnits']}"
																			alt="#{msg['maintenanceRequest.searchUnits']}"></h:graphicImage>
																		<h:graphicImage
																			rendered="#{! empty pages$newMaintenanceRequest.unitView && ! empty pages$newMaintenanceRequest.unitView.unitId}"
																			url="../resources/images/app_icons/No-Objection-letter.png"
																			onclick="javaScript:openEditUnitPopup(#{pages$newMaintenanceRequest.unitView.unitId});"
																			style="MARGIN: 0px 0px -4px;"
																			title="#{msg['unit.details']}"
																			alt="#{msg['unit.details']}"></h:graphicImage>
																	</h:panelGroup>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['floor.floorNo']}:"></h:outputLabel>
																	<h:inputText id="txtfloorNumber" styleClass="READONLY"
																		readonly="true" maxlength="20"
																		value="#{pages$newMaintenanceRequest.unitView.floorNumber}" />


																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.unitDescription']}:"></h:outputLabel>
																	<h:inputText id="txtunitDescription"
																		styleClass="READONLY" readonly="true" maxlength="20"
																		value="#{pages$newMaintenanceRequest.unitView.unitDesc}" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.endowed/minorName']}:"></h:outputLabel>
																	<h:inputText id="txtEndowedMinorName"
																		styleClass="READONLY" readonly="true"
																		value="#{pages$newMaintenanceRequest.unitView.propertyEndowedName}"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.commercialName']}:"></h:outputLabel>
																	<h:inputText id="txtCommercialName"
																		styleClass="READONLY" readonly="true"
																		value="#{pages$newMaintenanceRequest.unitView.propertyCommercialName}"></h:inputText>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.usage']}:"></h:outputLabel>
																	<h:inputText styleClass="READONLY" id="txtunitusageEn"
																		rendered="#{pages$newMaintenanceRequest.isEnglishLocale}"
																		readonly="true"
																		value="#{pages$newMaintenanceRequest.unitView.usageTypeEn}"></h:inputText>
																	<h:inputText styleClass="READONLY" id="txtunitusageAr"
																		rendered="#{pages$newMaintenanceRequest.isArabicLocale}"
																		readonly="true"
																		value="#{pages$newMaintenanceRequest.unitView.usageTypeAr}"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contact.country']}:"></h:outputLabel>
																	<h:inputText styleClass="READONLY" id="txtCountryEn"
																		rendered="#{pages$newMaintenanceRequest.isEnglishLocale}"
																		readonly="true"
																		value="#{pages$newMaintenanceRequest.unitView.countryEn}"></h:inputText>
																	<h:inputText styleClass="READONLY" id="txtCountryAr"
																		rendered="#{pages$newMaintenanceRequest.isArabicLocale}"
																		readonly="true"
																		value="#{pages$newMaintenanceRequest.unitView.countryAr}"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.Emirate']}:"></h:outputLabel>
																	<h:inputText styleClass="READONLY" id="txtEmirateEn"
																		rendered="#{pages$newMaintenanceRequest.isEnglishLocale}"
																		readonly="true"
																		value="#{pages$newMaintenanceRequest.unitView.stateEn}"></h:inputText>
																	<h:inputText styleClass="READONLY" id="txtEmirateAr"
																		rendered="#{pages$newMaintenanceRequest.isArabicLocale}"
																		readonly="true"
																		value="#{pages$newMaintenanceRequest.unitView.stateAr}"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.area']}:" />
																	<h:inputText styleClass="READONLY" id="txtunitArea"
																		readonly="true"
																		value="#{pages$newMaintenanceRequest.unitView.unitArea}"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contact.street']}:" />
																	<h:inputText styleClass="READONLY" id="txtstreet"
																		readonly="true"
																		value="#{pages$newMaintenanceRequest.unitView.street}"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.address']}:" />
																	<h:inputText styleClass="READONLY" id="txtaddress"
																		readonly="true"
																		value="#{pages$newMaintenanceRequest.unitView.unitAddress}"></h:inputText>


																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.Contract.Number']}:" />


																	<h:panelGroup>
																		<h:inputText styleClass="READONLY"
																			id="txtContractNumber" readonly="true"
																			style="width:170px;"
																			value="#{pages$newMaintenanceRequest.maintenanceContractView.contractNumber}"></h:inputText>
																		<h:graphicImage
																			binding="#{pages$newMaintenanceRequest.imgMaintenanceContractSearch}"
																			url="../resources/images/app_icons/Search-tenant.png"
																			onclick="javaScript:showContractSearchPopUp();"
																			style="MARGIN: 0px 0px -4px;"
																			title="#{msg['contractFollowUp.searchContract']}"
																			alt="#{msg['contractFollowUp.searchContract']}"></h:graphicImage>
																		<h:graphicImage
																			rendered="#{! empty pages$newMaintenanceRequest.maintenanceContractView && 
																			            ! empty pages$newMaintenanceRequest.maintenanceContractView.contractId}"
																			url="../resources/images/app_icons/No-Objection-letter.png"
																			onclick="javaScript:openLeaseContractPopup( #{pages$newMaintenanceRequest.maintenanceContractView.contractId} );"
																			style="MARGIN: 0px 0px -4px;"
																			title="#{msg['contract.LeaseContract']}"
																			alt="#{msg['contract.LeaseContract']}"></h:graphicImage>
																	</h:panelGroup>


																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['listReport.tenancyContractTemplate.tenantName']}:" />
																	<h:inputText styleClass="READONLY" id="txtTenantName"
																		readonly="true"
																		value="#{pages$newMaintenanceRequest.maintenanceContractView.tenantView.personFullName}"></h:inputText>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['complaint.lbl.complaintNumber']}:" />

																	<h:panelGroup>
																		<h:inputText styleClass="READONLY"
																			id="txtcomplaintNumber" readonly="true"
																			style="width:170px;"
																			value="#{pages$newMaintenanceRequest.complaintDetailsView.complaintNumber}"></h:inputText>

																		<h:graphicImage
																			rendered="#{! empty pages$newMaintenanceRequest.complaintDetailsView && 
																			            ! empty pages$newMaintenanceRequest.complaintDetailsView.complaintId}"
																			url="../resources/images/app_icons/No-Objection-letter.png"
																			onclick="javaScript:openComplaintDetailsPopup (#{pages$newMaintenanceRequest.complaintDetailsView.complaintId} );"
																			style="MARGIN: 0px 0px -4px;"
																			title="#{msg['complaint.lbl.complaintNumber']}"
																			alt="#{msg['complaint.lbl.complaintNumber']}"></h:graphicImage>
																	</h:panelGroup>


																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<t:outputLabel styleClass="LABEL"
																			value="#{msg['scopeOfWork.workType']}:"
																			style="padding-right:5px;padding-left:5px;margin-right:10px;" />
																	</h:panelGroup>

																	<t:div
																		style="overflow-y:auto;height: 100px; overflow-x: hidden; border-width: 1px; border-style: solid; border-color: #a2a2a2;">
																		<h:panelGrid
																			style="border-width: 1px; border-style: solid; border-color: #a2a2a2; background: white; overflow-x: auto; overflow-y: scroll; width: 80%; height: 60px;font-weight:400 ">
																			<h:selectOneRadio id="workTypeMenu"
																				binding="#{pages$newMaintenanceRequest.cmbWorkType}"
																				layout="pageDirection"
																				value="#{pages$newMaintenanceRequest.selectOneWorkType}">

																				<f:selectItems
																					value="#{pages$ApplicationBean.workTypesList}" />
																			</h:selectOneRadio>
																		</h:panelGrid>
																	</t:div>
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<t:outputLabel styleClass="LABEL"
																			value="#{msg['maintenanceRequest.lbl.maintenanceType']}:"
																			style="padding-right:5px;padding-left:5px;margin-right:10px;" />
																	</h:panelGroup>
																	<h:selectOneMenu id="maintenanceTypeMenu"
																		binding="#{pages$newMaintenanceRequest.cmbMaintenanceType}"
																		value="#{pages$newMaintenanceRequest.selectOneMaintenanceType}">
																		<f:selectItems
																			value="#{pages$ApplicationBean.maintenanceTypesList}" />
																	</h:selectOneMenu>


																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['person.contactNumber']}" />
																	</h:panelGroup>
																	<h:inputText id="txtTelephoneNumber" maxlength="15"
																		binding="#{pages$newMaintenanceRequest.txtTelephoneNumber}" />


																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel
																			value="#{msg['maintenanceRequest.tab.MaintenanceDetails']}:" />
																	</h:panelGroup>
																	<t:panelGroup colspan="3" style="width:100%">
																		<h:inputTextarea id="txtMaintenanceDetails"
																			style="width:100%"
																			binding="#{pages$newMaintenanceRequest.txtMaintenaceDetails}"
																			onblur="javaScript:validateMaintenanceDetails();"
																			onkeyup="javaScript:validateMaintenanceDetails();"
																			onmouseup="javaScript:validateMaintenanceDetails();"
																			onmousedown="javaScript:validateMaintenanceDetails();"
																			onchange="javaScript:validateMaintenanceDetails();"
																			onkeypress="javaScript:validateMaintenanceDetails();" />
																	</t:panelGroup>


																</t:panelGrid>
															</t:div>

														</rich:tab>

														<rich:tab id="tabSiteVisit"
															binding="#{pages$newMaintenanceRequest.tabSiteVisit}"
															action="#{pages$newMaintenanceRequest.tabSiteVisit_Click}"
															title="#{msg['maintenanceRequest.tab.SiteVisit']}"
															label="#{msg['maintenanceRequest.tab.SiteVisit']}">

															<t:div id="divSiteVisit" styleClass="contentDiv"
																style="width:98%;">
																<t:dataTable id="siteVisitTable" renderedIfEmpty="true"
																	width="100%" cellpadding="0" cellspacing="0"
																	var="siteVisitDataItem"
																	binding="#{pages$newMaintenanceRequest.siteVisitDataTable}"
																	value="#{pages$newMaintenanceRequest.siteVisitList}"
																	preserveDataModel="false" preserveSort="false"
																	rowClasses="row1,row2" rules="all" rows="15">
																	<t:column width="200">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['maintenanceRequest.visitDate']}" />
																		</f:facet>
																		<t:outputText value="#{siteVisitDataItem.visitDate}"
																			styleClass="A_LEFT" />

																	</t:column>
																	<t:column width="300">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['maintenanceRequest.remarks']}" />
																		</f:facet>
																		<t:outputText value="#{siteVisitDataItem.remarks}"
																			styleClass="A_LEFT" />

																	</t:column>
																	<t:column width="engineerNameEn"
																		rendered="#{pages$newMaintenanceRequest.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['maintenanceRequest.engineer']}" />
																		</f:facet>
																		<t:outputText
																			value="#{siteVisitDataItem.engineerFullNameEn}"
																			styleClass="A_LEFT" />
																	</t:column>
																	<t:column width="engineerNameAr"
																		rendered="#{!pages$newMaintenanceRequest.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['maintenanceRequest.engineer']}" />
																		</f:facet>
																		<t:outputText
																			value="#{siteVisitDataItem.engineerFullNameAr}"
																			styleClass="A_LEFT" />
																	</t:column>
																	<t:column width="200">
																		<f:facet name="header">
																			<h:outputText
																				value="#{msg['maintenanceRequest.estimatedCost']}" />
																		</f:facet>
																		<t:outputText
																			value="#{siteVisitDataItem.estimatedCost}"
																			styleClass="A_LEFT" />
																	</t:column>


																</t:dataTable>
															</t:div>
															<t:div id="divPagingSiteVisit"
																styleClass="contentDivFooter AUCTION_SCH_RF"
																style="width:100%;">
																<t:panelGrid columns="2" border="0" width="100%"
																	cellpadding="1" cellspacing="1">
																	<t:panelGroup
																		styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																		<t:div styleClass="JUG_NUM_REC_ATT">
																			<h:outputText value="#{msg['commons.records']}" />
																			<h:outputText value=" : " />
																			<h:outputText
																				value="#{pages$newMaintenanceRequest.recordSize}" />
																		</t:div>
																	</t:panelGroup>
																	<t:panelGroup>
																		<t:dataScroller id="siteVisitScroller"
																			for="siteVisitTable" paginator="true" fastStep="1"
																			paginatorMaxPages="5" immediate="false"
																			paginatorTableClass="paginator"
																			renderFacetsIfSinglePage="true"
																			paginatorTableStyle="grid_paginator"
																			layout="singleTable"
																			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																			paginatorActiveColumnStyle="font-weight:bold;"
																			paginatorRenderLinkForActive="false"
																			pageIndexVar="pageNumber" styleClass="JUG_SCROLLER">
																			<f:facet name="first">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblFSV"></t:graphicImage>
																			</f:facet>
																			<f:facet name="fastrewind">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFRSV"></t:graphicImage>
																			</f:facet>
																			<f:facet name="fastforward">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFFSV"></t:graphicImage>
																			</f:facet>
																			<f:facet name="last">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblLSV"></t:graphicImage>
																			</f:facet>
																			<t:div styleClass="PAGE_NUM_BG" rendered="true">
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																				<h:outputText styleClass="PAGE_NUM"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{requestScope.pageNumber}" />
																			</t:div>
																		</t:dataScroller>
																	</t:panelGroup>
																</t:panelGrid>
															</t:div>

														</rich:tab>
														<rich:tab id="tabFollowUp"
															binding="#{pages$newMaintenanceRequest.tabFollowUp}"
															action="#{pages$newMaintenanceRequest.tabFollowUp_Click}"
															title="#{msg['maintenanceRequest.tab.FollowUp']}"
															label="#{msg['maintenanceRequest.tab.FollowUp']}">
															<t:div styleClass="contentDiv" style="width:98%;">
																<t:dataTable id="followUpTable" renderedIfEmpty="true"
																	width="100%" cellpadding="0" cellspacing="0"
																	var="followUpDataItem"
																	binding="#{pages$newMaintenanceRequest.followUpDataTable}"
																	value="#{pages$newMaintenanceRequest.followUpList}"
																	preserveDataModel="false" preserveSort="false"
																	rowClasses="row1,row2" rules="all" rows="9">
																	<t:column width="200">
																		<f:facet name="header">
																			<t:outputText value=" Date " />
																		</f:facet>
																		<t:outputText value="#{followUpDataItem.followupDate}"
																			styleClass="A_LEFT" />

																	</t:column>
																	<t:column width="createByEn"
																		rendered="#{pages$newMaintenanceRequest.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['maintenanceRequest.followUpBy']}" />
																		</f:facet>
																		<t:outputText
																			value="#{followUpDataItem.createdByFullNameEn}"
																			styleClass="A_LEFT" />
																	</t:column>
																	<t:column width="createByEnAr"
																		rendered="#{!pages$newMaintenanceRequest.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['maintenanceRequest.followUpBy']}" />
																		</f:facet>
																		<t:outputText
																			value="#{followUpDataItem.createdByFullNameAr}"
																			styleClass="A_LEFT" />
																	</t:column>


																	<t:column>
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['maintenanceRequest.remarks']}" />
																		</f:facet>
																		<t:outputText value="#{followUpDataItem.remarks}"
																			styleClass="A_LEFT" />

																	</t:column>

																	<t:column>
																		<f:facet name="header">
																			<h:outputText
																				value="#{msg['maintenanceRequest.actualCost']}" />
																		</f:facet>
																		<t:outputText value="#{followUpDataItem.actualCost}"
																			styleClass="A_LEFT" />
																	</t:column>
																	<t:column>
																		<f:facet name="header">
																			<h:outputText value="" />
																		</f:facet>
																		<h:commandLink
																			action="#{pages$newMaintenanceRequest.addActualCostToFollowUp}">
																			<h:graphicImage
																				url="../resources/images/edit-icon.gif"></h:graphicImage>
																		</h:commandLink>
																	</t:column>


																</t:dataTable>
															</t:div>
															<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																style="width:100%;">
																<t:panelGrid columns="2" border="0" width="100%"
																	cellpadding="1" cellspacing="1">
																	<t:panelGroup
																		styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																		<t:div styleClass="JUG_NUM_REC_ATT">
																			<h:outputText value="#{msg['commons.records']}" />
																			<h:outputText value=" : " />
																			<h:outputText
																				value="#{pages$newMaintenanceRequest.recordSize}" />
																		</t:div>
																	</t:panelGroup>
																	<t:panelGroup>
																		<t:dataScroller id="FoollowupScroller1"
																			for="followUpTable" paginator="true" fastStep="1"
																			paginatorMaxPages="5" immediate="false"
																			paginatorTableClass="paginator"
																			renderFacetsIfSinglePage="true"
																			paginatorTableStyle="grid_paginator"
																			layout="singleTable"
																			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																			paginatorActiveColumnStyle="font-weight:bold;"
																			paginatorRenderLinkForActive="false"
																			pageIndexVar="pageNumber" styleClass="JUG_SCROLLER">
																			<f:facet name="first">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblFFU"></t:graphicImage>
																			</f:facet>
																			<f:facet name="fastrewind">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFRFU"></t:graphicImage>
																			</f:facet>
																			<f:facet name="fastforward">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFFFU"></t:graphicImage>
																			</f:facet>
																			<f:facet name="last">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblLFU"></t:graphicImage>
																			</f:facet>
																			<t:div styleClass="PAGE_NUM_BG" rendered="true">
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																				<h:outputText styleClass="PAGE_NUM"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{requestScope.pageNumber}" />
																			</t:div>
																		</t:dataScroller>
																	</t:panelGroup>
																</t:panelGrid>
															</t:div>
														</rich:tab>

														<rich:tab id="tabPaymentSchedule"
															binding="#{pages$newMaintenanceRequest.tabPaymentTerms}"
															action="#{pages$newMaintenanceRequest.onPaymentTermsTab}"
															title="#{msg['contract.paymentTerms']}"
															label="#{msg['contract.paymentTerms']}">

															<t:div style="width:98.6%">
																<t:panelGrid columns="4" cellpadding="5" cellspacing="5">

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['paymentSchedule.paymentDueOn']}:"></h:outputLabel>
																	<rich:calendar id="createdOnDateId"
																		styleClass="READONLY" disabled="false"
																		value="#{pages$newMaintenanceRequest.paymentInvoice.paymentDueOn}"
																		locale="#{pages$newMaintenanceRequest.locale}"
																		popup="true"
																		datePattern="#{pages$newMaintenanceRequest.dateFormat}"
																		showApplyButton="false" enableManualInput="false"
																		inputStyle="width:185px;height:17px" />

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel id="payModeField" styleClass="LABEL"
																			value="#{msg['cancelContract.payment.paymentmethod']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:selectOneMenu id="selectPaymentMethod"
																		value="#{pages$newMaintenanceRequest.paymentInvoice.paymentModeIdString}">

																		<f:selectItems
																			value="#{pages$ApplicationBean.paymentMethodList}" />
																	</h:selectOneMenu>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.amount']}" />
																	<h:inputText id="txtinvoiceAmount"
																		value="#{pages$newMaintenanceRequest.paymentInvoice.amountStr}" />

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.description']}" />
																	<h:inputTextarea id="txtinvoiceDescription"
																		onkeyup="removeExtraCharacter(this,500)"
																		onkeypress="removeExtraCharacter(this,500)"
																		onkeydown="removeExtraCharacter(this,500)"
																		onblur="removeExtraCharacter(this,500)"
																		onchange="removeExtraCharacter(this,500)"
																		value="#{pages$newMaintenanceRequest.paymentInvoice.description}" />


																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['paymentSchedule.InvoiceNumber']}" />
																	<h:inputText id="txtInvoiceNumber"
																		value="#{pages$newMaintenanceRequest.paymentInvoice.invoiceNumber}" />

																</t:panelGrid>
															</t:div>
															<t:div styleClass="BUTTON_TD" style="width:98.6%">
																<h:commandButton type="submit" styleClass="BUTTON"
																	style="width:auto;"
																	action="#{pages$newMaintenanceRequest.onAddInvoicePayment}"
																	value="#{msg['commons.saveButton']}" />
															</t:div>
															<t:div styleClass="contentDiv" style="width:98.6%">
																<t:dataTable id="tbl_PaymentSchedule" rows="15"
																	width="100%"
																	value="#{pages$newMaintenanceRequest.paymentScheduleDataList}"
																	binding="#{pages$newMaintenanceRequest.paymentScheduleDataTable}"
																	preserveDataModel="true" var="paymentScheduleDataItem"
																	rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true">

																	<t:column id="col_InvNo" style="width:15%;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.InvoiceNumber']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.invoiceNumber}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>

																	<t:column id="col_PaymentNumber" style="width:15%;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentNumber']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentNumber}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>

																	<t:column id="colTypeEn" style="width:8%;"
																		rendered="#{pages$newMaintenanceRequest.isEnglishLocale }">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentType']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{pages$newMaintenanceRequest.isEnglishLocale ?paymentScheduleDataItem.typeEn:paymentScheduleDataItem.typeAr}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>


																	<t:column id="colDesc">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.description']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.description}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>
																	<t:column id="colDueOn" style="width:8%;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentDueOn']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentDueOn}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}">
																			<f:convertDateTime
																				pattern="#{pages$newMaintenanceRequest.dateFormat}"
																				timeZone="#{pages$newMaintenanceRequest.timeZone}" />
																		</t:outputText>
																	</t:column>
																	<t:column id="colPaymentDate" style="width:8%;"
																		rendered="false">
																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['paymentSchedule.paymentDate']}">
																				<f:convertDateTime
																					pattern="#{pages$newMaintenanceRequest.dateFormat}"
																					timeZone="#{pages$newMaintenanceRequest.timeZone}" />
																			</t:outputText>
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentDate}">
																			<f:convertDateTime
																				pattern="#{pages$newMaintenanceRequest.dateFormat}"
																				timeZone="#{pages$newMaintenanceRequest.timeZone}" />
																		</t:outputText>
																	</t:column>

																	<t:column id="colModeAr" style="width:8%;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentMode']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{pages$newMaintenanceRequest.isArabicLocale ?paymentScheduleDataItem.paymentModeAr:paymentScheduleDataItem.paymentModeEn}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>

																	<t:column id="colStatusAr">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentStatus']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{pages$newMaintenanceRequest.isArabicLocale ?paymentScheduleDataItem.statusAr:paymentScheduleDataItem.statusEn}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>

																	<t:column id="colAmount">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.amount']}" />
																		</f:facet>
																		<t:outputText title="" styleClass="A_RIGHT_NUM"
																			value="#{paymentScheduleDataItem.amount}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>
																	<t:column id="col8">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<a4j:commandLink
																			reRender="tbl_PaymentSchedule,paymentSummary"
																			action="#{pages$newMaintenanceRequest.onDeleteInvoicePayment}"
																			rendered="#{paymentScheduleDataItem.isDeleted == 0  && paymentScheduleDataItem.statusId == 36003}">
																			<h:graphicImage id="deletePaySc"
																				title="#{msg['commons.delete']}"
																				rendered="#{(paymentScheduleDataItem.isReceived == 'N' )}"
																				url="../resources/images/delete_icon.png" />
																		</a4j:commandLink>
																		<t:commandLink
																			action="#{pages$newMaintenanceRequest.onEditInvoicePayment}"
																			rendered="#{paymentScheduleDataItem.isDeleted == 0  && paymentScheduleDataItem.statusId == 36003}">
																			>
																			<h:graphicImage title="#{msg['commons.edit']}"
																				url="../resources/images/edit-icon.gif" />
																		</t:commandLink>
																	</t:column>
																</t:dataTable>
															</t:div>
															<t:div id="divPaySch" styleClass="contentDivFooter"
																style="width:99.6%;">

																<t:dataScroller id="scrollerPaySch"
																	for="tbl_PaymentSchedule" paginator="true" fastStep="1"
																	paginatorMaxPages="10" immediate="false"
																	paginatorTableClass="paginator"
																	renderFacetsIfSinglePage="true"
																	pageIndexVar="pageNumber"
																	paginatorTableStyle="grid_paginator"
																	layout="singleTable"
																	paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																	paginatorActiveColumnStyle="font-weight:bold;"
																	paginatorRenderLinkForActive="false"
																	styleClass="SCH_SCROLLER">
																	<f:facet name="first">
																		<t:graphicImage url="../#{path.scroller_first}"
																			id="lblFPaySch"></t:graphicImage>
																	</f:facet>

																	<f:facet name="fastrewind">
																		<t:graphicImage url="../#{path.scroller_fastRewind}"
																			id="lblFRPaySch"></t:graphicImage>
																	</f:facet>

																	<f:facet name="fastforward">
																		<t:graphicImage url="../#{path.scroller_fastForward}"
																			id="lblFFPaySch"></t:graphicImage>
																	</f:facet>

																	<f:facet name="last">
																		<t:graphicImage url="../#{path.scroller_last}"
																			id="lblLPaySch"></t:graphicImage>
																	</f:facet>


																</t:dataScroller>

															</t:div>



														</rich:tab>
														<rich:tab
															label="#{msg['contract.tabHeading.RequestHistory']}"
															title="#{msg['commons.tab.requestHistory']}"
															action="#{pages$newMaintenanceRequest.tabAuditTrail_Click}">
															<%@ include file="../pages/requestTasks.jsp"%>
														</rich:tab>
														<rich:tab id="attachmentTab"
															label="#{msg['commons.attachmentTabHeading']}"
															action="#{pages$newMaintenanceRequest.tabAttachmentsComments_Click}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}"
															action="#{pages$newMaintenanceRequest.tabAttachmentsComments_Click}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>

													</rich:tabPanel>
												</div>



												<table cellpadding="1px" cellspacing="3px" width="95%">
													<tr>

														<td colspan="12" class="BUTTON_TD">

															<h:commandButton type="submit" styleClass="BUTTON"
																style="width:auto;"
																action="#{pages$newMaintenanceRequest.onSave}"
																binding="#{pages$newMaintenanceRequest.btnSave}"
																value="#{msg['commons.saveButton']}" />

															<h:commandButton id="btnSendForApproval" type="submit"
																styleClass="BUTTON" style="width:auto;"
																action="#{pages$newMaintenanceRequest.onSendForApproval}"
																binding="#{pages$newMaintenanceRequest.btnSend_For_Approval}"
																value="#{msg['commons.sendButton']}" />


														</td>
													</tr>
												</table>

											</div>
											</div>
										</td>
									</tr>
								</table>
							</h:form>
						</td>
					</tr>
					<tr>
						<td colspan="2">

							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>

