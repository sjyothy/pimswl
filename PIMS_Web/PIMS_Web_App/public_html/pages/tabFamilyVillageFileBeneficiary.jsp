
<t:div id="tabBDetails" style="width:100%;">
	<t:panelGrid id="tabBDetailsab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<%-- 
		<h:panelGroup>
			<h:outputLabel id='firstNamelbl' styleClass="mandatory" value="*" />
			<h:outputLabel id="firstnameText" styleClass="LABEL"
				value="#{msg['commons.Name']}:"></h:outputLabel>
		</h:panelGroup>
		<h:panelGroup>
			<h:inputText maxlength="500"
				value="#{pages$tabFamilyVillageFileBeneficiary.person.firstName}" />
			<h:graphicImage url="../resources/images/magnifier.gif"
				onclick="javaScript:openFamilyVillageAddBeneficiaryPopup(
																	'#{pages$familyVillageRequest.requestView.inheritanceFileView.inheritanceFileId}',
																		'-1');"
				style="MARGIN: 0px 0px -4px;" title="#{msg['commons.search']}"
				alt="#{msg['commons.search']}"></h:graphicImage>
		</h:panelGroup>
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL" id="DateOfBirthVillalble"
				value="#{msg['customer.dateOfBirth']}:" />
		</h:panelGroup>
		<rich:calendar id="DateOfBirthVilla"
			value="#{pages$tabFamilyVillageFileBeneficiary.person.dateOfBirth}"
			popup="true"
			datePattern="#{pages$tabFamilyVillageFileBeneficiary.dateFormat}"
			showApplyButton="false"
			locale="#{pages$tabFamilyVillageFileBeneficiary.locale}"
			enableManualInput="false" inputStyle="width: 170px; height: 14px" />


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="genderLblVilla" styleClass="LABEL"
				value="#{msg['customer.gender']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="selectGender"
			value="#{pages$tabFamilyVillageFileBeneficiary.person.gender}">
			<f:selectItem itemLabel="#{msg['tenants.gender.male']}" itemValue="M" />
			<f:selectItem itemLabel="#{msg['tenants.gender.female']}"
				itemValue="F" />
		</h:selectOneMenu>
--%>
	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;">

		<h:commandButton  styleClass="BUTTON" value="#{msg['commons.Add']}"
		   	onclick="javaScript:openFamilyVillageAddBeneficiaryPopup(
																	'#{pages$familyVillageRequest.requestView.inheritanceFileView.inheritanceFileId}',
																		'-1');">
		</h:commandButton>
	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableBeneficairyDetails" rows="15" width="100%"
			value="#{pages$tabFamilyVillageFileBeneficiary.dataList}"
			binding="#{pages$tabFamilyVillageFileBeneficiary.dataTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="beneficiaryNamedtc" sortable="true">
				<f:facet name="header">
					<t:outputText id="beneficiaryNameOT" value="#{msg['commons.Name']}" />
				</f:facet>
				<t:commandLink
					onclick="javascript:openFamilyVillageBeneficiaryPopup('#{dataItem.beneficiary.personId}')"
					value="#{pages$tabFamilyVillageFileBeneficiary.englishLocale? 
																				dataItem.beneficiary.personFullName:
																				dataItem.beneficiary.personFullName}"
					style="white-space: normal;" styleClass="A_LEFT" />


			</t:column>
			<t:column id="dateofbirthcil" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['customer.dateOfBirth']}" />
				</f:facet>
				<h:outputText id="fileDobRfam"
					value="#{dataItem.beneficiary.dateOfBirth}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageFileBeneficiary.dateFormat}"
						timeZone="#{pages$tabFamilyVillageFileBeneficiary.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="gendercol" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['customer.gender']}" />
				</f:facet>
				<h:outputText value="#{dataItem.beneficiary.gender}">

				</h:outputText>
			</t:column>

			<t:column id="action" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAction" value="#{msg['commons.action']}" />
				</f:facet>

				<h:commandLink id="lnkDeleteBeneficiary"
					onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else true;"
					action="#{pages$tabFamilyVillageFileBeneficiary.onDelete}">
					<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
						url="../resources/images/delete.gif" width="18px;" />
				</h:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>

</t:div>


