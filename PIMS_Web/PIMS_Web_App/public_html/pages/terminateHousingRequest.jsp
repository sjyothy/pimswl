<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	function performTabClick(elementid)
	{
			
		disableInputs();
		
		document.getElementById("detailsFrm:"+elementid).onclick();
		
		return 
		
	}
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
	}	
	
	function onMessageFromHousingReason()
	{
	     disableInputs();
	     document.getElementById("detailsFrm:onMessageFromHousingReason").onclick();
	}
	
	
		
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		disableInputs();	
		if( hdnPersonType=='APPLICANT' ) 
		{			    			 
			 document.getElementById("detailsFrm:hdnPersonId").value=personId;
			 document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
			 document.getElementById("detailsFrm:hdnPersonName").value=personName;
			 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
			 document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;
		}
				
	     document.getElementById("detailsFrm:onMessageFromSearchPerson").onclick();
	} 
	
    function populateBeneficiary(inhBenId)
	{
		document.getElementById("detailsFrm:onMessageFromSearchBeneficiary").onclick();
	
	}
	
		
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2"><jsp:include page="header.jsp" /></td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%"><jsp:include
								page="leftmenu.jsp" /></td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel
											value="#{msg['terminatehousingRequest.lbl.heading']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px; width: 100%;">
														<tr>
															<td>
																<h:messages></h:messages>
																<h:outputText id="errorId"
																	value="#{pages$terminateHousingRequest.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$terminateHousingRequest.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$terminateHousingRequest.pageMode}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$terminateHousingRequest.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$terminateHousingRequest.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$terminateHousingRequest.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$terminateHousingRequest.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$terminateHousingRequest.hdnIsCompany}"></h:inputHidden>

																<h:commandLink id="onMessageFromSearchPerson"
																	action="#{pages$terminateHousingRequest.onMessageFromSearchPerson}" />

																<h:commandLink id="onMessageFromHousingReason"
																	action="#{pages$terminateHousingRequest.onMessageFromHousingRejectionReason}" />

																<h:commandLink id="onMessageFromSearchBeneficiary"
																	action="#{pages$terminateHousingRequest.onMessageFromSearchBeneficiary}" />


															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notification.requestnumber']}:" />
															</td>
															<td>
																<h:inputText styleClass="READONLY" readonly="true"
																	value="#{pages$terminateHousingRequest.requestView.requestNumber}"></h:inputText>
															</td>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['request.date']}:"></h:outputLabel>
															</td>
															<td>
																<h:inputText styleClass="READONLY" readonly="true"
																	value="#{pages$terminateHousingRequest.requestView.requestDate}"></h:inputText>
															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.status']}:"></h:outputLabel>
															</td>
															<td>
																<h:inputText styleClass="READONLY" readonly="true"
																	value="#{pages$terminateHousingRequest.englishLocale?pages$terminateHousingRequest.requestView.statusEn:pages$terminateHousingRequest.requestView.statusAr}"></h:inputText>
															</td>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['SearchBeneficiary.researcher']}:"></h:outputLabel>
															</td>
															<td>
																<h:selectOneMenu
																	value="#{pages$terminateHousingRequest.requestView.socialResearcherId}"
																	tabindex="3">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.pleaseSelect']}" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.socialResearcherGroup}" />
																</h:selectOneMenu>
															</td>
														</tr>



														<tr>
															<td>
																<h:outputText id="idSendTo" styleClass="LABEL"
																	rendered="#{pages$terminateHousingRequest.showApproveRejectButton }"
																	value="#{msg['mems.investment.label.sendTo']}" />
															</td>
															<td colspan="3">
																<h:selectOneMenu id="idUserGrps"
																	rendered="#{pages$terminateHousingRequest.showApproveRejectButton }"
																	binding="#{pages$terminateHousingRequest.cmbReviewGroup}"
																	style="width: 200px;">
																	<f:selectItem
																		itemLabel="#{msg['commons.pleaseSelect']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.userGroups}" />
																</h:selectOneMenu>
															</td>
														</tr>


														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	rendered="#{pages$terminateHousingRequest.showApproveRejectButton || pages$terminateHousingRequest.showCompleteButton}"
																	value="#{msg['maintenanceRequest.remarks']}" />
															</td>
															<td colspan="3">
																<t:inputTextarea style="width: 90%;" id="txt_remarks"
																	rendered="#{pages$terminateHousingRequest.showApproveRejectButton || pages$terminateHousingRequest.showCompleteButton}"
																	value="#{pages$terminateHousingRequest.txtRemarks}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</td>
														</tr>
														<tr>
															<td>
																<t:outputLabel styleClass="LABEL"
																	rendered="#{pages$terminateHousingRequest.requestView.statusId== '90018'}"
																	value="#{msg['lbl.header.rejectReason']}:">
																</t:outputLabel>
															</td>
															<td>
																<h:inputText styleClass="READONLY" readonly="true"
																	rendered="#{pages$terminateHousingRequest.requestView.statusId== '90018'}"
																	value="#{pages$terminateHousingRequest.englishLocale?pages$terminateHousingRequest.requestView.housingRequestRejectionReasonEn:pages$terminateHousingRequest.requestView.housingRequestRejectionReasonAr}"></h:inputText>
															</td>
															<td>

																<t:outputLabel styleClass="LABEL"
																	rendered="#{pages$terminateHousingRequest.requestView.statusId== '90018'}"
																	value="#{msg['lbl.requestReasonDesc']}:  "
																	style="padding-right:5px;padding-left:5px;margin-right:26px;"></t:outputLabel>
															</td>
															<td>
																<t:inputTextarea styleClass="TEXTAREA READONLY"
																	rendered="#{pages$terminateHousingRequest.requestView.statusId== '90018'}"
																	readonly="true"
																	value="#{pages$terminateHousingRequest.requestView.housingRequestRejectionReasonDescription}"
																	rows="4" />
															</td>

														</tr>
													</table>


													<!-- Top Fields - End -->




													<div class="AUC_DET_PAD">
														<div class="TAB_PANEL_INNER">


															<rich:tabPanel
																binding="#{pages$terminateHousingRequest.tabPanel}"
																style="width: 100%">

																<rich:tab id="applicationTab"
																	label="#{msg['commons.tab.applicationDetails']}">
																	<%@ include file="ApplicantDetails.jsp"%>
																</rich:tab>

																<rich:tab id="inheritanceBeneficiaryTabId"
																	label="#{msg['mems.inheritanceFile.tabHeading.beneficiary']}">


																	<t:div
																		style="height:240px;overflow-y:scroll;width:100%;">


																		<t:panelGrid styleClass="TAB_DETAIL_SECTION_INNER"
																			id="pnlGrdBeneficiaryDetailsTabFields"
																			cellpadding="1px" width="100%" cellspacing="5px"
																			columns="4">
																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputText
																					value="#{msg['SearchBeneficiary.name']}" />
																			</h:panelGroup>

																			<h:panelGroup>
																				<h:inputText readonly="true" styleClass="READONLY"
																					value="#{pages$terminateHousingRequest.beneficiary.firstName}" />
																				<h:graphicImage title="#{msg['commons.search']}"
																					style="MARGIN: 1px 1px -5px;cursor:hand"
																					onclick="javaScript:openInheritenceBeneficiarySearch( 'familyVillage','MODE_SELECT_ONE_POPUP');"
																					url="../resources/images/app_icons/Search-tenant.png">
																				</h:graphicImage>
																				<h:graphicImage title="#{msg['commons.details']}"
																					rendered="#{pages$terminateHousingRequest.beneficiary ne null && pages$terminateHousingRequest.beneficiary.personId ne null }"
																					style="MARGIN: 1px 1px -5px;cursor:hand"
																					onclick="javascript:openFamilyVillageBeneficiaryPopup('#{pages$terminateHousingRequest.beneficiary.personId}')"
																					url="../resources/images/app_icons/Tenant.png">
																				</h:graphicImage>
																			</h:panelGroup>
																			<h:outputText value="#{msg['commons.dateofbirth']}" />
																			<rich:calendar disabled="true"
																				value="#{pages$terminateHousingRequest.beneficiary.dateOfBirth}"
																				locale="#{pages$terminateHousingRequest.locale}"
																				popup="true"
																				datePattern="#{pages$terminateHousingRequest.dateFormat}"
																				showApplyButton="false" enableManualInput="false" />

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['customer.gender']}:"></h:outputLabel>


																			<h:selectOneMenu id="selectGender" readonly="true"
																				value="#{pages$terminateHousingRequest.beneficiary.gender}">
																				<f:selectItem
																					itemLabel="#{msg['tenants.gender.male']}"
																					itemValue="M" />
																				<f:selectItem
																					itemLabel="#{msg['tenants.gender.female']}"
																					itemValue="F" />
																			</h:selectOneMenu>

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['terminatehousingRequest.lbl.terminationRequestReason']}:"></h:outputLabel>

																			<div class="SCROLLABLE_SECTION"
																				style="height: 100px; width: 298px; overflow-x: hidden; border-width: 1px; border-style: solid; border-color: #a2a2a2;">
																				<h:panelGroup
																					style="border-width: 1px; border-style: solid; border-color: #a2a2a2; background: white; overflow-x: auto; overflow-y: scroll; width: 90%; height: 60px; ">

																					<h:selectOneRadio layout="pageDirection"
																						disabled="#{!pages$terminateHousingRequest.showSaveButton}"
																						value="#{pages$terminateHousingRequest.requestView.terminateHousingReasonId}">
																						<f:selectItems
																							value="#{pages$ApplicationBean.terminateHousingRequestReasonsList}" />
																					</h:selectOneRadio>
																				</h:panelGroup>
																			</div>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['lbl.requestReasonDesc']}:"></h:outputLabel>
																			<h:inputTextarea rows="6" style="width: 320px;"
																				disabled="#{!pages$terminateHousingRequest.showSaveButton}"
																				value="#{pages$terminateHousingRequest.requestView.terminateHousingReasonDescription}" />


																		</t:panelGrid>


																	</t:div>

																</rich:tab>


																<rich:tab id="reviewTab"
																	label="#{msg['commons.tab.reviewDetails']}">
																	<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
																</rich:tab>


																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>
																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>
																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$terminateHousingRequest.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>


																<rich:tab id="commentsTabId"
																	label="#{msg['evaluation.recommendation']}">


																	<t:div
																		style="height:240px;overflow-y:scroll;width:100%;">


																		<t:panelGrid styleClass="TAB_DETAIL_SECTION_INNER"
																			cellpadding="1px" width="100%" cellspacing="5px"
																			columns="2">

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['lbl.director.comments']}:"></h:outputLabel>
																			<h:inputTextarea rows="6" style="width: 320px;"
																				value="#{pages$terminateHousingRequest.requestView.directorComments}" />


																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['lbl.researcher.comments']}:"></h:outputLabel>
																			<h:inputTextarea rows="6" style="width: 320px;"
																				value="#{pages$terminateHousingRequest.requestView.researcherComments}" />

																		</t:panelGrid>


																	</t:div>

																</rich:tab>

															</rich:tabPanel>
														</div>

														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">
															<pims:security
																screen="Pims.EndowMgmt.DonationRequest.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$terminateHousingRequest.showSaveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$terminateHousingRequest.onSave}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$terminateHousingRequest.showSubmitButton}"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink
																	action="#{pages$terminateHousingRequest.onSubmitted}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$terminateHousingRequest.showResubmitForApprovalButton}"
																	value="#{msg['socialResearch.btn.resubmit']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink
																	action="#{pages$terminateHousingRequest.onReSubmitted}" />




																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$terminateHousingRequest.showCompleteButton}"
																	value="#{msg['commons.completeButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink
																	action="#{pages$terminateHousingRequest.onComplete}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$terminateHousingRequest.showPostBackButton}"
																	value="#{msg['commons.sendBack']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink
																	action="#{pages$terminateHousingRequest.onPostBack}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$terminateHousingRequest.showApproveRejectButton}"
																	value="#{msg['commons.approveButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink
																	action="#{pages$terminateHousingRequest.onApprove}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$terminateHousingRequest.showApproveRejectButton}"
																	value="#{msg['commons.sendBack']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink
																	action="#{pages$terminateHousingRequest.onSendBackFromApprover}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$terminateHousingRequest.showApproveRejectButton}"
																	value="#{msg['commons.rejectButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else showAddRejectReasonPopup();">
																</h:commandButton>


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$terminateHousingRequest.showApproveRejectButton}"
																	value="#{msg['commons.review']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink
																	action="#{pages$terminateHousingRequest.onReviewRequired}" />



															</pims:security>

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$terminateHousingRequest.pageMode == 'REVIEW_REQUIRED'}"
																value="#{msg['commons.done']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink
																action="#{pages$terminateHousingRequest.onReviewed}" />

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$terminateHousingRequest.showPrintButton}"
																value="#{msg['commons.print']}"
																action="#{pages$terminateHousingRequest.onPrintReceipt}" />

														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>