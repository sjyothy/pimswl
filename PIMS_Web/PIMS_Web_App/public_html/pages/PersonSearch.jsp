


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->


			<script language="javascript" type="text/javascript">
    function clearWindow()
	{
       
        
            document.getElementById("formSearchPerson:txtfirstname").value="";
			document.getElementById("formSearchPerson:txtlastname").value="";	
			document.getElementById("formSearchPerson:txtpassportNumber").value="";
			document.getElementById("formSearchPerson:txtresidenseVisaNumber").value="";
			document.getElementById("formSearchPerson:txtsocialSecNumber").value="";
		
			
	}
	function addToContract()
	{
	   
	   
	  window.opener.document.forms[0].submit();
	  
	  window.close();
	}
	function sendToParent(PersonName,PersonId)
	{
	var controlName=document.getElementById("formSearchPerson:hdnControlName").value;
	
	var controlForId=document.getElementById("formSearchPerson:hdncontrolForId").value;
	var hdnDisplaycontrol=document.getElementById("formSearchPerson:hdndisplaycontrolName").value;
	window.opener.addSponsorManager(controlName,controlForId,PersonName,PersonId,hdnDisplaycontrol);
	window.close();
	}
	function closeWindow()
	{
	
	  window.opener="x";
	  window.close();
	
	}
	
	  function submitForm()
  		   {
           document.getElementById('formSearchPerson').submit();
		   }
       </SCRIPT>



		</head>
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td colspan="2">
					<jsp:include page="header.jsp" />
				</td>
			</tr>
			<tr width="100%">
				<td class="divLeftMenuWithBody" width="17%">
					<jsp:include page="leftmenu.jsp" />
				</td>
				<td width="83%" valign="top" class="divBackgroundBody">
					<table width="99%" class="greyPanelTable" cellpadding="0"
						cellspacing="0" border="0">
						<tr>
							<td class="HEADER_TD">
								<h:outputLabel value="#{msg['commons.searchPerson']}"
									styleClass="HEADER_FONT" />
							</td>
							<td width="100%">
								&nbsp;
							</td>
						</tr>
					</table>
					<table width="99%" style="margin-left:1px;"
						class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
						border="0" height="100%">
						<tr valign="top">
							<td width="100%" height="100%" valign="top" nowrap="nowrap">
								<div class="SCROLLABLE_SECTION">
									<h:form id="formSearchPerson" style="width:96%;">
									<input type="hidden" name="persontype" value="<%=request.getParameter("persontype")%>" >
										<input type="hidden" name="personGender" value="#{pages$PersonSearch.gender}" >
										<h:outputText id="ccc" escape="false" value="#{pages$PersonSearch.errorMessages}"/>
										<div class="MARGIN">
											<table cellpadding="0" cellspacing="0" width="100%">
												 <tr>
                                                            <td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
                                                            <td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
                                                            <td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
                                                            </tr>
											</table>
											<div class="DETAIL_SECTION">
												
												<c:choose>
												<c:when test="${param.persontype == 'OWNER'}">
												<h:outputLabel value="#{msg['person.owner']}"	styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
												</c:when>
												<c:otherwise>
												<h:outputLabel value="#{msg['person.representative']}"	styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>												
												</c:otherwise>
												</c:choose>
												<table cellpadding="1px" cellspacing="2px"
													class="DETAIL_SECTION_INNER">
													<tr>
														<td>

															&nbsp;
														</td>
													</tr>


													<tr>
														<td>
															&nbsp;&nbsp;
														</td>
														<td width="20%">
															<h:outputLabel styleClass="LABEL" value="#{msg['customer.firstname']}:"></h:outputLabel>
														</td>
														<td>
															<h:inputText size="30" maxlength="15" id="txtfirstname"
																value="#{pages$PersonSearch.firstName}">
															</h:inputText>
														</td>
														<td>
															&nbsp;&nbsp;
														</td>
														<td>
															&nbsp;&nbsp;
														</td>
														<td>
															<h:outputLabel styleClass="LABEL" value="#{msg['customer.lastname']}:"></h:outputLabel>
														</td>

														<td>
															<h:inputText size="30" maxlength="15" id="txtlastname"
																value="#{pages$PersonSearch.lastName}">
															</h:inputText>
														</td>
													</tr>

													<tr>
														<td>
															&nbsp;&nbsp;
														</td>
														<td colspan="1">
															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.country']}:"></h:outputLabel>
														</td>
														<td colspan="1" width="10%">
															<h:selectOneMenu id="country"
																valueChangeListener="#{pages$PersonSearch.loadState}"
																onchange="submitForm()" required="false"
																immediate="false"
																value="#{pages$PersonSearch.selectedCountryId}">
																<f:selectItem itemValue="0" itemLabel="All" />
																<f:selectItems value="#{view.attributes['country']}" />
															</h:selectOneMenu>
														</td>

														<td colspan="2">
															 &nbsp;&nbsp;
														</td>

														<td colspan="1">

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.state']}" />
															:
														</td>
														<td colspan="1" width="10%">
															<h:selectOneMenu id="state"
																style="width: 192px; height: 24px" required="false"
																value="#{pages$PersonSearch.stateId}">
																<f:selectItem itemValue="0" itemLabel="All" />
																<f:selectItems value="#{view.attributes['state']}" />
															</h:selectOneMenu>

														</td>
														<td colspan="1">



														</td>
													</tr>
													<tr>
														<td>
															&nbsp;&nbsp;
														</td>
														<td>
															<h:outputLabel styleClass="LABEL" value="#{msg['customer.socialSecNumber']}"></h:outputLabel>
															:
														</td>
														<td>
															<h:inputText size="30" maxlength="15"
																id="txtsocialSecNumber"
																value="#{pages$PersonSearch.socialSecNumber}">
															</h:inputText>
														</td>
														<td>
															&nbsp;&nbsp;
														</td>

														<td>
															&nbsp;&nbsp;
														</td>
														<td>
															<h:outputLabel styleClass="LABEL" value="#{msg['customer.passportNumber']}"></h:outputLabel>
															:
														</td>
														<td>
															<h:inputText size="30" maxlength="15"
																id="txtpassportNumber"
																value="#{pages$PersonSearch.passportNumber}">
															</h:inputText>
														</td>

													</tr>
													<tr>
														<td>
															&nbsp;&nbsp;
														</td>

														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.residenseVisaNumber']}"></h:outputLabel>
															:
														</td>
														<td>
															<h:inputText size="30" maxlength="15"
																id="txtresidenseVisaNumber"
																value="#{pages$PersonSearch.residenseVisaNumber}">
															</h:inputText>
														</td>

														<td>
															&nbsp;&nbsp;
														</td>

														<td>
															&nbsp;&nbsp;
														</td>

														<td colspan="1">
															<c:choose>
																<c:when test="${param.persontype == 'OWNER'}">
																	<h:outputLabel value="#{msg['person.owner.type']}:"
																		styleClass="LABEL"></h:outputLabel>
																</c:when>
																<c:otherwise>
																	<h:outputLabel
																		value="#{msg['person.representative.type']}:"
																		styleClass="LABEL"></h:outputLabel>
																</c:otherwise>
															</c:choose>
														</td>
														<td colspan="1">
															<h:selectOneMenu id="personSubType"
																style="width: 192px; height: 24px" required="false"
																value="#{pages$PersonSearch.personSubTypeId}">
																<f:selectItem itemValue="0" itemLabel="All" />
																<f:selectItems
																	value="#{pages$PersonSearch.personSubTypeList}" />
															</h:selectOneMenu>
														</td>
														<td></td>
													</tr>
													<tr>
														<td>
															&nbsp;&nbsp;
														</td>
														<td colspan="6" class="BUTTON_TD">
															<h:commandButton immediate="false" type="submit"
																styleClass="BUTTON" value="#{msg['commons.search']}"
																action="#{pages$PersonSearch.loadDataList}"></h:commandButton>

															<h:commandButton type="submit" styleClass="BUTTON"
																action="#{pages$PersonSearch.add}"
																value="#{msg['commons.Add']}" />

															<h:commandButton styleClass="BUTTON" type="button"
																value="#{msg['commons.reset']}" style="width: 54px"
																onclick="javascript:clearWindow();" />

														</td>

													</tr>

												</table>
											</div>
										</div>
										<br>
										<div class="MARGIN">
											<div class="imag">
												&nbsp;
											</div>
											<t:div styleClass="contentDiv" style="width:98%">
												<t:dataTable id="prospective" rows="5" width="100%"
													value="#{pages$PersonSearch.prospectivePersonDataList}"
													binding="#{pages$PersonSearch.propspectivePersonDataTable}"
													preserveDataModel="false" preserveSort="false"
													var="prospectiveDataItem" rowClasses="row1,row2"
													rules="all" renderedIfEmpty="true">
													<t:column sortable="true" id="col_PersonName">
														<f:facet name="header">
															<t:outputText value="#{msg['customer.firstname']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT"
															value="#{prospectiveDataItem.firstName} #{prospectiveDataItem.middleName} #{prospectiveDataItem.lastName}" />
													</t:column>
													<t:column sortable="true" id="col_nationality">
														<f:facet name="header">
															<t:outputText id="lbl_nationality"
																value="#{msg['customer.nationality']}" />

														</f:facet>
														<t:outputText id="txt_nationality" title="" styleClass="A_LEFT"
															value="#{prospectiveDataItem.nationalityEn}" />
													</t:column>
													<t:column sortable="true" id="col_Passport">


														<f:facet name="header">

															<t:outputText id="lbl_Passport"
																value="#{msg['customer.passportNumber']}" />

														</f:facet>
														<t:outputText id="txt_passport" title="" styleClass="A_LEFT"
															value="#{prospectiveDataItem.passportNumber}" />
													</t:column>


													<t:column id="col_Email">


														<f:facet name="header">

															<t:outputText id="lbl_email"
																value="#{msg['contact.email']}" />

														</f:facet>
														<t:outputText id="txt_email" title="" styleClass="A_LEFT"
															value="#{prospectiveDataItem.contactInfoView.email}" />
													</t:column>

													<t:column sortable="true" id="col_DrivingLicense">


														<f:facet name="header">

															<t:outputText id="lbl_driving"
																value="#{msg['customer.drivingLicenseNumber']}" />

														</f:facet>
														<t:outputText id="txt_driving" title="" styleClass="A_LEFT"
															value="#{prospectiveDataItem.drivingLicenseNumber}" />
													</t:column>
													

													<t:column sortable="true" id="col_SSN">


														<f:facet name="header">
															<t:outputText id="lbl_ssn"
																value="#{msg['customer.socialSecNumber']}" />

														</f:facet>
														<t:outputText id="txt_ssn" title="" styleClass="A_LEFT"
															value="#{prospectiveDataItem.socialSecNumber}" />
													</t:column>
														<t:column  id="col_status">


														<f:facet name="header">
															<t:outputText id="lbl_status"
																value="#{msg['commons.status']}" />
														</f:facet>
										 <t:commandLink  rendered="#{prospectiveDataItem.enabled}"    action="#{pages$PersonSearch.enableOrDisable}">&nbsp;
											 <h:graphicImage id="enableIcon" title="#{msg['commons.clickDisable']}" url="../resources/images/app_icons/enable.png" />&nbsp;
										  </t:commandLink>
										  
										  <t:commandLink  rendered="#{prospectiveDataItem.disabled}"  action="#{pages$PersonSearch.enableOrDisable}">&nbsp;
											 <h:graphicImage id="disableIcon" title="#{msg['commons.clickEnable']}" url="../resources/images/app_icons/disable.png" />&nbsp;
										  </t:commandLink>
													
													</t:column>		
												<t:column id="col_del">


														<f:facet name="header">
															<t:outputText id="lbl_delete"
																value="#{msg['commons.delete']}" />
														</f:facet>
														
														<h:commandLink id="deleteCmdLink" action="#{pages$PersonSearch.delete}">
													<h:graphicImage id="delete_Icon" title="#{msg['commons.delete']}" url="../resources/images/delete_icon.png" />
													</h:commandLink>
													
													</t:column>		
													
													<t:column id="col_edit">


														<f:facet name="header">
															<t:outputText id="lbl_edit"
																value="#{msg['commons.edit']}" />
														</f:facet>
														
														<h:commandLink id="editCmdLink" action="#{pages$PersonSearch.edit}">
													<h:graphicImage id="edit_Icon" title="#{msg['commons.delete']}" url="../resources/images/edit-icon.gif" />
													</h:commandLink>
													
													</t:column>															

												</t:dataTable>
											</t:div>
										
				<div class="contentDivFooter" style="width: 100%">
											<div >
												<t:dataScroller id="scroller" for="prospective" paginator="true"
													fastStep="1" paginatorMaxPages="2" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 
												
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

													<f:facet name="first">

														<t:graphicImage url="../resources/images/first_btn.gif" id="lblF"></t:graphicImage>

													</f:facet>

													<f:facet name="last">

														<t:graphicImage url="../resources/images/last_btn.gif" id="lblL"></t:graphicImage>

													</f:facet>

													<f:facet name="fastforward">

														<t:graphicImage url="../resources/images/next_btn.gif" id="lblFF"></t:graphicImage>

													</f:facet>

													<f:facet name="fastrewind">

														  <t:graphicImage url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>
													</f:facet>
												</t:dataScroller>
												</div>
                                           </div>
</div>
									</h:form>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								&nbsp;
							</td>
						</tr>

					</table>



				</td>
			</tr>





		</table>


		</body>
	</html>
</f:view>

