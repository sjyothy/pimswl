<%-- 
  - Author: Danish Farooq/Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for searching a Project
  --%>
  
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
        function sendToParent(projectId,projectName,projectNumber,estimatedCost)
        {
            window.opener.populateProject(projectId,projectName,projectNumber,estimatedCost);
		    window.close();
        }
        
	    function resetValues()
      	{
      		document.getElementById("searchFrm:projectNumber").value="";
      	    document.getElementById("searchFrm:projectName").value="";
			document.getElementById("searchFrm:projectStatusCombo").selectedIndex=0;
			document.getElementById("searchFrm:projectTypeCombo").selectedIndex=0;
			document.getElementById("searchFrm:studyTypeCombo").selectedIndex=0;
			document.getElementById("searchFrm:projectPurposeCombo").selectedIndex=0;
			document.getElementById("searchFrm:propertyTypeCombo").selectedIndex=0;
			document.getElementById("searchFrm:propertyOwnershipTypeCombo").selectedIndex=0;
			document.getElementById("searchFrm:projectSizeCombo").selectedIndex=0;
			document.getElementById("searchFrm:projectCategoryCombo").selectedIndex=0;
			document.getElementById("searchFrm:landNumber").value="";
			document.getElementById("searchFrm:projectDescription").value="";
        }
        
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

</head>
	<body class="BODY_STYLE" style="height:950px;">
			  <c:choose>
			<c:when test="${!pages$projectSearch.isViewModePopUp}">
					<div class="containerDiv" style="width:100%;height:900px;">
		     </c:when>
		     </c:choose>
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header --> 
	<table width="100%" cellpadding="0" cellspacing="0" border="0" style="height:900px;">
		  <c:choose>
			<c:when test="${!pages$projectSearch.isViewModePopUp}">
				<tr>
					<td colspan="2">
						<jsp:include page="SDTPS_header.jsp" />
					</td>
				</tr>
		   </c:when>
		</c:choose>
		<tr >
		      <c:choose>
				<c:when test="${!pages$projectSearch.isViewModePopUp}">
					<td class="divLeftMenuWithBody" width="17%" style="height:900px;">
						<jsp:include page="leftmenu.jsp" />
					</td>
			    </c:when>
		      </c:choose>
			
			<td width="83%" valign="top" class="divBackgroundBody" style="height:900px;">
				<table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['study.manage.tab.studyParticipant.btn.addInternalParticipants']}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>
				<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
					<tr valign="top">
						<td >
						<div class="SCROLLABLE_SECTION" style="height:900px;"> 
									<h:form id="searchFrm" style="width:98%;">
									<t:div styleClass="MESSAGE">
										<table border="0" class="layoutTable">
											<tr>
												<td>
													<h:outputText value="#{pages$projectSearch.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$projectSearch.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
									</t:div>
								<div class="MARGIN" > 
									<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
									<div class="DETAIL_SECTION">
										<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
										<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
										<tr>
										<td width="97%">
										<table width="100%">
											<tr>
												<td width="25%">
													<h:outputLabel styleClass="LABEL" value="#{msg['project.projectNumber']}:" />
												</td>
												<td width="25%">
													<h:inputText id="projectNumber"  value="#{pages$projectSearch.projectNumber}"></h:inputText>
												</td>
												<td width="25%">
													<h:outputLabel styleClass="LABEL" value="#{msg['project.projectName']}:" />
												</td>
												<td width="25%">
													<h:inputText id="projectName"  value="#{pages$projectSearch.projectName}"></h:inputText>
												</td>
											</tr>
											<tr>
											    <td >
													<h:outputLabel styleClass="LABEL" value="#{msg['project.projectStatus']}:" />
												</td>
												<td >
												<h:selectOneMenu id="projectStatusCombo"  required="false"
														value="#{pages$projectSearch.projectStatus}">
														<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
														<f:selectItems value="#{pages$projectSearch.projectStatusesList}" />
												</h:selectOneMenu>
												</td>
												<td >
													<h:outputLabel styleClass="LABEL" value="#{msg['project.projectType']}:" />
												</td>
												<td >
												<h:selectOneMenu  id="projectTypeCombo"  required="false"
														value="#{pages$projectSearch.projectType}" binding="#{pages$projectSearch.cmbProjectType}" >
														<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
														<f:selectItems  value="#{pages$projectSearch.projectTypeList}"  />
												</h:selectOneMenu>
												</td>
											</tr>
											<tr>
											    <td >
													<h:outputLabel styleClass="LABEL" value="#{msg['project.relatedStudy']}:" />
												</td>
												<td >
												<h:selectOneMenu id="studyTypeCombo"  required="false"
														value="#{pages$projectSearch.studyTypeId}">
														<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
														<f:selectItems value="#{pages$ApplicationBean.studyTypes}" />
												</h:selectOneMenu>
												</td>
												<td >
													<h:outputLabel styleClass="LABEL" value="#{msg['project.purpose']}:" />
												</td>
												<td >
												<h:selectOneMenu  id="projectPurposeCombo"  required="false"
														value="#{pages$projectSearch.projectPurposeId}" >
														<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
														<f:selectItems value="#{pages$ApplicationBean.projectPurposeList}" />
												</h:selectOneMenu>
												</td>
											</tr>
										
											<tr>
											    <td >
													<h:outputLabel styleClass="LABEL" value="#{msg['project.size']}:" />
												</td>
												<td >
												<h:selectOneMenu id="projectSizeCombo"  required="false"
														value="#{pages$projectSearch.projectSizeId}">
														<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
														<f:selectItems value="#{pages$ApplicationBean.projectSizeList}" />
												</h:selectOneMenu>
												</td>
												<td >
													<h:outputLabel styleClass="LABEL" value="#{msg['project.category']}:" />
												</td>
												<td >
												<h:selectOneMenu  id="projectCategoryCombo"  required="false"
														value="#{pages$projectSearch.projectCategoryId}" >
														<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
														<f:selectItems value="#{pages$ApplicationBean.projectCategoryList}" />
												</h:selectOneMenu>
												</td>
											</tr>
											<tr>
												
												<td >
													<h:outputLabel styleClass="LABEL" value="#{msg['project.description']}:" />
												</td>
												<td >
													<h:inputText id="projectDescription"  value="#{pages$projectSearch.projectDescription}"></h:inputText>
												</td>
											</tr>
											<tr>
												<td class="BUTTON_TD" colspan="6" >
												
												<pims:security screen="Pims.ConstructionMgmt.ProjectSearch.Search" action="create">
													<h:commandButton styleClass="BUTTON" value="#{msg['commons.search']}" action="#{pages$projectSearch.btnSearch_Click}" style="width: 75px" ></h:commandButton>
												</pims:security>
												   	<h:commandButton styleClass="BUTTON" type="button" value="#{msg['commons.clear']}" onclick="javascript:resetValues();" style="width: 75px" ></h:commandButton>
												<c:choose>
												     <c:when test="${!pages$projectSearch.isViewModePopUp}">
														<pims:security screen="Pims.ConstructionMgmt.ProjectSearch.AddProject" action="create">
																<h:commandButton styleClass="BUTTON" value="#{msg['project.search.addProjectButton']}" action="#{pages$projectSearch.onAddProject}" style="width: 90px" tabindex="7"></h:commandButton>
															</pims:security>											   		
														
											         </c:when>
											     </c:choose>
												
												</td>
											</tr>
											</table>
											</td>
											<td width="3%">
											</td>
											</tr>
										</table>
								</div>	
								</div>
								 <div style="padding-bottom:7px;padding-left:10px;padding-right:7px;padding-top:7px;">   
                                      <div class="imag">&nbsp;</div>
										<div class="contentDiv">
											<t:dataTable id="dt1"
												value="#{pages$projectSearch.projectDataList}"
												binding="#{pages$projectSearch.dataTable}"
												rows="#{pages$projectSearch.paginatorRows}" 
												preserveDataModel="false" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												width="100%">

												<t:column id="projectNumberCol" sortable="true" >
													<f:facet  name="header">
														<t:outputText value="#{msg['contract.person.Manager']}" />
													</f:facet>
													<t:outputText value="#{dataItem.projectNumber}" styleClass="A_LEFT"/>
												</t:column>
												<t:column id="projectNameCol" sortable="true" >
													<f:facet  name="header">
														<t:outputText value="#{msg['project.projectName']}" />
													</f:facet>
													<t:outputText value="" styleClass="A_LEFT"/>
												</t:column>
												
												<t:column id="projectTypeAr" sortable="true" rendered="#{pages$projectSearch.isArabicLocale}">
													<f:facet  name="header">
														<t:outputText value="#{msg['project.projectType']}" />
													</f:facet>
													<t:outputText value="" styleClass="A_LEFT"/>
												</t:column>
												
												<t:column id="projectPurposeColAr" sortable="true" rendered="#{pages$projectSearch.isArabicLocale}">
													<f:facet  name="header">
														<t:outputText value="#{msg['project.purpose']}" />
													</f:facet>
													<t:outputText value="" styleClass="A_LEFT"/>
												</t:column>
												
												<t:column id="projectStatusAr" sortable="true" rendered="#{pages$projectSearch.isArabicLocale}">
													<f:facet  name="header">
														<t:outputText value="#{msg['project.projectStatus']}" />
													</f:facet>
													<t:outputText value="" styleClass="A_LEFT"/>
												</t:column>
												
                                               <t:column id="actionCol" sortable="false" width="15%" rendered="#{pages$projectSearch.isFullMode}" styleClass="ACTION_COLUMN">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.action']}" />
													</f:facet>
												
												</t:column>
											</t:dataTable>
										</div>
										<t:div styleClass="contentDivFooter" style="width:99%;" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$projectSearch.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
												<CENTER>
													<t:dataScroller id="scroller" for="dt1" paginator="true"  
														fastStep="1" paginatorMaxPages="#{pages$projectSearch.paginatorMaxPages}" immediate="false"
														paginatorTableClass="paginator"
														renderFacetsIfSinglePage="true" 												
														paginatorTableStyle="grid_paginator" layout="singleTable" 
														paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
														paginatorActiveColumnStyle="font-weight:bold;"
														paginatorRenderLinkForActive="false" 
														pageIndexVar="pageNumber"
														styleClass="SCH_SCROLLER">
														<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
														<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>
													</t:dataScroller>
													</CENTER>
													
												</td></tr>
											</table>
									</t:div>
									</div>	
									
								
						
						
							</h:form>
						</div> 	
					  
						</td>			
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="footer">
		
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<c:choose>
	  <c:when test="${!pages$projectSearch.isViewModePopUp}">
		</div>
	  </c:when>
	</c:choose>

</body>
</html>
</f:view>