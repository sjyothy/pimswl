

<t:div id="tabDDetails" style="width:100%;">

	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="prdc" width="100%"
			rows="#{pages$manageEndowmentRevenueDistribution.paginatorRows}"
			value="#{pages$manageEndowmentRevenueDistribution.dataList}"
			binding="#{pages$manageEndowmentRevenueDistribution.dataTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="unitNumber">
				<f:facet name="header">
					<t:outputText value="#{msg['grp.UnitNumber']}" />
				</f:facet>
				<t:outputText value="#{dataItem.unitNumber}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>

			<t:column id="contractAmount">
				<f:facet name="header">
					<t:outputText value="#{msg['contract.Amount']}" />
				</f:facet>

				<t:outputText value="#{dataItem.contractAmount}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="realizedAmount">
				<f:facet name="header">
					<t:outputText value="#{msg['settlement.label.totalrealizedamt']}" />
				</f:facet>

				<t:outputText value="#{dataItem.realizedAmount}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>

			<t:column id="utilizedAmount">
				<f:facet name="header">
					<t:outputText value="#{msg['endowment.lbl.utilizedAmount']}" />
				</f:facet>

				<t:outputText value="#{dataItem.utilizedAmount}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			
			<t:column id="distributedAmount">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.distributedAmount']}" />
				</f:facet>

				<t:outputText value="#{dataItem.distributedAmount}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="amountToDistribute">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.amountToDistribute']}" />
				</f:facet>

				<t:outputText value="#{dataItem.amountToDistribute}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>

			<t:column id="actionTPRD" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdActionTPRD" value="#{msg['commons.action']}" />
				</f:facet>

				<t:commandLink
					action="#{pages$manageEndowmentRevenueDistribution.onDistributeSingle}"
					rendered="#{
								dataItem.amountToDistribute > 0 && 
								dataItem.distributedAmount != dataItem.amountToDistribute 
							   }"
							  >
					<h:graphicImage style="cursor:hand;"
						title="#{msg['commons.distribute']}"
						url="../resources/images/app_icons/Add-fee.png" />
				</t:commandLink>

			</t:column>
		</t:dataTable>
	</t:div>
	<t:div id="contentDivFooter"
		styleClass="contentDivFooter AUCTION_SCH_RF"
		style="width:100%;#width:100%;">

		<t:panelGrid id="RECORD_NUM_TD" cellpadding="0" cellspacing="0"
			width="100%" columns="2" columnClasses="RECORD_NUM_TD,BUTTON_TD">

			<t:div styleClass="RECORD_NUM_TD" style="width:50%">
				<t:panelGrid id="RECORD_NUM__INSTD" cellpadding="0" cellspacing="0"
					width="88%" columns="2" styleClass="RECORD_NUM_BG"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD">

					<h:outputText value="#{msg['commons.recordsFound']}:" />
					<h:outputText
						value="#{pages$manageEndowmentRevenueDistribution.recordSize}" />

				</t:panelGrid>


			</t:div>

			<t:div id="contentDivFooterColumnTwo" styleClass="BUTTON_TD"
				style="width: 53%; # width: 50%;">


				<TABLE border="0" class="SCH_SCROLLER">
					<tr>
						<td>
							<t:commandLink
								action="#{pages$manageEndowmentRevenueDistribution.pageFirst}"
								disabled="#{pages$manageEndowmentRevenueDistribution.firstRow == 0}">
								<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>

							</t:commandLink>
						</td>
						<td>
							<t:commandLink
								action="#{pages$manageEndowmentRevenueDistribution.pagePrevious}"
								disabled="#{pages$manageEndowmentRevenueDistribution.firstRow == 0}">
								<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
							</t:commandLink>
						</td>
						<td>
							<t:dataList
								value="#{pages$manageEndowmentRevenueDistribution.pages}"
								var="page">
								<h:commandLink value="#{page}"
									actionListener="#{pages$manageEndowmentRevenueDistribution.page}"
									rendered="#{page != pages$manageEndowmentRevenueDistribution.currentPage}" />
								<h:outputText value="<b>#{page}</b>" escape="false"
									rendered="#{page == pages$manageEndowmentRevenueDistribution.currentPage}" />
							</t:dataList>
						</td>
						<td>
							<t:commandLink
								action="#{pages$manageEndowmentRevenueDistribution.pageNext}"
								disabled="#{pages$manageEndowmentRevenueDistribution.firstRow + pages$manageEndowmentRevenueDistribution.rowsPerPage >= pages$manageEndowmentRevenueDistribution.totalRows}">
								<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
							</t:commandLink>
						</td>
						<td>
							<t:commandLink
								action="#{pages$manageEndowmentRevenueDistribution.pageLast}"
								disabled="#{pages$manageEndowmentRevenueDistribution.firstRow + pages$manageEndowmentRevenueDistribution.rowsPerPage >= pages$manageEndowmentRevenueDistribution.totalRows}">
								<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
							</t:commandLink>

						</td>
					</tr>
				</TABLE>
			</t:div>

		</t:panelGrid>
	</t:div>

</t:div>


