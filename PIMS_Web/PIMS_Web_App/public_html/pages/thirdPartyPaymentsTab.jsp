<t:div id="thirdPartytabDDetails" style="width:100%;">
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTablePayments" rows="15" width="100%"
			value="#{pages$thirdPartyPaymentsTab.dataList}"
			binding="#{pages$thirdPartyPaymentsTab.dataTable}"
			preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
			<t:column id="tppRrequestNumber" sortable="true">
				<f:facet name="header">
					<t:outputText id="requestNumbar"
						value="#{msg['commons.requestNumber']}" />
				</f:facet>
				<t:outputText id="requestNumbField" styleClass="A_LEFT"
					value="#{dataItem.requestNumber}" />
			</t:column>
			<t:column id="tppRrequestCreatedOn" sortable="true">
				<f:facet name="header">
					<t:outputText id="requestNumb" value="#{msg['commons.createdOn']}" />
				</f:facet>
				<t:outputText id="createdOnField" styleClass="A_LEFT"
					value="#{dataItem.requestCreatedOn}">
					<f:convertDateTime
						timeZone="#{pages$thirdPartyPaymentsTab.timeZone}"
						pattern="#{pages$thirdPartyPaymentsTab.dateFormat}" />
				</t:outputText>
			</t:column>
			<t:column id="payNumber" sortable="true">
				<f:facet name="header">
					<t:outputText id="payNum"
						value="#{msg['chequeList.paymentNumber']}" />
				</f:facet>
				<t:outputText id="payNoField" styleClass="A_LEFT"
					value="#{dataItem.paymentNumber}" />
			</t:column>

			<t:column id="forRebuildcol" sortable="true">
				<f:facet name="header">
					<t:outputText id="forRebuildHeader"
						value="#{msg['thirdPartyUnit.label.forRebuild']}" />
				</f:facet>
				<t:outputText id="forRebuildRow" styleClass="A_LEFT"
					value="#{!empty dataItem.forRebuild && dataItem.forRebuild=='1'?msg['commons.true']:msg['commons.false']}" />
			</t:column>

			<t:column id="payMode" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdMethod"
						value="#{msg['cancelContract.payment.paymentmethod']}" />
				</f:facet>
				<t:outputText id="paymentMode" styleClass="A_LEFT"
					value="#{dataItem.paymentModeAr}" />
			</t:column>


			<t:column id="amntCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAmnt"
						value="#{msg['mems.payment.request.label.Amount']}" />
				</f:facet>
				<t:outputText id="amountt" styleClass="A_LEFT"
					value="#{dataItem.amountStr}" />
			</t:column>
			<t:column id="statusCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdStatus" value="#{msg['commons.status']}" />
				</f:facet>
				<t:outputText id="psStatus" styleClass="A_LEFT"
					value="#{dataItem.statusAr}" />
			</t:column>

			<t:column id="distributedByAr" sortable="true">
				<f:facet name="header">
					<t:outputText id="distributedBy"
						value="#{msg['donationBox.lbl.distributedBy']}" />
				</f:facet>
				<t:outputText id="distributedByOT" styleClass="A_LEFT"
					value="#{dataItem.distributedByAr}" />
			</t:column>
			<t:column id="distributedOn" sortable="true">
				<f:facet name="header">
					<t:outputText id="distributedOnF"
						value="#{msg['donationBox.lbl.distributedOn']}" />
				</f:facet>
				<t:outputText id="distributedOnOT" styleClass="A_LEFT"
					value="#{dataItem.distributedOn}" />
			</t:column>

			<t:column id="action" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAction" value="#{msg['commons.action']}" />
				</f:facet>
				<h:commandLink id="lnkViewDistribution"
					rendered="#{dataItem.forRebuild=='0'}"
					action="#{pages$thirdPartyPaymentsTab.onDistribute}">
					<h:graphicImage id="viewDistributionIcon" style="cursor:hand;"
						title="#{msg['distributeEndRevenue.tooltip.viewDistribution']}"
						url="../resources/images/app_icons/view_tender.png" width="18px;" />
				</h:commandLink>
				
			</t:column>
		</t:dataTable>
	</t:div>

	<t:div id="pagingDivExpenses" styleClass="contentDivFooter"
		style="width:99.2%;">
		<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
			cellspacing="1">
			<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
				<t:div styleClass="JUG_NUM_REC_ATT">
					<h:outputText value="#{msg['commons.records']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$thirdPartyPaymentsTab.recordSize}" />
				</t:div>
			</t:panelGroup>
			<t:panelGroup>
				<t:dataScroller id="scrollerPaymentsList" for="dataTablePayments"
					paginator="true" fastStep="1" paginatorMaxPages="15"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
					styleClass="SCH_SCROLLER" pageIndexVar="pageNumber"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false">
					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="lblFExpensesList"></t:graphicImage>
					</f:facet>
					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="lblFRExpensesList"></t:graphicImage>
					</f:facet>
					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="lblFFExpensesList"></t:graphicImage>
					</f:facet>
					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="lblLExpensesList"></t:graphicImage>
					</f:facet>
					

				</t:dataScroller>
			</t:panelGroup>
		</t:panelGrid>
	</t:div>
</t:div>


