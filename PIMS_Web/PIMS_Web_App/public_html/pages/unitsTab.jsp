<h:panelGrid columns="2" columnClasses="presidentDiscussionColumn"
   rendered="#{pages$inspectProperty.unitsCurrent}">


<t:dataTable id="test" width="100%"
	value="#{pages$inspectProperty.unitsList}"
	binding="#{pages$inspectProperty.unitsDataTable}" rows="15"
	preserveDataModel="false" preserveSort="false" var="dataItemUnits"
	rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

	<t:column id="col1" width="20">

		<f:facet name="header">

			<t:outputText value="ID" />

		</f:facet>
		<t:outputText value="#{dataItemUnits.floorNumber}" />
	</t:column>


	<t:column id="col2" sortable="true" width="20%">
		<f:facet name="header">


			<t:outputText value="English" />

		</f:facet>
		<t:outputText value="#{dataItemUnits.floorNumber}" />
	</t:column>


	<t:column id="col3" sortable="true">


		<f:facet name="header">

			<t:outputText value="Arabic" />

		</f:facet>
		<t:outputText value="#{dataItemUnits.floorNumber}" />
	</t:column>

</t:dataTable>
</div>
<div class="contentDivFooter">
	<t:dataScroller id="scroller2" for="test" paginator="true" fastStep="1"
		paginatorMaxPages="2" immediate="true" styleClass="scroller"
		paginatorTableClass="paginator" renderFacetsIfSinglePage="false"
		paginatorTableStyle="grid_paginator" layout="singleTable"
		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
		paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

		<f:facet name="first">

			<t:outputLabel value="First" id="lblF2" />

		</f:facet>

		<f:facet name="last">

			<t:outputLabel value="Last" id="lblL2" />

		</f:facet>

		<f:facet name="fastforward">

			<t:outputLabel value="FFwd" id="lblFF2" />

		</f:facet>

		<f:facet name="fastrewind">

			<t:outputLabel value="FRev" id="lblFR2" />

		</f:facet>

	</t:dataScroller>
	</h:panelGrid>