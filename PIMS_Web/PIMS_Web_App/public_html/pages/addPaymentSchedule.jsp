<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	function closeWindow()
	{
		 window.close();
	}        
	function populatePaymentDetails() 
	{			
		document.getElementById("PaymentScheduleForm:populatePayments").onclick();
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<c:choose>
				<c:when test="${!pages$addPaymentSchedule.viewMode}">
					<div class="containerDiv">
				</c:when>
			</c:choose>
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<c:choose>
					<c:when test="${!pages$addPaymentSchedule.viewMode}">
						<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>
					</c:when>
				</c:choose>


				<tr width="100%">

					<c:choose>
						<c:when test="${!pages$addPaymentSchedule.viewMode}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>

					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['paymentDetailsPopUp.header']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="PaymentScheduleForm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$addPaymentSchedule.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText value="#{pages$addPaymentSchedule.successMessages}"
																	escape="false" styleClass="INFO_FONT" />																
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT" /></td>
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>	
														<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT" /></td>	
													</tr>
												</table>
												
												<div class="DETAIL_SECTION" style="width: 100%;">
													<h:outputLabel value="#{msg['mems.normaldisb.label.installmentpopup.heading']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">
																	<tr>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['mems.normaldisb.label.paymentType']}" />
																		</td>
																		<td width="25%">
																			<h:inputText 
																				value="#{pages$addPaymentSchedule.isEnglishLocale?pages$addPaymentSchedule.disbursementDetails.paymentTypeEn:pages$addPaymentSchedule.disbursementDetails.paymentTypeAr}"
																					readonly="true"
																					styleClass="READONLY"
																					>
																			
																			</h:inputText>
																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.amount']}" 
																				/>
																		</td>
																		<td width="25%">
																			<h:inputText value="#{pages$addPaymentSchedule.disbursementDetails.amount}"
																					readonly="true"
																					styleClass="READONLY">
																			</h:inputText>
																		</td>
																	</tr>
																	<tr>																																		<td width="25%">
																		<h:outputLabel styleClass="LABEL"
																				value="#{msg['mems.normaldisb.label.installment']}" />
																		</td>
																		<td width="25%">
																			<h:inputText maxlength="20" binding="#{pages$addPaymentSchedule.txtInstallments}" />																																																																
																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['mems.normaldisb.label.disburseEvery']}" />	
																		</td>
																		<td width="25%">
																			<h:selectOneMenu id="disburseEvery"																																																										
																				binding="#{pages$addPaymentSchedule.cmbDisburseEvery}"
																				style="width: 200px;">																																							
																				<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																				itemValue="-1" />
																				<f:selectItems value="#{pages$addPaymentSchedule.disburseEvery}"/>
																			</h:selectOneMenu>
																		</td>
																	</tr>
																	<tr>
																		<td width="40%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['mems.normaldisb.label.firstDate']}"></h:outputLabel>																			
																		</td>
																		
																		<td width="40%">
																			<rich:calendar id="createdOnDateId"
																				styleClass="READONLY" disabled="false"
																				binding="#{pages$addPaymentSchedule.clndFirstInstallmentDate}"
																				popup="true"
																				datePattern="#{pages$addPaymentSchedule.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:185px;height:17px" />
																		</td>
																	
																	<tr>
																		<td colspan="4" class="BUTTON_TD">
																		<%-- 
																			<h:commandButton type="submit" styleClass="BUTTON"
																				rendered="#{!pages$addPaymentSchedule.renderEditPaymentIcon}"
																				action="#{pages$addPaymentSchedule.addSchedule}"
																				value="#{msg['commons.Add']}" />
																				--%>
																			<h:commandButton type="submit" styleClass="BUTTON"
																				action="#{pages$addPaymentSchedule.addSchedule}"
																				value="#{msg['commons.Add']}" />
																				
																			<h:commandLink id="populatePayments" styleClass="BUTTON"
																				style="width: auto;visibility: hidden;"
																				action="#{pages$addPaymentSchedule.handlePaymentDetailsChange}">																																
																			</h:commandLink>																																							
																		</td>
																</table>
															</td>
														</tr>
													</table>
												</div>
												<t:div styleClass="contentDiv"
													style="width:98%;margin-top: 5px;">
													<t:dataTable id="pymtScId" rows="15" width="100%"
														value="#{pages$addPaymentSchedule.paymentDetails}"
														binding="#{pages$addPaymentSchedule.scheduleTable}"
														preserveDataModel="false" preserveSort="false"
														var="unitDataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true">
														<t:column id="dateId" sortable="true">
															<f:facet name="header">
																<t:outputText id="hdDateId"
																	value="#{msg['commons.date']}" />
															</f:facet>
															<t:outputText id="hdDateId_" styleClass="A_LEFT"
																value="#{unitDataItem.refDate}" >
																<f:convertDateTime pattern="#{pages$addPaymentSchedule.dateFormat}" timeZone="#{pages$addPaymentSchedule.timeZone}" />
															</t:outputText>
														</t:column>
														<t:column id="amnt" sortable="true">
															<f:facet name="header">
																<t:outputText id="hdAmnt"
																	value="#{msg['commons.amount']}" />
															</f:facet>
															<t:outputText id="amountt" styleClass="A_LEFT"
																value="#{unitDataItem.amount}" />
														</t:column>						
														
														<t:column id="actionId" sortable="true">
															<f:facet name="header">
																<t:outputText id="hdActions"
																	value="#{msg['commons.action']}" />
															</f:facet>
															<h:commandLink
																action="#{pages$addPaymentSchedule.editPaymentDetails}"
																rendered="#{pages$addPaymentSchedule.renderEditPaymentIcon}">
																<h:graphicImage id="editPaymentId"
																	title="#{msg['mems.normaldisb.label.editPaymentDetails']}"
																	url="../resources/images/app_icons/Pay-Fine.png" width="18px;" />
															</h:commandLink>
														</t:column>								
													</t:dataTable>
												</t:div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:99.1%;">
													<t:panelGrid columns="2" border="0" width="100%"
														cellpadding="1" cellspacing="1">
														<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
															<t:div styleClass="JUG_NUM_REC_ATT">
																<h:outputText value="#{msg['commons.records']}" />
																<h:outputText value=" : " />
																<h:outputText
																	value="#{pages$addPaymentSchedule.totalRows}" />
															</t:div>
														</t:panelGroup>
														<t:panelGroup>
															<t:dataScroller id="pymScroller" for="pymtScId"
																paginator="true" fastStep="1" immediate="false"
																paginatorTableClass="paginator"
																renderFacetsIfSinglePage="true"
																paginatorTableStyle="grid_paginator"
																layout="singleTable"
																paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																paginatorActiveColumnStyle="font-weight:bold;"
																paginatorRenderLinkForActive="false"
																pageIndexVar="pageNumber" styleClass="JUG_SCROLLER">
																<f:facet name="first">
																	<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
																</f:facet>
																<f:facet name="fastrewind">
																	<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
																</f:facet>
																<f:facet name="fastforward">
																	<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
																</f:facet>
																<f:facet name="last">
																	<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
																</f:facet>
																<t:div styleClass="PAGE_NUM_BG" rendered="true">
																	<h:outputText styleClass="PAGE_NUM"
																		value="#{msg['commons.page']}" />
																	<h:outputText styleClass="PAGE_NUM"
																		style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																		value="#{requestScope.pageNumber}" />
																</t:div>
															</t:dataScroller>
														</t:panelGroup>
													</t:panelGrid>
												</t:div>
												
												
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr><td height="5px"></td></tr>
													<tr>
														<td class="BUTTON_TD MARGIN" colspan="6">																																											
															<h:commandButton
																id="idSaveBtn"
																value="#{msg['commons.done']}"
																styleClass="BUTTON"																																													
																action="#{pages$addPaymentSchedule.done}">																																																																		
															</h:commandButton>
														</td>
													</tr>
												</table
															
												

											</div>
										</div>
									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
					<td colspan="2">
						<c:choose>
							<c:when test="${!pages$addPaymentSchedule.viewMode}">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</tr>
								</table>
							</c:when>
						</c:choose>
					</td>
				</tr>
			</table>
			<c:choose>
				<c:when test="${!pages$addPaymentSchedule.viewMode}">
					</div>
				</c:when>
			</c:choose>
		</body>
	</html>
</f:view>