<%-- Author: Kamran Ahmed--%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<t:panelGrid border="0" width="100%" cellpadding="1" cellspacing="1"
	id="ManageBeneficiaryBasicInfoTabControls">
	<t:panelGroup>
		<%--Column 1,2 Starts--%>
		<t:panelGrid columns="4"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2">
			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.beneficiary']}" />
			<h:inputText styleClass="READONLY" readonly="true"
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.personFullName}" />

			<h:outputText
				value="#{msg['commons.status']}" />
			<h:inputText styleClass="READONLY" readonly="true"
				value="#{pages$ManageBeneficiaryBasicInfoTab.englishLocale ?pages$ManageBeneficiaryBasicInfoTab.beneficiary.minorStatusEn:
						pages$ManageBeneficiaryBasicInfoTab.beneficiary.minorStatusAr
				}" />

			<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*" />
				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.passportName']}" />
			</h:panelGroup>
			<h:panelGroup>
				<h:inputText
					value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.passportName}" />
				<h:graphicImage title="#{msg['commons.view']}"
					url="../resources/images/app_icons/Tenant.png"
					style="MARGIN: 0px 0px -4px; CURSOR: hand;"
					onclick="javaScript:showPersonReadOnlyPopup('#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.personId}');" />
			</h:panelGroup>
			<%-- 
			<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*" />
				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.relationship']}" />
			</h:panelGroup>
			<h:selectOneMenu id="relationshipIdBeneficiary"
				styleClass="SELECT_MENU"
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.relationshipString}">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$ApplicationBean.relationList}" />
			</h:selectOneMenu>

			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.relationshipDesc']}" />
			<h:inputText  value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.relationshipDesc}"/>
--%>

			<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*" />
				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.gender']}" />
			</h:panelGroup>
			<h:selectOneMenu id="selectGenderBeneficiary"
				styleClass="SELECT_MENU"
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.gender}">
				<f:selectItem itemLabel="#{msg['tenants.gender.male']}"
					itemValue="M" />
				<f:selectItem itemLabel="#{msg['tenants.gender.female']}"
					itemValue="F" />
			</h:selectOneMenu>

			<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*" />
				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.nationality']}" />
			</h:panelGroup>
			<h:selectOneMenu id="nationalityBeneficiary" immediate="false"
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.nationalityIdString}">
				<f:selectItem itemValue="-1"
					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
				<f:selectItems value="#{pages$ApplicationBean.countryList}" />
			</h:selectOneMenu>


			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.nationalId']}" />
			<h:inputText
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.personalSecCardNo}" />

			<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*" />
				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.birthDate']}" />
			</h:panelGroup>
			<rich:calendar
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.dateOfBirth}"
				locale="#{pages$inheritanceFileBasicInfoTab.locale}" popup="true"
				datePattern="#{pages$inheritanceFileBasicInfoTab.dateFormat}"
				showApplyButton="false" enableManualInput="false" />

			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.birthPlace']}" />
			<h:selectOneMenu id="birthPlaceIdBeneficiary" immediate="false"
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.birthPlaceId}">
				<f:selectItem itemValue="-1"
					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
				<f:selectItems value="#{pages$ApplicationBean.countryList}" />
			</h:selectOneMenu>

			<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*" />
				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.resCountry']}" />
			</h:panelGroup>
			<h:selectOneMenu id="resCountryIdBeneficiary"
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.countryId}">
				<f:selectItem itemValue="-1"
					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
				<f:selectItems value="#{pages$ApplicationBean.countryList}" />
				<a4j:support event="onchange" reRender="resCityIdBeneficiary"
					action="#{pages$ManageBeneficiaryBasicInfoTab.countryChanged}" />
			</h:selectOneMenu>

			<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*" />
				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.resCity']}" />
			</h:panelGroup>
			<h:selectOneMenu id="resCityIdBeneficiary" immediate="false"
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.cityId}">
				<f:selectItem itemValue="-1"
					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
				<f:selectItems
					value="#{pages$ManageBeneficiaryBasicInfoTab.cityList}" />
			</h:selectOneMenu>
			<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*" />
				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.address']}" />
			</h:panelGroup>
			<h:inputText
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.address1}" />


			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.contactNumber']}" />
			<h:inputText
				value="#{
							!empty pages$ManageBeneficiaryBasicInfoTab.beneficiary.cellNumber?pages$ManageBeneficiaryBasicInfoTab.beneficiary.cellNumber:
							pages$ManageBeneficiaryBasicInfoTab.beneficiary.homePhone}" />

			<h:outputText value="#{msg['mems.inheritanceFile.fieldLabel.poBox']}" />
			<h:inputText
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.postCode}" />



			<h:outputText value="#{msg['mems.inheritanceFile.fieldLabel.email']}" />
			<h:inputText
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.email}" />

			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.motherTongue']}" />
			<h:inputText
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.motherTongue}" />

			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.passportType']}" />
			<h:selectOneMenu id="isMasroom"
				binding="#{pages$ManageBeneficiaryBasicInfoTab.cmbPassportType}">
				<f:selectItem itemValue="-1"
					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
				<f:selectItems value="#{pages$ApplicationBean.passportTypeList}" />
				<a4j:support event="onchange"
					reRender="passportNumberId,masroomNumberId,passportNumberIdLbl,masroomNumberIdLbl"
					action="#{pages$ManageBeneficiaryBasicInfoTab.passportTypeChanged}" />
			</h:selectOneMenu>
			<h:panelGroup id="passportNumberIdLbl">
				<h:outputText
					rendered="#{!pages$ManageBeneficiaryBasicInfoTab.beneficiary.masroom}"
					value="#{msg['mems.inheritanceFile.fieldLabel.passportNumber']}" />
			</h:panelGroup>
			<h:panelGroup id="passportNumberId">
				<h:inputText
					rendered="#{!pages$ManageBeneficiaryBasicInfoTab.beneficiary.masroom}"
					value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.passportNumber}" />
			</h:panelGroup>

			<h:panelGroup id="masroomNumberId">
				<h:outputText
					rendered="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.masroom}"
					value="#{msg['mems.inheritanceFile.fieldLabel.masroomNumber']}" />
			</h:panelGroup>

			<h:panelGroup id="masroomNumberIdLbl">
				<h:inputText
					rendered="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.masroom}"
					value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.masroomNumber}" />
			</h:panelGroup>

			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.passportPlace']}" />
			<h:selectOneMenu id="passportPlaceIdBeneficiary" immediate="false"
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.passportIssuePlaceIdString}">
				<f:selectItem itemValue="-1"
					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
				<f:selectItems value="#{pages$ApplicationBean.countryList}" />
			</h:selectOneMenu>

			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.maritalStatus']}" />
			<h:selectOneMenu id="cboMaritialStatusBeneficiary"
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.maritialStatusIdString}">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$ApplicationBean.maritialStatus}" />
			</h:selectOneMenu>

			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.jobDescription']}" />
			<h:inputText
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.jobDescription}" />

			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.placeOfWork']}" />
			<h:inputText
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.workingCompany}" />

			<h:outputText
				value="#{msg['financialAccConfiguration.bankRemitanceAccountName']}" />
			<h:inputText maxlength="23"
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.accountNumber}" />

			<h:outputText value="#{msg['grp.BankName']}" />
			<h:selectOneMenu
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.bankId}">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$ApplicationBean.bankList}" />
			</h:selectOneMenu>


			<h:outputText
				value="#{msg['mmems.inheritanceBeneficiary.interestedInProgram']}" />
			<h:selectBooleanCheckbox id="interestedInProgram" disabled="true"
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.interestedInPrograms}"
				tabindex="3" />



			<h:outputLabel />
			<h:outputLabel />

			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.isStudent']}" />
			<h:selectBooleanCheckbox id="isStudent"
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.student}"
				tabindex="3"></h:selectBooleanCheckbox>

			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.maturityDate']}" />
			<rich:calendar
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.maturityDate}"
				locale="#{pages$inheritanceFileBasicInfoTab.locale}" popup="true"
				datePattern="#{pages$inheritanceFileBasicInfoTab.dateFormat}"
				verticalOffset="-212" showApplyButton="false"
				enableManualInput="false" />


			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.expectedMaturityDate']}" />
			<h:outputText style="color:red;font-weight: bold;"
				rendered="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.highlightExpMaturityDate}"
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.expMaturityDate}" />
			<h:outputText
				rendered="#{!pages$ManageBeneficiaryBasicInfoTab.beneficiary.highlightExpMaturityDate}"
				value="#{pages$ManageBeneficiaryBasicInfoTab.beneficiary.expMaturityDate}" />

			<%--
			<h:outputText
				value="#{msg['inheritanceFile.fieldLabel.newCostCenter']}" />
			<h:inputText styleClass="READONLY" readonly="true"
				value="#{pages$ManageBeneficiaryBasicInfoTab.newCC}" />

			<h:outputText
				value="#{msg['inheritanceFile.fieldLabel.oldCostCenter']}" />
			<h:inputText styleClass="READONLY" readonly="true"
				value="#{pages$ManageBeneficiaryBasicInfoTab.oldCC}" />
 --%>
		</t:panelGrid>
	</t:panelGroup>
	<%--Column 3,4 Ends--%>
</t:panelGrid>
<%--</t:collapsiblePanel> --%>


