<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
  <t:div id="propertiesDetailsDiv" styleClass="contentDiv"  style="width:98%">
    <t:dataTable id="propertiesDataTable" 
	rows="#{pages$propertiesDetail.paginatorRows}" style="width:100%"
	value="#{pages$propertiesDetail.propertiesList}"
	binding="#{pages$propertiesDetail.propertiesTable}"
	preserveDataModel="false" preserveSort="false" var="dataItem"
	rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
<t:column id="colPropertyNumber" >
	<f:facet name="header" >
		<t:outputText value="#{msg['grp.propertyRefNumber']}" />
	</f:facet>
	<t:outputText  styleClass="A_LEFT" value="#{dataItem.propertyNumber}" />
</t:column>
<t:column id="propertyName" style="width:20%">
	<f:facet name="header" >
		<t:outputText value="#{msg['inquiryApplication.propertyName']}" />
	</f:facet>
	<t:outputText  styleClass="A_LEFT" value="#{dataItem.commercialName}" />
</t:column>
<t:column id="propertyType">
	<f:facet name="header">
		<t:outputText  value="#{msg['inquiryApplication.propertyType']}" />
	</f:facet>
	<t:outputText title="" styleClass="A_LEFT" value="#{pages$propertiesDetail.englishLocale ? dataItem.propertyTypeEn : dataItem.propertyTypeAr}" />
</t:column>
<t:column id="propertyStatus" >
	<f:facet name="header" >
		<t:outputText  value="#{msg['inquiryApplication.status']}" />
	</f:facet>
	<t:outputText title="" styleClass="A_LEFT" value="#{pages$propertiesDetail.englishLocale ? dataItem.statusTypeEn : dataItem.statusTypeAr}" />
</t:column>
<t:column id="actionCol2" sortable="false" 
	styleClass="ACTION_COLUMN">
	<f:facet name="header">
		<t:outputText value="#{msg['commons.action']}" />
	</f:facet>
	<t:commandLink action="#{pages$propertiesDetail.onOpenProperty}">
		<h:graphicImage id="viewIcon" title="#{msg['commons.view']}"
			url="../resources/images/app_icons/Lease-contract.png" />&nbsp;
	</t:commandLink>
	</t:column>					
 </t:dataTable>										
</t:div>

                               <t:div id="propertyScroller"   styleClass="contentDivFooter"  style="width:99.1%">
                       

			<t:panelGrid    columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
				<t:div  styleClass="RECORD_NUM_BG">
					<t:panelGrid id="rerrre" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
						<h:outputText value="#{msg['commons.recordsFound']}"/>
						<h:outputText value=" : "/>
						<h:outputText value="#{pages$propertiesDetail.recordSize}"/>
					</t:panelGrid>	
						
				</t:div>
				  
			      <CENTER>
				   <t:dataScroller id="propertiesScroller" for="propertiesDataTable" paginator="true"
					fastStep="1"  paginatorMaxPages="#{pages$propertiesDetail.paginatorMaxPages}" immediate="false"
					paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" 
				    pageIndexVar="pageNumber"
				    styleClass="SCH_SCROLLER"
				    paginatorActiveColumnStyle="font-weight:bold;"
				    paginatorRenderLinkForActive="false" 
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					    <f:facet  name="first">
							<t:graphicImage    url="../#{path.scroller_first}"  id="lblFpropertiesDetail"></t:graphicImage>
						</f:facet>

						<f:facet name="fastrewind">
							<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRpropertiesDetail"></t:graphicImage>
						</f:facet>

						<f:facet name="fastforward">
							<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFpropertiesDetail"></t:graphicImage>
						</f:facet>

						<f:facet name="last">
							<t:graphicImage  url="../#{path.scroller_last}"  id="lblLpropertiesDetail"></t:graphicImage>
						</f:facet>

                                              <t:div id="propertyPageNum"   styleClass="PAGE_NUM_BG">
					   <t:panelGrid id="propertyPageGrid"  styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0" >
					 					<h:outputText  styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
										<h:outputText   styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
						</t:panelGrid>
							
						</t:div>
				 </t:dataScroller>
				 </CENTER>
		      
	        </t:panelGrid>
                   </t:div>
				                           