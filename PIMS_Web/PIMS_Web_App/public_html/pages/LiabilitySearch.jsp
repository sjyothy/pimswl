<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Liabilities
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js" />




<script language="JavaScript" type="text/javascript">
	    function resetValues()
      	{
      		document.getElementById("searchFrm:progNumber").value="";
			
      	    $('searchFrm:auctionRegEndDateTo').component.resetSelectedDate();
      	        document.getElementById("searchFrm:auctionStartTimeMM").selectedIndex=0;
      	    
			
        }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">

					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>


					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>


						<td width="83%" height="470px" valign="top"
							class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['liabilitySearch.title']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" height="100%">
										<div class="SCROLLABLE_SECTION AUC_SCH_SS"
											style="height: 95%; width: 100%; # height: 95%; # width: 100%;">
											<h:form id="searchFrm"
												style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
												<t:div styleClass="MESSAGE">
													<table border="0" class="layoutTable">
														<tr>
															<td>
																<h:messages></h:messages>
																<h:outputText id="errorMessage"
																	value="#{pages$LiabilitySearch.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
															</td>
														</tr>
													</table>
												</t:div>
												<div class="MARGIN">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="DETAIL_SECTION">
														<h:outputLabel value="#{msg['commons.searchCriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER">
															<tr>
																<td width="97%">
																	<table width="100%">
																		<tr>

																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['liabilitySearch.lbl.BeneficiaryName']}"></h:outputLabel>
																			</td>

																			<td width="25%">
																				<h:inputText id="benefName"
																					value="#{pages$LiabilitySearch.searchCriteria.benefName}" />

																			</td>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['liabilitySearch.lbl.searchCriteria.reqNum']}"></h:outputLabel>
																			</td>

																			<td width="25%">
																				<h:inputText id="REQNum"
																					value="#{pages$LiabilitySearch.searchCriteria.reqNumber}"
																					maxlength="20"></h:inputText>
																			</td>
																		</tr>
																		<tr>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['liabilitySearch.lbl.searchCriteria.DisNum']}"></h:outputLabel>
																			</td>

																			<td width="25%">
																				<h:inputText id="disNum"
																					value="#{pages$LiabilitySearch.searchCriteria.disbursementNumber}"
																					maxlength="20"></h:inputText>
																			</td>

																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['liabilitySearch.lbl.Status']}"></h:outputLabel>
																			</td>

																			<td width="25%">
																				<h:selectOneMenu id="selectProgramType"
																					value="#{pages$LiabilitySearch.searchCriteria.statusId}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$LiabilitySearch.statusList}" />
																				</h:selectOneMenu>
																			</td>
																		</tr>
																		<tr>

																			<td class="BUTTON_TD" colspan="4" />

																				<table class="BUTTON_TD" cellpadding="1px"
																					cellspacing="1px">
																					<tr>
																						<td colspan="2">
																							<h:commandButton styleClass="BUTTON"
																								value="#{msg['commons.search']}"
																								action="#{pages$LiabilitySearch.doSearch}"
																								style="width: 75px">
																							</h:commandButton>
																							<h:commandButton styleClass="BUTTON" type="reset"
																								value="#{msg['commons.clear']}"
																								style="width: 75px"></h:commandButton>

																						</td>


																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>
															</tr>

														</table>
													</div>
												</div>
												<div
													style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
													<div class="imag">
														&nbsp;
													</div>
													<div class="contentDiv" style="width: 100%; # width: 99%;">
														<t:dataTable id="dt1"
															value="#{pages$LiabilitySearch.list}"
															binding="#{pages$LiabilitySearch.dataTable}"
															rows="#{pages$LiabilitySearch.paginatorRows}"
															preserveDataModel="false" preserveSort="false"
															var="dataItem" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true" width="100%">

															<t:column id="beneficiaryName" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="beneficiaryName"
																		arrow="true"
																		actionListener="#{pages$LiabilitySearch.sort}"
																		value="#{msg['liabilitySearch.lbl.BeneficiaryName']}">
																		<f:attribute name="sortField" value="firstName" />
																	</t:commandSortHeader>
																</f:facet>
																<h:commandLink id="lnkBeneficiaryName"
																	style="white-space: normal;"
																	value="#{dataItem.beneficiary}"
																	onclick="javaScript:openBeneficiaryPopup(#{dataItem.beneficiaryId});" />

															</t:column>
															<t:column id="amount">
																<f:facet name="header">
																	<t:outputText
																		value="#{msg['liabilitySearch.lbl.Amount']}">

																	</t:outputText>
																</f:facet>
																<t:outputText value="#{dataItem.benefAmount}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="refNum">
																<f:facet name="header">
																	<t:outputText
																		value="#{msg['liabilitySearch.lbl.RefNum'] }">
																	</t:outputText>
																</f:facet>
																<t:outputText value="#{dataItem.disReqNumber}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>

															<t:column id="status">
																<f:facet name="header">
																	<t:outputText
																		value="#{msg['liabilitySearch.lbl.Status']}" />
																</f:facet>
																<t:outputText
																	value="#{pages$LiabilitySearch.englishLocale? dataItem.statusEn:dataItem.statusAr}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="DisbursedBy">
																<f:facet name="header">
																	<t:outputText
																		value="#{msg['liabilitySearch.lbl.CreatedBy']}" />
																</f:facet>
																<t:outputText
																	value="#{pages$LiabilitySearch.englishLocale? dataItem.disbursedByEn:dataItem.disbursedByAr}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="actionCol" sortable="false" width="100"
																style="TEXT-ALIGN: center;">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.action']}" />
																</f:facet>
																<t:commandLink
																	rendered="#{dataItem.statusKey eq 'DISBURSED_EXEMPTED' || dataItem.statusKey eq 'DISBURSED_COLLECTED'}"
																	action="#{pages$LiabilitySearch.onView}">
																	<h:graphicImage title="#{msg['commons.view']}"
																		url="../resources/images/app_icons/view_tender.png" />
																</t:commandLink>
																<t:commandLink
																	rendered="#{dataItem.statusKey eq 'DISBURSED' || dataItem.statusKey eq 'BINDED_TO_REQUEST'}"
																	action="#{pages$LiabilitySearch.onExempt}">
																	<h:graphicImage
																		title="#{msg['liabilitySearch.lbl.Exempt']}"
																		url="../resources/images/edit-icon.gif" />
																</t:commandLink>
																<t:commandLink
																	rendered="#{dataItem.statusKey eq 'DISBURSED' || dataItem.statusKey eq 'BINDED_TO_REQUEST'}"
																	action="#{pages$LiabilitySearch.onCollect}">
																	<h:graphicImage
																		title="#{msg['liabilitySearch.lbl.Collect']}"
																		url="../resources/images/edit-icon.gif" />
																</t:commandLink>
															</t:column>



														</t:dataTable>
													</div>
													<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
														style="width:100%;#width:100%;">
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td class="RECORD_NUM_TD">
																	<div class="RECORD_NUM_BG">
																		<table cellpadding="0" cellspacing="0"
																			style="width: 182px; # width: 150px;">
																			<tr>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText value=" : " />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{pages$LiabilitySearch.recordSize}" />
																				</td>
																			</tr>
																		</table>
																	</div>
																</td>
																<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																	align="right">

																	<t:div styleClass="PAGE_NUM_BG" style="#width:20%">

																		<table cellpadding="0" cellspacing="0">
																			<tr>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																				</td>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{pages$LiabilitySearch.currentPage}" />
																				</td>
																			</tr>
																		</table>
																	</t:div>
																	<TABLE border="0" class="SCH_SCROLLER">
																		<tr>
																			<td>
																				<t:commandLink
																					action="#{pages$LiabilitySearch.pageFirst}"
																					disabled="#{pages$LiabilitySearch.firstRow == 0}">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$LiabilitySearch.pagePrevious}"
																					disabled="#{pages$LiabilitySearch.firstRow == 0}">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:dataList value="#{pages$LiabilitySearch.pages}"
																					var="page">
																					<h:commandLink value="#{page}"
																						actionListener="#{pages$LiabilitySearch.page}"
																						rendered="#{page != pages$LiabilitySearch.currentPage}" />
																					<h:outputText value="<b>#{page}</b>" escape="false"
																						rendered="#{page == pages$LiabilitySearch.currentPage}" />
																				</t:dataList>
																			</td>
																			<td>

																				<t:commandLink
																					action="#{pages$LiabilitySearch.pageNext}"
																					disabled="#{pages$LiabilitySearch.firstRow + pages$LiabilitySearch.rowsPerPage >= pages$LiabilitySearch.totalRows}">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$LiabilitySearch.pageLast}"
																					disabled="#{pages$LiabilitySearch.firstRow + pages$LiabilitySearch.rowsPerPage >= pages$LiabilitySearch.totalRows}">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblL"></t:graphicImage>
																				</t:commandLink>
																			</td>
																		</tr>
																	</TABLE>
																</td>
															</tr>
														</table>
													</t:div>
												</div>

											</h:form>

										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>