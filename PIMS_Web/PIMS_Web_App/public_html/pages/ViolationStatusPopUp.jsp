<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)\
  - Description: Used for Searching Violations
  --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%-- 
		<% UIViewRoot viewRoot = (UIViewRoot)session.getAttribute("view");
           if (viewRoot.getLocale().toString().equals("en"))
           { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>
        <%
            } 
            else
            { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_ar.js"></script>
        <%
            } 
        %>
--%>
<script language="JavaScript" type="text/javascript">

	    function resetValues()
      	{
      	    document.getElementById("searchFrm:auctionStatuses").selectedIndex=0;
		    document.getElementById("searchFrm:auctionNumber").value="";
			document.getElementById("searchFrm:auctionTitle").value="";
			document.getElementById("searchFrm:auctionDate").value="";
			document.getElementById("searchFrm:auctionCreationDate").value="";
			document.getElementById("searchFrm:auctionStartTime").value="";
			document.getElementById("searchFrm:auctionEndTime").value="";
			document.getElementById("searchFrm:venueName").value="";
        }
	    

</script>
<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
 
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is Violation search page">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>						


			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			
 	</head>

	<body>
			<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
			%>
	
		
	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				

				<tr width="100%">
				
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" 
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="99%" valign="top" nowrap="nowrap">
									<div style="height:450;overflow:auto;" >
									<h:form id="searchFrm">
											<table width="100%">
										        <tr>
												     <td>
													&nbsp;	
													<h:inputHidden id="hdnViolationId" value="#{pages$ViolationStatusPopUp.violationId}"/>
													</td>
													<td colspan="1" align="right" style="width: 118px">
													<h:outputLabel
														value="#{msg['violationDetails.description']}" />
												</td>
												<td colspan="3">
												    <h:inputHidden id="hdndescription" value="#{pages$ViolationStatusPopUp.description}"/>
													<h:inputTextarea readonly="true" id="description" value="#{pages$ViolationStatusPopUp.description}"
														style="width: 548px; height: 73px" cols="125" rows="2"/>
												</td>
												</tr>
												<tr>
												 <td>
													&nbsp;	
													
													</td>
													<td>
														<h:outputLabel value="Status"/>
													</td>
													<td>
													<%System.out.println("caf"); %>
														<h:selectOneMenu id="violationStatus" value="#{pages$ViolationStatusPopUp.selectedStatus}"
															style="width: 127px">
															<f:selectItems value="#{pages$ViolationStatusPopUp.statusList}" />
																	
														</h:selectOneMenu>
													</td>
													<tr>
												  <td colspan="6">
													<h:commandButton styleClass="BUTTON"
														value="#{msg['commons.saveButton']}" action ="#{pages$ViolationStatusPopUp.btnSave_Click}"
														style="width: 75px"></h:commandButton>
												</td>
												
										    </tr>
													
										    </tr>
										  
										</table>
										<br>
										<br><br>
										
									</h:form>
									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				
			</table>
		
	</body>
</html>
</f:view>