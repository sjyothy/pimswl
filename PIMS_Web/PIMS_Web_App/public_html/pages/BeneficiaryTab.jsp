<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%-- Please remove above taglibs after completing your tab contents--%>
<t:div styleClass="contentDiv" style="width:95%">
<t:dataTable id="assetGrid" width="100%" styleClass="grid"
	value="#{pages$BeneficiaryTab.assetDataList}" rows="100"
	preserveDataModel="true" preserveSort="false" var="dataItem"
	rowClasses="row1,row2">




	<t:column id="beneficiary" width="10%" sortable="false"
		style="white-space: normal;">
		<f:facet name="header">
			<t:outputText value="#{msg['PeriodicDisbursements.columns.beneficiary']}" />
		</f:facet>
		<t:outputText value="#{dataItem.beneficiary}" />
	</t:column>
	
	<t:column id="isMinor" width="10%" sortable="false"	 style="white-space: normal;">
		<f:facet name="header">
			<t:outputText value="#{msg['ResearchFormBenef.isMinor']}" />
		</f:facet>
		<t:outputText value="#{dataItem.isMinor==1?msg['commons.yes']:msg['commons.no']}" />
	</t:column>
	
	<t:column id="gender" width="10%" sortable="false"
		style="white-space: normal;">
		<f:facet name="header">
			<t:outputText value="#{msg['customer.gender']}" />
		</f:facet>
		<t:outputText value="#{dataItem.gender}" />
	</t:column>
	
	<t:column id="incomeCategory" width="10%" sortable="false"
		style="white-space: normal;">
		<f:facet name="header">
			<t:outputText value="#{msg['ResearchFormBenef.incomeCategory']}" />
		</f:facet>
		<t:outputText value="#{dataItem.incomeCategory}" />
	</t:column>
	
	<t:column id="fileNumber" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['SearchBeneficiary.fileNumber']}" />
		</f:facet>
		<t:outputText value="#{dataItem.fileNumber}" />
	</t:column>
	<t:column id="will" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['mems.inheritanceFile.chkBoxWill']}" />
		</f:facet>
		<t:outputText value="#{dataItem.will}" />
	</t:column>
	<t:column id="share" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['mems.inheritanceFile.tabHeadingShort.asstShare']}" />
		</f:facet>
		<t:outputText value="#{dataItem.share}" />
	</t:column>
	<t:column id="cc" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['property.fieldLabel.oldNewCostCenter']}" />
		</f:facet>
		<t:outputText value="#{dataItem.oldNewCostCenter}" />
	</t:column>
	



</t:dataTable>
</t:div>
<%--Column 3,4 Ends--%>
