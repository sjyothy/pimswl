
<t:div id="tabEMBDetails" style="width:100%;">
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableFileBen" rows="150" width="100%"
			value="#{pages$endowmentManage.listEndFileBen}"
			binding="#{pages$endowmentManage.dataTableFileBen}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">



			<t:column id="personNameBenef" sortable="true">
				<f:facet name="header">
					<t:outputText id="personNameString"
						value="#{msg['endowmentFileBen.lbl.beneficiary']}" />
				</f:facet>
				<t:commandLink rendered="#{dataItem.isDeleted == '0' }"
					action="#{pages$endowmentManage.onOpenPersonBeneficiaryPopup}"
					value="#{dataItem.beneficiaryName}" style="white-space: normal;"
					styleClass="A_LEFT" />
			</t:column>
			<t:column id="masrafBenef" sortable="true">
				<f:facet name="header">
					<t:outputText id="masrafNameBenef" value="#{msg['commons.Masraf']}" />
				</f:facet>
				<t:commandLink rendered="#{dataItem.isDeleted == '0' }"
					action="#{pages$endowmentManage.onOpenMasrafPopup}"
					value="#{dataItem.masrafName}" style="white-space: normal;"
					styleClass="A_LEFT" />
			</t:column>
			<t:column id="percentageBenef" sortable="true">
				<f:facet name="header">
					<t:outputText id="percentageValueBenef"
						value="#{msg['commons.percentage']}" />
				</f:facet>
				<t:outputText rendered="#{dataItem.isDeleted == '0' }"
					value="#{dataItem.sharePercentageFileString}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="amountValueBenef" sortable="true">
				<f:facet name="header">
					<t:outputText id="amountStringBenef"
						value="#{msg['commons.amount']}" />
				</f:facet>
				<t:outputText value="#{dataItem.amountString}"
					rendered="#{dataItem.isDeleted == '0' }"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="commentsBenef">
				<f:facet name="header">
					<t:outputText id="CommentsStringBenef"
						value="#{msg['commons.comments']}" />
				</f:facet>
				<t:outputText value="#{dataItem.comments}"
					rendered="#{dataItem.isDeleted == '0' }"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="revenuePeriodBenef">
				<f:facet name="header">
					<t:outputText id="revenuePeriodStringBenef"
						value="#{msg['endowmentFileBen.lbl.revenuePeriod']}" />
				</f:facet>
				<t:outputText value="#{dataItem.revenuePeriod}"
					rendered="#{dataItem.isDeleted == '0' }"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
		</t:dataTable>
	</t:div>

</t:div>

<t:div>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<h:outputText styleClass="PAGE_NUM" style="font-size:9;color:black;"
					value="#{msg['commons.totalPercentage']}:" />
			</td>
			<td>
				<h:outputText styleClass="PAGE_NUM"
					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:9;font-weight:bold;color:black"
					value="#{pages$endowmentManage.totalBeneficiaryPercentage}" />
			</td>
		</tr>
	</table>
</t:div>

