

<t:div id="projectDetailsDiv" styleClass="TAB_DETAIL_SECTION"
	style="width:100%">
	<t:panelGrid id="projectDetailsTable" cellpadding="1px" width="100%"
		cellspacing="5px" styleClass="TAB_DETAIL_SECTION_INNER" columns="4"
		columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
		
		<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['project.projectNumber']}" />
		</h:panelGroup>	
		<h:panelGroup>
			<h:inputText maxlength="20" styleClass="READONLY"
				id="txtprojectNumber" readonly="true"
				value="#{pages$projectDetailsTabBOT.projectNumber}"
				style="width:85%;"></h:inputText>
			<h:graphicImage url="../resources/images/magnifier.gif"
				binding="#{pages$projectDetailsTabBOT.imgSearchProject}"
				onclick="javaScript:showSearchProjectPopUp('#{pages$projectDetailsTabBOT.projectKey_PageMode}','#{pages$projectDetailsTabBOT.projectKey_PageModeSelectOnPopUp}','#{pages$projectDetailsTabBOT.projectKey_AllowedStatus}','#{pages$projectDetailsTabBOT.projectValue_AllowedStatuses}','#{pages$projectDetailsTabBOT.projectKey_AllowedType}','#{pages$projectDetailsTabBOT.projectValue_AllowedTypes}');"
				style="MARGIN: 0px 0px -4px;" alt="#{msg[commons.searchProject]}"></h:graphicImage>
		</h:panelGroup>
		<h:outputLabel id="lblprojectName" styleClass="LABEL"
			value="#{msg['project.projectName']}"></h:outputLabel>
		<h:inputText maxlength="150" styleClass="READONLY" id="txtprojectName"
			readonly="true" value="#{pages$projectDetailsTabBOT.projectName}"
			style="width:85%;"></h:inputText>



		<h:outputLabel styleClass="LABEL"
			value="#{msg['project.projectEstimatedCost']}"></h:outputLabel>
		<h:inputText id="projectEstimatedCost" readonly="true"
			style="width:85%;" styleClass="A_RIGHT_NUM READONLY" maxlength="15"
			value="#{pages$projectDetailsTabBOT.projectEstimatedCost}"></h:inputText>

		<h:outputLabel id="lblpropName" styleClass="LABEL"
			value="#{msg['cancelContract.tab.unit.propertyname']}"></h:outputLabel>
		<h:panelGroup>
			<h:inputText maxlength="150" styleClass="READONLY"
				id="txtpropertyName" readonly="true"
				value="#{pages$projectDetailsTabBOT.propertyView.commercialName}"
				style="width:85%;"></h:inputText>
			<h:graphicImage
				rendered="#{pages$projectDetailsTabBOT.isSearchPropertyShow}"
				binding="#{pages$projectDetailsTabBOT.imgSearchProperty}"
				url="../resources/images/magnifier.gif"
				onclick="javaScript:showPropertySearchPopUp();"
				style="MARGIN: 0px 0px -4px;"
				alt="#{msg['maintenanceRequest.searchProperties']}"></h:graphicImage>
		</h:panelGroup>

		<h:outputLabel id="lblcoordinates" styleClass="LABEL"
			value="#{msg['receiveProperty.buildingCoordinates']}"></h:outputLabel>
		<h:inputText maxlength="150" styleClass="READONLY" id="txtcoordinates"
			readonly="true"
			value="#{pages$projectDetailsTabBOT.propertyView.coordinates}"
			style="width:85%;"></h:inputText>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['botContractDetails.LandNumber']}:"></h:outputLabel>
		<h:inputText id="txtLandNumber" readonly="true" style="width:85%;"
			styleClass="READONLY" maxlength="150"
			value="#{pages$projectDetailsTabBOT.propertyView.landNumber}"></h:inputText>


		<h:outputLabel id="lbllandArea" styleClass="LABEL"
			value="#{msg['receiveProperty.landArea']}"></h:outputLabel>
		<h:inputText maxlength="150" styleClass="READONLY"
			id="txtpropLandArea" readonly="true"
			value="#{pages$projectDetailsTabBOT.propertyView.landArea}"
			style="width:85%;"></h:inputText>

		<h:outputLabel id="lblbuiltInArea" styleClass="LABEL"
			value="#{msg['receiveProperty.builtInArea']}"></h:outputLabel>
		<h:inputText maxlength="150" styleClass="READONLY" id="txtbuiltInArea"
			readonly="true"
			value="#{pages$projectDetailsTabBOT.propertyView.builtInArea}"
			style="width:85%;"></h:inputText>

		<h:outputLabel id="lblnoFloors" styleClass="LABEL"
			value="#{msg['receiveProperty.totalFloors']}"></h:outputLabel>
		<h:inputText maxlength="150" styleClass="READONLY" id="txtnoFloors"
			readonly="true"
			value="#{pages$projectDetailsTabBOT.propertyView.noFloors}"
			style="width:85%;"></h:inputText>

		<h:outputLabel id="lblnoOfMezzanineFloors" styleClass="LABEL"
			value="#{msg['receiveProperty.totalMezzanine']}"></h:outputLabel>
		<h:inputText maxlength="150" styleClass="READONLY"
			id="txtnoOfMezzanineFloors" readonly="true"
			value="#{pages$projectDetailsTabBOT.propertyView.noOfMezzanineFloors}"
			style="width:85%;"></h:inputText>

		<h:outputLabel id="lblnoOfUnits" styleClass="LABEL"
			value="#{msg['receiveProperty.totalUnits']}"></h:outputLabel>
		<h:inputText maxlength="150" styleClass="READONLY" id="txtnoOfUnits"
			readonly="true"
			value="#{pages$projectDetailsTabBOT.propertyView.noOfUnits}"
			style="width:85%;"></h:inputText>

		<h:outputLabel id="lblnoOfParkingFloors" styleClass="LABEL"
			value="#{msg['receiveProperty.totalParking']}"></h:outputLabel>
		<h:inputText maxlength="150" styleClass="READONLY"
			id="txtnoOfParkingFloors" readonly="true"
			value="#{pages$projectDetailsTabBOT.propertyView.noOfParkingFloors}"
			style="width:85%;"></h:inputText>

		<h:outputLabel id="lblnoOfLifts" styleClass="LABEL"
			value="#{msg['receiveProperty.numberOfLifts']}"></h:outputLabel>
		<h:inputText maxlength="150" styleClass="READONLY" id="txtnoOfLifts"
			readonly="true"
			value="#{pages$projectDetailsTabBOT.propertyView.noOfLifts}"
			style="width:85%;"></h:inputText>

	</t:panelGrid>
</t:div>
