<script language="JavaScript" type="text/javascript">
function disableDisburseButton(control)
		{
			
			control.disabled = true;
			document.getElementById("detailsFrm:lnkDisbrseByFinance").onclick();
			return 
		
		}
</script>
<t:div id="tabFincance" style="width:100%;">
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="fnTab" rows="15" width="100%"
			value="#{pages$tabFinance.disbursementDetails}"
			binding="#{pages$tabFinance.disbursementTable}"
			preserveDataModel="false" preserveSort="false" var="unitDataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
			<t:column id="checkbox" style="width:auto;">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.select']}" />
				</f:facet>
				<h:selectBooleanCheckbox id="idbox" immediate="true"
					rendered="#{(empty unitDataItem.paymentDetails[0].status || 0==unitDataItem.paymentDetails[0].status) 
										&& (pages$tabFinance.isStatusApproved || pages$tabFinance.isDisbursementRequired)}"
					value="#{unitDataItem.selected}">
				</h:selectBooleanCheckbox>
			</t:column>

			<t:column id="refNum" sortable="true">
				<f:facet name="header">
					<t:outputText id="lblRefNum" value="#{msg['commons.refNum']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="txtRefNum"
					styleClass="A_LEFT" value="#{unitDataItem.refNum}" />
			</t:column>
			<t:column id="beneficiary" sortable="true"
				style="white-space: normal;width:10%;">
				<f:facet name="header">
					<t:outputText id="hdBenef"
						value="#{msg['mems.normaldisb.label.beneficiary']}" />
				</f:facet>
				<t:commandLink
					actionListener="#{pages$tabFinance.openManageBeneficiaryPopUp}"
					value="#{unitDataItem.beneficiariesName}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="ptype" sortable="true"
				rendered="#{pages$tabFinance.renderPaymentType}">
				<f:facet name="header">
					<t:outputText id="fnTabhdBenef"
						value="#{msg['mems.finance.label.paymentType']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="s_b"
					styleClass="A_LEFT"
					value="#{pages$tabFinance.isEnglishLocale?unitDataItem.paymentTypeEn:unitDataItem.paymentTypeAr}" />
			</t:column>
			<t:column id="amnt" sortable="true"
				rendered="#{pages$tabFinance.renderAmount}">
				<f:facet name="header">
					<t:outputText id="hdAmnt" value="#{msg['commons.amount']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabamountt"
					styleClass="A_LEFT"
					value="#{0==unitDataItem.hasInstallment?unitDataItem.amount:unitDataItem.paymentDetails[0].amount}" />
			</t:column>

			<t:column id="fnSrcType" sortable="true"
				rendered="#{pages$tabFinance.renderSrcType}">
				<f:facet name="header">
					<t:outputText id="hdSrcType"
						value="#{msg['mems.finance.label.srcType']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabp_w"
					styleClass="A_LEFT"
					value="#{pages$tabFinance.isEnglishLocale?unitDataItem.srcTypeEn:unitDataItem.srcTypeAr}" />
			</t:column>
			<t:column id="fnReason" sortable="true"
				rendered="#{pages$tabFinance.renderReason}">
				<f:facet name="header">
					<t:outputText id="hdFnReason"
						value="#{msg['mems.finance.label.reason']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabReason"
					styleClass="A_LEFT"
					value="#{pages$tabFinance.isEnglishLocale?unitDataItem.reasonEn:unitDataItem.reasonAr}" />
			</t:column>
			<t:column id="fnDesc" sortable="true"
				rendered="#{pages$tabFinance.renderDescription}">
				<f:facet name="header">
					<t:outputText id="hdFnDesc"
						value="#{msg['mems.finance.label.desc']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabDesc"
					styleClass="A_LEFT" value="#{unitDataItem.description}" />
			</t:column>
			<t:column id="fnItems" sortable="true"
				rendered="#{pages$tabFinance.renderItems}">
				<f:facet name="header">
					<t:outputText id="hdFnItems"
						value="#{msg['mems.finance.label.items']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabItems"
					styleClass="A_LEFT"
					value="#{pages$tabFinance.isEnglishLocale?unitDataItem.itemEn:unitDataItem.itemAr}" />
			</t:column>
			<t:column id="fnPaidTo" sortable="true"
				rendered="#{pages$tabFinance.renderPaidTo}">
				<f:facet name="header">
					<t:outputText id="hdFnPaidTo"
						value="#{msg['mems.finance.label.paidTo']}" />

				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabPaidTo"
					styleClass="A_LEFT"
					value="#{unitDataItem.paidTo==0?(unitDataItem.vendorId !=null? msg['mems.normaldisb.label.vendor']:msg['mems.finance.label.externalParty'])
					                               :
					                               msg['mems.finance.label.beneficiary']}" />
			</t:column>
			<t:column id="fnDisSrc" sortable="true"
				rendered="#{pages$tabFinance.renderDisSource}">
				<f:facet name="header">
					<t:outputText id="hdFnDisSrc"
						value="#{msg['mems.finance.disSource']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabDisRc"
					styleClass="A_LEFT"
					value="#{pages$tabFinance.isEnglishLocale?unitDataItem.disburseSrcNameEn:unitDataItem.disburseSrcNameAr}" />
			</t:column>
			<t:column id="fnDisBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdFnDisBy" value="#{msg['commons.disbursedBy']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabDisBy"
					styleClass="A_LEFT" value="#{unitDataItem.disbursedBy}" />
			</t:column>

			<t:column id="fnTabactionId" sortable="true">
				<f:facet name="header">
					<t:outputText id="fnTabhdAmoun" value="#{msg['commons.action']}" />
				</f:facet>
				<h:commandLink rendered="#{empty unitDataItem.externalPartyName}"
					action="#{pages$tabFinance.showPaymentDetails}">
					<h:graphicImage id="pymtIcon"
						title="#{msg['mems.payment.request.label.paymentDetails']}"
						url="../resources/images/app_icons/Pay-Fine.png" width="18px;" />
				</h:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>
	<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
		style="width:99.1%;">
		<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
			cellspacing="1">
			<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
				<t:div styleClass="JUG_NUM_REC_ATT">
					<h:outputText value="#{msg['commons.records']}" />
					<h:outputText value=" : " />
					<h:outputText value="#{pages$tabFinance.totalRows}" />
				</t:div>
			</t:panelGroup>
			<t:panelGroup>
				<t:dataScroller id="fnTabshareScrollers" for="fnTab"
					paginator="true" fastStep="1" immediate="false"
					paginatorTableClass="paginator" renderFacetsIfSinglePage="true"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false" pageIndexVar="pageNumber"
					styleClass="JUG_SCROLLER">
					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
					</f:facet>
					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
					</f:facet>
					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
					</f:facet>
					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
					</f:facet>
					
					<t:div>
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<h:outputText styleClass="PAGE_NUM"
										style="font-size:9;color:black;"
										value="#{msg['mems.normaldisb.label.totalAmount']}:" />
								</td>
								<td>
									<h:outputText styleClass="PAGE_NUM"
										style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:9;font-weight:bold;color:black"
										value="#{pages$tabFinance.totalDisbursementCount}" />
								</td>
							</tr>
						</table>
					</t:div>
				</t:dataScroller>
			</t:panelGroup>
		</t:panelGrid>
	</t:div>
	<pims:security screen="Pims.MinorMgmt.FinanceDisburse" action="view">
		<t:div styleClass="BUTTON_TD"
			style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">
			<h:commandButton styleClass="BUTTON"
				value="#{msg['mems.finance.label.disburseButton']}"
				rendered="#{pages$tabFinance.renderDisburse}"
				onclick="disableDisburseButton(this)" />

			<h:commandLink id="lnkDisbrseByFinance"
				action="#{pages$tabFinance.disburse}" style="width: auto;">
			</h:commandLink>
			<h:commandButton styleClass="BUTTON"
				value="#{msg['tabFinance.btn.AddSupplier']}"
				rendered="#{pages$tabFinance.renderDisburse}"
				action="#{pages$tabFinance.onAddGRPSuppliers}" style="width: auto;">
			</h:commandButton>
		</t:div>
	</pims:security>
</t:div>
