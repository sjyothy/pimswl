<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript" src="../resources/jscripts/commons.js"></script>
    
<f:view>
        <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
       <f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
 
    
    <html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>						
            <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
			


			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			
 	</head>
     
		<script language="javascript" type="text/javascript">
		
		function openRequestHistoryPopUp()
        {
           
           var screen_width = 1024;
	       var screen_height = 450;
	        window.open('RequestHistoryPopUp.jsf?contractId='+document.getElementById("formRequest:hdnContractId").value,'_blank','width='+(screen_width-100)+',height='+(screen_height)+',left=50,top=150,scrollbars=yes,status=yes'); 
        }
		function openContractHistoryPopUp()
        {
           var screen_width = 1024;
	       var screen_height = 450;
	        window.open('ContractHistoryPopUp.jsf?contractId='+document.getElementById("formRequest:hdnContractId").value,'_blank','width='+(screen_width-100)+',height='+(screen_height)+',left=50,top=150,scrollbars=yes,status=yes'); 
        }
    
	function showSponsorManagerReadOnlyPopup(personIdName)
	{
	   //ID:NAME
	   var personIdNameArr=personIdName.split(":");
	   showPersonReadOnlyPopup(personIdNameArr[0]);
	}    
	function showPopup(personType)
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   //window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
	    window.open('SearchPerson.jsf?persontype='+personType+'&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	
	function showPartnerOccupierPopUp(persontype)
	{
    	var screen_width = 1024;
	    var screen_height = 470;
	    window.open('SearchPerson.jsf?persontype='+persontype+'&viewMode=popup&selectMode=many' ,'_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	
	}
	function showPersonManagerPopUp(persontype,displaycontrolname,hdncontrolname,hdndisplaycontrolname,clickable)
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   if(clickable=='true')
	   {
	     window.open('SearchPerson.jsf?persontype='+persontype+'&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	   }
	}
	
	
	function showPaymentSchedulePopUp()
	{
	 var screen_width = screen.width;
	   var screen_height = screen.height;
	   
	   window.open('PaymentSchedule.jsf?totalContractValue='+document.getElementById("formRequest:hdntotalContract").value,'_blank','width='+(screen_width-500)+',height='+(screen_height-300)+',left=300,top=250,scrollbars=yes,status=yes');
	   
	}
    
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		    
		   
		    if(hdnPersonType=='TENANT')
		    {
			 //document.getElementById("formRequest:txtTenantName").value=personName;
			 document.getElementById("formRequest:hdntenantId").value=personId;
	         //document.getElementById("formRequest:hdntenanttypeId").value=typeId;
	         
	        }
	        else if(hdnPersonType=='SPONSOR')
	        {
	         document.getElementById("formRequest:sponsorPerson").value=personName;
			 document.getElementById("formRequest:hdnSponsorPersonId").value=personId+":"+personName;
	         
	        }
	        else if(hdnPersonType=='MANAGER')
	        {
	         document.getElementById("formRequest:managerPerson").value=personName;
			 document.getElementById("formRequest:hdnManagerPersonId").value=personId+":"+personName;
	         
	        }
	        else if(hdnPersonType=='APPLICANT')
	        {
	         document.getElementById("formRequest:txtName").value=personName;
			 document.getElementById("formRequest:hdnApplicantId").value=personId;
	         //document.getElementById("formRequest:hdnApplicantPerson").value=personName;
	       
	        }
	        document.forms[0].submit();
	       
	}
	function deleteContractPerson(PersonType)
	{
	
	        if(PersonType=='SPONSOR')
	        {
	         document.getElementById("formRequest:sponsorPerson").value="";
			 document.getElementById("formRequest:hdnSponsorPersonId").value="";

	        }
	        else if(PersonType=='MANAGER')
	        {
	         document.getElementById("formRequest:managerPerson").value="";
			 document.getElementById("formRequest:hdnManagerPersonId").value="";

	         
	        }
	
	}
	function isDeleteContractPersonShow(PersonType)
	{
	       
		    if(PersonType=='SPONSOR'  && document.getElementById("formRequest:hdnSponsorPersonId").value.length<=0)
	        {
			 return false;
	        }
	        else if(PersonType=='MANAGER' && document.getElementById("formRequest:hdnManagerPersonId").value.length<=0)
	        {
	         return false;
	        }
	        return true;
	}
    
	function populateUnit(unitId,unitNum,usuageTypeId,unitRentAmount,unitUsuageType,propertyCommercialName,unitAddress,unitStatusId,unitStatus)
	{
	
	//document.getElementById("formRequest:hdnunitRefNumber").value=unitNum;
	document.getElementById("formRequest:hdnunitRentAmount").value=unitRentAmount;
	document.getElementById("formRequest:hdnunitId").value=unitId;
	//document.getElementById("formRequest:hdnunitUsuageTypeId").value=usuageTypeId;
	//document.getElementById("formRequest:hdnunitUsuageType").value=unitUsuageType;
	//document.getElementById("formRequest:hdnPropertyCommercialName").value=propertyCommercialName;
	//document.getElementById("formRequest:hdnUnitStatusId").value=unitStatusId;
	//document.getElementById("formRequest:hdnPropertyAddress").value=unitAddress;
	//document.getElementById("formRequest:hdnunitStatusDesc").value=unitStatus;
	}
	function populateAuctionUnit(unitId,unitNum,usuageTypeId,unitRentAmount,auctionUnitId,auctionNumber,unitAddress,propertyCommercialName,unitUsuageType,unitStatus,unitStatusId,bidderId)
	{
	 
	 //document.getElementById("formRequest:hdnunitRefNumber").value=unitNum;
	 document.getElementById("formRequest:hdnunitRentAmount").value=unitRentAmount;
	 document.getElementById("formRequest:hdnunitId").value=unitId;
	 document.getElementById("formRequest:hdntenantId").value=bidderId;
	 //document.getElementById("formRequest:hdnunitUsuageTypeId").value=usuageTypeId;
	//document.getElementById("formRequest:hdnunitUsuageType").value=unitUsuageType;
	//document.getElementById("formRequest:hdnPropertyCommercialName").value=propertyCommercialName;
	//document.getElementById("formRequest:hdnUnitStatusId").value=unitStatusId;
	//document.getElementById("formRequest:hdnPropertyAddress").value=unitAddress;
	//document.getElementById("formRequest:hdnunitStatusDesc").value=unitStatus;
	
	}
	
	  function isSponsorMangerDisplay()
      {
      
        return document.getElementById("formRequest:hdnSponsorMangerDisplay").value;
      }
      
      
    </script>
 	 <style type="text/css">
    .test { width: 22px; }
 </style>

				<!-- Header -->
	
		<body class="BODY_STYLE" >
	     <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
						 <c:choose>
					<c:when test="${!pages$amendLeaseContract.isViewModePopUp}">
					 <td colspan="2">
				        <jsp:include page="header.jsp"/>
				     </td>
				     </c:when>
		        </c:choose>
	            </tr>
				<tr>
				    <c:choose>
					 <c:when test="${!pages$amendLeaseContract.isViewModePopUp}">
					  <td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					  </td>
				     </c:when>
		            </c:choose>
					<td width="83%"   valign="top" class="divBackgroundBody">
						<h:form id="formRequest" style="width:99%;HEIGHT:95%;"  enctype="multipart/form-data">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
							<tr>
							    <td class="HEADER_TD" style="width:70%;" >
										 <h:outputLabel value="#{pages$amendLeaseContract.pageTitle}" styleClass="HEADER_FONT"/>
								</td>
						        <td >
									&nbsp;
								</td>
								
								
							</tr>
						</table>
					        <table width="99%" height="95%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
                                 <tr valign="top">
                                  <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
                                   </td>    
									<td   height="100%" valign="top" nowrap="nowrap">
									<div  class="SCROLLABLE_SECTION" style="width:815px;" >
									  
											<table border="0" class="layoutTable" width="90%" style="margin-left:15px;margin-right:15px;" >
											<tr>
													<td >
													    <h:outputText  id="successmsg"   escape="false" styleClass="INFO_FONT"  value="#{pages$amendLeaseContract.successMessages}"/>
														<h:outputText  id="errormsg"     escape="false" styleClass="ERROR_FONT" value="#{pages$amendLeaseContract.errorMessages}"/>
														<h:inputHidden id="hdnContractId" value="#{pages$amendLeaseContract.contractId}" />
														<h:inputHidden id="hdnContractCreatedOn" value="#{pages$amendLeaseContract.contractCreatedOn}" />
														<h:inputHidden id="hdnContractCreatedBy" value="#{pages$amendLeaseContract.contractCreatedBy}" />
														<h:inputHidden id="hdntenantId" value="#{pages$amendLeaseContract.hdnTenantId}" />
                                           			    <h:inputHidden id="hdnunitRentAmount" value="#{pages$amendLeaseContract.unitRentAmount}" />
													    <h:inputHidden id="hdnManagerPersonId" value="#{pages$amendLeaseContract.hdnManagerId}" />
														<h:inputHidden id="hdntotalContract" value="#{pages$amendLeaseContract.txttotalContractValue}"/>
												        <h:inputHidden id="pageMode"  value="#{pages$amendLeaseContract.pageMode}" />
													    <h:inputHidden id="hdnunitId" value="#{pages$amendLeaseContract.hdnUnitId}" />
                                   			            <h:inputHidden id="hdnSponsorPersonId" value="#{pages$amendLeaseContract.hdnSponsorId}" />
													    <h:inputHidden id="hdnApplicantId"  value="#{pages$amendLeaseContract.hdnApplicantId}" />											        
													</td>
												</tr>
											</table>
										
										<div class="MARGIN" style="height:410px;margin-bottom:0px;" >
   
						 		  <table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
										        </table> 
  		                    <rich:tabPanel  id="tabPanel"  binding="#{pages$amendLeaseContract.tabPanel}" style="height:320px;" headerSpacing="0">
								 
						        <rich:tab id="appDetailsTab" binding="#{pages$amendLeaseContract.tabApplicationDetails}" title="#{msg['commons.tab.applicationDetails']}" label="#{msg['contract.tabHeading.ApplicationDetails']}" >
									<%@ include file="ApplicationDetails.jsp"%>
															
									</rich:tab>		   
								    <rich:tab id="tabBasicInfo"  action="#{pages$amendLeaseContract.tabBasicInfo_Click}" title="#{msg['contract.tab.BasicInfo']}"  label="#{msg['contract.tab.BasicInfo']}" >
								    
										
										<t:div styleClass="TAB_DETAIL_SECTION" style="width:100%" >
										
										
										<t:panelGrid id="basicInfoTable" cellpadding="1px" width="100%" cellspacing="3px" styleClass="TAB_DETAIL_SECTION_INNER"  columns="4"
										columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2"
										>
												        <h:outputLabel styleClass="LABEL"  value="#{msg['commons.referencenumber']}:" binding="#{pages$amendLeaseContract.lblRefNum}"/>
													    <h:inputText styleClass="READONLY" maxlength="20"   id="txtRefNum" readonly="true" binding="#{pages$amendLeaseContract.txtRefNumber}" value="#{pages$amendLeaseContract.refNum}" style="width:83%;"></h:inputText>
												        <h:outputLabel styleClass="LABEL"  value="#{msg['commons.status']}:"></h:outputLabel>
														<h:inputText styleClass="READONLY" id="txtcontractStatus"   style="width:83%;" readonly="true" maxlength="20"  value="#{pages$amendLeaseContract.contractStatus}"/>
														<h:panelGroup>
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL" value="#{msg['contract.tenantName']}:"></h:outputLabel>
												        </h:panelGroup>
												        <h:panelGroup>
												          <h:inputText styleClass="READONLY" id="txtTenantName" style="width:83%"   readonly="true" title="#{pages$amendLeaseContract.txtTenantName}"  value="#{pages$amendLeaseContract.txtTenantName}" ></h:inputText>
												          <h:graphicImage  url="../resources/images/app_icons/Search-tenant.png" binding="#{pages$amendLeaseContract.imgTenant}" onclick="showPopup('#{pages$amendLeaseContract.personTenant}');" style ="MARGIN: 0px 0px -4px; CURSOR: hand;" alt="#{msg[contract.searchTenant]}" ></h:graphicImage>
                										  <h:graphicImage binding="#{pages$amendLeaseContract.imgViewTenant}"title="#{msg['commons.view']}" url="../resources/images/app_icons/Tenant.png" style ="MARGIN: 0px 0px -4px; CURSOR: hand;"  onclick="javaScript:showPersonReadOnlyPopup('#{pages$amendLeaseContract.hdnTenantId}');"/>
                										</h:panelGroup>
                										<h:outputLabel styleClass="LABEL"  value="#{msg['tenants.tenantsType']}:"></h:outputLabel>
                                                        <h:inputText styleClass="READONLY"  id="txtTenantType" readonly="true" style="width:83%" value="#{pages$amendLeaseContract.txtTenantType}" title="#{pages$amendLeaseContract.txtTenantType}" ></h:inputText>
												        <h:outputLabel styleClass="LABEL"  value="#{msg['contract.issueDate']}:"></h:outputLabel>
														<rich:calendar  id="contractIssueDate"  value="#{pages$amendLeaseContract.contractIssueDate}" 
											                            popup="true" datePattern="#{pages$amendLeaseContract.dateFormat}" 
											                            binding="#{pages$amendLeaseContract.issueDateCalendar}"
											                            timeZone="#{pages$amendLeaseContract.timeZone}"
											                            showApplyButton="false" disabled="true" 
											                            enableManualInput="false" cellWidth="24px" inputStyle="width:83%"
											                            locale="#{pages$amendLeaseContract.locale}"
											            ></rich:calendar>
											            <h:panelGroup>
													    <h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel id="lblContractType" styleClass="LABEL"  value="#{msg['contract.contractType']}:"></h:outputLabel>
														</h:panelGroup>
														<h:selectOneMenu id="selectcontractType" style="width: 83%; " binding="#{pages$amendLeaseContract.cmbContractType}" value="#{pages$amendLeaseContract.selectOneContractType}">
																<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$amendLeaseContract.contractTypeList}" />
															 <a4j:support event="onchange" action="#{pages$amendLeaseContract.selectContractType_Change}" reRender="tabPanel" />
														</h:selectOneMenu>	   
														
													
														<h:outputLabel  styleClass="LABEL"  value="#{msg['contract.date.Start']}:"></h:outputLabel>
														
														<rich:calendar	id="contractStartDate" value="#{pages$amendLeaseContract.contractStartDate}" 
											                            popup="true" datePattern="#{pages$amendLeaseContract.dateFormat}"
											                            binding="#{pages$amendLeaseContract.startDateCalendar}" 
											                            timeZone="#{pages$amendLeaseContract.timeZone}"
											                            showApplyButton="false"  
											                            enableManualInput="false" cellWidth="24px" inputStyle="width:83%"
											                            locale="#{pages$amendLeaseContract.locale}"
											                            todayControlMode="hidden" 
											                            
											          >
							                          <a4j:support event="onchanged" action="#{pages$amendLeaseContract.valueChangeListerner_RentValue}" reRender="totalContract,tbl_PaymentSchedule,errormsg" />
							                          </rich:calendar>
															<h:outputLabel   value="#{msg['contract.date.expiry']}:"></h:outputLabel>
													
													   <rich:calendar id="datetimeContractEndDate" value="#{pages$amendLeaseContract.contractEndDate}" 
													  binding="#{pages$amendLeaseContract.endDateCalendar}"
							                          popup="true" datePattern="#{pages$amendLeaseContract.dateFormat}" timeZone="#{pages$amendLeaseContract.timeZone}" showApplyButton="false" 
							                          enableManualInput="false"  cellWidth="24px" inputStyle="width:83%" todayControlMode="hidden"
							                          >
							                          <a4j:support event="onchanged" action="#{pages$amendLeaseContract.valueChangeListerner_RentValue}" reRender="totalContract,tbl_PaymentSchedule,errormsg" />
							                          
							                          </rich:calendar>
													
														<h:outputLabel styleClass="LABEL"  value="#{msg['contract.TotalContractValue']}:"></h:outputLabel>
													    <h:inputText id="totalContract" style="width:83%;"  styleClass="A_RIGHT_NUM READONLY" maxlength="15"  readonly="true"  value="#{pages$amendLeaseContract.txttotalContractValue}">
													    
												        </h:inputText>
												        <h:outputLabel id="lblSponsor" styleClass="LABEL"  rendered="#{pages$amendLeaseContract.isContractCommercial}" value="#{msg['contract.person.sponsor']}:"></h:outputLabel>
												        
												        <h:panelGroup id="grpSponsorIcons" rendered="#{pages$amendLeaseContract.isContractCommercial}">
												          <h:inputText id="sponsorPerson"   readonly="true"  style="width:83%"  title="#{pages$amendLeaseContract.txtSponsor}"  value="#{pages$amendLeaseContract.txtSponsor}" >
												          
												          </h:inputText>
												          <h:graphicImage   id="imgSearchSpnsr"											             
		                                         			                alt="#{msg['contract.person.sponsor']}"
																			url="../resources/images/app_icons/Search-tenant.png"
																		      onclick="javascript:showPersonManagerPopUp('#{pages$amendLeaseContract.personSponsor}','formRequest:sponsorPerson','formRequest:hdnSponsorPersonId','formRequest:hdnSponsorPerson','#{pages$amendLeaseContract.isItemEditable}')"
																		     style ="MARGIN: 0px 0px -4px;CURSOR: hand;"
																		     binding="#{pages$amendLeaseContract.imgSponsor}"
														  ></h:graphicImage>
														  <h:graphicImage  id="imgViewSpnsr" binding="#{pages$amendLeaseContract.imgViewSponsor}" onclick="javaScript:showSponsorManagerReadOnlyPopup('#{pages$amendLeaseContract.hdnSponsorId}');" title="#{msg['commons.view']}" url="../resources/images/app_icons/Tenant.png" style ="MARGIN: 0px 0px -4px; CURSOR: hand;"  />
														  
														 
														</h:panelGroup>
												        
                										<h:outputLabel id="lblManager" styleClass="LABEL"  rendered="#{pages$amendLeaseContract.isContractCommercial}" value="#{msg['contract.person.Manager']}:"></h:outputLabel>
                                                        <h:panelGroup id="grpManagerIcons" rendered="#{pages$amendLeaseContract.isContractCommercial}"> 
	                                                        <h:inputText id="managerPerson" readonly="true" 
													           style="width:83%"    
													           value="#{pages$amendLeaseContract.txtManager}" title="#{pages$amendLeaseContract.txtManager}" >
													        </h:inputText>
													        <h:graphicImage id="imgManager"  style="MARGIN: 0px 0px -4px;CURSOR: hand;" 
			                                         			            alt="#{msg['contract.person.Manager']}" 
																			url="../resources/images/app_icons/Search-tenant.png"
																			onclick="javascript:showPersonManagerPopUp('#{pages$amendLeaseContract.personManager}',
																				                   'formRequest:managerPerson',
																				                   'formRequest:hdnManagerPersonId',
																				                   'formRequest:hdnManagerPerson',
																				                   '#{pages$amendLeaseContract.isItemEditable}')"
																			binding="#{pages$amendLeaseContract.imgManager}">
															</h:graphicImage>
															<h:graphicImage id="imgViewMgr"onclick="javaScript:showSponsorManagerReadOnlyPopup('#{pages$amendLeaseContract.hdnManagerId}');"  binding="#{pages$amendLeaseContract.imgViewManager}" title="#{msg['commons.view']}" url="../resources/images/app_icons/Tenant.png" style ="MARGIN: 0px 0px -4px; CURSOR: hand;"  ></h:graphicImage>
															<h:graphicImage id="imgDeleteManager"  style="MARGIN: 0px 0px -4px;CURSOR: hand;" 
			                                         			            title="#{msg['commons.delete']}" 
																			url="../resources/images/delete_icon.png"
																			onclick="javascript:deleteContractPerson('#{pages$amendLeaseContract.personManager}')"
																			binding="#{pages$amendLeaseContract.imgDeleteManager}">
																			<a4j:support event="onclick"   reRender="grpManagerIcons,hdnManagerPersonId,managerPerson,imgManager,imgDeleteManager,imgViewMgr" 
												                            action="#{pages$amendLeaseContract.imgDelManager_Click}"/>
															</h:graphicImage>
															
															
															
															
                                                            
                                                        </h:panelGroup>
														 

													

												
								  </t:panelGrid>		              
                                          
                                          </t:div>
								    </rich:tab>
                                    
                                         <rich:tab   id="tabUnit" title="#{msg['contract.tab.UnitInfo']}" label="#{msg['contract.tab.UnitInfo']}" >
                                         <t:div style="width:100%;MARGIN: 0px 4px 0px 0px;">
                                           <t:panelGrid width="100%"   border="0" columns="3"  columnClasses="BUTTON_TD" >
												<t:panelGroup colspan="10">
												<h:commandButton  id="auctionUnitspop" styleClass="BUTTON" 
												 action="#{pages$amendLeaseContract.openAuctionUnitsPopUp}" value="#{msg['contract.auctionUnits']}" binding="#{pages$amendLeaseContract.btnAddAuctionUnit}" 
												 style="width:150px;"
												/>
												<h:commandButton  id="unit" styleClass="BUTTON" 
												 action="#{pages$amendLeaseContract.openUnitsPopUp}"
												 value="#{msg['units.unit']}"   
												 binding="#{pages$amendLeaseContract.btnAddUnit}"
												 style="width:150px;"
												/>
													
                                   		</t:panelGroup>
                                   		</t:panelGrid> 
                                   		</t:div>
                                   		<t:div styleClass="TAB_DETAIL_SECTION" style="width:100%" >
										     <t:panelGrid id="unitInfoTable" cellpadding="1px" width="100%" cellspacing="3px" styleClass="TAB_DETAIL_SECTION_INNER"  columns="4"
										      columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2"
										     >
										                <h:panelGroup>
												          <h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"  value="#{msg['unit.unitNumber']}:"/>
													    </h:panelGroup>
													    <h:inputText maxlength="20" styleClass="READONLY"  id="txtunitNumber" readonly="true"  value="#{pages$amendLeaseContract.unitDataItem.unitNumber}" style="width:85%;"></h:inputText>
												        <h:outputLabel styleClass="LABEL"  value="#{msg['floor.floorNo']}:"></h:outputLabel>
														<h:inputText id="txtfloorNumber" styleClass="READONLY"  style="width:85%;" readonly="true" maxlength="20"  value="#{pages$amendLeaseContract.unitDataItem.floorNumber}"/>
														<h:outputLabel styleClass="LABEL" value="#{msg['unit.endowed/minorName']}:"></h:outputLabel>
												        <h:inputText id="txtEndowedMinorName" style="width:85%;" styleClass="READONLY"   readonly="true"   value="#{pages$amendLeaseContract.unitDataItem.propertyEndowedName}" ></h:inputText>
                										<h:outputLabel styleClass="LABEL" value="#{msg['property.propertyNumber']}:"></h:outputLabel>
												        <h:inputText styleClass="READONLY" id="txtpropertyNumber" style="width:85%;"    readonly="true"   value="#{pages$amendLeaseContract.unitDataItem.propertyNumber}" ></h:inputText>
												        <h:outputLabel styleClass="LABEL" value="#{msg['unit.usage']}:"></h:outputLabel>
												        <h:inputText styleClass="READONLY" id="txtunitusageEn" style="width:85%;" rendered="#{pages$amendLeaseContract.isEnglishLocale}" readonly="true"   value="#{pages$amendLeaseContract.unitDataItem.usageTypeEn}" ></h:inputText>
								                        <h:inputText styleClass="READONLY"  id="txtunitusageAr" style="width:85%;" rendered="#{pages$amendLeaseContract.isArabicLocale}"  readonly="true"   value="#{pages$amendLeaseContract.unitDataItem.usageTypeAr}" ></h:inputText>
								                        <h:outputLabel styleClass="LABEL" value="#{msg['contact.country']}:"></h:outputLabel>
												        <h:inputText styleClass="READONLY" id="txtCountryEn" style="width:85%;"  rendered="#{pages$amendLeaseContract.isEnglishLocale}"   readonly="true"   value="#{pages$amendLeaseContract.unitDataItem.countryEn}" ></h:inputText>
								                        <h:inputText styleClass="READONLY"  id="txtCountryAr" style="width:85%;" rendered="#{pages$amendLeaseContract.isArabicLocale}"    readonly="true"   value="#{pages$amendLeaseContract.unitDataItem.countryAr}" ></h:inputText>
								                        <h:outputLabel styleClass="LABEL" value="#{msg['commons.Emirate']}:"></h:outputLabel>
												        <h:inputText styleClass="READONLY" id="txtEmirateEn" style="width:85%;"  rendered="#{pages$amendLeaseContract.isEnglishLocale}"   readonly="true"   value="#{pages$amendLeaseContract.unitDataItem.stateEn}" ></h:inputText>
								                        <h:inputText styleClass="READONLY"  id="txtEmirateAr" style="width:85%;" rendered="#{pages$amendLeaseContract.isArabicLocale}"    readonly="true"   value="#{pages$amendLeaseContract.unitDataItem.stateAr}" ></h:inputText>
								                        <h:outputLabel styleClass="LABEL"  value="#{msg['commons.area']}:"/>
													    <h:inputText styleClass="READONLY"   id="txtunitArea" readonly="true"  value="#{pages$amendLeaseContract.unitDataItem.unitArea}" style="width:85%;"></h:inputText>
							                            <h:outputLabel styleClass="LABEL"  value="#{msg['contact.street']}:"/>
													    <h:inputText styleClass="READONLY"   id="txtstreet" readonly="true"  value="#{pages$amendLeaseContract.unitDataItem.street}" style="width:85%;"></h:inputText>
													    <h:outputLabel styleClass="LABEL"  value="#{msg['unit.address']}:"/>
													    <h:inputText styleClass="READONLY"   id="txtaddress" readonly="true"  value="#{pages$amendLeaseContract.unitDataItem.unitAddress}" style="width:85%;"></h:inputText>
													    <h:outputLabel styleClass="LABEL"  value="#{msg['unit.offeredRent']}:"/>
													    <h:inputText styleClass="READONLY A_RIGHT_NUM"   id="txtofferedRent" readonly="true"  value="#{pages$amendLeaseContract.unitDataItem.rentValue}"  style="width:85%;"><f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/></h:inputText>					     
								                        <h:outputLabel styleClass="LABEL"  value="#{msg['unit.rentValue']}:"/>
													    <h:inputText   styleClass="A_RIGHT_NUM" id="txtactualRent" binding="#{pages$amendLeaseContract.txt_ActualRent}" readonly="#{pages$amendLeaseContract.unitDataItem.auctionUnitId!=null}"  value="#{pages$amendLeaseContract.unitRentAmount}" style="width:85%;">
													    
													    </h:inputText>
													    
								             </t:panelGrid>		              
                                        </t:div>
                                          
                                      
                                      
                                      
                                      </rich:tab>	
   
                                <rich:tab   id="tabPersonOccupier" title="#{msg['contract.person.Occupier']}" label="#{msg['contract.person.Occupier']}" binding="#{pages$amendLeaseContract.tabOccupier}" rendered="#{pages$amendLeaseContract.isCompanyTenant}">
                                    
                                   			<t:div style="width:100%;MARGIN: 0px 4px 0px 0px;">
                                   			<t:panelGrid width="100%" border="0" columns="3"  columnClasses="BUTTON_TD" >
                                           <t:panelGroup colspan="12" >  
                                            
	                               			<h:commandButton  id="btnOccupiers" styleClass="BUTTON" 
												 	                  value="#{msg['contract.AddOccupier']}"
												 	                  binding="#{pages$amendLeaseContract.btnOccuipers}"
												 	                  style="width:150px;"
												 	                  onclick="javascript:showPartnerOccupierPopUp('#{pages$amendLeaseContract.personOccupier}')"
														/>
												
                                   		</t:panelGroup>
                                   		</t:panelGrid> 
                                   		</t:div>
                                   			
                                   			
                                             <t:div  styleClass="contentDiv" style="width:99%"  >
					                                               <t:dataTable id="tbl_occupier"
																	 rows="9" width="100%"
																	 value="#{pages$amendLeaseContract.prospectiveOccupierDataList}"
																		binding="#{pages$amendLeaseContract.propspectiveOccupierDataTable}"
																		preserveDataModel="true" preserveSort="true" var="prospectiveOccupierDataItem"
																		rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
																	>
																	<t:column id="PersonName">
					
					
																		<f:facet name="header" >
					
																			<t:outputText value="#{msg['customer.firstname']}" />
					
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" id="txt_name" value="#{prospectiveOccupierDataItem.firstName} #{prospectiveOccupierDataItem.middleName} #{prospectiveOccupierDataItem.lastName}" />
																	</t:column>
																	<t:column id="Passport">
					
					
																		<f:facet name="header">
					
																			<t:outputText  value="#{msg['customer.passportNumber']}" />
					
																		</f:facet>
																		<t:outputText styleClass="A_RIGHT_NUM" title="" value="#{prospectiveOccupierDataItem.passportNumber}" />
																	</t:column>
																	
																	<t:column id="Visa" >
					
					
																		<f:facet name="header">
					
																			<t:outputText  value="#{msg['customer.residenseVisaNumber']}" />
					
																		</f:facet>
																		<t:outputText styleClass="A_RIGHT_NUM" value="#{prospectiveOccupierDataItem.residenseVisaNumber}" />
																	</t:column>
																													
																	<t:column id="License" >
					
					
																		<f:facet name="header">
					
																			<t:outputText  value="#{msg['customer.drivingLicenseNumber']}" />
					
																		</f:facet>
																		<t:outputText styleClass="A_RIGHT_NUM" title="" value="#{prospectiveOccupierDataItem.drivingLicenseNumber}" />
																	</t:column>
																													
																	<t:column id="SSN" >
																		<f:facet name="header">
																			<t:outputText  value="#{msg['customer.socialSecNumber']}" />
																		</f:facet>
																		<t:outputText styleClass="A_RIGHT_NUM" title="" value="#{prospectiveOccupierDataItem.socialSecNumber}" />
																	</t:column>
																	<t:column id="DeleteOccupier">
																		<f:facet name="header">
																			<t:outputText  value="#{msg['commons.action']}" />
																		</f:facet>
																		<h:graphicImage style="padding-left:4px;" title="#{msg['commons.view']}"
																				url="../resources/images/app_icons/Tenant.png" 
																				onclick="javaScript:showPersonReadOnlyPopup('#{prospectiveOccupierDataItem.personId}');"
																				/>
																		<a4j:commandLink id="del_cmdLink_tblOccupier" reRender="tbl_occupier" action="#{pages$amendLeaseContract.btnOccupierDelete_Click}" >
																	       <h:graphicImage id="deleteOccupier" binding="#{pages$amendLeaseContract.btnOccupierDelete}" 
																		title="#{msg['commons.delete']}" 
																		url="../resources/images/delete_icon.png"/>														
																	</a4j:commandLink>
			
																	</t:column>
																   </t:dataTable>										
										                          </t:div>
										                           <t:div id="pagingDivPersonOccupier" styleClass="contentDivFooter" style="width:100%;">
					
																	<t:dataScroller id="scrollerPersonOccupier" for="tbl_occupier" paginator="true"
																		fastStep="1" paginatorMaxPages="15" immediate="false"
																		paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true" 
																	    pageIndexVar="pageNumber"
																		paginatorTableStyle="grid_paginator" layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																		paginatorActiveColumnStyle="font-weight:bold;"
																	    paginatorRenderLinkForActive="false" 
																		styleClass="SCH_SCROLLER">
					                                                	<f:facet  name="first">
																				<t:graphicImage    url="../#{path.scroller_first}"  id="lblFPersonOccupier"></t:graphicImage>
																		</f:facet>
					
																		<f:facet name="fastrewind">
																			<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRPersonOccupier"></t:graphicImage>
																		</f:facet>
					
																		<f:facet name="fastforward">
																			<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFPersonOccupier"></t:graphicImage>
																		</f:facet>
					
																		<f:facet name="last">
																			<t:graphicImage  url="../#{path.scroller_last}"  id="lblLPersonOccupier"></t:graphicImage>
																		</f:facet>
					
																	</t:dataScroller>
									
                                                                </t:div>
										                       
                                      
                                      
                                      
                                      </rich:tab>
                                      <rich:tab  id="tabPersonPartnr"   title="#{msg['contract.person.Partners']}" label="#{msg['contract.person.Partners']}" binding="#{pages$amendLeaseContract.tabPartner}" rendered="#{pages$amendLeaseContract.isContractCommercial}">
                                       <t:div style="width:100%;MARGIN: 0px 4px 0px 0px;">
                                   		<t:panelGrid width="100%" border="0" columns="3"  columnClasses="BUTTON_TD" >
                                           <t:panelGroup colspan="12" >  
                                                 
												 <h:commandButton  id="btnPartners" styleClass="BUTTON" 
												 	                  value="#{msg['contract.AddPartner']}"
												 	                  binding="#{pages$amendLeaseContract.btnPartners}"
												 	                  style="width:150px;"
												                      onclick="javascript:showPartnerOccupierPopUp('#{pages$amendLeaseContract.personPartner}')"
												 />			    
                                   		</t:panelGroup>
                                   		</t:panelGrid> 
                                   		</t:div>
                                                                <t:div  styleClass="contentDiv" style="width:99%"  >
					                                               <t:dataTable id="partnerDataList"
																		rows="9" width="100%"
																			value="#{pages$amendLeaseContract.prospectivepartnerDataList}"
																		binding="#{pages$amendLeaseContract.propspectivepartnerDataTable}"
																		preserveDataModel="true" preserveSort="true" var="prospectivepartnerDataItem"
																		rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
																	>
																	<t:column id="PersonName" >
					
					
																		<f:facet name="header" >
					
																			<t:outputText id="lblpartnerFirstName" value="#{msg['customer.firstname']}" />
					
																		</f:facet>
																		<t:outputText id="txtpartnerFirstName" styleClass="A_LEFT"  value="#{prospectivepartnerDataItem.firstName} #{prospectivepartnerDataItem.middleName} #{prospectivepartnerDataItem.lastName}" />
																	</t:column>
																	<t:column id="Passport">
					
					
																		<f:facet name="header">
					
																			<t:outputText id="lblpartnerPassport" value="#{msg['customer.passportNumber']}" />
					
																		</f:facet>
																		<t:outputText  id="txtpartnerPassport" styleClass="A_RIGHT_NUM"  title="" value="#{prospectivepartnerDataItem.passportNumber}" />
																	</t:column>
																	
																	<t:column id="Visa" >
					
					
																		<f:facet name="header">
					
																			<t:outputText id="lblpartnerVisa" value="#{msg['customer.residenseVisaNumber']}" />
					
																		</f:facet>
																		<t:outputText id="txtpartnerVisa" styleClass="A_RIGHT_NUM"  value="#{prospectivepartnerDataItem.residenseVisaNumber}" />
																	</t:column>
																													
																	<t:column id="License" >
					
					
																		<f:facet name="header">
					
																			<t:outputText id="lblpartnerLicense" value="#{msg['customer.drivingLicenseNumber']}" />
					
																		</f:facet>
																		<t:outputText id="txtpartnerLicense" styleClass="A_RIGHT_NUM" title="" value="#{prospectivepartnerDataItem.drivingLicenseNumber}" />
																	</t:column>
																													
																	<t:column id="SSN" >
																		<f:facet name="header">
																			<t:outputText  id="lblSSN" value="#{msg['customer.socialSecNumber']}" />
																		</f:facet>
																		<t:outputText id="TXTSSN" styleClass="A_RIGHT_NUM" title=""  value="#{prospectivepartnerDataItem.socialSecNumber}" />
																	</t:column>
														            <t:column id="DeletePartner" >
																		<f:facet name="header">
																			<t:outputText  value="#{msg['commons.action']}" />
																		</f:facet>
																		<h:graphicImage style="padding-left:4px;" title="#{msg['commons.view']}"
																				url="../resources/images/app_icons/Tenant.png" 
																				onclick="javaScript:showPersonReadOnlyPopup('#{prospectivepartnerDataItem.personId}');"
																				/>
																		<a4j:commandLink reRender="partnerDataList" 
																		 action="#{pages$amendLeaseContract.btnPartnerDelete_Click}"
																		 
																		 >
																	       <h:graphicImage id="deletePartner" binding="#{pages$amendLeaseContract.btnPartnerDelete}" 
																		                   title="#{msg['commons.delete']}" 
																		                   url="../resources/images/delete_icon.png"/>														
																	    </a4j:commandLink>
			
																	</t:column>
			
																	
																   </t:dataTable>										
										                        </t:div>
										                        <t:div id="pagingDivPersonPartnr" styleClass="contentDivFooter" style="width:100%;">
					
																	<t:dataScroller id="scrollerPersonPartnr" for="partnerDataList" paginator="true"
																		fastStep="1" paginatorMaxPages="15" immediate="false"
																		paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true" 
																	    pageIndexVar="pageNumber"
																		paginatorTableStyle="grid_paginator" layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																		paginatorActiveColumnStyle="font-weight:bold;"
																	    paginatorRenderLinkForActive="false" 
																		styleClass="SCH_SCROLLER">
					                                                	<f:facet  name="first">
																			
																			
																			<t:graphicImage    url="../#{path.scroller_first}"  id="lblFPersonPartnr"></t:graphicImage>
																		</f:facet>
					
																		<f:facet name="fastrewind">
																			<t:graphicImage   url="../#{path.scroller_fastRewind}"  id="lblFRPersonPartnr"></t:graphicImage>
																		</f:facet>
					
																		<f:facet name="fastforward">
																			<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFPersonPartnr"></t:graphicImage>
																		</f:facet>
					
																		<f:facet name="last">
																			<t:graphicImage   url="../#{path.scroller_last}"  id="lblLPersonPartnr"></t:graphicImage>
											                            </f:facet>
					
																	</t:dataScroller>
									
                                                                </t:div>
										                    
                                      
                                      
                                      
                                      </rich:tab>
                                       <rich:tab id="tabCommercialActivity" rendered="#{pages$amendLeaseContract.isContractCommercial}" binding="#{pages$amendLeaseContract.tabCommercialActivity}" action="#{pages$amendLeaseContract.tabCommercialActivity_Click}" title="#{msg['contract.tab.CommercialActivity']}"  label="#{msg['contract.tab.CommercialActivity']}">
                                            
		                                                                        		
		                                    <t:div styleClass="contentDiv" style="align:center;"    >
		                                       <t:dataTable id="commercialActivityDataTable"
													rows="15" width="100%"
													value="#{pages$amendLeaseContract.commercialActivityList}"
													binding="#{pages$amendLeaseContract.commercialActivityDataTable}"
													preserveDataModel="false" preserveSort="false" var="commercialActivityItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												>
												 
												<t:column id="colActivityName" >


													<f:facet name="header" >

														<t:outputText value="#{msg['contract.tab.CommercialActivity.Name']}" />

													</f:facet>
													<t:outputText  styleClass="A_LEFT" value="#{commercialActivityItem.commercialActivityName}" />
												</t:column>
												<t:column id="colActivityDescEn" rendered="#{pages$amendLeaseContract.isEnglishLocale}">


													<f:facet name="header">

														<t:outputText  value="#{msg['contract.tab.CommercialActivity.description']}" />

													</f:facet>
													<t:outputText title="" styleClass="A_LEFT" value="#{commercialActivityItem.commercialActivityDescEn}" />
												</t:column>
												<t:column id="colActivityDescAr" rendered="#{pages$amendLeaseContract.isArabicLocale}">


													<f:facet name="header">

														<t:outputText  value="#{msg['contract.tab.CommercialActivity.description']}" />

													</f:facet>
													<t:outputText title=""  styleClass="A_LEFT" value="#{commercialActivityItem.commercialActivityDescAr}" />
												</t:column>
												<t:column id="colActivitySelected" >


													<f:facet name="header">
														<t:outputText  value="#{msg['commons.select']}" />
													</f:facet>
													    <t:selectBooleanCheckbox binding="#{pages$amendLeaseContract.chkCommercial}"
                                                                       value="#{commercialActivityItem.isSelected}"  />
												       
												</t:column>
												
											  </t:dataTable>										
											</t:div>
											<t:div styleClass="contentDivFooter" style="width:99%;align:center;MARGIN:0px 4px 0px 0px;">
                                                 
												<t:dataScroller id="scrollerd" for="commercialActivityDataTable" paginator="true"
													fastStep="1" paginatorMaxPages="2" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 
												    pageIndexVar="pageNumber"
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-weight:bold;"
												    paginatorRenderLinkForActive="false" 
													    styleClass="SCH_SCROLLER">
                                                	<f:facet name="first">
														
														<t:graphicImage url="../#{path.scroller_first}" id="lblFComAct"></t:graphicImage>
													</f:facet>

													<f:facet name="fastrewind">
														<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFRComAct"></t:graphicImage>
													</f:facet>

													<f:facet name="fastforward">
														<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFComAct"></t:graphicImage>
													</f:facet>

													<f:facet name="last">
														<t:graphicImage  url="../#{path.scroller_last}" id="lblLComAct"></t:graphicImage>
													</f:facet>

												</t:dataScroller>
									
                                           </t:div>
                                      
                                      </rich:tab>
                                      <rich:tab    id="tabPaidFacilities"  binding="#{pages$amendLeaseContract.tabFacilities}"  title="#{msg['contract.Facilities']}" label="#{msg['contract.Facilities']}"
                                                   action="#{pages$amendLeaseContract.tabPaidFacility_Click}"
                                      >
		                                      <t:div styleClass="contentDiv" style="align:center;"  >
		                                       <t:dataTable id="paidFaciliites"
													rows="15" width="100%"
														value="#{pages$amendLeaseContract.paidFacilitiesDataList}"
													binding="#{pages$amendLeaseContract.paidFacilitiesDataTable}"
													preserveDataModel="true" preserveSort="true" var="paidFacilitiesDataItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												>
												
												<t:column id="col2" >


													<f:facet name="header" >

														<t:outputText value="#{msg['paidFacilities.facilityName']}" />

													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{paidFacilitiesDataItem.facilityName}" />
												</t:column>
												<t:column id="col4">


													<f:facet name="header">

														<t:outputText  value="#{msg['paidFacilities.facilityDescription']}" />

													</f:facet>
													<t:outputText  styleClass="A_LEFT" value="#{paidFacilitiesDataItem.facilityDescription}" />
												</t:column>
												
											<t:column id="col5">
													<f:facet name="header">
														<t:outputText  value="#{msg['paidFacilities.Type']}" />
													</f:facet>
													<t:outputText  styleClass="A_LEFT" value="#{paidFacilitiesDataItem.isMandatory=='0'?msg['paidFacilities.IsMandatory.Optional']:msg['paidFacilities.IsMandatory.Mandatory']}" />
												</t:column>
																								
												<t:column id="col7" >


													<f:facet name="header">
														<t:outputText  value="#{msg['paidFacilities.rentValue']}" />
													</f:facet>
													<t:outputText title="" styleClass="A_RIGHT_NUM" value="#{paidFacilitiesDataItem.rent}">
													   <f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
													</t:outputText>
												</t:column>
												<t:column id="colFacilitySelected" >


													<f:facet name="header" >
														<t:outputText  value="#{msg['commons.select']}" />
													</f:facet>
													    <t:selectBooleanCheckbox  disabled="#{paidFacilitiesDataItem.isMandatory=='1'}"
													              binding="#{pages$amendLeaseContract.chkPaidFacilities}"
													              value="#{paidFacilitiesDataItem.selected}"  >
												            <a4j:support event="onclick"   reRender="totalContract" 
												             action="#{pages$amendLeaseContract.chkPaidFacilities_Changed}"/>
												         </t:selectBooleanCheckbox>
												</t:column>
												
																								
											  </t:dataTable>										
											</t:div>
							                 <t:div id="pagingDivPaidFacilities"  styleClass="contentDivFooter" style="width:99%;align:center;MARGIN:0px 4px 0px 0px;">
   
												<t:dataScroller id="scrollerPaidFacilities" for="paidFaciliites" paginator="true"
													        fastStep="1" paginatorMaxPages="15" immediate="false" 
													        paginatorTableClass="paginator"
															renderFacetsIfSinglePage="true" 
														    pageIndexVar="pageNumber"
															paginatorTableStyle="grid_paginator" layout="singleTable"
															paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
															paginatorActiveColumnStyle="font-weight:bold;"
														    paginatorRenderLinkForActive="false" 
														    styleClass="SCH_SCROLLER">
                                                	<f:facet  name="first">
														
														
														<t:graphicImage  url="../#{path.scroller_first}" id="lblFPaidFacilities"></t:graphicImage>
													</f:facet>

													<f:facet name="fastrewind">
														<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRPaidFacilities"></t:graphicImage>
													</f:facet>

													<f:facet name="fastforward">
														<t:graphicImage  url="../#{path.scroller_fastForward}"  id="lblFFPaidFacilities"></t:graphicImage>
													</f:facet>

													<f:facet name="last">
														<t:graphicImage  url="../#{path.scroller_last}" id="lblLPaidFacilities"></t:graphicImage>
													</f:facet>


												</t:dataScroller>
									
                                           </t:div>
							                         
										    
                                      
                                      
                                      </rich:tab>
                                      <rich:tab  id="tabPaymentSchedule" binding="#{pages$amendLeaseContract.tabPaymentTerms}" action="#{pages$amendLeaseContract.tabPaymentTerms_Click}"   title="#{msg['contract.paymentTerms']}" label="#{msg['contract.paymentTerms']}" >
                                            <t:div style="width:100%;MARGIN: 0px 4px 0px 0px;">
                                            <t:panelGrid width="100%" border="0" columns="3"  columnClasses="BUTTON_TD" >
                                           <t:panelGroup colspan="12" >  
                                                 
	                                   		   
				     	    					<h:commandButton  id="pmt_sch_add" styleClass="BUTTON" value="#{msg['contract.AddpaymentSchedule']}"
				     	    					                binding="#{pages$amendLeaseContract.btnAddPayments}"
				     	    					                action="#{pages$amendLeaseContract.btnAddPayments_Click}"
				     	    					                style="width:150px;"
														/>
                                   		  </t:panelGroup>
                                   		</t:panelGrid>  
                                   			</t:div>
                                   			 <t:div styleClass="contentDiv" style="width:98.2%"  >
                                                 <t:dataTable 
					                                id="tbl_PaymentSchedule" 
													rows="7" width="100%"
													value="#{pages$amendLeaseContract.paymentScheduleDataList}"
													binding="#{pages$amendLeaseContract.paymentScheduleDataTable}"
													preserveDataModel="true"  var="paymentScheduleDataItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												 >
												 <t:column id="select" style="width:5%;" >
													<f:facet name="header">
														<t:outputText id="selectTxt" value="#{msg['commons.select']}"/>
													</f:facet>
													<t:selectBooleanCheckbox id="selectChk" value="#{paymentScheduleDataItem.selected}" 
													rendered="#{paymentScheduleDataItem.isReceived == 'N'  && 
													            paymentScheduleDataItem.statusId==pages$amendLeaseContract.paymentSchedulePendingStausId && 
													            paymentScheduleDataItem.paymentScheduleId>0}"/>
												</t:column>
												 <t:column id="col_PaymentNumber" style="width:15%;" rendered="#{!pages$amendLeaseContract.isPageModeAdd}">
													<f:facet name="header" >
														<t:outputText value="#{msg['paymentSchedule.paymentNumber']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT"  value="#{paymentScheduleDataItem.paymentNumber}" />
												</t:column>
												<t:column id="colTypeEn" style="width:8%;" rendered="#{pages$amendLeaseContract.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentType']}" />
													</f:facet>
													<t:outputText     styleClass="A_LEFT"  title="" value="#{paymentScheduleDataItem.typeEn}" />
												</t:column>
												<t:column id="colTypeAr" style="width:8%;" rendered="#{pages$amendLeaseContract.isArabicLocale}">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentType']}" />
													</f:facet>
													<t:outputText     styleClass="A_LEFT"  title="" value="#{paymentScheduleDataItem.typeAr}" />
												</t:column>
												<t:column id="colDesc" >
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.description']}" />
													</f:facet>
													<t:outputText     styleClass="A_LEFT"  title="" value="#{paymentScheduleDataItem.description}" />
												</t:column>
												<t:column id="colDueOn" style="width:8%;">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentDueOn']}" />
													</f:facet>
													<t:outputText     styleClass="A_LEFT" value="#{paymentScheduleDataItem.paymentDueOn}" >
													<f:convertDateTime pattern="#{pages$amendLeaseContract.dateFormat}" timeZone="#{pages$amendLeaseContract.timeZone}" />
												    </t:outputText>
												</t:column>
                                                <t:column id="colPaymentDate" style="width:8%;">
													<f:facet name="header">

														<t:outputText  value="#{msg['paymentSchedule.paymentDate']}" >
                                                          <f:convertDateTime pattern="#{pages$amendLeaseContract.dateFormat}" timeZone="#{pages$amendLeaseContract.timeZone}" />
												        </t:outputText>
													</f:facet>
													<t:outputText     styleClass="A_LEFT"  title="" value="#{paymentScheduleDataItem.paymentDate}" >
												       <f:convertDateTime pattern="#{pages$amendLeaseContract.dateFormat}" timeZone="#{pages$amendLeaseContract.timeZone}" />
												    </t:outputText>
												</t:column>
																								
												<t:column id="colModeEn" style="width:8%;" rendered="#{pages$amendLeaseContract.isEnglishLocale}">


													<f:facet name="header" >

														<t:outputText value="#{msg['paymentSchedule.paymentMode']}" />

													</f:facet>
													<t:outputText    
													              styleClass="A_LEFT"  title=""
													              value="#{paymentScheduleDataItem.paymentModeEn}" />
												</t:column>
												<t:column id="colModeAr" style="width:8%;" rendered="#{pages$amendLeaseContract.isArabicLocale}">


													<f:facet name="header">

														<t:outputText  value="#{msg['paymentSchedule.paymentMode']}" />

													</f:facet>
													<t:outputText    
													              styleClass="A_LEFT"  title="" 
													              value="#{paymentScheduleDataItem.paymentModeAr}" />
												</t:column>
												<t:column  id="colStatusEn" rendered="#{pages$amendLeaseContract.isEnglishLocale}">


													<f:facet name="header" >

														<t:outputText value="#{msg['paymentSchedule.paymentStatus']}" />

													</f:facet>
													<t:outputText styleClass="A_LEFT"  title="" value="#{paymentScheduleDataItem.statusEn}" />
												</t:column>
												<t:column id="colStatusAr" rendered="#{pages$amendLeaseContract.isArabicLocale}">


													<f:facet name="header">

														<t:outputText  value="#{msg['paymentSchedule.paymentStatus']}" />

													</f:facet>
													<t:outputText    
													              styleClass="A_LEFT"  title="" 
													              value="#{paymentScheduleDataItem.statusAr}" />
												</t:column>
												<t:column id="colReceiptNumber" style="width:15%;" rendered="false">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.ReceiptNumber']}" />
													</f:facet>
													<t:outputText    
													              styleClass="A_LEFT"  title="" 
													              value="#{paymentScheduleDataItem.paymentReceiptNumber}" />
												</t:column>
												<t:column id="colAmount" >
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.amount']}" />
													</f:facet>
													<t:outputText title="" styleClass="A_RIGHT_NUM" value="#{paymentScheduleDataItem.amount}" >
													<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
													</t:outputText>
												</t:column>
												 <t:column id="col8" >
													<f:facet name="header">
														<t:outputText  value="#{msg['commons.action']}" />
													</f:facet>
													
													<h:commandLink action="#{pages$amendLeaseContract.btnEditPaymentSchedule_Click}">
													 <h:graphicImage id="editPayments" 
																		title="#{msg['commons.edit']}" 
																		url="../resources/images/edit-icon.gif"
																		rendered="#{paymentScheduleDataItem.isReceived == 'N'}"
																		/>
																																
													</h:commandLink>
													<a4j:commandLink reRender="tbl_PaymentSchedule,paymentSummary" action="#{pages$amendLeaseContract.btnDelPaymentSchedule_Click}">
																	       <h:graphicImage id="deletePaySc" 
																		title="#{msg['commons.delete']}" 
																		rendered="#{(paymentScheduleDataItem.isReceived == 'N' )
																		             }"
																		url="../resources/images/delete_icon.png"/>														
													</a4j:commandLink>
													<h:commandLink action="#{pages$amendLeaseContract.btnPrintPaymentSchedule_Click}">
													<h:graphicImage id="printPayments" 
																		title="#{msg['commons.print']}" 
																		url="../resources/images/app_icons/print.gif"
																		rendered="#{paymentScheduleDataItem.isReceived == 'Y'}"
																		/>
																																
													</h:commandLink>             
												</t:column>
											  </t:dataTable>		
											  								
										    </t:div>
										    <t:div id="pagingDivPaySch" styleClass="contentDivFooter" style="width:99.2%;">

												<t:dataScroller id="scrollerPaySch" for="tbl_PaymentSchedule" paginator="true"
													fastStep="1" paginatorMaxPages="15" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 
													pageIndexVar="pageNumber"
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-weight:bold;"
													paginatorRenderLinkForActive="false" 
														
														styleClass="SCH_SCROLLER">
                                                	<f:facet  name="first">
														<t:graphicImage  url="../#{path.scroller_first}"  id="lblFPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="fastrewind">
														<t:graphicImage  url="../#{path.scroller_fastRewind}"  id="lblFRPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="fastforward">
														<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="last">
														<t:graphicImage  url="../#{path.scroller_last}" id="lblLPaySch"></t:graphicImage>
													</f:facet>


												</t:dataScroller>
									
                                           </t:div>
                                           <f:verbatim>
                                           <br></br>
                                           </f:verbatim>
                                           <t:div id="divCollectPayment" styleClass="BUTTON_TD" style="padding:10px">
												<h:commandButton id="btnCollectPayment" binding="#{pages$amendLeaseContract.btnCollectPayment}" value="#{msg['settlement.actions.collectpayment']}" styleClass="BUTTON" action="#{pages$amendLeaseContract.openReceivePaymentsPopUp}" style="width: 150px" />
											</t:div>
                                           <f:verbatim>
                                           <br></br>
                                           </f:verbatim>
                                           <t:div  styleClass="TAB_DETAIL_SECTION" style="width:99%;" >
                                           <t:panelGrid id="paymentSummary" cellpadding="1px" width="100%" cellspacing="3px" styleClass="TAB_DETAIL_SECTION_INNER"  columns="4"
	                                           columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
											  
											  <t:outputLabel styleClass="A_LEFT"  value="#{msg['payments.totalCash']} :"/>
											  <t:outputLabel styleClass="A_LEFT"  value="#{pages$amendLeaseContract.totalCashAmount}">
											  <f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											  </t:outputLabel>
											  
											  <t:outputLabel styleClass="A_LEFT" value="#{msg['payments.totalChq']} :"/>
											  <t:outputLabel styleClass="A_LEFT" value="#{pages$amendLeaseContract.totalChqAmount}">
											  <f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											  </t:outputLabel>
											  
											  <t:outputLabel styleClass="A_LEFT" value="#{msg['payments.totalCashCheque']} :"/>
											  <t:outputLabel styleClass="A_LEFT" value="#{pages$amendLeaseContract.totalChqAmount  + pages$amendLeaseContract.totalCashAmount}">
											  <f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											  </t:outputLabel>
											  
											  <t:outputLabel styleClass="A_LEFT" value="#{msg['payments.totalDeposit']} :"/>
											  <t:outputLabel styleClass="A_LEFT" value="#{pages$amendLeaseContract.totalDepositAmount}">
											  <f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											  </t:outputLabel>
											  
											  <t:outputLabel styleClass="A_LEFT" value="#{msg['payments.totalRent']} :"/>
											  <t:outputLabel styleClass="A_LEFT" value="#{pages$amendLeaseContract.totalRentAmount}">
											  <f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											  </t:outputLabel>
											  
											 
 											  <t:outputLabel styleClass="A_LEFT" value="#{msg['payments.totalFees']} :"/>
											  <t:outputLabel styleClass="A_LEFT" value="#{pages$amendLeaseContract.totalFees}">
											  <f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											  </t:outputLabel>
											 											  
											  <t:outputLabel styleClass="A_LEFT" value="#{msg['payments.totalFines']} :"/>
											  <t:outputLabel styleClass="A_LEFT" value="#{pages$amendLeaseContract.totalFines}">
											  <f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											  </t:outputLabel>
											  
											  <t:outputLabel styleClass="A_LEFT" value="#{msg['payments.noOfInstallments']} :"/>
											  <t:outputLabel styleClass="A_LEFT" value="#{pages$amendLeaseContract.installments}"/>
											 
											 											  
											  </t:panelGrid>
											</t:div>
                                           
                                   		</rich:tab>
                                   		
										 
										  <rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}" action= "#{pages$amendLeaseContract.tabAttachmentsComments_Click}">
															<%@  include file="attachment/attachment.jsp"%>
											</rich:tab>
							               <rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}" action= "#{pages$amendLeaseContract.tabAttachmentsComments_Click}">
														<%@ include file="notes/notes.jsp"%>
											</rich:tab>
											<rich:tab label="#{msg['contract.tabHeading.RequestHistory']}"  title="#{msg['commons.tab.requestHistory']}"  action="#{pages$amendLeaseContract.tabAuditTrail_Click}">
					                                     <%@ include file="../pages/requestTasks.jsp"%>
										 </rich:tab>
								
                                    	</rich:tabPanel>
                                    	
		                                  
                                    	
                                    	<table cellpadding="0" cellspacing="0"  style="width:100%;">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
                                 
									
											
										
										<table cellpadding="1px" cellspacing="0px" width="100%" >
											<tr>
											    <td colspan="12" class="BUTTON_TD">
                                    
									               
                                           			<pims:security screen="Pims.LeaseContractManagement.LeaseContract.AddAmendLease" action="create">
                                           			<h:commandButton type="submit"  styleClass="BUTTON" 
                                           			                 action="#{pages$amendLeaseContract.btnSaveAmendLease_Click}"
                                           			                 binding="#{pages$amendLeaseContract.btnSaveAmendLease}"  
                                           			                 value="#{msg['commons.saveButton']}" />
                                           			 </pims:security>     
                                           			 <pims:security screen="Pims.LeaseContractManagement.LeaseContract.ApproveAmendLease" action="create">           
                                           			<h:commandButton type="submit"  styleClass="BUTTON" 
                                           			                 action="#{pages$amendLeaseContract.btnApproveAmend_Click}"
                                           			                 binding="#{pages$amendLeaseContract.btnApproveAmendLease}"  
                                           			                 value="#{msg['commons.approve']}" />
                                           			                 
                                           			     <h:commandButton type="submit"  styleClass="BUTTON" 
                                           			                 action="#{pages$amendLeaseContract.btnRejectAmend_Click}"
                                           			                 binding="#{pages$amendLeaseContract.btnRejectAmendLease}"  
                                           			                 value="#{msg['commons.reject']}" />
                                           			 </pims:security>
                                           			<h:commandButton type="submit"  styleClass="BUTTON" 
                                           			                 action="#{pages$amendLeaseContract.btnSubmitAmendLease_Click}"
                                           			                 binding="#{pages$amendLeaseContract.btnSubmitAmendLease}"  
                                           			                 value="#{msg['commons.sendButton']}" />
                                           			<pims:security screen="Pims.LeaseContractManagement.LeaseContract.CompleteAmendLease" action="create">
                                           			 <h:commandButton type="submit"  styleClass="BUTTON" 
                                           			                 action="#{pages$amendLeaseContract.btnCompleteAmend_Click}"
                                           			                 binding="#{pages$amendLeaseContract.btnCompleteAmendLease}"  
                                           			                 value="#{msg['commons.complete']}" />
                                           			</pims:security>
                                           	
										
										             <h:commandButton id="popUpCancel" styleClass="BUTTON" type= "button" onclick="javascript:window.close();" value="#{msg['commons.cancel']}"  
										                 rendered="#{pages$amendLeaseContract.isViewModePopUp}">	</h:commandButton>
										              
		                                               </td>
		                                 </tr>
		                                 
                                          </table>
                                          
                                   </div>
                            </div>
									</td>
									</tr>
									</table>
			</h:form>
			</td>
    </tr>
    <tr>
			<td colspan="2">
			    <table width="100%" cellpadding="0" cellspacing="0" border="0">
			       <tr><td class="footer"><h:outputLabel value="#{msg['commons.footer.message']}" /></td></tr>
			    </table>
			</td>
    </tr>
    </table>
			</div>
		</body>
	</html>
</f:view>

