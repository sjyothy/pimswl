<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



<f:view>
	<script>
	function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		
		function showContractorPopup()
		{
			var screen_width = screen.width;
			var screen_height = screen.height;
			var popup_width = screen_width-180;
			var popup_height = screen_height-265;
			var leftPos = (screen_width-popup_width)/2 -5, topPos = (screen_height-popup_height)/2 - 20;
			var popup = window.open('contractorSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
			popup.focus();
		}
		
		function viewContractorPopup()
                 	{
				    var screen_width = screen.width;
					var screen_height = screen.height;
					var popup_width = screen_width-180;
					var popup_height = screen_height-265;
					var leftPos = (screen_width-popup_width)/2 -5, topPos = (screen_height-popup_height)/2 - 20;
					var popup = window.open('contractorDetails.jsf?pageMode=popUp','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
				     popup.focus();
				      
	                }
		
		
	    function showSearchTenderPopUp()
		{
			var screen_width = screen.width;
			var screen_height = screen.height;
			var popup_width = screen_width-180;
			var popup_height = screen_height-240;
			var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
			
			var popup = window.open('tenderSearch.jsf?VIEW_MODE=popup','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
			popup.focus();
		}
		
		function populateContractor(contractorId, contractorNumber, contractorNameEn, contractorNameAr, licenseNumber, licenseSource, officeNumber){
			document.getElementById("receiveTenderProposalsForm:contractorId").value=contractorId;
			document.getElementById("receiveTenderProposalsForm:loadContractor").value='true';
		    document.forms[0].submit();
		}
		
		function populateTender(tenderId,tenderNumber,tenderDescription)
		{
			 document.getElementById("receiveTenderProposalsForm:tenderId").value=tenderId;
			 document.getElementById("receiveTenderProposalsForm:loadTender").value='true';
			 alert(document.getElementById("receiveTenderProposalsForm:loadTender").value);
		     document.forms[0].submit();
		}
		
		function openTenderPopUp()
		{
			var screen_width = screen.width;
			var screen_height = screen.height;
			var popup_width = screen_width-180;
			var popup_height = screen_height-240;
			var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
			
			var popup = window.open('TenderManagement.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
			popup.focus();
		}
		
</script>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

			<style>
.rich-calendar-input {
	width: 150px;
}
</style>

		</head>

		<!-- Header -->

		<body class="BODY_STYLE">
		    	<div class="containerDiv">


			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['receiveTenderProposals.heading']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height: 465px">

										<h:form id="receiveTenderProposalsForm"
											styleClass="ReceiveTenderProposalsForm"
											enctype="multipart/form-data" style="width:97%;">
											<h:inputHidden id="contractorId" value="#{pages$receiveTenderProposals.hiddenContractorId}"></h:inputHidden>
											<h:inputHidden id="tenderId" value="#{pages$receiveTenderProposals.hiddenTenderId}"></h:inputHidden>
											<h:inputHidden id="loadTender" value="#{pages$receiveTenderProposals.hiddenLoadTender}"></h:inputHidden>
											<h:inputHidden id="loadContractor" value="#{pages$receiveTenderProposals.hiddenLoadContractor}"></h:inputHidden>
											<div style="height: 50px">
												<table border="0" class="layoutTable"
													style="margin-left: 10px; margin-right: 10px;">
													<tr>
														<td>
															<h:outputText value="#{pages$receiveTenderProposals.errorMessages}" escape="false" styleClass="ERROR_FONT" />
															<h:outputText value="#{pages$receiveTenderProposals.successMessages}" escape="false" styleClass="INFO_FONT" />
														</td>
													</tr>
												</table>
											</div>

											<div class="MARGIN" style="width: 98%">
												<table cellpadding="0" cellspacing="0" width="98%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>

												<t:div styleClass="TAB_PANEL_INNER">

													<rich:tabPanel style="width:98%;height:235px;"
														headerSpacing="0"
														binding="#{pages$receiveTenderProposals.tabPanel}">
														<rich:tab id="tenderDetailsTab"
															label="#{msg['receiveTenderProposals.tab.proposalDetails']}">
															<t:div id="divTenderDetail" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid columns="4" cellpadding="1"
																	styleClass="TAB_DETAIL_SECTION_INNER" cellspacing="5">
                              									<h:panelGroup>	
			                                                       <h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel value="#{msg['tender.number']}:"></h:outputLabel>
																</h:panelGroup>	
																	<t:panelGroup>

																		<h:inputText id="tenderNumber" readonly="true"
																			style="width: 170px; height: 18px"
																			value="#{pages$receiveTenderProposals.tenderNumber}" />
																		<h:commandLink onclick="showSearchTenderPopUp();" rendered="#{!pages$receiveTenderProposals.readonlyMode && pages$receiveTenderProposals.showSelectTenderLink}">
																	   		<h:graphicImage id="imgTender"  style="MARGIN: 0px 0px -4px" url="../resources/images/magnifier.gif" title="#{msg['commons.search']}"></h:graphicImage>
																	   </h:commandLink>
																	   <h:commandLink action="#{pages$ReceiveTenderProposals.openTenderPopUp}"rendered="#{!pages$ReceiveTenderProposals.readonlyMode}">
															   		<h:graphicImage id="imgViewTender"  style="MARGIN: 0px 0px -4px" url="../resources/images/app_icons/Lease-contract.png" title="#{msg['commons.view']}"></h:graphicImage>
																	   </h:commandLink>
																	</t:panelGroup>
																	<h:outputLabel value="#{msg['tender.description']}:"></h:outputLabel>
																	<h:inputText id="tenderDescription" readonly="true"
																		style="width: 170px; height: 18px"
																		value="#{pages$receiveTenderProposals.tenderDescription}" />

 	`															<h:panelGroup>	
			                                                       <h:outputLabel styleClass="mandatory" value="*"/>
																	<h:outputLabel
																		value="#{msg['tenderManagement.contractorNumber']}: " />
																</h:panelGroup>	
																	<t:panelGroup>
																		<h:inputText id="contractorNo" readonly="true"
																			style="width: 170px; height: 18px"
																			value="#{pages$receiveTenderProposals.contractorNumber}" />

																		<h:commandLink actionListener="#{pages$receiveTenderProposals.openContractorPopup}" rendered="#{!pages$receiveTenderProposals.readonlyMode}">
																	   		<h:graphicImage id="imgContractor"  style="MARGIN: 0px 0px -4px" url="../resources/images/magnifier.gif" title="#{msg['commons.search']}"></h:graphicImage>
																	   </h:commandLink>
																	   <h:commandLink actionListener="#{pages$receiveTenderProposals.viewContractorPopup}" rendered="#{!pages$receiveTenderProposals.readonlyMode}">
																	   		<h:graphicImage id="imgViewContractor"  style="MARGIN: 0px 0px -4px" url="../resources/images/app_icons/Lease-contract.png" title="#{msg['commons.view']}"></h:graphicImage>
																	   </h:commandLink>
																	</t:panelGroup>
																	<h:outputLabel
																		value="#{msg['tenderManagement.contractorName']}:"></h:outputLabel>
																	<h:inputText id="contractorName" readonly="true"
																		style="width: 170px; height: 18px"
																		value="#{pages$receiveTenderProposals.contractorName}" />
																		
																<h:panelGroup>	
			                                                       <h:outputLabel styleClass="mandatory" value="*"/>		
																	<h:outputLabel
																		value="#{msg['tenderManagement.submittedDate']} : "></h:outputLabel>
																</h:panelGroup>		
																	<rich:calendar id="submittedDate" rendered="true"
																		value="#{pages$receiveTenderProposals.submittedDate}"
																		datePattern="#{pages$receiveTenderProposals.dateFormat}"
																		locale="#{pages$receiveTenderProposals.locale}"
																		disabled="true"
																		popup="true" showApplyButton="false"
																		enableManualInput="false" />
																<h:panelGroup>	
			                                                       <h:outputLabel styleClass="mandatory" value="*"/>		
																	<h:outputLabel
																		value="#{msg['tenderManagement.submittedBy']} : "></h:outputLabel>
																</h:panelGroup>		
																	<h:inputText id="submittedBy"
																		style="width: 170px; height: 18px"
																		value="#{pages$receiveTenderProposals.submittedBy}" 
																		readonly="#{pages$receiveTenderProposals.readonlyMode}"/>
																		
																<h:panelGroup>	
			                                                       <h:outputLabel styleClass="mandatory" value="*"/>		
																	<h:outputLabel
																		value="#{msg['tenderManagement.receivedBy']} : "></h:outputLabel>
																</h:panelGroup>		
																	<h:inputText id="receivedBy"
																		style="width: 170px; height: 18px"
																		value="#{pages$receiveTenderProposals.receivedBy}" 
																		readonly="#{pages$receiveTenderProposals.readonlyMode}"/>
															<%-- End new fiels--%>					
																	<h:outputLabel
																		value="#{msg['serviceContract.guarantee.expiryDate']} : "></h:outputLabel>
																	<rich:calendar id="guaranteExpDate" rendered="true"
																		value="#{pages$receiveTenderProposals.guaranteeExpiryDate}"
																		datePattern="#{pages$receiveTenderProposals.dateFormat}"
																		locale="#{pages$receiveTenderProposals.locale}"
																		popup="true" showApplyButton="false"
																		enableManualInput="false" />
																		
																	<h:outputLabel
																		value="#{msg['serviceContract.guarantee.percentage']} : "></h:outputLabel>
																	<h:inputText id="guaranteeValue"
																		style="width: 170px; height: 18px"
																		value="#{pages$receiveTenderProposals.guaranteePercentage}" />
																		
																<h:panelGroup>	
			                                                       <h:outputLabel styleClass="mandatory" value="*"/>		
																	<h:outputLabel
																		value="#{msg['bouncedChequesList.bankNameCol']} : "></h:outputLabel>
																</h:panelGroup>		
																	<h:selectOneMenu  id="bankList"
																		style="width: 180px; height: 24px"
																		value="#{pages$receiveTenderProposals.bankId}"
																		>
																		<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1"/>
																		<f:selectItems value="#{pages$ApplicationBean.bankList}"/>
																	</h:selectOneMenu >
																<%-- End new fiels--%>						
																<h:panelGroup>	
			                                                       <h:outputLabel styleClass="mandatory" value="*"/>		
																	<h:outputLabel
																		value="#{msg['tenderManagement.proposalDescription']} : "></h:outputLabel>
																</h:panelGroup>		
																	<h:inputTextarea id="proposalDescription"
																		readonly="false" style="width: 170px; "
																		binding="#{pages$receiveTenderProposals.proposalDescription}" />
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<rich:tab id="attachmentTab"
															label="#{msg['commons.attachmentTabHeading']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>

														<rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
													</rich:tabPanel>

												</t:div>
												<table cellpadding="0" cellspacing="0" width="98%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<t:div styleClass="BUTTON_TD" style="padding-top:10px; margin-left:15px; margin-right: 15px;">
													<t:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}"
														onclick="if (!confirm('#{msg['commons.messages.cancelConfirm']}')) return false" 
											        	action="#{pages$receiveTenderProposals.onCancel}" tabindex="12" 
														binding="#{pages$receiveTenderProposals.cancelButton}"/>
											 		<t:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" 
											 			onclick="if (!confirm('#{msg['receiveTenderProposals.messages.confirmSave']}')) return false"
											 			action="#{pages$receiveTenderProposals.onSave}" tabindex="13"
											 			binding="#{pages$receiveTenderProposals.saveButton}"/>
												</t:div>
											</div>
									</div>
									</h:form>

									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
          </div>
		</body>
	</html>
</f:view>
