

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="q"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
<head>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
		<meta http-equiv="pragma" content="no-cache"/>
		<meta http-equiv="Cache-Control" content="no-cache,must-revalidate,no-store"/>
		<meta http-equiv="expires" content="0"/>

		<link rel="stylesheet" type="text/css" href="../resources/css/simple_en.css"/>
		<link rel="stylesheet" type="text/css" href="../resources/css/amaf_en.css"/>
		<link rel="stylesheet" type="text/css" href="../resources/css/table.css"/>
		<title>PIMS</title>

		<script language="javascript" type="text/javascript">
  
       



function move(fbox, tbox) {
a = '_id0:'+fbox;
b = '_id0:'+tbox;
	var objSourceElement = document.getElementById(a);
	var objTargetElement = document.getElementById(b);   
    var aryTempSourceOptions = new Array();   
         var x = 0;                //looping through source element to find selected options 

         for (var i = 0; i < objSourceElement.length; i++) {     
         if (objSourceElement.options[i].selected) {      
         //need to move this option to target element  
               
                  var intTargetLen = objTargetElement.length++;
         objTargetElement.options[intTargetLen].text = objSourceElement.options[i].text;      
         objTargetElement.options[intTargetLen].value = objSourceElement.options[i].value;       
         }       
         else {       
         //storing options that stay to recreate select element  
         var objTempValues = new Object(); 
   
objTempValues.text = objSourceElement.options[i].text;
objTempValues.value = objSourceElement.options[i].value;
aryTempSourceOptions[x] = objTempValues;  
x++;       
}  
}      
//resetting length of source     
objSourceElement.length = aryTempSourceOptions.length;  
//looping through temp array to recreate source select element 
for (var i = 0; i < aryTempSourceOptions.length; i++) {     
objSourceElement.options[i].text = aryTempSourceOptions[i].text;    
objSourceElement.options[i].value = aryTempSourceOptions[i].value;
objSourceElement.options[i].selected = false;    
}   
}    //-->    </script>



	</head>
	<body>

<f:view>

<%System.out.println("Test1"); %>

<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
				var="msg" />
  
  <%System.out.println("Test2"); %>  
   
    
     
 <%
    UIViewRoot view = (UIViewRoot)session.getAttribute("view");
    System.out.println("Test211111");
    if (view.getLocale().toString().equals("en")){ %>
    <body dir="ltr">
    <%} else{%>
    <body dir="rtl">
    <%}
     System.out.println("Test22222");
    %>
    
    <%System.out.println("Test21"); %>  
    <!-- Header --> 
<table width="100%" cellpadding="0" cellspacing="0"  border="0">
    <tr>
    <td colspan="2">
        <jsp:include page="header.jsp"/>
    </td>
    </tr>

<tr width="100%">
    <td class="divLeftMenuWithBody" width="17%">
        <jsp:include page="leftmenu.jsp"/>
    </td>
    <td width="83%" valign="top" class="divBackgroundBody">
        <table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0"  border="0">
        <tr>
         <%if (view.getLocale().toString().equals("en"))
             {  %>
            <td background ="../resources/images/Grey panel/Grey-Panel-left-1.jpg" height="38" style="background-repeat:no-repeat;" width="100%">
             <font style="font-family:Trebuchet MS;font-weight:regular;font-size:19px;margin-left:15px;top:30px;">PROPERTY INQUIRY</font>
                <!--<IMG src="../resources/images/Grey panel/Grey-Panel-left-1.jpg"/>-->
             <%}else {%>
             <td width="100%">
                 <IMG src="../resources/images/ar/Detail/Grey Panel/Grey-Panel-right-1.jpg"></img>
             <%}%>    
             <%System.out.println("Test22"); %>  
            </td>    
            <td width="100%">
        &nbsp;
            </td>
	</tr>
        </table>
    
        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
          <tr valign="top">
             <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
             </td> 
	<%System.out.println("Test23"); %>  
  <td width="500px" height="100%" valign="top" nowrap="nowrap">
		<h:form>	                                 				    
    <%System.out.println("Test4"); %>
        						              				
													
						<%System.out.println("TestSecondLast"); %>							
							
							<div class="contentDiv">


										<q:dataTable id="test2"
													value="#{PropertyInquiry.propertyInquiryDataList}"
													binding="#{PropertyInquiry.dataTable}"
													rows="15"
													preserveDataModel="true" preserveSort="true" var="dataItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

													<q:column id="col1" width="50">

														<f:facet name="header">

															<q:outputText value="Unit Number" />

														</f:facet>
														<q:outputText value="#{dataItem.unitNumber}" />
													</q:column>
<%System.out.println("TestSecondLast1"); %>	

												<q:column id="col2" width="50" >
													<f:facet name="header">


														<q:outputText value="Unit Description" />

													</f:facet>
													<q:outputText value="#{dataItem.unitDesc}" />
												</q:column>
<%System.out.println("TestSecondLast2"); %>	

												<q:column id="col3"  width = "50">


													<f:facet name="header">

														<q:outputText value= "Unit Area" />

													</f:facet>
													<q:outputText value="#{dataItem.unitArea}" />
												</q:column>


<%System.out.println("TestSecondLast3"); %>	


												<q:column id="col4" width = "50">

													<f:facet name="header">
														
														<q:outputText value="No Of Bed" />
													</f:facet>
													<q:outputText value ="#{dataItem.noOfBed}" />
													</q:column>
<%System.out.println("TestSecondLast4"); %>									
										<q:column id="col5" width = "50">

													<f:facet name="header">
														
														<q:outputText value="No Of Living" />
													</f:facet>
													<q:outputText value ="#{dataItem.noOfLiving}" />
													</q:column>
<%System.out.println("TestSecondLast5"); %>					     
									     <q:column id="col6" width = "50">

													<f:facet name="header">
														
														<q:outputText value="No Of Bath" />
													</f:facet>
													<q:outputText value ="#{dataItem.noOfBath}" />
													</q:column>
<%System.out.println("TestSecondLast6"); %>						
										<q:column id="col7" width = "50">

													<f:facet name="header">
														
														<q:outputText value="Rent Value" />
													</f:facet>
													<q:outputText value ="#{dataItem.rentValue}" />
													</q:column>
<%System.out.println("TestSecondLast7"); %>						
											<q:column id="col8" width = "50">

													<f:facet name="header">
														
														<q:outputText value="Required Price" />
													</f:facet>
													<q:outputText value ="#{dataItem.requiredPrice}" />
									    	</q:column>
<%System.out.println("TestSecondLast8"); %>
                                     		<q:column id="col9" width="100" >

	                                            <f:facet name="header">
	                                            <q:outputText value="Select" />
	                                            </f:facet>
	                                            <h:selectBooleanCheckbox id="select"/>
          <%System.out.println("TestSecondLast9"); %>                                 
                                           </q:column>							
													
												</q:dataTable>										
											
		<%System.out.println("TestEnd1"); %>										
											</div>
									<h:panelGroup >
									<h:commandButton styleClass="BUTTON" type="button" value="#{msg['inquiry.sendsms']}" style="width: 74px">  </h:commandButton>
									<h:commandButton styleClass="BUTTON" type="button" value="#{msg['commons.cancel']}" style="width: 74px">	</h:commandButton>
									</h:panelGroup >	
<%System.out.println("TestEnd"); %>	

			</h:form>
			</body>
		</f:view>
	
</html>
