<%-- 
  - Author: Anil Verani
  - Date: 15/01/2010
  - Copyright Notice:
  - @(#)
  - Description: Used for Registering Bidder with Auction 
  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	function openPopupBidderSearch()
	{
		var screen_width = 1024;
       	var screen_height = 470;
       	var popup_width = screen_width-200;
       	var popup_height = screen_height;
       	var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
	   	window.open('SearchPerson.jsf?persontype=BIDDER&viewMode=popup','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+',scrollbars=yes,resizable=yes,status=yes');
	}
	
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		document.getElementById("frm:bidderId").value = personId;
		document.getElementById('frm:lnkReceiveBidderSelection').onclick();
		
	}
	
	function openPopupAuctionSearch()
	{
	   var screen_width = 1024;
       var screen_height = 450;
       var popup_width = screen_width-128;
       var popup_height = screen_height;
       var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
       window.open('auctionSearch.jsf?viewMode=popup','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=40,scrollbars=yes,status=yes');
    }
    
    function populateAuction(auctionId, auctionNumber, auctionDate, auctionTitle, venueName)
	{
		document.getElementById("frm:auctionId").value = auctionId;
		document.getElementById('frm:lnkReceiveAuctionSelection').onclick();
	}
	
	function onChangeDepositAmount()
    {
      document.getElementById('frm:disablingDiv').style.display='none';
    }
	function unitSelectionChanged(changedChkBox)
	{
	    document.getElementById('frm:disablingDiv').style.display='block';
		changedChkBox.nextSibling.onclick();
	}
	
		
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr>
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table class="greyPanelTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['auction.registerAuction']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<td width="100%" height="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION">

											<h:form id="frm" style="width:97%"
												enctype="multipart/form-data">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText id="errorMessages" escape="false"
																styleClass="ERROR_FONT"
																value="#{pages$auctionRegisterManage.errorMessages}" />
															<h:outputText id="successMessages" escape="false"
																styleClass="INFO_FONT"
																value="#{pages$auctionRegisterManage.successMessages}" />

															<a4j:commandLink id="lnkReceiveBidderSelection"
																action="#{pages$auctionRegisterManage.onReceiveBidderSelection}"
																reRender="pnlGrdBidder,dt1,pnlGrdSummary" />
															<a4j:commandLink id="lnkReceiveAuctionSelection"
																action="#{pages$auctionRegisterManage.onReceiveAuctionSelection}"
																reRender="pnlGrdAuction,dt1,pnlGrdSummary,errorMessages,pnlGrpBtn" />

															<h:inputHidden id="bidderId"
																value="#{pages$auctionRegisterManage.bidderView.personId}" />
														</td>
													</tr>
												</table>

												<div class="MARGIN" style="width: 98%">



													<rich:tabPanel>

														<!-- Bidder Tab - Start -->
														<rich:tab
															label="#{msg['auction.registration.tabs.bidderInfo']}">
															<t:panelGrid id="pnlGrdBidder" columns="4" border="0"
																width="100%" cellpadding="1" cellspacing="1">



																<h:outputLabel styleClass="LABEL"
																	value="#{msg['applicationDetails.number']}:" />
																<h:inputText styleClass="READONLY" style="width:166px;"
																	readonly="true"
																	value="#{pages$auctionRegisterManage.requestView.requestNumber}" />


																<h:outputLabel styleClass="LABEL"
																	value="#{msg['applicationDetails.status']}:" />
																<h:inputText styleClass="READONLY" style="width:166px;"
																	readonly="true"
																	value="#{pages$auctionRegisterManage.isEnglishLocale?
																										 pages$auctionRegisterManage.requestView.statusEn:
																					 					 pages$auctionRegisterManage.requestView.statusAr					 
																					}" />

																<t:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['bidder.name']}:" />
																</t:panelGroup>
																<t:panelGroup>
																	<h:inputText styleClass="READONLY" style="width:166px;"
																		readonly="true"
																		value="#{pages$auctionRegisterManage.bidderView.personFullName}" />
																	<t:commandLink
																		rendered="#{pages$auctionRegisterManage.isNewMode}"
																		action="#{pages$auctionRegisterManage.onOpenPopupBidderSearch}"
																		style="padding-top:12px;padding-left:2px; padding-right:2px;">
																		<h:graphicImage title="#{msg['bidder.searchbidder']}"
																			url="../resources/images/app_icons/searchBidder.png" />
																	</t:commandLink>
																</t:panelGroup>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['bidder.SSN']}: "></h:outputLabel>
																<h:inputText styleClass="READONLY" readonly="true"
																	value="#{pages$auctionRegisterManage.bidderView.socialSecNumber}" />

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['bidder.cellnumber']}: "></h:outputLabel>
																<h:inputText styleClass="READONLY" readonly="true"
																	value="#{pages$auctionRegisterManage.bidderView.cellNumber}" />

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['bidder.nationality']}: "></h:outputLabel>
																<h:inputText styleClass="READONLY" readonly="true"
																	value="#{pages$auctionRegisterManage.isEnglishLocale ? pages$auctionRegisterManage.bidderView.nationalityEn : pages$auctionRegisterManage.bidderView.nationalityAr}" />

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['bidder.email']}: "></h:outputLabel>
																<h:inputText styleClass="READONLY" readonly="true"
																	value="#{pages$auctionRegisterManage.bidderEmail}" />
																<t:div style="height: 20px;" />
															</t:panelGrid>
														</rich:tab>
														<!-- Bidder Tab - End -->

														<!-- Auction Tab - Start -->
														<rich:tab
															label="#{msg['auction.registration.tabs.Auction']}">
															<t:panelGrid id="pnlGrdAuction" columns="2" border="0"
																width="100%" cellpadding="1" cellspacing="1">
																<t:panelGroup>
																	<h:inputHidden id="auctionId"
																		value="#{pages$auctionRegisterManage.auctionView.auctionId}" />
																	<t:panelGrid columns="2">
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['auction.number']}:" />
																		</t:panelGroup>
																		<t:panelGroup>
																			<h:inputText styleClass="READONLY"
																				style="width:166px;" readonly="true"
																				value="#{pages$auctionRegisterManage.auctionView.auctionNumber}" />
																			<t:commandLink
																				rendered="#{pages$auctionRegisterManage.isNewMode}"
																				action="#{pages$auctionRegisterManage.onOpenPopupAuctionSearch}"
																				style="padding-top:2px;padding-left:2px; padding-right:2px;">
																				<h:graphicImage
																					title="#{msg['auction.searchAuction']}"
																					url="../resources/images/app_icons/Search-Auction.png" />
																			</t:commandLink>
																		</t:panelGroup>

																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['auction.requestStartDate']}: " />
																		<rich:calendar inputStyle="width:170px; height:14px"
																			disabled="true" inputClass="READONLY"
																			locale="#{pages$auctionRegisterManage.locale}"
																			datePattern="#{pages$auctionRegisterManage.dateFormat}"
																			value="#{pages$auctionRegisterManage.auctionView.requestStartDate}" />
																	</t:panelGrid>
																</t:panelGroup>
																<t:panelGroup>
																	<t:panelGrid columns="2">
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['auction.title']}: "></h:outputLabel>
																		<h:inputText styleClass="READONLY" readonly="true"
																			value="#{pages$auctionRegisterManage.auctionView.auctionTitle}" />

																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['auction.requestEndDate']}: "></h:outputLabel>
																		<rich:calendar inputStyle="width:170px; height:14px"
																			disabled="true" inputClass="READONLY"
																			locale="#{pages$auctionRegisterManage.locale}"
																			datePattern="#{pages$auctionRegisterManage.dateFormat}"
																			value="#{pages$auctionRegisterManage.auctionView.requestEndDate}" />
																	</t:panelGrid>
																</t:panelGroup>
															</t:panelGrid>
															<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
															<t:div style="width:100%;">
																<t:div styleClass="contentDiv" style="width:98.2%; ">
																	<t:dataTable id="dt1"
																		value="#{pages$auctionRegisterManage.auctionRequestRegViewList}"
																		rows="#{pages$auctionRegisterManage.paginatorRows}"
																		binding="#{pages$auctionRegisterManage.unitDataTable}"
																		preserveDataModel="false" preserveSort="false"
																		var="dataItem" rowClasses="row1,row2" rules="all"
																		renderedIfEmpty="true" width="100%">
																		<t:column id="colPropertyRefNo" sortable="true"
																			width="18%">
																			<f:facet name="header">
																				<t:outputText value="#{msg['property.refNumber']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.propertyRefNumber}" />
																		</t:column>
																		<t:column id="colPropertyCommName" sortable="true"
																			width="18%">
																			<f:facet name="header">
																				<t:outputText value="#{msg['property.name']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.commercialName}" />
																		</t:column>
																		<t:column id="colUnitNo" sortable="true" width="18%">
																			<f:facet name="header">
																				<t:outputText value="#{msg['unit.number']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.unitNumber}" />
																		</t:column>
																		<t:column id="colUnitDesc" sortable="true" width="18%">
																			<f:facet name="header">
																				<t:outputText value="#{msg['unit.unitDescription']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.unitDesc}" />
																		</t:column>
																		<t:column id="colUnitTypeEn" width="18%"
																			sortable="true">
																			<f:facet name="header">
																				<t:outputText value="#{msg['unit.type']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{pages$auctionRegisterManage.isEnglishLocale ? dataItem.unitTypeEn : dataItem.unitTypeAr}" />
																		</t:column>
																		<t:column id="colDepositAmount" width="18%"
																			sortable="true">
																			<f:facet name="header">
																				<t:outputText
																					value="#{msg['auctionUnit.depositColumn']}" />
																			</f:facet>
																			<t:outputText styleClass="A_RIGHT_NUM"
																				value="#{dataItem.depositAmount}">
																				<f:convertNumber maxFractionDigits="2"
																					minFractionDigits="2"
																					pattern="#{pages$auctionRegisterManage.numberFormat}" />
																			</t:outputText>
																		</t:column>
																		<t:column id="colSelect" width="10%">
																			<f:facet name="header">
																				<t:outputText value="#{msg['commons.select']}" />
																			</f:facet>
																			<h:selectBooleanCheckbox id="checkBoxes"
																				style="text-align:center"
																				value="#{dataItem.isSelected}"
																				disabled="#{dataItem.registered || pages$auctionRegisterManage.isCollectPaymentMode || pages$auctionRegisterManage.isCollectedMode}"
																				onclick="unitSelectionChanged(this);" />

																			<a4j:commandLink id="lnkUnitSelectionChanged"
																				onbeforedomupdate="javascript:onChangeDepositAmount();"
																				action="#{pages$auctionRegisterManage.onUnitSelectionChanged}"
																				reRender="dt1,pnlGrdSummary" />
																		</t:column>
																	</t:dataTable>
																</t:div>
																<t:div styleClass="contentDivFooter"
																	style="width:99.2%;">
																	<t:dataScroller id="regScroller" for="dt1"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$auctionRegisterManage.paginatorMaxPages}"
																		immediate="false" styleClass="JUG_SCROLLER_AUC_REG"
																		paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																		paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
																		</f:facet>

																	</t:dataScroller>
																</t:div>
															</t:div>
															<t:div>
																<t:panelGrid id="pnlGrdSummary" columns="1">
																	<t:panelGroup>
																		<h:outputLabel styleClass="LABEL"
																			style="padding-right: 5px;"
																			value="#{msg['auction.totalDepositAmount']} : "></h:outputLabel>
																		<h:inputText
																			value="#{pages$auctionRegisterManage.totalDepositAmount}"
																			id="inputTextTotalDepositAmount"
																			styleClass=" A_RIGHT_NUM" readonly="true"
																			style="padding-left: 2px; padding-right: 2px; display:inline;" />
																	</t:panelGroup>
																	<t:panelGroup>
																		<h:outputLabel styleClass="LABEL REG_AUC_SEL_LAB"
																			value="#{msg['auction.registration.totalSelectedUnits']} : "></h:outputLabel>
																		<h:inputText
																			value="#{pages$auctionRegisterManage.totalSelectedUnits}"
																			id="inputTextTotalSelected" styleClass="A_RIGHT_NUM"
																			readonly="true"
																			style="padding-left: 2px; padding-right: 2px; display:inline;" />
																	</t:panelGroup>
																</t:panelGrid>
															</t:div>
														</rich:tab>


														<!-- Auction Tab - End -->

														<!-- Payment Tab - Start -->
														<rich:tab id="paymentsTab"
															label="#{msg['auction.registration.tabs.payments']}">
															<t:div styleClass="contentDiv" style="width:97.1%">
																<t:dataTable id="tbl_PaymentSchedule"
																	rows="#{pages$auctionRegisterManage.paginatorRows}"
																	width="100%"
																	value="#{pages$auctionRegisterManage.paymentScheduleViewList}"
																	binding="#{pages$auctionRegisterManage.paymentScheduleDataTable}"
																	preserveDataModel="false" preserveSort="false"
																	var="paymentScheduleDataItem" rowClasses="row1,row2"
																	rules="all" renderedIfEmpty="true">
																	<t:column id="col_PaymentNumber" style="width:15%;"
																		sortable="true"
																		rendered="#{pages$auctionRegisterManage.isCollectPaymentMode || pages$auctionRegisterManage.isCollectedMode}">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentNumber']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentNumber}" />
																	</t:column>

																	<t:column id="col_ReceiptNumber" style="width:15%;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.ReceiptNumber']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentReceiptNumber}" />
																	</t:column>

																	<t:column id="colType" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentType']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{pages$auctionRegisterManage.isEnglishLocale ? paymentScheduleDataItem.typeEn : paymentScheduleDataItem.typeAr}" />
																	</t:column>
																	<t:column id="colDueOn" rendered="false"
																		sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentDueOn']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentDueOnString}" />
																	</t:column>
																	<t:column id="colPaymentDate" rendered="false"
																		sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentDate']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentDate}" />
																	</t:column>
																	<t:column id="colMode" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentMode']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{pages$auctionRegisterManage.isEnglishLocale ? paymentScheduleDataItem.paymentModeEn : paymentScheduleDataItem.paymentModeAr}" />
																	</t:column>
																	<t:column id="colStatus" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentStatus']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{pages$auctionRegisterManage.isEnglishLocale ? paymentScheduleDataItem.statusEn : paymentScheduleDataItem.statusAr}" />
																	</t:column>
																	<t:column id="colReceiptNumber" style="width:15%;"
																		rendered="false" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.ReceiptNumber']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentReceiptNumber}" />
																	</t:column>
																	<t:column id="colAmount" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.amount']}" />
																		</f:facet>
																		<t:outputText styleClass="A_RIGHT_NUM"
																			value="#{paymentScheduleDataItem.amount}" />
																	</t:column>
																	<t:column id="actionCol" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<h:commandLink
																			rendered="#{paymentScheduleDataItem.isReceived == 'N' && (pages$auctionRegisterManage.isCollectPaymentMode || pages$auctionRegisterManage.isNewMode)}"
																			action="#{pages$auctionRegisterManage.onOpenPopupEditPaymentSchedule}">
																			<h:graphicImage id="editPayments"
																				title="#{msg['commons.edit']}"
																				url="../resources/images/edit-icon.gif" />
																		</h:commandLink>
																		<h:commandLink
																			action="#{pages$auctionRegisterManage.btnPrintPaymentSchedule_Click}">
																			<h:graphicImage id="printPayments"
																				title="#{msg['commons.print']}"
																				url="../resources/images/app_icons/print.gif"
																				rendered="#{paymentScheduleDataItem.isReceived == 'Y'}" />

																			<h:commandLink
																				action="#{pages$auctionRegisterManage.btnViewPaymentDetails_Click}">
																				<h:graphicImage id="viewPayments"
																					title="#{msg['commons.group.permissions.view']}"
																					url="../resources/images/detail-icon.gif" />
																			</h:commandLink>



																		</h:commandLink>
																		<h:commandLink rendered="false"
																			action="#{pages$auctionRegisterManage.openReceivePaymentsPopUp}">&nbsp;
																    <h:graphicImage id="receivePayments"
																				title="#{msg['contract.paymentSchedule.receivePayment']}"
																				url="../resources/images/app_icons/Pay-Fine.png"
																				rendered="#{paymentScheduleDataItem.isReceived == 'N'  && paymentScheduleDataItem.statusId==pages$auctionRegisterManage.paymentSchedulePendingStausId}" />
																		</h:commandLink>
																		<a4j:commandLink rendered="false"
																			reRender="tbl_PaymentSchedule"
																			action="#{pages$auctionRegisterManage.btnDelPaymentSchedule_Click}">&nbsp;
															       <h:graphicImage id="deletePaySc"
																				title="#{msg['commons.delete']}"
																				rendered="#{(paymentScheduleDataItem.isReceived == 'N' && paymentScheduleDataItem.typeId==pages$auctionRegisterManage.rentIdFromDomainData)}"
																				url="../resources/images/delete_icon.png" />
																		</a4j:commandLink>
																	</t:column>
																</t:dataTable>
															</t:div>
															<t:div id="pagingDivPaySch" styleClass="contentDivFooter"
																style="width:98.2%;">

																<t:dataScroller id="scrollerPaySch"
																	for="tbl_PaymentSchedule" paginator="true" fastStep="1"
																	paginatorMaxPages="#{pages$auctionRegisterManage.paginatorMaxPages}"
																	immediate="false" paginatorTableClass="paginator"
																	renderFacetsIfSinglePage="true"
																	pageIndexVar="paySchedPageNumber"
																	paginatorTableStyle="grid_paginator"
																	layout="singleTable"
																	paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																	paginatorActiveColumnStyle="font-weight:bold;"
																	paginatorRenderLinkForActive="false"
																	styleClass="SCH_SCROLLER">
																	<f:facet name="first">
																		<t:graphicImage url="../#{path.scroller_first}"
																			id="lblFPaySch"></t:graphicImage>
																	</f:facet>

																	<f:facet name="fastrewind">
																		<t:graphicImage url="../#{path.scroller_fastRewind}"
																			id="lblFRPaySch"></t:graphicImage>
																	</f:facet>

																	<f:facet name="fastforward">
																		<t:graphicImage url="../#{path.scroller_fastForward}"
																			id="lblFFPaySch"></t:graphicImage>
																	</f:facet>

																	<f:facet name="last">
																		<t:graphicImage url="../#{path.scroller_last}"
																			id="lblLPaySch"></t:graphicImage>
																	</f:facet>
																</t:dataScroller>
															</t:div>
															<t:div styleClass="BUTTON_TD">
																<h:commandButton id="receivePaymentButton"
																	styleClass="BUTTON"
																	value="#{msg['auction.registration.collectPaymentButton']}"
																	rendered="false"
																	action="#{pages$auctionRegisterManage.openReceivePaymentsPopUp}"
																	style="width: 150px;">
																</h:commandButton>
															</t:div>
														</rich:tab>
														<!-- Payment Tab - End -->

														<!-- Attachments Tab - Start -->
														<rich:tab label="#{msg['contract.attachmentTabHeader']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<!-- Attachments Tab - End -->

														<!-- Comments Tab - Start -->
														<rich:tab label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														<!-- Comments Tab - End -->

														<rich:tab
															label="#{msg['contract.tabHeading.RequestHistory']}"
															title="#{msg['commons.tab.requestHistory']}"
															action="#{pages$auctionRegisterManage.onActivityLogTab}">
															<%@ include file="../pages/requestTasks.jsp"%>
														</rich:tab>

													</rich:tabPanel>


													<table cellpadding="10" cellspacing="0" width="97.5%">
														<tr>
															<td colspan="6" class="BUTTON_TD">
																<h:panelGroup id="pnlGrpBtn">

																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['auction.registration.registerButton']}"
																		action="#{pages$auctionRegisterManage.onRegister}"
																		rendered="#{pages$auctionRegisterManage.isNewMode}">
																	</h:commandButton>

																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['auction.registration.collectPaymentButton']}"
																		action="#{pages$auctionRegisterManage.onCollectPayment}"
																		binding="#{pages$auctionRegisterManage.btnCollectPayment}"
																		style="width: 100px;">
																	</h:commandButton>

																	<h:commandButton rendered="false" styleClass="BUTTON"
																		value="#{msg['commons.cancel']}"
																		action="#{pages$auctionRegisterManage.onCancel}">
																	</h:commandButton>

																</h:panelGroup>
															</td>
														</tr>
													</table>
												</div>

											</h:form>
										</div>


										</div>
									</td>
								</tr>
							</table>

						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>