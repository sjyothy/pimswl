<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="q"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">




		
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}"
style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 	<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
        
		<script language="javascript" type="text/javascript">
		
		function clearWindow()
	{
	       
	        document.getElementById("formTenant:txtTenantNumber").value="";
	        document.getElementById("formTenant:txtfirstName").value="";
		    document.getElementById("formTenant:txtmiddleName").value="";
		    document.getElementById("formTenant:txtlastName").value="";
		    document.getElementById("formTenant:dateOfBirth").value="";
		    document.getElementById("formTenant:txtcellNo").value="";
		    document.getElementById("formTenant:txtpassportNumber").value="";
		    document.getElementById("formTenant:datePassportIssueDate").value="";
		    document.getElementById("formTenant:datePassportExpiryDate").value="";
		    document.getElementById("formTenant:txtresidenseVisaNumber").value="";
		    document.getElementById("formTenant:dateVisaExpiryDate").value="";
		    document.getElementById("formTenant:txtpersonalSecCardNo").value="";
		    document.getElementById("formTenant:txtdrivingLicenseNumber").value="";
		    document.getElementById("formTenant:txtsocialSecNumber").value="";
		    document.getElementById("formTenant:txtcompanyname").value="";
		    document.getElementById("formTenant:txtworkingcompany").value="";
		    document.getElementById("formTenant:txtgovtdepartment").value="";
		    document.getElementById("formTenant:txtaddress1").value="";
		    document.getElementById("formTenant:txtaddress2").value="";
            document.getElementById("formTenant:txtemail").value="";
            document.getElementById("formTenant:txtfax").value="";
            document.getElementById("formTenant:txtofficePhone").value="";
            document.getElementById("formTenant:txthomePhone").value="";
            document.getElementById("formTenant:txtpostCode").value="";
            document.getElementById("formTenant:txtstreet").value="";
            
            var cmbTitle=document.getElementById("formTenant:selectTitle");
		     cmbTitle.selectedIndex = 0;
		    
		    var cmbGender =document.getElementById("formTenant:selectGender");
		      cmbGender.selectedIndex = 0; 

		    var cmbcity=document.getElementById("formTenant:selectcity");
		     cmbcity.selectedIndex = 0;
		    
		    var cmbstate=document.getElementById("formTenant:selectstate");
		     cmbstate.selectedIndex = 0;
		    
		    var cmbcountry=document.getElementById("formTenant:selectcountry");
		     cmbcountry.selectedIndex = 0; 
		    
            var cmbnationality=document.getElementById("formTenant:selectnationality");
		     cmbnationality.selectedIndex = 0; 		    
		    }
		    
		    function submitForm()
	   {
          document.getElementById('formTenant').submit();
          document.getElementById("formTenant:selectcountry").selectedIndex=0;
          document.getElementById("formTenant:selectstate").selectedIndex=0;
	   }
		    
		    function submitFormState(){
       
		       
		    var cmbState=document.getElementById("formTenant:selectstate");
		    var j = 0;
		    j=cmbState.selectedIndex;
		   
		    document.getElementById("formTenant:hdnCityValue").value=cmbState.options[j].value ;
		     
		    document.forms[0].submit();
		   
		    
		    }
		
    </script>
</head>
<body dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE"> 

  
    
   
    
    
    <!-- Header --> 
<table width="100%" cellpadding="0" cellspacing="0"  border="0">
    <tr>
    
            <c:choose>
			<c:when test="${!pages$AddTenant.isViewModePopUp}">
			<td colspan="2">
		        <jsp:include page="header.jsp"/>
		     </td>
		     </c:when>
		     </c:choose>
    </tr>

<tr width="100%">
     <c:choose>
			<c:when test="${!pages$AddTenant.isViewModePopUp}">
	
    <td class="divLeftMenuWithBody" width="17%">
        <jsp:include page="leftmenu.jsp"/>
    </td>
      </c:when>
		     </c:choose>
    <td width="83%" class="divBackgroundBody" height="472px">
        <table cellpadding="0" cellspacing="0" height="100%" width="100%">
		<tr height="1%">
			<td>
				<table width="99.2%" class="greyPanelTable" cellpadding="0"
					cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
								<h:outputLabel value="#{msg['tenants.tenantInformation']}" styleClass="HEADER_FONT"/>
						</td>
						<td width="100%">
							&nbsp;
						</td>
					</tr>
				</table>
			</td>
		</tr>    
		<tr height="99%" valign="top">
			<td>
        <table width="100%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%" style="height:98%;">

   
          <tr valign="top">
             <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
             </td> 
	
  <td width="100%" height="465px" valign="top" nowrap="nowrap">
	 <div class="SCROLLABLE_SECTION">		
			<h:form id="formTenant" style="width:93%" >
			    <div style = "height:25px;">
			      <table border="0" class="layoutTable" >
			                                       <tr>
      									    	      <td colspan="6">
														
														<h:outputText value="#{pages$AddTenant.errorMessages}" escape="false" styleClass="ERROR_FONT" style="padding:10px;"/>
											            <h:outputText value="#{pages$AddTenant.successMessages}" escape="false" styleClass="INFO_FONT" style="padding:10px;"/>
														<h:inputHidden id="hdnTenantId" value="#{pages$AddTenant.tenantId}"/>
														<h:inputHidden id="hdnContactInfoId" value="#{pages$AddTenant.contactInfoId}"/>
														<h:inputHidden id="hdnpageMode" value="#{pages$AddTenant.pageMode}"/>
														<h:inputHidden id="hdnViewMode" value="#{pages$AddTenant.viewMode}"/>
														<h:inputHidden id="hdnTenantNumber" value="#{pages$AddTenant.tenantNumber}" />
														
														
														
													  </td>
												</tr>
			      </table>
			     </div> 
			      <div class="MARGIN">
		       						<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
				<div class="DETAIL_SECTION">
			<h:outputLabel value="#{msg['tenants.tenantDetail']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>	
											<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
												 	
												
												<tr>
													<td >
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['customer.title']}:"></h:outputLabel>
													</td>
													<td >
															
													    <h:selectOneMenu  id="selectTitle" style="width: 138px" value="#{pages$AddTenant.selectOneTitle}" >
													       <f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$ApplicationBean.title}"/>
														</h:selectOneMenu>
													
													</td>
													
													<td  >
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['tenants.tenantsType']}:"></h:outputLabel>
													</td>
													<td >
															
													    <h:selectOneMenu  id="selectTenantType" style="width: 138px" value="#{pages$AddTenant.selectOneTenantType}" >
													       <f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$ApplicationBean.tenantType}"/>
														</h:selectOneMenu>
													
													</td>
												
													
												</tr>
											    
											    <tr>
											    	<td >
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['tenants.number']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT" style="width:134px;"  
												         maxlength="15" id = "txtTenantNumber"  value="#{pages$AddTenant.tenantNumber}" 
												         disabled="#{pages$AddTenant.tenantNumberDisable}">
												        
												        </h:inputText>
													</td>
											    <td >
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['customer.firstname']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT"  style="width:134px;"  maxlength="50" id = "txtfirstName"  value="#{pages$AddTenant.firstName}">
												        
												        </h:inputText>
													</td>
											    
												</tr>
											<tr>
											<td >
														<h:outputLabel value="#{msg['customer.middlename']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT" style="width:134px;"   maxlength="10" id="txtmiddleName" value="#{pages$AddTenant.middleName}">
												        
												        </h:inputText>
													</td>
											    	<td >
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['customer.lastname']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT" style="width:134px;"  maxlength="50" id="txtlastName"  value="#{pages$AddTenant.lastName}">
												        
												        </h:inputText>
													</td>
												</tr>
												<tr>
												<td ><h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['customer.gender']}:"></h:outputLabel>
														
													</td>
													<td><h:selectOneMenu id="selectGender" style="width: 138px" value="#{pages$AddTenant.gender1}">
													    <f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
														<f:selectItem itemLabel="#{msg['tenants.gender.male']}" itemValue="M" />
														<f:selectItem itemLabel="#{msg['tenants.gender.female']}" itemValue="F" />	
														</h:selectOneMenu>
												        
													</td>
											<td ><h:outputLabel value="#{msg['customer.dateOfBirth']}:"></h:outputLabel>
														
														
													</td>
													<td>

												    <rich:calendar  id="DateOfBirth" value="#{pages$AddTenant.dateOfBirth}" 
							                          popup="true" datePattern="#{pages$AddTenant.dateFormat}" showApplyButton="false"
							                          locale="#{pages$AddTenant.locale}" 
							                          enableManualInput="false" cellWidth="24px" 
							                          />    
												        
													</td>
										
																				
													
																						
												</tr>
												<tr>
													<td>
														
													<h:outputLabel value="#{msg['customer.cell']}:"/></td>
													<td><h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"   maxlength="15" id="txtcellNo" value="#{pages$AddTenant.cellNo}">
												        
												        </h:inputText>
														</td>
												
												<td ><h:outputLabel value="#{msg['customer.nationality']}:"></h:outputLabel>
														
													</td>
													<td><h:selectOneMenu id="selectnationality" style="width: 138px"  value="#{pages$AddTenant.selectOneNationality}">
														<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$AddTenant.nationality}"/>
														</h:selectOneMenu>
												        
													</td>
													</tr>
											
															
												<tr>
												<td ><h:outputLabel value="#{msg['customer.passportNumber']}:"></h:outputLabel>
														
													</td>
													<td>
												        
													<h:inputText styleClass="A_RIGHT_NUM"  style="width:134px;"  maxlength="15" id="txtpassportNumber" value="#{pages$AddTenant.passportNumber}">
												        
												        </h:inputText></td>
												
											
													<td >
														
													<h:outputLabel value="#{msg['customer.passportIssueDate']}:"></h:outputLabel></td>
													<td>
													<rich:calendar  id="datePassportIssueDate" value="#{pages$AddTenant.passportIssueDate}" 
							                          popup="true" datePattern="#{pages$AddTenant.dateFormat}" showApplyButton="false"
							                          locale="#{pages$AddTenant.locale}" 
							                          enableManualInput="false" cellWidth="24px" 
							                          />    
													</td>
												</tr>		
													<tr>
													
													<td><h:outputLabel value="#{msg['customer.passportExpiryDate']}:"></h:outputLabel>
														
													</td>
													<td>
														<rich:calendar  id="datePassportExpiryDate" value="#{pages$AddTenant.passportExpiryDate}" 
							                             popup="true" datePattern="#{pages$AddTenant.dateFormat}" showApplyButton="false"
							                             locale="#{pages$AddTenant.locale}" 
							                             enableManualInput="false" cellWidth="24px" 
							                          /> 
														</td>
												<td ><h:outputLabel  value="#{msg['customer.residenseVisaNumber']}"></h:outputLabel>
														
													</td>
													<td ><h:inputText styleClass="A_RIGHT_NUM" style="width:134px;" maxlength="15" id="txtresidenseVisaNumber" value="#{pages$AddTenant.residenseVisaNumber}">
												        
												        </h:inputText>
															
													</td>
													
												
													
													</tr>
													
													<tr>
													     	<td ><h:outputLabel value="#{msg['customer.residenseVisaExpDate']}:"></h:outputLabel>
														
													</td>
													<td>
													<rich:calendar  id="dateVisaExpiryDate" value="#{pages$AddTenant.residenseVidaExpDate}" 
							                             popup="true" datePattern="#{pages$AddTenant.dateFormat}" showApplyButton="false"
							                             locale="#{pages$AddTenant.locale}" 
							                             enableManualInput="false" cellWidth="24px" 
							                          /> 
													
													</td>
													
															
																									
											
												<td >
														
													<h:outputLabel value="#{msg['customer.drivingLicenseNumber']}:"></h:outputLabel></td>
													<td><h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"  maxlength="15" id="txtdrivingLicenseNumber" value="#{pages$AddTenant.drivingLicenseNumber}">
												        
												        </h:inputText>
												        
													</td>
												
													</tr>
													
													<tr>
													      <td ><h:outputLabel value="#{msg['customer.personalSecCardNo']}:"></h:outputLabel>
														
													</td>
													<td ><h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"   maxlength="15" id="txtpersonalSecCardNo" value="#{pages$AddTenant.personalSecCardNo}">
												        
												        </h:inputText>
															
													</td>
												
												
													
													
													<td >
														
													<h:outputLabel value="#{msg['customer.socialSecNumber']}:"></h:outputLabel></td>
													<td>
												        
													<h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"   maxlength="15" id="txtsocialSecNumber" value="#{pages$AddTenant.socialSecNumber}">
												        
												        </h:inputText></td>
												
													</tr>
													<tr>
													     		<td >
														
													<h:outputLabel value="#{msg['customer.companyName']}:"></h:outputLabel></td>
													<td>
												        
													<h:inputText styleClass="A_LEFT" style="width:134px;"  maxlength="50" id="txtcompanyname" value="#{pages$AddTenant.companyName}">
												        
												        </h:inputText></td> 
													<td >
														
													<h:outputLabel value="#{msg['customer.workingCompany']}:"></h:outputLabel></td>
													<td>
												        
													<h:inputText styleClass="A_LEFT" style="width:134px;"  maxlength="50" id="txtworkingcompany" value="#{pages$AddTenant.workingCompany}">
												        
												        </h:inputText></td>
												        
												       
													</tr>
													<tr>
													     <td >
														
													<h:outputLabel value="#{msg['customer.govtDepartmant']}:"></h:outputLabel></td>
													<td>
												        
													<h:inputText styleClass="A_LEFT" style="width:134px;"  maxlength="50" id="txtgovtdepartment" value="#{pages$AddTenant.govtDepartmant}">
												        
												        </h:inputText></td>
													</tr>	
												</table>
												</div>
												</div>
												<div class="MARGIN">
									<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
									<div class="DETAIL_SECTION">
										<h:outputLabel value="#{msg['contact.contactinformation']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
															
														
								
											
											
                                           <table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
												<tr >
													<td>
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['contact.address1']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT" style="width:134px;"  maxlength="150" id = "txtaddress1" value="#{pages$AddTenant.address1}">
												        
												        </h:inputText>
													</td>
													<td>
														<h:outputLabel value="#{msg['contact.address2']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT" style="width:134px;"  maxlength="150" id = "txtaddress2" value="#{pages$AddTenant.address2}">
												        
												        </h:inputText>
													</td>
												
													
												</tr>
                                                 
                                                 <tr>
                                                 	<td>
														<h:outputLabel value="#{msg['contact.street']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT" style="width:134px;"  maxlength="150" id = "txtstreet"   value="#{pages$AddTenant.street}">
												        
												        </h:inputText>
													</td>
                                                 <td>
														<h:outputLabel value="#{msg['contact.postcode']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"   maxlength="10" id= "txtpostCode"  value="#{pages$AddTenant.postCode}">
												        
												        </h:inputText>
													</td>
                                                 </tr>
												
												<tr>
												  <td>
													<h:outputLabel value="#{msg['contact.country']}:"></h:outputLabel>
												 </td>
													<td><h:selectOneMenu id="selectcountry" valueChangeListener="#{pages$AddTenant.loadStates}" 
													onchange="submitForm()"  style="width: 138px"  value="#{pages$AddTenant.selectOneCountry}">
													
													<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$AddTenant.countries}" />
															
														</h:selectOneMenu>
														
													</td>
													<td>
														<h:outputLabel value="#{msg['contact.state']}:"></h:outputLabel>
													</td>
													<td>
														<h:selectOneMenu id="selectstate"  valueChangeListener="#{pages$AddTenant.loadCities}"  
														onchange="submitFormState()" style="width: 138px" value="#{pages$AddTenant.selectOneState}">
														
														<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$AddTenant.states}"/>
															
														</h:selectOneMenu>	
													</td>
												
											
													
												</tr>
												<tr>
													<td>
														<h:outputLabel value="#{msg['contact.city']}"></h:outputLabel>
													</td>
												        <td>
														<h:selectOneMenu id="selectcity" style="width: 138px"  value="#{pages$AddTenant.selectOneCity}">
														<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}:" itemValue="-1"/>
															<f:selectItems value="#{pages$AddTenant.cities}"/>
														</h:selectOneMenu>	
														</td>
													<td>
														<h:outputLabel value="#{msg['contact.homephone']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"   maxlength="15" id = "txthomePhone" value="#{pages$AddTenant.homePhone}">
												        
												        </h:inputText>
													</td>
													
												</tr>
												
												<tr>
												    <td>
														<h:outputLabel value="#{msg['contact.officephone']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"  maxlength="15" id  ="txtofficePhone" value="#{pages$AddTenant.officePhone}">
												        
												        </h:inputText>
													</td>
												<td>
													 <h:outputLabel value="#{msg['contact.fax']}:"></h:outputLabel>
												</td>
													<td >
												        <h:inputText styleClass="A_RIGHT_NUM" style="width:134px;"  maxlength="15" id = "txtfax"  value="#{pages$AddTenant.fax}">
												        
												        </h:inputText>
													</td>
												
												</tr>
												<tr>
												     	<td>
														<h:outputLabel value="#{msg['contact.email']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT" style="width:134px;"  maxlength="50" id = "txtemail"  value="#{pages$AddTenant.email}">
												        
												        </h:inputText>
													</td>
												</tr>
												
												<h:inputHidden id="hdnStateValue" binding="#{pages$AddTenant.hiddenStateValue}"  />
												<h:inputHidden id="hdnCityValue" binding="#{pages$AddTenant.hiddenCityValue}"  />
											
											
											
                                		        
                                            </table>
                                         </div>
                                         </div>
                                         
                                       <div>
                                         <table width="100%" >
                                         <tr>
												
        										<td colspan="10"  class="BUTTON_TD">
                                                        
                                                  <pims:security screen="Pims.TenantManagement.Tenant.AddTenant" action="create">
                                      				   <h:commandButton rendered="#{!pages$AddTenant.isViewModeSetUpView}" styleClass="BUTTON" value="#{msg['commons.saveButton']}" type= "submit"   action="#{pages$AddTenant.btnAdd_Click}" >  </h:commandButton>
                                      			</pims:security>	   
												       <h:commandButton rendered="#{!pages$AddTenant.isViewModeSetUpView}" styleClass="BUTTON" type="button" onclick="javascript:clearWindow();" value="#{msg['commons.reset']}" >  </h:commandButton>
												       <h:commandButton rendered="#{!pages$AddTenant.isViewModeSetUpView && !pages$AddTenant.isViewModePopUp}" styleClass="BUTTON" type= "submit" value="#{msg['commons.cancel']}" action="#{pages$AddTenant.btnBack_Click}" >	</h:commandButton>
												       <h:commandButton rendered="#{pages$AddTenant.isViewModeSetUpView  || pages$AddTenant.isViewModePopUp}"  styleClass="BUTTON" type= "button" value="#{msg['commons.cancel']}" onclick="history.go(-1)" >	</h:commandButton>
                                                       
												       
                                		        </td>
                                		  </tr>
                                         </table>
                                       </div>  
 										
												                         
			        </h:form>
			</div>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			<tr>
						<td colspan="2">
							<c:choose>
			                   <c:when test="${!pages$AddTenant.isViewModePopUp}">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
							            	<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
					     
									
										</tr>
									</table>
						       </c:when>
		                     </c:choose>
					</td>
				</tr>
			</table>
			
			</body>
		</f:view>
	
</html>
