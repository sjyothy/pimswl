<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">	
</script>
<t:panelGrid id="pnlGrdFields" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4">
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['memsFollowupTab.cat']}:" />
	</h:panelGroup>
	<h:selectOneMenu id="categoryCmb" tabindex="1"
		value="#{pages$recommFollowupTab.memsFollowupView.category}">
		<f:selectItems value="#{pages$ApplicationBean.followupCategory}" />
	</h:selectOneMenu>

	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['memsFollowupTab.type']}:" />
	</h:panelGroup>
	<h:selectOneMenu id="followTypeCmb" tabindex="1"
		value="#{pages$recommFollowupTab.memsFollowupView.typeId}">
		<f:selectItems value="#{pages$ApplicationBean.memsFollowupTypeList}" />
	</h:selectOneMenu>


	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['memsFollowupTab.date']}:" />
	</h:panelGroup>
	<rich:calendar
		value="#{pages$recommFollowupTab.memsFollowupView.followupDate}"
		locale="#{pages$recommFollowupTab.locale}"
		datePattern="#{pages$recommFollowupTab.dateFormat}" popup="true"
		showApplyButton="false" enableManualInput="false" />

	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['memsFollowupTab.desc']}:" />
	</h:panelGroup>
	<h:inputText id="followupdesc"
		value="#{pages$recommFollowupTab.memsFollowupView.description}"></h:inputText>

	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['memsFollowupTab.details']}:" />
	</h:panelGroup>
	<h:inputTextarea
		value="#{pages$recommFollowupTab.memsFollowupView.details}"
		style="width: 185px;" />



</t:panelGrid>
<t:panelGrid id="pnlGrdActions" styleClass="BUTTON_TD" cellpadding="1px" width="100%" cellspacing="5px">
	<h:panelGroup >
		<h:commandButton styleClass="BUTTON"
			value="#{msg['commons.saveButton']}"
			action="#{pages$recommFollowupTab.onSave}" />
	</h:panelGroup>
</t:panelGrid>


<t:div styleClass="contentDiv" style="width:98%" >																
	<t:dataTable id="memsFollowupViewDataTable"  
				rows="#{pages$recommFollowupTab.paginatorRows}"
				value="#{pages$recommFollowupTab.memsFollowupViewList}"
				binding="#{pages$recommFollowupTab.memsFollowUpDataTable}"																	
				preserveDataModel="false" preserveSort="false" var="dataItem"
				rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">



		<t:column width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['receiveProperty.propertyCategory']}" style="white-space: normal;" />
			</f:facet>
			
			<t:outputText style="white-space: normal;"  value="#{pages$recommFollowupTab.isEnglishLocale?dataItem.categoryEn:dataItem.categoryAr}" />
		</t:column>
		<t:column width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['maintenanceRequest.followUpBy']}" style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"  value="#{pages$recommFollowupTab.isEnglishLocale?dataItem.createdByEn:dataItem.createdByAr}" />																		
		</t:column>																																
		<t:column width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['memsFollowupTab.date']}" style="white-space: normal;"  />
			</f:facet>
			<t:outputText value="#{dataItem.followupDate}"  style="white-space: normal;" />																		
		</t:column>
		
		<t:column width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['memsFollowupTab.type']}" style="white-space: normal;" />
			</f:facet>
			<t:outputText value="#{pages$recommFollowupTab.isEnglishLocale?dataItem.typeEn:dataItem.typeAr}" style="white-space: normal;" />																		
		</t:column>
		
		
		
		<t:column width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{memsFollowupTab.desc}" style="white-space: normal;" />
			</f:facet>
			<t:outputText value="#{dataItem.description}" style="white-space: normal;" />																		
		</t:column>
		
		<t:column width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['memsFollowupTab.details']}" style="white-space: normal;" />
			</f:facet>
			<t:outputText value="#{dataItem.details}" style="white-space: normal;" />																		
		</t:column>
																	
		<t:column width="10%" sortable="false">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.action']}" />
			</f:facet>
			<t:commandLink action="#{pages$recommFollowupTab.deleteFollowup}">
				<h:graphicImage title="#{msg['commons.delete']}"
					style="cursor:hand;" url="../resources/images/delete.gif" />&nbsp;
			</t:commandLink>
		</t:column>
		
	</t:dataTable>										
</t:div>
<t:div id="recommFollowupViewDataTableFooter" styleClass="contentDivFooter" style="width:99%">
	<t:panelGrid id="followupfooterTable"  columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
		
																				  
		<CENTER>
			<t:dataScroller id="followupscroller" for="memsFollowupViewDataTable" paginator="true"
							fastStep="1"  
							paginatorMaxPages="#{pages$recommFollowupTab.paginatorMaxPages}" 
							immediate="false"
							paginatorTableClass="paginator"
							renderFacetsIfSinglePage="true" 
							pageIndexVar="pageNumber"
							styleClass="SCH_SCROLLER"
							paginatorActiveColumnStyle="font-weight:bold;"
							paginatorRenderLinkForActive="false" 
							paginatorTableStyle="grid_paginator" layout="singleTable"
							paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;">
								
							<f:facet name="first">
								<t:graphicImage url="../#{path.scroller_first}" id="lblFollowupFirst"></t:graphicImage>
							</f:facet>
									
							<f:facet name="fastrewind">
								<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFollowupF"></t:graphicImage>
							</f:facet>
									
							<f:facet name="fastforward">
								<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFollowupFF"></t:graphicImage>
							</f:facet>
									
							<f:facet name="last">
								<t:graphicImage url="../#{path.scroller_last}" id="lblFollowupL"></t:graphicImage>
							</f:facet>
									
							
			</t:dataScroller>
		</CENTER>																		      
	</t:panelGrid>
</t:div>											
