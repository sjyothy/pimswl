<%-- Author: Kamran Ahmed--%>
<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript"> 
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr width="100%">
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['mems.inheritanceFile.tabHeading.recommFollowUp']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="memsFollowupForm"
												enctype="multipart/form-data" style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px; width: 100%;">
														<tr>
															<td>
																<h:outputText
																	value="#{pages$recommFollowup.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText
																	escape="false" styleClass="INFO_FONT" />
																<h:messages></h:messages>
															</td>
														</tr>
													</table>
												</div>		
												
												<div>
													<div class="AUC_DET_PAD">
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel switchType="server" 
																style="HEIGHT: 350px;#HEIGHT: 300px;width:80%">

																<rich:tab id="followupTabId"
																	title="#{msg['mems.inheritanceFile.tabHeading.followUp']}"
																	label="#{msg['mems.inheritanceFile.tabHeading.followUp']}">
																	<%@ include file="recommFollowupTab.jsp"%>
																</rich:tab>
																
																<rich:tab id="attachmentTab" 
																	title="#{msg['commons.attachmentTabHeading']}"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>
															</rich:tabPanel>
														</div>
														<br/>
														<t:div styleClass="BUTTON_TD" style="width:80%;">
															<h:commandButton styleClass="BUTTON"
																rendered="false"
																value="#{msg['commons.saveButton']}"
																action="#{pages$recommFollowup.onSave}" />
															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.closeButton']}"
																style="width: auto"
																onclick="window.close();">
															</h:commandButton>
															
														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

				</table>
			</div>
		</body>
	</html>
</f:view>