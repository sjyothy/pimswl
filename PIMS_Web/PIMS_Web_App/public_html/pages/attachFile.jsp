<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Uploading a file
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		function closeWindowSubmit()
		{
			window.opener.document.forms[0].submit();
		  	window.close();	  
		}	
		function showScanPopup()

            {

                  var screen_width = 1024;

                  var screen_height = 450;

                  var popup_width = screen_width-200;

                  var popup_height = screen_height;

                  var leftPos = (screen_width - (screen_width/3+150))/2 , topPos = (screen_height)/2 - 20;
                  
                  var popup = window.open('scanner.jsf','_blank','width='+ popup_width +',height='+ popup_height +
                      ',left='+ leftPos +',top='  + topPos + ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');

                  popup.focus();


            }
</script>

<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
		 <title>PIMS</title>
		 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
		 <meta http-equiv="pragma" content="no-cache">
		 <meta http-equiv="cache-control" content="no-cache">
		 <meta http-equiv="expires" content="0">
		 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 	<script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
	</head>

	<body class="BODY_STYLE">
			<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
			%>
	
				<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="100%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
											<h:outputLabel value="#{msg['attachment.header']}" styleClass="HEADER_FONT"/>
									</td>
								</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" 
							cellspacing="0" border="0">
								<tr valign="top">
								<td width="100%" height="100%" valign="top" >
								<div style="display:block;height:700px;width:100%">
									<h:form id="attachFileFrm" enctype="multipart/form-data">
										<t:div styleClass="MESSAGE"> 
											<t:panelGrid columns="1" style="margin-left:5px;margin-right:5px;">
												<h:outputText value="#{pages$attachFile.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
												<h:outputText value="#{pages$attachFile.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
											</t:panelGrid>
										</t:div>
										
										<t:panelGrid columns="1" cellpadding="2" cellspacing="0" binding="#{pages$attachFile.uploadPanel}" rendered="true" border="0" style="margin-bottom:20px;">
											<t:panelGroup rendered="#{!pages$attachFile.isProcDoc}">
												<t:outputLabel styleClass="mandatory" value="*"/>
												<t:outputLabel styleClass="LABEL" value="#{msg['attachment.docType']}:" style="padding-right:5px;padding-left:5px;margin-right:10px;"></t:outputLabel>			
												<t:selectOneMenu id="docTypeCombo"
													value="#{pages$attachFile.docTypeId}"
													 required="false" binding="#{pages$attachFile.docTypeCombo}">
													<f:selectItem itemValue="0" itemLabel="#{msg['commons.select']}" />
													<f:selectItems value="#{pages$ApplicationBean.documentTypeList}" />
												</t:selectOneMenu>
											</t:panelGroup>
											<t:panelGroup rendered="#{!pages$attachFile.isProcDoc}">
												<t:outputLabel styleClass="mandatory" value="*"/>
												<t:outputLabel styleClass="LABEL" value="#{msg['attachment.grid.docNameCol']}:" style="padding-right:5px;padding-left:5px;margin-right:5px;"></t:outputLabel>			
												<t:inputText id="docDesc" value="#{pages$attachFile.documentDescription}" binding="#{pages$attachFile.documentDescriptionField}"   maxlength="100"></t:inputText>
											</t:panelGroup>
											<t:panelGroup>
												<t:outputLabel styleClass="LABEL ATTACH_1" value="#{msg['attachment.comments']}: "></t:outputLabel>
												<t:inputTextarea id="docComments" styleClass="TEXTAREA ATTACH_3" value="#{pages$attachFile.comments}" binding="#{pages$attachFile.commentsField}"rows="4" style="width: 260px; height: 46px; "/>
											</t:panelGroup>
											<t:panelGroup rendered="#{!pages$attachFile.scannerImageSelected}">
												<t:outputLabel styleClass="mandatory" value="*"/><t:outputLabel styleClass="LABEL ATTACH_2" value="#{msg['attachment.location']}: " ></t:outputLabel>
												<t:inputFileUpload id="fileupload" size="350" storage="file" maxlength="250" value="#{pages$attachFile.selectedFile}" binding="#{pages$attachFile.fileUploadCtrl}" style="height: 16px;width: 190px; margin-left: 5px;"></t:inputFileUpload>
											</t:panelGroup>
											
											<t:panelGroup rendered="#{pages$attachFile.scannerImageSelected}">
											<t:outputLabel  value="#{msg['scan.Img.Selected']} " ></t:outputLabel>
											</t:panelGroup>
											<t:panelGroup>	
												<t:div styleClass="BUTTON_TD"  >			
													<t:commandButton id="btnUpload" styleClass="BUTTON" value="#{msg['attachment.buttons.upload']}" actionListener="#{pages$attachFile.uploadFile}" style="width: 75px;margin-right: 2px;margin-left: 2px;" />
													<t:commandButton id="btnScan" styleClass="BUTTON" value="#{msg['Tool.Scan']}" actionListener="#{pages$attachFile.openScanPopup}" style="width: 75px;margin-right: 2px;margin-left: 2px;" />
													<t:commandButton id="btnCancel" styleClass="BUTTON" value="#{msg['commons.cancel']}" onclick="javascript:window.close();" style="width: 75px; margin-right: 2px;margin-left: 2px;"/>

													</t:div>
											</t:panelGroup>
										</t:panelGrid>
										</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
        		</td>
		    </tr>
		</table>
	</body>
</html>
</f:view>
