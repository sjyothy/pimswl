<%-- 
  - Author: Munir chagani
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Auction
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<%
					response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
						response.setHeader("Pragma", "no-cache"); //HTTP 1.0
						response.setDateHeader("Expires", 0); //prevents caching at the proxy server
				%>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">


					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>


					<tr width="100%">

						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="83%" height="100%" valign="top"
							class="divBackgroundBody">
							<table class="greyPanelTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['userGroup.permission']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr valign="top">
									<td>

										<h:form id="searchFrm" enctype="multipart/form-data">
											<div class="SCROLLABLE_SECTION">
												<div>
													<table border="0" class="layoutTable">
														<tr>
															<td>
																<h:outputText value="#{pages$permissionsTree.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																	
																	<h:outputText value="#{pages$permissionsTree.infoMessages}"
																	escape="false" styleClass="INFO_FONT" />																	
																
															</td>
														</tr>
													</table>
												</div>
												<div class="MARGIN" style="width: 95%;">
												<table cellpadding="1px" cellspacing="2px" >
										
															<tr>
																<td class="DET_SEC_TD">																
																	<h:outputLabel  styleClass="LABEL" value="#{msg['userGroup.groupId']}: "/>
																</td>
																<td class="DET_SEC_TD">
																	
																	<h:inputText id="selectOneGroupId"
																		styleClass="INPUT_TEXT READONLY" readonly="true"
																		value="#{pages$permissionsTree.currentGroup.userGroupId}" 											
																		tabindex="1">
																	</h:inputText>																	
																	
																
																</td>
																<td class="DET_SEC_TD">
																	
																</td>
																<td class="DET_SEC_TD">
																	
																</td>
															</tr>	
															<tr>
																<td class="DET_SEC_TD">																	
										       						 <h:outputLabel  styleClass="LABEL"  value="#{msg['userGroup.groupName']}: "></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="txtGroupNameSec"
																		styleClass="INPUT_TEXT READONLY" readonly="true"
																		value="#{pages$permissionsTree.currentGroup.primaryName}" 																		
																		tabindex="2">
																	</h:inputText>
																</td>
																<td class="DET_SEC_TD">																	
										        					<h:outputLabel  styleClass="LABEL"  value="#{msg['userGroup.groupNameArabic']}: "></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<t:inputText id="txtGroupSec" tabindex="3" readonly="true"
																	value="#{pages$permissionsTree.currentGroup.secondaryName}" 	
															        styleClass="INPUT_TEXT READONLY" 
															        />
																</td>
															</tr>																	
										</table>	
										
										&nbsp;
										&nbsp;
										&nbsp;
													<table cellpadding="0" cellspacing="0" width="100%">

														<tr>
															<td colspan="5">


																<t:tree2 showRootNode="false" id="clientTree"
															value="#{pages$permissionsTree.permissionsTree}"
															var="node" varNodeToggler="t">
															<f:facet name="Module">
																<h:panelGroup style="width:100%;">
																	<h:panelGrid columns="8">
																	<t:graphicImage
																					value="../resources/images/tree/FolderOpen.gif"
																					rendered="#{t.nodeExpanded}" border="0" />
																				<t:graphicImage
																					value="../resources/images/tree/folder.gif"
																					rendered="#{!t.nodeExpanded}" border="0" />
																		<h:outputLabel style="font-weight:bold;"
																			value="#{node.permissionName}">
																		</h:outputLabel>
																		<h:outputText style="font-weight:bold;"
																			value=" (#{node.childCount})"
																			rendered="#{!empty node.children}" />

																		<%--          <t:outputText value="#{msg['commons.group.permissions.create']}"></t:outputText>
         <t:selectBooleanCheckbox id="personCheck" value="#{node.canCreate}"
                                 title="#{node.permissionBinding.permissionId}"
                                  disabled="#{!node.linkCanCreate}"></t:selectBooleanCheckbox>
         <t:outputText 
                       value="#{msg['commons.group.permissions.update']}" ></t:outputText>
         <t:selectBooleanCheckbox id="personCheck2" value="#{node.canUpdate}"
                              
                                  title="#{node.permissionBinding.permissionId}"
                                  disabled="#{!node.linkCanUpdate}"></t:selectBooleanCheckbox>
         <t:outputText
                       value="#{msg['commons.group.permissions.delete']}" ></t:outputText>
         <t:selectBooleanCheckbox id="personCheck3" value="#{node.canDelete}"
                                 
                                  title="#{node.permissionBinding.permissionId}"
                                  disabled="#{!node.linkCanDelete}"></t:selectBooleanCheckbox>
		                                   
         <t:outputText 
                       value="#{msg['commons.group.permissions.view']}" ></t:outputText>--%>
																		<h:selectBooleanCheckbox id="personCheck4"
																			value="#{node.canView}"
																			title="#{node.permissionBinding.permissionId}"></h:selectBooleanCheckbox>

																	</h:panelGrid>
																</h:panelGroup>
															</f:facet>
															<f:facet name="None">
																<h:panelGroup style="width:100%;">
																	<h:outputLabel style="font-weight:bold;"
																		value="#{node.permissionName}" />
																	<h:outputText style="font-weight:bold;"
																		value="(#{node.childCount})"
																		rendered="#{!empty node.children}" />
                      &nbsp;
        <h:panelGrid columns="8" styleClass="tabbedPaneHeader">
																		<t:outputText
																			value="#{msg['commons.group.permissions.create']}"></t:outputText>
																		<t:selectBooleanCheckbox id="foo-folderCheck"
																			value="#{node.canCreate}"
																			title="#{node.permissionBinding.permissionId}"
																			disabled="#{!node.linkCanCreate}"></t:selectBooleanCheckbox>
                                  &nbsp;
         <t:outputText
																			value="#{msg['commons.group.permissions.update']}"></t:outputText>
																		<t:selectBooleanCheckbox id="foo-folderCheck2"
																			value="#{node.canUpdate}"
																			title="#{node.permissionBinding.permissionId}"
																			disabled="#{!node.linkCanUpdate}"></t:selectBooleanCheckbox>
                                  &nbsp;
         <t:outputText
																			value="#{msg['commons.group.permissions.delete']}"></t:outputText>
																		<t:selectBooleanCheckbox id="foo-folderCheck3"
																			value="#{node.canDelete}"
																			title="#{node.permissionBinding.permissionId}"
																			disabled="#{!node.linkCanDelete}"></t:selectBooleanCheckbox>
                                  &nbsp;
         <t:outputText value="#{msg['commons.group.permissions.view']}"></t:outputText>
																		<h:selectBooleanCheckbox id="foo-folderCheck4"
																			value="#{node.canView}"
																			title="#{node.permissionBinding.permissionId}"></h:selectBooleanCheckbox>
                                  &nbsp;
        </h:panelGrid>
																</h:panelGroup>
															</f:facet>
															<f:facet name="Transaction">
																<h:panelGroup style="width:100%;">
																<t:graphicImage
																						value="../resources/images/tree/Transaction.gif"
																						rendered="#{t.nodeExpanded}" border="0" />
																					<t:graphicImage
																						value="../resources/images/tree/Transaction.gif"
																						rendered="#{!t.nodeExpanded}" border="0" />
																	<h:outputLabel style="font-weight:bold;"
																		value="#{node.permissionName}" />
																	<h:outputText style="font-weight:bold;"
																		value=" (#{node.childCount})"
																		rendered="#{!empty node.children}" />
																	<h:panelGrid columns="8">
																		<t:outputText
																			value="#{msg['commons.group.permissions.create']}"
																			styleClass="Label"></t:outputText>
																		<t:selectBooleanCheckbox id="bar-folderCheck"
																			value="#{node.canCreate}"
																			title="#{node.permissionBinding.permissionId}"
																			disabled="#{!node.linkCanCreate}"></t:selectBooleanCheckbox>
																		<t:outputText
																			value="#{msg['commons.group.permissions.update']}"></t:outputText>
																		<t:selectBooleanCheckbox id="bar-folderCheck2"
																			value="#{node.canUpdate}"
																			title="#{node.permissionBinding.permissionId}"
																			disabled="#{!node.linkCanUpdate}"></t:selectBooleanCheckbox>
																		<t:outputText
																			value="#{msg['commons.group.permissions.delete']}"></t:outputText>
																		<t:selectBooleanCheckbox id="bar-folderCheck3"
																			value="#{node.canDelete}"
																			title="#{node.permissionBinding.permissionId}"
																			disabled="#{!node.linkCanDelete}"></t:selectBooleanCheckbox>
																		<t:outputText
																			value="#{msg['commons.group.permissions.view']}"></t:outputText>
																		<h:selectBooleanCheckbox id="bar-folderCheck4"
																			value="#{node.canView}"
																			title="#{node.permissionBinding.permissionId}"></h:selectBooleanCheckbox>
																	</h:panelGrid>
																</h:panelGroup>
															</f:facet>
															<f:facet name="Action">
																<h:panelGroup style="width:100%;">
																<t:graphicImage
																						value="../resources/images/tree/Action.png"
																						rendered="#{t.nodeExpanded}" border="0" />
																					<t:graphicImage
																						value="../resources/images/tree/Action.png"
																						rendered="#{!t.nodeExpanded}" border="0" />

																	<h:outputText value="#{node.permissionName}" />
																	<f:param name="docNum" value="#{node.identifier}" />
																	<h:panelGrid columns="8">
																		<t:outputText
																			value="#{msg['commons.group.permissions.create']}"></t:outputText>
																		<t:selectBooleanCheckbox id="documentCheck"
																			value="#{node.canCreate}"
																			title="#{node.permissionBinding.permissionId}"
																			disabled="#{!node.linkCanCreate}"></t:selectBooleanCheckbox>
																		<t:outputText
																			value="#{msg['commons.group.permissions.update']}"></t:outputText>
																		<t:selectBooleanCheckbox id="documentCheck2"
																			value="#{node.canUpdate}"
																			title="#{node.permissionBinding.permissionId}"
																			disabled="#{!node.linkCanUpdate}"></t:selectBooleanCheckbox>
																		<t:outputText
																			value="#{msg['commons.group.permissions.delete']}"></t:outputText>
																		<t:selectBooleanCheckbox id="documentCheck3"
																			value="#{node.canDelete}"
																			title="#{node.permissionBinding.permissionId}"
																			disabled="#{!node.linkCanDelete}"></t:selectBooleanCheckbox>
																		<t:outputText
																			value="#{msg['commons.group.permissions.view']}"
																			styleClass="Label"></t:outputText>
																		<h:selectBooleanCheckbox id="documentCheck4"
																			value="#{node.canView}"
																			title="#{node.permissionBinding.permissionId}"></h:selectBooleanCheckbox>
																	</h:panelGrid>
																</h:panelGroup>
															</f:facet>
														</t:tree2>

															</td>
														</tr>


														<tr align="right">
															<td colspan="5">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$permissionsTree.cmdSavePerm_action}"
																	type="submit">
																</h:commandButton>
																&nbsp;
																<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}" 
																action="#{pages$permissionsTree.cancel}"></h:commandButton>															
																&nbsp;


															</td>

														</tr>

													</table>

												</div>
											</div>

										</h:form>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>