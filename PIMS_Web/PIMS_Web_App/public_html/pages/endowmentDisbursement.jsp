<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	function performTabClick(elementid)
	{
			
		disableInputs();
		
		document.getElementById("detailsFrm:"+elementid).onclick();
		
		return 
		
	}
	function onBeneficiaryChange()
	{
	
	disableInputs();
	 document.getElementById("detailsFrm:onBeneficiaryChange").onclick();
	}
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
	}	
	
		
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		disableInputs();	
		if( hdnPersonType=='APPLICANT' ) 
		{			    			 
			 document.getElementById("detailsFrm:hdnPersonId").value=personId;
			 document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
			 document.getElementById("detailsFrm:hdnPersonName").value=personName;
			 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
			 document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;
		}
				
	     document.getElementById("detailsFrm:onMessageFromSearchPerson").onclick();
	} 
	
		
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel
											value="#{pages$endowmentDisbursement.pageTitle}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>

																<h:outputText id="errorId"
																	value="#{pages$endowmentDisbursement.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$endowmentDisbursement.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$endowmentDisbursement.pageMode}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$endowmentDisbursement.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$endowmentDisbursement.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$endowmentDisbursement.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$endowmentDisbursement.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$endowmentDisbursement.hdnIsCompany}"></h:inputHidden>

																<h:commandLink id="onMessageFromSearchPerson"
																	action="#{pages$endowmentDisbursement.onMessageFromSearchPerson}" />

																<h:commandLink id="onMessageFromEndowmentPrograms"
																	action="#{pages$endowmentDisbursement.onMessageFromEndowmentProgramSearch}" />

																<h:commandLink id="onBeneficiaryChange"
																	action="#{pages$endowmentDisbursement.onBeneficiaryChange}" />


															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<t:div rendered="true" style="width:100%;">
														<t:panelGrid cellpadding="1px" width="100%"
															cellspacing="5px" columns="4">

															<h:outputLabel styleClass="LABEL"
																value="#{msg['maintenanceRequest.remarks']}"
																rendered="#{pages$endowmentDisbursement.showApproval || 
																 			pages$endowmentDisbursement.showComplete 
																           }" />
															<t:panelGroup colspan="3" style="width: 100%">
																<t:inputTextarea style="width: 90%;" id="txt_remarks"
																	rendered="#{pages$endowmentDisbursement.showApproval || 
																 				pages$endowmentDisbursement.showComplete
																 			    }"
																	value="#{pages$endowmentDisbursement.txtRemarks}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</t:panelGroup>
														</t:panelGrid>
													</t:div>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$endowmentDisbursement.tabPanel}"
																style="width: 100%">

																<rich:tab id="applicationTab"
																	label="#{msg['commons.tab.applicationDetails']}">
																	<%@ include file="ApplicationDetails.jsp"%>
																</rich:tab>
																<rich:tab id="disbursementsTab"
																	label="#{msg['mems.normaldisb.label.disbdetail']}">

																	<t:panelGrid id="basicInfoFields" border="0"
																		width="100%" cellpadding="1" cellspacing="1"
																		columns="4">

																		<h:outputLabel style="font-weight:normal;"
																			styleClass="TABLE_LABEL"
																			value="#{msg['paymentDetailsPopUp.paidTo']} :"></h:outputLabel>
																		<h:panelGroup>
																			<h:selectOneMenu id="beneficiary"
																				disabled="#{
																		              !( 
																		                pages$endowmentDisbursement.pageMode=='NEW'  || 
																		  				pages$endowmentDisbursement.pageMode=='PAGE_MODE_RESUBMITTED'
																		  				)  
																		             }"
																				onchange="javaScript:onBeneficiaryChange()"
																				value="#{pages$endowmentDisbursement.selectedBeneficiaryId}">
																				<f:selectItem
																					itemLabel="#{msg['commons.pleaseSelect']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$endowmentDisbursement.beneficiaryList}" />


																			</h:selectOneMenu>
																			<h:commandLink
																				action="#{pages$endowmentDisbursement.openStatmntOfAccount}">
																				<h:graphicImage
																					title="#{msg['report.tooltip.PrintStOfAcc']}"
																					style="MARGIN: 1px 1px -5px;cursor:hand"
																					url="../resources/images/app_icons/print.gif" />
																			</h:commandLink>
																		</h:panelGroup>
																		<h:outputLabel id="lblBalance" styleClass="LABEL"
																			value="#{msg['mems.normaldisb.label.balance']}:"
																			rendered="#{
																		                pages$endowmentDisbursement.pageMode=='NEW'  || 
																		  				pages$endowmentDisbursement.pageMode=='PAGE_MODE_RESUBMITTED' 
																		  				
																		             }">
																		</h:outputLabel>
																		<h:inputText id="accountBalance" readonly="true"
																			rendered="#{
																		                pages$endowmentDisbursement.pageMode=='NEW'  || 
																		  				pages$endowmentDisbursement.pageMode=='PAGE_MODE_RESUBMITTED'
																		  				
																		             }"
																			styleClass="READONLY"
																			binding="#{pages$endowmentDisbursement.accountBalance}">
																		</h:inputText>

																		<h:outputLabel id="lblAmount" styleClass="LABEL"
																			value="#{msg['commons.amount']}:"
																			rendered="#{
																		              !( 
																		                pages$endowmentDisbursement.pageMode=='NEW'  || 
																		  				pages$endowmentDisbursement.pageMode=='PAGE_MODE_RESUBMITTED'
																		  				)  
																		             }">
																		</h:outputLabel>
																		<h:inputText id="amount" styleClass="READONLY"
																			readonly="true"
																			rendered="#{
																		              !( 
																		                pages$endowmentDisbursement.pageMode=='NEW'  || 
																		  				pages$endowmentDisbursement.pageMode=='PAGE_MODE_RESUBMITTED'
																		  				)  
																		             }"
																			value="#{pages$endowmentDisbursement.totalAmount}" />


																		<h:outputLabel styleClass="LABEL"
																			rendered="#{!( 
																			                pages$endowmentDisbursement.pageMode=='NEW'   
																			  				 
																			  			  )
																		  			   }"
																			value="#{msg['cancelContract.payment.paymentmethod']} :"></h:outputLabel>

																		<h:selectOneMenu id="paymentMethodId"
																			rendered="#{!( 
																			                pages$endowmentDisbursement.pageMode=='NEW'   
																			  				 
																			  			  )
																		  			   }"
																			value="#{pages$endowmentDisbursement.selectedPaymentMethodId}"
																			tabindex="3">

																			<f:selectItems
																				value="#{pages$ApplicationBean.paymentMethodsFromPMTable}" />
																		</h:selectOneMenu>


																	</t:panelGrid>

																	<t:div id="contentDiv" styleClass="contentDiv"
																		style="width:98%;">
																		<t:dataTable id="disbursementDT" rows="215"
																			width="100%"
																			value="#{pages$endowmentDisbursement.disbursementList}"
																			binding="#{pages$endowmentDisbursement.disbursementTable}"
																			preserveDataModel="false" preserveSort="false"
																			var="dataItem" rowClasses="row1,row2" rules="all"
																			renderedIfEmpty="true">
																			<t:column id="checkbox"
																				style="width:auto;white-space: normal;">
																				<f:facet name="header">
																				</f:facet>
																				<h:selectBooleanCheckbox id="idbox"
																					rendered="#{dataItem.status!='165006' }"
																					value="#{dataItem.selected}">
																				</h:selectBooleanCheckbox>
																			</t:column>
																			<t:column id="refNumTd" sortable="true"
																				style="white-space: normal;width:3%;">
																				<f:facet name="header">
																					<t:outputText id="lblRefNumTd"
																						value="#{msg['commons.refNum']}" />
																				</f:facet>
																				<t:outputText style="white-space: normal;"
																					id="txtRefNumTd" styleClass="A_LEFT"
																					value="#{dataItem.refNum}" />
																			</t:column>
																			<t:column id="endowmentNumber" sortable="true"
																				style="white-space: normal;width:4%;">
																				<f:facet name="header">
																					<t:outputText id="hdendowmentNumbert"
																						value="#{msg['endowment.lbl.num']}" />
																				</f:facet>
																				<t:outputText id="endowmentNumbert"
																					styleClass="A_LEFT"
																					value="#{dataItem.endowmentNum}" />
																			</t:column>
																			<t:column id="Endowmentname" sortable="true"
																				style="white-space: normal;width:4%;">
																				<f:facet name="header">
																					<t:outputText id="hdEndowmentname"
																						value="#{msg['endowment.lbl.name']}" />
																				</f:facet>
																				<t:outputText id="Endowmentnamet"
																					styleClass="A_LEFT"
																					value="#{dataItem.endowmentName}" />
																			</t:column>

																			<t:column id="idCurStatus" sortable="true"
																				style="white-space: normal;width:3%;">
																				<f:facet name="header">
																					<t:outputText id="hdCurStatus"
																						value="#{msg['commons.status']}" />
																				</f:facet>
																				<t:outputText id="hdInsCurStatus"
																					styleClass="A_LEFT"
																					value="#{pages$endowmentDisbursement.englishLocale?dataItem.statusEn:dataItem.statusAr}" />
																			</t:column>

																			<t:column id="amnt" sortable="true"
																				style="white-space: normal;width:4%;">
																				<f:facet name="header">
																					<t:outputText id="hdAmnt"
																						value="#{msg['commons.amount']}" />
																				</f:facet>
																				<t:outputText id="amountt" styleClass="A_LEFT"
																					value="#{dataItem.amount}" />
																			</t:column>



																			<t:column id="idActions" sortable="true"
																				style="white-space: normal;">
																				<f:facet name="header">
																					<t:outputText id="hdActions"
																						value="#{msg['commons.action']}" />
																				</f:facet>
																				<h:commandLink
																					rendered="#{empty dataItem.status || dataItem.status=='165001' }"
																					action="#{pages$endowmentDisbursement.onDeleteDisbursement}"
																					onclick="if (!confirm('#{msg['mems.common.alertDelete']}')) return false;">
																					<h:graphicImage id="deleteIconDisbursement"
																						title="#{msg['commons.delete']}"
																						url="../resources/images/delete.gif" width="18px;" />
																				</h:commandLink>


																			</t:column>
																		</t:dataTable>
																	</t:div>

																	<t:div id="contentDivFooter"
																		styleClass="contentDivFooter AUCTION_SCH_RF"
																		style="width:99%;">
																		<t:panelGrid columns="2" border="0" width="100%"
																			cellpadding="1" cellspacing="1">
																			<t:panelGroup
																				styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																				<t:div styleClass="JUG_NUM_REC_ATT">
																					<h:outputText value="#{msg['commons.records']}" />
																					<h:outputText value=" : " />
																					<h:outputText id="totalRows"
																						value="#{pages$endowmentDisbursement.totalRows}" />
																				</t:div>
																				<t:div>
																					<table cellpadding="0" cellspacing="0">
																						<tr>
																							<td>
																								<h:outputText styleClass="PAGE_NUM"
																									style="font-size:9;color:black;"
																									value="#{msg['mems.normaldisb.label.totalAmount']}:" />
																							</td>
																							<td>
																								<h:outputText styleClass="PAGE_NUM"
																									id="ttotalAmount"
																									style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:9;font-weight:bold;color:black"
																									value="#{pages$endowmentDisbursement.totalAmount}" />
																							</td>
																						</tr>
																					</table>
																				</t:div>
																			</t:panelGroup>

																		</t:panelGrid>


																	</t:div>
																	<pims:security screen="Pims.EndowMgmt.Disburse.Pay"
																		action="view">

																		<t:div styleClass="BUTTON_TD"
																			rendered="#{pages$endowmentDisbursement.showComplete}"
																			style="padding-top:10px; padding-right:21px;">
																			<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.disburse']}"
																				onclick="performTabClick('lnkDisburse');">
																				<h:commandLink id="lnkDisburse"
																					action="#{pages$endowmentDisbursement.onDisburse}" />
																			</h:commandButton>

																		</t:div>
																	</pims:security>
																</rich:tab>
																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>
																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>
																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$endowmentDisbursement.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<t:div id="btnDiv" styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">
															<pims:security screen="Pims.EndowMgmt.Disburse.Save"
																action="view">

																<h:commandButton styleClass="BUTTON" id="btnSave"
																	rendered="#{pages$endowmentDisbursement.showSaveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$endowmentDisbursement.onSave}" />


																<h:commandButton styleClass="BUTTON" id="btnSubmit"
																	rendered="#{pages$endowmentDisbursement.showSubmitButton}"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSubmit"
																	action="#{pages$endowmentDisbursement.onSubmit}" />
															</pims:security>
															<pims:security screen="Pims.EndowMgmt.Disburse.Resubmit"
																action="view">

																<h:commandButton styleClass="BUTTON" id="btnReSubmit"
																	rendered="#{pages$endowmentDisbursement.showResubmitButton}"
																	value="#{msg['socialResearch.btn.resubmit']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkResubmit"
																	action="#{pages$endowmentDisbursement.onResubmitted}" />

															</pims:security>

															<pims:security
																screen="Pims.EndowMgmt.Disburse.ApproveReject"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentDisbursement.showApproval}"
																	value="#{msg['commons.approve']}"
																	onclick="performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkApprove"
																	action="#{pages$endowmentDisbursement.onApprove}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentDisbursement.showApproval}"
																	value="#{msg['commons.sendBack']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSendBack"
																	action="#{pages$endowmentDisbursement.onRejected}" />
															</pims:security>

															<pims:security screen="Pims.EndowMgmt.Disburse.Pay"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentDisbursement.showComplete}"
																	value="#{msg['commons.complete']}"
																	onclick="performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkDisburseComplete"
																	action="#{pages$endowmentDisbursement.onComplete}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentDisbursement.showComplete}"
																	value="#{msg['commons.sendBack']}"
																	onclick="performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkRejectedByFinance"
																	action="#{pages$endowmentDisbursement.onRejectedByFinance}" />


															</pims:security>

														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>