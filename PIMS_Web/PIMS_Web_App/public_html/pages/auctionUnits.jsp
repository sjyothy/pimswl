<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)\
  - Description: Used for Adding Auction Units to an Auction
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	    
	    function resetValues()
      	{
	      	document.getElementById("unitSearchFrm:investmentPurposeList").selectedIndex=0;
	      	document.getElementById("unitSearchFrm:propertyCategoryList").selectedIndex=0;
		    document.getElementById("unitSearchFrm:propertyUsageList").selectedIndex=0;
      	    document.getElementById("unitSearchFrm:propertyTypeList").selectedIndex=0;
		    document.getElementById("unitSearchFrm:propertyNumber").value="";
			document.getElementById("unitSearchFrm:commercialName").value="";
			document.getElementById("unitSearchFrm:endowedName").value="";
        }
	    

</script>

<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
		<title>PIMS</title>
		
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
		 	<script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>			
 	</head>

	<body class="BODY_STYLE">
			<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
			%>
	
		
	
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0" style="margin-left:1px; margin-right:1px;">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['auctionUnit.heading']}" styleClass="HEADER_FONT"/>
									</td>
								</tr>
							</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap" 	background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
									</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION_POPUP" style="height:500px;overflow:hidden;">
										<h:form id="unitSearchFrm" style="width:96%;">
										<div style="height:25px">
										<table border="0" class="layoutTable">
										<tr>
											<td>
												<h:outputText value="#{pages$auctionUnits.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
											</td>
										</tr>
										</table>
										</div>
										<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 900px; PADDING-TOP: 10px">
										<table cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
										
										<div class="DETAIL_SECTION">
											<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL" ></h:outputLabel>
										<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
											<tr>
												<td style="PADDING: 5px; padding-bottom:0px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['property.number']}:"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; padding-bottom:0px;">
													<h:inputText value="#{pages$auctionUnits.propertyView.propertyNumber}" style="width: 186px; height: 16px" tabindex="1" id="propertyNumber"></h:inputText>
												</td>
												<td style="PADDING: 5px; padding-bottom:0px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['property.type']}:"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; padding-bottom:0px;">
													<h:selectOneMenu value="#{pages$auctionUnits.propertyTypeId}" id="propertyTypeList" style="width: 192px; height: 24px" required="false" tabindex="2">
														<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
														<f:selectItems value="#{pages$ApplicationBean.propertyTypeList}" />
													</h:selectOneMenu>
												</td>
												<td style="PADDING: 5px; padding-bottom:0px;">
													&nbsp;
												</td>
											</tr>
											<tr>
													<td style="PADDING: 5px; padding-bottom:0px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['property.commercialName']}:"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; padding-bottom:0px;">
													<h:inputText id="commercialName" value="#{pages$auctionUnits.propertyView.commercialName}" style="width: 186px; height: 16px" maxlength="150" tabindex="3"></h:inputText>
												</td>
												<td style="PADDING: 5px; padding-bottom:0px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['property.endowedName']}:"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; padding-bottom:0px;">
													<h:inputText id="endowedName" value="#{pages$auctionUnits.propertyView.endowedName}" style="width: 186px; height: 16px" maxlength="150" tabindex="4"></h:inputText>
												</td>
											</tr>
											<tr>
												<td style="PADDING: 5px; padding-bottom:0px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['property.category']}:"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; padding-bottom:0px;">
													<h:selectOneMenu value="#{pages$auctionUnits.propertyCategoryId}" id="propertyCategoryList" style="width: 192px; height: 24px" required="false" tabindex="5" >
														<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
														<f:selectItems value="#{pages$ApplicationBean.propertyCategoryList}" />
													</h:selectOneMenu>
												</td>
												<td style="PADDING: 5px; padding-bottom:0px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['property.usage']}:"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; padding-bottom:0px;">
													<h:selectOneMenu value="#{pages$auctionUnits.propertyUsageId}"id="propertyUsageList" style="width: 192px; height: 24px" required="false" tabindex="6">
														<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
														<f:selectItems value="#{pages$ApplicationBean.propertyUsageList}" />
													</h:selectOneMenu>
												</td>
											</tr>
												<tr>
												<td style="PADDING: 5px; padding-bottom:0px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['property.investmentPurpose']}:"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; padding-bottom:0px;">
													<h:selectOneMenu value="#{pages$auctionUnits.investmentPurposeId}"id="investmentPurposeList" style="width: 192px; height: 24px" required="false" tabindex="7">
														<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
														<f:selectItems value="#{pages$ApplicationBean.investmentPurposeList}" />
													</h:selectOneMenu>
												</td>
												<td  colspan="1">
												</td>
												<td colspan="1">
												</td>
											</tr>
											<tr>
													<td class="BUTTON_TD JUG_BUTTON_TD" colspan="4">
													<table cellpadding="1" cellspacing="1">
													<tr>
													<td>
														<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}" actionListener="#{pages$auctionUnits.cancel}" style="width: 75px" tabindex="11"></h:commandButton>
													</td>
													<td>
														<h:commandButton styleClass="BUTTON" value="#{msg['commons.clear']}" onclick="javascript:resetValues();" style="width: 75px" tabindex="10"></h:commandButton>
													</td>
													<td>											
														<pims:security screen="Pims.AuctionManagement.PrepareAuction.AddAuctionUnits" action="create">
															<h:commandButton styleClass="BUTTON" value="#{msg['commons.search']}" action="#{pages$auctionUnits.searchVacantUnits}" style="width: 75px" tabindex="9"></h:commandButton>
														</pims:security>
													</td>
													</tr>
													</table>
												</td>
											</tr>
									</table>
									</div>		
									<div style="PADDING-BOTTOM: 5px; WIDTH: 100%; PADDING-TOP: 10px">
										<div class="imag">&nbsp;</div>
									<div class="contentDiv" style="width:98.2%; height:151px;#height:137px;">
									<t:dataTable id="dt1" 
										value="#{pages$auctionUnits.unitDataList}"
										binding="#{pages$auctionUnits.dataTable}"
										rows="#{pages$auctionUnits.paginatorRows}" 
										preserveDataModel="false" preserveSort="false" var="dataItem"
										rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
									
										<t:column id="propertyNumberCol" width="100" sortable="true" >
											<f:facet name="header">
												<t:outputText value="#{msg['property.propertyNumberCol']}" />
											</f:facet>
											<t:outputText value="#{dataItem.propertyNumber}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="propertyCommercialNameCol" width="100" sortable="true" >
											<f:facet name="header">
												<t:outputText value="#{msg['property.commercialName']}" />
											</f:facet>
											<t:outputText value="#{dataItem.propertyCommercialName}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="propertyEndowedNameCol" width="100" sortable="true" >
											<f:facet name="header">
												<t:outputText value="#{msg['property.endowedName']}" />
											</f:facet>
											<t:outputText value="#{dataItem.propertyEndowedName}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="landNumberCol" width="80" sortable="true">
											<f:facet name="header">
												<t:outputText value="#{msg['property.landNumber']}" />
											</f:facet>
											<t:outputText styleClass="A_LEFT" value="#{dataItem.landNumber}" />
										</t:column>
										<t:column id="emirateEnCol" width="100" sortable="true" rendered="#{pages$auctionUnits.isEnglishLocale}">
											<f:facet name="header">
												<t:outputText value="#{msg['commons.Emirate']}" />
											</f:facet>
											<t:outputText value="#{dataItem.emirateEn}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="emirateArCol" width="100" sortable="true" rendered="#{pages$auctionUnits.isArabicLocale}">
											<f:facet name="header">
												<t:outputText value="#{msg['commons.Emirate']}" />
											</f:facet>
											<t:outputText value="#{dataItem.emirateAr}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="propertyCategoryEnCol" width="100" sortable="true" rendered="#{pages$auctionUnits.isEnglishLocale}">
											<f:facet name="header">
												<t:outputText value="#{msg['property.category']}" />
											</f:facet>
											<t:outputText value="#{dataItem.propertyCategoryEn}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="propertyCategoryArCol" width="100" sortable="true" rendered="#{pages$auctionUnits.isArabicLocale}">
											<f:facet name="header">
												<t:outputText value="#{msg['property.category']}" />
											</f:facet>
											<t:outputText value="#{dataItem.propertyCategoryEn}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="unitNumberCol" width="100" sortable="true" >
											<f:facet name="header">
												<t:outputText value="#{msg['unit.numberCol']}" />
											</f:facet>
											<t:outputText value="#{dataItem.unitNumber}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="unitTypeEnCol" width="100" sortable="true" rendered="#{pages$auctionUnits.isEnglishLocale}">
											<f:facet name="header">
												<t:outputText value="#{msg['unit.type']}" />
											</f:facet>
											<t:outputText value="#{dataItem.unitTypeEn}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="unitTypeArCol" width="100" sortable="true" rendered="#{pages$auctionUnits.isArabicLocale}">
											<f:facet name="header">
												<t:outputText value="#{msg['unit.type']}" />
											</f:facet>
											<t:outputText value="#{dataItem.unitTypeAr}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="unitUsageEnCol" width="100" sortable="true" rendered="#{pages$auctionUnits.isEnglishLocale}">
											<f:facet name="header">
												<t:outputText value="#{msg['inquiry.unitUsage']}" />
											</f:facet>
											<t:outputText value="#{dataItem.unitUsageTypeEn}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="unitUsageTypeArCol" width="100" sortable="true" rendered="#{pages$auctionUnits.isArabicLocale}">
											<f:facet name="header">
												<t:outputText value="#{msg['inquiry.unitUsage']}" />
											</f:facet>
											<t:outputText value="#{dataItem.unitUsageTypeAr}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="unitInvestmentPurposeEnCol" width="100" sortable="true" rendered="#{pages$auctionUnits.isEnglishLocale}">
											<f:facet name="header">
												<t:outputText value="#{msg['unit.investmentType']}" />
											</f:facet>
											<t:outputText value="#{dataItem.unitInvestmentPurposeEn}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="unitInvestmentPurposeArCol" width="100" sortable="true" rendered="#{pages$auctionUnits.isArabicLocale}">
											<f:facet name="header">
												<t:outputText value="#{msg['unit.investmentType']}" />
											</f:facet>
											<t:outputText value="#{dataItem.unitInvestmentPurposeAr}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="rentValueCol" width="100" sortable="true" >
											<f:facet name="header">
												<t:outputText value="#{msg['unit.rentValueCol']}" />
											</f:facet>
											<t:outputText value="#{dataItem.rentValue}" styleClass="A_RIGHT_NUM"/>
										</t:column>
										<t:column id="openingPriceCol" width="100" sortable="true" >
											<f:facet name="header">
												<t:outputText value="#{msg['auctionUnit.openingPrice']}" />
											</f:facet>
											<t:outputText value="#{dataItem.openingPrice}" styleClass="A_RIGHT_NUM"/>
										</t:column>
										<t:column id="depositAmountCol"  width="100" sortable="true" >
											<f:facet name="header">
												<t:outputText value="#{msg['auctionUnit.depositColumn']}" />
											</f:facet>
											<t:outputText value="#{dataItem.depositAmount}" styleClass="A_RIGHT_NUM"/>
										</t:column>
										<pims:security screen="Pims.AuctionManagement.PrepareAuction.AddAuctionUnits" action="create">
											<t:column id="selectedCol" width="50"sortable="false">
											<f:facet name="header">
												<t:outputText value="#{msg['commons.select']}" />
											</f:facet>
												<h:selectBooleanCheckbox value="#{dataItem.selected}" valueChangeListener="#{pages$auctionUnits.selectAuctionUnit}"></h:selectBooleanCheckbox>
											</t:column>
										</pims:security>
									</t:dataTable>
								</div>
								<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:100%;" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
															<tr>
																<td class="RECORD_NUM_TD">
																	<h:outputText value="#{msg['commons.recordsFound']}"/>
																</td>
																<td class="RECORD_NUM_TD">
																	<h:outputText value=" : "/>
																</td>
																<td class="RECORD_NUM_TD">
																	<h:outputText value="#{pages$auctionUnits.recordSize}"/>
																</td>
															</tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:50%"> 
												<CENTER>	
												<t:dataScroller id="scroller" for="dt1" paginator="true"
													fastStep="1" paginatorMaxPages="#{pages$auctionUnits.paginatorMaxPages}" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 												
													paginatorTableStyle="grid_paginator" layout="singleTable" 
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
													paginatorRenderLinkForActive="false" 
													paginatorActiveColumnStyle="font-weight:bold;" pageIndexVar="pageNumber"
													styleClass="SCH_SCROLLER">
														
													<f:facet name="first">
														
														<t:graphicImage url="../resources/images/first_btn.gif" id="lblF"></t:graphicImage>
													</f:facet>

													<f:facet name="fastrewind">
														<t:graphicImage url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>
													</f:facet>

													<f:facet name="fastforward">
														<t:graphicImage url="../resources/images/next_btn.gif" id="lblFF"></t:graphicImage>
													</f:facet>

													<f:facet name="last">
														<t:graphicImage url="../resources/images/last_btn.gif" id="lblL"></t:graphicImage>
													</f:facet>
														<t:div styleClass="PAGE_NUM_BG">
															<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
														</t:div>
											</t:dataScroller>
											</CENTER>
									</td></tr>
								</table>
									</t:div>
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="BUTTON_TD" colspan="4">
										<pims:security screen="Pims.AuctionManagement.PrepareAuction.AddAuctionUnits" action="create">
											<h:commandButton styleClass="BUTTON" value="#{msg['auctionUnit.addToAuction']}" actionListener="#{pages$auctionUnits.addToAuction}" style="width: 107px" tabindex="7"></h:commandButton>&nbsp;
										</pims:security>
										</td>
									</tr>
								</table>
								</div>
								
								</div>
								
								
								
				</h:form>
				</div>
				</td>
				</tr>
				</table>
				</td>
				</tr>
				</table>
	</body>
</html>
</f:view>