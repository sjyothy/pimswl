


<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
        
<script language="javascript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>

		<script language="javascript" type="text/javascript">
		
		
		function Calendar(control,control2,x,y)
		{
		  var ctl2Name="formTenant:"+control2;
		  
		  var ctl2=document.getElementById(ctl2Name);
		  showCalendar(control,ctl2,'dd/mm/yyyy','','',x,y); 
		  return false;
		}
		
		function clearWindow()
	{
    	    document.getElementById("formFacility:txtName").value="";
	        document.getElementById("formFacility:txtDescription").value="";
	        
	        var cmbSelectFacilityType=document.getElementById("formFacility:selectFacilityType");
		     cmbSelectFacilityType.selectedIndex = 0;
		    
		    document.getElementById("formFacility:txtmiddleName").value="";
		    
		    		    }
		    
		    function submitForm(){
       
		     //alert("In submitForm");    
		    var cmbCountry=document.getElementById("formTenant:selectcountry");
		    var i = 0;
		    i=cmbCountry.selectedIndex;
		    //alert("value of i"+i);
		    document.getElementById("formTenant:hdnStateValue").value=cmbCountry.options[i].value ;
		     
		    
		    document.getElementById("formTenant:hdnCityValue").value= -1 ;
		     
		    document.forms[0].submit();
		    
		    //alert("End submitForm");
		    
		    }
		    
		    function submitFormState(){
       
		    // alert("In submitFormState");    
		    var cmbState=document.getElementById("formTenant:selectstate");
		    var j = 0;
		    j=cmbState.selectedIndex;
		   // alert("value of j"+j);
		    document.getElementById("formTenant:hdnCityValue").value=cmbState.options[j].value ;
		     
		    document.forms[0].submit();
		   // alert("End submitFormState");
		    
		    }
		    
	    function showHide(){
	    
	     //alert("In ShowHide");
	     var cmbSelectType=document.getElementById("formFacility:selectFacilityType");
	     var txtRent=document.getElementById("formFacility:txtrentValue");
	    // alert(cmbSelectType.value);
       if (cmbSelectType.value=="FACILITY_TYPE_PAID"){
        //alert("in:::::::if");
        txtRent.disabled=false;}
      else{ 
      txtRent.disabled=true;}
       //alert("in:::::::else");}
      // alert("In ShowHide2");
  }
		    
		
    </script>
</head>
	<body dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">
     <div class="containerDiv">
    
    <!-- Header --> 
<table width="100%" cellpadding="0" cellspacing="0"  border="0">
    <tr>
    <td colspan="2">
        <jsp:include page="header.jsp"/>
    </td>
    </tr>

<tr width="100%">
    <td class="divLeftMenuWithBody" width="17%">
        <jsp:include page="leftmenu.jsp"/>
    </td>
    <td width="83%" valign="top" class="divBackgroundBody">
        <table  class="greyPanelTable" cellpadding="0" cellspacing="0"  border="0">
         <tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['facility.facilityInformation']}" styleClass="HEADER_FONT"/>
						</td>
						<td width="100%">
							&nbsp;
						</td>
					</tr>
        </table>
    
        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
          <tr valign="top">
             <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
             </td> 
	
  <td width="100%" valign="top" nowrap="nowrap">
			
			<h:form id="formFacility">
			<div class="SCROLLABLE_SECTION" >
			                         <div style= "height:25px;">
											<table border="0" class="layoutTable">
												 	
												<tr>
      									    	      <td colspan="6">
														 <h:outputText value="#{pages$AddFacility.errorMessages}" escape="false" styleClass="ERROR_FONT" style="padding:10px;"/>
											            <h:outputText value="#{pages$AddFacility.successMessages}" escape="false" styleClass="INFO_FONT" style="padding:10px;"/>
														<h:inputHidden id="hdnTenantId" value="#{pages$AddFacility.facilityId}"/>
														<h:inputHidden id="hdnpageMode" value="#{pages$AddFacility.pageMode}"/>
														
														
													  </td>
												</tr>
											</table>
										</div>		
								<div class="MARGIN">
		       						<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
				               <div class="DETAIL_SECTION" style="width:99.8%;">
			                    <h:outputLabel value="#{msg['facility.facilityDetail']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>	
									<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
									<tr>
									<td width="97%">
									<table width="100%">
											<tr>
													<td width="25%">
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['facility.facilityName']}:" styleClass="LABEL"></h:outputLabel>
													</td>
													<td  width="25%">
															<h:inputText maxlength="50" id="txtName" value="#{pages$AddFacility.facilityName}"/>
														
													</td>
													  <td width="25%">
														<h:outputLabel value="#{msg['facility.facilityType']}:" styleClass="LABEL"></h:outputLabel>
													</td>
													<td  width="25%">
															
													    <h:selectOneMenu onchange="showHide();" id="selectFacilityType"  value="#{pages$AddFacility.selectOneFacilityType}" >
															<f:selectItems value="#{pages$ApplicationBean.facilityType}" />
														</h:selectOneMenu>
													
													</td>
													
													
												</tr>
													
												<tr>
												     
													<td>
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel value="#{msg['facility.description']}:"></h:outputLabel>
													</td>
													<td >
															
													<t:inputTextarea  styleClass="TEXTAREA"  value="#{pages$AddFacility.facilityDescription}"/>
														
													</td>
													
													
													<td>
														<h:outputLabel value="#{msg['facility.rentValue']}:" styleClass="LABEL"></h:outputLabel>
													</td>
													<td  >
												        <h:inputText id="txtrentValue" maxlength="20" value="#{pages$AddFacility.rentValue}"  styleClass="A_RIGHT_NUM" >
												        
												        </h:inputText>
												       
													</td>
												</tr>
                                            
                                    <tr>
										<td colspan="10" class="BUTTON_TD" >
                                                   <pims:security screen="Pims.PropertyManagement.Facility.AddFacility" action="create">     
                                      				   <h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" type= "submit"   action="#{pages$AddFacility.btnAdd_Click}" >  </h:commandButton>
                                      				</pims:security>   
												       <h:commandButton styleClass="BUTTON" type="button" onclick="javascript:clearWindow();" value="#{msg['commons.clear']}" >  </h:commandButton>
												       <h:commandButton styleClass="BUTTON" type= "submit" value="#{msg['commons.cancel']}" action="#{pages$AddFacility.btnBack_Click}" >	</h:commandButton>
												       
                                		        </td>
                                		        </tr>
                                		</table>
                                		</td>
										<td width="3%">
											&nbsp;
										</td>
                                		
                                		</tr>        
                                            </table>
                                            </div>
                                           </div>                                           

 										
				</div>								                         
			</h:form>
			
			</td>
			</tr>
			</table>
			</td>
			</tr>
			<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
			</body>
		</f:view>
	
</html>
