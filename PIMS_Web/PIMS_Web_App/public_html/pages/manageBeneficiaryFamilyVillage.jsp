<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++)
		{
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		       )
		    {
		        inputs[i].disabled = true;
		    }
		}
	}
	
	function onMessageFromSearchDonationBox()
	{
	  
	  disableInputs();
	  document.getElementById("detailsFrm:onMessageFromSearchDonationBox").onclick();
	}
	
	

	
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<c:choose>
						<c:when
							test="${!pages$manageBeneficiaryFamilyVillage.viewModePopUp}">
							<tr
								style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>


					<tr width="100%">
						<c:choose>
							<c:when
								test="${!pages$manageBeneficiaryFamilyVillage.viewModePopUp}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['manageBeneficiary.header']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>
																<h:outputText id="errorId"
																	value="#{pages$manageBeneficiaryFamilyVillage.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$manageBeneficiaryFamilyVillage.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$manageBeneficiaryFamilyVillage.pageMode}"></h:inputHidden>
																<h:commandLink id="onMessageFromSearchEndowments"
																	action="#{pages$manageBeneficiaryFamilyVillage.onMessageFromSearchEndowments}" />


															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<t:div rendered="true" style="width:100%;">
														<t:panelGrid cellpadding="1px" width="100%"
															cellspacing="5px" columns="4">

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.Name']}:" />
															<t:panelGroup>
																<h:inputText id="txtName" readonly="true"
																	value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.personFullName}"
																	styleClass="READONLY" />
																<%-- 
																<t:commandLink
																	action="#{pages$manageBeneficiaryFamilyVillage.onOpenResearcherForm}">
																	<h:graphicImage id="researchForm"
																		title="#{msg['beneficiaryDetails.tooltip.openResearchForm']}"
																		style="cursor:hand;margin-left:2px;margin-right:2px"
																		url="../resources/images/detail-icon.gif" />&nbsp;
				</t:commandLink>
				--%>
															</t:panelGroup>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.gender']}:"></h:outputLabel>

															<h:selectOneMenu id="selectGender"
																value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.gender}">
																<f:selectItem itemLabel="#{msg['tenants.gender.male']}"
																	itemValue="M" />
																<f:selectItem
																	itemLabel="#{msg['tenants.gender.female']}"
																	itemValue="F" />
															</h:selectOneMenu>


															<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.dateOfBirth']}:" />
															<rich:calendar id="DateOfBirth"
																value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.dateOfBirth}"
																popup="true"
																datePattern="#{pages$manageBeneficiaryFamilyVillage.dateFormat}"
																showApplyButton="false"
																locale="#{pages$manageBeneficiaryFamilyVillage.locale}"
																enableManualInput="false"
																inputStyle="width: 170px; height: 14px" />

															<h:outputLabel id="age" styleClass="LABEL"
																value="#{msg['familyVillageManageBeneficiary.lbl.age']}:"></h:outputLabel>

															<h:inputText readonly="true" id="ageTextIT"
																styleClass="READONLY"
																value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.age}" />

<h:outputLabel styleClass="LABEL"
																value="#{msg['familyVillageBeneficiary.lbl.lineage']}:"></h:outputLabel>
															<h:selectOneMenu id="lineage" immediate="false"
																style="WIDTH: 157px"
																value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.beneficiaryLineage}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$ApplicationBean.beneficiaryLineage}" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.nationality']}:"></h:outputLabel>
															<h:selectOneMenu id="nationality" immediate="false"
																style="WIDTH: 157px"
																value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.nationalityIdString}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$ApplicationBean.countryListForFamilyVillageBeneficiary}" />
															</h:selectOneMenu>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.socialSecNumber']}:"></h:outputLabel>

															<h:inputText maxlength="15" id="txtsocialSecNumber"
																value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.socialSecNumber}"></h:inputText>

															<h:outputText
																value="#{msg['mems.inheritanceFile.fieldLabel.passportType']}" />
															<h:selectOneMenu id="isMasroom"
																binding="#{pages$manageBeneficiaryFamilyVillage.cmbPassportType}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$ApplicationBean.passportTypeList}" />
																<a4j:support event="onchange"
																	reRender="passportNumberId,masroomNumberId,passportNumberIdLbl,masroomNumberIdLbl"
																	action="#{pages$manageBeneficiaryFamilyVillage.onPassportTypeChanged}" />
															</h:selectOneMenu>
															<h:panelGroup id="passportNumberIdLbl">
																<h:outputText
																	value="#{msg['mems.inheritanceFile.fieldLabel.passportNumber']}" />
															</h:panelGroup>
															<h:panelGroup id="passportNumberId">
																<h:inputText
																	disabled="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.masroom}"
																	value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.passportNumber}" />
															</h:panelGroup>

															<h:panelGroup id="masroomNumberId">
																<h:outputText
																	value="#{msg['mems.inheritanceFile.fieldLabel.masroomNumber']}" />
															</h:panelGroup>

															<h:panelGroup id="masroomNumberIdLbl">
																<h:inputText
																	disabled="#{!pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.masroom}"
																	value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.masroomNumber}" />
															</h:panelGroup>




															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" />
																<h:outputLabel styleClass="LABEL"
																	id="DateOfBirthJOININGlbl"
																	value="#{msg['familyVillageManageBeneficiary.lbl.joiningDate']}:" />
															</h:panelGroup>
															<rich:calendar id="DateOfBirthJoining" disabled="true"
																value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.dateOfJoining}"
																popup="true"
																datePattern="#{pages$manageBeneficiaryFamilyVillage.dateFormat}"
																showApplyButton="false"
																locale="#{pages$manageBeneficiaryFamilyVillage.locale}"
																enableManualInput="false"
																inputStyle="width: 170px; height: 14px">
															</rich:calendar>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['lbl.requestReason']}:"></h:outputLabel>
															<div class="SCROLLABLE_SECTION"
																style="height: 100px; width: 298px; overflow-x: hidden; border-width: 1px; border-style: solid; border-color: #a2a2a2;">
																<h:panelGroup
																	style="border-width: 1px; border-style: solid; border-color: #a2a2a2; background: white; overflow-x: auto; overflow-y: scroll; width: 90%; height: 60px; ">

																	<h:selectManyCheckbox layout="pageDirection"
																		id="housingRequestReasons"
																		value="#{pages$manageBeneficiaryFamilyVillage.housingRequestReasons}">
																		<f:selectItems
																			value="#{pages$ApplicationBean.housingRequestReason}" />
																	</h:selectManyCheckbox>
																</h:panelGroup>
															</div>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['lbl.requestReasonDesc']}:"></h:outputLabel>
															<t:panelGroup colspan="6">
																<h:inputTextarea rows="2" style="width: 90%;"
																	id="housingReasonDescription"
																	value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.housingReasonDescription}" />
															</t:panelGroup>

															<h:outputLabel value="#{msg['educationAspects.grade']} :"
																style="font-weight:normal;" styleClass="TABLE_LABEL" />
															<h:selectOneMenu id="grade"
																value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.educationAspects.gradeId}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
																<f:selectItems
																	value="#{pages$ApplicationBean.allGrades}" />
															</h:selectOneMenu>

															<h:outputLabel
																value="#{msg['educationAspects.gradeDesc']} :"
																style="font-weight:normal;" styleClass="TABLE_LABEL" />
															<h:inputTextarea id="gradeDesc"
																onkeyup="javaScript:removeExtraCharacter(this,50)"
																onkeypress="javaScript:removeExtraCharacter(this,50)"
																onkeydown="javaScript:removeExtraCharacter(this,50)"
																onblur="javaScript:removeExtraCharacter(this,50)"
																onchange="javaScript:removeExtraCharacter(this,50)"
																value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.educationAspects.gradeDesc}" />

															<h:outputLabel value="#{msg['educationAspects.level']} :"
																style="font-weight:normal;" styleClass="TABLE_LABEL" />
															<h:selectOneMenu id="edulevel"
																value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.educationAspects.educationLevelId}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
																<f:selectItems
																	value="#{pages$ApplicationBean.allEducationLevel}" />
															</h:selectOneMenu>

															<h:outputLabel
																value="#{msg['educationAspects.educationLevelDesc']} :"
																style="font-weight:normal;" styleClass="TABLE_LABEL" />
															<h:inputTextarea id="eduLevelDescription"
																onkeyup="javaScript:removeExtraCharacter(this,50)"
																onkeypress="javaScript:removeExtraCharacter(this,50)"
																onkeydown="javaScript:removeExtraCharacter(this,50)"
																onblur="javaScript:removeExtraCharacter(this,50)"
																onchange="javaScript:removeExtraCharacter(this,50)"
																value="#{pages$manageBeneficiaryFamilyVillage.beneficiaryPerson.educationAspects.educationLevelDesc}" />



														</t:panelGrid>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">
															<pims:security screen="Pims.EndowMgmt.EndowmentFile.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$manageBeneficiaryFamilyVillage.onSave}" />
															</pims:security>
														</t:div>
														<br />
													</t:div>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$manageBeneficiaryFamilyVillage.tabPanel}"
																style="width: 100%">

																<!-- File Tab - Start -->
																<rich:tab id="tabVilla"
																	label="#{msg['familyVillageManageBeneficiary.lbl.villa']}"
																	action="#{pages$manageBeneficiaryFamilyVillage.onVillaTab}">
																	<%@ include file="tabFamilyVillageBeneficiaryVilla.jsp"%>
																</rich:tab>
																<!-- File Tab - Finish -->
																<!-- Characteristics Tab - Start -->
																<rich:tab id="tabCharacteristics"
																	label="#{msg['commons.Characteristics']}"
																	action="#{pages$manageBeneficiaryFamilyVillage.onCharacteristicsTab}">
																	<%@ include
																		file="tabFamilyVillageBeneficiaryCharacteristics.jsp"%>
																</rich:tab>
																<!-- CharacteristicsTab - Finish -->
																<!-- Family Tab - Start -->
																<rich:tab id="tabFamilyData"
																	label="#{msg['familyVillageManageBeneficiary.lbl.familyDetails']}"
																	action="#{pages$manageBeneficiaryFamilyVillage.onFamilyDataTab}">
																	<%@ include file="tabFamilyVillageFamilyData.jsp"%>
																</rich:tab>
																<!-- Family Tab - Finish -->
																<!-- Recommendations Tab - Start -->
																<rich:tab id="tabRecommendation"
																	label="#{msg['socialProgram.lbl.Recommendations']}"
																	action="#{pages$manageBeneficiaryFamilyVillage.onRecommendationsTab}">
																	<%@ include
																		file="tabFamilyVillageBeneficiaryRecommendation.jsp"%>
																</rich:tab>
																<!-- Recommendations Tab - Finish -->
																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>



																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>


																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$manageBeneficiaryFamilyVillage.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>

													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<c:choose>
								<c:when
									test="${!pages$manageBeneficiaryFamilyVillage.viewModePopUp}">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
								</c:when>
							</c:choose>


						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>