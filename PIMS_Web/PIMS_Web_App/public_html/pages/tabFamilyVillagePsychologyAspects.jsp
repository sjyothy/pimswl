
<t:div id="tabPsychological" style="width:100%;">

	<t:panelGrid id="tabPsychologicalGrid" cellpadding="3px" width="100%"
		cellspacing="2px" columns="1" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.PsychologyAspect.psychologicalCondition']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">

		<h:commandButton id="btnAddPsychological" styleClass="BUTTON"
			type="button" value="#{msg['commons.Add']}"
			onclick="javaScript:openFamilyVillagePsychologyConditionPopup('#{pages$tabFamilyVillagePsychologyAspects.personId}');">


		</h:commandButton>

	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTablePsychologicalCondition" rows="5"
			width="100%"
			value="#{pages$tabFamilyVillagePsychologyAspects.dataListPsychologicalCondition}"
			binding="#{pages$tabFamilyVillagePsychologyAspects.dataTablePsychologicalCondition}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="psychologyAspectsCreatedBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="psychologyAspectsCreatedByOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillagePsychologyAspects.englishLocale? 
																				dataItem.createdByEn:
																				dataItem.createdByAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="psychologyAspectsCreatedOn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillagePsychologyAspects.dateFormat}"
						timeZone="#{pages$tabFamilyVillagePsychologyAspects.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="psychologyAspectsCategory" sortable="true">
				<f:facet name="header">
					<t:outputText id="psychologyAspectsCategoryOT"
						value="#{msg['cancelContract.inspection.grid.category']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillagePsychologyAspects.englishLocale? 
																				dataItem.categoryTypeEn:
																				dataItem.categoryTypeAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>

			<t:column id="psychologyAspectsEvaluation" sortable="true">
				<f:facet name="header">
					<t:outputText id="psychologyAspectsEvaluationOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.PsychologyAspect.EvaluationType']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillagePsychologyAspects.englishLocale? 
																				dataItem.evaluationTypeEn:
																				dataItem.evaluationTypeAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>

			<t:column id="psychologyAspectsConditionDetails" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.description']}" />
				</f:facet>
				<h:outputText value="#{dataItem.description}">

				</h:outputText>
			</t:column>


		</t:dataTable>
	</t:div>
	<t:div id="psychologyAspectsDivScroller" styleClass="contentDivFooter"
		style="width:99.1%">


		<t:panelGrid id="psychologyAspectsRecNumTable" columns="2"
			cellpadding="0" cellspacing="0" width="100%"
			columnClasses="RECORD_NUM_TD,BUTTON_TD">
			<t:div id="psychologyAspectsConditionRecNumDiv" styleClass="RECORD_NUM_BG">
				<t:panelGrid id="psychologyAspectsRecNumTbl" columns="3"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
					cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
					<h:outputText value="#{msg['commons.recordsFound']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$tabFamilyVillagePsychologyAspects.recordSizePsychologicalCondition}" />
				</t:panelGrid>

			</t:div>

			<CENTER>
				<t:dataScroller id="psychologyAspectsScroller"
					for="dataTablePsychologicalCondition" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$tabFamilyVillagePsychologyAspects.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="psychologyAspectsConditionlblFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="psychologyAspectsConditionlblFRRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="psychologyAspectsConditionlblFFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="psychologyAspectsConditionlblLRequestHistory"></t:graphicImage>
					</f:facet>

					<t:div id="psychologyAspectsConditionPageNumDiv" styleClass="PAGE_NUM_BG">
						<t:panelGrid id="psychologyAspectsConditionPageNumTable"
							styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
							cellspacing="0">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:panelGrid>

					</t:div>
				</t:dataScroller>
			</CENTER>

		</t:panelGrid>
	</t:div>

	<t:panelGrid id="tabPsychologicalTestsGrid" cellpadding="3px"
		width="100%" cellspacing="2px" columns="1"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.PsychologyAspect.psychologicalTest']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">

		<h:commandButton id="btnAddPsychologicalTest" styleClass="BUTTON"
			type="button" value="#{msg['commons.Add']}"
			onclick="javaScript:openFamilyVillagePsychologyTestsPopup('#{pages$tabFamilyVillagePsychologyAspects.personId}');">
		</h:commandButton>

	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTablePsychologicalTests" rows="5" width="100%"
			value="#{pages$tabFamilyVillagePsychologyAspects.dataListPsychologicalTests}"
			binding="#{pages$tabFamilyVillagePsychologyAspects.dataTablePsychologicalTests}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="psychologyAspectsTestCreatedBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="psychologyAspectsTestCreatedByOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillagePsychologyAspects.englishLocale? 
																				dataItem.createdByEn:
																				dataItem.createdByAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="psychologyAspectsCreatedOn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillagePsychologyAspects.dateFormat}"
						timeZone="#{pages$tabFamilyVillagePsychologyAspects.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="psychologyAspectsTestType" sortable="true">
				<f:facet name="header">
					<t:outputText id="psychologyAspectsTestTypeOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.PsychologyAspect.TestType']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillagePsychologyAspects.englishLocale? 
																				dataItem.testTypeEn:
																				dataItem.testTypeAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>

			<t:column id="psychologyAspectstestDate" sortable="true">
				<f:facet name="header">
					<h:outputText
						value="#{msg['researchFamilyVillageBeneficiary.lbl.PsychologyAspect.testDate']}" />
				</f:facet>
				<h:outputText value="#{dataItem.testDate}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillagePsychologyAspects.dateFormat}"
						timeZone="#{pages$tabFamilyVillagePsychologyAspects.timeZone}" />
				</h:outputText>
			</t:column>

			<t:column id="psychologyAspectsTestResult" sortable="true">
				<f:facet name="header">
					<h:outputText
						value="#{msg['researchFamilyVillageBeneficiary.lbl.PsychologyAspect.TestResult']}" />
				</f:facet>
				<h:outputText value="#{dataItem.testResult}">

				</h:outputText>
			</t:column>
			<t:column id="psychologyAspectsDiagnosis" sortable="true">
				<f:facet name="header">
					<h:outputText
						value="#{msg['researchFamilyVillageBeneficiary.lbl.PsychologyAspect.Diagnosis']}" />
				</f:facet>
				<h:outputText value="#{dataItem.diagnosis}">

				</h:outputText>
			</t:column>


		</t:dataTable>
	</t:div>
	<t:div id="psychologyAspectsTestDivScroller"
		styleClass="contentDivFooter" style="width:99.1%">


		<t:panelGrid id="psychologyAspectsTestRecNumTable" columns="2"
			cellpadding="0" cellspacing="0" width="100%"
			columnClasses="RECORD_NUM_TD,BUTTON_TD">
			<t:div id="psychologyAspectsRecNumDiv" styleClass="RECORD_NUM_BG">
				<t:panelGrid id="psychologyAspectsTestRecNumTbl" columns="3"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
					cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
					<h:outputText value="#{msg['commons.recordsFound']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$tabFamilyVillagePsychologyAspects.recordSizePsychologicalTests}" />
				</t:panelGrid>

			</t:div>

			<CENTER>
				<t:dataScroller id="psychologyAspectsTestScroller"
					for="dataTablePsychologicalTests" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$tabFamilyVillagePsychologyAspects.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="psychologyAspectsTestlblFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="psychologyAspectsTestlblFRRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="psychologyAspectsTestlblFFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="psychologyAspectsTestlblLRequestHistory"></t:graphicImage>
					</f:facet>

					<t:div id="psychologyAspectsPageNumDiv" styleClass="PAGE_NUM_BG">
						<t:panelGrid id="psychologyAspectsPageNumTable"
							styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
							cellspacing="0">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:panelGrid>

					</t:div>
				</t:dataScroller>
			</CENTER>

		</t:panelGrid>
	</t:div>


</t:div>


