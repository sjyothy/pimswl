<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	function sendDataToParent()
	{
		window.opener.onMessageFromEvaluateEndowmentPopup();
	  	window.close();	  
	}	
	function closeWindow()
	{
		 window.close();
	}
	        
	function disableButtons(control)
	{
			
			var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
			
			control.nextSibling.nextSibling.onclick();
			
			return 
		
	}
	
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">

				<tr width="100%">
					<td width="100%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel
										value="#{msg['endowmentFileAsso.lbl.evaluateEndowment']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="frm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION" style="width:95%;">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
														<h:messages></h:messages>
															<h:outputText
																value="#{pages$evaluateEndowmentsPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$evaluateEndowmentsPopup.successMessages}"
																escape="false" styleClass="INFO_FONT" />
														</td>
													</tr>
												</table>
											</div>

											<div class="POPUP_DETAIL_SECTION" style="width: 100%;">


												<table width="100%">
													<tr>
														<td width="7%">
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowment.lbl.num']}" />
														</td>
														<td width="25%">
															<h:inputText
																value="#{pages$evaluateEndowmentsPopup.endowmentFileAsso.endowment.endowmentNum}"
																readonly="true" styleClass="READONLY">

															</h:inputText>
														</td>
														<td width="7%">
															<h:outputLabel styleClass="LABEL"
																value="#{msg['settlement.label.costCenter']}" />
														</td>
														<td width="25%">
															<h:inputText
																value="#{pages$evaluateEndowmentsPopup.endowmentFileAsso.endowment.costCenter}"
																readonly="true" styleClass="READONLY">
															</h:inputText>
														</td>
													</tr>
													<tr>
														<td >
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowment.lbl.type']}" />
														</td>
														<td >
															<h:selectOneMenu disabled="true"
																value="#{pages$evaluateEndowmentsPopup.endowmentFileAsso.endowment.assetType.assetTypeId}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.assetTypesList}" />

															</h:selectOneMenu>
														</td>
														<td >
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowment.lbl.name']}" />
														</td>
														<td width="25%">
															<h:inputText readonly="true" styleClass="READONLY"
																value="#{pages$evaluateEndowmentsPopup.endowmentFileAsso.endowment.endowmentName}" />
														</td>

													</tr>

													<tr>
														<td >
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentFileAsso.lbl.expRevenue']}" />
														</td>
														<td >
															<h:inputText
																styleClass="#{!pages$evaluateEndowmentsPopup.isPageModeUpdatable?'READONLY':''}"
																readonly="#{!pages$evaluateEndowmentsPopup.isPageModeUpdatable}"
																value="#{pages$evaluateEndowmentsPopup.endowmentFileAsso.expectedRevenueString}"
																onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)">

															</h:inputText>
														</td>
														<td >
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentFileAsso.lbl.generatesRevenue']}" />
														</td>
														<td >
															<h:selectOneMenu
																readonly="#{!pages$evaluateEndowmentsPopup.isPageModeUpdatable}"
																value="#{pages$evaluateEndowmentsPopup.endowmentFileAsso.expRevFreqString}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.paymentPeriodList}" />
															</h:selectOneMenu>
														</td>
													</tr>
													<tr>
														<td >
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentfile.lbl.marketValue']}" />
														</td>
														<td >
															<h:inputText
																styleClass="#{!pages$evaluateEndowmentsPopup.isPageModeUpdatable?'READONLY':''}"
																readonly="#{!pages$evaluateEndowmentsPopup.isPageModeUpdatable}"
																value="#{pages$evaluateEndowmentsPopup.endowmentFileAsso.endowment.marketValStr}"
																onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)">

															</h:inputText>
														</td>
														<td></td>
														</tr>
													<tr>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.comments']}" />
														</td>
														<td colspan="3">
															<h:inputTextarea
																styleClass="#{!pages$evaluateEndowmentsPopup.isPageModeUpdatable?'READONLY':''}"
																readonly="#{!pages$evaluateEndowmentsPopup.isPageModeUpdatable}"
																value="#{pages$evaluateEndowmentsPopup.endowmentFileAsso.evalComments}"
																style="width: 580px;" />
														</td>
													</tr>
												</table>

											</div>


											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<td height="5px"></td>
												</tr>
												<tr>
													<td class="BUTTON_TD MARGIN" colspan="6">
														<h:commandButton id="idSaveBtn"
															rendered="#{pages$evaluateEndowmentsPopup.isPageModeUpdatable}"
															value="#{msg['commons.saveButton']}" styleClass="BUTTON"
															action="#{pages$evaluateEndowmentsPopup.onDone}">
														</h:commandButton>
														<h:commandButton value="#{msg['commons.closeButton']}"
															styleClass="BUTTON" onclick="closeWindow();"
															type="button" style="width:10%;"></h:commandButton>
													</td>
												</tr>
											</table>



										</div>
										</div>
									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>
		</body>
	</html>
</f:view>