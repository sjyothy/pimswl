<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Creates an Auction Advertisement for an Auction
  --%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
	<% System.out.println("pages$auctionAdvertisement.jsp just after <head>");%>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="Cache-Control"
			content="no-cache,must-revalidate,no-store">
		<meta http-equiv="expires" content="0">

		<link rel="stylesheet" type="text/css"
			href="../../resources/css/simple_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../../resources/css/amaf_en.css" />
		<title>PIMS</title>
		<script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>
		<script language="javascript" type="text/javascript"></script>
	</head>
		<f:view>
			<%
				UIViewRoot view = (UIViewRoot) session.getAttribute("view");
				if (view.getLocale().toString().equals("en")) {
			%>
				<body dir="ltr">
				<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg" />
			<%
				}
				else{
			%>
				<body dir="rtl">
				<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages_ar" var="msg" />
			<%
				}
			%>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr>
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<%
									
										if (view.getLocale().toString().equals("en")) {
										
									%>
		 							<td
										background="../resources/images/Grey panel/Grey-Panel-left-1.jpg"
										height="38" style="background-repeat: no-repeat;" width="100%">
										<font style="font-family: Trebuchet MS; font-weight: regular; font-size: 19px; margin-left: 15px; top: 30px;">
											<h:outputLabel value="#{msg['advertisement.heading']}"></h:outputLabel></font>
										<!--<IMG src="../resources/images/Grey panel/Grey-Panel-left-1.jpg"/>-->
										<%
										}
										else{
										%>
									</td>
 								    <td width="100%">
										<IMG
											src="../resources/images/ar/Detail/Grey Panel/Grey-Panel-right-1.jpg"></img>
										<%
											}
										%>
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap" 	background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
									</td>
									<td width="500px" height="100%" valign="top" nowrap="nowrap">
										<h:form>
											<table width="24">
												<tr>
													<td colspan="24">
													<% System.out.println("pages$auctionAdvertisement.jsp 1111111111111111111111111111111");%>
														<h:messages><h:outputText value="#{pages$auctionAdvertisement.message}"  escape="false"/></h:messages>
													</td>
												</tr>
												<tr>
												</tr>
												<tr>
												  	<td colspan="3">
												    	<h:outputLabel value="#{msg['auction.number']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
													<% System.out.println("pages$auctionAdvertisement.jsp 222222222222222222222222222222");%>
													<h:inputText value="#{pages$auctionAdvertisement.auctionNumber}" tabindex="1" style="width: 186px; height: 16px"></h:inputText>
													</td>
													<td>
														&nbsp;
													</td>
													
												</tr>
												<tr>
												  	<td colspan="3">
												    	<h:outputLabel value="#{msg['advertisement.publicationDate']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
												        <h:inputText  value="#{pages$auctionAdvertisement.requestStartDate}" tabindex="3" style="width: 186px; height: 16px"></h:inputText>
													</td>
													<td>
														&nbsp;
													</td>
													<td colspan="5">
												    	<h:outputLabel value="#{msg['property.name']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
												        <h:inputText  value="#{pages$auctionAdvertisement.auctionStartDatetime}" tabindex="4" style="width: 186px; height: 16px"></h:inputText>
													</td>
												</tr>
												<tr>
													<td colspan="3">
												    	<h:outputLabel value="#{msg['property.number']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
												        <h:inputText value="#{pages$auctionAdvertisement.requestEndDate}" tabindex="5" style="width: 186px; height: 16px"></h:inputText>
													</td>
													<td>
														&nbsp;
													</td>
													<td colspan="5">
												    	<h:outputLabel value="#{msg['property.type']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
												        <h:inputText  value="#{pages$auctionAdvertisement.auctionEndDatetime}" tabindex="6" style="width: 186px; height: 16px"></h:inputText>
													</td>
													
											</tr>
												<tr>
													<td colspan="3">
												    	<h:outputLabel value="#{msg['auction.openingPrice']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
												        <h:inputText value="#{pages$auctionAdvertisement.requestEndDate}" tabindex="8" style="width: 186px; height: 16px"></h:inputText>
													</td>
													<td>
														&nbsp;
													</td>
													<td colspan="5">
												    	<h:outputLabel value="#{msg['auction.depositAmount']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
												        <h:inputText  value="#{pages$auctionAdvertisement.auctionEndDatetime}" tabindex="6" style="width: 186px; height: 16px"></h:inputText>
													</td>
													
											</tr>
											<tr>
													<td colspan="3">
												    	<h:outputLabel value="#{msg['property.landNumber']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
												        <h:inputText value="#{pages$auctionAdvertisement.requestEndDate}" tabindex="9" style="width: 186px; height: 16px"></h:inputText>
													</td>
													<td>
														&nbsp;
													</td>
													<td colspan="5">
												    	<h:outputLabel value="#{msg['property.area']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
												        <h:inputText  value="#{pages$auctionAdvertisement.auctionEndDatetime}" tabindex="10" style="width: 186px; height: 16px"></h:inputText>
													</td>
													
											</tr>
											<tr>
													<td colspan="3">
												    	<h:outputLabel value="#{msg['contact.street']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
												        <h:inputText value="#{pages$auctionAdvertisement.requestEndDate}" tabindex="11" style="width: 186px; height: 16px"></h:inputText>
													</td>
													<td>
														&nbsp;
													</td>
													<td colspan="5">
												    	<h:outputLabel value="#{msg['contact.city']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
												        <h:inputText  value="#{pages$auctionAdvertisement.auctionEndDatetime}" tabindex="12" style="width: 186px; height: 16px"></h:inputText>
													</td>
													
											</tr>
											<tr>
													<td colspan="3">
												    	<h:outputLabel value="#{msg['contact.postcode']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
												        <h:inputText value="#{pages$auctionAdvertisement.requestEndDate}" tabindex="13" style="width: 186px; height: 16px"></h:inputText>
													</td>
													<td>
														&nbsp;
													</td>
													<td colspan="5">
												    	<h:outputLabel value="#{msg['contact.state']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
												        <h:inputText  value="#{pages$auctionAdvertisement.auctionEndDatetime}" tabindex="14" style="width: 186px; height: 16px"></h:inputText>
													</td>
													
											</tr>
											<tr>
													<td colspan="3">
												    	<h:outputLabel value="#{msg['contact.country']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
												        <h:inputText value="#{pages$auctionAdvertisement.requestEndDate}" tabindex="15" style="width: 186px; height: 16px"></h:inputText>
													</td>
													<td>
														&nbsp;
													</td>
													<td colspan="5">
												    	<h:outputLabel value="#{msg['contact.officephone']}:"></h:outputLabel>
												  	</td>
													<td colspan="8" >
												        <h:inputText  value="#{pages$auctionAdvertisement.auctionEndDatetime}" tabindex="14" style="width: 186px; height: 16px"></h:inputText>
													</td>
													
											</tr>
											<tr>
													<td colspan="3">
												    	<h:outputLabel value="#{msg['contact.address1']}:"></h:outputLabel>
												  	</td>
													<td colspan="16" >
												        <h:inputText value="#{pages$auctionAdvertisement.requestEndDate}" tabindex="16" style="width: 412px; height: 16px"></h:inputText>
													</td>
													
													
											</tr>
											<tr>
													<td colspan="3">
												    	<h:outputLabel value="#{msg['contact.address2']}:"></h:outputLabel>
												  	</td>
													<td colspan="16" >
												        <h:inputText value="#{pages$auctionAdvertisement.requestEndDate}" tabindex="17" style="width: 412px; height: 16px"></h:inputText>
													</td>
													
													
											</tr>
											<tr>
													<td colspan="3">
												    	<h:outputLabel value="#{msg['commons.note']}:"></h:outputLabel>
												  	</td>
													<td colspan="16" >
													<h:inputTextarea style="width: 273px; height: 80px"></h:inputTextarea>
													</td>
											</tr>
											
												<tr>
													<td colspan="2" style="width: 78px">
														<h:commandButton type="submit" value="#{msg['commons.saveButton']}"
															action="#{pages$auctionAdvertisement.saveAuction}"
															style="width: 75px" tabindex="7"/>
													</td>
													<td colspan="2">
														<h:commandButton type="submit" value="#{msg['commons.cancel']}"
															action="#{pages$auctionAdvertisement.cancel}"
															style="width: 75px" tabindex="8"/>
													</td>
													<td colspan="2">
														<h:commandButton type="submit" value="#{msg['commons.complete']}"
															action="#{pages$auctionAdvertisement.proceed}"
															style="width: 75px" tabindex="9"/>
													</td>
													
													
												</tr>
											</table>
										</h:form>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</body>
		</f:view>
</html>
