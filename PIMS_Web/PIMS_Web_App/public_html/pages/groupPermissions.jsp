

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/><meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="Cache-Control"
			content="no-cache,must-revalidate,no-store">
		<meta http-equiv="expires" content="0">

		<link rel="stylesheet" type="text/css"
			href="../resources/css/simple_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../resources/css/amaf_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../resources/css/table.css" />
		<title>PIMS</title>

		<script language="javascript" type="text/javascript">
  
       
function ads(){

var objSourceElement =document.getElementById('_id0:a1');
var objTargetElement = document.getElementById('_id0:a2');
  for (var i = 0; i < objSourceElement.length; i++) {  

	 document.getElementById('_id0:gnbtu').value = objSourceElement.options[i].value +','+document.getElementById('_id0:gnbtu').value;

}
for(var j =0;j<objTargetElement.length;j++){

	 document.getElementById('_id0:gbtu').value = objTargetElement.options[j].value +','+document.getElementById('_id0:gbtu').value;
}

this.form.submit();

}




function move(fbox, tbox) {
a = '_id0:'+fbox;
b = '_id0:'+tbox;
	var objSourceElement = document.getElementById(a);
	var objTargetElement = document.getElementById(b);   
    var aryTempSourceOptions = new Array();   
         var x = 0;                //looping through source element to find selected options 

         for (var i = 0; i < objSourceElement.length; i++) {     
         if (objSourceElement.options[i].selected) {      
         //need to move this option to target element  
               
                  var intTargetLen = objTargetElement.length++;
         objTargetElement.options[intTargetLen].text = objSourceElement.options[i].text;      
         objTargetElement.options[intTargetLen].value = objSourceElement.options[i].value;       
         }       
         else {       
         //storing options that stay to recreate select element  
         var objTempValues = new Object(); 
   
objTempValues.text = objSourceElement.options[i].text;
objTempValues.value = objSourceElement.options[i].value;
aryTempSourceOptions[x] = objTempValues;  
x++;       
}  
}      
//resetting length of source     
objSourceElement.length = aryTempSourceOptions.length;  
//looping through temp array to recreate source select element 
for (var i = 0; i < aryTempSourceOptions.length; i++) {     
objSourceElement.options[i].text = aryTempSourceOptions[i].text;    
objSourceElement.options[i].value = aryTempSourceOptions[i].value;
objSourceElement.options[i].selected = false;    
}   
}    //-->    </SCRIPT>



	</head>
	<body>

		<f:view>
			<%--



			<%
				UIViewRoot view = (UIViewRoot) session.getAttribute("view");
				if (view.getLocale().toString().equals("en")) {
			%>
			--%>
			<body dir="ltr">
				<%--
				<%
				} else {
				%>
			
			<body dir="rtl">
				<%
				}
				%>
			--%>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<%--
							<jsp:include page="header.jsp" />
						--%>
						</td>
					</tr>

					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<%--
									<%
									if (view.getLocale().toString().equals("en")) {
									%>
									--%>
									<td
										background="../resources/images/Grey panel/Grey-Panel-left-1.jpg"
										height="38" style="background-repeat:no-repeat;" width="100%">
										<font
											style="font-family:Trebuchet MS;font-weight:regular;font-size:19px;margin-left:15px;top:30px;">USER
											GROUP</font>
										<!--<IMG src="../resources/images/Grey panel/Grey-Panel-left-1.jpg"/>-->
										<%--<%
										} else {
										%>
									
									--%>
									<td width="100%">
										<IMG
											src="../resources/images/ar/Detail/Grey Panel/Grey-Panel-right-1.jpg"></img>
										<%--
										<%
										}
										%>
									--%>
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>

									<td width="500px" height="100%" valign="top" nowrap="nowrap">
										<h:form>
											<table>
												<tr>
													<td colspan="6">
														<h:messages></h:messages>
													</td>
												</tr>
												<tr>
													<td>
														
													</td>
													<td>
														
													</td>
													<td>
														&nbsp;
													</td>
													<td>
														
													</td>
													<td>
														

													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td colspan="5">

														<h:panelGrid width="200">

															<t:tree2 id="wrapTree"
																value="#{pages$userGroup.expandedTreeData}" var="node"
																varNodeToggler="t" clientSideToggle="true">


																<f:facet name="ROOT">
																	<h:panelGroup>
																		<h:outputText value="#{node.description}" styleClass="nodeFolder" />
																	</h:panelGroup>
																</f:facet>

																<f:facet name="LEAF">
																	<h:panelGroup>
																		<t:selectBooleanCheckbox value="true"
																		
																			title="#{node.identifier}" />
																		<h:outputText value="#{node.description}"
																			styleClass="nodeFolder" />
																		<h:outputText value=" (#{node.childCount})"
																			styleClass="childCount"
																			rendered="#{!empty node.children}" />
																	</h:panelGroup>
																</f:facet>

																
																	<f:facet name="RIGHTS">
																	<h:panelGroup>
																		<t:selectBooleanCheckbox value="false"
																				title="#{node.identifier}" />
																		<h:outputText value="#{node.description}" />

																	</h:panelGroup>
																</f:facet>
																
															</t:tree2>
														</h:panelGrid>


													</td>
												</tr>
												<tr>
													<td colspan="2">



													</td>
													<td align="center">




														<br>





													</td>
													<td colspan="2">

													</td>
												</tr>
												<tr align="right">
													<td colspan="5">
														<h:commandButton styleClass="BUTTON" value="Save"
															action="#{pages$userGroup.setPermissions}" type="submit"
															onclick="javascript:ads();">
														</h:commandButton>
														&nbsp;
														<h:commandButton styleClass="BUTTON" value="Reset"
															action="#{pages$userGroup.reset}"></h:commandButton>
													</td>

												</tr>
											</table>


										</h:form>
			</body>
		</f:view>
</html>
