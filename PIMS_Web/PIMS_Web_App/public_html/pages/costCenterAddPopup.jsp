<%-- 
  - Author: Kamran Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Add and Update Cost Center Screen
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>

<script language="JavaScript" type="text/javascript">
	    function resetValues()
      	{
      		document.getElementById("searchFrm:identifierLetter").value="";			
      	    document.getElementById("searchFrm:ownershipType").selectedIndex=0;
        }
        function closeAndResubmit()
        {
        	window.close();
        	window.opener.receivePopupCall();
        }
        
        </script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<%
					response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
						response.setHeader("Pragma", "no-cache"); //HTTP 1.0
						response.setDateHeader("Expires", 0); //prevents caching at the proxy server
				%>
				<!-- Header -->
				<table width="80%" style="height: 50%" cellspacing="0" border="0">
					<tr width="100%">
						<td width="83%" height="200px" valign="top"
							class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['label.addIdentifier']}"
											rendered="#{pages$costCenterAddPopup.addMode}"
											styleClass="HEADER_FONT" />
										<h:outputLabel value="#{msg['label.updateIdentifier']}"
											rendered="#{!pages$costCenterAddPopup.addMode}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" height="100%">
										<div
											style="height: 95%; width: 100%; # height: 95%; # width: 100%;">
											<h:form id="searchFrm"
												style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
												<t:div styleClass="MESSAGE">
													<table border="0" class="layoutTable">
														<tr>
															<td>
																<h:outputText
																	value="#{pages$costCenterAddPopup.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:outputText
																	value="#{pages$costCenterAddPopup.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
															</td>
														</tr>
													</table>
												</t:div>
												<div class="MARGIN">
													<div class="DETAIL_SECTION">
														<h:outputLabel value="#{msg['commons.searchCriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER">
															<tr>
																<td width="97%">
																	<table width="100%">
																		<tr>

																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['grp.ownerShipMenu']}:"></h:outputLabel>
																			</td>

																			<td width="25%">
																				<h:selectOneMenu id="ownershipType"
																					binding="#{pages$costCenterAddPopup.cboOwnershipTypeId}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyOwnershipType}" />
																				</h:selectOneMenu>
																			</td>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['label.ownershipIdentifier']}:"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText styleClass="INPUT"
																					id="identifierLetter"
																					onkeyup="removeMoreThanOneNonAlpha(this)"
																					onkeypress="removeMoreThanOneNonAlpha(this)"
																					onkeydown="removeMoreThanOneNonAlpha(this)"
																					onblur="removeMoreThanOneNonAlpha(this)"
																					onchange="removeMoreThanOneNonAlpha(this)"
																					binding="#{pages$costCenterAddPopup.txtIdentifierLetter}"></h:inputText>
																			</td>
																		</tr>

																		<tr>
																			<td class="BUTTON_TD" colspan="4" />
																				<table cellpadding="1px" cellspacing="1px">
																					<tr>
																						<td colspan="2">
																							<h:commandButton styleClass="BUTTON"
																								action="#{pages$costCenterAddPopup.onAdd}"
																								rendered="#{pages$costCenterAddPopup.addMode}"
																								value="#{msg['attachment.add']}"
																								style="width: 75px" tabindex="7" />
																							<h:commandButton styleClass="BUTTON"
																								action="#{pages$costCenterAddPopup.onAdd}"
																								rendered="#{!pages$costCenterAddPopup.addMode}"
																								value="#{msg['commons.Update']}"
																								style="width: 75px" tabindex="7" />
																							<h:commandButton styleClass="BUTTON"
																								value="#{msg['commons.clear']}"
																								rendered="#{pages$costCenterAddPopup.addMode}"
																								onclick="javascript:resetValues();"
																								style="width: 75px" tabindex="7" />
																							<h:commandButton styleClass="BUTTON"
																								value="#{msg['commons.closeButton']}"
																								onclick="closeAndResubmit();"
																								style="width: 75px" tabindex="7" />

																						</td>


																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>
															</tr>

														</table>
													</div>
												</div>
											</h:form>

										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>