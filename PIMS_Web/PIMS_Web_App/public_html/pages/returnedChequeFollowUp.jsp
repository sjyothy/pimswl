
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript" src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	function showAddFollowUpProgressPopup() 
	{	
		var screen_width = 1024;
		var screen_height = 920;
        var popup_width = screen_width-250;
        var popup_height = screen_height-600;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('addFollowUpProgress.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	} 
	function openReplaceChequePopup()
	 {
		var screen_width = 1024;
		var screen_height = 920;
        var popup_width = screen_width-250;
        var popup_height = screen_height-400;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('ReplaceCheque.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	 }
	
	
	
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr >
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['returnedChequeFollowUp.Heading']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" >

										<h:form id="frm" style="width:97%" enctype="multipart/form-data">
											<table border="0" class="layoutTable">

												<tr>

													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$returnedChequeFollowUp.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$returnedChequeFollowUp.successMessages}" />
													    <h:inputHidden id="txtTenantId" value="#{pages$returnedChequeFollowUp.txtTenantId}" />
													    <h:inputHidden id="hdnContractId" value="#{pages$returnedChequeFollowUp.contractView.contractId}" />
													</td>
												</tr>
											</table>

											<div class="MARGIN" style="width: 98%">
												<table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
										        </table>
												<rich:tabPanel style="width:100%;height:235px;" headerSpacing="0">
												
														<rich:tab label="#{msg['returnedChequeFollowUp.followUpDetailTab.Heading']}">
															<t:div  style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid id="followUpDetailsTable" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4"
																	columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['followup.number']}:" />
																	<h:inputText styleClass="READONLY" readonly="true" style="width:85%"
																			value="#{pages$returnedChequeFollowUp.followUpView.followUpNumber}" />																	
																			
																	<h:outputLabel styleClass="LABEL" value="#{msg['bouncedChequesList.chequeNumberCol']}:" />
																	<h:inputText styleClass="READONLY" readonly="true" style="width:85%"
																			value="#{pages$returnedChequeFollowUp.followUpView.paymentReceiptDetailView.methodRefNo}" />
																			
																	<h:outputLabel styleClass="LABEL" value="#{msg['commons.replaceCheque.oldChequeDate']}:" />
																	<h:inputText styleClass="READONLY" readonly="true" style="width:85%"
																			     value="#{pages$returnedChequeFollowUp.followUpView.paymentReceiptDetailView.methodRefDate}">
																		<f:convertDateTime pattern="#{pages$returnedChequeFollowUp.dateFormat}" timeZone="#{pages$returnedChequeFollowUp.timeZone}" />
																	</h:inputText>
																		
																	<h:outputLabel styleClass="LABEL" value="#{msg['chequeList.bank']}:" />
																	<h:inputText styleClass="READONLY" readonly="true" style="width:85%"
																			value="#{pages$returnedChequeFollowUp.isEnglishLocale ? pages$returnedChequeFollowUp.followUpView.paymentReceiptDetailView.bankEn : pages$returnedChequeFollowUp.followUpView.paymentReceiptDetailView.bankAr}" />
																    <h:outputLabel styleClass="LABEL" value="#{msg['followUp.lbl.stopAllowReplaceCheque']}:" />
																	<h:selectOneMenu disabled="#{pages$returnedChequeFollowUp.isFollowUpClosed}" value="#{pages$returnedChequeFollowUp.stopReplaceCheque}">
																       <f:selectItem itemValue="0" itemLabel="#{msg['followUp.combo.allowReplaceCheque']}" />
																       <f:selectItem itemValue="1" itemLabel="#{msg['followUp.combo.stopReplaceCheque']}" />
																	
																	</h:selectOneMenu>
																	<h:outputLabel value="#{msg['commons.label.bouncedReason']} :">
																	</h:outputLabel>
																	<h:inputTextarea
																	styleClass="READONLY"
																	readonly="true" 
																	value="#{pages$returnedChequeFollowUp.followUpView.paymentReceiptDetailView.bouncedReason}">
																	</h:inputTextarea>
																</t:panelGrid>
															</t:div>
														</rich:tab>

														<!-- Contract Details Tab - Start -->
														<rich:tab label="#{msg['contract.Contract.Details']}">
															<t:div  style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid id="contractDetailsTable" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4"
																	columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">

																	<h:outputLabel styleClass="LABEL"  
																		value="#{msg['commons.Contract.Number']}:" />
																	<h:panelGroup>																		
																		<h:inputText id="txtcontractNum" styleClass="READONLY" readonly="true"
																			style="width:85%"
																			value="#{pages$returnedChequeFollowUp.contractView.contractNumber}" />
																		<h:commandLink
																			action="#{pages$returnedChequeFollowUp.btnContract_Click}">
																			<h:graphicImage style="padding-left:4px;"
																				title="#{msg['commons.view']}"
																				url="../resources/images/app_icons/Lease-contract.png" />
																		</h:commandLink>
																	</h:panelGroup>

																	<h:outputLabel value="#{msg['contract.contractType']}:" styleClass="LABEL"  />																																			
																	<h:inputText id="txtContractType" styleClass="READONLY" readonly="true"
																			style="width:85%"
																			value="#{pages$returnedChequeFollowUp.isEnglishLocale ? pages$returnedChequeFollowUp.contractView.contractTypeEn : pages$returnedChequeFollowUp.contractView.contractTypeAr}" />
																	

																	<h:outputLabel styleClass="LABEL"   value="#{msg['contract.date.Start']}:" />																	
																	<h:inputText id="txtStartDate"  styleClass="READONLY" readonly="true"
																			style="width:85%"
																			value="#{pages$returnedChequeFollowUp.contractView.startDateString}" />
																	

																	<h:outputLabel styleClass="LABEL"   value="#{msg['contract.date.expiry']}:" />																	
																	<h:inputText id="txtEndDate"  styleClass="READONLY" readonly="true"
																			style="width:85%"
																			value="#{pages$returnedChequeFollowUp.contractView.endDateString}" />
																	

																	<h:outputLabel styleClass="LABEL"  
																		value="#{msg['cancelContract.tab.unit.propertyname']}" />																	
																	<h:inputText id="txtPropertyName" readonly="true"  styleClass="READONLY" 
																			style="width:85%"
																			value="#{pages$returnedChequeFollowUp.txtpropertyName}" />
																	

																	<h:outputLabel styleClass="LABEL"   value="#{msg['property.type']}" />																	
																	<h:inputText id="txtPropertyType" readonly="true"  styleClass="READONLY" 
																			style="width:85%"
																			value="#{pages$returnedChequeFollowUp.txtpropertyType}" />
																	

																	<h:outputLabel styleClass="LABEL"  
																		value="#{msg['changeTenantName.currentTenant']}:" />
																	<h:panelGroup>																		
																		<h:inputText id="txtTenantName" readonly="true"  styleClass="READONLY" 
																			style="width:85%"
																			value="#{pages$returnedChequeFollowUp.txtTenantName}" />
																		<h:commandLink
																			action="#{pages$returnedChequeFollowUp.btnTenant_Click}">
																			<h:graphicImage style="padding-left:4px;"
																				title="#{msg['commons.view']}"
																				url="../resources/images/app_icons/Tenant.png" />
																		</h:commandLink>
																	</h:panelGroup>

																	<h:outputLabel styleClass="LABEL"   value="#{msg['commons.status']}:" />																																			
																	<h:inputText id="txtStatus" readonly="true"  styleClass="READONLY" 
																			style="width:85%"
																			value="#{pages$returnedChequeFollowUp.isEnglishLocale ? pages$returnedChequeFollowUp.contractView.statusEn : pages$returnedChequeFollowUp.contractView.statusAr}">
																	</h:inputText>
																	

																	<h:outputLabel styleClass="LABEL"  
																		value="#{msg['contract.TotalContractValue']}:" />																	
																	<h:inputText styleClass="A_RIGHT_NUM READONLY" id="txtTotalContractValue" readonly="true"  style="width:85%"
																			value="#{pages$returnedChequeFollowUp.contractView.rentAmount}" >
																		<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
																	</h:inputText>
																	

																	<h:outputLabel styleClass="LABEL"   value="#{msg['units.number']}:" />																																			
																	<h:inputText id="txtunitRefNum" readonly="true" styleClass="READONLY" 
																			style="width:85%"
																			value="#{pages$returnedChequeFollowUp.txtunitRefNum}" />
																	

																	<h:outputLabel styleClass="LABEL"   value="#{msg['unit.rentValue']}:" />																																			
																	<h:inputText styleClass="A_RIGHT_NUM READONLY"
																			id="txtUnitRentValue" readonly="true"  
																			style="width:85%"
																			value="#{pages$returnedChequeFollowUp.txtUnitRentValue}" >
																		<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
																	</h:inputText>

																	<h:outputLabel styleClass="LABEL"   value="#{msg['unit.unitType']}:" />																	
																	<h:inputText id="txtUnitType" readonly="true" styleClass="READONLY" 
																			style="width:85%"
																			value="#{pages$returnedChequeFollowUp.txtUnitType}" />
																	
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Contract Details Tab - End -->
														
														
														<!-- Court Results Tab - Start -->
														<rich:tab label="#{msg['returnedChequeFollowUp.courtResultsTab.Heading']}">
															<h:panelGrid styleClass="BUTTON_TD" width="100%">
																<h:commandButton styleClass="BUTTON" rendered="#{!pages$returnedChequeFollowUp.isFollowUpClosed}"
																		value="#{msg['returnedChequeFollowUp.courtResultsTab.btnAddResult']}"
																		action="#{pages$returnedChequeFollowUp.btnAddResult_Click}">
																</h:commandButton>
															</h:panelGrid>
																													
															<t:div styleClass="contentDiv" style="width:96%">																
						                                       <t:dataTable id="dataTable" styleClass="grid" 
																	rows="#{pages$returnedChequeFollowUp.paginatorRows}"
																	value="#{pages$returnedChequeFollowUp.courtResultsList}"																	
																	preserveDataModel="false" preserveSort="false" var="dataItem"
																	rowClasses="row1,row2" rules="all" renderedIfEmpty="true">																
																<t:column width="20%">
																	<f:facet name="header" >
																		<t:outputText value="#{msg['commons.date']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" value="#{dataItem.followUpProgressDate}">
																		<f:convertDateTime pattern="#{pages$returnedChequeFollowUp.dateFormat}" timeZone="#{pages$returnedChequeFollowUp.timeZone}" />
																	</t:outputText>																	
																</t:column>
																<t:column width="25%">
																	<f:facet name="header">
																		<t:outputText value="#{msg['returnedChequeFollowUp.courtResultsTab.grid.courtResults']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" value="#{dataItem.courtResult}" />
																</t:column>
																<t:column width="25%">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.commentsTabHeading']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" value="#{dataItem.comments}" />
																</t:column>																
																<t:column width="20%">
																	<f:facet name="header">
																		<t:outputText value="#{msg['application.status.gridHeader']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" value="#{pages$returnedChequeFollowUp.isEnglishLocale ? dataItem.statusEn : dataItem.statusAr}" />
																</t:column>
															  </t:dataTable>										
															</t:div>
														
				                                           <t:div styleClass="contentDivFooter" style="width:97.1%">
																			<t:panelGrid id="rqtkRecNumTable"  columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
																				<t:div id="rqtkRecNumDiv"  styleClass="RECORD_NUM_BG">
																					<t:panelGrid id="rqtkRecNumTbl" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
																						<h:outputText value="#{msg['commons.recordsFound']}"/>
																						<h:outputText value=" : "/>
																						<h:outputText value="#{pages$returnedChequeFollowUp.courtResultsRecordSize}"/>
																					</t:panelGrid>
																				</t:div>
																				  
																			      <CENTER>
																				   <t:dataScroller id="rqtkscroller" for="dataTable" paginator="true"
																					fastStep="1"  paginatorMaxPages="#{pages$returnedChequeFollowUp.paginatorMaxPages}" immediate="false"
																					paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true" 
																				    pageIndexVar="pageNumber"
																				    styleClass="SCH_SCROLLER"
																				    paginatorActiveColumnStyle="font-weight:bold;"
																				    paginatorRenderLinkForActive="false" 
																					paginatorTableStyle="grid_paginator" layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">
								
																					    <f:facet  name="first">
																							<t:graphicImage    url="../#{path.scroller_first}"  id="lblFRequestHistory"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastrewind">
																							<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRRequestHistory"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastforward">
																							<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFRequestHistory"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="last">
																							<t:graphicImage  url="../#{path.scroller_last}"  id="lblLRequestHistory"></t:graphicImage>
																						</f:facet>
									
								                                                      <t:div id="rqtkPageNumDiv"   styleClass="PAGE_NUM_BG">
																					   <t:panelGrid id="rqtkPageNumTable"  styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0" >
																					 					<h:outputText  styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																										<h:outputText   styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																						</t:panelGrid>
																							
																						</t:div>
																				 </t:dataScroller>
																				 </CENTER>
																		      
																	        </t:panelGrid>
								                           </t:div>
															
														</rich:tab>                                                        
														<!-- Court Results Tab - End -->
														
														
														<!-- Actions History Tab - Start -->
														<rich:tab label="#{msg['returnedChequeFollowUp.actionsHistoryTab.Heading']}">
															<t:div styleClass="contentDiv" style="width:96%">
						                                       <t:dataTable id="dataTable2" styleClass="grid" 
																	rows="#{pages$returnedChequeFollowUp.paginatorRows}"
																	value="#{pages$returnedChequeFollowUp.actionsHistoryList}"																	
																	preserveDataModel="false" preserveSort="false" var="dataItem"
																	rowClasses="row1,row2" rules="all" renderedIfEmpty="true">																
																<t:column width="20%">
																	<f:facet name="header" >
																		<t:outputText value="#{msg['commons.date']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" value="#{dataItem.followUpProgressDate}">
																		<f:convertDateTime pattern="#{pages$returnedChequeFollowUp.dateFormat}" timeZone="#{pages$returnedChequeFollowUp.timeZone}" />
																	</t:outputText>																	
																</t:column>
																<t:column width="20%">
																	<f:facet name="header">
																		<t:outputText value="#{msg['returnedChequeFollowUp.actionsHistoryTab.grid.actionType']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" value="#{pages$returnedChequeFollowUp.isEnglishLocale ? dataItem.followUpActionDescriptionEn : dataItem.followUpActionDescriptionAr}" />
																</t:column>
																<t:column width="20%">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.username']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" value="#{dataItem.createdBy}" />
																</t:column>
																<t:column width="20%">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.commentsTabHeading']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" value="#{dataItem.comments}" />
																</t:column>																
																<t:column width="20%">
																	<f:facet name="header">
																		<t:outputText value="#{msg['application.status.gridHeader']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" value="#{pages$returnedChequeFollowUp.isEnglishLocale ? dataItem.statusEn : dataItem.statusAr}" />
																</t:column>
															  </t:dataTable>										
															</t:div>
														
				                                           <t:div styleClass="contentDivFooter" style="width:97.1%">
																			<t:panelGrid id="rqtkRecNumTable2"  columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
																				<t:div id="rqtkRecNumDiv2"  styleClass="RECORD_NUM_BG">
																					<t:panelGrid id="rqtkRecNumTbl2" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
																						<h:outputText value="#{msg['commons.recordsFound']}"/>
																						<h:outputText value=" : "/>
																						<h:outputText value="#{pages$returnedChequeFollowUp.actionsHistoryRecordSize}"/>
																					</t:panelGrid>
																				</t:div>
																				  
																			      <CENTER>
																				   <t:dataScroller id="rqtkscroller2" for="dataTable2" paginator="true"
																					fastStep="1"  paginatorMaxPages="#{pages$returnedChequeFollowUp.paginatorMaxPages}" immediate="false"
																					paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true" 
																				    pageIndexVar="pageNumber"
																				    styleClass="SCH_SCROLLER"
																				    paginatorActiveColumnStyle="font-weight:bold;"
																				    paginatorRenderLinkForActive="false" 
																					paginatorTableStyle="grid_paginator" layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">
								
																					    <f:facet  name="first">
																							<t:graphicImage    url="../#{path.scroller_first}"  id="lblFRequestHistory2"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastrewind">
																							<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRRequestHistory2"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastforward">
																							<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFRequestHistory2"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="last">
																							<t:graphicImage  url="../#{path.scroller_last}"  id="lblLRequestHistory2"></t:graphicImage>
																						</f:facet>
									
								                                                      <t:div id="rqtkPageNumDiv2"   styleClass="PAGE_NUM_BG">
																					   <t:panelGrid id="rqtkPageNumTable2"  styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0" >
																					 					<h:outputText  styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																										<h:outputText   styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																						</t:panelGrid>
																							
																						</t:div>
																				 </t:dataScroller>
																				 </CENTER>
																		      
																	        </t:panelGrid>
								                           </t:div>
														</rich:tab>                                                        
														<!-- Actions History Tab - End -->
														
														
                                                        
                                                        <!-- Attachments Tab - Start -->
														<rich:tab label="#{msg['contract.attachmentTabHeader']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<!-- Attachments Tab - End -->
														
														<!-- Comments Tab - Start -->
                                                         <rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														<!-- Comments Tab - End -->
																												
													</rich:tabPanel>
												
											<table cellpadding="0" cellspacing="0"  style="width:100%;">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
													<table cellpadding="0" cellspacing="5" width="100%">
													<tr>
														<td colspan="6" class="BUTTON_TD">
															<h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}"
																	rendered="#{pages$returnedChequeFollowUp.showBtnSave}" action="#{pages$returnedChequeFollowUp.btnSave_Click}">
															</h:commandButton>
															<h:commandButton styleClass="BUTTON" value="#{msg['extendApplication.search.toolTips.followUp']}"
																	rendered="#{pages$returnedChequeFollowUp.showBtnFollowup}" action="#{pages$returnedChequeFollowUp.btnFollowUp_Click}">
															</h:commandButton>
															<h:commandButton styleClass="BUTTON" 
																		style="width:auto;"
																		action="#{pages$returnedChequeFollowUp.showReplaceChequeRequest}"
																		value="#{msg['followup.btnLabel.showReplaceChequeRequest']}">
															</h:commandButton>
															<h:commandButton styleClass="BUTTON" 
																		onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;"
																		rendered="#{pages$returnedChequeFollowUp.showBtnSendToLegal}"
																		style="width:auto;"
																		action="#{pages$returnedChequeFollowUp.sendToLegalDeptt}"
																		value="#{msg['common.sendForFollowUp']}">
															</h:commandButton>
															<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}"
																	action="#{pages$returnedChequeFollowUp.btnCancel_Click}">
															</h:commandButton>
															
																														
														</td>
													</tr>
												</table>
											</div>
								
								
									    </h:form>
									</div>
								

									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
           </div>
		</body>
	</html>
</f:view>
