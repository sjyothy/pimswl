<%-- 
  - Author: Afzal Zulfiqar
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for add/edit a violation
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%-- JAVA SCRIPT FUNCTIONS START --%>
<script type="text/javascript">
	function resetViolationType() {
		document.getElementById("searchFrm:violationType").selectedIndex=0;
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}" >
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
		</head>
		
		<body class="BODY_STYLE">
		
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				
				<tr width="100%">
					
					<td width="100%" valign="top" class="divBackgroundBody">
						<table  class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['violationDetails.header']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99.1%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%"  valign="top" nowrap="nowrap">
									<h:form id="searchFrm" >
									<div class="SCROLLABLE_SECTION" >
										
										<t:div styleClass="MESSAGE" style="width:98%;">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText id="ccc" escape="false" styleClass="ERROR_FONT"
																value="#{pages$AddViolationPopup.errorMessages}" />
														</td>
													</tr>
												</table>
											</t:div>
													
														<div class="MARGIN" style="width:98%;">
														<table cellpadding="0" cellspacing="0" width="99.3%">
																<tr>
																<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
																<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
																<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
																</tr>
														</table>
														<div class="DETAIL_SECTION" style="width:99%" >
														     <h:outputLabel value="" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
																<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" width="100%">
																	<tr>
																	<td>
																			<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"
																				value="#{msg['unit.unitNumber']}:"></h:outputLabel>
																		</td>
																	<td>
																			<h:selectOneMenu id="cmbViolationUnit" readonly="#{pages$AddViolationPopup.enableActionOnly}"
																				value="#{pages$AddViolationPopup.violationUnit}"
																				styleClass="SELECT_MENU">
																				<f:selectItem id="plsSelectTypeUnit" itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$AddViolationPopup.unitList}" />
                                                                            <a4j:support event="onchange" action="#{pages$AddViolationPopup.loadViolationUnit}" reRender="txtContractNumber,txtTenantName" />
																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.replaceCheque.status']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="cmbViolationStatus" readonly="#{pages$AddViolationPopup.enableActionOnly}"
																				value="#{pages$AddViolationPopup.violationStatus}"
																				styleClass="SELECT_MENU">
																				<f:selectItem id="plsSelectTypeStatus" itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$AddViolationPopup.statusList}" />

																			</h:selectOneMenu>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['contract.tenantName']}:" />
																		</td>
																		<td>
																			<h:inputText id="txtTenantName"
																				value="#{pages$AddViolationPopup.tenantName}" 
																				styleClass="A_LEFT INPUT_TEXT READONLY" readonly="true" />
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['contract.contractNumber']}:" />
																		</td>
																		<td>
																			<h:inputText id="txtContractNumber"
																				value="#{pages$AddViolationPopup.contractNumber}"
																				styleClass="A_LEFT INPUT_TEXT READONLY" readonly="true" />
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"
																				value="#{msg['violation.Date']}:"></h:outputLabel>
																		</td>
																		<td>
																			<rich:calendar id="vdate"
																			disabled="#{pages$AddViolationPopup.enableActionOnly}"
																			datePattern="#{pages$AddViolationPopup.dateFormat}"
																			value="#{pages$AddViolationPopup.violationDateRich}"
																			locale="#{pages$AddViolationPopup.locale}" popup="true"
																			showApplyButton="false" enableManualInput="false"
																			cellWidth="24px" cellHeight="22px"
																			 />
																		</td>
																		<td>
																			<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"
																				value="#{msg['violation.category']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="violationCategory" readonly="#{pages$AddViolationPopup.enableActionOnly}"
																				value="#{pages$AddViolationPopup.violationCategory}"
																				valueChangeListener="#{pages$AddViolationPopup.violationCategorySelected}"
																				onchange="resetViolationType();submit()"
																				styleClass="SELECT_MENU">
																				<f:selectItem id="plsSelectCategory" itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.violationCategoryList}" />

																			</h:selectOneMenu>
																		</td>
																		
																		
																	</tr>
																																		
																	<tr>
																	    																	
																		<td >
																			<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"
																				value="#{msg['violation.type']}:" />
																		</td>
																		<td colspan="3"  >
																			<h:selectOneMenu style="width:100%;" id="violationType" readonly="#{pages$AddViolationPopup.enableActionOnly}"
																				value="#{pages$AddViolationPopup.violationType}"
																				styleClass="SELECT_MENU">
																				<f:selectItem id="plsSelectType" itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$AddViolationPopup.violationTypes}" />

																			</h:selectOneMenu>
																		</td>
														          </tr>	
														          <tr>
														              <td>			
														                <h:outputLabel styleClass="LABEL"
																           value="#{msg['violationAction.actions']}:" />
															          </td>
															           <td>
																	   <div class="SCROLLABLE_SECTION" style="height:100px;width:298px;overflow-x:hidden;border-width: 1px; border-style: solid; border-color: #a2a2a2;">   
																		<h:panelGrid
																			style="border-width: 1px; border-style: solid; border-color: #a2a2a2; background: white; overflow-x: auto; overflow-y: scroll; width:282px ;">
																			<h:selectManyCheckbox layout="pageDirection"
																				id="selectManyViolationActions"
																				value="#{pages$AddViolationPopup.violationActions}">
																				<f:selectItems
																					value="#{pages$AddViolationPopup.violationActionList}" />
																			</h:selectManyCheckbox>
																		</h:panelGrid>
																	 </div>	
																	</td>
																	<td>
																		<h:outputLabel styleClass="LABEL" value="#{msg['commons.description']}: "></h:outputLabel>
																	</td>
																	<td>                 
																		<h:inputTextarea id="txtAreaViolationDesc" styleClass="INPUT_TEXT_AREA" style="height:100px"  
																		                 value="#{pages$AddViolationPopup.voilationDescription}" 
																		                 disabled="#{pages$AddViolationPopup.enableActionOnly}" >
																		</h:inputTextarea>
																	 </td>				
														         </tr>
												         </table>
															</div>
																<TABLE width="100%">
																	<tr>
																		<td class="BUTTON_TD" colspan="6">	
																				<h:commandButton
																					styleClass="BUTTON ADD_VIOLATION_BUTTON"
																					value="#{msg['violation.addViolation']}" rendered="#{pages$AddViolationPopup.renderedDiv}"
																					action="#{pages$AddViolationPopup.addViolationToInspection}"
																					style="width: 100px" />
																					
																		</td>
																	</tr>
																</TABLE>

														
							 <t:div style="width:99.4%;" rendered="#{pages$AddViolationPopup.renderedDiv}" >		    
									<t:div styleClass="contentDiv"  style=" width:99.5%;margin-right:0px;" >
										<t:dataTable id="dtViolation" rows="5" preserveDataModel="false"
														preserveSort="false" var="dataItemVI" rules="all"
														renderedIfEmpty="true" width="100%" 
														value="#{pages$AddViolationPopup.violationViewDataList}"
														binding="#{pages$AddViolationPopup.dataTableViolation}">

												<t:column id="colViolUnitNumber" width="10%"  sortable="true">
													<f:facet name="header">
														<t:outputText id="otxtUnitNumbet" value="#{msg['unit.unitNumber']}"  />
													</f:facet>
													<t:outputText styleClass="A_LEFT" style="WHITE-SPACE: normal"  value="#{dataItemVI.unitNumber}" />
												</t:column>
												<t:column id="colViolationDate" rendered="false"  sortable="true" >
													<f:facet name="header">
														<t:outputText id="otxtVolDate" value="#{msg['violation.Date']}" />
													</f:facet>
													<t:outputText id="otxtVolDateValue" styleClass="A_LEFT" style="white-space: normal;" value="#{dataItemVI.violationDateString}" />
													
												</t:column>
												<t:column id="colViolCategory"  width="20%" sortable="true" >
													<f:facet name="header">
														<t:outputText id="otxtVolCategory"value="#{msg['violation.category']}" />
													</f:facet>
													<t:outputText id="otxtCatValueEn" styleClass="A_LEFT" style="WHITE-SPACE: normal" value="#{dataItemVI.categoryEn}" rendered="#{pages$AddViolationPopup.isEnglishLocale}"/>
													<t:outputText id="otxtCatValueAr" styleClass="A_LEFT" style="WHITE-SPACE: normal" value="#{dataItemVI.categoryAr}" rendered="#{pages$AddViolationPopup.isArabicLocale}"/>
													
												</t:column>
												<t:column id="colViolType" width="15%" sortable="true">
													<f:facet name="header">
														<t:outputText id="otxtVoilType" value="#{msg['violation.type']}" />
													</f:facet>
													<t:outputText id="otxtVoilTypeValueEn" styleClass="A_LEFT" style="WHITE-SPACE: normal" value="#{dataItemVI.typeEn}" rendered="#{pages$AddViolationPopup.isEnglishLocale}"/>
													<t:outputText id="otxtVoilTypeValueAr" styleClass="A_LEFT" style="WHITE-SPACE: normal" value="#{dataItemVI.typeAr}" rendered="#{pages$AddViolationPopup.isArabicLocale}"/>
												</t:column>
												<t:column id="colViolTenant" width="17%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['contract.tenantName']}"  />
													</f:facet>
													<t:outputText styleClass="A_LEFT" style="WHITE-SPACE: normal"  value="#{dataItemVI.tenantName}" />
												</t:column>
												<t:column id="colVioContract" width="15%"  sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['contract.contractNumber']}"  />
													</f:facet>
													<t:outputText styleClass="A_LEFT" style="WHITE-SPACE: normal"  value="#{dataItemVI.contractNumber}" />
												</t:column>
												<t:column id="colVioStatus" width="15%" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['violation.status']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" style="WHITE-SPACE: normal"  value="#{dataItemVI.statusEn}" rendered="#{pages$AddViolationPopup.isEnglishLocale}"/>
													<t:outputText styleClass="A_LEFT" style="WHITE-SPACE: normal"  value="#{dataItemVI.statusAr}" rendered="#{pages$AddViolationPopup.isArabicLocale}"/>
													
												</t:column>
												<t:column  id="colViolationAction" sortable="false"  width="7%">
													<f:facet name="header">
														<t:outputText  value="#{msg['commons.action']}" />
													</f:facet>
													
													<h:commandLink action="#{pages$AddViolationPopup.btnDeleteViolation_Click}"  >
													<h:graphicImage style="margin-left:13px;" title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />
													</h:commandLink>
													<h:commandLink action="#{pages$AddViolationPopup.btnEditViolation_Click}"  >
													<h:graphicImage style="margin-left:13px;" title="#{msg['commons.edit']}" url="../resources/images/edit-icon.gif" />
													</h:commandLink>
												</t:column>
											</t:dataTable>
							           </t:div>
									</t:div>
							 										
							
											 										
													<TABLE width="100%">
																	<tr>
																		<td class="BUTTON_TD" colspan="6">	
																					<h:commandButton
														                            styleClass="BUTTON ADD_VIOLATION_BUTTON"
														                            value="#{msg['commons.saveButton']}"
														                            action="#{pages$AddViolationPopup.saveViolation}"
														                            style="width: 100px" />
																		</td>
																	</tr>
																</TABLE>				
																
								
											

												<h:inputHidden id="contractNumberHidden"
													value="#{pages$AddViolationPopup.contractRefNo}" />
												<h:inputHidden id="unitNumberHidden"
													value="#{pages$AddViolationPopup.unitRefNo}" />
												<h:inputHidden id="unitTypeHidden"
													value="#{pages$AddViolationPopup.unitType}" />
												<h:inputHidden id="unitStatusHidden"
													value="#{pages$AddViolationPopup.unitStatus}" />
												<h:inputHidden id="propertyNameHidden"
													value="#{pages$AddViolationPopup.propertyName}" />
												<h:inputHidden id="unitId"
													value="#{pages$AddViolationPopup.unitId}" />
												<h:inputHidden id="inspectionId"
													value="#{pages$AddViolationPopup.inspectionId}" />
												<h:inputHidden id="formListHid"
													value="#{pages$AddViolationPopup.fromList}" />
												<h:inputHidden id="editHid"
													value="#{pages$AddViolationPopup.editable}" />
										
									</div>
									</h:form>
								</td>
							</tr>

						</table>
					</td>
				</tr>
			</table>


</body>
</html>
</f:view>