<t:div style="width:95%;padding-top:5px;padding-botton:5px;">

	<t:panelGrid id="ddTab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblExpCompDate" styleClass="LABEL"
				value="#{msg['endowmentProgram.lbl.expCompDate']}" />
		</h:panelGroup>
		<rich:calendar inputStyle="width:170px; height:14px"
			value="#{pages$endowmentPrograms.programMilestone.expCompletionDate}" />


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblStatus" styleClass="LABEL"
				value="#{msg['commons.status']}" />
		</h:panelGroup>
		<h:selectOneMenu id="cmbStatusId"
			value="#{pages$endowmentPrograms.programMilestone.statusId }"
			style="width: 200px;">
			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems
				value="#{pages$ApplicationBean.programMilestoneStatus}" />

		</h:selectOneMenu>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['endowmentProgram.lbl.expCompPercent']}:" />
		<h:inputText id="txtExpPercent"
			readonly="#{!pages$endowmentPrograms.showSaveButton && 
			    		!pages$endowmentPrograms.showResubmitButton 
			           }"
			value="#{pages$endowmentPrograms.programMilestone.expCompPercentageStr}"
			onkeyup="removeNonNumeric(this)" onkeypress="removeNonNumeric(this)"
			onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
			onchange="removeNonNumeric(this)" />



		<h:outputLabel value="" />
		<h:outputLabel value="" />


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="amnt" styleClass="LABEL"
				value="#{msg['endowmentProgram.lbl.activity']}:"></h:outputLabel>
		</h:panelGroup>
		<t:panelGroup colspan="3" style="width: 100%">
			<h:inputTextarea id="invDetails"
				value="#{pages$endowmentPrograms.programMilestone.description}"
				style="width: 97%; height: 70px;" />
		</t:panelGroup>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblCompDate" styleClass="LABEL"
				value="#{msg['endowmentProgram.lbl.compDate']}" />
		</h:panelGroup>
		<rich:calendar inputStyle="width:170px; height:14px"
			value="#{pages$endowmentPrograms.programMilestone.completionDate}" />


		<h:outputLabel styleClass="LABEL"
			value="#{msg['endowmentProgram.lbl.compPercent']}:" />
		<h:inputText id="txtPercent"
			readonly="#{!pages$endowmentPrograms.showSaveButton && 
			    		!pages$endowmentPrograms.showResubmitButton 
			           }"
			value="#{pages$endowmentPrograms.programMilestone.compPercentageStr}"
			onkeyup="removeNonNumeric(this)" onkeypress="removeNonNumeric(this)"
			onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
			onchange="removeNonNumeric(this)" />


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="mmstremarks" styleClass="LABEL"
				value="#{msg['commons.compRemarks']}:"></h:outputLabel>
		</h:panelGroup>
		<t:panelGroup colspan="3" style="width: 100%">
			<h:inputTextarea id="mmstRmarks"
				value="#{pages$endowmentPrograms.programMilestone.completionRemarks}"
				style="width: 97%; height: 70px;" />
		</t:panelGroup>


	</t:panelGrid>
	<t:div styleClass="BUTTON_TD">
		<h:commandButton styleClass="BUTTON"
			rendered="#{
						pages$endowmentPrograms.showSaveButton || 
						pages$endowmentPrograms.showResubmitButton 
					   }"
			value="#{msg['commons.saveButton']}"
			action="#{pages$endowmentPrograms.onAddProgramMilestone}">
		</h:commandButton>

	</t:div>

	<t:div styleClass="contentDiv" style="width:100%; margin-top:15px;">
		<t:dataTable id="programMSTGrid"
			binding="#{pages$endowmentPrograms.dataTableProgramMilestone}"
			value="#{pages$endowmentPrograms.listProgramMilestone}"
			preserveDataModel="true" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
			width="100%">

			<%-- 
			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['commons.refNum']}" />
				</f:facet>
				<h:outputText style="white-space: normal;" id="mstNum"
					value="#{dataItem.refNum}" />
			</t:column>
			--%>
			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['commons.status']}" />
				</f:facet>
				<h:outputText style="white-space: normal;" id="mststatus"
					value="#{dataItem.status.dataDescAr}" />
			</t:column>
			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['endowmentProgram.lbl.expCompDate']}" />
				</f:facet>
				<h:outputText style="white-space: normal;" id="mstEXPCOMPDATE"
					value="#{dataItem.expCompletionDate}">
					<f:convertDateTime timeZone="#{pages$endowmentPrograms.timeZone}"
						pattern="#{pages$endowmentPrograms.dateFormat}" />
				</h:outputText>
			</t:column>

			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['endowmentProgram.lbl.expCompPercent']}" />
				</f:facet>
				<h:outputText style="white-space: normal;" id="mstexpCompPercent"
					value="#{dataItem.expCompPercentageStr}" />
			</t:column>


			<t:column id="desc" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText id="hdTo"
						value="#{msg['endowmentProgram.lbl.activity']}" />
				</f:facet>
				<div style="width: 70%; white-space: normal;">
					<t:outputText id="p_w" styleClass="A_LEFT"
						style="white-space: normal;" value="#{dataItem.description}" />
				</div>
			</t:column>

			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['endowmentProgram.lbl.actCompDate']}" />
				</f:facet>
				<h:outputText style="white-space: normal;" id="mstCOMPDATE"
					value="#{dataItem.completionDate}">
					<f:convertDateTime timeZone="#{pages$endowmentPrograms.timeZone}"
						pattern="#{pages$endowmentPrograms.dateFormat}" />
				</h:outputText>
			</t:column>

			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['endowmentProgram.lbl.compPercent']}" />
				</f:facet>
				<h:outputText style="white-space: normal;" id="mscompPercent"
					value="#{dataItem.compPercentageStr}" />
			</t:column>

			<t:column id="rmrks" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText id="hdrmrks" value="#{msg['commons.compRemarks']}" />
				</f:facet>
				<div style="width: 70%; white-space: normal;">
					<t:outputText id="p_w_r" styleClass="A_LEFT"
						style="white-space: normal;" value="#{dataItem.completionRemarks}" />
				</div>
			</t:column>
			<t:column id="actionGridMST" width="5%" style="white-space: normal;"
				sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.action']}" />
				</f:facet>


				<t:commandLink
					rendered="#{
								pages$endowmentPrograms.showSaveButton || 
								pages$endowmentPrograms.showResubmitButton 
							   }"
					action="#{pages$endowmentPrograms.onEditProgramMilestone}">
					<h:graphicImage id="eidtIcon" title="#{msg['commons.edit']}"
						url="../resources/images/edit-icon.gif" width="18px;" />
				</t:commandLink>

				<t:commandLink
					rendered="#{
								pages$endowmentPrograms.showSaveButton || 
								pages$endowmentPrograms.showResubmitButton 
							   }"
					onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;"
					action="#{pages$endowmentPrograms.onDeleteProgramMilestone}">
					<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
						url="../resources/images/delete_icon.png" />
				</t:commandLink>


			</t:column>

		</t:dataTable>
	</t:div>



	<t:div styleClass="DETAIL_SECTION">
		<t:panelGrid width="100%" columns="4" cellpadding="1px"
			cellspacing="5px" styleClass="DETAIL_SECTION_INNER"
			columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
			
			
			<t:outputLabel style="font-weight:bold;"
				value="#{msg['endowmentProgram.lbl.sumExpPercent']} :" />
			<t:outputLabel style="font-weight:bold;"
				value="#{pages$endowmentPrograms.sumExpPercentagesStr}" />

			<t:outputLabel style="font-weight:bold;"
				value="#{msg['endowmentProgram.lbl.sumPercent']} :" />
			<t:outputLabel style="font-weight:bold;"
				value="#{pages$endowmentPrograms.sumPercentagesStr}" />

		</t:panelGrid>
	</t:div>


</t:div>