<%-- 
  - Author:Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for reading person info from emirates card.
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>

<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/base64.js"></script>

<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/jquery-1.8.2.js"></script>

<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/errors.js"></script>

<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/zfcomponent.js"></script>


<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/jquery-ui-1.9.0.js"></script>


<script language="JavaScript" type="text/javascript">
			
	function sendToParent(context)
	{
	    
		window.opener.responseFromReadEIDDataScreen(context);
		window.close();
	}
	
	
	function doReadPublicData() 
	{
    var Ret = Initialize();
    if(Ret == false)
        return;
    
    Ret = ReadPublicData(true, true, true, true, true, true);
    if(Ret == false)
        return;
    var ef_idn_cn = GetEF_IDN_CN();
    
    //$("#ef_idn_cn").val(ef_idn_cn );
    document.getElementById('frm:ef_idn_cn').value=ef_idn_cn ;

    var nonModData =GetEF_NonModifiableData();
    //$("#ef_non_mod_data").val(nonModData );
    document.getElementById('frm:ef_non_mod_data').value=nonModData;

    var ef_mod_data = GetEF_ModifiableData();
    //$("#ef_mod_data").val(ef_mod_data);
    document.getElementById('frm:ef_mod_data').value=ef_mod_data;

    var ef_home_address= GetEF_HomeAddressData();
   // $("#ef_home_address").val(ef_home_address);
    document.getElementById('frm:ef_home_address').value=ef_home_address;
    
    var ef_work_address= GetEF_WorkAddressData();
 //   $("#ef_work_address").val(ef_work_address);
    document.getElementById('frm:ef_work_address').value=ef_work_address;
    
    //$("#ef_sign_image").val(GetEF_HolderSignatureImage());
    //$("#ef_photo").val(GetEF_Photography());
    //$("#ef_root_cert").val(GetEF_RootCertificate());
    
    
    //$("#btnVerifyPubData").removeAttr("disabled");
    //$("#msg p:last").html("Public data read successfully");
    //$("#msg").show("fade", {}, 500);
    
    document.getElementById('frm:lnkScanEmiratesCard').onclick();
}

function verifyPublicData() {
    $("#form1").attr("action", "ParsePublicData");
    $('#form1').submit();
}

		
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">


			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
					String userAgent = request.getHeader("user-agent");
					if (userAgent.indexOf("MSIE") != -1
							&& userAgent.indexOf("x64") != -1) {
			%>
			<object id="ZFComponent" width="0" height="0"
				classid="CLSID:502A94C0-E6CB-4910-846D-6F4F261E98C0"
				codebase="EIDA_ZF_ActiveX64.CAB">
				<strong style="color: red">ActiveX is not supported by this
					browser, please use Internet Explorer</strong>
			</object>
			<%
				} else {
			%>

			<object id="ZFComponent" width="0" height="0"
				classid="CLSID:502A94C0-E6CB-4910-846D-6F4F261E98C0"
				codebase="EIDA_ZF_ActiveX.CAB">
				<strong style="color: red">ActiveX is not supported by this
					browser, please use Internet Explorer</strong>
			</object>

			<%
				}
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">




				<tr width="100%">


					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="Emirates Id Data Reader"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="frm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td >
															<h:outputText id="errorMessages"
																value="#{pages$ReadEIDData.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$ReadEIDData.successMessages}"
																escape="false" styleClass="INFO_FONT" />
															<h:inputHidden id="ef_idn_cn"
																value="#{pages$ReadEIDData.ef_idn_cn}" />
															<h:inputHidden id="ef_mod_data"
																value="#{pages$ReadEIDData.ef_mod_data}" />
															<h:inputHidden id="ef_non_mod_data"
																value="#{pages$ReadEIDData.ef_non_mod_data}" />
															<h:inputHidden id="ef_sign_image"
																value="#{pages$ReadEIDData.ef_sign_image}" />
															<h:inputHidden id="ef_home_address"
																value="#{pages$ReadEIDData.ef_home_address}" />
															<h:inputHidden id="ef_work_address"
																value="#{pages$ReadEIDData.ef_work_address}" />

														</td>
													</tr>
												</table>
											</div>
											<t:div style="width: 95%;" id="buttonSelectUnSelectDiv" styleClass="BUTTON_TD">

												<h:commandButton styleClass="BUTTON"
													value="#{msg['person.lbl.selectAll']}"
													action="#{pages$ReadEIDData.onSelectAll}" />

												<h:commandButton styleClass="BUTTON"
													value="#{msg['person.lbl.unSelectAll']}"
													action="#{pages$ReadEIDData.onDeSelectAll}" />

											</t:div>

											<div class="MARGIN" style="width: 95%;">

												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width: 99.8%;">
													<h:outputLabel value="#{msg['person.personDetail']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px" border="0"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">
																	<tr>

																		<td width="10%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['customer.socialSecNumber']}:"></h:outputLabel>

																		</td>
																		<td width="30%">
																			<h:inputText id="txtidn" readonly="true"
																				styleClass="READONLY"
																				value="#{pages$ReadEIDData.person.socialSecNumber}"
																				style="width:570px;">
																			</h:inputText>
																		</td>
																		<td width="20%">
																			<h:selectBooleanCheckbox id="chkSocialSecNumber"
																				binding="#{pages$ReadEIDData.chkSocialSecNumber}">
																			</h:selectBooleanCheckbox>
																		</td>


																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['customer.firstname']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="txtfirstName"
																				value="#{pages$ReadEIDData.person.firstName}"
																				style="width:570px;">
																			</h:inputText>
																		</td>
																		<td>
																			<h:selectBooleanCheckbox id="chkFirstName"
																				binding="#{pages$ReadEIDData.chkFirstName}">
																			</h:selectBooleanCheckbox>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['customer.lastname']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="txtlastName"
																				value="#{pages$ReadEIDData.person.lastName}"
																				style="width:570px;">
																			</h:inputText>
																		</td>
																		<td>
																			<h:selectBooleanCheckbox id="chkLastName"
																				binding="#{pages$ReadEIDData.chkLastName}">
																			</h:selectBooleanCheckbox>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['person.fullName.en']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="txtName"
																				value="#{pages$ReadEIDData.person.fullNameEn}"
																				style="width:570px;">
																			</h:inputText>
																		</td>
																		<td>
																			<h:selectBooleanCheckbox id="chkFullNameEn"
																				binding="#{pages$ReadEIDData.chkFullNameEn}">
																			</h:selectBooleanCheckbox>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['customer.dateOfBirth']}:"></h:outputLabel>
																		</td>
																		<td>

																			<rich:calendar id="DateOfBirth"
																				value="#{pages$ReadEIDData.person.dateOfBirth}"
																				disabled="true" popup="true"
																				datePattern="#{pages$ReadEIDData.dateFormat}"
																				showApplyButton="false"
																				locale="#{pages$ReadEIDData.locale}"
																				enableManualInput="false"
																				inputStyle="width: 170px; height: 14px" />
																		</td>
																		<td>
																			<h:selectBooleanCheckbox id="chkDateOfBirth"
																				binding="#{pages$ReadEIDData.chkDateOfBirth}">
																			</h:selectBooleanCheckbox>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['customer.gender']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="selectGender"
																				value="#{pages$ReadEIDData.person.gender}"
																				readonly="true">
																				<f:selectItem
																					itemLabel="#{msg['tenants.gender.male']}"
																					itemValue="M" />
																				<f:selectItem
																					itemLabel="#{msg['tenants.gender.female']}"
																					itemValue="F" />
																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:selectBooleanCheckbox id="chkGender"
																				binding="#{pages$ReadEIDData.chkGender}">
																			</h:selectBooleanCheckbox>
																		</td>
																	</tr>

																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['customer.nationality']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="txtNationality" readonly="true"
																				style="WIDTH: 30px" styleClass="READONLY"
																				value="#{pages$ReadEIDData.person.eidNationalityEn}">
																			</h:inputText>
																			<h:selectOneMenu id="nationality" immediate="false"
																				style="WIDTH: 160px"
																				value="#{pages$ReadEIDData.person.nationalId}"
																				binding="#{pages$ReadEIDData.cmbNationality}"
																				valueChangeListener="#{pages$ReadEIDData.onNationalityChanged}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.countryList}" />
																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:selectBooleanCheckbox id="chkNationality"
																				binding="#{pages$ReadEIDData.chkNationality}">
																			</h:selectBooleanCheckbox>
																		</td>
																	</tr>


																	<tr>

																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['customer.cell']}:"></h:outputLabel>

																		</td>
																		<td>
																			<h:inputText id="txtcellNumber"
																				value="#{pages$ReadEIDData.person.cellNumber}"
																				style="width:570px;">
																			</h:inputText>
																		</td>
																		<td>
																			<h:selectBooleanCheckbox id="chkCellNumber"
																				binding="#{pages$ReadEIDData.chkCellNumber}">
																			</h:selectBooleanCheckbox>
																		</td>
																	</tr>


																	<tr>

																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['SearchBeneficiary.passportNumber']}:"></h:outputLabel>

																		</td>
																		<td>
																			<h:inputText id="txtpassportNumber"
																				value="#{pages$ReadEIDData.person.passportNumber}"
																				style="width:570px;">
																			</h:inputText>
																		</td>
																		<td>
																			<h:selectBooleanCheckbox id="chkPassportNumber"
																				binding="#{pages$ReadEIDData.chkPassportNumber}">
																			</h:selectBooleanCheckbox>
																		</td>
																	</tr>
																	<tr>

																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.passport.issueplace']}:"></h:outputLabel>

																		</td>
																		<td>
																			<h:inputText id="txtpassportIssuePlace"
																				readonly="true" style="WIDTH: 30px"
																				styleClass="READONLY"
																				value="#{pages$ReadEIDData.person.eidPassportIssuePlace}">
																			</h:inputText>
																			<h:selectOneMenu id="passportIssuePlace"
																				immediate="false" style="WIDTH: 160px"
																				value="#{pages$ReadEIDData.person.passportIssuePlaceIdString}"
																				valueChangeListener="#{pages$ReadEIDData.onPassportPlaceChanged}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.countryList}" />
																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:selectBooleanCheckbox id="chkPassportIssuePlace"
																				binding="#{pages$ReadEIDData.chkPassportIssuePlace}">
																			</h:selectBooleanCheckbox>
																		</td>
																	</tr>


																</table>
																<t:div id="buttonDiv" styleClass="BUTTON_TD">

																	<h:commandButton type="button" styleClass="BUTTON"
																		value="#{msg['Tool.Scan']}"
																		onclick="javaScript:doReadPublicData();"></h:commandButton>
																	<h:commandLink id="lnkScanEmiratesCard"
																		action="#{pages$ReadEIDData.onScanEmiratesCard}"></h:commandLink>


																	<h:commandButton id="btnDone" styleClass="BUTTON"
																		binding="#{pages$ReadEIDData.btnDone}"
																		value="#{msg['commons.done']}" style="width: auto;"
																		action="#{pages$ReadEIDData.onSendBack}" />




																</t:div>

															</td>
														</tr>


													</table>

												</div>

											</div>



										</div>


										</div>
									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>



		</body>
	</html>
</f:view>