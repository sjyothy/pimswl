
<t:div id="tabDDetails" style="width:100%;">
	<t:panelGrid id="ddTab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:outputLabel id="lblRefNum" styleClass="LABEL"
			value="#{msg['commons.refNum']}" />
		<h:inputText id="txtRefNum" maxlength="20" readonly="true"
			styleClass="READONLY"
			value="#{pages$complaintDetails.requestView.complaintDetailsView.complaintNumber}" />

		<h:outputLabel id="lblstatus" styleClass="LABEL"
			value="#{msg['commons.status']}" />
		<h:inputText id="status" maxlength="20" readonly="true"
			styleClass="READONLY"
			value="#{pages$complaintDetails.englishLocale?
															pages$complaintDetails.requestView.complaintDetailsView.statusEn:
															pages$complaintDetails.requestView.complaintDetailsView.statusAr
					}" />

		<h:outputLabel id="lblOtherRefNum" styleClass="LABEL"
			value="#{msg['complaint.lbl.otherSystemRefNum']}" />
		<h:inputText id="txtOtherRefNum"
			value="#{pages$complaintDetails.requestView.complaintDetailsView.complaintNumberFromOtherSystem}" />
			
		<h:outputLabel styleClass="LABEL"
			value="#{msg['commons.originalReqNum']}:"></h:outputLabel>
		<h:inputText id="originalRequestNum"
			value="#{pages$complaintDetails.requestView.originalRequestNum}"
			maxlength="20"></h:inputText>
			
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="typeCol" styleClass="LABEL"
				value="#{msg['commons.typeCol']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="complaintDetailsViewtype"
			value="#{pages$complaintDetails.requestView.complaintDetailsView.type}">
			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems value="#{pages$ApplicationBean.complaintTypes}" />
		</h:selectOneMenu>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['grp.propertyRefNumber']}:" />
		</h:panelGroup>
		<h:panelGroup>
			<h:inputText styleClass="READONLY" id="txtpropertyNumber"
				style="width:170px;" readonly="true"
				value="#{pages$complaintDetails.requestView.complaintDetailsView.propertyNumber}"></h:inputText>
			<h:graphicImage url="../resources/images/app_icons/Search-tenant.png"
				onclick="javaScript:showPropertySearchPopUp();"
				style="MARGIN: 0px 0px -4px;"
				title="#{msg['maintenanceRequest.searchProperties']}"
				alt="#{msg['maintenanceRequest.searchProperties']}"></h:graphicImage>
		</h:panelGroup>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL" value="#{msg['unit.unitNumber']}:" />
		</h:panelGroup>
		<h:panelGroup>
			<h:inputText maxlength="20" styleClass="READONLY" id="txtunitNumber"
				style="width:170px;" readonly="true"
				value="#{pages$complaintDetails.requestView.complaintDetailsView.unitNumber}"></h:inputText>
			<h:graphicImage url="../resources/images/app_icons/Search-tenant.png"
				onclick="javaScript:showUnitSearchPopUp();"
				style="MARGIN: 0px 0px -4px;"
				title="#{msg['maintenanceRequest.searchUnits']}"
				alt="#{msg['maintenanceRequest.searchUnits']}"></h:graphicImage>
			<h:graphicImage
				rendered="#{! empty pages$complaintDetails.requestView.complaintDetailsView.unitId }"
				url="../resources/images/app_icons/No-Objection-letter.png"
				onclick="javaScript:openEditUnitPopup(#{pages$complaintDetails.requestView.complaintDetailsView.unitId});"
				style="MARGIN: 0px 0px -4px;" title="#{msg['unit.details']}"
				alt="#{msg['unit.details']}"></h:graphicImage>
		</h:panelGroup>

		<h:panelGroup>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['commons.Contract.Number']}:" />
		</h:panelGroup>
		<h:panelGroup>
			<h:inputText styleClass="READONLY" id="txtContractNumber"
				readonly="true" style="width:170px;"
				value="#{pages$complaintDetails.requestView.complaintDetailsView.contractNumber}"></h:inputText>
			<h:graphicImage url="../resources/images/app_icons/Search-tenant.png"
				onclick="javaScript:showContractSearchPopUp();"
				style="MARGIN: 0px 0px -4px;"
				title="#{msg['contractFollowUp.searchContract']}"
				alt="#{msg['contractFollowUp.searchContract']}"></h:graphicImage>
			<h:graphicImage
				rendered="#{! empty pages$complaintDetails.requestView.complaintDetailsView.contractId}"
				url="../resources/images/app_icons/No-Objection-letter.png"
				onclick="javaScript:openLeaseContractPopup( #{pages$complaintDetails.requestView.complaintDetailsView.contractId} );"
				style="MARGIN: 0px 0px -4px;"
				title="#{msg['contract.LeaseContract']}"
				alt="#{msg['contract.LeaseContract']}"></h:graphicImage>
		</h:panelGroup>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['commons.description']}" />
		<t:panelGroup colspan="3" style="width: 100%">
			<t:inputTextarea style="width: 90%;" id="description"
				value="#{pages$complaintDetails.requestView.complaintDetailsView.description}" />
		</t:panelGroup>
	</t:panelGrid>

</t:div>


