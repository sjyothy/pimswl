<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript">

</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<script language="javascript" type="text/javascript">
	function onProcessStart()
	{
		    document.getElementById('formClaimedTasks:disablingDiv').style.display='block';
			return true;
	}
	function onProcessComplete()
	{
	 document.getElementById('formClaimedTasks:disablingDiv').style.display='none';
	}
</script>

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>

		<body class="BODY_STYLE">
			<TABLE>
				<tr width="100%">

					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['releaseTask.heading']}"
										styleClass="HEADER_FONT" style="padding:10px" />
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg">
								</td>


								<td width="100%" height="99%" valign="top" nowrap="nowrap">
									<div class="MARGIN">
										<div class="SCROLLABLE_SECTION"
											style="height: 445px; # height: 450px;">
											<h:form id="formClaimedTasks"
												style="width:99%;#width:96%; padding-left:5px;padding-right:5px;">
												<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
												<table border="0" class="layoutTable"
													style="margin-left: 15px; margin-right: 15px;">
													<tr>
														<td>
															<h:outputText
																value="#{pages$releaseTaskPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$releaseTaskPopup.infoMessage}"
																escape="false" styleClass="INFO_FONT" />
														</td>
													</tr>
												</table>

												<pims:security
													screen="Pims.Home.TaskList.ReleaseOtherUsersTasks"
													action="view">
													<t:div styleClass="DETAIL_SECTION">
														<t:panelGrid columns="4" styleClass="DETAIL_SECTION_INNER"
															cellpadding="1px" cellspacing="2px">

															<h:outputLabel styleClass="LABEL"
																value="#{msg['releaseTask.claimedBy']}:"></h:outputLabel>

															<h:selectOneMenu id="requestCreatedBy"
																value="#{pages$releaseTaskPopup.claimedByUserId}">

																<f:selectItems
																	value="#{pages$ApplicationBean.userNameList}" />
															</h:selectOneMenu>

														</t:panelGrid>
													</t:div>
												</pims:security>
												<table cellpadding="1px" cellspacing="2px" width="100%"
													class="BUTTON_TD">

													<tr>
														<td class="BUTTON_TD" width="100%">
															<pims:security
																screen="Pims.Home.TaskList.ReleaseOtherUsersTasks"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.search']}"
																	onclick="javaScript:onProcessStart();"
																	action="#{pages$releaseTaskPopup.onSearch}"
																	style="width: 75px" tabindex="7">
																</h:commandButton>
															</pims:security>
															<h:commandButton styleClass="BUTTON"
																value="#{msg['releaseTask.releaseAll']}"
																onclick="if (!confirm('#{msg['TaskList.messages.confirmRelease']}')) return false ; else javaScript:onProcessStart();"
																action="#{pages$releaseTaskPopup.onReleaseAll}"
																style="width: 75px" tabindex="7">
															</h:commandButton>
															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.clear']}"
																onclick="javascript:resetValues();" style="width: 75px"
																tabindex="7"></h:commandButton>
														</td>
													</tr>
												</table>
												<div class="contentDiv" width="99%">
													<t:dataTable width="100%" id="dtTaskList"
														value="#{pages$releaseTaskPopup.userTasks}"
														binding="#{pages$releaseTaskPopup.dataTable}"
														rows="#{pages$releaseTaskPopup.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="userTask" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true">

														<t:column id="dcTaskNumber" style="white-space:normal"
															sortable="true" width="5%">
															<f:facet name="header">
																<t:outputText
																	style="text-align:right;white-space:normal"
																	value="#{msg['TaskList.DataTable.TaskNumber']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{userTask.taskNumber}" />
														</t:column>
														<t:column id="dcInstanceId" style="white-space:normal"
															sortable="true" width="5%">
															<f:facet name="header">
																<t:outputText
																	style="text-align:right;white-space:normal"
																	value="#{msg['TaskList.DataTable.InstanceNumber']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{userTask.instanceId}" />
														</t:column>
														<t:column id="dcRequestTitleEn" style="white-space:normal"
															rendered="#{pages$releaseTaskPopup.englishLocale}"
															sortable="true" width="25%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['TaskList.DataTable.RequestTitle']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																style="white-space:normal"
																value="#{userTask.requestTitle_en}" />
														</t:column>
														<t:column style="padding-right:5px;white-space:normal"
															id="dcRequestTitleAr"
															rendered="#{!pages$releaseTaskPopup.englishLocale}"
															sortable="true" width="25%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['TaskList.DataTable.RequestTitle']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																style="white-space:normal"
																value="#{userTask.requestTitle_ar}" />
														</t:column>
														<t:column id="dcTaskTitleEn" style="white-space:normal"
															rendered="#{pages$releaseTaskPopup.englishLocale}"
															sortable="true" width="25%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['TaskList.DataTable.TaskTitle']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																style="white-space:normal"
																value="#{userTask.taskTitle_en}" />
														</t:column>
														<t:column style="padding-right:5px;white-space:normal"
															id="dcTaskTitleAr"
															rendered="#{!pages$releaseTaskPopup.englishLocale}"
															sortable="true" width="25%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['TaskList.DataTable.TaskTitle']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																style="white-space:normal"
																value="#{userTask.taskTitle_ar}" />
														</t:column>
														<t:column style="padding-right:5px;white-space:normal"
															id="dcContractNumber" sortable="true" width="25%">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.refNum']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																style="white-space:normal"
																value="#{userTask.contractNumber}" />
														</t:column>
														<t:column style="padding-right:5pxwhite-space:normal"
															id="dcTenantName" sortable="true" width="25%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['commons.replaceCheque.tenantName']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																style="white-space:normal"
																value="#{userTask.contractTenantNameEn}" />
														</t:column>

														<t:column style="padding-right:5px;white-space:normal"
															id="dcSentByUser" rendered="true" sortable="true"
															width="15%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['TaskList.DataTable.SentByUser']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																style="white-space:normal"
																value="#{userTask.sentByUser}" />
														</t:column>
														<t:column style="white-space:normal" id="dcAssignedDate"
															defaultSorted="true" sortable="true" width="15%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['TaskList.DataTable.AssignedDate']}" />
															</f:facet>
															<h:outputText style="white-space:normal"
																value="#{userTask.assignedDate}">
																<f:convertDateTime
																	pattern="#{pages$releaseTaskPopup.dateFormat}"
																	timeZone="#{pages$releaseTaskPopup.timeZone}" />
															</h:outputText>
														</t:column>
														<t:column style="white-space:normal" id="dcExpiryDate"
															rendered="false" sortable="true" defaultSorted="true"
															width="12%">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['TaskList.DataTable.ExpiryDate']}" />
															</f:facet>
															<h:outputText style="white-space:normal"
																value="#{userTask.expiryDate}">
																<f:convertDateTime
																	pattern="#{pages$releaseTaskPopup.dateFormat}"
																	timeZone="#{pages$releaseTaskPopup.timeZone}" />
															</h:outputText>

														</t:column>
														<t:column id="dcTaskId" width="4%" rendered="false"
															style="white-space:normal">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['TaskList.DataTable.TaskId']}" />
															</f:facet>
															<t:outputText style="white-space:normal"
																value="#{userTask.taskId}" />
														</t:column>

														<t:column id="col7" width="5%" style="white-space:normal">
															<f:facet name="header">
																<t:outputText style="text-align:right" value="" />
															</f:facet>

															<h:commandLink action="#{pages$releaseTaskPopup.Release}"
																onclick="if (!confirm('#{msg['TaskList.messages.confirmRelease']}')) return false ; else javaScript:onProcessStart();">
																<h:graphicImage id="releaseIcon"
																	title="#{msg['commons.release']}"
																	url="../resources/images/app_icons/Release.png" />
															</h:commandLink>

														</t:column>
													</t:dataTable>
												</div>
												<t:div styleClass="contentDivFooter">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$releaseTaskPopup.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 50%" align="right">
																<CENTER>
																	<t:dataScroller id="scroller" for="dtTaskList"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$releaseTaskPopup.paginatorMaxPages}"
																		immediate="false" styleClass="scroller"
																		paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		pageIndexVar="pageNumber"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																		style="text-align:right;padding-left:0px;#padding-left:700px;padding-right:100px;"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false">
																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL"></t:graphicImage>
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG" style="width:300">

																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.pageNumber}" />
																					</td>
																				</tr>
																			</table>

																		</t:div>
																	</t:dataScroller>
																</CENTER>
															</td>
														</tr>
													</table>
												</t:div>
												<div>
											</h:form>
										</div>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>

		</body>
	</html>
</f:view>
