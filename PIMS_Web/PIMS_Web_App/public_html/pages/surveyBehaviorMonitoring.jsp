<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	function surveyOptionCheckedUnchecked( checkControl )
	{
	
		if (checkControl.checked)
		{
			//optionOne[0]
		   var rowNum = checkControl.id.split("[");
		   rowNum =  rowNum[1].split("]");
		   
		   if(checkControl.id!='optionOne['+rowNum[0]+']')
		   {
		   	
		     document.getElementById('optionOne['+rowNum[0]+']').checked=false;
		     //alert(document.getElementById('optionOne['+rowNum[0]+']'));
		   }
		   if(checkControl.id!='optionTwo['+rowNum[0]+']')
		   {
		   
		     document.getElementById('optionTwo['+rowNum[0]+']').checked=false;
		     //alert(document.getElementById('optionTwo['+rowNum[0]+']'));
		   }
		   if(checkControl.id!='optionThree['+rowNum[0]+']')
		   {
		     document.getElementById('optionThree['+rowNum[0]+']').checked=false;
		     //alert(document.getElementById('optionThree['+rowNum[0]+']'));
		   }
		   if(checkControl.id!='optionFour['+rowNum[0]+']')
		   {
		     document.getElementById('optionFour['+rowNum[0]+']').checked=false;
		     //alert(document.getElementById('optionFour['+rowNum[0]+']'));
		   }
			
		}
	}
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++)
		{
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		       )
		    {
		        inputs[i].disabled = true;
		    }
		}
	}
	
	function onMessageFromAddSurvey()
	{
	  
	  disableInputs();
	  document.getElementById("detailsFrm:onMessageFromAddSurvey").onclick();
	}
	

	
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<c:choose>
						<c:when test="${!pages$surveyBehaviorMonitoring.viewModePopUp}">
							<tr
								style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>


					<tr width="100%">
						<c:choose>
							<c:when test="${!pages$surveyBehaviorMonitoring.viewModePopUp}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['researchFamilyVillageBeneficiary.lbl.BehaviorAspect.BehaviorMonitoringForm']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>
																<h:outputText id="errorId"
																	value="#{pages$surveyBehaviorMonitoring.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$surveyBehaviorMonitoring.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$surveyBehaviorMonitoring.pageMode}"></h:inputHidden>
																<h:commandLink id="onMessageFromAddSurvey"
																	action="#{pages$surveyBehaviorMonitoring.onMessageFromAddSurvey}" />

															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<t:div rendered="true" style="width:100%;">
														<t:panelGrid cellpadding="1px" width="100%"
															cellspacing="5px" columns="4">

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.Name']}:" />
															<t:panelGroup>
																<h:inputText id="txtName" readonly="true"
																	value="#{pages$surveyBehaviorMonitoring.beneficiaryPerson.personFullName}"
																	styleClass="READONLY" />
															</t:panelGroup>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.gender']}:"></h:outputLabel>

															<h:selectOneMenu id="selectGender" disabled="true"
																value="#{pages$surveyBehaviorMonitoring.beneficiaryPerson.gender}">
																<f:selectItem itemLabel="#{msg['tenants.gender.male']}"
																	itemValue="M" />
																<f:selectItem
																	itemLabel="#{msg['tenants.gender.female']}"
																	itemValue="F" />
															</h:selectOneMenu>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.dateOfBirth']}:" />
															<rich:calendar id="DateOfBirth"
																value="#{pages$surveyBehaviorMonitoring.beneficiaryPerson.dateOfBirth}"
																popup="true"
																datePattern="#{pages$surveyBehaviorMonitoring.dateFormat}"
																showApplyButton="false"
																locale="#{pages$surveyBehaviorMonitoring.locale}"
																enableManualInput="false"
																inputStyle="width: 170px; height: 14px" />

															<h:outputLabel id="age" styleClass="LABEL"
																value="#{msg['familyVillageManageBeneficiary.lbl.age']}:"></h:outputLabel>

															<h:inputText readonly="true" id="ageTextIT"
																styleClass="READONLY"
																value="#{pages$surveyBehaviorMonitoring.beneficiaryPerson.age}" />
														</t:panelGrid>
														<br />
													</t:div>
													<t:div styleClass="BUTTON_TD"
														style="padding-top:5px; padding-right:21px;">

														<h:commandButton id="btnBack"
															styleClass="BUTTON" style="width:auto;"
															value="#{msg['commons.back']}"
															action="#{pages$surveyBehaviorMonitoring.onBackToCallingPage}">
														</h:commandButton>
														


													</t:div>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$surveyBehaviorMonitoring.tabPanel}"
																style="width: 100%">


																
																<rich:tab id="tabSurveys"
																	label="#{msg['survey.lbl.title']}"
																	action="#{pages$surveyBehaviorMonitoring.onSurveyHistoryTab}">
																	<%@ include file="tabSurveyInstanceHistory.jsp"%>
																</rich:tab>
																

															</rich:tabPanel>
														</div>

													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<c:choose>
								<c:when test="${!pages$surveyBehaviorMonitoring.viewModePopUp}">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
								</c:when>
							</c:choose>


						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>