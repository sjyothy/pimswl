


<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" 
     style="overflow:hidden;">
		<head>
			<title>PIMS</title>
     <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>	


			<script language="javascript" type="text/javascript">
  
   
  
	 function sendToParent()//tenantId,tenantNum,typeId)
	{
	   
      alert("in here"); 
     //  window.opener.populateTenant(tenantId,tenantNum,typeId);
       window.opener.document.forms[0].submit();
		window.close();
	  
	}
	
		function clearWindow()
	{


           var cmbProcedureType=document.getElementById("formFeeConfigList:selectProcedureType");
		       cmbProcedureType.selectedIndex = 0;  
		   
		   var cmbFeeType=document.getElementById("formFeeConfigList:selectFeeType");
		       cmbFeeType.selectedIndex = 0;  
		     
	        document.getElementById("formFeeConfigList:txtAccountNo").value="";
		    document.getElementById("formFeeConfigList:txtPercentage").value="";
		         
	       var cmbBasedType=document.getElementById("formFeeConfigList:selectBasedOn");
		       cmbBasedType.selectedIndex = 0;       
	       
	        document.getElementById("formFeeConfigList:txtMinAmount").value="";
		    document.getElementById("formFeeConfigList:txtMaxAmount").value="";
		    }
	
    </script>

			<%

   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1

   response.setHeader("Pragma","no-cache"); //HTTP 1.0

   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

%>



		</head>
		<body class="BODY_STYLE">
    <!-- Header --> 
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="2">
				<jsp:include page="header.jsp" />
			</td>
		</tr>
		<tr width="100%">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
			<td width="83%" height="505px" valign="top" class="divBackgroundBody">
				<table width="99.2%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['feeConfiguration.feeConfigurationList']}" styleClass="HEADER_FONT"/>
						</td>
						<td width="100%">
							&nbsp;
						</td>
					</tr>
				</table>
				<table width="99%" class="greyPanelMiddleTable" cellpadding="0"	cellspacing="0" border="0" height="100%">
					<tr valign="top">
						<td height="100%" valign="top" nowrap="nowrap"
							background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
							width="1">
						</td>
						<td width="100%" height="460px" valign="top" nowrap="nowrap">
								
								
								<div style="padding-bottom:7px;padding-left:10px;padding-right:7;padding-top:7px;">
									<h:form id="formFeeConfigList" style="width:99%;">
									<table border="0" class="layoutTable">
												<tr>
      									    	      <td colspan="6">
														<h:outputText value="#{pages$FeeConfigurationList.errorMessages}" styleClass="INFO_FONT" style="padding:10px;"/>
													  </td>
												</tr>
									</table>
									
									
									  <div class= "MARGIN">
									  <table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
									  <div class="DETAIL_SECTION">
									  	<h:outputLabel value="#{msg['feeConfiguration.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
										<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
											
											<tr>
											
												<td >
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfiguration.procedureType']}:"></h:outputLabel>
												</td>
												<td style="padding-right:2px;">

													<h:selectOneMenu id="selectProcedureType" style="width:192px;"
														value="#{pages$FeeConfigurationList.selectOneProcedureType}">
														<f:selectItem itemLabel="#{msg['commons.All']}"
															itemValue="-1" />
														<f:selectItems
															value="#{pages$ApplicationBean.procedureType}" />
													</h:selectOneMenu>

												</td>
																								<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfiguration.feeType']}:"></h:outputLabel>
												</td>
												<td style="padding-right:2px;">

													<h:selectOneMenu id="selectFeeType" style="width:190px;"
														value="#{pages$FeeConfigurationList.selectOneFeeType}">
														<f:selectItem itemLabel="#{msg['commons.All']}"
															itemValue="-1" />
														<f:selectItems
															value="#{pages$ApplicationBean.feeType}" />
													</h:selectOneMenu>

												</td>
												
											</tr>
											<tr>
												
												<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfiguration.accountNo']}:"></h:outputLabel>
												</td>
												<td >
													<h:inputText id="txtAccountNo" styleClass="A_LEFT"
														value="#{pages$FeeConfigurationList.accountNo}"  style="width:186px"/>

												</td>
												<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfiguration.percentage']}:"></h:outputLabel>
												</td>
												<td >
													<h:inputText id="txtPercentage" styleClass="A_RIGHT_NUM"
														value="#{pages$FeeConfigurationList.percentage}" style="width:186px">

													</h:inputText>
												</td>
												
												
											</tr>
											<tr>
												
												<td >
													<h:outputLabel  styleClass="LABEL" value="#{msg['feeConfiguration.basedOn']}:"></h:outputLabel>
												</td>
												<td style="padding-right:2px;">

													<h:selectOneMenu id="selectBasedOn" style="width:192px;"
														value="#{pages$FeeConfigurationList.selectOnebasedOn}">
														<f:selectItem itemLabel="#{msg['commons.All']}"
															itemValue="-1" />
														<f:selectItems
															value="#{pages$ApplicationBean.basedOnType}" />
													</h:selectOneMenu>

												</td>
												<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfiguration.minAmount']}:"></h:outputLabel>
												</td>
												<td>
													<h:inputText id="txtMinAmount" styleClass="A_RIGHT_NUM"
														value="#{pages$FeeConfigurationList.minAmount}" style="width:186px">

													</h:inputText>
												</td>
												
												

											</tr>

											<tr>
												
												<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfiguration.maxAmount']}:"></h:outputLabel>
												</td>
												<td>
													<h:inputText id="txtMaxAmount" styleClass="A_RIGHT_NUM"
														value="#{pages$FeeConfigurationList.maxAmount}" style="width:187px">

													</h:inputText>
												</td>
											
											</tr>
											
											<tr>
												
												<td colspan="4" class="BUTTON_TD">
												<pims:security screen="Pims.Home.FeeConfiguration.FeeConfigurationList" action="create">
												      <h:commandButton styleClass="BUTTON" type= "submit" 
									                  value="#{msg['commons.Add']}" 
									                  action="#{pages$FeeConfigurationList.btnAddFee_Click}" >	</h:commandButton>
									            </pims:security>      
													<h:commandButton styleClass="BUTTON" type="submit"
														value="#{msg['commons.search']}" 
														action="#{pages$FeeConfigurationList.btnSearch_Click}">
													</h:commandButton>
													<h:commandButton styleClass="BUTTON" type="button"
														onclick="javascript:clearWindow();"
														value="#{msg['commons.reset']}">
													</h:commandButton>
												</td>
											</tr>

											<h:inputHidden id="hdnMode"
												value="#{pages$FeeConfigurationList.hdnMode}" />
										</table>
									</div>
								</div>
                                    <div style="padding-bottom:7px;padding-left:10px;padding-right:0;padding-top:7px;">   
                                      <div class="imag">&nbsp;</div>
										<div class="contentDiv" style="width:98%"  align="center">
											<t:dataTable id="test2"
												value="#{pages$FeeConfigurationList.gridDataList}"
												binding="#{pages$FeeConfigurationList.dataTable}" rows="#{pages$FeeConfigurationList.paginatorRows}"
												preserveDataModel="false" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true" columnClasses="A_LEFT_GRID,A_LEFT_GRID,1,1,A_LEFT_GRID,1,1"
												width="100%">

                                             <t:column id="procedureTypeEn" sortable="true" rendered="#{pages$FeeConfigurationList.isEnglishLocale}">


													<f:facet name="header">

														<t:outputText style="width:20%" value="#{msg['feeConfiguration.procedureType']}" />

													</f:facet>
													<t:outputText value="#{dataItem.procedureTypeEn}" styleClass="A_LEFT" style="white-space: normal;" />
												</t:column>
												<t:column id="procedureTypeAr" sortable="true"  rendered="#{pages$FeeConfigurationList.isArabicLocale}">


													<f:facet name="header">

														<t:outputText value="#{msg['feeConfiguration.procedureType']}" />

													</f:facet>
													<t:outputText value="#{dataItem.procedureTypeAr}" styleClass="A_LEFT" style="white-space: normal;"/>
												</t:column>

												<t:column id="feeTypeEn" sortable="true"  rendered="#{pages$FeeConfigurationList.isEnglishLocale}">


													<f:facet name="header">

														<t:outputText value="#{msg['feeConfiguration.feeType']}" />

													</f:facet>
													<t:outputText value="#{dataItem.feeTypeEn}" styleClass="A_LEFT"  style="white-space: normal;"/>
												</t:column>
												<t:column id="feeTypeAr" sortable="true" rendered="#{pages$FeeConfigurationList.isArabicLocale}">


													<f:facet name="header">

														<t:outputText value="#{msg['feeConfiguration.feeType']}" />

													</f:facet>
													<t:outputText value="#{dataItem.feeTypeAr}" />
												</t:column>
                                              
                                              <t:column id="AccountNo" >
													<f:facet name="header">


														<t:outputText value="#{msg['feeConfiguration.accountNo']}" />

													</f:facet>
													<t:outputText value="#{dataItem.grpAccountNo}" styleClass="A_LEFT" />
												</t:column>
												
												<t:column id="percentage" >
													<f:facet name="header">


														<t:outputText value="#{msg['feeConfiguration.percentage']}" />

													</f:facet>
													<t:outputText value="#{dataItem.percent}" styleClass="A_RIGHT_NUM" />
												</t:column>
												
												<t:column id="basedOnEn" sortable="true"  rendered="#{pages$FeeConfigurationList.isEnglishLocale}">
													<f:facet name="header">


														<t:outputText value="#{msg['feeConfiguration.basedOn']}" />

													</f:facet>
													<t:outputText value="#{dataItem.basedOnTypeEn}" styleClass="A_LEFT" style="white-space: normal;"/>
												</t:column>
												<t:column id="basedOnAr" sortable="true"  rendered="#{pages$FeeConfigurationList.isArabicLocale}">
													<f:facet name="header">


														<t:outputText value="#{msg['feeConfiguration.basedOn']}" />

													</f:facet>
													<t:outputText value="#{dataItem.basedOnTypeAr}" />
												</t:column>
												<t:column id="minAmount" >
													<f:facet name="header">


														<t:outputText value="#{msg['feeConfiguration.minAmount']}" />

													</f:facet>
													<t:outputText value="#{dataItem.minAmount}"  styleClass="A_RIGHT_NUM"/>
												</t:column>
												<t:column id="maxAmount" >
													<f:facet name="header">


														<t:outputText value="#{msg['feeConfiguration.maxAmount']}" />

													</f:facet>
													<t:outputText value="#{dataItem.maxAmount}" styleClass="A_RIGHT_NUM" />
												</t:column>
											<pims:security screen="Pims.Home.FeeConfiguration.FeeConfigurationList" action="create">	
												<t:column id="deletebtn" >
														<f:facet name="header" >
															<t:outputText value="#{msg['commons.delete']}"/>
														</f:facet>
														<t:commandLink  action="#{pages$FeeConfigurationList.cmdDelete_Click}">&nbsp;
														 <h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
														  </t:commandLink>
														<t:outputText />
										    </t:column>
										    </pims:security>
										  <pims:security screen="Pims.Home.FeeConfiguration.FeeConfigurationList" action="create">	
										<t:column id="statusbtn" >
											<f:facet name="header">
												<t:outputText value="#{msg['commons.edit']}" />
											</f:facet>
											
										  <t:commandLink     action="#{pages$FeeConfigurationList.cmdEdit_Click}">&nbsp;
											 <h:graphicImage id="EditIcon" title="#{msg['commons.edit']}" url="../resources/images/edit-icon.gif" />&nbsp;
										  </t:commandLink>
										  
										 
											<t:outputText />
										</t:column>	
										</pims:security>
											</t:dataTable>
										</div>
										
										
									<div class="contentDivFooter" style="width:100%;" align="right">
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$FeeConfigurationList.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
												<CENTER>
											<t:dataScroller id="scroller" for="test2" paginator="true"
												fastStep="1" paginatorMaxPages="#{pages$FeeConfigurationList.paginatorMaxPages}" immediate="false"
												paginatorTableClass="paginator"
												renderFacetsIfSinglePage="true"
												pageIndexVar="pageNumber"
												styleClass="SCH_SCROLLER"
												paginatorActiveColumnStyle="font-weight:bold;"
												paginatorRenderLinkForActive="false" 
												paginatorTableStyle="grid_paginator" layout="singleTable" 
												paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

												        <f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
												<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>
											</t:dataScroller>
											</CENTER>
												</td></tr>
											</table>
										</div> 
									</div>
									    
							</h:form>
					   </div>
					   </td>
					   </tr>
					   </table>
					   </td>
					   </tr>
					   <tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
					   </table>
					   
		</body>
</f:view>

</html>
