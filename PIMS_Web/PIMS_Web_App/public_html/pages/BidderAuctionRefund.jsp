
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="avanza-security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="javascript" type="text/javascript">
	function closeWindow()
	{		
	  	window.close();	  
	}	
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
				<title>PIMS</title>
				<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
				<meta http-equiv="pragma" content="no-cache">
				<meta http-equiv="cache-control" content="no-cache">
				<meta http-equiv="expires" content="0">
				<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
				<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
		</head>
		<h:inputHidden id="hiddenRequestId">

		</h:inputHidden>

	<body dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<%--<jsp:include page="header.jsp" />--%>
					</td>
				</tr>

				<tr width="100%">
					<!--td class="divLeftMenuWithBody" width="17%">
        < jsp:include page="leftmenu.jsp"/>
    </td-->
					<td width="100%" valign="top" class="divBackgroundBody" colspan="2">
						<table width="100%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['auction.refund.requestDepositRefund']}" styleClass="HEADER_FONT"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="100%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<!--Use this below only for desiging forms Please do not touch other sections -->

								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<h:form id="conductAuctionForm">
										<table width="100%">
											<tr>
												<td colspan="6">
													<div class="MARGIN"> 
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
														<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
														<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
														</tr>
													</table>
													<div class="DETAIL_SECTION">
														<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" border="0">

														<tr>
															<td>
																<h:outputLabel value="#{msg['auction.number']}"></h:outputLabel>
															</td>
															<td>
																<h:inputText
																	value="#{pages$BidderAuctionRefund.bidderAuctionRegView.auctionView.auctionNumber}"
																	readonly="true">
																</h:inputText>
															</td>															
															<td>
																<h:outputLabel value="#{msg['auction.title']}"></h:outputLabel>
															</td>
															<td>
																<h:inputText
																	value="#{pages$BidderAuctionRefund.bidderAuctionRegView.auctionView.auctionTitle}"
																	readonly="true">
																</h:inputText>
															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel value="#{msg['bidder.bidderFirstName']}"></h:outputLabel>
															</td>
															<td>
																<h:inputText
																	value="#{pages$BidderAuctionRefund.bidderAuctionRegView.bidderView.firstName}"
																	readonly="true">
																</h:inputText>
															</td>
															<td>
																<h:outputLabel value="#{msg['bidder.bidderLastName']}"></h:outputLabel>
															</td>
															<td>
																<h:inputText
																	value="#{pages$BidderAuctionRefund.bidderAuctionRegView.bidderView.lastName}"
																	readonly="true">
																</h:inputText>
															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel value="#{msg['bidder.bidderPassportNo']}"></h:outputLabel>
															</td>
															<td>
																<h:inputText
																	value="#{pages$BidderAuctionRefund.bidderAuctionRegView.bidderView.passportNumber}"
																	readonly="true">
																</h:inputText>
															</td>
																														<td>
																<h:outputLabel value="#{msg['bidder.bidderSSN']}"></h:outputLabel>
															</td>
															<td>
																<h:inputText
																	value="#{pages$BidderAuctionRefund.bidderAuctionRegView.bidderView.socialSecNumber}"
																	readonly="true">
																</h:inputText>
															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel value="#{msg['auction.totalDepositAmount']}"></h:outputLabel>
															</td>
															<td>
																<h:inputText
																	value="#{pages$BidderAuctionRefund.bidderAuctionRegView.totalDepositAmount}"
																	readonly="true" style="text-align:right">
																</h:inputText>
															</td>
															<td>
																<h:outputLabel value="#{msg['auction.refund.confiscatedAmount']}"></h:outputLabel>
															</td>
															<td>
																<h:inputText style="text-align:right"
																	binding="#{pages$BidderAuctionRefund.inputTextConfiscatedAmount}">
																</h:inputText>
															</td>
														</tr>
														<tr align="right">
															<td colspan="4" class="BUTTON_TD">
																<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}"
																	onclick="javascript:closeWindow();"></h:commandButton>
			
																<h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}"
																	actionListener="#{pages$BidderAuctionRefund.saveAuctionRequest}"></h:commandButton>
															</td>
														</tr>

													</table>
													</div></div>
												</td>
											</tr>

											<tr>
												<td width="100%" colspan="6">

												</td>
											</tr>
	
											<tr>
												<td colspan="6">
															<br><br>
															<div class="MARGIN">
															<div class="imag">&nbsp;</div>
															<div class="contentDiv" >
															<t:dataTable id="test2" width="100%"
																value="#{pages$BidderAuctionRefund.auctionUnitList}"
																binding="#{pages$BidderAuctionRefund.auctionUnitTable}" rows="15"
																preserveDataModel="false" preserveSort="false"
																var="dataItem" rules="all" renderedIfEmpty="true">

																<t:column id="col1" width="80">
																	<f:facet name="header">
																		<t:outputText value="#{msg['property.number']}" />
																	</f:facet>
																	<t:outputText value="#{dataItem.propertyNumber}" />
																</t:column>
																<t:column id="col2" width="80">
																	<f:facet name="header">
																		<t:outputText value="#{msg['property.commercialName']}" />
																	</f:facet>
																	<t:outputText value="#{dataItem.propertyCommercialName}" />
																</t:column>
																<t:column id="col3" width="80">
																	<f:facet name="header">
																		<t:outputText value="#{msg['unit.number']}" />
																	</f:facet>
																	<t:outputText value="#{dataItem.unitNumber}" />
																</t:column>
																	<t:column id="col4" width="80">
																	<f:facet name="header">
																		<t:outputText value="#{msg['unit.type']}" />
																	</f:facet>
																	<t:outputText value="#{dataItem.unitTypeEn}" />
																</t:column>
																	<t:column id="col5" width="80">
																	<f:facet name="header">
																		<t:outputText value="#{msg['unit.usage']}" />
																	</f:facet>
																	<t:outputText value="#{dataItem.unitUsageTypeEn}" />
																</t:column>
																	<t:column id="col6" width="80">
																	<f:facet name="header">
																		<t:outputText value="#{msg['unit.rentValue']}" />
																	</f:facet>
																	<t:outputText  value="#{dataItem.rentValue}" />
																</t:column>
																<t:column id="col7" width="80">
																	<f:facet name="header">
																		<t:outputText value="#{msg['unit.depositAmount']}" />
																	</f:facet>
																	<t:outputText  value="#{dataItem.depositAmount}" />
																</t:column>
																<t:column id="col8" width="80">
																	<f:facet name="header">
																		<t:outputText value="#{msg['unit.OpeningPrice']}" />
																	</f:facet>
																	<t:outputText  value="#{dataItem.openingPrice}" />
																</t:column>
																	<t:column id="col9" width="80">
																	<f:facet name="header">
																		<t:outputText value="#{msg['unit.excluded']}" />
																	</f:facet>
																	<h:selectBooleanCheckbox value="#{dataItem.excluded}" />
																</t:column>
															</t:dataTable>
														</div>
														<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99.9%;" >
															<table cellpadding="0" cellspacing="0" width="100%">
																<tr>
																<td class="RECORD_NUM_TD">
																	<div class="RECORD_NUM_BG">
																		<table cellpadding="0" cellspacing="0">
																		<tr><td class="RECORD_NUM_TD">
																		<h:outputText value="#{msg['commons.recordsFound']}"/>
																		</td><td class="RECORD_NUM_TD">
																		<h:outputText value=" : "/>
																		</td><td class="RECORD_NUM_TD">
																		<h:outputText value="#{pages$BidderAuctionRefund.recordSize}"/>
																		</td></tr>
																		</table>
																	</div>
																</td>
																<td class="BUTTON_TD" style="width:50%">
			
																	<t:dataScroller id="scroller" for="test2" paginator="true"
																		fastStep="1" paginatorMaxPages="2" immediate="false"
																		styleClass="scroller" paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true" 												
																		paginatorTableStyle="grid_paginator" layout="singleTable" 
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																		paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																		pageIndexVar="pageNumber">
									
																		<f:facet name="first">																			
																			<t:graphicImage url="../resources/images/first_btn.gif" id="lblF"></t:graphicImage>
																		</f:facet>									
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>
																		</f:facet>									
																		<f:facet name="fastforward">
																			<t:graphicImage url="../resources/images/next_btn.gif" id="lblFF"></t:graphicImage>
																		</f:facet>									
																		<f:facet name="last">
																			<t:graphicImage url="../resources/images/last_btn.gif" id="lblL"></t:graphicImage>
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>	
																						<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																					</td>
																					<td>	
																						<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																					</td>
																				</tr>					
																			</table>										
																		</t:div>	
																	</t:dataScroller>
																</td>
																</tr>
															</table>
														</t:div>
														</div>
</td>
											</tr>

										</table>
									</h:form>

								</td>
							</tr>

							<tr>
								<td>

								</td>
							</tr>
						</table>





					</td>
				</tr>

			</table>
		</body>
	</html>
</f:view>
