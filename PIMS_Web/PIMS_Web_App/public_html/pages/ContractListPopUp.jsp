
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden">

	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>	
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
	 								
<style>
.rich-calendar-input{
width:75%;
}
</style>
			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>
		

<script language="javascript" type="text/javascript">

	
	function closeWindow()
	{
	  window.opener="x";
	  window.close();
	}
	
//	function Calendar(control,control2,x,y)
 //     {

  //      var ctl2Name="formContractList:"+control2;
 //       var ctl2=document.getElementById(ctl2Name);
  //      showCalendar(control,ctl2,'dd/mm/yyyy','','',x,y); 
  //      return false;
  //    }
	
	function submitForm(){
       
		     //alert("In submitForm");    
		    //var cmbCountry=document.getElementById("formPerson:selectcountry");
		    //var i = 0;
		    //i=cmbCountry.selectedIndex;
		    //alert("value of i"+i);
		    //document.getElementById("formPerson:hdnStateValue").value=cmbCountry.options[i].value ;
		     
		    
		    //document.getElementById("formPerson:hdnCityValue").value= -1 ;
		     
		    document.forms[0].submit();
		    
		    //alert("End submitForm");
		    
		    }
	function RowDoubleClick(contractNumber,contractId)
	{
	
	     
	     window.opener.populateContract(contractNumber,contractId);
	    
	    window.opener.document.forms[0].submit();
		window.close();
	  
	}	
	
</SCRIPT>

</head>
	<body class="BODY_STYLE">
		<div style="width:1024px;">
			
			<table cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>	<td class="HEADER_TD">
										<h:outputLabel value= "#{msg['contract.header']}" styleClass="HEADER_FONT" style="padding:10px"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						
					
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" 
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									>
								</td>
								<td width="0%" height="99%" valign="top" nowrap="nowrap">
								<h:form id="formContractList">
									<div class="SCROLLABLE_SECTION" style="width:842px;">
									<div class="MARGIN" style="width:95.5%">
									<table cellpadding="0" cellspacing="0"  >
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>

										<div class="DETAIL_SECTION" style="width:99.6%;">
										<h:outputLabel value="#{msg['commons.search']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="0px" cellspacing="0px"
														class="DETAIL_SECTION_INNER">

														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.Contract.Number']}" />
																:
															</td>
															<td>
																<h:inputText id="contNum" style="width:85%"
																	value="#{pages$ContractListBackingBean.contractNumber}"></h:inputText>
															</td>
															<td></td>
															<td></td>
														</tr>

														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.fromContractDate']}"> :</h:outputLabel>

															</td>
															<td width="25%">
																
																<rich:calendar id="fromContractD"
																	datePattern="MM/dd/yyyy"
																	value="#{pages$ContractListBackingBean.fromContractDateRich}" ></rich:calendar>
															</td>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.toContractDate']}" />
																:

															</td>
															<td width="25%">

																<rich:calendar id="toConractD" datePattern="MM/dd/yyyy"
																	value="#{pages$ContractListBackingBean.toContractDateRich}"></rich:calendar>
															</td>

														</tr>



														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.date.fromExpiry']}" />
																:
															</td>
															<td>

																<rich:calendar id="fromExpD" datePattern="MM/dd/yyyy"
																	value="#{pages$ContractListBackingBean.fromContractExpDateRich}"></rich:calendar>
															</td>

															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.date.toExpiry']}"></h:outputLabel>
																:
															</td>
															<td>

																<rich:calendar id="toExpD" datePattern="MM/dd/yyyy"
																	value="#{pages$ContractListBackingBean.toContractExpDateRich}"></rich:calendar>
															</td>

														</tr>
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.tenantNumber']}"></h:outputLabel>
																:
															</td>
															<td><h:inputText id="tenantNumber" style="width:85%"
																	value="#{pages$ContractListBackingBean.tenantNumber}"></h:inputText>
															</td>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.username']}"></h:outputLabel>
																:
															</td>
															<td><h:inputText id="useName" style="width:85%"></h:inputText>
															</td>
														</tr>
														<tr>
															<td><h:outputLabel styleClass="LABEL"
																	value="#{msg['chequeList.tenantName']}"></h:outputLabel>
																:
															</td>
															<td><h:inputText id="tenantName" style="width:85%"
																	value="#{pages$ContractListBackingBean.tenantName}"></h:inputText>
															</td>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['tenants.tenantsType']}"></h:outputLabel>
																:
															</td>
															<td>
																<h:selectOneMenu id="tenatType" 
																	value="#{pages$ContractListBackingBean.tenantType}"
																	style="width:87%">
																	<f:selectItem id="selectAllTenantType" itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ContractListBackingBean.tenantTypeList}" />
                                         						</h:selectOneMenu>
															</td>
														</tr>
														<tr>
															<td><h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.property.Name']}"></h:outputLabel>
																:
															</td>
															<td><h:inputText id="propertyName" style="width:85%"
																	value="#{pages$ContractListBackingBean.propertyComercialName}"></h:inputText>
															</td>
															<td><h:outputLabel styleClass="LABEL"
																	value="#{msg['property.endowedName']}"></h:outputLabel>
																:
															</td>
															<td><h:inputText id="propertyEndowedName"
																	style="width:85%"
																	value="#{pages$ContractListBackingBean.propertyEndowedName}"></h:inputText>
															</td>
														</tr>
														<tr>
															<td><h:outputLabel styleClass="LABEL"
																	value="#{msg['contractList.property.commercialName']}" />
																:
															</td>
															<td><h:inputText id="propertyComercialName"
																	style="width:85%"
																	value="#{pages$ContractListBackingBean.propertyComercialName}"></h:inputText>
															</td>
															<td><h:outputLabel styleClass="LABEL"
																	value="#{msg['contractList.endowed/minorNumber']}"></h:outputLabel>
																:
															</td>
															<td><h:inputText id="endownMinorNumber"
																	style="width:85%"></h:inputText>
															</td>
														</tr>
														<tr>
															<td><h:outputLabel styleClass="LABEL"
																	value="#{msg['property.usage']}"></h:outputLabel>
																:
															</td>
															<td><h:selectOneMenu id="propUsage"
																	value="#{pages$ContractListBackingBean.propertyUsage}"
																	style="width:87%">
																	<f:selectItem id="selectAllUsageType" itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ContractListBackingBean.propertyUsageList}" />





																</h:selectOneMenu>
															</td>
															<td><h:outputLabel styleClass="LABEL"
																	value="#{msg['property.type']}"></h:outputLabel>
																:
															</td>
															<td><h:selectOneMenu id="propType"
																	value="#{pages$ContractListBackingBean.propertyType}"
																	style="width:87%">
																	<f:selectItem id="selectAllPropType" itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ContractListBackingBean.propertyTypeList}" />





																</h:selectOneMenu>
															</td>
														</tr>





														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.country']}" />
																:
															</td>
															<td>
																<h:selectOneMenu id="countries"
																	valueChangeListener="#{pages$ContractListBackingBean.loadState}"
																	style="width:87%" onchange="submitForm()"
																	value="#{pages$ContractListBackingBean.countries}">
																	<f:selectItem id="contryAll" itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{view.attributes['countryList']}" />





																</h:selectOneMenu>
															</td>

															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contactInfo.state']}"></h:outputLabel>
																:
															</td>
															<td>
																<h:selectOneMenu id="states"
																	value="#{pages$ContractListBackingBean.states}"
																	style="width:87%">
																	<f:selectItem id="statesAll" itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{view.attributes['stateList']}" />





																</h:selectOneMenu>
															</td>

														</tr>
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.city']}" />
																:
															</td>
															<td>
																<h:selectOneMenu id="city"
																	value="#{pages$ContractListBackingBean.city}"
																	style="width:87%">
																	<f:selectItem id="cityAll" itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ContractListBackingBean.cityList}" />





																</h:selectOneMenu>
															</td>

															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.street']}"></h:outputLabel>
																:
															</td>
															<td>
																<h:inputText id="streetInput" style="width:85%"
																	value="#{pages$ContractListBackingBean.street}"></h:inputText>
															</td>

														</tr>
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.fromRent']}"></h:outputLabel>
																:
															</td>
															<td>

																<h:inputText id="fromRent" styleClass="A_RIGHT_NUM"
																	style="width:85%"
																	value="#{pages$ContractListBackingBean.fromRentAmount}"></h:inputText>
															</td>

															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.toRent']}"></h:outputLabel>
																:

															</td>
															<td>

																<h:inputText id="toRent" styleClass="A_RIGHT_NUM"
																	style="width:85%"
																	value="#{pages$ContractListBackingBean.toRentAmount}"></h:inputText>
															</td>

														</tr>
														<tr>
															<td><h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.contractStatus']}"></h:outputLabel>
																:
															</td>
															<td><h:selectOneMenu id="contractStatus"
																	value="#{pages$ContractListBackingBean.contractStatus}"
																	style="width:87%">
																	<f:selectItem id="selectAll" itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ContractListBackingBean.statusList}" />





																</h:selectOneMenu>
															</td>
															<td><h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.contractType']}"></h:outputLabel>
																:
															</td>
															<td><h:selectOneMenu id="contractType"
																	value="#{pages$ContractListBackingBean.contractType}"
																	style="width:87%">
																	<f:selectItem id="selectAllType" itemValue="-1"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItems
																		value="#{pages$ContractListBackingBean.typeList}" />





																</h:selectOneMenu>
															</td>
														</tr>
														<tr>
															<td><h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.unitRefNo']}"></h:outputLabel>
																:
															</td>
															<td><h:inputText id="unitRefNo" style="width:85%"
																	value="#{pages$ContractListBackingBean.unitRefNum}"></h:inputText>
															</td>
															<td></td>
															<td></td>
														</tr>
														<tr>




															<td colspan="4">
																<DIV class="A_RIGHT" style="padding-bottom:4px;">

																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.search']}"
																		action="#{pages$ContractListBackingBean.searchContracts}"></h:commandButton>
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.reset']}"
																		action="#{pages$ContractListBackingBean.reset}"></h:commandButton>
																</DIV>
															</td>
														</tr>

													</table>
												</div>
												<t:div >&nbsp;</t:div>
												
										<t:div styleClass="imag">&nbsp;</t:div>
										<t:div styleClass="contentDiv"  style="width:99.2%;align:center;">

												<t:dataTable id="contractListDT" preserveSort="false" var="dataItem" rules="all"
														renderedIfEmpty="true" width="100%" rowClasses="row1,row2" 
														rows="#{pages$ContractListBackingBean.paginatorRows}" value="#{pages$ContractListBackingBean.contractList}"
														binding="#{pages$ContractListBackingBean.dataTable}">
						
													<t:column id="col1" sortable="true" style="width:10%;">
														<f:facet name="header">
															<t:outputText value="#{msg['contract.contractNumber']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.contractNumber}" style="white-space: normal;" />
													</t:column>
													
													<t:column id="col2" sortable="true" style="width:8%;" rendered="false">
														<f:facet name="header">
															<t:outputText value="#{msg['contract.contractDate']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.contractDateString}" style="white-space: normal;" />
													</t:column>
													
													<t:column id="colStarttDate" sortable="true" style="width:7%;">
														<f:facet name="header">
															<t:outputText value="#{msg['contract.startDate']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.startDateString}" style="white-space: normal;" />
													</t:column>
													
													<t:column id="col3" sortable="true"  style="width:7%;">
														<f:facet name="header">
															<t:outputText value="#{msg['contract.endDate']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.endDateString}" style="white-space: normal;" />
													</t:column>
													<t:column id="colStatusEn" sortable="true"  style="width:7%;" rendered="#{pages$ContractListBackingBean.isEnglishLocale}">
														<f:facet name="header">
															<t:outputText value="#{msg['contract.contractStatus']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.statusEn}" style="white-space: normal;" />
													</t:column>
												   <t:column id="colStatusAr" style="padding-right:5px;width:7%"  sortable="true"  rendered="#{pages$ContractListBackingBean.isArabicLocale}">
														<f:facet name="header">
															<t:outputText value="#{msg['contract.contractStatus']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.statusAr}" style="white-space: normal;" />
													</t:column>
													
													
													<t:column id="colTypeEn" sortable="true" style="width:7%;" rendered="#{pages$ContractListBackingBean.isEnglishLocale}">
														<f:facet name="header">
															<t:outputText value="#{msg['contract.contractType']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.contractTypeEn}" style="white-space: normal;" />
													</t:column>
													
														<t:column id="colTypeAr" style="padding-right:5px;width:7%;" sortable="true" rendered="#{pages$ContractListBackingBean.isArabicLocale}">
														<f:facet name="header">
															<t:outputText value="#{msg['contract.contractType']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.contractTypeAr}" style="white-space: normal;" />
													</t:column>
													<t:column id="colTenant" sortable="true" style="width:10%;" >
														<f:facet name="header">
															<t:outputText value="#{msg['contract.tenantName']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.tenantView.personFullName}" style="white-space: normal;" />
													</t:column>
													<t:column id="colTenantType" sortable="true" style="width:8%;" >
														<f:facet name="header">
															<t:outputText value="#{msg['tenants.tenantsType']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.tenantView.isCompany==0? msg['tenants.tpye.individual']:msg['tenants.tpye.company']}" style="white-space: normal;" />
													</t:column>
													<t:column id="colPropertyName" sortable="true" style="width:10%;" >
														<f:facet name="header">
															<t:outputText value="#{msg['contract.property.Name']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.propertyName}" style="white-space: normal;"  />
													</t:column>
													<t:column id="selectContract" width="10%" sortable="true"  >
													<f:facet name="header" >
														<t:outputText value="#{msg['commons.select']}" />
                                                    </f:facet>
                                                          <h:graphicImage rendered="#{pages$ContractListBackingBean.isCLStatusTerminated}" style="padding-left:4px;" 
                                                            title="#{msg['commons.select']}" url="../resources/images/select-icon.gif"
														    onclick="javascript:RowDoubleClick('#{dataItem.contractNumber}','#{dataItem.contractId}','#{dataItem.tenantView.tenantId}');" /> 													
														 
														 <h:graphicImage style="padding-left:4px;" title="#{msg['commons.select']}" url="../resources/images/select-icon.gif"
														    onclick="javascript:RowDoubleClick('#{dataItem.contractNumber}','#{dataItem.contractId}');" />
							

												</t:column>
												</t:dataTable>										
											</t:div>
											<t:div styleClass="contentDivFooter" style="width:100.2%">
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$ContractListBackingBean.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
												<CENTER>
											<t:dataScroller id="scroller" for="contractListDT" paginator="true"
												fastStep="1" paginatorMaxPages="#{pages$ContractListBackingBean.paginatorMaxPages}" immediate="false"
												paginatorTableClass="paginator"
												
												renderFacetsIfSinglePage="true" 
												paginatorTableStyle="grid_paginator" layout="singleTable" 
												paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
												styleClass="SCH_SCROLLER"
												pageIndexVar="pageNumber"
												paginatorActiveColumnStyle="font-weight:bold;"
												paginatorRenderLinkForActive="false">

												<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
												<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>		
											</t:dataScroller>
											</CENTER>
													
												</td></tr>
											</table>
										</t:div>  
										</div>
									</div>
									</h:form>
</td></tr>

</table> </td></tr>

				</table>
				</div>
			</body>
		
</html>
				</f:view> 