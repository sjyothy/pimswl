<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript"> 
	function resetValues()
    {
     	document.getElementById("detailsFrm:searchCostcenter").value="";
     	document.getElementById("detailsFrm:searchBeneficiary").value="";
     	document.getElementById("detailsFrm:searchToBeneficiary").value="";
     	document.getElementById("detailsFrm:searchToBank").value="";
     	document.getElementById("detailsFrm:searchToAccNum").value="";
		document.getElementById("detailsFrm:searchPaymentMethod").selectedIndex=0;
			
    }
    function disableButtonsForMarkUnMarkSingle(control,callSecondLevel)
	{
		disableInputControls();
		document.getElementById("detailsFrm:onClickMarkUnMarkSingle").onclick();
	}
	function disableButtonsForMarkUnMark(control,callSecondLevel)
	{
		disableInputControls();
		document.getElementById("detailsFrm:onClickMarkUnMark").onclick();
	}
	function disableButtons(control,callSecondLevel)
	{
		disableInputControls();
		if( callSecondLevel==1 )
		control.nextSibling.nextSibling.onclick();
		else
		control.nextSibling.onclick();
			
	}
	
	function disableInputControls()
	{
		var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
	}
	function populatePaymentDetails()
	{
		  disableInputControls();
		  document.getElementById("detailsFrm:testing").onclick();
	
		  
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">

			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel
											value="#{msg['reviewAndConfirmDisbursementsHeader']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:messages></h:messages>
															<h:outputText id="errortxt"
																value="#{pages$ReviewConfirmDisbursements.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText id="successTxt" styleClass="INFO_FONT"
																escape="false"
																value="#{pages$ReviewConfirmDisbursements.successMessages}" />
															<h:inputHidden
																value="#{pages$ReviewConfirmDisbursements.pageMode}" />
															<h:inputHidden id="hdnListMonth"
																value="#{pages$ReviewConfirmDisbursements.hdnListMonth}" />
															<h:inputHidden id="hdnListYear"
																value="#{pages$ReviewConfirmDisbursements.hdnListYear}" />
															<h:commandLink id="onClickMarkUnMark"
																action="#{pages$ReviewConfirmDisbursements.onMarkUnMarkChanged}" />
																<h:commandLink id="onClickMarkUnMarkSingle"
																action="#{pages$ReviewConfirmDisbursements.onMarkUnMarkSingle}" />
																

														</td>
													</tr>
												</table>
												<div>
													<h:commandLink id="testing"
														action="#{pages$ReviewConfirmDisbursements.obtainChildParams}">
													</h:commandLink>

													<div>
														<table border="0" class="layoutTable"
															style="margin-left: 15px; margin-right: 15px;">
															<tr>
																<td>

																</td>
															</tr>
														</table>
														<div class="AUC_DET_PAD">
															<table cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<td style="FONT-SIZE: 0px">
																		<IMG
																			src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																			class="TAB_PANEL_LEFT" />
																	</td>
																	<td width="100%" style="FONT-SIZE: 0px">
																		<IMG
																			src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																			class="TAB_PANEL_MID" />
																	</td>
																	<td style="FONT-SIZE: 0px">
																		<IMG
																			src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																			class="TAB_PANEL_RIGHT" />
																	</td>
																</tr>
															</table>
															<div class="DETAIL_SECTION" style="width: 99.8%;">

																<h:outputLabel
																	value="#{msg['paymentReceiptDetail.receiptSectionHeading']}"
																	styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
																<table cellpadding="1px" cellspacing="2px"
																	class="DETAIL_SECTION_INNER" width="100%">
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['request.number']}:"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText id="requestNumber" styleClass="READONLY"
																				value="#{pages$ReviewConfirmDisbursements.requestView.requestNumber}"
																				maxlength="20"></h:inputText>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.createdBy']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="createdBy" styleClass="READONLY"
																				value="#{pages$ReviewConfirmDisbursements.requestView.createdByFullNameAr}"
																				maxlength="20"></h:inputText>
																		</td>

																	</tr>

																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['ReviewAndConfirmDisbursements.disbursementList']} :"></h:outputLabel>
																		</td>
																		<td style="width: 30%">
																			<h:selectOneMenu id="disbursementList"
																				binding="#{pages$ReviewConfirmDisbursements.disbursementListSelectMenu}"
																				tabindex="3" readonly="true" disabled="true">

																				<f:selectItems
																					value="#{pages$ReviewConfirmDisbursements.disbursementList}" />

																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['periodicDisbursement.lbl.listFor']} :"></h:outputLabel>
																		</td>
																		<td style="width: 30%">
																			<h:inputText id="txtListFor"
																				value="#{pages$ReviewConfirmDisbursements.requestView.circulationDate}"
																				rendered="#{! pages$ReviewConfirmDisbursements.pageModeDraftList && !pages$ReviewConfirmDisbursements.pageModeRejected }"
																				styleClass="A_RIGHT_NUM READONLY"
																				style="width: 185px;">
																				<f:convertDateTime
																					timeZone="#{pages$ReviewConfirmDisbursements.timeZone}"
																					pattern="MMM,yyyy" />

																			</h:inputText>

																			<h:selectOneMenu style="width:130px;"
																				value="#{pages$ReviewConfirmDisbursements.requestView.periodicMonth}"
																				rendered="#{
																							pages$ReviewConfirmDisbursements.pageModeDraftList || 
																							pages$ReviewConfirmDisbursements.pageModeRejected 
																						   }"
																				styleClass="SELECT_MENU">
																				<f:selectItem itemLabel="--" itemValue="-1" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.months}" />

																			</h:selectOneMenu>
																			<h:selectOneMenu style="width:60px;"
																				value="#{pages$ReviewConfirmDisbursements.requestView.periodicYear}"
																				rendered="#{
																							pages$ReviewConfirmDisbursements.pageModeDraftList || 
																							pages$ReviewConfirmDisbursements.pageModeRejected 
																						   }"
																				styleClass="SELECT_MENU">
																				<f:selectItem itemLabel="--" itemValue="-1" />
																				<f:selectItem
																					itemLabel="#{msg['commons.combo.PleaseSelect']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.years}" />

																			</h:selectOneMenu>

																		</td>


																	</tr>

																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['ReviewAndConfirmDisbursements.source']} :"></h:outputLabel>
																		</td>
																		<td style="width: 30%">
																			<h:selectOneMenu id="disbursementSource"
																				value="#{pages$ReviewConfirmDisbursements.disbursementSourceSelectMenu}"
																				tabindex="3">

																				<f:selectItem
																					itemLabel="#{msg['commons.pleaseSelect']}"
																					itemValue="-1" />

																				<f:selectItem
																					itemLabel="#{msg['PeriodicDisbursements.lbl.selfAccount']}"
																					itemValue="0" />

																				<f:selectItems
																					value="#{pages$ReviewConfirmDisbursements.disbursementSource}" />


																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['ReviewAndConfirmDisbursements.amount']} :"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="txtTotalAmount"
																				styleClass="A_RIGHT_NUM READONLY" readonly="true"
																				style="width: 185px;"></h:inputText>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel
																				rendered="#{pages$ReviewConfirmDisbursements.showCompleteButton }"
																				value="#{msg['mems.inheritanceFile.fieldLabel.rejectionReason']}"
																				styleClass="LABEL" />
																		</td>
																		<td colspan="3">
																			<h:inputTextarea style="width:90%;"
																				rendered="#{pages$ReviewConfirmDisbursements.showCompleteButton }"
																				value="#{pages$ReviewConfirmDisbursements.txtRejectionReason}" />
																		</td>
																	</tr>
																</table>
															</div>

															<t:div style="width:100%" styleClass="BUTTON_TD">
																<h:commandButton id="btnAddDelBen" styleClass="BUTTON"
																	value="#{msg['PeriodicDisbursements.lbl.AddDelBen']}"
																	type="submit"
																	action="#{pages$ReviewConfirmDisbursements.onOpenAddedDeletedTotalBeneficiaryExcel}"
																	style="width: auto" />
															</t:div>

															<br></br>
															<div>
																<rich:tabPanel>
																	<rich:tab
																		label="#{msg['ReviewAndConfirmDisbursements.disbursementList']}">

																		<t:panelGrid id="actionListGrdFields"
																			styleClass="TAB_DETAIL_SECTION_INNER"
																			cellpadding="1px" width="100%" cellspacing="5px"
																			columns="4">

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['settlement.label.costCenter']}:"></h:outputLabel>
																			<h:inputText id="searchCostcenter"
																				binding="#{pages$ReviewConfirmDisbursements.htmlAssetDesc}" />

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiptDetail.PaidBy']}:"></h:outputLabel>
																			<h:inputText id="searchBeneficiary"
																				binding="#{pages$ReviewConfirmDisbursements.htmlBeneficiary}" />

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['PeriodicDisbursements.columns.beneficiary']}" />
																			<h:inputText id="searchToBeneficiary"
																				binding="#{pages$ReviewConfirmDisbursements.htmlToBeneficiary}"
																				style="width: 185px;" />

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['financialAccConfiguration.paymentMethod']}:"></h:outputLabel>
																			<h:selectOneMenu id="searchPaymentMethod"
																				binding="#{pages$ReviewConfirmDisbursements.disbursementTypeSelectMenu}">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItem
																					itemLabel="#{msg['payments.tabTitle.bankTransfer']}"
																					itemValue="4" />
																				<f:selectItem
																					itemLabel="#{msg['renewContract.cheque']}"
																					itemValue="1" />

																			</h:selectOneMenu>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.replaceCheque.bank']}:"></h:outputLabel>
																			<h:inputText id="searchToBank"
																				binding="#{pages$ReviewConfirmDisbursements.htmlAssetNameAr}"
																				style="width: 185px;" />

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['feeConfiguration.accountNo']}:"></h:outputLabel>
																			<h:inputText id="searchToAccNum"
																				binding="#{pages$ReviewConfirmDisbursements.htmlAssetNumber}"
																				style="width: 185px;" />

																		</t:panelGrid>

																		<t:panelGrid id="actionListGrdActions"
																			styleClass="BUTTON_TD" cellpadding="1px" width="100%"
																			cellspacing="5px">
																			<h:panelGroup>
																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.search']}"
																					action="#{pages$ReviewConfirmDisbursements.onSearch}" />
																				<h:commandButton styleClass="BUTTON" type="button"
																					onclick="javascript:resetValues();"
																					value="#{msg['commons.clear']}" />

																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.All']}"
																					binding="#{pages$ReviewConfirmDisbursements.btnSelectAll}"
																					action="#{pages$ReviewConfirmDisbursements.onSelectAll}" />
																			</h:panelGroup>
																		</t:panelGrid>

																		<t:div styleClass="contentDiv" style="width:100%;">
																			<t:dataTable id="test2"
																				value="#{pages$ReviewConfirmDisbursements.dataList}"
																				binding="#{pages$ReviewConfirmDisbursements.dataTable}"
																				width="100%" rows="200" preserveDataModel="false"
																				preserveSort="false" var="dataItem"
																				rowClasses="row1,row2" rules="all"
																				renderedIfEmpty="true">

																				<t:column id="selectMany" width="4%"
																					sortable="false" style="white-space: normal;">
																					<f:facet name="header">
																						<h:selectBooleanCheckbox id="markUnMarkAll"
																							onclick="javaScript:disableButtonsForMarkUnMark()"
																							value="#{pages$ReviewConfirmDisbursements.markUnMarkAll}" />
																					</f:facet>
																					<h:selectBooleanCheckbox id="select"
																						rendered="#{dataItem.isDisbursed ==0 }"
																						value="#{dataItem.selected}" 
																						onclick="javaScript:disableButtonsForMarkUnMarkSingle()"
																						/>
																				</t:column>
																				<t:column id="fileNumber" width="20%"
																					sortable="true" style="white-space: normal;">
																					<f:facet name="header">
																						<t:commandSortHeader columnName="fileNumber"
																							actionListener="#{pages$ReviewConfirmDisbursements.sort}"
																							value="#{msg['searchAssets.fileNumber']}"
																							arrow="true">
																							<f:attribute name="sortField" value="fileNumber" />
																						</t:commandSortHeader>
																					</f:facet>
																					<t:outputText
																						value="#{dataItem.inheritanceFileNumber}"
																						style="white-space: normal;" />
																				</t:column>

																				<t:column id="costCenter" width="20%" defaultSorted="true"
																					sortable="true" style="white-space: normal;">
																					<f:facet name="header">
																						<t:commandSortHeader columnName="costCenter"
																							actionListener="#{pages$ReviewConfirmDisbursements.sort}"
																							value="#{msg['settlement.label.costCenter']}"
																							arrow="true">
																							<f:attribute name="sortField" value="costCenter" />
																						</t:commandSortHeader>

																					</f:facet>
																					<t:outputText value="#{dataItem.costCenter}"
																						style="white-space: normal;" />
																				</t:column>

																				<t:column id="beneficiary" width="20%"
																					sortable="true" style="white-space: normal;">

																					<f:facet name="header">

																						<t:commandSortHeader columnName="passportName"
																							actionListener="#{pages$ReviewConfirmDisbursements.sort}"
																							value="#{msg['receiptDetail.PaidBy']}"
																							arrow="true">
																							<f:attribute name="sortField"
																								value="passportName" />
																						</t:commandSortHeader>

																					</f:facet>
																					<t:commandLink
																						actionListener="#{pages$ReviewConfirmDisbursements.openManageBeneficiaryPopUp}"
																						value="#{dataItem.beneficiary}"
																						style="white-space: normal;" styleClass="A_LEFT" />
																				</t:column>

																				<t:column id="paidToName" width="10%"
																					sortable="true" style="white-space: normal;">

																					<f:facet name="header">
																						<t:commandSortHeader columnName="paidToName"
																							actionListener="#{pages$ReviewConfirmDisbursements.sort}"
																							value="#{msg['PeriodicDisbursements.columns.beneficiary']}"
																							arrow="true">
																							<f:attribute name="sortField" value="paidToName" />
																						</t:commandSortHeader>

																					</f:facet>

																					<t:outputText value="#{dataItem.paidToName}"
																						style="white-space: normal;" />
																				</t:column>

																				<t:column id="paymentMethod" width="10%"
																					sortable="true" style="white-space: normal;">

																					<f:facet name="header">
																						<t:commandSortHeader columnName="paymentMethod"
																							actionListener="#{pages$ReviewConfirmDisbursements.sort}"
																							value="#{msg['financialAccConfiguration.paymentMethod']}"
																							arrow="true">
																							<f:attribute name="sortField"
																								value="paymentMethod" />
																						</t:commandSortHeader>
																					</f:facet>

																					<t:outputText
																						value="#{
																											   dataItem.paymentDetailFormView.paymentMethodId==1?
																											   msg['renewContract.cheque']:
																											   msg['payments.tabTitle.bankTransfer'] 
																						
																											  }"
																						style="white-space: normal;" />
																				</t:column>

																				<t:column id="paidToBank" width="10%"
																					sortable="true" style="white-space: normal;">

																					<f:facet name="header">
																						<t:commandSortHeader columnName="paidToBank"
																							actionListener="#{pages$ReviewConfirmDisbursements.sort}"
																							value="#{msg['addMissingMigratedPayments.fieldName.bankName']}"
																							arrow="true">
																							<f:attribute name="sortField" value="paidToBank" />
																						</t:commandSortHeader>
																					</f:facet>

																					<t:outputText value="#{dataItem.paidToBank}"
																						style="white-space: normal;" />
																				</t:column>
																				<t:column id="accountNumber" width="5%"
																					sortable="true"
																					style="white-space: normal;word-wrap: hard;">

																					<f:facet name="header">

																						<t:commandSortHeader columnName="accountNumber"
																							actionListener="#{pages$ReviewConfirmDisbursements.sort}"
																							value="#{msg['feeConfiguration.accountNo']}"
																							arrow="true">
																							<f:attribute name="sortField"
																								value="accountNumber" />
																						</t:commandSortHeader>

																					</f:facet>
																					<t:div
																						style="white-space: normal;word-wrap: break-word;width:80%;">
																						<t:outputText value="#{dataItem.accountNumber}"
																							style="white-space: normal;word-wrap: break-word;" />
																					</t:div>
																				</t:column>
																				<t:column id="balance" width="10%"
																					style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['liabilityExemptCollect.balance']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.accountBalance}"
																						style="white-space: normal;" styleClass="A_LEFT">
																						<f:convertNumber maxIntegerDigits="15"
																							maxFractionDigits="2"
																							pattern="##############0.00" />
																					</t:outputText>
																				</t:column>
																				<t:column id="requestedBalance" width="10%"
																					style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['commons.requestedAmount']}" />
																					</f:facet>

																					<t:commandLink value="#{dataItem.requestedBalance}"
																						action="#{pages$ReviewConfirmDisbursements.onOpenBeneficiaryDisbursementDetailsPopup}"
																						style="white-space: normal;" styleClass="A_LEFT">

																					</t:commandLink>
																				</t:column>


																				<t:column id="blockingBalance" width="10%"
																					style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['blocking.lbl.amount']}" />
																					</f:facet>

																					<t:commandLink value="#{dataItem.blockingBalance}"
																						action="#{pages$ReviewConfirmDisbursements.onOpenBlockingDetailsPopup}"
																						style="white-space: normal;" styleClass="A_LEFT">

																					</t:commandLink>
																				</t:column>

																				<t:column id="to" width="10%" sortable="true"
																					style="white-space: normal;">

																					<f:facet name="header">
																						<t:commandSortHeader columnName="amount"
																							actionListener="#{pages$ReviewConfirmDisbursements.sort}"
																							value="#{msg['PeriodicDisbursements.columns.amount']}"
																							arrow="true">
																							<f:attribute name="sortField" value="amount" />
																						</t:commandSortHeader>

																					</f:facet>
																					<t:outputText value="#{dataItem.amount}"
																						style="white-space: normal;" styleClass="A_LEFT">
																						<f:convertNumber maxIntegerDigits="15"
																							maxFractionDigits="2"
																							pattern="##############0.00" />
																					</t:outputText>
																				</t:column>
																				<t:column id="action" width="18%"
																					style="white-space: normal;" sortable="false">
																					<f:facet name="header">
																						<t:outputText value="#{msg['commons.action']}" />
																					</f:facet>
																					<t:commandLink
																						actionListener="#{pages$ReviewConfirmDisbursements.openPaymentsPopUp}">
																						<h:graphicImage id="paymentReq"
																							rendered="#{!pages$ReviewConfirmDisbursements.pageModeView}"
																							title="#{msg['chequeList.chequeList']}"
																							url="../resources/images/app_icons/Pay-Fine.png" />
																					</t:commandLink>

																					<t:commandLink
																						action="#{pages$ReviewConfirmDisbursements.onDeleteItem}"
																						onclick="if (!confirm('#{msg['mems.common.alertDelete']}')) return false;else true;">
																						<h:graphicImage id="deleteIcon"
																							rendered="#{pages$ReviewConfirmDisbursements.pageModeDraftList ||
																										pages$ReviewConfirmDisbursements.pageModeRejected 
																							
																										}"
																							title="#{msg['commons.delete']}"
																							url="../resources/images/delete.gif"
																							width="18px;" />
																					</t:commandLink>
																				</t:column>
																			</t:dataTable>

																		</t:div>
																		<t:div id="pagingDivPaySch"
																			styleClass="contentDivFooter" style="width:99.2%;">
																			<t:panelGrid columns="2" border="0" width="100%"
																				cellpadding="1" cellspacing="1">
																				<t:panelGroup
																					styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																					<t:div styleClass="JUG_NUM_REC_ATT">
																						<h:outputText value="#{msg['commons.records']}" />
																						<h:outputText value=" : " />
																						<h:outputText
																							value="#{pages$ReviewConfirmDisbursements.recordSize}" />
																					</t:div>
																				</t:panelGroup>
																				<t:panelGroup>
																					<TABLE border="0" class="SCH_SCROLLER">
																						<tr>
																							<td>
																								<t:commandLink
																									action="#{pages$ReviewConfirmDisbursements.pageFirst}"
																									disabled="#{pages$ReviewConfirmDisbursements.firstRow == 0}">
																									<t:graphicImage url="../#{path.scroller_first}"
																										id="lblFPRDC"></t:graphicImage>
																								</t:commandLink>
																							</td>
																							<td>
																								<t:commandLink
																									action="#{pages$ReviewConfirmDisbursements.pagePrevious}"
																									disabled="#{pages$ReviewConfirmDisbursements.firstRow == 0}">
																									<t:graphicImage
																										url="../#{path.scroller_fastRewind}"
																										id="lblFRPRDC"></t:graphicImage>
																								</t:commandLink>
																							</td>
																							<td>
																								<t:dataList
																									value="#{pages$ReviewConfirmDisbursements.pages}"
																									var="page">
																									<h:commandLink value="#{page}"
																										actionListener="#{pages$ReviewConfirmDisbursements.page}"
																										rendered="#{page != pages$ReviewConfirmDisbursements.currentPage}" />
																									<h:outputText value="<b>#{page}</b>"
																										escape="false"
																										rendered="#{page == pages$ReviewConfirmDisbursements.currentPage}" />
																								</t:dataList>
																							</td>
																							<td>
																								<t:commandLink
																									action="#{pages$ReviewConfirmDisbursements.pageNext}"
																									disabled="#{pages$ReviewConfirmDisbursements.firstRow + pages$ReviewConfirmDisbursements.rowsPerPage >= pages$ReviewConfirmDisbursements.totalRows}">
																									<t:graphicImage
																										url="../#{path.scroller_fastForward}"
																										id="lblFFPRDC"></t:graphicImage>
																								</t:commandLink>
																							</td>
																							<td>
																								<t:commandLink
																									action="#{pages$ReviewConfirmDisbursements.pageLast}"
																									disabled="#{pages$ReviewConfirmDisbursements.firstRow + pages$ReviewConfirmDisbursements.rowsPerPage >= pages$ReviewConfirmDisbursements.totalRows}">
																									<t:graphicImage url="../#{path.scroller_last}"
																										id="lblLPRDC"></t:graphicImage>
																								</t:commandLink>
																							</td>
																						</tr>
																					</TABLE>
																				</t:panelGroup>
																			</t:panelGrid>
																		</t:div>

																		<t:div styleClass="TAB_DETAIL_SECTION"
																			style="width:99%;">
																			<t:panelGrid id="summaryGrid" cellpadding="1px"
																				width="100%" cellspacing="3px"
																				styleClass="TAB_DETAIL_SECTION_INNER" columns="4"
																				columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">

																				<t:outputLabel styleClass="A_LEFT"
																					style="width:75%;;font-weight:bold;"
																					value="#{msg['mems.normaldisb.label.totalAmount']} :" />

																				<t:outputLabel styleClass="A_LEFT"
																					style="width:75%;;font-weight:bold;"
																					value="#{pages$ReviewConfirmDisbursements.totalAmount} " />


																			</t:panelGrid>
																		</t:div>
																		<t:div style="width:100%" styleClass="BUTTON_TD">
																			<h:commandButton id="btnDisburse" styleClass="BUTTON"
																				value="#{msg['button.Disburse']}" type="submit"
																				binding="#{pages$ReviewConfirmDisbursements.btnDisburse}"
																				onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceedWithTheDisburse']}')) 
																						 return false;else disableButtons(this,0);
																						"
																				style="width: auto" />
																			<h:commandLink id='lnkDisburse_Click'
																				action="#{pages$ReviewConfirmDisbursements.btnDisburse_Click}" />


																			<h:commandButton styleClass="BUTTON"
																				value="#{msg['tabFinance.btn.AddSupplier']}"
																				binding="#{pages$ReviewConfirmDisbursements.btnAddGrpSuppliers}"
																				action="#{pages$ReviewConfirmDisbursements.onAddGRPSuppliers}"
																				style="width: auto;" />

																		</t:div>
																		<br />
																		<br />
																		<br />
																		<br />
																	</rich:tab>

																	<rich:tab label="#{msg['commons.commentsTabHeading']}"
																		action="#{pages$ReviewConfirmDisbursements.tabAttachmentsComments_Click}">
																		<%@ include file="notes/notes.jsp"%>

																	</rich:tab>

																	<rich:tab id="attachmentTab"
																		label="#{msg['commons.attachmentTabHeading']}"
																		action="#{pages$ReviewConfirmDisbursements.tabAttachmentsComments_Click}">
																		<%@  include file="attachment/attachment.jsp"%>
																	</rich:tab>
																	<rich:tab
																		label="#{msg['mems.inheritanceFile.tabHeadingShort.history']}"
																		action="#{pages$ReviewConfirmDisbursements.tabRequestHistoryClick}">
																		<%@ include file="../pages/requestTasks.jsp"%>
																	</rich:tab>
																</rich:tabPanel>
															</div>
															<table cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<td style="FONT-SIZE: 0px">
																		<IMG
																			src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																			class="TAB_PANEL_LEFT" />
																	</td>
																	<td width="100%" style="FONT-SIZE: 0px">
																		<IMG
																			src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																			class="TAB_PANEL_MID" style="width: 100%;" />
																	</td>
																	<td style="FONT-SIZE: 0px">
																		<IMG
																			src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																			class="TAB_PANEL_RIGHT" />
																	</td>
																</tr>

															</table>
															<t:div styleClass="BUTTON_TD"
																style="padding-top:10px; padding-right:21px;">
																<h:commandButton id="sendToFinance"
																	rendered="#{pages$ReviewConfirmDisbursements.pageModeDraftList || 
																				pages$ReviewConfirmDisbursements.pageModeRejected 
																			   }"
																	onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this,1);"
																	styleClass="BUTTON" type="submit"
																	value="#{msg['PeriodicDisbursements.buttons.sendToFinance']}"
																	style="width: auto" />

																<h:commandLink
																	action="#{pages$ReviewConfirmDisbursements.onSendToFinance}" />

																<h:commandButton id="cancelList"
																	rendered="#{pages$ReviewConfirmDisbursements.pageModeDraftList ||
																				pages$ReviewConfirmDisbursements.pageModeRejected }"
																	onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this,1);"
																	styleClass="BUTTON" type="submit"
																	value="#{msg['commons.cancel']}" style="width: auto" />
																<h:commandLink
																	action="#{pages$ReviewConfirmDisbursements.onCancelRequest}" />

																<h:commandButton id="btnComplete" styleClass="BUTTON"
																	value="#{msg['button.Complete']}" type="submit"
																	rendered="#{ pages$ReviewConfirmDisbursements.showCompleteButton && 
																				!pages$ReviewConfirmDisbursements.pageModeView
																				}"
																	onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceedWithComplete']}')) return false;else disableButtons(this,1);"
																	style="width: auto" />
																<h:commandLink
																	action="#{pages$ReviewConfirmDisbursements.btnComplete_Click}" />

																<h:commandButton id="btnReject" styleClass="BUTTON"
																	value="#{msg['socialResearch.btn.reject']}"
																	type="submit"
																	rendered="#{ pages$ReviewConfirmDisbursements.showCompleteButton  && 
																				!pages$ReviewConfirmDisbursements.pageModeView
																				}"
																	onclick="if (!confirm('#{msg['liabilityExemptCollect.messages.confirmCollect']}')) return false;else disableButtons(this,1);"
																	style="width: auto" />
																<h:commandLink
																	action="#{pages$ReviewConfirmDisbursements.onRejectFromFinance}" />
															</t:div>
														</div>
													</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

		</body>
	</html>
</f:view>