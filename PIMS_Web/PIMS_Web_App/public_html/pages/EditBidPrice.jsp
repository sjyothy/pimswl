

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="avanza-security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<script language="javascript" type="text/javascript">
	function closeWindow()
	{
	  window.close();
	}
	function submitAndClose()
	{
		window.opener.document.forms[0].submit();
		closeWindow();
	}

</script>


<f:view>

  <html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
  <head>   
  <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>  
        
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache,must-revalidate,no-store">
	<meta http-equiv="expires" content="0">

<link rel="stylesheet" type="text/css"
			href="../resources/css/simple_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../resources/css/amaf_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../resources/css/table.css" />
        <title>PIMS</title>
    </head>

    
    <%
    UIViewRoot view = (UIViewRoot)session.getAttribute("view");
    if (view.getLocale().toString().equals("en")){ %>
    <body dir="ltr">
    <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg" />
    <%}else{%>
    <body dir="rtl">
    <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages_ar" var="msg" />
    <%}
    
    %>
    <!-- Header --> 
    <table width="100%" cellpadding="0" cellspacing="0"  border="0">
    <tr>
    <td colspan="2">
        <jsp:include page="header.jsp"/>
    </td>
    </tr>

<tr width="100%">
    <!--td class="divLeftMenuWithBody" width="17%">
        < jsp:include page="leftmenu.jsp"/>
    </td-->
    <td width="100%" valign="top" class="divBackgroundBody" colspan="2">
        <table width="100%" class="greyPanelTable" cellpadding="0" cellspacing="0"  border="0">
	        <tr>
	         <%if (view.getLocale().toString().equals("en"))
	             {  %>
	            <td background ="../../resources/images/Grey panel/Grey-Panel-left-1.jpg" height="38" style="background-repeat:no-repeat;" width="100%">
	             <font style="font-family:Trebuchet MS;font-weight:regular;font-size:19px;margin-left:15px;top:30px;">
	             <h:outputLabel value="#{msg['auction.conductAuction.editBidWinner']}"></h:outputLabel>
	             </font>                
	             <%}else {%>
	             <td width="100%">
	                 <IMG src="../../resources/images/ar/Detail/Grey Panel/Grey-Panel-right-1.jpg"></img>
	             <%}%>    
	            </td>    
	            <td width="100%">
	        &nbsp;
	            </td>
			</tr>
        </table>
        <table width="100%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
          <tr valign="top">
             <td height="100%" valign="top" nowrap="nowrap" background ="../../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
             </td>    
             <!--Use this below only for desiging forms Please do not touch other sections -->             
    
             <td width="100%" height="100%" valign="top" nowrap="nowrap">
             	<h:form id="conductAuctionForm">
             	<table width="100%">					
					<tr>
						<td colspan="6">
							<table align="center">
								<tr>
									<td colspan="6">
										
									</td>
								</tr>
								<tr>
									<td>
										<h:outputLabel value="#{msg['unit.number']}"></h:outputLabel>
									</td>
									<td>
										<h:inputText id="unitNumber" readonly="true"
										value="#{pages$EditBidPrice.unitNumber}"></h:inputText>
									</td>
									<td>
										&nbsp;
									</td>
									<td>
										<h:outputLabel value="#{msg['unit.type']}"></h:outputLabel>
									</td>
									<td>
										<h:inputText id="unitType" readonly="true"
										value="#{pages$EditBidPrice.unitType}"></h:inputText>
									</td>
									<td>
										&nbsp;
									</td>
								</tr>
								<tr>
									<td>
										<h:outputLabel value="#{msg['bidder.name']}"></h:outputLabel>
									</td>
									<td>
										<h:inputText id="passport" readonly="true" 
										value="#{pages$EditBidPrice.bidderName}"></h:inputText>
									</td>
									<td style="width: 40px; height: 19px">&nbsp;</td>
									<td>
										<h:outputLabel value="#{msg['auction.conductAuction.bidPrice']}"></h:outputLabel>
									</td>
									<td>
										<h:inputText id="lastname"
										value="#{pages$EditBidPrice.bidPrice}"></h:inputText>
									</td>
									<td>
										&nbsp;
									</td>
								</tr>
								
								<tr>
									<td>
										<h:outputLabel value="#{msg['auction.conductAuction.bidWon']}"></h:outputLabel>
									</td>
									<td>
										<h:selectBooleanCheckbox id="unitWon"
										value="#{pages$EditBidPrice.bidWon}">
										</h:selectBooleanCheckbox>										
									</td>
									<td colspan="4">
										&nbsp;
									</td>
								</tr>
								
								<tr>
									<td colspan="2">
									</td>
									<td align="center">													
									</td>
									<td colspan="2">														
									</td>
									
								</tr>
								<tr align="right">
								
									<td colspan="5">
										<h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}"
												action="#{pages$EditBidPrice.save}"></h:commandButton>
		
										<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}"
												onclick="javascript:closeWindow();"></h:commandButton>
									</td>

								</tr>
							</table>																											
						</td>													
					</tr>

				</table>
                </h:form>
                                   
         	</td>
         </tr>
         
         <tr>
         	<td>

         	</td>         	
         </tr>
        </table>
    
    
    
    
    
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
       <tr><td class="footer"><h:outputLabel value="#{msg['commons.footer.message']}" /></td></tr>
    </table>
        </td>
    </tr>
    </table>
    </body>
</html>
</f:view>
