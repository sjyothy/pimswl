
<script type="text/javascript">

						
</script>
<t:div styleClass="TAB_DETAIL_SECTION" style="width:100%">
	<t:panelGrid id="contractDetailsTable" cellpadding="1px" width="100%"
		cellspacing="5px" styleClass="TAB_DETAIL_SECTION_INNER" columns="4">

		<h:outputLabel styleClass="LABEL"
			value="#{msg['commons.referencenumber']}:" />
		<h:inputText maxlength="20" id="txtContractRefNum" readonly="true"
			binding="#{pages$ServiceContractDetailsTab.txtContractNo}"
			value="#{pages$ServiceContractDetailsTab.contractNo}"
			></h:inputText>

		<h:outputLabel id="lblContractType" styleClass="LABEL"
			value="#{msg['contract.contractType']}:"></h:outputLabel>
		<h:inputText maxlength="20" id="txtContractType" readonly="true"
			binding="#{pages$ServiceContractDetailsTab.txtContractType}"
			value="#{pages$ServiceContractDetailsTab.contractType}"
			></h:inputText>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['serviceContract.tender.number']}:"></h:outputLabel>
		<h:panelGroup id="gridTender">
			<h:inputText id="txtTenderNumber" readonly="true"
				binding="#{pages$ServiceContractDetailsTab.txtTenderNum}"
				title="#{pages$ServiceContractDetailsTab.txtTenderNum}" style="width:170px;"
				value="#{pages$ServiceContractDetailsTab.tenderNum}"></h:inputText>
			<h:graphicImage url="../resources/images/app_icons/Search-tenant.png"
				onclick="javaScript:showSearchTenderPopUp('#{pages$ServiceContractDetailsTab.tenderSearchKey_tenderType}','#{pages$ServiceContractDetailsTab.tenderSearchKey_WinnerContractorPresent}','#{pages$ServiceContractDetailsTab.tenderSearchKey_NotBindedWithContract}');"
				binding="#{pages$ServiceContractDetailsTab.imgViewTender}"
				style="MARGIN: 0px 0px -4px;" alt="#{msg[commons.searchTender]}"></h:graphicImage>
			<h:graphicImage url="../resources/images/delete_icon.png"
				onclick="javaScript:document.getElementById('formRequest:cmdDeleteTender').onclick();"
				binding="#{pages$ServiceContractDetailsTab.imgRemoveTender}"
				style="MARGIN: 0px 0px -4px;" alt="#{msg[commons.delete]}"></h:graphicImage>
		</h:panelGroup>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['serviceContract.tender.description']}:"></h:outputLabel>
		<h:inputText id="txtTenderDesc" readonly="true" 
			binding="#{pages$ServiceContractDetailsTab.txtTenderDesc}"
			value="#{pages$ServiceContractDetailsTab.tenderDesc}"
			title="#{pages$ServiceContractDetailsTab.tenderDesc}"></h:inputText>

		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['serviceContract.contractor.number']}:"></h:outputLabel>
		</t:panelGroup>
		<h:panelGroup id="gridContractor">
			<h:inputText id="txtContractorNumber"
				readonly="true"
				binding="#{pages$ServiceContractDetailsTab.txtContractorNum}"
				title="#{pages$ServiceContractDetailsTab.contractorNum}" style="width:170px;"
				value="#{pages$ServiceContractDetailsTab.contractorNum}"></h:inputText>
			<h:graphicImage rendered="false"
				url="../resources/images/app_icons/Search-Person.png"
				binding="#{pages$ServiceContractDetailsTab.imgViewContractor}"
				style="MARGIN: 0px 0px -4px;" alt="#{msg[contract.searchTenant]}"></h:graphicImage>
			<h:graphicImage url="../resources/images/app_icons/Search-Person.png"
				onclick="javaScript:showSearchContractorPopUp('#{pages$ServiceContractDetailsTab.contractorScreenQueryStringViewMode}','#{pages$ServiceContractDetailsTab.contractorScreenQueryStringPopUpMode}');"
				binding="#{pages$ServiceContractDetailsTab.imgSearchContractor}"
				style="MARGIN: 0px 0px -4px;" alt="#{msg[commons.seachContractor]}"></h:graphicImage>
		</h:panelGroup>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['serviceContract.contractor.name']}:"></h:outputLabel>
		<h:inputText id="txtContractorName" readonly="true"
			binding="#{pages$ServiceContractDetailsTab.txtContractorName}"
			value="#{pages$ServiceContractDetailsTab.contractorName}"
			title="#{pages$ServiceContractDetailsTab.contractorName}"></h:inputText>


		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['contract.date.Start']}:"></h:outputLabel>
		</t:panelGroup>
		<rich:calendar id="contractStartDate"
			value="#{pages$ServiceContractDetailsTab.contractStartDate}"
			popup="true"
			datePattern="#{pages$ServiceContractDetailsTab.dateFormat}"
			binding="#{pages$ServiceContractDetailsTab.startDateCalendar}"
			timeZone="#{pages$ServiceContractDetailsTab.timeZone}"
			showApplyButton="false" enableManualInput="false" 
			inputStyle="width:170px; height:14px;"
			locale="#{pages$ServiceContractDetailsTab.locale}">
		</rich:calendar>

		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel value="#{msg['contract.date.expiry']}:"></h:outputLabel>
		</t:panelGroup>
		<rich:calendar id="datetimeContractEndDate"
			value="#{pages$ServiceContractDetailsTab.contractEndDate}"
			binding="#{pages$ServiceContractDetailsTab.endDateCalendar}"
			popup="true"
			datePattern="#{pages$ServiceContractDetailsTab.dateFormat}"
			timeZone="#{pages$ServiceContractDetailsTab.timeZone}"
			showApplyButton="false" enableManualInput="false" 
			inputStyle="width:170px; height:14px;"			>
		</rich:calendar>

		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['serviceContract.TotalAmount']}:"></h:outputLabel>
		</t:panelGroup>
		<h:inputText id="totalContract"
			onchange="javascript:calculateGuaranteeAmount();" 
			styleClass="A_RIGHT_NUM" maxlength="15"
			binding="#{pages$ServiceContractDetailsTab.txtTotalAmount}"
			value="#{pages$ServiceContractDetailsTab.totalAmount}"></h:inputText>

			<h:outputLabel styleClass="LABEL"
				value="#{msg['serviceContract.guarantee.percentage']}:"></h:outputLabel>
		<h:inputText onchange="javascript:calculateGuaranteeAmount();"
			id="txtGuaranteePercentage" 
			styleClass="A_RIGHT_NUM" maxlength="7"
			binding="#{pages$ServiceContractDetailsTab.txtGuaranteePercentage}"
			value="#{pages$ServiceContractDetailsTab.guaranteePercentage}">%</h:inputText>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['serviceContract.guarantee.bankAmount']}:"></h:outputLabel>
		<h:inputText id="txtBankGuaranteeAmount"
			styleClass="A_RIGHT_NUM" maxlength="15" readonly="true"
			binding="#{pages$ServiceContractDetailsTab.txtGuaranteeAmont}"
			value="#{pages$ServiceContractDetailsTab.guaranteeAmont}"></h:inputText>

		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputText value="#{msg['serviceContract.bankName']}:"></h:outputText>
		</t:panelGroup>
		<h:selectOneMenu id="selectBank" 
			value="#{pages$ServiceContractDetailsTab.selectOneBank}"
			binding="#{pages$ServiceContractDetailsTab.cmbBank}">
			<f:selectItem itemValue="-1"
				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
			<f:selectItems value="#{pages$ServiceContractDetailsTab.banks}" />
		</h:selectOneMenu>

	</t:panelGrid>
</t:div>
