
                                       <t:div id="requestTaskDiv" styleClass="contentDiv"  style="width:98%">
		                                       <t:dataTable id="requestTasksDataTable" 
													rows="#{pages$RequestHistory.paginatorRows}" style="width:100%"
													value="#{pages$RequestHistory.requestTasksViewDataList}"
													binding="#{pages$RequestHistory.requestHistoryDataTable}"
													preserveDataModel="false" preserveSort="false" var="notesDataItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
													
												>
												<t:column id="colUser" >
													<f:facet name="header" >
														<t:outputText value="#{msg['commons.tab.requestHistory.user']}" id="requestTasksDataTablecoluserhdn" />
													</f:facet>
													<t:outputText  styleClass="A_LEFT" value="#{pages$RequestHistory.isEnglishLocale? notesDataItem.createdByFullName:notesDataItem.createdByFullNameAr}" id="requestTasksDataTablecolusertxt"  />
												</t:column>
												<t:column id="colCreatedOn" style="width:20%" >
													<f:facet name="header" >
														<t:outputText value="#{msg['commons.date']}" id="requestTasksDataTablecolcreatedonhn" />
													</f:facet>
													<t:outputText  styleClass="A_LEFT" value="#{notesDataItem.notesDate}"  id="requestTasksDataTablecolcreatedonlbl" >
													<f:convertDateTime timeZone="#{pages$RequestHistory.timeZone}" pattern="#{pages$RequestHistory.dateFormat}" />
													</t:outputText>
													
												</t:column>
												<t:column id="colActivityNameEn" rendered="#{pages$RequestHistory.isEnglishLocale}" >
													<f:facet name="header">
														<t:outputText  value="#{msg['commons.tab.requestHistory.activity']}" id="requestTasksDataTableactivity" />
													</f:facet>
													<t:outputText title="" styleClass="A_LEFT" value="#{notesDataItem.systemNoteEn}" id="requestTasksDataTableactivitytxt"/>
												</t:column>
												<t:column id="colActivityNameAr"  rendered="#{pages$RequestHistory.isArabicLocale}">
													<f:facet name="header" >
														<t:outputText  value="#{msg['commons.tab.requestHistory.activity']}" id="requestTasksDataTableactivityar"/>
													</f:facet>
													<t:outputText title="" styleClass="A_LEFT" value="#{notesDataItem.systemNoteAr}" id="requestTasksDataTableactivitytxtar"/>
												</t:column>
											  </t:dataTable>										
											</t:div>
										
                                           <t:div id="rqtkDivScroller"   styleClass="contentDivFooter"  style="width:99.1%">
                                   
										
															<t:panelGrid id="rqtkRecNumTable"   columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
																<t:div id="rqtkRecNumDiv"  styleClass="RECORD_NUM_BG">
																	<t:panelGrid id="rqtkRecNumTbl" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
																		<h:outputText value="#{msg['commons.recordsFound']}"/>
																		<h:outputText value=" : "/>
																		<h:outputText value="#{pages$RequestHistory.recordSize}"/>
																	</t:panelGrid>	
																		
																</t:div>
																  
															      <CENTER>
																   <t:dataScroller id="rqtkscroller" for="requestTasksDataTable" paginator="true"
																	fastStep="1"  paginatorMaxPages="#{pages$RequestHistory.paginatorMaxPages}" immediate="false"
																	paginatorTableClass="paginator"
																	renderFacetsIfSinglePage="true" 
																    pageIndexVar="pageNumber"
																    styleClass="SCH_SCROLLER"
																    paginatorActiveColumnStyle="font-weight:bold;"
																    paginatorRenderLinkForActive="false" 
																	paginatorTableStyle="grid_paginator" layout="singleTable"
																	paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">
				
																	    <f:facet  name="first">
																			<t:graphicImage    url="../#{path.scroller_first}"  id="lblFRequestHistory"></t:graphicImage>
																		</f:facet>
					
																		<f:facet name="fastrewind">
																			<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRRequestHistory"></t:graphicImage>
																		</f:facet>
					
																		<f:facet name="fastforward">
																			<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFRequestHistory"></t:graphicImage>
																		</f:facet>
					
																		<f:facet name="last">
																			<t:graphicImage  url="../#{path.scroller_last}"  id="lblLRequestHistory"></t:graphicImage>
																		</f:facet>
					
				                                                      <t:div id="rqtkPageNumDiv"   styleClass="PAGE_NUM_BG">
																	   <t:panelGrid id="rqtkPageNumTable"  styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0" >
																	 					<h:outputText  styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																						<h:outputText   styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																		</t:panelGrid>
																			
																		</t:div>
																 </t:dataScroller>
																 </CENTER>
														      
													        </t:panelGrid>
				                           </t:div>
				                           