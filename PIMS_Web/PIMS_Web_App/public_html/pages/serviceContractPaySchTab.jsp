<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<script type="text/javascript">

						
</script>
   <t:div style="width:100%;MARGIN: 0px 4px 0px 0px;">
   
          
        <t:panelGrid width="100%" border="0" columns="4" columnClasses=" , ,BUTTON_TD">
            
            <h:outputText	value="#{msg['serviceContract.PaymentType']}:" binding="#{pages$serviceContractPaySchTab.cmbSelectSubTypeOutput}" ></h:outputText>
			<h:selectOneMenu id="selectPaymentScheduleSubType" style="width:138px;" value="#{pages$serviceContractPaySchTab.selectSubType}" binding="#{pages$serviceContractPaySchTab.cmbSelectSubType}" >
					<f:selectItems value="#{pages$ApplicationBean.paymentScheduleSubType}" />
			</h:selectOneMenu>   
            
            <h:outputText	value="" rendered="#{!pages$serviceContractPaySchTab.paymentSubType}" ></h:outputText>
            <h:outputText	value="" rendered="#{!pages$serviceContractPaySchTab.paymentSubType}" ></h:outputText>        
                    
            <t:panelGroup colspan="2">                    		   
    			<h:commandButton  id="pmt_sch_add" styleClass="BUTTON" value="#{msg['contract.AddpaymentSchedule']}"
  					                binding="#{pages$serviceContractPaySchTab.btnAddPayments}"
  					                action="#{pages$serviceContractPaySchTab.btnAddPayments_Click}"
  					                style="width:150px;"
  					                
		       />
			   <h:commandButton  id="pmt_sch_gen" styleClass="BUTTON" value="#{msg['contract.paymentSchedule.GeneratePayments']}" 
			                  style="width:150px;" action="#{pages$serviceContractPaySchTab.btnGenPayments_Click}" 
			                  binding="#{pages$serviceContractPaySchTab.btnGenPayments}"
			   />
           </t:panelGroup>
 		</t:panelGrid>  
 	</t:div>
                       		
	 <t:div styleClass="contentDiv" style="width:98.6%"  >
	           <t:dataTable 
                id="tbl_PaymentSchedule" rows="#{pages$serviceContractPaySchTab.paginatorRows}" width="100%" value="#{pages$serviceContractPaySchTab.paymentScheduleDataList}"
				binding="#{pages$serviceContractPaySchTab.paymentScheduleDataTable}" preserveDataModel="true"  var="paymentScheduleDataItem"
				rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
								<t:column id="select" style="width:5%;" rendered="#{pages$serviceContractPaySchTab.isDirectionTypeInbound }">
									 <f:facet name="header">
										  <t:outputText id="selectTxt" value="#{msg['commons.select']}"/>
									 </f:facet>
										  <t:selectBooleanCheckbox id="selectChk" value="#{paymentScheduleDataItem.selected}" rendered="#{paymentScheduleDataItem.isReceived == 'N'  && paymentScheduleDataItem.statusId==pages$serviceContractPaySchTab.paymentSchedulePendingStausId && paymentScheduleDataItem.paymentScheduleId>0 && paymentScheduleDataItem.isDeleted==0}"/>
								</t:column>
								<t:column id="col_InvNo" style="width:15%;" rendered="#{!pages$serviceContractPaySchTab.isDirectionTypeInbound }">
									<f:facet name="header" >
										<t:outputText value="#{msg['paymentSchedule.InvoiceNumber']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT"  value="#{paymentScheduleDataItem.invoiceNumber}" rendered="#{paymentScheduleDataItem.isDeleted==0}" />
								</t:column>
								<t:column id="col_PaymentNumber" style="width:15%;" rendered="#{!pages$serviceContractPaySchTab.isDirectionTypeInbound }" >
									<f:facet name="header" >
										<t:outputText value="#{msg['paymentSchedule.paymentNumber']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT"  value="#{paymentScheduleDataItem.paymentNumber}"  rendered="#{paymentScheduleDataItem.isDeleted==0}"/>
								</t:column>
								<t:column id="col_ReceiptNumber" style="width:15%;" rendered="#{!pages$serviceContractPaySchTab.isDirectionTypeInbound }" >
									<f:facet name="header" >
										<t:outputText value="#{msg['paymentSchedule.ReceiptNumber']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT"  value="#{paymentScheduleDataItem.paymentReceiptNumber}"  rendered="#{paymentScheduleDataItem.isDeleted==0}"/>
								</t:column>
								<t:column id="colTypeEn" style="width:8%;" rendered="#{pages$serviceContractPaySchTab.isEnglishLocale }">
									<f:facet name="header">
										<t:outputText  value="#{msg['paymentSchedule.paymentType']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT"  title="" value="#{paymentScheduleDataItem.typeEn}" rendered="#{paymentScheduleDataItem.isDeleted==0}" />
								</t:column>
								<t:column id="colTypeAr" style="width:8%;" rendered="#{pages$serviceContractPaySchTab.isArabicLocale }">
									<f:facet name="header">
										<t:outputText  value="#{msg['paymentSchedule.paymentType']}" />
									</f:facet>
									<t:outputText     styleClass="A_LEFT"  title="" value="#{paymentScheduleDataItem.typeAr}" rendered="#{paymentScheduleDataItem.isDeleted==0}" />
								</t:column>
								<t:column id="colSubTypeEn" style="width:8%;" rendered="#{pages$serviceContractPaySchTab.isEnglishLocale &&  pages$serviceContractPaySchTab.paymentSubType}">
									<f:facet name="header">
										<t:outputText  value="#{msg['paymentConfiguration.paymentSubType']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT"  title="" value="#{paymentScheduleDataItem.paymentScheduleSubTypeEn}" rendered="#{paymentScheduleDataItem.isDeleted==0}" />
								</t:column>
								<t:column id="colSubTypeAr" style="width:8%;" rendered="#{pages$serviceContractPaySchTab.isArabicLocale &&  pages$serviceContractPaySchTab.paymentSubType}">
									<f:facet name="header">
										<t:outputText  value="#{msg['paymentConfiguration.paymentSubType']}" />
									</f:facet>
									<t:outputText     styleClass="A_LEFT"  title="" value="#{paymentScheduleDataItem.paymentScheduleSubTypeAr}" rendered="#{paymentScheduleDataItem.isDeleted==0}" />
								</t:column>
								<t:column id="colDesc"  >
									<f:facet name="header">
										<t:outputText  value="#{msg['paymentSchedule.description']}" />
									</f:facet>
									<t:outputText     styleClass="A_LEFT"  title="" value="#{paymentScheduleDataItem.description}" rendered="#{paymentScheduleDataItem.isDeleted==0}" />
								</t:column>
								<t:column id="colDueOn" style="width:8%;" >
									<f:facet name="header">
										<t:outputText  value="#{msg['paymentSchedule.paymentDueOn']}" />
									</f:facet>
									<t:outputText     styleClass="A_LEFT" value="#{paymentScheduleDataItem.paymentDueOn}" rendered="#{paymentScheduleDataItem.isDeleted==0}" >
									<f:convertDateTime pattern="#{pages$serviceContractPaySchTab.dateFormat}" timeZone="#{pages$serviceContractPaySchTab.timeZone}" />
								    </t:outputText>
								</t:column>
                                            <t:column id="colPaymentDate" style="width:8%;" rendered ="false">
									<f:facet name="header">

										<t:outputText  value="#{msg['paymentSchedule.paymentDate']}" >
                                                      <f:convertDateTime pattern="#{pages$serviceContractPaySchTab.dateFormat}" timeZone="#{pages$serviceContractPaySchTab.timeZone}" />
								        </t:outputText>
									</f:facet>
									<t:outputText     styleClass="A_LEFT"  title="" value="#{paymentScheduleDataItem.paymentDate}" >
								       <f:convertDateTime pattern="#{pages$serviceContractPaySchTab.dateFormat}" timeZone="#{pages$serviceContractPaySchTab.timeZone}" />
								    </t:outputText>
								</t:column>
																				
								<t:column id="colModeEn" style="width:8%;" rendered="#{pages$serviceContractPaySchTab.isEnglishLocale }">


									<f:facet name="header" >

										<t:outputText value="#{msg['paymentSchedule.paymentMode']}" />

									</f:facet>
									<t:outputText    
									              styleClass="A_LEFT"  title=""
									              value="#{paymentScheduleDataItem.paymentModeEn}" rendered="#{paymentScheduleDataItem.isDeleted==0}" />
								</t:column>
								<t:column id="colModeAr" style="width:8%;" rendered="#{pages$serviceContractPaySchTab.isArabicLocale }">


									<f:facet name="header">

										<t:outputText  value="#{msg['paymentSchedule.paymentMode']}" />

									</f:facet>
									<t:outputText    
									              styleClass="A_LEFT"  title="" 
									              value="#{paymentScheduleDataItem.paymentModeAr}" rendered="#{paymentScheduleDataItem.isDeleted==0}" />
								</t:column>
								<t:column  id="colStatusEn" rendered="#{pages$serviceContractPaySchTab.isEnglishLocale }">


									<f:facet name="header" >

										<t:outputText value="#{msg['paymentSchedule.paymentStatus']}" />

									</f:facet>
									<t:outputText styleClass="A_LEFT"  title="" value="#{paymentScheduleDataItem.statusEn}" rendered="#{paymentScheduleDataItem.isDeleted==0}" />
								</t:column>
								<t:column id="colStatusAr" rendered="#{pages$serviceContractPaySchTab.isArabicLocale }">


									<f:facet name="header">

										<t:outputText  value="#{msg['paymentSchedule.paymentStatus']}" />

									</f:facet>
									<t:outputText    
									              styleClass="A_LEFT"  title="" 
									              value="#{paymentScheduleDataItem.statusAr}" rendered="#{paymentScheduleDataItem.isDeleted==0}" />
								</t:column>
								<t:column id="colReceiptNumber" style="width:15%;" rendered="false">
									<f:facet name="header">
										<t:outputText  value="#{msg['paymentSchedule.ReceiptNumber']}" />
									</f:facet>
									<t:outputText    
									              styleClass="A_LEFT"  title="" 
									              value="#{paymentScheduleDataItem.paymentReceiptNumber}" rendered="#{paymentScheduleDataItem.isDeleted==0}" />
								</t:column>
								<t:column id="colAmount"  >
									<f:facet name="header">
										<t:outputText  value="#{msg['paymentSchedule.amount']}" />
									</f:facet>
									<t:outputText title="" styleClass="A_RIGHT_NUM" value="#{paymentScheduleDataItem.amount}" rendered="#{paymentScheduleDataItem.isDeleted==0}" />
								</t:column>
								 <t:column id="col8"  >
									<f:facet name="header">
										<t:outputText  value="#{msg['commons.action']}" />
									</f:facet>
									<h:commandLink action="#{pages$serviceContractPaySchTab.btnPrintPaymentSchedule_Click}" rendered="#{pages$serviceContractPaySchTab.isPrintPaymentReceiptShow && paymentScheduleDataItem.isDeleted==0}">
								      <h:graphicImage id="printPayments" title="#{msg['commons.print']}" url="../resources/images/app_icons/print.gif" rendered="#{paymentScheduleDataItem.isReceived == 'Y'}"/>
									</h:commandLink>
									<h:commandLink action="#{pages$serviceContractPaySchTab.btnEditPaymentSchedule_Click}" rendered="#{paymentScheduleDataItem.isDeleted==0}"  >
									    <h:graphicImage id="editPayments" 
														title="#{msg['commons.edit']}" 
														url="../resources/images/edit-icon.gif"
														binding="#{pages$serviceContractPaySchTab.imgEditPayments}"
														rendered="#{paymentScheduleDataItem.isReceived == 'N'}"
														/>
																												
									</h:commandLink>
									<a4j:commandLink reRender="tbl_PaymentSchedule,paymentSummary" action="#{pages$serviceContractPaySchTab.btnDelPaymentSchedule_Click}" rendered="#{paymentScheduleDataItem.isDeleted==0}" >
								      <h:graphicImage id="deletePaySc" 
											title="#{msg['commons.delete']}" rendered="#{(paymentScheduleDataItem.isReceived == 'N' )}" url="../resources/images/delete_icon.png" binding="#{pages$serviceContractPaySchTab.imgDelPayments}" />														
									</a4j:commandLink>
									
									<h:commandLink action="#{pages$serviceContractPaySchTab.btnViewPaymentDetails_Click}">
										<h:graphicImage id="viewPayments" 
														title="#{msg['commons.group.permissions.view']}" 
														url="../resources/images/detail-icon.gif"
										/>
									</h:commandLink> 
									             
								</t:column>
							  </t:dataTable>		
							  								
						    </t:div>
						    <t:div id="divPaySch" styleClass="contentDivFooter" style="width:99.6%;">

								<t:dataScroller id="scrollerPaySch" for="tbl_PaymentSchedule" paginator="true"
									fastStep="1" paginatorMaxPages="#{pages$serviceContractPaySchTab.paginatorMaxPages}" immediate="false"
									paginatorTableClass="paginator"
									renderFacetsIfSinglePage="true" 
									pageIndexVar="pageNumber"
									paginatorTableStyle="grid_paginator" layout="singleTable"
									paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
									paginatorActiveColumnStyle="font-weight:bold;"
									paginatorRenderLinkForActive="false" 
										
										styleClass="SCH_SCROLLER">
                                            	<f:facet  name="first">
										<t:graphicImage  url="../#{path.scroller_first}"  id="lblFPaySch"></t:graphicImage>
									</f:facet>

									<f:facet name="fastrewind">
										<t:graphicImage  url="../#{path.scroller_fastRewind}"  id="lblFRPaySch"></t:graphicImage>
									</f:facet>

									<f:facet name="fastforward">
										<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFPaySch"></t:graphicImage>
									</f:facet>

									<f:facet name="last">
										<t:graphicImage  url="../#{path.scroller_last}" id="lblLPaySch"></t:graphicImage>
									</f:facet>


								</t:dataScroller>
					
                                       </t:div>
                                <t:div id="divCollectPayment" styleClass="BUTTON_TD" style="padding:10px">
												<h:commandButton id="btnCollectPayment" binding="#{pages$serviceContractPaySchTab.btnCollectPayment}" value="#{msg['settlement.actions.collectpayment']}" styleClass="BUTTON" action="#{pages$serviceContractPaySchTab.openReceivePaymentsPopUp}" style="width: 150px" />
								</t:div>
				                                          
