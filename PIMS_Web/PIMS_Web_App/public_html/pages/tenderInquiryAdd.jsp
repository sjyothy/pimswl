<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript"> 

            function showSearchTenderPopUp()
		   {
			var screen_width = screen.width;
			var screen_height = screen.height;
			var popup_width = screen_width-180;
			var popup_height = screen_height-240;
			var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
			
			var popup = window.open('tenderSearch.jsf?VIEW_MODE=popup&context=TenderInquiry','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
			popup.focus();
		   }	
           
         function showSearchConstructionTenderPopUp()
	     {
	     
	     var  screen_width = 1024;
	     var screen_height = 450;
	     window.open('constructionTenderSearch.jsf?VIEW_MODE=popup&context=TenderInquiry','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=20,top=150,scrollbars=yes,status=yes');
       	}

		function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		
		function populateContractor(contractorId, contractorNumber, contractorNameEn, contractorNameAr, licenseNumber, licenseSource, officeNumber){
			document.getElementById("tenderInquiryFrm:hdnContractorId").value=contractorId;
			document.getElementById("tenderInquiryFrm:hdnContractorNumber").value=contractorNumber;
			document.getElementById("tenderInquiryFrm:hdnContractorNameEn").value=contractorNameEn;
			document.getElementById("tenderInquiryFrm:hdnContractorNameAr").value=contractorNameAr;
			document.getElementById("tenderInquiryFrm:hdnLicencseNumber").value=licenseNumber;
			
		    document.forms[0].submit();
		}
		
		function populateTender(tenderId,tenderNumber,tenderDescription)
		{
			 document.getElementById("tenderInquiryFrm:hdnTenderId").value=tenderId;
			 document.getElementById("tenderInquiryFrm:hdnTenderNumber").value=tenderNumber;
			 document.getElementById("tenderInquiryFrm:hdnTenderDescription").value=tenderDescription;
		     
		     document.forms[0].submit();
		}
		
		function submitForm()
		{
			window.document.forms[0].submit();
		}	
	 
		function showContractorPopup()
		{
			var screen_width = screen.width;
			var screen_height = screen.height;
			var popup_width = screen_width-180;
			var popup_height = screen_height-265;
			var leftPos = (screen_width-popup_width)/2 -5, topPos = (screen_height-popup_height)/2 - 20;
			var popup = window.open('contractorSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
			popup.focus();
		}
		
3	   
       function closeWindow() 
       { 
	      window.close();
	   }    
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 


	
</head>
	<body class="BODY_STYLE">
	   <div class="containerDiv">
	<%	
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header --> 
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<c:choose>
		<c:when test="${!pages$tenderInquiryAdd.isViewModePopUp}">
			<tr>
				<td colspan="2">
					<jsp:include page="header.jsp" />
				</td>
			</tr>
		</c:when>
	</c:choose>
			
		<tr width="100%">
		<c:choose>
		  <c:when test="${!pages$tenderInquiryAdd.isViewModePopUp}">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
		 </c:when>
	   </c:choose>	 	
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['tenderInquiryAdd.heading']}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>
				<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%">
					<tr valign="top">
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
						<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
							<div class="SCROLLABLE_SECTION" style="height:475px;width:100%;#height:466px;">
									<h:form id="tenderInquiryFrm" enctype="multipart/form-data" style="WIDTH: 96%;">
	
									<h:inputHidden id="hdnContractorId" value="#{pages$tenderInquiryAdd.hdnContractorId}"></h:inputHidden>
									<h:inputHidden id="hdnContractorNumber" value="#{pages$tenderInquiryAdd.hdnContractorNumber}"></h:inputHidden>
									<h:inputHidden id="hdnContractorNameEn" value="#{pages$tenderInquiryAdd.hdnContractorNameEn}"></h:inputHidden>
									<h:inputHidden id="hdnContractorNameAr" value="#{pages$tenderInquiryAdd.hdnContractorNameAr}"></h:inputHidden>
									<h:inputHidden id="hdnTenderId" value="#{pages$tenderInquiryAdd.hdnTenderId}"></h:inputHidden>
									<h:inputHidden id="hdnTenderNumber" value="#{pages$tenderInquiryAdd.hdnTenderNumber}"></h:inputHidden>
									<h:inputHidden id="hdnTenderDescription" value="#{pages$tenderInquiryAdd.hdnTenderDescription}"></h:inputHidden>
									<h:inputHidden id="hdnApplicationDate" value="#{pages$tenderInquiryAdd.applicationDate}"></h:inputHidden>
									<h:inputHidden id="hdnRequest" value="#{pages$tenderInquiryAdd.hdnRequestId}"></h:inputHidden>
									<h:inputHidden id="hdnLicencseNumber" value="#{pages$tenderInquiryAdd.hdnLicencseNumber}"></h:inputHidden>
									
									
								
								<div >
										<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
											<tr>
												<td>
													<h:outputText value="#{pages$tenderInquiryAdd.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$tenderInquiryAdd.infoMessages}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
								 </div>
									
								<div>
								 
					
						<div class="AUC_DET_PAD">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
						<div class="TAB_PANEL_INNER"> 
							<rich:tabPanel  id="tabPanel" switchType="server" style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%" >
								<rich:tab id="InquiryDetailsTab" label="#{msg['tenderInquiryAdd.inquiryTabHeading']}">
						<t:div  styleClass="TAB_DETAIL_SECTION" style="width:100%" >
							<t:panelGrid id="InquiryDetailsTable" cellpadding="1px" width="100%" cellspacing="5px"  styleClass="TAB_DETAIL_SECTION_INNER"  columns="4">
								        <h:outputLabel styleClass="LABEL" value="#{msg['tenderInquiryAdd.inquiryNumber']}: #{pages$tenderInquiryAdd.asterisk}" />
									    <h:inputText  maxlength="20" id="txtApplicationNum" readonly="true" styleClass="READONLY"  value="#{pages$tenderInquiryAdd.applicationNumber}" style="width:190px; height: 18px"></h:inputText>
								        <h:outputLabel styleClass="LABEL" value="#{msg['tenderInquiryAdd.inquiryStatus']}: #{pages$tenderInquiryAdd.asterisk}"></h:outputLabel>
										<h:inputText id="txtApplicationStatus" style="width:190px; height: 18px" styleClass="READONLY" readonly="true" maxlength="20" value="#{pages$tenderInquiryAdd.applicationStatus}" />
					                    <h:outputLabel styleClass="LABEL" value="#{msg['tenderInquiryAdd.inquiryDate']}: #{pages$tenderInquiryAdd.asterisk}"/>
					                    <h:inputText id="txtApplicationDate" style="width:190px; height: 18px"  styleClass="READONLY"  readonly="true" value="#{pages$tenderInquiryAdd.applicationDate}">
					                    <f:convertDateTime pattern="#{pages$tenderInquiryAdd.dateFormat}"
														   timeZone="#{pages$tenderInquiryAdd.timeZone}" />
										</h:inputText>
										<h:panelGroup>	
										 <h:outputLabel styleClass="mandatory" value="*"/>							
					       				<h:outputLabel  styleClass="LABEL" style="width: 160px;" value="#{msg['application.source']}: #{pages$tenderInquiryAdd.asterisk}"/>
					       				</h:panelGroup>
					                    <h:selectOneMenu id="applicationSourceCombo" style="width: 192px;" required="false"
					                         styleClass="#{pages$tenderInquiryAdd.inReadOnlyMode}" readonly="#{pages$tenderInquiryAdd.inViewMode}"   
					                         value="#{pages$tenderInquiryAdd.applicationSourceId}">
											<f:selectItem itemValue="-1" itemLabel="#{msg['commons.pleaseSelect']}" />
											<f:selectItems value="#{pages$ApplicationBean.applicationSourceList}" />
										</h:selectOneMenu>
										<h:panelGroup>	
										 <h:outputLabel styleClass="mandatory" value="*"/>
								        <h:outputLabel styleClass="LABEL" value="#{msg['contractor.name']}: #{pages$tenderInquiryAdd.asterisk}"></h:outputLabel>
								         </h:panelGroup>
								        <t:panelGrid id="gridContractor" columns="2" width="70%" cellpadding="0" cellspacing="1" >
									        <h:inputText id="txtContractorName" readonly="true" styleClass="READONLY" style="width:190px; height: 18px" value="#{pages$tenderInquiryAdd.contractorName}"></h:inputText>
										    <h:commandLink onclick="showContractorPopup();" rendered="#{!pages$tenderInquiryAdd.inViewMode}" >
										   		<h:graphicImage id="imgPerson"  style="MARGIN: 0px 0px -4px" url="../resources/images/app_icons/Add-Person.png"></h:graphicImage>
										   </h:commandLink>
					                    </t:panelGrid>
									    <h:outputLabel id="lblAppType" value="#{msg['ContractorSearch.licenseNumber']}: #{pages$tenderInquiryAdd.asterisk}"></h:outputLabel>
										<h:inputText id="txtContractorType" style="width:190px; height:18px;" styleClass="READONLY" maxlength="15"  readonly="true" value="#{pages$tenderInquiryAdd.licencseNumber}" ></h:inputText>
						               <h:panelGroup>	
										 <h:outputLabel styleClass="mandatory" value="*"/>
										<h:outputLabel styleClass="LABEL" value="#{msg['tender.number']}: #{pages$tenderInquiryAdd.asterisk}"></h:outputLabel>
										 </h:panelGroup>
										<t:panelGrid id="gridTender" columns="2" width="70%" cellpadding="0" cellspacing="1" >
									        <h:inputText id="txtTenderNumber" style="width:190px; height: 18px" styleClass="READONLY"  maxlength="20"  readonly="true" value="#{pages$tenderInquiryAdd.tenderNumber}" ></h:inputText>
										    <h:commandLink action="#{pages$tenderInquiryAdd.onSearchTender}" rendered="#{!pages$tenderInquiryAdd.inViewMode}" >
										   		<h:graphicImage id="imgTender"  style="MARGIN: 0px 0px -4px" url="../resources/images/app_icons/Add-Person.png"></h:graphicImage>
										   </h:commandLink>
					                    </t:panelGrid>
								        <h:outputLabel  styleClass="LABEL"  value="#{msg['tender.description']}: #{pages$tenderInquiryAdd.asterisk}"></h:outputLabel>
								        <h:inputText id="txtTenderDesc" styleClass="READONLY" readonly="true" style="width:190px; height: 18px" value="#{pages$tenderInquiryAdd.tenderDes}" rendered="true"></h:inputText>
								        
								        <h:outputLabel styleClass="LABEL"  value="#{msg['tenderInquiryAdd.inquiryDetails']}: #{pages$tenderInquiryAdd.asterisk}"></h:outputLabel>
			                            <t:inputTextarea  value="#{pages$tenderInquiryAdd.reasons}"  
			                            styleClass="TEXTAREA  #{pages$tenderInquiryAdd.inReadOnlyMode}" readonly="#{pages$tenderInquiryAdd.inViewMode}" rows="6" style="width: 200px;"/>

								        <h:outputLabel value=""/>
								        <h:outputLabel value=""/>
					                  
							</t:panelGrid>	              
								</t:div>
								</rich:tab>
							    <rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}">
									<%@  include file="attachment/attachment.jsp"%>
								</rich:tab>
								<rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}">
									<%@ include file="notes/notes.jsp"%>
								</rich:tab>
							</rich:tabPanel>
							</div>
							<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT" /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID" style="width:100%;" /></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
							</table>
						<t:div styleClass="BUTTON_TD" style="padding-top:10px;">
							<t:commandButton styleClass="BUTTON" 
								value="#{msg['commons.cancel']}"
								onclick="if (!confirm('#{msg['extendApplication.details.confirm.cancel']}')) return false" 
					        	action="#{pages$tenderInquiryAdd.cancel}"
					        	rendered="#{!pages$tenderInquiryAdd.inViewMode}"
					        	>
					       	</t:commandButton>
						    <t:commandButton styleClass="BUTTON" 
							    value="#{msg['commons.saveButton']}" 
							    rendered="#{!pages$tenderInquiryAdd.inViewMode}" 
							    action="#{pages$tenderInquiryAdd.saveInquiry}" tabindex="13" />
						    <h:commandButton styleClass="BUTTON"
								value="#{msg['commons.cancel']}"
								rendered="#{pages$tenderInquiryAdd.inViewMode}"
								onclick="closeWindow();"/>
							<h:commandButton styleClass="BUTTON"
								value="#{msg['commons.reset']}"
								action="#{pages$tenderInquiryAdd.onReset}"
								rendered="#{!pages$tenderInquiryAdd.inViewMode}">
								</h:commandButton>	
						</t:div>
						</div>
								</div>
							</h:form>
					  </div> 
						</td>			
					</tr>
				</table>
			</td>
		</tr>
		<c:choose>
		  <c:when test="${!pages$tenderInquiryAdd.isViewModePopUp}">
		         <tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</c:when>
		</c:choose>		
	</table>
  </div>
</body>
</html>
</f:view>