<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>

<script language="javascript" type="text/javascript">

// Method Added for setting the default button to Submit Button

function submitOnEnter(myfield,e)
 {
 var keycode;
 if (window.event) keycode = window.event.keyCode;
 else if (e) keycode = e.which;
 else return true;
 
 if (keycode == 13)
    {
    
    document.getElementById('loginForm:loginbutton').onclick();
    
    return false;
    }
 else
    return true;
 }
 
 function reset()
 {
  document.getElementById("userName").value="";
  document.getElementById("password").value="";
  return false;
 }
 function authenticate()
 {
 	document.getElementById("loginForm:loginbutton").onclick();
 }
 //document.getElementById("usernameid").focus();
 
</script>
<f:view>
  <html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}"
  style="overflow:hidden;">
    <head>     
        <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache,must-revalidate,no-store">
	<meta http-equiv="expires" content="0">



	<link rel="stylesheet" type="text/css" href="../resources/css/simple_en.css" />
	<link rel="stylesheet" type="text/css" href="../resources/css/amaf_en.css" />
        <title>PIMS</title>
    </head>
    <body class="LoginBody" style="background-color:#333333">
  <div style="height:0px">
    <table border="0" class="layoutTable"  style="text-align:center;" width="100%"> 
		<tr>
			<td>
			<h:outputText id="successMsg_2" value="#{pages$index.messages}" escape="false"  styleClass="ERROR_FONT"/>
			</td>
		</tr>
	</table>
</div>	
	 
    <div>
    
   <h:form id="loginForm"  onkeypress="return submitOnEnter(this,event);">
    <b><h:outputLabel id="usernameid" styleClass="EnglishInput LoginUserNameLabel" value="#{msg['commons.username']}" /> </b>
    <b><h:outputLabel styleClass="EnglishInput LoginPasswordLabel" value="#{msg['commons.password']}" /> </b>
    <h:inputText  binding="#{pages$index.userName}" id="userName" styleClass="EnglishInput LoginUserNameField"/>
    <h:inputSecret binding="#{pages$index.password}" id="password" styleClass="EnglishInput LoginPasswordField"/>
     <input type="radio" value="1" name="idNumber" class="englishLoginRadio">
     <input type="radio" value="2" name="idNumber" class="arabicLoginRadio" checked="checked">
     
     <img src="../resources/images/login_btn.png" class="LoginButton" alt="Login" onclick="authenticate();"></img>      
     <img src="../resources/images/login_btn.png" class="CancelButton" alt="Cancel" onclick="return reset();"></img>
      
     
	<h:commandLink id="loginbutton"  immediate="true" styleClass="LoginButtonLabel" action="#{pages$index.authenticate}" value="#{msg['commons.login']}" >
     </h:commandLink>
          
          
     <h:commandLink  immediate="true" styleClass="LoginCancelButtonLabel" onclick="return reset();" value="#{msg['commons.cancel']}">
     </h:commandLink>
     
     <h:inputSecret value="" rendered="false"
            onkeypress="return submitOnEnter(this,event);"/>
    
     </h:form>
   </div>
        
    </body>
    <script language="javascript">
     var iid=document.getElementById('loginForm:userName');
     iid.focus();
     //document.getElementById(iid).focus();
  </script>
    
  </html>
</f:view>