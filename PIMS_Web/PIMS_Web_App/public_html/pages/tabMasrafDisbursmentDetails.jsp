<t:div id="tabDDetails" style="width:100%;">
	<t:panelGrid id="ddTab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4"
		rendered="#{
							!( 
				  				pages$masrafDisbursement.pageMode=='PAGE_MODE_VIEW'  ||
				  				pages$masrafDisbursement.pageMode=='PAGE_MODE_DISBURSEMENT_REQUIRED'
			  			 	) 
					   	   }">

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblBenef" styleClass="LABEL"
				value="#{msg['revenueFollowup.lbl.endowment']}" />
		</h:panelGroup>
		<h:panelGroup>
			<h:inputText id="endowmentName" maxlength="20" readonly="true"
				styleClass="READONLY"
				value="#{pages$masrafDisbursement.disbursementDetail.endowmentName}" />
			<h:commandLink
				action="#{pages$masrafDisbursement.onOpenEndowemntSearchPopup}">
				<h:graphicImage id="endowmentSearchpopup" style="margin-right:5px;"
					title="#{msg['endowment.search.title']}"
					url="../resources/images/magnifier.gif" />
			</h:commandLink>
			<h:graphicImage id="endowmentDetailspopup"
				rendered="#{
			    			!empty pages$masrafDisbursement.disbursementDetail.endowmentId  
			    		   }"
				style="margin-right:5px;" title="#{msg['commons.details']}"
				url="../resources/images/app_icons/No-Objection-letter.png"
				onclick="javaScript:openEndowmentManagePopup('#{pages$masrafDisbursement.disbursementDetail.endowmentId}');" />
		</h:panelGroup>
		<h:outputLabel id="lblBalance" styleClass="LABEL"
			value="#{msg['mems.normaldisb.label.balance']}:"
			rendered="#{
		                pages$masrafDisbursement.pageMode=='NEW'  || 
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_RESUBMITTED' ||
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_APPROVAL'  || 
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_DISBURSEMENT_REQUIRED' 
		  				 
			           }">
		</h:outputLabel>
		<h:inputText id="accountBalance" readonly="true"
			rendered="#{
		                pages$masrafDisbursement.pageMode=='NEW'  || 
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_RESUBMITTED'||
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_APPROVAL'  || 
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_DISBURSEMENT_REQUIRED' 
			           }"
			styleClass="READONLY"
			binding="#{pages$masrafDisbursement.accountBalance}">
		</h:inputText>

		<h:outputLabel id="labelrequestedAmount" styleClass="LABEL"
			value="#{msg['commons.requestedAmount']}:"
			rendered="#{
		                pages$masrafDisbursement.pageMode=='NEW'  || 
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_RESUBMITTED'||
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_APPROVAL'  || 
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_DISBURSEMENT_REQUIRED' 
			           }">
		</h:outputLabel>
		<h:panelGroup
			rendered="#{
		                pages$masrafDisbursement.pageMode=='NEW'  || 
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_RESUBMITTED'||
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_APPROVAL'  || 
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_DISBURSEMENT_REQUIRED' 
			           }">
			<h:inputText id="requestedAmount" readonly="true"
				styleClass="READONLY"
				binding="#{pages$masrafDisbursement.requestedAmount}">
			</h:inputText>
			<h:commandLink id="lnkDisbDetails"
				action="#{pages$masrafDisbursement.onOpenRequestDisbursementDetailsForMasrafPopup}">
				<h:graphicImage id="imgOpenRequestDisbursementDetailsForMasrafPopup"
					style="margin-right:5px;"
					title="#{msg['commons.disbursementDetails']}"
					url="../resources/images/app_icons/manage_tender.png" />
			</h:commandLink>
		</h:panelGroup>
		<h:outputLabel id="labelAVBalance" styleClass="LABEL"
			value="#{msg['commons.availableBalance']}:"
			rendered="#{
		                pages$masrafDisbursement.pageMode=='NEW'  || 
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_RESUBMITTED'||
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_APPROVAL'  ||
		  				 pages$masrafDisbursement.pageMode=='PAGE_MODE_DISBURSEMENT_REQUIRED' 
			           }">
		</h:outputLabel>
		<h:inputText id="availableBalance" readonly="true"
			styleClass="READONLY"
			rendered="#{
		                pages$masrafDisbursement.pageMode=='NEW'  || 
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_RESUBMITTED'||
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_APPROVAL'  || 
		  				pages$masrafDisbursement.pageMode=='PAGE_MODE_DISBURSEMENT_REQUIRED'
		  				
			           }"
			binding="#{pages$masrafDisbursement.availableBalance}">
		</h:inputText>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="amnt" styleClass="LABEL"
				value="#{msg['commons.amount']}:"></h:outputLabel>
		</h:panelGroup>
		<h:inputText id="txtAmount"
			value="#{pages$masrafDisbursement.disbursementDetail.amountString }"
			onkeyup="removeNonNumeric(this)" onkeypress="removeNonNumeric(this)"
			onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
			onchange="removeNonNumeric(this)" />


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblDescription" styleClass="LABEL"
				style="width: 25%" value="#{msg['commons.description']}" />
		</h:panelGroup>
		<t:panelGroup id="descTab" colspan="3">
			<h:inputTextarea id="invDetails"
				value="#{pages$masrafDisbursement.disbursementDetail.description}"
				style="width: 97%; height: 70px;" />
		</t:panelGroup>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['cancelContract.payment.paymentmethod']} :"></h:outputLabel>

		<h:selectOneMenu id="paymentMethodId"
			value="#{pages$masrafDisbursement.disbursementDetail.paymentDetails[0].paymentMethodString}"
			tabindex="3">

			<f:selectItems
				value="#{pages$ApplicationBean.paymentMethodsFromPMTable}" />
		</h:selectOneMenu>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['paymentDetailsPopUp.paidTo']}:"></h:outputLabel>

		<h:inputText id="txtPaidTo"
			value="#{pages$masrafDisbursement.disbursementDetail.paymentDetails[0].others}" />
	</t:panelGrid>

	<t:div styleClass="BUTTON_TD"
		rendered="#{ 
	                 pages$masrafDisbursement.pageMode=='NEW'  || 
	  				 pages$masrafDisbursement.pageMode=='PAGE_MODE_RESUBMITTED'||
	  				 pages$masrafDisbursement.pageMode=='PAGE_MODE_APPROVAL'
				  	 
			  	   }"
		style="padding-top:10px; padding-right:21px;">
		<h:commandButton styleClass="BUTTON" value="#{msg['commons.Add']}"
			onclick="performTabClick('lnkAdd');">
			<h:commandLink id="lnkAdd" action="#{pages$masrafDisbursement.onAdd}" />
		</h:commandButton>
	</t:div>

	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTablePayments" rows="15" width="100%"
			value="#{pages$masrafDisbursement.dataList}"
			binding="#{pages$masrafDisbursement.dataTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
			<t:column id="checkbox" style="width:auto;"
				rendered="#{pages$masrafDisbursement.pageMode!='PAGE_MODE_VIEW' ||
																	pages$masrafDisbursement.pageMode=='PAGE_MODE_DISBURSEMENT_REQUIRED' 
																	}">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.select']}" />
				</f:facet>
				<h:selectBooleanCheckbox id="idbox" immediate="true"
					rendered="#{
						  				
					  				pages$masrafDisbursement.pageMode=='PAGE_MODE_DISBURSEMENT_REQUIRED' && 
					  				dataItem.isDisbursed=='0' &&
					  				dataItem.status != '165006'  
							  }"
					value="#{dataItem.selected}">
				</h:selectBooleanCheckbox>
			</t:column>
			<t:column id="refNumber" sortable="true">
				<f:facet name="header">
					<t:outputText id="refNum"
						value="#{msg['chequeList.paymentNumber']}" />
				</f:facet>
				<t:outputText id="refNumField" styleClass="A_LEFT"
					value="#{dataItem.refNum}" />
			</t:column>
			<t:column id="statusCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="statusAmnt" value="#{msg['commons.status']}" />
				</f:facet>
				<t:outputText id="status" styleClass="A_LEFT"
					value="#{pages$masrafDisbursement.englishLocale? dataItem.statusEn:dataItem.statusAr}" />
			</t:column>
			<t:column id="amntCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAmnt"
						value="#{msg['mems.payment.request.label.Amount']}" />
				</f:facet>
				<t:outputText id="amountt" styleClass="A_LEFT"
					value="#{dataItem.amountString}" />
			</t:column>
			<t:column id="paymentmethodCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdpaymentmethod"
						value="#{msg['cancelContract.payment.paymentmethod']}" />
				</f:facet>
				<t:outputText id="paymentmethodd" styleClass="A_LEFT"
					value="#{pages$masrafDisbursement.englishLocale? dataItem.paymentDetails[0].paymentMethodEn:
																	 dataItem.paymentDetails[0].paymentMethodAr
								      }" />
			</t:column>
			<t:column id="paidToCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdpaidTo"
						value="#{msg['paymentDetailsPopUp.paidTo']}" />
				</f:facet>
				<t:outputText id="hdpaidTot" styleClass="A_LEFT"
					value="#{dataItem.paymentDetails[0].others}" />
			</t:column>
			<t:column id="desc" sortable="true" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText id="hdTo" value="#{msg['commons.description']}" />
				</f:facet>
				<div style="width: 70%; white-space: normal;">
					<t:outputText id="p_w" styleClass="A_LEFT"
						style="white-space: normal;" value="#{dataItem.description}" />
				</div>
			</t:column>
			<t:column id="fnDisBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdFnDisBy" value="#{msg['commons.disbursedBy']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabDisBy"
					styleClass="A_LEFT" value="#{dataItem.disbursedBy}" />
			</t:column>
			<t:column id="action" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAmoun" value="#{msg['commons.action']}" />
				</f:facet>

				<h:commandLink action="#{pages$masrafDisbursement.onEdit}"
					rendered="#{
							( 
				                pages$masrafDisbursement.pageMode=='NEW'  || 
				  				pages$masrafDisbursement.pageMode=='PAGE_MODE_RESUBMITTED'||
				  				pages$masrafDisbursement.pageMode=='PAGE_MODE_APPROVAL'  ||
				  				pages$masrafDisbursement.pageMode=='PAGE_MODE_RESUBMITTED'
			  			 	) &&
			  			 	dataItem.isDisbursed=='0' &&
					  		dataItem.status != '165006'
					   	   }">
					<h:graphicImage id="editIcon" title="#{msg['commons.edit']}"
						url="../resources/images/edit-icon.gif" width="18px;" />
				</h:commandLink>
				<h:commandLink id="lnkDelete"
					rendered="#{
							( 
				                pages$masrafDisbursement.pageMode=='NEW'  || 
				  				pages$masrafDisbursement.pageMode=='PAGE_MODE_RESUBMITTED'||
				  				pages$masrafDisbursement.pageMode=='PAGE_MODE_APPROVAL' ||
				  				pages$masrafDisbursement.pageMode=='PAGE_MODE_RESUBMITTED'  
			  			 	)&&
			  			 	 
			  			 	dataItem.isDisbursed=='0' &&
					  		dataItem.status != '165006'
					  		
					   	   }"
					onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableInputs();"
					action="#{pages$masrafDisbursement.onDelete}">
					<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
						url="../resources/images/delete.gif" width="18px;" />
				</h:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>

	<t:div>
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<h:outputText styleClass="PAGE_NUM"
						style="font-size:9;color:black;"
						value="#{msg['mems.normaldisb.label.totalAmount']}:" />
				</td>
				<td>
					<h:outputText styleClass="PAGE_NUM"
						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:9;font-weight:bold;color:black"
						value="#{pages$masrafDisbursement.totalDisbursementsAdded}" />
				</td>
			</tr>
		</table>
	</t:div>
	<pims:security screen="Pims.EndowMgmt.Disburse.Pay" action="view">

		<t:div styleClass="BUTTON_TD"
			rendered="#{ 
	                 pages$masrafDisbursement.pageMode=='PAGE_MODE_DISBURSEMENT_REQUIRED' 
			  	   }"
			style="padding-top:10px; padding-right:21px;">
			<h:commandButton styleClass="BUTTON"
				value="#{msg['commons.disburse']}"
				onclick="performTabClick('lnkDisburse');">
				<h:commandLink id="lnkDisburse"
					action="#{pages$masrafDisbursement.onDisburse}" />
			</h:commandButton>

		</t:div>
	</pims:security>
</t:div>


