
<t:div id="tabDDetails" style="width:100%;">
	<t:panelGrid id="ddTab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="beneficiaries" styleClass="LABEL"
				value="#{msg['mems.inheritanceFile.fieldLabel.beneficiary']}:"></h:outputLabel>
		</h:panelGroup>
		<h:panelGroup>
			<h:selectOneMenu id="selectOneBeneficiary"
				disabled="#{pages$MinorStatusChange.pageMode!='NEW'}"	
				value="#{pages$MinorStatusChange.selectOneBeneficiary}"
				onchange="javascript:onBenefChangeStart(this);">

				<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$MinorStatusChange.beneficiariesList}" />
			</h:selectOneMenu>
			<h:graphicImage id="manageBeneficiaryDetails"
				style="margin-right:5px;" title="#{msg['commons.details']}"
				url="../resources/images/detail-icon.gif"
				onclick="javaScript:openBeneficiaryPopup(#{pages$MinorStatusChange.beneficiary.personId});" />

		</h:panelGroup>

		<h:outputLabel id="lbldateOfBirth" styleClass="LABEL"
			value="#{msg['customer.dateOfBirth']}" />
		<h:inputText id="txtDob" maxlength="20" readonly="true"
			styleClass="READONLY"
			value="#{pages$MinorStatusChange.beneficiary.dateOfBirth}" />

		<h:outputLabel id="lblExpMatDate" styleClass="LABEL"
			value="#{msg['mems.inheritanceFile.fieldLabel.expectedMaturityDate']}" />
		<h:inputText id="txtExpMatDate" maxlength="20" readonly="true"
			styleClass="READONLY"
			value="#{pages$MinorStatusChange.beneficiary.expMaturityDate}" />

	</t:panelGrid>

</t:div>


