<%-- 
  - Author: Kamran Ahmed
  - Date:
  - Copyright Notice:
  - @(#)\
  - Description: This report will list the customer applications matching the following search criteria as input
  --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%-- 
		<% UIViewRoot viewRoot = (UIViewRoot)session.getAttribute("view");
           if (viewRoot.getLocale().toString().equals("en"))
           { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>
        <%
            } 
            else
            { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_ar.js"></script>
        <%
            } 
        %>
--%>
<script type="text/javascript">

	function openLoadReportPopup(pageName) 
	{	
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />


			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
				 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

			

		</head>

		<body>
		<div style="width:1024px;">
		

			<table  cellpadding="0" cellspacing="0" border="0">
				<tr>
						  
							<td colspan="2">
						        <jsp:include page="header.jsp"/>
						     </td>
						     
							
		       </tr>
		       <tr width="100%">						
						<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
						</td>					    
						<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['commons.report.name.applicationSummary.jsp']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									>
								</td>
								<td width="0%" height="99%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:450px;">
										<h:form id="searchFrm" style="#width:98%">
											
											<div class="MARGIN" style="width:96%">
												<table cellpadding="0" cellspacing="0" style="width:100%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
													<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>
												<div class="DETAIL_SECTION">
             		<h:outputLabel value="#{msg['commons.report.criteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>				
							<table cellpadding="0px" cellspacing="0px" class="DETAIL_SECTION_INNER"  >
			
      								<tr>  
      								    <td>
										 
										<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.applicationNumber']} :"></h:outputLabel></td>
										<td  ><h:inputText id="txtApplicationNumber" styleClass="INPUT_TEXT" value="#{pages$crApplicationSummary.applicationSummaryReportCriteria.applicationNumber}">
											</h:inputText>
										 
										</td>
										
										<td>
										 
										<h:outputLabel styleClass="LABEL" value="#{msg['applicationDetails.applicantName']} :"></h:outputLabel></td>
										<td  >
														 
										<h:inputText id="txtApplicantName" styleClass="INPUT_TEXT" value="#{pages$crApplicationSummary.applicationSummaryReportCriteria.applicantName}">
											</h:inputText></td>
      								
      								</tr>	  
												
									<tr>
									   <td>
										 
									  <h:outputLabel styleClass="LABEL" value="#{msg['application.dateFrom']} :"></h:outputLabel></td>
									  <td >
									    
													
									  <rich:calendar id="clndrApplicationDateFrom" 
									  value="#{pages$crApplicationSummary.applicationSummaryReportCriteria.applicationDateFrom}" 
									  popup="true" 
									  datePattern="#{pages$crApplicationSummary.dateFormat}" 
									  showApplyButton="false" 
									  locale="#{pages$crApplicationSummary.locale}" 
									 enableManualInput="false" 
										cellWidth="24px" cellHeight="22px" 
								        style="width:188px; height:16px"
								        inputStyle="width: 186px; height: 18px"
									  >
									  </rich:calendar></td>
									  
									  <td>
										 
									  <h:outputLabel styleClass="LABEL" value="#{msg['application.dateTo']} :"></h:outputLabel></td>
									  <td >
									    
													
									  <rich:calendar id="clndrApplicationDateTo" 
									  value="#{pages$crApplicationSummary.applicationSummaryReportCriteria.applicationDateTo}" 
									  popup="true" 
									  datePattern="#{pages$crApplicationSummary.dateFormat}" 
									  showApplyButton="false" 
									  locale="#{pages$crApplicationSummary.locale}" 
									  enableManualInput="false" 
										cellWidth="24px" cellHeight="22px" 
								        style="width:188px; height:16px"
								        inputStyle="width: 186px; height: 18px"
									  >
									  </rich:calendar></td>
									</tr>		
									
									<tr> 
									<td>
									 
									 <h:outputLabel styleClass="LABEL" value="#{msg['application.type']} :"></h:outputLabel></td>
									 <td  >
									 
									<h:selectOneMenu id="cboApplicationType" value="#{pages$crApplicationSummary.applicationSummaryReportCriteria.applicationType}" styleClass="SELECT_MENU">
																	<f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1" />
																	<f:selectItems value="#{pages$ApplicationBean.requestTypeList}" />
																</h:selectOneMenu></td>
									<td>
										 
									  <h:outputLabel styleClass="LABEL" value="#{msg['application.status']} :"></h:outputLabel></td>
									  <td >
									    
													
																	<h:selectOneMenu id="cboApplicationStatus" 
																	value="#{pages$crApplicationSummary.applicationSummaryReportCriteria.applicationStatus}" 
																	styleClass="SELECT_MENU">
																	<f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1" />
																		<f:selectItems value="#{pages$ApplicationBean.requestStatusList}" />
																	</h:selectOneMenu>
																	</td>	
									</tr>
									<tr>
									    
											<td>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receipt.receiptBy']} :"></h:outputLabel>
																</td>
																<td>
																	<h:selectOneMenu id="cboCreatedBy"
																		styleClass="SELECT_MENU"
 
																		value="#{pages$crApplicationSummary.applicationSummaryReportCriteria.createdBy}">
																		<f:selectItem
																			itemLabel="#{msg['commons.pleaseSelect']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.userNameList}" />
																	</h:selectOneMenu>

																</td>
																
																
										<td></td><td></td>
										
										
												
									</tr>
									
									<tr>
									   
										
										
										
									</tr>
									<tr>
									 	
										
										 
										
										</tr>
									<!-- 	<tr>
										<td></td>
										<td></td>
										<td><h:outputLabel styleClass="LABEL" value="#{msg['tenants.type']} :"></h:outputLabel></td>
										<td><h:selectOneMenu id="cboTenantType" tabindex="12" value="#{pages$crApplicationDetails.applicationDetailsReportCriteria.tenantType}" styleClass="SELECT_MENU">
																	<f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1" />
																	<f:selectItems value="#{pages$ApplicationBean.personKindList}" />
																</h:selectOneMenu></td>
										</tr>-->
										    								
      								
      										<tr>
                                               <td colspan="4">
															<DIV class="A_RIGHT" style="padding-bottom:4px;">
																	<h:commandButton styleClass="BUTTON" type= "submit" 
																	action="#{pages$crApplicationSummary.cmdView_Click}"  
																	immediate="false" value="#{msg['commons.view']}"> 
																	</h:commandButton>
															</DIV>
														</td>
                                                
                                           </tr>
									       
									       
      									 </table>
      									 </div>
									</div>
									</h:form>
								</td>
							</tr>
						</table>
                       </td>
				</tr>
				<tr>
						    <td colspan="2">
						    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
						       		<tr><td class="footer"><h:outputLabel value= "#{msg['commons.footer.message']}" />
						       				</td></tr>
					    		</table>
					        </td>
					    </tr> 
				</table>
				</div>
		</body>
	</html>
</f:view>
