<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for listing Distributed Collections 
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	function resetValues()
    {
      		document.getElementById("searchFrm:txtReqNum").value="";
      		document.getElementById("searchFrm:chequeNo").value="";
      		document.getElementById("searchFrm:assetName").value="";
      		document.getElementById("searchFrm:assetNumber").value="";
      		document.getElementById("searchFrm:toAmount").value="";
      		document.getElementById("searchFrm:amountFrom").value="";
      		document.getElementById("searchFrm:txttrxNumber").value="";
      		document.getElementById("searchFrm:paymentMehod").value="-1";
      		document.getElementById("searchFrm:assetTypes").value="-1";
//      		$('searchFrm:createdOnToId').component.resetSelectedDate();
    }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">

			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['distributeShares.lbl.distributedCollections']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="searchFrm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
														<h:messages></h:messages>
															<h:outputText
																value="#{pages$distributedCollectionsPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">

												<div class="DETAIL_SECTION" style="width: 99.8%;">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['collectionProcedure.TrxNumber']}"></h:outputLabel>
															</td>
															<td width="25%">
																<h:inputText id="txttrxNumber"
																	value="#{pages$distributedCollectionsPopup.criteria.trxNumber}"></h:inputText>
															</td>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.requestNumber']}"></h:outputLabel>
															</td>
															<td width="25%">
																<h:inputText id="txtReqNum"
																	value="#{pages$distributedCollectionsPopup.criteria.requestNumber}"></h:inputText>
															</td>

														</tr>

														<tr>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['PeriodicDisbursements.FromAmount']}"></h:outputLabel>
															</td>
															<td width="25%">
																<h:inputText id="amountFrom"
																	value="#{pages$distributedCollectionsPopup.criteria.amountFrom}"
																	onkeyup="removeNonNumeric(this)"
																	onkeypress="removeNonNumeric(this)"
																	onkeydown="removeNonNumeric(this)"
																	onblur="removeNonNumeric(this)"
																	onchange="removeNonNumeric(this)"></h:inputText>
															</td>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['PeriodicDisbursements.ToAmount']}"></h:outputLabel>
															</td>
															<td width="25%">
																<h:inputText id="toAmount"
																	value="#{pages$distributedCollectionsPopup.criteria.amountTo}"
																	onkeyup="removeNonNumeric(this)"
																	onkeypress="removeNonNumeric(this)"
																	onkeydown="removeNonNumeric(this)"
																	onblur="removeNonNumeric(this)"
																	onchange="removeNonNumeric(this)"></h:inputText>
															</td>

														</tr>
														<tr>
															<td width="25%" colspan="1">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['searchAssets.assetType']}:"></h:outputLabel>
															</td>
															<td width="25%" colspan="1">
																<h:selectOneMenu id="assetTypes"
																	value="#{pages$distributedCollectionsPopup.criteria.assetTypeId}"
																	tabindex="3">
																	<f:selectItem itemLabel="#{msg['commons.All']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.assetTypesList}" />
																</h:selectOneMenu>
															</td>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['inhDetails.reportLabel.assetNumber']}"></h:outputLabel>
															</td>
															<td width="25%">
																<h:inputText id="assetNumber"
																	value="#{pages$distributedCollectionsPopup.criteria.assetNumber}"></h:inputText>
															</td>

														</tr>
														<tr>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['revenueFollowup.lbl.assetName']}"></h:outputLabel>
															</td>
															<td width="25%">
																<h:inputText id="assetName"
																	value="#{pages$distributedCollectionsPopup.criteria.assetName}"></h:inputText>
															</td>

															<td width="25%" colspan="1">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['cancelContract.payment.paymentmethod']}:"></h:outputLabel>
															</td>
															<td width="25%" colspan="1">
																<h:selectOneMenu id="paymentMehod"
																	value="#{pages$distributedCollectionsPopup.criteria.paymentMethodId}"
																	tabindex="3">
																	<f:selectItem itemLabel="#{msg['commons.All']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.paymentMethodsFromPMTable}" />
																</h:selectOneMenu>
															</td>

														</tr>
														<tr>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.replaceCheque.chequeNo']}"></h:outputLabel>
															</td>
															<td width="25%">
																<h:inputText id="chequeNo"
																	value="#{pages$distributedCollectionsPopup.criteria.chequeNumber}"></h:inputText>
															</td>
														</tr>
														<tr>
															<td class="BUTTON_TD" colspan="4">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.search']}"
																	action="#{pages$distributedCollectionsPopup.onSearch}"
																	style="width: 75px" />

																<h:commandButton styleClass="BUTTON" type="button"
																	value="#{msg['commons.clear']}"
																	onclick="javascript:resetValues();" style="width: 75px" />

															</td>

														</tr>

													</table>
												</div>

												<t:div styleClass="contentDiv"
													style="width:100%;#width:100%;margin-top:15px;">

													<t:dataTable id="distributeDataList"
														preserveDataModel="true" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true"
														value="#{pages$distributedCollectionsPopup.dataList}"
														binding="#{pages$distributedCollectionsPopup.dataTable}"
														width="100%">

														<t:column sortable="false" width="25%">
															<f:facet name="header">
																<h:outputText value="#{msg['commons.refNum']}" />
															</f:facet>
															<t:outputText style="white-space: normal;"
																value="#{dataItem.refNum}" />
														</t:column>
														<t:column sortable="false" width="5%">
															<f:facet name="header">
																<h:outputText value="#{msg['commons.reasons']}" />
															</f:facet>
															<t:div
																style="white-space: normal;word-wrap: break-word;width:95%;">

																<t:outputText style="white-space: normal;"
																	value="#{dataItem.reason}" />
															</t:div>
														</t:column>
														<t:column sortable="false" width="6%">
															<f:facet name="header">
																<h:outputText
																	value="#{msg['ReviewAndConfirmDisbursements.amount']}" />
															</f:facet>
															<t:outputText style="white-space: normal;"
																value="#{dataItem.amount}">
																<f:convertNumber maxIntegerDigits="15"
																	maxFractionDigits="2" pattern="##############0.00" />

															</t:outputText>
														</t:column>

														<t:column sortable="false" width="5%">
															<f:facet name="header">
																<h:outputText value="#{msg['fieldsName.collectedOn']}" />
															</f:facet>
															<t:outputText style="white-space: normal;"
																value="#{dataItem.collectedOn}" />
														</t:column>

														<t:column sortable="false" width="5%">
															<f:facet name="header">
																<h:outputText value="#{msg['fieldsName.collectedAs']}" />
															</f:facet>
															<t:outputText style="white-space: normal;"
																value="#{pages$distributedCollectionsPopup.englishLocale?dataItem.collectedAsEn:dataItem.collectedAsAr}" />
														</t:column>

														<t:column sortable="false" width="15%">
															<f:facet name="header">
																<h:outputText value="#{msg['assetSearch.assetName']}" />
															</f:facet>
															<t:outputText style="white-space: normal;"
																value="#{pages$distributedCollectionsPopup.englishLocale?dataItem.assetNameEn:dataItem.assetNameAr}" />
														</t:column>

														<t:column sortable="false" width="15%">
															<f:facet name="header">
																<h:outputText value="#{msg['searchAssets.assetType']}" />
															</f:facet>
															<t:outputText style="white-space: normal;"
																value="#{pages$distributedCollectionsPopup.englishLocale?dataItem.assetTypeEn:dataItem.assetTypeAr}" />
														</t:column>

														<t:column id="actioCol" sortable="false">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['mems.inheritanceFile.columnLabel.action']}" />
															</f:facet>
															<t:commandLink
																action="#{pages$distributedCollectionsPopup.onOpenDistributePopUp}">
																<h:graphicImage style="cursor:hand;"
																	title="#{msg['commons.distributeAmongBeneficiaries']}"
																	url="../resources/images/app_icons/Add-New-Tenant.png" />

															</t:commandLink>
														</t:column>
													</t:dataTable>
												</t:div>

												<t:div id="contentDivFooter"
													styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:100%;#width:100%;">
													<table id="RECORD_NUM_TD" cellpadding="0" cellspacing="0"
														width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$distributedCollectionsPopup.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td id="contentDivFooterColumnTwo" class="BUTTON_TD"
																style="width: 53%; # width: 50%;" align="right">

																<t:div styleClass="PAGE_NUM_BG" style="#width:20%">

																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																			</td>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{pages$distributedCollectionsPopup.currentPage}" />
																			</td>
																		</tr>
																	</table>
																</t:div>
																<TABLE border="0" class="SCH_SCROLLER">
																	<tr>
																		<td>
																			<t:commandLink
																				action="#{pages$distributedCollectionsPopup.pageFirst}"
																				disabled="#{pages$distributedCollectionsPopup.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$distributedCollectionsPopup.pagePrevious}"
																				disabled="#{pages$distributedCollectionsPopup.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFR"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:dataList
																				value="#{pages$distributedCollectionsPopup.pages}"
																				var="page">
																				<h:commandLink value="#{page}"
																					actionListener="#{pages$distributedCollectionsPopup.page}"
																					rendered="#{page != pages$distributedCollectionsPopup.currentPage}" />
																				<h:outputText value="<b>#{page}</b>" escape="false"
																					rendered="#{page == pages$distributedCollectionsPopup.currentPage}" />
																			</t:dataList>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$distributedCollectionsPopup.pageNext}"
																				disabled="#{pages$distributedCollectionsPopup.firstRow + pages$distributedCollectionsPopup.rowsPerPage >= pages$distributedCollectionsPopup.totalRows}">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$distributedCollectionsPopup.pageLast}"
																				disabled="#{pages$distributedCollectionsPopup.firstRow + pages$distributedCollectionsPopup.rowsPerPage >= pages$distributedCollectionsPopup.totalRows}">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblL"></t:graphicImage>
																			</t:commandLink>
																		</td>
																	</tr>
																</TABLE>
															</td>
														</tr>
													</table>
												</t:div>


											</div>

										</div>
									</h:form>

								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>


		</body>
	</html>
</f:view>