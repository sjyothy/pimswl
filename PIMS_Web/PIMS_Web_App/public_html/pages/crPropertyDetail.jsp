<%-- 
  - Author: Kamran Ahmed
  - Date:
  - Copyright Notice:
  - @(#)\
  - Description: This report will show the details about the property 
  	matching the following criteria provided as an input.
  --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%-- 
		<% UIViewRoot viewRoot = (UIViewRoot)session.getAttribute("view");
           if (viewRoot.getLocale().toString().equals("en"))
           { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>
        <%
            } 
            else
            { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_ar.js"></script>
        <%
            } 
        %>
--%>

<script type="text/javascript">
	function submitForm()
	{
        document.getElementById('searchFrm').submit();
    }
	function openLoadReportPopup(pageName) 
	{	
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />


			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
				 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

			

		</head>

		<body>
				<div style="width:1024px;">
			
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
						   
							<td colspan="2">
						        <jsp:include page="header.jsp"/>
						     </td>
						    
							
		       </tr>
		       <tr width="100%">						
						<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
						</td>					    
						<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['report.propertyDetailReport']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="99%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:450px;">
										<h:form id="searchFrm" >
											
											<div class="MARGIN" style="width:96%">
												<table cellpadding="0" cellspacing="0" style="width:100%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
													<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>
												<div class="DETAIL_SECTION"  style="width:99.6%">
													<h:outputLabel value="#{msg['commons.report.criteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="0px" cellspacing="0px"
														class="DETAIL_SECTION_INNER" border="0">
														<tr>
															<td width="97%">
															<table width="100%">
																	<tr>
																		<td  width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['contract.property.Name']} :"></h:outputLabel>
																		</td>
																		<td  width="25%">
																			<h:inputText id="txtPropertyName"
																				value="#{pages$crPropertyDetail.propertyDetailReportCriteria.txtPropertyName}"
																				 maxlength="250">
																				</h:inputText>
																			</td>	
																		<td  width="25%">
																		<h:outputLabel styleClass="LABEL" value="#{msg['unit.unitNumber']} :"></h:outputLabel>
																		</td>
																		<td  width="25%">
																			<h:inputText id="unitNumber" value="#{pages$crPropertyDetail.propertyDetailReportCriteria.txtUnitNumber}"  maxlength="250"></h:inputText>
																		</td>
																	</tr>
																	<tr>

																		<td >
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['inspection.propertyRefNum']} :"></h:outputLabel>
																		</td>

																		<td >


																			<h:inputText id="propertyNumberInput"
																				value="#{pages$crPropertyDetail.propertyDetailReportCriteria.txtPropertyNumber}"
																				 maxlength="20"></h:inputText>

																		</td>
																		

																		<td >


																		<h:outputLabel styleClass="LABEL" value="#{msg['unit.unitType']} :"></h:outputLabel></td>
																	<td >


																		<h:selectOneMenu  id="unitType" required="false" value="#{pages$crPropertyDetail.propertyDetailReportCriteria.cboUnittype}">

																				<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																				<f:selectItems value="#{pages$ApplicationBean.unitTypeList}" />
																			</h:selectOneMenu></td>
																	</tr>
																	<tr>
																		<td >
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['property.type']} :"></h:outputLabel>
																		</td>
																		<td >


																			<h:selectOneMenu id="cbopropertyType"
																				 required="false"
																				value="#{pages$crPropertyDetail.propertyDetailReportCriteria.cboPropertyType}">

																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.All']}" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.propertyTypeList}" />
																			</h:selectOneMenu>
																		</td>

																		<td >


																		<h:outputLabel styleClass="LABEL" value="#{msg['unit.floorNumber']} :"></h:outputLabel></td>
																		<td >


																		<h:inputText id="txtFloorNumber" value="#{pages$crPropertyDetail.propertyDetailReportCriteria.txtFloorNumber}"  maxlength="20"></h:inputText></td>

																	</tr>

																	<tr>

																		<td >
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['property.usage']} :" />

																		</td>
																		<td >
																			<h:selectOneMenu id="cboPropertyUsage"
																				 required="false"
																				value="#{pages$crPropertyDetail.propertyDetailReportCriteria.cboPropertyUsage}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.All']}" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.propertyUsageList}" />
																			</h:selectOneMenu>

																		</td>

																	<td >
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.unitUsage']}:"></h:outputLabel>
																</td>
																<td >
																	<h:selectOneMenu id="selectUnitUsage"
																		value="#{pages$crPropertyDetail.propertyDetailReportCriteria.unitUsage}"
																		tabindex="2">
																		<f:selectItem itemLabel="#{msg['commons.All']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.unitUsageTypeList}" />
																	</h:selectOneMenu>
																</td>

																	</tr>


																	<tr>

																		<td >

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['contact.state']} :" />
																		</td>
																		<td >
																			<h:selectOneMenu id="cboEmirate"
																				 required="false"
 																				 valueChangeListener="#{pages$crPropertyDetail.loadCity}"
																				 onchange="submitForm();"
																				 immediate="false"
																				 value="#{pages$crPropertyDetail.propertyDetailReportCriteria.cboEmirate}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.All']}" />
																				<f:selectItems value="#{view.attributes['state']}" />
																			</h:selectOneMenu>
																		</td>

																		<td >

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.report.community']} :" />
																		</td>
																		<td >
																			<h:selectOneMenu id="cboCommunity"
																				 required="false"
																				value="#{pages$crPropertyDetail.propertyDetailReportCriteria.cboCommunity}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.All']}" />
																				<f:selectItems value="#{view.attributes['city']}" />
																			</h:selectOneMenu>
																		</td>

																	</tr>
																	<tr>

																		
																		<td >


																		<h:outputLabel styleClass="LABEL" value="#{msg['property.landNumber']} :"></h:outputLabel></td>
																		<td >

																		<h:inputText id="txtLandNmuber" value="#{pages$crPropertyDetail.propertyDetailReportCriteria.txtLandNumber}" ></h:inputText></td>



																		<td >


																		<h:outputLabel styleClass="LABEL" value="#{msg['commons.report.costCenter']} :"></h:outputLabel></td>
																		<td >

																		<h:inputText id="txtCostCenter" value="#{pages$crPropertyDetail.propertyDetailReportCriteria.txtCostCenter}" ></h:inputText></td>

																	</tr>
																	<tr>
																		<td ><h:outputLabel styleClass="LABEL" value="From Date"> :</h:outputLabel>


																			
																		</td>
																		<td >
																			

																		<rich:calendar id="fromDate" value="#{pages$crPropertyDetail.propertyDetailReportCriteria.clndrFromDate}" popup="true" datePattern="#{pages$crPropertyDetail.dateFormat}" showApplyButton="false" locale="#{pages$crPropertyDetail.locale}" enableManualInput="false" cellWidth="24px" cellHeight="22px"  inputStyle="width: 170px; height: 14px">
																		</rich:calendar></td>

																		<td >

																		<h:outputLabel styleClass="LABEL" value="To Date"> :</h:outputLabel></td>
																		<td >

																		<rich:calendar id="toDate" datePattern="#{pages$crPropertyDetail.dateFormat}" 
																		showApplyButton="false" locale="#{pages$crPropertyDetail.locale}"
																		 value="#{pages$crPropertyDetail.propertyDetailReportCriteria.cldrToDate}"
																		 
								                        			     inputStyle="width: 170px; height: 14px"
								                        			     enableManualInput="false"
																		 >
																		 </rich:calendar></td>

																	</tr>
																	<tr>
																		<td colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.ownershipType']} :"></h:outputLabel>
																		</td>
			
																		<td colspan="1">
																			<h:selectOneMenu id="cboOwnerShipType"
																				 required="false"
																				value="#{pages$crPropertyDetail.propertyDetailReportCriteria.cboOwnerShipType}">
			
																				<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.propertyOwnershipType}" />
																			</h:selectOneMenu>
			
																		</td>
																	</tr>


																	<tr>
																		<td class="BUTTON_TD" colspan="4">
																			<h:commandButton styleClass="BUTTON"
																				id="submitButton" 
																				action="#{pages$crPropertyDetail.cmdView_Click}"
																				immediate="false" value="#{msg['commons.view']}"
																				 tabindex="7"></h:commandButton>
																		</td>
																	</tr>

																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>
														

													</table>
										
												</div>
											</div>
											
											
											
										</h:form>
									</div>
								</td>
							</tr>
						</table>
                       </td>
				</tr>
				</table>
				</div>
		</body>
	</html>
</f:view>
