<%-- 
  - Author: Ahmed Faraz
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Notification Sending and searching
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

 		function showDetailPopup()
        {
              var screen_width = screen.width;
              var screen_height = screen.height;
              var popup_width = screen_width-590;
              var popup_height = screen_height-450;
              var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
              
              var popup = window.open('NotificationDetails.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,resizable=no,titlebar=no,dialog=yes');
              popup.focus();
        }
        
	    function resetValues()
      	{
      		document.getElementById("searchFrm:auctionNumber").value="";
			document.getElementById("searchFrm:auctionTitle").value="";
      	    $('searchFrm:auctionDate').component.resetSelectedDate();
      	    $('searchFrm:auctionCreationDate').component.resetSelectedDate();
      	    document.getElementById("searchFrm:auctionStatuses").selectedIndex=0;
      	    document.getElementById("searchFrm:auctionStartTimeMM").selectedIndex=0;
      	    document.getElementById("searchFrm:auctionStartTimeHH").selectedIndex=0;
      	    document.getElementById("searchFrm:auctionEndTimeMM").selectedIndex=0;
      	    document.getElementById("searchFrm:auctionEndTimeHH").selectedIndex=0;
			document.getElementById("searchFrm:auctionStartTime").value="";
			document.getElementById("searchFrm:auctionEndTime").value="";
			document.getElementById("searchFrm:venueName").value="";
        }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<div  class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>
				<tr >
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['notification.header']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td width="100%"    nowrap="nowrap">						

									<div class="SCROLLABLE_SECTION">

										<h:form id="searchFrm" style="WIDTH: 97.6%;">
											<div style="height: 25px">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$NotificationSend.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
													<tr>
													<td width = "97%">
													<table width = "100%">
														<tr>

															<td width = "25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notification.proceduretype']}:"></h:outputLabel>
															</td>

															<td width = "25%">
																<h:inputText id="procedureTypeTxt"
																	value="#{pages$NotificationSend.srchProcedureType}"
																	 maxlength="20"></h:inputText>



															</td>
															<td width = "25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notification.Subject']}:"></h:outputLabel>
															</td>
															<td width = "25%">
																<h:inputText id="sujectTxt"
																	value="#{pages$NotificationSend.srchSubject}"
																	 maxlength="100"></h:inputText>

															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notification.type']}:"></h:outputLabel>
															</td>
															<td >
																<h:selectOneMenu id="notificationTypeCmb"
																	value="#{pages$NotificationSend.srchChannelType}"
																	required="false">
																	<f:selectItem itemValue="0"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItem itemValue="SMTP" itemLabel="SMTP" />
																	<f:selectItem itemValue="SMS" itemLabel="SMS" />

																</h:selectOneMenu>

															</td>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notification.personname']}:" />
															</td>
															<td >
																<h:inputText id="personNameTxt"
																	value="#{pages$NotificationSend.srchPersonName}"
																	maxlength="100"></h:inputText>

															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notification.requestnumber']}:"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="requestNumberTxt"
																	value="#{pages$NotificationSend.srchRequestNumber}"
																	maxlength="100" />

															</td>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notification.senddate']}:" />
															</td>
															<td>
																<rich:calendar id="sendDateCal"
																	value="#{pages$NotificationSend.srchSendDate}"
																	locale="#{pages$NotificationSend.locale}" popup="true"
																	datePattern="#{pages$NotificationSend.dateFormat}"
																	showApplyButton="false" enableManualInput="false"
																	inputStyle="width:170px; height:14px" />

															</td>

														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.status']}:"></h:outputLabel>
															</td>
															<td s>
																<h:selectOneMenu id="notificationStatusCmb"
																	value="#{pages$NotificationSend.srchStatus}"
																	 required="false">
																	<f:selectItem itemValue="0"
																		itemLabel="#{msg['commons.All']}" />
																	<f:selectItem itemValue="None" itemLabel="None" />
																	<f:selectItem itemValue="New" itemLabel="New" />
																	<f:selectItem itemValue="Sent" itemLabel="Sent" />
																	<f:selectItem itemValue="Failed" itemLabel="Failed" />
																	<f:selectItem itemValue="None" itemLabel="None" />
																	<f:selectItem itemValue="Expired" itemLabel="Expired" />
																	<f:selectItem itemValue="Wait" itemLabel="Wait" />
																</h:selectOneMenu>
															</td>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notification.createdate']}:"></h:outputLabel>
															</td>
															<td >
																<rich:calendar id="createDateCal"
																	value="#{pages$NotificationSend.srchNotificationDate}"
																	locale="#{pages$NotificationSend.locale}" popup="true"
																	datePattern="#{pages$NotificationSend.dateFormat}"
																	showApplyButton="false" enableManualInput="false"
																	inputStyle="width:170px; height:14px" />

															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notification.contractnumber']}:"></h:outputLabel>
															</td>
															<td >
																<h:inputText id="contractNumberTxt"
																	value="#{pages$NotificationSend.srchContractNumber}"
																	maxlength="50"></h:inputText>
															</td>
															<td >

															</td>
															<td >
																&nbsp;

															</td>
														</tr>
														<tr>
															<td class="BUTTON_TD" colspan="4">
																			<pims:security
																				screen="Pims.Home.NotificationSetup"
																				action="create">
																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.search']}"
																					action="#{pages$NotificationSend.searchNotificationsTest}"
																					style="width: 75px" tabindex="7">
																				</h:commandButton>
																			</pims:security>

																			<h:commandButton type="reset" styleClass="BUTTON"
																				value="#{msg['commons.clear']}" style="width: 75px"
																				tabindex="7"></h:commandButton>
																			<pims:security
																				screen="Pims.Home.NotificationSetup"
																				action="create">
																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.send']}"
																					action="#{pages$NotificationSend.sendSelectedNotification}"
																					style="width: 75px" tabindex="7">
																				</h:commandButton>
																			</pims:security>
																			<pims:security
																				screen="Pims.Home.NotificationSetup"
																				action="create">
																				<h:commandButton styleClass="BUTTON" 
																					value="#{msg['notification.add']}"
																					action="#{pages$NotificationSend.gotoGenerateNotification}"
																					style="width: 110px" tabindex="7">
																				</h:commandButton>
																			</pims:security>
															</td>
														</tr>
													</table>
														</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 2px; padding-top: 7px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv">
													<t:dataTable id="dt1"
														value="#{pages$NotificationSend.dataList}"
														binding="#{pages$NotificationSend.dataTable}"
														rows="#{pages$NotificationSend.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%">
														<t:column id="selectCol" width="45">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.select']}" />
															</f:facet>

															<h:selectBooleanCheckbox id="selectchk"
																value="#{dataItem.selected}" />
														</t:column>
														<t:column id="procedureTypeCol" width="90" sortable="true">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['notification.proceduretype']}" />
															</f:facet>
															<t:outputText value="#{dataItem.procedureType}"
																styleClass="A_LEFT" />
														</t:column>
														<t:column id="subjectCol" width="105" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['notification.Subject']}" />
															</f:facet>
															<t:outputText value="#{dataItem.subject}"
																styleClass="A_LEFT" />
														</t:column>
														<t:column id="typeCol" width="55" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['notification.type']}" />
															</f:facet>
															<t:outputText value="#{dataItem.channelId}" />
														</t:column>
														<t:column id="personnameCol" width="110" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['notification.personname']}" />
															</f:facet>
															<t:outputText value="#{dataItem.personName}" />
														</t:column>
														<t:column id="notidicationDateCol" width="100"
															sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['notification.createdate']}" />
															</f:facet>
															<t:outputText value="#{dataItem.createdOn}" />
														</t:column>
														<t:column id="sendDateCol" width="85" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['notification.senddate']}" />
															</f:facet>
															<t:outputText value="#{dataItem.sentTime}"
																styleClass="A_LEFT" />
														</t:column>
														<t:column id="statusEnCol" width="65"
															rendered="#{pages$NotificationSend.isEnglishLocale}"
															sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.status']}" />
															</f:facet>
															<t:outputText value="#{dataItem.statusText}"
																styleClass="A_LEFT" />
														</t:column>
														<t:column id="statusArCol" width="65"
															rendered="#{pages$NotificationSend.isArabicLocale}"
															sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.status']}" />
															</f:facet>
															<t:outputText value="#{dataItem.statusText}"
																styleClass="A_LEFT" />
														</t:column>
														<t:column id="actionCol" sortable="false" width="40"
															style="TEXT-ALIGN: center;">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>
															<pims:security
																screen="Pims.Home.NotificationSetup"
																action="create">
																<t:commandLink  action="#{pages$NotificationSend.openDetailsPopupForView}"
																	 >&nbsp;
															<h:graphicImage id="viewIcon"
																		title="#{msg['commons.view']}"
																		url="../resources/images/app_icons/View-Auction.png" />&nbsp;
														</t:commandLink>
															</pims:security>
															<t:outputLabel value="  "></t:outputLabel>
															<pims:security
																screen="Pims.Home.NotificationSetup"
																action="update">
																<t:commandLink
																	action="#{pages$NotificationSend.openDetailsPopupForEdit}">
																	<h:graphicImage id="editIcon"
																		title="#{msg['commons.edit']}"
																		url="../resources/images/app_icons/Edit-Auction.png" />&nbsp;
														</t:commandLink>
															</pims:security>
															<t:outputLabel id="aa333asa" value="  "></t:outputLabel>


														</t:column>
													</t:dataTable>
												</div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
													>
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$NotificationSend.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">
																<CENTER>
																	<t:dataScroller id="scroller" for="dt1"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$NotificationSend.paginatorMaxPages}"
																		immediate="false" paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false"
																		pageIndexVar="pageNumber" styleClass="SCH_SCROLLER">
																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL"></t:graphicImage>
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
																			<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.pageNumber}" />
																					</td>
																				</tr>
																			</table>

																		</t:div>
																	</t:dataScroller>
																</CENTER>

															</td>
														</tr>
													</table>
												</t:div>
										</h:form>
									</div>







								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
	</html>
</f:view>