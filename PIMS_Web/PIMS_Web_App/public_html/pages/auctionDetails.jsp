<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript" src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript"> 
        function openUnitsPopup(unitId)
		{
		      var screen_width = 1024;
		      var screen_height = 450;
		      var popup_width = screen_width-250;
		      var popup_height = screen_height;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('editUnitPopup.jsf?UNIT_ID='+unitId+'&readOnlyMode=true','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}



 		function showUnitPopup()
        {
              var screen_width = 1024;
              var screen_height = 480;
              var popup_width = screen_width-200;
              var popup_height = screen_height;
              var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
              
              var popup = window.open('UnitSearch.jsf?pageMode=MODE_SELECT_MANY_POPUP&context=AuctionUnits','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
              popup.focus();
        }
        
        function showPriceEditPopup()
        {
              var screen_width = screen.width;
              var screen_height = screen.height;
              var popup_width = 850;
              var popup_height = screen_height-400;
              var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
              window.open('auctionUnitDepositEdit.jsf?PageMode=OpeningPrice','_blank','width='+ popup_width +',height=' +popup_height+ ',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');              
        }        
        function showDepositEditPopup()
        {
              var screen_width = screen.width;
              var screen_height = screen.height;
              var popup_width = 850;
              var popup_height = screen_height-400;
              var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
              window.open('auctionUnitDepositEdit.jsf?PageMode=DepositAmount','_blank','width='+ popup_width +',height=' +popup_height+ ',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
        }
        
        
		function showVenuePopup()
        {
              var screen_width = screen.width;
              var screen_height = screen.height;
              var popup_width = screen_width-220;
              var popup_height = 450;
              var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
              window.open('auctionVenue.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
        }
        
        function showAttachmentPopUp()
        { 
           var screen_width = screen.width;
           var screen_height = screen.height;
           window.open('Attachment.jsp','_blank','width='+(screen_width-500)+',height='+(screen_height-400)+',left=300,top=250,scrollbars=no,status=no');
       }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 <script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>


	
</head>
	<body class="BODY_STYLE">
	<div class="containerDiv">
	
	
    <!-- Header --> 
	<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
		<tr style="height:84px;width:100%;#height:84px;#width:100%;">
			<td colspan="2">
				<jsp:include page="header.jsp" />
			</td>
		</tr>
		<tr width="100%">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99.2%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{pages$auctionDetails.heading}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>
				<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%">
					<tr valign="top">
								<td width="100%" valign="top" nowrap="nowrap">
						<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
							<div class="SCROLLABLE_SECTION" style="height:470px;width:100%;#height:470px;#width:100%;">
									<h:form id="detailsFrm" enctype="multipart/form-data" style="WIDTH: 97.6%;">
									
								<div> 
									<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
											<tr>
												<td>
													<h:outputText value="#{pages$auctionDetails.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$auctionDetails.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
									<div class="AUC_DET_PAD">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
						<div class="TAB_PANEL_INNER"> 
							<rich:tabPanel binding="#{pages$auctionDetails.tabPanel}" id="auctionTabPanel" switchType="server" style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%" >
								<rich:tab id="auctionDetailsTab" label="#{msg['auction.details']}" >
									<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
										<t:panelGroup>
											<t:panelGrid columns="2">
											  <t:panelGroup>
										      	<h:outputLabel styleClass="mandatory" value="#{pages$auctionDetails.asterisk}"></h:outputLabel>
												<h:outputLabel styleClass="LABEL" value="#{msg['auction.number']}: " ></h:outputLabel>					      
										      </t:panelGroup>
											  <h:inputText id="auctionNumber" styleClass="READONLY INPUT_TEXT" readonly="true" binding="#{pages$auctionDetails.auctionNumberField}" value="#{pages$auctionDetails.auctionNumber}" tabindex="1"   maxlength="20"></h:inputText>
										      <t:panelGroup>
										      	<h:outputLabel styleClass="mandatory" value="#{pages$auctionDetails.asterisk}"></h:outputLabel>
												<h:outputLabel styleClass="LABEL" value="#{msg['auction.title']}: " ></h:outputLabel>					      
										      </t:panelGroup>
											  <h:inputText id="auctionTitle" styleClass="#{pages$auctionDetails.readonlyStyleClass} INPUT_TEXT" binding="#{pages$auctionDetails.auctionDescriptionField}" value="#{pages$auctionDetails.auctionDescription}"  maxlength="100" tabindex="3"></h:inputText>
											  <t:panelGroup>
										      	<h:outputLabel styleClass="mandatory" value="#{pages$auctionDetails.asterisk}"></h:outputLabel>
												<h:outputLabel styleClass="LABEL" value="#{msg['auction.date']}: " ></h:outputLabel>					      
										      </t:panelGroup>
											  <rich:calendar id="auctionDate" styleClass="#{pages$auctionDetails.readonlyStyleClass}" value="#{pages$auctionDetails.auctionDate}" binding="#{pages$auctionDetails.auctionDateField}" locale="#{pages$auctionDetails.locale}" popup="true" datePattern="#{pages$auctionDetails.dateFormat}" showApplyButton="false" enableManualInput="false" inputStyle="width:170px; height:14px" /> 
										      <t:panelGroup>
										      	<h:outputLabel styleClass="mandatory" value="#{pages$auctionDetails.asterisk}"></h:outputLabel>
												<h:outputLabel styleClass="LABEL" value="#{msg['auction.outbiddingValue']}: " ></h:outputLabel>					      
										      </t:panelGroup>
											  <h:inputText id="outbiddingValue" styleClass="A_RIGHT_NUM #{pages$auctionDetails.numericReadonlyStyleClass} INPUT_TEXT" binding="#{pages$auctionDetails.outbiddingValueField}" value="#{pages$auctionDetails.outBiddingValue}"  maxlength="100" tabindex="8">
											  </h:inputText>
											  <t:panelGroup>
										      	<h:outputLabel styleClass="mandatory" value="#{pages$auctionDetails.asterisk}"></h:outputLabel>
												<h:outputLabel styleClass="LABEL" value="#{msg['advertisement.publicationDate']}: " ></h:outputLabel>					      
										      </t:panelGroup>
											  <t:panelGroup>
											  	<rich:calendar id="advertisementDate" styleClass="#{pages$auctionDetails.readonlyStyleClass}" value="#{pages$auctionDetails.advertisementDate}" binding="#{pages$auctionDetails.publicationDateField}"
										                   	  locale="#{pages$auctionDetails.locale}" popup="true" datePattern="#{pages$auctionDetails.dateFormat}" showApplyButton="false" enableManualInput="false" inputStyle="width:170px; height:14px" />  
											  </t:panelGroup>
											  <t:panelGroup>
										      	<h:outputLabel styleClass="mandatory" value="#{pages$auctionDetails.asterisk}"></h:outputLabel>
												<h:outputLabel styleClass="LABEL" value="#{msg['auction.requestStartDate']}: " ></h:outputLabel>					      
										      </t:panelGroup>
								  			  <rich:calendar id="reqStartDate" styleClass="#{pages$auctionDetails.readonlyStyleClass}" value="#{pages$auctionDetails.requestStartDate}" binding="#{pages$auctionDetails.registrationStartDateField}" locale="#{pages$auctionDetails.locale}" popup="true" datePattern="#{pages$auctionDetails.dateFormat}" showApplyButton="false" enableManualInput="false" inputStyle="width:170px; height:14px" />
											</t:panelGrid>
										</t:panelGroup>
										<t:panelGroup>
											<t:panelGrid columns="2">
											  <t:div style="height:25px;">
											  </t:div>
											  <t:div style="height:25px;">
											  </t:div>
											  <h:outputLabel styleClass="LABEL" value="#{msg['auction.status']}:"></h:outputLabel>			
											  <h:inputText id="auctionStatus" styleClass="READONLY INPUT_TEXT" readonly="true" value="#{pages$auctionDetails.auctionStatus}" tabindex="2" ></h:inputText>
											  <t:panelGroup>
										      	<h:outputLabel styleClass="mandatory" value="#{pages$auctionDetails.asterisk}"></h:outputLabel>
												<h:outputLabel styleClass="LABEL" value="#{msg['auction.venue']}: " ></h:outputLabel>					      
										      </t:panelGroup>
											  <t:panelGroup>
											  <h:inputText id="venueName" readonly="true" styleClass="#{pages$auctionDetails.readonlyStyleClass} INPUT_TEXT" value="#{pages$auctionDetails.auctionVenue}"  maxlength="50" tabindex="4" 
											  					style="width:163px;" />
												<pims:security screen="Pims.AuctionManagement.PrepareAuction.SelectAuctionVenue" action="create">
													<h:commandButton actionListener="#{pages$auctionDetails.selectVenue}" binding="#{pages$auctionDetails.selectVenueButton}"
															style="width: 25px; height:20px; border-width:0px;" tabindex="8" image="../resources/images/magnifier.gif" />
												</pims:security>	
											</t:panelGroup>
											<t:panelGroup>
										      	<h:outputLabel styleClass="mandatory" value="#{pages$auctionDetails.asterisk}"></h:outputLabel>
												<h:outputLabel styleClass="LABEL" value="#{msg['auction.time']}: " ></h:outputLabel>					      
										    </t:panelGroup>
											 <t:panelGroup>
											  	<h:selectOneMenu id="auctionStartTimeHH" style="width: 40px; " required="false" binding="#{pages$auctionDetails.hhStartField}" styleClass="#{pages$auctionDetails.readonlyStyleClass}" 
														value="#{pages$auctionDetails.hhStart}" tabindex="6">
														<f:selectItem itemValue="" itemLabel="hh" />
														<f:selectItems value="#{pages$ApplicationBean.hours}" />
												</h:selectOneMenu>
												<h:selectOneMenu id="auctionStartTimeMM" style="width: 40px; " required="false" styleClass="#{pages$auctionDetails.readonlyStyleClass}" 
														value="#{pages$auctionDetails.mmStart}" binding="#{pages$auctionDetails.mmStartField}" tabindex="7">
														<f:selectItem itemValue="" itemLabel="mm" />
														<f:selectItems value="#{pages$ApplicationBean.minutes}" />
												</h:selectOneMenu>
											</t:panelGroup>	
										      <t:panelGroup>
										      	<h:outputLabel styleClass="mandatory" value="#{pages$auctionDetails.asterisk}"></h:outputLabel>
												<h:outputLabel styleClass="LABEL" value="#{msg['auction.confiscationPercentage']}: " ></h:outputLabel>					      
										      </t:panelGroup>
										  	  <h:inputText id="confiscationPercentage" styleClass="#{pages$auctionDetails.numericReadonlyStyleClass} INPUT_TEXT" binding="#{pages$auctionDetails.confiscationPercentField}" value="#{pages$auctionDetails.confiscationPercent}"  maxlength="100" tabindex="9">
										  	  </h:inputText>
										  	<t:panelGroup>
										      	<h:outputLabel styleClass="mandatory" value="#{pages$auctionDetails.asterisk}"></h:outputLabel>
												<h:outputLabel styleClass="LABEL" value="#{msg['auction.requestEndDate']}: " ></h:outputLabel>					      
										    </t:panelGroup>  
									  	 	<rich:calendar id="reqEndDate" styleClass="#{pages$auctionDetails.readonlyStyleClass}" value="#{pages$auctionDetails.requestEndDate}" binding="#{pages$auctionDetails.registrationEndDateField}" locale="#{pages$auctionDetails.locale}" popup="true" datePattern="#{pages$auctionDetails.dateFormat}" showApplyButton="false" enableManualInput="false" inputStyle="width:170px; height:14px" />
											</t:panelGrid>
										</t:panelGroup>
									</t:panelGrid>
								</rich:tab>
								<rich:tab id="auctionUnitsTab" label="#{msg['auctionUnit.tabHeading']}" >
									 <%-- 	<t:div styleClass="imag">&nbsp;</t:div> --%> 
										<t:div style="width:100%;">
										<pims:security screen="Pims.AuctionManagement.PrepareAuction.AddAuctionUnits" action="create">
											<t:div styleClass="BUTTON_TD" style="padding-bottom:5px;padding-right:8px;">
												<h:commandButton styleClass="BUTTON" value="#{msg['auctionUnit.addAuctionUnits']}" actionListener="#{pages$auctionDetails.openUnitPopup}" binding="#{pages$auctionDetails.addAuctionUnitsButton}" style="width: 115px" tabindex="14"/>
											</t:div>
										</pims:security>
										<t:div styleClass="contentDiv" style="width:97.75%; height:220px; border-width:4px;">
																<t:dataTable id="dt1"
																	value="#{pages$auctionDetails.auctionUnitDataList}"
																	binding="#{pages$auctionDetails.dataTable}"
																	rows="#{pages$auctionDetails.paginatorRows}"
																	preserveDataModel="false" preserveSort="false" var="dataItem"
																	rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
																	<t:column id="selectedCol" width="50" sortable="false" binding="#{pages$auctionDetails.selectColumn}">
																		<f:facet name="header">
																			<t:outputText value=" " />
																		</f:facet>
																		<h:selectBooleanCheckbox value="#{dataItem.selected}" valueChangeListener="#{pages$auctionDetails.selectAuctionUnit}"></h:selectBooleanCheckbox>
																	</t:column>
																	<t:column id="propertyNumberCol" width="100" sortable="true" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['property.propertyNumberCol']}" style="white-space: normal;"/>
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.propertyNumber}" style="white-space: normal;"/>
																	</t:column>
																	<t:column id="propertyCommercialNameCol" width="100" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['property.commercialName']}" style="white-space: normal;"/>
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.propertyCommercialName}" style="white-space: normal;"/>
																	</t:column>
																	<t:column rendered="false" id="propertyEndowedNameCol" width="120" sortable="true" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['property.endowedName']}" style="white-space: normal;"/>
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.propertyEndowedName}" style="white-space: normal;"/>
																	</t:column>
																	<t:column id="landNumberCol" width="80" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['property.landNumber']}" style="white-space: normal;"/>
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.landNumber}" style="white-space: normal;"/>
																	</t:column>
																	<t:column id="emirateEnCol" width="100" sortable="true" rendered="#{pages$auctionDetails.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.Emirate']}" style="white-space: normal;"/>
																		</f:facet>
																		<t:outputText value="#{dataItem.emirateEn}" styleClass="A_LEFT" style="white-space: normal;"/>
																	</t:column>
																	<t:column id="emirateArCol" width="100" sortable="true" rendered="#{pages$auctionDetails.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.Emirate']}" style="white-space: normal;"/>
																		</f:facet>
																		<t:outputText value="#{dataItem.emirateAr}" styleClass="A_LEFT" style="white-space: normal;"/>
																	</t:column>
																	<t:column id="unitNumberCol" width="60" sortable="true" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['unit.numberCol']}" style="white-space: normal;"/>
																		</f:facet>
																		<h:commandLink onclick="javascript:openUnitsPopup('#{dataItem.unitId}');" >
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.unitNumber}" style="white-space: normal;"/>
																	    </h:commandLink>
																	</t:column>
																	<t:column id="unitAccNumberCol" width="60" sortable="true" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['auction.unitTab.columnUnitAccountNumber']}" style="white-space: normal;"/>
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.unitAccountNumber}" style="white-space: normal;"/>
																	</t:column>
																	<t:column id="colUnitDesc" sortable="true" width="18%">
																	<f:facet name="header">
																		<t:outputText value="#{msg['unit.unitDescription']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" value="#{dataItem.unitDesc}" style="white-space: normal;"/>
																    </t:column>
																	<t:column id="unitAreaCol" width="80" sortable="true" rendered="#{pages$auctionDetails.advertisementMode}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['unit.unitArea']}" style="white-space: normal;"/>
																		</f:facet>
																		<t:outputText value="#{dataItem.area}" styleClass="A_RIGHT_NUM" style="white-space: normal;"/>
																	</t:column>
																	<t:column id="unitTypeEnCol" width="60" sortable="true" rendered="#{pages$auctionDetails.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['unit.type']}" style="white-space: normal;"/>
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.unitTypeEn}" style="white-space: normal;"/>
																	</t:column>
																	<t:column id="unitTypeArCol" width="60" sortable="true" rendered="#{pages$auctionDetails.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['unit.type']}" style="white-space: normal;"/>
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.unitTypeAr}" style="white-space: normal;"/>
																	</t:column>
																	<t:column id="unitUsageEnCol" width="80" sortable="true" rendered="#{pages$auctionDetails.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['inquiry.unitUsage']}" style="white-space: normal;"/>
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.unitUsageTypeEn}" style="white-space: normal;"/>
																	</t:column>
																	<t:column id="unitUsageTypeArCol" width="80" sortable="true" rendered="#{pages$auctionDetails.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['inquiry.unitUsage']}" style="white-space: normal;"/>
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.unitUsageTypeAr}" style="white-space: normal;"/>
																	</t:column>
																	<t:column id="rentValueCol" width="70" sortable="true">
																		<f:facet name="header"  >
																			<t:outputText value="#{msg['unit.rentValueCol']}" style="white-space: normal;"/>
																		</f:facet>
																		<t:outputText value="#{dataItem.rentValue}"  styleClass="A_RIGHT_NUM" style="white-space: normal;">
																			<f:convertNumber pattern="#{pages$auctionDetails.numberFormat}"/>
																		</t:outputText>
																	</t:column>
																	<t:column id="openingPriceCol" width="80" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['auctionUnit.openingPrice']}" style="white-space: normal;"/>
																		</f:facet>
																		<t:outputText value="#{dataItem.openingPrice}" styleClass="A_RIGHT_NUM" style="white-space: normal;">
																			<f:convertNumber pattern="#{pages$auctionDetails.numberFormat}"/>
																		</t:outputText>
																	</t:column>
																	<t:column id="depositAmountCol" width="70" sortable="true" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['auctionUnit.depositColumn']}" style="white-space: normal;"/>
																		<%-- 	<t:outputText value="#{msg['unit.depositAmount']}" />  --%>
																		</f:facet>
																		<t:outputText value="#{dataItem.depositAmount}" styleClass="A_RIGHT_NUM" style="white-space: normal;">
																			<f:convertNumber pattern="#{pages$auctionDetails.numberFormat}"/>
																		</t:outputText>
																	</t:column>
																	<pims:security screen="Pims.AuctionManagement.PrepareAuction.AuctionUnitAction" action="create">
																		<t:column id="actionCol" width="100" sortable="false" binding="#{pages$auctionDetails.actionColumn}" style="text-align: center; white-space: no-wrap;">
																			<f:facet name="header">
																	 			<t:outputText value="#{msg['commons.action']}" />  
																			</f:facet> 
																			<t:commandLink actionListener="#{pages$auctionDetails.editAuctionUnit}" rendered="false">
																				<h:graphicImage id="editIcon" title="#{msg['commons.edit']}" url="../resources/images/edit-icon.gif" />
																			</t:commandLink>
																			<t:outputLabel value="   "></t:outputLabel>
																			<t:commandLink action="#{pages$auctionDetails.removeAuctionUnit}"
																						   onclick="if (!confirm('#{msg['auctionUnit.messages.confirmRemove']}')) return">
																				<h:graphicImage id="deleteIcon" title="#{msg['commons.remove']}" url="../resources/images/delete_icon.png" />
																			</t:commandLink>				
																		</t:column>
																	</pims:security>
																</t:dataTable>
															</t:div>
																	<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																		style="width:98.8%;">
																		<t:panelGrid cellpadding="0" cellspacing="0"
																			width="100%" columns="2"
																			columnClasses="RECORD_NUM_TD">

																			<t:div styleClass="RECORD_NUM_BG">
																				<t:panelGrid cellpadding="0" cellspacing="0"
																					columns="3" columnClasses="RECORD_NUM_TD">

																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />

																					<h:outputText value=" : "
																						styleClass="RECORD_NUM_TD" />

																					<h:outputText
																						value="#{pages$auctionDetails.recordSize}"
																						styleClass="RECORD_NUM_TD" />

																				</t:panelGrid>
																			</t:div>

																			<t:dataScroller id="auctionUnitsScroller"
																				for="dt1" paginator="true" fastStep="1"
																				paginatorMaxPages="2" immediate="false"
																				paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true"
																				paginatorTableStyle="grid_paginator"
																				layout="singleTable"
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																				paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																				pageIndexVar="auctionUnitsPageNumber">

																				<f:facet name="first">
																					<t:graphicImage
																						url="../resources/images/first_btn.gif"
																						id="auctionUnitslblF"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastrewind">
																					<t:graphicImage
																						url="../resources/images/previous_btn.gif"
																						id="auctionUnitslblFR"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastforward">
																					<t:graphicImage
																						url="../resources/images/next_btn.gif"
																						id="auctionUnitsblFF"></t:graphicImage>
																				</f:facet>
																				<f:facet name="last">
																					<t:graphicImage
																						url="../resources/images/last_btn.gif"
																						id="auctionUnitslblL"></t:graphicImage>
																				</f:facet>
																				<t:div styleClass="PAGE_NUM_BG">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.auctionUnitsPageNumber}" />
																				</t:div>
																			</t:dataScroller>
																		</t:panelGrid>
																	</t:div>
																	</t:div>

																	<pims:security screen="Pims.AuctionManagement.PrepareAuction.AddAuctionUnits" action="create">
																<t:div styleClass="BUTTON_TD" style="padding-bottom:5px;padding-top:5px; padding-right:8px;">
																	<h:commandButton styleClass="BUTTON" value="#{msg['auctionUnit.applyOpeningPrice']}" actionListener="#{pages$auctionDetails.applyOpeningPriceToUnits}" binding="#{pages$auctionDetails.applyOpeningPriceButton}" style="width: 180px" tabindex="15"/>
																	<h:outputLabel value="  "/>
																	<h:commandButton styleClass="BUTTON" value="#{msg['auctionUnit.applyDeposit']}" actionListener="#{pages$auctionDetails.applyDepositToUnits}" binding="#{pages$auctionDetails.applyDepositButton}" style="width: 180px" tabindex="16"/>
																</t:div>
															</pims:security>
															</rich:tab>
													    <rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														
														<rich:tab id="requestHistoryTab"
															label="#{msg['contract.tabHeading.RequestHistory']}"
															action="#{pages$auctionDetails.tabRequestHistory_Click}">
															<t:div>
																<%@ include file="../pages/requestTasks.jsp"%>
															</t:div>
														</rich:tab>
														
																												
													</rich:tabPanel>
													
							</div>
							<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT" /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID" style="width:100%;" /></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
						<t:div styleClass="BUTTON_TD" style="padding-top:10px; padding-right:21px;">
							<pims:security screen="Pims.AuctionManagement.PrepareAuction.ApproveAdvertisement" action="create">
								<h:commandButton styleClass="BUTTON" value="#{msg['commons.approve']}"
									action="#{pages$auctionDetails.approveAuction}"
									onclick="if (!confirm('#{msg['auction.messages.confirmApprove']}')) return false"
									tabindex="19" binding="#{pages$auctionDetails.approveButton}"/>
							</pims:security>
							<pims:security screen="Pims.AuctionManagement.PrepareAuction.ApproveAdvertisement" action="create">
								<h:commandButton styleClass="BUTTON"
									value="#{msg['commons.reject']}"
									action="#{pages$auctionDetails.rejectAuction}"
									onclick="if (!confirm('#{msg['auction.messages.confirmReject']}')) return false"
									tabindex="20" rendered="#{pages$auctionDetails.canApproveAdvertisement}" 
									binding="#{pages$auctionDetails.rejectButton}"/>
							</pims:security>
							<pims:security screen="Pims.AuctionManagement.PrepareAuction.AddAuction" action="create">
								<h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" action="#{pages$auctionDetails.saveAuction}" binding="#{pages$auctionDetails.auctionDetailsSaveButton}" style="width: 75px" tabindex="13" />
							</pims:security>
							<pims:security screen="Pims.AuctionManagement.PrepareAuction.SubmitAuction" action="create">
								<h:commandButton styleClass="BUTTON" 
									onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false"
									action="#{pages$auctionDetails.done}" value="#{msg['commons.submitButton']}"
									tabindex="17" binding="#{pages$auctionDetails.submitButton}"/>
							</pims:security>
							<pims:security screen="Pims.AuctionManagement.PrepareAuction.PublishAdvertisement" action="create">
								<h:commandButton styleClass="BUTTON" value="#{msg['advertisement.publish']}"
									action="#{pages$auctionDetails.publishAuction}"
									onclick="if (!confirm('#{msg['auction.messages.confirmPublish']}')) return false"
									tabindex="18" binding="#{pages$auctionDetails.publishButton}"/>
							</pims:security>
							<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}" 
					        	 action="#{pages$auctionDetails.cancel}" binding="#{pages$auctionDetails.cancelButton}" style="width: 90px">
					       	</h:commandButton>
						</t:div>
						</div>
								</div>
							</h:form>
					  </div> 
						</td>			
					</tr>
				</table>
			</td>
		</tr>
		  
		<tr style="height:10px;width:100%;#height:10px;#width:100%;">
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
	</table>
	</div>
</body>
</html>
</f:view>