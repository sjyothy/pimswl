<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
		
	function closeWindow()
	{
		 window.opener.onMessageFromAddHealthAspectsRelatedPopup();
		 window.close();
		 
	}
	        
	function disableButtons(control)
	{
			
			var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
			
			control.nextSibling.nextSibling.onclick();
			
			return 
		
	}
	function surveyOptionCheckedUnchecked( checkControl )
	{
	
		if (checkControl.checked)
		{
			//optionOne[0]
		   var rowNum = checkControl.id.split("[");
		   rowNum =  rowNum[1].split("]");
		   
		   if(checkControl.id!='optionOne['+rowNum[0]+']')
		   {
		   	
		     document.getElementById('optionOne['+rowNum[0]+']').checked=false;
		     //alert(document.getElementById('optionOne['+rowNum[0]+']'));
		   }
		   if(checkControl.id!='optionTwo['+rowNum[0]+']')
		   {
		   
		     document.getElementById('optionTwo['+rowNum[0]+']').checked=false;
		     //alert(document.getElementById('optionTwo['+rowNum[0]+']'));
		   }
		   if(checkControl.id!='optionThree['+rowNum[0]+']')
		   {
		     document.getElementById('optionThree['+rowNum[0]+']').checked=false;
		     //alert(document.getElementById('optionThree['+rowNum[0]+']'));
		   }
		   if(checkControl.id!='optionFour['+rowNum[0]+']')
		   {
		     document.getElementById('optionFour['+rowNum[0]+']').checked=false;
		     //alert(document.getElementById('optionFour['+rowNum[0]+']'));
		   }
			
		}
	}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">

				<tr width="100%">
					<td width="100%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">

									<h:outputLabel
										value="#{pages$surveyForm.title}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="frm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION" style="width: 95%;">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:messages></h:messages>
															<h:outputText
																value="#{pages$surveyForm.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$surveyForm.successMessages}"
																escape="false" styleClass="INFO_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">

												<t:div styleClass="contentDiv"
													style="width:98%;margin-top: 5px;">
													<t:dataTable id="dataTableSurveyForm" rows="5" width="100%"
														value="#{pages$surveyForm.pageView.surveyResponses}"
														binding="#{pages$surveyForm.dataTable}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true">

														<t:column id="SurveyFormLinenumber" sortable="true">
															<f:facet name="header">
																<t:outputText id="SurveyFormLinenumberOT"
																	value="#{msg['commons.createdBy']}" />
															</f:facet>
															<h:outputText value="#{dataItem.lineNum}"
																style="white-space: normal;" styleClass="A_LEFT" />

														</t:column>
														<t:column id="SurveyFormquestions" sortable="true">
															<f:facet name="header">
																<t:outputText id="SurveyFormquestionOT"
																	value="#{msg['commons.createdBy']}" />
															</f:facet>
															<h:outputText
																value="#{pages$surveyForm.englishLocale? 
																				dataItem.questionEn:
																				dataItem.questionAr}"
																style="white-space: normal;" styleClass="A_LEFT" />

														</t:column>
														<t:column id="SurveyFormoptionOne" sortable="true">
															<f:facet name="header">
																<t:outputText id="SurveyFormoptionOneOT"
																	value="#{pages$surveyForm.englishLocale? 
																		pages$surveyForm.pageView.optionOneEn:
																		pages$surveyForm.pageView.optionOneAr}" />
															</f:facet>

															<t:selectBooleanCheckbox value="#{dataItem.optionOne}"
																forceId="true" id="optionOne"
																style="white-space: normal;" styleClass="A_LEFT"
																onclick="javascript:surveyOptionCheckedUnchecked(this);" />

														</t:column>
														<t:column id="SurveyFormoptionTwo" sortable="true">
															<f:facet name="header">
																<t:outputText id="SurveyFormoptionTwoOT"
																	value="#{pages$surveyForm.englishLocale? 
																		pages$surveyForm.pageView.optionTwoEn:
																		pages$surveyForm.pageView.optionTwoAr}" />
															</f:facet>

															<t:selectBooleanCheckbox value="#{dataItem.optionTwo}"
																forceId="true" id="optionTwo"
																style="white-space: normal;" styleClass="A_LEFT"
																onclick="javascript:surveyOptionCheckedUnchecked(this);" />

														</t:column>
														<t:column id="SurveyFormoptionThree" sortable="true">
															<f:facet name="header">
																<t:outputText id="SurveyFormoptionThreeOT"
																	value="#{pages$surveyForm.englishLocale? 
																		pages$surveyForm.pageView.optionThreeEn:
																		pages$surveyForm.pageView.optionThreeAr}" />
															</f:facet>

															<t:selectBooleanCheckbox value="#{dataItem.optionThree}"
																forceId="true" id="optionThree"
																onclick="javascript:surveyOptionCheckedUnchecked(this);" />

														</t:column>
														<t:column id="SurveyFormoptionFour" sortable="true">
															<f:facet name="header">
																<t:outputText id="SurveyFormoptionFourOT"
																	value="#{pages$surveyForm.englishLocale? 
																		pages$surveyForm.pageView.optionFourEn:
																		pages$surveyForm.pageView.optionFourAr}" />
															</f:facet>

															<t:selectBooleanCheckbox value="#{dataItem.optionFour}"
																forceId="true" id="optionFour"
																onclick="javascript:surveyOptionCheckedUnchecked(this);" />

														</t:column>


														<t:column id="SurveyFormDetails" sortable="true">
															<f:facet name="header">
																<h:outputText value="#{msg['commons.description']}" />
															</f:facet>
															<h:inputText value="#{dataItem.comments}" />


														</t:column>


													</t:dataTable>
												</t:div>


											</div>
											<t:div styleClass="BUTTON_TD"
												style="padding-top:10px; padding-right:21px;">

												<h:commandButton styleClass="BUTTON"
													value="#{msg['commons.saveButton']}"
													action="#{pages$surveyForm.onSave}">
												</h:commandButton>
												<h:commandButton styleClass="BUTTON" type="button"
													value="#{msg['commons.done']}"
													onclick="javaScript:closeWindow();">
												</h:commandButton>
											</t:div>



										</div>
									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>
		</body>
	</html>
</f:view>