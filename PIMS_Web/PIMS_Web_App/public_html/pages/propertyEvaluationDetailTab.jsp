<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<script type="text/javascript">
function showPropertySearchPopup()
 		{
 			var screen_width = screen.width;
 			var screen_height = screen.height;
 			var popup_width = screen_width-300;
 			var popup_height = screen_height-350;
 			var leftPos = (screen_width-popup_width)/2 -5, topPos = (screen_height-popup_height)/2 - 20;
 			var popup = window.open('propertyList.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
 			popup.focus();
 		}
 		function clearProperty()
      	{
      	    document.getElementById("detailsFrm:txtPropertyNamePE").value="";
      	    document.getElementById("detailsFrm:txtPropertyStatusPE").value="";
			document.getElementById("detailsFrm:txtEmiratePE").value="";
			document.getElementById("detailsFrm:txtLocationAreaPE").value="";
			document.getElementById("detailsFrm:txtLandNumberPE").value="";
			document.getElementById("detailsFrm:txtLandAreaPE").value="";
			document.getElementById("detailsFrm:txtAddressPE").value="";
			document.getElementById("detailsFrm:txtPropertyTypePE").value="";
			document.getElementById("detailsFrm:txtFloorCountPE").value="";
			document.getElementById("detailsFrm:txtConstructionDatePE").value="";
			document.forms[0].submit();
        }
	</script>
<t:div rendered="true" style="width:100%">
	<t:panelGrid width="100%" columns="5">
		<h:panelGroup rendered="#{pages$propertyEvaluationDetailTab.showEvaluationBy}">
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel styleClass="LABEL" style="width: 160px;"
				value="#{msg['property.evaluationBy']}:" />
		</h:panelGroup>
		<h:selectOneMenu id="evaluationBy" style="width: 196px;"
			value="#{pages$propertyEvaluationDetailTab.evaluationById}"
			readonly="#{pages$propertyEvaluationDetailTab.evaluationReadonlyMode}"
			rendered="#{pages$propertyEvaluationDetailTab.showEvaluationBy}"
			valueChangeListener="#{pages$propertyEvaluationDetailTab.evaluationByNewValue}"
			onchange="document.forms[0].submit();"
			required="false">
			<f:selectItem itemValue="1"
				itemLabel="#{msg['property.evaluationType.wholeProperty']}" />
			<f:selectItem itemValue="2"
				itemLabel="#{msg['property.evaluationType.unitType']}" />
		</h:selectOneMenu>
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel styleClass="LABEL" value="#{msg['property.name']}:"></h:outputLabel>
		</h:panelGroup>
		<h:inputText id="txtPropertyNamePE" styleClass="READONLY" 
			readonly="true" style="width:190px; height: 18px"
			value="#{pages$propertyEvaluationDetailTab.propertyName}"></h:inputText>
		<h:panelGroup>
			<h:commandLink onclick="showPropertySearchPopup();"
				rendered="#{!pages$propertyEvaluationDetailTab.readonlyMode && !pages$propertyEvaluationDetailTab.popUp}"
				title="#{msg['commons.search']}">
				<h:graphicImage id="imgProrewrewperty" style="MARGIN: 0px 0px -4px"
					url="../resources/images/app_icons/property.png"></h:graphicImage>
			</h:commandLink>
			<h:commandLink style="padding-left: 2px; padding-right:2px;"
				actionListener="#{pages$propertyEvaluationDetailTab.clearProperty}"
				onclick="clearProperty();"
				rendered="false"
				title="#{msg['commons.clear']}">
				<h:graphicImage id="imwerwegClear" style="MARGIN: 0px 0px -4px"
					url="../resources/images/delete.gif"></h:graphicImage>
			</h:commandLink>
			<h:commandLink style="padding-left: 2px; padding-right:2px;"
				onclick="showPropertyViewPopup();"
				rendered="false"
				title="#{msg['commons.view']}">
				<h:graphicImage id="ewrimgView" style="MARGIN: 0px 0px -4px"
					url="../resources/images/app_icons/Lease-contract.png"></h:graphicImage>
			</h:commandLink>
		</h:panelGroup>
		<h:outputLabel id="lblAwerppStatus" value="#{msg['property.status']}"></h:outputLabel>
		<h:inputText id="txtPropertyStatusPE"
			style="width:190px; height:18px;" styleClass="READONLY"
			readonly="true"
			value="#{pages$propertyEvaluationDetailTab.propertyStatus}"></h:inputText>

		<h:outputLabel styleClass="LABEL" value="#{msg['commons.Emirate']}: "></h:outputLabel>
		<h:inputText id="txtEmiratePE" style="width:190px; height:18px;"
			styleClass="READONLY" readonly="true"
			value="#{pages$propertyEvaluationDetailTab.emirate}"></h:inputText>
		<h:outputLabel value=" " />
		<h:outputLabel styleClass="LABEL" value="#{msg['commons.area']}: "></h:outputLabel>
		<h:inputText id="txtLocationAreaPE" style="width:190px; height:18px;"
			styleClass="READONLY" readonly="true"
			value="#{pages$propertyEvaluationDetailTab.locationArea}"></h:inputText>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['property.landNumber']}" />
		<h:inputText maxlength="20" id="txtLandNumberPE"
			styleClass="READONLY" readonly="true"
			value="#{pages$propertyEvaluationDetailTab.landNumber}"
			style="width:190px; height: 18px"></h:inputText>
		<h:outputLabel value=" " />
		<h:outputLabel styleClass="LABEL" value="#{msg['property.landArea']}"></h:outputLabel>
		<h:inputText id="txtLandAreaPE" style="width:190px; height: 18px"
			styleClass="READONLY" readonly="true" maxlength="20"
			value="#{pages$propertyEvaluationDetailTab.landArea}" rendered="true" />

		<h:outputLabel styleClass="LABEL" value="#{msg['commons.address']}: "></h:outputLabel>
		<h:inputText id="txtAddressPE" styleClass="READONLY" readonly="true"
			value="#{pages$propertyEvaluationDetailTab.address}"
			style="width:190px; height: 18px"></h:inputText>
		<h:outputLabel value=" " />
		<h:outputLabel styleClass="LABEL" value="#{msg['property.type']}"></h:outputLabel>
		<h:inputText id="txtPropertyTypePE"
			style="width:190px; height: 18px" styleClass="READONLY"
			readonly="true" maxlength="20"
			value="#{pages$propertyEvaluationDetailTab.propertyType}"
			rendered="true" />

		<h:outputLabel styleClass="LABEL"
			value="#{msg['property.floorCount']}: "></h:outputLabel>
		<h:inputText id="txtFloorCountPE" styleClass="READONLY"
			readonly="true"
			value="#{pages$propertyEvaluationDetailTab.floorCount}"
			style="width:190px; height: 18px"></h:inputText>
		<h:outputLabel value=" " />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['property.constructionDate']}"></h:outputLabel>
		<h:inputText id="txtConstructionDatePE"
			style="width:190px; height: 18px" styleClass="READONLY"
			readonly="true" maxlength="20"
			value="#{pages$propertyEvaluationDetailTab.constructionDate}"
			rendered="true" />
		
		<h:panelGroup rendered="#{pages$propertyEvaluationDetailTab.showUnitTypeDetails && !pages$propertyEvaluationDetailTab.evaluationReadonlyMode}">
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel 
				value="Unit Type :">
				</h:outputLabel>
		</h:panelGroup>
		
		<h:selectOneMenu id="unitTypesId" style="width: 196px;"
			value="#{pages$propertyEvaluationDetailTab.unitTypeId}"
			readonly="#{pages$propertyEvaluationDetailTab.evaluationReadonlyMode}"
			binding="#{pages$propertyEvaluationDetailTab.cboUnitType}"
			rendered="#{pages$propertyEvaluationDetailTab.showUnitTypeDetails && !pages$propertyEvaluationDetailTab.evaluationReadonlyMode}"
			required="false">
			<f:selectItem itemValue="-1"
				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
			<f:selectItems value="#{pages$propertyEvaluationDetailTab.propertyUnitTypeList}" />
		</h:selectOneMenu>
		<h:outputLabel value=" " />
		
		<h:outputLabel value="Unit Side :" rendered="#{pages$propertyEvaluationDetailTab.showUnitTypeDetails && !pages$propertyEvaluationDetailTab.evaluationReadonlyMode}">
		</h:outputLabel>
		<h:selectOneMenu id="unitSideId" style="width: 196px;"
			binding="#{pages$propertyEvaluationDetailTab.cboUnitSide}"
			readonly="#{pages$propertyEvaluationDetailTab.evaluationReadonlyMode}"
			value="#{pages$propertyEvaluationDetailTab.unitSideId}"
			rendered="#{pages$propertyEvaluationDetailTab.showUnitTypeDetails && !pages$propertyEvaluationDetailTab.evaluationReadonlyMode}"
			required="false">
			<f:selectItem itemValue="-1"
				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
			<f:selectItems value="#{pages$ApplicationBean.unitSideType}" />
		</h:selectOneMenu>
		
		<h:panelGroup rendered="#{pages$propertyEvaluationDetailTab.showUnitTypeDetails && !pages$propertyEvaluationDetailTab.evaluationReadonlyMode}">
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel value="Rent Amount :" >
		</h:outputLabel>
		</h:panelGroup>
		<h:inputText style="width:190px; height:18px;" 
		binding="#{pages$propertyEvaluationDetailTab.txtRent}"
		value="#{pages$propertyEvaluationDetailTab.unitTypeRent}"
		readonly="#{pages$propertyEvaluationDetailTab.evaluationReadonlyMode}"
		rendered="#{pages$propertyEvaluationDetailTab.showUnitTypeDetails && !pages$propertyEvaluationDetailTab.evaluationReadonlyMode}">
		</h:inputText>
		<h:outputLabel value=" " />
		
		<h:outputLabel value="Area From :" rendered="#{pages$propertyEvaluationDetailTab.showUnitTypeDetails && !pages$propertyEvaluationDetailTab.evaluationReadonlyMode}">
		</h:outputLabel>
		<h:inputText style="width:190px; height:18px;"  
		binding="#{pages$propertyEvaluationDetailTab.txtAreaFrom}"
		readonly="#{pages$propertyEvaluationDetailTab.evaluationReadonlyMode}"
		rendered="#{pages$propertyEvaluationDetailTab.showUnitTypeDetails && !pages$propertyEvaluationDetailTab.evaluationReadonlyMode}"
		value="#{pages$propertyEvaluationDetailTab.areaFrom}">
		</h:inputText>
		
		<h:outputLabel value="Area To :" rendered="#{pages$propertyEvaluationDetailTab.showUnitTypeDetails && !pages$propertyEvaluationDetailTab.evaluationReadonlyMode}">
		</h:outputLabel>
		<h:inputText style="width:190px; height:18px;" 
		binding="#{pages$propertyEvaluationDetailTab.txtAreaTo}"
		readonly="#{pages$propertyEvaluationDetailTab.evaluationReadonlyMode}"
		value="#{pages$propertyEvaluationDetailTab.areaTo}"
		rendered="#{pages$propertyEvaluationDetailTab.showUnitTypeDetails && !pages$propertyEvaluationDetailTab.evaluationReadonlyMode}">
		</h:inputText>
				

		<h:outputLabel value=" " />
		<t:panelGroup colspan="5" >
		<t:div styleClass="BUTTON_TD" >
		<h:commandButton styleClass="BUTTON" value="#{msg['propertyEvaluation.label.addRent']}" 
		action="#{pages$propertyEvaluationDetailTab.addUnitRentToGrid}"
		rendered="#{pages$propertyEvaluationDetailTab.showUnitTypeDetails && 
		!pages$propertyEvaluationDetailTab.evaluationReadonlyMode}">
		</h:commandButton>
		
		<h:commandButton styleClass="BUTTON" value="#{msg['propertyEvaluation.label.clearFields']}" 
		action="#{pages$propertyEvaluationDetailTab.clearUnitTypeFields}"
		rendered="#{pages$propertyEvaluationDetailTab.showUnitTypeDetails && 
		!pages$propertyEvaluationDetailTab.evaluationReadonlyMode}">
		</h:commandButton>
		</t:div>
		</t:panelGroup>
		
	</t:panelGrid>
</t:div>
<t:div 
	rendered="#{pages$propertyEvaluationDetailTab.showUnitTypeDetails}">
	<t:div styleClass="contentDiv" style="width:94.5%">
		<t:dataTable id="unitTypeGrid" rows="#{pages$propertyEvaluationDetailTab.paginatorRows}"
			value="#{pages$propertyEvaluationDetailTab.evaluationUnitTypeList}"
			binding="#{pages$propertyEvaluationDetailTab.evaluationUnitDT}" 
			preserveDataModel="true" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
			width="100%">

			<t:column id="unitType" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.replaceCheque.unitType']}" />
				</f:facet>
				<h:outputText value="#{dataItem.unitType}" />
			</t:column>
			
			<t:column id="unitSide" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['inquiry.unitSide']}" />
				</f:facet>
				<h:outputText value="#{dataItem.unitSide}" />
			</t:column>
			
			<t:column id="AreaFrom" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['receiveProperty.fieldName.areaFrom']}" />
				</f:facet>
				<h:outputText value="#{dataItem.areaFrom}" />
			</t:column>
			
			<t:column id="AreaTo" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['receiveProperty.fieldName.areaTo']}" />
				</f:facet>
				<h:outputText value="#{dataItem.areaTo}" />
			</t:column>
			
			<t:column id="noOfUnits" sortable="true" rendered="false">
				<f:facet name="header">
					<h:outputText value="#{msg['receiveProperty.noOfUnits']}" />
				</f:facet>
				<h:outputText value="#{dataItem.noOfUnits}" />
			</t:column>
			
			<t:column id="newRentValue" sortable="true">
				<f:facet name="header">
					<h:outputText
						value="#{msg['property.evaluation.NewRentValueForUnitType']}" />
				</f:facet>
				<h:outputText value="#{dataItem.newRentValue}" />
			</t:column>
			<t:column rendered="#{!pages$propertyEvaluationDetailTab.evaluationReadonlyMode}" id="actionCol" sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.action']}" />
				</f:facet>
				<t:commandLink
					action="#{pages$propertyEvaluationDetailTab.deleteUnitType}">
					<h:graphicImage id="delete"
						title="Delete"
						url="../resources/images/delete.gif" />&nbsp;
				</t:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>

</t:div>
<t:div
 	rendered="#{pages$propertyEvaluationDetailTab.showUnitTypeDetails}"
	styleClass="contentDivFooter AUCTION_SCH_RF" style="width:95.5%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']} :"/>
						<h:outputText value="#{pages$propertyEvaluationDetailTab.recordSize}"/>
						<h:outputText  />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="unitTypeScroller" for="unitTypeGrid" paginator="true"  
						fastStep="1" 
						paginatorTableClass="paginator"
						paginatorMaxPages="#{pages$propertyEvaluationDetailTab.paginatorMaxPages}" immediate="false"
						renderFacetsIfSinglePage="true" 												
						paginatorTableStyle="grid_paginator" layout="singleTable" 
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" 
						pageIndexVar="pageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}" id="lblF11"></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR11"></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF11"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}" id="lblL11"></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
							<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
	</t:div>
