<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript" src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript"> 

    			
	        function resetValues()
      	    {
      	    
      	       var cmbType=document.getElementById("inspectionDetailsForm:selectInspectionType");
		        cmbType.selectedIndex = 0;
		        document.getElementById("inspectionDetailsForm:txtInspectionNo").value="";
      	        $('inspectionDetailsForm:inspectionDateFrom').component.resetSelectedDate();
            }
            
			function showUnitSearchPopUp()
	        {
			   var screen_width = 1024;
			   var screen_height = 500;
               var popup_width = screen_width-200;
               var popup_height = screen_height;
               var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        
			   
	            window.open('UnitSearch.jsf?pageMode=MODE_SELECT_MANY_POPUP&context=InspectionUnits','_blank','width='+(popup_width)+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes,dialog=yes');
	        }
			function showAttachmentPopUp()
            { 
	           var screen_width = screen.width;
	           var screen_height = screen.height;
	           window.open('Attachment.jsp','_blank','width='+(screen_width-500)+',height='+(screen_height-400)+',left=300,top=250,scrollbars=yes,status=yes');
	
	       }
	       
	       function SearchMemberPopup(){
			   var screen_width = 1024;
			   var screen_height = 450;
			   var popup_width = screen_width-150;
		       var popup_height = screen_height-50;
		       var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		       var popup = window.open('SearchMemberPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
		       popup.focus();
			}
			
			function showAddViolationPopup(){
			   var screen_width = 1024;
			   var screen_height = 450;
			   var popup_width = screen_width-200;
		       var popup_height = screen_height;
		       var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		       var popup = window.open('AddViolationPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,location=no,directories=no,status=no,resizable=yes,titlebar=no,dialog=yes');
		       popup.focus();
			   
			}
			
			function showViolationActionsPopUp()
	        {
			   var screen_width = screen.width;
			   var screen_height = screen.height;
			   
	            window.open('ViolationActions.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-260)+',left=0,top=40,scrollbars=yes,status=yes');
	        }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE" >
			<div class="containerDiv">

				<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['inspection.details']}" styleClass="HEADER_FONT"/>

									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="inspectionDetailsForm" enctype="multipart/form-data"
												style="WIDTH: 96%;">

												<div styleClass="MESSAGE">
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:outputText
																	value="#{pages$inspectionDetails.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText
																	value="#{pages$inspectionDetails.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																	<h:inputHidden id="hdnInspectionId"
															value="#{pages$inspectionDetails.inspectionId}" />
														<h:inputHidden id="hdnCreatedOn"
															value="#{pages$inspectionDetails.createdOn}" />
														<h:inputHidden id="hdnCreatedBy"
															value="#{pages$inspectionDetails.createdBy}" /> 
																	
															</td>
														</tr>
													</table>
													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel id="auctionTabPanel" switchType="server"
																style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%">
																<rich:tab id="auctionDetailsTab"
																	label="#{msg['inspection.details']}">
																	<h:inputHidden id="hdnInspectionNo"
																		value="#{pages$inspectionDetails.inspectionView.inspectionNumber}" />
																	<t:panelGrid columns="4" border="0" width="100%"
																		cellpadding="2" cellspacing="2" >
																		<t:panelGroup>
																			<t:panelGrid columns="2" cellpadding="2"
																				cellspacing="2">
                                                                               <h:panelGroup> 
																				<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"
																					value="#{msg['inspection.inspectionNo']}:"></h:outputLabel>
																					</h:panelGroup>

																				<h:inputText id="txtInspectionNo"  readonly="true"
																					value="#{pages$inspectionDetails.inspectionView.inspectionNumber}"
																					styleClass="READONLY" />
																					
																			 <h:panelGroup> 	
																				<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"
																					value="#{msg['inspection.date']}:"></h:outputLabel>
																					</h:panelGroup>

																				<rich:calendar id="inspectionDateFrom"
																					value="#{pages$inspectionDetails.inspectionView.inspectionDate}"
																					binding="#{pages$inspectionDetails.calendarInspectionFrom}"
																					popup="true"
																					datePattern="#{pages$inspectionDetails.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:170px; height:14px;"
																					locale="#{pages$inspectionDetails.locale}" />	

                                                                                 <h:panelGroup> 
																				<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"
																					value="#{msg['inspection.type']}:"></h:outputLabel>
																				</h:panelGroup>	

																				<h:selectOneMenu id="selectInspectionType"
																					
																					binding="#{pages$inspectionDetails.comboInspectionType}"
																					value="#{pages$inspectionDetails.selectedType}">
																					<f:selectItem
																						itemLabel="#{msg['commons.pleaseSelect']}"
																						itemValue="-1" />
																					<f:selectItems
																						value="#{pages$inspectionDetails.inspectionTypeList}" />
																				</h:selectOneMenu>


																			</t:panelGrid>
																		</t:panelGroup>
																		<t:panelGroup>
																			<t:panelGrid columns="2" cellpadding="2"
																				cellspacing="2">
																				
																			<h:panelGroup> 
																				<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.status']}:"></h:outputLabel>
																					</h:panelGroup>

																				<h:inputText id="txtInspectionStatusEn"  readonly="true" rendered="#{pages$inspectionDetails.isEnglishLocale}"
																					value="#{pages$inspectionDetails.inspectionView.statusEn}"
																					styleClass="READONLY" />
																				<h:inputText id="txtInspectionStatusAr"  readonly="true" rendered="#{!pages$inspectionDetails.isEnglishLocale}"
																					value="#{pages$inspectionDetails.inspectionView.statusAr}"
																					styleClass="READONLY" />		
																						
																		
																					
																					<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.description']}: "></h:outputLabel>                 
																					<h:inputTextarea id="txtAreaInspectionDesc" styleClass="TEXTAREA"  binding="#{pages$inspectionDetails.inspectionDescription}" value="#{pages$inspectionDetails.inspectionView.remarks}">
																					</h:inputTextarea>
																			</t:panelGrid>
																		</t:panelGroup>
																	</t:panelGrid>
																</rich:tab>
																<rich:tab id="one2"
																	label="#{msg['inspection.tab.Units']}">
																	<t:panelGrid id="unitBtnGrid" width="99.7%" border="0"
																		columns="3" columnClasses="BUTTON_TD">
																		<t:panelGroup id="unitBtnGroup" colspan="10">
																			<h:outputText id="empty1" escape="false"
																				value="&nbsp;"></h:outputText>
																			<h:outputText id="empty2" escape="false"
																				value="&nbsp;"></h:outputText>
																			<h:commandButton id="unitBtn" styleClass="BUTTON"
																				value="#{msg['inspectionDetails.AddUnits']}"
																				binding="#{pages$inspectionDetails.btnAddUnits}"																				
																				actionListener="#{pages$inspectionDetails.openUnitPopup}"
																				style="width: 75px" />
																		</t:panelGroup>
																	</t:panelGrid>
																	<t:div id="one5" styleClass="contentDiv"
																		style="width:98.1%">
																		<t:dataTable id="dt1" 
																			value="#{pages$inspectionDetails.inspectionUnitsDataList}"
																			binding="#{pages$inspectionDetails.tbl_InspectionUnits}"
																			rows="10" preserveDataModel="true"
																			preserveSort="false" var="dataItem"
																			rowClasses="row1,row2" rules="all"
																			renderedIfEmpty="true" width="100%">

																			<t:column id="unitNo" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="dtone1"
																						value="#{msg['inspection.unitRefNum']}" />
																				</f:facet>
																				<t:outputText id="dtone16" styleClass="A_LEFT"
																					value="#{dataItem.unitView.unitNumber}" />
																			</t:column>
																			<t:column id="propertyName" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="dtone2"
																						value="#{msg['contract.property.Name']}" />
																				</f:facet>
																				<t:outputText id="dtone15" styleClass="A_LEFT"
																					value="#{dataItem.unitView.propertyCommercialName}" />
																			</t:column>
																			<t:column id="UnitTypeAR" sortable="true"
																				rendered="#{pages$inspectionDetails.isArabicLocale}">
																				<f:facet name="header">
																					<t:outputText id="dtone5"
																						value="#{msg['unit.type']}" />
																				</f:facet>
																				<t:outputText id="dtone12" styleClass="A_LEFT"
																					value="#{dataItem.unitView.unitTypeAr}" />
																			</t:column>
																			<t:column id="UnitTypeEn" sortable="true"
																				rendered="#{pages$inspectionDetails.isEnglishLocale}">
																				<f:facet name="header">
																					<t:outputText id="dtone6"
																						value="#{msg['unit.type']}" />
																				</f:facet>
																				<t:outputText id="dtone10" styleClass="A_LEFT"
																					value="#{dataItem.unitView.unitTypeEn}" />
																			</t:column>
																			<t:column id="UnitStatusEn" sortable="true"
																				rendered="#{pages$inspectionDetails.isEnglishLocale}">
																				<f:facet name="header">
																					<t:outputText id="dtone25"
																						value="#{msg['unit.unitStatus']}" />
																				</f:facet>
																				<t:outputText id="dtone26" styleClass="A_LEFT"
																					value="#{dataItem.unitView.statusEn}" />
																			</t:column>
																			<t:column id="UnitStatusAr" sortable="true"
																				rendered="#{pages$inspectionDetails.isArabicLocale}">
																				<f:facet name="header">
																					<t:outputText id="dtone27"
																						value="#{msg['unit.unitStatus']}" />
																				</f:facet>
																				<t:outputText id="dtone28" styleClass="A_LEFT"
																					value="#{dataItem.unitView.statusAr}" />
																			</t:column>
																			<t:column id="contractNumber" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="dtone4"
																						value="#{msg['inspection.contractRefNum']}" />
																				</f:facet>
																				<t:outputText id="dtone13" styleClass="A_LEFT"
																					value="#{dataItem.contractNumber}" />
																			</t:column>
																			<t:column id="tenantName" sortable="true">
																				<f:facet name="header">
																					<t:outputText id="dtone3"
																						value="#{msg['contract.tenantName']}" />
																				</f:facet>
																				<t:outputText id="dtone14" styleClass="A_LEFT"
																					value="#{dataItem.tenantName}" />
																			</t:column>
																			<t:column id="actions" rendered="#{pages$inspectionDetails.actionColume}">
																				<f:facet name="header">
																					<t:outputText id="dtone7"
																						value="#{msg['commons.actions']}" />
																				</f:facet>
																				<%-- <h:commandLink id="cmdLinkViolation"
																					action="#{pages$inspectionDetails.cmdLinkViolation_Clicked}"
																					binding="#{pages$inspectionDetails.violationLink}">
																					<h:graphicImage id="ViolationsInspectionUnits"
																						title="#{msg['inspection.action.Violation']}"
																						url="../resources/images/app_icons/Action.png" />
																				</h:commandLink>
																				--%>
																				<a4j:commandLink reRender="dt1,recordSize"
																					binding="#{pages$inspectionDetails.cmdDelUnit}"
																					action="#{pages$inspectionDetails.btnDeleteUnits_Click}">&nbsp;
																	      			<h:graphicImage id="deleteInspectionUnits" title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
																				</a4j:commandLink>
																			</t:column>
																		</t:dataTable>
																	</t:div>
																	<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																		style="width:99.2%">
																		<t:panelGrid cellpadding="0" cellspacing="0"
																			width="100%" columns="2"
																			columnClasses="RECORD_NUM_TD">
																			<t:div styleClass="RECORD_NUM_BG">
																				<t:panelGrid cellpadding="0" cellspacing="0"
																					columns="3" columnClasses="RECORD_NUM_TD">
																					<h:outputText style="#font-size:10px;"
																						value="#{msg['commons.recordsFound']}" />
																					<h:outputText style="#font-size:10px;" value=" : "
																						styleClass="RECORD_NUM_TD" />
																					<h:outputText id="recordSize"
																						style="#font-size:10px;"
																						value="#{pages$inspectionDetails.recordSize}"
																						styleClass="RECORD_NUM_TD" />
																				</t:panelGrid>
																			</t:div>
																			<t:dataScroller id="scroller1" for="dt1"
																				paginator="true" fastStep="1"
																				paginatorMaxPages="#{pages$inspectionDetails.paginatorMaxPages}"
																				immediate="false" paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true"
																				pageIndexVar="pageNumber"
																				paginatorTableStyle="grid_paginator"
																				layout="singleTable"
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																				paginatorActiveColumnStyle="font-weight:bold;"
																				paginatorRenderLinkForActive="false"
																				styleClass="SCH_SCROLLER">

																				<f:facet name="first">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblFk"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastrewind">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFRk"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastforward">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFFk"></t:graphicImage>
																				</f:facet>
																				<f:facet name="last">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblLk"></t:graphicImage>
																				</f:facet>
																				<t:div styleClass="PAGE_NUM_BG">
																					<h:outputText style="#font-size:10px;"
																						styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText id="pageNumber" styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;#font-size:10px;"
																						value="#{requestScope.pageNumber}" />
																				</t:div>
																			</t:dataScroller>
																		</t:panelGrid>
																	</t:div>
																</rich:tab>
																<rich:tab id="newInspectionTeam" style="width:100%"
																	action="#{pages$inspectionDetails.tabInspectionTeam_Click}"
																	label="#{msg['inspect.tab.Team']}">
																	<t:panelGrid id="receivingButtonGrid" width="100%"
																		columnClasses="BUTTON_TD" columns="3">
																		<h:outputText id="empty11" escape="false"
																			value="&nbsp;"></h:outputText>
																		<h:outputText id="empty21" escape="false"
																			value="&nbsp;"></h:outputText>
																		<t:panelGrid id="receivingButtonGridSub" width="100%"
																			columnClasses="BUTTON_TD" columns="1">
																			<h:commandButton styleClass="BUTTON"
																				binding="#{pages$inspectionDetails.btnAddTeamMembers}"
																				value="#{msg['receiveProperty.AddMember']}" style="width: 85px" 
																				action="#{pages$inspectionDetails.showMemberSearchPopup}" />
																		</t:panelGrid>
																	</t:panelGrid>
																		<t:div id="divmainTabSUB" styleClass="contentDiv"
																		style="width:98.1%">
																		<t:dataTable id="test8" 
																			value="#{pages$inspectionDetails.inspectionTeamDataList}"
																			binding="#{pages$inspectionDetails.inspectionTeamGrid}"
																			rows="10" preserveDataModel="true"
																			preserveSort="false" var="dataitemTeam"
																			rowClasses="row1,row2" rules="all"
																			renderedIfEmpty="true" width="100%">

																			<t:column id="dataItemRCV2" sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['member.fullName']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataitemTeam.secUserView.fullNamePrimary}" />
																				</t:column>
																				<t:column id="dataItemRCV4" sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['member.loginName']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT" 
																						value="#{dataitemTeam.secUserView.userName}" />
																				</t:column>
																				<t:column id="dataItemRCV5" sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['member.groupName']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT" 
																						value="#{dataitemTeam.secUserView.groupNamePrimary}" />

																				</t:column>
																				<t:column  rendered="#{pages$inspectionDetails.actionColume}">
																					<f:facet name="header">
																						<t:outputText id="lbl_delete_RCV"
																							value="#{msg['commons.delete']}" />
																					</f:facet>
																					<h:commandLink id="deleteIconRCV"
																						action="#{pages$inspectionDetails.cmdInspectionTeamMemberDelete_Click}"
																						binding="#{pages$inspectionDetails.deleteLink}">
																						<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
																					</h:commandLink>
																				</t:column>
																		</t:dataTable>
																	</t:div>
																	<t:div id="paginationdiv" styleClass="contentDivFooter AUCTION_SCH_RF"
																		style="width:99.1%">
																		<t:panelGrid cellpadding="0" cellspacing="0"
																			width="100%" columns="2"
																			columnClasses="RECORD_NUM_TD">
																			<t:div styleClass="RECORD_NUM_BG">
																				<t:panelGrid cellpadding="0" cellspacing="0"
																					columns="3" columnClasses="RECORD_NUM_TD">
																					<h:outputText style="#font-size:10px;"
																						value="#{msg['commons.recordsFound']}" />
																					<h:outputText style="#font-size:10px;" value=" : "
																						styleClass="RECORD_NUM_TD" />
																					<h:outputText id="receivingTeamRecordSize"
																						style="#font-size:10px;"
																						value="#{pages$inspectionDetails.receivingTeamRecordSize}"
																						styleClass="RECORD_NUM_TD" />
																				</t:panelGrid>
																			</t:div>
																			<t:dataScroller id="scroller2" for="test8"
																				paginator="true" fastStep="1"
																				paginatorMaxPages="#{pages$inspectionDetails.paginatorMaxPages}"
																				immediate="false" paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true"
																				pageIndexVar="teamPageNumber"
																				paginatorTableStyle="grid_paginator"
																				layout="singleTable"
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																				paginatorActiveColumnStyle="font-weight:bold;"
																				paginatorRenderLinkForActive="false"
																				styleClass="SCH_SCROLLER">

																				<f:facet name="first">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblFk1"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastrewind">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFRk1"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastforward">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFFk1"></t:graphicImage>
																				</f:facet>
																				<f:facet name="last">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblLk1"></t:graphicImage>
																				</f:facet>
																				<t:div styleClass="PAGE_NUM_BG">
																					<h:outputText style="#font-size:10px;"
																						styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText id="teamPageNumber" styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;#font-size:10px;"
																						value="#{requestScope.teamPageNumber}" />
																				</t:div>
																			</t:dataScroller>
																		</t:panelGrid>
																	</t:div>
																</rich:tab>
																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>
																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@  include file="notes/notes.jsp"%>
																</rich:tab>
																<rich:tab id="resultsTabVioaltions" binding="#{pages$inspectionDetails.violationTab}"
																	label="#{msg['violation.violations']}">
																	<t:panelGrid id="violationButtonGrid" width="100%"
																		columnClasses="BUTTON_TD" columns="3">
																		<h:outputText id="empty112" escape="false"
																			value="&nbsp;"></h:outputText>
																		<h:outputText id="empty212" escape="false"
																			value="&nbsp;"></h:outputText>
																		<t:panelGrid id="violationButtonGridSub" width="100%"
																			columnClasses="BUTTON_TD" columns="1">
																			<h:commandButton styleClass="BUTTON"
																				style="width:95px"
																				binding="#{pages$inspectionDetails.addViolationBtn}"
																				value="#{msg['violation.addViolation']}"
																				action="#{pages$inspectionDetails.showAddViolationPopup}" />
																		</t:panelGrid>
																	</t:panelGrid>
																	<t:div style="text-align:center; width:100%">
																		<t:div styleClass="contentDiv"
																			style="text-align:center; width:99%">

																			<t:dataTable id="dtViolation" rows="5"
																				preserveDataModel="false" preserveSort="false"
																				var="dataItemVI" rules="all" renderedIfEmpty="true"
																				width="100%"
																				value="#{pages$inspectionDetails.violationViewDataList}"
																				binding="#{pages$inspectionDetails.dataTableViolation}">

																				<t:column id="colViolUnitNumber" 
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText id="otxtUnitNumbet"
																							value="#{msg['unit.unitNumber']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						style="WHITE-SPACE: normal"
																						value="#{dataItemVI.unitNumber}" />
																				</t:column>
																				<t:column id="colViolationDate" 
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText id="otxtVolDate"
																							value="#{msg['violation.Date']}" />
																					</f:facet>
																					<t:outputText id="otxtVolDateValue"
																						styleClass="A_LEFT"
																						value="#{dataItemVI.violationDateString}" />

																				</t:column>

																				<t:column id="colViolCategory" 
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText id="otxtVolCategory"
																							value="#{msg['violation.category']}" />
																					</f:facet>
																					<t:outputText id="otxtCatValueEn"
																						styleClass="A_LEFT" style="WHITE-SPACE: normal"
																						value="#{dataItemVI.categoryEn}"
																						rendered="#{pages$inspectionDetails.isEnglishLocale}" />
																					<t:outputText id="otxtCatValueAr"
																						styleClass="A_LEFT" style="WHITE-SPACE: normal"
																						value="#{dataItemVI.categoryAr}"
																						rendered="#{pages$inspectionDetails.isArabicLocale}" />

																				</t:column>
																				<t:column id="colViolType" 
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText id="otxtVoilType"
																							value="#{msg['violation.type']}" />
																					</f:facet>
																					<t:outputText id="otxtVoilTypeValueEn"
																						styleClass="A_LEFT" style="WHITE-SPACE: normal"
																						value="#{dataItemVI.typeEn}"
																						rendered="#{pages$inspectionDetails.isEnglishLocale}" />
																					<t:outputText id="otxtVoilTypeValueAr"
																						styleClass="A_LEFT" style="WHITE-SPACE: normal"
																						value="#{dataItemVI.typeAr}"
																						rendered="#{pages$inspectionDetails.isArabicLocale}" />
																				</t:column>
																				<t:column id="colViolTenant" 
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['contract.tenantName']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						style="WHITE-SPACE: normal"
																						value="#{dataItemVI.tenantName}" />
																				</t:column>
																				<t:column id="colVioContract" 
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['contract.contractNumber']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						style="WHITE-SPACE: normal"
																						value="#{dataItemVI.contractNumber}" />
																				</t:column>

																				<t:column id="colVioStatus" 
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['violation.status']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						style="WHITE-SPACE: normal"
																						value="#{dataItemVI.statusEn}"
																						rendered="#{pages$inspectionDetails.isEnglishLocale}" />
																					<t:outputText styleClass="A_LEFT"
																						style="WHITE-SPACE: normal"
																						value="#{dataItemVI.statusAr}"
																						rendered="#{pages$inspectionDetails.isArabicLocale}" />

																				</t:column>
																				<t:column id="colViolationAction" sortable="false"
																					 rendered="#{pages$inspectionDetails.actionColumeForVoilation}">
																					<f:facet name="header">
																						<t:outputText value="#{msg['commons.action']}" />
																					</f:facet>
																					
																					<h:commandLink 
																					     binding="#{pages$inspectionDetails.cmdLnkEditViolation}"
 																						 action="#{pages$inspectionDetails.btnEditViolation_Click}">
																						<h:graphicImage style="margin-left:5px;"
																							title="#{msg['commons.edit']}"
																							url="../resources/images/edit-icon.gif" />
																					</h:commandLink>

																					<h:commandLink
																					    binding="#{pages$inspectionDetails.cmdLnkViolationActions}"
																						action="#{pages$inspectionDetails.btnAddViolationAction_Click}">
																						<h:graphicImage style="margin-left:5px;"
																							title="#{msg['violation.violationAction']}"
																							url="../resources/images/app_icons/Action.png" />
																					</h:commandLink>
																					<h:commandLink
																					    binding="#{pages$inspectionDetails.cmdLnkDelViolation}"
																						action="#{pages$inspectionDetails.btnDeleteAddedViolation_Click}">
																						<h:graphicImage style="margin-left:5px;"
																							title="#{msg['violation.violationDelete']}"
																							url="../resources/images/delete.gif" />
																					</h:commandLink>
																					

																				</t:column>

																			</t:dataTable>
																		</t:div>
																		<t:div id="paginationdivResult"
																			styleClass="contentDivFooter"
																			style="width:100%;">
																			<t:panelGrid id="paginationpanelResult" cellpadding="0"
																			cellspacing="0" width="100%" columns="2"
																			columnClasses="RECORD_NUM_TD">
																			<t:div id="paginationpanelSubDivResult"
																			styleClass="RECORD_NUM_BG">
																			<t:panelGrid cellpadding="0" cellspacing="0"
																			columns="3" columnClasses="RECORD_NUM_TD">
																			<h:outputText id="recordFounsOTResult"
																			value="#{msg['commons.recordsFound']}" />
																			<h:outputText id="recordFounsCololOTResult" value=" : "
																			styleClass="RECORD_NUM_TD" />
																			<h:outputText id="recordSizeNumberOTResult"
																			value="#{pages$inspectionDetails.violationRecordSize}"
																			styleClass="RECORD_NUM_TD" />
																			</t:panelGrid>
																			</t:div>
																			<t:dataScroller id="scrollerResult" for="dtViolation"
																			paginator="true" fastStep="1"
																			paginatorMaxPages="#{pages$inspectionDetails.paginatorMaxPages}"
																			immediate="false" paginatorTableClass="paginator"
																			renderFacetsIfSinglePage="true"
																			pageIndexVar="pageNumber"
																			paginatorTableStyle="grid_paginator"
																			layout="singleTable"
																			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																			paginatorActiveColumnStyle="font-weight:bold;"
																			paginatorRenderLinkForActive="false"
																			styleClass="SCH_SCROLLER">
																			<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																			id="lblFResult"></t:graphicImage>
																			</f:facet>
																			<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																			id="lblFRResult"></t:graphicImage>
																			</f:facet>
																			<f:facet name="fastforward">
																			<t:graphicImage
																			url="../#{path.scroller_fastForward}" id="lblFFResult"></t:graphicImage>
																			</f:facet>
																			<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																					id="lblLResult"></t:graphicImage>
																			</f:facet>
																			<t:div styleClass="PAGE_NUM_BG">
																				
																				<table cellpadding="0" cellspacing="0">
																					<tr>
																						<td>
																							<h:outputText styleClass="PAGE_NUM"
																								value="#{msg['commons.page']}" />
																						</td>
																						<td>
																							<h:outputText styleClass="PAGE_NUM"
																								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																								value="#{requestScope.pageNumber}" />
																						</td>
																					</tr>
																				</table>

																			</t:div>
																		</t:dataScroller>
																			</t:panelGrid>
																		</t:div>
																	</t:div>
																</rich:tab>

															</rich:tabPanel>


														</div>
										<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT" /></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID" style="width:100%;" /></td>
										<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT" /></td>
										</tr>
									</table>
														<t:div styleClass="BUTTON_TD" style="padding-top:10px;">
															    <h:commandButton styleClass="BUTTON"  
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$inspectionDetails.btnAdd_Click}"
																	binding="#{pages$inspectionDetails.btnAdd}"
																	style="width: 75px">
																</h:commandButton>																
																<h:commandButton type="submit" styleClass="BUTTON"  
																	value="#{msg['inspection.btn.send']}"
																	action="#{pages$inspectionDetails.onSend}"
																	binding="#{pages$inspectionDetails.send}"
																	style="width: 75px">
																</h:commandButton>
																
																<h:commandButton type="submit" styleClass="BUTTON"  
																	value="#{msg['inspection.btn.SendForClosingViolation']}"
																	action="#{pages$inspectionDetails.onAssignViolationActions}"
																	binding="#{pages$inspectionDetails.assignViolationActions}"
																	style="width: 150px">
																</h:commandButton>
																
																<h:commandButton type="submit" styleClass="BUTTON"  
																	value="#{msg['inspection.btn.SendForRecommendation']}"
																	action="#{pages$inspectionDetails.onSendForRecommendation}"
																	binding="#{pages$inspectionDetails.sendForRecommendation}"
																	style="width: 150px">
																</h:commandButton>
																
																<h:commandButton type="submit" styleClass="BUTTON"  
																	value="#{msg['inspection.btn.SendForClosingViolation']}"
																	action="#{pages$inspectionDetails.onSendForClosingViolation}"
																	binding="#{pages$inspectionDetails.sendForClosingViolation}"
																	style="width: 150px">
																</h:commandButton>
																
																<h:commandButton type="submit" styleClass="BUTTON"  
																	value="#{msg['inspection.btn.SendForViolationAction']}"
																	action="#{pages$inspectionDetails.onSendForViolationAction}"
																	binding="#{pages$inspectionDetails.sendForViolationAction}"
																	style="width: 150px">
																</h:commandButton>
																
																<h:commandButton type="submit" styleClass="BUTTON"  
																	value="#{msg['inspection.btn.CloseViolation']}"
																	action="#{pages$inspectionDetails.onCloseViolation}"
																	binding="#{pages$inspectionDetails.closeViolation}"
																	style="width: 150px">
																</h:commandButton>
																
																
																<h:commandButton styleClass="BUTTON" style="width: 75px" 
																	type="submit" value="#{msg['commons.cancel']}"
																	action="#{pages$inspectionDetails.sendToInspectionSearch}">
																</h:commandButton>

														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>

				</table>
			</div>
		</body>
	</html>
</f:view>