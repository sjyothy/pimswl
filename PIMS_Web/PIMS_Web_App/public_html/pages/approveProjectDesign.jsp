<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript"> 

	 function showAddDesignNotePopup()
        {
              var screen_width = 1024;
              var screen_height = 450;
              var popup_width = screen_width-124;
              var popup_height = screen_height-80;
              var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;              
              var popup = window.open('addDesignNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,resizable=no,titlebar=no,dialog=yes');
              popup.focus();
        }
        
		function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
        
        function showAttachmentPopUp()
        { 
           var screen_width = screen.width;
           var screen_height = screen.height;
           window.open('Attachment.jsp','_blank','width='+(screen_width-500)+',height='+(screen_height-400)+',left=300,top=250,scrollbars=no,status=no');
       }
       
       function openProjectViewPopup() 
		{
			var screen_width = 1024;
			var screen_height = 450;
	        var popup_width = screen_width-50;
	        var popup_height = screen_height-10;
	        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
	        window.open('projectDetails.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
		}
		
		function showPersonPopup(personId)
		{
		   var screen_width = 1024;
           var screen_height = 450;
           var popup_width = screen_width-200;
           var popup_height = screen_height+20;
           var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		   window.open('AddPerson.jsf?personId='+personId+'&viewMode=popup&readOnlyMode=true','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		    
		}
		
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 <script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>


	
</head>
	<body class="BODY_STYLE">
	<div class="containerDiv">
	
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header --> 
	<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
		<tr style="height:84px;width:100%;#height:84px;#width:100%;">
			<td colspan="2">
				<jsp:include page="header.jsp" />
			</td>
		</tr>
		<tr width="100%">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99.2%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['approveProjectDesign.heading']}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>
				<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%">
					<tr valign="top">
								<td width="100%" valign="top" nowrap="nowrap">
						<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
							<div class="SCROLLABLE_SECTION" style="height:470px;width:100%;#height:470px;#width:100%;">
									<h:form id="detailsFrm" enctype="multipart/form-data" style="WIDTH: 96.6%;">
									
								<div> 
									<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
											<tr>
												<td>
													<h:outputText value="#{pages$approveProjectDesign.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$approveProjectDesign.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
													<div style="padding-left: 20px;width: 98%">

														<table cellpadding="1px" cellspacing="2px" width="100%">

															<tr>
																<td class="DET_SEC_TD">
																
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['approveProjectDesign.projectNumber']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																<h:panelGroup>
																	<h:inputText id="txtProjectNumber" styleClass="READONLY INPUT_TEXT" readonly="true"
																	 binding="#{pages$approveProjectDesign.htmlInputProjectNumber}" tabindex="1">
																	</h:inputText>
																	<h:outputLabel value="  "></h:outputLabel>
																	<h:commandLink actionListener="#{pages$approveProjectDesign.openProjectViewPopup}" rendered="#{pages$approveProjectDesign.consultantId != null}" title="#{msg['commons.view']}">
																   		<h:graphicImage title="#{msg['commons.view']}" url="../resources/images/app_icons/Lease-contract.png" />
																    </h:commandLink>
																</h:panelGroup>
																</td>
																<td class="DET_SEC_TD">
																
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['approveProjectDesign.consultantName']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:panelGroup>
																		<h:inputText id="txtConsultantName" styleClass="READONLY INPUT_TEXT" readonly="true"
																		binding="#{pages$approveProjectDesign.htmlConsultantName}"																																	
																			tabindex="1">
																		</h:inputText>
																		<h:outputLabel value="  "></h:outputLabel>
																		<h:commandLink actionListener="#{pages$approveProjectDesign.openConsultantViewPopup}" rendered="#{pages$approveProjectDesign.consultantId != null}" title="#{msg['commons.view']}">
																	   		<h:graphicImage title="#{msg['commons.view']}" url="../resources/images/app_icons/Lease-contract.png" />
																	    </h:commandLink>
																	</h:panelGroup>
																</td>
															</tr>

															<tr>
																<td class="DET_SEC_TD">
																<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
																	<h:outputLabel styleClass="LABEL" value="#{msg['approveProjectDesign.designType']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:selectOneMenu id="selectOneDesignType"
																	binding="#{pages$approveProjectDesign.htmlSelectOneDesignType}"
																		styleClass="SELECT_MENU"																	
																		tabindex="2">
																		<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.projectDesignType}" />
																	</h:selectOneMenu>
																</td>
																<td class="DET_SEC_TD">
																<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['approveProjectDesign.designStatus']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:selectOneMenu id="selectOneDesignStatus"
																	binding="#{pages$approveProjectDesign.htmlSelectOneDesignStatus}"
																		styleClass="SELECT_MENU"															
																		tabindex="3">
																		<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.projectDesignStatus}" />
																	</h:selectOneMenu>

																</td>
															</tr>
															<%System.out.println("Presented Date"); %>
															<tr>
																<td class="DET_SEC_TD">
																<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['approveProjectDesign.presentedDate']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<rich:calendar id="presentedDate"		
																	binding="#{pages$approveProjectDesign.htmlCalendarPresentedDate}"																
																		locale="#{pages$approveProjectDesign.currentLocale}" popup="true"
																		datePattern="#{pages$approveProjectDesign.shortDateFormat}"																	
																		showApplyButton="false" enableManualInput="false"
																		cellWidth="24px" cellHeight="22px" />
																</td>
																<td class="DET_SEC_TD">
																<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
																	<h:outputLabel styleClass="LABEL" value="#{msg['approveProjectDesign.presentedTo']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:selectOneMenu id="selectOnePresentedTo"
																	valueChangeListener="#{pages$approveProjectDesign.displayFieldForPresentedTo}"
																	binding="#{pages$approveProjectDesign.htmlSelectOnePresentedTo}"
																	onchange="javascript:submit();"	
																		styleClass="SELECT_MENU"												
																		tabindex="3">
																		<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.projectDesignPresentedTo}" />
																	</h:selectOneMenu>
																</td>
															</tr>
															<tr>
																<td class="DET_SEC_TD">
																</td>
																<td class="DET_SEC_TD">
																</td>
																<td class="DET_SEC_TD">
																<h:outputLabel 
																	binding="#{pages$approveProjectDesign.lblPresentedTo}"
																	styleClass="LABEL" value="#{msg['projectDesignApproval.presenteeName']}:">
																</h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="txtPresentedTo" styleClass="INPUT_TEXT" 
																			binding="#{pages$approveProjectDesign.htmlPresentedTo}">
																	</h:inputText>
																</td>
															</tr>

															<tr>
															<td class="BUTTON_TD" colspan="4">

																<table cellpadding="1px" cellspacing="1px">
																	<tr>
																	
																		<td colspan="2">
																			<h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" rendered="#{pages$approveProjectDesign.showSave}"
																				action="#{pages$approveProjectDesign.saveProjectDesign}"
																				tabindex="7">
																			</h:commandButton>
																			<h:commandButton styleClass="BUTTON" value="#{msg['commons.approveButton']}" rendered="#{pages$approveProjectDesign.showApprove}"
																				onclick="if (!confirm('#{msg['approveProjectDesign.messages.confirmApprove']}')) return false" 
																				action="#{pages$approveProjectDesign.approve}"
																				tabindex="8">
																			</h:commandButton>
																			<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}" 
																				onclick="if (!confirm('#{msg['commons.messages.cancelConfirm']}')) return false"
																				rendered="#{pages$approveProjectDesign.showApprove}"
																			 	action="#{pages$approveProjectDesign.cancel}" 
																			 	tabindex="9">
																			</h:commandButton> 	
																		 	<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}" 
																				onclick="if (!confirm('#{msg['commons.messages.backConfirm']}')) return false"
																				rendered="#{!pages$approveProjectDesign.showApprove}"
																			 	action="#{pages$approveProjectDesign.cancel}" 
																			 	tabindex="9">
																			</h:commandButton>
																		</td>
																	</tr>
																</table>
																</td>
															</tr>
															<tr>
															</tr>

														</table>
													</div>
													
													<div class="AUC_DET_PAD">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																id="approveDesignTabPanel" switchType="server"
																style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%">
																
																<rich:tab id="designNotesTab" label="#{msg['approveProjectDesign.tabs.designNotesTab']}">
																	<t:div style="width:100%;">																		
																			<t:div styleClass="BUTTON_TD">
																				<h:commandButton styleClass="BUTTON"
																				 	rendered="#{pages$approveProjectDesign.showGridActions}"
																					value="#{msg['approveProjectDesign.tabs.designNotesTab.buttons.addNote']}"
																					actionListener="#{pages$approveProjectDesign.openAddDesignNotePopup}"
																					binding="#{pages$approveProjectDesign.addDesignNote}"
																					style="width: 90px" />
																			</t:div>
																		
																		<t:div styleClass="contentDiv"
																			style="width:97.75%; height:220px; border-width:4px;">
																			<t:dataTable id="dtDesignNotes"
																				value="#{pages$approveProjectDesign.designNoteViewList}"
																				binding="#{pages$approveProjectDesign.dataTableDesignNotes}"
																				rows="10" preserveDataModel="false"
																				preserveSort="false" var="dataItem"
																				rowClasses="row1,row2" rules="all"
																				renderedIfEmpty="true" width="100%">

																				<t:column id="dateCol" width="70" sortable="true" defaultSorted="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.Date']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.noteDate}"
																						style="white-space: normal;" >
																						<f:convertDateTime
																						 locale="#{pages$approveProjectDesign.currentLocale}"
																							pattern="#{pages$approveProjectDesign.shortDateFormat}"/>
																					</t:outputText>
																				</t:column>
																				<t:column id="ownerColEn" width="70" sortable="true" rendered="#{pages$approveProjectDesign.isEnglishLocale}">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.Owner']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.noteOwnerEn}"
																						style="white-space: normal;" />
																				</t:column>
																				<t:column id="ownerColAr" width="70" sortable="true" rendered="#{pages$approveProjectDesign.isArabicLocale}">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.Owner']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.noteOwnerAr}"
																						style="white-space: normal;" />
																				</t:column>
																				<t:column id="typeColEn" width="50" sortable="true"  rendered="#{pages$approveProjectDesign.isEnglishLocale}">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.Type']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.noteTypeEn}"
																						style="white-space: normal;" />
																				</t:column>
																				<t:column id="typeColAr" width="50" sortable="true"  rendered="#{pages$approveProjectDesign.isArabicLocale}">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.Type']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.noteTypeAr}"
																						style="white-space: normal;" />
																				</t:column>
																				<t:column id="noteCol" width="150" sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.Note']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.note}"
																						style="white-space: normal;" />
																				</t:column>
																				<t:column id="recommendationCol" width="150"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.Recommendation']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.recommendation}"
																						style="white-space: normal;" />
																				</t:column>
																				<t:column id="closingDateCol" width="90"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.closingDate']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.closingDate}"
																						style="white-space: normal;">
																						<f:convertDateTime
																						 locale="#{pages$approveProjectDesign.currentLocale}"
																							pattern="#{pages$approveProjectDesign.shortDateFormat}"/>
																					</t:outputText>
																				</t:column>
																				<t:column id="closingnoteCol" width="150"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.closingNote']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.closingNote}"
																						style="white-space: normal;" />
																				</t:column>
																				<t:column id="statusColEn" width="50" sortable="true" rendered="#{pages$approveProjectDesign.isEnglishLocale}">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.Status']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.statusEn}"
																						style="white-space: normal;" />
																				</t:column>
																				<t:column id="statusColAr" width="50" sortable="true" rendered="#{pages$approveProjectDesign.isArabicLocale}">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.Status']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.statusAr}"
																						style="white-space: normal;" />
																				</t:column>
																				<t:column id="actionCol" sortable="false" width="50"
																					style="TEXT-ALIGN: center;" rendered="#{pages$approveProjectDesign.showGridActions}">
																					<f:facet name="header">
																						<t:outputText value="#{msg['commons.action']}" />
																					</f:facet>
																					<t:commandLink
																						actionListener="#{pages$approveProjectDesign.openAddDesignNotePopup}"																				>
																						<h:graphicImage id="editIcon"
																							title="#{msg['commons.edit']}"
																							url="../resources/images/app_icons/application_form_edit.png" />&nbsp;
																					</t:commandLink>
																					<t:commandLink
																						onclick="if (!confirm('#{msg['designNote.confirmDelete']}')) return"
																						action="#{pages$approveProjectDesign.deleteProjectDesignNote}"																						>
																						<h:graphicImage id="deleteIcon"
																							title="#{msg['commons.delete']}"
																							url="../resources/images/delete.gif" />&nbsp;
																					</t:commandLink>
																				</t:column>
																			</t:dataTable>
																		</t:div>
																	<t:div styleClass="contentDivFooter"
																			style="width:98.8%;">
																			<t:panelGrid cellpadding="0" cellspacing="0"
																				width="100%" columns="2"
																				columnClasses="RECORD_NUM_TD">

																				<t:div styleClass="RECORD_NUM_BG">
																					<t:panelGrid cellpadding="0" cellspacing="0"
																						columns="3" columnClasses="RECORD_NUM_TD">

																						<h:outputText
																							value="#{msg['commons.recordsFound']}" />

																						<h:outputText value=" : "
																							styleClass="RECORD_NUM_TD" />

																						<h:outputText
																							value="#{pages$approveProjectDesign.designNotesRecordSize}"
																							styleClass="RECORD_NUM_TD" />

																					</t:panelGrid>
																				</t:div>

																				<t:dataScroller id="dtDesignNotesScroller" for="dtDesignNotes"
																					paginator="true" fastStep="1" paginatorMaxPages="2"
																					immediate="false" paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true"
																					paginatorTableStyle="grid_paginator"
																					layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																					paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																					pageIndexVar="designNotesPageNumber">

																					<f:facet name="first">
																						<t:graphicImage
																							url="../resources/images/first_btn.gif"
																							id="designNoteslblF"></t:graphicImage>
																					</f:facet>
																					<f:facet name="fastrewind">
																						<t:graphicImage
																							url="../resources/images/previous_btn.gif"
																							id="designNoteslblFR"></t:graphicImage>
																					</f:facet>
																					<f:facet name="fastforward">
																						<t:graphicImage
																							url="../resources/images/next_btn.gif"
																							id="designNotesblFF"></t:graphicImage>
																					</f:facet>
																					<f:facet name="last">
																						<t:graphicImage
																							url="../resources/images/last_btn.gif"
																							id="designNoteslblL"></t:graphicImage>
																					</f:facet>
																					<t:div styleClass="PAGE_NUM_BG">
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.designNotesPageNumber}" />
																					</t:div>
																				</t:dataScroller>
																			</t:panelGrid>
																		</t:div>
																	</t:div>											
																</rich:tab>

																<rich:tab id="designHistoryTab" label="#{msg['approveProjectDesign.tabs.DesignHistoryTab']}">
																	<%-- 	<t:div styleClass="imag">&nbsp;</t:div> --%>
																	<t:div style="width:100%;">																		
																		<t:div styleClass="contentDiv"
																			style="width:97.75%; height:220px; border-width:4px;">
																			<t:dataTable id="dtdesignHistory"
																				binding="#{pages$approveProjectDesign.dataTableProjectDesign}"
																				value="#{pages$approveProjectDesign.projectDesignViewList}"
																				rows="10" preserveDataModel="false"
																				preserveSort="false" var="dataItem"
																				rowClasses="row1,row2" rules="all"
																				renderedIfEmpty="true" width="100%">

																				<t:column id="designTypeColEn" width="100" rendered="#{pages$approveProjectDesign.isEnglishLocale}"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.DesignHistoryTab.grid.designType']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.designTypeEn}"
																						style="white-space: normal;" />
																				</t:column>
																				<t:column id="designTypeColAr" width="100" rendered="#{pages$approveProjectDesign.isArabicLocale}"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.DesignHistoryTab.grid.designType']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.designTypeAr}"
																						style="white-space: normal;" />
																				</t:column>
																				<t:column id="presentedToColEn" width="250" rendered="#{pages$approveProjectDesign.isEnglishLocale}"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.DesignHistoryTab.grid.presentedTo']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.presentedToEn}"
																						style="white-space: normal;" />
																				</t:column>
																				<t:column id="presentedToColAr" width="250" rendered="#{pages$approveProjectDesign.isArabicLocale}"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.DesignHistoryTab.grid.presentedTo']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.presentedToAr}"
																						style="white-space: normal;" />
																				</t:column>
																				<%System.out.println("Grid Presented Date"); %>
																				<t:column id="presentedDateCol" width="100" 
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.DesignHistoryTab.grid.presentedDate']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.presentingDate}"
																						style="white-space: normal;" >
																						<f:convertDateTime pattern="#{pages$approveProjectDesign.shortDateFormat}" timeZone="#{pages$approveProjectDesign.timeZone}"/>
																					</t:outputText>
																						
																				</t:column>
																				<t:column id="statusColEn" width="150" rendered="#{pages$approveProjectDesign.isEnglishLocale}"
																				sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.DesignHistoryTab.grid.status']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.designStatusEn}"
																						style="white-space: normal;" />
																				</t:column>
																				<t:column id="statusColAr" width="150" rendered="#{pages$approveProjectDesign.isArabicLocale}"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText value="#{msg['approveProjectDesign.tabs.DesignHistoryTab.grid.status']}"
																							style="white-space: normal;" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItem.designStatusAr}"
																						style="white-space: normal;" />
																				</t:column>

																			</t:dataTable>
																		</t:div>
																		<t:div styleClass="contentDivFooter"
																			style="width:98.8%;">
																			<t:panelGrid cellpadding="0" cellspacing="0"
																				width="100%" columns="2"
																				columnClasses="RECORD_NUM_TD">

																				<t:div styleClass="RECORD_NUM_BG">
																					<t:panelGrid cellpadding="0" cellspacing="0"
																						columns="3" columnClasses="RECORD_NUM_TD">

																						<h:outputText
																							value="#{msg['commons.recordsFound']}" />

																						<h:outputText value=" : "
																							styleClass="RECORD_NUM_TD" />

																						<h:outputText
																							value="#{pages$approveProjectDesign.designHistoryRecordSize}"
																							styleClass="RECORD_NUM_TD" />

																					</t:panelGrid>
																				</t:div>

																				<t:dataScroller id="dtdesignHistoryScroller" for="dtdesignHistory"
																					paginator="true" fastStep="1" paginatorMaxPages="2"
																					immediate="false" paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true"
																					paginatorTableStyle="grid_paginator"
																					layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																					paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																					pageIndexVar="designHistoryPageNumber">

																					<f:facet name="first">
																						<t:graphicImage
																							url="../resources/images/first_btn.gif"
																							id="auctionUnitslblF"></t:graphicImage>
																					</f:facet>
																					<f:facet name="fastrewind">
																						<t:graphicImage
																							url="../resources/images/previous_btn.gif"
																							id="auctionUnitslblFR"></t:graphicImage>
																					</f:facet>
																					<f:facet name="fastforward">
																						<t:graphicImage
																							url="../resources/images/next_btn.gif"
																							id="auctionUnitsblFF"></t:graphicImage>
																					</f:facet>
																					<f:facet name="last">
																						<t:graphicImage
																							url="../resources/images/last_btn.gif"
																							id="auctionUnitslblL"></t:graphicImage>
																					</f:facet>
																					<t:div styleClass="PAGE_NUM_BG">
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.designHistoryPageNumber}" />
																					</t:div>
																				</t:dataScroller>
																			</t:panelGrid>
																		</t:div>
																	</t:div>																
																</rich:tab>

																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>
																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>
															</rich:tabPanel>

														</div>
														<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT" /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID" style="width:100%;" /></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
						
						</div>
								</div>
							</h:form>
					  </div> 
						</td>			
					</tr>
				</table>
			</td>
		</tr>
		  
		<tr style="height:10px;width:100%;#height:10px;#width:100%;">
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
	</table>
	</div>
</body>
</html>
</f:view>