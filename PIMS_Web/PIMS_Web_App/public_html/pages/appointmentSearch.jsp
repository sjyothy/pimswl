<%-- 
  - Author: Imran Ali
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Endowment Programs
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	    function resetValues()
      	{
      		document.getElementById("searchFrm:txtRequestId").value="";
      		document.getElementById("searchFrm:cmbAppointmentWith").selectedIndex=0;
      		
      		$('searchFrm:txtCreatedOnFromId').component.resetSelectedDate();
		    $('searchFrm:txtCreatedOnToId').component.resetSelectedDate();
		    
		    $('searchFrm:txtAppointmentDateFrom').component.resetSelectedDate();
		    $('searchFrm:txtAppointmentDateTo').component.resetSelectedDate();
		    
		    document.getElementById("searchFrm:txtDurationFrom").value="";
      	    document.getElementById("searchFrm:txtDurationTo").value="";
      	    
      	    document.getElementById("searchFrm:txtCreatedBy").value="";
      	    document.getElementById("searchFrm:txtEmail").value="";

      	    document.getElementById("searchFrm:txtPhone").value="";
      	    
      	    //document.getElementById("searchFrm:txtBudgetFrom").value="";
      	    //document.getElementById("searchFrm:txtBudgetTo").value="";
			
			
        }
        
        function closeWindow()
        {
        	window.opener.onMessageFromEndowmentPrograms();
        	window.close();
        
        }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<c:choose>
				<c:when test="${!pages$appointmentSearch.sViewModePopUp}">
					<div class="containerDiv">
				</c:when>
			</c:choose>

			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<c:choose>
					<c:when test="${!pages$appointmentSearch.sViewModePopUp}">
						<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>

					</c:when>
				</c:choose>



				<tr width="100%">
					<c:choose>
						<c:when test="${!pages$appointmentSearch.sViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>

						</c:when>
					</c:choose>



					<td width="83%" height="470px" valign="top"
						class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['appointmentSearch.title.search']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
									<div class="SCROLLABLE_SECTION AUC_SCH_SS"
										style="height: 95%; width: 100%; # height: 95%; # width: 100%;">
										<h:form id="searchFrm"
											style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
											<t:div id="layoutTable" styleClass="MESSAGE">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:messages></h:messages>
															<h:outputText id="errorMessage"
																value="#{pages$appointmentSearch.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</t:div>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">
																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['appointmentSearch.lbl.requestId']}"></h:outputLabel>
																		</td>

																		<td width="25%">
																			<h:inputText id="txtRequestId"
																				value="#{pages$appointmentSearch.criteria.requestNumber}"
																				maxlength="20"></h:inputText>

																		</td>
																		
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['appointmentSearch.lbl.appointmentWith']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:selectOneMenu id="cmbAppointmentWith"
																				value="#{pages$appointmentSearch.criteria.appointmentWith}"
																				tabindex="3">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.secUserList}" />
																			</h:selectOneMenu>
																		</td>
																		


																	</tr>

																	
																	<tr>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['appointmentSearch.lbl.createdOnFrom']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			
																			<rich:calendar id="txtCreatedOnFromId"
																				value="#{pages$appointmentSearch.criteria.createdOnFrom}"
																				locale="#{pages$appointmentSearch.locale}"
																				popup="true" datePattern="dd/MM/yyyy"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:170px; height:14px" />

																			

																		</td>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['appointmentSearch.lbl.createdOnTo']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<rich:calendar id="txtCreatedOnToId"
																				value="#{pages$appointmentSearch.criteria.createdOnTo}"
																				locale="#{pages$appointmentSearch.locale}"
																				popup="true"
																				datePattern="#{pages$appointmentSearch.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:185px;height:17px" />

																		</td>
																	</tr>
																		
																		
																		
																	<tr>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['appointmentSearch.lbl.appointmentDateFrom']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<rich:calendar id="txtAppointmentDateFrom"
																				value="#{pages$appointmentSearch.criteria.appointmentDateFrom}"
																				locale="#{pages$appointmentSearch.locale}"
																				popup="true"
																				datePattern="#{pages$appointmentSearch.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:185px;height:17px" />

																		</td>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['appointmentSearch.lbl.appointmentDateTo']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<rich:calendar id="txtAppointmentDateTo"
																				value="#{pages$appointmentSearch.criteria.appointmentDateTo}"
																				locale="#{pages$appointmentSearch.locale}"
																				popup="true"
																				datePattern="#{pages$appointmentSearch.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:185px;height:17px" />

																		</td>

																	</tr>


																	<tr>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['appointmentSearch.lbl.durationFrom']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:inputText id="txtDurationFrom"
																				value="#{pages$appointmentSearch.criteria.durationFrom}"
																				maxlength="20" onkeyup="removeNonNumeric(this)"
																				onkeypress="removeNonNumeric(this)"
																				onkeydown="removeNonNumeric(this)"
																				onblur="removeNonNumeric(this)"
																				onchange="removeNonNumeric(this)"></h:inputText>

																		</td>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['appointmentSearch.lbl.durationTo']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:inputText id="txtDurationTo"
																				value="#{pages$appointmentSearch.criteria.durationTo}"
																				maxlength="20" onkeyup="removeNonNumeric(this)"
																				onkeypress="removeNonNumeric(this)"
																				onkeydown="removeNonNumeric(this)"
																				onblur="removeNonNumeric(this)"
																				onchange="removeNonNumeric(this)"></h:inputText>

																		</td>

																	</tr>
																	<tr>

																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['appointmentSearch.lbl.createdBy']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:inputText id="txtCreatedBy"
																				value="#{pages$appointmentSearch.criteria.createdBy}"
																				maxlength="20"></h:inputText>
																		</td>
																		
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['appointmentSearch.lbl.email']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:inputText id="txtEmail"
																				value="#{pages$appointmentSearch.criteria.contactEmail}"
																				maxlength="20"></h:inputText>
																		</td>

																	</tr>
																	
																	<tr>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['appointmentSearch.lbl.phone']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:inputText id="txtPhone"
																				value="#{pages$appointmentSearch.criteria.contactNumber}"
																				maxlength="20"></h:inputText>
																		</td>
																	
																	</tr>






																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>

													</table>


												</div>
											</div>
											<div class="BUTTON_TD">
												<table>
													<tr>

														<td style="width: 100" class="BUTTON_TD" colspan="4">
															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.search']}"
																action="#{pages$appointmentSearch.onSearch}"
																style="width: 75px" />

															<h:commandButton styleClass="BUTTON" type="button"
																value="#{msg['commons.clear']}"
																onclick="javascript:resetValues();" style="width: 75px" />
														</td>
													</tr>
												</table>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" style="width: 100%; # width: 99%;">
													<t:dataTable id="dt1"
														value="#{pages$appointmentSearch.dataList}"
														binding="#{pages$appointmentSearch.dataTable}"
														rows="#{pages$appointmentSearch.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%">

														<t:column id="col2" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="requestNumber"
																	actionListener="#{pages$appointmentSearch.sort}"
																	value="#{msg['appointmentSearch.lbl.requestId']}"
																	arrow="true">
																	<f:attribute name="sortField" value="request.requestNumber" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.request.requestNumber}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														
														
														<t:column id="col3" width="10%" sortable="true"
															style="white-space: normal;">
															<f:facet name="header">
																<t:commandSortHeader columnName="createdOn"
																	actionListener="#{pages$appointmentSearch.sort}"
																	value="#{msg['commons.createdOn']}" arrow="true">
																	<f:attribute name="sortField" value="createdOn" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.createdOn}"
																style="white-space: normal;" styleClass="A_LEFT" >
																	
																<f:convertDateTime
																		pattern="#{pages$appointmentSearch.dateFormat}"
																		timeZone="#{pages$appointmentSearch.timeZone}" />
																
																</t:outputText>
														</t:column>
														
														<t:column id="col4" width="10%" sortable="true"
															style="white-space: normal;">
															<f:facet name="header">
																<t:commandSortHeader columnName="appointmentWith"
																	actionListener="#{pages$appointmentSearch.sort}"
																	value="#{msg['appointmentSearch.lbl.appointmentWith']}" arrow="true">
																	<f:attribute name="sortField" value="aw.fullName" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.appointmentWith.fullName}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														
														<t:column id="col11" width="10%" sortable="true"
															style="white-space: normal;">
															<f:facet name="header">
																<t:commandSortHeader columnName="appointmentDate"
																	actionListener="#{pages$appointmentSearch.sort}"
																	value="#{msg['appointmentSearch.lbl.appointmentDate']}" arrow="true">
																	<f:attribute name="sortField" value="appointmentDate" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.appointmentDate}"
																style="white-space: normal;" styleClass="A_LEFT" >
																	<f:convertDateTime
																		pattern="MM/dd/yyyy hh:mm"
																		timeZone="#{pages$appointmentSearch.timeZone}" />
																
																</t:outputText>
														</t:column>
														
														<t:column id="col5" width="10%" sortable="true"
															style="white-space: normal;">
															<f:facet name="header">
																<t:commandSortHeader columnName="duration"
																	actionListener="#{pages$appointmentSearch.sort}"
																	value="#{msg['appointmentSearch.lbl.duration']}" arrow="true">
																	<f:attribute name="sortField" value="duration" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.duration}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														
														<t:column id="col6" width="10%" 
															style="white-space: normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['appointmentSearch.lbl.appointmentRequestedBy']}" />
															</f:facet>
															
															<t:outputText value="#{dataItem.appointmentRequestedBy.fullName}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column id="col7" width="10%" 
															style="white-space: normal;">
															
															
															<f:facet name="header">
																<t:outputText value="#{msg['appointmentSearch.lbl.contactInfo']}"/>
															</f:facet>
															
															<t:outputText value="#{dataItem.contactInfo}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="col8" width="10%" sortable="true"
															style="white-space: normal;">
															
															
															<f:facet name="header">
																<t:commandSortHeader columnName="title"
																	actionListener="#{pages$appointmentSearch.sort}"
																	value="#{msg['appointmentSearch.lbl.appointmentTitle']}" arrow="true">
																	<f:attribute name="sortField" value="title" />
																</t:commandSortHeader>
															</f:facet>
															
															<t:outputText value="#{dataItem.title}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
													
													
														<t:column id="actions">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>

															<t:commandLink
																rendered="#{!pages$appointmentSearch.pageModeSelectOnePopUp && 
																			!pages$appointmentSearch.pageModeSelectManyPopUp
																 			}"
																action="#{pages$appointmentSearch.onEdit}">

																<h:graphicImage title="#{msg['commons.edit']}"
																	url="../resources/images/edit-icon.gif" />
															</t:commandLink>

															
														</t:column>
															
			
													</t:dataTable>
												</div>
												<t:div id="contentDivFooter"
													styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:100%;#width:100%;">
													<table id="RECORD_NUM_TD" cellpadding="0" cellspacing="0"
														width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$appointmentSearch.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td id="contentDivFooterColumnTwo" class="BUTTON_TD"
																style="width: 53%; # width: 50%;" align="right">

																<t:div styleClass="PAGE_NUM_BG" style="#width:20%">

																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																			</td>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{pages$appointmentSearch.currentPage}" />
																			</td>
																		</tr>
																	</table>
																</t:div>
															<TABLE border="0" class="SCH_SCROLLER">
																	<tr>
																		<td>
																			<t:commandLink
																				action="#{pages$appointmentSearch.pageFirst}"
																				disabled="#{pages$appointmentSearch.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$appointmentSearch.pagePrevious}"
																				disabled="#{pages$appointmentSearch.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFR"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:dataList value="#{pages$appointmentSearch.pages}"
																				var="page">
																				<h:commandLink value="#{page}"
																					actionListener="#{pages$appointmentSearch.page}"
																					rendered="#{page != pages$appointmentSearch.currentPage}" />
																				<h:outputText value="<b>#{page}</b>" escape="false"
																					rendered="#{page == pages$appointmentSearch.currentPage}" />
																			</t:dataList>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$appointmentSearch.pageNext}"
																				disabled="#{pages$appointmentSearch.firstRow + pages$appointmentSearch.rowsPerPage >= pages$appointmentSearch.totalRows}">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$appointmentSearch.pageLast}"
																				disabled="#{pages$appointmentSearch.firstRow + pages$appointmentSearch.rowsPerPage >= pages$appointmentSearch.totalRows}">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblL"></t:graphicImage>
																			</t:commandLink>

																		</td>
																	</tr>
																</TABLE>
															</td>
														</tr>
													</table>
												</t:div>
											</div>

										</h:form>

									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">

					<td colspan="2">
						<c:choose>
							<c:when test="${!pages$appointmentSearch.sViewModePopUp}">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</tr>
								</table>

							</c:when>
						</c:choose>

					</td>
				</tr>

			</table>

			<c:choose>
				<c:when test="${!pages$appointmentSearch.sViewModePopUp}">
					</div>
				</c:when>
			</c:choose>
		</body>
	</html>
</f:view>