
<script language="JavaScript" type="text/javascript">	
</script>
<a4j:commandLink id="onClickMarkUnMark"
	reRender="participantFields,participantContentDiv,participantListsDataTable,participantListDataTableFooter"
	onbeforedomupdate="javaScript:onRemoveDisablingDiv(); "
	action="#{pages$programParticipants.onMarkUnMarkChanged}" />
<a4j:commandLink id="onParticipantTypeChange"
	onbeforedomupdate="javascript:onRemoveDisablingDiv();"
	action="#{pages$programParticipants.onParticipantTypeChange}"
	reRender="participantFields,txtExternalParticipantName,addSearchPersonButtonTable,btnAddInternalParticipants,participantListGrdActions,btnAddParticipant" />
<a4j:commandLink id="onParticipantWinner"
	reRender="hiddenFields,successmsg,participantContentDiv,participantListsDataTable,participantListDataTableFooter,btnAbsent,btnPresent"
	onbeforedomupdate="javaScript:onRemoveDisablingDiv(); "
	action="#{pages$programParticipants.onSetAsWinners}" />

<t:panelGrid id="addSearchPersonButtonTable" styleClass="BUTTON_TD"
	cellpadding="1px" width="100%" cellspacing="5px">
	<h:panelGroup>
		<h:commandButton id="btnAddInternalParticipants" styleClass="BUTTON"
			value="#{msg['socialProgram.btn.addInternalParticipant']}"
			action="#{pages$programParticipants.onAddInternalParticipants}"
			binding="#{pages$programParticipants.btnAddInternalParticipants}" />
	</h:panelGroup>
</t:panelGrid>
<t:panelGrid id="participantFields"
	styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%"
	cellspacing="5px" columns="4">

	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['socialProgram.lbl.participantType']}" />
	</h:panelGroup>
	<h:selectOneMenu id="selectParticipantType"
		value="#{pages$programParticipants.programParticipants.type}"
		onchange="javaScript:onParticipantTypeSelectionChanged(this);">
		<f:selectItem itemValue="-1"
			itemLabel="#{msg['commons.combo.PleaseSelect']}" />
		<f:selectItem itemValue="1"
			itemLabel="#{msg['socialProgram.participant.internal']}" />
		<f:selectItem itemValue="2"
			itemLabel="#{msg['socialProgram.participant.external']}" />

	</h:selectOneMenu>
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['socialProgram.lbl.ExternalParticipantName']}" />
	</h:panelGroup>
	<h:inputText id="externalParticipantName"
		value="#{pages$programParticipants.programParticipants.externalParticipantName}"
		binding="#{pages$programParticipants.txtExternalParticipantName}" />
</t:panelGrid>
<t:panelGrid id="participantListGrdActions" styleClass="BUTTON_TD"
	cellpadding="1px" width="100%" cellspacing="5px">
	<h:panelGroup>
		<h:commandButton  id = "btnAddParticipant" styleClass="BUTTON"
			value="#{msg['commons.saveButton']}"
			action="#{pages$programParticipants.onSave}" 
			binding="#{pages$programParticipants.btnAddParticipant}"/>
	</h:panelGroup>
</t:panelGrid>


<t:div id="participantContentDiv" styleClass="contentDiv"
	style="width:98%">
	<t:dataTable id="participantListsDataTable"
		rows="#{pages$programParticipants.paginatorRows}"
		value="#{pages$programParticipants.list}"
		binding="#{pages$programParticipants.dataTable}"
		preserveDataModel="false" preserveSort="false" var="dataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">


		<t:column id="selectParticiapnt" style="width:2%;">
			<f:facet name="header">
				<h:selectBooleanCheckbox id="markUnMarkAll"
					onclick="onParticipantChkChangeStart(this);"
					value="#{pages$programParticipants.markUnMarkAll}"
					rendered="#{dataItem.isDeleted != '1' }" />
			</f:facet>
			<h:selectBooleanCheckbox id="select" value="#{dataItem.seleted}"
				rendered="#{dataItem.isDeleted != '1' }" />
		</t:column>
		<t:column id="participantListtypeColumn" width="15%">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.lbl.participantType']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				rendered="#{dataItem.isDeleted != '1' }"
				value="#{dataItem.type=='1'? msg['socialProgram.participant.internal']:msg['socialProgram.participant.external']}" />
		</t:column>

		<t:column id="participantListName" width="15%">
			<f:facet name="header">
				<t:outputText
					value="#{msg['socialProgram.gridlbl.participantName']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				rendered="#{dataItem.isDeleted != '1'}"
				value="#{dataItem.type == '1'? dataItem.internalPersonName:dataItem.externalParticipantName}" />
		</t:column>
		<t:column id="participantListAttendance" width="15%">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.gridlbl.attendance']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				rendered="#{dataItem.isDeleted != '1' }"
				value="#{dataItem.isPresent == '1'? msg['socialProgram.gridlbl.present']:msg['socialProgram.gridlbl.absent']}" />
		</t:column>
		<t:column id="participantListIsWinner" width="15%">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.gridlbl.isWinner']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				rendered="#{dataItem.isDeleted != '1' }"
				value="#{dataItem.isWinner == '1'? msg['commons.yes']:msg['commons.no']}" />
		</t:column>
		<t:column id="participantListWinnerComments" width="15%">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.comments']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				rendered="#{dataItem.isDeleted != '1' }"
				value="#{dataItem.winnerComments}" />
		</t:column>
		<t:column id="participantListAction">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.action']}" />
			</f:facet>
			<h:commandLink
				rendered="#{dataItem.isPresent == '1' && dataItem.isWinner != '1'  && dataItem.isDeleted != '1' }"
				onclick="if (!confirm('#{msg['socialProgram.messages.confirmWinner']}')) return false;"
				action="#{pages$programParticipants.onWinnerClick}">
				<h:graphicImage id="selectWinnerIcon"
					title="#{msg['socialProgram.title.winner']}"
					url="../resources/images/app_icons/Dclare-Auction-Winner.png" />
			</h:commandLink>
			<h:graphicImage id="imgDeleteParticipant"
				rendered="#{dataItem.isDeleted != '1' }"
				title="#{msg['commons.delete']}"
				url="../resources/images/delete_icon.png"
				onclick="if (!confirm('#{msg['commons.confirmDelete']}')) return false; else onDeleted(this);">
			</h:graphicImage>
			<a4j:commandLink id="onParticipantDeleted"
				reRender="hiddenFields,participantContentDiv,participantListsDataTable,participantListDataTableFooter,absentPresentWinnerButtonTable,btnAbsent,btnPresent"
				onbeforedomupdate="javaScript:onRemoveDisablingDiv(); "
				action="#{pages$programParticipants.onDeleteParticipants}" />


		</t:column>
	</t:dataTable>
</t:div>
<t:div id="participantListDataTableFooter" styleClass="contentDivFooter"
	style="width:99%">
	<t:panelGrid id="participantListfooterTable" columns="2"
		cellpadding="0" cellspacing="0" width="100%"
		columnClasses="RECORD_NUM_TD,BUTTON_TD">


		<CENTER>
			<t:dataScroller id="participantListscroller"
				for="participantListsDataTable" paginator="true" fastStep="1"
				paginatorMaxPages="#{pages$programParticipants.paginatorMaxPages}"
				immediate="false" paginatorTableClass="paginator"
				renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
				styleClass="SCH_SCROLLER"
				paginatorActiveColumnStyle="font-weight:bold;"
				paginatorRenderLinkForActive="false"
				paginatorTableStyle="grid_paginator" layout="singleTable"
				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;">

				<f:facet name="first">
					<t:graphicImage url="../#{path.scroller_first}"
						id="lblparticipantListFirst"></t:graphicImage>
				</f:facet>

				<f:facet name="fastrewind">
					<t:graphicImage url="../#{path.scroller_fastRewind}"
						id="lblparticipantListF"></t:graphicImage>
				</f:facet>

				<f:facet name="fastforward">
					<t:graphicImage url="../#{path.scroller_fastForward}"
						id="lblparticipantListFF"></t:graphicImage>
				</f:facet>

				<f:facet name="last">
					<t:graphicImage url="../#{path.scroller_last}"
						id="lblparticipantListL"></t:graphicImage>
				</f:facet>


			</t:dataScroller>
		</CENTER>
	</t:panelGrid>
</t:div>
<t:panelGrid id="absentPresentWinnerButtonTable" styleClass="BUTTON_TD"
	cellpadding="1px" width="100%" cellspacing="5px">
	<h:panelGroup>
		<h:commandButton id="btnPresent" styleClass="BUTTON"
			onclick="if (!confirm('#{msg['socialProgram.messages.confirmPresent']}')) return false"
			value="#{msg['socialProgram.btn.present']}"
			action="#{pages$programParticipants.onSetAsPresent}"
			binding="#{pages$programParticipants.btnPresent}" />
		<h:commandButton id="btnAbsent" styleClass="BUTTON"
			onclick="if (!confirm('#{msg['socialProgram.messages.confirmAbsent']}')) return false"
			value="#{msg['socialProgram.btn.Absent']}"
			action="#{pages$programParticipants.onSetAsAbsent}"
			binding="#{pages$programParticipants.btnAbsent}" />
	</h:panelGroup>
</t:panelGrid>
