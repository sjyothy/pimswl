<%-- 
  - Author: Kamran Ahmed
  - Date:
  - Copyright Notice:
  - @(#)\
  - Description: This report will show Tenant details matching the following criteria provided as an input.
  --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%-- 
		<% UIViewRoot viewRoot = (UIViewRoot)session.getAttribute("view");
           if (viewRoot.getLocale().toString().equals("en"))
           { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>
        <%
            } 
            else
            { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_ar.js"></script>
        <%
            } 
        %>
--%>

<script type="text/javascript">

	function openLoadReportPopup(pageName) 
	{	
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />


			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
				 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

			

		
		</head>

		<body>
				<div style="width:1024px;">
			

			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
						   
							<td colspan="2">
						        <jsp:include page="header.jsp"/>
						     </td>
						     
							
		       </tr>
		       <tr width="100%">						
						<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
						</td>					    
						<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['report.tenantDetailsReport']}"	styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
								>
								</td>
								<td width="100%" height="99%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:450px;">
										<h:form id="searchFrm">
											
											<div class="MARGIN" style="width:96%">
												<table cellpadding="0" cellspacing="0" style="width:99.6%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
													<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>
												<div class="DETAIL_SECTION"  style="width:100%">
													<h:outputLabel value="#{msg['commons.report.criteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="0px" cellspacing="0px"
														class="DETAIL_SECTION_INNER">
													<tr>
														<td width="97%">
														<table width="100%">
														
														<tr>
														<td width="25%">
														<h:outputLabel styleClass="LABEL" value="#{msg['tenants.id']} :"/></td>
														<td width="25%">
														<h:inputText id="txtTenantId" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.tenantId}"  maxlength="20"></h:inputText></td>
														<td width="25%">
														<h:outputLabel styleClass="LABEL" value="#{msg['tenants.nationalId']} :"/></td>
														<td width="25%">
														<h:inputText id="txtNationalId" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.nationalId}"  maxlength="20"></h:inputText></td>
														</tr>
														<tr>
													<td><h:outputLabel styleClass="LABEL" value="#{msg['tenants.type']} :"></h:outputLabel></td>
																		<td><h:selectOneMenu id="cboTenantType"  required="false" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.tenantType}">

																				<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																				<f:selectItems value="#{pages$ApplicationBean.tenantType}" />
																			</h:selectOneMenu></td>
														<td><h:outputLabel styleClass="LABEL" value="#{msg['chequeList.tenantNo']} :"/></td>
														<td><h:inputText id="txtTenantNo" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.tenantNo}"  maxlength="20"></h:inputText></td>
														</tr>
														<tr>
															
																		<td >

																			
																		<h:outputLabel styleClass="LABEL" value="#{msg['tenants.name']} :"></h:outputLabel></td>

																		<td  >
																			
																			<h:inputText id="txtTenantName" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.tenantName}"  maxlength="20"></h:inputText></td>	
																				<td    >


																		
																		<h:outputLabel styleClass="LABEL" value="#{msg['tenants.nationality']} :"></h:outputLabel></td>
																		<td   >


																		
																		<h:selectOneMenu id="cboNationality"  required="false" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.nationality}">

																				<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																				<f:selectItems value="#{pages$ApplicationBean.countryList}" />
																			</h:selectOneMenu></td>
																		
																	</tr>
																	<tr>

																		<td  >
																			
																		<h:outputLabel styleClass="LABEL" value="#{msg['tenants.passportNumber']} :" /></td>

																		<td  >


																			

																		<h:inputText id="txtPassportNumber" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.passportNumber}"  maxlength="20"></h:inputText></td>
																		

																		<td  >


																		<h:outputLabel styleClass="LABEL" value="#{msg['tenants.visa.number']} :"></h:outputLabel></td>
																	<td  >


																		<h:inputText id="txtVisaNumber" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.visaNumber}"  maxlength="20"></h:inputText></td>
																	</tr>
																	<tr>
																		
																		

																		
																		

																	</tr>

																	<tr>

																		<td  >
																			

																		<h:outputLabel styleClass="LABEL" value="#{msg['tenants.licenseNumber']} :" /></td>
																		<td  >
																			

																		<h:inputText id="txtLicenseNumber" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.licenseNumber}"  maxlength="20"></h:inputText></td>

																		<td  >

																		<h:outputLabel styleClass="LABEL" value="#{msg['tenants.licenseSource']} :"></h:outputLabel></td>
																		<td  >
																		<h:selectOneMenu id="txtLicenseSource" 
																			 
																			required="false" 
																			value="#{pages$crTenantDetails.tenantDetailsReportCriteria.licenseSource}">

																				<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}"/>
																				<f:selectItems value="#{pages$ApplicationBean.licenseSourceList}"/>
																			</h:selectOneMenu>
																		</td>

																	</tr>


																	<tr>

																		<td  >

																			
																		<h:outputLabel styleClass="LABEL" value="#{msg['tenants.mobileNumber']} :" /></td>
																		<td  >
																			
																		<h:inputText id="txtMobileNumber" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.mobileNumber}"  maxlength="20"></h:inputText></td>


																		<td  >


																		<h:outputLabel styleClass="LABEL" value="#{msg['tenants.phoneNumber']} :"></h:outputLabel></td>
																		<td  >

																		<h:inputText id="txtPhoneNumber" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.phoneNumber}"  maxlength="20"></h:inputText></td>

																	</tr>
																	<tr>
																		<td  >
																	

																			
																		<h:outputLabel styleClass="LABEL" value="#{msg['property.name']} :" /></td>
																		<td  >
																			

																		<h:inputText id="txtPropertyName" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.propertyName}"  maxlength="20"></h:inputText></td>

																		<td  >

																		<h:outputLabel styleClass="LABEL" value="#{msg['property.ownership']}"> Type :</h:outputLabel></td>
																		<td   >

																		<h:selectOneMenu id="cboPropertyOwnershipType"  required="false" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.propertyOwnershipType}">

																				<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																				<f:selectItems value="#{pages$ApplicationBean.propertyOwnershipType}" />
																			</h:selectOneMenu></td>

																	</tr>
																	<tr>
																	<td><h:outputLabel styleClass="LABEL" value="#{msg['contract.startDateFrom']} :"></h:outputLabel></td>
																	<td><rich:calendar id="clndrCnrtctStartDateFrom" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.contractStartDateFrom}" popup="true" datePattern="#{pages$crTenantDetails.dateFormat}" showApplyButton="false" locale="#{pages$crTenantDetails.locale}" inputStyle="width: 170px; height: 14px" enableManualInput="false" cellWidth="24px" cellHeight="22px">
																		</rich:calendar></td>
																	<td><h:outputLabel styleClass="LABEL" value="#{msg['contract.startDateTo']} :"></h:outputLabel></td>
																	<td><rich:calendar id="clndrCnrtctStartDateTo" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.contractStartDateTo}" popup="true" datePattern="#{pages$crTenantDetails.dateFormat}" showApplyButton="false" locale="#{pages$crTenantDetails.locale}" inputStyle="width: 170px; height: 14px" enableManualInput="false" cellWidth="24px" cellHeight="22px">
																		</rich:calendar></td>
																	</tr>
																	<tr>
																	<td><h:outputLabel styleClass="LABEL" value="#{msg['date.expiryDateFrom']} :"></h:outputLabel></td>
																	<td><rich:calendar id="clndrExpiryDateFrom" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.expiryDateFrom}" popup="true" datePattern="#{pages$crTenantDetails.dateFormat}" showApplyButton="false" locale="#{pages$crTenantDetails.locale}" inputStyle="width: 170px; height: 14px" enableManualInput="false">
																		</rich:calendar></td>
																	<td><h:outputLabel styleClass="LABEL" value="#{msg['date.expiryDateTo']} :" /></td>
																	<td><rich:calendar id="clndrExpiryDateTo" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.expiryDateTo}" popup="true" datePattern="#{pages$crTenantDetails.dateFormat}" showApplyButton="false" locale="#{pages$crTenantDetails.locale}" inputStyle="width: 170px; height: 14px" enableManualInput="false" cellWidth="24px" cellHeight="22px">
																		</rich:calendar></td>
																	</tr>

																	
																	<tr>
																	
																	
																	
																	
																	</tr>
																	<tr>
																	<td><h:outputLabel styleClass="LABEL" value="#{msg['notificationGenerate.contractNumber']} :"></h:outputLabel></td>
																	<td><h:inputText id="txtContractNumber" value="#{pages$crTenantDetails.tenantDetailsReportCriteria.contractNumber}"  maxlength="20"></h:inputText></td>
																		<td></td>
																		<td></td>
																	</tr>
																	
																	
																	
																	
																	
																	
																	</tr>
																	
																	<tr>
																<td class="BUTTON_TD" colspan="4">
																			<h:commandButton styleClass="BUTTON"
																						id="submitButton"
																						action="#{pages$crTenantDetails.cmdView_Click}"
																						immediate="false" value="#{msg['commons.view']}"
																						 tabindex="7"></h:commandButton>
														</td>
														</tr>
												</table>
												</td>
													<td width="3%">
														&nbsp;
													</td>
												</tr>
												
												</table>
															
										
												</div>
											</div>
											
											
											
										</h:form>
									</div>
								</td>
							</tr>
						</table>
                       </td>
				</tr>
				<tr>
						    <td colspan="2">
						    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
						       		<tr><td class="footer"><h:outputLabel value= "#{msg['commons.footer.message']}" />
						       				</td></tr>
					    		</table>
					        </td>
					    </tr> 
				</table>
				</div>
		</body>
	</html>
</f:view>
