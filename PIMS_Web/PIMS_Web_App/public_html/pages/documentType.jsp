<%-- 
  - Author: Imran Ali
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used to Manage Docuemnt Type
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	    function resetValues()
      	{
		    document.getElementById("searchFrm:txtDescEn").value="";
		    document.getElementById("searchFrm:txtDescAr").value="";
		    document.getElementById("searchFrm:txtFileNetId").value="";
		    
			
        }
        
        function closeWindow()
        {
        	window.opener.onMessageFromEndowmentPrograms();
        	window.close();
        
        }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<c:choose>
				<c:when test="${!pages$documentType.sViewModePopUp}">
					<div class="containerDiv">
				</c:when>
			</c:choose>

			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<c:choose>
					<c:when test="${!pages$documentType.sViewModePopUp}">
						<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>

					</c:when>
				</c:choose>



				<tr width="100%">
					<c:choose>
						<c:when test="${!pages$documentType.sViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>

						</c:when>
					</c:choose>



					<td width="83%" height="470px" valign="top"
						class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel
										value="#{msg['documentType.lbl.title.documentType']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
									<div class="SCROLLABLE_SECTION AUC_SCH_SS"
										style="height: 95%; width: 100%; # height: 95%; # width: 100%;">
										<h:form id="searchFrm"
											style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
											<t:div id="layoutTable" styleClass="MESSAGE">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:messages></h:messages>
															<h:outputText id="errorMessage"
																value="#{pages$documentType.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText id="successMessages"
																value="#{pages$documentType.successMessages}"
																escape="false" styleClass="INFO_FONT" />
														</td>
													</tr>
												</table>
											</t:div>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">
																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['documentType.lbl.descEn']}"></h:outputLabel>
																			<h:outputLabel styleClass="mandatory" value="*" />
																		</td>

																		<td width="25%">
																			<h:inputText id="txtDescEn"
																				value="#{pages$documentType.documentTypeView.descriptionEn}"
																				maxlength="20"></h:inputText>

																		</td>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['documentType.lbl.descAr']}"></h:outputLabel>
																			<h:outputLabel styleClass="mandatory" value="*" />
																		</td>

																		<td width="25%">
																			<h:inputText id="txtDescAr"
																				value="#{pages$documentType.documentTypeView.descriptionAr}"
																				maxlength="20"></h:inputText>

																		</td>

																	</tr>

																	<tr>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['documentType.lbl.fileNetTypeId']}:"></h:outputLabel>
																			<h:outputLabel styleClass="mandatory" value="*" />
																		</td>
																		<td width="25%" colspan="1">
																			<h:inputText id="txtFileNetId"
																				value="#{pages$documentType.documentTypeView.fileNetTypeId}"
																				maxlength="20" onkeyup="removeNonNumeric(this)"
																				onkeypress="removeNonNumeric(this)"
																				onkeydown="removeNonNumeric(this)"
																				onblur="removeNonNumeric(this)"
																				onchange="removeNonNumeric(this)"></h:inputText>

																		</td>
																	</tr>


																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>

													</table>


												</div>
											</div>
											<div class="BUTTON_TD">
												<table>
													<tr>

														<td style="width: 100" class="BUTTON_TD" colspan="4">
															<h:commandButton styleClass="BUTTON"
																value="#{msg['benef.asset.shares.save']}"
																action="#{pages$documentType.onAdd}" style="width: 75px" />

															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.clear']}"
																action="#{pages$documentType.onClear}"
																style="width: 75px" />

														</td>
													</tr>
												</table>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" style="width: 100%; # width: 99%;">


													<t:dataTable width="100%"
														rows="#{pages$docTypeList.paginatorRows}"
														value="#{pages$documentType.docTypeList}"
														binding="#{pages$documentType.dataTable}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true">

														<t:column sortable="true" width="20%">
															<f:facet name="header">
																<t:commandSortHeader columnName="descriptionEn"
																	value="#{msg['documentType.lbl.descEn']}" arrow="true">
																	<f:attribute name="sortField" value="descriptionEn" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.descriptionEn}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column sortable="true" width="20%">
															<f:facet name="header">
																<t:commandSortHeader columnName="descriptionAr"
																	value="#{msg['documentType.lbl.descAr']}" arrow="true">
																	<f:attribute name="sortField" value="descriptionAr" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.descriptionAr}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column sortable="true" width="20%">
															<f:facet name="header">
																<t:commandSortHeader columnName="fileNetId"
																	value="#{msg['documentType.lbl.fileNetTypeId']}"
																	arrow="true">
																	<f:attribute name="sortField" value="fileNetTypeId" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.fileNetTypeId}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>


														<t:column id="action" sortable="true" width="10%">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>
															<h:commandLink action="#{pages$documentType.onEdit}"
																rendered="#{!pages$documentType.documentTypeView.showEdit}">

																<h:graphicImage style="cursor:hand;"
																	title="#{msg['commons.edit']}"
																	url="../resources/images/edit-icon.gif" width="18px;" />
															</h:commandLink>

														</t:column>


													</t:dataTable>
												</div>
												<t:div id="contentDivFooter"
													styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:100%;#width:100%;">
													<table id="RECORD_NUM_TD" cellpadding="0" cellspacing="0"
														width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$documentType.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td id="contentDivFooterColumnTwo" class="BUTTON_TD"
																style="width: 53%; # width: 50%;" align="right">

																<t:div styleClass="PAGE_NUM_BG" style="#width:20%">

																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																			</td>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{pages$documentType.currentPage}" />
																			</td>
																		</tr>
																	</table>
																</t:div>
																<TABLE border="0" class="SCH_SCROLLER">
																	<tr>
																		<td>
																			<t:commandLink
																				action="#{pages$documentType.pageFirst}"
																				disabled="#{pages$documentType.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$documentType.pagePrevious}"
																				disabled="#{pages$documentType.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFR"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:dataList value="#{pages$documentType.pages}"
																				var="page">
																				<h:commandLink value="#{page}"
																					actionListener="#{pages$documentType.page}"
																					rendered="#{page != pages$documentType.currentPage}" />
																				<h:outputText value="<b>#{page}</b>" escape="false"
																					rendered="#{page == pages$documentType.currentPage}" />
																			</t:dataList>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$documentType.pageNext}"
																				disabled="#{pages$documentType.firstRow + pages$documentType.rowsPerPage >= pages$documentType.totalRows}">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$documentType.pageLast}"
																				disabled="#{pages$documentType.firstRow + pages$documentType.rowsPerPage >= pages$documentType.totalRows}">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblL"></t:graphicImage>
																			</t:commandLink>

																		</td>


																	</tr>
																</TABLE>
															</td>
														</tr>
													</table>
												</t:div>
											</div>

										</h:form>

									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">

					<td colspan="2">
						<c:choose>
							<c:when test="${!pages$documentType.sViewModePopUp}">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</tr>
								</table>

							</c:when>
						</c:choose>

					</td>
				</tr>

			</table>

			<c:choose>
				<c:when test="${!pages$documentType.sViewModePopUp}">
					</div>
				</c:when>
			</c:choose>
		</body>
	</html>
</f:view>