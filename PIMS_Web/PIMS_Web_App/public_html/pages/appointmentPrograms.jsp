<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++)
		{
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		       )
		    {
		        inputs[i].disabled = true;
		    }
		}
	}
		
	function onMessageFromSearchEndowments()
	{
		document.getElementById("detailsFrm:onMessageFromSearchEndowments").onclick();
	
	}
	function onMessageFromSearchDonationBox()
	{
	  
	  disableInputs();
	  document.getElementById("detailsFrm:onMessageFromSearchDonationBox").onclick();
	}
	
	function   openSearchBoxPopup()
	{
		
	   var screen_width   = 0.65 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   window.open('searchDonationBox.jsf?pageMode=MODE_SELECT_MANY_POPUP','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	    
	}
	

	
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<c:choose>
						<c:when test="${!pages$endowmentPrograms.viewModePopup}">
							<tr
								style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>


					<tr width="100%">
						<c:choose>
							<c:when test="${!pages$endowmentPrograms.viewModePopup}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{pages$endowmentPrograms.pageTitle}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>
																<h:outputText id="errorId"
																	value="#{pages$endowmentPrograms.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$endowmentPrograms.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$endowmentPrograms.pageMode}"></h:inputHidden>
																<h:commandLink id="onMessageFromSearchDonationBox"
																	action="#{pages$endowmentPrograms.onMessageFromSearchDonationBox}" />
																<h:commandLink id="onMessageFromSearchEndowments"
																	action="#{pages$endowmentPrograms.onMessageFromSearchEndowments}" />


															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<t:div rendered="true" style="width:100%;">
														<t:panelGrid cellpadding="1px" width="100%"
															cellspacing="5px" columns="4">

															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentProgram.lbl.num']}:" />
															<h:inputText id="txtFileNumber" readonly="true"
																value="#{pages$endowmentPrograms.endowmentProgram.refNum}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.status']}:" />
															<h:inputText id="txtFileStatus"
																value="#{pages$endowmentPrograms.englishLocale? 
																	     pages$endowmentPrograms.endowmentProgram.status.dataDescEn:
																	     pages$endowmentPrograms.endowmentProgram.status.dataDescAr
																	    }"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.createdBy']}:" />
															<h:inputText id="txtFileCreatedB" readonly="true"
																value="#{pages$endowmentPrograms.endowmentProgram.createdByName}"
																styleClass="READONLY" />


															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.createdOn']}:" />
															<h:inputText id="txtFileCreatedOn" readonly="true"
																value="#{pages$endowmentPrograms.endowmentProgram.formattedCreatedOn}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentProgram.lbl.name']}:" />
															<h:inputText id="txtName"
																readonly="#{!pages$endowmentPrograms.showSaveButton && 
																    		!pages$endowmentPrograms.showResubmitButton 
																           }"
																styleClass="#{ (!pages$endowmentPrograms.showSaveButton && 
																    			!pages$endowmentPrograms.showResubmitButton ) ?'READONLY':'' 
																               }"
																value="#{pages$endowmentPrograms.endowmentProgram.progName}" />


															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentProgram.lbl.objective']}:" />
															<h:selectOneMenu id="cmbObjectives"
																disabled="#{!pages$endowmentPrograms.showSaveButton && 
																    		!pages$endowmentPrograms.showResubmitButton 
																           }"
																value="#{pages$endowmentPrograms.endowmentProgram.objectiveId}"
																tabindex="3">
																<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.endowmentProgramObjectives}" />
															</h:selectOneMenu>


															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentProgram.lbl.startDate']}:" />
															<rich:calendar id="txStartDate"
																disabled="#{!pages$endowmentPrograms.showSaveButton && 
																    		!pages$endowmentPrograms.showResubmitButton 
																           }"
																value="#{pages$endowmentPrograms.endowmentProgram.startDate}"
																locale="#{pages$endowmentPrograms.locale}" popup="true"
																datePattern="#{pages$endowmentPrograms.dateFormat}"
																showApplyButton="false" enableManualInput="false"
																cellWidth="24px" cellHeight="22px" style="height:16px" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentProgram.lbl.endDate']}:" />
															<rich:calendar id="txtEndDate"
																disabled="#{!pages$endowmentPrograms.showSaveButton && 
																    		!pages$endowmentPrograms.showResubmitButton 
																           }"
																value="#{pages$endowmentPrograms.endowmentProgram.endDate}"
																locale="#{pages$endowmentPrograms.locale}" popup="true"
																datePattern="#{pages$endowmentPrograms.dateFormat}"
																showApplyButton="false" enableManualInput="false"
																cellWidth="24px" cellHeight="22px" style="height:16px" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentProgram.lbl.manager']}:" />
															<h:selectOneMenu id="cmbManager"
																disabled="#{!pages$endowmentPrograms.showSaveButton && 
																    		!pages$endowmentPrograms.showResubmitButton 
																           }"
																value="#{pages$endowmentPrograms.endowmentProgram.manager}"
																tabindex="3">
																<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.secUserList}" />
															</h:selectOneMenu>


															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentProgram.lbl.responsibleEmp']}:" />
															<h:selectOneMenu id="cmbResponsibleEmployee"
																disabled="#{!pages$endowmentPrograms.showSaveButton && 
																    		!pages$endowmentPrograms.showResubmitButton 
																           }"
																value="#{pages$endowmentPrograms.endowmentProgram.responsibleEmployee}"
																tabindex="3">
																<f:selectItem itemLabel="--" itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.secUserList}" />
															</h:selectOneMenu>


															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentProgram.lbl.budget']}:" />
															<h:inputText id="txtBudget"
																readonly="#{!pages$endowmentPrograms.showSaveButton && 
																    		!pages$endowmentPrograms.showResubmitButton 
																           }"
																value="#{pages$endowmentPrograms.endowmentProgram.programBudgetString}"
																onkeyup="removeNonInteger(this)"
																onkeypress="removeNonInteger(this)"
																onkeydown="removeNonInteger(this)"
																onblur="removeNonInteger(this)"
																onchange="removeNonInteger(this)" />


															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentProgram.lbl.budgetSrc']}:" />
															<h:selectOneMenu id="cmbBudgetSrc"
																disabled="#{!pages$endowmentPrograms.showSaveButton && 
																    			!pages$endowmentPrograms.showResubmitButton 
																               }"
																value="#{pages$endowmentPrograms.endowmentProgram.endProgDisSrcId}"
																tabindex="3">
																<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.endowmentProgramDisbursemntSrc}" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentProgram.lbl.targetedAmount']}:" />
															<h:inputText id="txtTargetAmount"
																readonly="#{!pages$endowmentPrograms.showSaveButton && 
																    		!pages$endowmentPrograms.showResubmitButton 
																           }"
																value="#{pages$endowmentPrograms.endowmentProgram.targetAmountString}"
																onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentProgram.lbl.publishOn']}:" />
															<rich:calendar id="txPublishDate"
																disabled="#{!pages$endowmentPrograms.showSaveButton && 
																    			!pages$endowmentPrograms.showResubmitButton 
																               }"
																value="#{pages$endowmentPrograms.endowmentProgram.publishDate}"
																locale="#{pages$endowmentPrograms.locale}" popup="true"
																datePattern="#{pages$endowmentPrograms.dateFormat}"
																showApplyButton="false" enableManualInput="false"
																cellWidth="24px" cellHeight="22px" style="height:16px" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.description']}" />
															<t:panelGroup colspan="3" style="width: 100%">
																<t:inputTextarea style="width: 90%;" id="txt_desc"
																	readonly="#{!pages$endowmentPrograms.showSaveButton && 
																    			!pages$endowmentPrograms.showResubmitButton 
																               }"
																	value="#{pages$endowmentPrograms.endowmentProgram.description}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</t:panelGroup>


															<h:outputLabel id="lblEndowment" styleClass="LABEL"
																value="#{msg['revenueFollowup.lbl.endowment']}" />
															<h:panelGroup>

																<h:inputText id="endowment" readonly="true"
																	styleClass="READONLY" style="width:170px;"
																	value="#{!empty pages$endowmentPrograms.endowmentProgram.endowment ?pages$endowmentPrograms.endowmentProgram.endowment.endowmentName:''}" />

																<h:graphicImage id="endowmentPopup"
																	rendered="#{
																				 pages$endowmentPrograms.showSaveButton ||
																			     pages$endowmentPrograms.showResubmitButton ||
																			     pages$endowmentPrograms.showApproveRejectSendBack 
																			   }"
																	style="MARGIN: 0px 0px -4px;"
																	title="#{msg['endowment.search.title']}"
																	url="../resources/images/magnifier.gif"
																	onclick="javaScript:openSearchEndowmentsPopup('endowmentPrograms');" />
															</h:panelGroup>

															<h:outputLabel styleClass="LABEL"
																rendered="#{pages$endowmentPrograms.showClosingEvaluation}"
																value="#{msg['endowmentProgram.lbl.evaluationType']}:" />
															<t:panelGroup colspan="3">
																<h:selectOneMenu id="cmbEvalType"
																	disabled="#{!pages$endowmentPrograms.showClose}"
																	rendered="#{pages$endowmentPrograms.showClosingEvaluation}"
																	value="#{pages$endowmentPrograms.endowmentProgram.endProgEvalTypeId}"
																	tabindex="3">
																	<f:selectItem
																		itemLabel="#{msg['commons.pleaseSelect']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.endowmentProgramEvalutionType}" />
																</h:selectOneMenu>
															</t:panelGroup>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['maintenanceRequest.remarks']}"
																rendered="#{pages$endowmentPrograms.showClosingEvaluation}" />
															<t:panelGroup colspan="3" style="width: 100%">
																<t:inputTextarea style="width: 90%;"
																	readonly="#{!pages$endowmentPrograms.showClose}"
																	id="txt_evalremarks"
																	rendered="#{pages$endowmentPrograms.showClosingEvaluation}"
																	value="#{pages$endowmentPrograms.endowmentProgram.evalRemarks}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</t:panelGroup>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['maintenanceRequest.remarks']}"
																rendered="#{pages$endowmentPrograms.showRemarks}" />
															<t:panelGroup colspan="3" style="width: 100%">
																<t:inputTextarea style="width: 90%;" id="txt_remarks"
																	rendered="#{pages$endowmentPrograms.showRemarks}"
																	value="#{pages$endowmentPrograms.txtRemarks}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</t:panelGroup>


														</t:panelGrid>
													</t:div>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$endowmentPrograms.tabPanel}"
																style="width: 100%">

																<rich:tab id="masarifTab"
																	label="#{msg['endowmentProgram.tab.relatedMasarif']}"
																	title="#{msg['endowmentProgram.tab.relatedMasarif']}">
																	<%@ include file="tabProgramRelatedMasarif.jsp"%>

																</rich:tab>

																<rich:tab id="mileStoneTab"
																	rendered="#{ ! empty pages$endowmentPrograms.endowmentProgram.endowmentProgramId}"
																	label="#{msg['endowmentProgram.lbl.tasks']}"
																	title="#{msg['endowmentProgram.lbl.tasks']}"
																	action="#{pages$endowmentPrograms.onProgramMilestoneTab }">
																	<%@ include file="tabProgramMilestones.jsp"%>

																</rich:tab>

																<rich:tab id="collectionsTab"
																	label="#{msg['commons.collection']}"
																	title="#{msg['commons.collection']}"
																	action="#{pages$endowmentPrograms.onProgramCollectionsTab}">
																	<%@ include file="tabProgramCollections.jsp"%>

																</rich:tab>
																<rich:tab id="boxesTab"
																	label="#{msg['endowmentProgram.tab.realatedBoxes']}"
																	title="#{msg['endowmentProgram.tab.realatedBoxes']}">
																	<%@ include file="tabProgramRelatedBoxes.jsp"%>

																</rich:tab>
																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>



																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>


																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$endowmentPrograms.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">
															<pims:security screen="Pims.EndowMgmt.EndowmentFile.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentPrograms.showSaveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$endowmentPrograms.onSave}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentPrograms.showSubmitButton}"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSubmit"
																	action="#{pages$endowmentPrograms.onSubmit}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentPrograms.showResubmitButton}"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkReSubmit"
																	action="#{pages$endowmentPrograms.onResubmitted}" />
															</pims:security>

															<pims:security
																screen="Pims.EndowMgmt.EndowmentProgram.Approve"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentPrograms.showApproveRejectSendBack}"
																	value="#{msg['commons.approve']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkApprove"
																	action="#{pages$endowmentPrograms.onApprove}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentPrograms.showApproveRejectSendBack}"
																	value="#{msg['commons.sendBack']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkResubmitted"
																	action="#{pages$endowmentPrograms.onSendBack}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentPrograms.showApproveRejectSendBack}"
																	value="#{msg['commons.reject']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkRejected"
																	action="#{pages$endowmentPrograms.onRejected}" />
															</pims:security>

															<pims:security
																screen="Pims.EndowMgmt.EndowmentProgram.Finance"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentPrograms.showDisburse}"
																	value="#{msg['commons.disburse']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkDisburse"
																	action="#{pages$endowmentPrograms.onDisbursed}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentPrograms.showDisburse}"
																	value="#{msg['commons.reject']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkFinanceRejected"
																	action="#{pages$endowmentPrograms.onFinanceRejected}" />
															</pims:security>
															<pims:security
																screen="Pims.EndowMgmt.EndowmentProgram.Publish"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentPrograms.showPublished}"
																	value="#{msg['commons.publish']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkPublish"
																	action="#{pages$endowmentPrograms.onPublished}" />

															</pims:security>
															<pims:security
																screen="Pims.EndowMgmt.EndowmentProgram.Complete"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$endowmentPrograms.showClose}"
																	value="#{msg['commons.closeButton']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkClose"
																	action="#{pages$endowmentPrograms.onClose}" />

															</pims:security>

														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<c:choose>
								<c:when test="${!pages$endowmentPrograms.viewModePopup}">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
								</c:when>
							</c:choose>


						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>