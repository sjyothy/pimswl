<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>

<script language="javascript" type="text/javascript">
   
   	function sendToParentAfterSelection() 
   	{
	    window.opener.onMessageFromChequeList();
	    window.close();
	}
	function sendToParent()//tenantId,tenantNum,typeId)
	{
      	
     	//  window.opener.populateTenant(tenantId,tenantNum,typeId);
       	window.opener.document.forms[0].submit();
		window.close();
	  
	}
	function showPopup(personType)
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   //window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
	    window.open('SearchPerson.jsf?persontype='+personType+'&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	function openEditChequePopup()
	{
	  	var screen_width = screen.width;
        var screen_height = screen.height;
     	var popup_width = screen_width-500;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
	    window.open('EditChequeDetails.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,resizable=no,titlebar=no,dialog=yes');
	    
	}
	function fromEditCheque()
	{
		 document.getElementById("formChequeList:hdnIsFromEditCheque").value="1";
		 document.forms[0].submit();
	}

	function clearWindow()
	{
        document.getElementById("formChequeList:txtpaymentNumber").value="";
        document.getElementById("formChequeList:txtGRPNumber").value="";
	    document.getElementById("formChequeList:txtReceiptNumber").value="";
	    document.getElementById("formChequeList:txtPaymentAmount").value="";
	    document.getElementById("formChequeList:txtMethodNo").value="";
	    document.getElementById("formChequeList:txtPropertyName").value="";
	    document.getElementById("formChequeList:txtUnitNumber").value="";
	    document.getElementById("formChequeList:txtContractNumber").value="";
	    document.getElementById("formChequeList:txtTenantName").value="";
	   
	    $('formChequeList:dateFrom').component.resetSelectedDate();
		$('formChequeList:dateTo').component.resetSelectedDate();
		$('formChequeList:dueOnFrom').component.resetSelectedDate();
		$('formChequeList:dueOnTo').component.resetSelectedDate();
		$('formChequeList:ChequeDate').component.resetSelectedDate();

		document.getElementById("formChequeList:selectPaymentType").selectedIndex= 0;
		document.getElementById("formChequeList:selectPaymentMethod").selectedIndex= 0;
		document.getElementById("formChequeList:selectPaymentStatus").selectedIndex= 0;

	    }

		function showBouncedChequesPopup()
       {
             var screen_width = screen.width;
             var screen_height = screen.height;
             var popup_width = screen_width-325;
             var popup_height = screen_height-400;
             var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
             
             var popup = window.open('BouncedChequesList.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,resizable=no,titlebar=no,dialog=yes');
             popup.focus();
       }
	    
	 function   showContractPopup()
		{
	     // alert("In show PopUp");
		var screen_width = 1024;
        var screen_height = 470;

		    
		   window.open('ContractListPopUp.jsf?clStatus=ALL','_blank','width='+(screen_width-220)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
		    
		}  
   	function populateContract(contractNumber,contractId,tenantId)
	{
     
        
	    document.getElementById("formChequeList:hdnContactNumber").value=contractNumber;
	 
     }
     function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		   document.getElementById("formChequeList:txtTenantName").value=personName;
	}
     function showRedepositCheque()
	 {
		   var screen_width = 1024;
		   var screen_height = 450;
		   window.open('redepositCheque.jsf','_blank','width='+(screen_width-500)+',height='+(screen_height-100)+',left=300,top=250,scrollbars=yes,status=yes');
		   
	 }
    function btnSearch_Click()
    {
    
            
		    document.getElementById("formChequeList:cmdSearch").onclick();
		    
    }
    
    
   function updateChqueStatus()
    {
      document.getElementById('formChequeList:lnkUpdateSearch').onclick();
    }    

   </script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>



		</head>
		<body dir="${sessionScope.CurrentLocale.dir}"
			lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<c:choose>
						<c:when test="${!pages$chequeList.isModeSelectOnePopUp}">
							<tr>
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>

					<tr width="100%">
						<c:choose>
							<c:when test="${!pages$chequeList.isModeSelectOnePopUp}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>
						<td width="87%" height="470px" valign="top"
							class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['chequeList.chequeList']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>

									<td width="100%" height="440px" valign="top">
										<div class="SCROLLABLE_SECTION"
											style="padding-bottom: 7px; padding-left: 10px; padding-right: 0; padding-top: 7px;">
											<h:form id="formChequeList" style="width:96%"
												enctype="multipart/form-data">
												<h:inputHidden id="hdntenantId"
													value="#{pages$chequeList.hdnTenantId}" />
												<h:inputHidden id="hdnContactNumber"
													value="#{pages$chequeList.hdnContractNumber}"></h:inputHidden>
												<h:inputHidden id="hdnIsFromEditCheque"
													value="#{pages$chequeList.hdnIsFromEditCheque}"></h:inputHidden>
												<a4j:commandLink id="lnkUpdateSearch"
													action="#{pages$chequeList.updateSearch}"
													reRender="conDiv,footerDiV" />

												<div>
													<table border="0" class="layoutTable">
														<tr>
															<td colspan="6">
																<h:outputText value="#{pages$chequeList.errorMessages}"
																	escape="false" styleClass="INFO_FONT"
																	style="padding:10px;" />
																<h:commandLink id="cmdSearch"
																	action="#{pages$chequeList.btnSearch_Click}"></h:commandLink>
															</td>
														</tr>
													</table>
												</div>
												<div class="MARGIN">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="DETAIL_SECTION">
														<h:outputLabel value="#{msg['inquiry.searchcriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER" width="100%">

															<tr>
																<td width="97%">
																	<table width="100%">

																		<tr>
																			<td width="25%">
																				<h:outputLabel
																					value="#{msg['chequeList.paymentNumber']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="txtpaymentNumber"
																					value="#{pages$chequeList.paymentNumber}" />
																			</td>

																			<td width="25%">
																				<h:outputLabel
																					value="#{msg['chequeList.receiptNumber']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="txtReceiptNumber"
																					value="#{pages$chequeList.receiptNumber}">
																				</h:inputText>
																			</td>

																		</tr>

																		<tr>
																			<td>
																				<h:outputLabel
																					value="#{msg['cancelContract.payment.paymenttype']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="selectPaymentType"
																					value="#{pages$chequeList.selectOnePaymentType}">
																					<f:selectItem
																						itemLabel="#{msg['commons.pleaseSelect']}"
																						itemValue="-1" />
																					<f:selectItems
																						value="#{pages$chequeList.paymentTypeList}" />
																				</h:selectOneMenu>

																			</td>

																			<td>
																				<h:outputLabel
																					value="#{msg['chequeList.message.paymentStatus']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="selectPaymentStatus"
																					value="#{pages$chequeList.selectOnePaymentStatus}"
																					binding="#{pages$chequeList.paymentStatusMenu}">
																					<f:selectItem
																						itemLabel="#{msg['commons.pleaseSelect']}"
																						itemValue="-1" />
																					<f:selectItems
																						value="#{pages$chequeList.paymentStatusList}" />
																				</h:selectOneMenu>

																			</td>
																		</tr>

																		<tr>
																			<td>
																				<h:outputLabel
																					value="#{msg['chequeList.message.paymentAmount']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtPaymentAmount"
																					styleClass="A_RIGHT_NUM INPUT_TEXT"
																					value="#{pages$chequeList.paymentAmount}">
																				</h:inputText>
																			</td>
																			<td>
																				<h:outputLabel
																					value="#{msg['cancelContract.payment.paymentmethod']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="selectPaymentMethod"
																					value="#{pages$chequeList.selectOnePaymentMethod}"
																					binding="#{pages$chequeList.paymentMethodMenu}">
																					<f:selectItem
																						itemLabel="#{msg['commons.pleaseSelect']}"
																						itemValue="-1" />
																					<f:selectItems
																						value="#{pages$chequeList.paymentMethodList}" />
																				</h:selectOneMenu>

																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel
																					value="#{msg['chequeList.dueOnFrom']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="dueOnFrom"
																					value="#{pages$chequeList.dueOnFrom}" popup="true"
																					datePattern="#{pages$chequeList.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$chequeList.locale}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px" />

																			</td>

																			<td>
																				<h:outputLabel
																					value="#{msg['chequeList.dueOnTo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="dueOnTo"
																					value="#{pages$chequeList.dueOnTo}" popup="true"
																					datePattern="#{pages$chequeList.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$chequeList.locale}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px" />
																			</td>

																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel
																					value="#{msg['chequeList.dateFrom']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="dateFrom"
																					value="#{pages$chequeList.transactionDate}"
																					popup="true"
																					datePattern="#{pages$chequeList.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$chequeList.locale}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px" />

																			</td>

																			<td>
																				<h:outputLabel value="#{msg['chequeList.dateTo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="dateTo"
																					value="#{pages$chequeList.transactionDateTo}"
																					popup="true"
																					datePattern="#{pages$chequeList.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$chequeList.locale}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px" />
																			</td>

																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel
																					value="#{msg['chequeList.chequeNo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtMethodNo"
																					value="#{pages$chequeList.methodRefNo}" />
																			</td>

																			<td>
																				<h:outputLabel
																					value="#{msg['commons.replaceCheque.oldChequeDate']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="ChequeDate"
																					value="#{pages$chequeList.chequeDate}" popup="true"
																					datePattern="#{pages$chequeList.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$chequeList.locale}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel
																					value="#{msg['contract.property.Name']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtPropertyName"
																					value="#{pages$chequeList.propertyName}" />
																			</td>
																			<td>
																				<h:outputLabel
																					value="#{msg['inquiry.unitNumber']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtUnitNumber"
																					value="#{pages$chequeList.unitNumber}" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel
																					value="#{msg['chequeList.contractNo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtContractNumber"
																					value="#{pages$chequeList.contractNumber}"></h:inputText>
																			</td>
																			<td>
																				<h:outputLabel value="#{msg['tenants.name']} :"></h:outputLabel>
																			</td>
																			<td style="width: 30%">
																				<h:panelGroup>
																					<h:inputText id="txtTenantName"
																						binding="#{pages$chequeList.txtTenantName}">
																					</h:inputText>
																					<h:graphicImage
																						url="../resources/images/app_icons/Search-tenant.png"
																						binding="#{pages$LeaseContract.imgTenant}"
																						onclick="showPopup('#{pages$chequeList.personTenant}');"
																						style="MARGIN: 0px 0px -4px; CURSOR: hand;"
																						alt="#{msg[contract.searchTenant]}"></h:graphicImage>
																				</h:panelGroup>
																			</td>

																		</tr>
																		<TR>

																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['searchInheritenceFile.fileNo']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="fileNoCriteria"
																					binding="#{pages$chequeList.txtFileNumber}"
																					value="#{pages$chequeList.inheritanceFileNumber}"
																					style="width: 186px;" maxlength="20">
																				</h:inputText>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['searchInheritenceFile.filePerson']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="filePersonCriteria"
																					value="#{pages$chequeList.inheritanceFileOwner}"
																					style="width: 186px;" maxlength="200">
																				</h:inputText>
																			</td>
																		</TR>
																		<tr>
																			<td>
																				<h:outputLabel
																					value="#{msg['ContractorSearch.GRPNumber']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtGRPNumber"
																					value="#{pages$chequeList.grpNumber}">
																				</h:inputText>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['grp.ownerShipMenu']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="ownerShipMenu"
																					value="#{pages$chequeList.selectOneOwnershipType}"
																					style="width: 192px;" required="false">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyOwnershipType}" />
																				</h:selectOneMenu>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<t:outputLabel styleClass="LABEL ATTACH_2"
																					value="#{msg['attachment.location']}: "></t:outputLabel>
																			</td>
																			<td>
																				<t:inputFileUpload id="fileupload" size="350"
																					storage="file" maxlength="250"
																					value="#{pages$chequeList.selectedFile}"
																					binding="#{pages$chequeList.fileUploadCtrl}"
																					style="height: 16px;width: 190px; margin-left: 5px;"></t:inputFileUpload>
																			</td>
																		</tr>

																		<tr>
																			<td colspan="6" class="BUTTON_TD">
																				<t:commandButton id="btnUpload" styleClass="BUTTON"
																					value="#{msg['attachment.buttons.upload']}"
																					actionListener="#{pages$chequeList.uploadFile}"
																					style="width: 75px;margin-right: 2px;margin-left: 2px;" />
																				<h:commandButton id="btnSearch" styleClass="BUTTON"
																					type="submit" value="#{msg['commons.search']}"
																					style="width: 75px"
																					action="#{pages$chequeList.btnSearch_Click}">
																				</h:commandButton>
																				<h:commandButton styleClass="BUTTON" type="button"
																					onclick="javascript:clearWindow();"
																					value="#{msg['commons.clear']}" style="width: 75px">
																				</h:commandButton>
																			</td>
																		</tr>

																		<h:inputHidden id="hdnMode"
																			value="#{pages$chequeList.hdnMode}" />
																		<h:inputHidden id="replaceChequeden"
																			binding="#{pages$chequeList.replaceChequeHidden}" />
																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>

															</tr>

														</table>
													</div>
												</div>

												<div style="padding: 5px; width: 98%;">
													<div class="imag">
														&nbsp;
													</div>
													<div id="conDiv" class="contentDiv" style="width: 99%;">
														<t:dataTable id="dt1"
															value="#{pages$chequeList.gridDataList}"
															binding="#{pages$chequeList.dataTable}"
															rows="#{pages$chequeList.paginatorRows}"
															preserveDataModel="false" preserveSort="false"
															var="dataItem" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true" width="100%">

															<t:column id="Chequeselect" style="white-space:normal"
																rendered="#{!pages$chequeList.isModeSelectOnePopUp}"
																width="3%">
																<f:facet name="header">
																	<t:outputText id="selectTxt" value="" />
																</f:facet>
																<t:selectBooleanCheckbox id="selectChk"
																	value="#{dataItem.collectedChequeSelected}"
																	disabled="#{dataItem.collectedChequeEnable}" />
															</t:column>
															<t:column id="Gpaymuber" defaultSorted="true">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="Gpaymuber"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['chequeList.paymentNumber']}"
																		arrow="true">
																		<f:attribute name="sortField" value="paymentNumber" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="cmdlink" styleClass="A_LEFT"
																	value="#{dataItem.paymentNumber}"
																	style="white-space: normal;" />
																<t:outputText />
															</t:column>
															<t:column id="GpaymentTypeEn"
																rendered="#{pages$chequeList.isEnglishLocale}">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="GpaymentTypeEn"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['cancelContract.payment.paymenttype']}"
																		arrow="true">
																		<f:attribute name="sortField" value="description" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="payTypeEn" styleClass="A_LEFT"
																	value="#{dataItem.paymentTypeDescriptionEn}"
																	style="white-space: normal;" />
															</t:column>

															<t:column id="GpaymentTypeAr"
																rendered="#{pages$chequeList.isArabicLocale}">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="GpaymentTypeAr"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['cancelContract.payment.paymenttype']}"
																		arrow="true">
																		<f:attribute name="sortField" value="description" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="payTypeAr" styleClass="A_LEFT"
																	value="#{dataItem.paymentTypeDescriptionAr}"
																	style="white-space: normal;" />
															</t:column>
															<t:column id="GpaymentModeEn"
																rendered="#{pages$chequeList.isEnglishLocale}">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="GpaymentModeEn"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['cancelContract.payment.paymentmethod']}"
																		arrow="true">
																		<f:attribute name="sortField" value="paymentModeId" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="payModeEn" styleClass="A_LEFT"
																	value="#{dataItem.paymentModeEn}"
																	style="white-space: normal;" />
															</t:column>

															<t:column id="GpaymentModeAr"
																rendered="#{pages$chequeList.isArabicLocale}">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="GpaymentModeAr"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['cancelContract.payment.paymentmethod']}"
																		arrow="true">
																		<f:attribute name="sortField" value="paymentModeId" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="payModeAr" styleClass="A_LEFT"
																	value="#{dataItem.paymentModeAr}"
																	style="white-space: normal;" />
															</t:column>
															<t:column id="Gamount">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="Gamount"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['chequeList.paymentAmountCol']}"
																		arrow="true">
																		<f:attribute name="sortField" value="amount" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="amount" styleClass="A_RIGHT_NUM"
																	value="#{dataItem.amount}" style="white-space: normal;" />
															</t:column>
															<t:column id="GamountDetail">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="GamountDetail"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['chequeList.chequeAmountCol']}"
																		arrow="true">
																		<f:attribute name="sortField" value="amount" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="amountDetail" styleClass="A_RIGHT_NUM"
																	value="#{dataItem.amountDetails}"
																	style="white-space: normal;" />
															</t:column>
															<t:column id="GpaymentDate">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="GpaymentDate"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['commons.date']}" arrow="true">
																		<f:attribute name="sortField" value="paymentDate" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="GpayDate" styleClass="A_LEFT"
																	value="#{dataItem.paymentDueOn}"
																	style="white-space: normal;">
																	<f:convertDateTime
																		pattern="#{pages$chequeList.dateFormat}"
																		timeZone="#{pages$chequeList.timeZone}" />
																</t:outputText>
															</t:column>
															<t:column id="GContractNumber">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="contractNumber"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['contract.contractNumber']}" arrow="true">
																		<f:attribute name="sortField" value="contractNumber" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="GContNumber" styleClass="A_LEFT"
																	value="#{! empty dataItem.contractNumber?dataItem.contractNumber:dataItem.inheritanceFileNumber}"
																	style="white-space: normal;" />

															</t:column>
															<t:column id="GUnitNumber" style="white-space: normal;">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="GUnitNumber"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['inquiry.unitNumber']}" arrow="true">
																		<f:attribute name="sortField" value="unitNumber" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="GUnNumber" styleClass="A_LEFT"
																	value="#{dataItem.unitNumber}"
																	style="white-space: normal;" />

															</t:column>
															<t:column id="GchequeNumber" style="white-space: normal;">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="GchequeNumber"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['bouncedChequesList.chequeNumberCol']}"
																		arrow="true">
																		<f:attribute name="sortField" value="methodRefNo" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="chequeNumber" styleClass="A_LEFT"
																	value="#{dataItem.chequeNumber}"
																	style="white-space: normal;" />
															</t:column>
															<t:column id="GBankEn" rendered="false">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="GBankEn"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['chequeList.bankNameCol']}" arrow="true">
																		<f:attribute name="sortField" value="bankId" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="bankEn" styleClass="A_LEFT"
																	value="#{dataItem.bankNameEn}"
																	style="white-space: normal;" />
															</t:column>
															<t:column id="GBankAr" rendered="false">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="GBankAr"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['chequeList.bankNameCol']}" arrow="true">
																		<f:attribute name="sortField" value="bankId" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="BankAr" styleClass="A_LEFT"
																	value="#{dataItem.bankNameAr}"
																	style="white-space: normal;" />
															</t:column>
															<t:column id="GreceiptNumber"
																style="white-space: normal;">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="GreceiptNumber"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['chequeList.receiptNumber']}"
																		arrow="true">
																		<f:attribute name="sortField" value="receiptNumber" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="receiptNo" styleClass="A_LEFT"
																	value="#{dataItem.receiptNumber}"
																	style="white-space: normal;" />
															</t:column>
															<t:column id="bouncedReason" style="white-space: normal;">
																<f:facet name="header">
																	<h:outputLabel
																		value="#{msg['commons.label.bouncedReason']}">
																	</h:outputLabel>
																</f:facet>
																<h:outputLabel style="white-space: normal;"
																	value="#{dataItem.bouncedReason}">
																</h:outputLabel>
															</t:column>

															<t:column id="GstatusEn" style="white-space: normal;">
																<f:facet name="header">
																	<t:commandSortHeader immediate="false"
																		columnName="GstatusEn"
																		actionListener="#{pages$chequeList.sort}"
																		value="#{msg['chequeList.status']}" arrow="true">
																		<f:attribute name="sortField" value="statusId" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText id="statusEn" styleClass="A_LEFT"
																	value="#{pages$chequeList.isEnglishLocale?dataItem.statusEn:dataItem.statusAr}"
																	style="white-space: normal;" />
															</t:column>
															<t:column id="dselect"
																rendered="#{pages$chequeList.isModeSelectOnePopUp}">
																<t:commandLink id="selectSingleLink"
																	action="#{pages$chequeList.onSendToParent}">
																	<h:graphicImage alt="#{msg['commons.select']}"
																		url="../resources/images/select-icon.gif" />
																</t:commandLink>
															</t:column>

															<pims:security
																screen="Pims.PaymentManagement.PaymentDetails.ChequeList"
																action="create">

																<t:column id="GpaymentDetail"
																	rendered="#{!pages$chequeList.isModeSelectOnePopUp}">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.action']}" />
																	</f:facet>


																	<t:commandLink id="replaceLink"
																		rendered="#{!dataItem.isRealized && dataItem.showRealized}"
																		onclick="if (! confirmRepalceChequedaysExceed('#{dataItem.isExceedMaxDaysForWithDrawal}','#{msg['chequeList.msg.durationNotWithinLimit']}')) return false;"
																		action="#{pages$chequeList.showReplaceCheque}">
																		<h:graphicImage
																			alt="#{msg['chequeList.replaceCheque']}"
																			url="../resources/images/app_icons/replaceCheque.png" />
																	</t:commandLink>

																	<h:commandLink id="updateChequeStatusFromGrp"
																		rendered="#{dataItem.showUpdateFromGrpIcon }"
																		action="#{pages$chequeList.onUpdateStatusFromGRP}">
																		<h:graphicImage
																			alt="#{msg['chequeList.updateChequeStatusFromGRP']}"
																			url="../resources/images/app_icons/reopen_tender.png" />
																	</h:commandLink>

																	<h:commandLink id="receiptDetail"
																		rendered="#{pages$chequeList.isShowDetailIcon}"
																		action="#{pages$chequeList.showReceiptDetail}">
																		<h:graphicImage
																			alt="#{msg['commons.group.permissions.view']}"
																			url="../resources/images/detail-icon.gif" />
																	</h:commandLink>

																	<h:commandLink
																		action="#{pages$chequeList.btnPrintPaymentSchedule_Click}"
																		rendered="#{pages$chequeList.isShowDetailIcon}">
																		<h:graphicImage id="printPayments"
																			title="#{msg['commons.print']}"
																			url="../resources/images/app_icons/print.gif" />
																	</h:commandLink>

																	<h:commandLink id="editCheque"
																		rendered="#{
															              dataItem.statusId == pages$chequeList.ddvChequeStatusCollected.domainDataId &&
															              dataItem.paymentModeId == pages$chequeList.ddvPaymentMethodCheque.domainDataId &&
															              dataItem.grpStatusId != 10802
															            }"
																		action="#{pages$chequeList.openEditChequePopup}">
																		<h:graphicImage
																			alt="#{msg['TaskList.DataTable.Edit']}"
																			url="../resources/images/edit-icon.gif" />
																	</h:commandLink>

																	<h:commandLink
																		rendered="#{!pages$chequeList.isShowDetailIcon}"
																		action="#{pages$chequeList.onViewPaymentDetails}">
																		<h:graphicImage id="viewPayments"
																			title="#{msg['commons.group.permissions.view']}"
																			url="../resources/images/detail-icon.gif" />
																	</h:commandLink>

																	<h:commandLink id="attachFile"
																		rendered="#{dataItem.bounceCheque}"
																		onclick="showAttachmentPopup('#{dataItem.paymentReceiptDetailId}');">
																		<h:graphicImage
																			alt="#{msg['tooltip.attacheScannedCheque']}"
																			url="../resources/images/app_icons/attachmentBlue.png" />
																	</h:commandLink>



																</t:column>
															</pims:security>
														</t:dataTable>
													</div>
													<t:div id="footerDiV"
														styleClass="contentDivFooter AUCTION_SCH_RF"
														style="width:100%;#width:100%;">
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td id="recordTD" class="RECORD_NUM_TD">
																	<div class="RECORD_NUM_BG">
																		<table cellpadding="0" cellspacing="0"
																			style="width: 182px; # width: 150px;">
																			<tr>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText value=" : " />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{pages$chequeList.recordSize}" />
																				</td>
																			</tr>
																		</table>
																	</div>
																</td>
																<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																	align="right">

																	<t:div styleClass="PAGE_NUM_BG" style="#width:20%">
																		<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																		<table cellpadding="0" cellspacing="0">
																			<tr>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																				</td>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{pages$chequeList.currentPage}" />
																				</td>
																			</tr>
																		</table>
																	</t:div>
																	<TABLE border="0" class="SCH_SCROLLER">
																		<tr>
																			<td>
																				<t:commandLink
																					action="#{pages$chequeList.pageFirst}"
																					disabled="#{pages$chequeList.firstRow == 0}">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$chequeList.pagePrevious}"
																					disabled="#{pages$chequeList.firstRow == 0}">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:dataList value="#{pages$chequeList.pages}"
																					var="page">
																					<h:commandLink value="#{page}"
																						actionListener="#{pages$chequeList.page}"
																						rendered="#{page != pages$chequeList.currentPage}" />
																					<h:outputText value="<b>#{page}</b>" escape="false"
																						rendered="#{page == pages$chequeList.currentPage}" />
																				</t:dataList>
																			</td>
																			<td>
																				<t:commandLink action="#{pages$chequeList.pageNext}"
																					disabled="#{pages$chequeList.firstRow + pages$chequeList.rowsPerPage >= pages$chequeList.totalRows}">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink action="#{pages$chequeList.pageLast}"
																					disabled="#{pages$chequeList.firstRow + pages$chequeList.rowsPerPage >= pages$chequeList.totalRows}">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblL"></t:graphicImage>
																				</t:commandLink>
																			</td>
																		</tr>
																	</TABLE>
																</td>
															</tr>
														</table>
													</t:div>
												</div>
												<div id="divButton" class="TAB_PANEL_MARGIN"
													style="padding-top: 7px;">
													<table width="99%" border="0">
														<tr>
															<td colspan="6" class="BUTTON_TD">
																<pims:security
																	screen="Pims.PaymentManagement.PaymentDetails.ChequeList"
																	action="create">
																	<h:commandButton rendered="false" styleClass="BUTTON"
																		type="submit"
																		binding="#{pages$chequeList.cancelButton}"
																		value="#{msg['chequeList.cancelled']}"
																		action="#{pages$chequeList.btnCancelled_Click}">
																	</h:commandButton>
																</pims:security>
																<pims:security
																	screen="Pims.PaymentManagement.PaymentDetails.ChequeList.Bounce"
																	action="create">
																	<h:commandButton styleClass="BUTTON" type="submit"
																		binding="#{pages$chequeList.realizedButton}"
																		value="#{msg['chequeList.realized']}"
																		action="#{pages$chequeList.onRealized}">
																	</h:commandButton>
																</pims:security>
																<pims:security
																	screen="Pims.PaymentManagement.PaymentDetails.ChequeList.Bounce"
																	action="create">
																	<h:commandButton styleClass="BUTTON" type="submit"
																		value="#{msg['chequeList.bounced']}"
																		binding="#{pages$chequeList.bouncedButton}"
																		action="#{pages$chequeList.btnBouncePayment_Click}">
																	</h:commandButton>
																</pims:security>
															</td>
														</tr>
													</table>
												</div>

											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<c:choose>
							<c:when test="${!pages$chequeList.isModeSelectOnePopUp}">
								<td colspan="2">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>

											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>


										</tr>
									</table>
								</td>
							</c:when>
						</c:choose>
					</tr>
				</table>
			</div>
		</body>
</f:view>

</html>
