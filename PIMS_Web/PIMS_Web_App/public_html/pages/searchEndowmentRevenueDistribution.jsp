<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Auction
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	    function resetValues()
      	{
      		document.getElementById("searchFrm:txtcostCenter").value="";
      		document.getElementById("searchFrm:txtendowmentNum").value="";
      		document.getElementById("searchFrm:propertyNo").value="";
        }
        
        function sendMessageToParent()
        {
          	window.opener.onMessageFromSearchEndowments();
        	window.close();
        }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<c:choose>
				<c:when
					test="${!pages$SearchEndowmentRevenueDistribution.sViewModePopUp}">
					<div class="containerDiv">
				</c:when>
			</c:choose>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<c:choose>
					<c:when
						test="${!pages$SearchEndowmentRevenueDistribution.sViewModePopUp}">
						<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>

					</c:when>
				</c:choose>
				<tr width="100%">
					<c:choose>
						<c:when
							test="${!pages$SearchEndowmentRevenueDistribution.sViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>
					<td width="83%" height="470px" valign="top"
						class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['endowment.title.revenueDistribution']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
									<div class="SCROLLABLE_SECTION AUC_SCH_SS"
										style="height: 95%; width: 100%; # height: 95%; # width: 100%;">
										<h:form id="searchFrm"
											style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
											<t:div id="layoutTable" styleClass="MESSAGE">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:messages></h:messages>
															<h:outputText id="errorMessage"
																value="#{pages$SearchEndowmentRevenueDistribution.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</t:div>
											<div class="MARGIN">

												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">
																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['endowment.lbl.num']}"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText id="txtendowmentNum"
																				value="#{pages$SearchEndowmentRevenueDistribution.criteria.endowmentNumber}"
																				maxlength="20"></h:inputText>
																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.report.costCenter']}"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText id="txtcostCenter"
																				value="#{pages$SearchEndowmentRevenueDistribution.criteria.endowmentCostCenter}"
																				maxlength="20"></h:inputText>
																		</td>
																	</tr>
																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['inspection.propertyRefNum']}"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText id="propertyNo"
																				value="#{pages$SearchEndowmentRevenueDistribution.criteria.propertyNumber}"
																				maxlength="20"></h:inputText>
																		</td>
																		<td width="25%">

																		</td>
																		<td width="25%">

																		</td>
																	</tr>


																	<tr>
																		<td class="BUTTON_TD" colspan="4" />
																			<table cellpadding="1px" cellspacing="1px">
																				<tr>
																					<td colspan="2">
																						<h:commandButton styleClass="BUTTON"
																							value="#{msg['commons.search']}"
																							action="#{pages$SearchEndowmentRevenueDistribution.onSearch}"
																							style="width: 75px" />
																						<h:commandButton styleClass="BUTTON" type="button"
																							value="#{msg['commons.clear']}"
																							onclick="javascript:resetValues();"
																							style="width: 75px" />
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>

													</table>
												</div>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" style="width: 100%; # width: 99%;">
													<t:dataTable id="dt1"
														value="#{pages$SearchEndowmentRevenueDistribution.dataList}"
														binding="#{pages$SearchEndowmentRevenueDistribution.dataTable}"
														rows="#{pages$SearchEndowmentRevenueDistribution.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%">

														<t:column id="endowmentNumber" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="endowmentNumber"
																	actionListener="#{pages$SearchEndowmentRevenueDistribution.sort}"
																	value="#{msg['endowment.lbl.num']}" arrow="true">
																	<f:attribute name="sortField" value="endowmentNumber" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.endowmentNumber}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="endowmentName" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="endowmentName"
																	actionListener="#{pages$SearchEndowmentRevenueDistribution.sort}"
																	value="#{msg['endowment.lbl.name']}" arrow="true">
																	<f:attribute name="sortField" value="endowmentName" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.endowmentName}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="endowmentCostCenter" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="endowmentCostCenter"
																	actionListener="#{pages$SearchEndowmentRevenueDistribution.sort}"
																	value="#{msg['commons.report.costCenter']}"
																	arrow="true">
																	<f:attribute name="sortField"
																		value="endowmentCostCenter" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.endowmentCostCenter}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column id="propertyNumber">
															<f:facet name="header">
																<t:outputText value="#{msg['property.number']}" />
															</f:facet>
															<t:outputText value="#{dataItem.propertyNumber}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column id="amountToDistribute">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['commons.amountToDistribute']}" />
															</f:facet>

															<t:outputText value="#{dataItem.amountToDistribute}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column id="actions">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>

															<t:commandLink
																rendered="#{!pages$SearchEndowmentRevenueDistribution.pageModeSelectOnePopUp && 
																			!pages$SearchEndowmentRevenueDistribution.pageModeSelectManyPopUp
																 			}"
																action="#{pages$SearchEndowmentRevenueDistribution.onManage}">

																<h:graphicImage title="#{msg['commons.Manage']}"
																	url="../resources/images/app_icons/Tendering.png" />
															</t:commandLink>
														</t:column>

													</t:dataTable>
												</div>
												<t:div id="contentDivFooter"
													styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:100%;#width:100%;">
													<table id="RECORD_NUM_TD" cellpadding="0" cellspacing="0"
														width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$SearchEndowmentRevenueDistribution.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td id="contentDivFooterColumnTwo" class="BUTTON_TD"
																style="width: 53%; # width: 50%;" align="right">

																<t:div styleClass="PAGE_NUM_BG" style="#width:20%">

																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																			</td>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{pages$SearchEndowmentRevenueDistribution.currentPage}" />
																			</td>
																		</tr>
																	</table>
																</t:div>
																<TABLE border="0" class="SCH_SCROLLER">
																	<tr>
																		<td>
																			<t:commandLink
																				action="#{pages$SearchEndowmentRevenueDistribution.pageFirst}"
																				disabled="#{pages$SearchEndowmentRevenueDistribution.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$SearchEndowmentRevenueDistribution.pagePrevious}"
																				disabled="#{pages$SearchEndowmentRevenueDistribution.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFR"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:dataList
																				value="#{pages$SearchEndowmentRevenueDistribution.pages}"
																				var="page">
																				<h:commandLink value="#{page}"
																					actionListener="#{pages$SearchEndowmentRevenueDistribution.page}"
																					rendered="#{page != pages$SearchEndowmentRevenueDistribution.currentPage}" />
																				<h:outputText value="<b>#{page}</b>" escape="false"
																					rendered="#{page == pages$SearchEndowmentRevenueDistribution.currentPage}" />
																			</t:dataList>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$SearchEndowmentRevenueDistribution.pageNext}"
																				disabled="#{pages$SearchEndowmentRevenueDistribution.firstRow + pages$SearchEndowmentRevenueDistribution.rowsPerPage >= pages$SearchEndowmentRevenueDistribution.totalRows}">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$SearchEndowmentRevenueDistribution.pageLast}"
																				disabled="#{pages$SearchEndowmentRevenueDistribution.firstRow + pages$SearchEndowmentRevenueDistribution.rowsPerPage >= pages$SearchEndowmentRevenueDistribution.totalRows}">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblL"></t:graphicImage>
																			</t:commandLink>

																		</td>
																	</tr>
																</TABLE>
															</td>
														</tr>
													</table>
												</t:div>
											</div>

										</h:form>

									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<c:choose>
				<c:when
					test="${!pages$SearchEndowmentRevenueDistribution.sViewModePopUp}">
					</div>
				</c:when>
			</c:choose>

		</body>
	</html>
</f:view>