
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<SCRIPT>	
function closeWindow()
	{
	  //window.opener.document.getElementById('conductAuctionForm:isResultPopupCalled').value = "Y";
	  window.opener.document.forms[0].submit();
	  window.close();
	}
       </SCRIPT>
<f:view>
<SCRIPT>	
function closeRefresh()
	{
	  //window.opener.document.getElementById('conductAuctionForm:isResultPopupCalled').value = "Y";
	  window.opener.document.forms[0].submit();
	  window.close();
	}
       </SCRIPT>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />

			<style>
		span{
			PADDING-RIGHT: 5px; 
			PADDING-LEFT: 5px;
		}
		</style>
			<script language="javascript" type="text/javascript">
	function clearWindow()
	{
        
            document.getElementById("formDuplicationUnits:txtFloorPrefix").value="";
			document.getElementById("formDuplicationUnits:txtStartNo").value="";	
			document.getElementById("formDuplicationUnits:txtEndNo").value="";
			window.close();
			
	}
	

	
</script>



		</head>
		<body dir="${sessionScope.CurrentLocale.dir}"
			lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<tr width="100%">

					<td width="100%" valign="top" class="divBackgroundBody"
						height="610px">
						<table width="100%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr height="1%">
								<td>
									<table width="100%" class="greyPanelTable" cellpadding="0"
										cellspacing="0" border="0">
										<tr>
											<td class="HEADER_TD">
												<h:outputLabel
													value="#{msg['receiveProperty.unit.applyRentValue']}"
													styleClass="HEADER_FONT" />
											</td>
											<td width="100%">
												&nbsp;
											</td>
										</tr>

									</table>

									<table width="100%" class="greyPanelMiddleTable"
										cellpadding="0" cellspacing="0" border="0">
										<tr valign="top">
											<td height="100%" valign="top" nowrap="nowrap"
												background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
												width="1">
											</td>

											<td width="100%" height="576px" valign="top" nowrap="nowrap">




												<div>
													<h:form id="formDuplicationUnits" style="width:75%">
														<h:inputHidden
															binding="#{pages$ApplyRentValue.propertyId}"></h:inputHidden>
														<div class="MARGIN">
															<table cellpadding="0" cellspacing="0" width="100.3%">
																<tr>
																	<td style="FONT-SIZE: 0px;">
																		<IMG
																			src="../<h:outputText value="#{path.img_section_left}"/>"
																			class="TAB_PANEL_LEFT" />
																	</td>
																	<td width="100%" style="FONT-SIZE: 0px;">
																		<IMG
																			src="../<h:outputText value="#{path.img_section_mid}"/>"
																			class="TAB_PANEL_MID" />
																	</td>
																	<td style="FONT-SIZE: 0px;">
																		<IMG
																			src="../<h:outputText value="#{path.img_section_right}"/>"
																			class="TAB_PANEL_RIGHT" />
																	</td>
																</tr>
															</table>
															<div class="DETAIL_SECTION">
																<h:outputLabel
																	value="#{msg['receiveProperty.unit.applyRentValue']}"
																	styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
																<table cellpadding="1px" cellspacing="2px"
																	class="DETAIL_SECTION_INNER">


																	<tr>

																		<td width="30%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unit.floorNumberFrom']}:"></h:outputLabel>
																		</td>
																		<td width="20%">
																			<h:selectOneMenu id="floorsListFrom"
																				style="WIDTH: 115px;"
																				binding="#{pages$ApplyRentValue.floorFromMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />

																				<f:selectItems
																					value="#{pages$ApplyRentValue.floorFromList}" />
																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unit.floorNumberTo']}:"></h:outputLabel>

																		</td>
																		<td>
																			<h:selectOneMenu id="floorsListTo"
																				style="WIDTH: 115px;"
																				binding="#{pages$ApplyRentValue.floorToMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />

																				<f:selectItems
																					value="#{pages$ApplyRentValue.floorToList}" />
																			</h:selectOneMenu>
																		</td>
																	</tr>
																	<tr>

																		<td width="20%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unit.unitNumberFrom']}:"></h:outputLabel>
																		</td>
																		<td width="20%">
																			<h:selectOneMenu id="unitListFrom"
																				style="WIDTH: 115px;"
																				binding="#{pages$ApplyRentValue.unitFromMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />

																				<f:selectItems
																					value="#{pages$ApplyRentValue.unitFromList}" />
																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unit.unitNumberTo']}:"></h:outputLabel>

																		</td>
																		<td>
																			<h:selectOneMenu id="unitListTo"
																				style="WIDTH: 115px;"
																				binding="#{pages$ApplyRentValue.unitToMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />

																				<f:selectItems
																					value="#{pages$ApplyRentValue.unitToList}" />
																			</h:selectOneMenu>
																		</td>
																	</tr>
																	<tr>

																		<td width="20%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unit.numberOfBedroom']}:"></h:outputLabel>
																		</td>
																		<td width="20%">
																		
																			<h:inputText style="width:85%;" id="txtNumberOfBeds"
																				binding="#{pages$ApplyRentValue.numberOfBedroom}">
																			</h:inputText>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unit.unitSide']}:"></h:outputLabel>

																		</td>
																		<td>
																			<h:selectOneMenu id="unitSide" style="WIDTH: 115px;"
																				binding="#{pages$ApplyRentValue.unitSideMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />

																				<f:selectItems
																					value="#{pages$ApplyRentValue.unitSideTypeList}" />
																			</h:selectOneMenu>
																		</td>
																	</tr>


																	<tr>

																		<td width="20%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unit.unitTypeList']}:"></h:outputLabel>
																		</td>
																		<td width="20%">
																			<h:selectOneMenu id="unitTypeList"
																				style="WIDTH: 115px;"
																				binding="#{pages$ApplyRentValue.unitTypeMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />

																				<f:selectItems
																					value="#{pages$ApplyRentValue.unitTypeList}" />
																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unit.investmentTypeList']}:"></h:outputLabel>

																		</td>
																		<td>
																			<h:selectOneMenu id="investmentTypeList"
																				style="WIDTH: 115px;"
																				binding="#{pages$ApplyRentValue.investmentTypeMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />

																				<f:selectItems
																					value="#{pages$ApplyRentValue.investmentTypeList}" />
																			</h:selectOneMenu>
																		</td>
																	</tr>
																	<tr>

																		<td colspan="6" class="BUTTON_TD">
																			<h:commandButton type="submit" styleClass="BUTTON"
																				style="width: 100px;"
																				value="#{msg['receiveProperty.unit.search']}"
																				action="#{pages$ApplyRentValue.searchUnits}"></h:commandButton>


																			<h:commandButton styleClass="BUTTON" type="button"
																				value="#{msg['commons.cancel']}"
																				onclick="javascript:clearWindow();" />

																		</td>
																	</tr>
																	<tr>
																		<td colspan="4">
																			<t:div style="text-align:center;">
																				<t:div styleClass="contentDiv" style="width:82.8%">
																					<t:dataTable id="test8" rows="5" width="100%"
																						value="#{pages$ApplyRentValue.unitDataList}"
																						binding="#{pages$ApplyRentValue.searchResults}"
																						preserveDataModel="true" preserveSort="true"
																						var="dataItemRCV" rowClasses="row1,row2"
																						rules="all" renderedIfEmpty="true">




																						<t:column id="dataItemRCV2" sortable="true">


																							<f:facet name="header">
																								<t:outputText value="#{msg['commons.select']}" />
																							</f:facet>
																							<h:selectBooleanCheckbox id="select"
																								value="#{dataItemRCV.selected}" />
																						</t:column>


																						<t:column id="dataItemRCV4" sortable="true">


																							<f:facet name="header">

																								<t:outputText value="#{msg['member.loginName']}" />

																							</f:facet>
																							<t:outputText styleClass="A_LEFT" title="" id="un1"
																								value="#{dataItemRCV.unitNumber}" />
																						</t:column>
																						<t:column id="dataItemRCV5" sortable="true">


																							<f:facet name="header">

																								<t:outputText value="#{msg['member.groupName']}" />

																							</f:facet>
																							<t:outputText styleClass="A_LEFT" title="" id="un12"
																								value="#{dataItemRCV.floorNumber}" />
																						</t:column>
																						<t:column id="dataItemRCV6" sortable="true">


																							<f:facet name="header">

																								<t:outputText value="#{msg['member.groupName']}" />

																							</f:facet>
																							<t:outputText styleClass="A_LEFT" title="" id="un123"
																								value="#{dataItemRCV.noOfBed}" />
																						</t:column>
																						<t:column id="dataItemRCV7" sortable="true">


																							<f:facet name="header">

																								<t:outputText value="#{msg['member.groupName']}" />

																							</f:facet>
																							<t:outputText styleClass="A_LEFT" title="" id="un11234"
																								value="#{dataItemRCV.unitSideTypeEn}" />
																						</t:column>

																						<t:column id="dataItemRCV8" sortable="true">


																							<f:facet name="header">

																								<t:outputText value="#{msg['member.groupName']}" />

																							</f:facet>
																							<t:outputText styleClass="A_LEFT" title="" id="un12345"
																								value="#{dataItemRCV.unitTypeEn}" />
																						</t:column>

																					</t:dataTable>
																				</t:div>



																				<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																					style="width:85%;">
																					<t:panelGrid cellpadding="0" cellspacing="0"
																						width="100%" columns="2" 
																						columnClasses="RECORD_NUM_TD">

																						<t:div styleClass="RECORD_NUM_BG">
																							<t:panelGrid cellpadding="0" cellspacing="0"
																								columns="3" columnClasses="RECORD_NUM_TD">

																								<h:outputText
																									value="#{msg['commons.recordsFound']}" />

																								<h:outputText value=" : "
																									styleClass="RECORD_NUM_TD" />

																								<h:outputText id="fdsfds"
																									value="#{pages$ApplyRentValue.unitListRecordSize}"
																									styleClass="RECORD_NUM_TD" />

																							</t:panelGrid>
																						</t:div>

																						<t:dataScroller id="scroller" for="test8"
																							paginator="true" fastStep="1"
																							paginatorMaxPages="2" immediate="false"
																							paginatorTableClass="paginator"
																							renderFacetsIfSinglePage="true"
																							paginatorTableStyle="grid_paginator"
																							layout="singleTable"
																							paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																							paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																							pageIndexVar="unitPageNumber">

																							<f:facet name="firstdataItemRCV">
																								<t:graphicImage url="../#{path.scroller_first}"
																									id="lblFdataItemRCV"></t:graphicImage>
																							</f:facet>
																							<f:facet name="fastrewinddataItemRCV">
																								<t:graphicImage
																									url="../#{path.scroller_fastRewind}"
																									id="lblFRdataItemRCV"></t:graphicImage>
																							</f:facet>
																							<f:facet name="fastforwarddataItemRCV">
																								<t:graphicImage
																									url="../#{path.scroller_fastForward}"
																									id="lblFFdataItemRCV"></t:graphicImage>
																							</f:facet>
																							<f:facet name="lastdataItemRCV">
																								<t:graphicImage url="../#{path.scroller_last}"
																									id="lblLdataItemRCV"></t:graphicImage>
																							</f:facet>
																							<t:div styleClass="PAGE_NUM_BG">
																								<h:outputText styleClass="PAGE_NUM"
																									value="#{msg['commons.page']}" />
																								<h:outputText styleClass="PAGE_NUM"
																									style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																									value="#{requestScope.unitPageNumber}" />
																							</t:div>
																						</t:dataScroller>

																					</t:panelGrid>
																				</t:div>

																			</t:div>
																		</td>
																	</tr>
<tr><td>	<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unit.suggestedRentValue']}:"></h:outputLabel>
																	</td><td><h:inputText style="width:85%;" id="suggestedRentValue"
																				binding="#{pages$ApplyRentValue.suggestedRentValue}">
																			</h:inputText>
																		</td><td>	<h:commandButton type="submit" styleClass="BUTTON"
																				style="width: 100px;"
																				value="#{msg['receiveProperty.unit.applyRent']}"
																				action="#{pages$ApplyRentValue.applyRentValue}"></h:commandButton></td></tr>
																</table>
															</div>
														</div>




													</h:form>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
		</body>
</f:view>
</html>
