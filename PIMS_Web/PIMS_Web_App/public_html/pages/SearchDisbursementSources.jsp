<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript">
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
</head>

<body class="BODY_STYLE">
<div class="containerDiv">


	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="2"><jsp:include page="header.jsp" />
			</td>
		</tr>
		<tr width="100%">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="Search Disbursement Sources"
								styleClass="HEADER_FONT" style="padding:10px" />
						</td>
					</tr>
				</table>
				<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
		        	<tr valign="top">
		        		<td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="3">
		        		</td> 
		        		<td>		        			
		        			<div class="SCROLLABLE_SECTION">
		        				<div id="errMsg">
		        					<table border="0" class="layoutTable"
												style="margin-left: 10px; margin-right: 10px; height: 10px;">
										<tr>
											<td>
												<h:outputText
												value="#{pages$searchDisbursementSourceController.errorMessages}"
												escape="false" styleClass="ERROR_FONT" />
	
												<h:outputText id="oppMsgs"
												value="#{pages$searchDisbursementSourceController.messages}"
												styleClass="INFO_FONT" escape="false" />
											</td>
										</tr>
									</table>		        						        				
		        				</div>
		        				
		        				<div class="MARGIN" style="width: 97%;">
		        					<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
											<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
									<div class="DETAIL_SECTION" style="width: 99.6%;">
										<h:outputLabel value="#{msg['inquiry.searchcriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
					        			<t:div id="inhtTab" styleClass="DETAIL_SECTION_INNER" style="width:100%">
					        				<h:form>
					        					<t:panelGrid id="inhtAssetShareTable" cellpadding="1px" width="100%" cellspacing="5px" 		
			 										styleClass="DETAIL_SECTION_INNER" columns="4" columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
			 		
			 										<h:outputLabel styleClass="LABEL" value="#{msg['disuburse.label.sourceNameEn']}:"/> 										
			 										<h:inputText id="txtSrcNameEn" binding="#{pages$searchDisbursementSourceController.sourceNameEn}" style="width:95%;"></h:inputText>
			 		 	
													<h:outputLabel styleClass="LABEL" value="#{msg['disuburse.label.sourceNameAr']}:"/>								
													<h:inputText id="txtSrcNameAr" binding="#{pages$searchDisbursementSourceController.sourceNameAr}" style="width:95%;"></h:inputText>
					
					
													<h:outputLabel styleClass="LABEL" value="#{msg['disburseSrc.lbl.distCodeCombCode']}:"/>
													<h:inputText id="txtGrpNum" binding="#{pages$searchDisbursementSourceController.grpNum}" style="width:95%;"></h:inputText>
													
													<h:outputLabel styleClass="LABEL" value="#{msg['disburseSrc.lbl.acctsPayCombCode']}:"/>
													<h:inputText id="txtacctsPayCombCode" binding="#{pages$searchDisbursementSourceController.acctsPayCombCode}" style="width:95%;"></h:inputText>
																					
												</t:panelGrid>
					        					
					        					<table cellpadding="10px" cellspacing="2px" width="97.5%" >
													<tr >
														<td colspan="4" class="BUTTON_TD">															
																<h:commandButton  
																		styleClass="BUTTON"
																		value="#{msg['commons.search']}" action="#{pages$searchDisbursementSourceController.doSearch}">																			
																</h:commandButton>
																<h:commandButton  
																		styleClass="BUTTON"
																		value="#{msg['commons.clear']}" action="#{pages$searchDisbursementSourceController.clearAll}">																			
																</h:commandButton>
																<h:commandButton  
																		styleClass="BUTTON"
																		value="#{msg['commons.add']}" action="#{pages$searchDisbursementSourceController.addDisbursementSource}">																			
																</h:commandButton>
														</td>
													</tr>	
												</table>
													
					        				</h:form>
					        			</t:div>
					        		</div>					        		
			        			</div>
			        			<div style="padding:5px;width:98%;">
			        				<h:form>		    
				                   		<div class="imag">&nbsp;</div>
											<div class="contentDiv">
												<t:dataTable id="test3"
															rows="#{pages$searchDisbursementSourceController.rowsPerPage}"
															width="100%"							
															value="#{pages$searchDisbursementSourceController.pageResult}"
															binding="#{pages$searchDisbursementSourceController.searchResultsTable}"								
															preserveDataModel="false" preserveSort="false" var="dataItem"
															rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
															columnClasses="A_LEFT_GRID,A_LEFT_GRID,A_LEFT_GRID,A_LEFT_GRID,A_LEFT_GRID">
															
														<t:column id="srcEn" sortable="true" >
		                                                   	<f:facet name="header" >
		                                                    	<t:outputText id="srcEnTxt"
																	value="#{msg['disuburse.label.disbSrcEn']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"  value="#{dataItem.srcNameAr}"/>
														</t:column>
																								
														<t:column id="srcAr" sortable="true" >
		                                                    <f:facet name="header" >
		                                                    	<t:outputText id="srcArTxt"
																	value="#{msg['disuburse.label.disbSrcAr']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"  value="#{dataItem.srcNameAr}"/>
														</t:column>
														<t:column id="grpNum" >
															<f:facet name="header">
																	<t:outputText id="grpNumTxt"
																	value="#{msg['disburseSrc.lbl.distCodeCombCode']}" />															
															</f:facet>
															<t:outputText styleClass="A_LEFT"  title="" value="#{dataItem.grpNum}" />
														</t:column>
														<t:column id="acctsPayCombCode" >
															<f:facet name="header">
																	<t:outputText id="acctsPayCombCodeTxt"
																	value="#{msg['disburseSrc.lbl.acctsPayCombCode']}" />															
															</f:facet>
															<t:outputText styleClass="A_LEFT"  title="" value="#{dataItem.acctsPayCombCode}" />
														</t:column>
														<t:column id="desc" >			
															<f:facet name="header">
																<t:outputText id="descTxt"
																	value="#{msg['disuburse.label.desc']}" />															
															</f:facet>
															<t:outputText styleClass="A_LEFT" value="#{dataItem.description}" />
														</t:column>
										                <t:column id="edit" >										                
															<f:facet name="header">
																<t:outputText id="actionTxt" value="#{msg['commons.action']}" />																																
															</f:facet>
															<h:commandLink action="#{pages$searchDisbursementSourceController.editDisbursementSrc}">							
																<h:graphicImage id="editDetails"
																	title="#{msg['commons.duplicate']}"								
																	url="../resources/images/edit-icon.gif"
																	width="18px;" />
																</h:commandLink>
														</t:column>
												</t:dataTable>																															
											</div>
											<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99%;#width:99%;" >
												<table cellpadding="0" cellspacing="0" width="99%">
													<tr>
													<td class="RECORD_NUM_TD">
														<div class="RECORD_NUM_BG">
															<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
															<tr><td class="RECORD_NUM_TD">
															<h:outputText value="#{msg['commons.recordsFound']}"/>
															</td><td class="RECORD_NUM_TD">
															<h:outputText value=" : "/>
															</td><td class="RECORD_NUM_TD">
															<h:outputText value="#{pages$searchDisbursementSourceController.totalRows}"/>
															</td></tr>
															</table>
														</div>
													</td>
													<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
													
														<t:div styleClass="PAGE_NUM_BG" style="#width:20%">																																					
															 <table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{pages$searchDisbursementSourceController.currentPage}"/>
																	</td>
																</tr>					
															</table>
														</t:div>
														<TABLE border="0" class="SCH_SCROLLER">
																	 	<tr>
																	 		<td>   
																				<t:commandLink
																					action="#{pages$searchDisbursementSourceController.pageFirst}"
																					disabled="#{pages$searchDisbursementSourceController.firstRow == 0}">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																			<t:commandLink
																				action="#{pages$searchDisbursementSourceController.pagePrevious}"
																				disabled="#{pages$searchDisbursementSourceController.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFR"></t:graphicImage>
																			</t:commandLink>
																		  	</td>
																		  	<td>
																			<t:dataList value="#{pages$searchDisbursementSourceController.pages}"
																				var="page" >
																				<h:commandLink value="#{page}"
																					actionListener="#{pages$searchDisbursementSourceController.page}"
																					rendered="#{page != pages$searchDisbursementSourceController.currentPage}" />
																				<h:outputText value="<b>#{page}</b>" escape="false"
																					rendered="#{page == pages$searchDisbursementSourceController.currentPage}" />
																			</t:dataList>
																			</td>
																			<td>
																			<t:commandLink action="#{pages$searchDisbursementSourceController.pageNext}"
																				disabled="#{pages$searchDisbursementSourceController.firstRow + pages$searchDisbursementSourceController.rowsPerPage >= pages$searchDisbursementSourceController.totalRows}">
																				<t:graphicImage url="../#{path.scroller_fastForward}"
																					id="lblFF"></t:graphicImage>
																			</t:commandLink>
																			</td>
																			<td>
																			<t:commandLink action="#{pages$searchDisbursementSourceController.pageLast}"
																				disabled="#{pages$searchDisbursementSourceController.firstRow + pages$searchDisbursementSourceController.rowsPerPage >= pages$searchDisbursementSourceController.totalRows}">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblL"></t:graphicImage>
																			</t:commandLink>
																			</td>
																			</tr>
																		</TABLE>	
																		</td></tr>
																</table>
													</t:div>
											
									</div>
									</h:form>
				        		</div>
			        		</div>
		        		</td>
		        	</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	</body>
</html>
</f:view>