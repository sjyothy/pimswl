<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>


<h:commandLink action="#{pages$LeaseContract.btnEditPaymentSchedule_Click}">
	<h:graphicImage id="editPayments123" 
	title="#{msg['commons.edit']}" 
	url="../resources/images/edit-icon.gif"
	rendered="#{(paymentScheduleDataItem.showEdit) }"
	/>

</h:commandLink>
<a4j:commandLink reRender="tbl_PaymentSchedule,paymentSummary" action="#{pages$LeaseContract.btnDelPaymentSchedule_Click}">
	<h:graphicImage id="deletePaySc" title="#{msg['commons.delete']}"  rendered="#{(paymentScheduleDataItem.showDelete) }" 
	url="../resources/images/delete_icon.png"/>														
</a4j:commandLink>
<h:commandLink action="#{pages$LeaseContract.btnPrintPaymentSchedule_Click}">
<h:graphicImage id="printPayments" 
					title="#{msg['commons.print']}" 
url="../resources/images/app_icons/print.gif"
rendered="#{paymentScheduleDataItem.isReceived == 'Y'}"
/>
</h:commandLink> 
<h:commandLink action="#{pages$LeaseContract.btnViewPaymentDetails_Click}">
<h:graphicImage id="viewPayments" 
			title="#{msg['commons.group.permissions.view']}" 
url="../resources/images/detail-icon.gif"
/>

</h:commandLink> 