<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>

		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>

			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['procedureDocument.heading']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height: 500px;">
										<h:form id="venueSearchFrm">
											<table border="0" class="layoutTable">
												<tr>
													<td>
														<h:outputText
															value="#{pages$ProcedureDocument.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />
														<h:outputText
															value="#{pages$ProcedureDocument.infoMessages}"
															escape="false" styleClass="INFO_FONT" />
													</td>
												</tr>
											</table>
											<div
												style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; MARGIN: 0px; WIDTH: 95%; PADDING-TOP: 10px">

												<div class="MARGIN">
													<h:panelGrid columns="4" cellspacing="5px">
														<h:outputLabel styleClass="LABEL" style="width:150px;"
															value="#{msg['paymentConfiguration.procedureType']}:" />
														<h:inputText id="txtProcedureType"
															value="#{pages$ProcedureDocument.procedureType}"
															readonly="true" style="width:200px;" />

														<h:outputLabel styleClass="LABEL" style="width:150px;"
															value="#{msg['attachment.nolType']}:"
															rendered="#{pages$ProcedureDocument.isSelectedProcedureTypeIsNOL}" />
														<h:selectOneMenu id="selectNOLType" style="width:192px;"
															binding="#{pages$ProcedureDocument.cmbNolType}">
															<f:selectItem itemValue="-1"
																itemLabel="#{msg['commons.All']}" />
															<f:selectItems
																value="#{pages$ProcedureDocument.nolTypeItems}" />
														</h:selectOneMenu>
														<h:outputLabel styleClass="LABEL" style="width:150px;"
															value="#{msg['SearchBeneficiary.fileType']}:"
															rendered="#{pages$ProcedureDocument.selectedProcedureTypeIsOpenFile}" />
														<h:selectOneMenu id="selectFileType" style="width:192px;"
															binding="#{pages$ProcedureDocument.cmbFileType}">
															<f:selectItem itemValue="-1"
																itemLabel="#{msg['commons.All']}" />
															<f:selectItems
																value="#{pages$ApplicationBean.inheritenceFileTypeList}" />
														</h:selectOneMenu>
														<h:outputLabel styleClass="LABEL" style="width:150px;"
															value="#{msg['attachment.docType']}:" />
														<h:selectOneMenu id="selectDocumentType"
															style="width:192px;"
															value="#{pages$ProcedureDocument.selectOneDocumentType}">
															<f:selectItems
																value="#{pages$ApplicationBean.documentTypeList}" />
														</h:selectOneMenu>

														<h:outputLabel styleClass="LABEL" style="width:150px;"
															value="#{msg['attachment.docDesc']}:" />
														<h:inputTextarea id="txtDescription"
															value="#{pages$ProcedureDocument.documentDescription}"
															rows="5" style="width:200px;" />
															
														 
													</h:panelGrid>
												</div>
											</div>

											<div class="MARGIN">
												<table style="width: 98%">
													<tr>
														<td class="BUTTON_TD">
															
															<h:commandButton id="btnAdd" type="submit"
																styleClass="BUTTON" value="#{msg['commons.saveButton']}"
																action="#{pages$ProcedureDocument.saveDocument}"
																 />
																<h:commandButton id="btnCancel" type="submit" styleClass="BUTTON" value="#{msg['commons.cancel']}" onclick="javascript:window.close();" />
														</td>
													</tr>
												</table>
											</div>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</body>
	</html>
</f:view>