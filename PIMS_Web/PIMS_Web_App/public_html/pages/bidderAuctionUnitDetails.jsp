<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)\
  - Description: Used for displaying bidder wise unit details and perform appropriate actions
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	

</script>

<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
		<title>PIMS</title>
		
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
		 	<script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>			
 	</head>

	<body class="BODY_STYLE">
			<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
			%>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0" style="margin-left:1px; margin-right:1px;">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['auction.bidderUnitPopup.heading']}" styleClass="HEADER_FONT"/>
									</td>
								</tr>
							</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap" 	background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
									</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION_POPUP" style="height:500px;overflow:hidden;">
										<h:form id="unitSearchFrm" style="width:96%;">
										<div >
											<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
												<tr>
													<td>
														<h:outputText value="#{pages$bidderAuctionUnitDetails.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
														<h:outputText value="#{pages$bidderAuctionUnitDetails.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
														<br>
														<h:outputText binding="#{pages$bidderAuctionUnitDetails.enterConfiscationHtmlLabel}" styleClass="INFO_FONT" value="#{msg['auction.bidderUnitPopup.messages.enterNewConfiscation']}:"/>
		                            					<h:outputText binding="#{pages$bidderAuctionUnitDetails.clickActionColumnHtmlLabel}" styleClass="INFO_FONT" value="#{msg['auction.bidderUnitPopup.messages.clickActionColumn']}:"/>
													</td>
												</tr>
											</table>
										</div>
										<t:div>
			                            	<t:panelGrid  columns="3"  cellpadding="1px" cellspacing="3px" style="width:240px;padding-bottom:10px;" rendered="#{pages$bidderAuctionUnitDetails.isRefundAuctionApprovalMode}">
				                                <h:outputLabel styleClass="LABEL" style="padding-right:29px; white-space:nowrap;" value="#{msg['auction.refund.confiscatedAmount']}:"></h:outputLabel>
				                                <h:inputText  id="txtNewConfiscation" style="width:150px;" styleClass="A_RIGHT_NUM" value="#{pages$bidderAuctionUnitDetails.newConfiscation}"/>
										       	<t:commandButton  styleClass="BUTTON" action="#{pages$bidderAuctionUnitDetails.btnSaveNewConfiscationClick}" 
										                     title="#{msg['auction.bidderUnitPopup.buttons.newConfiscation.title']}"
										                     value="#{msg['commons.saveButton']}"
										                     binding="#{pages$bidderAuctionUnitDetails.btnSaveNewConfiscation}"/>
										    </t:panelGrid>
									    </t:div>
										<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 900px; PADDING-TOP: 10px">
										<table cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
										
									<div style="WIDTH: 100%;">
									<div class="contentDiv" style="width:99%; height:151px;#height:137px;">
									<t:dataTable id="dt1" 
										value="#{pages$bidderAuctionUnitDetails.dataList}"
										binding="#{pages$bidderAuctionUnitDetails.dataTable}"
										rows="99" 
										preserveDataModel="false" preserveSort="false" var="dataItem"
										rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
										<t:column id="unitNumberCol" width="100" sortable="true" >
											<f:facet name="header">
												<t:outputText value="#{msg['unit.number']}" />
											</f:facet>
											<t:outputText value="#{dataItem.unitNumber}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="replaceWithCol" width="100" sortable="true" >
											<f:facet name="header">
												<t:outputText value="#{msg['auction.unitDeatils.fieldLabel.replacedWith']}" />
											</f:facet>
											<t:outputText value="#{dataItem.replacedWithUnitNumber}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="requestNumberCol" width="100" sortable="true" >
											<f:facet name="header">
												<t:outputText value="#{msg['request.number']}" />
											</f:facet>
											<t:outputText value="#{dataItem.requestNumber}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="requestDateCol" width="100" sortable="true" >
											<f:facet name="header">
												<t:outputText value="#{msg['request.date']}" />
											</f:facet>
											<t:outputText value="#{dataItem.requestDate}" styleClass="A_LEFT">
												<f:convertDateTime pattern="#{pages$bidderAuctionUnitDetails.dateFormat}" 
													timeZone="#{pages$bidderAuctionUnitDetails.timeZone}"/>
											</t:outputText>
										</t:column>
										<t:column id="depositAmountCol" width="80" sortable="true">
											<f:facet name="header">
												<t:outputText value="#{msg['unit.depositAmount']}" />
											</f:facet>
											<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.depositAmount}" />
										</t:column>
										<t:column id="confiscatedAmountCol" width="100" sortable="true">
											<f:facet name="header">
												<t:outputText value="#{msg['auction.refund.confiscatedAmount']}" />
											</f:facet>
											<t:outputText value="#{dataItem.confiscationAmount}" styleClass="A_RIGHT_NUM"/>
										</t:column>
										<t:column id="hasRefundedCol" width="100" sortable="true">
											<f:facet name="header">
												<t:outputText value="#{msg['auction.bidderUnitPopup.grid.hasRefundedCol']}" />
											</f:facet>
											<t:outputText value="#{dataItem.hasRefunded}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="movedToContractCol" width="100" sortable="true">
											<f:facet name="header">
												<t:outputText value="#{msg['auction.bidderUnitPopup.grid.movedToContract']}" />
											</f:facet>
											<t:outputText value="#{dataItem.movedToContract}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="excludedCol" width="100" sortable="true" >
											<f:facet name="header">
												<t:outputText value="#{msg['auction.bidderUnitPopup.grid.excluded']}" />
											</f:facet>
											<t:outputText value="#{dataItem.isExcluded}" styleClass="A_LEFT"/>
										</t:column>
										<t:column id="wonAuctionCol" width="100" sortable="true" >
											<f:facet name="header">
												<t:outputText value="#{msg['auction.bidderUnitPopup.grid.wonAuction']}" />
											</f:facet>
											<t:outputText value="#{dataItem.wonAuction}" styleClass="A_LEFT"/>
										</t:column>
										
										<t:column id="actionCol" rendered="#{!pages$bidderAuctionUnitDetails.isRefundAuctionFinalMode}">
											<f:facet name="header">
												<t:outputText value="#{msg['commons.action']}" />
											</f:facet>
											<%-- 
											<h:commandLink id="moveToContractLink" actionListener="#{pages$bidderAuctionUnitDetails.moveToContract}" rendered="#{pages$bidderAuctionUnitDetails.canMoveToContract && dataItem.wonAuction=='No' && dataItem.movedToContract=='No'}">
												<h:graphicImage id="moveToContractIcon" title="#{msg['auction.bidderUnitPopup.grid.action.moveToContract']}" url="../resources/images/app_icons/Move-Deposit-Amount-to-Contract.png" />
											</h:commandLink>
											--%>
											<h:commandLink id="contractNotSignedLink" actionListener="#{pages$bidderAuctionUnitDetails.contractNotSigned}" rendered="#{dataItem.showContractNotSignedIcon}">
												<h:graphicImage id="contractNotSignedIcon" title="#{msg['auction.bidderUnitPopup.grid.action.contractNotSigned']}" url="../resources/images/app_icons/Move-Deposit-Amount-to-Contract.png" />
											</h:commandLink>
											<t:commandLink action="#{pages$bidderAuctionUnitDetails.setSelectedColumn}" 
											rendered="#{pages$bidderAuctionUnitDetails.isRefundAuctionApprovalMode && dataItem.showSetNewConfiscAmount}">
												<h:graphicImage id="newConfiscationIcon" title="#{msg['auction.bidderUnitPopup.grid.action.setSelectedColumn']}" url="../resources/images/app_icons/Modify-confiscated-Amunt.png" />
											</t:commandLink>
											<t:commandLink actionListener="#{pages$bidderAuctionUnitDetails.markAsRefunded}" rendered="#{pages$bidderAuctionUnitDetails.isRefundAuctionFinalMode && dataItem.hasRefunded=='No'}">
												<h:graphicImage id="markAsRefundIcon" title="#{msg['auction.bidderUnitPopup.grid.action.markAsRefunded']}" url="../resources/images/app_icons/Refund-Deposit-Amount.png" />
											</t:commandLink>
										</t:column>
										<t:column id="actionAfterRefundApprovalCol" rendered="#{pages$bidderAuctionUnitDetails.isRefundAuctionFinalMode}">
											<f:facet name="header">
												<t:outputText value="#{msg['commons.action']}" />
											</f:facet>
											
											<h:commandLink id="contractNotSignedLinkAfterRefundApproval" actionListener="#{pages$bidderAuctionUnitDetails.contractNotSigned}" rendered="#{dataItem.showContractNotSignedIcon}">
												<h:graphicImage id="contractNotSignedIconAfterRefundApproval" title="#{msg['auction.bidderUnitPopup.grid.action.contractNotSigned']}" url="../resources/images/app_icons/Move-Deposit-Amount-to-Contract.png" />
											</h:commandLink>
											
										</t:column>
									</t:dataTable>
								</div>
								<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:100%;" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
															<tr>
																<td class="RECORD_NUM_TD">
																	<h:outputText value="#{msg['commons.recordsFound']}"/>
																</td>
																<td class="RECORD_NUM_TD">
																	<h:outputText value=" : "/>
																</td>
																<td class="RECORD_NUM_TD">
																	<h:outputText value="#{pages$bidderAuctionUnitDetails.recordSize}"/>
																</td>
															</tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:50%"> 
												<CENTER>	
												<t:dataScroller id="scroller" for="dt1" paginator="true"
													fastStep="1" paginatorMaxPages="#{pages$bidderAuctionUnitDetails.paginatorMaxPages}" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 												
													paginatorTableStyle="grid_paginator" layout="singleTable" 
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
													paginatorRenderLinkForActive="false" 
													paginatorActiveColumnStyle="font-weight:bold;" pageIndexVar="pageNumber"
													styleClass="SCH_SCROLLER">
														
													<f:facet name="first">
														
														<t:graphicImage url="../resources/images/first_btn.gif" id="lblF"></t:graphicImage>
													</f:facet>

													<f:facet name="fastrewind">
														<t:graphicImage url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>
													</f:facet>

													<f:facet name="fastforward">
														<t:graphicImage url="../resources/images/next_btn.gif" id="lblFF"></t:graphicImage>
													</f:facet>

													<f:facet name="last">
														<t:graphicImage url="../resources/images/last_btn.gif" id="lblL"></t:graphicImage>
													</f:facet>
														<t:div styleClass="PAGE_NUM_BG">
															<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
														</t:div>
											</t:dataScroller>
											</CENTER>
									</td></tr>
								</table>
									</t:div>
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="BUTTON_TD" colspan="4">
											<h:commandButton styleClass="BUTTON" value="#{msg['auctionUnit.addToAuction']}" rendered="false" actionListener="#{pages$bidderAuctionUnitDetails.addToAuction}" style="width: 107px" tabindex="7"></h:commandButton>&nbsp;
											<h:commandButton styleClass="BUTTON" value="#{msg['commons.closeButton']}" onclick="javascript:closeWindowSubmit();" style="width: 75px;" tabindex="11"></h:commandButton>
										</td>
									</tr>
								</table>
								</div>
								</div>
				</h:form>
				</div>
				</td>
				</tr>
				</table>
				</td>
				</tr>
				</table>
	</body>
</html>
</f:view>