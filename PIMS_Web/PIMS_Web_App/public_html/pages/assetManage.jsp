<%-- 
  - Author: Muhammad Shiraz	
  - Date: 04/07/2009
  - Copyright Notice:
  - @(#)
  - Description: Used for Adding, Updating and Viewing Asset 
  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
	function  receiveSubClassList()
	{
	  document.getElementById('frm:lnkReceiveAssetSubClass').onclick();
	}
	
	function openAssetSubClassManagePopup()
	{
	 var screen_width = 1024;
     var screen_height = 220;
     window.open('assetSubClassManage.jsf','_blank','width='+(screen_width-250)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	function closeWindow() 
	{
	 window.close();
	}
	function openPortfolioSearchPopup()
    {
      var screen_width = 990;
      var screen_height = 475;
      var popup_width = screen_width;
      var popup_height = screen_height;
      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
      window.open('portfolioSearch.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
    }
	function receiveSelectedPortfolio() 
    { 
     document.forms[0].submit();
    }
	
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
						<c:when test="${!pages$assetManage.isPopupViewOnlyMode}">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</c:when>
					</c:choose>
				</tr>

				<tr>
					<c:choose>
						<c:when test="${!pages$assetManage.isPopupViewOnlyMode}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['assetAdd.header']}"   styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99.1%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:460px">

										<h:form id="frm" style="width:97%" enctype="multipart/form-data" >
											<table border="0" class="layoutTable">

												<tr>

													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$assetManage.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$assetManage.successMessages}" />
													</td>
												</tr>
											</table>

											<div class="MARGIN" style="width: 98%">
												
												<table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
										        </table>
												
												<rich:tabPanel id="tabpanel" style="width:100%;height:235px;" headerSpacing="0">
												
														<!-- Revenue And Expenses Tab - Start -->
														<rich:tab label="#{msg['revenueAndExpensesManage.Detail.Tab.heading']}">
															<t:div  style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid id="followUpDetailsTable" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4">																	
																 
															<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>
																    <h:outputLabel styleClass="LABEL" value="#{msg['assetSearch.assetNumber']}"></h:outputLabel>
															</h:panelGroup>	
																<h:inputText   id="txtAssetNumber"
																               style="width: 186px;"
																	           readonly="true" 
																	           styleClass="READONLY"
																               value="#{pages$assetManage.assetView.assetNumber}"></h:inputText>
															<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>
																    <h:outputLabel styleClass="LABEL"
																	           value="#{msg['assetSearch.assetStatus']}"></h:outputLabel>
															</h:panelGroup> 
															
															
																<h:selectOneMenu id="cmbAssetStatus" 
																                 style="width: 192px;" 
																                 readonly="#{pages$assetManage.isViewMode}" 
																	             styleClass="#{pages$assetManage.isViewReadOnly}"
																	             value="#{pages$assetManage.assetView.assetStatus}">
																<f:selectItem    itemLabel="#{msg['commons.pleaseSelect']}"
																		         itemValue="-1" />
															    <f:selectItems   value="#{pages$ApplicationBean.assetStatus}" />
																</h:selectOneMenu>
														  <h:panelGroup>	
																<h:outputLabel styleClass="mandatory" value="*"/>
																<h:outputLabel   styleClass="LABEL"
																	             value="#{msg['assetSearch.assetName']}"></h:outputLabel>
															</h:panelGroup>		             
															 
																<h:inputText     id="txtAssetName"
																                 style="width: 186px;"
																	             readonly="#{pages$assetManage.isViewMode}" 
																	             styleClass="#{pages$assetManage.isViewReadOnly}"
																	             value="#{pages$assetManage.assetView.assetNameEn}"></h:inputText>
															<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>
																    <h:outputLabel   styleClass="LABEL"
																                 value="#{msg['assetSearch.assetSymbol']}"></h:outputLabel>
															</h:panelGroup>	                 
															
																<h:inputText     id="txtAssetSymbol"
																                 style="width: 186px;"
																                 readonly="#{pages$assetManage.isViewMode}" 
																	             styleClass="#{pages$assetManage.isViewReadOnly}"  
																	             value="#{pages$assetManage.assetView.assetSymbol}">
																</h:inputText>
															
																<h:outputLabel   styleClass="LABEL"
																	             value="#{msg['assetSearch.MarketName']}"></h:outputLabel>
															
															
																<h:inputText    id="txtMarketName"
																                style="width: 186px;" 
																	            readonly="#{pages$assetManage.isViewMode}" 
																	            styleClass="#{pages$assetManage.isViewReadOnly}"
																	            value="#{pages$assetManage.assetView.marketName}"></h:inputText>
															
															<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>
															        <h:outputLabel   styleClass="LABEL"
																	            value="#{msg['assetSearch.currency']}"></h:outputLabel>
															</h:panelGroup>	            
															
															  <h:selectOneMenu  id="cmbCurrency"
															                    style="width: 192px;"
															                    readonly="#{pages$assetManage.isViewMode}" 
																	            styleClass="#{pages$assetManage.isViewReadOnly}" 
																	            value="#{pages$assetManage.assetView.currencyId}">
															  <f:selectItem     itemLabel="#{msg['commons.pleaseSelect']}"
																		        itemValue="-1" />
															  <f:selectItems    value="#{pages$ApplicationBean.currencyList}" />
															  </h:selectOneMenu>
															
															<h:panelGroup>	
																<h:outputLabel styleClass="mandatory" value="*"/>
															   <h:outputLabel   styleClass="LABEL"
																	            value="#{msg['assetSearch.assetClass']}"></h:outputLabel>
															</h:panelGroup>		            
															
															   <h:selectOneMenu id="cmbAssetClass"
															                    style="width: 192px;"
															                    readonly="#{pages$assetManage.isViewMode}" 
																	            styleClass="#{pages$assetManage.isViewReadOnly}" 
																	            value="#{pages$assetManage.assetView.assetClassId}">
															   <f:selectItem    itemLabel="#{msg['commons.pleaseSelect']}"
																		        itemValue="-1" />
															   <f:selectItems 	value="#{pages$ApplicationBean.assetClassesList}" />
															   <a4j:support     event="onchange"  
																	            reRender="tabpanel,cmbAssetSubClass" 
																	            action="#{pages$assetManage.onPopulateAssetSubClass}"	/>
															   </h:selectOneMenu>
															
															   <h:outputLabel   styleClass="LABEL"
																	            value="#{msg['assetSearch.assetSubClass']}"></h:outputLabel>
															
															   <h:selectOneMenu id="cmbAssetSubClass"
															                    style="width: 192px;"
															                    readonly="#{pages$assetManage.isViewMode}" 
																	            styleClass="#{pages$assetManage.isViewReadOnly}"                        
																	            value="#{pages$assetManage.assetView.assetSubClassId}">
															   <f:selectItem    itemLabel="#{msg['commons.pleaseSelect']}"
																		        itemValue="-1" />
															   <f:selectItems   value="#{pages$assetManage.assetSubClassList}" />
															   
															   </h:selectOneMenu>
															
															   <h:outputLabel   styleClass="LABEL"
																	            value="#{msg['assetSearch.assetSector']}"></h:outputLabel>
															
															  <h:selectOneMenu  id="cmbAssetSector" 
															                    style="width: 192px;"
															                    readonly="#{pages$assetManage.isViewMode}" 
																	            styleClass="#{pages$assetManage.isViewReadOnly}"
																	            value="#{pages$assetManage.assetView.assetSectorId}">
															  <f:selectItem     itemLabel="#{msg['commons.pleaseSelect']}"
																		        itemValue="-1" />
															  <f:selectItems    value="#{pages$ApplicationBean.sectorList}" />
															  <a4j:support      event="onchange"  
																	            reRender="tabpanel,cmbAssetSubSector" 
																	            action="#{pages$assetManage.onPopulateAssetSubSector}"	/>	
															  </h:selectOneMenu>
															
															  <h:outputLabel    styleClass="LABEL"
																	            value="#{msg['assetSearch.assetSubSector']}"></h:outputLabel>
															
															  <h:selectOneMenu id="cmbAssetSubSector" 
															                   style="width: 192px;"
															                   readonly="#{pages$assetManage.isViewMode}" 
																	           styleClass="#{pages$assetManage.isViewReadOnly}"
																	           value="#{pages$assetManage.assetView.assetSubSectorId}">
																<f:selectItem  itemLabel="#{msg['commons.pleaseSelect']}"
																		       itemValue="-1" />
																<f:selectItems
																		       value="#{pages$assetManage.assetSubSectorList}" />
															 </h:selectOneMenu>
															
															
															<h:outputLabel styleClass="LABEL" value="#{msg['assetSearch.expiryDateFrom']}:"></h:outputLabel>
															<rich:calendar id = "dateFrom"
															                   disabled="#{pages$assetManage.isViewMode}"
											  	                               inputClass="#{pages$assetManage.isViewReadOnly}" 
										                        			   locale="#{pages$assetSearch.locale}" 
										                        			   datePattern="#{pages$assetSearch.dateFormat}"
																               value="#{pages$assetManage.assetView.expiryDate}"
										                        			   inputStyle="width: 186px; height: 18px"
										                        			   popup="true"  
										                        			   showApplyButton="false" 
										                        			   enableManualInput="false" 
										                        			   cellWidth="24px" cellHeight="22px" 
										                        			   style="width:186px; height:16px"/>
															

																
														    <h:outputLabel     styleClass="LABEL" 
														                       value="#{msg['revenueAndExpensesManage.description']}:"></h:outputLabel>
														    <h:inputTextarea   id="txtDescription"
																	           readonly="#{pages$assetManage.isViewMode}" 
																	           styleClass="#{pages$assetManage.isViewReadOnly}" 
																	           value="#{pages$assetManage.assetView.description}"
																	           style="width: 186px;" />              
																	                   
																                    	                                                                 
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Revenue And Expenses Tab - End -->
														
																				
														                                                        
                                                        <!-- Attachments Tab - Start -->
														<rich:tab label="#{msg['contract.attachmentTabHeader']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<!-- Attachments Tab - End -->
														
														<!-- Comments Tab - Start -->
                                                         <rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														<!-- Comments Tab - End -->
																												
												</rich:tabPanel>
												
												<table cellpadding="0" cellspacing="0"  style="width:100%;">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
												</table>
												
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td colspan="6" class="BUTTON_TD">
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$assetManage.onSave}"
																	rendered="#{! (pages$assetManage.isPopupViewOnlyMode || pages$assetManage.isViewMode)}">
															</h:commandButton>
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.reset']}"
																	action="#{pages$assetManage.onReset}"
																	rendered="#{! (pages$assetManage.isPopupViewOnlyMode || pages$assetManage.isViewMode) && !pages$assetManage.isEditMode}">
															</h:commandButton>															
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	action="#{pages$assetManage.onCancel}"
																	rendered="#{! pages$assetManage.isPopupViewOnlyMode}">
															</h:commandButton>	
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	onclick="closeWindow(); return false;"
																	rendered="#{pages$assetManage.isViewMode}">
															</h:commandButton>	
																																												
														</td>
													</tr>
												</table>
											</div>								
								
									    </h:form>
									</div>
								

									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
           </div>
		</body>
	</html>
</f:view>
