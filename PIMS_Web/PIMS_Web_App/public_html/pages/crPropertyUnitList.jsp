<%-- 
  - Author: Kamran Ahmed
  - Date:
  - Copyright Notice:
  - @(#)\
  - Description: This report will list units of each property matching the following criteria provided as an input.
  --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%-- 
		<% UIViewRoot viewRoot = (UIViewRoot)session.getAttribute("view");
           if (viewRoot.getLocale().toString().equals("en"))
           { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>
        <%
            } 
            else
            { 
        %>
            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_ar.js"></script>
        <%
            } 
        %>
--%>
<script type="text/javascript">

	function openLoadReportPopup(pageName) 
	{	
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
	function showPopup(personType)
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   //window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
	    window.open('SearchPerson.jsf?persontype='+personType+'&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{		    	   
	    if(hdnPersonType=='TENANT')
	    {
			 document.getElementById("searchFrm:hdnTenantId").value = personId;
			 document.getElementById("searchFrm:txtTenantName").value = personName;
			 document.getElementById("searchFrm:retTenant").onclick();
        }
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />


			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />





		</head>

		<body>
			<div style="width: 1024px;">


				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>


					</tr>
					<tr width="100%">

						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['commons.report.name.propertyUnitListReport.rpt']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg">
									</td>
									<td width="100%" height="99%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION" style="height: 450px;">
											<h:form id="searchFrm">
												<h:inputHidden id="hdnTenantId" value="#{pages$crPropertyUnitList.hdnTenantId}" />
												<h:commandLink id="retTenant" action="#{pages$crPropertyUnitList.retrieveTenant}" />

												<div class="MARGIN" style="width: 96%">
													<table cellpadding="0" cellspacing="0">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td style="FONT-SIZE: 0px" width="100%">
																<IMG
																	src="../<h:outputText value="#{path.img_section_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="DETAIL_SECTION" style="width: 99.6%">
														<h:outputLabel value="#{msg['commons.report.criteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="0px" cellspacing="0px"
															class="DETAIL_SECTION_INNER" border="0">
															<tr>
																<td width="97%">
																	<table width="100%">
																		<tr>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.property.Name']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="txPropertyName"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.txtPropertyName}"
																					styleClass="INPUT_TEXT" maxlength="250"></h:inputText>
																			</td>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.unitType']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:selectOneMenu id="unitType" required="false"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.cboUnittype}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.unitTypeList}" />
																				</h:selectOneMenu>
																			</td>

																		</tr>
																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['inspection.propertyRefNum']} :"></h:outputLabel>
																			</td>

																			<td>


																				<h:inputText id="txtPropertyNumber"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.txtPropertNumber}"
																					styleClass="INPUT_TEXT" maxlength="20"></h:inputText>

																			</td>


																			<td>


																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.floorNumber']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtFloorNumber"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.txtFloorNumber}"
																					maxlength="20"></h:inputText>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['property.type']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:selectOneMenu id="cboPropertyType"
																					required="false"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.cboPropertyType}">

																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyTypeList}" />
																				</h:selectOneMenu>
																			</td>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['tenants.name']} :"></h:outputLabel>


																			</td>
																			<td>


																				<h:inputText id="txtTenantName"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.txtTenantName}"
																					maxlength="20"></h:inputText><h:graphicImage
																			url="../resources/images/app_icons/Search-tenant.png"
																			onclick="showPopup('#{pages$crPropertyUnitList.personTenant}');"
																			style="MARGIN: 0px 0px -4px; CURSOR: hand;"
																			alt="#{msg[contract.searchTenant]}"></h:graphicImage>
																			</td>

																		</tr>

																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['property.usage']} :" />

																			</td>
																			<td>
																				<h:selectOneMenu id="cboPropertyUsage"
																					required="false"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.cboPropertyUsage}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.unitUsageTypeList}" />
																				</h:selectOneMenu>

																			</td>

																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.contractNumber']} :"></h:outputLabel>
																			</td>
																			<td>

																				<h:inputText id="txtContractNumber"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.txtContractNumber}"
																					maxlength="20"></h:inputText>
																			</td>

																		</tr>


																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL" value="#{msg['report.FromDate']} :"> :</h:outputLabel>
																			</td>
																			<td>




																				<rich:calendar id="fromDate" popup="true"
																					datePattern="#{pages$crPropertyUnitList.dateFormat}"
																					locale="#{pages$crPropertyUnitList.locale}"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.clndrFromDate}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width: 170px; height: 14px"></rich:calendar>
																			</td>


																			<td>

																				<h:outputLabel styleClass="LABEL" value="#{msg['report.ToDate']} :"> :</h:outputLabel>
																			</td>
																			<td>

																				<rich:calendar id="toDate"
																					datePattern="#{pages$crPropertyUnitList.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$crPropertyUnitList.locale}"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.cldrToDate}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px">

																				</rich:calendar>
																			</td>

																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.community']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="cboCommunity" styleClass="SELECT_MENU" required="false" value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.cboCommunity}">
																					<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																					<f:selectItems value="#{view.attributes['city']}" />
																				</h:selectOneMenu>
																			</td>

																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['property.landNumber']} :"></h:outputLabel>
																			</td>
																			<td>

																				<h:inputText id="landNmuberInput"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.txtLandNumber}"></h:inputText>
																			</td>

																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.costCenter']} :" />
																			</td>
																			<td>
																				<h:inputText id="txtunitCostCenter"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.txtCostCenter}"
																					maxlength="250"></h:inputText>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.unitStatus']} :" />
																			</td>
																			<td>
																				<h:selectOneMenu id="cboUnitStatus" required="false"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.cboUnitStatus}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.unitStatusList}" />
																				</h:selectOneMenu>
																			</td>

																		</tr>
																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.unitRentAmountFrom']} :"></h:outputLabel>
																			</td>

																			<td>


																				<h:inputText id="txtUnitRentAmountFrom"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.unitRentAmountFrom}"
																					styleClass="INPUT_TEXT" maxlength="20"></h:inputText>

																			</td>


																			<td>


																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.unitRentAmountTo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtUnitRentAmountTo"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.unitRentAmountTo}"
																					maxlength="20"></h:inputText>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.contractAmountFrom']} :"></h:outputLabel>
																			</td>

																			<td>


																				<h:inputText id="txtContractAmountFrom"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.contractRentAmountFrom}"
																					styleClass="INPUT_TEXT" maxlength="20"></h:inputText>

																			</td>


																			<td>


																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.contractAmountTo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtContractAmountTo"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.contractRentAmountTo}"
																					maxlength="20"></h:inputText>
																			</td>
																		</tr>

																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.unitNumber']} :"></h:outputLabel>
																			</td>

																			<td>
																				<h:inputText id="txtUnitNumber"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.txtUnitNumber}"
																					styleClass="INPUT_TEXT" maxlength="20"></h:inputText>
																			</td>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.ownerShip']} :"></h:outputLabel>
																			</td>

																			<td>
																				<h:selectOneMenu id="txtOwnerShip" required="false"
																					value="#{pages$crPropertyUnitList.propertyUnitListReportCriteria.txtOwnerShip}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$crPropertyUnitList.ownerShipTypes}" />
																				</h:selectOneMenu>
																			</td>
																		</tr>

																		<tr>
																			<td class="BUTTON_TD" colspan="4">
																				<h:commandButton styleClass="BUTTON"
																					id="submitButton"
																					action="#{pages$crPropertyUnitList.cmdView_Click}"
																					immediate="false" value="#{msg['commons.view']}"
																					tabindex="7"></h:commandButton>
																			</td>
																		</tr>

																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>
															</tr>

														</table>


													</div>
												</div>



											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

		</body>
	</html>
</f:view>
