<%-- 
  - Author: Kamran Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Search and Manage Cost Center Screen
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>

<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
function receivePopupCall(){
	document.forms[0].submit();
	document.getElementById("searchFrm:hdnLink").onclick();
}
	     function resetValues()
      	{
      		document.getElementById("searchFrm:identifierLetter").value="";			
      	    document.getElementById("searchFrm:ownershipType").selectedIndex=0;
        }
      function   openAddUpdatePopup()
	{
	   var screen_width = 960;
	   var screen_height = 200;
	   var screen_top = screen.top;
	    window.open('costCenterAddPopup.jsf','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
        
        </script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<%
					response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
						response.setHeader("Pragma", "no-cache"); //HTTP 1.0
						response.setDateHeader("Expires", 0); //prevents caching at the proxy server
				%>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">


					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="83%" height="470px" valign="top"
							class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['label.identifier']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" height="100%">
										<div class="SCROLLABLE_SECTION AUC_SCH_SS"
											style="height: 95%; width: 100%; # height: 95%; # width: 100%;">
											<h:form id="searchFrm"
												style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
												<t:div styleClass="MESSAGE">
													<table border="0" class="layoutTable">
														<tr>
															<td>
																<h:commandLink id="hdnLink" action="#{pages$costCenterManage.onSearch}"></h:commandLink>
																<h:outputText
																	value="#{pages$costCenterManage.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:outputText
																	value="#{pages$costCenterManage.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
															</td>
														</tr>
													</table>
												</t:div>
												<div class="MARGIN">
													<div class="DETAIL_SECTION">
														<h:outputLabel value="#{msg['commons.searchCriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER">
															<tr>
																<td width="97%">
																	<table width="100%">
																		<tr>

																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['grp.ownerShipMenu']}:"></h:outputLabel>
																			</td>

																			<td width="25%">
																				<h:selectOneMenu id="ownershipType"
																					binding="#{pages$costCenterManage.cboOwnershipTypeId}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyOwnershipType}" />
																				</h:selectOneMenu>
																			</td>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['label.ownershipIdentifier']}:"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText styleClass="INPUT" id="identifierLetter"
																					onkeyup="removeMoreThanOneNonAlpha(this)"
																					onkeypress="removeMoreThanOneNonAlpha(this)"
																					onkeydown="removeMoreThanOneNonAlpha(this)"
																					onblur="removeMoreThanOneNonAlpha(this)"
																					onchange="removeMoreThanOneNonAlpha(this)"
																					binding="#{pages$costCenterManage.txtIdentifierLetter}"></h:inputText>
																			</td>
																		</tr>

																		<tr>
																			<td class="BUTTON_TD" colspan="4" />
																				<table cellpadding="1px" cellspacing="1px">
																					<tr>
																						<td colspan="2">
																							<h:commandButton styleClass="BUTTON" id="btnSearch"
																								value="#{msg['commons.search']}"
																								action="#{pages$costCenterManage.onSearch}"
																								style="width: 75px" tabindex="7"/>
																							<h:commandButton styleClass="BUTTON"
																								value="#{msg['commons.clear']}"
																								onclick="javascript:resetValues();"
																								style="width: 75px" tabindex="7"/>
																							<h:commandButton styleClass="BUTTON"
																								action="#{pages$costCenterManage.openAddPopup}"
																								value="#{msg['attachment.add']}"
																								style="width: 75px" tabindex="7"/>

																						</td>


																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>
															</tr>

														</table>
													</div>
												</div>
												<div
													style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
													<div class="imag">
														&nbsp;
													</div>
													<div class="contentDiv" style="width: 100%; # width: 99%;">
														<t:dataTable id="resultTable"
															value="#{pages$costCenterManage.identifiers}"
															binding="#{pages$costCenterManage.tblcostCenter}"
															preserveDataModel="false" preserveSort="false"
															var="dataItem" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true" width="100%">

															<t:column  sortable="false" style="white-space: normal;">
																<f:facet name="header">
																	<t:outputText value="#{msg['grp.ownerShipMenu']}" />
																</f:facet>
																<h:outputText value="#{pages$costCenterManage.englishLocale ? dataItem.ownershipTypeDD.dataDescEn : dataItem.ownershipTypeDD.dataDescAr}"/>
															</t:column>
															<t:column  sortable="false" style="white-space: normal;">
																<f:facet name="header">
																	<t:outputText value="#{msg['label.ownershipIdentifier']}" />
																</f:facet>
																<h:outputText value="#{dataItem.identifierLetter}"/>
															</t:column>
															<t:column sortable="false" style="white-space: normal;">
																<f:facet name="header">
																	<t:outputText value="#{msg['PeriodicDisbursements.columns.action']}" />
																</f:facet>
																<t:commandLink
																	action="#{pages$costCenterManage.openUpdatePopup}">
																	<h:graphicImage id="edit"
																		title="#{msg['commons.edit']}" style="cursor:hand;"
																		url="../resources/images/edit-icon.gif" />&nbsp;
																	</t:commandLink>
																<t:commandLink rendered="false"
																	action="#{pages$costCenterManage.onDelete}">
																	<h:graphicImage id="delete"
																		title="#{msg['commons.delete']}" style="cursor:hand;"
																		url="../resources/images/delete.gif" />&nbsp;
																	</t:commandLink>	
															</t:column>
														</t:dataTable>
													</div>
												</div>

											</h:form>

										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>