<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<script type="text/javascript">
</script>
<t:div styleClass="TAB_DETAIL_SECTION" style="width:100%">
	<t:panelGrid id="applicationDetailsTable" cellpadding="1px"
		width="100%" cellspacing="5px" styleClass="TAB_DETAIL_SECTION_INNER"
		columns="5">

		<h:outputLabel styleClass="LABEL" value="#{msg['application.number']}" />
		<h:inputText maxlength="20" id="txtApplicationNum" readonly="true"
			styleClass="READONLY"
			value="#{pages$evaluationRequestTab.applicationNumber}"
			style="width:190px; height: 18px"></h:inputText>
		<h:outputLabel value=" " />
		<h:outputLabel styleClass="LABEL" value="#{msg['application.status']}"></h:outputLabel>
		<h:inputText id="txtApplicationStatus"
			style="width:190px; height: 18px" styleClass="READONLY"
			readonly="true" maxlength="20"
			value="#{pages$evaluationRequestTab.applicationStatus}" />

		<h:outputLabel styleClass="LABEL"
			value="#{msg['applicationDetails.date']}" />
		<h:inputText id="txtApplicationDate" style="width:190px; height: 18px"
			styleClass="READONLY" readonly="true"
			value="#{pages$evaluationRequestTab.applicationDate}" />
		<h:outputLabel value=" " />
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel styleClass="LABEL" style="width: 160px;"
				value="#{msg['evaluation.purpose']}:" />
		</h:panelGroup>
		<h:selectOneMenu id="evaluationPurposeCombo" style="width: 196px;"
			rendered="#{!pages$evaluationRequestTab.readonlyMode}"
			required="false"
			value="#{pages$evaluationRequestTab.evaluationPurposeId}"
			binding="#{pages$evaluationRequestTab.evaluationCombo}">
			<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
			<f:selectItems value="#{pages$ApplicationBean.evaluationPurposeList}" />
		</h:selectOneMenu>
		<h:inputText id="txtSource1" style="width:190px; height: 18px"
			readonly="true" rendered="#{pages$evaluationRequestTab.readonlyMode}"
			value="#{pages$evaluationRequestTab.evaluationPurpose}" />
			
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['applicationDetails.dsescription']}:"></h:outputLabel>
		</h:panelGroup>
		<t:inputTextarea styleClass="TEXTAREA"
			value="#{pages$evaluationRequestTab.requestDescription}"
			readonly="#{pages$evaluationRequestTab.readonlyMode}" rows="6"
			style="width: 190px;" />
			<h:outputLabel value=""></h:outputLabel>
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory"
				value="#{msg['commons.mandatory']}" />
			<h:outputLabel styleClass="LABEL" 
				value="#{msg['property.evaluationOn']}:" />
		</h:panelGroup>
		<h:selectOneMenu id="evaluationOnIdCombo" style="width: 196px;"
			rendered="#{!pages$evaluationRequestTab.readonlyMode}"
			onchange="submit();"
			valueChangeListener="#{pages$evaluationRequestTab.evaluationOnAction}"
			required="false"
			value="#{pages$evaluationRequestTab.evaluationOnId}">
			<f:selectItem itemValue="1" itemLabel="#{msg['property']}" />
			<f:selectItem itemValue="2" itemLabel="#{msg['units.unit']}" />
		</h:selectOneMenu>
		<h:inputText id="txtSource" style="width:190px; height: 18px"
			readonly="true" rendered="#{pages$evaluationRequestTab.readonlyMode}"
			value="#{pages$evaluationRequestTab.evaluationOn}" />
	</t:panelGrid>
</t:div>
