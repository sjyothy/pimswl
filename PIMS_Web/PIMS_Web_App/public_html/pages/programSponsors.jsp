
<script language="JavaScript" type="text/javascript">	
</script>

<t:panelGrid id="sponsor" styleClass="TAB_DETAIL_SECTION_INNER"
	cellpadding="1px" width="100%" cellspacing="5px" columns="4">

	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['socialProgram.lbl.sponsorName']}" />
	</h:panelGroup>
	<h:panelGroup>
		<h:inputHidden id="hdnSelectedSponsorId"
			value="#{pages$programSponsors.programSponsorsView.sponsorId}" />
		<h:inputText id="txtSponsorName" styleClass="READONLY"
			binding="#{pages$programSponsors.txtSponsorName}"
			value="#{pages$programSponsors.programSponsorsView.sponsorName}" />
		<h:commandLink action="#{pages$programSponsors.onAddPersonClick}">
			<h:graphicImage title="#{msg['commons.search']}"
				style="MARGIN: 1px 1px -5px;cursor:hand"
				url="../resources/images/app_icons/Search-tenant.png">
			</h:graphicImage>
		</h:commandLink>
	</h:panelGroup>
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['socialProgram.lbl.sponsorAmount']}" />
	</h:panelGroup>

	<h:inputText
		value="#{pages$programSponsors.programSponsorsView.amount}" />

	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['socialProgram.lbl.sponsorDesc']}" />
	</h:panelGroup>
	<h:inputTextarea
		value="#{pages$programSponsors.programSponsorsView.description}"
		style="width: 185px;" />



</t:panelGrid>
<t:panelGrid id="sponsorGrdActions" styleClass="BUTTON_TD"
	cellpadding="1px" width="100%" cellspacing="5px">
	<h:panelGroup>
		<h:commandButton styleClass="BUTTON"
			value="#{msg['commons.saveButton']}"
			action="#{pages$programSponsors.onSave}" />
	</h:panelGroup>
</t:panelGrid>


<t:div id="sponsorContentDiv" styleClass="contentDiv" style="width:98%">
	<t:dataTable id="sponsorsDataTable"
		rows="200"
		value="#{pages$programSponsors.list}"
		binding="#{pages$programSponsors.dataTable}" preserveDataModel="false"
		preserveSort="false" var="dataItem" rowClasses="row1,row2" rules="all"
		renderedIfEmpty="true" width="100%">



		<t:column id="sponsordate" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.lbl.sponsorCreatedOn']}"
					style="white-space: normal;" />
			</f:facet>

			<t:outputText style="white-space: normal;"
				rendered="#{dataItem.isDeleted != '1' }"
				value="#{dataItem.createdOn}">
				<f:convertDateTime pattern="#{pages$programSponsors.dateFormat}"
					timeZone="#{pages$programSponsors.timeZone}" />
			</t:outputText>
		</t:column>
		<t:column id="sponsorCreatedBy" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.lbl.sponsorCreatedBy']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				rendered="#{dataItem.isDeleted != '1' }"
				value="#{dataItem.createdEn}" />
		</t:column>
		<t:column id="sponsorNamegRD" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.lbl.sponsorName']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				rendered="#{dataItem.isDeleted != '1' }"
				value="#{dataItem.sponsorName}" />
		</t:column>

		<t:column id="sponsordesc" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.lbl.sponsorDesc']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText value="#{dataItem.description}"
				rendered="#{dataItem.isDeleted != '1' }"
				style="white-space: normal;" />
		</t:column>
		<t:column id="sponsorAmount" width="15%" sortable="true"
			rendered="#{dataItem.isDeleted != '1' }">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.lbl.sponsorAmount']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText value="#{dataItem.amount}" style="white-space: normal;" />
		</t:column>
		<t:column id="sponsorAction">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.action']}" />
			</f:facet>
			<h:graphicImage id="imgDeletesSponsor"
				rendered="#{dataItem.isDeleted != '1' }"
				title="#{msg['commons.delete']}"
				url="../resources/images/delete_icon.png"
				onclick="if (!confirm('#{msg['commons.confirmDelete']}')) return false; else onDeleted(this);">
			</h:graphicImage>
			<a4j:commandLink id="onSponsorDeleted"
				reRender="hiddenFields,sponsorContentDiv,sponsorsDataTable,successmsg,errormsg"
				onbeforedomupdate="javaScript:onRemoveDisablingDiv(); "
				action="#{pages$programSponsors.onDelete}" />


		</t:column>
	</t:dataTable>
</t:div>
