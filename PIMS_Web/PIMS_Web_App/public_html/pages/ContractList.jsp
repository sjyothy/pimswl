
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<script language="javascript" type="text/javascript">
	
	function openBlockPopup()
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-750;
        var popup_height = screen_height-580;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('blockPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,titlebar=no,dialog=yes');
        popup.focus();
	} 
	function openUnblockPopup()
	{
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-750;
        var popup_height = screen_height-580;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup = window.open('unblockPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,titlebar=no,dialog=yes');
        popup.focus();
	} 
	 var selectedContracts;
	function AddContracts(field,contractId)
	{
	  if(field.checked)
	  {
		    if(selectedContracts!=null && selectedContracts.length>0 )
		    {
	
		       selectedContracts=selectedContracts+","+contractId;
		    }
		     else
		     selectedContracts = contractId;
	   }
	   else
	   {
		   var delimiters=",";
		   var contractArr = selectedContracts.split(delimiters);
		   selectedContracts="";
		   for(i=0;i<contractArr.length;i++)
		   {
		   if(contractArr[i]!=contractId)
		    {
	       	   if(selectedContracts.length>0 )
		       {
		             selectedContracts=selectedContracts+","+contractArr[i];
	
               }
		       else
		             selectedContracts=contractArr[i];
		    }
		   }
	   }
	   document.getElementById("formContractList:hdnSelectedContracts").value = selectedContracts;
	}
	function openLoadReportPopup(pageName) 
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
	function receiveBlockComment(comment)
	{
		document.getElementById("formContractList:comment").value = comment;
		document.getElementById("formContractList:blockNow").click();
	}
	function receiveUnblockComment(comment)
	{
		document.getElementById("formContractList:comment").value = comment;
		document.getElementById("formContractList:unblockNow").click();
	}
	function receiveRemarks()
	{
		document.getElementById("formContractList:cancelLink").onclick();
	}
	function showPopup(personType)
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   //window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
	    window.open('SearchPerson.jsf?persontype='+personType+'&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		    		   
		    if(hdnPersonType=='TENANT')
		    {
			 document.getElementById("formContractList:hdnTenantId").value=personId;
			 document.getElementById("formContractList:tenantName").value = personName;
			 document.getElementById("formContractList:retTenant").onclick();
	        }
	}
	
	function sendToParent(contractId) {
	    window.opener.populateMaintenanceContract(contractId);
	    window.close();
	}
	
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden">

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />

			<style>
.rich-calendar-input {
	width: 75%;
}
</style>
			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

			<script language="JavaScript" type="text/javascript"
				src="../resources/jscripts/popcalendar_en.js"></script>


			<script language="javascript" type="text/javascript">

	function closeWindow()
	{
	  window.opener="x";
	  window.close();
	}
	function RowDoubleClick(bidderName, bidderId)
	{
	    window.opener.document.getElementById("auctionRequestForm:bidderNameInputText").value=bidderName;
	    window.opener.document.getElementById("auctionRequestForm:bidderId").value=bidderId;
		window.close();
	}
//	function Calendar(control,control2,x,y)
 //     {

  //      var ctl2Name="formContractList:"+control2;
 //       var ctl2=document.getElementById(ctl2Name);
  //      showCalendar(control,ctl2,'dd/mm/yyyy','','',x,y); 
  //      return false;
  //    }
	
	function submitForm(){
       
		     //alert("In submitForm");    
		    //var cmbCountry=document.getElementById("formPerson:selectcountry");
		    //var i = 0;
		    //i=cmbCountry.selectedIndex;
		    //alert("value of i"+i);
		    //document.getElementById("formPerson:hdnStateValue").value=cmbCountry.options[i].value ;
		     
		    
		    //document.getElementById("formPerson:hdnCityValue").value= -1 ;
		     
		    document.forms[0].submit();
		    
		    //alert("End submitForm");
		    
		    }
	    
	
</SCRIPT>

		</head>
		<body class="BODY_STYLE">
			<div style="width: 1024px;">

				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<div style="display: none">
							<h:outputText
								value="#{!pages$ContractListBackingBean.isViewModePopUp}" />
						</div>

						<c:choose>
							<c:when test="${!pages$ContractListBackingBean.isViewModePopUp}">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</c:when>
						</c:choose>
					</tr>

					<tr width="100%">
						<c:choose>
							<c:when test="${!pages$ContractListBackingBean.isViewModePopUp}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['contract.header']}"
											styleClass="HEADER_FONT" style="padding:10px" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>


							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg">
									</td>
									<td width="0%" height="99%" valign="top" nowrap="nowrap">
										<h:form id="formContractList">
											<h:inputHidden id="comment"
												value="#{pages$ContractListBackingBean.blockComments}" />
											<h:inputHidden id="hdnTenantId"
												value="#{pages$ContractListBackingBean.hdnTenantId}" />
											<h:commandLink id="retTenant"
												action="#{pages$ContractListBackingBean.retrieveTenant}" />

											<div class="SCROLLABLE_SECTION" style="width: 842px;">
												<table border="0" class="layoutTable" width="90%"
													style="margin-left: 15px; margin-right: 15px;">
													<tr>
														<td>
															<h:outputText id="errorMsg" escape="false"
																styleClass="ERROR_FONT"
																value="#{pages$ContractListBackingBean.errorMessages}" />
														</td>
													</tr>
													<tr>
														<td>
															<h:outputText id="successMsg" escape="false"
																styleClass="INFO_FONT"
																value="#{pages$ContractListBackingBean.successMessages}" />
														</td>
													</tr>
												</table>
												<h:commandLink id="cancelLink"
													action="#{pages$ContractListBackingBean.cancelContract}"></h:commandLink>
												<div class="MARGIN" style="width: 95.5%">
													<table cellpadding="0" cellspacing="0">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>

													<div class="DETAIL_SECTION" style="width: 99.8%;">
														<h:outputLabel value="#{msg['commons.searchCriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER" border="0">
															<tr>
																<h:inputHidden id="hdnSelectedContracts"
																	value="#{pages$ContractListBackingBean.hdnSelectedContracts}">
																</h:inputHidden>
																<td width="97%">
																	<table width="100%">

																		<%@ include file="contractListCriteria.jsp"%>

																		<td colspan="4">
																			<DIV class="BUTTON_TD">


																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.search']}"
																					action="#{pages$ContractListBackingBean.searchContracts}"></h:commandButton>
																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.clear']}"
																					action="#{pages$ContractListBackingBean.reset}"></h:commandButton>

																				<h:commandButton
																					rendered="#{!pages$ContractListBackingBean.isPageModeSelectOnePopUp}"
																					styleClass="BUTTON"
																					alt="#{msg['renewalLetter.buttonLabel.printRenewalLetter']}"
																					action="#{pages$ContractListBackingBean.renewContractReminderLeters_Click}"
																					value="#{msg['renewalLetter.buttonLabel.printRenewalLetter']}"
																					style="width:auto;">
																				</h:commandButton>
																			</DIV>
																		</td>
																		</tr>
																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>
															</tr>

														</table>
													</div>
													<t:div>&nbsp;</t:div>

													<t:div styleClass="imag">&nbsp;</t:div>
													<t:div styleClass="contentDiv"
														style="width:99.2%;align:center;">

														<t:dataTable id="dt1" preserveSort="false" var="dataItem"
															rules="all" renderedIfEmpty="true" width="100%"
															rowClasses="row1,row2"
															rows="#{pages$ContractListBackingBean.paginatorRows}"
															value="#{pages$ContractListBackingBean.contractList}"
															binding="#{pages$ContractListBackingBean.dataTable}">


															<t:column id="checkbox" style="width:auto;"
																rendered="#{!pages$ContractListBackingBean.isPageModeSelectOnePopUp}">
																<f:facet name="header">
																	<h:selectBooleanCheckbox id="markUnMarkAll"
																		onclick="javaScript:disableButtonsForMarkUnMark()"
																		value="#{pages$ContractListBackingBean.markUnMarkAll}" />
																</f:facet>
																<h:selectBooleanCheckbox
																	rendered="#{!dataItem.blacklisted}"
																	onclick="javascript:AddContracts(this,'#{dataItem.contractId}');">
																</h:selectBooleanCheckbox>
															</t:column>

															<t:column id="col1" sortable="true" style="width:10%;">
																<f:facet name="header">
																	<t:commandSortHeader columnName="contractNumber"
																		actionListener="#{pages$ContractListBackingBean.sort}"
																		value="#{msg['contract.contractNumber']}" arrow="true">
																		<f:attribute name="sortField"
																			value="contracts.contractNumber" />
																	</t:commandSortHeader>
																</f:facet>

																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.contractNumber}"
																	style="white-space: normal;" />
															</t:column>


															<t:column id="colStarttDate" sortable="true"
																style="width:7%;">
																<f:facet name="header">
																	<t:commandSortHeader columnName="contractStartDate"
																		actionListener="#{pages$ContractListBackingBean.sort}"
																		value="#{msg['contract.startDate']}" arrow="true">
																		<f:attribute name="sortField"
																			value="contracts.startDate" />
																	</t:commandSortHeader>
																</f:facet>

																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.startDateString}"
																	style="white-space: normal;" />
															</t:column>

															<t:column id="col3" sortable="true" style="width:7%;">
																<f:facet name="header">
																	<t:commandSortHeader columnName="contractEndtDate"
																		actionListener="#{pages$ContractListBackingBean.sort}"
																		value="#{msg['contract.endDate']}" arrow="true">
																		<f:attribute name="sortField"
																			value="contracts.endDate" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.endDateString}"
																	style="white-space: normal;" />
															</t:column>
															<t:column id="colStatusEn" sortable="true"
																style="width:7%;"
																rendered="#{pages$ContractListBackingBean.isEnglishLocale}">
																<f:facet name="header">
																	<t:commandSortHeader columnName="contractStatusEn"
																		actionListener="#{pages$ContractListBackingBean.sort}"
																		value="#{msg['contract.contractStatus']}" arrow="true">
																		<f:attribute name="sortField"
																			value="contracts.statusId" />
																	</t:commandSortHeader>
																</f:facet>

																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.statusEn}"
																	style="white-space: normal;" />
															</t:column>
															<t:column id="colStatusAr"
																style="padding-right:5px;width:7%" sortable="true"
																rendered="#{pages$ContractListBackingBean.isArabicLocale}">
																<f:facet name="header">
																	<t:commandSortHeader columnName="contractStatusAr"
																		actionListener="#{pages$ContractListBackingBean.sort}"
																		value="#{msg['contract.contractStatus']}" arrow="true">
																		<f:attribute name="sortField"
																			value="contracts.statusId" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.statusAr}"
																	style="white-space: normal;" />
															</t:column>


															<t:column id="colTypeEn" sortable="true"
																style="width:7%;"
																rendered="#{pages$ContractListBackingBean.isEnglishLocale}">
																<f:facet name="header">
																	<t:commandSortHeader columnName="contractTypeEn"
																		actionListener="#{pages$ContractListBackingBean.sort}"
																		value="#{msg['contract.contractType']}" arrow="true">
																		<f:attribute name="sortField"
																			value="contracts.contractTypeId" />
																	</t:commandSortHeader>
																</f:facet>

																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.contractTypeEn}"
																	style="white-space: normal;" />
															</t:column>

															<t:column id="colTypeAr"
																style="padding-right:5px;width:7%;" sortable="true"
																rendered="#{pages$ContractListBackingBean.isArabicLocale}">
																<f:facet name="header">
																	<t:commandSortHeader columnName="contractTypeAr"
																		actionListener="#{pages$ContractListBackingBean.sort}"
																		value="#{msg['contract.contractType']}" arrow="true">
																		<f:attribute name="sortField"
																			value="contracts.contractTypeId" />
																	</t:commandSortHeader>
																</f:facet>

																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.contractTypeAr}"
																	style="white-space: normal;" />
															</t:column>


															<t:column id="colTenant" sortable="true"
																style="width:10%;">
																<f:facet name="header">
																	<t:commandSortHeader columnName="contracttenantName"
																		actionListener="#{pages$ContractListBackingBean.sort}"
																		value="#{msg['contract.tenantName']}" arrow="true">
																		<f:attribute name="sortField"
																			value="tenants.firstName" />
																	</t:commandSortHeader>
																</f:facet>

																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.tenantView.personFullName}"
																	style="white-space: normal;" />
															</t:column>
															<t:column id="colTenantBlacklisted" sortable="true"
																style="width:10%;">
																<f:facet name="header">
																	<t:outputText
																		value="#{msg['person.blacklisted']}" />
																</f:facet>
																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.blacklisted ? msg['commons.true']:
																									msg['commons.false']
																			}"
																	style="white-space: normal;" />
															</t:column>
															
															<t:column id="colTenantType" sortable="true"
																style="width:8%;">
																<f:facet name="header">
																	<t:commandSortHeader columnName="contracttenantsType"
																		actionListener="#{pages$ContractListBackingBean.sort}"
																		value="#{msg['tenants.tenantsType']}" arrow="true">
																		<f:attribute name="sortField"
																			value="tenants.isCompany" />
																	</t:commandSortHeader>
																</f:facet>

																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.tenantView.isCompany==0? msg['tenants.tpye.individual']:msg['tenants.tpye.company']}"
																	style="white-space: normal;" />
															</t:column>
															<t:column id="colPropertyName" sortable="true"
																style="width:10%;">
																<f:facet name="header">
																	<t:commandSortHeader columnName="contractproperty"
																		actionListener="#{pages$ContractListBackingBean.sort}"
																		value="#{msg['contract.property.Name']}" arrow="true">
																		<f:attribute name="sortField"
																			value="property.propertyNameEn" />
																	</t:commandSortHeader>
																</f:facet>

																<t:outputText styleClass="A_LEFT"
																	value="#{pages$ContractListBackingBean.isEnglishLocale?dataItem.propertyNameEn:dataItem.propertyName}"
																	style="white-space: normal;" />
															</t:column>




															<t:column id="col6" style="text-align:right;width:26%;"
																rendered="#{!pages$ContractListBackingBean.isPageModeSelectOnePopUp}">

																<f:facet name="header">
																	<t:outputText style="text-align:center"
																		value="#{msg['commons.action']}" />
																</f:facet>


																<t:outputText
																	style="text-align:center; color:red; font-weight:bold;"
																	rendered="#{dataItem.renewBlocked}"
																	value="#{msg['blocking.StatusMessage']}" />
																<h:commandLink styleClass=""
																	action="#{pages$ContractListBackingBean.btnCmdLink_Click}">
																	<h:graphicImage title="#{msg['commons.view']}"
																		url="../resources/images/app_icons/Lease-contract.png" />
																</h:commandLink>
																<pims:security screen="Pims.Contract.ClearanceLetter"
																	action="view">
																	<h:commandLink styleClass=""
																		rendered="#{pages$ContractListBackingBean.isCLStatus}"
																		action="#{pages$ContractListBackingBean.issueCLCmdLink_Click}">
																		<h:graphicImage
																			title="#{msg['BPM.WorkList.PIMSIssueClearanceLetterBPEL']}"
																			url="../resources/images/app_icons/Clearance-letter.png" />
																	</h:commandLink>
																</pims:security>
																<pims:security screen="Pims.Contract.NoObjectionLetter"
																	action="view">
																	<h:commandLink styleClass=""
																		rendered="#{pages$ContractListBackingBean.isContrctStatusActive}"
																		action="#{pages$ContractListBackingBean.issueNOLCmdLink_Click}">
																		<h:graphicImage
																			title="#{msg['contract.screenName.issueNOL']}"
																			url="../resources/images/app_icons/No-Objection-letter.png" />
																	</h:commandLink>
																	<h:commandLink styleClass=""
																		rendered="#{pages$ContractListBackingBean.isContrctStatusActive}"
																		action="#{pages$ContractListBackingBean.contractViolationCmdLink_Click}">
																		<h:graphicImage
																			title="#{msg['contract.screenName.contractViolation']}"
																			url="../resources/images/app_icons/Action.png" />
																	</h:commandLink>
																</pims:security>
																<pims:security screen="Pims.ChangeTenantName"
																	action="create">
																	<h:commandLink styleClass=""
																		rendered="#{pages$ContractListBackingBean.isContrctStatusActive}"
																		action="#{pages$ContractListBackingBean.cmdChangeTenant_Click}">
																		<h:graphicImage
																			title="#{msg['contractList.changeTenant']}"
																			alt="#{msg['contractList.changeTenant']}"
																			url="../resources/images/app_icons/Change-the-tenant-name.png" />
																	</h:commandLink>
																</pims:security>
																<pims:security screen="Pims.Contract.AmendLeaseContract"
																	action="create">
																	<h:commandLink styleClass=""
																		rendered="#{pages$ContractListBackingBean.isContrctStatusActive}"
																		action="#{pages$ContractListBackingBean.cmdAmendLeaseContract_Click}">
																		<h:graphicImage
																			title="#{msg['contractList.AmendLeaseContract']}"
																			alt="#{msg['contractList.AmendLeaseContract']}"
																			url="../resources/images/app_icons/Amend-Contract.png" />
																	</h:commandLink>
																</pims:security>



																<pims:security screen="Pims.Contract.Cancel"
																	action="view">

																	<h:commandLink styleClass=""
																		rendered="#{pages$ContractListBackingBean.cancelContractStatus}"
																		action="#{pages$ContractListBackingBean.cmdLinkCancelContract_Click}">
																		<h:graphicImage
																			title="#{msg['contract.screenName.cancelContract']}"
																			url="../resources/images/app_icons/Cancel-Contract.png" />
																	</h:commandLink>

																</pims:security>

																<h:commandLink styleClass=""
																	rendered="#{pages$ContractListBackingBean.cancelContractStatus}"
																	action="#{pages$ContractListBackingBean.cmdLinkSettleContract_Click}">
																	<h:graphicImage
																		title="#{msg['contract.screenName.settleContract']}"
																		url="../resources/images/app_icons/Settle-Contract.png" />
																</h:commandLink>

																<h:commandLink styleClass=""
																	rendered="#{pages$ContractListBackingBean.isContrctStatusActive && !dataItem.renewBlocked}"
																	action="#{pages$ContractListBackingBean.cmdTransferContract_Click}">
																	<h:graphicImage
																		title="#{msg['transferContract.heading']}"
																		url="../resources/images/app_icons/Transfer-Contract.png" />
																</h:commandLink>

																<h:commandLink styleClass=""
																	rendered="#{pages$ContractListBackingBean.isContrctStatusActive && !dataItem.blacklisted}"
																	action="#{pages$ContractListBackingBean.cmdRenewContractReminderLeter_Click}">
																	<h:graphicImage
																		title="#{msg['renewContract.reminderLetter']}"
																		url="../resources/images/app_icons/RenewContractReminderLetter.png" />
																</h:commandLink>
																<h:commandLink styleClass=""
																	rendered="#{pages$ContractListBackingBean.isContrctStatusActive && dataItem.blacklisted}"
																	action="#{pages$ContractListBackingBean.cmdRenewContractReminderLeter_Click}"
																	onclick="if (!confirm('#{msg['contract.print.renewal.confirmation']}')) return false;">
																	<h:graphicImage
																		title="#{msg['renewContract.reminderLetter']}"
																		url="../resources/images/app_icons/RenewContractReminderLetter.png" />
																</h:commandLink>
																<h:commandLink styleClass=""
																	rendered="#{pages$ContractListBackingBean.isContrctStatusActive }"
																	action="#{pages$ContractListBackingBean.cmdNewMaintenanceApplication_Click}">
																	<h:graphicImage
																		title="#{msg['maintenanceRequest.pageTitle.newMaintenanceApp']}"
																		url="../resources/images/app_icons/Maintenance-Request-Managem.png" />
																</h:commandLink>



																<pims:security screen="Pims.Contract.Renew"
																	action="create">
																	<h:commandLink
																		rendered="#{(pages$ContractListBackingBean.isContrctStatusActive || pages$ContractListBackingBean.isContrctStatusExpired || pages$ContractListBackingBean.isContrctStatusComplete) && !dataItem.renewBlocked}"
																		action="#{pages$ContractListBackingBean.renewContract}">
																		<h:graphicImage title="#{msg['renewContract.header']}"
																			url="../resources/images/app_icons/Renew-A-Contract.png" />
																	</h:commandLink>
																</pims:security>
																<h:commandLink
																	rendered="#{pages$ContractListBackingBean.isContrctStatusActive && dataItem.isMigrated}"
																	action="#{pages$ContractListBackingBean.openMissingPaymentsScreen}">
																	<h:graphicImage
																		title="#{msg['addMissingMigratedPayments.imgLabel.add']}"
																		url="../resources/images/app_icons/Add-fee.png" />
																</h:commandLink>
																<h:commandLink
																	rendered="#{ pages$ContractListBackingBean.isContrctStatusNew }"
																	action="#{pages$ContractListBackingBean.cmdCancelContract_Click}">
																	<h:graphicImage
																		title="#{msg['commons.cancelContract']}"
																		url="../resources/images/delete_icon.png" />
																</h:commandLink>




															</t:column>



															<t:column id="col8" sortable="false" width="10%"
																rendered="#{pages$ContractListBackingBean.isPageModeSelectOnePopUp}">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.select']}" />
																</f:facet>
																<t:commandLink
																	onclick="javascript:sendToParent('#{dataItem.contractId}');">
																	<h:graphicImage style="width: 16;height: 16;border: 0"
																		alt="#{msg['commons.select']}"
																		url="../resources/images/select-icon.gif" />
																</t:commandLink>
															</t:column>

														</t:dataTable>
													</t:div>
													<t:div id="contentDivFooter"
														styleClass="contentDivFooter AUCTION_SCH_RF"
														style="width:100%;#width:100%;">
														<table id="RECORD_NUM_TD" cellpadding="0" cellspacing="0"
															width="100%">
															<tr>
																<td class="RECORD_NUM_TD">
																	<div class="RECORD_NUM_BG">
																		<table cellpadding="0" cellspacing="0"
																			style="width: 182px; # width: 150px;">
																			<tr>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText value=" : " />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{pages$ContractListBackingBean.recordSize}" />
																				</td>
																			</tr>
																		</table>
																	</div>
																</td>
																<td id="contentDivFooterColumnTwo" class="BUTTON_TD"
																	style="width: 53%; # width: 50%;" align="right">

																	<t:div styleClass="PAGE_NUM_BG" style="#width:20%">

																		<table cellpadding="0" cellspacing="0">
																			<tr>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																				</td>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{pages$ContractListBackingBean.currentPage}" />
																				</td>
																			</tr>
																		</table>
																	</t:div>
																	<TABLE border="0" class="SCH_SCROLLER">
																		<tr>
																			<td>
																				<t:commandLink
																					action="#{pages$ContractListBackingBean.pageFirst}"
																					disabled="#{pages$ContractListBackingBean.firstRow == 0}">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$ContractListBackingBean.pagePrevious}"
																					disabled="#{pages$ContractListBackingBean.firstRow == 0}">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:dataList
																					value="#{pages$ContractListBackingBean.pages}"
																					var="page">
																					<h:commandLink value="#{page}"
																						actionListener="#{pages$ContractListBackingBean.page}"
																						rendered="#{page != pages$ContractListBackingBean.currentPage}" />
																					<h:outputText value="<b>#{page}</b>" escape="false"
																						rendered="#{page == pages$ContractListBackingBean.currentPage}" />
																				</t:dataList>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$ContractListBackingBean.pageNext}"
																					disabled="#{pages$ContractListBackingBean.firstRow + pages$ContractListBackingBean.rowsPerPage >= pages$ContractListBackingBean.totalRows}">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$ContractListBackingBean.pageLast}"
																					disabled="#{pages$ContractListBackingBean.firstRow + pages$ContractListBackingBean.rowsPerPage >= pages$ContractListBackingBean.totalRows}">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblL"></t:graphicImage>
																				</t:commandLink>

																			</td>
																		</tr>
																	</TABLE>
																</td>
															</tr>
														</table>
													</t:div>

												</div>
												<t:div styleClass="BUTTON_TD">


													<h:commandButton id="blockNow" styleClass="BUTTON"
														style="display:none;width:auto;"
														alt="#{msg['unblockContract.buttonLabel']}"
														action="#{pages$ContractListBackingBean.blockWithComments}"
														value="#{msg['unblockContract.buttonLabel']}">
													</h:commandButton>
													<h:commandButton id="unblockNow" styleClass="BUTTON"
														style="display:none;width:auto;"
														alt="#{msg['unblockContract.buttonLabel']}"
														action="#{pages$ContractListBackingBean.unblockWithComments}"
														value="#{msg['unblockContract.buttonLabel']}">
													</h:commandButton>

													<h:commandButton styleClass="BUTTON"
														alt="#{msg['unblockContract.buttonLabel']}"
														action="#{pages$ContractListBackingBean.unblockContract_Click}"
														value="#{msg['unblockContract.buttonLabel']}"
														style="width:auto;">
													</h:commandButton>

													<h:commandButton styleClass="BUTTON"
														alt="#{msg['blockContract.buttonLabel']}"
														action="#{pages$ContractListBackingBean.blockContract_Click}"
														value="#{msg['blockContract.buttonLabel']}"
														style="width:auto;">
													</h:commandButton>
													<h:commandButton styleClass="BUTTON"
														action="#{pages$ContractListBackingBean.renewContractReminderLeters_Click}"
														value="#{msg['renewalLetter.buttonLabel.printRenewalLetter']}"
														style="width:auto;">
													</h:commandButton>
												</t:div>
											</div>
										</h:form>
									</td>
								</tr>

							</table>
						</td>
					</tr>
					<c:choose>
						<c:when test="${!pages$ContractListBackingBean.isViewModePopUp}">
							<tr>
								<td colspan="2">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</c:when>
					</c:choose>
				</table>
			</div>
		</body>

	</html>
</f:view>
