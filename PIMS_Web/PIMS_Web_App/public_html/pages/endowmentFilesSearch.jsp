<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Auction
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	    function resetValues()
      	{
      		document.getElementById("searchFrm:txtrequestNum").value="";
      		document.getElementById("searchFrm:txtendowmentFileNum").value="";
		    document.getElementById("searchFrm:cmbstatus").selectedIndex=0;
		    document.getElementById("searchFrm:cmbcreatedBy").selectedIndex=0;
      	    document.getElementById("searchFrm:txtFileOwner").value="";
      	    document.getElementById("searchFrm:endowmentFileName").value="";
      	    
			$('searchFrm:createdOnFromId').component.resetSelectedDate();
			$('searchFrm:createdOnToId').component.resetSelectedDate();
        }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">

					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>


					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>


						<td width="83%" height="470px" valign="top"
							class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['endowmentFile.search.title']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" height="100%">
										<div class="SCROLLABLE_SECTION AUC_SCH_SS"
											style="height: 95%; width: 100%; # height: 95%; # width: 100%;">
											<h:form id="searchFrm"
												style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
												<t:div id="layoutTable" styleClass="MESSAGE">
													<table border="0" class="layoutTable">
														<tr>
															<td>
																<h:messages></h:messages>
																<h:outputText id="errorMessage"
																	value="#{pages$endowmentFilesSearch.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
															</td>
														</tr>
													</table>
												</t:div>
												<div class="MARGIN">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="DETAIL_SECTION">
														<h:outputLabel value="#{msg['commons.searchCriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER">
															<tr>
																<td width="97%">
																	<table width="100%">
																		<tr>

																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['endowmentFile.lbl.num']}"></h:outputLabel>
																			</td>

																			<td width="25%">
																				<h:inputText id="txtendowmentFileNum"
																					value="#{pages$endowmentFilesSearch.criteria.endowmentFile.fileNumber}"
																					maxlength="20"></h:inputText>

																			</td>

																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.requestNumber']}"></h:outputLabel>
																			</td>

																			<td width="25%">
																				<h:inputText id="txtrequestNum"
																					value="#{pages$endowmentFilesSearch.criteria.requestNum}"
																					maxlength="20"></h:inputText>

																			</td>


																		</tr>
																		<tr>


																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.status']}"></h:outputLabel>
																			</td>

																			<td width="25%" colspan="1">
																				<h:selectOneMenu id="cmbstatus"
																					value="#{pages$endowmentFilesSearch.criteria.endowentFileStatus}"
																					tabindex="3">
																					<f:selectItem itemLabel="#{msg['commons.All']}"
																						itemValue="-1" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.endowmentFileStatus}" />
																				</h:selectOneMenu>
																			</td>
																			<td width="25%" colspan="1">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.createdBy']}:"></h:outputLabel>
																			</td>
																			<td width="25%" colspan="1">
																				<h:selectOneMenu id="cmbcreatedBy"
																					value="#{pages$endowmentFilesSearch.criteria.endowmentFile.createdBy}"
																					tabindex="3">
																					<f:selectItem itemLabel="#{msg['commons.All']}"
																						itemValue="-1" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.secUserList}" />
																				</h:selectOneMenu>
																			</td>


																		</tr>
																		<tr>

																			<td width="25%" colspan="1">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['inheritanceFileSearch.createdOnFrom']}:"></h:outputLabel>
																			</td>
																			<td width="25%" colspan="1">
																				<rich:calendar id="createdOnFromId"
																					locale="#{pages$endowmentFilesSearch.locale}"
																					value="#{pages$endowmentFilesSearch.criteria.createdOnFrom}"
																					popup="true"
																					datePattern="#{pages$endowmentFilesSearch.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:185px;height:17px" />

																			</td>
																			<td width="25%" colspan="1">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.to']}:"></h:outputLabel>
																			</td>
																			<td width="25%" colspan="1">
																				<rich:calendar id="createdOnToId"
																					locale="#{pages$endowmentFilesSearch.locale}"
																					value="#{pages$endowmentFilesSearch.criteria.createdOnTo}"
																					popup="true"
																					datePattern="#{pages$endowmentFilesSearch.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:185px;height:17px" />

																			</td>

																		</tr>

																		<tr>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.Name']}"></h:outputLabel>
																			</td>

																			<td width="25%">
																				<h:inputText id="endowmentFileName"
																					value="#{pages$endowmentFilesSearch.criteria.endowmentFileName}"
																					maxlength="20"></h:inputText>

																			</td>

																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['endowmentFile.lbl.fileOwner']}"></h:outputLabel>
																			</td>

																			<td width="25%">
																				<h:inputText id="txtFileOwner"
																					value="#{pages$endowmentFilesSearch.criteria.endowentFilePersonName}"
																					maxlength="20"></h:inputText>

																			</td>




																		</tr>
                                                                                                                                                <tr>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['endowmentFileSearch.lbl.endowmentSource']}"></h:outputLabel>
																			</td>


																			<td width="25%">
																				<h:selectOneMenu id="cmbendowmentsource"
																					value="#{pages$endowmentFilesSearch.criteria.endowmentSource}"
																					tabindex="3">
																					<f:selectItem itemLabel="#{msg['commons.All']}"
																						itemValue="-1" />
																					<f:selectItem
																						itemLabel="#{msg['endowmentFileSearch.lbl.waqf']}"
																						itemValue="0" />
																					<f:selectItem
																						itemLabel="#{msg['inheritanceFile.tabHeader']}"
																						itemValue="1" />
																				</h:selectOneMenu>

																			</td>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['inheritanceFile.tabHeader']}"></h:outputLabel>
																			</td>


																			<td width="25%">
																				<h:inputText id="txtInheritanceFileNumber"
																					value="#{pages$endowmentFilesSearch.criteria.inheritanceFileNumber}"
																					maxlength="20"></h:inputText>

																			</td>

																		</tr>



																		<tr>

																			<td class="BUTTON_TD" colspan="4" />

																				<table cellpadding="1px" cellspacing="1px">
																					<tr>



																						<td colspan="2">
																							<h:commandButton styleClass="BUTTON"
																								value="#{msg['commons.search']}"
																								action="#{pages$endowmentFilesSearch.onSearch}"
																								style="width: 75px" />

																							<h:commandButton styleClass="BUTTON"
																								type="button" value="#{msg['commons.clear']}"
																								onclick="javascript:resetValues();"
																								style="width: 75px" />
																							<pims:security
																								screen="Pims.EndowMgmt.EndowmentFile.Save"
																								action="view">

																								<h:commandButton styleClass="BUTTON"
																									value="#{msg['commons.Add']}"
																									action="#{pages$endowmentFilesSearch.onAdd}"
																									style="width: 75px" />

																							</pims:security>

																						</td>


																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>
															</tr>

														</table>
													</div>
												</div>
												<div
													style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
													<div class="imag">
														&nbsp;
													</div>
													<div class="contentDiv" style="width: 100%; # width: 99%;">
														<t:dataTable id="dt1"
															value="#{pages$endowmentFilesSearch.dataList}"
															binding="#{pages$endowmentFilesSearch.dataTable}"
															rows="#{pages$endowmentFilesSearch.paginatorRows}"
															preserveDataModel="false" preserveSort="false"
															var="dataItem" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true" width="100%">

															<t:column id="fileNumber" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="fileNumber"
																		actionListener="#{pages$endowmentFilesSearch.sort}"
																		value="#{msg['endowmentFile.lbl.num']}" arrow="true">
																		<f:attribute name="sortField" value="fileNumber" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.fileNumber}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="fileName" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="fileName"
																		actionListener="#{pages$endowmentFilesSearch.sort}"
																		value="#{msg['commons.Name']}" arrow="true">
																		<f:attribute name="sortField" value="fileName" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.fileName}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>

															<t:column id="firstName" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="firstName"
																		actionListener="#{pages$endowmentFilesSearch.sort}"
																		value="#{msg['endowmentFile.lbl.fileOwner']}"
																		arrow="true">
																		<f:attribute name="sortField" value="firstName" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.person.fullName}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>

															<t:column id="statusId" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="statusId"
																		actionListener="#{pages$endowmentFilesSearch.sort}"
																		value="#{msg['commons.status']}" arrow="true">
																		<f:attribute name="sortField" value="statusId" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText
																	value="#{pages$endowmentFilesSearch.englishLocale? 
																	         dataItem.status.dataDescEn:
																	         dataItem.status.dataDescAr
																	        }"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>

															<t:column id="createdOn" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="createdOn"
																		actionListener="#{pages$endowmentFilesSearch.sort}"
																		value="#{msg['commons.createdOn']}" arrow="true">
																		<f:attribute name="sortField" value="createdOn" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.createdOn}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="actionCol" sortable="false" width="100"
																style="TEXT-ALIGN: center;">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.action']}" />
																</f:facet>

																<t:commandLink
																	action="#{pages$endowmentFilesSearch.onEdit}">
																	<h:graphicImage title="#{msg['commons.edit']}"
																		url="../resources/images/edit-icon.gif" />
																</t:commandLink>
																<t:commandLink
																	action="#{pages$endowmentFilesSearch.onIssueNOL}">
																	<h:graphicImage id="editIcon2"
																		title="#{msg['contract.screenName.issueNOL']}"
																		url="../resources/images/app_icons/No-Objection-letter.png" />
																</t:commandLink>
															</t:column>



														</t:dataTable>
													</div>
													<t:div id="contentDivFooter"
														styleClass="contentDivFooter AUCTION_SCH_RF"
														style="width:100%;#width:100%;">
														<table id="RECORD_NUM_TD" cellpadding="0" cellspacing="0"
															width="100%">
															<tr>
																<td class="RECORD_NUM_TD">
																	<div class="RECORD_NUM_BG">
																		<table cellpadding="0" cellspacing="0"
																			style="width: 182px; # width: 150px;">
																			<tr>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText value=" : " />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{pages$endowmentFilesSearch.recordSize}" />
																				</td>
																			</tr>
																		</table>
																	</div>
																</td>
																<td id="contentDivFooterColumnTwo" class="BUTTON_TD"
																	style="width: 53%; # width: 50%;" align="right">

																	<t:div styleClass="PAGE_NUM_BG" style="#width:20%">

																		<table cellpadding="0" cellspacing="0">
																			<tr>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																				</td>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{pages$endowmentFilesSearch.currentPage}" />
																				</td>
																			</tr>
																		</table>
																	</t:div>
																	<TABLE border="0" class="SCH_SCROLLER">
																		<tr>
																			<td>
																				<t:commandLink
																					action="#{pages$endowmentFilesSearch.pageFirst}"
																					disabled="#{pages$endowmentFilesSearch.firstRow == 0}">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$endowmentFilesSearch.pagePrevious}"
																					disabled="#{pages$endowmentFilesSearch.firstRow == 0}">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:dataList
																					value="#{pages$endowmentFilesSearch.pages}"
																					var="page">
																					<h:commandLink value="#{page}"
																						actionListener="#{pages$endowmentFilesSearch.page}"
																						rendered="#{page != pages$endowmentFilesSearch.currentPage}" />
																					<h:outputText value="<b>#{page}</b>" escape="false"
																						rendered="#{page == pages$endowmentFilesSearch.currentPage}" />
																				</t:dataList>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$endowmentFilesSearch.pageNext}"
																					disabled="#{pages$endowmentFilesSearch.firstRow + pages$endowmentFilesSearch.rowsPerPage >= pages$endowmentFilesSearch.totalRows}">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$endowmentFilesSearch.pageLast}"
																					disabled="#{pages$endowmentFilesSearch.firstRow + pages$endowmentFilesSearch.rowsPerPage >= pages$endowmentFilesSearch.totalRows}">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblL"></t:graphicImage>
																				</t:commandLink>
																			</td>
																		</tr>
																	</TABLE>
																</td>
															</tr>
														</table>
													</t:div>
												</div>

											</h:form>

										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>