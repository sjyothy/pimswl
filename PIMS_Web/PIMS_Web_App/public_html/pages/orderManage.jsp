<%-- 
  - Author: Anil Verani
  - Date: 30/07/2009
  - Copyright Notice:
  - @(#)
  - Description: Used for Adding, Updating and Viewing Orders 
  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

	function openPortfolioSearchPopup()
	{
		var screen_width = 990;
		var screen_height = 475;
        var popup_width = screen_width;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('portfolioSearch.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
	
	function receiveSelectedPortfolio()
	{
		document.getElementById('frm:lnkReceiveSelectedPortfolio').onclick(); 
	}
	
	function passOrderViewToOpener()
	{	
		window.opener.receiveOrderView();
		window.close();
	}
	
	function showUploadPopup()
	{
	      var screen_width = 1024;
	      var screen_height = 450;
	      var popup_width = screen_width-530;
	      var popup_height = screen_height-150;
	      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
	      
	      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	      popup.focus();
	}

	function showAddNotePopup()
	{
	      var screen_width = screen.width;
	      var screen_height = screen.height;
	      var popup_width = screen_width/3+120;
	      var popup_height = screen_height/3-80;
	      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
	      
	      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	      popup.focus();
	}
	
	function lnkCalcTotalAmountClick()
	{
	 document.getElementById("frm:lnkCalcTotalAmount").onclick();
	}
	
	function lnk2CalcTotalAmountClick()
	{
	 document.getElementById("frm:lnk2CalcTotalAmount").onclick();
	}
	
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
						<c:when test="${pages$orderManage.isAddMode || pages$orderManage.isEditMode || pages$orderManage.isViewMode || pages$orderManage.isApproveRejectMode}">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>						
						</c:when>
					</c:choose>
				</tr>

				<tr>
					<c:choose>
						<c:when test="${pages$orderManage.isAddMode || pages$orderManage.isEditMode || pages$orderManage.isViewMode || pages$orderManage.isApproveRejectMode}">
							<td class="divLeftMenuWithBody" width="17%">					
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>
					
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel rendered="#{pages$orderManage.isBuyingOrder}" value="#{msg['order.manage.heading.buying']}" styleClass="HEADER_FONT" />
									<h:outputLabel rendered="#{pages$orderManage.isSellingOrder}" value="#{msg['order.manage.heading.selling']}" styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" >

										<h:form id="frm" style="width:97%" enctype="multipart/form-data">
											<table border="0" class="layoutTable">
												<tr>
													<td>
														<h:outputText escape="false" id ="errorMessage" styleClass="ERROR_FONT" value="#{pages$orderManage.errorMessages}" />
													    <h:outputText escape="false" id ="successMessage" styleClass="INFO_FONT" value="#{pages$orderManage.successMessages}" />
													</td>
												</tr>
											</table>
											
											<t:div rendered="#{pages$orderManage.isApproveRejectMode}" style="width:97%; margin:10px;">
												<t:panelGrid cellpadding="1px" width="100%" cellspacing="5px" columns="4">																
													<h:outputLabel styleClass="LABEL" value="#{msg['tenderManagement.approveTender.actionDate']}: "/>
											        <rich:calendar inputStyle="width: 186px; height: 18px" locale="#{pages$orderManage.locale}" popup="true" datePattern="#{pages$orderManage.dateFormat}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px"/>
											                    
													<h:outputLabel styleClass="LABEL"  value="#{msg['commons.comments']}: " />
													<t:inputTextarea styleClass="TEXTAREA" rows="6" style="width: 200px; height:30px;" />
																
													<t:div style="height:10px;"/>
											   	</t:panelGrid>
											   	<table border="0" width="100%">
											   		<tr>
											   			<td class="BUTTON_TD">
											   				<h:commandButton value="#{msg['commons.approve']}" action="#{pages$orderManage.onTaskApprove}" styleClass="BUTTON"></h:commandButton>
											   				<h:commandButton value="#{msg['commons.reject']}" action="#{pages$orderManage.onTaskReject}" styleClass="BUTTON"></h:commandButton>
											   				<h:commandButton value="#{msg['commons.cancel']}" action="#{pages$orderManage.onTaskCancel}" styleClass="BUTTON"></h:commandButton>
											   			</td>
											   		</tr>
											   	</table>
											</t:div>
											
											<div class="MARGIN" style="width: 98%">
												
												<table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
														<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
										        </table>
												
												<rich:tabPanel>
												
														<!-- Portfolio Tab - Start -->
														<rich:tab label="#{msg['portfolio.manage.tab.portfolio.heading']}">
															<t:div style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid id="pnlGrdCntrls" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4">
																
																	<h:panelGroup>	
																		<h:outputLabel rendered="#{pages$orderManage.isAddMode || pages$orderManage.isEditMode}" styleClass="mandatory" value="*"/>	
																		<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.number']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:panelGroup>
																		<h:inputText value="#{pages$orderManage.orderView.portfolioView.portfolioNumber}" style="width: 186px;" readonly="true" styleClass="READONLY" />
																		<h:commandLink action="#{pages$orderManage.onSearchPortfolio}" rendered="#{pages$orderManage.isAddMode || pages$orderManage.isEditMode}">
																			<h:graphicImage title="#{msg['portfolio.search.heading']}" url="../resources/images/magnifier.gif" />
																		</h:commandLink>
																		<a4j:commandLink id="lnkReceiveSelectedPortfolio" action="#{pages$orderManage.onReceiveSelectedPortfolio}" reRender="pnlGrdCntrls" />
																	</h:panelGroup>
																	<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.status']}:"></h:outputLabel>
																	<h:selectOneMenu readonly="true" styleClass="READONLY" value="#{pages$orderManage.orderView.portfolioView.statusId}" style="width: 192px;">
																		<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
																		<f:selectItems value="#{pages$ApplicationBean.portfolioStatusList}" />
																	</h:selectOneMenu>
																			
																	<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.type']}:"></h:outputLabel>
											                        <h:selectOneMenu readonly="true" styleClass="READONLY" value="#{pages$orderManage.orderView.portfolioView.portfolioTypeId}" style="width: 192px;">
																		<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
																		<f:selectItems value="#{pages$ApplicationBean.portfolioTypeList}" />
																	</h:selectOneMenu>																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['commons.Name']}:"></h:outputLabel>
																	<h:inputText readonly="true" styleClass="READONLY" value="#{pages$orderManage.isEnglishLocale ? pages$orderManage.orderView.portfolioView.portfolioNameEn : pages$orderManage.orderView.portfolioView.portfolioNameAr}" style="width: 186px;" maxlength="50" />
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.unit.price']}:"></h:outputLabel>																	
																	<h:inputText readonly="true" styleClass="READONLY" value="#{pages$orderManage.orderView.portfolioView.unitPrice}" style="width: 186px;" maxlength="10" />
																	<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.total.unit']}:"></h:outputLabel>																	
																	<h:inputText readonly="true" styleClass="READONLY" value="#{pages$orderManage.orderView.portfolioView.totalUnits}" style="width: 186px;" maxlength="10" />
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['commons.startDate']}:"></h:outputLabel>
																	<h:inputText readonly="true" styleClass="READONLY" value="#{pages$orderManage.orderView.portfolioView.fundsDurationStartDate}" style="width: 186px;" />
																	<h:outputLabel styleClass="LABEL" value="#{msg['commons.endDate']}:"></h:outputLabel>
																	<h:inputText readonly="true" styleClass="READONLY" value="#{pages$orderManage.orderView.portfolioView.fundsDurationEndDate}" style="width: 186px;" />
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['portfolio.manage.tab.portfolio.principal.amount']}:"></h:outputLabel>
																	<h:inputText readonly="true" styleClass="READONLY" value="#{pages$orderManage.orderView.portfolioView.principalAmount}" style="width: 186px;" maxlength="10" />																																									
																
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Portfolio Tab - End -->
												
														<!-- Order Details Tab - Start -->
														<rich:tab label="#{msg['order.manage.tab.heading.order']}">
															<t:div style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4">																	
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['order.transaction.no']}:"></h:outputLabel>
																	<h:inputText value="#{pages$orderManage.orderView.transactionRefNo}" style="width: 186px;" maxlength="20" styleClass="READONLY" readonly="true" />
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['order.status']}:"></h:outputLabel>
																	<h:selectOneMenu readonly="true" styleClass="READONLY" value="#{pages$orderManage.orderView.orderStatusId}" style="width: 192px;">
																		<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
																		<f:selectItems value="#{pages$ApplicationBean.orderStatusList}" />
																	</h:selectOneMenu>
																	<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel styleClass="LABEL" value="#{msg['order.transaction.date']}:"></h:outputLabel>
																	</h:panelGroup>
																	<rich:calendar value="#{pages$orderManage.orderView.transactionDate}"
																				   inputStyle="width: 186px; height: 18px"
											                        			   locale="#{pages$orderManage.locale}" 
											                        			   popup="true" 
											                        			   datePattern="#{pages$orderManage.dateFormat}" 
											                        			   showApplyButton="false" 
											                        			   enableManualInput="false" 
											                        			   cellWidth="24px" cellHeight="22px" 
											                        			   style="width:186px; height:16px"/>
											                       <h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	 			   
											                        <h:outputLabel styleClass="LABEL" value="#{msg['order.transaction.time']}:"></h:outputLabel>
											                        </h:panelGroup>
											                        <h:panelGroup>
											                        	<h:selectOneMenu style="VERTICAL-ALIGN: middle; width: 50px; height: 24px"
																			value="#{pages$orderManage.orderView.transactionTimeHH}">
																			<f:selectItem itemValue="" itemLabel="HH" />
																			<f:selectItems value="#{pages$ApplicationBean.hours}" />
																		</h:selectOneMenu>
																		<h:outputText value=" : "></h:outputText>
																		<h:selectOneMenu style="VERTICAL-ALIGN: middle; width: 50px; height: 24px"
																			value="#{pages$orderManage.orderView.transactionTimeMM}">
																			<f:selectItem itemValue="" itemLabel="MM" />
																			<f:selectItems value="#{pages$ApplicationBean.minutes}" />
																		</h:selectOneMenu> 
																	</h:panelGroup>																	
																	<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel styleClass="LABEL" value="#{msg['order.transaction.log']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:selectOneMenu value="#{pages$orderManage.orderView.transactionLogId}" style="width: 192px;">
																		<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
																		<f:selectItems value="#{pages$ApplicationBean.transactionLogList}" />
																	</h:selectOneMenu>
																	<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel styleClass="LABEL" value="#{msg['order.current.value.nav']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText value="#{pages$orderManage.orderView.currentValueNav}" style="width: 186px;" />
																	<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel rendered="#{pages$orderManage.isSellingOrder}" styleClass="LABEL" value="#{msg['order.total.amount.returned']}:"></h:outputLabel>
																	<h:outputLabel rendered="#{pages$orderManage.isBuyingOrder}" styleClass="LABEL" value="#{msg['order.total.amount.investment']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:panelGroup>
																		<h:inputText onchange="lnk2CalcTotalAmountClick()" value="#{pages$orderManage.orderView.totalAmountInvestReturn}" style="width: 186px;" />
																		<a4j:commandLink id="lnk2CalcTotalAmount"
						                                                                     action="#{pages$orderManage.onCalcTotalAmount}"
						                                                                     reRender="txtTotalAmount,errorMessage" />
					                                                </h:panelGroup>                     	
																	
																	<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel rendered="#{pages$orderManage.isSellingOrder}" styleClass="LABEL" value="#{msg['order.total.units.remaining']}:"></h:outputLabel>
																	<h:outputLabel rendered="#{pages$orderManage.isBuyingOrder}" styleClass="LABEL" value="#{msg['order.total.units']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText value="#{pages$orderManage.orderView.totalUnits}" style="width: 186px;" />
																	
																	<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel styleClass="LABEL" value="#{msg['order.cost.basedOn']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:selectOneMenu value="#{pages$orderManage.orderView.investmentCostBasedOn}"
																	                 binding="#{pages$orderManage.selectInvestmentCostBasedOn}"
																	                 styleClass="#{pages$orderManage.readOnlyCostBasedOn}"   style="width: 192px;">
																		<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
																		<f:selectItems value="#{pages$ApplicationBean.investmentBasedOn}" />
																		<a4j:support event="onchange"
				                                                                     reRender="txtPercentage,txtTotalAmount"
				                                                                     action="#{pages$orderManage.onInvestmentCostBasedOn}" />
																    </h:selectOneMenu>
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['order.percentage']}:"></h:outputLabel>
																	<h:panelGroup>
																		<h:inputText onchange="lnkCalcTotalAmountClick()" id="txtPercentage"
																		             binding="#{pages$orderManage.txtPercentage}"
																		             styleClass="#{pages$orderManage.readOnlyPercentage}"  
																		             value="#{pages$orderManage.orderView.percentage}" style="width: 186px;" />
																		<a4j:commandLink id="lnkCalcTotalAmount"
					                                                                     action="#{pages$orderManage.onCalcTotalAmount}"
					                                                                     reRender="txtTotalAmount,errorMessage" />
																	</h:panelGroup>
																	
																	<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel rendered="#{pages$orderManage.isSellingOrder}" styleClass="LABEL" value="#{msg['order.total.units.sold']}:"></h:outputLabel>
																	<h:outputLabel rendered="#{pages$orderManage.isBuyingOrder}" styleClass="LABEL" value="#{msg['order.total.units.bought']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText id ="txtTotalUnitsBoughtSold" value="#{pages$orderManage.orderView.totalUnitsBoughtSold}" style="width: 186px;" />
																	
																	<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel rendered="#{pages$orderManage.isSellingOrder}" styleClass="LABEL" value="#{msg['order.total.worth.remaining']}:"></h:outputLabel>
																	<h:outputLabel rendered="#{pages$orderManage.isBuyingOrder}" styleClass="LABEL" value="#{msg['order.total.worth']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText id="txtTotalAmount"
																	             binding="#{pages$orderManage.txtTotalAmount}"
																	             styleClass="#{pages$orderManage.readOnlyTotalAmount}"  
																	             value="#{pages$orderManage.orderView.totalAmount}" style="width: 186px;" />
																																		
																	<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel styleClass="LABEL" value="#{msg['order.budget.rate.return']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText value="#{pages$orderManage.orderView.budgetRateReturn}" style="width: 186px;" />
																	
																	<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel styleClass="LABEL" value="#{msg['order.additional.expenses']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText value="#{pages$orderManage.orderView.additionalExpenses}" style="width: 186px;" />
																	
																	<h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel styleClass="LABEL" value="#{msg['order.description']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText value="#{pages$orderManage.orderView.description}" style="width: 186px;" />
																	
																	<h:panelGroup rendered="#{pages$orderManage.isSellingOrder}">	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel  styleClass="LABEL" value="#{msg['order.actual.return']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText rendered="#{pages$orderManage.isSellingOrder}" value="#{pages$orderManage.orderView.actualReturn}" style="width: 186px;" />
																	
																	
																	 <h:panelGroup>	
																	 	 			   
											                        <h:outputLabel styleClass="LABEL" value="#{msg['order.transaction.timetofullfill']}:"></h:outputLabel>
											                        </h:panelGroup>
																	<h:panelGroup>																	
																	   <h:inputText value="#{pages$orderManage.orderView.daysToCompleteOrder}"
																	    style="VERTICAL-ALIGN: top; width: 50px; height: 15px" />
																		<h:outputText value="  "></h:outputText>											                        	<h:selectOneMenu style="VERTICAL-ALIGN: middle; width: 50px; height: 24px"
																			value="#{pages$orderManage.orderView.timeToCompleteOrderHH}">
																			<f:selectItem itemValue="" itemLabel="HH" />
																			<f:selectItems value="#{pages$ApplicationBean.hours}" />
																		</h:selectOneMenu>
																		<h:outputText value=" : "></h:outputText>
																		<h:selectOneMenu style="VERTICAL-ALIGN: middle; width: 50px; height: 24px"
																			value="#{pages$orderManage.orderView.timeToCompleteOrderMM}">
																			<f:selectItem itemValue="" itemLabel="MM" />
																			<f:selectItems value="#{pages$ApplicationBean.minutes}" />
																		</h:selectOneMenu> 
																	</h:panelGroup>						
																	
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Order Details Tab - End -->
                                                        
                                                        <!-- Attachments Tab - Start -->
														<rich:tab label="#{msg['contract.attachmentTabHeader']}"
																  rendered="#{pages$orderManage.isAddMode || pages$orderManage.isEditMode || pages$orderManage.isViewMode || pages$orderManage.isApproveRejectMode}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<!-- Attachments Tab - End -->
														
														<!-- Comments Tab - Start -->
                                                         <rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}"
															rendered="#{pages$orderManage.isAddMode || pages$orderManage.isEditMode || pages$orderManage.isViewMode || pages$orderManage.isApproveRejectMode}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														<!-- Comments Tab - End -->
																												
												</rich:tabPanel>
												
												<table cellpadding="0" cellspacing="0"  style="width:100%;">
													<tr>
														<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
														<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>
												
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td colspan="6" class="BUTTON_TD">
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$orderManage.onSave}"
																	rendered="#{pages$orderManage.isAddMode || pages$orderManage.isEditMode}">																	
															</h:commandButton>															
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.send']}"
																	action="#{pages$orderManage.onSend}"
																	rendered="#{pages$orderManage.isEditMode}">
															</h:commandButton>
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	action="#{pages$orderManage.onCancel}"
																	rendered="#{pages$orderManage.isAddMode || pages$orderManage.isEditMode || pages$orderManage.isViewMode}">
															</h:commandButton>
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$orderManage.onPassOrderViewToOpener}"
																	rendered="#{pages$orderManage.isAddPopupMode || pages$orderManage.isEditPopupMode}">
															</h:commandButton>
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.closeButton']}"
																	onclick="window.close(); return false;"
																	rendered="#{pages$orderManage.isAddPopupMode || pages$orderManage.isEditPopupMode || pages$orderManage.isViewPopupMode}">
															</h:commandButton>
															
														</td>
													</tr>
												</table>
											</div>								
								
									    </h:form>
									</div>
								

									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<c:choose>
									<c:when test="${pages$orderManage.isAddMode || pages$orderManage.isEditMode || pages$orderManage.isViewMode}">
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</c:when>
								</c:choose>
							</tr>
						</table>
					</td>
				</tr>
			</table>
           </div>
		</body>
	</html>
</f:view>
