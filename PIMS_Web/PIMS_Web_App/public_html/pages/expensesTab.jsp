<t:div id="dataTableExpensestdiv" style="width:100%;">
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableExpenses" rows="15" width="100%"
			value="#{pages$endowmentManage.dataListExpenses}"
			binding="#{pages$endowmentManage.dataTableExpenses}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">


			<t:column id="expenserequestNumber" sortable="true">
				<f:facet name="header">
					<t:outputText id="expenserequestNumberot" value="#{msg['commons.requestNumber']}" />
				</f:facet>
				<t:outputText id="expenserequestNumberfld" styleClass="A_LEFT"
					value="#{ dataItem.requestNumber}" />
			</t:column>
			<t:column id="payNumber" sortable="true">
				<f:facet name="header">
					<t:outputText id="payNum"
						value="#{msg['chequeList.paymentNumber']}" />
				</f:facet>
				<t:outputText id="payNoField" styleClass="A_LEFT"
					value="#{!empty dataItem.paymentSchedules ?  
																								dataItem.paymentSchedules[0].paymentNumber:''}" />
			</t:column>
			<t:column id="typeCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="typeCoOT" value="#{msg['commons.typeCol']}" />
				</f:facet>
				<t:outputText id="typeO" styleClass="A_LEFT"
					value="#{pages$endowmentManage.englishLocale? dataItem.endowmentExpenseTypeEn:dataItem.endowmentExpenseTypeAr}" />
			</t:column>

			<t:column id="expUnitName" sortable="true">
				<f:facet name="header">
					<t:outputText id="expUnitNameOT"
						value="#{msg['cancelContract.tab.unit.unitno']}" />
				</f:facet>
				<t:outputText id="expUnitField" styleClass="A_LEFT"
					value="#{! empty dataItem.unitView?dataItem.unitView.unitNumber:''}" />
			</t:column>
			<t:column id="expbeneficiaryName" sortable="true">
				<f:facet name="header">
					<t:outputText id="expNbeneficiaryNameOT"
						value="#{msg['commons.Beneficiary']}" />
				</f:facet>
				<t:outputText id="expbeneficiaryNameField" styleClass="A_LEFT"
					value="#{dataItem.beneficiaryName}" />
			</t:column>


			<t:column id="statusCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="statusAmnt" value="#{msg['commons.status']}" />
				</f:facet>
				<t:outputText id="status" styleClass="A_LEFT"
					value="#{pages$endowmentManage.englishLocale? dataItem.statusEn:dataItem.statusAr}" />
			</t:column>


			<t:column id="periodFrom" sortable="true">
				<f:facet name="header">
					<t:outputText id="periodFromCOloutput"
						value="#{msg['endowmentExpense.lbl.periodFrom']}" />
				</f:facet>
				<t:outputText id="periodFromDate" styleClass="A_LEFT"
					value="#{dataItem.periodFrom}" style="white-space: normal;">
					<f:convertDateTime pattern="#{pages$endowmentManage.dateFormat}"
						timeZone="#{pages$endowmentManage.timeZone}" />
				</t:outputText>
			</t:column>
			<t:column id="periodTo" sortable="true">
				<f:facet name="header">
					<t:outputText id="periodToCOloutput"
						value="#{msg['endowmentExpense.lbl.periodTo']}" />
				</f:facet>
				<t:outputText id="periodToDate" styleClass="A_LEFT"
					value="#{dataItem.periodTo}" style="white-space: normal;">
					<f:convertDateTime pattern="#{pages$endowmentManage.dateFormat}"
						timeZone="#{pages$endowmentManage.timeZone}" />
				</t:outputText>
			</t:column>
			<t:column id="paymentDescription" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdpaymentDescription"
						value="#{msg['commons.description']}" />
				</f:facet>
				<t:outputText id="paymentDescriptionField" styleClass="A_LEFT"
					value="#{dataItem.description}" />
			</t:column>

			<t:column id="amntCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAmnt"
						value="#{msg['mems.payment.request.label.Amount']}" />
				</f:facet>
				<t:outputText id="amountt" styleClass="A_LEFT"
					value="#{dataItem.amountString}" />
			</t:column>
		</t:dataTable>
	</t:div>

	<t:div id="pagingDivExpenses2" styleClass="contentDivFooter"
		style="width:99.2%;">
		<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
			cellspacing="1">
			<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
				<t:div styleClass="JUG_NUM_REC_ATT">
					<h:outputText value="#{msg['commons.records']}" />
					<h:outputText value=" : " />
					<h:outputText value="#{pages$endowmentManage.recordSize}" />
				</t:div>
			</t:panelGroup>
			<t:panelGroup>
				<t:dataScroller id="scrollerExpensesList" for="dataTableExpenses"
					paginator="true" fastStep="1" paginatorMaxPages="15"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
					styleClass="SCH_SCROLLER" pageIndexVar="pageNumber"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false">
					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="lblFExpensesList2"></t:graphicImage>
					</f:facet>
					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="lblFRExpensesList2"></t:graphicImage>
					</f:facet>
					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="lblFFExpensesList2"></t:graphicImage>
					</f:facet>
					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="lblLExpensesList2"></t:graphicImage>
					</f:facet>


				</t:dataScroller>
			</t:panelGroup>
		</t:panelGrid>
	</t:div>
</t:div>


