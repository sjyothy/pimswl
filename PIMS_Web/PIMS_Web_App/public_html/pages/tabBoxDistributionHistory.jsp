
<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">

	<t:dataTable id="prdc" rows="15" width="100%"
		value="#{pages$donationBox.listDistribution}"
		binding="#{pages$donationBox.dataTableDistribution}"
		preserveDataModel="false" preserveSort="false" var="dataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true">


		<t:column id="distributedOn" sortable="true">
			<f:facet name="header">
				<t:outputText id="hddistributedOn"
					value="#{msg['donationBox.lbl.distributedOn']}" />
			</f:facet>
			<h:outputText value="#{dataItem.distributedOn}"
				style="white-space: normal;" styleClass="A_LEFT">
				<f:convertDateTime timeZone="#{pages$donationBox.timeZone}"
					pattern="#{pages$donationBox.dateFormat}" />
			</h:outputText>
		</t:column>
		<t:column id="distributedBy" sortable="true">
			<f:facet name="header">
				<t:outputText id="hddistributedBy"
					value="#{msg['donationBox.lbl.distributedBy']}" />
			</f:facet>
			<h:outputText value="#{dataItem.distributedBy}"
				style="white-space: normal;" styleClass="A_LEFT" />

		</t:column>
		<t:column id="typeNameAr" sortable="true">
			<f:facet name="header">
				<t:outputText id="hdtypeNameAr" value="#{msg['donationBox.label.place']}" />
			</f:facet>
			<h:outputText value="#{dataItem.boxPlaceType.typeNameAr}"
				style="white-space: normal;" styleClass="A_LEFT" />
		</t:column>

		<t:column id="address" sortable="true">
			<f:facet name="header">
				<t:outputText id="hdaddress" value="#{msg['donationBox.lbl.address']}" />
			</f:facet>
			<h:outputText value="#{dataItem.address}"
				style="white-space: normal;" styleClass="A_LEFT" />

		</t:column>

		<t:column id="communityAr" sortable="true">
			<f:facet name="header">
				<t:outputText id="hdcommunityAr"
					value="#{msg['donationBox.label.community']}" />
			</f:facet>
			<h:outputText value="#{dataItem.communityAr}"
				style="white-space: normal;" styleClass="A_LEFT" />

		</t:column>


	</t:dataTable>
</t:div>



