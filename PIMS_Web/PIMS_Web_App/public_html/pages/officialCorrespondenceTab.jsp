<%-- Author: Kamran Ahmed--%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<t:div style="height:240px;overflow-y:scroll;width:100%;">
<t:panelGrid border="0" width="97%" cellpadding="1" cellspacing="1" 
	id="offcialCorrespondencesTab">
	<t:panelGroup>
		<%--Column 1,2 Starts--%>
		<t:panelGrid columns="4" columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%"
		rendered="#{!(pages$inheritanceFile.viewModePopUp || pages$inheritanceFile.pageModeView)}">

			<h:outputLabel value="#{msg['customer.govtDepartmant']}" />
			<h:selectOneMenu styleClass="SELECT_MENU"
				binding="#{pages$officialCorrespondenceTab.cboGovtDepttId}">
				<f:selectItem itemValue="-1"
					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
				<f:selectItems value="#{pages$ApplicationBean.govtDepttList}" />
			</h:selectOneMenu>

			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.bank']}" />
			<h:selectOneMenu
				binding="#{pages$officialCorrespondenceTab.cboBankId}"
				styleClass="SELECT_MENU">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$ApplicationBean.bankList}" />
			</h:selectOneMenu>
		
			<h:panelGroup>
			<h:outputLabel styleClass="mandatory"
					value="#{msg['commons.mandatory']}" />
			<h:outputLabel value="#{msg['commons.description']}" styleClass="LABEL"/>
			</h:panelGroup>
			<h:inputTextarea 	style="width:186px"
			binding="#{pages$officialCorrespondenceTab.txtArDescription}"></h:inputTextarea>
		
			<h:outputLabel value="#{msg['mems.inheritanceFile.officialCoresTab.attacment']}" />
			<t:inputFileUpload id="fileupload" 
				storage="file"
				binding="#{pages$officialCorrespondenceTab.fileUploadCtrl}"
				value="#{pages$officialCorrespondenceTab.selectedFile}"
				style="width:261px;height: 20px; margin-right: 5px;"></t:inputFileUpload>		
			<t:div style="height:10px;" />
		</t:panelGrid>
		
	</t:panelGroup>
	<%--Column 3,4 Ends--%>
</t:panelGrid>

<t:div style="width:100%;">
	<t:div styleClass="A_RIGHT">
		<h:commandButton
			rendered="#{pages$inheritanceFile.showCorrespondenceButton}"
			value="#{msg['mems.inheritanceFile.officialCoresTab.save']}"
			action="#{pages$officialCorrespondenceTab.saveCorrespondence}"
			styleClass="BUTTON" style="width:10%;"></h:commandButton>
	</t:div>
	<t:div style="height:5px;"></t:div>
	<t:div styleClass="contentDiv" style="width:98%">
		<t:dataTable id="offcialCorrespondencesGrid" rows="200"
			value="#{pages$officialCorrespondenceTab.officialCorresList}"
			binding="#{pages$officialCorrespondenceTab.officialCorresDataTable}"
			preserveDataModel="true" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
			width="100%">
			
			<t:column  sortable="true">
				<f:facet name="header">
					<h:outputText/>
				</f:facet>
				<h:selectBooleanCheckbox 
					rendered="#{dataItem.opened && !(pages$inheritanceFile.viewModePopUp || pages$inheritanceFile.pageModeView)}"
				value="#{dataItem.selected}"></h:selectBooleanCheckbox>
			</t:column>
			
			<t:column  sortable="true">
				<f:facet name="header">
				<h:outputText value="#{msg['mems.inheritanceFile.officialCoresTab.deptBank']}" />
				</f:facet>
				<h:outputText value="#{pages$officialCorrespondenceTab.englishLocale?dataItem.govtDeptt.govtDepttEn:dataItem.govtDeptt.govtDepttAr}"/>
				<h:outputText value="#{pages$officialCorrespondenceTab.englishLocale?dataItem.bank.bankEn:dataItem.bank.bankAr}"/>
			</t:column>
			
			<t:column  sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.description']}" />
				</f:facet>
				<h:outputText value="#{dataItem.description}"/>
			</t:column>
			
			<t:column  sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['mems.inheritanceFile.officialCoresTab.status']}" />
				</f:facet>
				<h:outputText value="#{pages$officialCorrespondenceTab.englishLocale?dataItem.statusEn:dataItem.statusAr}"/>
			</t:column>
			
			<t:column  sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['mems.inheritanceFile.officialCoresTab.createdBy']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdByName}"/>
			</t:column>
			<t:column id="actioCol" sortable="false"
				rendered="#{!(pages$inheritanceFile.viewModePopUp || pages$inheritanceFile.pageModeView)}">
				<f:facet name="header">
					<t:outputText value="#{msg['mems.inheritanceFile.columnLabel.action']}" />
				</f:facet>
				<t:commandLink id="lnkDownload"  
					rendered="#{dataItem.downloadable &&  !(pages$inheritanceFile.viewModePopUp || pages$inheritanceFile.pageModeView)}"
					action="#{pages$officialCorrespondenceTab.downloadFile}">
					<h:graphicImage id="fileDownloadIcon"
						style="cursor:hand;"
						title="#{msg['attachment.buttons.download']}"
						url="../resources/images/app_icons/downloadAttachment.png" />
				</t:commandLink>

				<t:commandLink 
					onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToDeleteOfficialCorrespondence']}')) return false;"
					action="#{pages$officialCorrespondenceTab.deleteOffCorrspndnc}"
					rendered="#{pages$inheritanceFile.showInitLimDoneButton && dataItem.opened}">
					<h:graphicImage id="delete" title="#{msg['commons.delete']}"
						style="cursor:hand;"
						url="../resources/images/delete.gif" />&nbsp;
				</t:commandLink>
				<pims:security screen="Pims.MinorMgmt.InheritanceFile.AddFollowup" action="view">
				<t:commandLink 
				rendered="#{!(pages$inheritanceFile.viewModePopUp || pages$inheritanceFile.pageModeView)}"
				action="#{pages$officialCorrespondenceTab.openFollowupPopup}">
					<h:graphicImage title="#{msg['memsFollowupTab.addFollowup']}"
						style="cursor:hand;"
						url="../resources/images/app_icons/application_form_edit.png" />&nbsp;
				</t:commandLink>
				</pims:security>
				
			</t:column>
		</t:dataTable>
	</t:div>
	<t:div style="height:10px;"/>
	<t:div styleClass="A_RIGHT">
		<h:commandButton
			rendered="#{pages$inheritanceFile.showCorrespondenceButton}"
			value="#{msg['mems.inheritanceFile.officialCoresTab.close']}"
			onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToCloseCorres']}')) return false;"
			action="#{pages$officialCorrespondenceTab.closeCorrespondence}"
			styleClass="BUTTON" style="width:auto;"></h:commandButton>
	</t:div>
</t:div>
</t:div>