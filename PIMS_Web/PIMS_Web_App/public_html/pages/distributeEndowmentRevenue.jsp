<%-- 
  - Author:Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - 
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>

<script language="JavaScript" type="text/javascript">
	function performClick(control)
	{
			
		disableInputs();
		document.getElementById("frm:lnkDistribute").onclick() 
		
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
	}	
	
	function sendInfoToParent()
	{
	
		window.opener.onMessageFromDistributeEndowmentRevenue();
		window.close();
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">

				<tr width="100%">

					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel
										value="#{msg['distributeEndRevenue.title.distribute']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>
									<h:form id="frm" enctype="multipart/form-data">
										<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$distributeEndowmentRevenue.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$distributeEndowmentRevenue.successMessages}"
																escape="false" styleClass="INFO_FONT" />
															<h:messages></h:messages>
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">

												<div class="DETAIL_SECTION" style="width: 100%">
													<h:outputLabel value="" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>

													<table id="tableForm" cellpadding="1px" width="100%"
														cellspacing="2px" class="DETAIL_SECTION_INNER" columns="4">

														<tr>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['chequeList.paymentNumber']} :"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="personName" readonly="true"
																	styleClass="INPUT_TEXT READONLY"
																	value="#{pages$distributeEndowmentRevenue.paymentToDistribute.paymentNumber}"
																	style="width:190px;">
																</h:inputText>
															</td>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['paymentDetailsPopUp.payMode']} :"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="amountToDisburse" readonly="true"
																	styleClass="INPUT_TEXT READONLY"
																	value="#{pages$distributeEndowmentRevenue.paymentToDistribute.paymentModeAr}"
																	style="width:190px;">
																</h:inputText>
															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['donationBox.lbl.distributedBy']} :"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="distributedBy" readonly="true"
																	styleClass="INPUT_TEXT READONLY"
																	value="#{pages$distributeEndowmentRevenue.paymentToDistribute.distributedByAr}"
																	style="width:190px;">
																</h:inputText>
															</td>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['donationBox.lbl.distributedOn']} :"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="distributedOn" readonly="true"
																	styleClass="INPUT_TEXT READONLY"
																	value="#{pages$distributeEndowmentRevenue.paymentToDistribute.distributedOn}"
																	style="width:190px;">
																</h:inputText>
															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['revenueFollowup.lbl.endowment']} :"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="endowmentNameField" readonly="true"
																	styleClass="INPUT_TEXT READONLY"
																	value="#{pages$distributeEndowmentRevenue.paymentToDistribute.endowmentView.endowmentName}"
																	style="width:190px;">
																</h:inputText>
															</td>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['commons.typeCol']} :"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="endType" readonly="true"
																	styleClass="INPUT_TEXT READONLY"
																	value="#{pages$distributeEndowmentRevenue.paymentToDistribute.endowmentView.assetTypeAr}"
																	style="width:190px;">
																</h:inputText>
															</td>
														</tr>

														<tr>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['commons.amount']} :"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="amountToDistriute" readonly="true"
																	styleClass="INPUT_TEXT READONLY"
																	value="#{pages$distributeEndowmentRevenue.paymentToDistribute.amountStr}"
																	style="width:190px;">
																</h:inputText>
															</td>
														</tr>

													</table>
												</div>
												<t:div>&nbsp;</t:div>
												<table class="BUTTON_TD" cellpadding="1px" width="100%"
													cellspacing="3px">
													<tr>
														<td class="BUTTON_TD" colspan="10">

															<h:commandButton styleClass="BUTTON"
																value="#{msg['distribution.distribute']}"
																rendered="#{pages$distributeEndowmentRevenue.showDistributeButton}"
																onclick="if (!confirm('#{msg['distributeEndRevenue.msg.confirmDistribution']}')) return false;else performClick(this);"
																style="width: 135px">
															</h:commandButton>
															<h:commandLink id="lnkDistribute"
																action="#{pages$distributeEndowmentRevenue.onDistribute}" />

														</td>
													</tr>
												</table>

												<div class="contentDiv"
													style="width: 99%; margin-top: 15px;">
													<t:dataTable id="test2"
														value="#{pages$distributeEndowmentRevenue.dataList}"
														binding="#{pages$distributeEndowmentRevenue.dataTable}"
														width="100%"
														rows="#{pages$distributeEndowmentRevenue.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true">

														<t:column id="person" width="10%" defaultSorted="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText value="#{msg['commons.person']}" />
															</f:facet>
															<t:outputText value="#{dataItem.endFileBen.personName}"
																style="white-space: normal;" />
														</t:column>
														<t:column id="masraf" width="10%"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText value="#{msg['commons.Masraf']}" />
															</f:facet>
															<t:outputText value="#{dataItem.endFileBen.masrafName}"
																style="white-space: normal;" />
														</t:column>
														<t:column id="amount" width="5%"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText value="#{msg['commons.amount']}" />
															</f:facet>
															<t:outputText value="#{dataItem.amountString}"
																style="white-space: normal;" />
														</t:column>
														<t:column id="percentage" width="5%"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText value="#{msg['commons.percentage']}" />
															</f:facet>
															<t:outputText value="#{dataItem.percentageString}"
																style="white-space: normal;" />
														</t:column>
														<t:column id="suggestedAmt" width="10%"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText
																	value="#{msg['distributeEndRevenue.lbl.suggestedAmt']}" />
															</f:facet>
															<t:outputText value="#{dataItem.suggestedAmountString}"
																style="white-space: normal;" />
														</t:column>
														<t:column id="distributedAmt" width="10%"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText
																	value="#{msg['distributeEndRevenue.lbl.distributeAmount']}" />
															</f:facet>
															<t:outputText
																rendered="#{!pages$distributeEndowmentRevenue.showDistributeButton}"
																value="#{dataItem.distributedAmountString}" />
															<t:inputText value="#{dataItem.distributedAmountString}"
																rendered="#{pages$distributeEndowmentRevenue.showDistributeButton}"
																onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)"
																style="white-space: normal;" />
														</t:column>
														<t:column id="action" width="10%"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>
															<h:commandLink
																action="#{pages$distributeEndowmentRevenue.openStatmntOfAccount}">
																<h:graphicImage
																	title="#{msg['report.tooltip.PrintStOfAcc']}"
																	style="MARGIN: 1px 1px -5px;cursor:hand"
																	url="../resources/images/app_icons/print.gif" />
															</h:commandLink>
														</t:column>

													</t:dataTable>
												</div>

											</div>
									</h:form>

								</td>
							</tr>


						</table>

						</div>

						</div>



						</div>




					</td>
				</tr>
			</table>
			</td>
			</tr>
			</table>



		</body>
	</html>
</f:view>