<t:div id="tabDDetails" style="width:100%;">

	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">

		<t:dataTable id="dataTablePayments" rows="15" width="100%"
			value="#{pages$openBoxInGroup.listOpenBox}"
			binding="#{pages$openBoxInGroup.dataTableOpenBoxes}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="refNumDonation" sortable="true">
				<f:facet name="header">
					<t:outputText id="refNumHeadDonation"
						value="#{msg['donationBox.lbl.boxNum']}" />
				</f:facet>
				<t:outputText id="refNumDonationtxt" styleClass="A_LEFT"
					value="#{dataItem.donationBox.boxNum}" />
			</t:column>

			<t:column id="refNumEndowment" sortable="true">
				<f:facet name="header">
					<t:outputText id="refNumHeadEndowment"
						value="#{msg['donationRequest.lbl.endowmentProgram']}" />
				</f:facet>
				<h:commandLink id="lnkEndowment"
					value="#{dataItem.endowmentProgram.progName}"
					onclick="javaScript:openManageEndowmentPrograms('#{dataItem.endowmentProgram.endowmentProgramId}');">
				</h:commandLink>
			</t:column>

			<t:column id="reftransactionNumck" sortable="true">
				<f:facet name="header">
					<t:outputText id="reftransactionNumh"
						value="#{msg['openBox.lbl.transactionNum']}" />
				</f:facet>
				<t:outputText id="reftransactionNumhtxt" styleClass="A_LEFT"
					value="#{dataItem.refNum}" />
			</t:column>

			<t:column id="statusck" sortable="true">
				<f:facet name="header">
					<t:outputText id="statush" value="#{msg['commons.status']}" />
				</f:facet>
				<t:outputText id="statushtxt" styleClass="A_LEFT"
					value="#{pages$openBoxInGroup.englishLocale? 
															     dataItem.statusEn:
															     dataItem.statusAr}" />
			</t:column>


			<t:column id="amountck" sortable="true">
				<f:facet name="header">
					<t:outputText id="amounthead" value="#{msg['openBox.lbl.amount']}" />
				</f:facet>
				<t:outputText id="amounttxt" styleClass="A_LEFT"
					value="#{dataItem.amountCollectedStr}" />
			</t:column>

			<t:column id="action" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAction" value="#{msg['commons.action']}" />
				</f:facet>



				<h:commandLink
					action="#{pages$openBoxInGroup.openAmountDetailsPopup}">
					<h:graphicImage id="openAmountDetailsPopup"
						style="margin-right:5px;"
						title="#{msg['openBox.heading.amountDetails']}"
						url="../resources/images/app_icons/Add-Payment-schedule.png"
						rendered="#{pages$openBoxInGroup.showAddAmountDetails}" />
				</h:commandLink>
				<h:graphicImage id="openAmountDetailsHistoryPopup"
					style="margin-right:5px;"
					title="#{msg['openBox.heading.amountDetailsHistory']}"
					url="../resources/images/app_icons/manage_tender.png"
					onclick="javaScript:openAmountDetailsHistoryPopup('#{dataItem.openBoxId}')"
					rendered="#{dataItem.statusId==204002}" />
				<h:graphicImage id="openAmountDetailsPopupForChange"
					style="margin-right:5px;"
					title="#{msg['openBox.heading.amountDetails']}"
					url="../resources/images/app_icons/application_form_edit.png"
					rendered="#{dataItem.statusId==204002}" />





				<h:commandLink id="lnkDelete"
					onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableInputs();"
					rendered="#{pages$openBoxInGroup.showSaveButton }"
					action="#{pages$openBoxInGroup.onDelete}">
					<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
						url="../resources/images/delete.gif" width="18px;" />
				</h:commandLink>
			</t:column>


		</t:dataTable>

	</t:div>

</t:div>


