
<t:div id="tabBDetails" style="width:100%;">
	<t:panelGrid id="tabBDetailsab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['socialProgram.lbl.Recommendations']}:"></h:outputLabel>
		</h:panelGroup>
		<t:panelGroup colspan="3" style="width: 100%">
			<t:inputTextarea style="width: 90%;" id="txtRecommendation"
				
				value="#{pages$tabFamilyVillageBeneficiaryRecommendation.pageView.comments}"
				onblur="javaScript:removeExtraCharacter(txtRecommendation,500);" 
				onkeyup="javaScript:removeExtraCharacter(txtRecommendation,500);"
				onmouseup="javaScript:removeExtraCharacter(txtRecommendation,500);"
				onmousedown="javaScript:removeExtraCharacter(txtRecommendation,500);"
				onchange="javaScript:removeExtraCharacter(txtRecommendation,500);"
				onkeypress="javaScript:removeExtraCharacter(txtRecommendation,500);" />
		</t:panelGroup>

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;">

		<h:commandButton styleClass="BUTTON" value="#{msg['commons.Add']}"
			action="#{pages$tabFamilyVillageBeneficiaryRecommendation.onAdd}">
		</h:commandButton>
	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableRecommendation" rows="15" width="100%"
			value="#{pages$tabFamilyVillageBeneficiaryRecommendation.dataList}"
			binding="#{pages$tabFamilyVillageBeneficiaryRecommendation.dataTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="beneficiaryNamedtc" sortable="true">
				<f:facet name="header">
					<t:outputText id="beneficiaryNameOT" value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageBeneficiaryRecommendation.englishLocale? 
																				dataItem.createdBy:
																				dataItem.createdBy}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="dateofbirthcil" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageBeneficiaryRecommendation.dateFormat}"
						timeZone="#{pages$tabFamilyVillageBeneficiaryRecommendation.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="gendercol" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['socialProgram.lbl.Recommendations']}" />
				</f:facet>
				<h:outputText value="#{dataItem.comments}">

				</h:outputText>
			</t:column>


		</t:dataTable>
	</t:div>

</t:div>


