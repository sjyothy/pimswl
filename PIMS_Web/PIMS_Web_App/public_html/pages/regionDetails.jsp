<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Adding/Editing region
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	    function resetValues()
      	{
      		document.getElementById("searchFrm:countryEn").value="";
			document.getElementById("searchFrm:countryAr").value="";
			document.getElementById("searchFrm:cityEn").value="";
			document.getElementById("searchFrm:cityAr").value="";
			document.getElementById("searchFrm:areaEn").value="";
			document.getElementById("searchFrm:areaAr").value="";
        }

     	function showAddRegionPopup()
		{
		      var screen_width = 1024;
		      var screen_height = 450;
		      var popup_width = screen_width-380;
		      var popup_height = screen_height-265;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2-20;
		      
		      var popup = window.open('addRegion.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

</head>
	<body class="BODY_STYLE">
	<div class="containerDiv">
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<c:choose>
					<c:when test="${!pages$regionDetails.isViewModePopUp}">
						<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>
					</c:when>
				</c:choose>


				<tr width="100%">

					<c:choose>
						<c:when test="${!pages$regionDetails.isViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>

					<td width="83%" height="470px" valign="top" class="divBackgroundBody">
						<table width="99.2%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{pages$regionDetails.heading}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
									<div class="SCROLLABLE_SECTION AUC_SCH_SS">
										<h:form id="searchFrm" style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
											<t:div styleClass="MESSAGE">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$regionDetails.infoMessage}"
																escape="false" styleClass="INFO_FONT" />
															<h:outputText
																value="#{pages$regionDetails.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</t:div>
											<div class="MARGIN" style="margin:11px;">
												<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
												<div class="DETAIL_SECTION" style="#width:99.64%;">
													<h:outputLabel value="#{msg['commons.searchCriteria']}" rendered="#{!pages$regionDetails.isViewModePopUp}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<h:outputLabel value="#{msg['regionDetails']}" rendered="#{pages$regionDetails.isViewModePopUp}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>

															<td style="PADDING: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL" rendered="#{pages$regionDetails.showCountry}"   
																	value="#{msg['regionDetails.countryEn']}:"></h:outputLabel>
															</td>

															<td style="PADDING: 5px; PADDING-TOP: 5px;">
																<h:inputText id="countryEn"
																	styleClass="INPUT_TEXT #{pages$regionDetails.readonlyStyleClassCountry}" rendered="#{pages$regionDetails.showCountry}"
																	value="#{pages$regionDetails.region.countryEn}" binding="#{pages$regionDetails.countryEnText}"
																	></h:inputText>

															</td>
															<td style="PADDING: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL" rendered="#{pages$regionDetails.showCountry}"
																	value="#{msg['regionDetails.countryAr']}:"></h:outputLabel>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 5px;">
																<h:inputText id="countryAr"
																	styleClass="INPUT_TEXT #{pages$regionDetails.readonlyStyleClassCountry}" rendered="#{pages$regionDetails.showCountry}"
																	value="#{pages$regionDetails.region.countryAr}" binding="#{pages$regionDetails.countryArText}"
																	 ></h:inputText>
															</td>
														</tr>
														<tr>

															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="LABEL" rendered="#{pages$regionDetails.showCity}"
																	value="#{msg['regionDetails.cityEn']}:"></h:outputLabel>
															</td>

															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:inputText id="cityEn"
																	styleClass="INPUT_TEXT #{pages$regionDetails.readonlyStyleClassCity}" rendered="#{pages$regionDetails.showCity}"
																	value="#{pages$regionDetails.region.cityEn}" binding="#{pages$regionDetails.cityEnText}"
																	></h:inputText>

															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="LABEL" rendered="#{pages$regionDetails.showCity}"
																	value="#{msg['regionDetails.cityAr']}:"></h:outputLabel>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:inputText id="cityAr"
																	styleClass="INPUT_TEXT #{pages$regionDetails.readonlyStyleClassCity}" rendered="#{pages$regionDetails.showCity}"
																	value="#{pages$regionDetails.region.cityAr}" binding="#{pages$regionDetails.cityArText}"
																	 ></h:inputText>
															</td>
														</tr>
														<tr>

															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="LABEL" rendered="#{pages$regionDetails.showArea}"
																	value="#{msg['regionDetails.areaEn']}:"></h:outputLabel>
															</td>

															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:inputText id="areaEn"
																	styleClass="INPUT_TEXT #{pages$regionDetails.readonlyStyleClassArea}" rendered="#{pages$regionDetails.showArea}"
																	value="#{pages$regionDetails.region.areaEn}" binding="#{pages$regionDetails.areaEnText}"
																	></h:inputText>

															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="LABEL" rendered="#{pages$regionDetails.showArea}"
																	value="#{msg['regionDetails.areaAr']}:"></h:outputLabel>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:inputText id="areaAr"
																	styleClass="INPUT_TEXT #{pages$regionDetails.readonlyStyleClassArea}" rendered="#{pages$regionDetails.showArea}"
																	value="#{pages$regionDetails.region.areaAr}" binding="#{pages$regionDetails.areaArText}"
																	 ></h:inputText> 
															</td>
														</tr>
														<tr>
														<c:choose>
															<c:when test="${pages$regionDetails.isViewModePopUp}">
																<td class="JUG_BUTTON_TD_AUC_POP BUTTON_TD" colspan="4">
															</c:when>
															<c:otherwise>
																<td class="JUG_BUTTON_TD_AUC_SCH BUTTON_TD" colspan="4">
															</c:otherwise>
														</c:choose>	
																<table cellpadding="1px" cellspacing="1px">
																	<tr>
																		<td colspan="2">
																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.search']}" rendered="#{!pages$regionDetails.isViewModePopUp}"
																					action="#{pages$regionDetails.searchRegions}"
																					 tabindex="10">
																				</h:commandButton>
																				<h:commandButton styleClass="BUTTON"
																						value="#{msg['regionDetails.heading.country']}" rendered="#{pages$regionDetails.isCountryMode}"
																						actionListener="#{pages$regionDetails.openAddCountryPopup}"
																						 tabindex="7">
																				</h:commandButton>
																				<h:commandButton styleClass="BUTTON"
																						value="#{msg['regionDetails.heading.city']}" rendered="#{pages$regionDetails.isCityMode}"
																						actionListener="#{pages$regionDetails.openAddCityPopup}"
																						 tabindex="8">
																					</h:commandButton>
																				<h:commandButton styleClass="BUTTON"
																						value="#{msg['regionDetails.heading.area']}" rendered="#{pages$regionDetails.isAreaMode}"
																						actionListener="#{pages$regionDetails.openAddAreaPopup}"
																						 tabindex="9">
																				</h:commandButton>
																				
																				<h:commandButton  styleClass="BUTTON"
																					value="#{msg['commons.clear']}" rendered="true"
																					onclick="javascript:resetValues();"
																					 tabindex="12"></h:commandButton>
																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.back']}" rendered="#{!pages$regionDetails.isCountryMode}"
																					action="#{pages$regionDetails.back}"
																					 tabindex="11">
																				</h:commandButton>
																		</td>


																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</div>
											</div>
											<t:div rendered="#{!pages$regionDetails.isViewModePopUp}"
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
												<t:div styleClass="imag" rendered="#{!pages$regionDetails.isViewModePopUp}">
													&nbsp;
												</t:div>
												<t:div styleClass="contentDiv" style="width: 100%; # width: 98.29%;">
													<t:dataTable id="dt1"
														value="#{pages$regionDetails.regionList}"
														binding="#{pages$regionDetails.regionDataTable}"
														rows="#{pages$regionDetails.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%">

														<t:column id="areaEnCol" rendered="#{pages$regionDetails.showArea}" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['regionDetails.areaEn']}" />
															</f:facet>
															<t:outputText value="#{dataItem.areaEn}"  style="white-space: normal;"/>
														</t:column>
														<t:column id="areaArCol" rendered="#{pages$regionDetails.showArea}" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['regionDetails.areaAr']}" />
															</f:facet>
															<t:outputText value="#{dataItem.areaAr}" />
														</t:column>
														<t:column id="cityEnCol" rendered="#{pages$regionDetails.showCity}" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['regionDetails.cityEn']}" />
															</f:facet>
															<t:outputText  style="white-space: normal;"
																value="#{dataItem.cityEn}" />
														</t:column>
														<t:column id="cityArCol" rendered="#{pages$regionDetails.showCity}" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['regionDetails.cityAr']}" />
															</f:facet>
															<t:outputText value="#{dataItem.cityAr}"  style="white-space: normal;"/>
														</t:column>
														<t:column id="countryEnCol" rendered="#{pages$regionDetails.showCountry}" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['regionDetails.countryEn']}" />
															</f:facet>
															<t:outputText value="#{dataItem.countryEn}"  style="white-space: normal;"
																styleClass="A_LEFT" />
														</t:column>
														<t:column id="countryArCol" rendered="#{pages$regionDetails.showCountry}" sortable="true" defaultSorted="true">
															<f:facet name="header">
																<t:outputText value="#{msg['regionDetails.countryAr']}" />
															</f:facet>
															<t:outputText value="#{dataItem.countryAr}"  style="white-space: normal;"
																styleClass="A_LEFT" />
														</t:column>
														<t:column id="actionCol" sortable="false" width="100"
															style="TEXT-ALIGN: center;">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>
																<t:commandLink
																	action="#{pages$regionDetails.openCities}"
																	rendered="#{dataItem.showCountryActions}">
																<h:graphicImage id="viewIcon"
																			title="#{msg['commons.cities']}"
																			url="../resources/images/app_icons/Contrect.png" />&nbsp;
																</t:commandLink>
																<t:outputLabel value="  "></t:outputLabel>
																<t:commandLink
																	action="#{pages$regionDetails.openAreas}"
																	rendered="#{dataItem.showCityActions}">
																	<h:graphicImage id="editIcon"
																		title="#{msg['commons.areas']}"
																		url="../resources/images/app_icons/Contrect.png" />&nbsp;
																</t:commandLink>
																<t:commandLink
																	actionListener="#{pages$regionDetails.editCountry}"
																	rendered="#{dataItem.showCountryActions}">
																	<h:graphicImage 
																		title="#{msg['commons.edit']}"
																		url="../resources/images/edit-icon.gif" />&nbsp;
																</t:commandLink>
																<t:commandLink
																	actionListener="#{pages$regionDetails.editCity}"
																	rendered="#{dataItem.showCityActions}">
																	<h:graphicImage 
																		title="#{msg['commons.edit']}"
																		url="../resources/images/edit-icon.gif" />&nbsp;
																</t:commandLink>
																<t:commandLink
																	actionListener="#{pages$regionDetails.editArea}"
																	rendered="#{dataItem.showAreaActions}">
																	<h:graphicImage 
																		title="#{msg['commons.edit']}"
																		url="../resources/images/edit-icon.gif" />&nbsp;
																</t:commandLink>
																<t:commandLink
																	onclick="if (!confirm('#{msg['regionDetails.messages.confirmDeleteCountry']}')) return"
																	actionListener="#{pages$regionDetails.deleteCountry}"
																	rendered="#{dataItem.showCountryActions}">
																	<h:graphicImage 
																		title="#{msg['commons.delete']}"
																		url="../resources/images/delete.gif" />&nbsp;
																</t:commandLink>
																<t:commandLink
																	onclick="if (!confirm('#{msg['regionDetails.messages.confirmDeleteCity']}')) return"
																	actionListener="#{pages$regionDetails.deleteCity}"
																	rendered="#{dataItem.showCityActions}">
																	<h:graphicImage 
																		title="#{msg['commons.delete']}"
																		url="../resources/images/delete.gif" />&nbsp;
																</t:commandLink>
																<t:commandLink
																	onclick="if (!confirm('#{msg['regionDetails.messages.confirmDeleteArea']}')) return"
																	actionListener="#{pages$regionDetails.deleteArea}"
																	rendered="#{dataItem.showAreaActions}">
																	<h:graphicImage 
																		title="#{msg['commons.delete']}"
																		url="../resources/images/delete.gif" />&nbsp;
																</t:commandLink>
														</t:column>
													</t:dataTable>
												</t:div>
												<c:choose>
													<c:when test="${!pages$regionDetails.isEnglishLocale}">
														<div class="contentDivFooter AUCTION_SCH_RF" style="width:101%;#width:99.29%;">
													</c:when>
													<c:otherwise>
														<div class="contentDivFooter AUCTION_SCH_RF" style="width:101%;#width:99.35%;">
													</c:otherwise>	
												</c:choose>
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$regionDetails.regionRecordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">
																<CENTER>
																	<t:dataScroller id="scroller" for="dt1"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$regionDetails.paginatorMaxPages}"
																		immediate="false" paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false"
																		pageIndexVar="pageNumber" styleClass="SCH_SCROLLER">
																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL"></t:graphicImage>
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
																			<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.pageNumber}" />
																					</td>
																				</tr>
																			</table>

																		</t:div>
																	</t:dataScroller>
																</CENTER>

															</td>
														</tr>
													</table>
												<div>
											</t:div>
									</div>

									</h:form>

									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
					<td colspan="2">
					<c:choose>
						<c:when test="${!pages$regionDetails.isViewModePopUp}">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</c:when>
					</c:choose>
					</td>
				</tr>
			</table>
			</div>
		</body>
</html>
</f:view>