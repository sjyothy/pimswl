
<t:div id="tabDDetails" style="width:100%;">
	<t:panelGrid id="ddTab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="selectOneFamilyVillageVillaStafflbl"
				styleClass="LABEL" value="#{msg['commons.Name']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="selectOneFamilyVillageVillaStaff"
			disabled="#{pages$familyVillageRequest.pageMode=='PAGE_MODE_REJECTED'}"
			value="#{pages$familyVillageRequest.selectOneFamilyVillageVillaStaff}">

			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems
				value="#{pages$familyVillageRequest.familyVillageStaffList}" />
		</h:selectOneMenu>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="selectOneFamilyVillageVillaStaffRolelbl"
				styleClass="LABEL"
				value="#{msg['familyVillagefile.lbl.StaffRole']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="selectOneFamilyVillageVillaStaffRole"
			disabled="#{pages$familyVillageRequest.pageMode=='PAGE_MODE_REJECTED'}"
			value="#{pages$familyVillageRequest.selectOneFamilyVillageVillaStaffRole}">

			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems value="#{pages$ApplicationBean.villaStaffRolesList}" />
		</h:selectOneMenu>
	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;">

		<h:commandButton styleClass="BUTTON" value="#{msg['commons.Add']}"
			onclick="performTabClick('lnkAdd');">
			<h:commandLink id="lnkAdd"
				action="#{pages$familyVillageRequest.onAddVillaStaff}" />
		</h:commandButton>
	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableVillaStaff" rows="15" width="100%"
			value="#{pages$familyVillageRequest.villaStaffList}"
			binding="#{pages$familyVillageRequest.dataTableVillaStaff}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="staffNamedtc" sortable="true">
				<f:facet name="header">
					<t:outputText id="staffNameOT" value="#{msg['commons.Name']}" />
				</f:facet>
				<h:outputText
					value="#{pages$familyVillageRequest.englishLocale? dataItem.secUser.fullName:dataItem.secUser.secondaryFullName}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="staffRoledtc" sortable="true">
				<f:facet name="header">
					<t:outputText id="staffRoleOT"
						value="#{msg['familyVillagefile.lbl.StaffRole']}" />
				</f:facet>
				<h:outputText
					value="#{pages$familyVillageRequest.englishLocale? dataItem.role.dataDescEn:dataItem.role.dataDescAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="action" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAmoun" value="#{msg['commons.action']}" />
				</f:facet>

				<h:commandLink id="lnkDelete"
					onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableInputs();"
					action="#{pages$familyVillageRequest.onDeleteVillaStaff}">
					<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
						url="../resources/images/delete.gif" width="18px;" />
				</h:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>

</t:div>


