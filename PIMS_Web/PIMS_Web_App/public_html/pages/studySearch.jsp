<%-- 
  - Author: Anil Verani
  - Date: 03/07/2009
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Intial / Feasibility Studies
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	
	function passSelectedStudiesAndClosePopup()
	{
		window.opener.receiveSelectedStudies();
		window.close();
	}

	function clearValues() 
	{
		document.getElementById('searchFrm:cntrlStudyNumber').value = '';
		document.getElementById('searchFrm:cntrlStudyStatus').value = '';
		document.getElementById('searchFrm:cntrlStudyStartDateFromInputDate').value = '';
		document.getElementById('searchFrm:cntrlStudyStartDateToInputDate').value = '';
		document.getElementById('searchFrm:cntrlStudyType').value = '';
		document.getElementById('searchFrm:cntrlStudySubject').value = '';
	}
	
	function openExportPopup()
	{
		var screen_width = 980;
		var screen_height = 350;
        var popup_width = screen_width;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('export.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
			 <title>PIMS</title>
			 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			 <meta http-equiv="pragma" content="no-cache">
			 <meta http-equiv="cache-control" content="no-cache">
			 <meta http-equiv="expires" content="0">
			 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
	</head>

	<body class="BODY_STYLE">
			<%
			   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
			   response.setHeader("Pragma","no-cache"); //HTTP 1.0
			   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
			%>
		<div class="containerDiv">	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
						<c:when test="${! (pages$studySearch.isPopupSingleMode || pages$studySearch.isPopupMultiMode)}">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>					
						</c:when>
					</c:choose>
				</tr>

				<tr width="100%">
					<c:choose>
						<c:when test="${! (pages$studySearch.isPopupSingleMode || pages$studySearch.isPopupMultiMode)}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>						
						</c:when>
					</c:choose>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['study.search.heading']}" styleClass="HEADER_FONT"/>																				
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td width="100%" height="466px" valign="top" >
									<div class="SCROLLABLE_SECTION  AUC_SCH_SS">
									<h:form id="searchFrm" style="WIDTH: 97.6%;">
										<table border="0" class="layoutTable">
											<tr>
												<td>
													<h:outputText value="#{pages$studySearch.errorMessages}"  escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$studySearch.successMessages}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
										<div class="MARGIN"> 
										<table cellpadding="0" cellspacing="0" width="100%">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
											<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
										<div class="DETAIL_SECTION">
											<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
											<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
											<tr>
											<td width="97%"> 
											<table width="100%">
												<tr>
													<td width="25%">
														<h:outputLabel styleClass="LABEL" value="#{msg['study.number']}:"></h:outputLabel>
													</td>
													<td width="25%">
														<h:inputText id="cntrlStudyNumber" value="#{pages$studySearch.studyDetailView.studyNumber}" maxlength="20" />														
													</td>													
													<td width="25%">
														<h:outputLabel styleClass="LABEL" value="#{msg['study.status']}:"></h:outputLabel>
													</td>
													<td width="25%">
														<h:selectOneMenu id="cntrlStudyStatus" binding="#{pages$studySearch.cmbStudyStatus}"
														 value="#{pages$studySearch.studyDetailView.statusId}" required="false">
															<f:selectItem itemValue="" itemLabel="#{msg['commons.All']}" />
															<f:selectItems value="#{pages$ApplicationBean.studyStatusList}" />
														</h:selectOneMenu>
													</td>
												</tr>
												
												<tr>
													<td >
														<h:outputLabel styleClass="LABEL" value="#{msg['study.startDateFrom']}:"></h:outputLabel>
													</td>
													<td >
														<rich:calendar id="cntrlStudyStartDateFrom"
																	   value="#{pages$studySearch.studyDetailView.startDate}"
																	   inputStyle="width: 170px; height: 14px"
								                        			   locale="#{pages$studySearch.locale}" 
								                        			   popup="true" 
								                        			   datePattern="#{pages$studySearch.dateFormat}" 
								                        			   showApplyButton="false" 
								                        			   enableManualInput="false" 
								                        			  />
													</td>
													<td >
														<h:outputLabel styleClass="LABEL" value="#{msg['study.startDateTo']}:" />
													</td>
													<td >
														<rich:calendar id="cntrlStudyStartDateTo"
																	   value="#{pages$studySearch.studyDetailView.endDate}"
																	   inputStyle="width: 170px; height: 14px"
								                        			   locale="#{pages$studySearch.locale}" 
								                        			   popup="true" 
								                        			   datePattern="#{pages$studySearch.dateFormat}" 
								                        			   showApplyButton="false" 
								                        			   enableManualInput="false" 
								                        			   />
								                    </td>
												</tr>
												
												<tr>
													<td >
														<h:outputLabel styleClass="LABEL" value="#{msg['study.type']}:"></h:outputLabel>
													</td>
													<td >
														<h:selectOneMenu id="cntrlStudyType" value="#{pages$studySearch.studyDetailView.studyTypeId}"  required="false">
															<f:selectItem itemValue="" itemLabel="#{msg['commons.All']}" />
															<f:selectItems value="#{pages$ApplicationBean.studyTypeList}" />
														</h:selectOneMenu>
													</td>
													
													<td >
														<h:outputLabel styleClass="LABEL" value="#{msg['study.subject']}:"></h:outputLabel>
													</td>
													<td >
														<h:selectOneMenu id="cntrlStudySubject" value="#{pages$studySearch.studyDetailView.studySubjectId}"  required="false">
															<f:selectItem itemValue="" itemLabel="#{msg['commons.All']}" />
															<f:selectItems value="#{pages$ApplicationBean.studySubjectList}" />
														</h:selectOneMenu>
													</td>
												</tr> 
												<tr>
													<td class="BUTTON_TD" colspan="4">
																	<h:commandButton styleClass="BUTTON" value="#{msg['commons.search']}" action="#{pages$studySearch.onSearch}" style="width: 75px" tabindex="7"></h:commandButton>							     									
																</td>
															</tr>
																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>

											</table>
										</div>	
										</div>
										<div style="padding-bottom:7px;padding-left:10px;padding-right:7px;padding-top:7px;">
										<div class="imag">&nbsp;</div>
										<div class="contentDiv" style="width: 99%">
											<t:dataTable 
												binding="#{pages$studySearch.dataTable}"
												id="dt1"
												value="#{pages$studySearch.studyDetailViewList}"												 
												rows="#{pages$studySearch.rowsPerPage}"
												preserveDataModel="false" preserveSort="false" 
												var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												width="100%">
												<t:column width="15%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['study.number']}"  />
													</f:facet>
													<t:outputText value="#{dataItem.studyNumber}" />
												</t:column>
												<t:column width="10%" sortable="true">
													<f:facet  name="header">
														<t:outputText value="#{msg['study.startDate']}" />
													</f:facet>
													<t:outputText value="#{dataItem.startDate}">
														<f:convertDateTime timeZone="#{pages$studySearch.timeZone}" pattern="#{pages$studySearch.dateFormat}" />
													</t:outputText>
												</t:column>
												<t:column width="15%" sortable="true">
													<f:facet  name="header">
														<t:outputText value="#{msg['study.endDate']}" />
													</f:facet>
													<t:outputText value="#{dataItem.endDate}">
														<f:convertDateTime timeZone="#{pages$studySearch.timeZone}" pattern="#{pages$studySearch.dateFormat}" />
													</t:outputText>
												</t:column>
												<t:column width="12%" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['commons.typeCol']}" />
													</f:facet>
													<t:outputText value="#{pages$studySearch.isEnglishLocale ? dataItem.studyTypeEn : dataItem.studyTypeAr}" />
												</t:column>												
												<t:column width="13%" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['TaskList.DataTable.Subject']}" />
													</f:facet>
													<t:outputText value="#{pages$studySearch.isEnglishLocale ? dataItem.studySubjectEn : dataItem.studySubjectAr}" />
												</t:column>												
												<t:column width="10%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['bidder.title']}" />
													</f:facet>
													<t:outputText value="#{dataItem.studyTitle}" />
												</t:column>												
												<t:column width="10%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.status']}" />
													</f:facet>
													<t:outputText value="#{pages$studySearch.isEnglishLocale ? dataItem.statusEn : dataItem.statusAr}" />
												</t:column>
												
												<t:column rendered="#{pages$studySearch.isPopupMultiMode}" sortable="false" width="15%">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.select']}" />
													</f:facet>
													<h:selectBooleanCheckbox value="#{dataItem.isSelected}" />
												</t:column>
												
												<t:column rendered="#{pages$studySearch.isPopupSingleMode}" sortable="false" width="15%">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.select']}" />
													</f:facet>
													<t:commandLink action="#{pages$studySearch.onSingleSelect}">															
														<h:graphicImage title="#{msg['commons.select']}" url="../resources/images/select-icon.gif" />&nbsp;
													</t:commandLink>
												</t:column>
												
												<t:column rendered="#{! (pages$studySearch.isPopupSingleMode || pages$studySearch.isPopupMultiMode)}" sortable="false" width="15%">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.action']}" />
													</f:facet>
													<t:commandLink id ="sdadsd" action="#{pages$studySearch.onView}">
															<h:graphicImage title="#{msg['commons.view']}" url="../resources/images/detail-icon.gif" />&nbsp;
													</t:commandLink>													
													
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													
													<t:commandLink action="#{pages$studySearch.onEdit}" rendered="#{pages$studySearch.editingEnable}">
															<h:graphicImage title="#{msg['commons.edit']}" url="../resources/images/edit-icon.gif" />&nbsp;
													</t:commandLink>
													
													<t:commandLink action="#{pages$studySearch.onAddToInvestmentPlan}" rendered="#{! pages$studySearch.editingEnable}">
															<h:graphicImage title="#{msg['add.to.investment.plan']}" url="../resources/images/app_icons/Approve-Request.png" />&nbsp;
													</t:commandLink>
													
													<t:outputLabel value=" " rendered="true"></t:outputLabel>													
													
													<t:commandLink rendered="#{pages$studySearch.editingEnable}" action="#{pages$studySearch.onCancel}" onclick="if (!confirm('#{msg['study.confirm.cancel']}')) return">
															<h:graphicImage title="#{msg['commons.cancel']}" url="../resources/images/app_icons/disable.png" />&nbsp;
													</t:commandLink>													
													
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													
													<t:commandLink rendered="#{pages$studySearch.editingEnable}" onclick="if (!confirm('#{msg['study.confirm.delete']}')) return" action="#{pages$studySearch.onDelete}">
														<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
													</t:commandLink>													
												</t:column>
												
											</t:dataTable>
										</div>
										<t:div styleClass="contentDivFooter" style="width:100%">
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$studySearch.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">
												 		
												<t:dataScroller id="scroller" for="dt1" paginator="true"
													fastStep="1" paginatorMaxPages="#{pages$studySearch.paginatorMaxPages}" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 								
												    paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;" 
													pageIndexVar="pageNumber"
													styleClass="SCH_SCROLLER" >
														<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
														<t:div styleClass="PAGE_NUM_BG">
															<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
														</t:div>
												</t:dataScroller>
													
                                        		</td></tr>
											</table>
											</t:div>
											
											<div>
												<table width="100%">
													<tr>
														<td class="BUTTON_TD" colspan="4">															
															<h:commandButton action="#{pages$studySearch.onMultiSelect}" rendered="#{pages$studySearch.isPopupMultiMode}" value="#{msg['commons.select']}" styleClass="BUTTON" />															
														</td>
													</tr>
												</table>
											</div>
											 
                                           </div>
                                           </div>
                                           
									</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${! (pages$studySearch.isPopupSingleMode || pages$studySearch.isPopupMultiMode)}">					
							<td colspan="2">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</tr>
								</table>
							</td>
						</c:when>
					</c:choose>					
				</tr>
			</table>
		</div>
	</body>
</html>
</f:view>