<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript"> 
function calculateMonthlyIncome(control){
removeNonNumeric(control);
var nodes = document.getElementById('detailsFrm:lastMonthlyIncomeTableId').getElementsByTagName('input');
	document.getElementById('detailsFrm:lastMonthlyIncomeTableId:0:totalMonthlyIncomeId').value ="";
	var total = 0 ;
	for(index = 0; index < nodes.length ; index++){
		if(nodes[index].value != null && nodes[index].value.length > 0 && isNumeric(nodes[index].value)){
			total = total + parseFloat(nodes[index].value);
		}
	}
	document.getElementById('detailsFrm:lastMonthlyIncomeTableId:0:totalMonthlyIncomeId').value = total;
}

function calculateAnnualIncome(control){
removeNonNumeric(control);
var nodes = document.getElementById('detailsFrm:lastAnnualIncomeTableId').getElementsByTagName('input');
document.getElementById('detailsFrm:lastAnnualIncomeTableId:0:totalAnnualIncomeId').value ="";
	var total = 0 ;
	for(index = 0; index < nodes.length ; index++){
		if(nodes[index].value != null && nodes[index].value.length > 0 && isNumeric(nodes[index].value)){
			total = total + parseFloat(nodes[index].value);
		}
	}
	document.getElementById('detailsFrm:lastAnnualIncomeTableId:0:totalAnnualIncomeId').value = total;
}

function calculateMonthlyExpense(control){
removeNonNumeric(control);
document.getElementById('detailsFrm:lastMonthlyExpenseTableId:0:totalMonthlyExpenseId').value ="";
	var nodes = document.getElementById('detailsFrm:lastMonthlyExpenseTableId').getElementsByTagName('input');
	var total = 0 ;
	for(index = 0; index < nodes.length ; index++){
		if(nodes[index].value != null && nodes[index].value.length > 0 && isNumeric(nodes[index].value)){
			total = total + parseFloat(nodes[index].value);
		}
	}
	document.getElementById('detailsFrm:lastMonthlyExpenseTableId:0:totalMonthlyExpenseId').value = total;
}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>">
				
			</script>
				
				
		</head>
		<body class="BODY_STYLE">
			<!-- Header -->
			<table width="100%" height="100%" cellpadding="0" cellspacing="0"
				border="0">
				<tr width="100%">
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99.2%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['beneficiaryDetails.tooltip.financial']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION"
										style="height: 420px; width: 100%; # height: 420px; # width: 100%;">
										<h:form id="detailsFrm" enctype="multipart/form-data"
											style="WIDTH: 94%;">


											<table border="0" class="layoutTable"
												style="margin-left: 15px; margin-right: 15px;">
												<tr>
													<td>
														<h:messages></h:messages>
														<h:outputText
															value="#{pages$financialAspectsPopup.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />
														<h:outputText
															value="#{pages$financialAspectsPopup.successMessages}"
															escape="false" styleClass="INFO_FONT" />
													</td>
												</tr>
											</table>



											<t:panelGrid cellpadding="1px" width="100%" cellspacing="5px"
												columns="4">
												<h:outputLabel styleClass="LABEL"
													value="#{msg['commons.Name']}:"></h:outputLabel>
												<h:outputText
													value="#{pages$financialAspectsPopup.view.personName}" />

												<h:outputLabel styleClass="LABEL"
													value="#{msg['ResearchFormBenef.incomeCategory']}:"></h:outputLabel>

												<h:selectOneMenu id="incomeCate"
													value="#{pages$financialAspectsPopup.view.incomeCategory}">
													<f:selectItem itemValue="-1"
														itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
													<f:selectItems
														value="#{pages$financialAspectsPopup.incomeCategoryList}" />
												</h:selectOneMenu>

												<h:outputLabel styleClass="LABEL"
													rendered="#{!pages$financialAspectsPopup.openedForOwner}"
													value="#{msg['generalAspects.orphanCategory']}:"></h:outputLabel>
												<h:selectOneMenu id="orphanCategory"
													rendered="#{!pages$financialAspectsPopup.openedForOwner}"
													value="#{pages$financialAspectsPopup.view.orphanCategoryId}">
													<f:selectItem itemValue="-1"
														itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
													<f:selectItems
														value="#{pages$ApplicationBean.allOrphanCategories}" />
												</h:selectOneMenu>

												<h:outputLabel styleClass="LABEL"
													value="#{msg['ResearchFormBenef.isMinor']}:"></h:outputLabel>
												<h:selectBooleanCheckbox id="isMinor"
													value="#{pages$financialAspectsPopup.view.minor}">
													<f:converter converterId="javax.faces.Boolean" />
												</h:selectBooleanCheckbox>


												<h:outputLabel styleClass="LABEL"
													value="#{msg['ResearchFormBenef.isStudent']}:"></h:outputLabel>
												<h:selectBooleanCheckbox id="isStudent"
													value="#{pages$financialAspectsPopup.view.student}">
													<f:converter converterId="javax.faces.Boolean" />
												</h:selectBooleanCheckbox>


											</t:panelGrid>
											<br />
											<t:div styleClass="BUTTON_TD">
												<h:commandButton styleClass="BUTTON" id="btnSaveUp"
													value="#{msg['commons.saveButton']}"
													onclick="if (!confirm('#{msg['confirmMsg.areuSureToSave']}')) return false;"
													action="#{pages$financialAspectsPopup.onSave}">
												</h:commandButton>

												<h:commandButton styleClass="BUTTON" id="btnCloseButton"
													value="#{msg['commons.closeButton']}"
													onclick="window.close();">
												</h:commandButton>
											</t:div>
											<div class="TAB_PANEL_INNER">
												<rich:tabPanel
													binding="#{pages$financialAspectsPopup.tabPanel}"
													style="MIN-HEIGHT: 200px;#MIN-HEIGHT: 200px;width: 100%;margin-left:5px;margin-top:5px;
												        margin-right:5px;margin-bottom:5px;">

													<rich:tab id="tabFinancialAspects"
														title="#{msg['researchFormBenef.financialAspects']}"
														label="#{msg['researchFormBenef.financialAspects']}">
														<%@include file="financialAspectsTab.jsp"%>
													</rich:tab>

												</rich:tabPanel>
											</div>

											<t:div styleClass="BUTTON_TD">
												<h:commandButton styleClass="BUTTON" id="btnSave"
													value="#{msg['commons.saveButton']}"
													onclick="if (!confirm('#{msg['confirmMsg.areuSureToSave']}')) return false;"
													action="#{pages$financialAspectsPopup.onSave}">
												</h:commandButton>

												<h:commandButton styleClass="BUTTON"
													value="#{msg['commons.closeButton']}"
													onclick="window.close();">
												</h:commandButton>
											</t:div>
										</h:form>
									</div>

								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>

		</body>
	</html>
</f:view>