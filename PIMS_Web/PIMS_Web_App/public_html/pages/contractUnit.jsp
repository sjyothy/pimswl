

<t:div styleClass="TAB_DETAIL_SECTION" style="width:100%">
	<t:panelGrid id="unitInfoTable" cellpadding="1px" width="100%"
		cellspacing="3px" styleClass="TAB_DETAIL_SECTION_INNER" columns="4"
		columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL" value="#{msg['unit.unitNumber']}:" />
		</h:panelGroup>
		<h:inputText maxlength="20" styleClass="READONLY" id="txtunitNumber"
			readonly="true"
			value="#{pages$LeaseContract.unitDataItem.unitNumber}"
			style="width:78%;"></h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['unit.costCenter']}:"></h:outputLabel>
		<h:inputText id="txtCostCenter" styleClass="READONLY"
			style="width:78%;" readonly="true" maxlength="20"
			value="#{pages$LeaseContract.unitDataItem.accountNumber}" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['unit.unitDescription']}:"></h:outputLabel>
		<h:inputText id="txtUnitDesc" styleClass="READONLY" style="width:78%;"
			readonly="true" maxlength="20"
			value="#{pages$LeaseContract.unitDataItem.unitDesc}" />
		<h:outputLabel styleClass="LABEL" value="#{msg['unit.ejariRefNum']}"></h:outputLabel>
		<h:inputText id="unitEjariRefNum" styleClass="READONLY" style="width:78%;"
			readonly="true" 
			value="#{pages$LeaseContract.unitDataItem.ejariReferenceNumber}" />
		<h:outputLabel styleClass="LABEL" value="#{msg['floor.floorNo']}:"></h:outputLabel>
		<h:inputText id="txtfloorNumber" styleClass="READONLY"
			style="width:78%;" readonly="true" maxlength="20"
			value="#{pages$LeaseContract.unitDataItem.floorNumber}" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['unit.endowed/minorName']}:"></h:outputLabel>
		<h:inputText id="txtEndowedMinorName" style="width:78%;"
			styleClass="READONLY" readonly="true"
			value="#{pages$LeaseContract.unitDataItem.propertyEndowedName}"></h:inputText>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['property.propertyNumber']}:"></h:outputLabel>
		<h:inputText styleClass="READONLY" id="txtpropertyNumber"
			style="width:78%;" readonly="true"
			value="#{pages$LeaseContract.unitDataItem.propertyNumber}"></h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['unit.usage']}:"></h:outputLabel>
		<h:inputText styleClass="READONLY" id="txtunitusageEn"
			style="width:78%;" rendered="#{pages$LeaseContract.isEnglishLocale}"
			readonly="true"
			value="#{pages$LeaseContract.unitDataItem.usageTypeEn}"></h:inputText>
		<h:inputText styleClass="READONLY" id="txtunitusageAr"
			style="width:78%;" rendered="#{pages$LeaseContract.isArabicLocale}"
			readonly="true"
			value="#{pages$LeaseContract.unitDataItem.usageTypeAr}"></h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['contact.country']}:"></h:outputLabel>
		<h:inputText styleClass="READONLY" id="txtCountryEn"
			style="width:78%;" rendered="#{pages$LeaseContract.isEnglishLocale}"
			readonly="true" value="#{pages$LeaseContract.unitDataItem.countryEn}"></h:inputText>
		<h:inputText styleClass="READONLY" id="txtCountryAr"
			style="width:78%;" rendered="#{pages$LeaseContract.isArabicLocale}"
			readonly="true" value="#{pages$LeaseContract.unitDataItem.countryAr}"></h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['commons.Emirate']}:"></h:outputLabel>
		<h:inputText styleClass="READONLY" id="txtEmirateEn"
			style="width:78%;" rendered="#{pages$LeaseContract.isEnglishLocale}"
			readonly="true" value="#{pages$LeaseContract.unitDataItem.stateEn}"></h:inputText>
		<h:inputText styleClass="READONLY" id="txtEmirateAr"
			style="width:78%;" rendered="#{pages$LeaseContract.isArabicLocale}"
			readonly="true" value="#{pages$LeaseContract.unitDataItem.stateAr}"></h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['commons.area']}:" />
		<h:inputText styleClass="READONLY" id="txtunitArea" readonly="true"
			value="#{pages$LeaseContract.unitDataItem.unitArea}"
			style="width:78%;"></h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['contact.street']}:" />
		<h:inputText styleClass="READONLY" id="txtstreet" readonly="true"
			value="#{pages$LeaseContract.unitDataItem.street}" style="width:78%;"></h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['unit.address']}:" />
		<h:inputText styleClass="READONLY" id="txtaddress" readonly="true"
			value="#{pages$LeaseContract.unitDataItem.unitAddress}"
			style="width:78%;"></h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['unit.offeredRent']}:" />
		<h:inputText styleClass="READONLY A_RIGHT_NUM" id="txtofferedRent"
			readonly="true" value="#{pages$LeaseContract.unitDataItem.rentValue}"
			style="width:78%;">
			<f:convertNumber minFractionDigits="2" maxFractionDigits="2"
				pattern="##,###,###.##" />
		</h:inputText>
		<h:outputLabel styleClass="LABEL" value="#{msg['unit.rentValue']}:" />
		<h:inputText styleClass="READONLY A_RIGHT_NUM" id="txtactualRent"
			binding="#{pages$LeaseContract.txt_ActualRent}" readonly="true"
			value="#{pages$LeaseContract.unitRentAmount}" style="width:78%;">

		</h:inputText>

	</t:panelGrid>
</t:div>
