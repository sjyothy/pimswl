
<script language="JavaScript" type="text/javascript">	
</script>
<t:div id="revenueAndExpenseContentDiv" styleClass="contentDiv"
	style="width:98%">
	<t:dataTable id="participantListsDataTable"
		rows="#{pages$tabRevenueAndExpensesList.paginatorRows}"
		value="#{pages$tabRevenueAndExpensesList.dataList}"
		binding="#{pages$tabRevenueAndExpensesList.dataTable}"
		preserveDataModel="false" preserveSort="false" var="dataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">

		<t:column id="revenueListyeartypeColumn">
			<f:facet name="header">
				<t:outputText value="#{msg['portfolioDetailsReport.year']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;" value="#{dataItem.year}" />
		</t:column>

		<t:column id="revenueListtypeColumn">
			<f:facet name="header">
				<t:outputText value="#{msg['inhDetails.reportLabel.revenue']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				value="#{dataItem.totalRevenue}" />
		</t:column>
		<t:column id="revenueListexpensestypeColumn">
			<f:facet name="header">
				<t:outputText value="#{msg['revenueandexpenseslist.lbl.expenses']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				value="#{dataItem.totalExpenses}" />
		</t:column>
		<t:column id="revenueListnetProfittypeColumn">
			<f:facet name="header">
				<t:outputText value="Net Profit" style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				value="#{dataItem.netProfit}" />
		</t:column>
		<t:column id="revenueListnreconsProfittypeColumn">
			<f:facet name="header">
				<t:outputText value="Reconstruction Percentage"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				value="#{dataItem.reconstructionPercentage}" />
		</t:column>

		<t:column id="revenueListnreconsAmProfittypeColumn">
			<f:facet name="header">
				<t:outputText value="Reconstruction Amount"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				value="#{dataItem.reconstructionAmount}" />
		</t:column>
		<t:column id="masrafAlWaqfiPercentagetcl">
			<f:facet name="header">
				<t:outputText value="Masraf Percentage"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				value="#{dataItem.masrafAlWaqfiPercentage}" />
		</t:column>

		<t:column id="masrafAlWaqfiAmounttclg">
			<f:facet name="header">
				<t:outputText value="Masraf Amount"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				value="#{dataItem.masrafAlWaqfiAmount}" />
		</t:column>


	</t:dataTable>
</t:div>
<t:div id="participantListDataTableFooter" styleClass="contentDivFooter"
	style="width:99%">
	<t:panelGrid id="participantListfooterTable" columns="2"
		cellpadding="0" cellspacing="0" width="100%"
		columnClasses="RECORD_NUM_TD,BUTTON_TD">


		<CENTER>
			<t:dataScroller id="participantListscroller"
				for="participantListsDataTable" paginator="true" fastStep="1"
				paginatorMaxPages="#{pages$tabRevenueAndExpensesList.paginatorMaxPages}"
				immediate="false" paginatorTableClass="paginator"
				renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
				styleClass="SCH_SCROLLER"
				paginatorActiveColumnStyle="font-weight:bold;"
				paginatorRenderLinkForActive="false"
				paginatorTableStyle="grid_paginator" layout="singleTable"
				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;">

				<f:facet name="first">
					<t:graphicImage url="../#{path.scroller_first}"
						id="lblparticipantListFirst"></t:graphicImage>
				</f:facet>

				<f:facet name="fastrewind">
					<t:graphicImage url="../#{path.scroller_fastRewind}"
						id="lblparticipantListF"></t:graphicImage>
				</f:facet>

				<f:facet name="fastforward">
					<t:graphicImage url="../#{path.scroller_fastForward}"
						id="lblparticipantListFF"></t:graphicImage>
				</f:facet>

				<f:facet name="last">
					<t:graphicImage url="../#{path.scroller_last}"
						id="lblparticipantListL"></t:graphicImage>
				</f:facet>


			</t:dataScroller>
		</CENTER>
	</t:panelGrid>
</t:div>
