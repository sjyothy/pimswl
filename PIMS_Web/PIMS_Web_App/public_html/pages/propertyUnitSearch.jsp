<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

            
<script language="javascript" type="text/javascript">
  
	function clearWindow()
	{
		    document.getElementById("formUnitSearch:txtPropertyNumber").value="";
		    document.getElementById("formUnitSearch:txtBuildingName").value="";
		    document.getElementById("formUnitSearch:txtUnitNo").value="";
		    document.getElementById("formUnitSearch:txtFloorNo").value="";
		    
		    var cmbCity=document.getElementById("formUnitSearch:state");
		     cmbCity.selectedIndex = 0;
		    
		    var cmbUnitUsage=document.getElementById("formUnitSearch:selectUnitUsage");
		     cmbUnitUsage.selectedIndex = 0;
		    
		    var cmbUnitType=document.getElementById("formUnitSearch:selectUnitType");
		     cmbUnitType.selectedIndex = 0;
		    
		    var cmbStatus=document.getElementById("formUnitSearch:selectStatus");
		     cmbStatus.selectedIndex = 0;
		     
		    var cmbStatus=document.getElementById("formUnitSearch:selectUnitSide");
		     cmbStatus.selectedIndex = 0; 
		     
		    
	}
	
</script>

<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
 
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	<title>PIMS</title>
	<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
	<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	<script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
 	</head>


	<body class="BODY_STYLE">
      <!-- Header --> 
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="2">
		        <jsp:include page="header.jsp"/>
		     </td>
		</tr>
		<tr width="100%">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
								<h:outputLabel value="#{msg['unit.SearchUnits']}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" 
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="99%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:475px;">
						   			<h:form id="formUnitSearch" style="width:100%;#width:98.1%;">
									<div style="height:25px">
										<table border="0" class="layoutTable">
											<tr>
												<td>
													<h:outputText value="#{pages$propertyUnitSearch.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
												</td>
											</tr>
										</table>
									</div>
									<div class="MARGIN" style="width:95%">
									<table cellpadding="0" cellspacing="0"  >
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
										<div class="DETAIL_SECTION">
										<h:outputLabel value="#{msg['inquiry.searchcriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
												<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
													<tr>
														<td colspan="4">
															<h:inputHidden id="pageMode"
																value="#{pages$propertyUnitSearch.pageMode}" />
														</td>
													</tr>
													<tr>
														<td class="DET_SEC_TD">
															<h:outputLabel value="#{msg['unit.unitNumber']}:"></h:outputLabel>
														</td>
														<td class="DET_SEC_TD">
															<h:inputText id="txtUnitNo" style="width: 142px;"
																value="#{pages$propertyUnitSearch.unitNumber}">
															</h:inputText>
														</td>
														<td class="DET_SEC_TD">
															<h:outputLabel value="#{msg['inquiry.floorNumber']}:"></h:outputLabel>
														</td>
														<td class="DET_SEC_TD">
															<h:inputText id="txtFloorNo" style="width: 142px;"
																value="#{pages$propertyUnitSearch.floorNumber}">

															</h:inputText>
														</td>
													</tr>
													<tr>
														<td class="DET_SEC_TD">
															<h:outputLabel value="#{msg['property.propertyNumber']}:"></h:outputLabel>
														</td>
														<td class="DET_SEC_TD">
															<h:inputText id="txtPropertyNumber" style="width: 142px;"
																value="#{pages$propertyUnitSearch.propertyNumber}">
															</h:inputText>
														</td>
														<td class="DET_SEC_TD">
															<h:outputLabel value="#{msg['property.buildingName']}:"></h:outputLabel>
														</td>
														<td class="DET_SEC_TD">
															<h:inputText id="txtBuildingName" style="width: 142px;"
																value="#{pages$propertyUnitSearch.buildingName}">
															</h:inputText>
														</td>
													</tr>
													<tr>
														<td class="DET_SEC_TD">
															<h:outputLabel value="#{msg['unit.unitUsage']}:"></h:outputLabel>
														</td>
														<td class="DET_SEC_TD">
															<h:selectOneMenu id="selectUnitUsage"
																style="width: 142px;"
																value="#{pages$propertyUnitSearch.selectOneUnitUsage}">
																<f:selectItem itemLabel="#{msg['commons.All']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$ApplicationBean.unitUsageTypeList}" />
															</h:selectOneMenu>
														</td>
														<td class="DET_SEC_TD">
															<h:outputLabel value="#{msg['unit.unitSide']}:"></h:outputLabel>
														</td>
														<td class="DET_SEC_TD">
															<h:selectOneMenu id="selectUnitSide" style="width: 142px;"
																value="#{pages$propertyUnitSearch.selectOneUnitSide}">
																<f:selectItem itemLabel="#{msg['commons.All']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$ApplicationBean.unitSideType}" />
															</h:selectOneMenu>
														</td>
													</tr>
													<tr>
														<td class="DET_SEC_TD">
															<h:outputLabel value="#{msg['unit.unitType']}:"></h:outputLabel>
														</td>
														<td class="DET_SEC_TD">
															<h:selectOneMenu id="selectUnitType" style="width: 142px;"
																value="#{pages$propertyUnitSearch.selectOneUnitType}">
																<f:selectItem itemLabel="#{msg['commons.All']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$ApplicationBean.unitTypeList}" />
															</h:selectOneMenu>

														</td>
														<td class="DET_SEC_TD">
															<h:outputLabel value="#{msg['unit.unitStatus']}:"></h:outputLabel>
														</td>
														<td class="DET_SEC_TD">
															<h:selectOneMenu id="selectStatus" style="width: 142px;"
																value="#{pages$propertyUnitSearch.selectOneUnitStatus}">
																<f:selectItem itemLabel="#{msg['commons.All']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$ApplicationBean.unitStatusList}" />
															</h:selectOneMenu>
														</td>
													</tr>
													<tr>
														<td class="DET_SEC_TD">
															<h:outputLabel value="#{msg['commons.Emirate']}:"></h:outputLabel>
														</td>
														<td class="DET_SEC_TD">
															<h:selectOneMenu id="state" style="width: 142px;" tabindex="5" value="#{pages$propertyUnitSearch.selectOneState}">
															<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
															<f:selectItems value="#{pages$propertyUnitSearch.state}" />
														    </h:selectOneMenu>
														</td>													</tr>
													<tr>
														<td class="BUTTON_TD JUG_BUTTON_TD_US" colspan="4" >
														    <h:commandButton styleClass="BUTTON" value="#{msg['unit.addButton']}" action="#{pages$propertyUnitSearch.addUnit}" />
															<h:commandButton styleClass="BUTTON" onclick="javascript:clearWindow();" value="#{msg['commons.reset']}" />
															<pims:security screen="Pims.PropertyManagement.Unit.Search" action="create">
																<h:commandButton styleClass="BUTTON" value="#{msg['commons.search']}"  action="#{pages$propertyUnitSearch.doSearch}" />
															</pims:security>
														</td>
													</tr>
												</table>
												</div>
												</div>
												<br>
												<div style="padding:5px;">
												<div class="imag">&nbsp;</div>
                   			               <div class="contentDiv" style="width:95%">
										         <t:dataTable id="test2"
													value="#{pages$propertyUnitSearch.unitDataList}"
													binding="#{pages$propertyUnitSearch.dataTable}"
													width="100%" 
													rows="#{pages$propertyUnitSearch.paginatorRows}" 
													preserveDataModel="false" preserveSort="false" var="dataItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

													<t:column id="col1" width="10%" sortable="true">

														<f:facet name="header" >

															<t:outputText value="#{msg['unit.numberCol']}" />

														</f:facet>
														<t:outputText value="#{dataItem.unitNumber}" styleClass="A_LEFT"/>
													</t:column>

												<t:column id="col2"  width="10%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['floor.floorNo']}" />
													</f:facet>
													<t:outputText value="#{dataItem.floorView.floorNumber}" styleClass="A_LEFT"/>
												</t:column>
												<t:column id="col3"  width = "10%" sortable="true">
													<f:facet name="header">
														<t:outputText value= "#{msg['property.propertyNumberCol']}" />
													</f:facet>
													<t:outputText value="#{dataItem.propertyNumber}" styleClass="A_LEFT"/>
												</t:column>
												<t:column id="propertyCommercialNameCol"  width = "10%" sortable="true">
													<f:facet name="header">
														<t:outputText value= "#{msg['property.buildingName']}" />
													</f:facet>
													<t:outputText value="#{dataItem.propertyCommercialName}" styleClass="A_LEFT"/>
												</t:column>
												<t:column id="stateEnCol" width = "10%" rendered="#{pages$propertyUnitSearch.isEnglishLocale}" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.Emirate']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.stateEn}" styleClass="A_LEFT"/>
													</t:column>
												<t:column id="stateArCol" width = "10%" rendered="#{pages$propertyUnitSearch.isArabicLocale}" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.Emirate']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.stateAr}" styleClass="A_LEFT"/>
													</t:column>	
												<t:column id="col5" width = "10%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.area']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.unitArea}" styleClass="A_RIGHT_NUM"/>
												</t:column>	
												<t:column id="unitTypeEnCol" width = "10%" rendered="#{pages$propertyUnitSearch.isEnglishLocale}" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.typeCol']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.unitTypeEn}" styleClass="A_LEFT"/>
													</t:column>
												<t:column id="unitTypeArCol" width = "10%" rendered="#{pages$propertyUnitSearch.isArabicLocale}" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.typeCol']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.unitTypeAr}" styleClass="A_LEFT"/>
													</t:column>	
									     		<t:column id="usageTypeEnCol" width = "10%" rendered="#{pages$propertyUnitSearch.isEnglishLocale}" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.usageCol']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.usageTypeEn}" styleClass="A_LEFT"/>
												</t:column>
												<t:column id="usageTypeArCol" width = "10%" rendered="#{pages$propertyUnitSearch.isArabicLocale}" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.usageCol']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.usageTypeAr}" styleClass="A_LEFT"/>
												</t:column>
												
												<t:column id="unitSideTypeEnCol" width = "10%" rendered="#{pages$propertyUnitSearch.isEnglishLocale}" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['unit.unitSideCol']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.unitSideTypeEn}" styleClass="A_LEFT"/>
												</t:column>
												<t:column id="unitSideTypeArCol" width = "10%" rendered="#{pages$propertyUnitSearch.isArabicLocale}" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['unit.unitSideCol']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.unitSideTypeAr}" styleClass="A_LEFT"/>
												</t:column>
												
												<t:column id="col7" width = "10%" rendered="#{pages$propertyUnitSearch.isEnglishLocale}" sortable="true">
													<f:facet name="header">
													 <t:outputText value="#{msg['commons.status']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.statusEn}" styleClass="A_LEFT"/>
												</t:column>
												<t:column id="statusArCol" width = "10%" rendered="#{pages$propertyUnitSearch.isArabicLocale}" sortable="true">
													<f:facet name="header">
													 <t:outputText value="#{msg['commons.status']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.statusAr}" styleClass="A_LEFT"/>
												</t:column>
												<t:column id="unitRent" width = "10%" sortable="true">
													<f:facet name="header">
													 <t:outputText value="#{msg['unit.rentValue']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.rentValue}" styleClass="A_RIGHT_NUM"/>
												</t:column>
											<pims:security screen="Pims.PropertyManagement.Unit.SearchActions" action="create">
	                                            <t:column id="editCol" width="5%" rendered="true" sortable="false">
		                                            <f:facet name="header">
		                                            	<t:outputText value="#{msg['commons.edit']}" />
		                                            </f:facet>
	                                            
		                                            <t:commandLink action="#{pages$propertyUnitSearch.editUnit}" rendered="true">
														<h:graphicImage id="editIcon" title="#{msg['commons.edit']}" url="../resources/images/edit-icon.gif" />
													</t:commandLink>
	                                           </t:column>
	                                        </pims:security>	
                                        </t:dataTable>	
											</div>
											<t:div styleClass="contentDivFooter" style="width:95.8%" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$propertyUnitSearch.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:40%" align="right">
											<CENTER>
												<t:dataScroller id="scroller" for="test2" paginator="true"
													fastStep="1" paginatorMaxPages="#{pages$propertyUnitSearch.paginatorMaxPages}" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 
													pageIndexVar="pageNumber"
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">
													
													<f:facet name="first">
														<t:graphicImage url="../resources/images/first_btn.gif" id="lblF"></t:graphicImage>
													</f:facet>
													<f:facet name="fastrewind">
														<t:graphicImage url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>
													</f:facet>
													<f:facet name="fastforward">
														<t:graphicImage url="../resources/images/next_btn.gif" id="lblFF"></t:graphicImage>
													</f:facet>
													<f:facet name="last">
														<t:graphicImage url="../resources/images/last_btn.gif" id="lblL"></t:graphicImage>
													</f:facet>
													<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
													</t:div>
												</t:dataScroller>
											</CENTER>
											</td></tr>
											</table>
                                           </t:div>
                                           </div>
									</h:form>
				</div>
				</td></tr></table></td></tr>
					<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
</table>
			</body>
	</html>
</f:view>