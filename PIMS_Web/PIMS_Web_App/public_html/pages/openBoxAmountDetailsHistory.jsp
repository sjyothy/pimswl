
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
		
	function openAmountDetailsPopup(id,amountDetailsId)
	{
	
	   var screen_width   = 0.65 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   window.open('addOpenBoxAmountDetails.jsf?id='+id+'&amountDetailsId='+amountDetailsId,'_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	}
	
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel
										value="#{msg['openBox.heading.amountDetailsHistory']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="frm" enctype="multipart/form-data">
										<div>
											<table border="0" class="layoutTable">
												<tr>
													<td>
														<h:messages></h:messages>
														<h:outputText
															value="#{pages$openBoxAmountDetailsHistory.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />


													</td>
												</tr>
											</table>
										</div>
										<div class="SCROLLABLE_SECTION">

											<t:div styleClass="contentDiv" id="parentDivs"
												style="width:95%">
												<t:dataTable id="tdatatable" width="100%" styleClass="grid"
													value="#{pages$openBoxAmountDetailsHistory.list}"
													rows="200" preserveDataModel="true" preserveSort="false"
													var="dataItem" rowClasses="row1,row2">

													<t:column id="userName" width="10%" sortable="false"
														style="white-space: normal;">
														<f:facet name="header">
															<t:outputText value="#{msg['commons.username']}" />
														</f:facet>
														<t:outputText value="#{dataItem.createdByName}" />
													</t:column>

													<t:column id="date" width="10%" sortable="false"
														style="white-space: normal;">
														<f:facet name="header">
															<t:outputText value="#{msg['commons.date']}" />
														</f:facet>
														<t:outputText value="#{dataItem.createdOn}" />
													</t:column>

													<t:column id="oldAmount" width="10%" sortable="false"
														style="white-space: normal;">
														<f:facet name="header">
															<t:outputText value="#{msg['openBox.oldAmount']}" />
														</f:facet>
														<t:outputText value="#{dataItem.currentAmount}" />
													</t:column>

													<t:column id="newAmount" width="10%" sortable="false"
														style="white-space: normal;">
														<f:facet name="header">
															<t:outputText value="#{msg['openBox.lbl.newAmount']}" />
														</f:facet>
														<t:outputText value="#{dataItem.newAmount}" />
													</t:column>
													<t:column id="reasons" width="10%" sortable="false"
														style="white-space: normal;">
														<f:facet name="header">
															<t:outputText value="#{msg['commons.reasons']}" />
														</f:facet>
														<t:outputText value="#{dataItem.changeReason}" />
													</t:column>
													<t:column id="action" width="10%" sortable="false"
														style="white-space: normal;">
														<f:facet name="header">
															<t:outputText value="#{msg['commons.action']}" />
														</f:facet>
														<h:graphicImage id="openAmountDetailsPopup"
															style="margin-right:5px;"
															title="#{msg['openBox.heading.amountDetails']}"
															onclick="javaScript:openAmountDetailsPopup('#{dataItem.openBox.openBoxId}',
																									   '#{dataItem.amountDetailsId}'
																									   );"
															url="../resources/images/app_icons/Settle-Contract.png" />
													</t:column>


												</t:dataTable>
											</t:div>
										</div>
										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

		</body>
	</html>
</f:view>