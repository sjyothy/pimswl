<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript" src="../resources/jscripts/commons.js"></script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html style="overflow:hidden; " dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<script language="javascript" type="text/javascript">
	function showSearchProjectPopUp(pageModeKey,selectOnePopKey)
   {
		var screen_width = 1024;
		var screen_height = 450;
		window.open('projectSearch.jsf?'+'PAGE_MODE'+'='+'MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
   }
   function populateProject(projectId, projectName, projectNumber, projectEstimatedCost)
    {    	
    	document.getElementById("cancelContractForm:hdnProjectId").value = projectId;
    	document.getElementById("cancelContractForm:hdnProjectNumber").value = projectNumber;
    	document.getElementById("cancelContractForm:projectNumber").value = projectNumber;
    	 document.forms[0].submit();
    }



	
		
	function GenerateFloorsPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 120;
	   var screen_left = 150;
	   window.open('GenerateFloors.jsf','_blank','width='+(screen_width)+',height='+(screen_height)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}
function SearchMemberPopup(){
   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 120;
	   var screen_left = 150;
	   window.open('SearchMemberPopup.jsf','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');

}
function DuplicateFloorPopup(){
   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 300;
	   var screen_left = 250;
	   window.open('DuplicateFloorsPopup.jsf','_blank','width='+(screen_width-400)+',height='+(screen_height-200)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');

}
function DuplicateUnitPopup(){
   var screen_width = 800;
	   var screen_height = 270;
	   var screen_top = 300;
	   var screen_left = 250;
	   window.open('DuplicateUnits.jsf','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');

}

function ApplyRentValuePopup(){
   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 120;
	   var screen_left = 150;
	   window.open('ApplyRentValue.jsf','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');

}

		function OwnerSearchPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 120;
	   var screen_left = 150;
	   window.open('SearchPerson.jsf?persontype=OWNER&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}
	
		function RepSearchPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 120;
	   var screen_left = 150;
	   window.open('SearchPerson.jsf?persontype=REPRESENTATIVE&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		    
		   
		    if(hdnPersonType=='OWNER')
		    {
			 document.getElementById("cancelContractForm:ownerRefNo").value=personName;
			 document.getElementById("cancelContractForm:ownerHiddenRefNo").value=personName;

			 document.getElementById("cancelContractForm:ownerHiddenId").value=personId;
	         //document.getElementById("formRequest:hdntenanttypeId").value=typeId;
	        }
	        else if(hdnPersonType=='REPRESENTATIVE')
	        {
	         document.getElementById("cancelContractForm:representative").value=personName;
			 document.getElementById("cancelContractForm:representativeHiddenId").value=personId;
			 document.getElementById("cancelContractForm:representativeHiddenRefNo").value=personName;

	     
	        }
	        document.forms[0].submit();
	
	}
	function GenerateUnitsPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 1024/4;
	   var screen_left = 450/4;
	   window.open('GenerateUnits.jsf','_blank','width='+(screen_width-300)+',height='+(screen_height-500)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}
	       function submitForm()
	   {
          document.getElementById('ReceivePropertyForm').submit();
	   }
	function FacilitiesPopup()
	{
   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 1024/4;
	   var screen_left = 450/4;
	   window.open('FacilityListPopup.jsf','_blank','width='+(screen_width-280)+',height='+(screen_height-400)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=no,status=yes,location=no,directories=no,resizable=no');	
	}
</script>
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<style>
.COL1 {
	width: 18%;
}



</style>
		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table class="greyPanelTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['receiveProperty.tabName.ChangeRent']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0" height="88%">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<td width="100%" height="99%" valign="top" nowrap="nowrap">
										<h:form id="cancelContractForm" enctype="multipart/form-data">
											<div class="SCROLLABLE_SECTION">

												<table border="0" class="layoutTable"
													style="margin-left: 10px; margin-right: 10px;">
													<tr>
														<td>
															<h:outputText
																value="#{pages$ChangePropertyUnitRent.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />

															<h:outputText id="opMsgs"
																value="#{pages$ChangePropertyUnitRent.messages}"
																styleClass="INFO_FONT" escape="false" />
														</td>
													</tr>
												</table>
												<h:inputHidden id="hdnProjectId"
													value="#{pages$ChangePropertyUnitRent.hdnprojectId}">
												</h:inputHidden>
												<h:inputHidden id="hdnProjectNumber"
													value="#{pages$ChangePropertyUnitRent.hdnprojectNumber}">
												</h:inputHidden>
												<h:inputHidden id="ownerHiddenId"
													binding="#{pages$ChangePropertyUnitRent.ownerHiddenId}" />
												<h:inputHidden id="showtabsHiddenId"
													binding="#{pages$ChangePropertyUnitRent.showTabsHidden}" />
												<h:inputHidden id="representativeHiddenId"
													binding="#{pages$ChangePropertyUnitRent.representativeHiddenId}" />

												<div class="MARGIN" style="width: 95%; height: 48%;">
													<div style="height: 206px;">
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td style="FONT-SIZE: 0px;">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" />
																</td>
																<td style="FONT-SIZE: 0px;" width="100%">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" />
																</td>
																<td style="FONT-SIZE: 0px;">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER"
															style="height: 300px; # height: 371px;">
															<rich:tabPanel id="one1"
																style="width:100%;HEIGHT: 355px;"
																binding="#{pages$ChangePropertyUnitRent.richTabPanel}">
																<rich:tab id="propertyBaseTab"
																	label="#{msg['receiveProperty.propertyTabHeader']}">
																	<h:inputHidden
																		binding="#{pages$ChangePropertyUnitRent.floorHiddenId}" />
																	<h:inputHidden
																		binding="#{pages$ChangePropertyUnitRent.hiddenCounter}" />
																	<h:inputHidden
																		binding="#{pages$ChangePropertyUnitRent.unitHiddenId}" />
																	<h:inputHidden
																		binding="#{pages$ChangePropertyUnitRent.unitStatusId}" />
																	<h:inputHidden
																		binding="#{pages$ChangePropertyUnitRent.contactInfoId}" />
																	<t:panelGrid width="100%" columns="4">
																		<h:outputLabel
																			value="#{msg['receiveProperty.propertyReferenceNumber']}:"></h:outputLabel>

																		<h:inputText id="propertyRefNo"
																			binding="#{pages$ChangePropertyUnitRent.propertyReferenceNumber}"
																			styleClass="READONLY"></h:inputText>
																		<h:outputLabel
																			value="#{msg['receiveProperty.propertyStatus']}:"></h:outputLabel>

																		<h:inputText id="propertyStatus" readonly="true"
																			styleClass="READONLY"
																			binding="#{pages$ChangePropertyUnitRent.statusText}" />
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel
																				value="#{msg['receiveProperty.commercialName']}:"></h:outputLabel>
																		</t:panelGroup>
																		<h:inputText id="commercialName" maxlength="150"
																			binding="#{pages$ChangePropertyUnitRent.commercialName}" />
																		
																		<h:outputLabel
																			value="#{msg['receiveProperty.projectNumber']}:"></h:outputLabel>
																		<h:panelGroup>
																		<h:inputText id="projectNumber" maxlength="50"
																			disabled="true"
																			binding="#{pages$ChangePropertyUnitRent.projectNumber}" />
																			<h:graphicImage url="../resources/images/app_icons/Search-tenant.png"
																							binding="#{pages$ChangePropertyUnitRent.projectImage}"
																							onclick="javaScript:showSearchProjectPopUp();"  
																							alt="#{msg[commons.searchProject]}">
																			</h:graphicImage>	
																		</h:panelGroup>
																		<h:outputLabel
																			value="#{msg['receiveProperty.endowedName']}:"></h:outputLabel>

																		<h:inputText id="endowedName" maxlength="150"
																			binding="#{pages$ChangePropertyUnitRent.endowedName}" />
																		<h:outputLabel
																			value="#{msg['receiveProperty.endowedMinorNo']}:"></h:outputLabel>

																		<h:inputText id="endowedNo" maxlength="50"
																			binding="#{pages$ChangePropertyUnitRent.endowedMinorNo}" />
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel
																				value="#{msg['receiveProperty.ownershipType']}:"></h:outputLabel>
																		</t:panelGroup>
																		<h:selectOneMenu id="owner" 
																			binding="#{pages$ChangePropertyUnitRent.ownershipType}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />

																			<f:selectItems
																				value="#{pages$ChangePropertyUnitRent.ownershipItems}" />
																		</h:selectOneMenu>
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel
																				value="#{msg['receiveProperty.investmentPurpose']}:"></h:outputLabel>
																		</t:panelGroup>
																		<h:selectOneMenu id="purpose" 
																			binding="#{pages$ChangePropertyUnitRent.proeprtyInvestmentPurpose}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />

																			<f:selectItems
																				value="#{pages$ChangePropertyUnitRent.investmentPurposes}" />
																		</h:selectOneMenu>

																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel
																				value="#{msg['receiveProperty.propertyUsage']}:"></h:outputLabel>
																		</t:panelGroup>
																		<h:selectOneMenu id="usage" 
																			binding="#{pages$ChangePropertyUnitRent.propertyUsage}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />

																			<f:selectItems
																				value="#{pages$ChangePropertyUnitRent.usageItems}" />
																		</h:selectOneMenu>


																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel
																				value="#{msg['receiveProperty.propertyType']}:"></h:outputLabel>
																		</t:panelGroup>
																		<h:selectOneMenu id="propertyType"
																			
																			binding="#{pages$ChangePropertyUnitRent.propertyType}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />

																			<f:selectItems
																				value="#{pages$ChangePropertyUnitRent.propertyItems}" />
																		</h:selectOneMenu>
																		<h:outputLabel
																			value="#{msg['receiveProperty.country']}:"></h:outputLabel>
																		<h:selectOneMenu id="country" 
																			value="#{pages$ChangePropertyUnitRent.countryId}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																			<f:selectItems
																				value="#{pages$ChangePropertyUnitRent.countryList}" />
																			<a4j:support event="onchange"
																				action="#{pages$ChangePropertyUnitRent.loadState}"
																				reRender="state,selectcity" />
																		</h:selectOneMenu>
																		<h:outputLabel
																			value="#{msg['receiveProperty.emirate']}:"></h:outputLabel>
																		<h:selectOneMenu id="state" 
																			required="false"
																			value="#{pages$ChangePropertyUnitRent.stateId}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																			<f:selectItems
																				value="#{pages$ChangePropertyUnitRent.stateList}" />
																			<a4j:support event="onchange"
																				action="#{pages$ChangePropertyUnitRent.loadCity}"
																				reRender="selectcity" />
																		</h:selectOneMenu>

																		<h:outputLabel value="#{msg['receiveProperty.city']}:"></h:outputLabel>
																		<h:selectOneMenu id="selectcity"
																			binding="#{pages$ChangePropertyUnitRent.cityMenu}"
																			value="#{pages$ChangePropertyUnitRent.cityId}">
																			<f:selectItem itemLabel="#{msg['commons.All']}"
																				itemValue="1" />
																			<f:selectItems
																				value="#{pages$ChangePropertyUnitRent.cityList}" />
																		</h:selectOneMenu>
																			<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel
																				value="#{msg['receiveProperty.landNumber']}:"></h:outputLabel>
																		</t:panelGroup>
																		<h:inputText id="landNumber" maxlength="50"
																			binding="#{pages$ChangePropertyUnitRent.landNumber}" />
																		
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel
																				value="#{msg['receiveProperty.addressLineOne']}:"></h:outputLabel>
																		</t:panelGroup>
																		<h:inputText id="addressLineOne" maxlength="150"
																			binding="#{pages$ChangePropertyUnitRent.addressLineOne}" />
																		<h:outputLabel
																			value="#{msg['receiveProperty.streetInformation']}:"></h:outputLabel>
																		<h:inputText id="streetInfo" maxlength="150"
																			binding="#{pages$ChangePropertyUnitRent.streetInformation}" />	
																		<h:outputLabel
																			value="#{msg['receiveProperty.EWUtilityNo']}:"></h:outputLabel>
																		<h:inputText id="eqewUtilityNo" maxlength="50"
																			binding="#{pages$ChangePropertyUnitRent.ewUtilityNo}" />

																		<h:outputLabel
																			value="#{msg['receiveProperty.buildingCoordinates']}:"></h:outputLabel>
																		<h:inputText id="coordinates"
																			binding="#{pages$ChangePropertyUnitRent.buildingCoordinates}"
																			maxlength="50" />
																		<h:outputLabel
																			value="#{msg['receiveProperty.landArea']}:"></h:outputLabel>


																		<h:inputText id="landArea" style="text-align: right;"
																			binding="#{pages$ChangePropertyUnitRent.landArea}" />

																		<h:outputLabel
																			value="#{msg['receiveProperty.builtInArea']}:"></h:outputLabel>


																		<h:inputText id="builtInAreaNumber"
																			style="text-align: right;"
																			binding="#{pages$ChangePropertyUnitRent.builtInArea}" />
																		<h:outputLabel
																			value="#{msg['receiveProperty.totalFloors']}:"></h:outputLabel>


																		<h:inputText id="totalNoOfFloors" readonly="true"
																			binding="#{pages$ChangePropertyUnitRent.totalNoOfFloors}">

																		</h:inputText>

																		<h:outputLabel
																			value="#{msg['receiveProperty.totalMezzanine']}:"></h:outputLabel>


																		<h:inputText id="totalMezzanineFloors" readonly="true"
																			binding="#{pages$ChangePropertyUnitRent.totalMezzanineFloors}">

																		</h:inputText>
																		<h:outputLabel
																			value="#{msg['receiveProperty.totalUnits']}:"></h:outputLabel>


																		<h:inputText id="totalNoOfUnits" readonly="true"
																			binding="#{pages$ChangePropertyUnitRent.totalNoOfUnits}">

																		</h:inputText>
																		<h:outputLabel
																			value="#{msg['receiveProperty.totalParking']}:"></h:outputLabel>


																		<h:inputText id="totalNoOfParkingFloors"
																			readonly="true"
																			binding="#{pages$ChangePropertyUnitRent.totalNoOfParkingFloors}">

																		</h:inputText>
																		<h:outputLabel
																			value="#{msg['receiveProperty.numberOfLifts']}:"></h:outputLabel>

																		<h:inputText id="numberOfLifts"
																			binding="#{pages$ChangePropertyUnitRent.numberOfLifts}" />




																		<h:outputLabel
																			value="#{msg['receiveProperty.annualRent']}:"></h:outputLabel>

																		<h:inputText id="totalRent" styleClass="A_RIGHT"
																			style="width:187px;"
																			binding="#{pages$ChangePropertyUnitRent.totalAnnualRent}" />


																	





																	</t:panelGrid>

																</rich:tab>

																<rich:tab rendered="#{pages$ChangePropertyUnitRent.showTabs}"
																	label="#{msg['receiveProperty.facilityTabHeader']}">
																	<jsp:include page="propertyFacilityTab.jsp" />
																</rich:tab>



																<rich:tab rendered="#{pages$ChangePropertyUnitRent.showTabs}"
																	label="#{msg['receiveProperty.floorTabHeader']}">
																	<t:div style="height:320px;overflow-y:scroll;">
																		<t:panelGrid width="97%" columns="4" id="floorGrid" >
																			<h:outputLabel styleClass="LABEL" value="#{msg['receiveProperty.floorType']}:"/>
																			<h:selectOneMenu id="floorTYpe" binding="#{pages$ChangePropertyUnitRent.floorTypeSelectOneMenu}">
																				<f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems value="#{pages$ChangePropertyUnitRent.floorTypeItems}" />
																			</h:selectOneMenu>
																			
																			<h:outputLabel styleClass="LABEL" value="#{msg['receiveProperty.floorSeperator']}:"/>
																			<h:inputText id="floorSeperateor" binding="#{pages$ChangePropertyUnitRent.floorSeperator}" />
																			
																			<h:outputLabel styleClass="LABEL" value="#{msg['receiveProperty.floorPrefix']}:"/>
																			<h:inputText id="floorPrefix" binding="#{pages$ChangePropertyUnitRent.floorPrefix}" />



																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL" id="floorNoFromLabel" value="#{msg['receiveProperty.floorNumberFrom']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:inputText id="floorNoFrom"
																				binding="#{pages$ChangePropertyUnitRent.floorFrom}" />
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					id="floorNoToLabel"
																					value="#{msg['receiveProperty.floorNumberTo']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:inputText id="floorNoTo"
																				binding="#{pages$ChangePropertyUnitRent.floorTo}" />
																				<t:panelGroup>
																				<!--<h:outputLabel styleClass="mandatory" value="*" />-->
																				<h:outputLabel styleClass="LABEL" value="#{msg['receiveProperty.generateAfterThisFloor']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:selectOneMenu id="afterFloorsMenu"  binding="#{pages$ChangePropertyUnitRent.afterFloorMenu}">
																				<f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems value="#{pages$ChangePropertyUnitRent.floorsList}" />

																			</h:selectOneMenu>
																			<h:outputLabel id="floorDescription" styleClass="LABEL" value="#{msg['receiveProperty.floorDescription']}:"/>
																			<h:inputTextarea id="floorDescriptionTextArea" style="WIDTH: 186px;" binding="#{pages$ChangePropertyUnitRent.floorDescription}"/>

																		</t:panelGrid>



																		<t:panelGrid id="floorbuttongrid" width="97%"
																			columnClasses="BUTTON_TD" columns="1">


																			<h:commandButton styleClass="BUTTON"
																				binding="#{pages$ChangePropertyUnitRent.addFloorButton}"
																				value="#{msg['commons.Add']}"
																				action="#{pages$ChangePropertyUnitRent.generatePropertyFloors}" />

																		</t:panelGrid>
																		<t:div style="text-align:center;">
																			<t:div styleClass="contentDiv" style="width:95%">
																				<t:dataTable id="floors" rows="15" width="100%"
																					value="#{pages$ChangePropertyUnitRent.floorsDataList}"
																					binding="#{pages$ChangePropertyUnitRent.floorTable}"
																					preserveDataModel="false" preserveSort="false"
																					var="floorDataItem" rowClasses="row1,row2"
																					rules="all" renderedIfEmpty="true">
																					<t:column id="col_mutliSelect" style="width:10%;" >
																						<h:selectBooleanCheckbox id="multiFloor"
																						disabled="#{floorDataItem.typeId == pages$ChangePropertyUnitRent.groundFloorTypeID}"
																							style="width:25%;"
																							value="#{floorDataItem.isSelected}" />
																					</t:column>


																					<t:column id="col_floorNo" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.floorNumberHeading']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{floorDataItem.floorNumber}" />
																					</t:column>
																					<t:column id="col_floorDesc">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.floorDescription']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{floorDataItem.floorDescription}" />
																					</t:column>
																					<t:column id="col_floorStatus" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.floorType']}" />
																						</f:facet>
																						<t:outputText value="#{floorDataItem.floorTypeEn}"
																							rendered="#{pages$ChangePropertyUnitRent.isEnglishLocale}" />
																						<t:outputText value="#{floorDataItem.floorTypeAr}"
																							rendered="#{pages$ChangePropertyUnitRent.isArabicLocale}" />
																					</t:column>
																					<t:column id="col_floorUnits" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.noOfUnits']}" />
																						</f:facet>
																						<t:outputText value="#{floorDataItem.noOfUnits}" />
																					</t:column>
																					<t:column
																						rendered="#{pages$ChangePropertyUnitRent.deleteAction}">
																						<f:facet name="header">
																							<t:outputText id="lbl_edit"
																								value="#{msg['commons.action']}" />
																						</f:facet>

																						<h:commandLink
																						rendered="#{floorDataItem.typeId != pages$ChangePropertyUnitRent.groundFloorTypeID}"
																							action="#{pages$ChangePropertyUnitRent.showDuplicateFloorsSearchPopup}">
																							<h:graphicImage id="edit_Icon"
																								title="#{msg['commons.duplicate']}"
																								url="../resources/images/duplicate.png"
																								width="18px;" />
																						</h:commandLink>
																						<h:commandLink id="deleteIconRCVFloor" 
																						rendered="#{floorDataItem.typeId != pages$ChangePropertyUnitRent.groundFloorTypeID}"
																							action="#{pages$ChangePropertyUnitRent.deleteFloor}">
																							<h:graphicImage id="delete_IconRCVFloor"
																								title="#{msg['commons.delete']}"
																								url="../resources/images/delete_icon.png" />
																						</h:commandLink>

																					</t:column>
																					
																					<t:column id="col_floorSeq" rendered="false" >
																						<f:facet name="header">
																							<t:outputText
																								value="Foor Sequence" />
																						</f:facet>
																						<t:outputText value="#{floorDataItem.floorSequence}" />
																					</t:column>

																				</t:dataTable>
																			</t:div>

																			<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																				style="width:96%;">
																				<t:panelGrid cellpadding="0" cellspacing="0"
																					width="100%" columns="2"
																					columnClasses="RECORD_NUM_TD">

																					<t:div styleClass="RECORD_NUM_BG">
																						<t:panelGrid cellpadding="0" cellspacing="0"
																							columns="3" columnClasses="RECORD_NUM_TD">

																							<h:outputText
																								value="#{msg['commons.recordsFound']}" />

																							<h:outputText value=" : "
																								styleClass="RECORD_NUM_TD" />

																							<h:outputText
																								value="#{pages$ChangePropertyUnitRent.floorRecordSize}"
																								styleClass="RECORD_NUM_TD" />

																						</t:panelGrid>
																					</t:div>

																					<t:dataScroller id="floorScroller" for="floors"
																						paginator="true" fastStep="1"
																						paginatorMaxPages="10" immediate="false"
																						styleClass="SCH_SCROLLER"
																						paginatorTableClass="paginator"
																						renderFacetsIfSinglePage="true"
																						paginatorTableStyle="grid_paginator"
																						layout="singleTable"
																						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																						paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																						pageIndexVar="floorPageNumber">

																						<f:facet name="first">
																							<t:graphicImage url="../#{path.scroller_first}"
																								id="lblFfloor"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastrewind">
																							<t:graphicImage
																								url="../#{path.scroller_fastRewind}"
																								id="lblFRfloor"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastforward">
																							<t:graphicImage
																								url="../#{path.scroller_fastForward}"
																								id="lblFFfloor"></t:graphicImage>
																						</f:facet>
																						<f:facet name="last">
																							<t:graphicImage url="../#{path.scroller_last}"
																								id="lblLfloor"></t:graphicImage>
																						</f:facet>
																						<t:div styleClass="PAGE_NUM_BG">
																							<h:outputText styleClass="PAGE_NUM"
																								value="#{msg['commons.page']}" />
																							<h:outputText styleClass="PAGE_NUM"
																								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																								value="#{requestScope.floorPageNumber}" />
																						</t:div>
																					</t:dataScroller>

																				</t:panelGrid>
																			</t:div>

																		</t:div>
																		<t:panelGrid id="deleteFloorsbuttongrid" width="97%"
																			columnClasses="BUTTON_TD" columns="1">

																			<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.delete']}"
																				action="#{pages$ChangePropertyUnitRent.deleteMultipleFloors}"
																				rendered="#{pages$ChangePropertyUnitRent.deleteAction}">
																			</h:commandButton>
																		</t:panelGrid>
																	</t:div>

																</rich:tab>
																<rich:tab rendered="#{pages$ChangePropertyUnitRent.showTabs}" id="unitTab" label="#{msg['receiveProperty.unitTabHeader']}">
														             <t:div style="height:320px;overflow-y:scroll;">
																		<t:panelGrid width="97%" columns="4" id="unitGrid">
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.floorNumber']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:selectOneMenu id="floorsMenu"
																				
																				binding="#{pages$ChangePropertyUnitRent.floorNumberMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ChangePropertyUnitRent.floorsList}" />

																			</h:selectOneMenu>

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unitPrefix']}:"></h:outputLabel>

																			<h:inputText id="unitPrefix"
																				binding="#{pages$ChangePropertyUnitRent.unitPrefix}" />
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.unitNo']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:inputText id="unitNo"
																				binding="#{pages$ChangePropertyUnitRent.unitNo}" />

																			<h:outputLabel styleClass="LABEL" rendered="false"
																				value="#{msg['receiveProperty.unitNumber']}:"></h:outputLabel>

																			<h:inputText id="unitNumber" rendered="false"
																				binding="#{pages$ChangePropertyUnitRent.unitNumber}" />

																			<h:outputLabel styleClass="LABEL" rendered="false"
																				value="#{msg['receiveProperty.unitStatus']}:"></h:outputLabel>

																			<h:inputText id="unitStatus" readonly="true"
																				styleClass="READONLY"
																				binding="#{pages$ChangePropertyUnitRent.unitStatus}"
																				rendered="false" />

																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.unitType']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:selectOneMenu id="unitTypeMenu"
																				
																				binding="#{pages$ChangePropertyUnitRent.unitTypeMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ChangePropertyUnitRent.unitTypeList}" />

																			</h:selectOneMenu>
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.unitSide']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:selectOneMenu id="unitSideMenu"
																				
																				binding="#{pages$ChangePropertyUnitRent.unitSideMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ChangePropertyUnitRent.unitSideList}" />

																			</h:selectOneMenu>

																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.unitUsageType']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:selectOneMenu id="unitUsageTypeMenu"
																				
																				binding="#{pages$ChangePropertyUnitRent.unitUsageTypeMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ChangePropertyUnitRent.unitUsageTypeList}" />

																			</h:selectOneMenu>
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.unitInvestmentType']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:selectOneMenu id="unitInvestmentTypeMenu"
																				
																				binding="#{pages$ChangePropertyUnitRent.unitInvestmentTypeMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ChangePropertyUnitRent.unitInvestmentTypeList}" />

																			</h:selectOneMenu>

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unitDescription']}:"></h:outputLabel>

																			<h:inputText id="unitDescription"
																				binding="#{pages$ChangePropertyUnitRent.unitDescription}" />






		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.areaInSquare']}:"></h:outputLabel></t:panelGroup>
																			<h:inputText id="areaInSquare"
																				style="text-align: right;"
																				binding="#{pages$ChangePropertyUnitRent.areaInSquare}" />
																			<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />		<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.bedRooms']}:"></h:outputLabel></t:panelGroup>
																			<h:inputText id="bedRooms" style="text-align: right;"
																				binding="#{pages$ChangePropertyUnitRent.bedRooms}" />
																			<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />		<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.livingRooms']}:"></h:outputLabel></t:panelGroup>
																			<h:inputText id="livingRooms"
																				style="text-align: right;"
																				binding="#{pages$ChangePropertyUnitRent.livingRooms}" />
																		<t:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.bathRooms']}:"></h:outputLabel></t:panelGroup>
																			<h:inputText id="bathRooms"
																				style="text-align: right;"
																				binding="#{pages$ChangePropertyUnitRent.bathRooms}" />



																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unitEWUtilityNo']}:"></h:outputLabel>
																			<h:inputText id="unitEWUtilityNumber"
																				binding="#{pages$ChangePropertyUnitRent.unitEWUtilityNumber}" />
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.rentValueForUnits']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:inputText id="rentValueForUnits"
																				style="text-align: right;"
																				binding="#{pages$ChangePropertyUnitRent.rentValueForUnits}" >	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/></h:inputText>

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unitRemarks']}:"></h:outputLabel>
																			<h:inputText id="unitRemarks"
																				binding="#{pages$ChangePropertyUnitRent.unitRemarks}" />
                                                                            <t:panelGroup colspan="2" >
																		    <h:selectBooleanCheckbox
																				style="width: 15%;height: 22px; border: 0px;"
																				binding="#{pages$ChangePropertyUnitRent.includeFloorNumber}"
																				value="#{pages$ChangePropertyUnitRent.includeFloorNumberVar}">
																				<h:outputText
																					value="#{msg['receiveProperty.unit.addWithFloors']}:" />
																			</h:selectBooleanCheckbox>
																			</t:panelGroup>
																		</t:panelGrid>
<t:panelGrid id="unitbuttongrid" columnClasses="BUTTON_TD" style="width:97%;" columns="1">

																			<h:commandButton styleClass="BUTTON"
																				binding="#{pages$ChangePropertyUnitRent.addUnitButton}"
																				
																				action="#{pages$ChangePropertyUnitRent.generatePropertyUnits}" />
																		</t:panelGrid>
																																				<t:div style="text-align:center;">
																			<t:div styleClass="contentDiv" style="width:95%">
																				<t:dataTable id="units" rows="15" width="100%"
																					value="#{pages$ChangePropertyUnitRent.unitDataList}"
																					binding="#{pages$ChangePropertyUnitRent.unitTable}"
																					preserveDataModel="false" preserveSort="false"
																					var="unitDataItem" rowClasses="row1,row2"
																					rules="all" renderedIfEmpty="true">
																					<t:column id="col_mutliSelectUnit"
																						style="width:10%;">
																						<h:selectBooleanCheckbox id="multiUnit"
																							style="width:25%;"
																							value="#{unitDataItem.selected}" />
																					</t:column>

																					<t:column id="col_unitNo" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.unitNumberHeading']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{unitDataItem.unitNumber}" />
																					</t:column>
																					<t:column id="unit_floorNo" sortable="true">
																						<f:facet name="header">
																							<t:outputText id="unit_FloorNoHeading"
																								value="#{msg['receiveProperty.floorNumber']}" />
																						</f:facet>
																						
																						<t:outputText  id ="unit_FLNO"
																						  styleClass="A_LEFT" value="#{unitDataItem.floorNumber}" />
																					</t:column>

																					<t:column id="col_unitStatus" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.unitStatusHeading']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{unitDataItem.statusEn}"
																							rendered="#{pages$ChangePropertyUnitRent.isEnglishLocale}" />
																						<t:outputText styleClass="A_LEFT"
																							value="#{unitDataItem.statusAr}"
																							rendered="#{pages$ChangePropertyUnitRent.isArabicLocale}" />


																					</t:column>
																					<t:column id="col_unitRent"   sortable="true">
																						<f:facet name="header">
																							<t:outputText 
																								value="#{msg['receiveProperty.unitRentHeading']}" />
																						</f:facet>
																					<t:outputText 
																						styleClass="A_RIGHT" 
																						value="#{unitDataItem.rentValue}">
																						<f:convertNumber minFractionDigits="2"
																							maxFractionDigits="2" pattern="##,###,###.##" />
																					</t:outputText>
																				</t:column>
																					<t:column id="col_unitType" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.unitTypeHeading']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{unitDataItem.unitTypeEn}"
																							rendered="#{pages$ChangePropertyUnitRent.isEnglishLocale}" />
																						<t:outputText styleClass="A_LEFT"
																							rendered="#{pages$ChangePropertyUnitRent.isArabicLocale}"
																							value="#{unitDataItem.unitTypeAr}" />
																					</t:column>
																					<t:column
																						rendered="#{pages$ChangePropertyUnitRent.deleteAction}">
																						<f:facet name="header">
																							<t:outputText id="lbl_edit2"
																								value="#{msg['commons.action']}" />
																						</f:facet>
																						<h:commandLink
																							action="#{pages$ChangePropertyUnitRent.showDuplicateUnitsSearchPopup}">
																							<h:graphicImage id="edit_Icon2"
																								title="#{msg['commons.duplicate']}"
																								url="../resources/images/duplicate.png"
																								width="18px;" />

																						</h:commandLink>
																						<h:commandLink
																							action="#{pages$ChangePropertyUnitRent.editUnit}">
																																					<h:graphicImage style="margin-left:6px;"
																	id="editUnitImage" title="#{msg['commons.edit']}"
																	url="../resources/images/edit-icon.gif" />
																						</h:commandLink>
																						<h:commandLink
																							rendered="#{pages$ChangePropertyUnitRent.onRentValue}"
																							action="#{pages$ChangePropertyUnitRent.showRentValuePopup}">
																							<h:graphicImage id="edit_Icon3"
																								title="#{msg['commons.applyRentValue']}"
																								url="../resources/images/conduct.gif" />
																						</h:commandLink>

																						<h:commandLink id="deleteIconRCVUnit"
																							action="#{pages$ChangePropertyUnitRent.deleteUnit}">
																							<h:graphicImage id="delete_IconRCVUnit"
																								title="#{msg['commons.delete']}"
																								url="../resources/images/delete_icon.png" />
																						</h:commandLink>

																					</t:column>

																				</t:dataTable>
																			</t:div>
																			<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																				style="width:96%;">
																				<t:panelGrid cellpadding="0" cellspacing="0"
																					width="100%" columns="2"
																					columnClasses="RECORD_NUM_TD">

																					<t:div styleClass="RECORD_NUM_BG">
																						<t:panelGrid cellpadding="0" cellspacing="0"
																							columns="3" columnClasses="RECORD_NUM_TD">

																							<h:outputText
																								value="#{msg['commons.recordsFound']}" />

																							<h:outputText value=" : "
																								styleClass="RECORD_NUM_TD" />

																							<h:outputText
																								value="#{pages$ChangePropertyUnitRent.unitRecordSize}"
																								styleClass="RECORD_NUM_TD" />

																						</t:panelGrid>
																					</t:div>

																					<t:dataScroller id="unitScroller" for="units"
																						paginator="true" fastStep="1"
																						paginatorMaxPages="10" immediate="false"
																						styleClass="SCH_SCROLLER"
																						paginatorTableClass="paginator"
																						renderFacetsIfSinglePage="true"
																						paginatorTableStyle="grid_paginator"
																						layout="singleTable"
																						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																						paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																						pageIndexVar="unitPageNumber">

																						<f:facet name="first">
																							<t:graphicImage url="../#{path.scroller_first}"
																								id="lblFunit"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastrewind">
																							<t:graphicImage
																								url="../#{path.scroller_fastRewind}"
																								id="lblFRunit"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastforward">
																							<t:graphicImage
																								url="../#{path.scroller_fastForward}"
																								id="lblFFunit"></t:graphicImage>
																						</f:facet>
																						<f:facet name="last">
																							<t:graphicImage url="../#{path.scroller_last}"
																								id="lblLunit"></t:graphicImage>
																						</f:facet>
																						<t:div styleClass="PAGE_NUM_BG">
																							<h:outputText styleClass="PAGE_NUM"
																								value="#{msg['commons.page']}" />
																							<h:outputText styleClass="PAGE_NUM"
																								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																								value="#{requestScope.unitPageNumber}" />
																						</t:div>
																					</t:dataScroller>

																				</t:panelGrid>
																			</t:div>
																		</t:div>
																		<t:panelGrid id="deleteUnitsbuttongrid" 
																			columnClasses="BUTTON_TD" columns="1" style="width:97%;">

																			<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.delete']}"
																				action="#{pages$ChangePropertyUnitRent.deleteMultipleUnits}"
																				rendered="#{pages$ChangePropertyUnitRent.deleteAction}">
																			</h:commandButton>
																		</t:panelGrid>
														           </t:div>
																</rich:tab>
																<rich:tab id="unitTypeAmountEditTab"
																	rendered="#{pages$ChangePropertyUnitRent.showChangeRentTab}"
																	action="#{pages$ChangePropertyUnitRent.unitTypeAmountEditTabClick}"
																	label="#{msg['receiveProperty.tabName.ChangeRent']}">
																	<%@ include file="unitTypeAmountEditTab.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td>
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" />
																</td>
																<td width="100%">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" />
																</td>
																<td>
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" />
																</td>
															</tr>
														</table>
														<table cellpadding="0px" cellspacing="1px" class="A_RIGHT" width="100%">


															<tr>

																<td colspan="4" class="BUTTON_TD">
																	<h:commandButton styleClass="BUTTON"
																	    id="saveBtn"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ChangePropertyUnitRent.saveButton}"
																		value="#{msg['commons.saveButton']}"
																		action="#{pages$ChangePropertyUnitRent.saveProperty}" />
																	<h:commandButton styleClass="BUTTON"
																	id="submitButton"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ChangePropertyUnitRent.submitButton}"
																		value="#{msg['commons.sendButton']}"
																		action="#{pages$ChangePropertyUnitRent.sendForEvaluation}" />
																	<h:commandButton styleClass="BUTTON"
																	    id="submitAfterEvaluation"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ChangePropertyUnitRent.submitAfterEvaluation}"
																		value="#{msg['commons.submitButton']}"
																		action="#{pages$ChangePropertyUnitRent.sendAfterEvaluation}" />
																	<h:commandButton styleClass="BUTTON"
																	    id="approveEvaluation"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ChangePropertyUnitRent.approveEvaluation}"
																		value="#{msg['commons.approve']}"
																		action="#{pages$ChangePropertyUnitRent.approveEvaluation}" />
																	<h:commandButton styleClass="BUTTON"
																	id="reviewApproveEvaluation"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ChangePropertyUnitRent.reviewApproveEvaluation}"
																		value="#{msg['commons.review']}"
																		action="#{pages$ChangePropertyUnitRent.reviewApproveEvaluation}" />
																	<h:commandButton styleClass="BUTTON"
																	id="rejectEval"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ChangePropertyUnitRent.rejectEvaluation}"
																		value="#{msg['commons.reject']}"
																		action="#{pages$ChangePropertyUnitRent.rejectEvaluation}" />
																	<h:commandButton styleClass="BUTTON"
																	id="approveRentCommittee"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ChangePropertyUnitRent.approveRentCommittee}"
																		value="#{msg['commons.approve']}"
																		action="#{pages$ChangePropertyUnitRent.approveAndUpdate}" />
																		
																</td>
																	
															</tr>
														</table>
													</div>

												</div>


											</div>

										</h:form>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>
