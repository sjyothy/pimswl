<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<script language="javascript" type="text/javascript">
	function closeWindow(message)
	{
	  
	   //window.opener.document.getElementById("violationActionForm:reasonHidden").value = document.getElementById("reasonForm:reasonTxt").value;
	   window.opener.document.getElementById("BlackListMsg").innerHTML = '<FONT COLOR=RED><B><UL>\n<LI>'+message+'</UL></B></FONT>\n';
	   window.opener.document.getElementById("violationActionForm:blacklistBtn").disabled = true; 
	   
	   window.close();
	}
	
</script>
		
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
		
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
		<title>PIMS</title>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>		
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>

			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			
	</head>

	<body>
		




    <body>
  

	<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						
					</td>
				</tr>

				<tr width="100%">
					
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['violationAction.addReason']}" styleClass="HEADER_FONT"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" 
							cellspacing="0" border="0">
							<tr valign="top">
								
								<td width="100%" height="99%" valign="top" nowrap="nowrap">
								
								<div style="height:430px;width:100%">
									
															
								
    <h:form id="reasonForm">
    
    								<h:inputHidden id="unitId" value="#{pages$ViolationActions.unitId}"/>
									<h:inputHidden id="inspectionId" value="#{pages$ViolationActions.inspectionId}"/>
									<h:inputHidden id="violationId" value="#{pages$ViolationActions.violationId}"/>		
									<h:inputHidden id="violationDate" value="#{pages$ViolationActions.violationDate}"/>
        
        	<div class="MARGIN">
									<table cellpadding="0" cellspacing="0" >
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>

										<div class="DETAIL_SECTION">
											<h:outputLabel value="#{msg['violationAction.reason']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
       	<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
	        
								<tr>
								<td width="50%">
								<h:outputLabel value="#{msg['violationAction.reason']}" styleClass="LABLE"/> :
								</td>
									<td colspan="1" width="50%">
										
										<h:inputTextarea id="reasonTxt" value="#{pages$ViolationActions.reason}" style="width: 260px; height: 76px"></h:inputTextarea>
									</td>
									
									
									
									
									
								</tr>
								
								<tr>
									
									<td colspan="2" align="right">
									<br>
									  <h:commandButton styleClass="BUTTON" value="#{msg['commons.ok']}" action="#{pages$ViolationActions.blackList}" onclick="closeWindow('#{msg['violationAction.msg.tenantIsNowBlackListed']}')" ></h:commandButton> </td>
									
									
									
								</tr>
								<tr align="right">
								
									

								</tr>
							</table>																											
						</td>													
					</tr>
					
					<tr>
						<td width="100%" colspan="6">
							
						</td>					
					</tr>

				</table>
                </h:form>
                                   
         	</td>
         </tr>
         
         <tr>
         	<td>

         	</td>         	
         </tr>
        </table>
    
    
    
    
    
    </td>
    </tr>
    <tr>
    <td colspan="2">
    </td>
    </tr>
    </table>
    </body>
</html>
</f:view>
