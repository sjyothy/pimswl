<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">	
</script>
<%-- rendered="#{!(pages$memsFollowupTab.viewModePopup || pages$memsFollowupTab.pageModeView)&& ( pages$memsFollowupTab.activatedFromFollowup || pages$memsFollowupTab.activatedFromCorrespondence)}"--%>
<t:div style="height:240px;overflow-y:scroll;width:100%;">
	<t:panelGrid
		rendered="#{!pages$memsFollowupTab.viewModePopup || pages$memsFollowupTab.activatedFromFollowup || pages$memsFollowupTab.activatedFromCorrespondence}"
		id="pnlGrdFields" styleClass="TAB_DETAIL_SECTION_INNER"
		cellpadding="1px" width="97%" cellspacing="5px" columns="4">
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['memsFollowupTab.cat']}:" />
		</h:panelGroup>
		<h:selectOneMenu
			disabled="#{pages$memsFollowupTab.activatedFromFollowup}"
			id="categoryCmb" tabindex="1"
			value="#{pages$memsFollowupTab.memsFollowupView.category}">
			<f:selectItems value="#{pages$memsFollowupTab.followupCategory}" />
		</h:selectOneMenu>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['memsFollowupTab.type']}:" />
		</h:panelGroup>
		<h:selectOneMenu
			disabled="#{pages$memsFollowupTab.activatedFromFollowup}"
			id="followTypeCmb" tabindex="1"
			value="#{pages$memsFollowupTab.memsFollowupView.typeId}">
			<f:selectItems value="#{pages$ApplicationBean.memsFollowupTypeList}" />
		</h:selectOneMenu>


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['memsFollowupTab.date']}:" />
		</h:panelGroup>
		<rich:calendar
			disabled="#{pages$memsFollowupTab.activatedFromFollowup}"
			value="#{pages$memsFollowupTab.memsFollowupView.followupDate}"
			locale="#{pages$memsFollowupTab.locale}"
			datePattern="#{pages$memsFollowupTab.dateFormat}" popup="true"
			showApplyButton="false" enableManualInput="false" />

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['memsFollowupTab.desc']}:" />
		</h:panelGroup>
		<h:inputText id="followupdesc"
			disabled="#{pages$memsFollowupTab.activatedFromFollowup}"
			value="#{pages$memsFollowupTab.memsFollowupView.description}"></h:inputText>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['memsFollowupTab.details']}:" />
		</h:panelGroup>
		<h:inputTextarea
			disabled="#{pages$memsFollowupTab.activatedFromFollowup}"
			value="#{pages$memsFollowupTab.memsFollowupView.details}"
			style="width: 185px;" />



	</t:panelGrid>
	<t:panelGrid
		rendered="#{!pages$memsFollowupTab.viewModePopup || pages$memsFollowupTab.activatedFromCorrespondence}"
		id="pnlGrdActions" styleClass="BUTTON_TD" cellpadding="1px"
		width="95%" cellspacing="5px">
		<h:panelGroup>
			<h:commandButton styleClass="BUTTON"
				value="#{msg['commons.saveButton']}"
				action="#{pages$memsFollowupTab.onSave}" />
		</h:panelGroup>
	</t:panelGrid>


	<t:div styleClass="contentDiv" style="width:95%"
		rendered="#{!pages$memsFollowupTab.activatedFromFollowup}">
		<t:dataTable id="memsFollowupViewDataTable"
			rows="#{pages$memsFollowupTab.paginatorRows}"
			value="#{pages$memsFollowupTab.memsFollowupViewList}"
			binding="#{pages$memsFollowupTab.memsFollowUpDataTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
			width="100%">


			<pims:security
				screen="Pims.MinorMgmt.InheritanceFile.SendRecommForApproval"
				action="view">
				<t:column sortable="true">
					<h:selectBooleanCheckbox
						rendered="#{!(pages$memsFollowupTab.viewModePopup || pages$memsFollowupTab.pageModeView) && dataItem.showCheckBox}"
						value="#{dataItem.selected}"></h:selectBooleanCheckbox>
				</t:column>
			</pims:security>
			<t:column width="15%" sortable="true">
				<f:facet name="header">
					<t:outputText
						value="#{msg['cancelContract.inspection.grid.category']}"
						style="white-space: normal;" />
				</f:facet>

				<t:outputText style="white-space: normal;"
					value="#{pages$memsFollowupTab.isEnglishLocale?dataItem.categoryEn:dataItem.categoryAr}" />
			</t:column>
			<t:column width="15%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['maintenanceRequest.followUpBy']}"
						style="white-space: normal;" />
				</f:facet>
				<t:outputText style="white-space: normal;"
					value="#{pages$memsFollowupTab.isEnglishLocale?dataItem.createdByEn:dataItem.createdByAr}" />
			</t:column>
			<t:column width="15%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['memsFollowupTab.date']}"
						style="white-space: normal;" />
				</f:facet>
				<t:outputText value="#{dataItem.followupDate}"
					style="white-space: normal;">
					<f:convertDateTime pattern="#{pages$memsFollowupTab.dateFormat}"
						timeZone="#{pages$memsFollowupTab.timeZone}" />
				</t:outputText>
			</t:column>

			<t:column width="15%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['memsFollowupTab.type']}"
						style="white-space: normal;" />
				</f:facet>
				<t:outputText
					value="#{pages$memsFollowupTab.isEnglishLocale?dataItem.typeEn:dataItem.typeAr}"
					style="white-space: normal;" />
			</t:column>



			<t:column width="15%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['memsFollowupTab.desc']}"
						style="white-space: normal;" />
				</f:facet>
				<t:outputText value="#{dataItem.description}"
					style="white-space: normal;" />
			</t:column>

			<t:column width="15%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['memsFollowupTab.details']}"
						style="white-space: normal;" />
				</f:facet>
				<t:outputText value="#{dataItem.details}"
					style="white-space: normal;" />
			</t:column>

			<t:column width="10%" sortable="false"
				rendered="#{!pages$memsFollowupTab.pageModeView}">

				<f:facet name="header">
					<t:outputText value="#{msg['commons.action']}" />
				</f:facet>
				<pims:security
					screen="Pims.MinorMgmt.InheritanceFile.SendRecommForApproval"
					action="view">
					<t:commandLink
						rendered="#{!pages$memsFollowupTab.viewModePopup && pages$memsFollowupTab.socialResearchCatId == dataItem.category}"
						action="#{pages$memsFollowupTab.openRecommendationPopup}">
						<h:graphicImage
							title="#{msg['study.manage.tab.recommendations.btn.addRecommendations']}"
							style="cursor:hand;"
							url="../resources/images/app_icons/Status.png" />&nbsp;
					</t:commandLink>
					
					<t:commandLink
								rendered="#{!pages$memsFollowupTab.viewModePopup && pages$memsFollowupTab.socialResearchCatId == dataItem.category &&
											pages$inheritanceFile.showFileRequestOnFollowup}"
								action="#{pages$memsFollowupTab.onNavigateToDisbursement}">
								<h:graphicImage title="#{msg['mems.normaldisb.label.heading']}"
									style="cursor:hand;"
									url="../resources/images/app_icons/replaceCheque.png" />&nbsp;
					</t:commandLink>

					<t:commandLink rendered="#{!pages$memsFollowupTab.viewModePopup &&  pages$inheritanceFile.showFileRequestOnFollowup}"
					
						action="#{pages$memsFollowupTab.onNavigateToMaintenance}">
						<h:graphicImage 
							title="#{msg['BPM.WorkList.MEMSMinorMaintenanceRequest.RequestTitle']}"
							style="cursor:hand;"
							url="../resources/images/app_icons/Maintenance-Request-Managem.png" />
					</t:commandLink>

				</pims:security>
				<t:commandLink rendered="#{pages$memsFollowupTab.viewModePopup}"
					action="#{pages$memsFollowupTab.editFollowup}">
					<h:graphicImage title="#{msg['commons.edit']}" style="cursor:hand;"
						url="../resources/images/edit-icon.gif" />&nbsp;
			</t:commandLink>
				<t:commandLink
					onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToDeleteFollowUp']}')) return false;"
					rendered="#{pages$memsFollowupTab.socialResearchCatId != dataItem.category || dataItem.showCheckBox}"
					action="#{pages$memsFollowupTab.deleteFollowup}">
					<h:graphicImage title="#{msg['commons.delete']}"
						style="cursor:hand;" url="../resources/images/delete.gif" />&nbsp;
			</t:commandLink>
			</t:column>

		</t:dataTable>
	</t:div>
	<t:div rendered="#{!pages$memsFollowupTab.activatedFromFollowup}"
		id="memsFollowupViewDataTableFooter" styleClass="contentDivFooter"
		style="width:95%">
		<t:panelGrid id="followupfooterTable" columns="2" cellpadding="0"
			cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD">


			<CENTER>
				<t:dataScroller id="followupscroller"
					for="memsFollowupViewDataTable" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$memsFollowupTab.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="lblFollowupFirst"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="lblFollowupF"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="lblFollowupFF"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}" id="lblFollowupL"></t:graphicImage>
					</f:facet>


				</t:dataScroller>
			</CENTER>
		</t:panelGrid>
	</t:div>
</t:div>