
<t:div id="tabEducationAspect" style="width:100%;">

	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">

		<h:commandButton id="btnAddEducationAspect" styleClass="BUTTON"
			type="button" value="#{msg['commons.Add']}"
			onclick="javaScript:openFamilyVillageEducationAspectPopup('#{pages$tabFamilyVillageEducationAspect.personId}');">


		</h:commandButton>

	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableEducationAspect" rows="5" width="100%"
			value="#{pages$tabFamilyVillageEducationAspect.dataListEducationAspects}"
			binding="#{pages$tabFamilyVillageEducationAspect.dataTableEducationAspects}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
 
			<t:column id="EducationAspectAspectsCreatedBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="EducationAspectAspectsCreatedByOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdBy}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="EducationAspectAspectsCreatedOn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageEducationAspect.dateFormat}"
						timeZone="#{pages$tabFamilyVillageEducationAspect.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="EducationAspectAspectsinstitute" sortable="false" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText value="#{msg['educationAspects.instituteName']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" styleClass="A_LEFT"
					value="#{dataItem.instituteName}" />
			</t:column>
			<t:column id="EducationAspectAspectsgrade" sortable="false" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText value="#{msg['educationAspects.grade']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" styleClass="A_LEFT"
					value="#{pages$tabFamilyVillageEducationAspect.englishLocale?dataItem.gradeEn:dataItem.gradeAr}" />
			</t:column>

			<t:column id="EducationAspectAspectslevel" sortable="false" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText value="#{msg['educationAspects.level']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" styleClass="A_LEFT"
					value="#{pages$tabFamilyVillageEducationAspect.englishLocale?dataItem.educationLevelEn:dataItem.educationLevelAr}" />
			</t:column>

			<t:column id="EducationAspectAspectsattendancestatus" sortable="false" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText
						value="#{msg['researchFamilyVillageBeneficiary.lbl.EducationAspect.Attendancestatus']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" styleClass="A_LEFT"
					value="#{pages$tabFamilyVillageEducationAspect.englishLocale?dataItem.attendanceStatusEn:dataItem.attendanceStatusAr}" />
			</t:column>
			<t:column id="EducationAspectAspectsnoofyearsfailed" sortable="false" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText
						value="#{msg['researchFamilyVillageBeneficiary.lbl.EducationAspect.NoOfYearsFailed']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" styleClass="A_LEFT"
					value="#{dataItem.noOfYearsFailedColValue}" />
			</t:column>
			<t:column id="EducationAspectAspectsenrollmentyear" sortable="false" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText
						value="#{msg['researchFamilyVillageBeneficiary.lbl.EducationAspect.EnrollmentYear']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" styleClass="A_LEFT"
					value="#{dataItem.enrollementAgeColValue}" />
			</t:column>
			<t:column id="EducationAspectAspectsisdropout" sortable="false" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText
						value="#{msg['educationAspect.lbl.isDropout']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" styleClass="A_LEFT"
					value="#{dataItem.dropOutColValue}" />
			</t:column>
		</t:dataTable>
	</t:div>
	<t:div id="EducationAspectAspectsDivScroller"
		styleClass="contentDivFooter" style="width:99.1%">
		<t:panelGrid id="EducationAspectAspectsRecNumTable" columns="2"
			cellpadding="0" cellspacing="0" width="100%"
			columnClasses="RECORD_NUM_TD,BUTTON_TD">
			<t:div id="EducationAspectAspectsRecNumDiv"
				styleClass="RECORD_NUM_BG">
				<t:panelGrid id="EducationAspectAspectsRecNumTbl" columns="3"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
					cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
					<h:outputText value="#{msg['commons.recordsFound']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$tabFamilyVillageEducationAspect.recordSizeEducationAspects}" />
				</t:panelGrid>

			</t:div>

			<CENTER>
				<t:dataScroller id="EducationAspectAspectsScroller"
					for="dataTableEducationAspect" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$tabFamilyVillageEducationAspect.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="EducationAspectAspectslblFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="EducationAspectAspectslblFRRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="EducationAspectAspectslblFFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="EducationAspectAspectslblLRequestHistory"></t:graphicImage>
					</f:facet>

					<t:div id="EducationAspectAspectsPageNumDiv"
						styleClass="PAGE_NUM_BG">
						<t:panelGrid id="EducationAspectAspectsPageNumTable"
							styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
							cellspacing="0">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:panelGrid>

					</t:div>
				</t:dataScroller>
			</CENTER>

		</t:panelGrid>
	</t:div>
</t:div>

