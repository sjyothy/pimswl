<t:div id="tabFincance" style="width:100%;">
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="fnTab" rows="15" width="100%"
			value="#{pages$DisbursementDetailsTab.list}"
			binding="#{pages$DisbursementDetailsTab.dataTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="refNum" sortable="true">
				<f:facet name="header">
					<t:outputText id="lblRefNum" value="#{msg['commons.refNum']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="txtRefNum"
					styleClass="A_LEFT" escape="false"
					value="#{dataItem.reqNumber}/#{dataItem.refNum}" />
			</t:column>
			<t:column id="paymentType" sortable="true">
				<f:facet name="header">
					<t:outputText id="fnTabhdBenef"
						value="#{msg['mems.finance.label.paymentType']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="s_b"
					styleClass="A_LEFT"
					value="#{pages$DisbursementDetailsTab.englishLocale?dataItem.paymentTypeEn:dataItem.paymentTypeAr}" />
			</t:column>
			<t:column id="amnt" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAmnt" value="#{msg['commons.amount']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabamountt"
					styleClass="A_LEFT"
					value="#{0==dataItem.hasInstallment?dataItem.amount:dataItem.paymentDetails[0].amount}" />
			</t:column>
			<t:column id="fnstatus" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdFnstatus" value="#{msg['commons.status']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabstatus"
					styleClass="A_LEFT"
					value="#{pages$DisbursementDetailsTab.englishLocale?dataItem.statusEn:dataItem.statusAr}" />
			</t:column>
			<t:column id="fnSrcType" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdSrcType"
						value="#{msg['mems.finance.label.srcType']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabp_w"
					styleClass="A_LEFT"
					value="#{pages$DisbursementDetailsTab.englishLocale?dataItem.srcTypeEn:dataItem.srcTypeAr}" />
			</t:column>
			<t:column id="fnReason" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdFnReason"
						value="#{msg['mems.finance.label.reason']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabReason"
					styleClass="A_LEFT"
					value="#{pages$DisbursementDetailsTab.englishLocale?dataItem.reasonEn:dataItem.reasonAr}" />
			</t:column>
			<t:column id="fnDesc" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdFnDesc"
						value="#{msg['mems.finance.label.desc']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabDesc"
					styleClass="A_LEFT" value="#{dataItem.description}" />
			</t:column>
			<t:column id="fnItems" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdFnItems"
						value="#{msg['mems.finance.label.items']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabItems"
					styleClass="A_LEFT"
					value="#{pages$DisbursementDetailsTab.englishLocale?dataItem.itemEn:dataItem.itemAr}" />
			</t:column>
			<t:column id="fnPaidTo" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdFnPaidTo"
						value="#{msg['mems.finance.label.paidTo']}" />

				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabPaidTo"
					styleClass="A_LEFT"
					value="#{dataItem.paidTo==0?(dataItem.vendorId !=null? msg['mems.normaldisb.label.vendor']:msg['mems.finance.label.externalParty'])
					                               :
					                               msg['mems.finance.label.beneficiary']}" />
			</t:column>
			<t:column id="fnDisSrc" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdFnDisSrc"
						value="#{msg['mems.finance.disSource']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" id="fnTabDisRc"
					styleClass="A_LEFT"
					value="#{pages$DisbursementDetailsTab.englishLocale?dataItem.disburseSrcNameEn:dataItem.disburseSrcNameAr}" />
			</t:column>

		</t:dataTable>
	</t:div>
	<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
		style="width:99.1%;">
		<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
			cellspacing="1">
			<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
				<t:div styleClass="JUG_NUM_REC_ATT">
					<h:outputText value="#{msg['commons.records']}" />
					<h:outputText value=" : " />
					<h:outputText value="#{pages$DisbursementDetailsTab.totalRows}" />
				</t:div>
			</t:panelGroup>
			<t:panelGroup>
				<t:dataScroller id="fnTabshareScrollers" for="fnTab"
					paginator="true" fastStep="1" immediate="false"
					paginatorTableClass="paginator" renderFacetsIfSinglePage="true"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false" pageIndexVar="pageNumber"
					styleClass="JUG_SCROLLER">
					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
					</f:facet>
					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
					</f:facet>
					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
					</f:facet>
					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
					</f:facet>
					<t:div styleClass="PAGE_NUM_BG" rendered="true">
						<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}" />
						<h:outputText styleClass="PAGE_NUM"
							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
							value="#{requestScope.pageNumber}" />
					</t:div>
				</t:dataScroller>
			</t:panelGroup>
		</t:panelGrid>
	</t:div>

</t:div>
