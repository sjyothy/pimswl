<script language="JavaScript" type="text/javascript">
 function showUnitSearchPopUp()
 {
	var screen_width = 1024;
	var screen_height = 450;
//	window.open('UnitSearch.jsf?pageMode=MODE_SELECT_MANY_POPUP','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=10,top=150,scrollbars=yes,status=yes');
	//			var screen_width = 1024;
		//		var screen_height = 450;
	window.open('UnitSearch.jsf?pageMode=MODE_SELECT_MANY_POPUP','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	
 }
  function showPropertySearchPopUp()
 {
	var screen_width = 1024;
	var screen_height = 450;
	window.open('propertyList.jsf?pageMode=MODE_SELECT_MANY_POPUP','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
 }
 
 function showSetUpWorkersPopUp(rowId,workerNum,workerRemarks)
 {
	   var screen_width = 1024;
	   var screen_height = 450;
	   window.open('setUpWorkers.jsf?rowId='+rowId+'&workerNum='+workerNum+'&workerRemarks='+workerRemarks,'_blank','width='+(screen_width-500)+',height='+(screen_height-100)+',left=300,top=250,scrollbars=yes,status=yes');
	   
 }
</script>
<t:panelGrid id="btnGrid" width="100%"   border="0" columns="2" columnClasses="BUTTON_TD">
  <t:panelGroup colspan="10">
          <h:commandButton id="btnAddUnit" styleClass="BUTTON"
				value="#{msg['inspectionDetails.AddUnits']}" binding="#{pages$ServiceContractPropertyDetails.btnAddUnits}"
				onclick="javascript:showUnitSearchPopUp();"  style="width: 90px" />
		  <h:commandButton id="btnAddProperty" styleClass="BUTTON"
				value="#{msg['serviceContract.addProperties']}" binding="#{pages$ServiceContractPropertyDetails.btnAddProperty}"
				onclick="javascript:showPropertySearchPopUp();"  style="width: 110px" />
  </t:panelGroup>				

</t:panelGrid>
<t:div styleClass="contentDiv" style="width:98.3%;">
	<t:dataTable id="tbl_PropDetails" styleClass="grid" value="#{pages$ServiceContractPropertyDetails.tenderPropertyViewList}"	
	    binding="#{pages$ServiceContractPropertyDetails.tenderPropertyDataTable}" rowClasses="row1,row2"
	    rules="all" preserveDataModel="false" preserveSort="false" var="dataItem" 
	    rows="#{pages$ServiceContractPropertyDetails.paginatorRows}" 
		renderedIfEmpty="true" width="100%">
			       <t:column id="chk" style="width:5%;">
		            <t:selectBooleanCheckbox id="chk_TenderProp" value="#{dataItem.selected}" />
		          </t:column>
		        
				<t:column id="propertyNoCol" sortable="true"
					style="width:90;">
					<f:facet name="header">
						<t:outputText
							value="#{msg['inspection.propertyRefNum']}" />
					</f:facet>
					<t:outputText styleClass="A_LEFT"
						value="#{dataItem.propertyNumber}" />
				</t:column>
				<t:column id="propertyNameCol" sortable="true"
					style="width:120;">
					<f:facet name="header">
						<t:outputText value="#{msg['contract.property.Name']}" />
					</f:facet>
					<t:outputText styleClass="A_LEFT"
						value="#{dataItem.commercialName}" />
				</t:column>
				<t:column id="propertyTypeColEn" width="100" sortable="true" rendered="#{pages$ServiceContractPropertyDetails.isEnglishLocale}">
					<f:facet name="header">
						<t:outputText value="#{msg['property.type']}" />
					</f:facet>
					<t:outputText styleClass="A_LEFT"
						value="#{dataItem.propertyTypeEn}" />
				</t:column>
				<t:column id="propertyTypeColAr" width="100" sortable="true" rendered="#{pages$ServiceContractPropertyDetails.isArabicLocale}">
					<f:facet name="header">
						<t:outputText value="#{msg['property.type']}" />
					</f:facet>
					<t:outputText styleClass="A_LEFT"
						value="#{dataItem.propertyTypeAr}" />
				</t:column>
				
				<t:column id="unitRefNum" width="100" sortable="true">
					<f:facet name="header">
						<t:outputText value="#{msg['unit.numberCol']}" />
					</f:facet>
					<t:outputText styleClass="A_LEFT"
						value="#{dataItem.unitRefNo}" />
				</t:column>
				<t:column id="emirateEn" width="100" sortable="true" rendered="#{pages$ServiceContractPropertyDetails.isEnglishLocale}">
					<f:facet name="header">
						<t:outputText value="#{msg['commons.Emirate']}" />
					</f:facet>
					<t:outputText styleClass="A_LEFT"
						value="#{dataItem.emirateNameEn}" />
				</t:column>
				<t:column id="emirateAr" width="100" sortable="true" rendered="#{pages$ServiceContractPropertyDetails.isArabicLocale}">
					<f:facet name="header">
						<t:outputText value="#{msg['commons.Emirate']}" />
					</f:facet>
					<t:outputText styleClass="A_LEFT"
						value="#{dataItem.emirateNameAr}" />
				</t:column>
				
				<t:column id="actionCol" width="80">
					<f:facet name="header">
						<t:outputText value="#{msg['commons.action']}" />
					</f:facet>
					<h:commandLink >
						<h:graphicImage style="margin-left:6px;" title="#{msg['serviceContract.setWorkers']}" binding="#{pages$ServiceContractPropertyDetails.imgAddWorkers}"
							url="../resources/images/app_icons/Add-Payment-schedule.png" 
							onclick="javascript:showSetUpWorkersPopUp('#{dataItem.propertyNumber}_#{dataItem.unitRefNo}','#{dataItem.noOfWorkers}','#{dataItem.noofWorkersRemarks}')"
							/>
					</h:commandLink>
					<h:commandLink action="#{pages$ServiceContractPropertyDetails.imgDelProp_Click}" binding="#{pages$ServiceContractPropertyDetails.lnkDelPropUnit}">
						<h:graphicImage style="margin-left:6px;" title="#{msg['commons.delete']}"
							url="../resources/images/delete_icon.png" />
					</h:commandLink>
					
				</t:column>
	</t:dataTable>
</t:div>
<t:div styleClass="contentDivFooter"  style="width:99.3%">
   <t:panelGrid columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
		<t:div styleClass="RECORD_NUM_BG">
					<t:panelGrid columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
						<h:outputText value="#{msg['commons.recordsFound']}"/>
					    <h:outputText value=" : "/>
					    <h:outputText value="#{pages$ServiceContractPropertyDetails.recordSize}"/>
				    </t:panelGrid>	
		</t:div>
        <CENTER>
			   <t:dataScroller id="scrollerPropDetails" for="tbl_PropDetails" paginator="true"
				fastStep="1"  paginatorMaxPages="#{pages$ServiceContractPropertyDetails.paginatorMaxPages}" immediate="false"
				paginatorTableClass="paginator" renderFacetsIfSinglePage="true" pageIndexVar="pageNumber" 
				styleClass="SCH_SCROLLER" paginatorActiveColumnStyle="font-weight:bold;" paginatorRenderLinkForActive="false" 
				paginatorTableStyle="grid_paginator" layout="singleTable"
				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">
			
				    <f:facet  name="first">
						<t:graphicImage    url="../#{path.scroller_first}"  id="lblFPropDet"></t:graphicImage>
					</f:facet>
			
					<f:facet name="fastrewind">
						<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRPropDet"></t:graphicImage>
					</f:facet>
			
					<f:facet name="fastforward">
						<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFPropDet"></t:graphicImage>
					</f:facet>
			
					<f:facet name="last">
						<t:graphicImage  url="../#{path.scroller_last}"  id="lblLPropDet"></t:graphicImage>
					</f:facet>
	               <t:div styleClass="PAGE_NUM_BG">
				       <t:panelGrid  align="left"  columns="2" cellpadding="0" cellspacing="0" >
				 					<h:outputText  styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
									<h:outputText   styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
					   </t:panelGrid>
				   </t:div>
			 </t:dataScroller>
		</CENTER>
	</t:panelGrid>
</t:div>
<t:panelGrid id="btnGrid_SetWorkers" width="100%"   border="0" columns="2" columnClasses="BUTTON_TD">
       <t:panelGroup colspan="10">
                <h:commandButton id="btnSetWorkers" styleClass="BUTTON"
				value="#{msg['serviceContract.setWorkers']}" 
				binding="#{pages$ServiceContractPropertyDetails.btnSetWorkers}"
				onclick="javascript:showSetUpWorkersPopUp(' ',' ',' ')"  style="width: 110px" />
        </t:panelGroup>				
</t:panelGrid>