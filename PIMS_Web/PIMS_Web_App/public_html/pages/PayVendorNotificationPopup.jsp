<%-- 
  - Author:Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - 
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	function closeWindow(  )
	{
		window.close();
	}
	function disableAddControl( cntrl )
	{
		  
		  document.getElementById('frm:btnAdd').disabled = "true";
		  document.getElementById('frm:addLink').onclick();
	}
	            
	function onChkChange()
    {
      document.getElementById('frm:disablingDiv').style.display='none';
    }
    function onSelectionChanged(changedChkBox)
	{
	    document.getElementById('frm:disablingDiv').style.display='block';
		document.getElementById('frm:lnkUnitSelectionChanged').onclick();
	}
	</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">


			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['payVendorNotification.heading']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>
									<h:form id="frm" enctype="multipart/form-data">
										<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText id="errorMessages"
																value="#{pages$PayVendorNotificationPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$PayVendorNotificationPopup.successMessages}"
																escape="false" styleClass="INFO_FONT" />
															<h:messages></h:messages>

															<h:commandLink id="addLink"
																action="#{pages$PayVendorNotificationPopup.onAdd}" />


														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">

												<div style="width: 100%">
													<h:outputLabel value="" styleClass="DETAIL_SECTION_LABEL" />
													<table id="tableForm" cellpadding="1px" width="100%"
														cellspacing="2px" columns="4">

														<tr>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['commons.subject']} :"></h:outputLabel>
															</td>
															<td>

																<h:inputTextarea cols="100" rows="3"
																	binding="#{pages$PayVendorNotificationPopup.txtEmailSubject}" />


															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['commons.body']} :"></h:outputLabel>
															</td>
															<td>
																<t:inputHtml showAllToolBoxes="false"
																	showDebugToolBox="false" showLinksToolBox="false"
																	showPropertiesToolBox="false" showImagesToolBox="false"
																	showCleanupExpressionsToolBox="false"
																	showTablesToolBox="false" addKupuLogo="false"
																	binding="#{pages$PayVendorNotificationPopup.htmlTxt}">
																</t:inputHtml>

															</td>
														</tr>

													</table>

												</div>
												<table class="BUTTON_TD" cellpadding="1px" width="100%"
													cellspacing="3px">
													<tr>
														<td class="BUTTON_TD" colspan="10">
															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.send']}"
																binding="#{pages$PayVendorNotificationPopup.btnSend}"
																action="#{pages$PayVendorNotificationPopup.onSend}"
																style="width: 135px">
															</h:commandButton>
														</td>
													</tr>
												</table>


											</div>

										</div>
									</h:form>

								</td>
							</tr>
						</table>
						</div>
						</div>
						</div>

					</td>
				</tr>
			</table>
			</td>
			</tr>
			</table>
		</body>
	</html>
</f:view>