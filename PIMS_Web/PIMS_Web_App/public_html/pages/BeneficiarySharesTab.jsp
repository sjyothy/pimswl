<script language="JavaScript" type="text/javascript">
function loadAssetShare(control)
{
 
  var selIdx = control.selectedIndex;
  var selectedItem = control.options[selIdx].value
  if( selectedItem != -1 )
  {
	  var assetShareMap= document.getElementById("hdnAssetShareMap").value.split( "," );
	  for(i=0;i<assetShareMap.length;i++)
	  {
	      if( assetShareMap[i].indexOf( selectedItem+"_" ) != -1 )
	      {
	        
	        var share =  assetShareMap[i].split("_");
	       document.getElementById( "inheritanceFileForm:txtToalAssetShare" ).value =share[1]; 
	        
	      }
	  
	  }
  }
  else
  {
         document.getElementById(  "inheritanceFileForm:txtToalAssetShare"  ).value =0;
  }
}
function loadBenefShare(control)
{
 
  var selIdx = control.selectedIndex;
  var selectedItem = control.options[selIdx].value
  if( selectedItem != -1 )
  {
	  var assetShareMap= document.getElementById("hdnBenefShareMap").value.split( "," );
	  for(i=0;i<assetShareMap.length;i++)
	  {
	      if( assetShareMap[i].indexOf( selectedItem+"_" ) != -1 )
	      {
	        
	        var share =  assetShareMap[i].split("_");
	       document.getElementById( "inheritanceFileForm:txtPct" ).value =share[1]; 
	        
	      }
	  
	  }
  }
  else
  {
         document.getElementById(  "inheritanceFileForm:txtPct"  ).value =0;
  }
}

function populateBeneficiarySharesDetails()
{
		  document.getElementById("inheritanceFileForm:benefSharePopupLnk").onclick();
		  
}
		
</script>

<t:div style="min-height:210px;overflow-y:scroll;width:100%;">
	<t:inputHidden id="hdnBenefShareMap" forceId="true"
		value="#{pages$beneficiarySharesTabController.hdnBenefShareMap}" />
	<t:inputHidden id="hdnAssetShareMap" forceId="true"
		value="#{pages$beneficiarySharesTabController.hdnAssetShareMap}" />
	<%--
	<t:panelGrid border="0" width="97%" cellpadding="1" cellspacing="1">
		<t:panelGroup>
		
			
			<t:panelGrid columns="4"
				columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" id="sharePanel"
				rendered="#{!(pages$inheritanceFile.viewModePopUp ||pages$inheritanceFile.pageModeView )}">
				<h:outputLabel styleClass="LABEL"
					value="#{msg['benef.asset.shares.benef']}:">
				</h:outputLabel>

				<h:selectOneMenu id="selectBeneficiary"
					binding="#{pages$beneficiarySharesTabController.cmbBnfList}"
					onchange="javaScript:loadBenefShare(this);">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />

					<f:selectItems
						value="#{pages$beneficiarySharesTabController.beneficiaryList}" />
				</h:selectOneMenu>

				<h:outputLabel styleClass="LABEL"
					value="#{msg['benef.asset.shares.inhasset']}:">
				</h:outputLabel>

				<h:selectOneMenu id="selectInheritanceAsset"
					
					onchange="javaScript:loadAssetShare(this);"
					binding="#{pages$beneficiarySharesTabController.cmbInheritanceAsset}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems
						value="#{pages$beneficiarySharesTabController.assetClassList}" />
				</h:selectOneMenu>
				<h:outputLabel id="lblToalAssetShare" styleClass="LABEL"
					value="#{msg['inheritanceFile.label.totalAssetShare']}:" />
				<h:inputText id="txtToalAssetShare" 
					styleClass="READONLY"
					binding="#{pages$beneficiarySharesTabController.txtAssetShare}"></h:inputText>
				<h:outputLabel id="lblPct" styleClass="LABEL"
					value="#{msg['inheritanceFile.label.beneShareValue']}:" />
				<h:inputText id="txtPct" onkeyup="removeNonInteger(this)"
					onkeypress="removeNonInteger(this)"
					onkeydown="removeNonInteger(this)" onblur="removeNonInteger(this)"
					onchange="removeNonInteger(this)"
					binding="#{pages$beneficiarySharesTabController.pctWeight}"></h:inputText>


				<h:outputLabel id="lblWill" styleClass="LABEL"
					value="#{msg['mems.inheritanceFile.chkBoxWill']}:" />
				<h:selectBooleanCheckbox
					binding="#{pages$beneficiarySharesTabController.chkWill}"></h:selectBooleanCheckbox>

			</t:panelGrid>
		</t:panelGroup>
		
	</t:panelGrid>
--%>
	<t:div style="height:5px;"></t:div>
	<t:div style="width:96%;">
		<t:div styleClass="A_RIGHT">
			<h:commandLink id="benefSharePopupLnk"
				action="#{pages$beneficiarySharesTabController.onMessageFromBeneficiarySharePopUp}" />
			<pims:security screen="Pims.MinorMgmt.InheritanceFile.Save"
				action="view">
				<h:commandButton styleClass="BUTTON"
					rendered="#{
								pages$inheritanceFile.showSaveShareButton && 
								pages$beneficiarySharesTabController.showDistributeAssetAccordingToSharia
							   }"
					value="#{msg['distributeAsset.lbl.distributeSharia']}"
					style="width:190px;"
					action="#{pages$beneficiarySharesTabController.onDistributeAssetAccordingToShariaShares}">
				</h:commandButton>

				<h:commandButton styleClass="BUTTON"
					rendered="#{pages$inheritanceFile.showSaveShareButton}"
					value="#{msg['beneficiaryShares.Distribute']}"
					action="#{pages$beneficiarySharesTabController.onOpenBeneficiarySharesPopup}">
				</h:commandButton>
			</pims:security>
			<pims:security
				screen="Pims.MinorMgmt.InheritanceFile.SaveAfterFinalize"
				action="view">

				<h:commandButton styleClass="BUTTON"
					rendered="#{
								pages$inheritanceFile.showSaveShareButtonAfterFinalize && 
								pages$beneficiarySharesTabController.showDistributeAssetAccordingToSharia
							   }"
					value="#{msg['distributeAsset.lbl.distributeSharia']}"
					style="width:190px;"
					action="#{pages$beneficiarySharesTabController.onDistributeAssetAccordingToShariaShares}">
				</h:commandButton>
				<h:commandButton styleClass="BUTTON"
					rendered="#{pages$inheritanceFile.showSaveShareButtonAfterFinalize}"
					value="#{msg['beneficiaryShares.Distribute']}"
					action="#{pages$beneficiarySharesTabController.onOpenBeneficiarySharesPopup}">
				</h:commandButton>
			</pims:security>
		</t:div>
		<t:div style="height:5px;"></t:div>
		<t:div styleClass="contentDiv" style="width:96%">
			<t:dataTable id="inhShareInfo" rows="15" width="100%"
				value="#{pages$beneficiarySharesTabController.assetShareDataList}"
				binding="#{pages$beneficiarySharesTabController.inhShareSummaryTable}"
				preserveDataModel="false" preserveSort="false" var="unitDataItem"
				rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
				<t:column id="singleBenf" sortable="true">
					<f:facet name="header">
						<t:outputText id="headerSingleBenf"
							value="#{msg['mems.inheritanceFile.columnLabel.beneficiary']}" />
					</f:facet>

					<t:outputText id="s_b" styleClass="A_LEFT"
						value="#{unitDataItem.inheritedBeneficiaryFullName}" />
				</t:column>
				<t:column id="benfName" sortable="true">
					<f:facet name="header">
						<t:outputText id="headerBenfName"
							value="#{msg['assetSearch.assetName']}" />
					</f:facet>
					<t:outputText id="b_n" styleClass="A_LEFT"
						value="#{unitDataItem.inheritedAssetNameEn}" />
				</t:column>
				<t:column id="pctWeight" sortable="true">
					<f:facet name="header">
						<t:outputText id="headerPctWeight"
							value="#{msg['inheritanceFile.label.beneShareValue']}" />
					</f:facet>
					<t:outputText id="p_w" styleClass="A_LEFT"
						value="#{unitDataItem.pctWeight}" />
				</t:column>
				<t:column id="idAction" sortable="true"
					rendered="#{(pages$inheritanceFile.showSaveShareButtonAfterFinalize||pages$inheritanceFile.showSaveShareButton)}">
					<f:facet name="header">
						<t:outputText id="headerIdAction" value="#{msg['commons.action']}" />
					</f:facet>
					<pims:security screen="Pims.MinorMgmt.InheritanceFile.Save"
						action="view">
						<h:commandLink
							rendered="#{pages$inheritanceFile.showSaveShareButton}"
							action="#{pages$beneficiarySharesTabController.onBeneficiaryShareClicked}">
							<h:graphicImage id="edit_Icon" style="cursor:hand;"
								url="../resources/images/edit-icon.gif" width="18px;" />
						</h:commandLink>
						<h:commandLink
							rendered="#{pages$inheritanceFile.showSaveShareButton}"
							onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToDeleteAssetShare']}')) return false;"
							actionListener="#{pages$beneficiarySharesTabController.deleteInheritedShare}">
							<h:graphicImage id="delete_Icon" style="cursor:hand;"
								title="#{msg['commons.delete']}"
								url="../resources/images/delete.gif" width="18px;" />
						</h:commandLink>
					</pims:security>
					<pims:security
						screen="Pims.MinorMgmt.InheritanceFile.SaveAfterFinalize"
						action="view">
						<h:commandLink
							rendered="#{pages$inheritanceFile.showSaveShareButtonAfterFinalize}"
							action="#{pages$beneficiarySharesTabController.onBeneficiaryShareClicked}">
							<h:graphicImage id="edit_Icon2" style="cursor:hand;"
								url="../resources/images/edit-icon.gif" width="18px;" />
						</h:commandLink>
						<h:commandLink
							rendered="#{pages$inheritanceFile.showSaveShareButtonAfterFinalize}"
							onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToDeleteAssetShare']}')) return false;"
							actionListener="#{pages$beneficiarySharesTabController.deleteInheritedShare}">
							<h:graphicImage id="delete_Icon2" style="cursor:hand;"
								title="#{msg['commons.delete']}"
								url="../resources/images/delete.gif" width="18px;" />
						</h:commandLink>
					</pims:security>
				</t:column>
			</t:dataTable>
		</t:div>
		<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:97%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
				cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']}" />
						<h:outputText value=" : " />
						<h:outputText
							value="#{pages$beneficiarySharesTabController.totalRows}" />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="shareScrollers" for="inhShareInfo"
						paginatorMaxPages="10" paginator="true" fastStep="1"
						immediate="false" paginatorTableClass="paginator"
						renderFacetsIfSinglePage="true"
						paginatorTableStyle="grid_paginator" layout="singleTable"
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" pageIndexVar="pageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
						</f:facet>

					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
		</t:div>
	</t:div>
</t:div>