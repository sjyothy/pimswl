<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
		function disableButtons(control)
		{
			
			var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
			
			control.nextSibling.nextSibling.onclick();
			
			return 
		
		}
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{	
		if( hdnPersonType=='APPLICANT' ) {			    			 
			 document.getElementById("detailsFrm:hdnPersonId").value=personId;
			 document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
			 document.getElementById("detailsFrm:hdnPersonName").value=personName;
			 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
			 document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;
		} else {
			document.getElementById("detailsFrm:hdnVendorId").value=personId;		
			document.getElementById("detailsFrm:hdnVendorName").value=personName;		
		}		
	     document.forms[0].submit();
	} 
	function populatePaymentDetails()
	{			
	  	document.getElementById("detailsFrm:populatePayments").onclick();	  	 
	}
	function handlePopupChange()
	{
	  	document.getElementById("detailsFrm:beneficiaryId").onclick();	  	 
	}
	function handlePaymentSchedule() 
	{
		document.getElementById("detailsFrm:addPayment").onclick();
	}
	
	function showVendorPopup()
	{	
   		var screen_width = 1024;
   		var screen_height = 470;
   		var screen_top = screen.top;
     	window.open('SearchPerson.jsf?viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');         		  
	}						
	function onMessageFromAssociateCostCenter()
	{	
	  	document.getElementById("detailsFrm:onMessageFromAssociateCostCenter").onclick();	  	 
	}
</script>
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['mems.normaldisb.label.heading']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>

																<h:outputText id="errorId"
																	value="#{pages$NormalDisbursement.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$NormalDisbursement.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="hdnRequestId"
																	value="#{pages$NormalDisbursement.hdnRequestId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$NormalDisbursement.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$NormalDisbursement.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$NormalDisbursement.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$NormalDisbursement.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$NormalDisbursement.hdnIsCompany}"></h:inputHidden>
																<h:inputHidden id="hdnVendorId"
																	value="#{pages$NormalDisbursement.hdnVendorId}"></h:inputHidden>
																<h:inputHidden id="hdnVendorName"
																	value="#{pages$NormalDisbursement.hdnVendorName}">
																</h:inputHidden>
																<h:commandLink id="onMessageFromAssociateCostCenter"
																	style="width: auto;visibility: hidden;"
																	action="#{pages$NormalDisbursement.onMessageFromAssociateCostCenter}">
																</h:commandLink>
															</td>
														</tr>
													</table>

													<table cellpadding="0" cellspacing="6px" width="100%">
														<tr>
															<td class="BUTTON_TD MARGIN" colspan="6">

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['attachment.file']}"
																	action="#{pages$NormalDisbursement.showInheritanceFilePopup}"
																	rendered="#{pages$NormalDisbursement.isFileAssociated}">
																</h:commandButton>
															</td>
														</tr>


													</table>

													<t:panelGrid cellpadding="1px" width="100%"
														cellspacing="5px"
														rendered="#{pages$NormalDisbursement.isFileAssociated}"
														columns="4">
														<!-- Top Fields - Start -->
														<h:outputLabel styleClass="LABEL"
															value="#{msg['collectionProcedure.fileNumber']} :" />
														<h:inputText styleClass="READONLY" readonly="true"
															value="#{pages$NormalDisbursement.fileNumber}" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['report.memspaymentreceiptreport.fileOwner']}:" />
														<h:inputText styleClass="READONLY" readonly="true"
															binding="#{pages$NormalDisbursement.txtFileOwner}" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['searchInheritenceFile.fileType']}:" />
														<h:inputText styleClass="READONLY" readonly="true"
															binding="#{pages$NormalDisbursement.htmlFileType}" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['commons.status']}:" />
														<h:inputText styleClass="READONLY" readonly="true"
															binding="#{pages$NormalDisbursement.htmlFileStatus}" />

														
														<!-- Top Fields - End -->
														<h:outputText id="idRejectionReason" styleClass="LABEL"
															rendered="#{pages$NormalDisbursement.isDisbursementRequired || 
													                 		  pages$NormalDisbursement.isApproved || 
													                 		  pages$NormalDisbursement.isFinanceRejected ||
													                 			 pages$NormalDisbursement.isApprovalRequired ||
													                 			 pages$NormalDisbursement.isReviewRequired
													                 		 }"
															value="#{msg['mems.inheritanceFile.fieldLabel.rejectionReason']}" />
														<t:panelGroup colspan="3">
															<h:inputTextarea id="txtRejectionReason"
																style="width: 300px;"
																rendered="#{pages$NormalDisbursement.isDisbursementRequired || 
													                 			 pages$NormalDisbursement.isApproved || 
													                 			 pages$NormalDisbursement.isFinanceRejected ||
													                 			 pages$NormalDisbursement.isApprovalRequired ||
													                 			 pages$NormalDisbursement.isReviewRequired
													                 			}"
																binding="#{pages$NormalDisbursement.txtRejectionReason}">
															</h:inputTextarea>
														</t:panelGroup>
													</t:panelGrid>

													<%--<t:panelGrid id="applicationDetailsTable1" cellpadding="5px" width="85%" style="height:20px"  cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
																rendered="#{pages$NormalDisbursement.isFileAssociated}"
																columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">																																
																<h:outputLabel id="fileNum" styleClass="LABEL"  value="#{msg['mems.payment.request.label.fileNumber']}:"></h:outputLabel>																																																																												        				
																<h:inputText id="idFileNum" styleClass="READONLY APPLICANT_DETAILS_INPUT" readonly="true" maxlength="20" value="#{pages$NormalDisbursement.fileNumber}" align="right" style="width:200px"/>																																
																									
													</t:panelGrid>--%>
													<t:panelGrid id="reviewTableId" cellpadding="5px"
														width="100%" cellspacing="10px" columns="4"
														styleClass="TAB_DETAIL_SECTION_INNER"
														columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4"
														rendered="#{pages$NormalDisbursement.renderReview}">

														<h:outputText id="idSendTo" styleClass="LABEL"
															value="#{msg['mems.investment.label.sendTo']}" />


														<h:selectOneMenu id="idUserGrps"
															binding="#{pages$NormalDisbursement.cmbUserGroups}"
															style="width: 200px;">
															<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																itemValue="-1" />
															<f:selectItems
																value="#{pages$NormalDisbursement.allUserGroups}" />
														</h:selectOneMenu>

														<h:outputText id="idRevComments" styleClass="LABEL"
															value="#{msg['commons.comments']}" />
														<h:inputTextarea id="txtReviewCmts" style="width: 300px;"
															binding="#{pages$NormalDisbursement.txtReviewComments}">
														</h:inputTextarea>
													</t:panelGrid>

													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$NormalDisbursement.tabPanel}"
																style="width: 100%">

																<rich:tab id="applicationTab"
																	label="#{msg['commons.tab.applicationDetails']}">
																	<%@ include file="ApplicationDetails.jsp"%>
																</rich:tab>

																<rich:tab id="idDetails"
																	label="#{msg['mems.normaldisb.label.disbdetail']}">
																	<%@ include file="tabNormalDisbursement.jsp"%>
																</rich:tab>

																<rich:tab id="financeTab"
																	label="#{msg['mems.finance.label.tab']}"
																	rendered="#{ pages$NormalDisbursement.isDisbursementRequired || 
																	             pages$NormalDisbursement.isCompleted
																	           }">
																	<%@  include file="tabFinance.jsp"%>
																</rich:tab>

																<rich:tab id="reviewTab"
																	label="#{msg['commons.tab.reviewDetails']}"
																	rendered="#{!pages$NormalDisbursement.isNewStatus}">
																	<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
																</rich:tab>

																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>

																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>


																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$NormalDisbursement.getRequestHistory}"
																	rendered="#{!pages$NormalDisbursement.isInvalidStatus}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">


															<pims:security
																screen="Pims.MinorMgmt.DisbursementRequest.Create"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else disableButtons(this);"
																	rendered="#{pages$NormalDisbursement.isNewStatus}">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$NormalDisbursement.save}" />
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.submitButton']}"
																	rendered="#{pages$NormalDisbursement.isNewStatus}"
																	onclick="if (!confirm('#{msg['mems.common.alertSubmit']}')) return false;else disableButtons(this);">
																</h:commandButton>
																<h:commandLink id="lnkSubmit"
																	action="#{pages$NormalDisbursement.submitQuery}" />

															</pims:security>
															<pims:security
																screen="Pims.MinorMgmt.DisbursementRequest.ResearcherApprove"
																action="view">
																
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.save']}"
																	rendered="#{pages$NormalDisbursement.isRenderResearcherApprove }"
																	onclick="if (!confirm('#{msg['mems.common.alertOperation']}')) return false;else disableButtons(this);">
																</h:commandButton>
																<h:commandLink id="lnkSaveAfterSubmission"
																	action="#{pages$NormalDisbursement.onSaveAfterSubmission}" />
																
																
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.done']}"
																	rendered="#{pages$NormalDisbursement.isRenderResearcherApprove && 
																				!pages$NormalDisbursement.isRequestDateAfterLowerBound}"
																	onclick="if (!confirm('#{msg['mems.common.alertOperation']}')) return false;else disableButtons(this);">
																</h:commandButton>
																<h:commandLink id="lnkApprove"
																	action="#{pages$NormalDisbursement.approve}" />


																<h:commandButton styleClass="BUTTON"
																	value="#{msg['socialResearch.btn.sendToFinance']}"
																	onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this);"
																	rendered="#{pages$NormalDisbursement.isRenderResearcherApprove }">
																</h:commandButton>
																<h:commandLink id="lnkDoneResearcher"
																	action="#{pages$NormalDisbursement.onFinanceFromResearch}" />

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.reject']}"
																	rendered="#{pages$NormalDisbursement.isRenderResearcherApprove }"
																	onclick="if (!confirm('#{msg['mems.common.alertOperation']}')) return false;else disableButtons(this);">
																</h:commandButton>
																<h:commandLink id="lnkReject"
																	action="#{pages$NormalDisbursement.reject}" />

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['request.lbl.suspend']}"
																	rendered="#{pages$NormalDisbursement.isRenderResearcherApprove && !pages$NormalDisbursement.isSuspended}"
																	onclick="if (!confirm('#{msg['mems.common.alertOperation']}')) return false;else disableButtons(this);">
																</h:commandButton>
																<h:commandLink id="lnkSuspend"
																	action="#{pages$NormalDisbursement.onSuspended}" />


															</pims:security>
															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.reviewButton']}"
																rendered="#{pages$NormalDisbursement.renderReview}"
																onclick="if (!confirm('#{msg['mems.common.alertReview']}')) return false;else disableButtons(this);">
															</h:commandButton>
															<h:commandLink id="lnkReview"
																action="#{pages$NormalDisbursement.review}" />
															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.done']}"
																rendered="#{pages$NormalDisbursement.isReviewRequired && pages$NormalDisbursement.isTaskPresent}"
																onclick="if (!confirm('#{msg['mems.common.alertReviewDone']}')) return false;else disableButtons(this);">
															</h:commandButton>
															<h:commandLink id="lnkReviewDone"
																action="#{pages$NormalDisbursement.reviewDone}" />


															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.unblock']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else disableButtons(this);"
																rendered="#{	pages$NormalDisbursement.isRenderResearcherApprove }">
															</h:commandButton>
															<h:commandLink id="lnkUnblock"
																action="#{pages$NormalDisbursement.onUnblockSelected}" />
															<pims:security
																screen="Pims.MinorMgmt.DisbursementRequest.CaringApprove"
																action="view">


																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.done']}"
																	onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this);"
																	rendered="#{pages$NormalDisbursement.isRenderCaringApprove }">
																</h:commandButton>
																<h:commandLink id="lnkDoneWhenApproved"
																	action="#{pages$NormalDisbursement.doneWhenApproved}" />
																<h:commandButton id="btnRejectCaring"
																	rendered="#{pages$NormalDisbursement.isRenderCaringApprove }"
																	styleClass="BUTTON"
																	onclick="if (!confirm('#{msg['mems.common.alertReject']}')) return false;else disableButtons(this);"
																	value="#{msg['commons.reject']}">
																</h:commandButton>
																<h:commandLink id="lnkRejectCaring"
																	action="#{pages$NormalDisbursement.rejectCaring}" />
															</pims:security>
															<pims:security
																screen="Pims.MinorMgmt.DisbursementRequest.Disburse"
																action="view">

																<h:commandButton
																	rendered="#{pages$NormalDisbursement.isDisbursementRequired && pages$NormalDisbursement.isTaskPresent}"
																	styleClass="BUTTON"
																	onclick="if (!confirm('#{msg['mems.common.alertComplete']}')) return false;else disableButtons(this);"
																	value="#{msg['commons.complete']}">
																</h:commandButton>
																<h:commandLink id="lnkComplete"
																	action="#{pages$NormalDisbursement.complete}" />
																<h:commandButton id="btnRejectFinance"
																	rendered="#{pages$NormalDisbursement.isDisbursementRequired && pages$NormalDisbursement.isTaskPresent}"
																	styleClass="BUTTON"
																	onclick="if (!confirm('#{msg['mems.common.alertReject']}')) return false;else disableButtons(this);"
																	value="#{msg['commons.reject']}">
																</h:commandButton>
																<h:commandLink id="lnkRejecFinance"
																	action="#{pages$NormalDisbursement.rejectFinance}" />
															</pims:security>
															<h:commandButton styleClass="BUTTON" id="btnPrint"
																value="#{msg['commons.print']}"
																rendered="#{!pages$NormalDisbursement.isNewStatus}"
																onclick="disableButtons(this);">
															</h:commandButton>
															<h:commandLink id="lnkPrint"
																action="#{pages$NormalDisbursement.onPrint}" />

															<h:commandLink id="beneficiaryId" styleClass="BUTTON"
																style="width: auto;visibility: hidden;"
																action="#{pages$NormalDisbursement.handlePopupChange}">
															</h:commandLink>
															<a4j:commandLink id="addPayment" styleClass="BUTTON"
																style="width: auto;visibility: hidden;"
																reRender="successId"
																action="#{pages$NormalDisbursement.handlePaymentSchedule}">
															</a4j:commandLink>

															<h:commandLink id="populatePayments" styleClass="BUTTON"
																style="width: auto;visibility: hidden;"
																actionListener="#{pages$NormalDisbursement.loadPaymentDetails}">
															</h:commandLink>
														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

		</body>
	</html>
</f:view>