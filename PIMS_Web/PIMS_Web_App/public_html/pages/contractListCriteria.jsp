<script language="javascript" type="text/javascript">
	
	
	function showPopup(personType)
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   //window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
	    window.open('SearchPerson.jsf?persontype='+personType+'&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	
</script>

<tr>
	<td width="25%">
		<h:outputLabel styleClass="LABEL"
			value="#{msg['commons.Contract.Number']}" />
		:
	</td>
	<td width="25%">
		<h:inputText id="contNum"
			value="#{pages$contractListCriteria.contractNumber}"></h:inputText>
	</td>
	<td width="25%">
		<h:outputLabel styleClass="LABEL" value="#{msg['commons.username']}"></h:outputLabel>
		:
	</td>
	<td width="25%">
		<h:inputText id="useName"
			value="#{pages$contractListCriteria.userName}"></h:inputText>
	</td>
</tr>

<tr>
	<td width="25%">
		<h:outputLabel styleClass="LABEL"
			value="#{msg['contract.fromContractDate']}"> :</h:outputLabel>

	</td>
	<td width="25%">

		<rich:calendar id="fromContractD" datePattern="MM/dd/yyyy"
			value="#{pages$contractListCriteria.fromContractDateRich}"
			inputStyle="width:170px; height:14px"></rich:calendar>
	</td>

	<td width="25%">
		<h:outputLabel styleClass="LABEL"
			value="#{msg['contract.toContractDate']}" />
		:

	</td>
	<td width="25%">

		<rich:calendar id="toConractD" datePattern="MM/dd/yyyy"
			value="#{pages$contractListCriteria.toContractDateRich}"
			inputStyle="width:170px; height:14px"></rich:calendar>
	</td>

</tr>


<tr>
	<td width="25%">
		<h:outputLabel styleClass="LABEL"
			value="#{msg['contract.date.fromStart']}"> :</h:outputLabel>

	</td>
	<td width="25%">

		<rich:calendar id="fromContractStart" datePattern="MM/dd/yyyy"
			value="#{pages$contractListCriteria.fromContractStartDateRich}"
			inputStyle="width:170px; height:14px"></rich:calendar>
	</td>

	<td width="25%">
		<h:outputLabel styleClass="LABEL"
			value="#{msg['contract.date.toStart']}" />
		:

	</td>
	<td width="25%">

		<rich:calendar id="toConractStart" datePattern="MM/dd/yyyy"
			value="#{pages$contractListCriteria.toContractStartDateRich}"
			inputStyle="width:170px; height:14px"></rich:calendar>
	</td>

</tr>



<tr>
	<td>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['contract.date.fromExpiry']}" />
		:
	</td>
	<td>

		<rich:calendar id="fromExpD" datePattern="MM/dd/yyyy"
			value="#{pages$contractListCriteria.fromContractExpDateRich}"
			inputStyle="width:170px; height:14px"></rich:calendar>
	</td>

	<td>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['contract.date.toExpiry']}"></h:outputLabel>
		:
	</td>
	<td>

		<rich:calendar id="toExpD" datePattern="MM/dd/yyyy"
			value="#{pages$contractListCriteria.toContractExpDateRich}"
			inputStyle="width:170px; height:14px"></rich:calendar>
	</td>

</tr>


<tr>
	<td>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['ContractorSearch.contractStatus']}"></h:outputLabel>
		:
	</td>
	<td>
		<h:selectOneMenu id="contractStatus"
			value="#{pages$contractListCriteria.contractStatus}">
			<f:selectItem id="selectAll" itemValue="-1"
				itemLabel="#{msg['commons.All']}" />
			<f:selectItems value="#{pages$ApplicationBean.contractStatus}" />





		</h:selectOneMenu>
	</td>
	<td>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['contract.contractType']}"></h:outputLabel>
		:
	</td>
	<td>
		<h:selectOneMenu id="contractType"
			value="#{pages$contractListCriteria.contractType}">
			<f:selectItem id="selectAllType" itemValue="-1"
				itemLabel="#{msg['commons.All']}" />
			<f:selectItems value="#{pages$ApplicationBean.contractType}" />
		</h:selectOneMenu>
	</td>
</tr>



<tr>

	<td>
		<h:outputLabel styleClass="LABEL" value="#{msg['contract.unitRefNo']}"></h:outputLabel>
		:
	</td>
	<td>
		<h:inputText id="unitRefNo"
			value="#{pages$contractListCriteria.unitRefNum}"></h:inputText>
	</td>

	<td>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['contractList.unitCostCenter']}"></h:outputLabel>
		:
	</td>
	<td>
		<h:inputText id="unitCostCenter"
			value="#{pages$contractListCriteria.unitCostCenter}"></h:inputText>
	</td>

</tr>
<tr>

	<td>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['contractList.UnitDesc']}"></h:outputLabel>
		:
	</td>
	<td>
		<h:inputText id="unitDesc"
			value="#{pages$contractListCriteria.unitDesc}"></h:inputText>
	</td>

	<td>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['contractList.endowed/minorNumber']}"></h:outputLabel>
		:
	</td>
	<td>
		<h:inputText id="endownMinorNumber"
			value="#{pages$contractListCriteria.endowedNo}"></h:inputText>
	</td>

</tr>
<tr>
	<td>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['property.CommercialName']}"></h:outputLabel>
		:
	</td>
	<td>
		<h:inputText id="propertyName"
			value="#{pages$contractListCriteria.propertyComercialName}"></h:inputText>
	</td>
	<td>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['property.EndowedPropName']}"></h:outputLabel>
		:
	</td>
	<td>
		<h:inputText id="propertyEndowedName"
			value="#{pages$contractListCriteria.propertyEndowedName}"></h:inputText>
	</td>
</tr>
<tr>


</tr>
<tr>
	<td>
		<h:outputLabel styleClass="LABEL" value="#{msg['property.usage']}"></h:outputLabel>
		:
	</td>
	<td>
		<h:selectOneMenu id="propUsage"
			value="#{pages$contractListCriteria.propertyUsage}">
			<f:selectItem id="selectAllUsageType" itemValue="-1"
				itemLabel="#{msg['commons.All']}" />
			<f:selectItems value="#{pages$ApplicationBean.propertyUsageList}" />





		</h:selectOneMenu>
	</td>
	<td>
		<h:outputLabel styleClass="LABEL" value="#{msg['property.type']}"></h:outputLabel>
		:
	</td>
	<td>
		<h:selectOneMenu id="propType"
			value="#{pages$contractListCriteria.propertyType}">
			<f:selectItem id="selectAllPropType" itemValue="-1"
				itemLabel="#{msg['commons.All']}" />
			<f:selectItems value="#{pages$ApplicationBean.propertyTypeList}" />





		</h:selectOneMenu>
	</td>
</tr>


<tr>
	<td>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['chequeList.tenantName']}"></h:outputLabel>
		:
	</td>
	<td>
		<h:panelGroup>
		<h:inputText id="tenantName"
			value="#{pages$contractListCriteria.tenantName}"></h:inputText>
			<h:graphicImage
																			url="../resources/images/app_icons/Search-tenant.png"
																			onclick="showPopup('#{pages$contractListCriteria.personTenant}');"
																			style="MARGIN: 0px 0px -4px; CURSOR: hand;"
																			alt="#{msg[contract.searchTenant]}"></h:graphicImage>
		</h:panelGroup>
	</td>
	<td>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['tenants.tenantsType']}"></h:outputLabel>
		:
	</td>
	<td>
		<h:selectOneMenu id="tenatType"
			value="#{pages$contractListCriteria.tenantType}">
			<f:selectItem id="selectAllTenantType" itemValue="-1"
				itemLabel="#{msg['commons.All']}" />
			<f:selectItems value="#{pages$ApplicationBean.personKindList}" />
		</h:selectOneMenu>
	</td>
</tr>



<tr>
	<td>
		<h:outputLabel styleClass="LABEL" value="#{msg['contact.country']}" />
		:
	</td>
	<td>
		<h:selectOneMenu id="countries"
			valueChangeListener="#{pages$contractListCriteria.loadState}"
			onchange="submitForm()"
			value="#{pages$contractListCriteria.countries}">
			<f:selectItem id="contryAll" itemValue="-1"
				itemLabel="#{msg['commons.All']}" />
			<f:selectItems value="#{pages$ApplicationBean.countryList}" />





		</h:selectOneMenu>
	</td>

	<td>
		<h:outputLabel styleClass="LABEL" value="#{msg['contactInfo.state']}"></h:outputLabel>
		:
	</td>
	<td>
		<h:selectOneMenu id="states"
			valueChangeListener="#{pages$contractListCriteria.loadCities}"
			onchange="submitForm()" value="#{pages$contractListCriteria.states}">
			<f:selectItem id="statesAll" itemValue="-1"
				itemLabel="#{msg['commons.All']}" />
			<f:selectItems value="#{pages$contractListCriteria.stateList}" />


		</h:selectOneMenu>
	</td>

</tr>
<tr>
	<td>
		<h:outputLabel styleClass="LABEL" value="#{msg['contact.city']}" />
		:
	</td>
	<td>
		<h:selectOneMenu id="city" value="#{pages$contractListCriteria.city}">
			<f:selectItem id="cityAll" itemValue="-1"
				itemLabel="#{msg['commons.All']}" />
			<f:selectItems value="#{pages$contractListCriteria.cityList}" />





		</h:selectOneMenu>
	</td>

	<td>
		<h:outputLabel styleClass="LABEL" value="#{msg['contact.street']}"></h:outputLabel>
		:
	</td>
	<td>
		<h:inputText id="streetInput"
			value="#{pages$contractListCriteria.street}"></h:inputText>
	</td>

</tr>
<tr>
	<td>
		<h:outputLabel styleClass="LABEL" value="#{msg['contract.fromRent']}"></h:outputLabel>
		:
	</td>
	<td>

		<h:inputText id="fromRent" styleClass="A_RIGHT_NUM"
			value="#{pages$contractListCriteria.fromRentAmount}"></h:inputText>
	</td>

	<td>
		<h:outputLabel styleClass="LABEL" value="#{msg['contract.toRent']}"></h:outputLabel>
		:

	</td>
	<td>

		<h:inputText id="toRent" styleClass="A_RIGHT_NUM"
			value="#{pages$contractListCriteria.toRentAmount}"></h:inputText>
	</td>

</tr>
<tr>
	<td>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['contract.printedBeforeActive']}"></h:outputLabel>
		:
	</td>
	<td>
		<t:selectBooleanCheckbox id="printedBeforeActive"
			value="#{pages$contractListCriteria.printedBeforeActive}" />
	</td>
</tr>
<tr>
</tr>

