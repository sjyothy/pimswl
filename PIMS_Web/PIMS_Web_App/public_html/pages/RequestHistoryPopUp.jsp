<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)\
  - Description: Used for Searching Violations
  --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<script language="JavaScript" type="text/javascript">

	   

</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description"
				content="This is Violation search page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />


			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

		</head>

		<body>
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>


			<div class="containerDiv">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table class="greyPanelTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr>
									<td class="HEADER_TD" style="width: 70%;">
										<h:outputLabel value="#{msg['contract.requestHistory']}"
											styleClass="HEADER_FONT" />
									</td>

								</tr>
							</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<td width="100%" height="99%" valign="top" nowrap="nowrap">
										<h:form id="searchFrm" style="width:92%">
											<div class="SCROLLABLE_SECTION">

												<table width="100%">
													<tr>
														<td>
															&nbsp;

														</td>


													</tr>

												</table>
												<br>
												<br>
												<br>
												<div class="imag" style="margin: 0px, 4px, 0px, 0px;">
													&nbsp;
												</div>

												<t:div styleClass="contentDiv">
													<t:dataTable id="requestHistoryDataTable"
														value="#{pages$RequestHistoryPopUp.requestHistoryDataList}"
														binding="#{pages$RequestHistoryPopUp.requestHistoryDataTable}"
														rows="15" width="100%" preserveDataModel="false"
														preserveSort="false" var="requestHistoryDataItem"
														rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

														<t:column id="requestNumberCol" width="8%" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['request.numberCol']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{requestHistoryDataItem.requestNumber}" />
														</t:column>

														<t:column id="requestDateCol" width="6%" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.date']}" />
															</f:facet>
															<h:outputText styleClass="A_LEFT"
																value="#{requestHistoryDataItem.requestDate}">

															</h:outputText>
														</t:column>
														<t:column id="requestApp" width="6%" sortable="true">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['applicationDetails.applicantName']}" />
															</f:facet>
															<h:outputText styleClass="A_LEFT"
																value="#{requestHistoryDataItem.applicantView.personFullName}">

															</h:outputText>
														</t:column>

														<t:column id="requestStatusColEn" width="10%"
															sortable="true"
															rendered="#{pages$RequestHistoryPopUp.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText id="lblStatusEn"
																	value="#{msg['commons.status']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{requestHistoryDataItem.statusEn}" />
														</t:column>
														<t:column id="requestStatusColAr" width="10%"
															sortable="true"
															rendered="#{pages$RequestHistoryPopUp.isArabicLocale}">
															<f:facet name="header">
																<t:outputText id="lblStatusAr"
																	value="#{msg['commons.status']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{requestHistoryDataItem.statusAr}" />
														</t:column>
														<t:column id="requestTypeColEn" width="10%"
															sortable="true"
															rendered="#{pages$RequestHistoryPopUp.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText id="lblTypeEn"
																	value="#{msg['request.type']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{requestHistoryDataItem.requestTypeEn}" />
														</t:column>
														<t:column id="requestTypeColAr" width="10%"
															sortable="true"
															rendered="#{pages$RequestHistoryPopUp.isArabicLocale}">
															<f:facet name="header">
																<t:outputText styleClass="A_LEFT" id="lblTypeAr"
																	value="#{msg['request.type']}" />
															</f:facet>
															<t:outputText
																value="#{requestHistoryDataItem.requestTypeAr}" />
														</t:column>
														<t:column id="createdByCol" width="10%" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.createdBy']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{requestHistoryDataItem.createdBy}" />
														</t:column>

													</t:dataTable>
												</t:div>
												<t:div styleClass="contentDivFooter" style="width:98%">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText styleClass="PAGE_NUM"
																					style="font-size:10;"
																					value="#{msg['commons.recordsFound']}:" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText style="font-size:10;"
																					value="#{pages$RequestHistoryPopUp.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">
																<CENTER>

																	<t:dataScroller id="scroller_RequestHistory"
																		for="requestHistoryDataTable" paginator="true"
																		fastStep="1" paginatorMaxPages="15" immediate="false"
																		paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																		styleClass="SCH_SCROLLER" pageIndexVar="pageNumber"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false">

																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblFRequestHistory"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFRRequestHistory"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFFRequestHistory"></t:graphicImage>
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblLRequestHistory"></t:graphicImage>
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="font-size:10;"
																							value="#{msg['commons.page']}:" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:10;"
																							value="#{requestScope.pageNumber}" />
																					</td>
																				</tr>
																			</table>
																		</t:div>

																	</t:dataScroller>
																</CENTER>

															</td>
														</tr>
													</table>
												</t:div>

											</div>
										</h:form>

									</td>
								</tr>
							</table>

						</td>
					</tr>
					<tr>

					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>