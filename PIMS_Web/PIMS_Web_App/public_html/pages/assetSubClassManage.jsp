<%-- 
  - Author: Anil Verani
  - Date: 03/07/2009
  - Copyright Notice:
  - @(#)
  - Description: Used for Adding, Updating and Viewing Intial / Feasibility Studies 
  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	
	function callOpenerFn()
	{
	 window.opener.receiveSubClassList();
	}

	function closeWindow() 
	{
	 window.close();
	}
	
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['assetSubClassManage.heading']}" styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div style="height:450px" >

										<h:form id="frm" style="width:75%" enctype="multipart/form-data">
											<table border="0" class="layoutTable">

												<tr>

													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$assetSubClassManage.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$assetSubClassManage.successMessages}" />
													</td>
												</tr>
											</table>

  			                                 <div class="MARGIN">
														<table cellpadding="1px" cellspacing="2px"
																	class="TAB_DETAIL_SECTION_INNER" width="100%">
																<tr>																	
																   <td>	
																    <h:outputLabel styleClass="mandatory" value="*"/> 
																	<h:outputLabel styleClass="LABEL" value="#{msg['assetManage.assetClassNameAr']}:"></h:outputLabel>
																   </td>
																  <td>	
																	<h:inputText value="#{pages$assetSubClassManage.assetClassView.assetClassNameAr}" style="width: 186px;" maxlength="50" />
																  </td>
																  <td>	
																    <h:outputLabel styleClass="mandatory" value="*"/>
																	<h:outputLabel styleClass="LABEL" value="#{msg['assetManage.assetClassNameEn']}:"></h:outputLabel>
																  </td>
																  <td>	
																	<h:inputText value="#{pages$assetSubClassManage.assetClassView.assetClassNameEn}" style="width: 186px;" maxlength="50" />
																  </td>	
																</tr>	
																<tr>
																   <td>	
																     <h:outputLabel styleClass="mandatory" value="*"/>
																	 <h:outputLabel styleClass="LABEL" value="#{msg['assetManage.status']}:"></h:outputLabel>
																  </td>
																  <td> 
																	<h:selectOneMenu  value="#{pages$assetSubClassManage.assetClassView.statusIdForUI}" style="width: 192px;">
																		<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
																		<f:selectItems value="#{pages$ApplicationBean.assetClassStatus}" />
																	</h:selectOneMenu>
																  </td>
																	
																   <td>	
																	<h:outputLabel styleClass="LABEL" value="#{msg['assetManage.assetClassDescription']}:"></h:outputLabel>
																   </td>
																   <td>	
																	<h:inputTextarea value="#{pages$assetSubClassManage.assetClassView.description}" style="width: 186px;" />
																   </td>
																</tr>	
														</table>	
																	                                                                 

												
												<table cellpadding="0" cellspacing="0" width="98.5%">
													<tr>
														<td colspan="6" class="BUTTON_TD">
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$assetSubClassManage.onSave}"
																	rendered="#{! (pages$assetSubClassManage.isPopupViewOnlyMode || pages$assetSubClassManage.isViewMode)}">
															</h:commandButton>															
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	onclick="closeWindow();">
															</h:commandButton>																														
														</td>
													</tr>
												</table>
												</div>
											</div>								
								
									    </h:form>
									</div>
								

									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
			</table>
           </div>
		</body>
	</html>
</f:view>
