
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>




<t:div id="tabSurveyForms" style="width:100%;">

	<t:panelGrid id="tabSurveyFormGrid" cellpadding="3px" width="100%"
		cellspacing="2px" columns="1" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.tabSurveyForm']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">

		<h:commandButton id="btnAddSurveyForm" styleClass="BUTTON"
			type="button" value="#{msg['commons.Add']}"
			onclick="javaScript:openFamilyVillageSurveyFormPopup('#{pages$tabSurveyForm.personId}');">


		</h:commandButton>
		<h:commandButton id="btnAddSurveys" styleClass="BUTTON"
			 value="Add Surveys"
			action="#{pages$tabSurveyForm.onPersistSurvey}">


		</h:commandButton>

	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableSurveyForm" rows="5" width="100%"
			value="#{pages$tabSurveyForm.pageView.surveyResponses}"
			binding="#{pages$tabSurveyForm.dataTable}" preserveDataModel="false"
			preserveSort="false" var="dataItem" rowClasses="row1,row2"
			rules="all" renderedIfEmpty="true">

			<t:column id="SurveyFormLinenumber" sortable="true">
				<f:facet name="header">
					<t:outputText id="SurveyFormLinenumberOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText value="#{dataItem.lineNumber}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="SurveyFormquestions" sortable="true">
				<f:facet name="header">
					<t:outputText id="SurveyFormquestionOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabSurveyForm.englishLocale? 
																				dataItem.questionEn:
																				dataItem.questionAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="SurveyFormoptionOne" sortable="true">
				<f:facet name="header">
					<t:outputText id="SurveyFormoptionOneOT"
						value="#{pages$tabSurveyForm.englishLocale? 
																		pages$tabSurveyForm.pageView.optionOneEn:
																		pages$tabSurveyForm.pageView.optionOneAr}" />
				</f:facet>

				<t:selectBooleanCheckbox value="#{dataItem.optionOne}"
					forceId="true" id="optionOne" style="white-space: normal;"
					styleClass="A_LEFT"
					onclick="javascript:surveyOptionCheckedUnchecked(this);" />

			</t:column>
			<t:column id="SurveyFormoptionTwo" sortable="true">
				<f:facet name="header">
					<t:outputText id="SurveyFormoptionTwoOT"
						value="#{pages$tabSurveyForm.englishLocale? 
																		pages$tabSurveyForm.pageView.optionTwoEn:
																		pages$tabSurveyForm.pageView.optionTwoAr}" />
				</f:facet>

				<t:selectBooleanCheckbox value="#{dataItem.optionTwo}"
					forceId="true" id="optionTwo" style="white-space: normal;"
					styleClass="A_LEFT"
					onclick="javascript:surveyOptionCheckedUnchecked(this);" />

			</t:column>
			<t:column id="SurveyFormoptionThree" sortable="true">
				<f:facet name="header">
					<t:outputText id="SurveyFormoptionThreeOT"
						value="#{pages$tabSurveyForm.englishLocale? 
																		pages$tabSurveyForm.pageView.optionThreeEn:
																		pages$tabSurveyForm.pageView.optionThreeAr}" />
				</f:facet>

				<t:selectBooleanCheckbox value="#{dataItem.optionThree}"
					forceId="true" id="optionThree"
					onclick="javascript:surveyOptionCheckedUnchecked(this);" />

			</t:column>
			<t:column id="SurveyFormoptionFour" sortable="true">
				<f:facet name="header">
					<t:outputText id="SurveyFormoptionFourOT"
						value="#{pages$tabSurveyForm.englishLocale? 
																		pages$tabSurveyForm.pageView.optionFourEn:
																		pages$tabSurveyForm.pageView.optionFourAr}" />
				</f:facet>

				<t:selectBooleanCheckbox value="#{dataItem.optionFour}"
					forceId="true" id="optionFour"
					onclick="javascript:surveyOptionCheckedUnchecked(this);" />

			</t:column>


			<t:column id="SurveyFormDetails" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.description']}" />
				</f:facet>
				<h:inputText value="#{dataItem.comments}" />


			</t:column>


		</t:dataTable>
	</t:div>
</t:div>

