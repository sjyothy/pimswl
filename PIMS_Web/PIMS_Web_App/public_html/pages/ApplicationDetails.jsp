
<script type="text/javascript">
	function   showPersonPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	   var isSelectBlackList = document.getElementById(document.forms[0].name+":hdnIsSelectBlackList").value;
	   if(isSelectBlackList =='0')
	     window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	   else if(isSelectBlackList =='1')
	     window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup&SELECT_BLACK_LIST=true','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
	
	function populateApplicantInParentPerson( control )
	{	
	    var personId  = control.options[control.selectedIndex].value;
	    
		populatePerson('APPLICANT','',personId,'','','','');    			 
	}
						
</script>

<t:div style="width:100%" styleClass="TAB_DETAIL_SECTION">


	<h:inputHidden id="hdnIsSelectBlackList"
		value="#{pages$ApplicationDetails.selectBlackList}"></h:inputHidden>
	<t:panelGrid id="applicationDetailsTable" cellpadding="1px"
		width="100%" cellspacing="5px" columns="4"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:outputLabel styleClass="LABEL"
			value="#{msg['applicationDetails.number']}:" />
		<h:inputText styleClass="READONLY APPLICANT_DETAILS_INPUT"
			maxlength="20" id="txtApplicationNum" readonly="true"
			value="#{pages$ApplicationDetails.applicationNo}"
			binding="#{pages$ApplicationDetails.txtApplicationNo}"></h:inputText>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['applicationDetails.status']}:"></h:outputLabel>
		<h:inputText styleClass="READONLY APPLICANT_DETAILS_INPUT"
			id="txtApplicationStatus" readonly="true" maxlength="20"
			binding="#{pages$ApplicationDetails.txtApplicationStatus}"
			value="#{pages$ApplicationDetails.status}" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['applicationDetails.date']}:"></h:outputLabel>
		<h:inputText styleClass="READONLY APPLICANT_DETAILS_INPUT"
			id="txtApplicationDate" readonly="true"
			value="#{pages$ApplicationDetails.appDate}"
			binding="#{pages$ApplicationDetails.txtApplicationDate}">
			<f:convertDateTime pattern="#{pages$ApplicationDetails.dateFormat}"
				timeZone="#{pages$ApplicationDetails.timeZone}" />
		</h:inputText>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['applicationDetails.dsescription']}:"></h:outputLabel>
		<t:inputTextarea style="width: 176px;"
			styleClass="#{pages$ApplicationDetails.applicationDescriptionReadonlyMode}"
			id="txtDesc" value="#{pages$ApplicationDetails.description}"
			binding="#{pages$ApplicationDetails.txtDescription}"
			onblur="javaScript:validate();" onkeyup="javaScript:validate();"
			onmouseup="javaScript:validate();"
			onmousedown="javaScript:validate();"
			onchange="javaScript:validate();" onkeypress="javaScript:validate();" />
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['applicationDetails.applicantName']}:"></h:outputLabel>
		</h:panelGroup>
		<t:panelGrid id="gridApplicant" columns="3" width="100%"
			cellpadding="0" cellspacing="1"
			columnClasses="APP_DETAIL_GRID_APPLICANT_TXTIMG_C1,APP_DETAIL_GRID_APPLICANT_TXTIMG_C2">

			<h:inputText styleClass="READONLY APPLICANT_DETAILS_INPUT"
				id="txtName" readonly="true" style="width:170px;"
				value="#{pages$ApplicationDetails.name}"
				binding="#{pages$ApplicationDetails.txtName}"></h:inputText>

			<h:selectOneMenu id="cmbAllowedApplicants"
				binding="#{pages$ApplicationDetails.cmbAllowedApplicants}"
				onchange="populateApplicantInParentPerson(this);">
				<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
					itemValue="-1" />
				<f:selectItems
					value="#{pages$ApplicationDetails.memsAllowedApplicantsList}" />

			</h:selectOneMenu>

			<t:panelGroup colspan="2"
				rendered="#{!pages$ApplicationDetails.showAllowedApplicantsList}">
				<h:graphicImage id="imgPerson" title="#{msg['commons.search']}"
					onclick="showPersonPopup();"
					rendered="#{!pages$ApplicationDetails.applicationDetailsReadonlyMode}"
					style="MARGIN: 0px 0px -4px; cursor:hand;horizontal-align:right;"
					url="../resources/images/app_icons/Search-tenant.png"></h:graphicImage>
				<h:graphicImage id="imgViewPerson" title="#{msg['commons.details']}"
					rendered="#{pages$ApplicationDetails.isViewPersonImgShow}"
					onclick="javascript:showPersonReadOnlyPopup('#{pages$ApplicationDetails.applicationId}')"
					style="MARGIN: 0px 0px -4px; CURSOR: hand;"
					url="../resources/images/app_icons/Tenant.png"></h:graphicImage>
			</t:panelGroup>

		</t:panelGrid>

		<h:outputLabel id="lblAppType" styleClass="LABEL"
			value="#{msg['applicationDetails.applicantType']}:"></h:outputLabel>
		<h:inputText styleClass="READONLY APPLICANT_DETAILS_INPUT"
			id="txtAppType" maxlength="15" readonly="true"
			binding="#{pages$ApplicationDetails.txtApplicantType}"
			value="#{pages$ApplicationDetails.applicantType}"></h:inputText>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['applicationDetails.cell']}:"></h:outputLabel>
		<h:inputText id="txtCell"
			styleClass="READONLY APPLICANT_DETAILS_INPUT" maxlength="15"
			readonly="true" binding="#{pages$ApplicationDetails.txtCellNo}"
			value="#{pages$ApplicationDetails.cellNo}"></h:inputText>
		<h:outputLabel rendered="false" styleClass="LABEL"
			value="#{msg['applicationDetails.email']}"></h:outputLabel>
		<h:inputText styleClass="READONLY APPLICANT_DETAILS_INPUT"
			rendered="false" id="txtemail" readonly="true"
			binding="#{pages$ApplicationDetails.txtEmail}"
			value="#{pages$ApplicationDetails.email}"></h:inputText>
	</t:panelGrid>
</t:div>
