<%-- 
  - Author: M. Shiraz Siddiqui
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Search Revenues and Expenses  
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
 function resetValues()
      	{
      	    
      	    document.getElementById("searchFrm:selectTranstionNo").selectedIndex=0;
      	    document.getElementById("searchFrm:cmbStatus").selectedIndex=0;
      	    document.getElementById("searchFrm:cmbType").selectedIndex=0;
			
			document.getElementById("searchFrm:txtPortfolio").value="";
			document.getElementById("searchFrm:txtMarker").value="";
			document.getElementById("searchFrm:txtLog").value="";
			
		    $('searchFrm:dateFrom').component.resetSelectedDate();
		    $('searchFrm:dateTo').component.resetSelectedDate();
				
        }
	function openExpenseAndRevenuePopup()
	{
	 var screen_width = 1024;
     var screen_height = 400;
     window.open('revenueAndExpensesManage.jsf','_blank','width='+(screen_width)+',height='+(screen_height)+',left=120,top=150,scrollbars=no,status=yes');
	}
	
	function openPortfolioSearchPopup()
    {
      var screen_width = 990;
      var screen_height = 475;
      var popup_width = screen_width;
      var popup_height = screen_height;
      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
      window.open('portfolioSearch.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
    }
	function receiveSelectedPortfolio() 
    { 
     document.forms[0].submit();
    }
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

</head>
	<body class="BODY_STYLE">
	<div class="containerDiv">
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>
				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" height="470px" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['revenuesAndExpensesSearch.heading']}" styleClass="HEADER_FONT"/>
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
								<div class="SCROLLABLE_SECTION AUC_SCH_SS"
										style="height: 70%; width: 100%; # height: 85%; # width: 100%;">
								
									<h:form id="searchFrm" style="WIDTH: 96%;">
											<div class="MESSAGE">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$revenuesAndExpensesSearch.infoMessages}"
																escape="false" styleClass="INFO_FONT" />
															<h:outputText
																value="#{pages$revenuesAndExpensesSearch.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
													<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
													</tr>
												</table>
												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">

														<tr>
															<td class="DET_SEC_TD">
																<h:outputLabel styleClass="LABEL"
																               value="#{msg['revenueAndExpensesManage.TransactionRefNo']}:"></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<h:selectOneMenu       id="selectTranstionNo"
																	                   binding="#{pages$revenuesAndExpensesSearch.htmlSelectNumber}" 
																	                   style="width: 192px;">
																		<f:selectItem  itemValue="" itemLabel="#{msg['commons.All']}" />
																		<f:selectItems value="#{pages$ApplicationBean.orderList}" />
																</h:selectOneMenu>	
															</td>
															<td class="DET_SEC_TD">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['revenuesAndExpensesSearch.status']}"></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<h:selectOneMenu id="cmbStatus" 
																	binding="#{pages$revenuesAndExpensesSearch.htmlSelectOneStatus}"
																	styleClass="SELECT_MENU">
																	<f:selectItem itemLabel="#{msg['commons.All']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.revenueAndExpenseStatus}" />
																</h:selectOneMenu>
															</td>
														</tr>

														<tr>
															<td class="DET_SEC_TD">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['revenuesAndExpensesSearch.type']}"></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<h:selectOneMenu id="cmbType" 
																	binding="#{pages$revenuesAndExpensesSearch.htmlSelectOneType}"
																	styleClass="SELECT_MENU">
																	<f:selectItem itemLabel="#{msg['commons.All']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.revenueAndExpenseType}" />
																</h:selectOneMenu>
															</td>
															<td class="DET_SEC_TD">
																<h:outputLabel styleClass="LABEL"
																value="#{msg['revenuesAndExpensesSearch.portfolio']}"></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
															<h:inputText               
												                           id="txtPortfolio"
														                   binding="#{pages$revenuesAndExpensesSearch.htmlPortfolio}" 
														                   style="width: 186px;" maxlength="20"  />
															</td>
														</tr>
														<tr >
															<td class="DET_SEC_TD" >
																<h:outputLabel styleClass="LABEL" 
																 value="#{msg['revenuesAndExpensesSearch.date/from']}"></h:outputLabel>
															</td>
															<td class="DET_SEC_TD" >
																<rich:calendar  id="dateFrom" binding="#{pages$revenuesAndExpensesSearch.htmlDateFrom}" 
										                          popup="true" datePattern="#{pages$revenuesAndExpensesSearch.dateFormat}" showApplyButton="false"
										                          locale="#{pages$revenuesAndExpensesSearch.locale}" 
										                          enableManualInput="false" cellWidth="24px" cellHeight="22px"
										                          />
															</td>
															<td class="DET_SEC_TD">
																<h:outputLabel styleClass="LABEL" 
																value="#{msg['revenuesAndExpensesSearch.date/to']}"></h:outputLabel>
															</td>
															<td class="DET_SEC_TD" >
																<rich:calendar  id="dateTo" binding="#{pages$revenuesAndExpensesSearch.htmlDateTo}" 
										                          popup="true" datePattern="#{pages$revenuesAndExpensesSearch.dateFormat}" showApplyButton="false"
										                          locale="#{pages$revenuesAndExpensesSearch.locale}" 
										                          enableManualInput="false" cellWidth="24px" cellHeight="22px"
										                          />
															</td>
														</tr>
														<tr >
															<td class="DET_SEC_TD" >
																<h:outputLabel styleClass="LABEL" 
																	value="#{msg['revenuesAndExpensesSearch.marker']}"></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<h:inputText id="txtMarker" 
																	styleClass="INPUT_TEXT"
																	binding="#{pages$revenuesAndExpensesSearch.htmlMarker}"></h:inputText>
															</td>
															<td class="DET_SEC_TD">
																<h:outputLabel styleClass="LABEL" 
																value="#{msg['revenuesAndExpensesSearch.log']}"></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<h:inputText id="txtLog" 
																	binding="#{pages$revenuesAndExpensesSearch.htmlLog}"
																	styleClass="INPUT_TEXT">
																</h:inputText>
															</td>
														</tr>
														<tr>
															<td colspan="4" CLASS="BUTTON_TD">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.search']}"
																	action="#{pages$revenuesAndExpensesSearch.btnSearch_Click}"
																	style="width: 75px" />

																<h:commandButton type="BUTTON" styleClass="BUTTON"
																	value="#{msg['commons.clear']}"
																	onclick="javascript:resetValues();" style="width: 75px" />
																	
																	<h:commandButton styleClass="BUTTON"
																	value="#{msg['revenuesAndExpensesSearch.btn.addRevenue']}"
																	action="#{pages$revenuesAndExpensesSearch.btnAddRevenue_Click}"
																	 style="width: auto"/>
																	
																	<h:commandButton styleClass="BUTTON"
																	value="#{msg['revenuesAndExpensesSearch.btn.addExpense']}"
																	action="#{pages$revenuesAndExpensesSearch.btnAddExpenses_Click}"
																	style="width: auto" />
															</td>
														</tr>
													</table>
												</div>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" style="width: 100%; # width: 99%;">
													<t:dataTable id="dt1" 
												value="#{pages$revenuesAndExpensesSearch.dataList}"
												binding="#{pages$revenuesAndExpensesSearch.tbl_RevenueAndExpenseSearch}" 
												rows="#{pages$revenuesAndExpensesSearch.paginatorRows}"
												preserveDataModel="false" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												width="100%">

												<t:column id="numberCol" defaultSorted="true" sortable="true" >
													<f:facet  name="header">
														<t:outputText value="#{msg['revenuesAndExpensesSearch.number']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.transectionRefNumber}" />
												</t:column>
												<t:column id="dateCol" sortable="true" >
													<f:facet  name="header">
														<t:outputText  value="#{msg['revenuesAndExpensesSearch.date']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.transectionDate}" >
														<f:convertDateTime pattern="#{pages$revenuesAndExpensesSearch.dateFormat}" timeZone="#{pages$revenuesAndExpensesSearch.timeZone}" />
													</t:outputText>	
												</t:column>
												<t:column id="portfolioCol" defaultSorted="true" sortable="true" >
													<f:facet  name="header">
														<t:outputText value="#{msg['revenuesAndExpensesSearch.portfolio']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.portfolioView.portfolioNumber}" />
												</t:column>
												<t:column id="markerCol" defaultSorted="true" sortable="true" >
													<f:facet  name="header">
														<t:outputText value="#{msg['revenuesAndExpensesSearch.marker']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.marker}" />
												</t:column>
												<t:column id="typeEnCol" sortable="true"  rendered="#{pages$revenuesAndExpensesSearch.isEnglishLocale}">
													<f:facet  name="header">
														<t:outputText value="#{msg['revenuesAndExpensesSearch.type']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.expRevTypeEn}" />
												</t:column>
												<t:column id="typeArCol" sortable="true" rendered="#{pages$revenuesAndExpensesSearch.isArabicLocale}">
													<f:facet  name="header">
														<t:outputText value="#{msg['revenuesAndExpensesSearch.type']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.expRevTypeAr}" />
												</t:column>
												<t:column id="StatusEnCol" sortable="true" rendered="#{pages$revenuesAndExpensesSearch.isEnglishLocale}">
													<f:facet  name="header">
														<t:outputText value="#{msg['revenuesAndExpensesSearch.status']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.statusEn}" />
												</t:column>
												<t:column id="StatusArCol" sortable="true" rendered="#{pages$revenuesAndExpensesSearch.isArabicLocale}">
													<f:facet  name="header">
														<t:outputText value="#{msg['revenuesAndExpensesSearch.status']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.statusAr}" />
												</t:column>
												
												<t:column id="actions">
													<f:facet  name="header">
														<t:outputText value="#{msg['commons.action']}" />
													</f:facet>
													<t:commandLink   action="#{pages$revenuesAndExpensesSearch.cmdView_Click}" >
													<h:graphicImage title="#{msg['commons.view']}" 
															                alt="#{msg['commons.view']}"
															                 url="../resources/images/app_icons/View_Icon.png"
															                />
												    </t:commandLink>
													<t:outputText escape="false" value="&nbsp;"></t:outputText>
													<t:commandLink   action="#{pages$revenuesAndExpensesSearch.cmdEdit_Click}" >
													   <h:graphicImage title="#{msg['commons.edit']}" 
															                alt="#{msg['commons.edit']}"
															                 url="../resources/images/edit-icon.gif"
															                />
												    </t:commandLink>
												    <t:outputText escape="false" value="&nbsp;"></t:outputText>
													<t:commandLink   action="#{pages$revenuesAndExpensesSearch.cmdDelete_Click}" >
													   <h:graphicImage title="#{msg['commons.delete']}" 
															                alt="#{msg['commons.delete']}"
															                 url="../resources/images/delete.gif"
															                />
												    </t:commandLink>
												</t:column>
											</t:dataTable>
												</div>
												<t:div styleClass="contentDivFooter"
													style="width:101%;#width:100%;">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$revenuesAndExpensesSearch.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">
																<CENTER>
																	<t:dataScroller id="scroller" for="dt1"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$revenuesAndExpensesSearch.paginatorMaxPages}"
																		immediate="false" paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false"
																		pageIndexVar="pageNumber" styleClass="SCH_SCROLLER">
																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL"></t:graphicImage>
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
																			<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.pageNumber}" />
																					</td>
																				</tr>
																			</table>

																		</t:div>
																	</t:dataScroller>
																</CENTER>

															</td>
														</tr>
													</table>
												</t:div>
											</div>
									</div>
									</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
					<td colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
</html>
</f:view>