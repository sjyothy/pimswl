<%-- 
  - Author:Muhammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Beneficiaries
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	    function resetValues()
      	{
      	  
      		document.getElementById("searchFrm:txtFloorNo").value="";
      		document.getElementById("searchFrm:txtPropertyNumber").value="";
      		document.getElementById("searchFrm:txtEndowedName").value="";
      		document.getElementById("searchFrm:txtEndowedNumber").value="";
			document.getElementById("searchFrm:txtUnitNo").value="";
			document.getElementById("searchFrm:txtUnitCostCenter").value="";        	
			document.getElementById("searchFrm:txtBedRooms").value="";
			document.getElementById("searchFrm:txtDewaNumber").value="";
			document.getElementById("searchFrm:txtRentAmountFrom").value="";
			document.getElementById("searchFrm:txtRentAmountTo").value="";
			document.getElementById("searchFrm:txtPropertyName").value="";    
      	    document.getElementById("searchFrm:selectUnitUsage").selectedIndex=0;
      	    document.getElementById("searchFrm:selectUnitType").selectedIndex=0;
      	    document.getElementById("searchFrm:selectInvestmentType").selectedIndex=0;
      	    document.getElementById("searchFrm:selectUnitSide").selectedIndex=0;
      	    document.getElementById("searchFrm:selectPropertyOwnerShip").selectedIndex=0;
      	    document.getElementById("searchFrm:selectStatus").selectedIndex=0;
			document.getElementById("searchFrm:state").selectedIndex=0;
			document.getElementById("searchFrm:txtUnitDesc").value="";
			$('searchFrm:expMatDateTo').component.resetSelectedDate();
			
        }        
		
		function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	function closeWindow()
		{
		  window.close();
		}
        
        	  function	openPopUp()
		{
		
		    var width = 300;
            var height = 300;
            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + ",menubar=yes,toolbar=yes,scrollbars=yes,resizable=yes,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

            var child1 = window.open("about:blank", "subWind", windowFeatures);

		      child1.focus();
		}
		
		
			function GenerateUnitsPopup()
		{
		      var screen_width = 1024;
		      var screen_height = 450;
		      var popup_width = screen_width-250;
		      var popup_height = screen_height;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('editUnitPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		function openLoadReportPopup(pageName) 
	{	
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<div style="width: 1024px;">

				<table cellpadding="0" cellspacing="0" border="0">
					<c:choose>
						<c:when test="${!pages$SearchBeneficiary.isViewModePopUp}">
							<tr
								style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>

					<tr width="100%">
						<c:choose>
							<c:when test="${!pages$SearchBeneficiary.isViewModePopUp}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['SearchBeneficiary.Header']}"
											styleClass="HEADER_FONT" style="padding:10px" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>


							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg">
									</td>
									<td width="0%" height="99%" valign="top" nowrap="nowrap">
										<h:form id="searchFrm">
											<div class="SCROLLABLE_SECTION" >
												<table border="0" class="layoutTable" width="90%"
													style="margin-left: 15px; margin-right: 15px;">
													<tr>
														<td>
															<h:outputText id="errorMsg" escape="false"
																styleClass="ERROR_FONT" />
														</td>
														<td>
															<h:outputText id="successMsg" escape="false"
																styleClass="INFO_FONT" />
														</td>

													</tr>
												</table>
												<div class="MARGIN" style="width: 95.5%">
													<table cellpadding="0" cellspacing="0">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>

													<div class="DETAIL_SECTION" style="width: 99.8%;">
														<h:outputLabel value="#{msg['commons.searchCriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER" border="0">
															<tr>

																<td width="97%">
																	<table width="100%">

																		<tr>
																			<td width="25%" colspan="1">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.name']}:"></h:outputLabel>
																			</td>
																			<td width="25%" colspan="1">
																				<h:inputText id="beneficiaryName"
																					binding="#{pages$SearchBeneficiary.htmlBeneficiaryName}"
																					tabindex="10">
																				</h:inputText>
																			</td>
																			<td width="25%" colspan="1">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.isMinor']}:"></h:outputLabel>
																			</td>
																			<td width="25%" colspan="1">
																				<h:selectBooleanCheckbox id="isMinorBool"
																					binding="#{pages$SearchBeneficiary.htmlisMinorBool}"
																					tabindex="3">

																				</h:selectBooleanCheckbox>
																			</td>

																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.isStudent']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectBooleanCheckbox id="isStudent"
																					binding="#{pages$SearchBeneficiary.htmlIsStudent}"
																					tabindex="10">
																				</h:selectBooleanCheckbox>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.gender']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="genderSelect"
																					binding="#{pages$SearchBeneficiary.genderSelectMenu}"
																					tabindex="3">
																					<f:selectItem itemLabel="#{msg['commons.All']}"
																						itemValue="-1" />
																					<f:selectItem itemLabel="#{msg['tenants.gender.male']}" itemValue="0" />
																					<f:selectItem itemLabel="#{msg['tenants.gender.female']}" itemValue="1" />
																					<%--<f:selectItems value="#{pages$SearchBeneficiary.genderSelectList}"/>--%>
																				</h:selectOneMenu>
																			</td>

																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.passportNumber']}:"></h:outputLabel>

																			</td>
																			<td>
																				<h:inputText id="passPortNumber"
																					binding="#{pages$SearchBeneficiary.htmlpassportNumber}"
																					tabindex="10">
																				</h:inputText>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.nationalIDNumber']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="nationIdNumber"
																					binding="#{pages$SearchBeneficiary.htmlNationalIdNumber}"
																					tabindex="10">
																				</h:inputText>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.cellNumber']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="cellNumber"
																					binding="#{pages$SearchBeneficiary.htmlCellNumber}"
																					tabindex="1">
																				</h:inputText>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.incCategory']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="incCategory"
																					binding="#{pages$SearchBeneficiary.incCategorySelectMenu}"
																					tabindex="3">
																					<f:selectItem itemLabel="#{msg['commons.All']}"
																						itemValue="-1" />
																					<f:selectItems
																						value="#{pages$SearchBeneficiary.incCategoryList}" />
																				</h:selectOneMenu>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.nurse']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="nurse"
																					binding="#{pages$SearchBeneficiary.htmlNurse}"
																					tabindex="1">
																				</h:inputText>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.guardian']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="guardian"
																					binding="#{pages$SearchBeneficiary.htmlGuardian}"
																					tabindex="1">
																				</h:inputText>

																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['beneficiary.lbl.birthdateFrom']}:"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="birthDateFrom"
																					binding="#{pages$SearchBeneficiary.birthDateFrom}"
																					locale="#{pages$SearchBeneficiary.locale}"
																					popup="true"
																					datePattern="#{pages$SearchBeneficiary.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:185px;height:17px" />

																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.to']}:"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="birthDateTo"
																					binding="#{pages$SearchBeneficiary.birthDateTo}"
																					locale="#{pages$SearchBeneficiary.locale}"
																					popup="true"
																					datePattern="#{pages$SearchBeneficiary.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:185px;height:17px" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['beneficiary.lbl.matdateFrom']}:"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="matDateFrom"
																					binding="#{pages$SearchBeneficiary.matDateFrom}"
																					locale="#{pages$SearchBeneficiary.locale}"
																					popup="true"
																					datePattern="#{pages$SearchBeneficiary.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:185px;height:17px" />

																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.to']}:"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="matDateTo"
																					binding="#{pages$SearchBeneficiary.matDateTo}"
																					locale="#{pages$SearchBeneficiary.locale}"
																					popup="true"
																					datePattern="#{pages$SearchBeneficiary.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:185px;height:17px" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['beneficiary.lbl.expMatdateFrom']}:"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="expMatDateFrom"
																					binding="#{pages$SearchBeneficiary.expMatDateFrom}"
																					locale="#{pages$SearchBeneficiary.locale}"
																					popup="true"
																					datePattern="#{pages$SearchBeneficiary.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:185px;height:17px" />

																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.to']}:"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="expMatDateTo"
																					binding="#{pages$SearchBeneficiary.expMatDateTo}"
																					locale="#{pages$SearchBeneficiary.locale}"
																					popup="true"
																					datePattern="#{pages$SearchBeneficiary.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:185px;height:17px" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel
																					value="#{msg['healthAspects.status']} :"
																					styleClass="LABEL" />
																			</td>
																			<td>
																				<h:selectOneMenu id="healthStatus"
																					binding="#{pages$SearchBeneficiary.healthSelectMenu}">
																					<f:selectItem itemValue=""
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.allHealthStatus}" />
																				</h:selectOneMenu>
																			</td>
																			<td>
																				<h:outputLabel
																					value="#{msg['healthAspects.diseaseType']} :"
																					styleClass="LABEL" />
																			</td>
																			<td>
																				<h:selectOneMenu id="diseaseType"
																					binding="#{pages$SearchBeneficiary.diseaseTypeSelectMenu}">
																					<f:selectItem itemValue=""
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.allDiseaseType}" />
																				</h:selectOneMenu>
																			</td>

																		</tr>

																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.fileNumber']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="fileNumber"
																					binding="#{pages$SearchBeneficiary.htmlFileNumber}"
																					tabindex="1">
																				</h:inputText>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.fileStatus']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="fileStatusList"
																					binding="#{pages$SearchBeneficiary.fileStatusSelectMenu}"
																					tabindex="3">
																					<f:selectItem itemLabel="#{msg['commons.All']}"
																						itemValue="-1" />
																					<f:selectItems
																						value="#{pages$SearchBeneficiary.fileStatusList}" />
																				</h:selectOneMenu>

																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.fileType']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="fileTypeList"
																					binding="#{pages$SearchBeneficiary.fileTypeSelectMenu}"
																					tabindex="3">
																					
																					<f:selectItems
																						value="#{pages$SearchBeneficiary.fileTypeList}" />
																				</h:selectOneMenu>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.researcher']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:selectOneMenu id="researcher"
																					binding="#{pages$SearchBeneficiary.researcherSelectMenu}"
																					tabindex="3">
																					<f:selectItem itemLabel="#{msg['commons.All']}"
																						itemValue="-1" />
																					<f:selectItems
																						value="#{pages$SearchBeneficiary.researcherList}" />
																				</h:selectOneMenu>

																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['SearchBeneficiary.filePerson']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="filePerson"
																					binding="#{pages$SearchBeneficiary.htmlFilePerson}"
																					tabindex="1">
																				</h:inputText>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['inheritanceFile.fieldLabel.costCenter']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="fileCC"
																					binding="#{pages$SearchBeneficiary.textCC}"
																					tabindex="8" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['inheritanceFile.fieldLabel.benCostCenter']}:"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="benCC"
																					binding="#{pages$SearchBeneficiary.textBenCC}"
																					tabindex="8" />
																			</td>
																		</tr>
																		<td colspan="4">
																			<DIV class="BUTTON_TD">
																				<h:commandButton styleClass="BUTTON" type="submit"
																					value="#{msg['commons.search']}"
																					action="#{pages$SearchBeneficiary.doSearch}" />
																				<h:commandButton styleClass="BUTTON" type="button"
																					onclick="javascript:resetValues();"
																					value="#{msg['commons.clear']}" />
																				<h:commandButton styleClass="BUTTON"
																					rendered="false" value="#{msg['unit.addButton']}" />
																				<h:commandButton styleClass="BUTTON" type="button"
																					value="#{msg['commons.cancel']}"
																					onclick="javascript:window.close();">
																				</h:commandButton>
																			</DIV>
																		</td>
																		</tr>
																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>
															</tr>

														</table>
													</div>
													<t:div>&nbsp;</t:div>

													<t:div styleClass="imag">&nbsp;</t:div>
													<t:div styleClass="contentDiv"
														style="width:99.2%;align:center;">

														<t:dataTable id="dt1" preserveSort="false" var="dataItem"
															rules="all" renderedIfEmpty="true" width="100%"
															rowClasses="row1,row2"
															rows="#{pages$SearchBeneficiary.paginatorRows}"
															value="#{pages$SearchBeneficiary.dataList}"
															binding="#{pages$SearchBeneficiary.dataTable}">

															<t:column id="col1" width="10%" defaultSorted="true"
																style="white-space: normal;">

																<f:facet name="header">
																	<t:commandSortHeader columnName="col1"
																		actionListener="#{pages$SearchBeneficiary.sort}"
																		value="#{msg['SearchBeneficiary.name']}" arrow="true">
																		<f:attribute name="sortField" value="firstName" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.personName}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="inheritanceFileNumber" width="10%"
																defaultSorted="true" style="white-space: normal;">

																<f:facet name="header">
																	<t:commandSortHeader columnName="fileNumber"
																		actionListener="#{pages$SearchBeneficiary.sort}"
																		value="#{msg['SearchBeneficiary.fileNumber']}"
																		arrow="true">
																		<f:attribute name="sortField" value="fileNumber" />
																	</t:commandSortHeader>

																</f:facet>
																<t:outputText value="#{dataItem.fileNumbers}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="inheritanceFilefieldLabelcostCenter"
																width="10%" defaultSorted="true"
																style="white-space: normal;">

																<f:facet name="header">
																	<t:outputText
																		value="#{msg['inheritanceFile.fieldLabel.costCenter']}" />
																</f:facet>
																<t:outputText value="#{dataItem.costCenter}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="isMinor" width="10%" sortable="true"
																style="white-space: normal;">
																<f:facet name="header">
																	<t:commandSortHeader columnName="isMinor"
																		actionListener="#{pages$SearchBeneficiary.sort}"
																		value="#{msg['SearchBeneficiary.isMinor']}"
																		arrow="true">
																		<f:attribute name="sortField" value="isMinor" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText
																	value="#{dataItem.isMinor==null?msg['commons.notAvailable']:(dataItem.isMinor==1? msg['commons.true']:msg['commons.false'])}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="dateOfBirth" width="10%"
																defaultSorted="true" style="white-space: normal;">

																<f:facet name="header">
																	<t:commandSortHeader columnName="dateOfBirth"
																		actionListener="#{pages$SearchBeneficiary.sort}"
																		value="#{msg['commons.dateofbirth']}" arrow="true">
																		<f:attribute name="sortField" value="dateOfBirth" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.dateOfBirth}"
																	style="white-space: normal;" styleClass="A_LEFT">
																	<f:convertDateTime
																		timeZone="#{pages$SearchBeneficiary.timeZone}" />
																</t:outputText>
															</t:column>
															<t:column id="maturityDate" width="10%"
																defaultSorted="true" style="white-space: normal;">

																<f:facet name="header">

																	<t:commandSortHeader columnName="maturityDate"
																		actionListener="#{pages$SearchBeneficiary.sort}"
																		value="#{msg['mems.inheritanceFile.fieldLabel.maturityDate']}"
																		arrow="true">
																		<f:attribute name="sortField" value="maturityDate" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.maturityDate}"
																	style="white-space: normal;" styleClass="A_LEFT">
																	<f:convertDateTime
																		timeZone="#{pages$SearchBeneficiary.timeZone}" />
																</t:outputText>
															</t:column>
															<t:column id="expMaturityDate" width="10%"
																defaultSorted="true" style="white-space: normal;">

																<f:facet name="header">

																	<t:commandSortHeader columnName="expMaturityDate"
																		actionListener="#{pages$SearchBeneficiary.sort}"
																		value="#{msg['mems.inheritanceFile.fieldLabel.expectedMaturityDate']}"
																		arrow="true">
																		<f:attribute name="sortField" value="expMaturityDate" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.expMaturityDate}"
																	style="white-space: normal;" styleClass="A_LEFT">
																	<f:convertDateTime
																		timeZone="#{pages$SearchBeneficiary.timeZone}" />
																</t:outputText>
															</t:column>

															<t:column id="passPortNumber" width="10%" sortable="true"
																style="white-space: normal;">

																<f:facet name="header">
																	<t:outputText
																		value="#{msg['SearchBeneficiary.passportNumber']}" />
																</f:facet>
																<t:outputText value="#{dataItem.passportNumber}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>

															<t:column id="isStudent" width="10%" sortable="true"
																style="white-space: normal;">

																<f:facet name="header">
																	<t:commandSortHeader columnName="isStudent"
																		actionListener="#{pages$SearchBeneficiary.sort}"
																		value="#{msg['SearchBeneficiary.isStudent']}"
																		arrow="true">
																		<f:attribute name="sortField" value="isStudent" />
																	</t:commandSortHeader>

																</f:facet>
																<t:outputText
																	value="#{dataItem.isStudent==1? msg['commons.true']:msg['commons.false']}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>

															<t:column id="gender" width="10%" sortable="false"
																style="white-space: normal;">

																<f:facet name="header">
																	<t:commandSortHeader columnName="gender"
																		actionListener="#{pages$SearchBeneficiary.sort}"
																		value="#{msg['SearchBeneficiary.gender']}"
																		arrow="true">
																		<f:attribute name="sortField" value="gender" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText
																	value="#{dataItem.gender==null?msg['commons.notAvailable']:(dataItem.gender=='M' ? msg['tenants.gender.male'] : msg['tenants.gender.female']) }"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>

															<t:column id="incomeCate" width="10%" sortable="false"
																style="white-space: normal;">

																<f:facet name="header">
																	<t:commandSortHeader columnName="incomeCate"
																		actionListener="#{pages$SearchBeneficiary.sort}"
																		value="#{msg['SearchBeneficiary.incCategory']}"
																		arrow="true">
																		<f:attribute name="sortField" value="incomeCategory" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText
																	value="#{pages$SearchBeneficiary.englishLocale?dataItem.incomeCategoryEn:dataItem.incomeCategoryAr}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>





															<t:column id="selectMany" width="10%" sortable="false"
																style="white-space: normal;"
																rendered="#{pages$SearchBeneficiary.isPageModeSelectManyPopUp}">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.select']}" />
																</f:facet>
																<h:selectBooleanCheckbox id="select"
																	value="#{dataItem.selected}" />
															</t:column>

															<t:column id="selectOne" sortable="false" width="10%"
																style="white-space: normal;"
																rendered="#{pages$SearchBeneficiary.isPageModeSelectOnePopUp}">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.select']}" />
																</f:facet>
																<t:commandLink
																	actionListener="#{pages$SearchBeneficiary.sendBeneficiaryInfoToParent}">
																	<h:graphicImage style="width: 16;height: 16;border: 0"
																		alt="#{msg['commons.select']}"
																		url="../resources/images/select-icon.gif"></h:graphicImage>
																</t:commandLink>
															</t:column>

															<t:column id="editCol" width="5%"
																style="white-space: normal;"
																rendered="#{!pages$SearchBeneficiary.isPageModeSelectManyPopUp && !pages$SearchBeneficiary.isPageModeSelectOnePopUp}"
																sortable="false">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.edit']}" />
																</f:facet>
																<t:commandLink action="#{pages$SearchBeneficiary.onEditFamilyVillageBeneficiary}"
																	rendered="#{!pages$SearchBeneficiary.isPageModeSelectManyPopUp}">
																	<h:graphicImage id="editIcon"
																		title="#{msg['commons.edit']}"
																		url="../resources/images/edit-icon.gif" />
																</t:commandLink>
															</t:column>
															<t:column id="researchFamilyVillageBeneficiary" width="5%"
																style="white-space: normal;"
																rendered="#{!pages$SearchBeneficiary.isPageModeSelectManyPopUp &&
																			!pages$SearchBeneficiary.isPageModeSelectOnePopUp &&
																			 pages$SearchBeneficiary.contextFamilyVillage
																		   }"
																sortable="false">
																<f:facet name="header">
																	<t:outputText value="#{msg['ResearchFormBenef.Header']}" />
																</f:facet>
																<t:commandLink action="#{pages$SearchBeneficiary.onResearchFamilyVillageBeneficiary}"
																	rendered="#{!pages$SearchBeneficiary.isPageModeSelectManyPopUp}">
																	<h:graphicImage id="researchFamilyVillageBeneficiaryIcon"
																		title="#{msg['commons.edit']}"
																		url="../resources/images/detail-icon.gif" />
																</t:commandLink>
															</t:column>
														</t:dataTable>
													</t:div>
													<t:div styleClass="contentDivFooter" style="width:100.2%">
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td class="RECORD_NUM_TD">
																	<div class="RECORD_NUM_BG">
																		<table cellpadding="0" cellspacing="0"
																			style="width: 182px; # width: 150px;">
																			<tr>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText value=" : " />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{pages$SearchBeneficiary.recordSize}" />
																				</td>
																			</tr>
																		</table>
																	</div>
																</td>
																<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																	align="right">

																	<t:div styleClass="PAGE_NUM_BG" style="#width:20%">
																		<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																		<table cellpadding="0" cellspacing="0">
																			<tr>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																				</td>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{pages$SearchBeneficiary.currentPage}" />
																				</td>
																			</tr>
																		</table>
																	</t:div>
																	<TABLE border="0" class="SCH_SCROLLER">
																		<tr>
																			<td>
																				<t:commandLink
																					action="#{pages$SearchBeneficiary.pageFirst}"
																					disabled="#{pages$SearchBeneficiary.firstRow == 0}">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$SearchBeneficiary.pagePrevious}"
																					disabled="#{pages$SearchBeneficiary.firstRow == 0}">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:dataList value="#{pages$SearchBeneficiary.pages}"
																					var="page">
																					<h:commandLink value="#{page}"
																						actionListener="#{pages$SearchBeneficiary.page}"
																						rendered="#{page != pages$SearchBeneficiary.currentPage}" />
																					<h:outputText value="<b>#{page}</b>" escape="false"
																						rendered="#{page == pages$SearchBeneficiary.currentPage}" />
																				</t:dataList>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$SearchBeneficiary.pageNext}"
																					disabled="#{pages$SearchBeneficiary.firstRow + pages$SearchBeneficiary.rowsPerPage >= pages$SearchBeneficiary.totalRows}">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$SearchBeneficiary.pageLast}"
																					disabled="#{pages$SearchBeneficiary.firstRow + pages$SearchBeneficiary.rowsPerPage >= pages$SearchBeneficiary.totalRows}">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblL"></t:graphicImage>
																				</t:commandLink>
																			</td>
																		</tr>
																	</TABLE>
																</td>
															</tr>
														</table>
													</t:div>
												</div>
												<table width="96.6%" border="0">
													<tr>
														<td class="BUTTON_TD JUG_BUTTON_TD_US">

															<h:commandButton type="submit"
																rendered="#{pages$SearchBeneficiary.isPageModeSelectManyPopUp}"
																styleClass="BUTTON"
																actionListener="#{pages$SearchBeneficiary.sendManyBeneficiaryInfoToParent}"
																value="#{msg['commons.select']}" />
														</td>

													</tr>


												</table>
											</div>
										</h:form>
									</td>
								</tr>

							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>