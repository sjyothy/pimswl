<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	function performTabClick(elementid)
	{
			
		disableInputs();
		
		document.getElementById("detailsFrm:"+elementid).onclick();
		
		return 
		
	}
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
	}	
	
	
	
	function onMessageFromEndowmentPrograms()
	{
	     disableInputs();
	     document.getElementById("detailsFrm:onMessageFromEndowmentPrograms").onclick();
	}
		
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		disableInputs();	
		if( hdnPersonType=='APPLICANT' ) 
		{			    			 
			 document.getElementById("detailsFrm:hdnPersonId").value=personId;
			 document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
			 document.getElementById("detailsFrm:hdnPersonName").value=personName;
			 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
			 document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;
		}
				
	     document.getElementById("detailsFrm:onMessageFromSearchPerson").onclick();
	 }
	  function showPropertySearchPopUp()
	 {
		var screen_width = 1024;
		var screen_height = 600;
		window.open('propertyList.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	 }
	 
	  
	 function populateProperty( propertyId )
	 {
		disableInputs();	
		document.getElementById("detailsFrm:hdnPropertyId").value=propertyId;
		document.getElementById("detailsFrm:onMessageFromPropertySearch").onclick();
	 }
	
	 function showContractSearchPopUp()
	 {
		var screen_width = 1024;
		var screen_height = 600;
		window.open('ContractList.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	 }
	 function populateMaintenanceContract( contractId )
	 {
		disableInputs();	
		document.getElementById("detailsFrm:hdnContractId").value=contractId ;
		document.getElementById("detailsFrm:onMessageFromContractSearch").onclick();
	 }

 	 function showUnitSearchPopUp()
	 {
		var screen_width = 1024;
		var screen_height = 600;
		window.open('UnitSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP&context=Complaints','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=10,top=150,scrollbars=yes,status=yes');
	 }
	 
	 function populateUnit(unitId,unitNum,usuageTypeId,unitRentAmount,unitUsuageType,propertyCommercialName,unitAddress,unitStatusId,unitStatus)
	 {
	    disableInputs();	
		document.getElementById("detailsFrm:onMessageFromUnitSearch").onclick();
	 }
	 
	 function openMaintenanceHistoryPopUp(complaintId)
	 {
		 var screen_width = 1024;
	       var screen_height = 450;
	        window.open('MaintenanceRequestHistory.jsf?complaintId='+complaintId,'_blank','width='+(screen_width-100)+',height='+(screen_height)+',left=50,top=150,scrollbars=yes,status=yes');
		}
		
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">

					<c:choose>
						<c:when test="${pages$complaintDetails.pageMode != 'POPUP'}">
							<tr
								style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>

					<tr width="100%">
						<c:choose>
							<c:when test="${pages$complaintDetails.pageMode != 'POPUP'}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{pages$complaintDetails.pageTitle}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>

							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">
												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>

																<h:outputText id="errorId"
																	value="#{pages$complaintDetails.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$complaintDetails.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$complaintDetails.pageMode}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$complaintDetails.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$complaintDetails.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$complaintDetails.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$complaintDetails.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$complaintDetails.hdnIsCompany}"></h:inputHidden>
																<h:inputHidden id="hdnPropertyId"
																	value="#{pages$complaintDetails.hdnPropertyId}"></h:inputHidden>
																<h:inputHidden id="hdnContractId"
																	value="#{pages$complaintDetails.hdnContractId}"></h:inputHidden>
																<h:commandLink id="onMessageFromSearchPerson"
																	action="#{pages$complaintDetails.onMessageFromSearchPerson}" />
																<h:commandLink id="onMessageFromUnitSearch"
																	action="#{pages$complaintDetails.onMessageFromUnitSearch}" />
																<h:commandLink id="onMessageFromPropertySearch"
																	action="#{pages$complaintDetails.onMessageFromPropertySearch}" />
																<h:commandLink id="onMessageFromContractSearch"
																	action="#{pages$complaintDetails.onMessageFromContractSearch}" />
															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<t:div style="width:100%;">

														<t:panelGrid cellpadding="1px" width="100%"
															cellspacing="5px" columns="8"
															columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">

															<h:outputText id="idSendTo" styleClass="LABEL"
																rendered="#{pages$complaintDetails.pageMode=='FM_TASK' || 
																			pages$complaintDetails.pageMode=='PM_TASK' ||
																			pages$complaintDetails.pageMode=='APPROVAL_REQUIRED'
																			}"
																value="#{msg['mems.investment.label.sendTo']}" />
															<t:panelGroup colspan="7" style="width: 100%"
																rendered="#{pages$complaintDetails.pageMode=='FM_TASK' || 
																			pages$complaintDetails.pageMode=='PM_TASK' ||
																			pages$complaintDetails.pageMode=='APPROVAL_REQUIRED'
																			}">
																<h:selectOneMenu id="idUserGrps"
																	rendered="#{pages$complaintDetails.pageMode=='FM_TASK' || 
																			pages$complaintDetails.pageMode=='PM_TASK' ||
																			pages$complaintDetails.pageMode=='APPROVAL_REQUIRED'
																			}"
																	binding="#{pages$complaintDetails.cmbReviewGroup}"
																	style="width: 200px;">
																	<f:selectItem
																		itemLabel="#{msg['commons.pleaseSelect']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.userGroups}" />
																</h:selectOneMenu>
															</t:panelGroup>


															<h:outputLabel styleClass="LABEL"
																value="#{msg['maintenanceRequest.remarks']}" />
															<t:panelGroup colspan="7" style="width: 100%">
																<t:inputTextarea style="width: 90%;" id="txt_remarks"
																	value="#{pages$complaintDetails.txtRemarks}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</t:panelGroup>
														</t:panelGrid>
													</t:div>
													<t:panelGrid columnClasses="BUTTON_TD" columns="2"
														cellpadding="1px" width="100%" cellspacing="3px"
														rendered="#{
																	!  empty pages$complaintDetails.requestView &&
																	!  empty pages$complaintDetails.requestView.complaintDetailsView && 
																	! empty pages$complaintDetails.requestView.complaintDetailsView.complaintId
																  }">
														<t:panelGroup colspan="10">
															<h:commandButton type="button" styleClass="BUTTON"
																style="width:150px;"
																title="#{msg['contract.tabtooltip.MaintenanceHistory']}"
																value="#{msg['contract.tabHeading.MaintenanceHistory']}"
																onclick="javascript:openMaintenanceHistoryPopUp(#{pages$complaintDetails.requestView.complaintDetailsView.complaintId});" />
														</t:panelGroup>
													</t:panelGrid>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">

														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$complaintDetails.tabPanel}"
																style="width: 100%">

																<rich:tab id="applicationTab"
																	label="#{msg['commons.tab.applicationDetails']}">
																	<%@ include file="ApplicationDetails.jsp"%>
																</rich:tab>
																<rich:tab id="detailsTab"
																	label="#{msg['commons.details']}">
																	<%@ include file="tabComplaintDetails.jsp"%>

																</rich:tab>
																<rich:tab id="reviewTab"
																	label="#{msg['commons.tab.reviewDetails']}">
																	<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
																</rich:tab>
																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>
																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>
																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$complaintDetails.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>

														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">

															<pims:security
																screen="Pims.Miscellaneous.Complaints.Save"
																action="create">
																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$complaintDetails.showSaveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$complaintDetails.onSave}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$complaintDetails.showSubmitButton}"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSubmit"
																	action="#{pages$complaintDetails.onSubmit}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$complaintDetails.showResubmitButton}"
																	value="#{msg['socialResearch.btn.resubmit']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkResubmit"
																	action="#{pages$complaintDetails.onResubmitted}" />

															</pims:security>


															<pims:security
																screen="Pims.Miscellaneous.Complaints.BusinessManager"
																action="create">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$complaintDetails.showBusinessManagerTaskButtons}"
																	value="#{msg['commons.complete']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkComplete"
																	action="#{pages$complaintDetails.onComplete}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$complaintDetails.showBusinessManagerTaskButtons}"
																	value="#{msg['complaint.lbl.sendToFM']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSentToFM"
																	action="#{pages$complaintDetails.onSentToFM}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$complaintDetails.showBusinessManagerTaskButtons}"
																	value="#{msg['complaint.lbl.sendToPM']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSentToPM"
																	action="#{pages$complaintDetails.onSentToPM}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$complaintDetails.showBusinessManagerTaskButtons}"
																	value="#{msg['commons.reject']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkReject"
																	action="#{pages$complaintDetails.onReject}" />


															</pims:security>

															<pims:security
																screen="Pims.Miscellaneous.Complaints.FacilitiesManager"
																action="create">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$complaintDetails.showFMTaskButtons}"
																	style="width:auto;"
																	value="#{msg['maintenanceRequest.siteVisitReq']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSiteVisitRequired"
																	action="#{pages$complaintDetails.onSiteVisitRequired}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$complaintDetails.showFMTaskButtons}"
																	value="#{msg['commons.done']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkDoneByFM"
																	action="#{pages$complaintDetails.onDoneByFM}" />

																<h:commandButton styleClass="BUTTON" style="width:auto;"
																	rendered="#{pages$complaintDetails.showFMTaskButtons}"
																	value="#{msg['complaint.lbl.maintenanceRequest']}"
																	onclick="performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkInitiateMaintenanceRequest"
																	action="#{pages$complaintDetails.onInitiateMaintenanceRequest}" />

															</pims:security>

															<pims:security
																screen="Pims.Miscellaneous.Complaints.PropertiesManager"
																action="create">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$complaintDetails.showPMTaskButtons}"
																	value="#{msg['commons.done']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkDoneByPM"
																	action="#{pages$complaintDetails.onDoneByPM}" />
															</pims:security>

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$complaintDetails.showPMTaskButtons || 
																			pages$complaintDetails.showFMTaskButtons ||
																			pages$complaintDetails.showBusinessManagerTaskButtons
																			}"
																value="#{msg['commons.review']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkReviewRequired"
																action="#{pages$complaintDetails.onReviewRequired}" />

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$complaintDetails.showReviewTaskButtons}"
																value="#{msg['commons.done']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkReviewed"
																action="#{pages$complaintDetails.onReviewed}" />



															<pims:security
																screen="Pims.Miscellaneous.Complaints.SiteVisit"
																action="create">


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$complaintDetails.showSiteVisitTaskButtons}"
																	value="#{msg['commons.done']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSiteVisitDone"
																	action="#{pages$complaintDetails.onSiteVisitDone}" />
															</pims:security>
														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<c:choose>
								<c:when test="${pages$complaintDetails.pageMode != 'POPUP'}">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td class="footer">
												<h:outputLabel id="ftrMssg"
													value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
								</c:when>
							</c:choose>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>