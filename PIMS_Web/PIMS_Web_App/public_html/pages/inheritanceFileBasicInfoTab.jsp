<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%-- 
<t:collapsiblePanel id="test" title="testTitle" var="test2collapsed" value="true">
      <f:facet name="header">
        <t:div style="width:100%;background-color:#CCCCCC;">
          <h:outputText value="Person "/>
          <t:headerLink immediate="true">
            <h:outputText value="Details" rendered="#{test2collapsed}"/>
            <h:outputText value="Overview" rendered="#{!test2collapsed}"/>
          </t:headerLink>
        </t:div>
      </f:facet>
      --%>
<t:div style="height:200px;overflow-y:scroll;width:100%;">
	<t:panelGrid border="0" width="97%" cellpadding="1" cellspacing="1"
		id="inheritanceFileBasicInfoTabControls">
		<t:panelGroup>
			<%--Column 1,2 Starts--%>
			<t:panelGrid columns="4"
				columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

				<h:panelGroup>
					<h:panelGroup>
						<h:outputLabel styleClass="mandatory" value="*" />
						<h:outputText
							value="#{msg['mems.inheritanceFile.fieldLabel.deceasedPerosn']}"
							rendered="#{pages$inheritanceFileBasicInfoTab.deceased}" />
						<h:outputText
							value="#{msg['mems.inheritanceFile.fieldLabel.filePerson']}"
							rendered="#{pages$inheritanceFileBasicInfoTab.legalAsstnc || 
							pages$inheritanceFileBasicInfoTab.underGuradian ||
							(!pages$inheritanceFileBasicInfoTab.deceased && 
							!pages$inheritanceFileBasicInfoTab.founding &&
							!pages$inheritanceFileBasicInfoTab.absent ) }" />
						<h:outputText
							value="#{msg['mems.inheritanceFile.fieldLabel.foundingPerosn']}"
							rendered="#{pages$inheritanceFileBasicInfoTab.founding}" />
						<h:outputText
							value="#{msg['mems.inheritanceFile.fieldLabel.absentPerosn']}"
							rendered="#{pages$inheritanceFileBasicInfoTab.absent}" />
					</h:panelGroup>
				</h:panelGroup>
				<h:panelGroup>
					<h:inputText readonly="true"
						value="#{pages$inheritanceFileBasicInfoTab.filePerson.personFullName}" />
					<h:graphicImage title="#{msg['commons.search']}"
						rendered="#{pages$inheritanceFile.showFilePersonSearch}"
						style="MARGIN: 1px 1px -5px;cursor:hand"
						onclick="hdnLinkClicked();"
						url="../resources/images/app_icons/Search-tenant.png">
					</h:graphicImage>
					<t:commandLink
						rendered="#{pages$inheritanceFile.showPrintStatmentOfAccImage}"
						action="#{pages$inheritanceFileBasicInfoTab.openStatmntOfAccount}">
						<h:graphicImage title="#{msg['report.tooltip.PrintStOfAcc']}"
							style="MARGIN: 1px 1px -5px;cursor:hand"
							url="../resources/images/app_icons/print.gif">
						</h:graphicImage>&nbsp;
				</t:commandLink>
					<t:commandLink
						rendered="#{pages$inheritanceFile.showSocialResearchImage}"
						action="#{pages$inheritanceFileBasicInfoTab.openResearcherForm}">
						<h:graphicImage id="researchForm"
							title="#{msg['beneficiaryDetails.tooltip.openResearchForm']}"
							style="cursor:hand;" url="../resources/images/detail-icon.gif" />
					</t:commandLink>

					<t:commandLink
						rendered="#{pages$inheritanceFile.showSocialResearchImage}"
						onclick="javaScript:openBeneficiaryPopup(#{pages$inheritanceFileBasicInfoTab.filePerson.personId});">
						<h:graphicImage id="manageBeneficiaryUnderguradin"
							title="#{msg['commons.details']}"
							style="cursor:hand;margin-left:5px;margin-right:5px;"
							url="../resources/images/detail-icon.gif" />
					</t:commandLink>
				</h:panelGroup>

				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.passportName']}" />
				</h:panelGroup>
				<h:inputText maxlength="100"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.passportName}" />

				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.gender']}" />
				</h:panelGroup>
				<h:selectOneMenu id="selectGender" styleClass="SELECT_MENU"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.gender}">
					<f:selectItem itemLabel="#{msg['tenants.gender.male']}"
						itemValue="M" />
					<f:selectItem itemLabel="#{msg['tenants.gender.female']}"
						itemValue="F" />
				</h:selectOneMenu>

				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.nationality']}" />
				</h:panelGroup>
				<h:selectOneMenu id="nationality" immediate="false"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.nationalityIdString}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems value="#{pages$ApplicationBean.countryList}" />
				</h:selectOneMenu>



				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.nationalId']}" />
				<h:inputText maxlength="15"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.personalSecCardNo}" />

				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.birthDate']}" />
				</h:panelGroup>
				<rich:calendar
					disabled="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.dateOfBirth}"
					locale="#{pages$inheritanceFileBasicInfoTab.locale}" popup="true"
					datePattern="#{pages$inheritanceFileBasicInfoTab.dateFormat}"
					showApplyButton="false" enableManualInput="false" />

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.birthPlace']}" />
				<h:selectOneMenu id="birthPlaceId" immediate="false"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.birthPlaceId}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems value="#{pages$ApplicationBean.countryList}" />
				</h:selectOneMenu>

				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.resCountry']}" />
				</h:panelGroup>
				<h:selectOneMenu id="resCountryId"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.countryId}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems value="#{pages$ApplicationBean.countryList}" />
					<a4j:support event="onchange" reRender="resCityId"
						action="#{pages$inheritanceFileBasicInfoTab.countryChanged}" />
				</h:selectOneMenu>

				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.resCity']}" />
				</h:panelGroup>
				<h:selectOneMenu id="resCityId" immediate="false"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.cityId}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems
						value="#{pages$inheritanceFileBasicInfoTab.cityList}" />
				</h:selectOneMenu>

				<h:panelGroup>
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.address']}" />
				</h:panelGroup>
				<h:inputText maxlength="150"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.address1}" />

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.passportNumber']}" />
				<h:inputText maxlength="100"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.passportNumber}" />

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.passportPlace']}" />
				<h:selectOneMenu id="passportPlaceId" immediate="false"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.passportIssuePlaceIdString}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems value="#{pages$ApplicationBean.countryList}" />

					<a4j:support event="onchange" reRender="passportPlaceCity"
						action="#{pages$inheritanceFileBasicInfoTab.passportCountryChanged}" />
				</h:selectOneMenu>
				<h:outputText value="#{msg['person.passportIssueCity']}" />
				<h:selectOneMenu id="passportPlaceCity" immediate="false"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.passportIssueCityString}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems
						value="#{pages$inheritanceFileBasicInfoTab.passportCityList}" />
				</h:selectOneMenu>



				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.maritalStatus']}" />
				<h:selectOneMenu id="cboMaritialStatus"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.maritialStatusIdString}">
					<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
						itemValue="-1" />
					<f:selectItems value="#{pages$ApplicationBean.maritialStatus}" />
				</h:selectOneMenu>

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.jobDescription']}" />
				<h:inputText maxlength="50"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.jobDescription}" />

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.placeOfWork']}" />
				<h:inputText maxlength="50"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.workingCompany}" />
				<%-- Removed on Barhoumneh Request
			<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*" />--%>
				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.residenceNmber']}" />
				<%-- Removed on Barhoumneh Request
			</h:panelGroup> --%>
				<h:inputText maxlength="15"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.homePhone}" />

				<h:panelGroup id="lblCurrentWives"
					rendered="#{pages$inheritanceFileBasicInfoTab.deceased}">
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.currentWives']}" />
				</h:panelGroup>
				<h:inputText id="txtCurrentWives" maxlength="150"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					rendered="#{pages$inheritanceFileBasicInfoTab.deceased}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.currentWives}" />

				<h:outputText id="lblPreviousWives"
					rendered="#{pages$inheritanceFileBasicInfoTab.deceased}"
					value="#{msg['mems.inheritanceFile.fieldLabel.previosWives']}" />
				<h:inputText id="txtPreviousWives"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					rendered="#{pages$inheritanceFileBasicInfoTab.deceased}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.previousWives}" />

				<h:panelGroup id="lblDeathDate"
					rendered="#{pages$inheritanceFileBasicInfoTab.deceased}">
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.deathDate']}"
						rendered="#{pages$inheritanceFileBasicInfoTab.deceased}" />
				</h:panelGroup>
				<rich:calendar id="clndrDeathDate"
					disabled="#{!pages$inheritanceFile.showFilePersonSearch}"
					rendered="#{pages$inheritanceFileBasicInfoTab.deceased}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.dateOfDeath}"
					locale="#{pages$inheritanceFileBasicInfoTab.locale}" popup="true"
					datePattern="#{pages$inheritanceFileBasicInfoTab.dateFormat}"
					verticalOffset="-212" showApplyButton="false"
					enableManualInput="false" />




				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.deathPlace']}"
					rendered="#{pages$inheritanceFileBasicInfoTab.deceased}" />
				<h:selectOneMenu id="clndrDeathPlace" immediate="false"
					readonly="#{!pages$inheritanceFile.showFilePersonSearch}"
					rendered="#{pages$inheritanceFileBasicInfoTab.deceased}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.deathPlace}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems value="#{pages$ApplicationBean.countryList}" />
				</h:selectOneMenu>


				<h:panelGroup rendered="#{pages$inheritanceFileBasicInfoTab.absent}">
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.missingFrom']}"
						id="missingFromLabel"
						rendered="#{pages$inheritanceFileBasicInfoTab.absent}" />
				</h:panelGroup>
				<rich:calendar id="missingFromDate"
					disabled="#{!pages$inheritanceFile.showFilePersonSearch}"
					rendered="#{pages$inheritanceFileBasicInfoTab.absent}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.missingFrom}"
					locale="#{pages$inheritanceFileBasicInfoTab.locale}" popup="true"
					datePattern="#{pages$inheritanceFileBasicInfoTab.dateFormat}"
					showApplyButton="false" enableManualInput="false" />

				<h:panelGroup
					rendered="#{pages$inheritanceFile.filePersonBeneficiary}"
					id="lblFilePersonBeneShareId">
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.share']}" />
				</h:panelGroup>
				<h:inputText readonly="true"
					rendered="#{pages$inheritanceFile.filePersonBeneficiary}"
					id="txtFilePersonBeneShareId" maxlength="50"
					value="#{pages$inheritanceFileBasicInfoTab.filePersonBeneficiary.sharePercentage}" />

				<%--New Fields --%>
				<h:outputText
					rendered="#{pages$inheritanceFile.filePersonBeneficiary}"
					value="#{msg['financialAccConfiguration.bankRemitanceAccountName']}" />
				<h:inputText
					rendered="#{pages$inheritanceFile.filePersonBeneficiary}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.accountNumber}" />

				<h:outputText
					rendered="#{pages$inheritanceFile.filePersonBeneficiary}"
					value="#{msg['grp.BankName']}" />
				<h:selectOneMenu
					rendered="#{pages$inheritanceFile.filePersonBeneficiary}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.bankId}">
					<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
						itemValue="-1" />
					<f:selectItems value="#{pages$ApplicationBean.bankList}" />
				</h:selectOneMenu>

				<h:outputText
					rendered="#{pages$inheritanceFile.filePersonBeneficiary}"
					value="#{msg['label.familyNumber']}" />
				<h:inputText
					rendered="#{pages$inheritanceFile.filePersonBeneficiary}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.familyNumber}" />

				<h:outputText
					rendered="#{pages$inheritanceFile.filePersonBeneficiary}"
					value="#{msg['migrationProject.fieldName.townNumber']}" />
				<h:inputText
					rendered="#{pages$inheritanceFile.filePersonBeneficiary}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.townNumber}" />

				<h:outputText
					rendered="#{pages$inheritanceFile.filePersonBeneficiary}"
					value="#{msg['sessionDetailsTab.maturityDate']}" />
				<rich:calendar
					rendered="#{pages$inheritanceFile.filePersonBeneficiary}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.maturityDate}"
					locale="#{pages$inheritanceFileBasicInfoTab.locale}" popup="true"
					datePattern="#{pages$inheritanceFileBasicInfoTab.dateFormat}"
					showApplyButton="false" enableManualInput="false" />
				<%--New Fields --%>
				<h:panelGroup id="pgLblSponsor"
					rendered="#{pages$inheritanceFileBasicInfoTab.founding}">
					<h:outputLabel styleClass="mandatory" value="*" />
					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.sponsor']}" />
				</h:panelGroup>
				<h:panelGroup id="pgtxtSponsor"
					rendered="#{pages$inheritanceFileBasicInfoTab.founding}">
					<h:inputText readonly="true"
						value="#{pages$inheritanceFileBasicInfoTab.sponsor.personFullName}" />
					<h:graphicImage title="#{msg['commons.search']}"
						rendered="#{pages$inheritanceFile.showSponsorSearch}"
						style="MARGIN: 1px 1px -5px;cursor:hand"
						onclick="showSponsorPopup();"
						url="../resources/images/app_icons/Search-tenant.png">
					</h:graphicImage>
				</h:panelGroup>
				

			</t:panelGrid>
		</t:panelGroup>
		<%--Column 3,4 Ends--%>
	</t:panelGrid>
	<%--</t:collapsiblePanel> --%>
	<t:div rendered="#{pages$inheritanceFile.filePersonBeneficiary}"
		style="width:97%;background-color:#CCCCCC; height:1px; cell-padding:0px; cell-spacing:0px"></t:div>
	<t:panelGrid border="0" width="97%" cellpadding="1" cellspacing="1"
		rendered="#{pages$inheritanceFile.filePersonBeneficiary}"
		id="controlsBasedOnFileType">
		<t:panelGroup>
			<t:panelGrid columns="4"
				columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.nurse']}" />
				<h:panelGroup>
					<h:inputText readonly="true"
						value="#{pages$inheritanceFileBasicInfoTab.filePersonNurse.personFullName}" />
					<h:graphicImage title="#{msg['commons.search']}"
						rendered="#{pages$inheritanceFile.showPersonNurseAndGuardianSearch}"
						style="MARGIN: 1px 1px -5px;cursor:hand"
						onclick="showNursePopup();"
						url="../resources/images/app_icons/Search-tenant.png">
					</h:graphicImage>
					<t:commandLink
						rendered="#{!empty pages$inheritanceFileBasicInfoTab.filePersonNurse.personId}"
						onclick="if (!confirm('#{msg['migrationProject.confirmMsg.sureToDelete']}')) return false;"
						action="#{pages$inheritanceFileBasicInfoTab.deleteNurseFromBeneDetail}">
						<h:graphicImage id="deleteNurseFP"
							title="#{msg['commons.delete']}" style="cursor:hand;"
							url="../resources/images/delete.gif" />&nbsp;
				</t:commandLink>
				</h:panelGroup>

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.relationship']}" />
				<h:selectOneMenu styleClass="SELECT_MENU"
					readonly="#{!pages$inheritanceFile.showPersonNurseAndGuardianSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePersonNurse.relationshipString}">
					<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
						itemValue="-1" />
					<f:selectItems value="#{pages$ApplicationBean.relationList}" />
				</h:selectOneMenu>

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.relationshipDesc']}" />
				<h:inputText
					readonly="#{!pages$inheritanceFile.showPersonNurseAndGuardianSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePersonNurse.relationshipDesc}" />
				<h:outputText />
				<h:outputText />

				<%-- Remove this section if you want to put the nurse and guradian detail in separat block and uncomment the CODE_GUARDIAN --%>

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.guardian']}" />
				<h:panelGroup>
					<h:inputText readonly="true"
						value="#{pages$inheritanceFileBasicInfoTab.filePersonGuardian.personFullName}" />
					<h:graphicImage title="#{msg['commons.search']}"
						rendered="#{pages$inheritanceFile.showGuardianSearch}"
						style="MARGIN: 1px 1px -5px;cursor:hand"
						onclick="showGuardianPopup();"
						url="../resources/images/app_icons/Search-tenant.png">
					</h:graphicImage>
					<t:commandLink
						rendered="#{!empty pages$inheritanceFileBasicInfoTab.filePersonGuardian.personId}"
						onclick="if (!confirm('#{msg['migrationProject.confirmMsg.sureToDelete']}')) return false;"
						action="#{pages$inheritanceFileBasicInfoTab.deleteGuardianFromBeneDetail}">
						<h:graphicImage id="deleteGuardianFP"
							title="#{msg['commons.delete']}" style="cursor:hand;"
							url="../resources/images/delete.gif" />&nbsp;
				</t:commandLink>
				</h:panelGroup>

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.relationship']}" />
				<h:selectOneMenu styleClass="SELECT_MENU"
					rendered="#{pages$inheritanceFile.showGuardianSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePersonGuardian.relationshipString}">
					<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
						itemValue="-1" />
					<f:selectItems value="#{pages$ApplicationBean.relationList}" />
				</h:selectOneMenu>

				<h:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.relationshipDesc']}" />
				<h:inputText rendered="#{pages$inheritanceFile.showGuardianSearch}"
					value="#{pages$inheritanceFileBasicInfoTab.filePersonGuardian.relationshipDesc}" />
				
				<h:panelGroup rendered="#{pages$inheritanceFileBasicInfoTab.underGuradian}">
				
				<h:outputLabel styleClass="mandatory" value="*" />
				<h:outputText 
					
					value="#{msg['blackList.reason']}" />
					
				</h:panelGroup>
				<h:inputTextarea 
					rendered="#{pages$inheritanceFileBasicInfoTab.underGuradian}"
					value="#{pages$inheritanceFileBasicInfoTab.filePerson.reason}" />
			
			</t:panelGrid>
			
			
		</t:panelGroup>
	</t:panelGrid>
</t:div>