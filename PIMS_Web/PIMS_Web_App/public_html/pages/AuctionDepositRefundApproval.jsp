<%-- 
  - Author:Munir Chagani
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Auction
  --%>

<%@taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="avanza-security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="javascript" type="text/javascript">
	function showPopup()
	{
	   var screen_width = screen.width;
	   var screen_height = screen.height;
	   //alert('hello world');
	   window.open('BidderAuctionRefund.jsf','_blank','width='+(screen_width-150)+',height='+(screen_height-200)+',left=0,top=40,scrollbars=yes,status=yes');		
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
	
	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
         	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
		</head>

		<body>
	
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
						        <td class="HEADER_TD">
										<h:outputLabel value="#{msg['AuctionDepositRefundApproval.Header']}" styleClass="HEADER_FONT"/>
									</td>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<!--Use this below only for desiging forms Please do not touch other sections -->


								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:499px;">
									<h:form>
										<table width="100%" border="0">
											<tr>
											<td>
												<div class="MARGIN"> 
													<table cellpadding="0" cellspacing="0" width="100%" border="0">
														<tr>
														<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
														<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
														</tr>
													</table>
													<div class="DETAIL_SECTION">
														<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" border="0">
															<tr>
																<td width="20%">
																	<h:outputLabel value="#{msg['auction.number']}:"></h:outputLabel>
																</td>
				
																<td>
																	<h:inputText id="auctionNumber" readonly="true"
																		value="#{pages$AuctionDepositRefundApproval.auctionView.auctionNumber}"
																		tabindex="-1" style="width: 186px; height: 16px"
																		maxlength="20"></h:inputText>
																</td>												
				
																<td width="20%">
																	<h:outputLabel value="#{msg['auction.title']}:"></h:outputLabel>
																</td>
				
																<td>
																	<h:inputText id="auctionTitle" readonly="true"
																		value="#{pages$AuctionDepositRefundApproval.auctionView.auctionTitle}"
																		style="width: 186px; height: 16px" maxlength="250"></h:inputText>
				
																</td>
															</tr>
															<tr>
																<td width="20%">
																	<h:panelGroup rendered="true">
																		<h:outputLabel value="#{msg['auction.venue']}:"></h:outputLabel>
																	</h:panelGroup>
																</td>
																<td>
																	<h:panelGroup rendered="true">
																		<h:inputText id="venueName" readonly="true"
																			value="#{pages$AuctionDepositRefundApproval.auctionView.auctionVenue.venueName}"
																			style="width: 186px; height: 16px" maxlength="50">
																		</h:inputText>
																	</h:panelGroup>
																</td>
				
																<td width="20%">
																	<h:panelGroup rendered="true">
																		<h:outputLabel value="#{msg['auction.date']}:"></h:outputLabel>
																	</h:panelGroup>
																</td>
																<td>
																	<h:panelGroup rendered="true">
																		<h:inputText id="auctionEndDate" readonly="true"
																			value="#{pages$AuctionDepositRefundApproval.auctionView.auctionDate}"
																			style="width: 186px; height: 16px">
																			<f:convertDateTime
																				pattern="#{pages$AuctionDepositRefundApproval.longDateFormat}" />
																		</h:inputText>
																	</h:panelGroup>
																</td>
															</tr>
															<tr>
																<td>
																	<h:panelGroup rendered="true">
																		<h:outputLabel value="#{msg['bidder.firstName']}"></h:outputLabel>
																	</h:panelGroup>
																</td>
																<td colspan="3">
																	<h:panelGroup rendered="true">
																		<h:inputText id="inputTextBidderNameSearch"  tabindex="0"
																			binding="#{pages$AuctionDepositRefundApproval.inputTextBidderNameSearch}"
																			readonly="#{!pages$AuctionDepositRefundApproval.showActionButtons}"
																			style="width: 186px; height: 16px" maxlength="50">
																		</h:inputText>
																	</h:panelGroup>
																</td>												
															</tr>
															<tr>
																<td colspan="4" class="BUTTON_TD">
																	<h:commandButton type="submit" 
																		value="#{msg['commons.search']}"
																		rendered="#{pages$AuctionDepositRefundApproval.showActionButtons}"														
																		action="#{pages$AuctionDepositRefundApproval.searchBidderAuctionReg}"
																		styleClass="BUTTON" tabindex="1" />
																</td>
															</tr>
														</table>
													</div>
												</div>
											</td>
											</tr>
											<tr>
												<td>
													<div class="MARGIN">
													<div class="imag">&nbsp;</div>
													<div class="contentDiv" width="100%">
														<t:dataTable width="100%" id="dtAuctionRequests"
															value="#{pages$AuctionDepositRefundApproval.bidderAuctionRegList}"
															binding="#{pages$AuctionDepositRefundApproval.dataTable}"
															rows="100" preserveDataModel="false" preserveSort="false"
															var="bidderAuctionRegView" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true">

															<t:column id="dbBidderFirst" sortable="true"
																width="10%">
																<f:facet name="header">
																	<t:outputText style="text-align:right"
																		value="#{msg['bidder.firstName']}" />
																</f:facet>
																<t:outputText styleClass="A_LEFT" value="#{bidderAuctionRegView.bidderView.firstName}" />
															</t:column>
														
															<t:column id="dbBidderPassport" sortable="true"
																width="10%">
																<f:facet name="header">
																	<t:outputText style="text-align:right"
																		value="#{msg['bidder.passport']}" />
																</f:facet>
																<t:outputText styleClass="A_LEFT" value="#{bidderAuctionRegView.bidderView.passportNumber}" />
															</t:column>
															<t:column id="dbBidderSocialSec" sortable="true"
																width="10%">
																<f:facet name="header">
																	<t:outputText style="text-align:right"
																		value="#{msg['bidder.SSN']}" />
																</f:facet>
																<t:outputText styleClass="A_LEFT" value="#{bidderAuctionRegView.bidderView.socialSecNumber}" />
															</t:column>

															<t:column id="dcIsAttendendedAuction" sortable="true"
																width="10%">
																<f:facet name="header">
																	<t:outputText style="text-align:right"
																		value="#{msg['AuctionDepositRefundApproval.DataTable.AttendedAuction']}" />
																</f:facet>
																<t:outputText value="#{bidderAuctionRegView.attendedDisplay}" />
															</t:column>

															<t:column id="dcDepositAmount" sortable="true"
																width="10%">
																<f:facet name="header">
																	<t:outputText style="text-align:right"
																		value="#{msg['AuctionDepositRefundApproval.DataTable.DepositAmount']}" />
																</f:facet>
																<t:outputText styleClass="A_RIGHT_NUM" value="#{bidderAuctionRegView.totalDepositAmount}" />
															</t:column>

															<t:column id="dcConfiscatedAmount" sortable="true"
																width="10%">
																<f:facet name="header">
																	<t:outputText style="text-align:right"
																		value="#{msg['AuctionDepositRefundApproval.DataTable.ConfiscatedAmount']}" />
																</f:facet>
																<t:outputText  styleClass="A_RIGHT_NUM"
																	value="#{bidderAuctionRegView.confiscatedAmount}" />
															</t:column>
															<t:column id="col7" width="10%" rendered="#{pages$AuctionDepositRefundApproval.showActionButtons}">
																<f:facet name="header">
																	<t:outputText style="text-align:right" value="#{msg['commons.edit']}" />
																</f:facet>
																<%--<h:commandLink actionListener="#{pages$AuctionDepositRefundApproval.editBidderAuctionRefund}"	value="Edit" />--%>
																<t:commandLink actionListener="#{pages$AuctionDepositRefundApproval.editBidderAuctionRefund}">
																	<h:graphicImage id="editIcon" title="#{msg['commons.edit']}" url="../resources/images/edit-icon.gif"/> 
																</t:commandLink>
															</t:column>
														</t:dataTable>
													</div>
													<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:100%;" >
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
														<td class="RECORD_NUM_TD">
															<div class="RECORD_NUM_BG">
																<table cellpadding="0" cellspacing="0">
																<tr><td class="RECORD_NUM_TD">
																<h:outputText value="#{msg['commons.recordsFound']}"/>
																</td><td class="RECORD_NUM_TD">
																<h:outputText value=" : "/>
																</td><td class="RECORD_NUM_TD">
																<h:outputText value="#{pages$AuctionDepositRefundApproval.recordSize}"/>
																</td></tr>
																</table>
															</div>
														</td>
														<td class="BUTTON_TD" style="width:50%">

															<t:dataScroller id="scroller" for="dtAuctionRequests" paginator="true"
																fastStep="1" paginatorMaxPages="2" immediate="false"
																styleClass="scroller" paginatorTableClass="paginator"
																renderFacetsIfSinglePage="true" 												
																paginatorTableStyle="grid_paginator" layout="singleTable" 
																paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																pageIndexVar="pageNumber">
																
																<f:facet name="first">																	
																	<t:graphicImage url="../resources/images/first_btn.gif" id="lblF"></t:graphicImage>
																</f:facet>							
																<f:facet name="fastrewind">
																	<t:graphicImage url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>
																</f:facet>							
																<f:facet name="fastforward">
																	<t:graphicImage url="../resources/images/next_btn.gif" id="lblFF"></t:graphicImage>
																</f:facet>							
																<f:facet name="last">
																	<t:graphicImage url="../resources/images/last_btn.gif" id="lblL"></t:graphicImage>
																</f:facet>
																<t:div styleClass="PAGE_NUM_BG">
																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td>	
																				<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																			</td>
																			<td>	
																				<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																			</td>
																		</tr>					
																	</table>										
																</t:div>
															</t:dataScroller>
															
														</td>
														</tr>
													</table>
												</t:div>
												</div>
												</td>
											</tr>
											<tr>
												<td>
													<h:messages>
														<h:outputText value="#{pages$AuctionDepositRefundApproval.errorMessages}"
															escape="false" styleClass="ERROR_FONT"/>
													</h:messages>
												</td>
											</tr>
											<tr>
												<td>
													<h:messages>
														<h:outputText value="#{pages$AuctionDepositRefundApproval.successMessages}"
															escape="false" styleClass="INFO_FONT"/>
													</h:messages>
												</td>
											</tr>
											<tr>
												<td class="BUTTON_TD">												
													<div class="MARGIN">
													<h:commandButton type="submit" 
													 value="#{msg['AuctionDepositRefundApproval.Buttons.Approve']}"
														action="#{pages$AuctionDepositRefundApproval.approveAuctionRequests}"
														rendered="#{pages$AuctionDepositRefundApproval.showActionButtons}"
														onclick="return confirm('Are you sure you want to approve refund');"
														styleClass="BUTTON" tabindex="7" />
													<h:commandButton type="submit" 
													 value="#{msg['AuctionDepositRefundApproval.Buttons.Save']}"
														action="#{pages$AuctionDepositRefundApproval.saveAuctionRequests}"
														rendered="#{pages$AuctionDepositRefundApproval.showActionButtons}"
														styleClass="BUTTON" tabindex="8" />														
													</div>
												</td>
											</tr>

										</table>


									</h:form>
									</div>
								</td>
							</tr>

						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</body>
	</html>
</f:view>
