<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript"> 

</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr width="100%">

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['commons.Beneficiary']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">

										<h:form id="beneficiaryPersonForm"
											enctype="multipart/form-data" style="WIDTH: 97.6%;">

											<div class="SCROLLABLE_SECTION"
												style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
												<div class="AUC_DET_PAD">
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>
																<h:outputText
																	value="#{pages$EndowmentPersonBeneficiaryBean.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText
																	value="#{pages$EndowmentPersonBeneficiaryBean.successMessages}"
																	escape="false" styleClass="INFO_FONT" />

															</td>
														</tr>
													</table>
													<div class="TAB_PANEL_INNER">
														<rich:tabPanel style="width: 100%;">

															<rich:tab label="#{msg['endowmentFile.tab.Endowments']}"
																action="#{pages$EndowmentPersonBeneficiaryBean.onEndowmentTab}">

																<t:div styleClass="contentDiv"
																	style="width:98%;margin-top: 5px;">
																	<t:dataTable id="prdc" rows="15" width="100%"
																		value="#{pages$EndowmentPersonBeneficiaryBean.dataListEndowemntFileAsso}"
																		binding="#{pages$EndowmentPersonBeneficiaryBean.dataTableFileAsso}"
																		preserveDataModel="false" preserveSort="false"
																		var="dataItem" rowClasses="row1,row2" rules="all"
																		renderedIfEmpty="true">

																		<t:column id="endNumber" sortable="true"
																			defaultSorted="true">
																			<f:facet name="header">
																				<t:outputText id="endNum"
																					value="#{msg['endowment.lbl.num']}" />
																			</f:facet>
																			<t:commandLink
																				onclick="openEndowmentManagePopup('#{dataItem.endowment.endowmentId}')"
																				value="#{dataItem.endowment.endowmentNum}"
																				style="white-space: normal;" styleClass="A_LEFT" />
																		</t:column>
																		<t:column id="endowName" sortable="true">
																			<f:facet name="header">
																				<t:outputText id="endname"
																					value="#{msg['endowment.lbl.name']}" />
																			</f:facet>
																			<t:outputText id="namee" styleClass="A_LEFT"
																				value="#{dataItem.endowment.endowmentName}" />
																		</t:column>

																		<t:column id="endowCostCenter" sortable="true">
																			<f:facet name="header">
																				<t:outputText id="endCostCenter"
																					value="#{msg['unit.costCenter']}" />
																			</f:facet>
																			<t:outputText id="costCentere" styleClass="A_LEFT"
																				value="#{dataItem.endowment.costCenter}" />
																		</t:column>
																		<t:column id="colenTypeName" sortable="true">
																			<f:facet name="header">
																				<t:outputText id="endTypeName"
																					value="#{msg['commons.typeCol']}" />
																			</f:facet>
																			<t:outputText id="endTypeNamee" styleClass="A_LEFT"
																				value="#{pages$EndowmentPersonBeneficiaryBean.englishLocale?
																													dataItem.endowment.assetType.assetTypeNameEn:
																	 												dataItem.endowment.assetType.assetTypeNameAr
																					}" />
																		</t:column>
																		<t:column id="colcategory" sortable="true">
																			<f:facet name="header">
																				<t:outputText id="endcategory"
																					value="#{msg['endowment.lbl.category']}" />
																			</f:facet>
																			<t:outputText id="endcategorye" styleClass="A_LEFT"
																				value="#{dataItem.endowmentCategory.categoryNameAr}" />
																		</t:column>
																		<t:column id="colpurpose" sortable="true">
																			<f:facet name="header">
																				<t:outputText id="endpurpose"
																					value="#{msg['endowment.lbl.purpose']}" />
																			</f:facet>
																			<t:outputText id="endpurposee" styleClass="A_LEFT"
																				value="#{dataItem.endowmentPurpose.purposeNameAr}" />
																		</t:column>
																		<t:column id="colManager" sortable="false">
																			<f:facet name="header">
																				<t:outputText id="endManager"
																					value="#{msg['contract.person.Manager']}" />
																			</f:facet>
																			<t:outputText id="endManagere" styleClass="A_LEFT"
																				rendered="#{dataItem.endowment.isAmafManager!= null && dataItem.endowment.isAmafManager=='1'}"
																				value="#{msg['inheritanceFile.inheritedAssetTab.managerAmaf']}" />
																			<t:commandLink
																				rendered="#{
																						dataItem.endowment.isAmafManager!= null && 
																						dataItem.endowment.isAmafManager=='0' &&
					            														dataItem.endowment.manager.personId != null 
					            														}"
																				onclick="javascript:showPersonReadOnlyPopup(#{dataItem.endowment.manager.personId });retrun "
																				value="#{dataItem.endowment.manager.fullName}"
																				style="white-space: normal;" styleClass="A_LEFT" />
																		</t:column>

																		<t:column id="colEndFileStatus" sortable="true">
																			<f:facet name="header">
																				<t:outputText id="endFileStatus"
																					value="#{msg['commons.status']}" />
																			</f:facet>
																			<t:outputText id="endFileStatuse" styleClass="A_LEFT"
																				value="#{pages$EndowmentPersonBeneficiaryBean.englishLocale?
																														 dataItem.status.dataDescEn:
																	 													 dataItem.status.dataDescAr
																	 		  		}" />
																		</t:column>
																		<t:column id="percentageBenef" sortable="true">
																			<f:facet name="header">
																				<t:outputText id="percentageValueBenef"
																					value="#{msg['commons.percentage']}" />
																			</f:facet>
																			<t:outputText
																				rendered="#{dataItem.isDeleted == '0' }"
																				value="#{dataItem.beneficiary.sharePercentageFileString}"
																				style="white-space: normal;" styleClass="A_LEFT" />
																		</t:column>
																		<t:column id="amountValueBenef" sortable="true">
																			<f:facet name="header">
																				<t:outputText id="amountStringBenef"
																					value="#{msg['commons.amount']}" />
																			</f:facet>
																			<t:outputText value="#{dataItem.beneficiary.amountString}"
																				rendered="#{dataItem.isDeleted == '0' }"
																				style="white-space: normal;" styleClass="A_LEFT" />
																		</t:column>
																		<t:column id="commentsBenef">
																			<f:facet name="header">
																				<t:outputText id="CommentsStringBenef"
																					value="#{msg['commons.comments']}" />
																			</f:facet>
																			<t:outputText value="#{dataItem.beneficiary.comments}"
																				rendered="#{dataItem.isDeleted == '0' }"
																				style="white-space: normal;" styleClass="A_LEFT" />
																		</t:column>
																		<t:column id="revenuePeriodBenef">
																			<f:facet name="header">
																				<t:outputText id="revenuePeriodStringBenef"
																					value="#{msg['endowmentFileBen.lbl.revenuePeriod']}" />
																			</f:facet>
																			<t:outputText value="#{dataItem.beneficiary.revenuePeriod}"
																				rendered="#{dataItem.isDeleted == '0' }"
																				style="white-space: normal;" styleClass="A_LEFT" />
																		</t:column>


																	</t:dataTable>
																</t:div>
															</rich:tab>
															<rich:tab label="#{msg['commons.commentsTabHeading']}">
																<%@ include file="notes/notes.jsp"%>
															</rich:tab>

															<rich:tab id="attachmentTab"
																label="#{msg['commons.attachmentTabHeading']}">
																<%@  include file="attachment/attachment.jsp"%>
															</rich:tab>

														</rich:tabPanel>
													</div>
												</div>
											</div>
										</h:form>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>