
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript" src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript"> 


		
	   function addBasicInfo()
	   {
	    var screen_width = 1024;
	    var screen_height = 450;
	    window.open('AddPerson.jsf?viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    }
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

		</head>

		<script language="javascript" type="text/javascript">
		function showPopup()
		{
		   var screen_width = 1024;
		   var screen_height = 470;
		   window.open('SearchPerson.jsf?persontype=TENANT&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
		    
		}
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		    
		   
		    if(hdnPersonType=='TENANT')
		    {
			 
			 
			 document.getElementById("changeTenantForm:hdnNewTenantId").value=personId;
			 document.getElementById("changeTenantForm:hdnNewTenantName").value=personName;
			 document.getElementById("changeTenantForm:txtNewTenantName").value=personName;
			 document.getElementById("changeTenantForm:hdnNewTenantCellNumber").value=cellNumber;
			 document.getElementById("changeTenantForm:hdnNewTenantPassportNumber").value=passportNumber;
			 document.getElementById("changeTenantForm:hdnNewTenantDesignation").value=designation;
	         }
	        else if(hdnPersonType=='APPLICANT')
	        {
	         document.getElementById("changeTenantForm:txtName").value=personName;
			 document.getElementById("changeTenantForm:hdnApplicantId").value=personId;
	         
	        }
	       document.forms[0].submit();
	}
	
    </script>


		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr >
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table  class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['changeTenantName.Heading']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
								<h:form id="changeTenantForm" style="width:97%" enctype="multipart/form-data">
									<div class="SCROLLABLE_SECTION" >

										
											<table border="0" class="layoutTable">

												<tr>

													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$ChangeTenantName.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$ChangeTenantName.successMessages}" />
														

														<h:inputHidden id="hdnRequestId"
															value="#{pages$ChangeTenantName.txtRequestId}" />
														<h:inputHidden id="hdnPageMode"
															value="#{pages$ChangeTenantName.pageMode}" />
														<h:inputHidden id="hdnContractId"
															value="#{pages$ChangeTenantName.txtContractId}" />
														<h:inputHidden id="txtTenantId"
															value="#{pages$ChangeTenantName.txtTenantId}" />
														<h:inputHidden id="hdnCreatedOn"
															value="#{pages$ChangeTenantName.createdOn}" />
														<h:inputHidden id="hdnCreatedBy"
															value="#{pages$ChangeTenantName.createdBy}" />
														<h:inputHidden id="hdnApplicantId"
															value="#{pages$ChangeTenantName.hdnApplicantId}" />


													</td>
												</tr>
											</table>

											<div class="MARGIN" style="width: 98%">
												<table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
										        </table>
										        <div class="TAB_PANEL_INNER">
												<rich:tabPanel style="width:100%;height:235px;" headerSpacing="0"   binding="#{pages$ChangeTenantName.tabPanel}">

														<!-- Application Details Tab - Start -->
														<rich:tab id="tabApplicantDetails" label="#{msg['commons.tab.applicationDetails']}" binding="#{pages$ChangeTenantName.tabApplicantDetails}">
															<%@ include file="ApplicationDetails.jsp"%>
														</rich:tab>
														<!-- Application Details Tab - End -->

														<!-- Contract Details Tab - Start -->
														<rich:tab id="tabContractDetails" label="#{msg['contract.Contract.Details']}" binding="#{pages$ChangeTenantName.tabContractDetails}">
														
															<t:div  style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid id="contractDetailsTable" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4"
																	columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">

																	<h:outputLabel styleClass="LABEL"  
																		value="#{msg['commons.Contract.Number']}:" />
																	<h:panelGroup>
																		<h:inputHidden id="hdncontractNum"
																			value="#{pages$ChangeTenantName.txtcontractNum}" />
																		<h:inputText id="txtcontractNum" styleClass="READONLY" readonly="true"
																			style="width:85%"
																			value="#{pages$ChangeTenantName.txtcontractNum}" />
																		<h:commandLink
																			action="#{pages$ChangeTenantName.btnContract_Click}">
																			<h:graphicImage style="padding-left:4px;"
																				title="#{msg['commons.view']}"
																				url="../resources/images/app_icons/Lease-contract.png" />
																		</h:commandLink>
																	</h:panelGroup>

																	<h:outputLabel value="#{msg['contract.contractType']}:" styleClass="LABEL"  />
																	<h:panelGroup>
																		<h:inputHidden id="hdnContractType"
																			value="#{pages$ChangeTenantName.txtContractType}" />
																		<h:inputText id="txtContractType" styleClass="READONLY" readonly="true"
																			style="width:85%"
																			value="#{pages$ChangeTenantName.txtContractType}" />
																	</h:panelGroup>

																	<h:outputLabel styleClass="LABEL"   value="#{msg['contract.date.Start']}:" />
																	<h:panelGroup>
																		<h:inputHidden id="hdnStartDate"
																			value="#{pages$ChangeTenantName.txtStartDate}" />
																		<h:inputText id="txtStartDate"  styleClass="READONLY" readonly="true"
																			style="width:85%"
																			value="#{pages$ChangeTenantName.txtStartDate}" />
																	</h:panelGroup>

																	<h:outputLabel styleClass="LABEL"   value="#{msg['contract.date.expiry']}:" />
																	<h:panelGroup>
																		<h:inputHidden id="hdnEndDate"
																			value="#{pages$ChangeTenantName.txtEndDate}" />
																		<h:inputText id="txtEndDate"  styleClass="READONLY" readonly="true"
																			style="width:85%"
																			value="#{pages$ChangeTenantName.txtEndDate}" />
																	</h:panelGroup>

																	<h:outputLabel styleClass="LABEL"  
																		value="#{msg['cancelContract.tab.unit.propertyname']}" />
																	<h:panelGroup>
																		<h:inputText id="txtPropertyName" readonly="true"  styleClass="READONLY" 
																			style="width:85%"
																			value="#{pages$ChangeTenantName.txtpropertyName}" />
																	</h:panelGroup>

																	<h:outputLabel styleClass="LABEL"   value="#{msg['property.type']}" />
																	<h:panelGroup>
																		<h:inputText id="txtPropertyType" readonly="true"  styleClass="READONLY" 
																			style="width:85%"
																			value="#{pages$ChangeTenantName.txtpropertyType}" />
																	</h:panelGroup>

																	<h:outputLabel styleClass="LABEL"  
																		value="#{msg['changeTenantName.currentTenant']}:" />
																	<h:panelGroup>
																		<h:inputHidden id="hdnTenantName"
																			value="#{pages$ChangeTenantName.txtTenantName}" />
																		<h:inputText id="txtTenantName" readonly="true"  styleClass="READONLY" 
																			style="width:85%"
																			value="#{pages$ChangeTenantName.txtTenantName}" />
																		<h:commandLink
																			action="#{pages$ChangeTenantName.btnTenant_Click}">
																			<h:graphicImage style="padding-left:4px;"
																				title="#{msg['commons.view']}"
																				url="../resources/images/app_icons/Tenant.png" />
																		</h:commandLink>
																	</h:panelGroup>

																	<h:outputLabel styleClass="LABEL"   value="#{msg['commons.status']}:" />
																	<h:panelGroup>
																		<h:inputHidden id="hdnStatus"
																			value="#{pages$ChangeTenantName.txtStatus}" />
																		<h:inputText id="txtStatus" readonly="true"  styleClass="READONLY" 
																			style="width:85%"
																			value="#{pages$ChangeTenantName.txtStatus}">
																		</h:inputText>
																	</h:panelGroup>

																	<h:outputLabel styleClass="LABEL"  
																		value="#{msg['contract.TotalContractValue']}:" />
																	<h:panelGroup>
																		<h:inputHidden id="hdnTotalContractValue"
																			value="#{pages$ChangeTenantName.txtTotalContractValue}" />
																		<h:inputText styleClass="A_RIGHT_NUM READONLY" id="txtTotalContractValue" readonly="true"   style="width:85%"
																			value="#{pages$ChangeTenantName.txtTotalContractValue}" >
																			<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
																			</h:inputText>
																	</h:panelGroup>

																	<h:outputLabel styleClass="LABEL"   value="#{msg['units.number']}:" />
																	<h:panelGroup>
																		<h:inputHidden id="hdnunitRefNum"
																			value="#{pages$ChangeTenantName.txtunitRefNum}" />
																		<h:inputText id="txtunitRefNum" readonly="true" styleClass="READONLY" 
																			style="width:85%"
																			value="#{pages$ChangeTenantName.txtunitRefNum}" />
																	</h:panelGroup>

																	<h:outputLabel styleClass="LABEL"   value="#{msg['unit.rentValue']}:" />
																	<h:panelGroup>
																		<h:inputHidden id="hdnUnitRentValue"
																			value="#{pages$ChangeTenantName.txtUnitRentValue}" />
																		<h:inputText styleClass="A_RIGHT_NUM READONLY"
																			id="txtUnitRentValue" readonly="true"  
																			style="width:85%"
																			value="#{pages$ChangeTenantName.txtUnitRentValue}" >
																			<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
																			</h:inputText>
																	</h:panelGroup>

																	<h:outputLabel styleClass="LABEL"  
																		value="#{msg['contract.property.Name']}:" />
																	<h:panelGroup>
																		<h:inputHidden id="hdnpropertyName"
																			value="#{pages$ChangeTenantName.txtpropertyName}" />
																		<h:inputText id="txtpropertyName" readonly="true" styleClass="READONLY" 
																			style="width:85%"
																			value="#{pages$ChangeTenantName.txtpropertyName}" />
																	</h:panelGroup>

																	<h:outputLabel styleClass="LABEL"   value="#{msg['unit.unitType']}:" />
																	<h:panelGroup>
																		<h:inputHidden id="hdnUnitType"
																			value="#{pages$ChangeTenantName.txtUnitType}" />
																		<h:inputText id="txtUnitType" readonly="true" styleClass="READONLY" 
																			style="width:85%"
																			value="#{pages$ChangeTenantName.txtUnitType}" />
																	</h:panelGroup>
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Contract Details Tab - End -->

														<!-- Change Tenant - Start -->
														<rich:tab id="tabNewTenant" label="#{msg['changeTenantName.tab.changeTenant']}" binding="#{pages$ChangeTenantName.tabNewTenant}">
															<h:panelGrid>
																<h:inputHidden id="hdnNewTenantId"
																	value="#{pages$ChangeTenantName.txtNewTenantId}" />
																<h:inputHidden id="hdnNewTenantName"
																	value="#{pages$ChangeTenantName.txtNewTenantName}" />
																<h:inputHidden id="hdnNewTenantCellNumber"
																	value="#{pages$ChangeTenantName.txtNewTenantCellNumber}" />
																<h:inputHidden id="hdnNewTenantPassportNumber"
																	value="#{pages$ChangeTenantName.txtNewTenantPassportNumber}" />
																<h:inputHidden id="hdnNewTenantDesignation"
																	value="#{pages$ChangeTenantName.txtNewTenantDesignation}" />
																<h:outputLabel>&nbsp;</h:outputLabel>
															</h:panelGrid>
															<t:div  style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid id="changeTenantGrid" cellpadding="1px" styleClass="TAB_DETAIL_SECTION_INNER"
																	width="100%" cellspacing="5px" columns="4"
																	columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">

																	<h:outputLabel styleClass="LABEL"  
																		value="#{msg['changeTenantName.tab.changeTenant']} :" />
																	<h:panelGroup>
																		<h:inputText id="txtNewTenantName" readonly="true"  style="width:83%"  styleClass="READONLY" value="#{pages$ChangeTenantName.txtNewTenantName}" />
																		<h:graphicImage  binding="#{pages$ChangeTenantName.imgSearchNewTenant}"  title="#{msg['commons.view']}" url="../resources/images/app_icons/Search-tenant.png" style ="MARGIN: 0px 0px -4px; CURSOR: hand;"  onclick="showPopup();"/>
																		<h:graphicImage  binding="#{pages$ChangeTenantName.imgViewNewTenant}"    title="#{msg['commons.view']}"    url="../resources/images/app_icons/Tenant.png"        style ="MARGIN: 0px 0px -4px; CURSOR: hand;"  onclick="javaScript:showPersonReadOnlyPopup('#{pages$ChangeTenantName.txtNewTenantId}');"/>
																		<h:commandLink binding="#{pages$ChangeTenantName.lnkAddNewTenant}" action="#{pages$ChangeTenantName.imgAddPerson_Click}">
																		  <h:graphicImage     title="#{msg['changeTenantName.msg.addPerson']}" url="../resources/images/app_icons/Add-New-Tenant.png" style ="MARGIN: 0px 0px -4px; CURSOR: hand;"  />
																	    </h:commandLink>
																	</h:panelGroup>

																	<h:outputLabel styleClass="LABEL"  
																		value="#{msg['notificationGenerate.cellNumber']} :" />
																	<h:inputText id="txtNewTenantCellNumber" style="width:83%" readonly="true" styleClass="READONLY" 
																		value="#{pages$ChangeTenantName.txtNewTenantCellNumber}" />

																	<h:outputLabel styleClass="LABEL"  
																		value="#{msg['commons.passport.number']} :" />
																	<h:inputText id="txtNewTenantPassportNumber"  style="width:83%" readonly="true" styleClass="READONLY" 
																		value="#{pages$ChangeTenantName.txtNewTenantPassportNumber}" />

																	<h:outputLabel styleClass="LABEL"  
																		value="#{msg['customer.designation']} :" />
																	<h:inputText id="txtNewTenantDesignation"  style="width:83%" readonly="true" styleClass="READONLY" 
																		value="#{pages$ChangeTenantName.txtNewTenantDesignation}" />
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Change Tenant Tab - End -->

														<!-- Payment Tab - Start -->
														<rich:tab id="tabPayments" label="#{msg['cancelContract.tab.payments']}" binding="#{pages$ChangeTenantName.tabPayments}">

															<t:div id="divtt" styleClass="contentDiv"
																style="width:98%">

																<t:dataTable id="ttbb" width="100%"
																	value="#{pages$ChangeTenantName.paymentSchedules }"
																	binding="#{pages$ChangeTenantName.dataTablePaymentSchedule}"
																	preserveDataModel="false" preserveSort="false"
																	var="paymentScheduleDataItem" rowClasses="row1,row2"
																	renderedIfEmpty="true">

																	<t:column id="col_PaymentNumber">
																		<f:facet name="header">
																			<t:outputText id="ac20"
																				value="#{msg['paymentSchedule.paymentNumber']}" />
																		</f:facet>
																		<t:outputText id="ac19" styleClass="A_LEFT"
																			rendered="#{paymentScheduleDataItem.paymentScheduleId > 0}"
																			value="#{paymentScheduleDataItem.paymentNumber}" />
																	</t:column>
																	
																	<t:column id="col_ReceiptNumber">
																		<f:facet name="header">
																			<t:outputText id="ac21"
																				value="#{msg['paymentSchedule.ReceiptNumber']}" />
																		</f:facet>
																		<t:outputText id="ac22" styleClass="A_LEFT"
																			rendered="#{paymentScheduleDataItem.paymentScheduleId > 0}"
																			value="#{paymentScheduleDataItem.paymentReceiptNumber}" />
																	</t:column>


																	<t:column id="colTypeEnt"
																		rendered="#{pages$ChangeTenantName.isEnglishLocale}">


																		<f:facet name="header">

																			<t:outputText id="ac17"
																				value="#{msg['paymentSchedule.paymentType']}" />

																		</f:facet>
																		<t:outputText id="ac16" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.typeEn}" />
																	</t:column>
																	<t:column id="colTypeArt"
																		rendered="#{pages$ChangeTenantName.isArabicLocale}">

																		<f:facet name="header">
																			<t:outputText id="ac15"
																				value="#{msg['paymentSchedule.paymentType']}" />
																		</f:facet>
																		<t:outputText id="ac14" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.typeAr}" />
																	</t:column>

																	<t:column id="colDesc">
																		<f:facet name="header">
																			<t:outputText id="descHead"
																				value="#{msg['renewContract.description']}" />
																		</f:facet>
																		<t:outputText id="descText" styleClass="A_LEFT"
																			title=""
																			value="#{paymentScheduleDataItem.description}" />
																	</t:column>

																	<t:column id="colDueOnt">
																		<f:facet name="header">
																			<t:outputText id="ac13"
																				value="#{msg['paymentSchedule.paymentDueOn']}" />
																		</f:facet>
																		<t:outputText id="ac12" styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentDueOn}">
																			<f:convertDateTime
																				pattern="#{pages$ChangeTenantName.dateFormatForDataTable}" />
																		</t:outputText>
																	</t:column>

																	<t:column id="colStatusEnt"
																		rendered="#{pages$ChangeTenantName.isEnglishLocale}">

																		<f:facet name="header">
																			<t:outputText id="ac11"
																				value="#{msg['commons.status']}" />
																		</f:facet>
																		<t:outputText id="ac10" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.statusEn}" />
																	</t:column>
																	<t:column id="colStatusArt"
																		rendered="#{pages$ChangeTenantName.isArabicLocale}">

																		<f:facet name="header">
																			<t:outputText id="ac9"
																				value="#{msg['commons.status']}" />
																		</f:facet>
																		<t:outputText id="ac8" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.statusAr}" />
																	</t:column>

																	<t:column id="colModeEnt"
																		rendered="#{pages$ChangeTenantName.isEnglishLocale}">

																		<f:facet name="header">
																			<t:outputText id="ac7"
																				value="#{msg['paymentSchedule.paymentMode']}" />
																		</f:facet>
																		<t:outputText id="ac6" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentModeEn}" />
																	</t:column>
																	<t:column id="colModeArt"
																		rendered="#{pages$ChangeTenantName.isArabicLocale}">

																		<f:facet name="header">
																			<t:outputText id="ac5"
																				value="#{msg['paymentSchedule.paymentMode']}" />
																		</f:facet>
																		<t:outputText id="ac4" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentModeAr}" />
																	</t:column>

																	<t:column id="colAmountt">
																		<f:facet name="header">
																			<t:outputText id="ac1"
																				value="#{msg['paymentSchedule.amount']}" />
																		</f:facet>
																		<t:outputText id="ac2" styleClass="A_RIGHT_NUM"
																			value="#{paymentScheduleDataItem.amount}" />
																	</t:column>
																    <t:column id="DelPaymentCol">
																				<f:facet name="header">
																					<t:outputText id="ac3"
																						value="#{msg['commons.action']}" />
																				</f:facet>
																				<h:commandLink action="#{pages$ChangeTenantName.btnPrintPaymentSchedule_Click}">
																                    <h:graphicImage id="printPayments" 
																					                title="#{msg['commons.print']}" 
																					                url="../resources/images/app_icons/print.gif"
																					                rendered="#{paymentScheduleDataItem.statusId == pages$ChangeTenantName.paymentScheduleRealizedId ||
																					                            paymentScheduleDataItem.statusId == pages$ChangeTenantName.paymentScheduleCollectedId
																					                            }"
																					/>
																                </h:commandLink>
																                <h:commandLink action="#{pages$ChangeTenantName.btnEditPaymentSchedule_Click}">
																				    <h:graphicImage id="editPayments" 
																									title="#{msg['commons.edit']}" 
																									url="../resources/images/edit-icon.gif"
																									rendered="#{pages$ChangeTenantName.isPageModeAdd ||
																									            pages$ChangeTenantName.isPageModeUpdate||
																									            pages$ChangeTenantName.isPageModeApproveReject||
																									            pages$ChangeTenantName.isPageModeUpdateApprove
																									            }"
																									/>
													                          	</h:commandLink>
													                          
													                          	<h:commandLink action="#{pages$ChangeTenantName.btnViewPaymentDetails_Click}">
																			  		<h:graphicImage id="viewPayments" 
																									title="#{msg['commons.group.permissions.view']}" 
																									url="../resources/images/detail-icon.gif"
																									/>
																				</h:commandLink> 
													                          
													                          
																				
																	</t:column>
																	
																</t:dataTable>
															</t:div>
															<t:div>
																<t:div id="cmdDiv" styleClass="BUTTON_TD"
																	style="padding:10px">

																	<pims:security screen="Pims.ChangeTenantName.Pay"
																		action="create">
																		<h:commandButton styleClass="BUTTON" value="#{msg['settlement.actions.collectpayment']}"
																			action="#{pages$ChangeTenantName.openReceivePaymentsPopUp}"
																			binding="#{pages$ChangeTenantName.btnCollectPayment}"
																			style="width: auto;"></h:commandButton>
																	</pims:security>
																</t:div>


																<t:panelGrid columns="4" cellpadding="1px"
																	cellspacing="5px">




																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Payment Tab - End -->
														<!-- Request History Tab - Start -->
														<rich:tab label="#{msg['commons.tab.requestHistory']}"
															action="#{pages$ChangeTenantName.tabRequestHistory_Click}">
															<%@ include file="../pages/requestTasks.jsp"%>
														</rich:tab>
														<!-- Request History Tab - End -->
                                                        
                                                        <!-- Attachments Tab - Start -->
														<rich:tab id="tabAttachments" label="#{msg['contract.attachmentTabHeader']}" action= "#{pages$ChangeTenantName.tabAttachmentsComments_Click}" binding="#{pages$ChangeTenantName.tabAttachments}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<!-- Attachments Tab - End -->
														<!-- Comments Tab - Start -->
                                                         <rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}" action= "#{pages$ChangeTenantName.tabAttachmentsComments_Click}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														<!-- Comments Tab - End -->

														

														
													</rich:tabPanel>
													</div>
												
											<table cellpadding="0" cellspacing="0"  style="width:100%;">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
													<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td colspan="6" class="BUTTON_TD">

															<pims:security screen="Pims.ChangeTenantName"
																action="create">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$ChangeTenantName.btnAdd_Click}"
																	binding="#{pages$ChangeTenantName.btnSave}"
																	style="width: 75px"></h:commandButton>
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.sendButton']}"
																	action="#{pages$ChangeTenantName.btnSend_Click}"
																	binding="#{pages$ChangeTenantName.btnSend}"
																	style="width: 75px"></h:commandButton>
															</pims:security>
															<pims:security screen="Pims.ChangeTenantName.Approval"
																action="create">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.approve']}"
																	action="#{pages$ChangeTenantName.btnApprove_Click}"
																	binding="#{pages$ChangeTenantName.btnApprove}"
																	style="width: 75px"></h:commandButton>
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.reject']}"
																	action="#{pages$ChangeTenantName.btnReject_Click}"
																	binding="#{pages$ChangeTenantName.btnReject}"
																	style="width: 75px"></h:commandButton>
															</pims:security>

															<pims:security screen="Pims.ChangeTenantName.Complete"
																action="create">
																<h:commandButton styleClass="BUTTON" value="#{msg['common.completeRequest']}"
																	action="#{pages$ChangeTenantName.btnComplete_Click}"
																	binding="#{pages$ChangeTenantName.btnComplete}"
																	style="width: 75px"></h:commandButton>
															</pims:security>
															<pims:security screen="Pims.ChangeTenantName.Print" action="create">
															<h:commandButton id="btnPrint" type="submit"  styleClass="BUTTON" 
                                           			                 action="#{pages$ChangeTenantName.btnPrint_Click}"
                                           			                 binding="#{pages$ChangeTenantName.btnPrint}"  
                                           			                 value="#{msg['commons.print']}" />
                                           			        </pims:security>
															<h:commandButton styleClass="BUTTON" type="submit"
																value="#{msg['commons.cancel']}"
																action="#{pages$ChangeTenantName.btnBack_Click}">
															</h:commandButton>
														</td>
													</tr>
												</table>
											</div>
								
								
									    
									</div>
								</h:form>

									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
           </div>
		</body>
	</html>
</f:view>
