<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	function sendDataToParent()
	{
		window.opener.onMessageFromEndowmentFileBen();
	  	//window.close();	  
	}	
	function closeWindow()
	{
		 window.close();
	}
	        
	function disableButtons(control)
	{
			
			var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
			
			control.nextSibling.nextSibling.onclick();
			
			return 
		
	}
	
	
	function   showPersonSearchPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('SearchPerson.jsf?viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
	
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{	
	     
		 document.getElementById("frm:hdnPersonId").value=personId;
		 document.getElementById("frm:hdnPersonName").value=personName;
		 document.getElementById("frm:beneficiaryName").value=personName;
		 if( document.getElementById("frm:masrafShared")!= null )
		 {
		  document.getElementById("frm:masrafShared").selectedValue="-1";
		 }
		 document.getElementById("frm:onPersonChanged").onclick();
	
	}
	function onMasrafChange(control)
	{	
	     
		 if (control.selectedValue!= "-1" )
		 {
			 document.getElementById("frm:hdnPersonId").value="";
			 document.getElementById("frm:hdnPersonName").value="";
			 document.getElementById("frm:beneficiaryName").value="";
			 document.getElementById("frm:onPersonChanged").onclick();
		 }
	
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">

				<tr width="100%">
					<td width="100%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['endowmentFileBen.lbl.title']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="frm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION" style="width: 95%;">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$endowmentFileBenPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$endowmentFileBenPopup.successMessages}"
																escape="false" styleClass="INFO_FONT" />
															<h:inputHidden id="hdnPersonId"
																value="#{pages$endowmentFileBenPopup.hdnPersonId}" />
															<h:inputHidden id="hdnPersonName"
																value="#{pages$endowmentFileBenPopup.hdnPersonName}" />
															<h:commandLink id="onPersonChanged"
																action="#{pages$endowmentFileBenPopup.onPersonChanged}" />

														</td>
													</tr>
												</table>
											</div>
											<div class="POPUP_DETAIL_SECTION">
												<table width="100%" cellspacing="1px" cellpadding="5px">
													<tr>
														<td width="10%">
															<h:outputLabel
																value="#{	
																		  pages$endowmentFileBenPopup.generation ||
																		 !pages$endowmentFileBenPopup.charity ?
																           msg['endowmentFileBen.lbl.beneficiary']:msg['commons.Masraf']
																         }" />
														</td>
														<td width="25%">

															<h:inputText id="beneficiaryName"
																rendered="#{ pages$endowmentFileBenPopup.generation || 
																            !pages$endowmentFileBenPopup.charity 
																            }"
																value="#{pages$endowmentFileBenPopup.hdnPersonName}"
																styleClass="READONLY" />
															<h:graphicImage
																rendered="#{ pages$endowmentFileBenPopup.generation || 
																            !pages$endowmentFileBenPopup.charity 
																            }"
																title="#{msg['endowmentFileBen.tooltip.beneficiary']}"
																style="MARGIN: 1px 1px -5px;cursor:hand;"
																onclick="showPersonSearchPopup();"
																url="../resources/images/app_icons/Search-tenant.png">
															</h:graphicImage>
															<h:selectOneMenu
																rendered="#{pages$endowmentFileBenPopup.charity }"
																value="#{pages$endowmentFileBenPopup.endowmentFileBen.masrafId}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$endowmentFileBenPopup.allMasarif}" />

															</h:selectOneMenu>

														</td>
														<td width="10%">
															<h:outputLabel styleClass="LABEL"
																rendered="#{!pages$endowmentFileBenPopup.charity && 
																!pages$endowmentFileBenPopup.generation}"
																value="#{msg['commons.Masraf']}" />
														</td>
														<td width="25%">
															<h:selectOneMenu id="masrafShared"
																onchange="javaScript:onMasrafChange(this);"
																rendered="#{!pages$endowmentFileBenPopup.charity && 
																            !pages$endowmentFileBenPopup.generation
																            }"
																value="#{pages$endowmentFileBenPopup.endowmentFileBen.masrafId}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$endowmentFileBenPopup.allMasarif}" />

															</h:selectOneMenu>
														</td>
													</tr>


													<tr>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.amount']}" />
														</td>
														<td>
															<h:inputText
																value="#{pages$endowmentFileBenPopup.endowmentFileBen.amountString}"
																onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)" />
														</td>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.percentage']}" />
														</td>
														<td>
															<h:inputText
																value="#{pages$endowmentFileBenPopup.endowmentFileBen.sharePercentageFileString}"
																onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)" />
														</td>
													</tr>
													<tr>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.from']}" />
														</td>

														<td>
															<h:selectOneMenu style="width:130px;"
																value="#{pages$endowmentFileBenPopup.endowmentFileBen.fromMonth}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$ApplicationBean.months}" />

															</h:selectOneMenu>
															<h:selectOneMenu style="width:60px;"
																value="#{pages$endowmentFileBenPopup.endowmentFileBen.fromYear}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$ApplicationBean.years}" />

															</h:selectOneMenu>

														</td>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.to']}" />
														</td>

														<td>
															<h:selectOneMenu style="width:130px;"
																value="#{pages$endowmentFileBenPopup.endowmentFileBen.toMonth}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$ApplicationBean.months}" />

															</h:selectOneMenu>
															<h:selectOneMenu style="width:60px;"
																value="#{pages$endowmentFileBenPopup.endowmentFileBen.toYear}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$ApplicationBean.years}" />

															</h:selectOneMenu>

														</td>

													</tr>
													<tr>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.comments']}" />
														</td>
														<td colspan="3">
															<h:inputTextarea
																value="#{pages$endowmentFileBenPopup.endowmentFileBen.comments}"
																style="width: 540px;" />
														</td>
													</tr>
												</table>
											</div>
											<div class="BUTTON_TD MARGIN" style="width: 95%;">
												<h:commandButton id="idAddBtn" value="#{msg['commons.Add']}"
													styleClass="BUTTON"
													action="#{pages$endowmentFileBenPopup.onAdd}">
												</h:commandButton>

											</div>




											<t:div styleClass="contentDiv"
												style="width:98%;margin-top: 5px;">
												<t:dataTable id="prdc" rows="15" width="100%"
													value="#{pages$endowmentFileBenPopup.dataList}"
													binding="#{pages$endowmentFileBenPopup.dataTable}"
													preserveDataModel="false" preserveSort="false"
													var="dataItem" rowClasses="row1,row2" rules="all"
													renderedIfEmpty="true">

													<t:column id="personName" sortable="true">
														<f:facet name="header">
															<t:outputText id="personNameString"
																value="#{msg['endowmentFileBen.lbl.beneficiary']}" />
														</f:facet>
														<t:commandLink rendered="#{dataItem.isDeleted == '0' }"
															actionListener="#{pages$endowmentFileBenPopup.openEndowmentPopUp}"
															value="#{dataItem.beneficiaryName}"
															style="white-space: normal;" styleClass="A_LEFT" />
													</t:column>
													<t:column id="masraf" sortable="true">
														<f:facet name="header">
															<t:outputText id="masrafName"
																value="#{msg['commons.Masraf']}" />
														</f:facet>
														<t:commandLink rendered="#{dataItem.isDeleted == '0' }"
															actionListener="#{pages$endowmentFileBenPopup.openEndowmentPopUp}"
															value="#{dataItem.masrafName}"
															style="white-space: normal;" styleClass="A_LEFT" />
													</t:column>
													<t:column id="percentage" sortable="true">
														<f:facet name="header">
															<t:outputText id="percentageValue"
																value="#{msg['commons.percentage']}" />
														</f:facet>
														<t:outputText rendered="#{dataItem.isDeleted == '0' }"
															value="#{dataItem.sharePercentageFileString}"
															style="white-space: normal;" styleClass="A_LEFT" />
													</t:column>
													<t:column id="amountValue" sortable="true">
														<f:facet name="header">
															<t:outputText id="amountString"
																value="#{msg['commons.amount']}" />
														</f:facet>
														<t:outputText value="#{dataItem.amountString}"
															rendered="#{dataItem.isDeleted == '0' }"
															style="white-space: normal;" styleClass="A_LEFT" />
													</t:column>
													<t:column id="comments">
														<f:facet name="header">
															<t:outputText id="CommentsString"
																value="#{msg['commons.comments']}" />
														</f:facet>
														<t:outputText value="#{dataItem.comments}"
															rendered="#{dataItem.isDeleted == '0' }"
															style="white-space: normal;" styleClass="A_LEFT" />
													</t:column>
													<t:column id="revenuePeriod">
														<f:facet name="header">
															<t:outputText id="revenuePeriodString"
																value="#{msg['endowmentFileBen.lbl.revenuePeriod']}" />
														</f:facet>
														<t:outputText value="#{dataItem.revenuePeriod}"
															rendered="#{dataItem.isDeleted == '0' }"
															style="white-space: normal;" styleClass="A_LEFT" />
													</t:column>
													<t:column id="colAction" sortable="false">
														<f:facet name="header">
															<t:outputText id="hdction"
																value="#{msg['commons.action']}" />
														</f:facet>

														<h:commandLink rendered="#{dataItem.isDeleted == '0' }"
															action="#{pages$endowmentFileBenPopup.onEdit}">
															<h:graphicImage id="editIcon"
																title="#{msg['commons.edit']}"
																url="../resources/images/edit-icon.gif" width="18px;" />
														</h:commandLink>

														<h:commandLink rendered="#{dataItem.isDeleted == '0' }"
															onclick="if (!confirm('#{msg['endowmentFileBen.confirm.delete']}')) return false;"
															action="#{pages$endowmentFileBenPopup.onDelete}">
															<h:graphicImage id="deleteIcon"
																title="#{msg['commons.delete']}"
																url="../resources/images/delete.gif" width="18px;" />
														</h:commandLink>


													</t:column>
												</t:dataTable>
											</t:div>
											<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
												style="width:99%;">
												<t:panelGrid columns="2" border="0" width="100%"
													cellpadding="1" cellspacing="1">
													<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
														<t:div styleClass="JUG_NUM_REC_ATT">
															<h:outputText value="#{msg['commons.records']}" />
															<h:outputText value=" : " />
															<h:outputText
																value="#{pages$endowmentFileBenPopup.totalRows}" />
														</t:div>
													</t:panelGroup>
													<t:panelGroup>
														<t:dataScroller id="shareScrollers" for="prdc"
															paginator="true" fastStep="1" immediate="false"
															paginatorTableClass="paginator"
															renderFacetsIfSinglePage="true" paginatorMaxPages="5"
															paginatorTableStyle="grid_paginator" layout="singleTable"
															paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
															paginatorActiveColumnStyle="font-weight:bold;"
															paginatorRenderLinkForActive="false"
															pageIndexVar="pageNumber" styleClass="JUG_SCROLLER">
															<f:facet name="first">
																<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
															</f:facet>
															<f:facet name="fastrewind">
																<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
															</f:facet>
															<f:facet name="fastforward">
																<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
															</f:facet>
															<f:facet name="last">
																<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
															</f:facet>

															<t:div>
																<table cellpadding="0" cellspacing="0">
																	<tr>
																		<td>
																			<h:outputText styleClass="PAGE_NUM"
																				style="font-size:9;color:black;"
																				value="#{msg['commons.totalPercentage']}:" />
																		</td>
																		<td>
																			<h:outputText styleClass="PAGE_NUM"
																				style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:9;font-weight:bold;color:black"
																				value="#{pages$endowmentFileBenPopup.totalPercentage}" />
																		</td>
																	</tr>
																</table>
															</t:div>
														</t:dataScroller>


													</t:panelGroup>
												</t:panelGrid>
											</t:div>


										</div>

									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>
		</body>
	</html>
</f:view>