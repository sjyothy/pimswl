<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Popup Used for Adding/Editing region
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

    function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:auto;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

</head>
	<body class="BODY_STYLE" >
	<div class="containerDiv">
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<td width="83%" height="200px" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{pages$addRegion.heading}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
									<div class="SCROLLABLE_SECTION"
										style="height: 70%; width: 100%; #height: 190px; #width: 640px;">
										<h:form id="searchFrm" style="WIDTH: 98%;#WIDTH: 100%;">
											<t:div styleClass="MESSAGE">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$addRegion.infoMessage}"
																escape="false" styleClass="INFO_FONT" />
															<h:outputText
																value="#{pages$addRegion.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</t:div>
											<div class="MARGIN">
													<table cellpadding="1px" cellspacing="2px">
														<tr>

															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{pages$addRegion.isCountryMode}"/><h:outputLabel styleClass="LABEL" rendered="#{pages$addRegion.isCountryMode}"   
																	value="#{msg['regionDetails.countryEn']}:"></h:outputLabel>
															</td>

															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:inputText id="countryEn" styleClass="INPUT_TEXT" binding="#{pages$addRegion.countryEnText}" rendered="#{pages$addRegion.isCountryMode}" value="#{pages$addRegion.region.countryEn}"></h:inputText>

															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{pages$addRegion.isCountryMode}"/><h:outputLabel styleClass="LABEL" rendered="#{pages$addRegion.isCountryMode}"
																	value="#{msg['regionDetails.countryAr']}:"></h:outputLabel>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:inputText id="countryAr" 
																	styleClass="INPUT_TEXT" rendered="#{pages$addRegion.isCountryMode}"
																	value="#{pages$addRegion.region.countryAr}" binding="#{pages$addRegion.countryArText}"
																	 ></h:inputText>
															</td>
														</tr>
														<tr>

															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{pages$addRegion.isCityMode}"/><h:outputLabel styleClass="LABEL" rendered="#{pages$addRegion.isCityMode}"
																	value="#{msg['regionDetails.cityEn']}:"></h:outputLabel>
															</td>

															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:inputText id="cityEn"
																	styleClass="INPUT_TEXT" rendered="#{pages$addRegion.isCityMode}"
																	value="#{pages$addRegion.region.cityEn}" binding="#{pages$addRegion.cityEnText}"
																	 ></h:inputText>

															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{pages$addRegion.isCityMode}"/><h:outputLabel styleClass="LABEL" rendered="#{pages$addRegion.isCityMode}"
																	value="#{msg['regionDetails.cityAr']}:"></h:outputLabel>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:inputText id="cityAr"
																	styleClass="INPUT_TEXT" rendered="#{pages$addRegion.isCityMode}"
																	value="#{pages$addRegion.region.cityAr}" binding="#{pages$addRegion.cityArText}"
																	 ></h:inputText>
															</td>
														</tr>
														<tr>

															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{pages$addRegion.isAreaMode}"/><h:outputLabel styleClass="LABEL" rendered="#{pages$addRegion.isAreaMode}"
																	value="#{msg['regionDetails.areaEn']}:"></h:outputLabel>
															</td>

															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:inputText id="areaEn"
																	styleClass="INPUT_TEXT" rendered="#{pages$addRegion.isAreaMode}"
																	value="#{pages$addRegion.region.areaEn}" binding="#{pages$addRegion.areaEnText}"
																	></h:inputText>

															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{pages$addRegion.isAreaMode}"/><h:outputLabel styleClass="LABEL" rendered="#{pages$addRegion.isAreaMode}"
																	value="#{msg['regionDetails.areaAr']}:"></h:outputLabel>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:inputText id="areaAr"
																	styleClass="INPUT_TEXT" rendered="#{pages$addRegion.isAreaMode}"
																	value="#{pages$addRegion.region.areaAr}" binding="#{pages$addRegion.areaArText}"></h:inputText> 
															</td>
														</tr>
														<tr>
                                                           	
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{pages$addRegion.isCountryMode}"/><h:outputLabel styleClass="LABEL" rendered="#{pages$addRegion.isCountryMode}"
																	value="#{msg['regionDetails.GRPCountry']}:"></h:outputLabel>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:selectOneMenu id="grpCountry"  rendered="#{pages$addRegion.isCountryMode}" 
																 binding="#{pages$addRegion.grpCountryMenu}" 
																 value="#{pages$addRegion.region.grpCountryCode}" 
																 style="width: 192px;" required="true">
																	<f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems value="#{pages$ApplicationBean.xxAmafCountry}" />
																</h:selectOneMenu>
															</td>
														</tr>
														<tr>
															<td class="JUG_BUTTON_TD_AUC_POP BUTTON_TD" colspan="4">
																<table cellpadding="1px" cellspacing="1px">
																	<tr>
																		<td colspan="2">
																			<h:commandButton styleClass="BUTTON"
																				rendered="#{!pages$addRegion.showAddMore}"
																				value="#{msg['commons.saveButton']}"
																				action="#{pages$addRegion.saveRegion}"
																				tabindex="7">
																			</h:commandButton>
																			<h:commandButton styleClass="BUTTON"
																				rendered="#{pages$addRegion.showAddMore}"
																				value="#{msg['commons.addMore']}"
																				action="#{pages$addRegion.addMore}"
																				tabindex="7">
																			</h:commandButton>
																			<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.closeButton']}"
																				onclick="javascript:closeWindowSubmit();"
																				tabindex="8">
																			</h:commandButton>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</div>
										  </div>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
</html>
</f:view>