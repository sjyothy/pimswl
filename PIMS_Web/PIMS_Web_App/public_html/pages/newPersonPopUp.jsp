<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- TAG LIBS START --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%-- TAG LIBS END --%>

<%-- JAVA SCRIPT FUNCTIONS START --%>
<script type="text/javascript">
function alertTheCallerBean()
{
	window.opener.document.forms[0].submit();
    window.close();
}
</script>
<%-- JAVA SCRIPT FUNCTIONS END --%>

<%-- JSF VIEW START --%>
<f:view>
	<%-- LOADING RESOURCES START --%>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<%-- LOADING RESOURCES END --%>
	<%-- HTML START --%> 
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<%-- HEAD START --%>
		<head>
			<title><h:outputText value="#{msg['commons.page.title']}" />
			</title>
			<meta http-equiv="content-type"
				content="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is a template page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_dtree}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_dtree}"/>"></script>
		</head>
		<%-- HEAD END --%>

		<%-- BODY START --%>
		<body>
			<%-- FORM START --%>
			<h:form id="addPersonForm">
				<%-- MAIN CONTAINER START --%>
				<table class="MAIN_CONTAINER" cellpadding="0" cellspacing="0"
					align="center">
					<%-- CONTENT START --%>
					<tr class="CONTENT_ROW">
						<td class="CONTENT_COL">
							<table cellpadding="0" cellspacing="0" border="0" width="100%"
								height="100%">
								<tr class="LEFT_MENU_PAGE_CONTENT_ROW">
									<td class="PAGE_CONTENT_COL">
										<table border="0" width="100%" height="100%">
											<tr>
												<td>
													<h:outputLabel value="#{msg['addPerson.Header']}"
														styleClass="HEADER_FONT" />
												</td>
											</tr>

											<tr>
												<td class="HEADER_SEPARATOR_COL" />
											</tr>

											<tr>
												<td height="5px" />
											</tr>

											<tr>
												<td height="5px" />
											</tr>

											<tr>
												<td>
													<h:outputText id="successmsg" escape="false"
														styleClass="INFO_FONT" />
													<h:outputText id="errormsg" escape="false"
														value="#{pages$newPersonPopUp.errorMessages}"
														styleClass="ERROR_FONT" />
												</td>
											</tr>
											<tr>
												<td>
													<TABLE width="100%" id="addPersonMainTable">
														<tr>
															<td colspan="4">
																<h:selectOneMenu onchange="submit();" valueChangeListener="#{pages$newPersonPopUp.changeTenantType}"
																	value="#{pages$newPersonPopUp.tenantType}">
																	<f:selectItem
																		itemLabel="#{msg['tenants.tpye.individual']}"
																		itemValue="0" />
																	<f:selectItem
																		itemLabel="#{msg['tenants.tpye.company']}"
																		itemValue="1" />
																</h:selectOneMenu>
															</td>
														</tr>
														
														<t:panelGrid columns="4" id="individualDiv" binding="#{pages$newPersonPopUp.individualPanel}">
																	
																	<h:panelGroup>
																		<h:outputLabel rendered="#{!pages$tenantDetails.isReadOnly}" styleClass="mandatory" value="*"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['bidder.bFirstName']}">
																		</h:outputLabel>
																	</h:panelGroup>
																	
																	<h:inputText styleClass="INPUT" value="#{pages$newPersonPopUp.newPerson.firstName}">
																	</h:inputText>
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['customer.middlename']}">
																	</h:outputLabel>
																	<h:inputText id="idMiddleName" styleClass="INPUT" value="#{pages$newPersonPopUp.newPerson.middleName}">
																	</h:inputText>
																	
																	<h:panelGroup>
																		<h:outputLabel rendered="#{!pages$tenantDetails.isReadOnly}" styleClass="mandatory" value="*"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['customer.lastname']}">
																	</h:outputLabel>
																	</h:panelGroup>
																	<h:inputText styleClass="INPUT" value="#{pages$newPersonPopUp.newPerson.lastName}">
																	</h:inputText>
																	
																	<h:panelGroup>
																		<h:outputLabel rendered="#{!pages$tenantDetails.isReadOnly}" styleClass="mandatory" value="*"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['applicationDetails.cell']}">
																	</h:outputLabel>
																	</h:panelGroup>
																	<h:inputText styleClass="INPUT" value="#{pages$newPersonPopUp.newPerson.cellNumber}">
																	</h:inputText>
																	
																	<h:panelGroup>
																		<h:outputLabel rendered="#{!pages$tenantDetails.isReadOnly}" styleClass="mandatory" value="*"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['applicationDetails.email']}">
																	</h:outputLabel>
																	</h:panelGroup>
																	<h:inputText styleClass="INPUT" value="#{pages$newPersonPopUp.newPersonCInfo.email}">
																	</h:inputText>

																	<h:outputLabel styleClass="LABEL" value="#{msg['bidder.passport']}">
																	</h:outputLabel>
																	<h:inputText styleClass="INPUT" value="#{pages$newPersonPopUp.newPerson.passportNumber}">
																	</h:inputText>
														</t:panelGrid>
														
														
														
														
														<t:panelGrid columns="4"  id="companyDiv" binding="#{pages$newPersonPopUp.companyPanel}">
																	
																	<h:panelGroup>
																		<h:outputLabel rendered="#{!pages$tenantDetails.isReadOnly}" styleClass="mandatory" value="*"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['customer.companyName']}">
																	</h:outputLabel>
																	</h:panelGroup>
																	<h:inputText styleClass="INPUT" value="#{pages$newPersonPopUp.newPerson.companyName}">
																	</h:inputText>
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['tenants.licenseNumber']}">
																	</h:outputLabel>
																	<h:inputText styleClass="INPUT" value="#{pages$newPersonPopUp.newPerson.licenseNumber}">
																	</h:inputText>
																	
																	<h:panelGroup>
																		<h:outputLabel rendered="#{!pages$tenantDetails.isReadOnly}" styleClass="mandatory" value="*"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['person.contactNumber']}">
																	</h:outputLabel>
																	</h:panelGroup>
																	<h:inputText styleClass="INPUT" value="#{pages$newPersonPopUp.newPerson.cellNumber}">
																	</h:inputText>
																	
																	<h:panelGroup>
																		<h:outputLabel rendered="#{!pages$tenantDetails.isReadOnly}" styleClass="mandatory" value="*"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['applicationDetails.email']}">
																	</h:outputLabel>
																	</h:panelGroup>
																	<h:inputText styleClass="INPUT" value="#{pages$newPersonPopUp.newPersonCInfo.email}">
																	</h:inputText>
														</t:panelGrid>
														<tr>
															<td>
																<h:commandButton styleClass="BUTTON"
																	action="#{pages$newPersonPopUp.sendPersonToCallerBean}"
																	value="#{msg['cancelContract.submit']}">
																</h:commandButton>
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['bouncedChequesList.close']}"
																	onclick="window.close();">
																</h:commandButton>
															</td>
														</tr>
													</TABLE>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<%-- CONTENT END --%>
				</table>
				<%-- MAIN CONTAINER END --%>
			</h:form>
			<%-- FORM END --%>
		</body>
		<%-- BODY END --%>
	</html>
	<%-- HTML END --%>
</f:view>
<%-- JSF VIEW END --%>
