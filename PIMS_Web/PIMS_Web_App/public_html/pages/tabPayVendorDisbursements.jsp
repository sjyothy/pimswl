

<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
	<t:dataTable id="disburseId" rows="15" width="100%"
		value="#{pages$PayVendorRequest.disbursementDetails}"
		binding="#{pages$PayVendorRequest.disbursementTable}"
		preserveDataModel="false" preserveSort="false" var="unitDataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true">



		<t:column id="checkbox" style="width:2%;"
			rendered="#{pages$PayVendorRequest.isNewStatus}">

			<f:facet name="header">
				<h:selectBooleanCheckbox id="markUnMarkAll"
					onclick="onChkChangeStart();"
					value="#{pages$PayVendorRequest.markUnMarkAll}" />
			</f:facet>
			<h:selectBooleanCheckbox id="idbox" immediate="true"
				value="#{unitDataItem.selected}">
			</h:selectBooleanCheckbox>
		</t:column>

		<t:column id="idreqNumbers" sortable="true"
			style="width:10%;white-space: normal;">
			<f:facet name="header">
				<t:outputText id="hdreqNumber"
					value="#{msg['commons.requestNumber']}" />
			</f:facet>
			<div style="width: 80%; white-space: normal;">

				<t:outputText id="hdreqNumbers" styleClass="A_LEFT"
					style="white-space: normal;"
					value="#{unitDataItem.reqNumber} / #{unitDataItem.refNum}" />
			</div>
		</t:column>

		<t:column id="idfileNum" sortable="true" style="white-space: normal;">
			<f:facet name="header">
				<t:outputText id="hdfileNum"
					value="#{msg['SearchBeneficiary.fileNumber']}" />
			</f:facet>

			<t:commandLink action="#{pages$PayVendorRequest.onOpenFilePopup}"
				value="#{unitDataItem.fileNumber}" style="white-space: normal;"
				styleClass="A_LEFT" />
		</t:column>



		<t:column id="idPmtType" sortable="true" style="white-space: normal;">
			<f:facet name="header">
				<t:outputText id="hdPmtType"
					value="#{msg['mems.normaldisb.label.paymentType']}" />
			</f:facet>
			<t:outputText id="hdInsPmtType" styleClass="A_LEFT"
				style="white-space: normal;"
				value="#{pages$PayVendorRequest.isEnglishLocale?unitDataItem.paymentTypeEn:unitDataItem.paymentTypeAr}" />
		</t:column>
		<t:column id="idCurStatus" sortable="true"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText id="hdCurStatus" value="#{msg['commons.status']}" />
			</f:facet>
			<t:outputText id="hdInsCurStatus" styleClass="A_LEFT"
				style="white-space: normal;"
				value="#{pages$PayVendorRequest.isEnglishLocale?unitDataItem.statusEn:unitDataItem.statusAr}" />
		</t:column>
		<t:column id="beneficiary" sortable="true"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText id="hdBenef"
					value="#{msg['mems.normaldisb.label.beneficiary']}" />
			</f:facet>
			<t:commandLink
				action="#{pages$PayVendorRequest.openManageBeneficiaryPopUp}"
				value="#{unitDataItem.beneficiariesName}"
				style="white-space: normal;" styleClass="A_LEFT" />
		</t:column>
		<t:column id="amnt" sortable="true" style="white-space: normal;">
			<f:facet name="header">
				<t:outputText id="hdAmnt" value="#{msg['commons.amount']}" />
			</f:facet>
			<t:outputText id="amountt" styleClass="A_LEFT"
				style="white-space: normal;" value="#{unitDataItem.amount}" />
		</t:column>
		<t:column id="pmtSrc" sortable="true" style="white-space: normal;">
			<f:facet name="header">
				<t:outputText id="hdPmtSrc"
					value="#{msg['mems.normaldisb.label.paymentSrc']}" />
			</f:facet>
			<t:outputText id="paymentSrc" styleClass="A_LEFT"
				style="white-space: normal;"
				value="#{pages$PayVendorRequest.isEnglishLocale?unitDataItem.srcTypeEn:unitDataItem.srcTypeAr}" />
		</t:column>
		<t:column id="desc" sortable="true" width="6%"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText id="hdTo" value="#{msg['commons.description']}" />
			</f:facet>
			<div style="width: 70%; white-space: normal;">
				<t:outputText id="p_w" styleClass="A_LEFT"
					style="white-space: normal;" value="#{unitDataItem.description}" />
			</div>
		</t:column>
		<t:column id="idReason" sortable="true" width="9%"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText id="hdReason" value="#{msg['commons.reasons']}" />
			</f:facet>
			<div style="width: 70%; white-space: normal;">
				<t:outputText id="idInsReason" styleClass="A_LEFT"
					style="white-space: normal;"
					value="#{pages$PayVendorRequest.isEnglishLocale?unitDataItem.reasonEn:unitDataItem.reasonAr}" />
			</div>
		</t:column>
		<t:column id="idItems" sortable="true" width="9%"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText id="hdItems"
					value="#{msg['mems.normaldisb.label.items']}" />
			</f:facet>
			<div style="width: 70%; white-space: normal;">
				<t:outputText id="hdInsItems" styleClass="A_LEFT"
					style="white-space: normal;"
					value="#{pages$PayVendorRequest.isEnglishLocale?unitDataItem.itemEn:unitDataItem.itemAr}" />
			</div>
		</t:column>

		<t:column id="idDisbSrcName" sortable="true" width="5%"
			style="white-space: normal;">
			<f:facet name="header">
				<t:outputText id="hdDisbSrcName"
					value="#{msg['mems.normaldisb.label.disbSrc']}" />
			</f:facet>
			<div style="width: 70%; white-space: normal;">
				<t:outputText id="hdInsDisbSrcName" styleClass="A_LEFT"
					style="white-space: normal;"
					value="#{pages$PayVendorRequest.isEnglishLocale?unitDataItem.disburseSrcNameEn:unitDataItem.disburseSrcNameAr}" />
			</div>
		</t:column>
		<t:column id="idActions" sortable="true"
			style="white-space: normal;width:7%;">
			<f:facet name="header">
				<t:outputText id="hdActions" value="#{msg['commons.action']}" />
			</f:facet>
			<div style="width: 70%; white-space: normal;">
				<h:commandLink action="#{pages$PayVendorRequest.editPaymentDetails}"
					rendered="#{pages$PayVendorRequest.isApprovalRequired ||
							pages$PayVendorRequest.isReviewed || 
							pages$PayVendorRequest.isFinanceRejected
						   }">
					<h:graphicImage id="pymtIcon"
						title="#{msg['mems.payment.request.label.paymentDetails']}"
						url="../resources/images/app_icons/Pay-Fine.png" width="18px;" />
				</h:commandLink>

				<h:commandLink id="lnkBlockingDetails"
					action="#{pages$PayVendorRequest.onOpenBlockingDetailsPopup}">
					<h:graphicImage id="openBlockingDetailsPopup"
						style="margin-right:5px;"
						title="#{msg['blocking.lbl.blockingDetails']}"
						url="../resources/images/app_icons/manage_tender.png" />
				</h:commandLink>
				<h:commandLink id="lnkDisbDetails"
					action="#{pages$PayVendorRequest.onOpenBeneficiaryDisbursementDetailsPopup}">
					<h:graphicImage id="onOpenBeneficiaryDisbursementDetailsPopup"
						style="margin-right:5px;"
						title="#{msg['commons.disbursementDetails']}"
						url="../resources/images/app_icons/manage_tender.png" />
				</h:commandLink>



				<h:commandLink
					action="#{pages$PayVendorRequest.onRemoveDisbursement}"
					rendered="#{pages$PayVendorRequest.isApprovalRequired ||
							pages$PayVendorRequest.isReviewed || 
							pages$PayVendorRequest.isFinanceRejected
						   }"
					onclick="if (!confirm('#{msg['mems.common.alertDelete']}')) return false;">
					<h:graphicImage id="deleteIconDisbursement"
						title="#{msg['commons.delete']}"
						url="../resources/images/delete.gif" width="18px;" />
				</h:commandLink>
			</div>
		</t:column>
	</t:dataTable>
</t:div>

<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99%;">
	<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
		cellspacing="1">
		<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
			<t:div styleClass="JUG_NUM_REC_ATT">
				<h:outputText value="#{msg['commons.records']}" />
				<h:outputText value=" : " />
				<h:outputText value="#{pages$PayVendorRequest.totalRows}" />
			</t:div>
		</t:panelGroup>
		<t:panelGroup>
			<t:dataScroller id="shareScrollers" for="disburseId" paginator="true"
				fastStep="1" immediate="false" paginatorTableClass="paginator"
				renderFacetsIfSinglePage="true" paginatorTableStyle="grid_paginator"
				layout="singleTable"
				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
				paginatorActiveColumnStyle="font-weight:bold;"
				paginatorRenderLinkForActive="false" pageIndexVar="pageNumber"
				styleClass="JUG_SCROLLER">
				<f:facet name="first">
					<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
				</f:facet>
				<f:facet name="fastrewind">
					<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
				</f:facet>
				<f:facet name="fastforward">
					<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
				</f:facet>
				<f:facet name="last">
					<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
				</f:facet>
				
				<t:div>
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<h:outputText styleClass="PAGE_NUM"
										style="font-size:9;color:black;"
										value="#{msg['mems.normaldisb.label.totalAmount']}:" />
								</td>
								<td>
									<h:outputText styleClass="PAGE_NUM"
										style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:9;font-weight:bold;color:black"
										value="#{pages$PayVendorRequest.sumDisbursements}" />
								</td>
							</tr>
						</table>
					</t:div>
			</t:dataScroller>
		</t:panelGroup>
	</t:panelGrid>
</t:div>
