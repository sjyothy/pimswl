<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
<head>
	
	<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css"
			href="../resources/css/simple_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../resources/css/amaf_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../resources/css/table.css" />
	<script>
var selectedUnits;

function AddUsers(field,unitNumber)

      {

 

    

        if(field.checked)

        {

                if(selectedUnits!=null && selectedUnits.length>0 )

                {

      

                   selectedUnits=selectedUnits+","+unitNumber;

                }

                 else

                 selectedUnits=unitNumber;

         }

         else

         {

               var delimiters=",";

               var unitArr=selectedUnits.split(delimiters);

               selectedUnits="";

               for(i=0;i<unitArr.length;i++)

               {

               if(unitArr[i]!=unitNumber)

                {

                     if(selectedUnits.length>0 )

                   {

                         selectedUnits=selectedUnits+","+unitArr[i];

      

               }

                   else

                         selectedUnits=unitArr[i];

                 

                }

                

                

               }

         

         }
</script>
</head>
  
<body>
	<f:view>
		<h:form>
				<div class="contentDiv">
											


												<t:dataTable id="test2" width="100%"
													value="#{pages$TeamMember.secUserDataList}"
													binding="#{pages$TeamMember.dataTable}"
													rows="15"
													preserveDataModel="true" preserveSort="true" var="dataItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

													<t:column id="col1" width="20">

														<f:facet name="header">

															<t:outputText value="Full Name" />

														</f:facet>
														<t:outputText value="#{dataItem.fullName}" />
													</t:column>


												<t:column id="col2"  width="20%" >
													<f:facet name="header">


&nbsp;

													</f:facet>
														<h:selectBooleanCheckbox onclick="javascript:AddUsers(this,'#{dataItem.loginId}_#{dataItem.fullName}');"></h:selectBooleanCheckbox>
												</t:column>


												</t:dataTable>										
											</div>
										<div class="contentDivFooter">
												<t:dataScroller id="scroller" for="test2" paginator="true"
													fastStep="1" paginatorMaxPages="2" immediate="true"
													styleClass="scroller" paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true"
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

													<f:facet name="first">

														<t:outputLabel value="First" id="lblF" />

													</f:facet>

													<f:facet name="last">

														<t:outputLabel value="Last" id="lblL" />

													</f:facet>

													<f:facet name="fastforward">

														<t:outputLabel value="FFwd" id="lblFF" />

													</f:facet>

													<f:facet name="fastrewind">

														<t:outputLabel value="FRev" id="lblFR" />

													</f:facet>

												</t:dataScroller>
									
</div>
		</h:form>
	</f:view>
</body>
</html>