<script language="JavaScript" type="text/javascript">
	
			function onMessageFromSearchEndowments()
		{
			document.getElementById("detailsFrm:onMessageFromSearchEndowments").onclick();
		
		}
		
		function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.onclick();
		return; 
		
	}
	
</script>

<t:div style="width:100%;">
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">

		<t:dataTable rows="15" width="100%"
			id="dataTableBuildFrom"
			value="#{pages$endowmentManage.manageCompensateDataList}"
			preserveSort="false" var="dataItem" rowClasses="row1,row2"
			rules="all" renderedIfEmpty="true">
			<t:column sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['endowment.lbl.num']}" />
				</f:facet>
				<t:commandLink
					onclick="openEndowmentManagePopup('#{dataItem.endowment.endowmentId}')"
					value="#{dataItem.endowment.endowmentNum}"
					style="white-space: normal;" styleClass="A_LEFT" />


			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['endowment.lbl.name']}" />
				</f:facet>
				<t:outputText styleClass="A_LEFT"
					value="#{dataItem.endowment.endowmentName}" />
			</t:column>
			<t:column sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.report.costCenter']}" />
				</f:facet>
				<t:outputText styleClass="A_LEFT"
					value="#{dataItem.endowment.costCenter}" />
			</t:column>
			<t:column sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['endowment.lbl.compensatedpercentage']}" />
				</f:facet>
				<t:outputText styleClass="A_LEFT" value="#{dataItem.percentage}">
					<f:convertNumber maxIntegerDigits="15" maxFractionDigits="3"
						pattern="##############0.000" />
				</t:outputText>
			</t:column>
			<t:column sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.amount']}" />
				</f:facet>
				<t:outputText styleClass="A_LEFT" id="compensationAmountold"
					value="#{dataItem.compensationAmount}">
					<f:convertNumber maxIntegerDigits="15" maxFractionDigits="2"
						pattern="##############0.00" />
				</t:outputText>
			</t:column>
			<t:column sortable="true">
				<f:facet name="header">
					<t:outputText
						value="#{msg['endowment.lbl.newEndowmentpercentage']}">
						<f:convertNumber maxIntegerDigits="15" maxFractionDigits="3"
							pattern="##############0.000" />
					</t:outputText>
				</f:facet>
				<t:outputText styleClass="A_LEFT"
					value="#{dataItem.newEndowmentPercentage}" />
			</t:column>




		</t:dataTable>
	</t:div>

	<t:div styleClass="contentDivFooter" style="width:99.2%;">
		<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
			cellspacing="1">
			<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
				<t:div styleClass="JUG_NUM_REC_ATT">
					<h:outputText value="#{msg['commons.records']}" />
					<h:outputText value=" : " />
					<h:outputText value="#{pages$endowmentManage.recordSize}" />
				</t:div>
			</t:panelGroup>


			<t:panelGroup>
				<t:dataScroller for="dataTableBuildFrom" paginator="true"
					fastStep="1" paginatorMaxPages="15" immediate="false"
					paginatorTableClass="paginator" renderFacetsIfSinglePage="true"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
					styleClass="SCH_SCROLLER" pageIndexVar="pageNumber"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false">
					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
					</f:facet>
					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
					</f:facet>
					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
					</f:facet>
					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
					</f:facet>


				</t:dataScroller>
			</t:panelGroup>
		</t:panelGrid>
	</t:div>


</t:div>