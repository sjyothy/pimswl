
<%@page import="com.avanza.ui.util.ResourceUtil"%><%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<f:view >
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg" />
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path" />

	<html  dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}"  >

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"	CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.js_popup_calendar}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_print}"/>" media="print" />
		</head>
		<body   class="BODY_STYLE">

		<div  id="containerDivId" class="containerDiv" >
			<h:inputHidden id="requestId" value="#{pages$printSettlement.requestId}"/>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				
				<tr >
					<td width="83%" valign="top" class="divBackgroundBody">
						<table  class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD"><h:outputLabel value="#{msg['contractList.settleContract']}" styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>	
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
							<tr valign="top">
								<td valign="top" nowrap="nowrap" width="1">
								</td>
								<td width="100%" valign="top" nowrap="nowrap">
									<h:form id="printSettlementForm" enctype="multipart/form-data" >
									<div class="SCROLLABLE_SECTION" >
										<div id="marginId" class="MARGIN" style="width:95%;">
											
											<div style="width:99.6%;">
											   <h:outputLabel value="#{msg['contract.Contract.Details']}" style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px;" />
												<t:div>&nbsp;</t:div>
												<t:panelGrid style="width:100%;" columns="4" columnClasses="GRID_C1, GRID_C2, GRID_C1, GRID_C2">
												    <h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['commons.Contract.Number']}:"/>
											        <h:outputText style="width:100%;" id="txtContractNumber"  value="#{pages$printSettlement.contractBean.contractNo}" />
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['contract.tenantName']}:"/>
											        <h:outputText style="width:100%;"  id="txtTenentName" value="#{pages$printSettlement.contractBean.tenantName}" />
													
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.startdate']}:"/>
											        <h:outputText style="width:100%;"  id="txtStartDate" value="#{pages$printSettlement.contractBean.contractStartDate}" />
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.enddate']}:"/>
											        <h:outputText style="width:100%;"  id="txtEndDate" value="#{pages$printSettlement.contractBean.contractExpiryDate}" />
													
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.rentalamount']}:"/>
													<h:outputText style="width:100%;" id="txtUnitRentValue" value="#{pages$printSettlement.contractBean.rentalAmount}">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
													</h:outputText>
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.evacdate']}:"/>
													<h:outputText style="width:100%;"  id="evacdate"   value="#{pages$printSettlement.contractBean.evacDate}">
												         <f:convertDateTime timeZone="#{pages$printSettlement.timeZone}" pattern="#{pages$printSettlement.dateFormat}" />
												    </h:outputText>		
													
													
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.actualrentperiod']}:"/>
													<t:panelGrid cellpadding="0" cellspacing="0" columns="4">
														<h:outputText style="width:20px;" id="months" value="#{pages$printSettlement.contractBean.months}-"></h:outputText>
														<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.months']}"/>
														<h:outputText style="width:20px;" id="days"  value="#{pages$printSettlement.contractBean.days}-" ></h:outputText>
														<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="  #{msg['settlement.label.days']}"/>
													</t:panelGrid>
													<h:outputLabel styleClass="TABLE_LABEL" value="#{msg['settlement.label.actualrentamt']}:"/>
													<h:outputText style="width:100%;" id="actualRentalAmount" value="#{pages$printSettlement.contractBean.actualRentalAmount}">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
													</h:outputText>

													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.totaldepositamt']}:"/>
											        <h:outputText style="width:100%;" id="totalDepositAmount" value="#{pages$printSettlement.contractBean.totalDepositAmount}">
											        	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											        </h:outputText>
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.totalrealizedamt']}:"/>
											        <h:outputText style="width:100%;" id="totalRealizedAmount" value="#{pages$printSettlement.contractBean.totalRealizedAmount}">
											        	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											        </h:outputText>

													<t:div>&nbsp;</t:div>
													<t:div>&nbsp;</t:div>
													<h:outputLabel styleClass="TABLE_LABEL" value="#{msg['settlement.label.paidamt']}:" style="font-weight:bold;"/>
											        <h:outputText style="width:100%;" id="totalPaidAmount" value="#{pages$printSettlement.contractBean.totalPaidAmount}">
											        	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											        </h:outputText>

													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.unrealizedamt']}:"/>
											        <h:outputText style="width:100%;" id="totalUnrealizedAmount" value="#{pages$printSettlement.contractBean.totalUnrealizedAmount}">
											        	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											        </h:outputText>
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.evacfines']}:"/>
											        <h:outputText style="width:100%;"  id="totalEvacFines" value="#{pages$printSettlement.contractBean.totalEvacFines}"  />
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.unpaidfines']}:"/>
											        <h:outputText style="width:100%;"   id="otherUnpaidfines" value="#{pages$printSettlement.contractBean.otherUnpaidfines}">
											        	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											        </h:outputText>
													<h:outputLabel styleClass="TABLE_LABEL" value="#{msg['settlement.label.unpaidamt']}:" style="font-weight:bold;"/>
											        <h:outputText style="width:100%;" id="totalUnpaidAmount" value="#{pages$printSettlement.contractBean.totalUnpaidAmount}">
											        	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											        </h:outputText>
												</t:panelGrid>
											</div>
											<t:div>&nbsp;</t:div>
											<table>
											  <tr>
											      <td >
											        <h:outputLabel value="#{msg['settlement.lbl.rentPaymentsDetails']}" style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px;" />
											      </td>
											  </tr>
											</table>
											<t:div>
												
												<t:div styleClass="contentDiv" style="width:99%">
													<t:dataTable id="dt1" var="paymentItem" rows="100" binding="#{pages$printSettlement.paymentTable}" value="#{pages$printSettlement.payments}"  width="100%">
														
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymenttype']}"/></f:facet>
															<h:outputText value="#{paymentItem.paymentType}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymentdate']}"/></f:facet>
															<h:outputText value="#{paymentItem.dueDate}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymentmethod']}"/></f:facet>
															<h:outputText value="#{paymentItem.paymentMethod}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['commons.amount']}"/></f:facet>
															<h:outputText styleClass="A_RIGHT_NUM" value="#{paymentItem.amountVal}">
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
															</h:outputText>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['commons.status']}"/></f:facet>
															<h:outputText value="#{paymentItem.status}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.receiptno']}"/></f:facet>
															<h:outputText value="#{paymentItem.recieptNo}"/>
														</t:column>
												
												
													</t:dataTable>
												</t:div>
												</t:div>
											
												
												

												<%--Other PAYMENTS  start--%>
											<t:div>&nbsp;</t:div>
										<table>
										  <tr>
										      <td >
										        <h:outputLabel value="#{msg['settlement.lbl.otherPayments']}" style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px;" />
										      </td>
										  </tr>
										</table>
											
											<t:div>
												<t:div styleClass="contentDiv" style="width:99%">
													<t:dataTable id="dt3" var="paymentItem" rows="100" binding="#{pages$printSettlement.paymentTableOther}" value="#{pages$printSettlement.otherPayments}"  width="100%">
														
														<t:column width="10%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymenttype']}"/></f:facet>
															<h:outputText value="#{paymentItem.paymentType}"/>
														</t:column>
														<t:column width="10%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymentdate']}"/></f:facet>
															<h:outputText value="#{paymentItem.dueDate}"/>
														</t:column>
														<t:column width="10%">
															<f:facet name="header"><h:outputText value="#{msg['paymentSchedule.description']}"/></f:facet>
															<h:outputText value="#{paymentItem.paymentScheduleView.description}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymentmethod']}"/></f:facet>
															<h:outputText value="#{paymentItem.paymentMethod}"/>
														</t:column>
														<t:column width="12%">
															<f:facet name="header"><h:outputText value="#{msg['commons.amount']}"/></f:facet>
															<h:outputText styleClass="A_RIGHT_NUM" value="#{paymentItem.amountVal}">
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
															</h:outputText>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['commons.status']}"/></f:facet>
															<h:outputText value="#{paymentItem.status}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.receiptno']}"/></f:facet>
															<h:outputText value="#{paymentItem.recieptNo}"/>
														</t:column>
												
													</t:dataTable>
												</t:div>
													</t:div>
												
												

											<%-- Other Payments End--%>
											<t:div>&nbsp;</t:div>
											<t:div>
												<t:panelGrid cellpadding="0" cellspacing="10" columns="6" width="70%">
													<h:outputLabel id="lblRefund" style="font-weight:bold;" styleClass="TABLE_LABEL" value="#{msg['settlement.payment.totalrefundamount']}"/>
												    <h:outputText styleClass="A_RIGHT_NUM" id="totalrefundAmount" value="#{pages$printSettlement.contractBean.totalRefundAmount}">
												      	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
												    </h:outputText>
												    <h:outputLabel style="font-weight:bold;" styleClass="TABLE_LABEL" value="#{msg['settlement.payment.currency']}"/>
													<h:outputLabel id="lblDue" style="font-weight:bold;" styleClass="TABLE_LABEL" value="#{msg['settlement.payment.totaldueamount']}"/>
													<h:outputText  styleClass="A_RIGHT_NUM" id="totalDueAmount" value="#{pages$printSettlement.contractBean.totalDueAmount}">
												      	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
												    </h:outputText>
												    <h:outputLabel style="font-weight:bold;" styleClass="TABLE_LABEL" value="#{msg['settlement.payment.currency']}"/>
												</t:panelGrid>
											</t:div>
											
											<%--Cheques to be returned start--%>
											<t:div>&nbsp;</t:div>
											<t:div>&nbsp;</t:div>
										
										<table>
										  <tr>
										      <td >
										        <h:outputLabel value="#{msg['settlement.lbl.returnCheques']}" style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px;" />
										      </td>
										  </tr>
										</table>
											<t:div>
												
												<t:div styleClass="contentDiv" style="width:99%">
													<t:dataTable id="dt2" var="paymentItem" rows="100" binding="#{pages$printSettlement.paymentChequestToReturnTable}" value="#{pages$printSettlement.paymentsChequesToBeReturned}"  width="100%">
														
														
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymentdate']}"/></f:facet>
															<h:outputText value="#{paymentItem.dueDate}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymentmethod']}"/></f:facet>
															<h:outputText value="#{paymentItem.paymentMethod}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['commons.amount']}"/></f:facet>
															<h:outputText styleClass="A_RIGHT_NUM" value="#{paymentItem.amountVal}">
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
															</h:outputText>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['commons.status']}"/></f:facet>
															<h:outputText value="#{paymentItem.status}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.receiptno']}"/></f:facet>
															<h:outputText value="#{paymentItem.recieptNo}"/>
														</t:column>
														
													</t:dataTable>
												</t:div>
													
											
									</t:div>
												
												
											
											
                                    
                                   
											
										</div>
									</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
	</html>
<script type="text/javascript">
       
       
         
         window.print();
       
		
</script>		


</f:view>