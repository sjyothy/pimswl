
<t:div styleClass="contentDiv" style="width:98%">
	<t:dataTable id="childMasarifTable"
		rows="#{pages$childMasarifTab.paginatorRows}" style="width:100%"
		value="#{pages$childMasarifTab.childMasarifList}"
		binding="#{pages$childMasarifTab.childMasarifTable}"
		preserveDataModel="false" preserveSort="false" var="child"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
		<t:column sortable="true" style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['masraf.label.masrafName']}" />
			</f:facet>
			<t:commandLink value="#{child.masrafName}"
				onclick="javaScript:openManageMasrafPopup('#{child.masrafId}')">

			</t:commandLink>

		</t:column>

		<t:column sortable="true" style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['masraf.label.parentMasraf']}" />
			</f:facet>
			<h:outputText value="#{child.parentName}" />
		</t:column>

		<t:column sortable="true" style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['grp.BankName']}" />
			</f:facet>
			<h:outputText
				value="#{pages$searchMasarif.englishLocale?child.bankNameEn:child.bankNameAr}" />
		</t:column>

		<t:column sortable="true" style="white-space: normal;">
			<f:facet name="header">
				<t:outputText
					value="#{msg['financialAccConfiguration.bankRemitanceAccountName']}" />
			</f:facet>
			<h:outputText value="#{child.bankAccountNumber}" />
		</t:column>

		<t:column sortable="true" style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['accountStatus.GRPAccountNumber']}" />
			</f:facet>
			<h:outputText value="#{child.grpAccNumber}" />
		</t:column>

		<t:column sortable="true" style="white-space: normal;">
			<f:facet name="header">
				<t:outputText value="#{msg['thirdPartyUnit.label.description']}" />
			</f:facet>
			<h:outputText value="#{child.description}" />
		</t:column>
		<t:column>
			<f:facet name="header">
				<t:outputText
					value="#{msg['mems.inheritanceFile.columnLabel.action']}" />
			</f:facet>
			<t:commandLink
				action="#{pages$childMasarifTab.onOpenStatementOfAccountCriteriaPopup}">
				<h:graphicImage id="printReport"
					title="#{msg['report.tooltip.PrintStOfAcc']}" style="cursor:hand;"
					url="../resources/images/app_icons/print.gif" />&nbsp;
			</t:commandLink>
		</t:column>
	</t:dataTable>
</t:div>

<t:div styleClass="contentDivFooter" style="width:99.1%">


	<t:panelGrid columns="2" cellpadding="0" cellspacing="0" width="100%"
		columnClasses="RECORD_NUM_TD,BUTTON_TD">
		<t:div styleClass="RECORD_NUM_BG">
			<t:panelGrid columns="3"
				columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
				cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
				<h:outputText value="#{msg['commons.recordsFound']}" />
				<h:outputText value=" : " />
				<h:outputText value="#{pages$childMasarifTab.recordSize}" />
			</t:panelGrid>

		</t:div>

		<CENTER>
			<t:dataScroller id="childScroller" for="childMasarifTable"
				paginator="true" fastStep="1"
				paginatorMaxPages="#{pages$childMasarifTab.paginatorMaxPages}"
				immediate="false" paginatorTableClass="paginator"
				renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
				styleClass="SCH_SCROLLER"
				paginatorActiveColumnStyle="font-weight:bold;"
				paginatorRenderLinkForActive="false"
				paginatorTableStyle="grid_paginator" layout="singleTable"
				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

				<f:facet name="first">
					<t:graphicImage url="../#{path.scroller_first}" id="lblFChild"></t:graphicImage>
				</f:facet>

				<f:facet name="fastrewind">
					<t:graphicImage url="../#{path.scroller_fastRewind}"
						id="lblFRChild"></t:graphicImage>
				</f:facet>

				<f:facet name="fastforward">
					<t:graphicImage url="../#{path.scroller_fastForward}"
						id="lblFFChild"></t:graphicImage>
				</f:facet>

				<f:facet name="last">
					<t:graphicImage url="../#{path.scroller_last}" id="lblLChild"></t:graphicImage>
				</f:facet>

				<t:div styleClass="PAGE_NUM_BG">
					<t:panelGrid styleClass="PAGE_NUM_BG_TABLE" columns="2"
						cellpadding="0" cellspacing="0">
						<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}" />
						<h:outputText styleClass="PAGE_NUM"
							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
							value="#{requestScope.pageNumber}" />
					</t:panelGrid>

				</t:div>
			</t:dataScroller>
		</CENTER>

	</t:panelGrid>
</t:div>
