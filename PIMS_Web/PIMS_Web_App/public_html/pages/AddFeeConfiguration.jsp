
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}"
style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				

		<script language="javascript" type="text/javascript">
		
		
		function Calendar(control,control2,x,y)
		{
		  var ctl2Name="formTenant:"+control2;
		  
		  var ctl2=document.getElementById(ctl2Name);
		  showCalendar(control,ctl2,'dd/mm/yyyy','','',x,y); 
		  return false;
		}
		
		function clearWindow()
	{
    	     var cmbselectProcedureType=document.getElementById("formFeeConfiguration:selectProcedureType");
		     cmbselectProcedureType.selectedIndex = 0;
    	     document.getElementById("formFeeConfiguration:txtAccountNo").value="";
    	     var cmbselectFeeType=document.getElementById("formFeeConfiguration:selectFeeType");
		     cmbselectFeeType.selectedIndex = 0;
		     var cmbselectBasedOn=document.getElementById("formFeeConfiguration:selectBasedOn");
		     cmbselectBasedOn.selectedIndex = 0;
	         document.getElementById("formFeeConfiguration:txtMinAmount").value="";
	         document.getElementById("formFeeConfiguration:txtMaxAmount").value="";
	         document.getElementById("formFeeConfiguration:txtPercentage").value="";
		    		    }
		    	
		   function clearWindowSubType(){
		     var cmbselectSubFee=document.getElementById("formFeeConfiguration:selectSubFee");
		     cmbselectSubFee.selectedIndex = 0;
	         document.getElementById("formFeeConfiguration:txtfeeAmount").value="";
	         document.getElementById("formFeeConfiguration:txtDescriptionEn").value="";
	         //document.getElementById("formFeeConfiguration:txtDescriptionAr").value="";
	         document.getElementById("formFeeConfiguration:txtMinValue").value="";
	         document.getElementById("formFeeConfiguration:txtMaxValue").value="";
		    }	    
		    
		    function submitForm(){
       
		     //alert("In submitForm");    
		    var cmbCountry=document.getElementById("formTenant:selectcountry");
		    var i = 0;
		    i=cmbCountry.selectedIndex;
		    //alert("value of i"+i);
		    document.getElementById("formTenant:hdnStateValue").value=cmbCountry.options[i].value ;
		     
		    
		    document.getElementById("formTenant:hdnCityValue").value= -1 ;
		     
		    document.forms[0].submit();
		    
		    //alert("End submitForm");
		    
		    }
		    
		    
		    function submitFormState(){
       
		    // alert("In submitFormState");    
		    var cmbState=document.getElementById("formTenant:selectstate");
		    var j = 0;
		    j=cmbState.selectedIndex;
		   // alert("value of j"+j);
		    document.getElementById("formTenant:hdnCityValue").value=cmbState.options[j].value ;
		     
		    document.forms[0].submit();
		   // alert("End submitFormState");
		    
		    }
		    
	    function showHide(){
	    
	     alert("In ShowHide");
	     var cmbSelectType=document.getElementById("formTenant:selectFacilityType");
	     var txtRent=document.getElementById("formTenant:txtrentValue");
	   alert(cmbSelectType.value);
       if (cmbSelectType.value=="FACILITY_TYPE_PAID"){
        alert("in:::::::if");
        txtRent.disabled=false;}
    else{ 
    txtRent.disabled=true;
    alert("in:::::::else");}
       alert("In ShowHide2");
  }
		    
		
    </script>
</head>
	<body class="BODY_STYLE">
    <!-- Header --> 
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="2">
				<jsp:include page="header.jsp" />
			</td>
		</tr>
		<tr width="100%">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
			<td width="83%" height="505px" valign="top" class="divBackgroundBody">
				<table width="99.2%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['feeConfiguration.feeConfigurationDetails']}" styleClass="HEADER_FONT"/>
						</td>
						<td width="100%">
							&nbsp;
						</td>
					</tr>
				</table>
				<table width="99%" class="greyPanelMiddleTable" cellpadding="0"	cellspacing="0" border="0" height="100%">
					<tr valign="top">
						<td height="100%" valign="top" nowrap="nowrap"
							background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
							width="1">
						</td>
						<td width="100%" height="460px" valign="top" nowrap="nowrap">
							
							
						
							
							<div class="SCROLLABLE_SECTION">
							<h:form id="formFeeConfiguration" style="width:97%;">
								<table border="0" class="layoutTable" >
									<tr>
							    		<td>
											
											<h:outputText value="#{pages$AddFeeConfiguration.errorMessages}" styleClass="ERROR_FONT" style="padding:10px;"/>
										    <h:outputText value="#{pages$AddFeeConfiguration.successMessages}" styleClass="INFO_FONT" style="padding:10px;"/>
											<h:inputHidden id="hdnfeeConfigId" value="#{pages$AddFeeConfiguration.feeConfigurationId}"/>
											<h:inputHidden id="hdnpageMode" value="#{pages$AddFeeConfiguration.pageMode}"/>
											<h:inputHidden id="hdnFeesubTypeDetailId" value="#{pages$AddFeeConfiguration.feeSubTypeDetailId}"/>
											<h:inputHidden id="hdnpageModeSubFee" value="#{pages$AddFeeConfiguration.pageModeSubType}"/>
											<h:inputHidden id="hdnSubTypeRender" value="#{pages$AddFeeConfiguration.subTypeRander}"/>
											<h:inputHidden id="hdncomboDisable" value="#{pages$AddFeeConfiguration.comboDisable}"/>
										  </td>
									</tr>
									
								</table>
								
								<div  class="MARGIN">
									<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
									
									<div class="DETAIL_SECTION">
										<h:outputLabel value="#{msg['feeConfiguration.feeDetails']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
										<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
											<tr>
												<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfiguration.procedureType']}:"></h:outputLabel>
												</td>
												<td style="padding-right:2px;">
													<h:selectOneMenu id="selectProcedureType" style="width:190px;" disabled="#{pages$AddFeeConfiguration.comboDisable}"
														value="#{pages$AddFeeConfiguration.selectOneProcedureType}">
														<f:selectItem itemLabel="#{msg['commons.All']}"
															itemValue="-1" />
														<f:selectItems
															value="#{pages$ApplicationBean.procedureType}" />
													</h:selectOneMenu>
												</td>
									
												<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfiguration.accountNo']}:"></h:outputLabel>
												</td>
												<td width="145px">
													<h:inputText id="txtAccountNo" style="width:186px"
														value="#{pages$AddFeeConfiguration.accountNo}"/>
												</td>
											</tr>
											<tr>
												<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfiguration.feeType']}:"></h:outputLabel>
												</td>
												<td style="padding-right:2px;">
													<h:selectOneMenu id="selectFeeType" style="width:190px;"  disabled="#{pages$AddFeeConfiguration.comboDisable}"
														value="#{pages$AddFeeConfiguration.selectOneFeeType}">
														<f:selectItem itemLabel="#{msg['commons.All']}"
															itemValue="-1" />
														<f:selectItems
															value="#{pages$ApplicationBean.feeType}" />
													</h:selectOneMenu>
		
												</td>
										
												<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfiguration.basedOn']}:"></h:outputLabel>
												</td>
												<td style="padding-right:2px;">
													<h:selectOneMenu id="selectBasedOn" style="width:190px;" disabled="#{pages$AddFeeConfiguration.comboDisable}"
														value="#{pages$AddFeeConfiguration.selectOnebasedOn}">
														<f:selectItem itemLabel="#{msg['commons.All']}"
															itemValue="-1" />
														<f:selectItems
															value="#{pages$ApplicationBean.basedOnType}" />
													</h:selectOneMenu>
												</td>
											</tr>
									
											<tr>
												<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfiguration.minAmount']}:"></h:outputLabel>
												</td>
												<td>
													<h:inputText id="txtMinAmount" style="width:186px"
														value="#{pages$AddFeeConfiguration.minAmount}" styleClass="A_RIGHT_NUM">
													</h:inputText>
												</td>
												
												<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfiguration.maxAmount']}:"></h:outputLabel>
												</td>
												<td>
													<h:inputText id="txtMaxAmount" style="width:186px"
														value="#{pages$AddFeeConfiguration.maxAmount}" styleClass="A_RIGHT_NUM">
													</h:inputText>
												</td>
											</tr>
										
											<tr>										
												<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['feeConfiguration.percentage']}:"></h:outputLabel>
												</td>
												<td>
													<h:inputText id="txtPercentage" style="width:186px"
														value="#{pages$AddFeeConfiguration.percentage}" styleClass="A_RIGHT_NUM">
													</h:inputText>
												</td>
											</tr>
											<tr>
												<td colspan="4" class="BUTTON_TD" >
												<pims:security screen="Pims.Home.FeeConfiguration.AddFeeConfiguration" action="create">
	                               				   <h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" type= "submit"   action="#{pages$AddFeeConfiguration.btnAdd_Click}" >  </h:commandButton>
	                               				</pims:security>   
											       <h:commandButton styleClass="BUTTON" type="button" onclick="javascript:clearWindow();" value="#{msg['commons.reset']}" >  </h:commandButton>
											       <h:commandButton styleClass="BUTTON" type= "submit" value="#{msg['commons.cancel']}" action="#{pages$AddFeeConfiguration.btnBack_Click}" >	</h:commandButton>
	                                		    </td>
	                           		        </tr>
										</table>
									</div>
								</div>
						
					<c:if test="${pages$AddFeeConfiguration.subTypeRander == \"true\"}">
						<t:div id="subTypeHeader" styleClass="MARGIN" rendered="#{pages$AddFeeConfiguration.subTypeRander}" >
										<table id="imageTable" cellpadding="0" cellspacing="0" width="100%">
											<tr>
											<td><IMG id="image1" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_left")%>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG id="image2" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_mid")%>" class="TAB_PANEL_MID"/></td>
											<td><IMG id="image3" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_right")%>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
								<t:div id="subTypeSection" styleClass="DETAIL_SECTION" rendered="#{pages$AddFeeConfiguration.subTypeRander}" >
					
								
							 	
									<h:outputLabel value="#{msg['feeConfigurationDetail.additionalDetails']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
									
									    <table id="detailSectionTable" cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" >
									    <tr>
											
												<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfigurationDetail.feeSubType']}:"></h:outputLabel>
												</td>
												<td>
													<h:selectOneMenu id="selectSubFee" style="width:191px;"
														value="#{pages$AddFeeConfiguration.selectOneSubFee}">
														<f:selectItem itemLabel="#{msg['commons.All']}"
															itemValue="-1" />
														<f:selectItems
															value="#{pages$ApplicationBean.feeSubType}" />
													</h:selectOneMenu>
												</td>
									
												<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfigurationDetail.feeAmount']}:"></h:outputLabel>
												</td>
												<td >
													<h:inputText id="txtfeeAmount"
														value="#{pages$AddFeeConfiguration.subFeeAmount}"
														style="width:186px" styleClass="A_RIGHT_NUM"/>
												</td>	
												
											</tr>
											<tr>
												<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfigurationDetail.description']}:" rendered="#{pages$AddFeeConfiguration.isEnglishLocale}"></h:outputLabel>
												</td>
												<td >
													<h:inputText id="txtDescriptionEn"
														value="#{pages$AddFeeConfiguration.subFeeDescriptionEn}" rendered="#{pages$AddFeeConfiguration.isEnglishLocale}"
														style="width:186px">

													</h:inputText>
												</td>
												
				          						<td  >
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfigurationDetail.description']}:" rendered="#{pages$AddFeeConfiguration.isArabicLocale}"></h:outputLabel>
												</td>
												<td >
													<h:inputText id="txtDescriptionAr"
														value="#{pages$AddFeeConfiguration.subFeeDescriptionAr}" rendered="#{pages$AddFeeConfiguration.isArabicLocale}"
														style="width:187px">

													</h:inputText>
												</td>								
                 
											</tr>
							
											
											<tr>
												
												
												<td>
													<h:outputLabel styleClass="LABEL" value="#{msg['feeConfigurationDetail.minValue']}:"></h:outputLabel>
												</td>
												<td >
													<h:inputText id="txtMinValue"
														value="#{pages$AddFeeConfiguration.subFeeMinValue}"
														style="width:186px" styleClass="A_RIGHT_NUM">

													</h:inputText>
												</td>
												
				          						<td>
												<h:outputLabel styleClass="LABEL" value="#{msg['feeConfigurationDetail.maxValue']}:"></h:outputLabel>
												</td>
												<td >
													<h:inputText id="txtMaxValue"
														value="#{pages$AddFeeConfiguration.subFeeMaxValue}"
														style="width:186px" styleClass="A_RIGHT_NUM">

													</h:inputText>
												</td>								
									  
									          </tr>
									          
									          <tr>
												<td colspan="4" class="BUTTON_TD" >
												<pims:security screen="Pims.Home.FeeConfiguration.AddFeeConfiguration" action="create">
													<h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" 
													type= "submit"   action="#{pages$AddFeeConfiguration.btnAddSubType_Click}" >  </h:commandButton>
												</pims:security>	
													<h:commandButton styleClass="BUTTON" type="button"
														onclick="javascript:clearWindowSubType();"
														value="#{msg['commons.reset']}" >
													</h:commandButton>
													

												</td>
											</tr>
					          
									    </table>
									  
								</t:div>	 
							</t:div>  
							</c:if> 
						<t:div style="padding-bottom:7px;padding-left:10px;padding-right:0;padding-top:7px;" id="feeDiv1" styleClass="padding:5px;" 
						       rendered="#{pages$AddFeeConfiguration.subTypeRander}">
								<t:div id="feeDiv2" styleClass="SECTION_IMAGE" style="padding-right:4px;" rendered="#{pages$AddFeeConfiguration.subTypeRander}">
								<h:outputText id="OutPutText" styleClass="LIST_SECTION_LABEL" value="#{msg['feeConfiguration.feeDetails']}"/></t:div>
										<t:div id="feeDiv3" styleClass="contentDiv" style="width:97%;" rendered="#{pages$AddFeeConfiguration.subTypeRander}">
											<t:dataTable id="SubFeeGrid" styleClass="grid" 
												value="#{pages$AddFeeConfiguration.gridDataList}"
												binding="#{pages$AddFeeConfiguration.dataTable}" rows="10"
												preserveDataModel="true" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												width="100%">

                                             <t:column id="descriptonEn" sortable="true"  rendered="#{pages$AddFeeConfiguration.isEnglishLocale}">


													<f:facet name="header">

														<t:outputText id="dsras" value="#{msg['feeConfigurationDetail.description']}"  />

													</f:facet>
													<t:outputText id="sdf32443" styleClass="A_LEFT" value="#{dataItem.descriptionEn}" style="white-space: normal;" />
												</t:column>
												<t:column id="descriptionAr" sortable="true"  rendered="#{pages$AddFeeConfiguration.isArabicLocale}">


													<f:facet name="header">

														<t:outputText id="dsfds" value="#{msg['feeConfigurationDetail.description']}" />

													</f:facet>
													<t:outputText id="llop" styleClass="A_LEFT" value="#{dataItem.descriptionAr}" style="white-space: normal;"/>
												</t:column>
									
												<t:column id="minValue" >
													<f:facet name="header">


														<t:outputText id="ewrewdf" value="#{msg['feeConfigurationDetail.minValue']}" />

													</f:facet>
													<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.minValue}" style="white-space: normal;"/>
												</t:column>
												<t:column id="maxValue" >
													<f:facet name="header">


														<t:outputText id="dfsadfasdf" value="#{msg['feeConfigurationDetail.maxValue']}" />

													</f:facet>
													<t:outputText id="sdfsd34234" styleClass="A_RIGHT_NUM" value="#{dataItem.maxValue}" style="white-space: normal;"/>
												</t:column>
												<t:column id="amountSubType">
													<f:facet name="header">


														<t:outputText id="dsfsd3rwe" value="#{msg['feeConfigurationDetail.feeAmount']}" />

													</f:facet>
													<t:outputText id="dsfsdfewrw3223" styleClass="A_RIGHT_NUM" value="#{dataItem.amount}" style="white-space: normal;" />
												</t:column>
										

								   <pims:security screen="Pims.Home.FeeConfiguration.AddFeeConfiguration" action="create">		
										<t:column id="deletebtn"  >
														<f:facet name="header" >
															<t:outputText value="#{msg['commons.delete']}"/>
														</f:facet>
														<t:commandLink  action="#{pages$AddFeeConfiguration.cmdDelete_Click}">&nbsp;
														 <h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
														  </t:commandLink>
														<t:outputText />
										 </t:column>
									</pims:security>	 
									<pims:security screen="Pims.Home.FeeConfiguration.AddFeeConfiguration" action="create">		
										<t:column id="statusbtn"  >
											<f:facet name="header">
												<t:outputText value="#{msg['commons.edit']}" />
											</f:facet>
											
										  <t:commandLink     action="#{pages$AddFeeConfiguration.cmdEdit_Click}">&nbsp;
											 <h:graphicImage id="EditIcon" title="#{msg['commons.edit']}" url="../resources/images/edit-icon.gif" />&nbsp;
										  </t:commandLink>
										  
										 
											<t:outputText />
										</t:column>	
                                    </pims:security>


											</t:dataTable>

											
										</t:div>
     										<t:div id="divfooter" styleClass="contentDivFooter"  style="width:99%;">
     										<table cellpadding="0" cellspacing="0" width="100%" >
												<tr>
												<td id="asdaad" class="RECORD_NUM_TD" >
												
													<t:div id="divRecordCount" styleClass="RECORD_NUM_BG"  rendered="#{pages$AddFeeConfiguration.subTypeRander}">
														<table id="sdfgsdg" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;" >
														<tr><td id="dsadasdasda" class="RECORD_NUM_TD">
														<h:outputText id="dasdasdasda" value="#{msg['commons.recordsFound']}"/>
														</td><td id="dasdasdad" class="RECORD_NUM_TD">
														<h:outputText id="dasdadad" value=" : "/>
														</td><td id="asdasdasdadf" class="RECORD_NUM_TD">
														<h:outputText id="hghfghfhfhfhfh" value="#{pages$AddFeeConfiguration.recordSize}"/>
														</td></tr>
														</table>
													</t:div>
													
												</td>
												<td id="sadsdasdadsadgdfg" class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
											<CENTER>
											<t:dataScroller id="scroller" for="SubFeeGrid" paginator="true"
													fastStep="1" paginatorMaxPages="#{pages$AddFeeConfiguration.paginatorMaxPages}" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 
												    pageIndexVar="pageNumber"
												    styleClass="SCH_SCROLLER"
												    paginatorActiveColumnStyle="font-weight:bold;"
												    paginatorRenderLinkForActive="false" 
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

												<f:facet name="first">
															<t:graphicImage  url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
														
															<t:div id="dsfsdfsdfsdfsd" styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table id="dfsdgfsgdfgdg" cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText id="sadasdasdasds" styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText id="asdasfsafsdfsf" styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
														</t:div>
											
											</t:dataScroller>
                                             </CENTER>
											 </td></tr>
											 </table>

										</t:div>
							       </t:div>
							</h:form>
							</div>
						</td>			
					</tr>
				</table>
			</td>
		</tr>
		<tr>
					
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
	</table>
	
</body>
</html>
</f:view>