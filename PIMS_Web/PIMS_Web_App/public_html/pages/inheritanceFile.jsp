<%-- Author: Kamran Ahmed--%>
<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
function disableButtons()
{
	var inputs = document.getElementsByTagName("INPUT");
	for (var i = 0; i < inputs.length; i++) {
	    if ( inputs[i] != null &&
	         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
	        )
	     {
	        inputs[i].disabled = true;
	    }
	}
}
function updateRecommendation()
{
 //Will remain empty:Used only to close memsFollowup popup which requires this functoin for close
  
}
	function changeResearcher()
	{
		document.getElementById("inheritanceFileForm:lnkChangeResearcher").onclick();
	}
	
	function changeAssignedExpert()
	{
		document.getElementById("inheritanceFileForm:lnkChangeInheritanceExpert").onclick();
	}
	

function onSave( )
{

		disableButtons();
		document.getElementById("inheritanceFileForm:lnkSaveId").onclick();
}
function onGenCostCenter( )
{

		disableButtons();
		document.getElementById("inheritanceFileForm:lnkGenCostCenter").onclick();
}
function onSubmit( )
{

		disableButtons();
		document.getElementById("inheritanceFileForm:lnkSubmit").onclick();
}
function onApprove( )
{

		disableButtons();
		document.getElementById("inheritanceFileForm:lnkApprove").onclick();
}
function onReject( )
{

		disableButtons();
		document.getElementById("inheritanceFileForm:lnkReject").onclick();
}

function onInitLimDone(){

		disableButtons();
		document.getElementById("inheritanceFileForm:lnkInitLimDone").onclick();
}

function onFinalLimDone(){

		disableButtons();
		document.getElementById("inheritanceFileForm:lnkFinalLimDone").onclick();
}

function onInitLimReject(){

		disableButtons();
		document.getElementById("inheritanceFileForm:lnkInitLimReject").onclick();
}

function onSendToReseacrher(){

		disableButtons();
		document.getElementById("inheritanceFileForm:lnkSendToReseacrher").onclick();
}
function onAcknowledge( )
{

		disableButtons();
		document.getElementById("inheritanceFileForm:lnkAcknowledge").onclick();
}

function onSendRecommForApproval( )
{

		disableButtons();
		document.getElementById("inheritanceFileForm:lnkSendRecommForApproval").onclick();
}

function onDistributedCollection(control)
{

		disableButtons();
		control.style.visibility = 'hidden';
		control.nextSibling.onclick();
		onProcessStart();
		return true;
		
}
function onDistributionDone()
{

		disableButtons();
		document.getElementById("inheritanceFileForm:lnkDistributionDone").onclick();
}
function onProcessStart()
	{
	    document.getElementById('inheritanceFileForm:disablingDiv').style.display='block';
		return true;
	}
	function onProcessComplete()
	{
	 document.getElementById('inheritanceFileForm:disablingDiv').style.display='none';
	}
function hdnLinkClicked()
{
	document.getElementById("inheritanceFileForm:hdnOpenFilePersonId").onclick();
}
function   openFollowupPopup()
	{
	   var screen_width = 900;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('memsFollowup.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width)+',height='+(screen_height)+
	     			 ',left=120,top=150,scrollbars=yes,status=yes,resizeable=yes');
	    
	}


	function   openSplitPopUp()
	{
	   var screen_width = 900;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('SplitCollectionPayments.jsf','_blank','width='+(screen_width)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
function   openBeneficiarySharesPopup()
	{
	   var screen_width = 900;
	   var screen_height = 500;
	   var screen_top = screen.top;
	     window.open('BeneficiarySharesPopup.jsf','_blank','width='+(screen_width)+',height='+(screen_height)+',left=100,top=100,scrollbars=yes,status=yes');
	    
	}
		
function   openChangeResearcherPopUp()
	{
	   var screen_width = 900;
	   var screen_height = 330;
	   var screen_top = screen.top;
	     window.open('ChangeResearcher.jsf','_blank','width='+(screen_width)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
	
function   openChangeAssignExpertPopUp()
	{
	   var screen_width = 900;
	   var screen_height = 330;
	   var screen_top = screen.top;
	     window.open('ChangeAssignedExpert.jsf','_blank','width='+(screen_width)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
function disableAndCallSubmitLink()
{
	document.getElementById("inheritanceFileForm:btnSaveFile").disabled = true;
	document.getElementById("inheritanceFileForm:btnSubmit").disabled = true;
	document.getElementById("inheritanceFileForm:lnkSubmit").onclick();
}

function disableAndCallSaveLink()
{
	document.getElementById("inheritanceFileForm:btnSaveFile").disabled = true;
	if(document.getElementById("inheritanceFileForm:btnSubmit"))
		document.getElementById("inheritanceFileForm:btnSubmit").disabled = true;
		document.getElementById("inheritanceFileForm:lnkSaveId").onclick();
}
function beneficiaryLinkClick()
{
	document.getElementById("inheritanceFileForm:lnkBeneficiary").onclick();
}
function searchAssetClicked()
{
	document.getElementById("inheritanceFileForm:lnkSeachAsset").onclick();
}
function populateAsset(aasetDesc,assetNameEn,assetNameAr,assetType,fileNumber,personName,assetId,status)
{
	document.getElementById("inheritanceFileForm:hdnAssetId").value = assetId;
	document.getElementById("inheritanceFileForm:linkAssetId").onclick();
}
function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		    
	        if(hdnPersonType=='APPLICANT')
	        {
			 document.getElementById("inheritanceFileForm:hdnApplicantId").value=personId;
	        }
	        else
            if(hdnPersonType=='INHER_FILE_SPONSOR')
	        {
			 document.getElementById("inheritanceFileForm:hdnInheritanceSponsorId").value=personId;
	        }
	        else
            if(hdnPersonType=='GUARDIAN')
	        {
			 document.getElementById("inheritanceFileForm:hdnGuardianId").value=personId;
	        }
	        else
            if(hdnPersonType=='BENEFICIARY')
	        {
			 document.getElementById("inheritanceFileForm:hdnBeneficiaryId").value=personId;
	        }
	        else
            if(hdnPersonType=='FILE_PERSON')
	        {
			 document.getElementById("inheritanceFileForm:hdnInheritancePersonId").value=personId;
	        }
	        else
            if(hdnPersonType=='NURSE')
	        {
			 document.getElementById("inheritanceFileForm:hdnNurseId").value=personId;
	        }
	        else
            if(hdnPersonType=='REPRESENTATIVE')
	        {
		        document.getElementById("inheritanceFileForm:hdnrepresentativePersonId").value=personId;
		        document.getElementById("inheritanceFileForm:hdnrepresentativePersonName").value=personName;
		        document.getElementById("inheritanceFileForm:representativeName").value=personName;
	        }
	        else{
	        document.getElementById("inheritanceFileForm:hdnManagerId").value=personId;
	        }
	        document.forms[0].submit();
	       
	}
	function   showSponsorPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('SearchPerson.jsf?persontype=INHER_FILE_SPONSOR&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
	function   showFilePersonPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('SearchPerson.jsf?persontype=FILE_PERSON&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
	function   showNursePopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('SearchPerson.jsf?persontype=NURSE&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
	function   showBeneficiaryPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('SearchPerson.jsf?persontype=BENEFICIARY&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
	function   showRepresentativePopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('SearchPerson.jsf?persontype=REPRESENTATIVE&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
	function   showGuardianPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('SearchPerson.jsf?persontype=GUARDIAN&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
	function   showAssetSearchPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('SearchAssets.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}

function searchManagerClicked()
{
	document.getElementById("inheritanceFileForm:lnkSeachManager").onclick();
}
function   showManagerSearchPopup()
{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('SearchPerson.jsf?viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>

			<script type="text/javascript">
        
        </script>

		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">

					<c:choose>
						<c:when test="${!pages$inheritanceFile.viewModePopUp}">
							<tr
								style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>

					<tr width="100%">
						<c:choose>
							<c:when test="${!pages$inheritanceFile.viewModePopUp}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel
											value="#{msg['mems.inheritanceFile.pageTitle.openfile']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<%-- 	<div class="SCROLLABLE_SECTION" >  --%>

										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="inheritanceFileForm"
												enctype="multipart/form-data" style="WIDTH:100%;">
												<t:div id="disablingDiv" styleClass="disablingDiv"
													style="height: 470px;"></t:div>

												<div>
													<table id="layoutTable" border="0" class="layoutTable"
														style="margin-left: 15px; width: 96%;">
														<tr>
															<td>
																<h:outputText id="errorMesssages"
																	value="#{pages$inheritanceFile.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successMessages"
																	value="#{pages$inheritanceFile.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:messages></h:messages>

																<h:inputHidden id="hdnApplicantId"
																	value="#{pages$inheritanceFile.hdnApplicantId}" />

																<h:inputHidden id="hdnInheritanceSponsorId"
																	value="#{pages$inheritanceFile.hdnInheritanceSponsorId}" />

																<h:inputHidden id="hdnGuardianId"
																	value="#{pages$inheritanceFile.hdnGuardianId}" />

																<h:inputHidden id="hdnBeneficiaryId"
																	value="#{pages$inheritanceFile.hdnBeneficiaryId}" />

																<h:inputHidden id="hdnInheritancePersonId"
																	value="#{pages$inheritanceFile.hdnInheritancePersonId}" />

																<h:inputHidden id="hdnNurseId"
																	value="#{pages$inheritanceFile.hdnNurseId}" />

																<h:inputHidden id="hdnAssetId"
																	value="#{pages$inheritanceFile.hdnAssetId}" />

																<h:commandLink id="linkAssetId"
																	action="#{pages$inheritanceFile.receiveAsset}" />
																<h:commandLink id="lnkSaveId"
																	action="#{pages$inheritanceFile.saveFile}" />
																<h:commandLink id="lnkSubmit"
																	action="#{pages$inheritanceFile.submitFile}" />
																<h:commandLink id="lnkGenCostCenter"
																	action="#{pages$inheritanceFile.onGenerateCostCenterForFileAfterFinalization}" />


																<h:commandLink id="hdnOpenFilePersonId"
																	action="#{pages$inheritanceFile.openFilePersonPopup}" />

																<h:inputHidden id="hdnManagerId"
																	value="#{pages$inheritanceFile.hdnManagerId}" />

																

																<a4j:commandLink id="lnkChangeResearcher"
																	reRender="layoutTable,errorMesssages,successMessages,researcher,btnSendRecommForApproval"
																	action="#{pages$inheritanceFile.onChangeResearcher}" />

																<a4j:commandLink id="lnkChangeInheritanceExpert"
																	reRender="layoutTable,errorMesssages,successMessages,inheritanceExpert,btnSendRecommForApproval"
																	action="#{pages$inheritanceFile.onChangeAssignedExpert}" />

															</td>
														</tr>
													</table>
												</div>

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px; width: 90%;">
														<tr>
															<td>

															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel
																	value="#{msg['mems.inheritanceFile.fieldLabel.fileNumber']} :"
																	styleClass="LABEL" />
															</td>
															<td>
																<h:inputText id="fileNumberId"
																	styleClass="#{pages$inheritanceFile.afterApproval?'READONLY':''}"
																	readonly="#{pages$inheritanceFile.afterApproval}"
																	binding="#{pages$inheritanceFile.txtFileNumber}" />
															</td>

															<td>
																<h:outputText
																	value="#{msg['mems.inheritanceFile.fieldLabel.filePerson']}" />
															</td>
															<td>
																<h:panelGroup>
																	<h:inputText readonly="true" styleClass="READONLY"
																		value="#{pages$inheritanceFileBasicInfoTab.filePerson.personFullName}" />

																	<t:commandLink
																		rendered="#{pages$inheritanceFile.showPrintStatmentOfAccImage}"
																		action="#{pages$inheritanceFileBasicInfoTab.openStatmntOfAccount}">
																		<h:graphicImage
																			title="#{msg['report.tooltip.PrintStOfAcc']}"
																			style="MARGIN: 1px 1px -5px;cursor:hand"
																			url="../resources/images/app_icons/print.gif">
																		</h:graphicImage>&nbsp;
																	</t:commandLink>
																	<t:commandLink
																		rendered="#{pages$inheritanceFile.showSocialResearchImage}"
																		action="#{pages$inheritanceFileBasicInfoTab.openResearcherForm}">
																		<h:graphicImage id="researchFormTwo"
																			title="#{msg['beneficiaryDetails.tooltip.openResearchForm']}"
																			style="cursor:hand;"
																			url="../resources/images/detail-icon.gif" />
																	</t:commandLink>
																</h:panelGroup>

															</td>


														</tr>
														<tr>
															<td>
																<h:outputLabel
																	value="#{msg['mems.inheritanceFile.fieldLabel.createdOn']} :"
																	styleClass="LABEL" />
															</td>
															<td>
																<rich:calendar id="createdOnDateId"
																	styleClass="READONLY" disabled="TRUE"
																	binding="#{pages$inheritanceFile.clndrCreatedOn}"
																	locale="#{pages$inheritanceFile.locale}" popup="true"
																	datePattern="#{pages$inheritanceFile.dateFormat}"
																	showApplyButton="false" enableManualInput="false"
																	inputStyle="width:185px;height:17px" />
															</td>
															<td>
																<h:outputLabel
																	value="#{msg['mems.inheritanceFile.fieldLabel.fileStatus']} :"
																	styleClass="LABEL" />
															</td>
															<td>
																<h:inputText id="statusId" styleClass="READONLY"
																	readonly="true"
																	binding="#{pages$inheritanceFile.txtFileStatus}" />
															</td>

														</tr>
														<tr>

															<td>
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel
																		value="#{msg['mems.inheritanceFile.fieldLabel.fileType']} :"
																		styleClass="LABEL" />
																</h:panelGroup>
															</td>
															<td>
																<h:selectOneMenu id="fileTypeId"
																	readonly="#{pages$inheritanceFile.afterApproval }"
																	disabled="#{pages$inheritanceFile.fileTypeDisabled}"
																	binding="#{pages$inheritanceFile.cboFileType}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.inheritenceFileTypeList}" />
																	<a4j:support event="onchange"
																		reRender="tabPanelInhFile,beneficiaryDetailsDiv,btnAdd,inheritanceFileBasicInfoTabControls,inheritanceBeneficiaryTabControls2,inheritanceBeneficiaryTabControls,lblCurrentWives,txtCurrentWives,lblPreviousWives,txtPreviousWives,lblDeathDate,clndrDeathDate,clndrDeathPlace,controlsBasedOnFileType,lblFilePersonBeneShareId,txtFilePersonBeneShareId,pgLblSponsor,pgtxtSponsor"
																		action="#{pages$inheritanceFile.fileTypeChanged}" />
																</h:selectOneMenu>
															</td>

															<TD>
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['inheritanceFile.label.totalShare']}"></h:outputLabel>
																</h:panelGroup>
															</TD>
															<td>
																<h:inputText
																	disabled="#{pages$inheritanceFile.fileTypeDisabled}"
																	id="txtTotalFileShare" onkeyup="removeNonInteger(this)"
																	onkeypress="removeNonInteger(this)"
																	onkeydown="removeNonInteger(this)"
																	onblur="removeNonInteger(this)"
																	onchange="removeNonInteger(this)"
																	value="#{pages$inheritanceFile.txtTotalFileShare}">


																</h:inputText>
															</td>

														</tr>
														<tr>
															<td>
																<h:outputLabel
																	value="#{msg['inheritanceFile.fieldLabel.oldCostCenter']} :"
																	styleClass="LABEL" />
															</td>
															<td>
																<h:inputText id="oldCostCenter"
																	styleClass="#{pages$inheritanceFile.afterApproval?'READONLY':''}"
																	readonly="#{pages$inheritanceFile.afterApproval}"
																	value="#{pages$inheritanceFile.txtOldCostCenter}" />
															</td>
															<td>
																<h:outputLabel
																	value="#{msg['inheritanceFile.fieldLabel.newCostCenter']} :"
																	styleClass="LABEL" />
															</td>
															<td>
																<h:inputText id="newCostCenter"
																	binding="#{pages$inheritanceFile.txtNewCostCenter}" />
															</td>


														</tr>



														<tr>

															<td>
																<h:panelGroup
																	rendered="#{pages$inheritanceFile.researcherAavailable}">
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel
																		value="#{msg['mems.inheritanceFile.reasercher']} :"
																		styleClass="LABEL" />
																</h:panelGroup>
															</td>

															<td>
																<h:selectOneMenu id="researcher"
																	rendered="#{pages$inheritanceFile.researcherAavailable}"
																	disabled="#{pages$inheritanceFile.taskForResearcher}"
																	binding="#{pages$inheritanceFile.cboResearcher}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems
																		value="#{pages$inheritanceFile.researcherList}" />
																</h:selectOneMenu>

																<t:commandLink
																	rendered="#{pages$inheritanceFile.researcherAavailable}"
																	action="#{pages$inheritanceFile.onOpenChangeResearcher}">
																	<h:graphicImage
																		title="#{msg['mems.inheritanceFile.label.changeResearcher']}"
																		style="MARGIN: 1px 1px -1px 0px;cursor:hand"
																		url="../resources/images/app_icons/Change-tenant.png">
																	</h:graphicImage>&nbsp;
																</t:commandLink>
															</td>

															<td>
																<h:panelGroup
																	rendered="#{pages$inheritanceFile.inheritanceExpertAavailable}">
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel
																		value="#{msg['mems.inheritanceFile.assginedExpert']} :"
																		styleClass="LABEL" />
																</h:panelGroup>
															</td>

															<td>
																<h:selectOneMenu id="inheritanceExpert"
																	rendered="#{pages$inheritanceFile.inheritanceExpertAavailable}"
																	binding="#{pages$inheritanceFile.cboAssignedExpert}">
																	<f:selectItem itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems
																		value="#{pages$inheritanceFile.assignedExpertList}" />
																</h:selectOneMenu>

																<t:commandLink
																	rendered="#{pages$inheritanceFile.inheritanceExpertAavailable}"
																	action="#{pages$inheritanceFile.onOpenChangeAssignedExpert}">
																	<h:graphicImage
																		title="#{msg['mems.inheritanceFile.label.changeAssginedExpert']}"
																		style="MARGIN: 1px 1px -1px 0px;cursor:hand"
																		url="../resources/images/app_icons/Change-tenant.png">
																	</h:graphicImage>&nbsp;
																</t:commandLink>
															</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel
																	binding="#{pages$inheritanceFile.lblRejectionReason}"
																	rendered="#{pages$inheritanceFile.showRejectionReason}"
																	value="#{msg['mems.inheritanceFile.fieldLabel.rejectionReason']}"
																	styleClass="LABEL" />
															</td>
															<td colspan="3">
																<h:inputTextarea style="width:90%;"
																	readonly="#{pages$inheritanceFile.pageModeInitLimRjctd}"
																	rendered="#{pages$inheritanceFile.showRejectionReason}"
																	binding="#{pages$inheritanceFile.txtAreaRejectionReason}">
																</h:inputTextarea>
															</td>
														</tr>
													</table>
													<DIV style="height: 10px"></DIV>
													<div class="AUC_DET_PAD">
														<table id="table_1" cellpadding="0" cellspacing="0"
															width="97%">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel switchType="server"
																binding="#{pages$inheritanceFile.fileTabPanel}"
																id="tabPanelInhFile" style="width:97%;">

																<rich:tab id="appDetailsTab"
																	title="#{msg['commons.tab.applicationDetails']}"
																	label="#{msg['mems.inheritanceFile.tabHeadingShort.appDtl']}">
																	<%@ include file="ApplicationDetails.jsp"%>
																</rich:tab>

																<rich:tab id="inheritanceFileBasicInfoTabId"
																	rendered="#{pages$inheritanceFile.showBasicInfoTab}"
																	action="#{pages$inheritanceFile.basicInfoTabClick}"
																	label="#{msg['mems.inheritanceFile.tabHeadingShort.basicInfo']}"
																	title="#{msg['mems.inheritanceFile.tabName.basicInfo']}">
																	<jsp:include page="inheritanceFileBasicInfoTab.jsp" />
																</rich:tab>

																<rich:tab id="inheritanceBeneficiaryTabId"
																	rendered="#{pages$inheritanceFile.showBenenficiaryTab}"
																	action="#{pages$inheritanceFile.inheritanceBeneficiaryTabClick}"
																	label="#{msg['mems.inheritanceFile.tabHeading.beneficiary']}">

																	<jsp:include page="inheritanceBeneficiaryTab.jsp" />
																</rich:tab>
																<rich:tab id="inheritanceBeneficiaryRepresentativeTab"
																	rendered="#{pages$inheritanceFile.showBenenficiaryRepresentativeTab}"
																	action="#{pages$inheritanceFile.onBeneficiaryRepresentativeTab}"
																	label="#{msg['person.representative']}">

																	
																	<%@ include file="../pages/tabBeneficiaryRepresentative.jsp"%>
																</rich:tab>

																<rich:tab id="inheritedAssetsId"
																	rendered="#{pages$inheritanceFile.showInheritedAssetTab}"
																	action="#{pages$inheritanceFile.inheritedAssetTabClick}"
																	title="#{msg['mems.inheritanceFile.tabHeading.inheritedAsset']}"
																	label="#{msg['mems.inheritanceFile.tabHeadingShort.iAsset']}">
																	<jsp:include page="inheritedAssetsTab.jsp"></jsp:include>
																</rich:tab>

																<rich:tab id="beneficiarySharesTabId"
																	rendered="#{pages$inheritanceFile.showAssetShareTab}"
																	action="#{pages$inheritanceFile.beneficiarySharesTabClick}"
																	label="#{msg['mems.inheritanceFile.tabHeadingShort.asstShare']}">
																	<%@  include file="BeneficiarySharesTab.jsp"%>
																</rich:tab>

																<rich:tab id="officialCorrespondenceTabId"
																	title="#{msg['mems.inheritanceFile.tabHeading.officialCorrs']}"
																	action="#{pages$inheritanceFile.officialCorrespondenceTabClick}"
																	rendered="#{pages$inheritanceFile.showOfficialCorrespondence}"
																	label="#{msg['mems.inheritanceFile.tabHeadingShort.offCor']}">
																	<jsp:include page="officialCorrespondenceTab.jsp"></jsp:include>
																</rich:tab>

																<rich:tab id="memsfollowUpTabId"
																	rendered="#{pages$inheritanceFile.showFollowUpTab}"
																	label="#{msg['mems.inheritanceFile.tabHeading.followUp']}">
																	<jsp:include page="memsFollowupTab.jsp" />
																</rich:tab>
																<rich:tab id="distributionTab"
																	label="#{msg['mems.inheritanceFile.tabHeading.Distribution']}"
																	action="#{pages$inheritanceFile.onDistributionTabClick}">
																	<%@ include file="../pages/distributeShareTab.jsp"%>
																</rich:tab>
																<rich:tab id="blockingDetailsTab"
																	label="#{msg['inheritanceFile.tab.blockingList']}"
																	action="#{pages$inheritanceFile.onBlockingTabClick}">
																	<%@ include file="../pages/tabFileBlockingDetails.jsp"%>
																</rich:tab>


																<rich:tab id="attachmentTab"
																	rendered="#{pages$inheritanceFile.showAttachmentTab}"
																	title="#{msg['commons.attachmentTabHeading']}"
																	label="#{msg['mems.inheritanceFile.tabHeadingShort.attch']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>

																<rich:tab id="commentsTab"
																	title="#{msg['commons.commentsTabHeading']}"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>

																<rich:tab
																	label="#{msg['mems.inheritanceFile.tabHeadingShort.history']}"
																	action="#{pages$inheritanceFile.requestHistoryTabClick}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>
															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" style="width: 97%;">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" />
																</td>
															</tr>
														</table>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;width:96%;">
															<pims:security
																screen="Pims.MinorMgmt.InheritanceFile.Save"
																action="view">
																<h:commandButton styleClass="BUTTON" id="btnSaveFile"
																	binding="#{pages$inheritanceFile.btnSaveRequest}"
																	rendered="#{pages$inheritanceFile.showSaveButton}"
																	value="#{msg['mems.inheritanceFile.buttonLabel.saveFile']}"
																	style="width: auto"
																	onclick="if (!confirm('#{msg['confirmMsg.areuSureToSave']}')) return false;else onSave();">


																</h:commandButton>
															</pims:security>
															<pims:security
																screen="Pims.MinorMgmt.InheritanceFile.SaveAfterFinalize"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	id="btnSaveAfterFinalize"
																	rendered="#{ (pages$inheritanceFile.pageModeFinalLimDone || pages$inheritanceFile.pageModeDistReq) &&  
																	           !(pages$inheritanceFile.viewModePopUp || pages$inheritanceFile.pageModeView)
																	          }"
																	value="#{msg['mems.inheritanceFile.buttonLabel.saveFile']}"
																	style="width: auto"
																	onclick="if (!confirm('#{msg['confirmMsg.areuSureToSave']}')) return false;else onSave();">


																</h:commandButton>

																<h:commandButton styleClass="BUTTON"
																	id="btnGenerateCostCenterAfterFinalize"
																	rendered="#{pages$inheritanceFile.showGenerateCostCenterButtonButton}"
																	value="#{msg['inheritanceFile.lbl.generateCostCenter']}"
																	style="width: auto"
																	onclick="if (!confirm('#{msg['confirmMsg.areuSureToSave']}')) return false;else onGenCostCenter();">


																</h:commandButton>

															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.InheritanceFile.Save"
																action="view">
																<h:commandButton styleClass="BUTTON" id="btnSubmit"
																	binding="#{pages$inheritanceFile.btnSubmitRequest}"
																	rendered="#{pages$inheritanceFile.showSubmitButton}"
																	value="#{msg['mems.inheritanceFile.buttonLabel.submitFile']}"
																	style="width: 12%"
																	onclick="if (!confirm('#{msg['confirmMsg.areuSureToSubmit']}')) return false;else onSubmit();">
																</h:commandButton>
															</pims:security>
															<pims:security
																screen="Pims.MinorMgmt.InheritanceFile.Approved"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	binding="#{pages$inheritanceFile.btnApproveRequest}"
																	rendered="#{pages$inheritanceFile.showApproveButton}"
																	value="#{msg['mems.inheritanceFile.buttonLabel.approve']}"
																	style="width: auto"
																	onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToAppRequest']}')) return false;else onApprove();">
																</h:commandButton>
																<h:commandLink id="lnkApprove"
																	action="#{pages$inheritanceFile.approveRequest}" />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.InheritanceFile.Approved"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	binding="#{pages$inheritanceFile.btnRejectResubmit}"
																	rendered="#{pages$inheritanceFile.showRejectButton}"
																	value="#{msg['mems.inheritanceFile.buttonLabel.rejectRequest']}"
																	style="width: auto"
																	onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToRejRequest']}')) return false;else onReject();">
																</h:commandButton>
																<h:commandLink id="lnkReject"
																	action="#{pages$inheritanceFile.rejectRequest}" />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.InheritanceFile.DoneInitLim"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	binding="#{pages$inheritanceFile.btnInitLimDone}"
																	rendered="#{pages$inheritanceFile.showInitLimDoneButton}"
																	value="#{msg['mems.inheritanceFile.buttonLabel.initLimDone']}"
																	style="width: auto"
																	onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToDoneInitLim']}')) return false;else onInitLimDone();">
																</h:commandButton>
																<h:commandLink id="lnkInitLimDone"
																	action="#{pages$inheritanceFile.doneInitLimitation}" />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.InheritanceFile.DoneFinalLim"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	binding="#{pages$inheritanceFile.btnFinalLimDone}"
																	rendered="#{pages$inheritanceFile.showFinalLimDoneButton}"
																	value="#{msg['mems.inheritanceFile.buttonLabel.finalLimDone']}"
																	style="width: auto"
																	onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToDoneFinalLim']}')) return false; else onFinalLimDone();">
																</h:commandButton>
																<h:commandLink id="lnkFinalLimDone"
																	action="#{pages$inheritanceFile.doneFinalLimitation}" />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.InheritanceFile.DoneFinalLim"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	binding="#{pages$inheritanceFile.btnInitLimRejected}"
																	rendered="#{pages$inheritanceFile.showRejectInitLimButton}"
																	value="#{msg['mems.inheritanceFile.buttonLabel.initLimReject']}"
																	style="width: auto"
																	onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToRejectInitLim']}')) return false;else  onInitLimReject();">
																</h:commandButton>
																<h:commandLink id="lnkInitLimReject"
																	action="#{pages$inheritanceFile.rejectInitLimitation}" />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.InheritanceFile.AssignResearcher"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$inheritanceFile.showSendToResearcherButton}"
																	binding="#{pages$inheritanceFile.btnResearcher}"
																	value="#{msg['mems.inheritanceFile.buttonLabel.sendToReseacrher']}"
																	style="width: auto"
																	onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToAssginResearcher']}')) return false;else onSendToReseacrher();">
																</h:commandButton>
																<h:commandLink id="lnkSendToReseacrher"
																	action="#{pages$inheritanceFile.sendToResearcher}" />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.InheritanceFile.AssignResearcher"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$inheritanceFile.showAcknowledgeButton}"
																	binding="#{pages$inheritanceFile.btnAcknowledge}"
																	value="#{msg['btnLabel.acknowledgeTask']}"
																	style="width: auto"
																	onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToAcknowledge']}')) return false;else onAcknowledge();"
																	action="#{pages$inheritanceFile.acknowledgeResearcherTask}">
																</h:commandButton>
																<h:commandLink id="lnkAcknowledge"
																	action="#{pages$inheritanceFile.acknowledgeResearcherTask}" />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.InheritanceFile.SendRecommForApproval"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	id="btnSendRecommForApproval"
																	binding="#{pages$inheritanceFile.btnSendRecommForApproval}"
																	rendered="#{pages$inheritanceFile.showSendRecommForApproval}"
																	value="#{msg['commons.sendForApproval']}"
																	style="width: auto"
																	onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToSendRecommForApp']}')) return false;else onSendRecommForApproval();">
																</h:commandButton>
																<h:commandLink id="lnkSendRecommForApproval"
																	action="#{pages$inheritanceFile.sendRecommendationsForApproval}" />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.InheritanceFile.DistributionFromGeneralAccount"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	binding="#{pages$inheritanceFile.btnDistDone}"
																	rendered="#{pages$inheritanceFile.showDistDoneBtn}"
																	value="#{msg['mems.inheritanceFile.buttonLabel.distribute']}"
																	style="width: auto"
																	onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToCompleteDistribution']}')) return false;else onDistributionDone();">
																</h:commandButton>
																<h:commandLink id="lnkDistributionDone"
																	action="#{pages$inheritanceFile.onDistributionDone}" />
															</pims:security>


															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$inheritanceFile.showCloseButton}"
																value="#{msg['commons.closeButton']}" style="width: 10%"
																onclick="window.close();">
															</h:commandButton>

														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<c:choose>
						<c:when test="${!pages$inheritanceFile.viewModePopUp}">
							<tr
								style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
								<td colspan="2">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>

											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>

										</tr>
									</table>
								</td>
							</tr>
						</c:when>
					</c:choose>
				</table>
			</div>

		</body>
	</html>
</f:view>