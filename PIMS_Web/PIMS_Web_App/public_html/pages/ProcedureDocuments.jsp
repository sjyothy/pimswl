<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		function showProcedureDocumentPopup()
        {
              var screen_width = screen.width;
              var screen_height = screen.height;
              var popup_width = screen_width-700;
              var popup_height = screen_height-500;
              var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
              window.open('ProcedureDocument.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
        }
        
        function changeTooltip()
        {
        	var selectObj = document.getElementById('searchFrm:selectProcedureType');
        	selectObj.title = selectObj[selectObj.selectedIndex].innerHTML;
        }
        
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
		
		
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>


				<tr width="100%">

				<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>

					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table  class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['procedureDocuments.ListTitle']}"
										styleClass="HEADER_FONT" />

								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" >
							<tr valign="top">
								<td>

											<h:form id="searchFrm" enctype="multipart/form-data" >
											<div class="SCROLLABLE_SECTION" >
												<div>
													<table border="0" class="layoutTable">
														<tr>
															<td>
															<h:outputText
																value="#{pages$ProcedureDocuments.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$ProcedureDocuments.infoMessages}"
																escape="false" styleClass="INFO_FONT" />
														</td>

														</tr>
													</table>
												</div>
												<div class="MARGIN" style="width:95%;">
													<table cellpadding="0" cellspacing="0" width="100%">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
											<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
													<div class="DETAIL_SECTION" style="width:99.6%;">
														<h:outputLabel value="#{msg['commons.searchCriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER">
														<tr>

															<td class="PADDING: 5px; PADDING-TOP: 5px;width:20%;">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['paymentConfiguration.procedureType']}:" />
															</td>

															<td style="PADDING: 5px; PADDING-TOP: 5px; width: 20%;">
																<h:selectOneMenu id="selectProcedureType"
																	style="width:192px;"
																	value="#{pages$ProcedureDocuments.selectOneProcedureType}"
																	onchange="changeTooltip();">
																	<f:selectItems
																		value="#{pages$ApplicationBean.procedureType}" />
																</h:selectOneMenu>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 5px; width: 10%;">
																<h:commandLink
																	action="#{pages$ProcedureDocuments.btnSearch_Click}">
																	<h:graphicImage id="searchPayments"
																		title="#{msg['procedureDocuments.searchDocuments']}"
																		url="../resources/images/magnifier.gif" />
																</h:commandLink>
																&nbsp;
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 5px; width: 50%">

															</td>
														</tr>
													</table>
														
													</div>
												</div>
												<div
													style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;width:95%;">
													<div class="imag">
														&nbsp;
													</div>
													<div class="contentDiv" style="width:99%" >
														<t:dataTable id="test2"
														value="#{pages$ProcedureDocuments.gridDataList}"
														binding="#{pages$ProcedureDocuments.dataTable}"
														preserveDataModel="false" preserveSort="false"
														var="document" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true"
														columnClasses="A_LEFT_GRID,A_LEFT_GRID,1,1,A_LEFT_GRID,1,1"
														width="100%">

														<t:column id="task" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['attachment.docType']}" />
															</f:facet>
															<t:outputText value="#{document.docTypeEn}"
																styleClass="A_LEFT" style="white-space: normal;"
																rendered="#{pages$ProcedureDocuments.isEnglishLocale}" />
															<t:outputText value="#{document.docTypeAr}"
																styleClass="A_LEFT" style="white-space: normal;"
																rendered="#{!pages$ProcedureDocuments.isEnglishLocale}" />
														</t:column>

														<t:column id="groups" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['attachment.docDesc']}" />
															</f:facet>
															<t:outputText value="#{document.procDocDescription}"
																styleClass="A_LEFT" style="white-space: normal;" />
														</t:column>
														
														<t:column id="nolTypeEn" sortable="true" rendered="#{pages$ProcedureDocuments.isSelectedProcedureTypeIsNOL}">
															<f:facet name="header">
																<t:outputText value="#{msg['attachment.nolTypeEn']}" />
															</f:facet>
															<t:outputText value="#{document.nolTypeEn}"
																styleClass="A_LEFT" style="white-space: normal;" />
														</t:column>
														
														<t:column id="nolTypeAr" sortable="true" rendered="#{pages$ProcedureDocuments.isSelectedProcedureTypeIsNOL}">
															<f:facet name="header">
																<t:outputText value="#{msg['attachment.nolTypeAr']}" />
															</f:facet>
															<t:outputText value="#{document.nolTypeAr}"
																styleClass="A_LEFT" style="white-space: normal;" />
														</t:column>

														<t:column id="actionLinks">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>
															<h:commandLink id="editLink"
																actionListener="#{pages$ProcedureDocuments.editProcedureDocument}">
																<h:graphicImage id="editIcon"
																	title="#{msg['commons.edit']}"
																	url="../resources/images/edit-icon.gif" />
															</h:commandLink>
															<h:commandLink id="deleteLink"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;"
																actionListener="#{pages$ProcedureDocuments.deleteProcedureDocument}">
																<h:graphicImage id="deleteIcon"
																	title="#{msg['commons.delete']}"
																	url="../resources/images/delete_icon.png" />
															</h:commandLink>
														</t:column>
													</t:dataTable>
														
													</div>
													
												</div>
											<table width="96%" border="0">
												<tr>
													<td class="BUTTON_TD JUG_BUTTON_TD_US" >

														<h:commandButton id="btnAddNewDoc" type="submit"
																	styleClass="BUTTON"
																	value="#{msg['procedureDocuments.addNewDocument']}"
																	action="#{pages$ProcedureDocuments.addNewDocument}"
																	style="width:auto;" />

													</td>

												</tr>


											</table></div>
										
										
               </div>
										</h:form>

									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
					<td colspan="2">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
				</div>
		
		</body>
	</html>
</f:view>