<%-- 
  - Author: Anil Verani
  - Date: 03/07/2009
  - Copyright Notice:
  - @(#)
  - Description: Used for Adding, Updating and Viewing Intial / Feasibility Studies 
  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
	function  receiveSubClassList()
	{
	  document.getElementById('frm:lnkReceiveAssetSubClass').onclick();
	}
	
	function openAssetSubClassManagePopup()
	{
	 var screen_width = 1024;
     var screen_height = 220;
     window.open('assetSubClassManage.jsf','_blank','width='+(screen_width-250)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	function closeWindow() 
	{
	 window.close();
	}
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
						<c:when test="${! pages$accountManage.isPopupViewOnlyMode}">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</c:when>
					</c:choose>
				</tr>

				<tr>
					<c:choose>
						<c:when test="${! pages$accountManage.isPopupViewOnlyMode}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['accountManage.Heading']}" styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99.1%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:460px">

										<h:form id="frm" style="width:97%" enctype="multipart/form-data" >
											<table border="0" class="layoutTable">

												<tr>

													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$accountManage.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$accountManage.successMessages}" />
													</td>
												</tr>
											</table>

											<div class="MARGIN" style="width: 98%">
												
												<table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
										        </table>
												
												<rich:tabPanel style="width:100%;height:235px;" headerSpacing="0">
												
														<!-- Account Tab - Start -->
														<rich:tab label="#{msg['accountManage.AccountClass.Tab.heading']}">
															<t:div  style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid id="followUpDetailsTable" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4">																	
																  <h:panelGroup>	
																	<h:outputLabel     styleClass="mandatory" value="*"/>	
																	<h:outputLabel    styleClass="LABEL" value="#{msg['accountSearch.accountNumber']}:"></h:outputLabel>
																  </h:panelGroup>
																	<h:inputText       readonly="#{pages$accountManage.isViewMode}" 
																	                   styleClass="#{pages$accountManage.isViewReadOnly}" 
																	                   value="#{pages$accountManage.accountView.accountNumber}" style="width: 186px;" maxlength="20"  />
																  <h:panelGroup>	
																	<h:outputLabel     styleClass="mandatory" value="*"/>
																	<h:outputLabel     styleClass="LABEL" value="#{msg['accountStatus.accountStatus']}:"></h:outputLabel>
																  </h:panelGroup>	
																	<h:selectOneMenu   readonly="#{pages$accountManage.isViewMode}" 
																	                   styleClass="#{pages$accountManage.isViewReadOnly}"
																	                   value="#{pages$accountManage.accountView.accountStatusId}" 
																	                   style="width: 192px;">
																		<f:selectItem  itemValue="" itemLabel="#{msg['commons.select']}" />
																		<f:selectItems value="#{pages$ApplicationBean.accountStatus}" />
																	</h:selectOneMenu>
																 <h:panelGroup>	
																	<h:outputLabel     styleClass="mandatory" value="*"/>	
																	<h:outputLabel     styleClass="LABEL" value="#{msg['accountStatus.accountName']}:"></h:outputLabel>
																</h:panelGroup>	
																	<h:inputText       readonly="#{pages$accountManage.isViewMode}" 
																	                   styleClass="#{pages$accountManage.isViewReadOnly}" 
																	                   value="#{pages$accountManage.accountView.accountName}" style="width: 186px;" maxlength="50" />
																 <h:panelGroup>	
																	<h:outputLabel     styleClass="mandatory" value="*"/>	
																	<h:outputLabel     styleClass="LABEL" value="#{msg['accountStatus.accountType']}:"></h:outputLabel>
																 </h:panelGroup>	
																	<h:selectOneMenu   readonly="#{pages$accountManage.isViewMode}" 
																	                   styleClass="#{pages$accountManage.isViewReadOnly}"
																	                   value="#{pages$accountManage.accountView.accountTypeId}" 
																	                   style="width: 192px;">
																		<f:selectItem  itemValue="" itemLabel="#{msg['commons.select']}" />
																		<f:selectItems value="#{pages$ApplicationBean.accountTypeList}" />
																	</h:selectOneMenu>
																 <h:panelGroup>	
																	<h:outputLabel     styleClass="mandatory" value="*"/>	
																	<h:outputLabel    styleClass="LABEL" value="#{msg['accountStatus.GRPAccountNumber']}:"></h:outputLabel>
																  </h:panelGroup>
																	<h:inputText       readonly="#{pages$accountManage.isViewMode}" 
																	                   styleClass="#{pages$accountManage.isViewReadOnly}" 
																	                   value="#{pages$accountManage.accountView.GRPAccountNumber}" style="width: 186px;" maxlength="20"  />
																  	
																
																<h:outputLabel styleClass="LABEL" value="#{msg['accountManage.accountDescription']}:"></h:outputLabel>
																	<h:inputTextarea   readonly="#{pages$accountManage.isViewMode}" 
																	                   styleClass="#{pages$accountManage.isViewReadOnly}" 
																	                   value="#{pages$accountManage.accountView.accountDesc}" style="width: 186px;" />	                                                                 
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Account Tab - End -->
														
																				
														                                                        
                                                        <!-- Attachments Tab - Start -->
														<rich:tab label="#{msg['contract.attachmentTabHeader']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<!-- Attachments Tab - End -->
														
														<!-- Comments Tab - Start -->
                                                         <rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														<!-- Comments Tab - End -->
																												
												</rich:tabPanel>
												
												<table cellpadding="0" cellspacing="0"  style="width:100%;">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
												</table>
												
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td colspan="6" class="BUTTON_TD">
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$accountManage.onSave}"
																	rendered="#{! (pages$accountManage.isPopupViewOnlyMode || pages$accountManage.isViewMode)}">
															</h:commandButton>
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.reset']}"
																	action="#{pages$accountManage.onReset}"
																	rendered="#{! (pages$accountManage.isPopupViewOnlyMode || pages$accountManage.isViewMode) && !pages$accountManage.isEditMode}">
															</h:commandButton>															
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	action="#{pages$accountManage.onCancel}"
																	rendered="#{! pages$accountManage.isPopupViewOnlyMode}">
															</h:commandButton>	
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	onclick="closeWindow();"
																	rendered="#{pages$accountManage.isViewMode}">
															</h:commandButton>	
																																												
														</td>
													</tr>
												</table>
											</div>								
								
									    </h:form>
									</div>
								

									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
           </div>
		</body>
	</html>
</f:view>
