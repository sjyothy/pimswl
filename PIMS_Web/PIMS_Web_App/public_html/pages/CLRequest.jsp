
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript" src="../resources/jscripts/commons.js"></script>
<script language="javascript" type="text/javascript">
	
	function populateContract(contractNumber,contractId)
	{
     
        document.getElementById("cLRForm:hdnContractId").value=contractId;
	    document.getElementById("cLRForm:hdnContactNumber").value=contractNumber;
	  
     }
     
    function   showContractPopup(clStatus)
		{
	     // alert("In show PopUp");
	     var screen_width = 1024;
         var screen_height = 470;

		    
		   window.open('ContractListPopUp.jsf?clStatus='+clStatus,'_blank','width='+(screen_width-220)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
		    
		}  
		
		
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
	    
	    //alert("in populatePerson");
	    
	    document.getElementById("cLRForm:hdnPersonId").value=personId;
	    document.getElementById("cLRForm:hdnCellNo").value=cellNumber;
	    document.getElementById("cLRForm:hdnPersonName").value=personName;
	    document.getElementById("cLRForm:hdnPersonType").value=hdnPersonType;
	    document.getElementById("cLRForm:hdnIsCompany").value=isCompany;
		
        document.forms[0].submit();
	}	

	function  showPersonPopup()
	{
				   var screen_width = 1024;
				   var screen_height = 470;
				   window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
						    
	}
	</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />


<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" >
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
				<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table_ff}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
	 </head>
	
	

<body class="BODY_STYLE">
      <div class="containerDiv">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>
<tr width="100%">
    <td class="divLeftMenuWithBody" width="17%">
        <jsp:include page="leftmenu.jsp"/>
    </td>
    <td width="83%" height="505px" valign="top" class="divBackgroundBody">
 <table width="99.2%" class="greyPanelTable" cellpadding="0"
					cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
								<h:outputLabel value="#{pages$CLRequest.screenName}" styleClass="HEADER_FONT"/>
						</td>
						<td width="100%">
							&nbsp;
						</td>
					</tr>
				</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
								<!--Use this below only for desiging forms Please do not touch other sections -->


								<td height="100%" width="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:420px;#height:445px;">
									<h:form id="cLRForm" enctype="multipart/form-data" style="width:98%">
										<h:inputHidden id="reqId" value="#{pages$CLRequest.requestId}"></h:inputHidden>
										<h:inputHidden id="contractId" value="#{pages$CLRequest.contractId}"></h:inputHidden>
										<h:inputHidden id="statusId" value="#{pages$CLRequest.statusId}"></h:inputHidden>
												<h:inputHidden id="tenantId"
													value="#{pages$CLRequest.tenantId}"></h:inputHidden>
												<h:inputHidden id="contractIdForNavigation" value="#{pages$CLRequest.contractIdForNavigationToLeaseContract}"></h:inputHidden>
										<h:inputHidden id="hdnContractId" value="#{pages$CLRequest.hdnContractId}"></h:inputHidden>
										<h:inputHidden id="hdnContactNumber" value="#{pages$CLRequest.hdnContractNumber}"></h:inputHidden>
										<h:inputHidden id="hdnTenantId" value="#{pages$CLRequest.hdnTenantId}"></h:inputHidden>
										<h:inputHidden id="hdnPersonId" value="#{pages$CLRequest.hdnPersonId}"></h:inputHidden>
										<h:inputHidden id="hdnPersonType" value="#{pages$CLRequest.hdnPersonType}"></h:inputHidden>
										<h:inputHidden id="hdnPersonName" value="#{pages$CLRequest.hdnPersonName}"></h:inputHidden>
										<h:inputHidden id="hdnCellNo" value="#{pages$CLRequest.hdnCellNo}"></h:inputHidden>
										<h:inputHidden id="hdnIsCompany" value="#{pages$CLRequest.hdnIsCompany}"></h:inputHidden>
						    
					
						           <div >
										<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
											<tr>
												<td>
													<h:outputText id="successMsg_2" value="#{pages$CLRequest.successMessages}" escape="false"  styleClass="INFO_FONT"/>
													<h:outputText id="errorMsg_1" value="#{pages$CLRequest.errorMessages}"  escape="false" styleClass="ERROR_FONT"/>
												</td>
											</tr>
										</table>
									</div>
											
										 
										
										<table class="TAB_PANEL_MARGIN" width="100%" border="0">

											<tr>
												<td colspan="2">
												<div >
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
													<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
										</table>
													
											
												<rich:tabPanel binding="#{pages$CLRequest.richTabPanel}" style="HEIGHT: 330px;width: 100%">
												<rich:tab id="applicationTab" label="#{msg['commons.tab.applicationDetails']}">
														<%@ include file="ApplicationDetails.jsp"%>
												</rich:tab>
												
												<rich:tab id = "contractDetailTab" label="#{msg['contract.Contract.Details']}">
											<t:div id="divContract" styleClass="TAB_DETAIL_SECTION"> 
												<t:panelGrid cellpadding="1px" width="100%" cellspacing="3px" styleClass="TAB_DETAIL_SECTION_INNER"  columns="4">
										          <t:panelGroup >
											         <t:panelGrid columns="2" cellpadding="1" cellspacing="5">
												     							
												      <h:outputText styleClass="LABEL" value="#{msg['contract.contractNumber']}" />
												        <t:panelGroup>
											         
														   <h:inputText styleClass="READONLY" readonly="true" binding="#{pages$CLRequest.contractNoText}" style="width: 186px; height: 18px"></h:inputText>
														   <h:commandLink  binding="#{pages$CLRequest.populateContract}"  onclick="javascript:showContractPopup('#{pages$CLRequest.clStatus}')" >
														   <h:graphicImage id="populateIcon" style="padding-left:5px;" title="#{msg['commons.populate']}" url="../resources/images/magnifier.gif" />
														  </h:commandLink>
														   <h:commandLink   action="#{pages$CLRequest.btnContract_Click}" >
														    <h:graphicImage style="padding-left:4px;" title="#{msg['commons.view']}" url="../resources/images/app_icons/Lease-contract.png"/>
														  </h:commandLink>
													
													   </t:panelGroup>
													  
													  														
													  <h:outputLabel styleClass="LABEL" value="#{msg['contract.tenantName']}" />
													  
													 <t:panelGroup> 
													  <h:inputText styleClass="READONLY" readonly="true" binding="#{pages$CLRequest.tenantNameText}" style="width: 186px; height: 18px"></h:inputText>
													  <h:commandLink    action="#{pages$CLRequest.btnTenant_Click}" >
														    <h:graphicImage style="padding-left:4px;" title="#{msg['commons.view']}" url="../resources/images/app_icons/Tenant.png"/>
													 </h:commandLink>
													</t:panelGroup>	  
													  
													  <h:outputLabel styleClass="LABEL" value="#{msg['contract.startDate']}" />
													  <h:inputText styleClass="READONLY" readonly="true" binding="#{pages$CLRequest.contractStartDateText}" style="width: 186px; height: 18px"></h:inputText>
													  
													  <h:outputLabel styleClass="LABEL" value="#{msg['cancelContract.tab.unit.propertyname']}" />
													  <h:inputText styleClass="READONLY" readonly="true" binding="#{pages$CLRequest.txtpropertyName}" style="width: 186px; height: 18px"></h:inputText>
													  
													  <h:outputLabel styleClass="LABEL" value="#{msg['unit.unitNumber']}" />
													  <h:inputText styleClass="READONLY"  readonly="true" binding="#{pages$CLRequest.txtunitRefNum}" style="width: 186px; height: 18px"></h:inputText>
													  
													  <h:outputLabel styleClass="LABEL" value="#{msg['unit.rentValue']}" />
													  <h:inputText styleClass="READONLY"  readonly="true" binding="#{pages$CLRequest.txtUnitRentValue}" style="width: 186px; height: 18px"></h:inputText>
													  
												 </t:panelGrid>	  
												</t:panelGroup>
												
												  
											   <t:div style="padding-bottom:21px;#padding-bottom:25px;"> 	
												<t:panelGroup >
												 <t:panelGrid columns="2" cellpadding="1" cellspacing="5">						
												      
												      <h:outputLabel styleClass="LABEL" value="#{msg['contract.contractType']}" />
													  <h:inputText styleClass="READONLY"  readonly="true" binding="#{pages$CLRequest.contractTypeText}" style="width: 186px; height: 18px"></h:inputText>
													  
													  <h:outputLabel styleClass="LABEL" value="#{msg['transferContract.oldTenant.Number']}" />
    												  <h:inputText styleClass="READONLY"  style="width: 186px; height: 18px" readonly="true" binding="#{pages$CLRequest.tenantNumberType}"></h:inputText>
												        
													  <h:outputLabel styleClass="LABEL" value="#{msg['contract.endDate']}" />
													  <h:inputText styleClass="READONLY" readonly="true" binding="#{pages$CLRequest.contractEndDateText}" style="width: 186px; height: 18px"></h:inputText>
																	
													  <h:outputLabel styleClass="LABEL" value="#{msg['property.type']}" />
													  <h:inputText styleClass="READONLY"  readonly="true" binding="#{pages$CLRequest.txtpropertyType}" style="width: 186px; height: 18px"></h:inputText>
													  
													  <h:outputLabel styleClass="LABEL" value="#{msg['commons.replaceCheque.unitType']}" />
													  <h:inputText styleClass="READONLY" readonly="true" binding="#{pages$CLRequest.txtUnitType}" style="width: 186px; height: 18px"></h:inputText>
													  
													
													 </t:panelGrid>
												</t:panelGroup>
											 </t:div>
										  </t:panelGrid> 
										</t:div>																						
								</rich:tab>		
											<rich:tab id="PaymentTab" label="#{msg['cancelContract.tab.payments']}" >

					                           <c:if test="">
												<c:if test="">
											
													<t:div styleClass="A_RIGHT" style="padding:10px">
														<h:commandButton id="generatePayments" styleClass="BUTTON" style="width:125px" action="#{pages$RenewContract.generatePayments}"  value="#{msg['renewContract.generatePayments']}"/>
														<h:commandButton  id="pmt_sch_add" styleClass="BUTTON" value="#{msg['contract.AddpaymentSchedule']}"
				     	    					                onclick="javascript:showPaymentSchedulePopUp();"
												                style="width:150px;"
														/>
													</t:div>	
												
												</c:if>
												</c:if>
													
                                                       
                                        <t:div id="divtt" styleClass="contentDiv" style="width:100%">
													
			                               <t:dataTable id="ttbb" width="100%" value="#{pages$CLRequest.paymentSchedules}" binding="#{pages$CLRequest.dataTablePaymentSchedule}"
													   preserveDataModel="false" preserveSort="false" var="paymentScheduleDataItem" rowClasses="row1,row2" renderedIfEmpty="true">
													   
												
													   
											 
											   <t:column id="col_PaymentNumber" rendered="#{pages$CLRequest.requestStatusForColumn}">
													<f:facet name="header">
														<t:outputText id="ac20" value="#{msg['paymentSchedule.paymentNumber']}"/>
													</f:facet>
													<t:outputText id="ac19" styleClass="A_LEFT" value="#{paymentScheduleDataItem.paymentNumber}"/>
												</t:column>
												
												<t:column id="col_ReceiptNumber" style="width:15%;">
													<f:facet name="header">
														<t:outputText value="#{msg['paymentSchedule.ReceiptNumber']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" title=""
														value="#{paymentScheduleDataItem.paymentReceiptNumber}" />
												</t:column>
												
												
												<t:column id="colTypeEnt" rendered="#{pages$CLRequest.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText id="ac17" value="#{msg['paymentSchedule.paymentType']}"/>
													</f:facet>
													<t:outputText id="ac16" styleClass="A_LEFT" title="" value="#{paymentScheduleDataItem.typeEn}"/>
												</t:column>
												<t:column id="colTypeArt" rendered="#{pages$CLRequest.isArabicLocale}">
													<f:facet name="header">
														<t:outputText id="ac15" value="#{msg['paymentSchedule.paymentType']}"/>
													</f:facet>
													<t:outputText id="ac14" styleClass="A_LEFT" title="" value="#{paymentScheduleDataItem.typeAr}"/>
												</t:column>
												
												<t:column id="colDesc">
													<f:facet name="header">
														<t:outputText id="descHead" value="#{msg['renewContract.description']}"/>
													</f:facet>
													<t:outputText id="descText" styleClass="A_LEFT" title="" value="#{paymentScheduleDataItem.description}"/>
												</t:column>
												
												
												<t:column id="colDueOnt">
													<f:facet name="header">
														<t:outputText id="ac13" value="#{msg['paymentSchedule.paymentDueOn']}"/>
													</f:facet>
													<t:outputText id="ac12" styleClass="A_LEFT" value="#{paymentScheduleDataItem.paymentDueOn}">
													<f:convertDateTime pattern="#{pages$CLRequest.dateFormatForDataTable}" />
													</t:outputText>
												</t:column>
                                               
                                               <t:column id="colStatusEnt" rendered="#{pages$CLRequest.isEnglishLocale}">


													<f:facet name="header">
														<t:outputText id="ac11" value="#{msg['commons.status']}"/>
													</f:facet>
													<t:outputText id="ac10" styleClass="A_LEFT" title="" value="#{paymentScheduleDataItem.statusEn}"/>
												</t:column>
												<t:column id="colStatusArt" rendered="#{pages$CLRequest.isArabicLocale}">
													<f:facet name="header">
														<t:outputText id="ac9" value="#{msg['commons.status']}"/>
													</f:facet>
													<t:outputText id="ac8" styleClass="A_LEFT" title="" value="#{paymentScheduleDataItem.statusAr}"/>
												</t:column>
																								
												<t:column id="colModeEnt" rendered="#{pages$CLRequest.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText id="ac7" value="#{msg['paymentSchedule.paymentMode']}"/>
													</f:facet>
													<t:outputText id="ac6" styleClass="A_LEFT" title="" value="#{paymentScheduleDataItem.paymentModeEn}"/>
												</t:column>
												<t:column id="colModeArt" rendered="#{pages$CLRequest.isArabicLocale}">
													<f:facet name="header">
														<t:outputText id="ac5" value="#{msg['paymentSchedule.paymentMode']}"/>
													</f:facet>
													<t:outputText id="ac4" styleClass="A_LEFT" title="" value="#{paymentScheduleDataItem.paymentModeAr}"/>
												</t:column>
												
												
												<t:column id="colAmountt">
													<f:facet name="header">
														<t:outputText id="ac1" value="#{msg['paymentSchedule.amount']}"/>
													</f:facet>
													<t:outputText id="ac2" styleClass="A_RIGHT_NUM" value="#{paymentScheduleDataItem.amount}"/>
												</t:column>
												
												
											
												 <t:column id="DelPaymentCol" >
												 <f:facet name="header">
															<t:outputText id="ac3" value="#{msg['commons.action']}"/>
												 </f:facet>
												  <h:commandLink action="#{pages$CLRequest.btnEditPaymentSchedule_Click}" rendered="#{pages$CLRequest.editPaymentEnable}">
													    <h:graphicImage id="editPayments" 
																		title="#{msg['commons.edit']}" 
																		url="../resources/images/edit-icon.gif"
																		rendered="#{paymentScheduleDataItem.isReceived == 'N'}"
																		/>
													</h:commandLink>
												 <h:commandLink action="#{pages$CLRequest.btnPrintPaymentSchedule_Click}">
								                    <h:graphicImage id="printPayments" 
													                title="#{msg['commons.print']}" 
													                url="../resources/images/app_icons/print.gif"
													                rendered="#{paymentScheduleDataItem.isReceived == 'Y'}"
													/>
												 </h:commandLink>
												 
												 <h:commandLink action="#{pages$CLRequest.btnViewPaymentDetails_Click}">
													<h:graphicImage id="viewPayments" 
																	title="#{msg['commons.group.permissions.view']}" 
																	url="../resources/images/detail-icon.gif"
													/>
												 </h:commandLink> 
											
												
												  </t:column>
												  
												  
											
																					  
								</t:dataTable>
											</t:div>
       											<t:div>
       											
       											
       										
											
												<t:div id="cmdDiv" styleClass="A_RIGHT" style="padding:10px">
												<h:commandButton id="btnCollectPayment" value="#{msg['settlement.actions.collectpayment']}" styleClass="BUTTON"  binding ="#{pages$CLRequest.btnCollectPayment}"     action="#{pages$CLRequest.openReceivePaymentsPopUp}" style="width: 119px"/>
												</t:div>
												
											
											  <t:panelGrid columns="4" cellpadding="1px" cellspacing="5px">
											   
									     
											 
											 											  
											  </t:panelGrid>
											  </t:div>
												</rich:tab>
								 <rich:tab id="requestHistoryTab" label="#{msg['commons.tab.requestHistory']}" 
												                        action="#{pages$CLRequest.tabRequestHistory_Click}">
					                                                  <t:div >
					                                                   <%@ include file="../pages/requestTasks.jsp"%>
					                                                   </t:div>
										 </rich:tab>
                                                 
                                                 <rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}">
															<%@  include file="attachment/attachment.jsp"%>
												 </rich:tab>
												 <rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
												</rich:tab>
										</rich:tabPanel>
											
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
													<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>
												</div>
												</td>
											</tr>
											<tr>
												<td class="BUTTON_TD MARGIN" colspan="8">												
													<h:commandButton styleClass="BUTTON" 
													binding="#{pages$CLRequest.rejectButton}"
													action="#{pages$CLRequest.reject}"
													onclick="return confirm('#{msg['clearanceLetter.message.confirmReject']}');"></h:commandButton>
													
													<h:commandButton styleClass="BUTTON" 
													binding="#{pages$CLRequest.approveButton}"
													action="#{pages$CLRequest.approve}"
													onclick="return confirm('#{msg['clearanceLetter.message.confirmApprove']}');"></h:commandButton>
													
													<h:commandButton styleClass="BUTTON" 
													binding="#{pages$CLRequest.saveButton}"
													action="#{pages$CLRequest.saveRequest}"></h:commandButton>
													
													<h:commandButton styleClass="BUTTON" 
													binding="#{pages$CLRequest.sendButton}"
													action="#{pages$CLRequest.send}"
													onclick="return confirm('#{msg['clearanceLetter.message.confirmSend']}');"></h:commandButton>
													
													<h:commandButton styleClass="BUTTON" style="width:auto;"
													binding="#{pages$CLRequest.completeRequest}"
													action="#{pages$CLRequest.completed}"
													onclick="return confirm('#{msg['clearanceLetter.message.confirmComplete']}');"></h:commandButton>
													
													<h:commandButton styleClass="BUTTON" style="width:auto;"
													binding="#{pages$CLRequest.cancelRequest}"
													action="#{pages$CLRequest.canceled}"
													onclick="return confirm('#{msg['clearanceLetter.message.confirmCancel']}');"></h:commandButton>
													
													
													<h:commandButton styleClass="BUTTON" style="width:auto;" 
													binding="#{pages$CLRequest.reprintButton}"
													action="#{pages$CLRequest.rePrintReceipt}"></h:commandButton>
													
													<h:commandButton styleClass="BUTTON" 
													binding="#{pages$CLRequest.cancelButton}"
													action="#{pages$CLRequest.cancel}"></h:commandButton>												
												</td>
											</tr>
										</table>
									</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
	</div>
</html>
</f:view>


