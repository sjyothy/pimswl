<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	function performTabClick(elementid)
	{
			
		disableInputs();
		
		document.getElementById("detailsFrm:"+elementid).onclick();
		
		return 
		
	}
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
	}	
	
	function onMessageFromSearchEndowments()
	{
	  
	  disableInputs();
	  document.getElementById("detailsFrm:onMessageFromSearchEndowments").onclick();
	}
	
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		disableInputs();	
		if( hdnPersonType=='APPLICANT' ) 
		{			    			 
			 document.getElementById("detailsFrm:hdnPersonId").value=personId;
			 document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
			 document.getElementById("detailsFrm:hdnPersonName").value=personName;
			 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
			 document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;
		}
				
	     document.getElementById("detailsFrm:onMessageFromSearchPerson").onclick();
	}
	
	
	function onMessageFromDistributeEndowmentRevenue()
	{
	  
	  disableInputs();
	  document.getElementById("detailsFrm:onMessageFromDistributeEndowmentRevenue").onclick();
	}
	 
	
		
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{pages$thirdPartyRevenue.pageTitle}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>

																<h:outputText id="errorId"
																	value="#{pages$thirdPartyRevenue.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$thirdPartyRevenue.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$thirdPartyRevenue.pageMode}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$thirdPartyRevenue.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$thirdPartyRevenue.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$thirdPartyRevenue.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$thirdPartyRevenue.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$thirdPartyRevenue.hdnIsCompany}"></h:inputHidden>

																<h:commandLink id="onMessageFromSearchPerson"
																	action="#{pages$thirdPartyRevenue.onMessageFromSearchPerson}" />

																<h:commandLink id="onMessageFromSearchEndowments"
																	action="#{pages$thirdPartyRevenue.onMessageFromSearchEndowments}" />

																<h:commandLink
																	id="onMessageFromDistributeEndowmentRevenue"
																	action="#{pages$thirdPartyRevenue.onMessageFromDistributeEndowmentRevenue}" />

															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<t:div rendered="true" style="width:100%;">
														<t:panelGrid cellpadding="1px" width="100%"
															cellspacing="5px" columns="4">

															<h:outputLabel styleClass="LABEL"
																value="#{msg['maintenanceRequest.remarks']}"
																rendered="#{pages$thirdPartyRevenue.showCollectButton}" />
															<t:panelGroup colspan="6" style="width: 100%">
																<t:inputTextarea style="width: 612px;" id="txt_remarks"
																	rendered="#{pages$thirdPartyRevenue.showCollectButton}"
																	value="#{pages$thirdPartyRevenue.txtRemarks}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</t:panelGroup>
														</t:panelGrid>
													</t:div>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$thirdPartyRevenue.tabPanel}"
																style="width: 100%">

																<rich:tab id="applicationTab"
																	label="#{msg['commons.tab.applicationDetails']}">
																	<%@ include file="ApplicationDetails.jsp"%>
																</rich:tab>
																<rich:tab id="paymentsTab"
																	label="#{msg['thirdPartRevenue.tab.revenueDetails']}">
																	<%@ include file="tabThirdPartyRevenueDetails.jsp"%>
																</rich:tab>
																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>
																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>
																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$thirdPartyRevenue.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">
															<pims:security
																screen="Pims.EndowMgmt.ThirdPartyRevenue.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$thirdPartyRevenue.showSaveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$thirdPartyRevenue.onSave}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$thirdPartyRevenue.showSubmitButton}"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSubmit"
																	action="#{pages$thirdPartyRevenue.onSubmit}" />
															</pims:security>
															<pims:security
																screen="Pims.EndowMgmt.ThirdPartyRevenue.Resubmit"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$thirdPartyRevenue.showResubmitButton}"
																	value="#{msg['socialResearch.btn.resubmit']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkResubmit"
																	action="#{pages$thirdPartyRevenue.onResubmitted}" />

															</pims:security>

															<pims:security
																screen="Pims.EndowMgmt.ThirdPartyRevenue.Collect"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	binding="#{pages$thirdPartyRevenue.btnCollect}"
																	rendered="#{pages$thirdPartyRevenue.showCollectButton}"
																	value="#{msg['commons.collectAppFee']}"
																	onclick="performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkCollect"
																	action="#{pages$thirdPartyRevenue.onCollect}" />


																<h:commandButton styleClass="BUTTON"
																	binding="#{pages$thirdPartyRevenue.btnCollectionreject}"
																	rendered="#{pages$thirdPartyRevenue.showCollectButton}"
																	value="#{msg['commons.sendBack']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSendBack"
																	action="#{pages$thirdPartyRevenue.onRejected}" />
															</pims:security>

															<pims:security
																screen="Pims.EndowMgmt.ThirdPartyRevenue.DistributeAndComplete"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$thirdPartyRevenue.showComplete}"
																	value="#{msg['commons.complete']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkComplete"
																	action="#{pages$thirdPartyRevenue.onComplete}" />

															</pims:security>
															
															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$thirdPartyRevenue.showPrintReceiptButton}"
																value="#{msg['commons.print']}"
																action="#{pages$thirdPartyRevenue.onPrintReceipt}" />
														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

		</body>
	</html>
</f:view>