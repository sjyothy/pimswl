<%-- 
  - Author: Wasif Kirmani
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Financial Transaction
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	    
		function onChkChangeStart( changedChkBox )
		{
		    
		    
	        document.getElementById('searchFrm:onClickMarkUnMark').onclick();
			document.getElementById('searchFrm:disablingDiv').style.display='block';
			
		}
		function  onChkChangeFinish ()
		{
			document.getElementById('searchFrm:disablingDiv').style.display='none';
		}
		function resetValues()
      	{
		
			var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'text' ) 
			        )
			     {
			        inputs[i].value = "";
			    }
			}
		
			
		
      		var inputs = document.getElementsByTagName("SELECT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null )
			     {
			        inputs[i].selectedIndex=0;
			    }
			}
		    
			
			
			
        }
        
        function showPostingVerification(){
              var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width;
		      var popup_height = screen_height/2-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('verifyGRPPosting.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		
        }
        function showPopup(personType)
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   //window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
	    window.open('SearchPerson.jsf?persontype='+personType+'&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		   document.getElementById("searchFrm:tenantName").value=personName;
	}
        
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<%
			response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
				response.setHeader("Pragma", "no-cache"); //HTTP 1.0
				response.setDateHeader("Expires", 0); //prevents caching at the proxy serverateHeader ("Expires", 0); //prevents caching at the proxy server
		%>
		<!-- Header -->
		<body class="BODY_STYLE">

			<div class="containerDiv">
				<div id="disablingDiv" styleClass="disablingDiv"></div>

				<table width="100%" cellpadding="0" cellspacing="0" border="0">


					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>


					<tr width="100%">

						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="83%" height="470px" valign="top"
							class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['grp.search.heading']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" height="100%">

										<div class="SCROLLABLE_SECTION AUC_SCH_SS"
											style="height: 95%; width: 100%; # height: 95%; # width: 100%;">
											<h:form id="searchFrm"
												style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
												<h:inputHidden id="hdntenantId"
													value="#{pages$grpPosting.hdnTenantId}" />
												<t:div styleClass="MESSAGE">
													<table border="0" class="layoutTable">
														<tr>
															<td>
																<h:outputText value="#{pages$grpPosting.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText
																	value="#{pages$grpPosting.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<a4j:commandLink id="onClickMarkUnMark"
																	reRender="dataTableDiv,contentDiv,dt1"
																	onbeforedomupdate="javaScript:onChkChangeFinish(); "
																	action="#{pages$grpPosting.chkMarkUnMark_Changed}" />
															</td>
														</tr>
													</table>
												</t:div>

												<div class="MARGIN">

													<table cellpadding="0" cellspacing="0" width="100%">

														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="DETAIL_SECTION">
														<h:outputLabel value="#{msg['commons.searchCriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER">
															<tr>

																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.paymentReceiptId']}:"></h:outputLabel>
																</td>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:inputText id="paymentReceiptId"
																		binding="#{pages$grpPosting.paymentReceiptId}"
																		style="width: 186px;" maxlength="20">
																	</h:inputText>
																</td>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.BankName']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="bankName"
																		binding="#{pages$grpPosting.htmlInputTextBankName}"
																		style="width: 186px;" maxlength="20">
																	</h:inputText>
																</td>

															</tr>


															<tr>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.paymentReceiptDateFrom']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<rich:calendar id="receiptDateFrom" popup="true"
																		binding="#{pages$grpPosting.htmlCalendarReceiptDateFrom}"
																		datePattern="#{pages$grpPosting.shortDateFormat}"
																		showApplyButton="false"
																		locale="#{pages$grpPosting.currentLocale}"
																		enableManualInput="false" cellWidth="24px"
																		cellHeight="22px" />
																</td>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.paymentReceiptDateTo']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<rich:calendar id="receiptDateTo" popup="true"
																		binding="#{pages$grpPosting.htmlCalendarReceiptDateTo}"
																		datePattern="#{pages$grpPosting.shortDateFormat}"
																		showApplyButton="false"
																		locale="#{pages$grpPosting.currentLocale}"
																		enableManualInput="false" cellWidth="24px"
																		cellHeight="22px" />
																</td>
															</tr>

															<tr>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.PaymentMethod']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:selectOneMenu id="paymentMethodMenu"
																		binding="#{pages$grpPosting.htmlSelectOnePaymentMethods}"
																		style="width: 192px;" required="false">
																		<f:selectItem itemValue="0"
																			itemLabel="#{msg['commons.All']}" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.paymentMethodsFromPMTable}" />
																	</h:selectOneMenu>
																</td>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.paymentType']}:"></h:outputLabel>
																</td>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:selectOneMenu id="paymentTypeMenu"
																		binding="#{pages$grpPosting.paymentTypeMenu}"
																		style="width: 192px;" required="false">
																		<f:selectItem itemValue="0"
																			itemLabel="#{msg['commons.All']}" />
																		<f:selectItems
																			value="#{pages$grpPosting.paymentTypeList}" />
																	</h:selectOneMenu>
																</td>

															</tr>






															<tr>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.requestRefNumber']}:"></h:outputLabel>
																</td>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:inputText id="requestNo"
																		binding="#{pages$grpPosting.requestRefNo}"
																		style="width: 186px;" maxlength="20">
																	</h:inputText>
																</td>

																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.contractRefNumber']}:"></h:outputLabel>
																</td>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:inputText id="contractNo"
																		binding="#{pages$grpPosting.contractRefNo}"
																		style="width: 186px;" maxlength="20">
																	</h:inputText>
																</td>
															</tr>

															<tr>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.contractDateFrom']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<rich:calendar id="contractDateFrom" popup="true"
																		binding="#{pages$grpPosting.htmlCalendarContractFrom}"
																		datePattern="#{pages$grpPosting.shortDateFormat}"
																		showApplyButton="false"
																		locale="#{pages$grpPosting.currentLocale}"
																		enableManualInput="false" cellWidth="24px"
																		cellHeight="22px" />
																</td>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.contractDateTo']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<rich:calendar id="contractDatTo" popup="true"
																		binding="#{pages$grpPosting.htmlCalendarContractTo}"
																		datePattern="#{pages$grpPosting.shortDateFormat}"
																		showApplyButton="false"
																		locale="#{pages$grpPosting.currentLocale}"
																		enableManualInput="false" cellWidth="24px"
																		cellHeight="22px" />
																</td>
															</tr>

															<tr>

																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.TenantName']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:panelGroup>
																		<h:inputText id="tenantName"
																			binding="#{pages$grpPosting.htmlInputTextTenantName}"
																			style="width: 186px;" maxlength="20">
																		</h:inputText>
																		<h:graphicImage
																			url="../resources/images/app_icons/Search-tenant.png"
																			onclick="showPopup('#{pages$grpPosting.personTenant}');"
																			style="MARGIN: 0px 0px -4px; CURSOR: hand;"
																			alt="#{msg[contract.searchTenant]}"></h:graphicImage>
																	</h:panelGroup>
																</td>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['auction.number']}:"></h:outputLabel>
																</td>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:inputText id="auctionNumber"
																		binding="#{pages$grpPosting.htmlAuctionNumber}"
																		style="width: 186px;" maxlength="20">
																	</h:inputText>
																</td>

															</tr>





															<tr>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.propertyRefNumber']}:"></h:outputLabel>
																</td>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:inputText id="propertyRefNumber"
																		binding="#{pages$grpPosting.propertyRefNo}"
																		style="width: 186px;" maxlength="20">
																	</h:inputText>
																</td>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.propertyName']}:"></h:outputLabel>
																</td>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:inputText id="propertyName"
																		binding="#{pages$grpPosting.propertyName}"
																		style="width: 186px;" maxlength="20">
																	</h:inputText>
																</td>


															</tr>



															<tr>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.ownerShipMenu']}:"></h:outputLabel>
																</td>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:selectOneMenu id="ownerShipMenu"
																		binding="#{pages$grpPosting.ownerShipMenu}"
																		style="width: 192px;" required="false">
																		<f:selectItem itemValue="0"
																			itemLabel="#{msg['commons.All']}" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.propertyOwnershipTypeExcludingThirdParty}" />
																	</h:selectOneMenu>
																</td>

																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.UnitNumber']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="unitNumber"
																		binding="#{pages$grpPosting.htmlInputTextUnitNumber}"
																		style="width: 186px;" maxlength="20">
																	</h:inputText>
																</td>

															</tr>


															<tr>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.postingDateFrom']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<rich:calendar id="postingDateFrom" popup="true"
																		binding="#{pages$grpPosting.htmlPostingDateFrom}"
																		datePattern="#{pages$grpPosting.shortDateFormat}"
																		showApplyButton="false"
																		locale="#{pages$grpPosting.currentLocale}"
																		enableManualInput="false" cellWidth="24px"
																		cellHeight="22px" />
																</td>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.postingDateTo']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<rich:calendar id="postingDateTo" popup="true"
																		binding="#{pages$grpPosting.htmlPostingDateTo}"
																		datePattern="#{pages$grpPosting.shortDateFormat}"
																		showApplyButton="false"
																		locale="#{pages$grpPosting.currentLocale}"
																		enableManualInput="false" cellWidth="24px"
																		cellHeight="22px" />
																</td>
															</tr>

															<tr>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.trxNumber']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="grpRctNumber"
																		binding="#{pages$grpPosting.htmlInputTextGRPRcptNumber}"
																		style="width: 186px;" maxlength="20">
																	</h:inputText>
																</td>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.PostedBy']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="postedBy"
																		binding="#{pages$grpPosting.htmlInputTextPostedBy}"
																		style="width: 186px;" maxlength="20">
																	</h:inputText>
																</td>
															</tr>
															<tr>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.transactionTypeMenu']}:"></h:outputLabel>
																</td>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:selectOneMenu id="fintransactionTypeMenu"
																		binding="#{pages$grpPosting.transactionTypeMenu}"
																		style="width: 192px;" required="false">
																		<f:selectItem itemValue="0"
																			itemLabel="#{msg['commons.All']}" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.finTranType}" />
																	</h:selectOneMenu>
																</td>

																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['grp.TransactionStatus']}:"></h:outputLabel>
																</td>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:selectOneMenu id="grpStatus"
																		binding="#{pages$grpPosting.finTrxMenu}"
																		style="width: 192px;" required="false">
																		<f:selectItem itemValue="0"
																			itemLabel="#{msg['commons.All']}" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.GRPStatus}" />
																	</h:selectOneMenu>
																</td>
															</tr>
															<TR>

																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['searchInheritenceFile.fileNo']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="fileNoCriteria"
																		binding="#{pages$grpPosting.htmlInheritanceFileNumber}"
																		style="width: 186px;" maxlength="20">
																	</h:inputText>
																</td>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['searchInheritenceFile.filePerson']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="filePersonCriteria"
																		binding="#{pages$grpPosting.htmlInheritanceFilePerson}"
																		style="width: 186px;" maxlength="20">
																	</h:inputText>
																</td>
															</TR>
															<TR>
																<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['bouncedChequesList.chequeNumberCol']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="chequeCriteria"
																		binding="#{pages$grpPosting.htmlInputTextchequeNumber}"
																		style="width: 186px;" maxlength="20">
																	</h:inputText>
																</td>
																	<td
																	style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['settlement.label.costCenter']}:"></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="costCenterCriteria"
																		binding="#{pages$grpPosting.txtCostCenter}"
																		style="width: 186px;" >
																	</h:inputText>
																</td>

															</TR>
															<tr>
																<td class="BUTTON_TD JUG_BUTTON_TD" colspan="4">
																	<table cellpadding="1px" cellspacing="1px">
																		<tr>
																			<td>

																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.search']}"
																					action="#{pages$grpPosting.searchTransactions}"
																					style="width: 80px" tabindex="7"></h:commandButton>
																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['tabFinance.btn.AddSupplier']}"
																					action="#{pages$grpPosting.onAddGRPSuppliers}"
																					binding="#{pages$grpPosting.btnAddSupplier}"
																					style="width: 80px" tabindex="7"></h:commandButton>
																				<pims:security
																					screen="Pims.GRP.Transaction.SearchButton"
																					action="create">
																				</pims:security>
																			</td>
																			<td>
																				<h:commandButton styleClass="BUTTON"
																					value="#{msg['commons.clear']}" type="button"
																					onclick="javascript:resetValues();"
																					style="width: 80px" tabindex="7"></h:commandButton>

																			</td>

																		</tr>
																	</table>
																</td>
															</tr>

														</table>
													</div>
												</div>
												<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
												<div id="dataTableDiv"
													style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">

													<div class="imag">
														&nbsp;
													</div>
													<div class="contentDiv" style="width: 100%; # width: 99%;">

														<t:dataTable id="dt1"
															value="#{pages$grpPosting.transactionsDataList}"
															binding="#{pages$grpPosting.transactionsGrid}"
															
															preserveDataModel="false" preserveSort="false"
															var="dataItem" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true" width="100%">
															<t:column id="selectTrans" width="5%">
																<f:facet name="header">
																	<h:selectBooleanCheckbox id="markUnMarkAll"
																		onclick="onChkChangeStart(this);"
																		value="#{pages$grpPosting.markUnMarkAll}" />

																</f:facet>

																<h:selectBooleanCheckbox id="select"
																	rendered="#{dataItem.showChkBox ==1}"
																	value="#{dataItem.selected}" />
															</t:column>
															<t:column id="Id" width="12%">
																<f:facet name="header">
																	<t:outputText style="white-space: normal;"
																		value="#{msg['grp.financiaTtransactionNo']}" />
																</f:facet>
																<t:outputText style="white-space: normal;"
																	value="#{dataItem.trxNumber}" />

															</t:column>
															<t:column id="chequeNumber" width="12%" sortable="true">

																<f:facet name="header">
																	<t:outputText style="white-space: normal;"
																		value="#{msg['bouncedChequesList.chequeNumberCol']}" />
																</f:facet>
																<t:outputText style="white-space: normal;"
																	value="#{dataItem.chequeNumber}" />

															</t:column>
															<t:column id="paymentTypeId" width="10%" sortable="true"
																style="white-space: normal;">
																<f:facet name="header">
																	<t:commandSortHeader columnName="paymentTypeId"
																		actionListener="#{pages$grpPosting.sort}"
																		value="#{msg['grp.paymentType']}" arrow="true">
																		<f:attribute name="sortField" value="paymentTypeId" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText style="white-space: normal;"
																	value="#{dataItem.paymentTypeEn}"
																	rendered="#{pages$grpPosting.isEnglishLocale}" />
																<t:outputText style="white-space: normal;"
																	value="#{dataItem.paymentTypeAr}"
																	rendered="#{pages$grpPosting.isArabicLocale}" />
															</t:column>

															<t:column id="receiptCustomerCol" width="12%"
																sortable="true"
																rendered="#{!pages$grpPosting.isAPSearch}">
																<f:facet name="header">
																	<t:outputText style="white-space: normal;"
																		value="#{msg['grp.customerName']}" />
																</f:facet>
																<t:outputText style="white-space: normal;"
																	value="#{dataItem.personFullName}" />
															</t:column>
															<t:column id="costCenter" width="12%"
																sortable="true"
																>
																<f:facet name="header">
																	<t:outputText style="white-space: normal;"
																		value="#{msg['settlement.label.costCenter']}" />
																</f:facet>
																<t:outputText style="white-space: normal;"
																	value="#{dataItem.location}" />
															</t:column>
															<t:column id="trxType" width="10%" sortable="true"
																style="white-space: normal;">
																<f:facet name="header">
																	<t:outputText style="white-space: normal;"
																		value="#{msg['grp.transactionTypeMenu']}" />
																</f:facet>
																<t:outputText style="white-space: normal;"
																	value="#{dataItem.trxTypeEn}"
																	rendered="#{pages$grpPosting.isEnglishLocale}" />
																<t:outputText style="white-space: normal;"
																	value="#{dataItem.trxTypeAr}"
																	rendered="#{pages$grpPosting.isArabicLocale}" />
															</t:column>
															<t:column id="grpAmount" width="10%" sortable="true">
																<f:facet name="header">
																	<t:outputText style="white-space: normal;"
																		value="#{msg['grp.amount']}" />
																</f:facet>
																<t:outputText style="white-space: normal;"
																	value="#{dataItem.amountToShow}">
																	<f:convertNumber minFractionDigits="2"
																		maxFractionDigits="2" pattern="##,###,###.##" />
																</t:outputText>
															</t:column>
															<t:column id="paymentMethodCol" width="10%"
																sortable="true">
																<f:facet name="header">
																	<t:outputText style="white-space: normal;"
																		value="#{msg['grp.paymentMethod']}" />
																</f:facet>
																<t:outputText style="white-space: normal;"
																	value="#{dataItem.paymentMethodName}" />
															</t:column>

															<t:column id="ownerShipCol" width="10%" sortable="true">
																<f:facet name="header">
																	<t:outputText style="white-space: normal;"
																		value="#{msg['grp.ownership']}" />
																</f:facet>
																<t:outputText style="white-space: normal;"
																	value="#{dataItem.ownerShipTypeEn}"
																	rendered="#{pages$grpPosting.isEnglishLocale}" />
																<t:outputText value="#{dataItem.ownerShipTypeAr}"
																	rendered="#{pages$grpPosting.isArabicLocale}" />
															</t:column>
															<t:column id="grpStatus" width="16%" sortable="true">
																<f:facet name="header">
																	<t:outputText style="white-space: normal;"
																		value="#{msg['grp.TransactionStatus']}" />
																</f:facet>
																<t:outputText style="white-space: normal;"
																	value="#{dataItem.transactionStatusEn}"
																	rendered="#{pages$grpPosting.isEnglishLocale}" />
																<t:outputText value="#{dataItem.transactionStatusAr}"
																	rendered="#{pages$grpPosting.isArabicLocale}" />

															</t:column>

														</t:dataTable>

													</div>
													<t:div id="contentDivFooter"
														styleClass="contentDivFooter AUCTION_SCH_RF"
														style="width:100%;#width:100%;">
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td class="RECORD_NUM_TD">
																	<div class="RECORD_NUM_BG">
																		<table cellpadding="0" cellspacing="0"
																			style="width: 182px; # width: 150px;">
																			<tr>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText value=" : " />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{pages$grpPosting.recordSize}" />
																				</td>
																			</tr>
																		</table>
																	</div>
																</td>
																<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																	align="right">

																	<t:div styleClass="PAGE_NUM_BG" style="#width:20%">
																		<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																		<table cellpadding="0" cellspacing="0">
																			<tr>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																				</td>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{pages$grpPosting.currentPage}" />
																				</td>
																			</tr>
																		</table>
																	</t:div>
																	<TABLE border="0" class="SCH_SCROLLER">
																		<tr>
																			<td>
																				<t:commandLink
																					action="#{pages$grpPosting.pageFirst}"
																					disabled="#{pages$grpPosting.firstRow == 0}">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$grpPosting.pagePrevious}"
																					disabled="#{pages$grpPosting.firstRow == 0}">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:dataList value="#{pages$grpPosting.pages}"
																					var="page">
																					<h:commandLink value="#{page}"
																						actionListener="#{pages$grpPosting.page}"
																						rendered="#{page != pages$grpPosting.currentPage}" />
																					<h:outputText value="<b>#{page}</b>" escape="false"
																						rendered="#{page == pages$grpPosting.currentPage}" />
																				</t:dataList>
																			</td>
																			<td>
																				<t:commandLink action="#{pages$grpPosting.pageNext}"
																					disabled="#{pages$grpPosting.firstRow + pages$grpPosting.rowsPerPage >= pages$grpPosting.totalRows}">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink action="#{pages$grpPosting.pageLast}"
																					disabled="#{pages$grpPosting.firstRow + pages$grpPosting.rowsPerPage >= pages$grpPosting.totalRows}">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblL"></t:graphicImage>
																				</t:commandLink>
																			</td>
																		</tr>
																	</TABLE>

																</td>
															</tr>
														</table>
													</t:div>
												</div>

												<table width="100%">
													<tr>
														<td class="BUTTON_TD">


															<h:commandButton styleClass="BUTTON" id="postData"
																value="#{msg['commons.post']}"
																action="#{pages$grpPosting.postData}"
																style="width: 80px" tabindex="7"></h:commandButton>
															
															<h:commandButton styleClass="BUTTON" id="postAllData"
																value="#{msg['commons.postAllData']}"
																action="#{pages$grpPosting.postAllData}"
																style="width: 80px" tabindex="7"></h:commandButton>
														</td>

													</tr>
												</table>
										</div>

										</h:form>

										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			</div>
		</body>
	</html>
</f:view>