<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
	}	
		
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		disableInputs();	
		if( hdnPersonType=='APPLICANT' ) 
		{			    			 
			 document.getElementById("detailsFrm:hdnPersonId").value=personId;
			 document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
			 document.getElementById("detailsFrm:hdnPersonName").value=personName;
			 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
			 document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;
		}
				
	     document.getElementById("detailsFrm:onMessageFromSearchPerson").onclick();
	} 
	
	function onBenefChangeStart()
	{
	    document.getElementById('detailsFrm:disablingDiv').style.display='block';
		document.getElementById('detailsFrm:lnkBenefChange').onclick();
	}
	function onBenefChangeComplete()
	{
	 document.getElementById('detailsFrm:disablingDiv').style.display='none';
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['minorStatusChange.heading']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">
												<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>

																<h:outputText id="errorId"
																	value="#{pages$MinorStatusChange.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$MinorStatusChange.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$MinorStatusChange.pageMode}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$MinorStatusChange.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$MinorStatusChange.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$MinorStatusChange.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$MinorStatusChange.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$MinorStatusChange.hdnIsCompany}"></h:inputHidden>

																<h:commandLink id="onMessageFromSearchPerson"
																	action="#{pages$MinorStatusChange.onMessageFromSearchPerson}" />
																<h:commandLink id="lnkBenefChange"
																	action="#{pages$MinorStatusChange.onBeneficiaryChanged}" />

															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<t:div rendered="true" style="width:100%;">


														<t:panelGrid cellpadding="1px" width="100%"
															cellspacing="5px" columns="4">

															<!-- Top Fields - Start -->
															<h:outputLabel styleClass="LABEL"
																value="#{msg['searchInheritenceFile.fileNo']}:" />
															<h:inputText styleClass="READONLY" readonly="true"
																value="#{pages$MinorStatusChange.requestView.inheritanceFileView.fileNumber}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['searchInheritenceFile.fileType']}:" />
															<h:inputText styleClass="READONLY" readonly="true"
																value="#{pages$MinorStatusChange.englishLocale ? 
																					pages$MinorStatusChange.requestView.inheritanceFileView.fileTypeEn : 
																					pages$MinorStatusChange.requestView.inheritanceFileView.fileTypeAr}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.status']}:" />
															<h:inputText styleClass="READONLY" readonly="true"
																value="#{pages$MinorStatusChange.englishLocale ? 
																						pages$MinorStatusChange.requestView.inheritanceFileView.statusEn : 
																						pages$MinorStatusChange.requestView.inheritanceFileView.statusAr}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.createdBy']}:" />
															<h:inputText styleClass="READONLY" readonly="true"
																value="#{pages$MinorStatusChange.requestView.inheritanceFileView.createdBy}" />


															<h:outputLabel styleClass="LABEL"
																rendered="#{pages$MinorStatusChange.statusApprovalRequired ||
																			pages$MinorStatusChange.statusReviewDone  
																		}"
																value="#{msg['mems.investment.label.sendTo']}:" />
															<t:panelGroup
																rendered="#{pages$MinorStatusChange.statusApprovalRequired ||
																			pages$MinorStatusChange.statusReviewDone 
																		   }">
																<h:selectOneMenu
																	binding="#{pages$MinorStatusChange.cmbReviewGroup}">
																	<f:selectItem itemLabel="#{msg['commons.select']}"
																		itemValue="" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.allUserGroups}" />
																</h:selectOneMenu>
															</t:panelGroup>
															<h:outputLabel styleClass="LABEL"
																rendered="#{pages$MinorStatusChange. statusApprovalRequired ||
																			pages$MinorStatusChange.statusReviewDone 
																		}"
																value="" />
															<h:outputLabel styleClass="LABEL"
																rendered="#{pages$MinorStatusChange. statusApprovalRequired ||
																			pages$MinorStatusChange.statusReviewDone 
																		}"
																value="" />
															<h:outputLabel styleClass="LABEL"
																value="#{msg['maintenanceRequest.remarks']}"
																rendered="#{
																				pages$MinorStatusChange.showCompleteButton  ||
																				pages$MinorStatusChange.showResubmitButton
																			}" />
															<t:panelGroup colspan="3" style="width: 100%">
																<t:inputTextarea style="width: 90%;" id="txt_remarks"
																	rendered="#{pages$MinorStatusChange.showCompleteButton  ||
																				pages$MinorStatusChange.showResubmitButton}"
																	value="#{pages$MinorStatusChange.txtRemarks}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</t:panelGroup>

															<h:outputLabel value="" />
															<h:outputLabel value="" />
															<!-- Top Fields - End -->
														</t:panelGrid>
													</t:div>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">

														<div class="TAB_PANEL_INNER">

															<rich:tabPanel
																binding="#{pages$MinorStatusChange.tabPanel}"
																style="width: 100%">

																<rich:tab id="applicationTab"
																	label="#{msg['commons.tab.applicationDetails']}">
																	<%@ include file="ApplicationDetails.jsp"%>
																</rich:tab>
																<!-- Details Tab - Start -->
																<rich:tab id="detailsTab"
																	label="#{msg['commons.details']}">

																	<%@ include file="tabMinorStatusChangeDetails.jsp"%>
																</rich:tab>
																<!-- Details Tab - Finish -->
																<!-- Review Details Tab - Start -->
																<rich:tab id="tabReview"
																	rendered="#{!pages$MinorStatusChange.statusNew}"
																	label="#{msg['commons.tab.reviewDetails']}">
																	<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
																</rich:tab>
																<!-- Review Details Tab - End -->
																<rich:tab id="commentsTab"
																	action="#{pages$MinorStatusChange.onAttachmentsCommentsClick}"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>

																<rich:tab id="attachmentTab"
																	action="#{pages$MinorStatusChange.onAttachmentsCommentsClick}"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>

																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$MinorStatusChange.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>

														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">
															<pims:security
																screen="Pims.MinorMgmt.MinorStatusChange.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$MinorStatusChange.showSaveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$MinorStatusChange.onSave}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$MinorStatusChange.showSubmitButton}"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSubmit"
																	action="#{pages$MinorStatusChange.onSubmit}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$MinorStatusChange.showResubmitButton}"
																	value="#{msg['socialResearch.btn.resubmit']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkResubmit"
																	action="#{pages$MinorStatusChange.onResubmitted}" />
																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$MinorStatusChange.showResubmitButton}"
																	value="#{msg['commons.reject']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkReject"
																	action="#{pages$MinorStatusChange.onRejected}" />


															</pims:security>
															<pims:security
																screen="Pims.MinorMgmt.MinorStatusChange.Complete"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$MinorStatusChange.showCompleteButton}"
																	value="#{msg['commons.complete']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkComplete"
																	action="#{pages$MinorStatusChange.onComplete}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$MinorStatusChange.showCompleteButton}"
																	value="#{msg['commons.sendBack']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSendBack"
																	action="#{pages$MinorStatusChange.onSentBack}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$MinorStatusChange.showCompleteButton}"
																	value="#{msg['commons.reviewButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkReviewButton"
																	action="#{pages$MinorStatusChange.onReview}" />

															</pims:security>

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$MinorStatusChange.showReviewDoneButton}"
																value="#{msg['commons.done']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkReviewDone"
																action="#{pages$MinorStatusChange.onReviewDone}" />


														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>