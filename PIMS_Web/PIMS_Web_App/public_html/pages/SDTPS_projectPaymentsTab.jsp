<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
	<script type="text/javascript">		
	</script>
	<t:div style="width:100%;">
		<t:panelGrid columns="1" cellpadding="0" cellspacing="0">			
			<t:div styleClass="contentDiv" style="width:98%;">
				<t:dataTable id="dtProjectPayments" renderedIfEmpty="true" width="100%" cellpadding="0" cellspacing="0" var="dataItem" 
					binding="#{pages$projectPaymentsTab.dataTable}" value="#{pages$projectPaymentsTab.projectPaymentScheduleViewList}"
				 	preserveDataModel="false" preserveSort="false" rowClasses="row1,row2" rules="all" rows="9">
					
					<t:column sortable="true" width="200" id="paymentNumberCol" defaultSorted="true">
						<f:facet name="header">
							<t:outputText value="#{msg['chequeList.paymentNumber']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.paymentNumber}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="contractNumber" defaultSorted="true">
						<f:facet name="header">
							<t:outputText value="#{msg['bouncedChequesList.contractNumberCol']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.contractNumber}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="paymentTypeEnCol" rendered="#{pages$projectPaymentsTab.isEnglishLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['commons.typeCol']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.typeEn}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="paymentTypeArCol" rendered="#{pages$projectPaymentsTab.isArabicLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['commons.typeCol']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.typeAr}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="paymentSubTypeEnCol" rendered="#{pages$projectPaymentsTab.isEnglishLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['paymentConfiguration.paymentSubType']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.paymentScheduleSubTypeEn}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="paymentSubTypeArCol" rendered="#{pages$projectPaymentsTab.isArabicLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['paymentConfiguration.paymentSubType']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.paymentScheduleSubTypeAr}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="paymentDescCol">
						<f:facet name="header">
							<t:outputText value="#{msg['commons.description']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.description}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="paymentDateCol">
						<f:facet name="header">
							<t:outputText value="#{msg['bouncedChequesList.dueDateCol']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.paymentDueOn}" styleClass="A_LEFT" style="white-space: normal;">
							<f:convertDateTime pattern="#{pages$projectPaymentsTab.dateFormat}" timeZone="#{pages$projectPaymentsTab.timeZone}" />
						</t:outputText>
					</t:column>
					<t:column sortable="true" width="200" id="amountCol">
						<f:facet name="header">
							<t:outputText value="#{msg['payment.amount']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.amount}" styleClass="A_LEFT" style="white-space: normal;">
							<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="#{pages$projectPaymentsTab.numberFormat}"/>
						</t:outputText>
					</t:column>
					<t:column sortable="true" width="200" id="invoiceCol">
						<f:facet name="header">
							<t:outputText value="#{msg['paymentSchedule.InvoiceNumber']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.invoiceNumber}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="statusEnCol" rendered="#{pages$projectPaymentsTab.isEnglishLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['commons.status']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.statusEn}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="statusArCol" rendered="#{pages$projectPaymentsTab.isArabicLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['commons.status']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.statusAr}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
				<t:column id="actionCol" sortable="false" width="50"
					styleClass="ACTION_COLUMN">
					<f:facet name="header">
						<t:outputText value="#{msg['commons.action']}" />
					</f:facet>
					<t:commandLink action="#{pages$projectPaymentsTab.onView}">
						<h:graphicImage id="viewIcon" title="#{msg['commons.view']}"
							url="../resources/images/app_icons/Lease-contract.png" />
					</t:commandLink>
					<t:commandLink action="#{pages$projectPaymentsTab.onAccept}" rendered="#{dataItem.statusId == 36003}"
					onclick="if (!confirm('#{msg['payments.confirmAcceptPayment']}')) return" >
						<h:graphicImage id="acceptIcon" title="Accept"
							url="../resources/images/app_icons/Action.png" />
					</t:commandLink>
				</t:column>

			</t:dataTable>
			</t:div>
			<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99.1%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']}"/>
						<h:outputText value=" : "/>
						<h:outputText value="#{pages$projectPaymentsTab.recordSize}" />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="dtProjectPaymentsScroller" for="dtProjectPayments" paginator="true"  
						fastStep="1" paginatorMaxPages="5" immediate="false"
						paginatorTableClass="paginator"
						renderFacetsIfSinglePage="true" 												
						paginatorTableStyle="grid_paginator" layout="singleTable" 
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" 
						pageIndexVar="projectPaymentsPageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
							<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.projectPaymentsPageNumber}"/>
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
		</t:div>
		</t:panelGrid>
	</t:div>