<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	function onBenefChangeStart()
	{
	    document.getElementById('detailsFrm:disablingDiv').style.display='block';
		document.getElementById('detailsFrm:lnkBenefChange').onclick();
	}
	function onBenefChangeComplete()
	{
	 document.getElementById('detailsFrm:disablingDiv').style.display='none';
	}
	function disableButtons(control)
	{
		var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
		
		control.nextSibling.nextSibling.onclick();
		
	}
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{		    			 
		    document.getElementById("detailsFrm:hdnPersonId").value=personId;
		    document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
		    document.getElementById("detailsFrm:hdnPersonName").value=personName;
		    document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
		    document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;		
	       document.forms[0].submit();
	} 
	function populatePaymentDetails()
	{
	  	document.getElementById("detailsFrm:populatePayments").onclick();	  	 
	}
	function onMessageFromAssociateCostCenter()
	{	
	  	document.getElementById("detailsFrm:onMessageFromAssociateCostCenter").onclick();	  	 
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">

			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['mems.payment.label.title']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">
												<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>


																<h:outputText
																	value="#{pages$PaymentRequests.successMessages}"
																	escape="false" styleClass="INFO_FONT" />

																<h:outputText id="errorMessages"
																	value="#{pages$PaymentRequests.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />

																<a4j:commandLink id="lnkBenefChange"
																	reRender="tabDDetails,errorMessages"
																	onbeforedomupdate="javascript:onBenefChangeComplete();"
																	action="#{pages$PaymentRequests.handleBeneficiaryChange}" />
																<h:inputHidden id="hdnRequestId"
																	value="#{pages$PaymentRequests.hdnRequestId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$PaymentRequests.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$PaymentRequests.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$PaymentRequests.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$PaymentRequests.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$PaymentRequests.hdnIsCompany}"></h:inputHidden>
																<h:commandLink id="onMessageFromAssociateCostCenter"
																	style="width: auto;visibility: hidden;"
																	action="#{pages$PaymentRequests.onMessageFromAssociateCostCenter}">
																</h:commandLink>
															</td>
														</tr>
													</table>
													<table cellpadding="0" cellspacing="6px" width="100%">
														<tr>
															<td class="BUTTON_TD MARGIN" colspan="6">

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['attachment.file']}"
																	action="#{pages$PaymentRequests.showInheritanceFilePopup}"
																	rendered="#{pages$PaymentRequests.isFileAssociated}">
																</h:commandButton>
															</td>
														</tr>


													</table>
													<t:div rendered="true" style="width:100%;">
														<t:panelGrid cellpadding="1px" width="100%"
															cellspacing="5px"
															rendered="#{pages$PaymentRequests.isFileAssociated}"
															columns="4">
															<!-- Top Fields - Start -->
															<h:outputLabel styleClass="LABEL"
																value="#{msg['collectionProcedure.fileNumber']} :" />
															<h:inputText styleClass="READONLY" readonly="true"
																value="#{pages$PaymentRequests.fileNumber}" />


															<h:outputLabel styleClass="LABEL"
																value="#{msg['report.memspaymentreceiptreport.fileOwner']}:" />
															<h:inputText styleClass="READONLY" readonly="true"
																binding="#{pages$PaymentRequests.txtFileOwner}" />


															<h:outputLabel styleClass="LABEL"
																value="#{msg['searchInheritenceFile.fileType']}:" />
															<h:inputText styleClass="READONLY" readonly="true"
																binding="#{pages$PaymentRequests.htmlFileType}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.status']}:" />
															<h:inputText styleClass="READONLY" readonly="true"
																binding="#{pages$PaymentRequests.htmlFileStatus}" />



															<h:outputLabel styleClass="LABEL"
																rendered="#{pages$PaymentRequests.isApprovalRequired ||
																			pages$PaymentRequests.isSuspended || 
																			pages$PaymentRequests.isJudgeSessionDone || 
																			pages$PaymentRequests.isReviewDone ||
																			pages$PaymentRequests.isApproved  ||
																			pages$PaymentRequests.isDisbursementRequired ||
																			pages$PaymentRequests.isFinanceRejected
																		  }"
																value="#{msg['commons.comments']}:" />
															<t:panelGroup
																rendered="#{pages$PaymentRequests.isApprovalRequired ||
																       	pages$PaymentRequests.isSuspended || 
																		pages$PaymentRequests.isJudgeSessionDone || 
																		pages$PaymentRequests.isReviewDone ||
																		pages$PaymentRequests.isApproved   ||
																		pages$PaymentRequests.isDisbursementRequired ||
																		pages$PaymentRequests.isFinanceRejected
																		}">
																<h:inputTextarea
																	binding="#{pages$PaymentRequests.htmlRFC}"
																	style="width: 450px;" />
															</t:panelGroup>

															<h:outputLabel styleClass="LABEL" />
															<h:outputLabel styleClass="LABEL" />

															<h:outputLabel styleClass="LABEL"
																rendered="#{pages$PaymentRequests.isApprovalRequired ||
																		pages$PaymentRequests.isSuspended || 
																		pages$PaymentRequests.isJudgeSessionDone || 
																		pages$PaymentRequests.isReviewDone ||
																		pages$PaymentRequests.isFinanceRejected
																		 
																		}"
																value="#{msg['mems.investment.label.sendTo']}:" />
															<t:panelGroup
																rendered="#{pages$PaymentRequests.isApprovalRequired ||
																		pages$PaymentRequests.isSuspended || 
																		pages$PaymentRequests.isJudgeSessionDone || 
																		pages$PaymentRequests.isReviewDone ||
																		pages$PaymentRequests.isFinanceRejected
																		
																		 
																		}">
																<h:selectOneMenu
																	binding="#{pages$PaymentRequests.cmbReviewGroup}">
																	<f:selectItem itemLabel="#{msg['commons.select']}"
																		itemValue="" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.allUserGroups}" />
																</h:selectOneMenu>

															</t:panelGroup>


															<!-- Top Fields - End -->
														</t:panelGrid>


														<t:panelGrid cellpadding="1px" width="100%"
															cellspacing="5px" columns="4">
															<table border="0" width="100%">
																<tr>
																	<td class="BUTTON_TD">
																		<!-- Top Actions - Start -->
																		<!-- Top Actions - End -->
																	</td>
																</tr>
															</table>
														</t:panelGrid>

													</t:div>
													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$PaymentRequests.richPanel}"
																style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%">

																<rich:tab id="applicationTab"
																	label="#{msg['commons.tab.applicationDetails']}">
																	<%@ include file="ApplicationDetails.jsp"%>
																</rich:tab>

																<rich:tab id="disbDetailsTab"
																	label="#{msg['mems.payment.request.lable.disbDetails']}">
																	<%@ include file="tabDisbursementDetails.jsp"%>
																</rich:tab>

																<rich:tab id="sessionTab"
																	label="#{msg['commons.tab.sessionDetails']}"
																	rendered="#{!pages$PaymentRequests.isNewStatus}">
																	<jsp:include page="sessionDetailsTab.jsp"></jsp:include>
																</rich:tab>

																<rich:tab id="financeTab"
																	label="#{msg['mems.finance.label.tab']}"
																	rendered="#{pages$PaymentRequests.isApproved || pages$PaymentRequests.isDisbursementRequired }">
																	<%@ include file="tabFinance.jsp"%>
																</rich:tab>
																<!-- Review Details Tab - Start -->
																<rich:tab id="reviewTab"
																	rendered="#{!pages$PaymentRequests.isNewStatus }"
																	label="#{msg['commons.tab.reviewDetails']}">
																	<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
																</rich:tab>
																<!-- Review Details Tab - End -->
																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>

																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>

																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$PaymentRequests.getRequestHistory}"
																	rendered="#{!pages$PaymentRequests.isInvalidStatus}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>
															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">

															<h:commandLink id="populatePayments" styleClass="BUTTON"
																value="#{msg['commons.saveButton']}"
																style="width: auto;visibility: hidden;"
																actionListener="#{pages$PaymentRequests.loadPaymentDetails}">
															</h:commandLink>

															<pims:security
																screen="Pims.MinorMgmt.MatureSharePaymentRequest.Create"
																action="create">

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else disableButtons(this);"
																	rendered="#{pages$PaymentRequests.isNewStatus || pages$PaymentRequests.isRejectedResubmitted}">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$PaymentRequests.saveApplication}" />

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSubmit']}')) return false;else disableButtons(this);"
																	rendered="#{pages$PaymentRequests.isNewStatus}">
																</h:commandButton>
																<h:commandLink id="lnkSubmit"
																	action="#{pages$PaymentRequests.submitQuery}" />


																<h:commandButton styleClass="BUTTON"
																	value="#{msg['socialResearch.btn.resubmit']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSubmit']}')) return false;else disableButtons(this);"
																	rendered="#{pages$PaymentRequests.isRejectedResubmitted && !pages$PaymentRequests.isInvalidStatus && pages$PaymentRequests.isTaskPresent}">
																</h:commandButton>
																<h:commandLink id="lnkresubmit"
																	action="#{pages$PaymentRequests.resubmit}" />


																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	onclick="if (!confirm('#{msg['commons.messages.confirmCancelRequest']}')) return false;else disableButtons(this);"
																	rendered="#{pages$PaymentRequests.isRejectedResubmitted && !pages$PaymentRequests.isInvalidStatus && pages$PaymentRequests.isTaskPresent}">
																</h:commandButton>
																<h:commandLink id="lnkCancel"
																	action="#{pages$PaymentRequests.cancelled}" />

															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.MatureSharePaymentRequest.Approve"
																action="create">

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.unblock']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else disableButtons(this);"
																	rendered="false">
																</h:commandButton>
																<h:commandLink id="lnkUnblock"
																	action="#{pages$PaymentRequests.onUnblockSelected}" />


																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.approve']}"
																	onclick="if (!confirm('#{msg['mems.common.alertApprove']}')) return false;else disableButtons(this);"
																	>
																</h:commandButton>
																<h:commandLink id="lnkApprove"
																	action="#{pages$PaymentRequests.approve}" />
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.reject']}"
																	onclick="if (!confirm('#{msg['mems.common.alertReject']}')) return false;else disableButtons(this);"
																	rendered="#{
																				    (
																					 pages$PaymentRequests.isApprovalRequired ||
																					 pages$PaymentRequests.isSuspended ||
																					 pages$PaymentRequests.isJudgeSessionDone || 
																					 pages$PaymentRequests.isReviewDone || 
																					 pages$PaymentRequests.isFinanceRejected
																					)&& pages$PaymentRequests.isTaskPresent
																				}">
																</h:commandButton>
																<h:commandLink id="lnkReject"
																	action="#{pages$PaymentRequests.reject}" />
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['mems.payment.request.label.btn.judge']}"
																	onclick="if (!confirm('#{msg['mems.common.alertJudge']}')) return false;else disableButtons(this);"
																	rendered="#{ (
																					 pages$PaymentRequests.isApprovalRequired ||
																					 pages$PaymentRequests.isSuspended || 
																					 pages$PaymentRequests.isJudgeSessionDone || 
																					 pages$PaymentRequests.isReviewDone || 
																					 pages$PaymentRequests.isFinanceRejected
																				 )&& pages$PaymentRequests.isTaskPresent	 
																				}">
																</h:commandButton>
																<h:commandLink id="lnkJudge"
																	action="#{pages$PaymentRequests.judge}" />
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.reviewButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else disableButtons(this);"
																	rendered="#{ (
																					pages$PaymentRequests.isApprovalRequired ||
																					pages$PaymentRequests.isSuspended || 
																					pages$PaymentRequests.isReviewDone || 
																					pages$PaymentRequests.isJudgeSessionDone || 
																					pages$PaymentRequests.isFinanceRejected
																				  )&& pages$PaymentRequests.isTaskPresent	
																			  }">
																</h:commandButton>
																<h:commandLink id="lnkReview"
																	action="#{pages$PaymentRequests.review}" />


																<h:commandButton styleClass="BUTTON"
																	value="#{msg['request.lbl.suspend']}"
																	rendered="#{
																				 pages$PaymentRequests.isApprovalRequired ||
																				 pages$PaymentRequests.isReviewDone || 
																				 pages$PaymentRequests.isJudgeSessionDone || 
																				 pages$PaymentRequests.isFinanceRejected
																			   }"
																	onclick="if (!confirm('#{msg['mems.common.alertOperation']}')) return false;else disableButtons(this);">
																</h:commandButton>
																<h:commandLink id="lnkSuspend"
																	action="#{pages$PaymentRequests.onSuspended}" />


															</pims:security>

															<h:commandButton
																rendered="#{pages$PaymentRequests.isJudgeSessionRequired && pages$PaymentRequests.isTaskPresent}"
																styleClass="BUTTON"
																onclick="if (!confirm('#{msg['mems.common.alertJudgeDone']}')) return false;else disableButtons(this);"
																value="#{msg['commons.done']}">
															</h:commandButton>
															<h:commandLink id="lnkjudgeDone"
																action="#{pages$PaymentRequests.judgeDone}" />
															<h:commandButton
																rendered="#{pages$PaymentRequests.isReviewRequired && pages$PaymentRequests.isTaskPresent}"
																styleClass="BUTTON"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else disableButtons(this);"
																value="#{msg['commons.done']}">
															</h:commandButton>
															<h:commandLink id="lnkreviewDone"
																action="#{pages$PaymentRequests.reviewDone}" />

															<pims:security
																screen="Pims.MinorMgmt.MatureSharePaymentRequest.Complete"
																action="create">
																<h:commandButton
																	rendered="#{
																				(
																				  pages$PaymentRequests.isApproved || 
																				  pages$PaymentRequests.isDisbursementRequired
																				)
																				&& pages$PaymentRequests.isTaskPresent
																				}"
																	onclick="if (!confirm('#{msg['mems.common.alertComplete']}')) return false;else disableButtons(this); "
																	styleClass="BUTTON" value="#{msg['commons.complete']}">
																</h:commandButton>
																<h:commandLink id="lnkcomplete"
																	action="#{pages$PaymentRequests.complete}" />

																<h:commandButton
																	rendered="#{
																				(
																					pages$PaymentRequests.isApproved || 
																					pages$PaymentRequests.isDisbursementRequired
																				)&& pages$PaymentRequests.isTaskPresent
																			   }"
																	onclick="if (!confirm('#{msg['mems.common.alertReject']}')) return false;else disableButtons(this); "
																	styleClass="BUTTON" value="#{msg['commons.reject']}">
																</h:commandButton>
																<h:commandLink id="lnkFinanceReject"
																	action="#{pages$PaymentRequests.rejectFinance}" />
															</pims:security>
															<h:commandButton styleClass="BUTTON" id="btnPrint"
																value="#{msg['commons.print']}"
																rendered="#{!pages$PaymentRequests.isNewStatus}"
																onclick="disableButtons(this);">
															</h:commandButton>
															<h:commandLink id="lnkPrint"
																action="#{pages$PaymentRequests.onPrint}" />


														</t:div>
													</div>
												</div>

											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

		</body>
	</html>
</f:view>