
		
		


	<t:div style="width:99.9%;">
		<t:panelGrid columns="1" cellpadding="0" cellspacing="0">
			<t:div styleClass="contentDiv" style="width:98%;">
				<t:dataTable id="contractorsTable" renderedIfEmpty="true" width="100%" cellpadding="0" cellspacing="0" 
				var="dataItem" binding="#{pages$Contractors.contractorGrid}" value="#{pages$Contractors.contractorsList}"
				 	preserveDataModel="false" preserveSort="false" rowClasses="row1,row2" rules="all" rows="#{pages$Contractors.paginatorRows}">
					<t:column width="200"  id="S1">
						<f:facet name="header" >
							<t:outputText value="#{msg['tender.contractors.contractorName']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.contractorNameEn}" rendered="#{dataItem.tenderContractorIsDeleted == 0}" 
						              styleClass="A_LEFT"/>
					
					</t:column>
					<t:column width="200"  id="S12" rendered="#{pages$Conttractors.isArabicLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['tender.contractors.contractorType']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.contractorTypeAr}" rendered="#{dataItem.tenderContractorIsDeleted == 0}"
						              styleClass="A_LEFT"/>

					</t:column>
					<t:column id="S31" width="200" rendered="#{pages$Conttractors.isEnglishLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['tender.contractors.contractorType']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.contractorTypeEn}" rendered="#{dataItem.tenderContractorIsDeleted == 0}" 
						              styleClass="A_LEFT"/>

					</t:column>
					<t:column width="200" id="S1334">
						<f:facet name="header">
							<t:outputText value="#{msg['tender.contractors.licenseNo']}" />
						</f:facet>
						<t:outputText value="#{dataItem.licenseNumber}" rendered="#{dataItem.tenderContractorIsDeleted == 0}" styleClass="A_LEFT"/>
					</t:column>
					<t:column width="200"  id="S143e4" rendered="#{pages$Contractors.isArabicLocale}">
						<f:facet name="header"><h:outputText value="#{msg['tender.contractors.status']}"/></f:facet>
						<t:outputText value="#{dataItem.contractorStatusAr}" rendered="#{dataItem.tenderContractorIsDeleted == 0}"
						              styleClass="A_LEFT"/>
					</t:column>
					<t:column width="280" id="S12112" rendered="#{pages$Contractors.isEnglishLocale}">
						<f:facet name="header"><h:outputText value="#{msg['tender.contractors.status']}"/></f:facet>
						<t:outputText value="#{dataItem.contractorStatusEn}" rendered="#{dataItem.tenderContractorIsDeleted == 0}"
						              styleClass="A_LEFT"/>
					</t:column>
					<t:column  rendered="#{pages$Contractors.canDeleteTenderContractor}">
						<f:facet name="header">
							<t:outputText id="lbl_delete_Contractors"
								value="#{msg['commons.delete']}" />
						</f:facet>
						<h:commandLink id="deleteIconContractor" rendered="#{dataItem.tenderContractorIsDeleted == 0}"
							action="#{pages$Contractors.cmdContractorDelete_Click}">
							<h:graphicImage id="delete_IconContractor"
								title="#{msg['commons.delete']}"
								url="../resources/images/delete_icon.png" />
						</h:commandLink>

					</t:column>
				</t:dataTable>
			</t:div>
			<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99.1%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']}"/>
						<h:outputText value=" : "/>
						<h:outputText value="#{pages$Contractors.recordSize}" />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="contractorsScroller" for="contractorsTable" paginator="true"  
						fastStep="1" paginatorMaxPages="#{pages$Contractors.paginatorMaxPages}" immediate="false"
						paginatorTableClass="paginator"
						renderFacetsIfSinglePage="true" 												
						paginatorTableStyle="grid_paginator" layout="singleTable" 
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" 
						pageIndexVar="pageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}" id="lblFContractors"></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFRContractors"></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFContractors"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}" id="lblLContractors"></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
							<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
		</t:div>
		</t:panelGrid>
	</t:div>