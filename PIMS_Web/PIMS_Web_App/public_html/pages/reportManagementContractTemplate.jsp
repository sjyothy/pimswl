<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">

	function openLoadReportPopup(pageName) 	{	
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />


			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />





		</head>

		<body>
			<div style="width: 1024px;">


				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>


					</tr>
					<tr width="100%">

						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel
											value="#{msg['listReport.managementContractTemplate.reportSearchPage.heading']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg">
									</td>
									<td width="100%" height="99%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION" style="height: 450px;">

											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$reportManagementContractTemplate.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>


											<h:form id="searchFrm">

												<div class="MARGIN" style="width: 96%">
													<table cellpadding="0" cellspacing="0">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td style="FONT-SIZE: 0px" width="100%">
																<IMG
																	src="../<h:outputText value="#{path.img_section_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="DETAIL_SECTION" style="width: 99.6%">
														<h:outputLabel value="#{msg['commons.report.criteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="0px" cellspacing="0px"
															class="DETAIL_SECTION_INNER" border="0">
															<tr>
																<td width="97%">
																	<table width="100%">

																		<%-- Contract --%>
																		
																		
																		<tr>
																		
																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.contractNumber']} :"></h:outputLabel>
																			</td>
																			<td>

																				<h:inputText id="txtContractNumber"
																					value="#{pages$reportManagementContractTemplate.criteria.txtContractNumber}"
																					maxlength="20"></h:inputText>
																			</td>
																			
	
																			
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['ContractorSearch.contractStatus']} :" />
																			</td>
																			<td>
																				<h:selectOneMenu id="cboContractStatus"
																					required="false"
																					value="#{pages$reportManagementContractTemplate.criteria.cboContractStatus}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.contractStatus}" />
																				</h:selectOneMenu>
																			</td>

																		</tr>
																		

																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.contractAmountFrom']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtContractValueFrom"
																					value="#{pages$reportManagementContractTemplate.criteria.txtContractValueFrom}"
																					styleClass="INPUT_TEXT" maxlength="20"></h:inputText>
																			</td>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.contractAmountTo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtContractValueTo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtContractValueTo}"
																					maxlength="20"></h:inputText>
																			</td>
																		</tr>



																		


																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.contractDateFrom']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="clndrContractDateFrom" popup="true"
																					datePattern="#{pages$reportManagementContractTemplate.dateFormat}"
																					locale="#{pages$reportManagementContractTemplate.locale}"
																					value="#{pages$reportManagementContractTemplate.criteria.clndrContractDateFrom}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width: 170px; height: 14px"></rich:calendar>
																			</td>


																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.contractDateTo']} :"></h:outputLabel>
																			</td>
																			<td>

																				<rich:calendar id="clndrContractDateTo"
																					datePattern="#{pages$reportManagementContractTemplate.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$reportManagementContractTemplate.locale}"
																					value="#{pages$reportManagementContractTemplate.criteria.clndrContractDateTo}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px">

																				</rich:calendar>
																			</td>

																		</tr>

																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.contractExpiryDateFrom']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="clndrContractExpiryDateFrom" popup="true"
																					datePattern="#{pages$reportManagementContractTemplate.dateFormat}"
																					locale="#{pages$reportManagementContractTemplate.locale}"
																					value="#{pages$reportManagementContractTemplate.criteria.clndrContractExpiryDateFrom}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width: 170px; height: 14px"></rich:calendar>
																			</td>


																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.contractExpiryDateTo']} :"></h:outputLabel>
																			</td>
																			<td>

																				<rich:calendar id="clndrContractExpiryDateTo"
																					datePattern="#{pages$reportManagementContractTemplate.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$reportManagementContractTemplate.locale}"
																					value="#{pages$reportManagementContractTemplate.criteria.clndrContractExpiryDateTo}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px">

																				</rich:calendar>
																			</td>

																		</tr>

																		<%-- OWNER --%>
																		
																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.ownerName']} :"></h:outputLabel>


																			</td>
																			<td>


																				<h:inputText id="txtOwnerName"
																					value="#{pages$reportManagementContractTemplate.criteria.txtOwnerName}"
																					maxlength="20"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.ownerNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtOwnerNo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtOwnerNo}"
																					maxlength="20"></h:inputText>
																			</td>

																		</tr>
																		
																		<tr>
																			<td>
																			<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.nationality']} :" />

																			</td>
																			<td>
																				<h:selectOneMenu id="cboOwnerNationality"
																					required="false"
																					value="#{pages$reportManagementContractTemplate.criteria.cboOwnerNationality}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.countryList}" />
																				</h:selectOneMenu>
																			</td>
																			
																			
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.nidNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtOwnerNidNo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtOwnerNidNo}"
																					maxlength="20"></h:inputText>
																			</td>
																		</tr>
																		
																		<tr>
																			
																			<td>
																			<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.passportNo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtOwnerPassportNo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtOwnerPassportNo}"
																					maxlength="30"></h:inputText>
																			</td>
																		
																		
																			<td>
																			<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.visaNo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtOwnerVisaNo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtOwnerVisaNo}"
																					maxlength="30"></h:inputText>
																			</td>
																		
																		</tr>
																		
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.tradeLNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtOwnerTradeLNo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtOwnerTradeLNo}"
																					maxlength="10"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.phoneNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtOwnerPhoneNo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtOwnerPhoneNo}"
																					maxlength="30"></h:inputText>
																			</td>


																		</tr>
																		
																		<%-- UNIT --%>
																		
																		<tr>
																		
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['units.number']} :" />
																			</td>
																			<td>
																				<h:inputText id="txtUnitNumber"
																					value="#{pages$reportManagementContractTemplate.criteria.txtUnitNumber}"
																					maxlength="20"></h:inputText>
																			</td>
																			
																		
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.costCenter']} :" />
																			</td>
																			<td>
																				<h:inputText id="txtUnitCostCenter"
																					value="#{pages$reportManagementContractTemplate.criteria.txtUnitCostCenter}"
																					maxlength="250"></h:inputText>
																			</td>
																			
																		</tr>
																		
																		<tr>
																		
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.unitType']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:selectOneMenu id="unitType" required="false"
																					value="#{pages$reportManagementContractTemplate.criteria.cboUnittype}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.unitTypeList}" />
																				</h:selectOneMenu>
																			</td>
																		
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.unitStatus']} :" />
																			</td>
																			<td>
																				<h:selectOneMenu id="cboUnitStatus" required="false"
																					value="#{pages$reportManagementContractTemplate.criteria.cboUnitStatus}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.unitStatusList}" />
																				</h:selectOneMenu>
																			</td>
																		
																		</tr>
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.unitRentAmountFrom']} :"></h:outputLabel>
																			</td>

																			<td>
																				<h:inputText id="txtUnitRentAmountFrom"
																					value="#{pages$reportManagementContractTemplate.criteria.unitRentAmountFrom}"
																					styleClass="INPUT_TEXT" maxlength="20"></h:inputText>

																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.unitRentAmountTo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtUnitRentAmountTo"
																					value="#{pages$reportManagementContractTemplate.criteria.unitRentAmountTo}"
																					maxlength="20"></h:inputText>
																			</td>
																		</tr>
																		
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.unitSubType']} :"></h:outputLabel>
																			</td>

																			<td>
																				<h:selectOneMenu id="cboUnitSubType"
																					required="false"
																					value="#{pages$reportManagementContractTemplate.criteria.cboUnitSubType}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.unitSideType}" />
																				</h:selectOneMenu>

																			</td>
																			
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.unitUsage']} :"></h:outputLabel>
																			</td>

																			<td>
																				<h:selectOneMenu id="cboUnitUsage" required="false"
																					value="#{pages$reportManagementContractTemplate.criteria.cboUnitUsage}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.unitUsageTypeList}" />
																				</h:selectOneMenu>

																			</td>

																		</tr>
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['inquiry.unitAreaMin']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtUnitAreaSqFtFrom"
																					value="#{pages$reportManagementContractTemplate.criteria.txtUnitAreaSqFtFrom}"
																					maxlength="12"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['inquiry.unitAreaMax']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtUnitAreaSqFtTo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtUnitAreaSqFtTo}"
																					maxlength="12"></h:inputText>
																			</td>

																		</tr>
																		
																		
																		<%-- PROPERTY --%>

																		<tr>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.property.Name']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="txPropertyName"
																					value="#{pages$reportManagementContractTemplate.criteria.txtPropertyName}"
																					styleClass="INPUT_TEXT" maxlength="250"></h:inputText>
																			</td>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['inspection.propertyRefNum']} :"></h:outputLabel>
																			</td>

																			<td>


																				<h:inputText id="txtPropertyNumber"
																					value="#{pages$reportManagementContractTemplate.criteria.txtPropertyNumber}"
																					styleClass="INPUT_TEXT" maxlength="20"></h:inputText>

																			</td>


																		</tr>
																		

																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['property.type']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:selectOneMenu id="cboPropertyType"
																					required="false"
																					value="#{pages$reportManagementContractTemplate.criteria.cboPropertyType}">

																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyTypeList}" />
																				</h:selectOneMenu>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['property.status']} :" />
																			</td>
																			<td>
																				<h:selectOneMenu id="cboPropertyStatus"
																					required="false"
																					value="#{pages$reportManagementContractTemplate.criteria.cboPropertyStatus}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyStatusList}" />
																				</h:selectOneMenu>
																			</td>

																		</tr>
																		
																		<tr>
																		
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.ownershipType']} :" />

																			</td>
																			<td>
																				<h:selectOneMenu id="cboPropertyOwnershipType"
																					required="false"
																					value="#{pages$reportManagementContractTemplate.criteria.cboPropertyOwnershipType}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyOwnershipType}" />
																				</h:selectOneMenu>
																			</td>

																		

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.property.usageType']} :" />

																			</td>
																			<td>
																				<h:selectOneMenu id="cboPropertyUsage"
																					required="false"
																					value="#{pages$reportManagementContractTemplate.criteria.cboPropertyUsage}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyUsageList}" />
																				</h:selectOneMenu>

																			</td>
																		</tr>	
																		
																		
																		<tr>
																			
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.dmNo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtPropertyDmNo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtPropertyDmNo}"
																					maxlength="7"></h:inputText>
																			</td>
																			
																			
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['property.landNumber']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="landNmuberInput"
																					value="#{pages$reportManagementContractTemplate.criteria.txtLandNumber}"
																					maxlength="30"></h:inputText>
																			</td>
																			
																		
																		</tr>
																		
																		
																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyReceivingDateFrom']} :"></h:outputLabel>
																			</td>
																			<td>




																				<rich:calendar id="clndrPropertyReceivingDateFrom" popup="true"
																					datePattern="#{pages$reportManagementContractTemplate.dateFormat}"
																					locale="#{pages$reportManagementContractTemplate.locale}"
																					value="#{pages$reportManagementContractTemplate.criteria.clndrPropertyReceivingDateFrom}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width: 170px; height: 14px"></rich:calendar>
																			</td>


																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyReceivingDateTo']} :"></h:outputLabel>
																			</td>
																			<td>

																				<rich:calendar id="clndrPropertyReceivingDateTo"
																					datePattern="#{pages$reportManagementContractTemplate.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$reportManagementContractTemplate.locale}"
																					value="#{pages$reportManagementContractTemplate.criteria.clndrPropertyReceivingDateTo}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px">

																				</rich:calendar>
																			</td>

																		</tr>
																		
																		<tr>
																			
																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.community']} :" />

																			</td>
																			<td>
																				<h:selectOneMenu id="cboPropertyCommunity"
																					required="false"
																					value="#{pages$reportManagementContractTemplate.criteria.cboPropertyCommunity}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems value="#{view.attributes['city']}" />
																				</h:selectOneMenu>
																			</td>
																			
																			
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.address']} :" />
																			</td>
																			<td>
																				<h:inputText id="txtPropertyAddress"
																					value="#{pages$reportManagementContractTemplate.criteria.txtPropertyAddress}"
																					maxlength="250"></h:inputText>
																			</td>
																		
																		</tr>
																		
																	
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyTotalUnitsMin']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtNoOfUnitsFrom"
																					value="#{pages$reportManagementContractTemplate.criteria.txtNoOfUnitsFrom}"
																					maxlength="12"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyTotalUnitsMax']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtNoOfUnitsTo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtNoOfUnitsTo}"
																					maxlength="12"></h:inputText>
																			</td>

																		</tr>
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyTotalFloorsMin']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtNoOfFloorsFrom"
																					value="#{pages$reportManagementContractTemplate.criteria.txtNoOfFloorsFrom}"
																					maxlength="12"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyTotalFloorsMax']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtNoOfFloorsTo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtNoOfFloorsTo}"
																					maxlength="12"></h:inputText>
																			</td>

																		</tr>
																		
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyTotalParkingFloorsMin']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtNoOfParkingFrom"
																					value="#{pages$reportManagementContractTemplate.criteria.txtNoOfParkingFrom}"
																					maxlength="12"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyTotalParkingFloorsMax']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtNoOfParkingTo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtNoOfParkingTo}"
																					maxlength="12"></h:inputText>
																			</td>

																		</tr>
																		
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyCommercialAreaMin']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtCommercialAreaFrom"
																					value="#{pages$reportManagementContractTemplate.criteria.txtCommercialAreaFrom}"
																					maxlength="12"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyCommercialAreaMax']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtCommercialAreaTo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtCommercialAreaTo}"
																					maxlength="12"></h:inputText>
																			</td>

																		</tr>
																		
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyBuiltAreaMin']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtPropertyBuiltAreaSqFtFrom"
																					value="#{pages$reportManagementContractTemplate.criteria.txtPropertyBuiltAreaSqFtFrom}"
																					maxlength="12"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.propertyBuiltAreaMax']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtPropertyBuiltAreaSqFtTo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtPropertyBuiltAreaSqFtTo}"
																					maxlength="12"></h:inputText>
																			</td>

																		</tr>
																		
																		
																		
																		<tr>
																			

																		<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.managementContractTemplate.propertyDewaAccNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtPropertyDewaAccNo"
																					value="#{pages$reportManagementContractTemplate.criteria.txtPropertyDewaAccNo}"
																					maxlength="15"></h:inputText>
																			</td>
																		

																		</tr>
																		
																		
																		
			
							
																		


																		<tr>
																			<td class="BUTTON_TD" colspan="4">
																				<h:commandButton styleClass="BUTTON"
																					id="submitButton"
																					action="#{pages$reportManagementContractTemplate.cmdView_Click}"
																					immediate="false" value="#{msg['commons.view']}"
																					tabindex="7"></h:commandButton>
																			</td>
																		</tr>

																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>
															</tr>

														</table>


													</div>
												</div>



											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

		</body>
	</html>
</f:view>
