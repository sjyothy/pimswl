<%@ page import="javax.faces.component.UIViewRoot;"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="javascript" type="text/javascript">
	function showUploadPopup() {
	      var screen_width = screen.width;
	      var screen_height = screen.height;
	      var popup_width = screen_width/3+150;
	      var popup_height = screen_height/3-10;
	      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
	      
	      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	      popup.focus();
	}

	function showAddNotePopup() {
	      var screen_width = screen.width;
	      var screen_height = screen.height;
	      var popup_width = screen_width/3+120;
	      var popup_height = screen_height/3-80;
	      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
	      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	      popup.focus();
	}
	
	
	function resetStateCR() {
		document.getElementById('frmSearchContractor:selectStateCR').selectedIndex=0;
		document.getElementById('frmSearchContractor:selectCityCR').selectedIndex=0;
	}	
	
	
	function resetCitiesCR() {
		document.getElementById('frmSearchContractor:selectCityCR').selectedIndex=0;
	}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />				
				
		</head>

		<body class="BODY_STYLE">
		   <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
						<c:when test="${!pages$addContractor.isModePopUp}">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</c:when>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!pages$addContractor.isModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>

					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['commons.addContractor']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="3" />
								<td width="100%" height="450px" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION">
										<h:form id="frmSearchContractor" style="width:97%">
											<div >
												<table border="0" class="layoutTable">
													<tr>
														<td colspan="6">
															<h:outputText value="#{pages$addContractor.errorMessages}" escape="false" styleClass="ERROR_FONT" style="padding:10px;" />
															<h:outputText value="#{pages$addContractor.infoMessages}" escape="false" styleClass="INFO_FONT" style="padding:10px;" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN">
												<rich:tabPanel style="width:98%; height:240px;">

													<!--  Contractor Details - Tab (Start) -->
													<rich:tab id="contractorDetailsTab"
														label="#{msg['contractAdd.contractorDetails']}">
														<t:panelGrid columns="4" cellspacing="10px">
															<h:outputLabel styleClass="LABEL"
																value="#{msg['ContractorSearch.GRPNumber']}:" />
															<h:inputText  
																id="txtGrpNumber" 
																value="#{pages$addContractor.grpNumber}" />

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['applicationDetails.status']}:" />
															</h:panelGroup>
															<h:selectOneMenu id="selectStatus"
																
																value="#{pages$addContractor.contractorStatus}">
																<f:selectItem id="selectAllStatus" itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$addContractor.contractorStatusList}" />
															</h:selectOneMenu>

															
															
															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.contractorNameEn']}:" />
															</h:panelGroup>
															<h:inputText 
																id="txtContractorNameEn" 
																value="#{pages$addContractor.contractorNameEn}" />

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.contractorNameAr']}:" />
															</h:panelGroup>
															<h:inputText  
																id="txtContractorNameAr" 
																value="#{pages$addContractor.contractorNameAr}" />

															
															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.commercialName']}:" />
															</h:panelGroup>
															<h:inputText  
																id="txtCommercialName" 
																value="#{pages$addContractor.commercialName}" />

															
															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.contractorType']}:" />
															</h:panelGroup>
															<h:selectOneMenu id="selectContractorType"
																
																binding="#{pages$addContractor.cmbContractorType}"
																value="#{pages$addContractor.contractorType}">
																<f:selectItem id="selectAllType" itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$addContractor.contractorTypeList}" />
															</h:selectOneMenu>

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.licenseNumber']}:" />
															</h:panelGroup>
															<h:inputText  
																id="txtLicenseNumber" 
																value="#{pages$addContractor.licenseNumber}" />

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.licenseSource']}:" />
															</h:panelGroup>
															<h:selectOneMenu id="selectLicenseSource"
																
																value="#{pages$addContractor.licenseSource}">
																<f:selectItem id="selectAllSource" itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$addContractor.licenseSourceList}" />
															</h:selectOneMenu>

															
															
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.issueDate']}:" />
															
															<rich:calendar id="txtIssueDate" inputStyle="width: 170px;height:14px; "
																	popup="true" value="#{pages$addContractor.issueDate}"
																	showApplyButton="false" enableManualInput="false"
																	datePattern="dd/MM/yyyy"/> 
																
															
															
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['TaskList.DataTable.ExpiryDate']}:" />
															
															<rich:calendar id="txtExpiryDate" inputStyle="width: 170px;height:14px; "
																	popup="true" value="#{pages$addContractor.expiryDate}"
																	showApplyButton="false" enableManualInput="false"
																	datePattern="dd/MM/yyyy"/>																


															<h:outputLabel styleClass="LABEL"
																value="#{msg['contractAdd.businessActivity']}:" />
															<div class="SCROLLABLE_SECTION" style="height:100px;width:298px;overflow-x:hidden;border-width: 1px; border-style: solid; border-color: #a2a2a2;">
															<h:panelGrid
																style="border-width: 1px; border-style: solid; border-color: #a2a2a2; background: white; overflow-x: auto; overflow-y: scroll; width: 90%; height: 60px; ">
																<h:selectManyCheckbox layout="pageDirection"
																	id="selectManyBusinessActivity"
																	value="#{pages$addContractor.businessActivities}">
																	<f:selectItems
																		value="#{pages$addContractor.businessActivityList}" />
																</h:selectManyCheckbox>
															</h:panelGrid>
															</div>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['serviceContract.serviceType']}:" />
															<div class="SCROLLABLE_SECTION" style="height:100px;width:298px;overflow-x:hidden;border-width: 1px; border-style: solid; border-color: #a2a2a2;">
															<h:panelGrid
																style="border-width: 1px; border-style: solid; border-color: #a2a2a2; background: white; overflow-x: auto; overflow-y: scroll; width: 90%; height: 60px;">
																<h:selectManyCheckbox layout="pageDirection"
																	id="selectManyServiceType"
																	value="#{pages$addContractor.serviceTypes}">
																	<f:selectItems
																		value="#{pages$addContractor.serviceTypeList}" />
																</h:selectManyCheckbox>
															</h:panelGrid>
															</div>
														</t:panelGrid>
													</rich:tab>
													<!--  Contractor Details - Tab (End) -->

													<!--  Address Details - Tab (Start) -->
													<rich:tab id="addressDetailsTab"
														label="#{msg['contractAdd.addressDetails']}">
														<t:panelGrid width="100%">
														<t:panelGrid cellpadding="1px" cellspacing="2px"
															styleClass="TAB_DETAIL_SECTION" width="100%" columns="4">
															<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"
																value="#{msg['contact.address1']}:" />
															</h:panelGroup>
															<h:inputText 
																maxlength="150" id="txtaddress1"
																value="#{pages$addContractor.address1}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.address2']}:" />
															<h:inputText 
																maxlength="150" id="txtaddress2"
																value="#{pages$addContractor.address2}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.street']}:" />
															<h:inputText 
																maxlength="150" id="txtstreet"
																value="#{pages$addContractor.street}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contractorAdd.POBox']}:" />
															<h:inputText styleClass="A_RIGHT_NUM"
																maxlength="10" id="txtpostCode"
																value="#{pages$addContractor.postCode}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.country']}:" />
															<h:selectOneMenu id="selectCountry" 
																value="#{pages$addContractor.country}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$contractorSearch.countryList}" />
																<a4j:support event="onchange"
																	action="#{pages$addContractor.loadState}"
																	reRender="selectState,selectCity" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.state']}:" />
															<h:selectOneMenu id="selectState"
																 required="false"
																value="#{pages$addContractor.state}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$contractorSearch.stateList}" />
																<a4j:support event="onchange"
																	action="#{pages$addContractor.loadCity}"
																	reRender="selectState,selectCity" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.city']}:" />
															<h:selectOneMenu id="selectCity"
																value="#{pages$addContractor.city}">
																<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$addContractor.cityList}" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.homephone']}:" />
															<h:inputText styleClass="A_RIGHT_NUM"
																   id="txthomePhone"
																value="#{pages$addContractor.homePhone}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.officephone']}:" />
															<h:inputText styleClass="A_RIGHT_NUM"
																   id="txtofficePhone"
																value="#{pages$addContractor.officePhone}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.fax']}:" />
															<h:inputText id="txtfax"
																value="#{pages$addContractor.fax}" />
															<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"
																value="#{msg['contact.email']}:" />
															</h:panelGroup>
															<h:inputText 
																maxlength="50" id="txtemail"
																value="#{pages$addContractor.email}" />

													</t:panelGrid>
														<t:panelGrid align="right" width="100" columns="1" columnClasses="BUTTON_TD">
															<h:commandButton id="btnAddContactInfo"
																styleClass="BUTTON" value="#{msg['commons.Add']}"
																action="#{pages$addContractor.addContactInfo}" />
														</t:panelGrid>
												</t:panelGrid>	
														
													<t:div style="text-align:center;">
														<t:div id="contactInfoDiv" styleClass="contentDiv"
															style="align:center;">
															<t:dataTable id="contactInfoDataTable"
															    rows="#{pages$addContractor.paginatorRows}"
																width="100%"
																binding="#{pages$addContractor.contactInfoDataTable}"
																value="#{pages$addContractor.contactInfoList}"
																preserveDataModel="false" preserveSort="false"
																var="contactInfoItem" rowClasses="row1,row2" rules="all"
																renderedIfEmpty="true">
																<t:column id="colAddress1" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['contact.address1']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{contactInfoItem.address1}" />
																</t:column>

																<t:column id="colAddress2">
																	<f:facet name="header">
																		<t:outputText value="#{msg['contact.email']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{contactInfoItem.email}" />
																</t:column>

																<t:column id="colActionContactInfo" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.select']}" />
																	</f:facet>
																	<%-- <a4j:commandLink reRender="contactInfoDataTable,contactInfoRecordSize,extendscroller"
																		action="#{pages$addContractor.removeContactInfo}">
																		<h:graphicImage id="imgRemoveContactInfo"
																			title="#{msg['commons.delete']}"
																			url="../resources/images/delete_icon.png" />
																	</a4j:commandLink>--%>
																	<h:commandLink 
																		action="#{pages$addContractor.removeContactInfo}">
																		<h:graphicImage id="imgRemoveContactInfo"
																			title="#{msg['commons.delete']}"
																			url="../resources/images/delete_icon.png" />
																	</h:commandLink>
																	<h:commandLink
																		action="#{pages$addContractor.editContactInfo}">
																		<h:graphicImage id="btnPopulateContactInfoFromList"
																			title="#{msg['commons.edit']}"
																			url="../resources/images/edit-icon.gif" />
																	</h:commandLink>
																</t:column>
															</t:dataTable>
														</t:div>
														
														
														
														
														
														<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																	style="width:99%;">
																	<t:panelGrid cellpadding="0" cellspacing="0"
																		width="100%" columns="2" columnClasses="RECORD_NUM_TD">
																		<t:div styleClass="RECORD_NUM_BG">
																			<t:panelGrid cellpadding="0" cellspacing="0"
																				columns="3" columnClasses="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.records']}" />
																				<h:outputText value=" : " styleClass="RECORD_NUM_TD" />
																				<h:outputText
																					id="contactInfoRecordSize"
																					value="#{pages$addContractor.contactInfoRecordSize}"
																					styleClass="RECORD_NUM_TD" />
																			</t:panelGrid>
																		</t:div>
	                                                                      <t:dataScroller id="extendscroller" for="contactInfoDataTable" paginator="true"
																											fastStep="1"  paginatorMaxPages="#{pages$addContractor.contactInfoPaginatorMaxPages}" immediate="false"
																											paginatorTableClass="paginator"
																											renderFacetsIfSinglePage="true" 								
																										    paginatorTableStyle="grid_paginator" layout="singleTable"
																											paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																											paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;" 
																											pageIndexVar="pageNumber"
																											styleClass="SCH_SCROLLER" >
														
																											<f:facet name="first">
																													<t:graphicImage url="../#{path.scroller_first}" id="lblFinquiry"></t:graphicImage>
																												</f:facet>
																												<f:facet name="fastrewind">
																													<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFRinquiry"></t:graphicImage>
																												</f:facet>
																												<f:facet name="fastforward">
																													<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFinquiry"></t:graphicImage>
																												</f:facet>
																												<f:facet name="last">
																													<t:graphicImage url="../#{path.scroller_last}" id="lblLinquiry"></t:graphicImage>
																												</f:facet>
																											<t:div styleClass="PAGE_NUM_BG" rendered="true">
																					<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																					<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																				</t:div>
																			</t:dataScroller>

																	</t:panelGrid>
																</t:div>
														
														
														
														
														
														
														
														
														
														
														
														
														
													</t:div>
													</rich:tab>
													<!--  Address Details - Tab (End) -->

													<!--  Contact Person - Tab (Start) -->
													<rich:tab id="contactPersonTab"
														label="#{msg['contractAdd.contactPersons']}">
														<t:panelGrid width="100%">														
														<t:panelGrid id="crDetailTable" cellpadding="1px"
															cellspacing="2px" styleClass="TAB_DETAIL_SECTION"
															width="100%" columns="4">
															
															<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.title']}:" />
															<h:selectOneMenu id="selectCRTitle" 
																value="#{pages$addContractor.crTitleId}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems value="#{pages$ApplicationBean.title}" />
															</h:selectOneMenu>
															<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"
																value="#{msg['customer.firstname']}:" />
															</h:panelGroup>
															<h:inputText 
																maxlength="50" id="txtCRFirstName"
																value="#{pages$addContractor.crFirstName}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.middlename']}:" />
															<h:inputText 
																maxlength="10" id="txtCRMiddleName"
																value="#{pages$addContractor.crMiddleName}" />
															<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"
																value="#{msg['customer.lastname']}:" />
															</h:panelGroup>
															<h:inputText 
																maxlength="50" id="txtCRLastName"
																value="#{pages$addContractor.crLastName}" />
															<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"
																value="#{msg['contact.address1']}:" />
															</h:panelGroup>
															<h:inputText 
																maxlength="150" id="txtCRAddress1"
																value="#{pages$addContractor.crAddress1}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.address2']}:" />
															<h:inputText 
																maxlength="150" id="txtCRAddress2"
																value="#{pages$addContractor.crAddress2}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.street']}:" />
															<h:inputText 
																maxlength="150" id="txtCRStreet"
																value="#{pages$addContractor.crStreet}" />

															<%--<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.designation']}:" />
															<h:inputText styleClass="A_LEFT"
																style="width:150px;" maxlength="15"
																id="txtCRDesignation"
																value="#{pages$addContractor.crDesignation}" /> --%>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.postcode']}:" />
															<h:inputText styleClass="A_RIGHT_NUM"
																maxlength="10" id="txtCRPostCode"
																value="#{pages$addContractor.crPostCode}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.country']}:" />
															<h:selectOneMenu id="selectCountryCR"
																
																valueChangeListener="#{pages$addContractor.loadContactRefState}" 
															 	onchange="resetStateCR();submit()" 
																value="#{pages$addContractor.crCountryId}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$addContractor.countryList}" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.state']}:" />
															<h:selectOneMenu id="selectStateCR"
																required="false"
																valueChangeListener="#{pages$addContractor.loadContactRefCity}" 
															 	onchange="resetCitiesCR();submit()"
																value="#{pages$addContractor.crStateId}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems value="#{pages$addContractor.CRStateList}" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.city']}:" />
															<h:selectOneMenu id="selectCityCR" 
																value="#{pages$addContractor.crCityId}">
																<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$addContractor.CRCityList}" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.homephone']}:" />
															<h:inputText styleClass="A_RIGHT_NUM"
																 id="txtCRHomePhone"
																value="#{pages$addContractor.crHomePhone}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.officephone']}:" />
															<h:inputText styleClass="A_RIGHT_NUM"
																id="txtCROfficePhone"
																value="#{pages$addContractor.crOfficePhone}" />

														<%--	<h:outputLabel styleClass="LABEL"
																value="#{msg['contractorAdd.mobileNumber']}:" />
															<h:inputText styleClass="A_RIGHT_NUM"
																style="width:150px;" maxlength="15"
																id="txtCRMobileNumber"
																value="#{pages$addContractor.crMobileNumber}" /> --%>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.fax']}:" />
															<h:inputText styleClass="A_RIGHT_NUM"
																id="txtCRFax"
																value="#{pages$addContractor.crFax}" />
															<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"
																value="#{msg['contact.email']}:" />
															</h:panelGroup>
															<h:inputText 
																maxlength="50" id="txtCREmail"
																value="#{pages$addContractor.crEmail}" />
															</t:panelGrid>

															<t:panelGrid align="right" width="715" columns="1" columnClasses="BUTTON_TD">
															<h:commandButton id="btnAddContactRef"
																styleClass="BUTTON" value="#{msg['commons.Add']}"
																action="#{pages$addContractor.addContactReference}" />
														    </t:panelGrid>
														</t:panelGrid>

														<t:div style="text-align:center;">
														<t:div id="contactRefDiv" styleClass="contentDiv"
															style="align:center;">
															<t:dataTable id="contactRefDataTable" rows="#{pages$addContractor.paginatorRows}"
																width="100%"
																binding="#{pages$addContractor.contactRefDataTable}"
																value="#{pages$addContractor.contactReferencesList}"
																preserveDataModel="false" preserveSort="false"
																var="contactRefItem" rowClasses="row1,row2" rules="all"
																renderedIfEmpty="true">

																<t:column id="colCRName" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.Name']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{contactRefItem.fullName}" />
																</t:column>

																<t:column id="colCRAddress2">
																	<f:facet name="header">
																		<t:outputText value="#{msg['contact.email']}" />
																	</f:facet>
																	<t:outputText title="" styleClass="A_LEFT"
																		value="#{contactRefItem.contactInfoView.email}" />
																</t:column>

																<t:column id="colActionContactRef" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.select']}" />
																	</f:facet>
																	<h:commandLink 
																		action="#{pages$addContractor.removeContactReference}">
																		<h:graphicImage id="imgRemoveContactRef"
																			title="#{msg['commons.delete']}"
																			url="../resources/images/delete_icon.png" />
																	</h:commandLink>
																	<h:commandLink
																		action="#{pages$addContractor.editContactReference}">
																		<h:graphicImage id="btnPopulateContactRefFromList"
																			title="#{msg['commons.edit']}"
																			url="../resources/images/edit-icon.gif" />
																	</h:commandLink>
																</t:column>
															</t:dataTable>
														</t:div>
														
														
														
														<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																	style="width:99%;">
																	<t:panelGrid cellpadding="0" cellspacing="0"
																		width="100%" columns="2" columnClasses="RECORD_NUM_TD">
																		<t:div styleClass="RECORD_NUM_BG">
																			<t:panelGrid cellpadding="0" cellspacing="0"
																				columns="3" columnClasses="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.records']}" />
																				<h:outputText value=" : " styleClass="RECORD_NUM_TD" />
																				<h:outputText id="contactRefRecordSize"
																					value="#{pages$addContractor.contactRefRecordSize}"
																					styleClass="RECORD_NUM_TD" />
																			</t:panelGrid>
																		</t:div>
	                                                                      <t:dataScroller id="scroller" for="contactRefDataTable" paginator="true"
																											fastStep="1"  paginatorMaxPages="#{pages$addContractor.contactRefPaginatorMaxPages}" immediate="false"
																											paginatorTableClass="paginator"
																											renderFacetsIfSinglePage="true" 								
																										    paginatorTableStyle="grid_paginator" layout="singleTable"
																											paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																											paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;" 
																											pageIndexVar="pageNumber"
																											styleClass="SCH_SCROLLER" >
														
																											<f:facet name="first">
																													<t:graphicImage url="../#{path.scroller_first}" id="first"></t:graphicImage>
																												</f:facet>
																												<f:facet name="fastrewind">
																													<t:graphicImage url="../#{path.scroller_fastRewind}" id="rewind"></t:graphicImage>
																												</f:facet>
																												<f:facet name="fastforward">
																													<t:graphicImage url="../#{path.scroller_fastForward}" id="forward"></t:graphicImage>
																												</f:facet>
																												<f:facet name="last">
																													<t:graphicImage url="../#{path.scroller_last}" id="last"></t:graphicImage>
																												</f:facet>
																											<t:div styleClass="PAGE_NUM_BG" rendered="true">
																					<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																					<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																				</t:div>
																			</t:dataScroller>

																	</t:panelGrid>
																</t:div>
														
														
														
														
													</t:div>
													</rich:tab>
													<!--  Contact Person - Tab (End) -->

													<!--  Attachment - Tab (Start) -->
													<rich:tab id="attachmentTab"
														label="#{msg['commons.attachmentTabHeading']}">
														<%@  include file="attachment/attachment.jsp"%>
													</rich:tab>
													<!--  Attachment - Tab (End) -->

													<!--  Comments - Tab (Start) -->
													<rich:tab id="commentsTab"
														label="#{msg['commons.comments']}">
														<%@ include file="notes/notes.jsp"%>
													</rich:tab>
													<!--  Comments - Tab (End) -->
												</rich:tabPanel>

												<table width="98%">
													<tr>
														<td align="right">
															<h:commandButton styleClass="BUTTON" type="button"
																onclick="javascript:closeWindow();"
																value="#{msg['commons.cancel']}"
																rendered="#{pages$addContractor.isModePopUp}" />
															<h:commandButton type="submit" styleClass="BUTTON"
																value="#{msg['commons.saveButton']}"
																action="#{pages$addContractor.saveContractor}" />
																
																
															<h:commandButton id="generateLoginId" styleClass="BUTTON"  
                               									   style="width: 110px"
                               									   value="#{msg['commons.generateLoginId']}" 
                               									   action ="#{pages$addContractor.generateLoginId}" 
                               									   rendered="#{pages$addContractor.isGenerateLogin}"/>
                               									   																
															<h:commandButton type="submit" styleClass="BUTTON"
																value="#{msg['commons.back']}"
																action="#{pages$addContractor.btnBack_Click}" />	
														</td>
													</tr>
												</table>
											</div>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		   </div>	
		</body>
	</html>
</f:view>