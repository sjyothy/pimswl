<%-- Author: Kamran Ahmed--%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<script language="JavaScript" type="text/javascript"> 



	function onComboBeneficiaryClick()
	{
		var cbo = document.getElementById("memsFollowupForm:cboBeneficiaryPerson");
		var id = cbo.options[cbo.selectedIndex].value;
	    if( id>0)
	    {
	       openBeneficiaryPopup(id);
	    }	
				
	}
</script>

<t:panelGrid border="0" width="96%" cellpadding="1" cellspacing="1"
	id="socialResearchRecommTab">
	<t:panelGroup>
		<%--Column 1,2 Starts--%>
		<t:panelGrid columns="4"
			rendered="#{!pages$socialReseacrhRecommTab.viewMode}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

			<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*" />
				<h:outputLabel
					value="#{msg['PeriodicDisbursements.columns.beneficiary']} :" />
			</h:panelGroup>
			<h:panelGroup>
				<h:selectOneMenu id="cboBeneficiaryPerson"
					binding="#{pages$socialReseacrhRecommTab.cboBeneficiaryPersonId}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems
						value="#{pages$socialReseacrhRecommTab.beneficiaryList}" />
				</h:selectOneMenu>
				<h:graphicImage id="benefDetails"
					style="MARGIN: 1px 1px 0px;cursor:hand"
					onclick=" onComboBeneficiaryClick();"
					url="../resources/images/app_icons/Tenant.png">
				</h:graphicImage>
			</h:panelGroup>
			<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*" />
				<h:outputLabel value="#{msg['caringType.Title']} :" />
			</h:panelGroup>
			<h:selectOneMenu
				binding="#{pages$socialReseacrhRecommTab.cboCaringList}">
				<f:selectItem itemValue="-1"
					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
				<f:selectItems value="#{pages$ApplicationBean.caringTypeList}" />
				<a4j:support event="onchange"
					action="#{pages$socialReseacrhRecommTab.caringTypeChanged}"
					reRender="lblDisList,chkDisReqId,txtAmountId,rdoPeriodicId,cboPeriodId,txtDurationValueId,cboDurationFreqId,startDate,endDate,txtComments,cboBeneficiaryPerson,txtRejectionReason" />
			</h:selectOneMenu>
			<h:outputLabel
				value="#{msg['PeriodicDisbursements.disbursementList']} :" />
			<h:selectOneMenu id="lblDisList"
				binding="#{pages$socialReseacrhRecommTab.cboDisList}">
				<f:selectItem itemValue="-1"
					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
				<f:selectItems
					value="#{pages$socialReseacrhRecommTab.disbursementType}" />
				<a4j:support event="onchange"
					action="#{pages$socialReseacrhRecommTab.disburementListChanged}"
					reRender="chkDisReqId,txtAmountId,rdoPeriodicId,cboPeriodId,txtDurationValueId,cboDurationFreqId,startDate,endDate" />
			</h:selectOneMenu>


			<h:outputLabel value="#{msg['label.disbursementRequired']} :" />
			<h:selectBooleanCheckbox id="chkDisReqId"
				binding="#{pages$socialReseacrhRecommTab.chkDisReq}">
				<a4j:support event="onclick"
					action="#{pages$socialReseacrhRecommTab.disburemtentRequiredClicked}"
					reRender="socialResearchRecommTab,txtAmountId,rdoPeriodicId,cboPeriodId,txtDurationValueId,cboDurationFreqId,startDate,endDate" />
			</h:selectBooleanCheckbox>

			<h:outputLabel
				value="#{msg['ReviewAndConfirmDisbursements.amount']} :" />
			<h:inputText id="txtAmountId"
				binding="#{pages$socialReseacrhRecommTab.txtAmount}"></h:inputText>

			<%--<h:outputLabel value="#{msg['label.disbursementType']} :" />
			<h:panelGroup>
				<h:selectOneRadio id="rdoPeriodicId"
					binding="#{pages$socialReseacrhRecommTab.rdoPeriodic}">
					<f:selectItem itemLabel="#{msg['disType.periodic']}" itemValue="1" />
					<f:selectItem itemLabel="#{msg['disType.urgent']}" itemValue="0" />
					<a4j:support event="onclick"
						action="#{pages$socialReseacrhRecommTab.disburementTypeChanged}"
						reRender="socialResearchRecommTab,txtAmountId,rdoPeriodicId,cboPeriodId,txtDurationValueId,cboDurationFreqId,startDate,endDate" />
				</h:selectOneRadio>
			</h:panelGroup>
           --%>
			<h:outputLabel
				value="#{msg['commons.report.name.paymentReceipt.period']} :" />
			<h:selectOneMenu id="cboPeriodId"
				binding="#{pages$socialReseacrhRecommTab.cboPeriod}">
				<f:selectItem itemValue="-1"
					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
				<f:selectItems value="#{pages$ApplicationBean.paymentPeriodList}" />
			</h:selectOneMenu>

			<h:outputLabel value="#{msg['label.Duration']} :" />
			<t:panelGrid columns="2">
				<h:inputText style="width:20px" id="txtDurationValueId"
					binding="#{pages$socialReseacrhRecommTab.txtDurationValue}">
					<a4j:support event="onchange"
						action="#{pages$socialReseacrhRecommTab.durationValueChanged}"
						reRender="endDate" />
				</h:inputText>
				<h:selectOneMenu style="width:100px" id="cboDurationFreqId"
					binding="#{pages$socialReseacrhRecommTab.cboDurationFrequency}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems value="#{pages$ApplicationBean.gracePeriodTypeList}" />
					<a4j:support event="onchange"
						action="#{pages$socialReseacrhRecommTab.durationFrequencyChanged}"
						reRender="endDate" />
				</h:selectOneMenu>
			</t:panelGrid>

			<h:outputLabel value="#{msg['commons.startDate']} :" />
			<rich:calendar id="startDate"
				binding="#{pages$socialReseacrhRecommTab.clndrStartDate}"
				locale="#{pages$socialReseacrhRecommTab.locale}" popup="true"
				datePattern="#{pages$socialReseacrhRecommTab.dateFormat}"
				showApplyButton="false" enableManualInput="false">
				<a4j:support event="onchanged"
					action="#{pages$socialReseacrhRecommTab.startDateChanged}"
					reRender="endDate" />
			</rich:calendar>

			<h:outputLabel value="#{msg['commons.endDate']} :" />
			<rich:calendar id="endDate" disabled="true"
				binding="#{pages$socialReseacrhRecommTab.clndrEndDate}"
				locale="#{pages$socialReseacrhRecommTab.locale}" popup="true"
				datePattern="#{pages$socialReseacrhRecommTab.dateFormat}"
				showApplyButton="false" enableManualInput="false" />

			<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*" />
				<h:outputLabel value="#{msg['inspection.comments']} :" />
			</h:panelGroup>
			<t:panelGroup colspan="4">
				<h:inputTextarea style="width:90%" id="txtComments"
					binding="#{pages$socialReseacrhRecommTab.txtAreaComments}"
					value="#{pages$socialReseacrhRecommTab.researchRecommView.comments}"></h:inputTextarea>
			</t:panelGroup>
			<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*" />
				<h:outputLabel
					value="#{msg['ReviewAndConfirmDisbursements.rejectionReason']} :" />
			</h:panelGroup>
			<t:panelGroup colspan="4">
				<h:inputTextarea style="width:90%" id="txtRejectionReason"
					binding="#{pages$socialReseacrhRecommTab.txtRejectionReason}"
					value="#{pages$socialReseacrhRecommTab.researchRecommView.rejectionReason}"></h:inputTextarea>
			</t:panelGroup>

		</t:panelGrid>
	</t:panelGroup>
	<%--Column 3,4 Ends--%>
</t:panelGrid>
<t:div style="width:95%;padding-top:5px;padding-botton:5px;">
	<t:div styleClass="A_RIGHT"
		rendered="#{!pages$socialReseacrhRecommTab.viewMode}">
		<h:commandButton value="#{msg['notes.add']}"
			action="#{pages$socialReseacrhRecommTab.addRecommendation}"
			styleClass="BUTTON" style="width:10%;"></h:commandButton>
	</t:div>
	<t:div style="height:5px;"></t:div>
	<t:div styleClass="contentDiv"
		style="width:95%;padding-top:5px;padding-botton:5px;">
		<t:dataTable id="socialResearchGrid"
			value="#{pages$socialReseacrhRecommTab.researchRecommList}"
			binding="#{pages$socialReseacrhRecommTab.researchRecommDataTable}"
			preserveDataModel="true" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
			width="100%">

			<t:column sortable="true">
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['commons.refNum']}" />
				</f:facet>
				<h:outputText style="white-space: normal;"
					value="#{dataItem.recommendationNumber}" />
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['caringType.Title']}" />
				</f:facet>
				<h:outputText style="white-space: normal;"
					value="#{pages$socialReseacrhRecommTab.englishLocale?dataItem.caringType.caringTypeNameEn:dataItem.caringType.caringTypeNameAr}" />
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['PeriodicDisbursements.columns.beneficiary']}" />
				</f:facet>

				<t:commandLink
					onclick="javascript:openBeneficiaryPopup('#{dataItem.person.personId}');return false;"
					value="#{dataItem.person.personFullName}" styleClass="A_LEFT" />
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['label.Disbursement']}" />
				</f:facet>
				<h:outputText style="white-space: normal;"
					value="#{dataItem.disbursementRequired == '1' ? msg['label.Disbursement.required']: msg['label.Disbursement.notRequired']}" />
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['PeriodicDisbursements.disbursementList']}" />
				</f:facet>
				<h:outputText style="white-space: normal;"
					value="#{pages$socialReseacrhRecommTab.englishLocale?dataItem.disbursementListTypeEn:dataItem.disbursementListTypeAr}" />
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['commons.report.name.paymentReceipt.period']}" />
				</f:facet>
				<h:outputText style="white-space: normal;"
					value="#{pages$socialReseacrhRecommTab.englishLocale?dataItem.periodEn:dataItem.periodAr}" />
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['label.isUrgent']}" />
				</f:facet>
				<h:outputText style="white-space: normal;"
					value="#{dataItem.isPeriodic == '1'?msg['commons.no']:(dataItem.isPeriodic == '0'?msg['commons.yes']:'---')}" />
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['commons.date.From']}" />
				</f:facet>
				<h:outputText value="#{dataItem.startDate}" />
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['commons.date.To']}" />
				</f:facet>
				<h:outputText value="#{dataItem.endDate}" />
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['inspection.comments']}" />
				</f:facet>
				<h:outputText style="white-space: normal;"
					value="#{dataItem.comments}" />
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['commons.replaceCheque.oldChequeAmount']}" />
				</f:facet>
				<h:outputText style="white-space: normal;"
					value="#{dataItem.amount}" />
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['commons.status']}" />
				</f:facet>
				<h:outputText style="white-space: normal;"
					value="#{pages$socialReseacrhRecommTab.englishLocale?dataItem.statusEn:dataItem.statusAr}" />
			</t:column>

			<t:column sortable="true">
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['ReviewAndConfirmDisbursements.rejectionReason']}" />
				</f:facet>
				<h:outputText style="white-space: normal;"
					value="#{dataItem.rejectionReason}" />
			</t:column>

			<t:column id="actioCol" sortable="false">
				<f:facet name="header">
					<t:outputText
						value="#{msg['mems.inheritanceFile.columnLabel.action']}" />
				</f:facet>

				<t:commandLink rendered="#{!pages$socialReseacrhRecommTab.viewMode}"
					onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToSendRecommForApp']}')) return false;"
					action="#{pages$socialReseacrhRecommTab.deleteRecomm}">
					<h:graphicImage id="delete" title="#{msg['commons.delete']}"
						style="cursor:hand;" url="../resources/images/delete.gif" />&nbsp;
				</t:commandLink>

			</t:column>
		</t:dataTable>
	</t:div>
</t:div>