<%@page import="com.avanza.core.constants.CoreConstants"%>
<%@page import="com.avanza.core.security.User"%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ page import="javax.faces.component.UIViewRoot"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>



<script language="JavaScript" type="text/javascript" src="../resources/jscripts/jquery-1.8.2.js"></script>
<script>
var $jq =  jQuery.noConflict();


</script>
<script language="JavaScript" type="text/javascript"	src="../resources/jscripts/commons.js"></script>


<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
	var="msg" />

<table class="header" cellpadding="0" cellspacing="0" border="0"
	width="100%">
	<tr>
		<td>
			<img
				src="../<%=ResourceUtil.getInstance().getPathProperty(
							"img_topheaderleft")%>"
				height="59" align="left" border="0" hspace="0" vspace="0" />
		</td>
		<td width="530px">
			<div class="LOGIN_DATE">
				<%
					User user = (User) request.getSession().getAttribute(
							CoreConstants.CurrentUser);
				%>
				<b> <%=ResourceUtil.getInstance()
							.getProperty("header.logininfo")%> </b>
				<%=user.getFirstName()%>

				&nbsp;&nbsp;&nbsp;&nbsp;
				<b> <%=ResourceUtil.getInstance().getProperty("header.date")%> </b>
				<%=new java.util.Date()%>
			</div>
		</td>
		<td>
			<img
				src="../<%=ResourceUtil.getInstance().getPathProperty(
							"img_topheaderright")%>"
				height="59" border="0" align="left" hspace="0" vspace="0" />
		</td>
	</tr>
</table>
<table class="header1" cellpadding="0" cellspacing="0" border="0"
	width="100%">
	<tr>
		<td>
			<img
				src="../<%=ResourceUtil.getInstance().getPathProperty(
							"img_blackframeleft1")%>"
				style="height: 27px;" />
		</td>
		<td>

		</td>
		<td>
			<table id="headerIconsTable" cellpadding="0" cellspacing="0"
				class="HEADER_ICONS">
				<tr id="headerIconsTR">

					<td class="BUTTON_TD">
					</td>
					<td id="alertsTD" class="BUTTON_TD"
						style="position: relative; z-index: 1000;" width="5%">

						<img id="alertImg" src="../resources/images/app_icons/Alert.png" style="height: 17px;padding:0px;margin:0px;cursor:pointer" 
							onclick="javaScript:onAlertsClick();" style="position: relative;" title="<%=ResourceUtil.getInstance().getProperty("alert.title.newonlinerequests")%>"/>
						<span id= "alertCountBadge" class="ALERT_BADGE_HIDDEN" onclick="javaScript:onAlertsClick();" title="<%=ResourceUtil.getInstance().getProperty("alert.title.newonlinerequests")%>"></span>
						<div id="alertDiv" class="ALERT_DIV_DISABLE">
							<div class="ALERT_DIV_UPARROW">
								<img id="upArrow" src="../resources/images/uparrow.png" />
							</div>
							<div>
								<fieldset>
									<legend>
										Notifications:
									</legend>
									LoggedInUser:
									<input type="text" id="lblAlert">
								</fieldset>
							</div>
						</div>

					</td>

					<td id="releaseTaskTD" class="BUTTON_TD" width="5%">

						<img onclick="javaScript:openReleaseTasksPopup();"
							src="../resources/images/app_icons/Release.png" />

					</td>
					<td id="logouttd" width="5%">
						<a href="logout.jsf"> <img
								src="../resources/images/Left Panel/picture_go.png" /> </a>
					</td>


				</tr>
			</table>
		</td>
	</tr>
</table>
