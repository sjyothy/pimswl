<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Auction
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	    function resetValues()
      	{
      		document.getElementById("searchFrm:progNumber").value="";
			
      	    $('searchFrm:auctionRegEndDateTo').component.resetSelectedDate();
      	        document.getElementById("searchFrm:auctionStartTimeMM").selectedIndex=0;
      	    
			
        }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">

					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>


					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>


						<td width="83%" height="470px" valign="top"
							class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['socialProgram.search.title']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" height="100%">
										<div class="SCROLLABLE_SECTION AUC_SCH_SS"
											style="height: 95%; width: 100%; # height: 95%; # width: 100%;">
											<h:form id="searchFrm"
												style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
												<t:div styleClass="MESSAGE">
													<table border="0" class="layoutTable">
														<tr>
															<td>
																<h:messages></h:messages>
																<h:outputText id="errorMessage"
																	value="#{pages$socialProgramSearch.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
															</td>
														</tr>
													</table>
												</t:div>
												<div class="MARGIN">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="DETAIL_SECTION">
														<h:outputLabel value="#{msg['commons.searchCriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER">
															<tr>
																<td width="97%">
																	<table width="100%">
																		<tr>

																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['socialProgram.lbl.ProgramNum']}"></h:outputLabel>
																			</td>

																			<td width="25%">
																				<h:inputText id="progNumber"
																					value="#{pages$socialProgramSearch.searchCriteria.programNum}"
																					maxlength="20"></h:inputText>

																			</td>

																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['socialProgram.lbl.ProgramName']}"></h:outputLabel>
																			</td>

																			<td width="25%">
																				<h:inputText id="progName"
																					value="#{pages$socialProgramSearch.searchCriteria.programName}"
																					maxlength="20"></h:inputText>

																			</td>


																		</tr>
																		
																		<tr>

																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['socialProgram.lbl.ProgramType']}"></h:outputLabel>

																			</td>

																			<td width="25%">


																				<h:selectOneMenu id="selectProgramType"
																					value="#{pages$socialProgramSearch.searchCriteria.programType}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.socialProgramTypeList}" />
																				</h:selectOneMenu>
																			</td>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['socialProgram.lbl.TargetGrp']}"></h:outputLabel>

																			</td>

																			<td width="25%">


																				<h:selectOneMenu id="selectProgramTarget"
																					value="#{pages$socialProgramSearch.searchCriteria.targetGrp}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.socialProgramTargetGrpList}" />
																				</h:selectOneMenu>
																			</td>
																			</tr>
																		<tr>

																			<td class="BUTTON_TD" colspan="4" />

																			<table cellpadding="1px" cellspacing="1px">
																				<tr>



																					<td colspan="2">
																						<h:commandButton styleClass="BUTTON"
																							value="#{msg['commons.search']}"
																							action="#{pages$socialProgramSearch.doSearch}"
																							style="width: 75px" >
																						</h:commandButton>
																						<h:commandButton styleClass="BUTTON"
																							value="#{msg['commons.Add']}"
																								action="#{pages$socialProgramSearch.doAdd}"
																							style="width: 75px" ></h:commandButton>
																						
																						<h:commandButton styleClass="BUTTON"
																							value="#{msg['commons.clear']}"
																							onclick="javascript:resetValues();"
																							style="width: 75px" ></h:commandButton>

																					</td>


																				</tr>
																			</table>
																			</td>
																		</tr>
																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>
															</tr>

														</table>
													</div>
												</div>
												<div
													style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
													<div class="imag">
														&nbsp;
													</div>
													<div class="contentDiv" style="width: 100%; # width: 99%;">
														<t:dataTable id="dt1"
															value="#{pages$socialProgramSearch.list}"
															binding="#{pages$socialProgramSearch.dataTable}"
															rows="#{pages$socialProgramSearch.paginatorRows}"
															preserveDataModel="false" preserveSort="false"
															var="dataItem" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true" width="100%">

															<t:column id="progNumCol" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="programNumCol"
																		actionListener="#{pages$socialProgramSearch.sort}"
																		value="#{msg['socialProgram.lbl.ProgramNum']}" arrow="true">
																		<f:attribute name="sortField" value="programNum" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.programNum}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="programName" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="programNameCol"
																		actionListener="#{pages$socialProgramSearch.sort}"
																		value="#{msg['socialProgram.lbl.ProgramName']}" arrow="true">
																		<f:attribute name="sortField" value="programName" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.programName}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>

															<t:column id="programType">
																<f:facet name="header">
																	<t:outputText value="#{msg['socialProgram.lbl.ProgramType']}" />
																</f:facet>
																<t:outputText
																	value="#{pages$socialProgramSearch.englishLocale? dataItem.programTypeEn:dataItem.programTypeAr}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="status">
																<f:facet name="header">
																	<t:outputText value="#{msg['socialProgram.lbl.Status']}" />
																</f:facet>
																<t:outputText
																	value="#{pages$socialProgramSearch.englishLocale? dataItem.statusEn:dataItem.statusAr}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="createdBy">
																<f:facet name="header">
																	<t:outputText value="#{msg['socialProgram.lbl.CreatedBy']}" />
																</f:facet>
																<t:outputText
																	value="#{pages$socialProgramSearch.englishLocale? dataItem.createdByEn:dataItem.createdByAr}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="startDate">
																<f:facet name="header">
																	<t:outputText value="#{msg['socialProgram.lbl.StartDate']}" />
																</f:facet>
																<t:outputText value="#{dataItem.startDate}"
																	style="white-space: normal;" styleClass="A_LEFT">
																	<f:convertDateTime
																		pattern="#{pages$socialProgramSearch.dateFormat}"
																		timeZone="#{pages$socialProgramSearch.timeZone}" />
																</t:outputText>
															</t:column>



															<t:column id="description">
																<f:facet name="header">
																	<t:outputText value="#{msg['socialProgram.lbl.Description']}" />
																</f:facet>
																<t:outputText value="#{dataItem.description}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="actionCol" sortable="false" width="100"
																style="TEXT-ALIGN: center;">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.action']}" />
																</f:facet>

																<t:commandLink
																	action="#{pages$socialProgramSearch.onEdit}">
																	<h:graphicImage title="#{msg['commons.edit']}"
																		url="../resources/images/edit-icon.gif" />
																</t:commandLink>
															</t:column>



														</t:dataTable>
													</div>
													<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
														style="width:100%;#width:100%;">
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td class="RECORD_NUM_TD">
																	<div class="RECORD_NUM_BG">
																		<table cellpadding="0" cellspacing="0"
																			style="width: 182px; # width: 150px;">
																			<tr>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText value=" : " />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{pages$socialProgramSearch.recordSize}" />
																				</td>
																			</tr>
																		</table>
																	</div>
																</td>
																<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																	align="right">

																	<t:div styleClass="PAGE_NUM_BG" style="#width:20%">

																		<table cellpadding="0" cellspacing="0">
																			<tr>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																				</td>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{pages$socialProgramSearch.currentPage}" />
																				</td>
																			</tr>
																		</table>
																	</t:div>
																	<TABLE border="0" class="SCH_SCROLLER">
																		<tr>
																			<td>
																				<t:commandLink
																					action="#{pages$socialProgramSearch.pageFirst}"
																					disabled="#{pages$socialProgramSearch.firstRow == 0}">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$socialProgramSearch.pagePrevious}"
																					disabled="#{pages$socialProgramSearch.firstRow == 0}">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:dataList
																					value="#{pages$socialProgramSearch.pages}"
																					var="page">
																					<h:commandLink value="#{page}"
																						actionListener="#{pages$socialProgramSearch.page}"
																						rendered="#{page != pages$socialProgramSearch.currentPage}" />
																					<h:outputText value="<b>#{page}</b>" escape="false"
																						rendered="#{page == pages$socialProgramSearch.currentPage}" />
																				</t:dataList>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$socialProgramSearch.pageNext}"
																					disabled="#{pages$socialProgramSearch.firstRow + pages$socialProgramSearch.rowsPerPage >= pages$socialProgramSearch.totalRows}">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$socialProgramSearch.pageLast}"
																					disabled="#{pages$socialProgramSearch.firstRow + pages$socialProgramSearch.rowsPerPage >= pages$socialProgramSearch.totalRows}">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblL"></t:graphicImage>
																				</t:commandLink>
																			</td>
																		</tr>
																	</TABLE>
																</td>
															</tr>
														</table>
													</t:div>
												</div>

											</h:form>

										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>