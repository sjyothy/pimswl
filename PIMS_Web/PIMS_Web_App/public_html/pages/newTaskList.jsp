<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript">
	    function resetValues()
	    {        
		    document.getElementById("TaskList:applicationTitle").value="";
			document.getElementById("TaskList:taskTitle").value="";
			$('TaskList:auctionDate').component.resetSelectedDate();
			$('TaskList:auctionCreationDate').component.resetSelectedDate();
			 }        
			 function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		    
		   
		    document.getElementById("TaskList:requestApplicant").value=personName;
			 
		   /* 
		    if(hdnPersonType=='TENANT')
		    {
			 //document.getElementById("formRequest:txtTenantName").value=personName;
			 document.getElementById("TaskList:hdntenantId").value=personId;
			if(document.getElementById("TaskList:hdnApplicantId").value=="")
			 	document.getElementById("TaskList:hdnApplicantId").value=personId;
	         //document.getElementById("formRequest:hdntenanttypeId").value=typeId;
	         
	        }
	       
	        
	        document.forms[0].submit();
	        */
	       
	}
	function showPopup(personType)
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   //window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
	    window.open('SearchPerson.jsf?persontype='+personType+'&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}

</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>

		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['TaskList.Header']}"
											styleClass="HEADER_FONT" style="padding:10px" />
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg">
									</td>
									<!--Use this below only for desiging forms Please do not touch other sections -->


									<td width="100%" height="99%" valign="top" nowrap="nowrap">
										<div style="height: 100;">
											<div class="SCROLLABLE_SECTION"
												style="height: 445px; # height: 450px;">
												<h:form id="TaskList"
													style="width:99%;#width:96%; padding-left:5px;padding-right:5px;">
													<h:inputHidden id="hdntenantId"
														value="#{pages$newTaskList.hdnTenantId}" />

													<h:inputHidden id="hdnApplicantId"
														value="#{pages$newTaskList.hdnApplicantId}" />
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:outputText value="#{pages$newTaskList.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText value="#{pages$newTaskList.infoMessage}"
																	escape="false" styleClass="INFO_FONT" />
															</td>
														</tr>
													</table>
													<br />
													<div class="MARGIN">
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td style="font-size:0px;">
																	<IMG
																		src="../<h:outputText value="#{path.img_section_left}"/>"
																		class="TAB_PANEL_LEFT" />
																</td>
																<td width="100%" style="font-size:0px;">
																	<IMG
																		src="../<h:outputText value="#{path.img_section_mid}"/>"
																		class="TAB_PANEL_MID" />
																</td>
																<td style="font-size:0px;">
																	<IMG
																		src="../<h:outputText value="#{path.img_section_right}"/>"
																		class="TAB_PANEL_RIGHT" />
																</td>
															</tr>
														</table>

														<div class="DETAIL_SECTION">
															<h:outputLabel value="#{msg['commons.searchCriteria']}"
																styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
															<table cellpadding="1px" cellspacing="2px"
																class="DETAIL_SECTION_INNER">
																<tr>
																	<td width="97%">
																		<table width="100%">
																			<tr>
																				<td width="25%">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['TaskList.SearchCriteria.FromAssignedDate']}:"></h:outputLabel>
																				</td>
																				<td width="25%">
																					<rich:calendar id="auctionDate"
																						value="#{pages$newTaskList.criteria.assignedDateFrom}"
																						locale="#{pages$newTaskList.locale}" popup="true"
																						datePattern="#{pages$newTaskList.dateFormat}"
																						showApplyButton="false" enableManualInput="false"
																						inputStyle="width: 170px; height: 14px" />
																				</td>
																				<td width="25%">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['TaskList.SearchCriteria.ToAssignedDate']}:" />
																				</td>
																				<td width="25%">
																					<rich:calendar id="auctionCreationDate"
																						value="#{pages$newTaskList.criteria.assignedDateTo}"
																						locale="#{pages$newTaskList.locale}" popup="true"
																						datePattern="#{pages$newTaskList.dateFormat}"
																						showApplyButton="false" enableManualInput="false"
																						inputStyle="width: 170px; height: 14px" />
																				</td>
																			</tr>


																			<tr>
																				<td width="25%">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['TaskList.SearchCriteria.RequestNumber']}:"></h:outputLabel>
																				</td>
																				<td width="25%">
																					<h:inputText id="requestNumber"
																						value="#{pages$newTaskList.criteria.requestNumber}"></h:inputText>
																				</td>
																				<td width="25%">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['TaskList.SearchCriteria.RequestCreatedBy']}:"></h:outputLabel>
																				</td>
																				<td width="25%">
																					<h:selectOneMenu id="requestCreatedBy"
																						value="#{pages$newTaskList.criteria.requestCreatedBy}">
																						<f:selectItem itemValue="-1"
																							itemLabel="#{msg['commons.pleaseSelect']}" />
																						<f:selectItems
																							value="#{pages$ApplicationBean.userNameList}" />
																					</h:selectOneMenu>
																				</td>
																			</tr>
																			<tr>
																				<td width="25%">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['TaskList.SearchCriteria.ApplicationTitle']}:"></h:outputLabel>
																				</td>
																				<td width="25%">
																					<h:inputText id="applicationTitle"
																						value="#{pages$newTaskList.criteria.applicationTitle}"></h:inputText>
																				</td>
																				<td width="25%">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['TaskList.SearchCriteria.TaskTitle']}:"></h:outputLabel>
																				</td>
																				<td width="25%">
																					<h:inputText id="taskTitle"
																						value="#{pages$newTaskList.taskTitle}"></h:inputText>
																				</td>
																			</tr>


																			<tr>

																				<td width="25%">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['TaskList.SearchCriteria.ApplicantName']}:"></h:outputLabel>
																				</td>
																				<td width="25%">
																				<h:panelGroup>
																					<h:inputText id="requestApplicant"
																						value="#{pages$newTaskList.criteria.requestApplicant}"></h:inputText>
																					<h:graphicImage
																							url="../resources/images/app_icons/Search-tenant.png"
																							onclick="showPopup('#{pages$newTaskList.personTenant}');"
																							style="MARGIN: 0px 0px -4px; CURSOR: hand;"
																							alt="#{msg[contract.searchTenant]}"></h:graphicImage>
																				</h:panelGroup>				
																				</td>
																				
																				
																				<td width="25%">
																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['TaskList.SearchCriteria.ContractNumber']}:"></h:outputLabel>
																				</td>
																				<td width="25%">
																					<h:inputText id="contractNumber"
																						value="#{pages$newTaskList.criteria.contractNumber}"></h:inputText>
																				</td>
																		
																			</tr>
																			<tr>
																			
																				
																						<td width="25%">
																					<h:outputLabel
																					rendered="false" 
																					styleClass="LABEL"
																						value="#{msg['TaskList.SearchCriteria.ContractTenantName']}:"></h:outputLabel>
																				</td>
																				<td width="25%">
																					<h:panelGroup rendered="false">
																						<h:inputText id="contractTenantName"
																							value="#{pages$newTaskList.criteria.contractTenantName}"></h:inputText>
																						<h:graphicImage
																							url="../resources/images/app_icons/Search-tenant.png"
																							onclick="showPopup('#{pages$newTaskList.personTenant}');"
																							style="MARGIN: 0px 0px -4px; CURSOR: hand;"
																							alt="#{msg[contract.searchTenant]}"></h:graphicImage>
																					</h:panelGroup>
																				</td>
																				<td width="25%">
																					<h:outputLabel styleClass="LABEL" rendered="false"
																						value="#{msg['TaskList.SearchCriteria.ContractUnitNumber']}:"></h:outputLabel>
																				</td>
																				<td width="25%">
																					<h:inputText rendered="false"
																						id="contractUnitNumber"
																						value="#{pages$newTaskList.criteria.contractUnitNumber}"></h:inputText>
																				</td>
																			</tr>
																			<tr>
																				<td width="25%">
																					<h:outputLabel styleClass="LABEL" 
																						value="#{msg['searchInheritenceFile.fileNo']}:"></h:outputLabel>
																				</td>
																				<td width="25%">
																					<h:inputText 
																						id="fileNumber"
																						value="#{pages$newTaskList.criteria.inheritanceFileNumber}"></h:inputText>
																				</td>
																				<td width="25%">
																					<h:outputLabel styleClass="LABEL" 
																						value="#{msg['endowmentFile.title.heading']}:"></h:outputLabel>
																				</td>
																				<td width="25%">
																					<h:inputText 
																						id="endowmentFileNumber"
																						value="#{pages$newTaskList.criteria.endowmentFileNumber}"></h:inputText>
																				</td>
																			</tr>
																			</tr>
																			<tr>
																				<td class="BUTTON_TD" colspan="4">
																					<h:commandButton styleClass="BUTTON" id ="onSearch"
																						value="#{msg['commons.search']}"
																						action="#{pages$newTaskList.onSearch}"
																						style="width: 75px" tabindex="7">
																					</h:commandButton>

																					<h:commandButton styleClass="BUTTON"
																						value="#{msg['commons.clear']}"
																						onclick="javascript:resetValues();"
																						style="width: 75px" tabindex="7"></h:commandButton>
																				</td>
																			</tr>





																		</table>
																	</td>
																	<td width="3%">
																		&nbsp;
																	</td>
																</tr>

															</table>
														</div>
													</div>
													<div
														style="padding-bottom: 7px; padding-left: 10px; padding-top: 7px;">
														<div class="imag">
															&nbsp;
														</div>
														<div class="contentDiv" width="99%">
															<t:dataTable width="100%" id="dtTaskList"
																value="#{pages$newTaskList.dataList}"
																binding="#{pages$newTaskList.dataTable}"
																rows="#{pages$newTaskList.paginatorRows}"
																preserveDataModel="false" preserveSort="false"
																var="userTask" rowClasses="row1,row2" rules="all"
																renderedIfEmpty="true">

																<t:column id="dcTaskNumber" style="white-space:normal"
																	sortable="true" width="5%">
																	<f:facet name="header">
																		<t:outputText
																			style="text-align:right;white-space:normal"
																			value="#{msg['TaskList.DataTable.TaskNumber']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{userTask.taskNumber}" />
																</t:column>
																<t:column id="dcInstanceId" style="white-space:normal"
																	sortable="true" width="5%">
																	<f:facet name="header">
																		<t:outputText
																			style="text-align:right;white-space:normal"
																			value="#{msg['TaskList.DataTable.InstanceNumber']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{userTask.instanceId}" />
																</t:column>
																<t:column id="dcRequestTitleEn"
																	style="white-space:normal"
																	rendered="#{pages$newTaskList.isEnglishLocale}"
																	sortable="true" width="25%">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['TaskList.DataTable.RequestTitle']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		style="white-space:normal"
																		value="#{userTask.requestTitleEn}" />
																</t:column>
																<t:column style="padding-right:5px;white-space:normal"
																	id="dcRequestTitleAr"
																	rendered="#{pages$newTaskList.isArabicLocale}"
																	sortable="true" width="25%">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['TaskList.DataTable.RequestTitle']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		style="white-space:normal"
																		value="#{userTask.requestTitleAr}" />
																</t:column>
																<t:column id="dcTaskTitleEn" style="white-space:normal"
																	rendered="#{pages$newTaskList.isEnglishLocale}"
																	sortable="true" width="25%">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['TaskList.DataTable.TaskTitle']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		style="white-space:normal"
																		value="#{userTask.taskSubjectEn}" />
																</t:column>
																<t:column style="padding-right:5px;white-space:normal"
																	id="dcTaskTitleAr"
																	rendered="#{pages$newTaskList.isArabicLocale}"
																	sortable="true" width="25%">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['TaskList.DataTable.TaskTitle']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		style="white-space:normal"
																		value="#{userTask.taskSubjectAr}" />
																</t:column>
																<t:column style="padding-right:5px;white-space:normal"
																	id="dcContractNumber" sortable="true" width="25%">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['commons.refNum']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		style="white-space:normal"
																		value="#{userTask.referenceEn}" />
																</t:column>
																<t:column style="padding-right:5pxwhite-space:normal"
																	id="dcTenantName" sortable="true" width="25%">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['commons.replaceCheque.tenantName']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		style="white-space:normal"
																		value="#{userTask.personName}" />
																</t:column>
																
																<t:column style="padding-right:5px;white-space:normal"
																	id="dcSentByUser" rendered="true" sortable="true"
																	width="15%">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['TaskList.DataTable.SentByUser']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		style="white-space:normal"
																		value="#{userTask.sentByUser}" />
																</t:column>
																<t:column style="white-space:normal" id="dcAssignedDate"
																	defaultSorted="true" sortable="true" width="15%">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['TaskList.DataTable.AssignedDate']}" />
																	</f:facet>
																	<h:outputText style="white-space:normal"
																		value="#{userTask.assignedDate}">
																		<f:convertDateTime
																			pattern="#{pages$newTaskList.dateFormat}"
																			timeZone="#{pages$newTaskList.timeZone}" />
																	</h:outputText>
																</t:column>
																<t:column style="white-space:normal" id="dcExpiryDate"
																	rendered="false" sortable="true" defaultSorted="true"
																	width="12%">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['TaskList.DataTable.ExpiryDate']}" />
																	</f:facet>
																	<h:outputText style="white-space:normal"
																		value="#{userTask.expiryDate}">
																		<f:convertDateTime
																			pattern="#{pages$newTaskList.dateFormat}"
																			timeZone="#{pages$newTaskList.timeZone}" />
																	</h:outputText>

																</t:column>
																<t:column id="dcTaskId" width="4%" rendered="false"
																	style="white-space:normal">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['TaskList.DataTable.TaskId']}" />
																	</f:facet>
																	<t:outputText style="white-space:normal"
																		value="#{userTask.taskId}" />
																</t:column>
																<t:column style="padding-right:5px;white-space:normal;"
																	id="dcTaskAccquiredBy" width="5%">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['TaskList.DataTable.ClaimedBy']}" />
																	</f:facet>
																	<t:outputText style="white-space:normal"
																		value="#{userTask.claimBy}" />
																</t:column>
																
																<t:column style="padding-right:5px;white-space:normal;"
																	 width="5%">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['TaskList.DataTable.assginUserOrGroup']}" />
																	</f:facet>
																	<t:outputText style="white-space:normal" rendered="#{( userTask.assignUser != null) }"
																		value="#{userTask.assignUser}" />
                                                                                                                                                
																		<t:outputText style="white-space:normal" rendered="#{( userTask.assignGroup != null) }"
																		value="#{userTask.assignGroup}" />
																</t:column>
																
																
																<t:column id="col7" width="5%"  
																	style="white-space:normal">
																	<f:facet name="header">
																		<t:outputText style="text-align:right" value="" />
																	</f:facet> 
																	<h:commandLink action="#{pages$newTaskList.Claim}"
																		rendered="#{userTask.claimBy eq null || empty userTask.claimBy }">
																		<h:graphicImage id="claimIcon" 
																			title="#{msg['commons.claim']}"
																			url="../resources/images/app_icons/Claim.png" />
																	</h:commandLink> 
															&nbsp;
															<h:commandLink action="#{pages$newTaskList.Release}"
																		onclick="if (!confirm('#{msg['TaskList.messages.confirmRelease']}')) return"
																		rendered="#{( userTask.claimBy == pages$newTaskList.loggedInUserId ) }">
																		<h:graphicImage id="releaseIcon"
																			title="#{msg['commons.release']}"
																			url="../resources/images/app_icons/Release.png" />
																	</h:commandLink>
															&nbsp;
															<h:commandLink action="#{pages$newTaskList.actionClick}"
																		rendered="#{(userTask.claimBy == pages$newTaskList.loggedInUserId )}">
																		<h:graphicImage id="editIcon"
																			title="#{msg['commons.edit']}"
																			url="../resources/images/edit-icon.gif" />
																	</h:commandLink>
																</t:column>
															</t:dataTable>
														</div>
														<t:div styleClass="contentDivFooter">
															<table cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<td class="RECORD_NUM_TD">
																		<div class="RECORD_NUM_BG">
																			<table cellpadding="0" cellspacing="0"
																				style="width: 182px; # width: 150px;">
																				<tr>
																					<td class="RECORD_NUM_TD">
																						<h:outputText
																							value="#{msg['commons.recordsFound']}" />
																					</td>
																					<td class="RECORD_NUM_TD">
																						<h:outputText value=" : " />
																					</td>
																					<td class="RECORD_NUM_TD">
																						<h:outputText value="#{pages$newTaskList.recordSize}" />
																					</td>
																				</tr>
																			</table>
																		</div>
																	</td>
																	<td class="BUTTON_TD" style="width: 50%" align="right">
																		<CENTER>
																			<t:dataScroller id="scroller" for="dtTaskList"
																				paginator="true" fastStep="1"
																				paginatorMaxPages="#{pages$newTaskList.paginatorMaxPages}"
																				immediate="false" styleClass="scroller"
																				paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true"
																				pageIndexVar="pageNumber"
																				paginatorTableStyle="grid_paginator"
																				layout="singleTable"
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																				style="text-align:right;padding-left:0px;#padding-left:700px;padding-right:100px;"
																				paginatorActiveColumnStyle="font-weight:bold;"
																				paginatorRenderLinkForActive="false">
																				<f:facet name="first">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblF"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastrewind">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastforward">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																				</f:facet>
																				<f:facet name="last">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblL"></t:graphicImage>
																				</f:facet>
																				<t:div styleClass="PAGE_NUM_BG" style="width:300">
																					<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																					<table cellpadding="0" cellspacing="0">
																						<tr>
																							<td>
																								<h:outputText styleClass="PAGE_NUM"
																									value="#{msg['commons.page']}" />
																							</td>
																							<td>
																								<h:outputText styleClass="PAGE_NUM"
																									style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																									value="#{requestScope.pageNumber}" />
																							</td>
																						</tr>
																					</table>

																				</t:div>
																			</t:dataScroller>
																		</CENTER>
																	</td>
																</tr>
															</table>
														</t:div>
														<div>
												</h:form>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>
