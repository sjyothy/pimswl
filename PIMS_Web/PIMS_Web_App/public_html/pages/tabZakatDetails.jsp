
<t:div id="tabDDetails" style="width:100%;">


	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dtZakat"
			value="#{pages$zakatDeductionManage.dataList}"
			binding="#{pages$zakatDeductionManage.dataTable}"
			rows="#{pages$zakatDeductionManage.paginatorRows}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
			width="100%">
			<t:column id="refNum" sortable="true">
				<f:facet name="header">
					<t:commandSortHeader columnName="refNum"
						actionListener="#{pages$zakatSearch.sort}"
						value="#{msg['commons.refNum']}" arrow="true">
						<f:attribute name="sortField" value="refNum" />
					</t:commandSortHeader>
				</f:facet>
				<t:outputText value="#{dataItem.refNum}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>


			<t:column id="passportName" sortable="true">
				<f:facet name="header">
					<t:outputText id="nameTxt" value="#{msg['commons.Name']}" />
				</f:facet>
				<t:commandLink value="#{dataItem.person.passportName}"
					style="white-space: normal;" styleClass="A_LEFT"
					action="#{pages$zakatDeductionManage.onNameClicked}">
				</t:commandLink>
			</t:column>

			<t:column id="status" sortable="true">
				<f:facet name="header">
					<t:outputText id="statusTxt" value="#{msg['commons.status']}" />
				</f:facet>
				<t:outputText
					value="#{pages$zakatDeductionManage.englishLocale? 
															         dataItem.status.dataDescEn:
															         dataItem.status.dataDescAr
							}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>

			<t:column id="createdOn" sortable="true">
				<f:facet name="header">
					<t:outputText id="CreatedOnTxt" value="#{msg['commons.createdOn']}" />
				</f:facet>
				<t:outputText value="#{dataItem.createdOn}"
					style="white-space: normal;" styleClass="A_LEFT">
					<f:convertDateTime
						timeZone="#{pages$zakatDeductionManage.timeZone}"
						pattern="#{pages$zakatDeductionManage.dateFormat}" />
				</t:outputText>
			</t:column>
			<t:column id="dueOn" sortable="true">
				<f:facet name="header">
					<t:outputText id="dueOnTxt" value="#{msg['zakat.lbl.dueOn']}" />
				</f:facet>
				<t:outputText value="#{dataItem.dueOn}" style="white-space: normal;"
					styleClass="A_LEFT">
					<f:convertDateTime
						timeZone="#{pages$zakatDeductionManage.timeZone}"
						pattern="#{pages$zakatDeductionManage.dateFormat}" />
				</t:outputText>
			</t:column>
			<t:column id="amount" sortable="true">
				<f:facet name="header">
					<t:outputText id="amountTxt" value="#{msg['commons.amount']}" />
				</f:facet>
				<t:outputText value="#{dataItem.amount}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="amountToDeduct" sortable="true">
				<f:facet name="header">
				<t:outputText id="amountToDeducttxt" value="#{msg['zakat.lbl.amountToDeduct']}" />
					
				</f:facet>
				<t:outputText value="#{dataItem.amountToDeduct}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>

			<t:column id="balance" sortable="true">
				<f:facet name="header">
					<t:outputText id="balanceTxt"
						value="#{msg['mems.normaldisb.label.balance']}" />

				</f:facet>
				<t:outputText value="#{dataItem.balance}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>

			<t:column id="actionCol" sortable="false" width="100"
				style="TEXT-ALIGN: center;">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.action']}" />
				</f:facet>


				<h:graphicImage id="zakatTrx"
					onclick="javaScript:openZakatPersonalAccountTransLinkPopup('#{dataItem.zakatId}')"
					title="#{msg['zakat.heading.zakatTrx']}"
					url="../resources/images/app_icons/replaceCheque.png" />

				<h:graphicImage id="zakatHistory"
					onclick="javaScript:openZakatHistoryPopup('#{dataItem.zakatId}')"
					style="margin-right:5px;"
					title="#{msg['zakat.heading.zakatHistory']}"
					url="../resources/images/app_icons/manage_tender.png" />

			</t:column>



		</t:dataTable>
	</t:div>
	<t:div id="pagingDivPaySch" styleClass="contentDivFooter"
		style="width:99.2%;">
		<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
			cellspacing="1">
			<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
				<t:div styleClass="JUG_NUM_REC_ATT">
					<h:outputText value="#{msg['commons.records']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$zakatDeductionManage.zakatListRecordsSize}" />
				</t:div>
			</t:panelGroup>
			<t:panelGroup>

				<t:dataScroller id="scrollerZakatList" for="dtZakat"
					paginator="true" fastStep="1" paginatorMaxPages="15"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
					styleClass="SCH_SCROLLER" pageIndexVar="pageNumber"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}" id="lblFZakatList"></t:graphicImage>
					</f:facet>
					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="lblFRZakatList"></t:graphicImage>
					</f:facet>
					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="lblFFZakatList"></t:graphicImage>
					</f:facet>
					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}" id="lblLZakatList"></t:graphicImage>
					</f:facet>
					<t:div>
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<h:outputText styleClass="PAGE_NUM"
										style="font-size:9;color:black;"
										value="#{msg['mems.normaldisb.label.totalAmount']}:" />
								</td>
								<td>
									<h:outputText styleClass="PAGE_NUM"
										style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:9;font-weight:bold;color:black"
										value="#{pages$zakatDeductionManage.totalZakatAmount}" />
								</td>
							</tr>
						</table>
					</t:div>

				</t:dataScroller>
			</t:panelGroup>
		</t:panelGrid>
	</t:div>
</t:div>


