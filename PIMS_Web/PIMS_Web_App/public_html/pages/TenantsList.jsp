
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
		
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>	
<html style="overflow:hidden;">
<head>
		<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>

		<script language="javascript" type="text/javascript">
  
   
  
	 function sendToParent()//tenantId,tenantNum,typeId)
	{
	   
      
     //  window.opener.populateTenant(tenantId,tenantNum,typeId);
       window.opener.document.forms[0].submit();
		window.close();
	  
	}
	
		function clearWindow()
	{
	        document.getElementById("formTenantList:txtName").value="";
		    document.getElementById("formTenantList:txtPassportNumber").value="";
		    document.getElementById("formTenantList:txtVisaNumber").value="";
		    document.getElementById("formTenantList:txtLicenseNumber").value="";
		    document.getElementById("formTenantList:txtSecurityCardNo").value="";
		    document.getElementById("formTenantList:txtTenantNumber").value="";         
	
		    }
	
    </script>

<%

   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1

   response.setHeader("Pragma","no-cache"); //HTTP 1.0

   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

%>



	</head>
	<body>

<body dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE"> 
    <!-- Header --> 
<table width="100%" cellpadding="0" cellspacing="0"  border="0">

    <tr>
			<c:choose>
			<c:when test="${!pages$TenantsList.isModeSelectOnePopUp}">
			<td colspan="2">
		        <jsp:include page="header.jsp"/>
		     </td>
		     </c:when>
		     </c:choose>
    </tr>
		
<tr width="100%">
			<%if (request.getAttribute("modeSelectOnePopUp")!=null) {%>
			<input type="hidden" id ="personName" value="<%=request.getAttribute("modeSelectOnePopUp") %>" >
			<%} %>
		    <c:choose>
			<c:when test="${!pages$TenantsList.isModeSelectOnePopUp}">
			<td class="divLeftMenuWithBody" width="17%" >
	        		<jsp:include page="leftmenu.jsp"/>
	    		</td>
		     </c:when>
		     </c:choose>	
			

    <td width="83%" valign="top" height="505px" class="divBackgroundBody">
        <table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0"  border="0">
        <tr>
         		 <td class="HEADER_TD">
         		    &nbsp;&nbsp; <h:outputLabel value="#{msg['tenants.tenantsList']}" styleClass="HEADER_FONT"/>
		               
                 </td>
				    
            <td width="100%">
                &nbsp;
            </td>
	</tr>
        </table>
    
        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
          <tr valign="top">
             <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
             </td> 
	  
  <td width="100%" height="460px" valign="top" nowrap="nowrap">
			<div  class="SCROLLABLE_SECTION"  >
				
			<h:form id="formTenantList" style="width:97%"> 
			 <div style="height:25px;">
			    <table border="0" class="layoutTable">
												<tr>
      									    	      <td colspan="6">
														<h:outputText value="#{pages$TenantsList.errorMessages}" escape="false" styleClass="INFO_FONT" style="padding:10px;"/>
													  </td>
												</tr>
									</table>
							</div>		
			<div class= "MARGIN">
			<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
				</table>
				 <div class="DETAIL_SECTION">
             		<h:outputLabel value="#{msg['inquiry.searchcriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>				
											

 										 <table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" width="100%" >

												
												<tr>
												     <td style="width:20%">
														<h:outputLabel styleClass="LABEL"  value="#{msg['contract.TenantsNumber']}:"></h:outputLabel>
													</td>
													<td style="width:30%">
												        <h:inputText style="width:150px;"  styleClass="A_LEFT_SMALL" id="txtTenantNumber" value="#{pages$TenantsList.number}">
												        
												        </h:inputText>
													</td>
													<td style="width:20%">
														<h:outputLabel  styleClass="LABEL" value="#{msg['tenants.name']}:"></h:outputLabel>
													</td>
													<td style="width:30%">
															<h:inputText style="width:150px;"  styleClass="A_LEFT_SMALL"  id="txtName" value="#{pages$TenantsList.name}"/>
													</td>
												</tr>
												<tr>
												<td style="width:20%">
														<h:outputLabel   styleClass="LABEL"  value="#{msg['commons.passport.number']}:"></h:outputLabel>
													</td>
													<td style="width:30%">
												        <h:inputText style="width:150px;"  styleClass="A_LEFT_SMALL" id="txtPassportNumber" value="#{pages$TenantsList.passportNumber}">
												        </h:inputText>
													</td>
												      <td style="width:20%">
														<h:outputLabel styleClass="LABEL"  value="#{msg['tenants.visa.number']}:"></h:outputLabel>
													</td>
													<td style="width:30%">
												        <h:inputText style="width:150px;"  styleClass="A_LEFT_SMALL" id="txtVisaNumber" value="#{pages$TenantsList.visaNumber}">
												        </h:inputText>
													</td>
													
													</tr>
													<tr>
													<td style="width:20%">
														<h:outputLabel  styleClass="LABEL"  value="#{msg['tenants.license.licenseno']}:"></h:outputLabel>
													</td>
													<td style="width:30%">
												        <h:inputText style="width:150px;"  styleClass="A_LEFT_SMALL"  id="txtLicenseNumber" value="#{pages$TenantsList.licenseNumber}">
												        
												        </h:inputText>
													</td>
													   												
													<td style="width:20%">
														<h:outputLabel  styleClass="LABEL" value="#{msg['tenants.personalsecuritycardno']}:"></h:outputLabel>
													</td>
													<td style="width:30%" >
												        <h:inputText style="width:150px;"  styleClass="A_LEFT_SMALL" id="txtSecurityCardNo" value="#{pages$TenantsList.personalSecurityCardNo}">
												        
												        </h:inputText>
													</td>
                      								
	
													
													</tr>
													
												
										
																									
											<tr>

												 
                                                <td colspan="6" class="BUTTON_TD" >
                                  <pims:security screen="Pims.TenantManagement.Tenant.TenantList" action="create">   
                                      <h:commandButton styleClass="BUTTON" type= "submit" 
									    value="#{msg['commons.Add']}" 
									    action="#{pages$TenantsList.btnAddTenant_Click}" 
									    >	
									 </h:commandButton>
								</pims:security>	 
                              	                       <h:commandButton styleClass="BUTTON" type= "submit" value="#{msg['commons.search']}"  action="#{pages$TenantsList.btnSearch_Click}" > </h:commandButton>
												       <h:commandButton styleClass="BUTTON" type="button" onclick="javascript:clearWindow();" value="#{msg['commons.reset']}" >  </h:commandButton>
												       <h:commandButton id="setUpCancel" styleClass="BUTTON" type= "submit" value="#{msg['commons.cancel']}" action="#{pages$TenantsList.btnBack_Click}" rendered="#{!pages$TenantsList.isModeSelectOnePopUp}" style="width: 74px">	</h:commandButton>
												       <h:commandButton id="popUpCancel" styleClass="BUTTON" type= "button" onclick="javascript:window.close();" value="#{msg['commons.cancel']}"  style="width: 74px" rendered="#{pages$TenantsList.isModeSelectOnePopUp}">	</h:commandButton>


                                		        </td>
                                          </tr>
									       
									       <h:inputHidden id="hdnMode"  value="#{pages$TenantsList.hdnMode}"  />
									       				
      									 </table>
      									 </div>
      									 </div>
                                 				    
                   			             <div style="padding-bottom:7px;padding-left:10px;padding-right:0;padding-top:7px;">		    
                   		                  <div class="imag">&nbsp;</div>
                   			               <div class="contentDiv" style="width: 97%"  >
										         <t:dataTable id="test2"
													value="#{pages$TenantsList.gridDataList}"
													binding="#{pages$TenantsList.dataTable}"
												    rows="#{pages$TenantsList.paginatorRows}"
												    preserveDataModel="true" preserveSort="false" var="dataItem"
												    rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												    width="100%">

                                                    <t:column id="number"  sortable="true">

														<f:facet name="header" >

															<t:outputText value="#{msg['commons.referencenumber']}" />

														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.tenantNumber}"
														style="white-space: normal;" />
													</t:column>

													<t:column id="nameEnLink"  sortable="true" rendered="#{!pages$TenantsList.isModeSelectOnePopUp}">
														<f:facet name="header" >
															<t:outputText value="#{msg['tenants.name']}" />
														</f:facet>
														
														<t:commandLink  styleClass="A_LEFT" action="#{pages$TenantsList.cmdTenantsName_Click}" value="#{dataItem.firstName} #{dataItem.middleName} #{dataItem.lastName} #{dataItem.companyName}"
														style="white-space: normal;" />
														
														<t:outputText />
													</t:column>
													<t:column id="nameEn"  sortable="true" rendered="#{pages$TenantsList.isModeSelectOnePopUp}">
														<f:facet name="header" >
															<t:outputText value="#{msg['tenants.name']}" />
														</f:facet>
														
														<t:outputText   styleClass="A_LEFT"  value="#{dataItem.firstName} #{dataItem.middleName} #{dataItem.lastName} #{dataItem.companyName}"
														style="white-space: normal;" />
														
														<t:outputText />
													</t:column>
									                 

												<t:column id="passportnumber"  >
													<f:facet name="header">


														<t:outputText value="#{msg['commons.passport.number']}" />

													</f:facet>
													<t:outputText styleClass="A_LEFT"  value="#{dataItem.passportNumber}" 
													style="white-space: normal;"/>
												</t:column>
	

												<t:column id="visanmuber" >


													<f:facet name="header">

														<t:outputText value= "#{msg['tenants.visa.number']}" />

													</f:facet>
													<t:outputText  styleClass="A_LEFT" value="#{dataItem.residenseVisaNumber}"
													style="white-space: normal;" />
												</t:column>


	


												<t:column id="licenseno" >

													<f:facet name="header">
														
														<t:outputText value="#{msg['tenants.license.licenseno']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value ="#{dataItem.licenseNumber}" 
													style="white-space: normal;"/>
											   </t:column>
												<t:column id="personalSecurityNo" >

													<f:facet name="header">
														
														<t:outputText value="#{msg['tenants.personalsecuritycardno']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value ="#{dataItem.personalSecurityCardNo}" 
													style="white-space: normal;"/>
											   </t:column>	
								
					                       <t:column id="selectbtn"  rendered="#{pages$TenantsList.isModeSelectOnePopUp}" >

	                                            <f:facet name="header">
	                                            <t:outputText value="#{msg['commons.select']}" />
	                                            </f:facet>
	                                            	 <t:commandLink  
	                                            	         actionListener="#{pages$TenantsList.sendTenantInfoToContractInRequest}">
																          	 <h:graphicImage  style="width: 16;height: 16;border: 0" 
					                                         			                alt="#{msg['commons.select']}"
																						url="../resources/images/select-icon.gif"
																				
																				
																		     ></h:graphicImage>
																	  </t:commandLink>
	                
                                                                        
                                           </t:column>					
                                        <pims:security screen="Pims.TenantManagement.Tenant.TenantList" action="create">
                                     		<t:column id="deletebtn"  rendered="#{pages$TenantsList.isModeSetup}" >
														<f:facet name="header" >
															<t:outputText value="#{msg['commons.delete']}"/>
														</f:facet>
														<t:commandLink  action="#{pages$TenantsList.cmdDelete_Click}">&nbsp;
														 <h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
														  </t:commandLink>
														<t:outputText />
										    </t:column>
										</pims:security>
									<pims:security screen="Pims.TenantManagement.Tenant.TenantList" action="create">		
										<t:column id="statusbtn" rendered="#{pages$TenantsList.isModeSetup}" >
											<f:facet name="header">
												<t:outputText value="#{msg['commons.status']}" />
											</f:facet>
											
										  <t:commandLink  rendered="#{dataItem.enable}"    action="#{pages$TenantsList.cmdStatus_Click}">&nbsp;
											 <h:graphicImage id="enableIcon" title="#{msg['commons.clickDisable']}" url="../resources/images/app_icons/enable.png" />&nbsp;
										  </t:commandLink>
										  
										  <t:commandLink  rendered="#{dataItem.disable}"  action="#{pages$TenantsList.cmdStatus_Click}">&nbsp;
											 <h:graphicImage id="disableIcon" title="#{msg['commons.clickEnable']}" url="../resources/images/app_icons/disable.png" />&nbsp;
										  </t:commandLink>
											<t:outputText />
										</t:column>	
                                    </pims:security>    
                                        </t:dataTable>										
									 </div>
											
											<div class="contentDivFooter" style="width: 98.7%">
											<div >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$TenantsList.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:50%" align="right">
											     <CENTER>
														<t:dataScroller id="scroller" for="test2" paginator="true"
															fastStep="1" paginatorMaxPages="#{pages$TenantsList.paginatorMaxPages}" immediate="false"
															paginatorTableClass="paginator"
															renderFacetsIfSinglePage="true" 
														pageIndexVar="pageNumber"
															paginatorTableStyle="grid_paginator" layout="singleTable"
															paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
															paginatorActiveColumnStyle="font-weight:bold;"
														paginatorRenderLinkForActive="false" 
														
														styleClass="SCH_SCROLLER">
		
													<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
																<t:div styleClass="PAGE_NUM_BG">
												        	 		<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td>	
																				<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																			</td>
																			<td>	
																				<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																			</td>
																		</tr>					
																	</table>
															
													</t:div>
														</t:dataScroller>
													</CENTER>
													</td>
												</tr></table>	
												</div>
                                           </div>
                                       </div>

                  			</h:form>
			             </div>
         			</td>
         			</tr>
			</table>
			</td>
			</tr>
			   <tr>
						<td colspan="2">
						<c:choose>
						        <c:when test="${!pages$TenantsList.isModeSelectOnePopUp}">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
										   
						
						
					     
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
						           </c:when>
		                 </c:choose>	
		
					</td>
				</tr>
			</table>
		
		</body>
</html>
</f:view>