<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Zakats
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	    function resetValues()
      	{
      		document.getElementById("searchFrm:txtPersonName").value="";
      		document.getElementById("searchFrm:txtRefNum").value="";
      		document.getElementById("searchFrm:txtCostCenter").value="";
      		document.getElementById("searchFrm:txtFileNum").value="";
		    document.getElementById("searchFrm:cmbstatus").selectedIndex=0;
		    document.getElementById("searchFrm:cmbcreatedBy").selectedIndex=0;
			$('searchFrm:createdOnFromId').component.resetSelectedDate();
			$('searchFrm:createdOnToId').component.resetSelectedDate();
			
			$('searchFrm:dueOnFromId').component.resetSelectedDate();
			$('searchFrm:dueOnToId').component.resetSelectedDate();
        }
        function performbDeductionRequestClick()
		{
			
			disableInputs();
			document.getElementById("searchFrm:lnkDeductionRequest").onclick();
			return; 
		
		}
		function disableInputs()
		{
		    var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) 
			{
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
		}	
	
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">

					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" height="470px" valign="top"
							class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['zakat.heading.searchZakat']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" height="100%">
										<div class="SCROLLABLE_SECTION AUC_SCH_SS"
											style="height: 95%; width: 100%; # height: 95%; # width: 100%;">
											<h:form id="searchFrm"
												style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
												<t:div id="layoutTable" styleClass="MESSAGE">
													<table border="0" class="layoutTable">
														<tr>
															<td>
																<h:messages></h:messages>
																<h:outputText id="successTxt" styleClass="INFO_FONT"
																	escape="false"
																	value="#{pages$zakatSearch.successMessages}" />
																<h:outputText id="errorMessage"
																	value="#{pages$zakatSearch.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
															</td>
														</tr>
													</table>
												</t:div>
												<div class="MARGIN">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="DETAIL_SECTION">
														<h:outputLabel value="#{msg['commons.searchCriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER">
															<tr>
																<td width="97%">
																	<table width="100%">
																		<tr>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.refNum']}"></h:outputLabel>
																			</td>

																			<td width="25%">
																				<h:inputText id="txtRefNum"
																					value="#{pages$zakatSearch.criteria.refNum}"></h:inputText>
																			</td>

																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.Name']}"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="txtPersonName"
																					value="#{pages$zakatSearch.criteria.personName}"></h:inputText>
																			</td>
																		</tr>
																		<tr>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.status']}"></h:outputLabel>
																			</td>
																			<td width="25%" colspan="1">
																				<h:selectOneMenu id="cmbstatus"
																					value="#{pages$zakatSearch.criteria.statusId}"
																					tabindex="3">

																					<f:selectItems
																						value="#{pages$ApplicationBean.zakatStatus}" />
																				</h:selectOneMenu>
																			</td>
																			<td width="25%" colspan="1">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['zakat.lbl.deductedBy']}:"></h:outputLabel>
																			</td>
																			<td width="25%" colspan="1">
																				<h:selectOneMenu id="cmbcreatedBy"
																					value="#{pages$zakatSearch.criteria.deductedBy}"
																					tabindex="3">
																					<f:selectItem itemLabel="#{msg['commons.All']}"
																						itemValue="-1" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.secUserList}" />
																				</h:selectOneMenu>
																			</td>
																		</tr>
																		<tr>
																			<td width="25%" colspan="1">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['inheritanceFileSearch.createdOnFrom']}:"></h:outputLabel>
																			</td>
																			<td width="25%" colspan="1">
																				<rich:calendar id="createdOnFromId"
																					locale="#{pages$zakatSearch.locale}"
																					value="#{pages$zakatSearch.criteria.createdOnFrom}"
																					popup="true"
																					datePattern="#{pages$zakatSearch.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:185px;height:17px" />

																			</td>
																			<td width="25%" colspan="1">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.to']}:"></h:outputLabel>
																			</td>
																			<td width="25%" colspan="1">
																				<rich:calendar id="createdOnToId"
																					locale="#{pages$zakatSearch.locale}"
																					value="#{pages$zakatSearch.criteria.createdOnTo}"
																					popup="true"
																					datePattern="#{pages$zakatSearch.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:185px;height:17px" />
																			</td>
																		</tr>
																		<tr>
																			<td width="25%" colspan="1">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['zakat.lbl.dueOnFrom']}:"></h:outputLabel>
																			</td>
																			<td width="25%" colspan="1">
																				<rich:calendar id="dueOnFromId"
																					locale="#{pages$zakatSearch.locale}"
																					value="#{pages$zakatSearch.criteria.dueOnFrom}"
																					popup="true"
																					datePattern="#{pages$zakatSearch.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:185px;height:17px" />
																			</td>
																			<td width="25%" colspan="1">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.to']}:"></h:outputLabel>
																			</td>
																			<td width="25%" colspan="1">
																				<rich:calendar id="dueOnToId"
																					locale="#{pages$zakatSearch.locale}"
																					value="#{pages$zakatSearch.criteria.dueOnTo}"
																					popup="true"
																					datePattern="#{pages$zakatSearch.dateFormat}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width:185px;height:17px" />
																			</td>
																		</tr>
																		<tr>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['endowmentFile.lbl.num']}"></h:outputLabel>
																			</td>

																			<td width="25%">
																				<h:inputText id="txtFileNum"
																					value="#{pages$zakatSearch.criteria.fileNumber}"></h:inputText>
																			</td>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['settlement.label.costCenter']}"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="txtCostCenter"
																					value="#{pages$zakatSearch.criteria.costCenter}"></h:inputText>

																			</td>
																		</tr>
																		<tr>

																			<td class="BUTTON_TD" colspan="4">
																				<table cellpadding="1px" cellspacing="1px">
																					<tr>
																						<td colspan="2">
																							<h:commandButton styleClass="BUTTON"
																								value="#{msg['commons.search']}"
																								action="#{pages$zakatSearch.onSearch}"
																								style="width: 75px" />

																							<h:commandButton styleClass="BUTTON"
																								type="button" value="#{msg['commons.clear']}"
																								onclick="javascript:resetValues();"
																								style="width: 75px" />

																							<pims:security
																								screen="Pims.MinorMgmt.ZakatDeduction.Save"
																								action="view">

																								<h:commandButton styleClass="BUTTON"
																									id="btnDeductionRequest"
																									value="#{msg['zakat.lbl.zakatDeductionRequest']}"
																									onclick="javaScript:performbDeductionRequestClick(this);"
																									style="width: auto;" />
																								<h:commandLink id="lnkDeductionRequest"
																									action="#{pages$zakatSearch.onCreateZakatDeductionRequest}" />

																							</pims:security>

																							<h:commandButton styleClass="BUTTON"
																								value="Calculate"
																								action="#{pages$zakatSearch.onCalculateZakat}"
																								style="width: 75px" />
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>
															</tr>

														</table>
													</div>
												</div>
												<div
													style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
													<div class="imag">
														&nbsp;
													</div>
													<div class="contentDiv" style="width: 100%; # width: 99%;">
														<t:dataTable id="dt1"
															value="#{pages$zakatSearch.dataList}"
															binding="#{pages$zakatSearch.dataTable}"
															rows="#{pages$zakatSearch.paginatorRows}"
															preserveDataModel="false" preserveSort="false"
															var="dataItem" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true" width="100%">

															<t:column id="refNum" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="refNum"
																		actionListener="#{pages$zakatSearch.sort}"
																		value="#{msg['commons.refNum']}" arrow="true">
																		<f:attribute name="sortField" value="refNum" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.refNum}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>


															<t:column id="passportName" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="passportName"
																		actionListener="#{pages$zakatSearch.sort}"
																		value="#{msg['commons.Name']}" arrow="true">
																		<f:attribute name="sortField" value="passportName" />
																	</t:commandSortHeader>
																</f:facet>
																<t:commandLink value="#{dataItem.person.passportName}"
																	style="white-space: normal;" styleClass="A_LEFT"
																	action="#{pages$zakatSearch.onNameClicked}">
																</t:commandLink>
															</t:column>

															<t:column id="status" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="status"
																		actionListener="#{pages$zakatSearch.sort}"
																		value="#{msg['commons.status']}" arrow="true">
																		<f:attribute name="sortField" value="status" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText
																	value="#{pages$zakatSearch.englishLocale? 
																	         dataItem.status.dataDescEn:
																	         dataItem.status.dataDescAr
																	        }"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>

															<t:column id="createdOn" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="createdOn"
																		actionListener="#{pages$zakatSearch.sort}"
																		value="#{msg['commons.createdOn']}" arrow="true">
																		<f:attribute name="sortField" value="createdOn" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.createdOn}"
																	style="white-space: normal;" styleClass="A_LEFT">
																	<f:convertDateTime
																		timeZone="#{pages$zakatSearch.timeZone}"
																		pattern="#{pages$zakatSearch.dateFormat}" />
																</t:outputText>
															</t:column>
															<t:column id="calculateFrom" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="calculateFrom"
																		actionListener="#{pages$zakatSearch.sort}"
																		value="#{msg['zakat.lbl.calculateFrom']}" arrow="true">
																		<f:attribute name="sortField" value="calculateFrom" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.calculateFrom}"
																	style="white-space: normal;" styleClass="A_LEFT">
																	<f:convertDateTime
																		timeZone="#{pages$zakatSearch.timeZone}"
																		pattern="#{pages$zakatSearch.dateFormat}" />
																</t:outputText>
															</t:column>

															<t:column id="amount" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="amount"
																		actionListener="#{pages$zakatSearch.sort}"
																		value="#{msg['commons.amount']}" arrow="true">
																		<f:attribute name="sortField" value="amount" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.amount}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>

															<t:column id="amountToDeduct" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="amount"
																		actionListener="#{pages$zakatSearch.sort}"
																		value="#{msg['zakat.lbl.amountToDeduct']}" arrow="true">
																		<f:attribute name="sortField" value="amount" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.amountToDeduct}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>


															<t:column id="balance" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="balance"
																		actionListener="#{pages$zakatSearch.sort}"
																		value="#{msg['mems.normaldisb.label.balance']}"
																		arrow="true">
																		<f:attribute name="sortField" value="balance" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.balance}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>

															<t:column id="dueOn" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="dueOn"
																		actionListener="#{pages$zakatSearch.sort}"
																		value="#{msg['zakat.lbl.dueOn']}" arrow="true">
																		<f:attribute name="sortField" value="dueOn" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.dueOn}"
																	style="white-space: normal;" styleClass="A_LEFT">
																	<f:convertDateTime
																		timeZone="#{pages$zakatSearch.timeZone}"
																		pattern="#{pages$zakatSearch.dateFormat}" />
																</t:outputText>
															</t:column>

															<t:column id="deductedBy" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="deductedBy"
																		actionListener="#{pages$zakatSearch.sort}"
																		value="#{msg['zakat.lbl.deductedBy']}" arrow="true">
																		<f:attribute name="sortField" value="deductedBy" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.deductedBy}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>

															<t:column id="deductedOn" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="deductedOn"
																		actionListener="#{pages$zakatSearch.sort}"
																		value="#{msg['zakat.lbl.deductedOn']}" arrow="true">
																		<f:attribute name="sortField" value="deductedOn" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.deductedOn}"
																	style="white-space: normal;" styleClass="A_LEFT">
																	<f:convertDateTime
																		timeZone="#{pages$zakatSearch.timeZone}"
																		pattern="#{pages$zakatSearch.dateFormat}" />
																</t:outputText>
															</t:column>
															<t:column id="actionCol" sortable="false" width="100"
																style="TEXT-ALIGN: center;">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.action']}" />
																</f:facet>


																<h:graphicImage id="zakatTrx"
																	onclick="javaScript:openZakatPersonalAccountTransLinkPopup('#{dataItem.zakatId}')"
																	title="#{msg['zakat.heading.zakatTrx']}"
																	url="../resources/images/app_icons/replaceCheque.png" />

																<h:graphicImage id="zakatHistory"
																	onclick="javaScript:openZakatHistoryPopup('#{dataItem.zakatId}')"
																	style="margin-right:5px;"
																	title="#{msg['zakat.heading.zakatHistory']}"
																	url="../resources/images/app_icons/manage_tender.png" />

															</t:column>



														</t:dataTable>
													</div>
													<t:div id="contentDivFooter"
														styleClass="contentDivFooter AUCTION_SCH_RF"
														style="width:100%;#width:100%;">
														<table id="RECORD_NUM_TD" cellpadding="0" cellspacing="0"
															width="100%">
															<tr>
																<td class="RECORD_NUM_TD">
																	<div class="RECORD_NUM_BG">
																		<table cellpadding="0" cellspacing="0"
																			style="width: 182px; # width: 150px;">
																			<tr>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText value=" : " />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{pages$zakatSearch.recordSize}" />
																				</td>
																			</tr>
																		</table>
																	</div>
																</td>
																<td id="contentDivFooterColumnTwo" class="BUTTON_TD"
																	style="width: 53%; # width: 50%;" align="right">

																	<t:div styleClass="PAGE_NUM_BG" style="#width:20%">

																		<table cellpadding="0" cellspacing="0">
																			<tr>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																				</td>
																				<td>
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{pages$zakatSearch.currentPage}" />
																				</td>
																			</tr>
																		</table>
																	</t:div>
																	<TABLE border="0" class="SCH_SCROLLER">
																		<tr>
																			<td>
																				<t:commandLink
																					action="#{pages$zakatSearch.pageFirst}"
																					disabled="#{pages$zakatSearch.firstRow == 0}">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$zakatSearch.pagePrevious}"
																					disabled="#{pages$zakatSearch.firstRow == 0}">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:dataList value="#{pages$zakatSearch.pages}"
																					var="page">
																					<h:commandLink value="#{page}"
																						actionListener="#{pages$zakatSearch.page}"
																						rendered="#{page != pages$zakatSearch.currentPage}" />
																					<h:outputText value="<b>#{page}</b>" escape="false"
																						rendered="#{page == pages$zakatSearch.currentPage}" />
																				</t:dataList>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$zakatSearch.pageNext}"
																					disabled="#{pages$zakatSearch.firstRow + pages$zakatSearch.rowsPerPage >= pages$zakatSearch.totalRows}">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																				</t:commandLink>
																			</td>
																			<td>
																				<t:commandLink
																					action="#{pages$zakatSearch.pageLast}"
																					disabled="#{pages$zakatSearch.firstRow + pages$zakatSearch.rowsPerPage >= pages$zakatSearch.totalRows}">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblL"></t:graphicImage>
																				</t:commandLink>
																			</td>
																		</tr>
																	</TABLE>
																</td>
															</tr>
														</table>
													</t:div>
												</div>

											</h:form>

										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>