<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<!-- Replace all labels with message resource labels -->
<script type="text/javascript">
</script>
<t:div rendered="true" style="width:100%">
	<t:panelGrid width="100%" columns="4">
		
		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel value="#{msg['commons.replaceCheque.unitType']} :">
			</h:outputLabel>
		</t:panelGroup>
	<h:selectOneMenu id="unitTypeItemsId"
		binding="#{pages$unitTypeAmountEditTab.unitType}">
		<f:selectItem itemValue="-1"
			itemLabel="#{msg['commons.combo.PleaseSelect']}" />
		<f:selectItems
			value="#{pages$unitTypeAmountEditTab.unitTypeItems}" />
	</h:selectOneMenu>
	
		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel value="#{msg['property.evaluation.NewRentValueForUnitType']}">
		</h:outputLabel>
		</t:panelGroup>
		<h:inputText value="#{pages$unitTypeAmountEditTab.newAmountType}">
		</h:inputText>
		
		<h:outputLabel value="#{msg['receiveProperty.fieldName.areaFrom']}">
		</h:outputLabel>
		<h:inputText value="#{pages$unitTypeAmountEditTab.areaFrom}">
		</h:inputText>
		
		<h:outputLabel value="#{msg['receiveProperty.fieldName.areaTo']}">
		</h:outputLabel>
		<h:inputText value="#{pages$unitTypeAmountEditTab.areaTo}">
		</h:inputText>
		
	</t:panelGrid>
	<t:div styleClass="BUTTON_TD" >	
		<h:commandButton 
			action="#{pages$unitTypeAmountEditTab.addUnitTypeToGrid}"
			value="#{msg['attachment.add']}" styleClass="BUTTON" style="width:60px;" >
		</h:commandButton>
	</t:div>
</t:div>

<t:div style="text-align:left;">
	<t:div styleClass="contentDiv" style="width:94.5%">
	<t:dataTable id="unitTypeAmountGrid" 
			rows="#{pages$unitTypeAmountEditTab.paginatorRows}"
			preserveDataModel="true" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
			value="#{pages$unitTypeAmountEditTab.unitTypeDescList}"
			binding="#{pages$unitTypeAmountEditTab.unitTypeTable}"
			width="100%">
			<t:column  sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.replaceCheque.unitType']}" />
				</f:facet>
				<h:outputText 
				style="white-space: normal;" styleClass="A_LEFT"
				value="#{pages$unitTypeAmountEditTab.englishLocale?dataItem.descEn:dataItem.descAr}" />
			</t:column>
			
			<t:column  sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['property.evaluation.NewRentValueForUnitType']}" />
				</f:facet>
				<h:outputText 
				style="white-space: normal;"
				value="#{dataItem.typeAmountDouble}" styleClass="A_RIGHT_NUM">
					<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
				</h:outputText>
			</t:column>
			
			<t:column  sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['receiveProperty.fieldName.areaFrom']}" />
				</f:facet>
				<h:outputText 
				style="white-space: normal;"	styleClass="A_RIGHT_NUM"
				value="#{ dataItem.areaFrom}" />
			</t:column>
			
			<t:column  sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['receiveProperty.fieldName.areaTo']}" />
				</f:facet>
				<h:outputText 
				style="white-space: normal;" styleClass="A_RIGHT_NUM"
				value="#{ dataItem.areaTo}" />
			</t:column>
			
			<t:column  sortable="true">
				<f:facet name="header">
					<h:outputText value="Action" />
				</f:facet>
				<h:commandLink action="#{pages$unitTypeAmountEditTab.deleteFromGrid}">
					<h:graphicImage style="margin-left:6px;"
						title="#{msg['commons.edit']}"
						url="../resources/images/delete_icon.png" />
				</h:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>

</t:div>
<t:div 
	styleClass="contentDivFooter AUCTION_SCH_RF" style="width:95.5%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']} :"/>
						<h:outputText value="#{pages$unitTypeAmountEditTab.recordSize}"/>
						<h:outputText  />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="unitTypeScroller" for="unitTypeAmountGrid" paginator="true"  
						fastStep="1" 
						paginatorTableClass="paginator"
						paginatorMaxPages="#{pages$unitTypeAmountEditTab.paginatorMaxPages}" immediate="false"
						renderFacetsIfSinglePage="true" 												
						paginatorTableStyle="grid_paginator" layout="singleTable" 
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" 
						pageIndexVar="pageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}" id="lblF11"></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR11"></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF11"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}" id="lblL11"></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
							<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
	</t:div>
<t:div style="width:100%;MARGIN: 0px 4px 0px 0px;">
	<t:panelGrid width="100%" border="0" columns="3"
		columnClasses="BUTTON_TD">
		<t:panelGroup colspan="12">
		<h:commandButton  
		rendered="#{pages$unitTypeAmountEditTab.showApplyButton}"
		action="#{pages$unitTypeAmountEditTab.saveAmountWithTypeAndArea}"
		value="#{msg['receiveProperty.buttonLabel.applyValues']}" styleClass="BUTTON" style="width:auto;" >
		</h:commandButton>
		<h:commandButton 
		action="#{pages$unitTypeAmountEditTab.clearGrid}"
		value="#{msg['receiveProperty.buttonLabel.clearGrid']}" styleClass="BUTTON" style="width:auto;" >
		</h:commandButton>
		</t:panelGroup>
	</t:panelGrid>
</t:div>