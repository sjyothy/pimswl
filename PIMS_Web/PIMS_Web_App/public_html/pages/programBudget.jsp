
<script language="JavaScript" type="text/javascript">	
</script>

<t:panelGrid 

	id="budgetGrdFields" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4">
	
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*"/>
		<h:outputLabel styleClass="LABEL" value="#{msg['socialProgram.Budget.lbl.Amount']}" />
	</h:panelGroup>
	
	<h:inputText 
	value="#{pages$programBudget.programBudgetView.amount}"  />
	
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*"/>
		<h:outputLabel styleClass="LABEL" value="#{msg['socialProgram.Budget.lbl.Desc']}" />
	</h:panelGroup>
	<h:inputTextarea  
	value="#{pages$programBudget.programBudgetView.description}" style="width: 185px;" />
	
		
		
</t:panelGrid>
<t:panelGrid 
id="budgetGrdActions" styleClass="BUTTON_TD" cellpadding="1px" width="100%" cellspacing="5px">
	<h:panelGroup >
		<h:commandButton styleClass="BUTTON"
			value="#{msg['commons.saveButton']}"
			action="#{pages$programBudget.onSave}" />
	</h:panelGroup>
</t:panelGrid>


<t:div styleClass="contentDiv" style="width:98%"  id="budgetContentDiv">																
	<t:dataTable id="budgetsDataTable"  
				rows="100"
				value="#{pages$programBudget.list}"
				binding="#{pages$programBudget.dataTable}"																	
				preserveDataModel="false" preserveSort="false" var="dataItem"
				rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">


		
		<t:column id="budgetdate" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.Budget.lbl.Date']}" style="white-space: normal;" />
			</f:facet>
			
			<t:outputText style="white-space: normal;"  value="#{dataItem.createdOn}" rendered="#{dataItem.isDeleted != '1' }" >
			<f:convertDateTime pattern="#{pages$programBudget.dateFormat}" timeZone="#{pages$programBudget.timeZone}" />
			</t:outputText>
		</t:column>
		<t:column id="budgetCreatedBy" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.Budget.lbl.CreatedBy']}" style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"  value="#{dataItem.createdEn}" rendered="#{dataItem.isDeleted != '1' }"/>																		
		</t:column>																																
		<t:column id="budgetdesc" width="55%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.Budget.lbl.Desc']}" style="white-space: normal;"  />
			</f:facet>
			<t:outputText value="#{dataItem.description}"  style="white-space: normal;" rendered="#{dataItem.isDeleted != '1' }"/>																		
		</t:column>
		<t:column id="budgetAmount" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.Budget.lbl.Amount']}" style="white-space: normal;"  />
			</f:facet>
			<t:outputText value="#{dataItem.amount}"  style="white-space: normal;" rendered="#{dataItem.isDeleted != '1' }" />																		
		</t:column>

		<t:column id="budgetActionColumn" width="15%">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.action']}" />
			</f:facet>
			<h:graphicImage id="imgDeleteBudget"
				rendered="#{dataItem.isDeleted != '1' }"
				title="#{msg['commons.delete']}"
				url="../resources/images/delete_icon.png"
				onclick="if (!confirm('#{msg['commons.confirmDelete']}')) return false; else onDeleted(this);">
			</h:graphicImage>
			<a4j:commandLink id="onBudgetDeleted"
				reRender="hiddenFields,budgetContentDiv,budgetsDataTable,successmsg,errormsg"
				onbeforedomupdate="javaScript:onRemoveDisablingDiv(); "
				action="#{pages$programBudget.onDelete}" />
		</t:column>
	</t:dataTable>										
</t:div>
