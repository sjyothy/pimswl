

<t:div style="width:95%;padding-top:5px;padding-botton:5px;">

	<t:div styleClass="BUTTON_TD">
		<h:commandButton styleClass="BUTTON" type="button"
			rendered="#{ pages$openBox.showSaveButton }"
			value="#{msg['commons.add']}"
			onclick="javascript:SearchMemberPopup();">
		</h:commandButton>

	</t:div>

	<t:div styleClass="contentDiv" style="width:95%;">
		<t:dataTable id="programBoxesGrid"
			binding="#{pages$openBox.dataTableCommittee}"
			value="#{pages$openBox.listCommittee}" preserveDataModel="true"
			preserveSort="false" var="dataItem" rowClasses="row1,row2"
			rules="all" renderedIfEmpty="true" width="100%">


			<t:column>
				<f:facet name="header">
					<h:outputText style="white-space: normal;"
						value="#{msg['member.fullName']}" />
				</f:facet>
				<h:outputText style="white-space: normal;" id="secondaryFullName"
					value="#{dataItem.member.secondaryFullName}" />
			</t:column>
	
			<t:column id="actionGrid" width="5%" style="white-space: normal;"
				sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.action']}" />
				</f:facet>
				<t:commandLink rendered="#{pages$openBox.showSaveButton}"
					onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;"
					action="#{pages$openBox.onDeleteCommitteeMember}">
					<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
						url="../resources/images/delete_icon.png" />
				</t:commandLink>

			</t:column>
		</t:dataTable>
	</t:div>
</t:div>