<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Editing an Auction Unit Prices
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		function closeWindowSubmit()
		{
			window.opener.document.forms[0].submit();
		  	window.close();	  
		}	
</script>
<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
		 <title>PIMS</title>
		 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
		 <meta http-equiv="pragma" content="no-cache">
		 <meta http-equiv="cache-control" content="no-cache">
		 <meta http-equiv="expires" content="0">
		 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 <script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
	</head>

	<body class="BODY_STYLE">
			<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
			%>
	
				<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="100%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
											<h:outputLabel value="#{msg['auctionUnit.editPriceHeading']}" styleClass="HEADER_FONT"/>
									</td>
								</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" 
							cellspacing="0" border="0">
								<tr valign="top">
								<td width="100%" height="100%" valign="top" >
								<div style="display:block;height:700px;width:100%">
										<h:form id="detailsFrm" style="width:88%">
										<div style="height:15px; padding-left:9px;">
										<table border="0" class="layoutTable">
											<tr>
												<td>
													<h:outputText value="#{pages$auctionUnitPriceEdit.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
												</td>
											</tr>
										</table>
										</div>
										<t:panelGrid rendered="false">
										<t:div style="width:100%;padding-left:10px;">
										<t:div styleClass="contentDiv" style="width:98.2%; height:200px;">
											<t:dataTable id="dt1"
												value="#{pages$auctionUnitPriceEdit.dataList}"
												binding="#{pages$auctionUnitPriceEdit.dataTable}"
												rows="5"
												preserveDataModel="false" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
												<t:column id="propertyNumberCol" width="100" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['property.propertyNumberCol']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.propertyNumber}" />
												</t:column>
												<t:column id="propertyCommercialNameCol" width="100" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['property.commercialName']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.propertyCommercialName}" style="white-space: normal;"/>
												</t:column>
												<t:column id="propertyEndowedNameCol" width="120" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['property.endowedName']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.propertyEndowedName}" style="white-space: normal;"/>
												</t:column>
												<t:column id="landNumberCol" width="80" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['property.landNumber']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.landNumber}" style="white-space: normal;"/>
												</t:column>
												<t:column id="emirateEnCol" width="100" sortable="true" rendered="#{pages$auctionDetails2.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.Emirate']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText value="#{dataItem.emirateEn}" styleClass="A_LEFT" style="white-space: normal;"/>
												</t:column>
												<t:column id="emirateArCol" width="100" sortable="true" rendered="#{pages$auctionDetails2.isArabicLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.Emirate']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText value="#{dataItem.emirateAr}" styleClass="A_LEFT" style="white-space: normal;"/>
												</t:column>
												<t:column id="unitNumberCol" width="60" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['unit.numberCol']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.unitNumber}" style="white-space: normal;"/>
												</t:column>
												<t:column id="unitAreaCol" width="80" sortable="true" rendered="#{pages$auctionDetails2.advertisementMode}">
													<f:facet name="header">
														<t:outputText value="#{msg['unit.unitArea']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText value="#{dataItem.area}" styleClass="A_RIGHT_NUM" style="white-space: normal;"/>
												</t:column>
												<t:column id="unitTypeEnCol" width="60" sortable="true" rendered="#{pages$auctionDetails2.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['unit.type']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.unitTypeEn}" style="white-space: normal;"/>
												</t:column>
												<t:column id="unitTypeArCol" width="60" sortable="true" rendered="#{pages$auctionDetails2.isArabicLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['unit.type']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.unitTypeAr}" style="white-space: normal;"/>
												</t:column>
												<t:column id="unitUsageEnCol" width="80" sortable="true" rendered="#{pages$auctionDetails2.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['inquiry.unitUsage']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.unitUsageTypeEn}" style="white-space: normal;"/>
												</t:column>
												<t:column id="unitUsageTypeArCol" width="80" sortable="true" rendered="#{pages$auctionDetails2.isArabicLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['inquiry.unitUsage']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.unitUsageTypeAr}" style="white-space: normal;"/>
												</t:column>
												<t:column id="rentValueCol" width="70" sortable="true">
													<f:facet name="header"  >
														<t:outputText value="#{msg['unit.rentValueCol']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText value="#{dataItem.rentValue}"  styleClass="A_RIGHT_NUM" style="white-space: normal;">
														<f:convertNumber pattern="#{pages$auctionDetails2.numberFormat}"/>
													</t:outputText>
												</t:column>
												<t:column id="openingPriceCol" width="80" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['auctionUnit.openingPrice']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText value="#{dataItem.openingPrice}" styleClass="A_RIGHT_NUM" style="white-space: normal;">
														<f:convertNumber pattern="#{pages$auctionDetails2.numberFormat}"/>
													</t:outputText>
												</t:column>
												<t:column id="depositAmountCol" width="70" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['auctionUnit.depositColumn']}" style="white-space: normal;"/>
													<%-- 	<t:outputText value="#{msg['unit.depositAmount']}" />  --%>
													</f:facet>
													<t:outputText value="#{dataItem.depositAmount}" styleClass="A_RIGHT_NUM" style="white-space: normal;">
														<f:convertNumber pattern="#{pages$auctionDetails2.numberFormat}"/>
													</t:outputText>
												</t:column>
											</t:dataTable>
										</t:div>
										</t:div>
										</t:panelGrid>
										<div class="MARGIN" style="padding-top:10px;"> 
									<table cellpadding="0" cellspacing="0" style="width:115%;#width:115%;">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
									
									<div style="width:280px;">
										<table cellpadding="1px" cellspacing="2px" style="width:280px;">
											<tr>
												<td style="PADDING: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="LABEL" binding="#{pages$auctionUnitPriceEdit.openingPriceLabel}" value="#{msg['auctionUnit.openingPrice']}"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; PADDING-TOP: 5px;">
													<h:inputText id="openingPrice" maxlength="20" binding="#{pages$auctionUnitPriceEdit.openingPriceField}" value="#{pages$auctionUnitPriceEdit.openingPrice}" tabindex="1" styleClass="A_RIGHT_NUM">
													</h:inputText>
												</td>
											</tr>
											<tr>
												<td style="PADDING: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="LABEL" binding="#{pages$auctionUnitPriceEdit.depositAmountLabel}" value="#{msg['unit.depositAmount']}"></h:outputLabel>
												</td>
												<td style="PADDING: 5px; PADDING-TOP: 5px;">
													<h:inputText id="depositAmount" maxlength="20" binding="#{pages$auctionUnitPriceEdit.depositAmountField}" value="#{pages$auctionUnitPriceEdit.depositAmount}" tabindex="2" styleClass="A_RIGHT_NUM">
													</h:inputText>
												</td>
												
											</tr>
											<tr>
												<td colspan="2">
													<h:outputLabel id="maxAmountTxt" binding="#{pages$auctionUnitPriceEdit.maxDepositLabel}" value="#{msg['auctionUnit.maxDepositAmount']} : #{pages$auctionUnitPriceEdit.maxAmount}" tabindex="1" styleClass="INFO_FONT" style="PADDING-LEFT: 5px; font-size:11px; width:100%;"></h:outputLabel>
												</td>
											</tr>
											<tr>
												<td class="BUTTON_TD AUC_OP_JUG_BUTTON_TD" colspan="2">
													<table>
													<tr>
														<td>
															<h:commandButton styleClass="BUTTON"  value="#{msg['commons.cancel']}"
																actionListener="#{pages$auctionUnitPriceEdit.cancel}"
																tabindex="15"/>
														</td>
														<td>
														<pims:security screen="Pims.AuctionManagement.PrepareAuction.AuctionUnitAction" action="create">
															<h:commandButton styleClass="BUTTON" value="#{msg['commons.setValue']}"
																action="#{pages$auctionUnitPriceEdit.saveAuctionUnitPrices}"
																tabindex="16"/>
														</pims:security>
														</td>
													<tr>
													</table>
												</td>
											</tr>
											</table>
											</div>
											</div>
										</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
        		</td>
		    </tr>
		</table>
	</body>
</html>
</f:view>
