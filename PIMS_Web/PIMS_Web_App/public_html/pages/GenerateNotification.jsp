<%-- 
  - Author: Ahmed Faraz
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Notification Sending and searching
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

 		function showDetailPopup()
        {
              var screen_width = screen.width;
              var screen_height = screen.height;
              
              var popup_width = screen_width-590;
              var popup_height = screen_height-450;
              var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
              
              var popup = window.open('NotificationDetails.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,resizable=no,titlebar=no,dialog=yes');
              popup.focus();
        }
        
	    function resetValues()
      	{
      		document.getElementById("searchFrm:auctionNumber").value="";
			document.getElementById("searchFrm:auctionTitle").value="";
      	    $('searchFrm:auctionDate').component.resetSelectedDate();
      	    $('searchFrm:auctionCreationDate').component.resetSelectedDate();
      	    document.getElementById("searchFrm:auctionStatuses").selectedIndex=0;
      	    document.getElementById("searchFrm:auctionStartTimeMM").selectedIndex=0;
      	    document.getElementById("searchFrm:auctionStartTimeHH").selectedIndex=0;
      	    document.getElementById("searchFrm:auctionEndTimeMM").selectedIndex=0;
      	    document.getElementById("searchFrm:auctionEndTimeHH").selectedIndex=0;
			document.getElementById("searchFrm:auctionStartTime").value="";
			document.getElementById("searchFrm:auctionEndTime").value="";
			document.getElementById("searchFrm:venueName").value="";
        }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<div  class="containerDiv">
			
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>
				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['notificationGenerate.heading']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
									<div class="SCROLLABLE_SECTION">

										<h:form id="searchFrm" style="WIDTH: 97.6%;">
											<div style="height: 25px">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$GenerateNotification.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
													<tr>
													<td width = "97%">
													<table width = "100%">
														
														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notificationGenerate.expdatefrom']}:"></h:outputLabel>
															</td>
															<td width="25%">
															<rich:calendar id="sendDateCal"
																	value="#{pages$GenerateNotification.srchExpiryDateFrom}"
																	locale="#{pages$GenerateNotification.locale}" popup="true"
																	datePattern="#{pages$GenerateNotification.dateFormat}"
																	showApplyButton="false" enableManualInput="false"
																	inputStyle="width:170px; height:14px" />
															</td>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notificationGenerate.expdateto']}:"></h:outputLabel>
															</td>
															<td width="25%">
															<rich:calendar id="createDateCal"
																	value="#{pages$GenerateNotification.srchExpiryDateTo}"
																	locale="#{pages$GenerateNotification.locale}" popup="true"
																	datePattern="#{pages$GenerateNotification.dateFormat}"
																	showApplyButton="false" enableManualInput="false"
																	inputStyle="width:170px; height:14px" />
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notificationGenerate.contractNumber']}:"></h:outputLabel>
															</td>
															<td >
																<h:inputText id="requestNumberTxt"
																	value="#{pages$GenerateNotification.srchContractNumber}"
																	 maxlength="100" />

															</td>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notificationGenerate.name']}:" />
															</td>
															<td >
																<h:inputText id="personNameTxt"
																	value="#{pages$GenerateNotification.srchPersonName}"
																	 maxlength="100"></h:inputText>

															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notificationGenerate.email']}:"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="contractNumberTxt"
																	value="#{pages$GenerateNotification.srchEmail}"
																	 maxlength="50"></h:inputText>
															</td>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notificationGenerate.cellNumber']}:" />
															</td>
															<td>
																<h:inputText id="procedureTypeTxt"
																	value="#{pages$GenerateNotification.srchCellNumber}"
																	 maxlength="20"></h:inputText>
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notificationGenerate.requestNumber']}:"></h:outputLabel>
															</td>
															<td >
															<h:inputText id="requestNumber"
																	value="#{pages$GenerateNotification.srchRequestNumber}"
																	 maxlength="100"></h:inputText>
																
															</td>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notificationGenerate.requestType']}:"></h:outputLabel>
															</td>
															<td >
																<h:inputText id="requestTypeId"
																	value="#{pages$GenerateNotification.srchProcedureType}"
																	 maxlength="100"></h:inputText>
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notificationGenerate.personType']}:"></h:outputLabel>
															</td>
															<td >
																<h:selectOneMenu id="notificationTypeCmb"
																	value="#{pages$GenerateNotification.srchPersonType}"
																	required="false">
																	<f:selectItem itemValue="30008"
																		itemLabel="TENANT" />
																	<f:selectItem itemValue="30010"
																		itemLabel="BIDDER" />
																</h:selectOneMenu>
															</td>
															<td >&nbsp;
															</td>
														</tr>
														<tr>

															<td class="BUTTON_TD" colspan="4">
																			<pims:security
																				screen="Pims.Home.NotificationSetup"
																				action="create">
																				<h:commandButton id="searchButtonId" styleClass="BUTTON"
																					value="#{msg['commons.search']}"
																					action="#{pages$GenerateNotification.searchToGenerate}"
																					style="width: 75px" tabindex="7">
																				</h:commandButton>
																			</pims:security>
																			<h:commandButton type="reset" styleClass="BUTTON"
																				value="#{msg['commons.clear']}" style="width: 75px"
																				tabindex="7"></h:commandButton>
																			<pims:security
																				screen="Pims.Home.NotificationSetup"
																				action="create">
																				<h:commandButton id="generateButtonId" styleClass="BUTTON"
																					value="#{msg['notificationGenerate.generate']}"
																					action="#{pages$GenerateNotification.openDetailsPopupForGenerate}"
																					style="width: 75px" tabindex="7">
																				</h:commandButton>
																			</pims:security>
															</td>
														</tr>
													</table>
														</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>
														
													</table>
												</div>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 2px; padding-top: 7px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv">
													<t:dataTable id="dt1"
														value="#{pages$GenerateNotification.dataList}"
														binding="#{pages$GenerateNotification.dataTable}"
														rows="#{pages$GenerateNotification.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%">
														<t:column id="selectCol" width="45">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.select']}" />
															</f:facet>

															<h:selectBooleanCheckbox id="selectchk"
																value="#{dataItem.selected}" />
														</t:column>
														
														<t:column id="subjectCol" width="105" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['notificationGenerate.name']}" />
															</f:facet>
															<t:outputText value="#{dataItem.personName}"
																styleClass="A_LEFT" />
														</t:column>
														<t:column id="typeCol" width="55" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['notificationGenerate.contractNumber']}" />
															</f:facet>
															<t:outputText value="#{dataItem.contractNo}" />
														</t:column>
														<t:column id="sendDateCol" width="85" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['notificationGenerate.requestNumber']}" />
															</f:facet>
															<t:outputText value="#{dataItem.requestNo}"
																styleClass="A_LEFT" />
														</t:column>
														<t:column id="personnameCol" width="110" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['notificationGenerate.email']}" />
															</f:facet>
															<t:outputText value="#{dataItem.emailAddress}" />
														</t:column>
														<t:column id="notidicationDateCol" width="100"
															sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['notificationGenerate.cellNumber']}" />
															</f:facet>
															<t:outputText value="#{dataItem.mobileNumber}" />
														</t:column>
														<t:column id="actionCol" sortable="false" width="40"
															style="TEXT-ALIGN: center;">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>
															
															<t:outputLabel id="aa333asa" value="  "></t:outputLabel>


														</t:column>
													</t:dataTable>
												</div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:99%;">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr style="padding-right: 2px; ">
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$GenerateNotification.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">
																<CENTER>
																	<t:dataScroller id="scroller" for="dt1"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$GenerateNotification.paginatorMaxPages}"
																		immediate="false" paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false"
																		pageIndexVar="pageNumber" styleClass="SCH_SCROLLER">
																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL"></t:graphicImage>
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
																			<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.pageNumber}" />
																					</td>
																				</tr>
																			</table>

																		</t:div>
																	</t:dataScroller>
																</CENTER>

															</td>
														</tr>
													</table>
												</t:div>
										</h:form>
									</div>







								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
	</html>
</f:view>