<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>



<t:div styleClass="contentDiv" style="width:95%;">
	<t:dataTable id="dataListNextRevFollowupGrid" width="100%"
		styleClass="grid"
		value="#{pages$ManageAssets.dataListNextRevFollowup}" rows="10000"
		preserveDataModel="true" preserveSort="false" var="dataItem"
		rowClasses="row1,row2">

		<t:column id="refNum" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['revenueFollowup.lbl.RefNum']}" />
			</f:facet>
			<t:outputText value="#{dataItem.refNum}" style="white-space: normal;"
				styleClass="A_LEFT" />
		</t:column>


		<t:column id="statusId" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.status']}" />
			</f:facet>
			<t:outputText
				value="#{pages$searchRevenueFollowups.englishLocale? 
															         dataItem.status.dataDescEn:
															         dataItem.status.dataDescAr
						}"
				style="white-space: normal;" styleClass="A_LEFT" />
		</t:column>
		<t:column id="followUpOn" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['revenueFollowup.lbl.followupOn']}" />
			</f:facet>
			<t:outputText value="#{dataItem.followUpOn}"
				style="white-space: normal;" styleClass="A_LEFT">
				<f:convertDateTime
					timeZone="#{pages$searchRevenueFollowups.timeZone}"
					pattern="#{pages$searchRevenueFollowups.dateFormat}" />
			</t:outputText>
		</t:column>
		<t:column id="createdBy" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.createdBy']}" />
			</f:facet>
			<t:outputText value="#{dataItem.createdByName}"
				style="white-space: normal;" styleClass="A_LEFT" />
		</t:column>
		<t:column id="expectedRevnue" sortable="true">
			<f:facet name="header">
				<t:outputText
					value="#{msg['mems.inheritanceFile.fieldLabel.expRev']}" />
			</f:facet>
			<t:outputText value="#{dataItem.expRevenue}"
				style="white-space: normal;" styleClass="A_LEFT" />
		</t:column>



	</t:dataTable>
</t:div>

