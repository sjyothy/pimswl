
<script type="text/javascript">

						
</script>
<t:div id="contractDetailsTab" styleClass="TAB_DETAIL_SECTION"
	style="width:100%">
	<t:panelGrid id="contractDetailsTable" cellpadding="1px" width="100%"
		cellspacing="5px" styleClass="TAB_DETAIL_SECTION_INNER" columns="4"
		columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">

		<h:outputLabel styleClass="LABEL"
			value="#{msg['commons.referencenumber']}:" />
		<h:inputText maxlength="20" styleClass="READONLY"
			id="txtContractRefNum" readonly="true"
			binding="#{pages$BOTContractDetailsTab.txtContractNo}"
			value="#{pages$BOTContractDetailsTab.contractNo}" ></h:inputText>

		<h:outputLabel id="lblContractType" styleClass="LABEL"
			value="#{msg['contract.contractType']}:"></h:outputLabel>
		<h:inputText maxlength="20" styleClass="READONLY" id="txtContractType"
			readonly="true"
			binding="#{pages$BOTContractDetailsTab.txtContractType}"
			value="#{pages$BOTContractDetailsTab.contractType}"
			></h:inputText>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['serviceContract.tender.number']}:"></h:outputLabel>
		<t:panelGrid id="gridTender" columns="2" width="100%" cellpadding="0"
			cellspacing="1"
			columnClasses="LEASE_CONTRACT_BASIC_INFO_SPONSORMANGER_GRID_C1,LEASE_CONTRACT_BASIC_INFO_SPONSORMANGER_GRID_C2">
			<h:inputText id="txtTenderNumber" styleClass="READONLY"
				 readonly="true"
				binding="#{pages$BOTContractDetailsTab.txtTenderNum}"
				title="#{pages$BOTContractDetailsTab.txtTenderNum}"
				value="#{pages$BOTContractDetailsTab.tenderNum}"></h:inputText>
			<h:graphicImage url="../resources/images/magnifier.gif"
				onclick="javaScript:showSearchTenderPopUp('#{pages$BOTContractDetailsTab.tenderSearchKey_tenderType}','#{pages$BOTContractDetailsTab.tenderSearchKey_WinnerContractorPresent}','#{pages$BOTContractDetailsTab.tenderSearchKey_NotBindedWithContract}');"
				binding="#{pages$BOTContractDetailsTab.imgSearchTender}"
				style="MARGIN: 0px 0px -4px;" alt="#{msg[commons.searchTender]}"></h:graphicImage>
			<h:graphicImage url="../resources/images/delete_icon.png"
				onclick="javaScript:document.getElementById('formRequest:cmdDeleteTender').onclick();"
				binding="#{pages$BOTContractDetailsTab.imgRemoveTender}"
				style="MARGIN: 0px 0px -4px;" alt="#{msg[commons.delete]}"></h:graphicImage>

		</t:panelGrid>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['botContractDetails.ProposalOpenDate']}:"></h:outputLabel>
		<h:inputText id="txtproposalOpenDate" styleClass="READONLY"
			readonly="true" 
			value="#{pages$BOTContractDetailsTab.proposalOpenDate}"
			title="#{pages$BOTContractDetailsTab.proposalOpenDate}"></h:inputText>
		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['botContractDetails.InvestorNumber']}:"></h:outputLabel>
		</t:panelGroup>

		<t:panelGrid id="gridContractor" columns="2" width="100%"
			cellpadding="0" cellspacing="1"
			columnClasses="LEASE_CONTRACT_BASIC_INFO_SPONSORMANGER_GRID_C1,LEASE_CONTRACT_BASIC_INFO_SPONSORMANGER_GRID_C2">
			<h:inputText id="txtinvestorNumber" 
				styleClass="READONLY" readonly="true"
				binding="#{pages$BOTContractDetailsTab.txtContractorNum}"
				title="#{pages$BOTContractDetailsTab.contractorNum}"
				value="#{pages$BOTContractDetailsTab.contractorNum}"></h:inputText>
			<h:graphicImage
				binding="#{pages$BOTContractDetailsTab.imgInvestorSearch}"
				url="../resources/images/app_icons/Search-tenant.png"
				onclick="javaScript:showPopup();" style="MARGIN: 0px 0px -4px;"
				alt="#{msg[contract.searchTenant]}"></h:graphicImage>
			<h:graphicImage id="imgViewInvestor"
				binding="#{pages$BOTContractDetailsTab.imgViewInvestor}"
				onclick="javaScript:showPersonReadOnlyPopup();"
				title="#{msg['commons.view']}"
				url="../resources/images/app_icons/Tenant.png"
				style="MARGIN: 0px 0px -4px; CURSOR: hand;" />
		</t:panelGrid>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['botContractDetails.InvestorName']}:"></h:outputLabel>
		<h:inputText id="txtinvestorName" readonly="true"
			styleClass="READONLY" 
			binding="#{pages$BOTContractDetailsTab.txtContractorName}"
			value="#{pages$BOTContractDetailsTab.contractorName}"
			title="#{pages$BOTContractDetailsTab.contractorName}"></h:inputText>

		<h:outputLabel rendered="false" styleClass="LABEL"
			value="#{msg['botContractDetails.EmirateCity']}:"></h:outputLabel>
		<h:selectOneMenu rendered="false" id="selectEmirateCity"
			styleClass="READONLY" disabled="true" style="width:95%;"
			value="#{pages$BOTContractDetailsTab.crStateId}"
			binding="#{pages$BOTContractDetailsTab.cmbEmirateCity}">

		</h:selectOneMenu>

		<h:outputLabel rendered="false" styleClass="LABEL"
			value="#{msg['botContractDetails.Area']}:"></h:outputLabel>
		<h:selectOneMenu rendered="false" id="selectArea"
			styleClass="READONLY" disabled="true" style="width:95%;"
			value="#{pages$BOTContractDetailsTab.crCityId}"
			binding="#{pages$BOTContractDetailsTab.cmbArea}">
			<f:selectItem itemValue="-1"
				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
		</h:selectOneMenu>

		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['contract.contractDate']}:"></h:outputLabel>
		</t:panelGroup>
		<rich:calendar id="contractDate"
			value="#{pages$BOTContractDetailsTab.contractDate}" popup="true"
			datePattern="#{pages$BOTContractDetailsTab.dateFormat}"
			binding="#{pages$BOTContractDetailsTab.contractDateCalendar}"
			timeZone="#{pages$BOTContractDetailsTab.timeZone}"
			showApplyButton="false" enableManualInput="false" cellWidth="24px"
			 locale="#{pages$BOTContractDetailsTab.locale}">
			<a4j:support event="onchanged"
				action="#{pages$BOTContractDetailsTab.calculateContractStartDate}"
				reRender="contractStartDate,datetimeContractEndDate" />
		</rich:calendar>
		<h:outputLabel
			value="#{msg['botContractDetails.ContractHejjriDate']}:"></h:outputLabel>
		<h:inputText id="txtContractHejjriDate" 
			styleClass="A_RIGHT_NUM" maxlength="12"></h:inputText>

		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['botContractDetails.ContractPeriod']}"></h:outputLabel>
		</t:panelGroup>
		<t:panelGrid columns="2" style="width:100%;"
			columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
			<h:inputText id="txtContractPeriod" style="width:95%;"
				styleClass="A_RIGHT_NUM" maxlength="4"
				value="#{pages$BOTContractDetailsTab.txtContractPeriod}">
				<a4j:support event="onchange"
					action="#{pages$BOTContractDetailsTab.calculateContractExpiryDate}"
					reRender="datetimeContractEndDate" />
			</h:inputText>
			<h:selectOneMenu id="selectContractPeriod" style="width:40%;"
				value="#{pages$BOTContractDetailsTab.selectOneContractPeriod}"
				binding="#{pages$BOTContractDetailsTab.cmbContractPeriod}">

				<f:selectItems value="#{pages$ApplicationBean.gracePeriodTypeList}" />
				<a4j:support event="onchange"
					action="#{pages$BOTContractDetailsTab.calculateContractExpiryDate}"
					reRender="datetimeContractEndDate" />
			</h:selectOneMenu>
		</t:panelGrid>
		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['botContractDetails.GracePeriod']}:"></h:outputLabel>
		</t:panelGroup>
		<t:panelGrid columns="2" style="width:100%;"
			columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
			<h:inputText id="txtGracePeriod" style="width:95%;"
				styleClass="A_RIGHT_NUM" maxlength="4"
				value="#{pages$BOTContractDetailsTab.txtGracePeriod}">
				<a4j:support event="onchange"
					action="#{pages$BOTContractDetailsTab.calculateContractStartDate}"
					reRender="contractStartDate,datetimeContractEndDate" />
			</h:inputText>
			<h:selectOneMenu id="selectGracePeriod" style="width:40%;"
				value="#{pages$BOTContractDetailsTab.selectOneGracePeriod}"
				binding="#{pages$BOTContractDetailsTab.cmbGracePeriod}">

				<f:selectItems value="#{pages$ApplicationBean.gracePeriodTypeList}" />
				<a4j:support event="onchange"
					action="#{pages$BOTContractDetailsTab.calculateContractStartDate}"
					reRender="contractStartDate,datetimeContractEndDate" />
			</h:selectOneMenu>
		</t:panelGrid>



		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['contract.date.Start']}:"></h:outputLabel>
		</t:panelGroup>
		<rich:calendar id="contractStartDate"
			value="#{pages$BOTContractDetailsTab.contractStartDate}" popup="true"
			datePattern="#{pages$BOTContractDetailsTab.dateFormat}"
			binding="#{pages$BOTContractDetailsTab.startDateCalendar}"
			timeZone="#{pages$BOTContractDetailsTab.timeZone}"
			showApplyButton="false" enableManualInput="false" cellWidth="24px"
			 locale="#{pages$BOTContractDetailsTab.locale}">
			<a4j:support event="onchanged"
				action="#{pages$BOTContractDetailsTab.calculateContractExpiryDate}"
				reRender="datetimeContractEndDate" />
		</rich:calendar>
		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel value="#{msg['contract.date.expiry']}:"></h:outputLabel>
		</t:panelGroup>
		<rich:calendar id="datetimeContractEndDate"
			value="#{pages$BOTContractDetailsTab.contractEndDate}"
			binding="#{pages$BOTContractDetailsTab.endDateCalendar}"
			disabled="true" popup="true"
			datePattern="#{pages$BOTContractDetailsTab.dateFormat}"
			timeZone="#{pages$BOTContractDetailsTab.timeZone}"
			showApplyButton="false" enableManualInput="false" cellWidth="24px"
			>
		</rich:calendar>
		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['botContractDetails.RentAmount']}:"></h:outputLabel>
		</t:panelGroup>

		<h:inputText id="totalContract" 
			styleClass="A_RIGHT_NUM" maxlength="15"
			binding="#{pages$BOTContractDetailsTab.txtTotalAmount}"
			value="#{pages$BOTContractDetailsTab.totalAmount}"></h:inputText>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['botContractDetails.RentType']}:"></h:outputLabel>
		<h:selectOneMenu id="selectRentType" style="width:95%;"
			value="#{pages$BOTContractDetailsTab.selectOneRentType}"
			binding="#{pages$BOTContractDetailsTab.cmbRentType}">

			<f:selectItems value="#{pages$ApplicationBean.paymentPeriodList}" />
		</h:selectOneMenu>

	</t:panelGrid>
</t:div>
