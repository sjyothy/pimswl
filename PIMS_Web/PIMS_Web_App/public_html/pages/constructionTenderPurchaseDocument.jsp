<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript" src="../resources/jscripts/commons.js"></script>
<script type="text/javascript">
		
		
		function showContractorPopup()
		{
			var screen_width = screen.width;
			var screen_height = screen.height;
			var popup_width = screen_width-180;
			var popup_height = screen_height-265;
			var leftPos = (screen_width-popup_width)/2 -5, topPos = (screen_height-popup_height)/2 - 20;
			var popup = window.open('contractorSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
			popup.focus();
		}
		
		function populateContractor(contractorId, contractorNumber, contractorNameEn, contractorNameAr, licenseNumber, licenseSource, officeNumber, contractorTypeId)
		{
			
			document.getElementById("frm:hdnVendorId").value = contractorId;
			document.getElementById("frm:hdnVendorName").value = contractorNameEn;
			document.getElementById("frm:txtVendorName").value = contractorNameEn;
			document.getElementById("frm:hdnVendorTypeId").value = contractorTypeId; 
			document.getElementById("frm:ddnVendorType").value = contractorTypeId;			
		}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 <script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>	
	</head>
	
	<body class="BODY_STYLE">
	<div class="containerDiv">
	<%	
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header --> 
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="2">
				<jsp:include page="header.jsp" />
			</td>
		</tr>
		<tr width="100%">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['purchaseTenderDocument.heading']}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>
				<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%">
					<tr valign="top">
							<td width="100%" height="100%" valign="top" nowrap="nowrap">						
							<div class="SCROLLABLE_SECTION" style="height:475px;width:100%;#height:466px;">
									<h:form id="frm" enctype="multipart/form-data" style="WIDTH: 96%;">
									<div>
										<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
											<tr>
												<td>
													<h:outputText value="#{pages$constructionTenderPurchaseDocument.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$constructionTenderPurchaseDocument.infoMessages}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
									</div>
																
						
								
						<div class="AUC_DET_PAD">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
						</table>
						<div class="TAB_PANEL_INNER">
							<h:inputHidden id="hdnVendorId" value="#{pages$constructionTenderPurchaseDocument.contractorView.personId}" /> 
							<h:inputHidden id="hdnVendorName" value="#{pages$constructionTenderPurchaseDocument.contractorView.contractorNameEn}"  />
							<h:inputHidden id="hdnVendorTypeId" value="#{pages$constructionTenderPurchaseDocument.contractorView.contractorTypeId}" />
							
							<rich:tabPanel binding="#{pages$constructionTenderPurchaseDocument.tabPanel}" id="tabPanel" switchType="server" style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%">															
							    <rich:tab binding="#{pages$constructionTenderPurchaseDocument.purchaseRequestTab}" id="purchaseRequestTab" label="#{msg['purchaseTenderDocument.purchaseRequestTab']}">
							    	<h:panelGrid width="100%" columns="4">
							    		
							    		
								    	<h:outputLabel styleClass="LABEL" value="#{msg['purchaseTenderDocument.vendorName']}:" />
								    	<h:panelGroup>
								    		<h:inputText readonly="true" id="txtVendorName"	style="width:150px"				 
														 binding="#{pages$constructionTenderPurchaseDocument.txtVendorName}"
														 value="#{pages$constructionTenderPurchaseDocument.contractorView.contractorNameEn}">
											</h:inputText>											
											<h:graphicImage onclick="showContractorPopup();" id="imgContractor"  style="MARGIN: 0px 0px -4px" url="../resources/images/magnifier.gif" title="#{msg['commons.search']}"></h:graphicImage>											
										</h:panelGroup>
										
										
										<h:outputLabel styleClass="LABEL" value="#{msg['purchaseTenderDocument.vendorType']}:" />
										<h:selectOneMenu readonly="true" id="ddnVendorType" 
														 binding="#{pages$constructionTenderPurchaseDocument.ddnVendorType}"
														 value="#{pages$constructionTenderPurchaseDocument.contractorView.contractorTypeId}">
											<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
											<f:selectItems value="#{pages$ApplicationBean.contractorTypeList}" />
										</h:selectOneMenu>
										
										
										<h:outputLabel styleClass="LABEL" value="#{msg['purchaseTenderDocument.tenderNumber']}:" />
										<h:inputText readonly="true" id="txtTenderNumber" style="width:150px"					 
													 binding="#{pages$constructionTenderPurchaseDocument.txtTenderNumber}"
													 value="#{pages$constructionTenderPurchaseDocument.tenderDetailView.tenderNumber}">
										</h:inputText>
										
										
										<h:outputLabel styleClass="LABEL" value="#{msg['purchaseTenderDocument.tenderDescription']}:" />
										<h:inputText readonly="true" id="txtTenderDescription"	style="width:150px"				 
													 binding="#{pages$constructionTenderPurchaseDocument.txtTenderDescription}"
													 value="#{pages$constructionTenderPurchaseDocument.tenderDetailView.tenderRemarks}">
										</h:inputText>
										
										
										<h:outputLabel styleClass="LABEL" value="#{msg['purchaseTenderDocument.paymentDate']}:" />
										<rich:calendar id="calPaymentDate"	
													   binding="#{pages$constructionTenderPurchaseDocument.calPaymentDate}"
													   value="#{pages$constructionTenderPurchaseDocument.tenderDetailView.tenderPresentingDate}"
													   datePattern="#{pages$constructionTenderPurchaseDocument.dateFormat}"
													   locale="#{pages$constructionTenderPurchaseDocument.locale}" 
													   popup="true"
													   disabled="true"
													   showApplyButton="false" 
													   enableManualInput="false">
										</rich:calendar>
										
										
										<h:outputLabel styleClass="LABEL" value="#{msg['purchaseTenderDocument.tenderPrice']}:" />
										<h:inputText readonly="true" id="txtTenderPrice" style="width:150px"					 
													 binding="#{pages$constructionTenderPurchaseDocument.txtTenderPrice}"
													 value="#{pages$constructionTenderPurchaseDocument.tenderDetailView.tenderPrice}">
										</h:inputText>
										
										<h:outputLabel styleClass="LABEL" value="#{msg['notification.requestnumber']}:" />
										<h:inputText readonly="true" id="txtRequestNumber" style="width:150px"					 
													 value="#{pages$constructionTenderPurchaseDocument.requestView.requestNumber}">
										</h:inputText>
										
										<h:outputLabel styleClass="LABEL" value="#{msg['request.requestStatus']}:" />
										<h:inputText readonly="true" id="txtRequestStatusEn" style="width:150px"
										             rendered="#{pages$constructionTenderPurchaseDocument.isEnglishLocale}"					 
													 value="#{pages$constructionTenderPurchaseDocument.requestView.statusEn}">
										</h:inputText>			 
										<h:inputText readonly="true" id="txtRequestStatusAr" style="width:150px"
										             rendered="#{!pages$constructionTenderPurchaseDocument.isEnglishLocale}"					 
													 value="#{pages$constructionTenderPurchaseDocument.requestView.statusAr}">			 
										</h:inputText>
										
							    	</h:panelGrid>									
								</rich:tab>
								 <rich:tab id="PaymentTab" label="#{msg['cancelContract.tab.payments']}" >
					                                      
                                        <t:div id="divtt" styleClass="contentDiv" style="width:97%">
			                               <t:dataTable id="ttbb" width="100%" value="#{pages$constructionTenderPurchaseDocument.paymentScheduleViewList}" 
			                                            binding="#{pages$constructionTenderPurchaseDocument.dataTablePaymentSchedule}"
													    preserveDataModel="false" preserveSort="false" 
													    var="paymentScheduleDataItem" rowClasses="row1,row2" renderedIfEmpty="true">
											    <t:column  >
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentNumber']}"/>
													</f:facet>
													<t:outputText  styleClass="A_LEFT" value="#{paymentScheduleDataItem.paymentNumber}"/>
												</t:column>
												
												<t:column>
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.ReceiptNumber']}"/>
													</f:facet>
													<t:outputText  styleClass="A_LEFT" value="#{paymentScheduleDataItem.paymentReceiptNumber}"/>
												</t:column>
												
												<t:column  rendered="#{pages$constructionTenderPurchaseDocument.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentType']}"/>
													</f:facet>
													<t:outputText  styleClass="A_LEFT" title="" value="#{paymentScheduleDataItem.typeEn}"/>
												</t:column>
												<t:column  rendered="#{pages$constructionTenderPurchaseDocument.isArabicLocale}">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentType']}"/>
													</f:facet>
													<t:outputText  styleClass="A_LEFT" title="" value="#{paymentScheduleDataItem.typeAr}"/>
												</t:column>
												<t:column >
													<f:facet name="header">
														<t:outputText  value="#{msg['renewContract.description']}"/>
													</f:facet>
													<t:outputText  styleClass="A_LEFT" title="" value="#{paymentScheduleDataItem.description}"/>
												</t:column>
												<t:column >
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentDueOn']}"/>
													</f:facet>
													<t:outputText  styleClass="A_LEFT" value="#{paymentScheduleDataItem.paymentDueOn}">
													<f:convertDateTime pattern="#{pages$constructionTenderPurchaseDocument.dateFormatForDataTable}" />
													</t:outputText>
												</t:column>
                                                <t:column  rendered="#{pages$constructionTenderPurchaseDocument.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText  value="#{msg['commons.status']}"/>
													</f:facet>
													<t:outputText  styleClass="A_LEFT" title="" value="#{paymentScheduleDataItem.statusEn}"/>
												</t:column>
												<t:column  rendered="#{pages$constructionTenderPurchaseDocument.isArabicLocale}">
													<f:facet name="header">
														<t:outputText  value="#{msg['commons.status']}"/>
													</f:facet>
													<t:outputText  styleClass="A_LEFT" title="" value="#{paymentScheduleDataItem.statusAr}"/>
												</t:column>
												<t:column  rendered="#{pages$constructionTenderPurchaseDocument.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentMode']}"/>
													</f:facet>
													<t:outputText  styleClass="A_LEFT" title="" value="#{paymentScheduleDataItem.paymentModeEn}"/>
												</t:column>
												<t:column  rendered="#{pages$constructionTenderPurchaseDocument.isArabicLocale}">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentMode']}"/>
													</f:facet>
													<t:outputText  styleClass="A_LEFT" title="" value="#{paymentScheduleDataItem.paymentModeAr}"/>
												</t:column>
												<t:column >
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.amount']}"/>
													</f:facet>
													<t:outputText  styleClass="A_RIGHT_NUM" value="#{paymentScheduleDataItem.amount}"/>
												</t:column>
												<t:column id="col8"  >
													<f:facet name="header">
														<t:outputText  value="#{msg['commons.action']}" />
													</f:facet>
													
													<h:commandLink action="#{pages$constructionTenderPurchaseDocument.btnEditPaymentSchedule_Click}" >
													    <h:graphicImage id="editPayments" 
																		title="#{msg['commons.edit']}" 
																		url="../resources/images/edit-icon.gif"
																		rendered="#{paymentScheduleDataItem.isReceived == 'N'}"
																		/>
													</h:commandLink>
													<h:commandLink action="#{pages$constructionTenderPurchaseDocument.btnPrintPaymentSchedule_Click}">
								                    <h:graphicImage id="printPayments" 
													                title="#{msg['commons.print']}" 
													                url="../resources/images/app_icons/print.gif"
													                rendered="#{paymentScheduleDataItem.isReceived == 'Y'}"/>
												    </h:commandLink>
												    
												    <h:commandLink action="#{pages$constructionTenderPurchaseDocument.btnViewPaymentDetails_Click}">
														<h:graphicImage id="viewPayments" 
																		title="#{msg['commons.group.permissions.view']}" 
																		url="../resources/images/detail-icon.gif"
														/>
													</h:commandLink> 
												</t:column>
								              </t:dataTable>
											</t:div>
										</rich:tab>	  
							            <rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}">
									        <%@  include file="attachment/attachment.jsp"%>
								       </rich:tab>
								       <rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}">
									        <%@ include file="notes/notes.jsp"%>
								       </rich:tab>								
							</rich:tabPanel>
							</div>
							<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT" /></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID" style="width:100%;" /></td>
							<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT" /></td>
							</tr>
							</table>
						
						<t:div styleClass="BUTTON_TD" style="padding-top:10px;">
							<t:commandButton styleClass="BUTTON" value="#{msg['commons.collectAppFee']}" action="#{pages$constructionTenderPurchaseDocument.onCollect}" binding="#{pages$constructionTenderPurchaseDocument.btnCollect}" />
							<t:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" action="#{pages$constructionTenderPurchaseDocument.onSave}" binding="#{pages$constructionTenderPurchaseDocument.btnSave}" />
							<t:commandButton styleClass="BUTTON" value="#{msg['commons.AddNew']}" action="#{pages$constructionTenderPurchaseDocument.onAddNew}" rendered="false" binding="#{pages$constructionTenderPurchaseDocument.btnAddNew}" />						
							<t:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}" action="#{pages$constructionTenderPurchaseDocument.onCancel}" binding="#{pages$constructionTenderPurchaseDocument.btnCancel}" />
							
							
						</t:div>
						</div>
								</div>
							</h:form>
					  </div> 
						</td>			
					</tr>
				</table>
			</td>
		</tr>
		<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
	</table>
	</div>
</body>
</html>
</f:view>