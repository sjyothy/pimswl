
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
	<script language="javascript" type="text/javascript">
	
		</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />


<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" >
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
				<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table_ff}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
	 </head>
	
			


<body class="BODY_STYLE">
      <div class="containerDiv">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

		<tr width="100%">
    <td class="divLeftMenuWithBody" width="17%">
        <jsp:include page="leftmenu.jsp"/>
    </td>
    <td width="83%" height="505px" valign="top" class="divBackgroundBody">
 <table width="99.2%" class="greyPanelTable" cellpadding="0"
					cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
								<h:outputLabel value="#{msg['inspectionApplication.Details']}" styleClass="HEADER_FONT"/>
						</td>
						<td width="100%">
							&nbsp;
						</td>
					</tr>
				</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
								<!--Use this below only for desiging forms Please do not touch other sections -->


								<td width="100%" height="100%" valign="top" nowrap="nowrap">
								   
									<h:form id="inspectionForm" enctype="multipart/form-data">
									<div class="SCROLLABLE_SECTION" style="height:440px;">	 
									<div >
										<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
											<tr>
												<td>
													<h:outputText id="errorMsg_1" value="#{pages$inspectionApplicationDetails.errorMessages}"  escape="false" styleClass="ERROR_FONT"/>
												</td>
											</tr>
										</table>
									</div>
									
												<table cellpadding="0" cellspacing="0" width="100%">	
																							
										<table class="TAB_PANEL_MARGIN" width="98%" border="0">
										
									
											<tr>
												<td colspan="2">
												<div >
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
													<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>

						<rich:tabPanel id="inspectionTabPanel" style="HEIGHT: 330px;width: 100%">
																
							<rich:tab id="applicationTab" label="#{msg['commons.tab.applicationDetails']}">
								<%@ include file="ApplicationDetails.jsp"%>
					        </rich:tab>
									
									
									
						    <rich:tab id="inspectionInfoTab" title="#{msg['inpectionInfo.tab']}"
								label="#{msg['inpectionInfo.tab']}">

								<t:div id="divInspectionTab" styleClass="TAB_DETAIL_SECTION">
										<h:outputLabel 
											value="#{msg['inpectionAplication.Reason']} :">
										</h:outputLabel>
										<t:inputTextarea rows="3"
											readonly="#{pages$inspectionApplicationDetails.isViewMode}"
										 	cols="30"
										 	value="#{pages$inspectionApplicationDetails.inspectionReason}">
										</t:inputTextarea>
								</t:div>
								
								<t:div>
										
								</t:div>
								
								<t:div id="one5" styleClass="contentDiv"
									style="width:98.1%">
									<t:dataTable id="dt1"
										value="#{pages$inspectionApplicationDetails.inspectionUnitsDataList}"
										binding="#{pages$inspectionApplicationDetails.tbl_InspectionUnits}"
										rows="5" preserveDataModel="true"
										preserveSort="false" var="dataItem"
										rowClasses="row1,row2" rules="all"
										renderedIfEmpty="true" width="100%">

										<t:column id="unitNo" sortable="true">
											<f:facet name="header">
												<t:outputText id="dtone1"
													value="#{msg['commons.replaceCheque.unitRefNo']}" />
											</f:facet>
											<t:outputText id="dtone16" styleClass="A_LEFT"
												value="#{dataItem.unitView.unitNumber}" />
										</t:column>
										<t:column id="propertyName" sortable="true">
											<f:facet name="header">
												<t:outputText id="dtone2"
													value="#{msg['contract.property.Name']}" />
											</f:facet>
											<t:outputText id="dtone15" styleClass="A_LEFT"
												value="#{dataItem.unitView.propertyCommercialName}" />
										</t:column>
										<t:column id="UnitTypeEn" sortable="true">
											<f:facet name="header">
												<t:outputText id="dtone6"
													value="#{msg['unit.type']}" />
											</f:facet>
											<t:outputText id="dtone10" styleClass="A_LEFT"
												value="#{pages$inspectionApplicationDetails.isEnglishLocale?dataItem.unitView.unitTypeEn:dataItem.unitView.unitTypeAr}" />
										</t:column>
										<t:column id="UnitStatusEn" sortable="true">
											<f:facet name="header">
												<t:outputText id="dtone25"
													value="#{msg['unit.unitStatus']}" />
											</f:facet>
											<t:outputText id="dtone26" styleClass="A_LEFT"
												value="#{pages$inspectionApplicationDetails.isEnglishLocale?dataItem.unitView.statusEn:dataItem.unitView.statusAr}" />
										</t:column>
										<t:column id="contractNumber" sortable="true">
											<f:facet name="header">
												<t:outputText id="dtone4"
													value="#{msg['inspection.contractRefNum']}" />
											</f:facet>
											<t:outputText id="dtone13" styleClass="A_LEFT"
												value="#{dataItem.contractNumber}" />
										</t:column>
										<t:column id="tenantName" sortable="true">
											<f:facet name="header">
												<t:outputText id="dtone3"
													value="#{msg['contract.tenantName']}" />
											</f:facet>
											<t:outputText id="dtone14" styleClass="A_LEFT"
												value="#{dataItem.tenantName}" />
										</t:column>
										<t:column 
											rendered="#{!(pages$inspectionApplicationDetails.isViewMode)}"
											id="deleteImage" >
											<f:facet name="header">
												<t:outputText id="opDelete"
													value="" />
											</f:facet>
											<h:commandLink
													action="#{pages$inspectionApplicationDetails.removeUnit_Click}">
													<h:graphicImage id="imageew" title="#{msg['commons.delete']}"
														url="../resources/images/app_icons/delete_icon.png" />
											</h:commandLink>
										</t:column>

									</t:dataTable>
								</t:div>
								<t:dataScroller id="scroller1" for="dt1"
									paginator="true" fastStep="1"
									paginatorMaxPages="#{pages$inspectionApplicationDetails.paginatorMaxPages}"
									immediate="false" paginatorTableClass="paginator"
									renderFacetsIfSinglePage="true"
									pageIndexVar="pageNumber"
									paginatorTableStyle="grid_paginator"
									layout="singleTable"
									paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
									paginatorActiveColumnStyle="font-weight:bold;"
									paginatorRenderLinkForActive="false"
									styleClass="SCH_SCROLLER">

									<f:facet name="first">
										<t:graphicImage url="../#{path.scroller_first}"
											id="lblFk"></t:graphicImage>
									</f:facet>
									<f:facet name="fastrewind">
										<t:graphicImage url="../#{path.scroller_fastRewind}"
											id="lblFRk"></t:graphicImage>
									</f:facet>
									<f:facet name="fastforward">
										<t:graphicImage url="../#{path.scroller_fastForward}"
											id="lblFFk"></t:graphicImage>
									</f:facet>
									<f:facet name="last">
										<t:graphicImage url="../#{path.scroller_last}"
											id="lblLk"></t:graphicImage>
									</f:facet>
								</t:dataScroller>
							</rich:tab>
									
									
											
									
												                 
												           

    						<rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}">
				              <%@  include file="attachment/attachment.jsp"%>
	                        </rich:tab>
	                     
	                        <rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}">
				              <%@ include file="notes/notes.jsp"%>
	                        </rich:tab>


						</rich:tabPanel>
														

	<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT" /></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID" style="width:100%;" /></td>
													<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT" /></td>
													</tr>
												</table>
												</div>
														</td>
													</tr>
													<tr>
														<td class="BUTTON_TD MARGIN" colspan="3">

															<h:commandButton type="submit" styleClass="BUTTON"
																value="#{msg['commons.back']}"
																action="#{pages$inspectionApplicationDetails.back}" />	
														
												    	</td>
													</tr>


												</table>
												</div>
										</h:form>
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</body>
     </div>
	</html>
</f:view>
