<%-- 
  - Author: Jawwad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: this popup wil save the remarks of user while block/unblock the contract.
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%-- JAVA SCRIPT FUNCTIONS START --%>
<script type="text/javascript">

function closeAndSendRemarksToParent()
{
	element = document.getElementById('searchFrm:comment');
	window.opener.receiveUnblockComment(element.value);
	window.close();
}

</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}" >
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
		</head>
		
		<body class="BODY_STYLE">
		
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				
				<tr width="100%">
					
					<td width="100%" valign="top" class="divBackgroundBody">
						<table  class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['blackList.remarks']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99.1%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%"  valign="top" nowrap="nowrap">
									<h:form id="searchFrm" >
									<div class="SCROLLABLE_SECTION" >
										
										<t:div styleClass="MESSAGE" style="width:98%;">
												<table border="0" class="layoutTable" width="90%"
													style="margin-left: 15px; margin-right: 15px;">
													<tr>
														<td>
															<h:outputText id="errorMsg" escape="false"
																styleClass="ERROR_FONT"
																value="#{pages$ContractListBackingBean.errorMessages}" />
														</td>
														<td>
															<h:outputText id="successMsg" escape="false"
																styleClass="INFO_FONT"
																value="#{pages$ContractListBackingBean.successMessages}" />
														</td>
													
													</tr>
												</table>
											</t:div>
													
														<div class="MARGIN" style="width:97%;">
														<table cellpadding="0" cellspacing="0" style="width:97%;">
																<tr>
																<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
																<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
																<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
																</tr>
														</table>
														<div class="DETAIL_SECTION" style="width:97%;" >
														     <h:outputLabel value="" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														 <table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" width="100%" style="height:150px">
														<tr>
															<td style="vertical-align:top;">
																<h:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel value="#{msg['blackList.remarks']}">
																	</h:outputLabel>
																</h:panelGroup>
															</td>
															<td>
																<h:inputTextarea id="comment"
																	style="height:120px;width:200px">
																</h:inputTextarea>
															</td>
														</tr>
													</table>
															</div>
																<TABLE width="97%">
																	<tr>
																		<td class="BUTTON_TD" colspan="6">	
																				<h:commandButton
																					styleClass="BUTTON" type="button"
																					onclick="javascript:return closeAndSendRemarksToParent();"
																					value="#{msg['commons.saveButton']}"
																					 />
																		</td>
																	</tr>
																</TABLE>
									</div>
									</h:form>
								</td>
							</tr>

						</table>
					</td>
				</tr>
			</table>


</body>
</html>
</f:view>