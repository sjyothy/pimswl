<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<script type="text/javascript">

</script>
<t:div style="text-align:left;">
	<t:div styleClass="contentDiv" style="width:98.1%">
		<t:dataTable id="evaluationHistory"
			rows="#{pages$evaluationRequestHistoryTab.paginatorRows}"
			value="#{pages$evaluationRequestHistoryTab.oldEvaluationRequest}"
			
			binding="#{pages$evaluationRequestHistoryTab.requestHistoryTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
			width="100%">

			<t:column id="requestNumberCol" width="15%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['application.number.gridHeader']}" />
				</f:facet>
				<t:outputText value="#{dataItem.requestNumber}" />
			</t:column>
			<t:column id="requestDateCol" width="14%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['application.date.gridHeader']}" />
				</f:facet>
				<t:outputText value="#{dataItem.requestDateStr}" />
			</t:column>
			<t:column id="propertyNameCol" width="20%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['property.name']}" />
				</f:facet>
				<t:outputText value="#{dataItem.propertyName}" />
			</t:column>
			<t:column id="landNumberCol" width="12%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['applicationDetails.ownershipType']}" />
				</f:facet>
				<t:outputText value="#{dataItem.ownershipType}" />
			</t:column>
			<t:column id="receivedDateCol" width="12%" sortable="true">
				<f:facet name="header">
					<t:outputText
						value="#{msg['evaluationApplication.search.grid.receivedDate']}" />
				</f:facet>
				<t:outputText value="#{dataItem.receivedDateStr}" />
			</t:column>
			<t:column id="applicationStatusCol" width="12%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['application.status.gridHeader']}" />
				</f:facet>
				<t:outputText value="#{dataItem.applicationStatus}" />
			</t:column>
			<t:column id="actionCol" sortable="false" width="15%">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.action']}" />
				</f:facet>
				<t:commandLink action="#{pages$evaluationRequestHistoryTab.openEvaluationRequestPopup}">
					<h:graphicImage id="viewIcon" title="#{msg['commons.view']}"
						url="../resources/images/app_icons/View_Icon.png" />&nbsp;
				</t:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>

</t:div>
<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99.2%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']} : "/>
						<h:outputText value="#{pages$evaluationRequestHistoryTab.recordSize}"/>
						<h:outputText  />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="historyScroller" for="evaluationHistory" paginator="true"  
						fastStep="1" 
						paginatorTableClass="paginator"
						paginatorMaxPages="#{pages$evaluationRequestHistoryTab.paginatorMaxPages}" immediate="false"
						renderFacetsIfSinglePage="true" 												
						paginatorTableStyle="grid_paginator" layout="singleTable" 
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" 
						pageIndexVar="pageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}" id="lblF1"></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR1"></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF1"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}" id="lblL1"></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
							<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
	</t:div>
