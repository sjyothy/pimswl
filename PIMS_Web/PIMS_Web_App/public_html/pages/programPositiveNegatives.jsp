
<script language="JavaScript" type="text/javascript">	
</script>

<t:panelGrid id="progPosNegGrdFields"
	styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%"
	cellspacing="5px" columns="4">

	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['socialProgram.lbl.positiveNegativecommentType']}" />
	</h:panelGroup>
	<h:selectOneMenu id="selectPositiveNegativeType"
		value="#{pages$programPositiveNegatives.programPositiveNegativesView.isPositive}">
		<f:selectItem itemValue="-1"
			itemLabel="#{msg['commons.combo.PleaseSelect']}" />
		<f:selectItem itemValue="1"
			itemLabel="#{msg['socialProgram.lbl.positiveNegative.positive']}" />
		<f:selectItem itemValue="0"
			itemLabel="#{msg['socialProgram.lbl.positiveNegative.negative']}" />

	</h:selectOneMenu>
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['socialProgram.lbl.positiveNegativeComments']}" />
	</h:panelGroup>
	<h:inputTextarea
		value="#{pages$programPositiveNegatives.programPositiveNegativesView.comments}"
		style="width: 185px;" />



</t:panelGrid>
<t:panelGrid id="progPosNegGrdActions" styleClass="BUTTON_TD"
	cellpadding="1px" width="100%" cellspacing="5px">
	<h:panelGroup>
		<h:commandButton styleClass="BUTTON"
			value="#{msg['commons.saveButton']}"
			action="#{pages$programPositiveNegatives.onSave}" />
	</h:panelGroup>
</t:panelGrid>


<t:div styleClass="contentDiv" style="width:98%"
	id="progPosNegContentDiv">
	<t:dataTable id="progPosNegsDataTable" rows="200"
		value="#{pages$programPositiveNegatives.list}"
		binding="#{pages$programPositiveNegatives.dataTable}"
		preserveDataModel="false" preserveSort="false" var="dataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">

		<t:column id="progPosNegdate" width="10%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.PurposeGrid.lbl.Date']}"
					style="white-space: normal;" />
			</f:facet>

			<t:outputText style="white-space: normal;"
				value="#{dataItem.createdOn}"
				rendered="#{dataItem.isDeleted != '1' }">
				<f:convertDateTime
					pattern="#{pages$programPositiveNegatives.dateFormat}"
					timeZone="#{pages$programPositiveNegatives.timeZone}" />
			</t:outputText>
		</t:column>
		<t:column id="progPosNegCreatedBy" sortable="true">
			<f:facet name="header">
				<t:outputText
					value="#{msg['socialProgram.Requirement.lbl.CreatedBy']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				rendered="#{dataItem.isDeleted != '1' }"
				value="#{pages$programPositiveNegatives.englishLocale? dataItem.createdEn:dataItem.createdAr}" />
		</t:column>
		<t:column id="progPosNegType" width="10%" sortable="true">
			<f:facet name="header">
				<t:outputText
					value="#{msg['socialProgram.lbl.positiveNegativecommentType']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText
				value="#{dataItem.isPositive=='1' ? msg['socialProgram.lbl.positiveNegative.positive']:msg['socialProgram.lbl.positiveNegative.negative'] }"
				rendered="#{dataItem.isDeleted != '1' }"
				style="white-space: normal;" />
		</t:column>

		<t:column id="progPosNegdesc" width="60%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.comments']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText value="#{dataItem.comments}"
				rendered="#{dataItem.isDeleted != '1' }"
				style="white-space: normal;" />
		</t:column>


		<t:column id="programActionColumn" width="10%">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.action']}" />
			</f:facet>
			<h:graphicImage id="imgDeleteReq"
				rendered="#{dataItem.isDeleted != '1' }"
				title="#{msg['commons.delete']}"
				url="../resources/images/delete_icon.png"
				onclick="if (!confirm('#{msg['commons.confirmDelete']}')) return false; else onDeleted(this);">
			</h:graphicImage>
			<a4j:commandLink id="onReqDeleted"
				reRender="hiddenFields,progPosNegContentDiv,progPosNegsDataTable,successmsg,errormsg"
				onbeforedomupdate="javaScript:onRemoveDisablingDiv(); "
				action="#{pages$programPositiveNegatives.onDelete}" />
		</t:column>
	</t:dataTable>
</t:div>
