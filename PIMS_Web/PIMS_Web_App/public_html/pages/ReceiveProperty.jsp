<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html style="overflow:hidden; " dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<script language="javascript" type="text/javascript">
function   showAssetDetailsPopup()
{
	document.getElementById("cancelContractForm:lnkAssetDetails").onclick();
}
function populateAsset(aasetDesc,assetNameEn,assetNameAr,assetType,fileNumber,personName,assetId,status)
{
	document.getElementById("cancelContractForm:lnkAssetId").onclick();
}	

function   showAssetSearchPopup()
{
   var screen_width = 1024;
   var screen_height = 470;
   var screen_top = screen.top;
   //used context = MAINTENANCE_REQUEST bcoz we dont need to change the code in search asset for receive property request, we can search the same type of assets by passing this context
     window.open('SearchAssets.jsf?pageMode=MODE_SELECT_ONE_POPUP&context=MAINTENANCE_REQUEST','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
    
}	
	function showSearchProjectPopUp(pageModeKey,selectOnePopKey)
   {
		var screen_width = 1024;
		var screen_height = 450;
		window.open('projectSearch.jsf?'+'PAGE_MODE'+'='+'MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
   }
   function populateProject(projectId, projectName, projectNumber, projectEstimatedCost)
    {    	
    	document.getElementById("cancelContractForm:hdnProjectId").value = projectId;
    	document.getElementById("cancelContractForm:hdnProjectNumber").value = projectNumber;
    	document.getElementById("cancelContractForm:projectNumber").value = projectNumber;
    	 document.forms[0].submit();
    }



	
		
	function GenerateFloorsPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 120;
	   var screen_left = 150;
	   window.open('GenerateFloors.jsf','_blank','width='+(screen_width)+',height='+(screen_height)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}
function SearchMemberPopup(){
   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 120;
	   var screen_left = 150;
	   window.open('SearchMemberPopup.jsf','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');

}
function DuplicateFloorPopup(){
   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 300;
	   var screen_left = 250;
	   window.open('DuplicateFloorsPopup.jsf','_blank','width='+(screen_width-400)+',height='+(screen_height-200)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,resizable=yes');

}
function DuplicateUnitPopup(){
   var screen_width = 1024;
	   var screen_height = 350;
	   var screen_top = 300;
	   var screen_left = 275;
	   window.open('DuplicateUnits.jsf','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');

}

function ApplyRentValuePopup(){
   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 120;
	   var screen_left = 150;
	   window.open('ApplyRentValue.jsf','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');

}

		function OwnerSearchPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 120;
	   var screen_left = 150;
	   window.open('SearchPerson.jsf?persontype=OWNER&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}
	
		function RepSearchPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 120;
	   var screen_left = 150;
	   window.open('SearchPerson.jsf?persontype=REPRESENTATIVE&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		    
		   
		    if(hdnPersonType=='OWNER')
		    {
			 document.getElementById("cancelContractForm:ownerRefNo").value=personName;
			 document.getElementById("cancelContractForm:ownerHiddenRefNo").value=personName;

			 document.getElementById("cancelContractForm:ownerHiddenId").value=personId;
	         //document.getElementById("formRequest:hdntenanttypeId").value=typeId;
	        }
	        else if(hdnPersonType=='REPRESENTATIVE')
	        {
	         document.getElementById("cancelContractForm:representative").value=personName;
			 document.getElementById("cancelContractForm:representativeHiddenId").value=personId;
			 document.getElementById("cancelContractForm:representativeHiddenRefNo").value=personName;

	     
	        }
	        document.forms[0].submit();
	
	}
	function GenerateUnitsPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 1024/4;
	   var screen_left = 450/4;
	   window.open('GenerateUnits.jsf','_blank','width='+(screen_width-300)+',height='+(screen_height-500)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}
	       function submitForm()
	   {
          document.getElementById('cancelContractForm').submit();
	   }
	function FacilitiesPopup()
	{
   var screen_width = 1024;
	   var screen_height = 450;
	   var screen_top = 1024/4;
	   var screen_left = 450/4;
	   window.open('FacilityListPopup.jsf','_blank','width='+(screen_width-280)+',height='+(screen_height-400)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=no,status=yes,location=no,directories=no,resizable=no');	
	}
</script>
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<style>
.COL1 {
	width: 18%;
}
</style>
		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<%-- Header --%>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table class="greyPanelTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['receiveProperty.title']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0" height="88%">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<td width="100%" height="99%" valign="top" nowrap="nowrap">
										<h:form id="cancelContractForm" enctype="multipart/form-data">
											<div class="SCROLLABLE_SECTION">

												<table border="0" class="layoutTable"
													style="margin-left: 10px; margin-right: 10px;">
													<tr>
														<td>
															<h:outputText
																value="#{pages$ReceiveProperty.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />

															<h:outputText id="opMsgs"
																value="#{pages$ReceiveProperty.messages}"
																styleClass="INFO_FONT" escape="false" />
															<h:commandLink id="lnkAssetId"
																action="#{pages$ReceiveProperty.receiveAsset}"></h:commandLink>
															<h:commandLink id="lnkAssetDetails"
																action="#{pages$ReceiveProperty.openManageAsset}"></h:commandLink>
														</td>
													</tr>
												</table>
												<h:inputHidden id="hdnProjectId"
													value="#{pages$ReceiveProperty.hdnprojectId}">
												</h:inputHidden>
												<h:inputHidden id="hdnProjectNumber"
													value="#{pages$ReceiveProperty.hdnprojectNumber}">
												</h:inputHidden>
												<h:inputHidden id="ownerHiddenId"
													binding="#{pages$ReceiveProperty.ownerHiddenId}" />
												<h:inputHidden id="showtabsHiddenId"
													binding="#{pages$ReceiveProperty.showTabsHidden}" />
												<h:inputHidden id="representativeHiddenId"
													binding="#{pages$ReceiveProperty.representativeHiddenId}" />

												<div class="MARGIN" style="width: 95%; height: 48%;">
													<div style="height: 206px;">

														<div class="TAB_PANEL_INNER">
															<rich:tabPanel id="one1" style="width:100%;"
																binding="#{pages$ReceiveProperty.richTabPanel}">
																<rich:tab id="propertyBaseTab"
																	label="#{msg['receiveProperty.propertyTabHeader']}">
																	<%@ include file="tabPropertyDetails.jsp"%>
																</rich:tab>

																<rich:tab rendered="#{pages$ReceiveProperty.showTabs}"
																	style="width:95%;" id="receiveTeamTab"
																	label="#{msg['receiveProperty.tab.receivingTeam']}">
																	<t:panelGrid id="receivingButtonGrid" width="98%"
																		columnClasses="BUTTON_TD" columns="1">

																		<h:commandButton styleClass="BUTTON"
																			binding="#{pages$ReceiveProperty.addReceiveingTeam}"
																			value="#{msg['receiveProperty.AddMember']}"
																			style="width: 85px;"
																			action="#{pages$ReceiveProperty.showReceivingSearchPopup}" />
																	</t:panelGrid>
																	<t:div style="text-align:center;">
																		<t:div styleClass="contentDiv" style="width:95%">
																			<t:dataTable id="test8" rows="50" width="100%"
																				value="#{pages$ReceiveProperty.receivingDataList}"
																				binding="#{pages$ReceiveProperty.receivingTeamGrid}"
																				preserveDataModel="false" preserveSort="false"
																				var="dataItemRCV" rowClasses="row1,row2" rules="all"
																				renderedIfEmpty="true">




																				<t:column id="dataItemRCV2" sortable="true">


																					<f:facet name="header">

																						<t:outputText value="#{msg['member.fullName']}" />

																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItemRCV.fullNamePrimary}"
																						rendered="#{pages$ReceiveProperty.isEnglishLocale && dataItemRCV.teamMemberIsDeleted == 0}" />
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItemRCV.fullNameSecondary}"
																						rendered="#{pages$ReceiveProperty.isArabicLocale && dataItemRCV.teamMemberIsDeleted == 0}" />

																				</t:column>
																				<t:column id="dataItemRCV4" sortable="true">


																					<f:facet name="header">

																						<t:outputText value="#{msg['member.loginName']}" />

																					</f:facet>
																					<t:outputText styleClass="A_LEFT" title=""
																						value="#{dataItemRCV.userName}"
																						rendered="#{dataItemRCV.teamMemberIsDeleted == 0}" />
																				</t:column>
																				<t:column id="dataItemRCV5" sortable="true"
																					style="white-space:pre-line">


																					<f:facet name="header">

																						<t:outputText value="#{msg['member.groupName']}" />

																					</f:facet>
																					<t:outputText styleClass="A_LEFT" title=""
																						value="#{dataItemRCV.groupNamePrimary}"
																						rendered="#{pages$ReceiveProperty.isEnglishLocale && dataItemRCV.teamMemberIsDeleted == 0}" />
																					<t:outputText styleClass="A_LEFT" title=""
																						value="#{dataItemRCV.groupNameSecondary}"
																						rendered="#{pages$ReceiveProperty.isArabicLocale && dataItemRCV.teamMemberIsDeleted == 0}" />


																				</t:column>

																				<t:column
																					rendered="#{pages$ReceiveProperty.deleteAction}">
																					<f:facet name="header">
																						<t:outputText id="lbl_delete_RCV"
																							value="#{msg['commons.delete']}" />
																					</f:facet>
																					<h:commandLink id="deleteIconRCV"
																						rendered="#{dataItemRCV.teamMemberIsDeleted == 0}"
																						action="#{pages$ReceiveProperty.cmdReceivingTeamMemberDelete_Click}">
																						<h:graphicImage id="delete_IconRCV"
																							title="#{msg['commons.delete']}"
																							url="../resources/images/delete_icon.png" />
																					</h:commandLink>

																				</t:column>


																			</t:dataTable>
																		</t:div>
																	</t:div>
																</rich:tab>

																<rich:tab rendered="#{pages$ReceiveProperty.showTabs}"
																	style="width:95%;" id="evaluationTeamTab"
																	label="#{msg['receiveProperty.tab.evaluationTeam']}">


																	<t:panelGrid id="evaluationButtonGrid" width="98%"
																		columnClasses="BUTTON_TD" columns="1">

																		<h:commandButton styleClass="BUTTON"
																			binding="#{pages$ReceiveProperty.addEvaluationTeam}"
																			value="#{msg['receiveProperty.AddMember']}"
																			style="width: 85px;"
																			action="#{pages$ReceiveProperty.showEvaluationSearchPopup}" />
																	</t:panelGrid>
																	<t:div style="text-align:center;">
																		<t:div styleClass="contentDiv" style="width:95%">
																			<t:dataTable id="test9" rows="15" width="100%"
																				value="#{pages$ReceiveProperty.evaluationDataList}"
																				binding="#{pages$ReceiveProperty.evaluationTeamGrid}"
																				preserveDataModel="false" preserveSort="false"
																				var="dataItemEVA" rowClasses="row1,row2" rules="all"
																				renderedIfEmpty="true">




																				<t:column id="dataItemEVA2" sortable="true">


																					<f:facet name="header">

																						<t:outputText value="#{msg['member.fullName']}" />

																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItemEVA.fullNamePrimary}"
																						rendered="#{pages$ReceiveProperty.isEnglishLocale && dataItemEVA.teamMemberIsDeleted == 0}" />
																					<t:outputText styleClass="A_LEFT"
																						value="#{dataItemEVA.fullNameSecondary}"
																						rendered="#{pages$ReceiveProperty.isArabicLocale && dataItemEVA.teamMemberIsDeleted == 0}" />

																				</t:column>
																				<t:column id="dataItemEVA4" sortable="true">


																					<f:facet name="header">

																						<t:outputText value="#{msg['member.loginName']}" />

																					</f:facet>
																					<t:outputText styleClass="A_LEFT" title=""
																						value="#{dataItemEVA.userName}"
																						rendered="#{dataItemEVA.teamMemberIsDeleted == 0}" />
																				</t:column>
																				<t:column id="dataItemEVA5" sortable="true">


																					<f:facet name="header">

																						<t:outputText value="#{msg['member.groupName']}" />

																					</f:facet>
																					<t:outputText styleClass="A_LEFT" title=""
																						value="#{dataItemEVA.groupNamePrimary}"
																						rendered="#{pages$ReceiveProperty.isEnglishLocale && dataItemEVA.teamMemberIsDeleted == 0}" />
																					<t:outputText styleClass="A_LEFT" title=""
																						value="#{dataItemEVA.groupNameSecondary}"
																						rendered="#{pages$ReceiveProperty.isArabicLocale && dataItemEVA.teamMemberIsDeleted == 0}" />
																				</t:column>
																				<t:column
																					rendered="#{pages$ReceiveProperty.deleteAction}">
																					<f:facet name="header">
																						<t:outputText id="lbl_delete_EVA"
																							value="#{msg['commons.delete']}" />
																					</f:facet>
																					<t:commandLink id="deleteIconEVA"
																						rendered="#{dataItemEVA.teamMemberIsDeleted == 0}"
																						action="#{pages$ReceiveProperty.cmdEvaluationTeamMemberDelete_Click}">
																						<h:graphicImage id="delete_IconEVA"
																							title="#{msg['commons.delete']}"
																							url="../resources/images/delete_icon.png" />
																					</t:commandLink>

																				</t:column>




																			</t:dataTable>
																		</t:div>



																		<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																			style="width:96%;">
																			<t:panelGrid cellpadding="0" cellspacing="0"
																				width="100%" columns="2"
																				columnClasses="RECORD_NUM_TD">

																				<t:div styleClass="RECORD_NUM_BG">
																					<t:panelGrid cellpadding="0" cellspacing="0"
																						columns="3" columnClasses="RECORD_NUM_TD">

																						<h:outputText
																							value="#{msg['commons.recordsFound']}" />

																						<h:outputText value=" : "
																							styleClass="RECORD_NUM_TD" />

																						<h:outputText
																							value="#{pages$ReceiveProperty.evaluationTeamRecordSize}"
																							styleClass="RECORD_NUM_TD" />

																					</t:panelGrid>
																				</t:div>

																				<t:dataScroller id="evalscroller" for="test9"
																					paginator="true" fastStep="1" paginatorMaxPages="2"
																					immediate="false" paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true"
																					paginatorTableStyle="grid_paginator"
																					layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																					paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																					pageIndexVar="pageNumber" styleClass="SCH_SCROLLER">

																					<f:facet name="first">
																						<t:graphicImage url="../#{path.scroller_first}"
																							id="lblFinquiry"></t:graphicImage>
																					</f:facet>
																					<f:facet name="fastrewind">
																						<t:graphicImage
																							url="../#{path.scroller_fastRewind}"
																							id="lblFRinquiry"></t:graphicImage>
																					</f:facet>
																					<f:facet name="fastforward">
																						<t:graphicImage
																							url="../#{path.scroller_fastForward}"
																							id="lblFFinquiry"></t:graphicImage>
																					</f:facet>
																					<f:facet name="last">
																						<t:graphicImage url="../#{path.scroller_last}"
																							id="lblLinquiry"></t:graphicImage>
																					</f:facet>
																					<t:div styleClass="PAGE_NUM_BG" rendered="true">
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.pageNumber}" />
																					</t:div>
																				</t:dataScroller>

																			</t:panelGrid>
																		</t:div>

																	</t:div>




																</rich:tab>
																<rich:tab rendered="#{pages$ReceiveProperty.showTabs}"
																	label="#{msg['contract.ownersTabHeader']}">
																	<t:div style="height:320px;overflow-y:scroll;">
																		<h:inputHidden
																			value="#{pages$ReceiveProperty.ownerRefNo}"
																			id="ownerHiddenRefNo" />
																		<h:inputHidden
																			value="#{pages$ReceiveProperty.representativeRefNo}"
																			id="representativeHiddenRefNo" />
																		<t:panelGrid width="97.5%" columns="4">
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.owner']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:panelGroup>

																				<h:inputText id="ownerRefNo"
																					binding="#{pages$ReceiveProperty.ownerReferenceNo}"
																					value="#{pages$ReceiveProperty.ownerRefNo}"
																					readonly="true" styleClass="READONLY" />

																				<h:commandLink id="OwnerLink"
																					actionListener="#{pages$ReceiveProperty.showOwnersSearchPopup}">
																					<h:graphicImage
																						binding="#{pages$ReceiveProperty.ownerSearchButton}"
																						alt="#{msg['receiveProperty.owner']}"
																						url="../resources/images/app_icons/Add-Sponsor.png" />
																				</h:commandLink>


																			</h:panelGroup>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.representative']}:"></h:outputLabel>

																			<h:panelGroup>
																				<h:inputText id="representative" readonly="true"
																					styleClass="READONLY"
																					binding="#{pages$ReceiveProperty.ownerRepresentative}"
																					value="#{pages$ReceiveProperty.representativeRefNo}" />

																				<h:commandLink id="facRefNoLink"
																					actionListener="#{pages$ReceiveProperty.showRepresenatativesSearchPopup}">
																					<h:graphicImage
																						binding="#{pages$ReceiveProperty.representativeSearchButton}"
																						alt="#{msg['receiveProperty.representative']}"
																						url="../resources/images/app_icons/Add-Sponsor.png" />
																				</h:commandLink>



																			</h:panelGroup>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.percentage']}:"></h:outputLabel>

																			<h:inputText id="percentage" styleClass="A_RIGHT_NUM"
																				binding="#{pages$ReceiveProperty.ownerPercentage}" />

																		</t:panelGrid>

																		<t:panelGrid id="ownerbuttongrid" width="97%"
																			columnClasses="BUTTON_TD" columns="1">

																			<h:commandButton styleClass="BUTTON"
																				binding="#{pages$ReceiveProperty.ownerSaveButton}"
																				value="#{msg['commons.Add']}"
																				action="#{pages$ReceiveProperty.saveOwner}" />
																		</t:panelGrid>





																		<t:div style="text-align:center;">
																			<t:div styleClass="contentDiv" style="width:95%">
																				<t:dataTable id="owners" rows="15" width="100%"
																					value="#{pages$ReceiveProperty.ownerDataList}"
																					binding="#{pages$ReceiveProperty.ownerDataGrid}"
																					preserveDataModel="false" preserveSort="false"
																					var="ownerDataItem" rowClasses="row1,row2"
																					rules="all" renderedIfEmpty="true">
																					<t:column id="col_ownerName" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.ownerName']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{ownerDataItem.ownerName}" />
																					</t:column>
																					<t:column id="col_representativeName">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.representativeName']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{ownerDataItem.representativeName}" />
																					</t:column>
																					<t:column id="col_percentage">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.percentage']}" />
																						</f:facet>
																						<t:outputText styleClass="A_RIGHT"
																							value="#{ownerDataItem.ownerSharePercent}" />
																					</t:column>
																					<t:column
																						rendered="#{pages$ReceiveProperty.deleteAction}">
																						<f:facet name="header">
																							<t:outputText id="lbl_delete"
																								value="#{msg['commons.delete']}" />
																						</f:facet>
																						<t:commandLink id="ownerDeleteIcon"
																							action="#{pages$ReceiveProperty.cmdOwnerDelete_Click}">
																							<h:graphicImage id="delete_Icon"
																								title="#{msg['commons.delete']}"
																								url="../resources/images/delete_icon.png" />
																						</t:commandLink>

																					</t:column>

																				</t:dataTable>
																			</t:div>



																			<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																				style="width:96%;">
																				<t:panelGrid cellpadding="0" cellspacing="0"
																					width="100%" columns="2"
																					columnClasses="RECORD_NUM_TD">

																					<t:div styleClass="RECORD_NUM_BG">
																						<t:panelGrid cellpadding="0" cellspacing="0"
																							columns="3" columnClasses="RECORD_NUM_TD">

																							<h:outputText
																								value="#{msg['commons.recordsFound']}" />

																							<h:outputText value=" : "
																								styleClass="RECORD_NUM_TD" />

																							<h:outputText
																								value="#{pages$ReceiveProperty.ownerRecordSize}"
																								styleClass="RECORD_NUM_TD" />

																						</t:panelGrid>
																					</t:div>

																					<t:dataScroller id="ownerDataItemssss" for="owners"
																						paginator="true" fastStep="1"
																						paginatorMaxPages="10" immediate="false"
																						styleClass="SCH_SCROLLER"
																						paginatorTableClass="paginator"
																						renderFacetsIfSinglePage="true"
																						paginatorTableStyle="grid_paginator"
																						layout="singleTable"
																						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																						paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																						pageIndexVar="ownerPageNumber">

																						<f:facet name="first">
																							<t:graphicImage url="../#{path.scroller_first}"
																								id="lblFowner"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastrewind">
																							<t:graphicImage
																								url="../#{path.scroller_fastRewind}"
																								id="lblFRowner"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastforward">
																							<t:graphicImage
																								url="../#{path.scroller_fastForward}"
																								id="lblFFowner"></t:graphicImage>
																						</f:facet>
																						<f:facet name="last">
																							<t:graphicImage url="../#{path.scroller_last}"
																								id="lblLowner"></t:graphicImage>
																						</f:facet>
																						<t:div styleClass="PAGE_NUM_BG">
																							<h:outputText styleClass="PAGE_NUM"
																								value="#{msg['commons.page']}" />
																							<h:outputText styleClass="PAGE_NUM"
																								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																								value="#{requestScope.ownerPageNumber}" />
																						</t:div>
																					</t:dataScroller>

																				</t:panelGrid>
																			</t:div>

																		</t:div>

																	</t:div>


																</rich:tab>
																<rich:tab rendered="#{pages$ReceiveProperty.showTabs}"
																	label="#{msg['receiveProperty.facilityTabHeader']}">
																	<jsp:include page="propertyFacilityTab.jsp" />
																</rich:tab>



																<rich:tab rendered="#{pages$ReceiveProperty.showTabs}"
																	label="#{msg['receiveProperty.floorTabHeader']}">
																	<t:div style="height:320px;overflow-y:scroll;">
																		<t:panelGrid width="97%" columns="4" id="floorGrid">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.floorType']}:" />
																			<h:selectOneMenu id="floorTYpe"
																				binding="#{pages$ReceiveProperty.floorTypeSelectOneMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ReceiveProperty.floorTypeItems}" />
																			</h:selectOneMenu>

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.floorSeperator']}:" />
																			<h:inputText id="floorSeperateor"
																				binding="#{pages$ReceiveProperty.floorSeperator}" />

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.floorPrefix']}:" />
																			<h:inputText id="floorPrefix"
																				binding="#{pages$ReceiveProperty.floorPrefix}" />



																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					id="floorNoFromLabel"
																					value="#{msg['receiveProperty.floorNumberFrom']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:inputText id="floorNoFrom"
																				binding="#{pages$ReceiveProperty.floorFrom}" />
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					id="floorNoToLabel"
																					value="#{msg['receiveProperty.floorNumberTo']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:inputText id="floorNoTo"
																				binding="#{pages$ReceiveProperty.floorTo}" />
																			<t:panelGroup>
																				<%--<h:outputLabel styleClass="mandatory" value="*" />--%>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.generateAfterThisFloor']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:selectOneMenu id="afterFloorsMenu"
																				binding="#{pages$ReceiveProperty.afterFloorMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ReceiveProperty.floorsList}" />

																			</h:selectOneMenu>
																			<h:outputLabel id="floorDescription"
																				styleClass="LABEL"
																				value="#{msg['receiveProperty.floorDescription']}:" />
																			<h:inputTextarea id="floorDescriptionTextArea"
																				style="WIDTH: 186px;"
																				binding="#{pages$ReceiveProperty.floorDescription}" />
																		</t:panelGrid>



																		<t:panelGrid id="floorbuttongrid" width="97%"
																			columnClasses="BUTTON_TD" columns="1">


																			<h:commandButton styleClass="BUTTON"
																				binding="#{pages$ReceiveProperty.addFloorButton}"
																				value="#{msg['commons.Add']}"
																				action="#{pages$ReceiveProperty.generatePropertyFloors}" />
																			<pims:security
																				screen="Pims.PropertyManagement.Unit.AddUnit"
																				action="view">
																				<h:commandButton styleClass="BUTTON"
																					rendered="#{pages$ReceiveProperty.propertyReceived}"
																					value="#{msg['commons.Add']}"
																					onclick="if (!confirm('#{msg['warningMsg.suretoAddFloor']}')) return false;"
																					action="#{pages$ReceiveProperty.generatePropertyFloorsAfterReceived}" />
																			</pims:security>

																		</t:panelGrid>
																		<t:div style="text-align:center;">
																			<t:div styleClass="contentDiv" style="width:95%">
																				<t:dataTable id="floors" rows="15" width="100%"
																					value="#{pages$ReceiveProperty.floorsDataList}"
																					binding="#{pages$ReceiveProperty.floorTable}"
																					preserveDataModel="false" preserveSort="false"
																					var="floorDataItem" rowClasses="row1,row2"
																					rules="all" renderedIfEmpty="true">
																					<t:column id="col_mutliSelect" style="width:10%;">
																						<h:selectBooleanCheckbox id="multiFloor"
																							disabled="#{floorDataItem.typeId == pages$ReceiveProperty.groundFloorTypeID}"
																							style="width:25%;"
																							value="#{floorDataItem.isSelected}" />
																					</t:column>


																					<t:column id="col_floorNo" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.floorNumberHeading']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{floorDataItem.floorNumber}" />
																					</t:column>
																					<t:column id="col_floorDesc">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.floorDescription']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{floorDataItem.floorDescription}" />
																					</t:column>
																					<t:column id="col_floorStatus" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.floorType']}" />
																						</f:facet>
																						<t:outputText value="#{floorDataItem.floorTypeEn}"
																							rendered="#{pages$ReceiveProperty.isEnglishLocale}" />
																						<t:outputText value="#{floorDataItem.floorTypeAr}"
																							rendered="#{pages$ReceiveProperty.isArabicLocale}" />
																					</t:column>
																					<t:column id="col_floorUnits" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.noOfUnits']}" />
																						</f:facet>
																						<t:outputText value="#{floorDataItem.noOfUnits}" />
																					</t:column>
																					<t:column
																						rendered="#{pages$ReceiveProperty.deleteAction}">
																						<f:facet name="header">
																							<t:outputText id="lbl_edit"
																								value="#{msg['commons.action']}" />
																						</f:facet>

																						<h:commandLink
																							rendered="#{floorDataItem.typeId != pages$ReceiveProperty.groundFloorTypeID}"
																							action="#{pages$ReceiveProperty.showDuplicateFloorsSearchPopup}">
																							<h:graphicImage id="edit_Icon"
																								title="#{msg['commons.duplicate']}"
																								url="../resources/images/duplicate.png"
																								width="18px;" />
																						</h:commandLink>
																						<h:commandLink id="deleteIconRCVFloor"
																							rendered="#{floorDataItem.typeId != pages$ReceiveProperty.groundFloorTypeID}"
																							action="#{pages$ReceiveProperty.deleteFloor}">
																							<h:graphicImage id="delete_IconRCVFloor"
																								title="#{msg['commons.delete']}"
																								url="../resources/images/delete_icon.png" />
																						</h:commandLink>

																					</t:column>

																					<t:column id="col_floorSeq" rendered="false">
																						<f:facet name="header">
																							<t:outputText value="Foor Sequence" />
																						</f:facet>
																						<t:outputText
																							value="#{floorDataItem.floorSequence}" />
																					</t:column>

																				</t:dataTable>
																			</t:div>

																			<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																				style="width:96%;">
																				<t:panelGrid cellpadding="0" cellspacing="0"
																					width="100%" columns="2"
																					columnClasses="RECORD_NUM_TD">

																					<t:div styleClass="RECORD_NUM_BG">
																						<t:panelGrid cellpadding="0" cellspacing="0"
																							columns="3" columnClasses="RECORD_NUM_TD">

																							<h:outputText
																								value="#{msg['commons.recordsFound']}" />

																							<h:outputText value=" : "
																								styleClass="RECORD_NUM_TD" />

																							<h:outputText
																								value="#{pages$ReceiveProperty.floorRecordSize}"
																								styleClass="RECORD_NUM_TD" />

																						</t:panelGrid>
																					</t:div>

																					<t:dataScroller id="floorScroller" for="floors"
																						paginator="true" fastStep="1"
																						paginatorMaxPages="10" immediate="false"
																						styleClass="SCH_SCROLLER"
																						paginatorTableClass="paginator"
																						renderFacetsIfSinglePage="true"
																						paginatorTableStyle="grid_paginator"
																						layout="singleTable"
																						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																						paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																						pageIndexVar="floorPageNumber">

																						<f:facet name="first">
																							<t:graphicImage url="../#{path.scroller_first}"
																								id="lblFfloor"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastrewind">
																							<t:graphicImage
																								url="../#{path.scroller_fastRewind}"
																								id="lblFRfloor"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastforward">
																							<t:graphicImage
																								url="../#{path.scroller_fastForward}"
																								id="lblFFfloor"></t:graphicImage>
																						</f:facet>
																						<f:facet name="last">
																							<t:graphicImage url="../#{path.scroller_last}"
																								id="lblLfloor"></t:graphicImage>
																						</f:facet>
																						<t:div styleClass="PAGE_NUM_BG">
																							<h:outputText styleClass="PAGE_NUM"
																								value="#{msg['commons.page']}" />
																							<h:outputText styleClass="PAGE_NUM"
																								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																								value="#{requestScope.floorPageNumber}" />
																						</t:div>
																					</t:dataScroller>

																				</t:panelGrid>
																			</t:div>

																		</t:div>
																		<t:panelGrid id="deleteFloorsbuttongrid" width="97%"
																			columnClasses="BUTTON_TD" columns="1">

																			<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.delete']}"
																				action="#{pages$ReceiveProperty.deleteMultipleFloors}"
																				rendered="#{pages$ReceiveProperty.deleteAction}">
																			</h:commandButton>
																		</t:panelGrid>
																	</t:div>

																</rich:tab>
																<rich:tab rendered="#{pages$ReceiveProperty.showTabs}"
																	id="unitTab"
																	label="#{msg['receiveProperty.unitTabHeader']}">
																	<t:div style="height:320px;overflow-y:scroll;">
																		<t:panelGrid width="97%" columns="4" id="unitGrid">
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.floorNumber']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:selectOneMenu id="floorsMenu"
																				binding="#{pages$ReceiveProperty.floorNumberMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ReceiveProperty.floorsList}" />

																			</h:selectOneMenu>

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unitPrefix']}:"></h:outputLabel>

																			<h:inputText id="unitPrefix" maxlength="5"
																				binding="#{pages$ReceiveProperty.unitPrefix}" />
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.unitNo']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:inputText id="unitNo" maxlength="15"
																				binding="#{pages$ReceiveProperty.unitNo}" />

																			<h:outputLabel styleClass="LABEL" rendered="false"
																				value="#{msg['receiveProperty.unitNumber']}:"></h:outputLabel>

																			<h:inputText id="unitNumber" rendered="false"
																				binding="#{pages$ReceiveProperty.unitNumber}" />

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['settlement.label.costCenter']}:"></h:outputLabel>

																			<h:inputText id="unitCostCenter"
																				binding="#{pages$ReceiveProperty.accountNumber}" />


																			<h:outputLabel styleClass="LABEL" rendered="false"
																				value="#{msg['receiveProperty.unitStatus']}:"></h:outputLabel>

																			<h:inputText id="unitStatus" readonly="true"
																				styleClass="READONLY"
																				binding="#{pages$ReceiveProperty.unitStatus}"
																				rendered="false" />

																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.unitType']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:selectOneMenu id="unitTypeMenu"
																				binding="#{pages$ReceiveProperty.unitTypeMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ReceiveProperty.unitTypeList}" />

																			</h:selectOneMenu>
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.unitSide']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:selectOneMenu id="unitSideMenu"
																				binding="#{pages$ReceiveProperty.unitSideMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ReceiveProperty.unitSideList}" />

																			</h:selectOneMenu>

																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.unitUsageType']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:selectOneMenu id="unitUsageTypeMenu"
																				binding="#{pages$ReceiveProperty.unitUsageTypeMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ReceiveProperty.unitUsageTypeList}" />

																			</h:selectOneMenu>
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.unitInvestmentType']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:selectOneMenu id="unitInvestmentTypeMenu"
																				binding="#{pages$ReceiveProperty.unitInvestmentTypeMenu}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ReceiveProperty.unitInvestmentTypeList}" />

																			</h:selectOneMenu>

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unitDescription']}:"></h:outputLabel>

																			<h:inputText id="unitDescription"
																				binding="#{pages$ReceiveProperty.unitDescription}" />






																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.areaInSquare']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:inputText id="areaInSquare"
																				style="text-align: right;"
																				binding="#{pages$ReceiveProperty.areaInSquare}" />
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.bedRooms']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:inputText id="bedRooms" style="text-align: right;"
																				binding="#{pages$ReceiveProperty.bedRooms}" />
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.livingRooms']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:inputText id="livingRooms"
																				style="text-align: right;"
																				binding="#{pages$ReceiveProperty.livingRooms}" />
																			<t:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.bathRooms']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:inputText id="bathRooms"
																				style="text-align: right;"
																				binding="#{pages$ReceiveProperty.bathRooms}" />



																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unitEWUtilityNo']}:"></h:outputLabel>
																			<h:inputText id="unitEWUtilityNumber"
																				binding="#{pages$ReceiveProperty.unitEWUtilityNumber}" />
																			<t:panelGroup>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.rentValueForUnits']}:"></h:outputLabel>
																			</t:panelGroup>
																			<h:inputText id="rentValueForUnits"
																				style="text-align: right;"
																				binding="#{pages$ReceiveProperty.rentValueForUnits}">
																				<f:convertNumber minFractionDigits="2"
																					maxFractionDigits="2" pattern="##,###,###.##" />
																			</h:inputText>

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['receiveProperty.unitRemarks']}:"></h:outputLabel>
																			<h:inputText id="unitRemarks"
																				binding="#{pages$ReceiveProperty.unitRemarks}" />
																			<t:panelGroup colspan="2">
																				<h:selectBooleanCheckbox
																					style="width: 15%;height: 22px; border: 0px;"
																					binding="#{pages$ReceiveProperty.includeFloorNumber}"
																					value="#{pages$ReceiveProperty.includeFloorNumberVar}">
																					<h:outputText
																						value="#{msg['receiveProperty.unit.addWithFloors']}:" />
																				</h:selectBooleanCheckbox>
																			</t:panelGroup>
																		</t:panelGrid>
																		<t:panelGrid id="unitbuttongrid"
																			columnClasses="BUTTON_TD" style="width:97%;"
																			columns="2">

																			<h:commandButton styleClass="BUTTON"
																				binding="#{pages$ReceiveProperty.addUnitButton}"
																				value="#{msg['commons.Add']}"
																				action="#{pages$ReceiveProperty.generatePropertyUnits}" />
																			<pims:security
																				screen="Pims.PropertyManagement.Unit.AddUnit"
																				action="view">
																				<h:commandButton styleClass="BUTTON"
																					rendered="#{pages$ReceiveProperty.propertyReceived}"
																					value="#{msg['commons.Add']}"
																					onclick="if (!confirm('#{msg['warningMsg.suretoAddUnit']}')) return false;"
																					action="#{pages$ReceiveProperty.generatePropertyUnitsAfterReceived}" />
																					
																					<h:commandButton styleClass="BUTTON"
																					style="width:auto;"
																					rendered="#{pages$ReceiveProperty.propertyReceived}"
																					value="#{msg['inheritanceFile.lbl.generateCostCenter']}"
																					action="#{pages$ReceiveProperty.onGenerateUnitCostCenter}" />
																			</pims:security>
																			<h:commandButton styleClass="BUTTON"
																				binding="#{pages$ReceiveProperty.btnUpdateUnitAtApproval}"
																				value="#{msg['commons.Update']}"
																				action="#{pages$ReceiveProperty.updateUnitAtApproval}" />
																		</t:panelGrid>
																		<t:div style="text-align:center;">
																			<t:div styleClass="contentDiv" style="width:95%">
																				<t:dataTable id="units" rows="15" width="100%"
																					value="#{pages$ReceiveProperty.unitDataList}"
																					binding="#{pages$ReceiveProperty.unitTable}"
																					preserveDataModel="false" preserveSort="false"
																					var="unitDataItem" rowClasses="row1,row2"
																					rules="all" renderedIfEmpty="true">
																					<t:column id="col_mutliSelectUnit"
																						style="width:10%;">
																						<h:selectBooleanCheckbox id="multiUnit"
																							style="width:25%;"
																							value="#{unitDataItem.selected}" />
																					</t:column>

																					<t:column id="col_unitNo" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.unitNumberHeading']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{unitDataItem.unitNumber}" />
																					</t:column>
																					<t:column id="unit_floorNo" sortable="true">
																						<f:facet name="header">
																							<t:outputText id="unit_FloorNoHeading"
																								value="#{msg['receiveProperty.floorNumber']}" />
																						</f:facet>

																						<t:outputText id="unit_FLNO" styleClass="A_LEFT"
																							value="#{unitDataItem.floorNumber}" />
																					</t:column>

																					<t:column id="col_unitStatus" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.unitStatusHeading']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{unitDataItem.statusEn}"
																							rendered="#{pages$ReceiveProperty.isEnglishLocale}" />
																						<t:outputText styleClass="A_LEFT"
																							value="#{unitDataItem.statusAr}"
																							rendered="#{pages$ReceiveProperty.isArabicLocale}" />


																					</t:column>
																					<t:column id="oldNewCC" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['property.fieldLabel.oldNewCostCenter']}" />
																						</f:facet>
																						<t:outputText value="#{unitDataItem.oldNewCC}" />
																					</t:column>

																					<t:column id="col_unitRent" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.unitRentHeading']}" />
																						</f:facet>
																						<t:outputText styleClass="A_RIGHT"
																							value="#{unitDataItem.rentValue}">
																							<f:convertNumber minFractionDigits="2"
																								maxFractionDigits="2" pattern="##,###,###.##" />
																						</t:outputText>
																					</t:column>
																					<t:column id="col_unitType" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['receiveProperty.unitTypeHeading']}" />
																						</f:facet>
																						<t:outputText styleClass="A_LEFT"
																							value="#{unitDataItem.unitTypeEn}"
																							rendered="#{pages$ReceiveProperty.isEnglishLocale}" />
																						<t:outputText styleClass="A_LEFT"
																							rendered="#{pages$ReceiveProperty.isArabicLocale}"
																							value="#{unitDataItem.unitTypeAr}" />
																					</t:column>

																					<t:column id="col_unitAreaSqFt" sortable="true">
																						<f:facet name="header">
																							<t:outputText
																								value="#{msg['listReport.unitTemplate.unitAreaSqFt']}" />
																						</f:facet>
																						<t:outputText styleClass="A_RIGHT"
																							value="#{unitDataItem.unitArea}">
																							<f:convertNumber minFractionDigits="2"
																								maxFractionDigits="2" pattern="##,###,###.##" />
																						</t:outputText>
																					</t:column>

																					<t:column
																						rendered="#{pages$ReceiveProperty.approvalTask}">
																						<%--Change by Kamran: code was too difficult to manage, thats y i m duplicating the edit button for approval task --%>
																						<h:commandLink
																							action="#{pages$ReceiveProperty.editUnitAtApproval}">
																							<h:graphicImage style="margin-left:6px;"
																								id="editUnitImage2"
																								title="#{msg['commons.edit']}"
																								url="../resources/images/edit-icon.gif" />
																						</h:commandLink>
																					</t:column>

																					<t:column
																						rendered="#{pages$ReceiveProperty.deleteAction}">
																						<f:facet name="header">
																							<t:outputText id="lbl_edit2"
																								value="#{msg['commons.action']}" />
																						</f:facet>
																						<h:commandLink
																							action="#{pages$ReceiveProperty.showDuplicateUnitsSearchPopup}">
																							<h:graphicImage id="edit_Icon2"
																								title="#{msg['commons.duplicate']}"
																								url="../resources/images/duplicate.png"
																								width="18px;" />

																						</h:commandLink>
																						<h:commandLink
																							action="#{pages$ReceiveProperty.editUnit}">
																							<h:graphicImage style="margin-left:6px;"
																								id="editUnitImage"
																								title="#{msg['commons.edit']}"
																								url="../resources/images/edit-icon.gif" />
																						</h:commandLink>
																						<h:commandLink
																							rendered="#{pages$ReceiveProperty.onRentValue}"
																							action="#{pages$ReceiveProperty.showRentValuePopup}">
																							<h:graphicImage id="edit_Icon3"
																								title="#{msg['commons.applyRentValue']}"
																								url="../resources/images/conduct.gif" />
																						</h:commandLink>

																						<h:commandLink id="deleteIconRCVUnit"
																							action="#{pages$ReceiveProperty.deleteUnit}">
																							<h:graphicImage id="delete_IconRCVUnit"
																								title="#{msg['commons.delete']}"
																								url="../resources/images/delete_icon.png" />
																						</h:commandLink>

																					</t:column>

																				</t:dataTable>
																			</t:div>
																			<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																				style="width:96%;">
																				<t:panelGrid cellpadding="0" cellspacing="0"
																					width="100%" columns="2"
																					columnClasses="RECORD_NUM_TD">

																					<t:div styleClass="RECORD_NUM_BG">
																						<t:panelGrid cellpadding="0" cellspacing="0"
																							columns="3" columnClasses="RECORD_NUM_TD">

																							<h:outputText
																								value="#{msg['commons.recordsFound']}" />

																							<h:outputText value=" : "
																								styleClass="RECORD_NUM_TD" />

																							<h:outputText
																								value="#{pages$ReceiveProperty.unitRecordSize}"
																								styleClass="RECORD_NUM_TD" />

																						</t:panelGrid>
																					</t:div>

																					<t:dataScroller id="unitScroller" for="units"
																						paginator="true" fastStep="1"
																						paginatorMaxPages="10" immediate="false"
																						styleClass="SCH_SCROLLER"
																						paginatorTableClass="paginator"
																						renderFacetsIfSinglePage="true"
																						paginatorTableStyle="grid_paginator"
																						layout="singleTable"
																						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																						paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																						pageIndexVar="unitPageNumber">

																						<f:facet name="first">
																							<t:graphicImage url="../#{path.scroller_first}"
																								id="lblFunit"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastrewind">
																							<t:graphicImage
																								url="../#{path.scroller_fastRewind}"
																								id="lblFRunit"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastforward">
																							<t:graphicImage
																								url="../#{path.scroller_fastForward}"
																								id="lblFFunit"></t:graphicImage>
																						</f:facet>
																						<f:facet name="last">
																							<t:graphicImage url="../#{path.scroller_last}"
																								id="lblLunit"></t:graphicImage>
																						</f:facet>
																						<t:div styleClass="PAGE_NUM_BG">
																							<h:outputText styleClass="PAGE_NUM"
																								value="#{msg['commons.page']}" />
																							<h:outputText styleClass="PAGE_NUM"
																								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																								value="#{requestScope.unitPageNumber}" />
																						</t:div>
																					</t:dataScroller>

																				</t:panelGrid>
																			</t:div>
																		</t:div>
																		<t:panelGrid id="deleteUnitsbuttongrid"
																			columnClasses="BUTTON_TD" columns="1"
																			style="width:97%;">

																			<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.delete']}"
																				action="#{pages$ReceiveProperty.deleteMultipleUnits}"
																				rendered="#{pages$ReceiveProperty.deleteAction}">
																			</h:commandButton>
																		</t:panelGrid>
																	</t:div>
																</rich:tab>
																<rich:tab id="attachmentTab"
																	rendered="#{pages$ReceiveProperty.showDocNComTabs}"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>
																<rich:tab id="commentsTab"
																	rendered="#{pages$ReceiveProperty.showDocNComTabs}"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" />
																</td>
															</tr>
														</table>
														<table cellpadding="0px" cellspacing="1px" class="A_RIGHT"
															width="100%">


															<tr>

																<td colspan="4" class="BUTTON_TD">
																	<h:commandButton styleClass="BUTTON" id="saveBtn"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ReceiveProperty.saveButton}"
																		value="#{msg['commons.saveButton']}"
																		action="#{pages$ReceiveProperty.saveProperty}" />
																	<h:commandButton styleClass="BUTTON"
																		id="saveAfterReceivedButton"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ReceiveProperty.saveAfterReceivedButton}"
																		value="#{msg['commons.saveButton']}"
																		action="#{pages$ReceiveProperty.saveProperty}" />
																	<h:commandButton styleClass="BUTTON" id="submitButton"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ReceiveProperty.submitButton}"
																		value="#{msg['commons.sendButton']}"
																		action="#{pages$ReceiveProperty.sendForEvaluation}" />
																	<h:commandButton styleClass="BUTTON"
																		id="submitAfterEvaluation"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ReceiveProperty.submitAfterEvaluation}"
																		value="#{msg['commons.submitButton']}"
																		action="#{pages$ReceiveProperty.sendAfterEvaluation}" />
																	<h:commandButton styleClass="BUTTON"
																		id="approveEvaluation"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ReceiveProperty.approveEvaluation}"
																		value="#{msg['commons.approve']}"
																		action="#{pages$ReceiveProperty.approveEvaluation}" />
																	<h:commandButton styleClass="BUTTON"
																		id="reviewApproveEvaluation"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ReceiveProperty.reviewApproveEvaluation}"
																		value="#{msg['commons.review']}"
																		action="#{pages$ReceiveProperty.receiveProperty}" />
																	<h:commandButton styleClass="BUTTON" id="rejectEval"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ReceiveProperty.rejectEvaluation}"
																		value="#{msg['commons.reject']}"
																		action="#{pages$ReceiveProperty.rejectEvaluation}" />
																	<h:commandButton styleClass="BUTTON"
																		id="approveRentCommittee"
																		onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false"
																		binding="#{pages$ReceiveProperty.approveRentCommittee}"
																		value="#{msg['commons.approve']}"
																		action="#{pages$ReceiveProperty.approveAndUpdate}" />

																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.back']}"
																		action="#{pages$ReceiveProperty.onBack}"
																		rendered="#{pages$ReceiveProperty.showBackButton}">
																	</h:commandButton>

																</td>

															</tr>
														</table>
													</div>

												</div>


											</div>

										</h:form>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>
