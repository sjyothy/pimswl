					<script type="text/javascript">
						function onfilename(elt){
							var url = $(elt).next().getAttribute('value');
							if (url != null && url!='')
								url='file://///'+url.substr(2);
								window.open(url);
						}
					</script>
					<t:div styleClass="contentDiv" style="border:0px none;">
						<t:dataTable id="viewAttachmentTable" renderedIfEmpty="false" var="filedata" value="#{attachmentController.filesList}" preserveDataModel="false" preserveSort="false">
							<t:column>
								<f:facet name="header"><h:outputText value="#{msg['attachment.file']}" styleClass="filename_th"/></f:facet>
								<t:outputText value="#{filedata.fileName}" onclick="javascript: onfilename(this);" styleClass="filename_url"/>
								<t:inputHidden value="#{filedata.url}"/>
							</t:column>
							<t:column>
								<f:facet name="header"><h:outputText value="#{msg['attachment.type']}"/></f:facet>
								<t:outputText value="#{filedata.type}" />
							</t:column>
							<t:column>
								<f:facet name="header"><h:outputText value="#{msg['attachment.addedby']}"/></f:facet>
								<t:outputText value="#{filedata.doc.createdBy}" />
							</t:column>
							<t:column>
								<f:facet name="header"><h:outputText value="#{msg['attachment.datetime']}"/></f:facet>
								<t:outputText value="#{filedata.doc.createdOn}" />
							</t:column>
						</t:dataTable>
					</t:div>
