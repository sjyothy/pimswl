					<script type="text/javascript">
						function onfilename(elt){
							var url = $(elt).next().getAttribute('value');
							if (url != null && url!=''){
								url='file://///'+url.substr(2);
								try{
								window.open(url);
								}catch(e){alert(e);}
							}
						}
					</script>
					<t:div styleClass="contentDiv" style="border:0px none;">
						<t:panelGrid columns="1" cellpadding="0" cellspacing="0">
							<t:panelGrid columns="2" cellpadding="0" cellspacing="2">
								<t:inputFileUpload styleClass="INPUT" storage="file" value="#{attachmentController.filedata}" binding="#{attachmentController.fileupload}"/>
								<t:commandButton value="#{msg['attachment.add']}" actionListener="#{attachmentController.uploadFile}" styleClass="BUTTON"/>
							</t:panelGrid>
							<t:dataTable id="attachmentTable" renderedIfEmpty="false" width="100%" cellpadding="0" cellspacing="0" var="filedata" binding="#{attachmentController.dataTable}" value="#{attachmentController.filesList}" preserveDataModel="false" preserveSort="false">
								<t:column>
									<f:facet name="header"><h:outputText value="#{msg['attachment.file']}" styleClass="filename_th"/></f:facet>
									<t:outputText value="#{filedata.fileName}" onclick="javascript: onfilename(this);" styleClass="filename_url"/>
									<t:inputHidden value="#{filedata.url}"/>
								</t:column>
								<t:column>
									<f:facet name="header"><h:outputText value="#{msg['attachment.type']}"/></f:facet>
									<t:outputText value="#{filedata.type}" />
								</t:column>
								<t:column>
									<f:facet name="header"><h:outputText value="#{msg['attachment.addedby']}"/></f:facet>
									<t:outputText value="#{filedata.doc.createdBy}" />
								</t:column>
								<t:column>
									<f:facet name="header"><h:outputText value="#{msg['attachment.datetime']}"/></f:facet>
									<t:outputText value="#{filedata.doc.createdOn}" />
								</t:column>
								<t:column>
									<f:facet name="header"></f:facet>
									<t:commandLink actionListener="#{attachmentController.removeAttachment}" value="" styleClass="DELETE_STYLE">
										<f:param name="linkid" value="#{filedata.id}"/>
									</t:commandLink>
								</t:column>
							</t:dataTable>
						</t:panelGrid>
					</t:div>
