<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<t:div style="width:100%;">
	<t:panelGrid columns="1" cellpadding="0" cellspacing="0">
		<t:div styleClass="BUTTON_TD">
			<t:commandButton id="btnOpenArchivedDocs" styleClass="BUTTON"
				value="#{msg['attachment.buttons.archivedDocs']}"
				action="#{pages$attachment.onOpenArchiveDocumentsPopup}"
				style="width: 110px;margin: 5px;" />
			<t:commandButton id="btnAddAttachment" styleClass="BUTTON"
				rendered="#{pages$attachment.canAddAttachment}"
				value="#{msg['attachment.buttons.addAttachment']}"
				actionListener="#{pages$attachment.openUploadPopup}"
				style="width: 110px;margin: 5px;" />
		</t:div>

		<t:collapsiblePanel value="true" title="testTitle"
			rendered="#{pages$attachment.searchDocument.associatedObjectId != null}"
			var="test2collapsed">
			<f:facet name="header">
				<t:div>
					<h:outputText value="" />
					<t:headerLink immediate="true">

						<h:commandButton styleClass="BUTTON" style="width:auto;"
							value="#{msg['attachment.tab.search.btn']}"
							onclick="javaScript:onProcessStart();"
							rendered="#{test2collapsed}" />
						<h:commandButton styleClass="BUTTON" style="width:auto;"
							value="#{msg['attachment.tab.searchHide.btn']}"
							rendered="#{!test2collapsed}" />
					</t:headerLink>
				</t:div>
			</f:facet>
			<f:facet name="closedContent">
				<h:panelGroup>
				</h:panelGroup>
			</f:facet>
			<%-- Inherited Assets Tab --%>
			<t:panelGrid border="0" width="95%" cellpadding="1" cellspacing="1">
				<t:panelGroup>
					<%--Column 1,2 Starts--%>
					<t:panelGrid columns="4"
						columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%"
						>

						<h:outputText value="#{msg['attachment.tab.lbl.documentId']}" />
						<h:inputText
							value="#{pages$attachment.searchDocument.fileNetDocId}" />

						<h:outputText value="#{msg['attachment.grid.docNameCol']}" />
						<h:inputText
							value="#{pages$attachment.searchDocument.documentFileName}" />

						<h:outputText value="#{msg['commons.createdBy']}" />
						<h:inputText value="#{pages$attachment.searchDocument.createdBy}" />

						<h:outputText value="#{msg['commons.uploadedBy']}" />
						<h:inputText value="#{pages$attachment.searchDocument.uploadedBy}" />


						<h:outputText value="#{msg['inheritanceFileSearch.createdOnTo']}" />
						<rich:calendar
							value="#{pages$attachment.searchDocument.createdOnTo}"
							locale="#{pages$attachment.locale}" popup="true"
							datePattern="dd/MM/yyyy" showApplyButton="false"
							enableManualInput="false" inputStyle="width:170px; height:14px" />

						<h:outputText value="#{msg['inheritanceFileSearch.createdOnTo']}" />
						<rich:calendar
							value="#{pages$attachment.searchDocument.createdOnFrom}"
							locale="#{pages$attachment.locale}" popup="true"
							datePattern="dd/MM/yyyy" showApplyButton="false"
							enableManualInput="false" inputStyle="width:170px; height:14px" />







						<h:outputText value="#{msg['documentSearch.uploadedOnTo']}" />
						<rich:calendar
							value="#{pages$attachment.searchDocument.uploadedOnTo}"
							locale="#{pages$attachment.locale}" popup="true"
							datePattern="dd/MM/yyyy" showApplyButton="false"
							enableManualInput="false" inputStyle="width:170px; height:14px" />

						<h:outputText value="#{msg['documentSearch.uploadedOnFrom']}" />
						<rich:calendar
							value="#{pages$attachment.searchDocument.uploadedOnFrom}"
							locale="#{pages$attachment.locale}" popup="true"
							datePattern="dd/MM/yyyy" showApplyButton="false"
							enableManualInput="false" inputStyle="width:170px; height:14px" />

						<t:outputLabel styleClass="LABEL"
							value="#{msg['attachment.docType']}:"
							style="padding-right:5px;padding-left:5px;margin-right:10px;"></t:outputLabel>
						<t:selectOneMenu 
							value="#{pages$attachment.searchDocument.documentTypeIdStr}" 
							>
							<f:selectItem itemValue="0" itemLabel="#{msg['commons.select']}" />
							<f:selectItems value="#{pages$ApplicationBean.documentTypeList}" />
						</t:selectOneMenu>

					</t:panelGrid>
				</t:panelGroup>
			</t:panelGrid>

			<t:div style="width:95%;">
				<t:div styleClass="A_RIGHT">
					<h:commandButton action="#{pages$attachment.onSearchDocument}"
						onclick="javaScript:onProcessStart();"
						value="#{msg['commons.search']}" styleClass="BUTTON"
						style="width:10%;"></h:commandButton>
					<br />

				</t:div>
			</t:div>

		</t:collapsiblePanel>




		<t:div styleClass="contentDiv" style="width:98%;">
			<t:dataTable id="attachmentTable" renderedIfEmpty="true" width="100%"
				cellpadding="0" cellspacing="0" var="dataItem"
				binding="#{pages$attachment.dataTable}"
				value="#{pages$attachment.allDocs}" preserveDataModel="false"
				preserveSort="false" rowClasses="row1,row2" rules="all"
				rows="#{pages$attachment.paginatorRows}">
				<t:column width="280" sortable="true"
					id="attachmentTabledoccreatedBy">
					<f:facet name="header">
						<h:outputText value="#{msg['commons.createdBy']}" />
					</f:facet>
					<t:outputText value="#{dataItem.createdBy}" styleClass="A_LEFT"
						id="attachmentTabledoccreatedBytxt" />
				</t:column>

				<t:column width="280" sortable="true">
					<f:facet name="header">
						<h:outputText value="#{msg['commons.createdOn']}" />
					</f:facet>
					<t:outputText value="#{dataItem.createdOn}" styleClass="A_LEFT" />
				</t:column>

				<t:column id="attachmentTabledoctype" width="200"
					rendered="#{pages$attachment.isEnglishLocale}" sortable="true">
					<f:facet name="header">
						<t:outputText value="#{msg['attachment.grid.docTypeCol']}"
							id="attachmentTabledoctypehdn" />
					</f:facet>
					<t:outputText value="#{dataItem.docTypeEn}" styleClass="A_LEFT"
						id="attachmentTabledoctypetxt" />
					<t:inputHidden value="#{dataItem.documentPath}"
						id="attachmentTabledoctyphdntxte" />
				</t:column>
				<t:column width="200" rendered="#{pages$attachment.isArabicLocale}"
					sortable="true" id="attachmentTabledoctypear">
					<f:facet name="header">
						<t:outputText value="#{msg['attachment.grid.docTypeCol']}"
							id="attachmentTabledoctypearheading" />
					</f:facet>
					<t:outputText value="#{dataItem.docTypeAr}" styleClass="A_LEFT"
						id="attachmentTabledoctypepathar" />
					<t:inputHidden value="#{dataItem.documentPath}"
						id="attachmentTabledoctypepatharhdn" />
				</t:column>
				<t:column width="200" sortable="true" id="attachmentTabledocname">
					<f:facet name="header">
						<t:outputText value="#{msg['attachment.grid.docNameCol']}"
							id="attachmentTabledocnamehdn" />
					</f:facet>
					<t:outputText value="#{dataItem.documentTitle}" styleClass="A_LEFT"
						id="attachmentTabledocnametxt" />
				</t:column>
				<t:column width="200" sortable="true"
					id="attachmentTabledocnamefiled">
					<f:facet name="header">
						<h:outputText value="#{msg['attachment.grid.fileNameCol']}"
							id="attachmentTablefileNme" />
					</f:facet>
					<t:outputText value="#{dataItem.documentFileName}"
						styleClass="A_LEFT" id="attachmentTablefileNametxt" />
				</t:column>
				<t:column width="280" sortable="true" id="attachmentTabledoccomment">
					<f:facet name="header">
						<h:outputText value="#{msg['attachment.grid.commentsCol']}"
							id="attachmentTabledoccommenthdn" />
					</f:facet>
					<t:outputText value="#{dataItem.comments}" styleClass="A_LEFT"
						id="attachmentTabledoccommenttxt" />
				</t:column>
				<t:column width="280" sortable="false" id="attachmentFileNetDocId">
					<f:facet name="header">

						<h:outputText value="#{msg['attachment.grid.fileNetDocId']}"
							id="attachmentFileNetDocIdLbl" />

					</f:facet>
					<t:outputText value="#{dataItem.fileNetDocId}" styleClass="A_LEFT"
						id="attachmentFileNetDocIdtxt" />
				</t:column>

				<t:column width="120" id="attachmentTabledocaction">
					<f:facet name="header"></f:facet>
					<t:commandLink id="lnkAddAttachment"
						rendered="#{dataItem.isProcDoc && pages$attachment.canAddAttachment}"
						actionListener="#{pages$attachment.openUploadPopup}" value=""
						style="padding-left:1px;padding-right:1px;">
						<h:graphicImage id="fileAddIcon"
							title="#{msg['attachment.grid.attachFileCol']}"
							url="../resources/images/app_icons/addAttachment.png" />
					</t:commandLink>
					<t:commandLink
						rendered="#{dataItem.canDelete || pages$attachment.canAddAttachment}"
						actionListener="#{pages$attachment.removeAttachment}" value=""
						style="padding-left:1px;padding-right:1px;"
						onclick="if (!confirm('#{msg['attachment.messages.confirmRemove']}')) return">
						<h:graphicImage id="fileRemoveIcon"
							title="#{msg['commons.delete']}"
							url="../resources/images/delete.gif" />
					</t:commandLink>
					<t:commandLink id="lnkDownload" rendered="#{dataItem.canDownload}"
						action="#{pages$attachment.download}" value=""
						style="padding-left:1px;padding-right:1px;">
						<h:graphicImage id="fileDownloadIcon"
							title="#{msg['attachment.buttons.download']}"
							url="../resources/images/app_icons/downloadAttachment.png" />
					</t:commandLink>
					<t:commandLink
						rendered="#{empty dataItem.fileNetDocId && pages$attachment.searchDocument.associatedObjectId != null}"
						actionListener="#{pages$attachment.updateToFileNet}" value=""
						style="padding-left:1px;padding-right:1px;">
						<h:graphicImage title="#{msg['attachment.buttons.uploadFileNet']}"
							url="../resources/images/app_icons/addAttachment.png" />
					</t:commandLink>

				</t:column>
				<t:column width="50"
					rendered="#{pages$attachment.openFileProcedure}">
					<f:facet name="header">
						<t:outputText
							value="#{msg['attachment.columnName.toBeProvidedLater']}" />
					</f:facet>
					<h:selectBooleanCheckbox
						rendered="#{dataItem.isProcDoc && !dataItem.canDownload}"
						value="#{dataItem.toBeProvidedLater}"></h:selectBooleanCheckbox>
				</t:column>
			</t:dataTable>
		</t:div>
		<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
			style="width:99.1%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
				cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']}" />
						<h:outputText value=" : " />
						<h:outputText value="#{pages$attachment.recordSize}" />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="attachmentScroller" for="attachmentTable"
						paginator="true" fastStep="1"
						paginatorMaxPages="#{pages$attachment.paginatorMaxPages}"
						immediate="false" paginatorTableClass="paginator"
						renderFacetsIfSinglePage="true"
						paginatorTableStyle="grid_paginator" layout="singleTable"
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" pageIndexVar="pageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
		</t:div>
	</t:panelGrid>
</t:div>