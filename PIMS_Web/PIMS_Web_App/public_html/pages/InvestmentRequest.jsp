<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
		function disableButtons(control)
		{
			
			var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
			
			control.nextSibling.nextSibling.onclick();
			
			return 
		
		}
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{		    			 
		    document.getElementById("detailsFrm:hdnPersonId").value=personId;
		    document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
		    document.getElementById("detailsFrm:hdnPersonName").value=personName;
		    document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
		    document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;		
	       document.forms[0].submit();
	} 
	function populatePaymentDetails()
	{	
	  	document.getElementById("detailsFrm:populatePayments").onclick();	  	 
	}
	function onMessageFromAssociateCostCenter()
	{	
	  	document.getElementById("detailsFrm:onMessageFromAssociateCostCenter").onclick();	  	 
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['mems.investment.label.title']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>

																<h:outputText id="errorMessages"
																	value="#{pages$InvestmentRequest.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText
																	value="#{pages$InvestmentRequest.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="hdnRequestId"
																	value="#{pages$InvestmentRequest.hdnRequestId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$InvestmentRequest.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$InvestmentRequest.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$InvestmentRequest.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$InvestmentRequest.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$InvestmentRequest.hdnIsCompany}"></h:inputHidden>

																<h:commandLink id="onMessageFromAssociateCostCenter"
																	style="width: auto;visibility: hidden;"
																	action="#{pages$InvestmentRequest.onMessageFromAssociateCostCenter}">
																</h:commandLink>
															</td>
														</tr>
													</table>

													<table cellpadding="0" cellspacing="6px" width="100%">
														<tr>
															<td class="BUTTON_TD MARGIN" colspan="6">

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['attachment.file']}"
																	action="#{pages$InvestmentRequest.showInheritanceFilePopup}"
																	rendered="#{pages$InvestmentRequest.isFileAssociated}">
																</h:commandButton>
															</td>
														</tr>


													</table>
													<t:panelGrid cellpadding="1px" width="100%"
														cellspacing="5px"
														rendered="#{pages$InvestmentRequest.isFileAssociated}"
														columns="4">
														<!-- Top Fields - Start -->
														<h:outputLabel styleClass="LABEL"
															value="#{msg['collectionProcedure.fileNumber']} :" />
														<h:inputText styleClass="READONLY" readonly="true"
															binding="#{pages$InvestmentRequest.htmlFileNumber}" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['searchInheritenceFile.fileType']}:" />
														<h:inputText styleClass="READONLY" readonly="true"
															binding="#{pages$InvestmentRequest.htmlFileType}" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['commons.status']}:" />
														<h:inputText styleClass="READONLY" readonly="true"
															binding="#{pages$InvestmentRequest.htmlFileStatus}" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['commons.createdBy']}:" />
														<h:inputText styleClass="READONLY" readonly="true"
															binding="#{pages$InvestmentRequest.htmlFileDate}" />

														<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*" />
															<h:outputLabel id="lblInvType" styleClass="LABEL"
																value="#{msg['mems.investment.label.investmentType']}:"></h:outputLabel>
														</h:panelGroup>
														<h:selectOneMenu id="invtType"
															binding="#{pages$InvestmentRequest.cmbInvestTypes}"
															style="width: 200px;"
															styleClass="#{pages$InvestmentRequest.investmentTypeCss}"
															readonly="#{!pages$InvestmentRequest.isNewStatus}">
															<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																itemValue="-1" />
															<f:selectItems
																value="#{pages$InvestmentRequest.investmentTypes}" />
														</h:selectOneMenu>
														<h:panelGroup>
															<h:outputLabel id="lblTotalAmnt" styleClass="LABEL"
																value="#{msg['collectPayment.TotalAmount']}:"></h:outputLabel>
														</h:panelGroup>
														<h:inputText id="totalAmount" readonly="true"
															styleClass="READONLY"
															binding="#{pages$InvestmentRequest.totalAmount}">
														</h:inputText>

														<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*" />
															<h:outputLabel id="lblDesc" styleClass="LABEL"
																value="#{msg['mems.investment.label.investmentDetails']}:"></h:outputLabel>

														</h:panelGroup>
														<t:panelGroup colspan="3">
															<h:inputTextarea id="invDetails" style="width: 580px;"
																readonly="#{!pages$InvestmentRequest.isNewStatus}"
																styleClass="#{pages$InvestmentRequest.investmentTypeCss}"
																binding="#{pages$InvestmentRequest.txtInvestmentDetails}" />

														</t:panelGroup>


														<!-- Top Fields - End -->
													</t:panelGrid>
													<t:panelGrid id="applicationDetailsTable1"
														cellpadding="5px" width="100%" cellspacing="10px"
														columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
														columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">




														<pims:security
															screen="Pims.MinorMgmt.InvestmentRequest.Approve"
															action="create">
															<h:outputText id="idSendTo" styleClass="LABEL"
																rendered="#{pages$InvestmentRequest.isApprovalRequired || pages$InvestmentRequest.isReviewed}"
																value="#{msg['mems.investment.label.sendTo']}" />


															<h:selectOneMenu id="idUserGrps"
																binding="#{pages$InvestmentRequest.cmbUserGroups}"
																rendered="#{pages$InvestmentRequest.isApprovalRequired || pages$InvestmentRequest.isReviewed}"
																style="width: 200px;">
																<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$InvestmentRequest.allUserGroups}" />
															</h:selectOneMenu>

															<h:outputText id="idRevComments" styleClass="LABEL"
																rendered="#{pages$InvestmentRequest.isApprovalRequired || pages$InvestmentRequest.isReviewed}"
																value="#{msg['commons.comments']}" />
															<h:inputTextarea id="txtReviewCmts" style="width: 300px;"
																rendered="#{pages$InvestmentRequest.isApprovalRequired || pages$InvestmentRequest.isReviewed}"
																binding="#{pages$InvestmentRequest.txtReviewComments}">
															</h:inputTextarea>
														</pims:security>
													</t:panelGrid>

													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$InvestmentRequest.richPanel}"
																style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%">

																<rich:tab id="applicationTab"
																	label="#{msg['commons.tab.applicationDetails']}">
																	<%@ include file="ApplicationDetails.jsp"%>
																</rich:tab>

																<rich:tab id="investTab"
																	label="#{msg['mems.investment.label.investmentDetailsTab']}">
																	<%@ include file="tabInvestmentDetails.jsp"%>
																</rich:tab>

																<rich:tab id="financeTab"
																	label="#{msg['mems.finance.label.tab']}"
																	rendered="#{pages$InvestmentRequest.isApproved}">
																	<%@  include file="tabFinance.jsp"%>
																</rich:tab>

																<rich:tab id="reviewTab"
																	label="#{msg['commons.tab.reviewDetails']}"
																	action="#{pages$InvestmentRequest.getReviewHistory}"
																	rendered="#{!pages$InvestmentRequest.isNewStatus}">
																	<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
																</rich:tab>

																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>

																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>

																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$InvestmentRequest.getRequestHistory}"
																	rendered="#{!pages$InvestmentRequest.isInvalidStatus}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" />
																</td>
															</tr>
														</table>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">

															<pims:security
																screen="Pims.MinorMgmt.InvestmentRequest.Create"
																action="create">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else disableButtons(this);"
																	rendered="#{pages$InvestmentRequest.isNewStatus}">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$InvestmentRequest.save}" />
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSubmit']}')) return false;else disableButtons(this);"
																	rendered="#{pages$InvestmentRequest.isNewStatus}">
																</h:commandButton>
																<h:commandLink id="lnkSubmit"
																	action="#{pages$InvestmentRequest.submitQuery}" />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.InvestmentRequest.Approve"
																action="create">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.approve']}"
																	onclick="if (!confirm('#{msg['mems.common.alertApprove']}')) return false;else disableButtons(this);"
																	rendered="#{pages$InvestmentRequest.isApprovalRequired || pages$InvestmentRequest.isReviewed}">
																</h:commandButton>
																<h:commandLink id="lnkApprove"
																	action="#{pages$InvestmentRequest.approve}" />
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.reject']}"
																	onclick="if (!confirm('#{msg['mems.common.alertReject']}')) return false;else disableButtons(this);"
																	rendered="#{pages$InvestmentRequest.isApprovalRequired || pages$InvestmentRequest.isReviewed}">
																</h:commandButton>
																<h:commandLink id="lnkReject"
																	action="#{pages$InvestmentRequest.reject}" />
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.reviewButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertReview']}')) return false;else disableButtons(this);"
																	rendered="#{pages$InvestmentRequest.isApprovalRequired || pages$InvestmentRequest.isReviewed}">
																</h:commandButton>
																<h:commandLink id="lnkReview"
																	action="#{pages$InvestmentRequest.review}" />
															</pims:security>

															<h:commandButton
																rendered="#{pages$InvestmentRequest.isReviewRequired && pages$InvestmentRequest.isUserInGroup}"
																styleClass="BUTTON" value="#{msg['commons.done']}"
																onclick="if (!confirm('#{msg['mems.common.alertReviewDone']}')) return false;else disableButtons(this);" />

															<h:commandLink id="lnkReviewDone"
																action="#{pages$InvestmentRequest.done}" />
															<pims:security
																screen="Pims.MinorMgmt.InvestmentRequest.Complete"
																action="create">
																<h:commandButton
																	rendered="#{pages$InvestmentRequest.isApproved}"
																	styleClass="BUTTON"
																	onclick="if (!confirm('#{msg['mems.common.alertComplete']}')) return false;else disableButtons(this);"
																	value="#{msg['commons.complete']}">
																</h:commandButton>
																<h:commandLink id="lnkComplete"
																	action="#{pages$InvestmentRequest.complete}" />
															</pims:security>
															<h:commandButton styleClass="BUTTON" id="btnPrint"
																value="#{msg['commons.print']}"
																rendered="#{!pages$InvestmentRequest.isNewStatus}"
																onclick="disableButtons(this);">
															</h:commandButton>
															<h:commandLink id="lnkPrint"
																action="#{pages$InvestmentRequest.onPrint}" />


															<h:commandLink id="populatePayments" styleClass="BUTTON"
																value="#{msg['commons.saveButton']}"
																style="width: auto;visibility: hidden;"
																actionListener="#{pages$InvestmentRequest.loadPaymentDetails}">
															</h:commandLink>
														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			+
		</body>
	</html>
</f:view>