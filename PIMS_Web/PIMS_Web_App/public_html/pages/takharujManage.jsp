<%-- 
  - Author: Anil Verani
  - Date: 07/12/2010
  - Description: Used for Managing Takharuj Process
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	function disableButtons(control)
	{
		var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
		
		control.nextSibling.nextSibling.onclick();
		
	}

	function openTakharujDetailManagePopup()
	{
		var screen_width = 980;
		var screen_height = 480;
        var popup_width = screen_width;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('takharujDetailManage.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
	
	function receiveTakharujInheritedAssetView()
	{
		document.getElementById('frm:lnkReceiveTakharujInheritedAssetView').onclick();
	}
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr>
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table class="greyPanelTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['takharuj.manage.heading']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<td width="100%" height="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION">

											<h:form id="frm" style="width:97%"
												enctype="multipart/form-data">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<a4j:commandLink
																id="lnkReceiveTakharujInheritedAssetView"
																reRender="layoutTable,successMessages,errorMessages"
																action="#{pages$takharujManage.onReceiveTakharujInheritedAssetView}" />
															<h:outputText id="errorMessages" escape="false"
																styleClass="ERROR_FONT"
																value="#{pages$takharujManage.errorMessages}" />
															<h:outputText id="successMessages" escape="false"
																styleClass="INFO_FONT"
																value="#{pages$takharujManage.successMessages}" />
														</td>
													</tr>
												</table>
												<t:div styleClass="BUTTON_TD">
													<h:commandButton styleClass="BUTTON"
														value="#{msg['reportName.fileDetails']}"
														action="#{pages$takharujManage.onOpenFile}"
														style="width: 135px">
													</h:commandButton>
												</t:div>
												<t:div rendered="true" style="width:100%;">
													<t:panelGrid cellpadding="1px" width="100%"
														cellspacing="5px" columns="4">
														<!-- Top Fields - Start -->
														<h:outputLabel styleClass="LABEL"
															value="#{msg['searchInheritenceFile.fileNo']}:" />
														<h:inputText styleClass="READONLY" readonly="true"
															value="#{pages$takharujManage.inheritanceFileView.fileNumber}" />
															
														<h:outputLabel styleClass="LABEL"
															value="#{msg['report.memspaymentreceiptreport.fileOwner']}:" />
														<h:inputText styleClass="READONLY" readonly="true"
															value="#{pages$takharujManage.inheritanceFileView.filePerson}" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['searchInheritenceFile.fileType']}:" />
														<h:inputText styleClass="READONLY" readonly="true"
															value="#{pages$takharujManage.englishLocale ? pages$takharujManage.inheritanceFileView.fileTypeEn : pages$takharujManage.inheritanceFileView.fileTypeAr}" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['commons.status']}:" />
														<h:inputText styleClass="READONLY" readonly="true"
															value="#{pages$takharujManage.englishLocale ? pages$takharujManage.inheritanceFileView.statusEn : pages$takharujManage.inheritanceFileView.statusAr}" />


														<!-- Top Fields - End -->
													</t:panelGrid>

													<t:panelGrid
														rendered="#{pages$takharujManage.isPageModeApprovalRequired}"
														cellpadding="1px" cellspacing="5px" columns="2">
														<h:outputLabel styleClass="LABEL"
															value="#{msg['mems.investment.label.sendTo']}:" />
														<h:selectOneMenu
															value="#{pages$takharujManage.reviewRequestView.groupId}">
															<f:selectItem itemLabel="#{msg['commons.select']}"
																itemValue="" />
															<f:selectItems
																value="#{pages$ApplicationBean.allUserGroups}" />
														</h:selectOneMenu>
														<h:outputLabel styleClass="LABEL"
															value="#{msg['commons.comments']}:" />
														<h:inputTextarea
															value="#{pages$takharujManage.reviewRequestView.rfc}"
															style="width: 600px;" />
													</t:panelGrid>

													<t:panelGrid cellpadding="1px" width="100%"
														cellspacing="5px" columns="4">
														<table border="0" width="100%">
															<tr>
																<td class="BUTTON_TD">
																	<!-- Top Actions - Start -->
																	<!-- Top Actions - End -->
																</td>
															</tr>
														</table>
													</t:panelGrid>
												</t:div>

												<div class="MARGIN" style="width: 98%">

													<table id="table_1" cellpadding="0" cellspacing="0"
														width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>

													<rich:tabPanel
														binding="#{pages$takharujManage.takharujTabPanel}">

														<!-- Application Details Tab - Start -->
														<rich:tab id="applicationDetailsTab"
															label="#{msg['commons.tab.applicationDetails']}">
															<%@ include file="applicationDetailsTab.jsp"%>
														</rich:tab>
														<!-- Application Details Tab - End -->

														<!-- Takharuj Details Tab - Start -->
														<rich:tab id="takharujDetailsTab"
															label="#{msg['takharujDetailsTab.heading']}">
															<t:panelGrid
																rendered="#{pages$takharujManage.isPageModeRequestNew || 
																            pages$takharujManage.isPageModeRequestRejectedResubmit || 
																            pages$takharujManage.isPageModeApprovalRequired}"
																styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px"
																width="50%" cellspacing="5px" columns="3">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['takharujDetailsTab.asset']}:" />
																<h:selectOneMenu
																	value="#{pages$takharujManage.selectedInheritedAssetId}">
																	<f:selectItem itemValue=""
																		itemLabel="#{msg['commons.select']}" />
																	<f:selectItems
																		value="#{pages$takharujManage.inheritedAssetSelectItems}" />
																</h:selectOneMenu>
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.Add']}"
																	action="#{pages$takharujManage.onAddTakharujInheritedAsset}" />
															</t:panelGrid>

															<t:div styleClass="contentDiv" style="width:98%">
																<t:dataTable id="takharujInheritedAssetViewDataTable"
																	rows="#{pages$takharujManage.paginatorRows}"
																	value="#{pages$takharujManage.takharujInheritedAssetViewList}"
																	binding="#{pages$takharujManage.takharujInheritedAssetViewDataTable}"
																	preserveDataModel="false" preserveSort="false"
																	var="dataItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true" width="100%">

																	<t:column width="20%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['takharujDetailsTab.completeTakharuj']}" />
																		</f:facet>
																		<h:selectBooleanCheckbox
																			value="#{dataItem.isCompleteTakharuj}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['searchAssets.assetNumber']}" />
																		</f:facet>
																		<h:commandLink
																			action="#{pages$takharujManage.openManageAsset}"
																			value="#{dataItem.inheritedAssetView.assetMemsView.assetNumber}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['searchAssets.assetType']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$takharujManage.englishLocale ? dataItem.inheritedAssetView.assetMemsView.assetTypeView.assetTypeNameEn : dataItem.inheritedAssetView.assetMemsView.assetTypeView.assetTypeNameAr}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['assetSearch.assetName']}" />
																		</f:facet>
																		<t:outputText
																			value="#{pages$takharujManage.englishLocale ? dataItem.inheritedAssetView.assetMemsView.assetNameEn : dataItem.inheritedAssetView.assetMemsView.assetNameAr}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['searchAssets.assetDesc']}" />
																		</f:facet>
																		<t:outputText
																			value="#{dataItem.inheritedAssetView.assetMemsView.description}"
																			rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>

																	<t:column width="20%" sortable="false">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<a4j:commandLink
																			onclick="if (!confirm('#{msg['takharujInheritedAssetView.confirm.delete']}')) return"
																			action="#{pages$takharujManage.onDeleteTakharujInheritedAsset}"
																			rendered="#{
																			             (
																						  pages$takharujManage.isPageModeRequestNew || 
																			              pages$takharujManage.isPageModeRequestRejectedResubmit || 
																			              pages$takharujManage.isPageModeApprovalRequired
																			             ) && 
																			             dataItem.isDeleted == 0
																			            }"
																			reRender="takharujInheritedAssetViewDataTable,takharujInheritedAssetViewGridInfo">
																			<h:graphicImage title="#{msg['commons.delete']}"
																				url="../resources/images/delete.gif" />
																		</a4j:commandLink>

																		<h:outputText value=" " />

																		<h:commandLink
																			action="#{pages$takharujManage.onDetailTakharujInheritedAsset}"
																			rendered="#{dataItem.isDeleted == 0}">
																			<h:graphicImage
																				title="#{msg['takharujDetailsTab.heading']}"
																				url="../resources/images/detail-icon.gif" />
																		</h:commandLink>

																		<h:commandLink
																			action="#{pages$takharujManage.openTakharujEffect}">
																			<h:graphicImage
																				title="#{msg['takharujDetails.takharujEffects']}"
																				url="../resources/images/magnifier.gif" />
																		</h:commandLink>
																	</t:column>
																</t:dataTable>
															</t:div>

															<t:div id="takharujInheritedAssetViewGridInfo"
																styleClass="contentDivFooter" style="width:99%">
																<t:panelGrid columns="2" cellpadding="0" cellspacing="0"
																	width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD">
																	<t:div styleClass="RECORD_NUM_BG">
																		<t:panelGrid columns="3"
																			columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
																			cellpadding="0" cellspacing="0"
																			style="width:182px;#width:150px;">
																			<h:outputText value="#{msg['commons.recordsFound']}" />
																			<h:outputText value=" : " />
																			<h:outputText
																				value="#{pages$takharujManage.takharujInheritedAssetViewListRecordSize}" />
																		</t:panelGrid>
																	</t:div>

																	<CENTER>
																		<t:dataScroller
																			for="takharujInheritedAssetViewDataTable"
																			paginator="true" fastStep="1"
																			paginatorMaxPages="#{pages$takharujManage.paginatorMaxPages}"
																			immediate="false" paginatorTableClass="paginator"
																			renderFacetsIfSinglePage="true"
																			pageIndexVar="pageNumber" styleClass="SCH_SCROLLER"
																			paginatorActiveColumnStyle="font-weight:bold;"
																			paginatorRenderLinkForActive="false"
																			paginatorTableStyle="grid_paginator"
																			layout="singleTable"
																			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;">

																			<f:facet name="first">
																				<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastrewind">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastforward">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}"></t:graphicImage>
																			</f:facet>

																			<f:facet name="last">
																				<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
																			</f:facet>

																			<t:div styleClass="PAGE_NUM_BG">
																				<t:panelGrid styleClass="PAGE_NUM_BG_TABLE"
																					columns="2" cellpadding="0" cellspacing="0">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</t:panelGrid>
																			</t:div>
																		</t:dataScroller>
																	</CENTER>
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Takharuj Details Tab - End -->

														<!-- Session Details Tab - Start -->
														<rich:tab id="sessionTab"
															action="#{pages$takharujManage.onSessionTabClick}"
															rendered="#{pages$takharujManage.isPageModeApprovalRequired || pages$takharujManage.isPageModeJudgeSessionReq || pages$takharujManage.isPageModeViewOnly}"
															label="#{msg['commons.tab.sessionDetails']}">
															<jsp:include page="sessionDetailsTab.jsp"></jsp:include>
														</rich:tab>
														<!-- Session Details Tab - End -->

														<!-- Review Details Tab - Start -->
														<rich:tab id="reviewTab"
															action="#{pages$takharujManage.onReviewTabClick}"
															rendered="#{pages$takharujManage.isPageModeApprovalRequired || pages$takharujManage.isPageModeReviewRequired || pages$takharujManage.isPageModeViewOnly}"
															label="#{msg['commons.tab.reviewDetails']}">
															<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
														</rich:tab>
														<!-- Review Details Tab - End -->

														<!-- Attachments Tab - Start -->
														<rich:tab id="attachmentTab"
															label="#{msg['commons.attachmentTabHeading']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<!-- Attachments Tab - End -->

														<!-- Comments Tab - Start -->
														<rich:tab id="commentsTab"
															label="#{msg['commons.comments']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														<!-- Comments Tab - End -->

														<!-- History Tab - End -->

														<rich:tab
															label="#{msg['mems.inheritanceFile.tabHeadingShort.history']}"
															action="#{pages$takharujManage.requestHistoryTabClick}">
															<%@ include file="../pages/requestTasks.jsp"%>
														</rich:tab>
														<!-- History Tab - End -->

													</rich:tabPanel>

													<table cellpadding="0" cellspacing="0" style="width: 100%;">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>

													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td colspan="6" class="BUTTON_TD">
																<!-- Actions - Start -->
																<pims:security
																	screen="Pims.MinorMgmt.TakharujManagement.ReviewDone"
																	action="view">
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.done']}"
																		rendered="#{pages$takharujManage.isPageModeReviewRequired}"
																		onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false; else disableButtons(this);" />
																	<h:commandLink id="lnkReviewDone"
																		action="#{pages$takharujManage.onReviewDone}" />
																</pims:security>

																<pims:security
																	screen="Pims.MinorMgmt.TakharujManagement.Review"
																	action="view">
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.reviewButton']}"
																		rendered="#{pages$takharujManage.isPageModeApprovalRequired}"
																		onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false; else disableButtons(this);" />
																	<h:commandLink id="lnkReview"
																		action="#{pages$takharujManage.onReview}" />
																</pims:security>
																<pims:security
																	screen="Pims.MinorMgmt.TakharujManagement.JudgeDone"
																	action="view">
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.done']}"
																		rendered="#{pages$takharujManage.isPageModeJudgeSessionReq}"
																		onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false; else disableButtons(this);" />
																	<h:commandLink id="lnkJudgeDone"
																		action="#{pages$takharujManage.onJudgeDone}" />
																</pims:security>
																<pims:security
																	screen="Pims.MinorMgmt.TakharujManagement.Judge"
																	action="view">
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['mems.payment.request.label.btn.judge']}"
																		rendered="#{pages$takharujManage.isPageModeApprovalRequired}"
																		onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false; else disableButtons(this);" />
																	<h:commandLink id="lnkJudge"
																		action="#{pages$takharujManage.onJudge}" />
																</pims:security>

																<pims:security
																	screen="Pims.MinorMgmt.TakharujManagement.Reject"
																	action="view">
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.reject']}"
																		rendered="#{pages$takharujManage.isPageModeApprovalRequired}"
																		onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false; else disableButtons(this);" />
																	<h:commandLink id="lnkReject"
																		action="#{pages$takharujManage.onReject}" />
																</pims:security>

																<pims:security
																	screen="Pims.MinorMgmt.TakharujManagement.Approve"
																	action="view">
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.complete']}"
																		action="#{pages$takharujManage.onComplete}"
																		rendered="#{pages$takharujManage.isPageModeApproved}"
																		onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false; else disableButtons(this);" />
																	<h:commandLink id="lnkComplete"
																		action="#{pages$takharujManage.onComplete}" />
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.approve']}"
																		action="#{pages$takharujManage.onApprove}"
																		rendered="#{pages$takharujManage.isPageModeApprovalRequired}"
																		onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false; else disableButtons(this);" />
																	<h:commandLink id="lnkApprove"
																		action="#{pages$takharujManage.onApprove}" />
																</pims:security>

																<pims:security
																	screen="Pims.MinorMgmt.TakharujManagement.Send"
																	action="view">
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['socialResearch.btn.resubmit']}"
																		rendered="#{pages$takharujManage.isPageModeRequestRejectedResubmit && 
																		            ! empty pages$takharujManage.requestView.requestId
																		            }"
																		onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false; else disableButtons(this);" />
																	<h:commandLink id="lnkReSubmit"
																		action="#{pages$takharujManage.onResubmit}" />

																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.cancel']}"
																		rendered="#{pages$takharujManage.isPageModeRequestRejectedResubmit && 
																		            ! empty pages$takharujManage.requestView.requestId
																		            }"
																		onclick="if (!confirm('#{msg['commons.messages.confirmCancelRequest']}')) return false; else disableButtons(this);" />
																	<h:commandLink id="lnkCancell"
																		action="#{pages$takharujManage.onCancelled}" />

																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.send']}"
																		action="#{pages$takharujManage.onSend}"
																		rendered="#{pages$takharujManage.isPageModeRequestNew  && 
																		            ! empty pages$takharujManage.requestView.requestId}"
																		onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false; else disableButtons(this);" />
																	<h:commandLink id="lnkSend"
																		action="#{pages$takharujManage.onSend}" />
																</pims:security>
																<pims:security
																	screen="Pims.MinorMgmt.TakharujManagement.Save"
																	action="view">

																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.saveButton']}"
																		rendered="#{pages$takharujManage.isPageModeRequestNew ||
																		            pages$takharujManage.isPageModeRequestRejectedResubmit || 
																				    pages$takharujManage.isPageModeApprovalRequired}"
																		onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false; else disableButtons(this);" />
																	<h:commandLink id="lnkSave"
																		action="#{pages$takharujManage.onSave}" />

																</pims:security>
																<h:commandButton styleClass="BUTTON" id="btnPrint"
																	value="#{msg['commons.print']}"
																	rendered="#{!pages$takharujManage.isPageModeRequestNew}"
																	onclick="disableButtons(this);">
																</h:commandButton>
																<h:commandLink id="lnkPrint"
																	action="#{pages$takharujManage.onPrint}" />

																<!-- Actions - End -->
															</td>
														</tr>
													</table>
												</div>

											</h:form>
										</div>



									</td>
								</tr>
							</table>

						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>