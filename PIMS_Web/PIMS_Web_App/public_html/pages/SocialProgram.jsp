


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
		function validate() 
		{
		   maxlength=500;
		   if(document.getElementById("formRequest:txt_remarks").value.length>=maxlength) 
		   {
		     document.getElementById("formRequest:txt_remarks").value=
		     document.getElementById("formRequest:txt_remarks").value.substr(0,maxlength);  
		   }
		}
		function onSearchMemberPopup()
		{
	   		var screen_width = screen.width;
		   	var screen_height = screen.height;
		   	var screen_top = screen.width/4;
		   	var screen_left = screen.height/4;
		   	window.open('SearchMemberPopup.jsf?singleselectmode=singleselectmode','_blank','width='+(screen_width-280)+',height='+(screen_height-380)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	    }
		function onParticipantChkChangeStart( changedChkBox )
		{
	        document.getElementById('formRequest:onClickMarkUnMark').onclick();
			document.getElementById('formRequest:disablingDiv').style.display='block';
		}
		function onParticipantTypeSelectionChanged(changedChkBox)
	    {
	        document.getElementById('formRequest:disablingDiv').style.display='block';
		    document.getElementById('formRequest:onParticipantTypeChange').onclick();
	    }
	    function onDeleted(control)
	    {
	        document.getElementById('formRequest:disablingDiv').style.display='block';
		    control.nextSibling.onclick();
	    }
	    function openParticipantWinnerCommentsPopup()
	    {
			var screen_width = 1024;
	       	var screen_height = 350;
	       	var popup_width = screen_width-128;
	       	var popup_height = screen_height;
	       	var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
	       	window.open('programParticipantsWinner.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=40,resizable=yes,scrollbars=yes,status=yes');

	    }
	    
	    function onMessageFromParticipantWinnerPopup()
	    {
	        document.getElementById('formRequest:onParticipantWinner').onclick();
	    }
	    function onCalculateEndDate()
	    {
	    
	        document.getElementById('formRequest:onCalculateEndDate').onclick();
	        document.getElementById('formRequest:disablingDiv').style.display='block';
	    }
		function  onRemoveDisablingDiv ()
		{
			document.getElementById('formRequest:disablingDiv').style.display='none';
		}
		function showPersonPopupForParticipants()
	    {
	     var screen_width = 1024;
	     var screen_height = 470;
	      
	     window.open('SearchPerson.jsf?viewMode=popup&selectMode=many','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    }
	    function showPersonPopupForSponsor()
	    {
	     var screen_width = 1024;
	     var screen_height = 470;
	     window.open('SearchPerson.jsf?persontype=SPONSOR&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    }
		function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	    {
		  if(hdnPersonType=='SPONSOR')
	      {
		    document.getElementById('formRequest:txtSponsorName').value=personName;
		    document.getElementById('formRequest:hdnSelectedSponsorId').value=personId;
		  }
	    }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />


	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />


		</head>

		<%-- Header --%>
		<body class="BODY_STYLE">
			<div class="containerDiv">

				<table width="99%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr>
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" height="84%" valign="top"
							class="divBackgroundBody">
							<h:form id="formRequest" style="height:95%;width:99.7%;"
								enctype="multipart/form-data">
								<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>

								<table class="greyPanelTable" cellpadding="0" cellspacing="0"
									border="0">
									<tr>
										<td class="HEADER_TD" style="width: 70%;">
											<h:outputLabel value="#{pages$SocialProgram.pageTitle}"
												styleClass="HEADER_FONT" />
										</td>
									</tr>
								</table>

								<table height="95%" width="99%" class="greyPanelMiddleTable"
									cellpadding="0" cellspacing="0" border="0">
									<tr valign="top">
										<td height="100%" valign="top" nowrap="nowrap"
											background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
											width="1">
										</td>
										<td height="100%" valign="top" nowrap="nowrap">
											<div class="SCROLLABLE_SECTION">
												<t:panelGrid id="hiddenFields" border="0"
													styleClass="layoutTable" width="94%"
													style="margin-left:15px;margin-right:15px;" columns="1">
													<h:outputText id="successmsg" escape="false"
														styleClass="INFO_FONT"
														value="#{pages$SocialProgram.successMessages}" />
													<h:outputText id="errormsg" escape="false"
														styleClass="ERROR_FONT"
														value="#{pages$SocialProgram.errorMessages}" />
													<a4j:commandLink id="onCalculateEndDate"
														oncomplete="onRemoveDisablingDiv ();"
														action="#{pages$SocialProgramBasicInfo.onCalculateEndDate}"
														reRender="endDate,hiddenFields,errormsg" />
												</t:panelGrid>


												<rich:tabPanel id="tabPanel"
													binding="#{pages$SocialProgram.tabPanel}"
													style="width:80%;height:320px;" headerSpacing="0">


													<%-- Basic Info Tab - Start --%>
													<rich:tab id="basicInfo"
														label="#{msg['socialProgram.tab.basicInfo']}"
														title="#{msg['socialProgram.tab.basicInfo']}">
														<%@ include file="../pages/programBasicInfo.jsp"%>
													</rich:tab>
													<%-- Basic Info Tab - Finish --%>

													<%-- Participant Tab - Start --%>
													<rich:tab id="participantTab"
														label="#{msg['socialProgram.tab.Participants']}"
														title="#{msg['socialProgram.tab.Participants']}"
														action="#{pages$SocialProgram.onProgramParticipantTabClick}">
														<%@ include file="../pages/programParticipants.jsp"%>
													</rich:tab>
													<%-- Participant Tab - Finish --%>
													<%-- Purpose Tab - Start --%>
													<rich:tab id="purpose"
														label="#{msg['socialProgram.tab.Purpose']}"
														title="#{msg['socialProgram.tab.Purpose']}"
														action="#{pages$SocialProgram.onProgramPurposeTabClick}">
														<%@ include file="../pages/programPurposes.jsp"%>

													</rich:tab>
													<%-- Purpose Tab - Finish --%>

													<%-- Program Sponsors - Start --%>
													<rich:tab id="sponsorsTab"
														label="#{msg['socialProgram.tab.programSponsors']}"
														title="#{msg['socialProgram.tab.programSponsors']}"
														action="#{pages$SocialProgram.onProgramSponsorsTabClick}">
														<%@ include file="../pages/programSponsors.jsp"%>

													</rich:tab>
													<%-- Program Sponsors - Finish --%>

													<%-- Action Tab - Start --%>
													<rich:tab id="actionList"
														label="#{msg['socialProgram.tab.ActionList']}"
														title="#{msg['socialProgram.tab.ActionList']}"
														action="#{pages$SocialProgram.onProgramActionTabClick}">
														<%@ include file="../pages/programActionList.jsp"%>

													</rich:tab>
													<%-- Action Tab - Finish --%>

													<%-- Requirement Tab - Start --%>
													<rich:tab id="programReq"
														label="#{msg['socialProgram.tab.Requirement']}"
														title="#{msg['socialProgram.tab.Requirement']}"
														action="#{pages$SocialProgram.onProgramRequirementTabClick}">
														<%@ include file="../pages/programRequirement.jsp"%>

													</rich:tab>
													<%-- Requirement Tab - Finish --%>

													<%-- BUDGET Tab - Start --%>
													<rich:tab id="budget"
														label="#{msg['socialProgram.tab.Budget']}"
														title="#{msg['socialProgram.tab.Budget']}"
														action="#{pages$SocialProgram.onProgramBudgetTabClick}">
														<%@ include file="../pages/programBudget.jsp"%>

													</rich:tab>
													<%-- BUDGET Tab - Finish --%>
													<%-- Positive Negative Tab - Start --%>
													<rich:tab id="programPosNeg"
														label="#{msg['socialProgram.tab.positiveNegatives']}"
														title="#{msg['socialProgram.tab.positiveNegatives']}"
														action="#{pages$SocialProgram.onProgramPositiveNegativesTabClick}">
														<%@ include file="../pages/programPositiveNegatives.jsp"%>

													</rich:tab>
													<%-- Positive Negative Tab - Finish --%>
													<%-- Recommndation Tab - Start --%>
													<rich:tab id="programRecommendationsTab"
														label="#{msg['socialProgram.tab.Recommendation']}"
														title="#{msg['socialProgram.tab.Recommendation']}"
														action="#{pages$SocialProgram.onProgramRecommendationsTabClick}">
														<%@ include file="../pages/programRecommendations.jsp"%>

													</rich:tab>
													<%-- Recommndation Tab - Finish --%>

													<rich:tab id="attachmentTab"
														title="#{msg['commons.attachmentTabHeading']}"
														label="#{msg['mems.inheritanceFile.tabHeadingShort.attch']}"
														action="#{pages$SocialProgram.tabAttachmentsComments_Click}">

														<%@  include file="attachment/attachment.jsp"%>
													</rich:tab>

													<rich:tab id="commentsTab"
														title="#{msg['commons.commentsTabHeading']}"
														label="#{msg['commons.commentsTabHeading']}">
														<%@ include file="notes/notes.jsp"%>
													</rich:tab>

													<rich:tab
														label="#{msg['mems.inheritanceFile.tabHeadingShort.history']}"
														action="#{pages$SocialProgram.requestHistoryTabClick}">
														<%@ include file="../pages/requestTasks.jsp"%>
													</rich:tab>


												</rich:tabPanel>

												<table cellpadding="1px" cellspacing="3px" width="95%">
													<tr>

														<td colspan="12" class="BUTTON_TD">

															<pims:security screen="Pims.MinorMgmt.SocialProgram.Save"
																action="view">
																<h:commandButton type="submit" styleClass="BUTTON"
																
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false"
																	action="#{pages$SocialProgram.onSave}"
																	binding="#{pages$SocialProgram.btnSave}"
																	value="#{msg['commons.saveButton']}" />
															</pims:security>
															<pims:security screen="Pims.MinorMgmt.SocialProgram.Save"
																action="view">
																<h:commandButton type="submit" styleClass="BUTTON"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false"
																	action="#{pages$SocialProgram.onSubmit}"
																	binding="#{pages$SocialProgram.btnSubmit}"
																	value="#{msg['commons.submitButton']}" />
															</pims:security>
															<pims:security
																screen="Pims.MinorMgmt.SocialProgram.Approve"
																action="view">

																<h:commandButton type="submit" styleClass="BUTTON"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false"
																	action="#{pages$SocialProgram.onApprove}"
																	binding="#{pages$SocialProgram.btnApprove}"
																	value="#{msg['commons.approve']}" />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.SocialProgram.Reject"
																action="view">
																<h:commandButton type="submit" styleClass="BUTTON"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false"
																	action="#{pages$SocialProgram.onReject}"
																	binding="#{pages$SocialProgram.btnReject}"
																	value="#{msg['commons.reject']}" />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.SocialProgram.Complete"
																action="view">

																<h:commandButton type="submit" styleClass="BUTTON"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false"
																	action="#{pages$SocialProgram.onComplete}"
																	binding="#{pages$SocialProgram.btnComplete}"
																	value="#{msg['socialProgram.btn.Complete']}" />
															</pims:security>


														</td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
								</table>

							</h:form>
						</td>
					</tr>
					<tr>
						<td colspan="2">

							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>

