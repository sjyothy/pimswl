
<t:div id="tabDDetails" style="width:100%;">
	<t:panelGrid id="ddTab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="payModeField" styleClass="LABEL"
				value="#{msg['commons.Name']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="selectOneBlockFor"
			value="#{pages$blockingRequest.selectOneBlockFor}">

			<f:selectItems value="#{pages$blockingRequest.blockForList}" />
			<a4j:support event="onchange"
				reRender="accountBalance,blockinBalance,availableBalance"
				action="#{pages$blockingRequest.onBlockForChange}" />
		</h:selectOneMenu>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="ReasonField" styleClass="LABEL"
				value="#{msg['commons.reason']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="selectOneReason"
			value="#{pages$blockingRequest.selectOneReason}">

			<f:selectItems value="#{pages$ApplicationBean.blockingReasons}" />
		</h:selectOneMenu>


		<h:outputLabel id="labelBalance" styleClass="LABEL"
			value="#{msg['mems.normaldisb.label.balance']}:">
		</h:outputLabel>
		<h:inputText id="accountBalance" readonly="true" styleClass="READONLY"
			binding="#{pages$blockingRequest.accountBalance}">
			
		</h:inputText>

		<h:outputLabel id="lblBlockBalance" styleClass="LABEL"
			value="#{msg['blocking.lbl.blockedamount']}"></h:outputLabel>
		<h:panelGroup>
			<h:inputText id="blockinBalance" styleClass="READONLY"
				readonly="true" binding="#{pages$blockingRequest.blockingBalance}"
				maxlength="20">
			
			</h:inputText>
			<h:commandLink id="lnkBlockingDetails"
				action="#{pages$blockingRequest.onOpenBlockingDetailsPopup}">
				<h:graphicImage id="openBlockingDetailsPopup"
					style="margin-right:5px;"
					title="#{msg['blocking.lbl.blockingDetails']}"
					url="../resources/images/app_icons/manage_tender.png" />
			</h:commandLink>
		</h:panelGroup>

		<h:outputLabel id="labelAVBalance" styleClass="LABEL"
			value="#{msg['commons.availableBalance']}:">
		</h:outputLabel>
		<h:inputText id="availableBalance" readonly="true"
			styleClass="READONLY"
			binding="#{pages$blockingRequest.availableBalance}">
			
		</h:inputText>


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="amnt" styleClass="LABEL"
				value="#{msg['blocking.lbl.amount']}:"></h:outputLabel>
		</h:panelGroup>
		<h:inputText id="txtAmount"
			value="#{pages$blockingRequest.blocking.amount}"
			onkeyup="removeNonNumeric(this)" onkeypress="removeNonNumeric(this)"
			onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
			onchange="removeNonNumeric(this)">
			
		</h:inputText>



		<h:outputLabel id="lblDescription" styleClass="LABEL"
			style="width: 25%" value="#{msg['commons.description']}" />
		<t:panelGroup colspan="3">
			<h:inputTextarea id="txtDescription"
				value="#{pages$blockingRequest.blocking.description}"
				style="width: 560px; height: 70px;"
				onkeyup="removeExtraCharacter(this,250)"
				onkeypress="removeExtraCharacter(this,250)"
				onkeydown="removeExtraCharacter(this,250)"
				onblur="removeExtraCharacter(this,250)"
				onchange="removeExtraCharacter(this,250)" />
		</t:panelGroup>
	</t:panelGrid>

	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;">
		<h:commandButton styleClass="BUTTON" value="#{msg['commons.Add']}"
			onclick="performTabClick('lnkAdd');">
			<h:commandLink id="lnkAdd" action="#{pages$blockingRequest.onAdd}" />
		</h:commandButton>

	</t:div>

	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTablePayments" rows="15" width="100%"
			value="#{pages$blockingRequest.blockingList}"
			binding="#{pages$blockingRequest.dataTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="check" sortable="true"
				rendered="#{pages$blockingRequest.showUnblockButton}">
				<f:facet name="header">
					<t:outputText id="chkHeading" />
				</f:facet>
				<h:selectBooleanCheckbox id="select"
					rendered="#{dataItem.showCheckBox}" value="#{dataItem.selected}" />

			</t:column>
			<t:column id="refNumcolblocking" sortable="true">
				<f:facet name="header">
					<t:outputText id="refNumHeadingblocking"
						value="#{msg['commons.refNum']}" />
				</f:facet>
				<t:outputText id="refNumblocking" styleClass="A_LEFT"
					value="#{dataItem.refNum}" />
			</t:column>
			<t:column id="fullNamecol" sortable="true">
				<f:facet name="header">
					<t:outputText id="fullNameHeading" value="#{msg['commons.Name']}" />
				</f:facet>
				<t:outputText id="fullName" styleClass="A_LEFT"
					value="#{dataItem.person.fullName}" />
			</t:column>

			<t:column id="statusCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="statusAmnt" value="#{msg['commons.status']}" />
				</f:facet>
				<t:outputText id="status" styleClass="A_LEFT"
					value="#{pages$blockingRequest.englishLocale? dataItem.status.dataDescEn:dataItem.status.dataDescAr}" />
			</t:column>


			<t:column id="REASONCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="REASONOTT" value="#{msg['commons.reason']}" />
				</f:facet>
				<t:outputText id="REASONTT" styleClass="A_LEFT"
					value="#{pages$blockingRequest.englishLocale? dataItem.reason.reasonEn:dataItem.reason.reasonAr}" />
			</t:column>

			<t:column id="UNREASONCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="UNREASONOTT"
						value="#{msg['unblocking.lbl.unblockReason']}" />
				</f:facet>
				<t:outputText id="UNREASONTT" styleClass="A_LEFT"
					value="#{pages$blockingRequest.englishLocale? dataItem.unblockReason.reasonEn:dataItem.unblockReason.reasonAr}" />
			</t:column>
			<t:column id="desc" sortable="true" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText id="hdTo" value="#{msg['commons.description']}" />
				</f:facet>
				<div style="width: 70%; white-space: normal;">
					<t:outputText id="p_w" styleClass="A_LEFT"
						style="white-space: normal;" value="#{dataItem.description}" />
				</div>
			</t:column>

			<t:column id="amntCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAmnt" value="#{msg['blocking.lbl.amount']}" />
				</f:facet>
				<t:outputText id="amountt" styleClass="A_LEFT"
					value="#{dataItem.amountStr}" >
					<f:convertNumber  maxIntegerDigits="15" maxFractionDigits="2"
				pattern="##############0.00" />
					</t:outputText>
			</t:column>

			<t:column id="action" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAction" value="#{msg['commons.action']}" />
				</f:facet>
				<h:commandLink id="lnkEdit" onclick=" disableInputs();"
					rendered="#{pages$blockingRequest.showSaveButton || pages$blockingRequest.showApproveButton}"
					action="#{pages$blockingRequest.onEdit}">
					<h:graphicImage id="editIcon" title="#{msg['commons.edit']}"
						url="../resources/images/edit-icon.gif" width="18px;" />
				</h:commandLink>

				<h:commandLink id="lnkDelete"
					onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableInputs();"
					rendered="#{pages$blockingRequest.showSaveButton || pages$blockingRequest.showApproveButton}"
					action="#{pages$blockingRequest.onDelete}">
					<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
						url="../resources/images/delete.gif" width="18px;" />
				</h:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>
</t:div>


