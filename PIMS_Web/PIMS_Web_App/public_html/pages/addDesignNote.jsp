<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

    function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

</head>
	<body class="BODY_STYLE" >
	<div class="containerDiv">
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<td width="83%" height="200px" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['designNote.header']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
									<div class="SCROLLABLE_SECTION"
										style="height: 350px; width: 900px; #height: 350px; #width: 900px;">
										<h:form id="searchFrm" style="WIDTH: 98%;#WIDTH: 100%;">
											<t:div styleClass="MESSAGE">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$addDesignNote.infoMessage}"
																escape="false" styleClass="INFO_FONT" />
															<h:outputText
																value="#{pages$addDesignNote.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</t:div>
											<div class="MARGIN">
													<table cellpadding="1px" cellspacing="2px"  style="WIDTH: 98%;#WIDTH: 100%;">
														<tr>
															<td class="DET_SEC_TD">
															<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
																<h:outputLabel styleClass="LABEL" value="#{msg['commons.date']}: "></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<rich:calendar id="noteDate"
																	value="#{pages$addDesignNote.designNoteView.noteDate}"
																	locale="#{pages$addDesignNote.currentLocale}" popup="true"
																	datePattern="#{pages$addDesignNote.shortDateFormat}"
																	showApplyButton="false" enableManualInput="false"
																	cellWidth="24px" cellHeight="22px" />
															</td>
															<td class="DET_SEC_TD">
															<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
																<h:outputLabel styleClass="LABEL" value="#{msg['commons.status']}: "></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<h:selectOneMenu id="selectOneDesignNoteStatus" 
																	value="#{pages$addDesignNote.designNoteStatus}"
																	styleClass="SELECT_MENU" tabindex="1">
																	<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.projectDesignNoteStatus}" />
																</h:selectOneMenu>
															</td>
														</tr>
														<tr>
															<td class="DET_SEC_TD">
																<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
																<h:outputLabel styleClass="LABEL" value="#{msg['commons.typeCol']}: "></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<h:selectOneMenu id="selectOneNoteType"
																	value="#{pages$addDesignNote.designNoteType}"
																	styleClass="SELECT_MENU" tabindex="2">
																	<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.projectDesignNoteType}" />
																</h:selectOneMenu>
															</td>
															<td class="DET_SEC_TD">
															<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
																<h:outputLabel styleClass="LABEL" value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.Owner']}: "></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<h:selectOneMenu id="selectOneOwner"
																	value="#{pages$addDesignNote.designNoteOwner}"
																	styleClass="SELECT_MENU" tabindex="3">
																	<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
																		itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.projectDesignNoteOwner}" />
																</h:selectOneMenu>
															</td>
														</tr>
														
														<tr>
															<td class="DET_SEC_TD">
															<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
																<h:outputLabel styleClass="LABEL" value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.Note']}: "></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<h:inputTextarea id="htmlInputAreaNote" tabindex="4"
																value="#{pages$addDesignNote.designNoteView.note}"
																 styleClass="INPUT_TEXT_AREA">
																</h:inputTextarea>
															</td>
															<td class="DET_SEC_TD">
																<h:outputLabel styleClass="LABEL" value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.Recommendation']}: "></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<h:inputTextarea id="htmlInputAreaRecommendation" tabindex="5"
																value="#{pages$addDesignNote.designNoteView.recommendation}"
																 styleClass="INPUT_TEXT_AREA">
																</h:inputTextarea>
															</td>
														</tr>
														
														<tr>
															<td class="DET_SEC_TD">
															<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
																<h:outputLabel styleClass="LABEL" value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.closingDate']}: "></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<rich:calendar id="closingDate"
																	value="#{pages$addDesignNote.designNoteView.closingDate}"
																	locale="#{pages$addDesignNote.currentLocale}" popup="true"
																	datePattern="#{pages$addDesignNote.shortDateFormat}"
																	showApplyButton="false" enableManualInput="false"
																	cellWidth="24px" cellHeight="22px" />
															</td>
															<td class="DET_SEC_TD">
															<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
																<h:outputLabel styleClass="LABEL" value="#{msg['approveProjectDesign.tabs.designNotesTab.grid.closingNote']}: "></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<h:inputTextarea id="htmlInputAreaClosingNote" tabindex="6"
																 value="#{pages$addDesignNote.designNoteView.closingNote}"
																 styleClass="INPUT_TEXT_AREA">
																</h:inputTextarea>
																	
															</td>
														</tr>

														<tr>
															<td class="BUTTON_TD" colspan="4">
																<table cellpadding="1px" cellspacing="1px" width="90%">
																	<tr>
																		<td colspan="4">
																				<h:commandButton styleClass="BUTTON"																				
																					value="#{msg['commons.saveButton']}"
																					action="#{pages$addDesignNote.persistDesignNote}" tabindex="7">
																				</h:commandButton>
																				<h:commandButton styleClass="BUTTON"
																					onclick="if (confirm('#{msg['commons.messages.cancelConfirm']}')) closeWindowSubmit();"																					
																					value="#{msg['commons.closeButton']}"
																					tabindex="8">
																				</h:commandButton>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</div>
										  </div>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
</html>
</f:view>