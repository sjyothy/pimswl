<%@ page import="javax.faces.component.UIViewRoot;"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="javascript" type="text/javascript">
	function showUploadPopup() {
	      var screen_width = screen.width;
	      var screen_height = screen.height;
	      var popup_width = screen_width/3+150;
	      var popup_height = screen_height/3-10;
	      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
	      
	      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	      popup.focus();
	}

	function showAddNotePopup() {
	      var screen_width = screen.width;
	      var screen_height = screen.height;
	      var popup_width = screen_width/3+120;
	      var popup_height = screen_height/3-80;
	      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
	      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	      popup.focus();
	}
	
	
		function resetState() {
		document.getElementById('frmSearchContractor:selectState').selectedIndex=0;
		document.getElementById('frmSearchContractor:selectCity').selectedIndex=0;
	}	
	
	
	function resetCities() {
		document.getElementById('frmSearchContractor:selectCity').selectedIndex=0;
	}
	
	function resetStateCR() {
		document.getElementById('frmSearchContractor:selectStateCR').selectedIndex=0;
		document.getElementById('frmSearchContractor:selectCityCR').selectedIndex=0;
	}	
	
	
	function resetCitiesCR() {
		document.getElementById('frmSearchContractor:selectCityCR').selectedIndex=0;
	}
	
	
	 function closeWindow() {
					window.close();
				}
	
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />				
				
		</head>

		<body class="BODY_STYLE">
		<c:choose>
		  <c:when test="${!pages$contractorDetails.isViewModePopUp}">
		   <div class="containerDiv">
		  </c:when>
		 </c:choose>  
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
						<c:when test="${!pages$contractorDetails.isViewModePopUp}">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</c:when>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!pages$contractorDetails.isViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>

					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['contractAdd.contractorDetails']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="3" />
								<td width="100%" height="450px" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION">
										<h:form id="frmSearchContractor" style="width:97%">
											<div >
												<table border="0" class="layoutTable">
													<tr>
														<td colspan="6">
															<h:outputText value="#{pages$contractorDetails.errorMessages}" escape="false" styleClass="ERROR_FONT" style="padding:10px;" />
															<h:outputText value="#{pages$contractorDetails.infoMessages}" escape="false" styleClass="INFO_FONT" style="padding:10px;" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN">
												<rich:tabPanel style="width:98%; height:240px;">

													<!--  Contractor Details - Tab (Start) -->
													<rich:tab id="contractorDetailsTab"
														label="#{msg['contractAdd.contractorDetails']}">
														<t:panelGrid columns="4" cellspacing="10px">
															
															<h:outputLabel styleClass="LABEL"
																value="#{msg['ContractorSearch.GRPNumber']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																readonly="#{pages$contractorDetails.isViewMode}"
																id="txtGrpNumber" style="width:195px;"
																value="#{pages$contractorDetails.contractorView.grpNumber}" />

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$contractorDetails.isViewMode}"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['applicationDetails.status']}:" />
															</h:panelGroup>
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																rendered="#{pages$contractorDetails.isViewMode && pages$contractorDetails.isEnglishLocale}"
																readonly="#{pages$contractorDetails.isViewMode}"
																id="txtconrtactorStatusEn" style="width:195px;"
																value="#{pages$contractorDetails.contractorView.contractorStatusEn}" />																
													    													    	
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																rendered="#{pages$contractorDetails.isViewMode && pages$contractorDetails.isArabicLocale}"
																readonly="#{pages$contractorDetails.isViewMode}"
																id="txtconrtactorStatusAr" style="width:195px;"
																value="#{pages$contractorDetails.contractorView.contractorStatusAr}" />													    	
													    	
													      	<h:selectOneMenu id="selectStatus"
																style="width: 200px"
																rendered="#{!pages$contractorDetails.isViewMode}"
																value="#{pages$contractorDetails.contractorView.statusId}">
																<f:selectItem id="selectAllStatus" itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$ApplicationBean.contractorStatus}" />
															</h:selectOneMenu> <%-- --%>
															

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$contractorDetails.isViewMode}"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.contractorNameEn']}:" />
															</h:panelGroup>
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" maxlength="100"
																readonly="#{pages$contractorDetails.isViewMode}"
																id="txtContractorNameEn" style="width:195px;"
																value="#{pages$contractorDetails.contractorView.contractorNameEn}" />

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$contractorDetails.isViewMode}"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.contractorNameAr']}:" />
															</h:panelGroup>
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" maxlength="100"
																readonly="#{pages$contractorDetails.isViewMode}" 
																id="txtContractorNameAr" style="width:195px;"
																value="#{pages$contractorDetails.contractorView.contractorNameAr}" />

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$contractorDetails.isViewMode}"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.commercialName']}:" />
															</h:panelGroup>
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" maxlength="100"
																readonly="#{pages$contractorDetails.isViewMode}"
																id="txtCommercialName" style="width:195px;"
																value="#{pages$contractorDetails.contractorView.commercialName}" />

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$contractorDetails.isViewMode}"/>
																<h:outputLabel styleClass="LABEL"
																value="#{msg['ContractorSearch.contractorType']}:" />
															</h:panelGroup>
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																rendered="#{pages$contractorDetails.isViewMode}"
																readonly="#{pages$contractorDetails.isViewMode}"
																id="txtconrtactorType" style="width:195px;"
																value="#{pages$contractorDetails.isEnglishLocale?  pages$contractorDetails.contractorView.contractorTypeEn : pages$contractorDetails.contractorView.contractorTypeAr}" />																	
															
															<h:selectOneMenu id="selectContractorType"
																style="width: 200px"
																binding="#{pages$addContractor.cmbContractorType}"
																rendered="#{!pages$contractorDetails.isViewMode}"
																value="#{pages$contractorDetails.contractorView.contractorTypeId}">
																<f:selectItem id="selectAllType" itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$ApplicationBean.contractorTypeList}" />
															</h:selectOneMenu>

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$contractorDetails.isViewMode}"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.licenseNumber']}:" />
															</h:panelGroup>
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" maxlength="50"
																readonly="#{pages$contractorDetails.isViewMode}"
																id="txtLicenseNumber" style="width:195px;"
																value="#{pages$contractorDetails.contractorView.licenseNumber}" />

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$contractorDetails.isViewMode}"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.licenseSource']}:" />
															</h:panelGroup>
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																rendered="#{pages$contractorDetails.isViewMode}"
																readonly="#{pages$contractorDetails.isViewMode}"
																id="txtLicenseSource" style="width:195px;"
																value="#{pages$contractorDetails.contractorView.licenseSource}" />																	
															<h:selectOneMenu id="selectLicenseSource"
																style="width: 200px"
																rendered="#{!pages$contractorDetails.isViewMode}"
																value="#{pages$contractorDetails.contractorView.licenseSourceId}">
																<f:selectItem id="selectAllSource" itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$addContractor.licenseSourceList}" />
															</h:selectOneMenu>

														
															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$contractorDetails.isViewMode}"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.issueDate']}:" />
															</h:panelGroup>
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																rendered="#{pages$contractorDetails.isViewMode}"
																readonly="#{pages$contractorDetails.isViewMode}"
																id="issueDate" style="width:195px;"
																value="#{pages$contractorDetails.contractorView.licenseIssueDate}" >
																<f:convertDateTime	pattern="#{ges$contractorDetails.dateFormat}" />
															</h:inputText>																	
															<rich:calendar id="txtIssueDate" inputStyle="width: 180px; "
																	rendered="#{!pages$contractorDetails.isViewMode}"
																	popup="true" value="#{pages$contractorDetails.contractorView.licenseIssueDate}"
																	showApplyButton="false" enableManualInput="false" 
																	datePattern="dd/MM/yyyy"/> 
																
															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$contractorDetails.isViewMode}"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['TaskList.DataTable.ExpiryDate']}:" />
															</h:panelGroup>
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																rendered="#{pages$contractorDetails.isViewMode}"
																readonly="#{pages$contractorDetails.isViewMode}"
																id="expiryDate" style="width:195px;"
																value="#{pages$contractorDetails.contractorView.licenseExpiryDate}" >
																<f:convertDateTime	pattern="#{ges$contractorDetails.dateFormat}" />
															</h:inputText>																
															<rich:calendar id="txtExpiryDate" inputStyle="width: 180px;"
																	rendered="#{!pages$contractorDetails.isViewMode}"
																	popup="true" value="#{pages$contractorDetails.contractorView.licenseExpiryDate}"
																	showApplyButton="false" enableManualInput="false"
																	datePattern="dd/MM/yyyy"/>																

 
														<h:outputLabel styleClass="LABEL"
																value="#{msg['contractAdd.businessActivity']}:" />
															<t:dataList
																rendered="#{pages$contractorDetails.isViewMode}"
																var="itemInfo" style="text-indent:-15px;" 
																rows="15" value="#{pages$contractorDetails.businessAcitvitiesList}"
																layout="unorderedList">
																<h:outputText value="#{itemInfo}"  />
															</t:dataList>																
															
															<h:selectManyCheckbox layout="pageDirection"
																	id="selectManyBusinessActivity"
																	rendered="#{!pages$contractorDetails.isViewMode}"
																	value="#{pages$contractorDetails.businessAcitvitiesList}">
																	<f:selectItems
																		value="#{pages$contractorDetails.allBusinessActivityList}" />
															</h:selectManyCheckbox>
															

																<h:outputLabel styleClass="LABEL"
																value="#{msg['serviceContract.serviceType']}:" />
															<t:dataList
																rendered="#{pages$contractorDetails.isViewMode}"
																var="serviceType" style="text-indent:-15px" 
																id="serviceTypeData"
																rows="15" value="#{pages$contractorDetails.serviceTypeList}"
																layout="unorderedList">
																<h:outputText value="#{serviceType}"  />
															</t:dataList>																
																
															<h:selectManyCheckbox layout="pageDirection"
																id="selectManyServiceType"
																rendered="#{!pages$contractorDetails.isViewMode}"
																value="#{pages$contractorDetails.serviceTypeList}">
																<f:selectItems
																value="#{pages$contractorDetails.allServiceTypesList}" />
															</h:selectManyCheckbox>
															  <%--  --%> 
														</t:panelGrid>
													</rich:tab>
													<!--  Contractor Details - Tab (End) -->

													<!--  Address Details - Tab (Start) -->
													<rich:tab id="addressDetailsTab"
														label="#{msg['contractAdd.addressDetails']}">
														
														<t:div  rendered="#{pages$contractorDetails.isAddressShow && !pages$contractorDetails.isClosed}" style="width:94.5%">
														<t:panelGrid width="100%">
														
														<t:panelGrid cellpadding="1px" cellspacing="2px"
															styleClass="TAB_DETAIL_SECTION" width="100%" columns="4">
																														
															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$contractorDetails.isViewMode}"/>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.address1']}:" />
															</h:panelGroup>
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}"  
																readonly="#{pages$contractorDetails.isViewMode}" 
																style="width:150px;"
																maxlength="150" id="txtaddress1"
																value="#{pages$contractorDetails.contactInfoView.address1}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.address2']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																readonly="#{pages$contractorDetails.isViewMode}"  style="width:150px;"
																maxlength="150" id="txtaddress2"
																value="#{pages$contractorDetails.contactInfoView.address2}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.street']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																readonly="#{pages$contractorDetails.isViewMode}"  style="width:150px;"
																maxlength="150" id="txtstreet"
																value="#{pages$contractorDetails.contactInfoView.street}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contractorAdd.POBox']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																readonly="#{pages$contractorDetails.isViewMode}" 
																style="width:150px; text-align:right;" maxlength="10" id="txtpostCode"
																value="#{pages$contractorDetails.contactInfoView.postCode}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.country']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																rendered="#{pages$contractorDetails.isViewMode}"
																readonly="#{pages$contractorDetails.isViewMode}"
																id="contactCountry" style="width:150px;"
																value="#{pages$contractorDetails.contactInfoView.country}" />	
																															
															<h:selectOneMenu id="selectCountry" style="WIDTH: 155px"
																rendered="#{!pages$contractorDetails.isViewMode}"
																onchange="resetState();submit()" 
																valueChangeListener="#{pages$contractorDetails.loadState}" 
																value="#{pages$contractorDetails.countryId}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$ApplicationBean.countryList}" />
															</h:selectOneMenu>


															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.state']}:" />
																
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																rendered="#{pages$contractorDetails.isViewMode}"
																readonly="#{pages$contractorDetails.isViewMode}"
																id="contactState" style="width:150px;"
																value="#{pages$contractorDetails.contactInfoView.state}" />																	
																
															<h:selectOneMenu id="selectState"
																style="width: 155px; height: 24px" required="false"
																valueChangeListener="#{pages$contractorDetails.loadCities}"
																onchange="resetCities();submit()" 
																rendered="#{!pages$contractorDetails.isViewMode}"
																value="#{pages$contractorDetails.stateId}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$contractorDetails.states}" />
															</h:selectOneMenu>
															
															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.city']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																rendered="#{pages$contractorDetails.isViewMode}"
																readonly="#{pages$contractorDetails.isViewMode}"
																id="contactCity" style="width:150px;"
																value="#{pages$contractorDetails.contactInfoView.city}" />																	
																
															<h:selectOneMenu id="selectCity" style="width: 155px"
																rendered="#{!pages$contractorDetails.isViewMode}"
																value="#{pages$contractorDetails.cityId}">
																<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$contractorDetails.cities}" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.homephone']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																readonly="#{pages$contractorDetails.isViewMode}" 
																style="width:152px; text-align:right;" maxlength="15" id="txthomePhone"
																value="#{pages$contractorDetails.contactInfoView.homePhone}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.officephone']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																readonly="#{pages$contractorDetails.isViewMode}" 
																style="width:150px; text-align:right;" maxlength="15" id="txtofficePhone"
																value="#{pages$contractorDetails.contactInfoView.officePhone}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.fax']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																readonly="#{pages$contractorDetails.isViewMode}" 
																style="width:152px; text-align:right;" maxlength="15" id="txtfax"
																value="#{pages$contractorDetails.contactInfoView.fax}" />
															<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$contractorDetails.isViewMode}"/>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.email']}:" />
															</h:panelGroup>
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																readonly="#{pages$contractorDetails.isViewMode}"  style="width:150px;"
																maxlength="50" id="txtemail"
																value="#{pages$contractorDetails.contactInfoView.email}" />
													
													</t:panelGrid>
														<t:panelGrid align="right" width="715" columns="1" columnClasses="BUTTON_TD">
															<h:commandButton id="btnUpdateContactInfo"
																rendered="#{!(pages$contractorDetails.isViewMode)}" 
																styleClass="BUTTON" value="#{msg['commons.Update']}"
																action="#{pages$contractorDetails.updateContactInfo}" />
																
															<h:commandButton id="btnCloseContactInfo"
																rendered="#{(pages$contractorDetails.isViewMode)}" 
																styleClass="BUTTON" value="#{msg['commons.closeButton']}"
																action="#{pages$contractorDetails.close}" />																
																									
													</t:panelGrid>
												</t:panelGrid>	
											</t:div>	
													
													<t:div style="text-align:center; width:94.5;">
														<t:div id="contactInfoDiv" styleClass="contentDiv"
															 style="align:center;">
															<t:dataTable id="contactInfoDataTable"
															    rows="#{pages$contractorDetails.paginatorRows}"
																binding="#{pages$contractorDetails.contactInfoDataTable}"
																value="#{pages$contractorDetails.contactInfoList}"
																preserveDataModel="false" preserveSort="false"
																var="contactInfoItem" rowClasses="row1,row2" rules="all"
																renderedIfEmpty="true" width="100%">
																<t:column id="colAddress1" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['contact.address1']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{contactInfoItem.address1}" />
																</t:column>

																<t:column id="colAddress2">
																	<f:facet name="header">
																		<t:outputText value="#{msg['contact.email']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{contactInfoItem.email}" />
																</t:column>

																<t:column id="colActionContactInfo" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.select']}" />
																	</f:facet>
																	<h:commandLink 
																		rendered="#{!pages$contractorDetails.isViewMode}"
																		action="#{pages$contractorDetails.removeContactInfo}">
																		<h:graphicImage id="imgRemoveContactInfo"
																			title="#{msg['commons.delete']}"
																			url="../resources/images/delete_icon.png" />
																	</h:commandLink>
																	<h:commandLink
																		rendered="#{!pages$contractorDetails.isViewMode}"
																		action="#{pages$contractorDetails.viewContactInfo}">
																		<h:graphicImage id="btnPopulateContactInfoFromList"
																			title="#{msg['commons.edit']}"
																			url="../resources/images/edit-icon.gif" />
																	</h:commandLink>
																	<h:commandLink
																		rendered="#{pages$contractorDetails.isViewMode}"
																		action="#{pages$contractorDetails.viewContactInfo}">
																		<h:graphicImage id="btnviewContactinfo"
																			title="#{msg['commons.view']}"
																			url="../resources/images/detail-icon.gif" />
																	</h:commandLink>
																	
																</t:column>
															</t:dataTable>
														</t:div>
														<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																	style="width:99%;">
																	<t:panelGrid cellpadding="0" cellspacing="0"
																		width="100%" columns="2" columnClasses="RECORD_NUM_TD">
																		<t:div styleClass="RECORD_NUM_BG">
																			<t:panelGrid cellpadding="0" cellspacing="0"
																				columns="3" columnClasses="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.records']}" />
																				<h:outputText value=" : " styleClass="RECORD_NUM_TD" />
																				<h:outputText
																					id="contactInfoRecordSize"
																					value="#{pages$contractorDetails.contactInfoRecordSize}"
																					styleClass="RECORD_NUM_TD" />
																			</t:panelGrid>
																		</t:div>
	                                                                      <t:dataScroller id="extendscroller" for="contactInfoDataTable" paginator="true"
																											fastStep="1"  paginatorMaxPages="#{pages$contractorDetails.contactInfoPaginatorMaxPages}" immediate="false"
																											paginatorTableClass="paginator"
																											renderFacetsIfSinglePage="true" 								
																										    paginatorTableStyle="grid_paginator" layout="singleTable"
																											paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																											paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;" 
																											pageIndexVar="pageNumber"
																											styleClass="SCH_SCROLLER" >
														
																											<f:facet name="first">
																													<t:graphicImage url="../#{path.scroller_first}" id="lblFinquiry"></t:graphicImage>
																												</f:facet>
																												<f:facet name="fastrewind">
																													<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFRinquiry"></t:graphicImage>
																												</f:facet>
																												<f:facet name="fastforward">
																													<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFinquiry"></t:graphicImage>
																												</f:facet>
																												<f:facet name="last">
																													<t:graphicImage url="../#{path.scroller_last}" id="lblLinquiry"></t:graphicImage>
																												</f:facet>
																											<t:div styleClass="PAGE_NUM_BG" rendered="true">
																					<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																					<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																				</t:div>
																			</t:dataScroller>

																	</t:panelGrid>
																</t:div>
														
														
														
														
														
														
														
														
														
														
														
														
														
													</t:div>
													</rich:tab>
													<!--  Address Details - Tab (End) -->

													<!--  Contact Person - Tab (Start) -->
													<rich:tab id="contactPersonTab"
														label="#{msg['contractAdd.contactPersons']}">
														
														<t:div  rendered="#{pages$contractorDetails.isContactPersonShow && !pages$contractorDetails.isContactPersonClosed}" style="width:94.5%">
														<t:panelGrid width="100%">														
														
														<t:panelGrid id="crDetailTable" cellpadding="1px"
															cellspacing="2px" styleClass="TAB_DETAIL_SECTION"
															width="100%" columns="4">
															
															<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.title']}:" />
																
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																rendered="#{pages$contractorDetails.isViewMode && pages$contractorDetails.isEnglishLocale}"
																readonly="#{pages$contractorDetails.isViewMode}" 
																style="width:150px;" maxlength="15" id="txtTitleId"
																value="#{pages$contractorDetails.contactReferenceView.titleEn}" />	
																
																																											    	
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																rendered="#{pages$contractorDetails.isViewMode && pages$contractorDetails.isArabicLocale}"
																readonly="#{pages$contractorDetails.isViewMode}"
																id="txtconrtactorTitleAr" style="width:195px;"
																value="#{pages$contractorDetails.contactReferenceView.titleAr}" />	
																															
																
															<h:selectOneMenu id="selectCRTitle" style="width: 155px"
																rendered="#{!pages$contractorDetails.isViewMode}"
																value="#{pages$contractorDetails.contactReferenceView.title}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems value="#{pages$ApplicationBean.title}" />
															</h:selectOneMenu>
															
															<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$contractorDetails.isViewMode}" />
															<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.firstname']}:" />
															</h:panelGroup>
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																	readonly="#{pages$contractorDetails.isViewMode}" 
																 	style="width:150px;"
																	maxlength="100" id="txtCRFirstName"
																	value="#{pages$contractorDetails.contactReferenceView.firstName}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.middlename']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																		 readonly="#{pages$contractorDetails.isViewMode}" 
																	 	 style="width:150px;"
																		 maxlength="100" id="txtCRMiddleName"
																         value="#{pages$contractorDetails.contactReferenceView.middleName}" />
															<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$contractorDetails.isViewMode}" />
															<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.lastname']}:" />
															</h:panelGroup>
																
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																		 readonly="#{pages$contractorDetails.isViewMode}" 
																		 style="width:150px;"
																		 maxlength="50" id="txtCRLastName"
																         value="#{pages$contractorDetails.contactReferenceView.lastName}" />
															
															<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*" rendered="#{!pages$contractorDetails.isViewMode}"/>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.address1']}:" />
															</h:panelGroup>
															
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																		 readonly="#{pages$contractorDetails.isViewMode}" 
																		 style="width:150px;"
																		 maxlength="150" id="txtCRAddress1"
																		 value="#{pages$contractorDetails.contactReferenceView.contactInfoView.address1}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.address2']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																		 readonly="#{pages$contractorDetails.isViewMode}" 
																		 style="width:150px;"
																		 maxlength="150" id="txtCRAddress2"
																		 value="#{pages$contractorDetails.contactReferenceView.contactInfoView.address2}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.street']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																		 readonly="#{pages$contractorDetails.isViewMode}" 
																		 style="width:150px;"
																		 maxlength="150" id="txtCRStreet"
																		 value="#{pages$contractorDetails.contactReferenceView.contactInfoView.street}" />

															<%--<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.designation']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																		 readonly="#{pages$contractorDetails.isViewMode}" 
																		 style="width:150px;" maxlength="15"
																		 id="txtCRDesignation"
																		 value="#{pages$contractorDetails.contactReferenceView.contactInfoView.designation}" /> --%>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.postcode']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																		 readonly="#{pages$contractorDetails.isViewMode}" 
																		 style="width:150px; text-align:right;" maxlength="10" id="txtCRPostCode"
																		 value="#{pages$contractorDetails.contactReferenceView.contactInfoView.postCode}" />

														
															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.country']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
														 				 readonly="#{pages$contractorDetails.isViewMode}" 
														 				 rendered="#{pages$contractorDetails.isViewMode}"
																		 style="width:150px;" maxlength="10" id="txtCRCountry"
																		 value="#{pages$contractorDetails.contactReferenceView.contactInfoView.country}" />
																
															<h:selectOneMenu id="selectCountryCR"
																style="WIDTH: 155px"
															    valueChangeListener="#{pages$contractorDetails.loadContactRefState}" 
															 	onchange="resetStateCR();submit()" 
														 		rendered="#{!(pages$contractorDetails.isViewMode)}"																
																value="#{pages$contractorDetails.CRcountryId}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems
																	value="#{pages$ApplicationBean.countryList}" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.state']}:" />
															
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
														 				 readonly="#{pages$contractorDetails.isViewMode}" 
																		 rendered="#{pages$contractorDetails.isViewMode}"	
																		 style="width:150px;" maxlength="10" id="txtCRState"
																		 value="#{pages$contractorDetails.contactReferenceView.contactInfoView.state}" />
																
															<h:selectOneMenu id="selectStateCR"
																style="width: 155px; height: 24px" required="false"
															    valueChangeListener="#{pages$contractorDetails.loadContactRefCity}" 
															 	onchange="resetCitiesCR();submit()" 
														 		rendered="#{!(pages$contractorDetails.isViewMode)}"																
																value="#{pages$contractorDetails.CRstateId}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems value="#{pages$contractorDetails.CRStateList}" />
															</h:selectOneMenu>
															
															

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.city']}:" />
																
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
														 				 readonly="#{pages$contractorDetails.isViewMode}" 
														 				 rendered="#{pages$contractorDetails.isViewMode}"	
																		 style="width:150px;" maxlength="10" id="txtCRCity"
																		 value="#{pages$contractorDetails.contactReferenceView.contactInfoView.city}" />																
																
															<h:selectOneMenu id="selectCityCR" style="width: 155px"
																rendered="#{!(pages$contractorDetails.isViewMode)}"	
																value="#{pages$contractorDetails.CRCityId}">
																<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems value="#{pages$contractorDetails.CRCityList}" />
															</h:selectOneMenu>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.homephone']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																		 readonly="#{pages$contractorDetails.isViewMode}" 
																		 style="width:152px; text-align:right;" maxlength="15" id="txtCRHomePhone"
																		 value="#{pages$contractorDetails.contactReferenceView.contactInfoView.homePhone}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.officephone']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																		 readonly="#{pages$contractorDetails.isViewMode}" 
																		 style="width:150px; text-align:right;" maxlength="15"
																		 id="txtCROfficePhone"
																		 value="#{pages$contractorDetails.contactReferenceView.contactInfoView.officePhone}" />

															<%--<h:outputLabel styleClass="LABEL"
																value="#{msg['contractorAdd.mobileNumber']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																		 readonly="#{pages$contractorDetails.isViewMode}" 
																		 style="width:150px; text-align:right;" maxlength="15"
																		 id="txtCRMobileNumber"
																		 value="#{pages$contractorDetails.contactReferenceView.contactInfoView.mobileNumber}" /> --%>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['contact.fax']}:" />
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																		 readonly="#{pages$contractorDetails.isViewMode}" 
																		 style="width:152px; text-align:right;" maxlength="15" id="txtCRFax"
																		 value="#{pages$contractorDetails.contactReferenceView.contactInfoView.fax}" />
															<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*" rendered="#{!(pages$contractorDetails.isViewMode)}" />
															<h:outputLabel styleClass="LABEL" value="#{msg['contact.email']}:" />
															</h:panelGroup>
															
															<h:inputText styleClass="#{pages$contractorDetails.styleClass}" 
																		 readonly="#{pages$contractorDetails.isViewMode}" 
																		 style="width:150px;"
																         maxlength="50" id="txtCREmail"
																         value="#{pages$contractorDetails.contactReferenceView.contactInfoView.email}" />
															</t:panelGrid>
															
															<t:panelGrid align="right" width="715" columns="1" columnClasses="BUTTON_TD">
															<h:commandButton id="btnUpdateContactRefInfo"
																rendered="#{!(pages$contractorDetails.isViewMode)}" 
																styleClass="BUTTON" value="#{msg['commons.Update']}"
																action="#{pages$contractorDetails.updateContactReference}" />
																
															<h:commandButton id="btnCloseContactRefInfo"
																rendered="#{(pages$contractorDetails.isViewMode)}" 
																styleClass="BUTTON" value="#{msg['commons.closeButton']}"
																action="#{pages$contractorDetails.contactReferenceClose}" />
														    </t:panelGrid>
														    
												</t:panelGrid>	
													</t:div>	
													
													<t:div>
													</t:div>														    
														    

														<t:div style="text-align:center;">
														<t:div id="contactRefDiv" styleClass="contentDiv"
															style="align:center;">
															<t:dataTable id="contactRefDataTable" rows="#{pages$contractorDetails.paginatorRows}"
																width="100%"
																binding="#{pages$contractorDetails.contactRefDataTable}"
																value="#{pages$contractorDetails.contactReferencesList}"
																preserveDataModel="false" preserveSort="false"
																var="contactRefItem" rowClasses="row1,row2" rules="all"
																renderedIfEmpty="true">

																<t:column id="colCRName" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.Name']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" rendered="#{pages$contractorDetails.isViewMode}"
																		value="#{contactRefItem.fullName}" />
																	<t:outputText styleClass="A_LEFT" rendered="#{!pages$contractorDetails.isViewMode}"
																		value="#{contactRefItem.firstName}" />																		
																</t:column>

																<t:column id="colCRAddress2">
																	<f:facet name="header">
																		<t:outputText value="#{msg['contact.email']}" />
																	</f:facet>
																	<t:outputText title="" styleClass="A_LEFT"
																		value="#{contactRefItem.contactInfoView.email}" />
																</t:column>

																<t:column id="colActionContactRef" sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.select']}" />
																	</f:facet>
																	<h:commandLink 
																		rendered="#{!pages$contractorDetails.isViewMode}"
																		action="#{pages$contractorDetails.removeContactReference}">
																		<h:graphicImage id="imgRemoveContactRef"
																			title="#{msg['commons.delete']}"
																			url="../resources/images/delete_icon.png" />
																	</h:commandLink>
																	<h:commandLink
																		rendered="#{!pages$contractorDetails.isViewMode}"
																		action="#{pages$contractorDetails.viewContactRefInfo}">
																		<h:graphicImage id="btnPopulateContactRefFromList"
																			title="#{msg['commons.edit']}"
																			url="../resources/images/edit-icon.gif" />
																	</h:commandLink>
																	
																	<h:commandLink
																		rendered="#{pages$contractorDetails.isViewMode}"
																		action="#{pages$contractorDetails.viewContactRefInfo}">
																		<h:graphicImage id="btnviewContactinfo"
																			title="#{msg['commons.view']}"
																			url="../resources/images/detail-icon.gif" />
																	</h:commandLink>																	
																	
																</t:column>
															</t:dataTable>
														</t:div>
														
														<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																	style="width:99%;">
																	<t:panelGrid cellpadding="0" cellspacing="0"
																		width="100%" columns="2" columnClasses="RECORD_NUM_TD">
																		<t:div styleClass="RECORD_NUM_BG">
																			<t:panelGrid cellpadding="0" cellspacing="0"
																				columns="3" columnClasses="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.records']}" />
																				<h:outputText value=" : " styleClass="RECORD_NUM_TD" />
																				<h:outputText id="contactRefRecordSize"
																					value="#{pages$contractorDetails.contactReferenceRecordSize}"
																					styleClass="RECORD_NUM_TD" />
																			</t:panelGrid>
																		</t:div>
	                                                                      <t:dataScroller id="scroller" for="contactRefDataTable" paginator="true"
																											fastStep="1"  paginatorMaxPages="#{pages$contractorDetails.contactReferencePaginatorMaxPages}" immediate="false"
																											paginatorTableClass="paginator"
																											renderFacetsIfSinglePage="true" 								
																										    paginatorTableStyle="grid_paginator" layout="singleTable"
																											paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																											paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;" 
																											pageIndexVar="pageNumber"
																											styleClass="SCH_SCROLLER" >
														
																											<f:facet name="first">
																													<t:graphicImage url="../#{path.scroller_first}" id="first"></t:graphicImage>
																												</f:facet>
																												<f:facet name="fastrewind">
																													<t:graphicImage url="../#{path.scroller_fastRewind}" id="rewind"></t:graphicImage>
																												</f:facet>
																												<f:facet name="fastforward">
																													<t:graphicImage url="../#{path.scroller_fastForward}" id="forward"></t:graphicImage>
																												</f:facet>
																												<f:facet name="last">
																													<t:graphicImage url="../#{path.scroller_last}" id="last"></t:graphicImage>
																												</f:facet>
																											<t:div styleClass="PAGE_NUM_BG" rendered="true">
																					<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																					<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																				</t:div>
																			</t:dataScroller>

																	</t:panelGrid>
																</t:div>
														
														
														
														
													</t:div>
													</rich:tab>
													<!--  Contact Person - Tab (End) -->

													<!--  Attachment - Tab (Start) -->
													<rich:tab id="attachmentTab"
														label="#{msg['commons.attachmentTabHeading']}">
														<%@  include file="attachment/attachment.jsp"%>
													</rich:tab>
													<!--  Attachment - Tab (End) -->

													<!--  Comments - Tab (Start) -->
													<rich:tab id="commentsTab"
														label="#{msg['commons.comments']}">
														<%@ include file="notes/notes.jsp"%>
													</rich:tab>
													<!--  Comments - Tab (End) -->
												</rich:tabPanel>

												<table width="98%">
													<tr>
														<td align="right">
															<h:commandButton styleClass="BUTTON" type="button"
																onclick="javascript:closeWindow();"
																value="#{msg['commons.cancel']}"
																rendered="#{pages$contractorDetails.isViewModePopUp}" />
															<h:commandButton type="submit" styleClass="BUTTON"
																rendered="#{!(pages$contractorDetails.isViewMode)}" 
																value="#{msg['commons.saveButton']}"
																action="#{pages$contractorDetails.save}" />
																
															 <h:commandButton id="generateLoginId" styleClass="BUTTON"  
                               									   style="width: 110px"
                               									   value="#{msg['commons.generateLoginId']}" 
                               									   action ="#{pages$contractorDetails.generateLoginId}" 
                               									   rendered="#{pages$contractorDetails.isGenerateLogin}"/>	
																
															<h:commandButton type="submit" styleClass="BUTTON"
																rendered="#{!pages$contractorDetails.isViewModePopUp}"
																value="#{msg['commons.back']}"
																action="#{pages$contractorDetails.back}" />	
														</td>
													</tr>
												</table>
											</div>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		<c:choose>
			<c:when test="${!pages$contractorDetails.isViewModePopUp}">	
		 </div>
		 </c:when>
		</c:choose> 	
		</body>
	</html>
</f:view>