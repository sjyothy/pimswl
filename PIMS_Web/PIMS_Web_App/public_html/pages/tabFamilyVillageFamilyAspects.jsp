
<t:div id="tabFamilyAspectsSocial" style="width:100%;">

	<t:panelGrid id="tabFamilyAspectsSocialGrid" cellpadding="3px"
		width="100%" cellspacing="2px" columns="1"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.FamilyAspectSocial']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px" />

		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.FamilyAspectSocialDetails']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 10px;" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">

		<h:commandButton id="btnAddFamilyAspectSocial" styleClass="BUTTON"
			type="button" value="#{msg['commons.Add']}"
			onclick="javaScript:openFamilyVillageFamilyAspectSocialPopup('#{pages$tabFamilyVillageFamilyAspects.personId}');">


		</h:commandButton>

	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableFamilyAspectSocial" rows="5" width="100%"
			value="#{pages$tabFamilyVillageFamilyAspects.dataListFamilyAspectSocial}"
			binding="#{pages$tabFamilyVillageFamilyAspects.dataTableFamilyAspectSocial}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="socialAspectCreatedBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="socialAspectCreatedByOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageFamilyAspects.englishLocale? 
																				dataItem.createdByEn:
																				dataItem.createdByAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="socialAspectCreatedOn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageFamilyAspects.dateFormat}"
						timeZone="#{pages$tabFamilyVillageFamilyAspects.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="socialAspectMothersUnderstadning" sortable="true">
				<f:facet name="header">
					<t:outputText id="socialAspectMothersUnderstadningOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.FamilyAspectSocialMotherUnderstanding']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageFamilyAspects.englishLocale? 
																				dataItem.mothersUnderStandingEn:
																				dataItem.mothersUnderStandingAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>

			<t:column id="socialAspectMothersCaring" sortable="true">
				<f:facet name="header">
					<t:outputText id="socialAspectMothersCaringOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.FamilyAspectSocialMotherCaring']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageFamilyAspects.englishLocale? 
																				dataItem.mothersCaringEn:
																				dataItem.mothersCaringAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="socialAspectsiblingsUnderStanding" sortable="true">
				<f:facet name="header">
					<t:outputText id="socialAspectsiblingsUnderStandingOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.FamilyAspectSocialSiblingUnderstanding']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageFamilyAspects.englishLocale? 
																				dataItem.siblingsUnderStandingEn:
																				dataItem.siblingsUnderStandingAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>

			<t:column id="socialAspectDetails" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.description']}" />
				</f:facet>
				<h:outputText value="#{dataItem.socialAspectDescription}">

				</h:outputText>
			</t:column>


		</t:dataTable>
	</t:div>
	<t:div id="socialAspectDivScroller" styleClass="contentDivFooter"
		style="width:99.1%">


		<t:panelGrid id="socialAspectRecNumTable" columns="2" cellpadding="0"
			cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD">
			<t:div id="socialAspectRecNumDiv" styleClass="RECORD_NUM_BG">
				<t:panelGrid id="socialAspectRecNumTbl" columns="3"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
					cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
					<h:outputText value="#{msg['commons.recordsFound']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$tabFamilyVillageFamilyAspects.recordSizeFamilyAspectSocial}" />
				</t:panelGrid>

			</t:div>

			<CENTER>
				<t:dataScroller id="socialAspectScroller"
					for="dataTableFamilyAspectSocial" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$tabFamilyVillageFamilyAspects.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="socialAspectlblFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="socialAspectlblFRRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="socialAspectlblFFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="socialAspectlblLRequestHistory"></t:graphicImage>
					</f:facet>

					<t:div id="socialAspectPageNumDiv" styleClass="PAGE_NUM_BG">
						<t:panelGrid id="socialAspectPageNumTable"
							styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
							cellspacing="0">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:panelGrid>

					</t:div>
				</t:dataScroller>
			</CENTER>

		</t:panelGrid>
	</t:div>

	<t:panelGrid id="tabFamilyAspectsPshychologyGrid" cellpadding="3px"
		width="100%" cellspacing="2px" columns="1" style="margin-top:20px;"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.FamilyAspectPsychology']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px;" />

		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.FamilyAspectPsychologyDetails']}"
						  
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 10px;" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">

		<h:commandButton id="btnAddFamilyAspectPshychology" styleClass="BUTTON"
			value="#{msg['commons.Add']}" type="button"
			onclick="javaScript:openFamilyVillageFamilyAspectPsychologyPopup('#{pages$tabFamilyVillageFamilyAspects.personId}');">

		</h:commandButton>
	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableFamilyAspectPshychology" rows="5"
			width="100%"
			value="#{pages$tabFamilyVillageFamilyAspects.dataListFamilyAspectPsychology}"
			binding="#{pages$tabFamilyVillageFamilyAspects.dataTableFamilyAspectPsychology}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="SocialAspectCreatedBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="SocialAspectCreatedByOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageFamilyAspects.englishLocale? 
																				dataItem.createdByEn:
																				dataItem.createdByAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="SocialAspectCreatedOn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageFamilyAspects.dateFormat}"
						timeZone="#{pages$tabFamilyVillageFamilyAspects.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="SocialAspectSiblingName" sortable="true">
				<f:facet name="header">
					<t:outputText id="SocialAspectSiblingNameOT"
						value="#{msg['commons.Name']}" />
				</f:facet>
				<h:outputText
					value="#{ dataItem.familyDetails.familyMember.personFullName}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="SocialAspectSiblingRelation" sortable="true">
				<f:facet name="header">
					<t:outputText id="SocialAspectSiblingRelationOT"
						value="#{msg['SearchBeneficiary.relationship']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageFamilyAspects.englishLocale? 
																				dataItem.familyDetails.relationship.relationshipDescEn:
																				dataItem.familyDetails.relationship.relationshipDescAr
																				
						   }"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="SocialAspectDisorderType" sortable="true">
				<f:facet name="header">
					<t:outputText id="SocialAspectDisorderTypeOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.FamilyAspectPsychology.disorderType']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageFamilyAspects.englishLocale? 
																				dataItem.disorderTypeEn:
																				dataItem.disorderTypeAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="SocialAspectDetails" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.description']}" />
				</f:facet>
				<h:outputText value="#{dataItem.description}">

				</h:outputText>
			</t:column>


		</t:dataTable>
	</t:div>
	<t:div id="SocialAspectDivScroller" styleClass="contentDivFooter"
		style="width:99.1%">


		<t:panelGrid id="SocialAspectRecNumTable" columns="2"
			cellpadding="0" cellspacing="0" width="100%"
			columnClasses="RECORD_NUM_TD,BUTTON_TD">
			<t:div id="SocialAspectRecNumDiv" styleClass="RECORD_NUM_BG">
				<t:panelGrid id="SocialAspectRecNumTbl" columns="3"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
					cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
					<h:outputText value="#{msg['commons.recordsFound']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$tabFamilyVillageFamilyAspects.recordSizeFamilyAspectSocial}" />
				</t:panelGrid>

			</t:div>

			<CENTER>
				<t:dataScroller id="SocialAspectScroller"
					for="dataTableFamilyAspectSocial" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$tabFamilyVillageFamilyAspects.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="SocialAspectlblFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="SocialAspectlblFRRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="SocialAspectlblFFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="SocialAspectlblLRequestHistory"></t:graphicImage>
					</f:facet>

					<t:div id="SocialAspectPageNumDiv" styleClass="PAGE_NUM_BG">
						<t:panelGrid id="SocialAspectPageNumTable"
							styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
							cellspacing="0">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:panelGrid>

					</t:div>
				</t:dataScroller>
			</CENTER>

		</t:panelGrid>
	</t:div>

	<t:panelGrid id="tabFamilyAspectsFinancialGrid" cellpadding="3px"
		width="100%" cellspacing="2px" columns="1" style="margin-top:20px;"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.FamilyAspectFinancial']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px;" />

		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.FamilyAspectFinancialDetails']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 10px;" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">
		<h:commandButton id="btnAddFamilyAspectFinancial" styleClass="BUTTON"
			type="button" value="#{msg['commons.Add']}"
			onclick="javaScript:openFamilyVillageFamilyAspectFinancialPopup('#{pages$tabFamilyVillageFamilyAspects.personId}');">
		</h:commandButton>
	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableFamilyAspectFinancial" rows="5" width="100%"
			value="#{pages$tabFamilyVillageFamilyAspects.dataListFamilyAspectFinancial}"
			binding="#{pages$tabFamilyVillageFamilyAspects.dataTableFamilyAspectFinancial}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="FinancialAspectCreatedBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="FinancialAspectCreatedByOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageFamilyAspects.englishLocale? 
																				dataItem.createdByEn:
																				dataItem.createdByAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="FinancialAspectCreatedOn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageFamilyAspects.dateFormat}"
						timeZone="#{pages$tabFamilyVillageFamilyAspects.timeZone}" />
				</h:outputText>
			</t:column>

			<t:column id="FinancialAspectDisorderType" sortable="true">
				<f:facet name="header">
					<t:outputText id="FinancialAspectDisorderTypeOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.FamilyAspectFinancial.monthlyIncome']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageFamilyAspects.englishLocale? 
																				dataItem.monthlyIncomeEn:
																				dataItem.monthlyIncomeAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="FinancialAspectincomeSource" sortable="true">
				<f:facet name="header">
					<t:outputText id="FinancialAspectincomeSourceOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.FamilyAspectFinancial.incomeSource']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageFamilyAspects.englishLocale? 
																				dataItem.monthlyIncomeSourceEn:
																				dataItem.monthlyIncomeSourceAr
																				
						   }"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="FinancialAspectDetails" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.description']}" />
				</f:facet>
				<h:outputText value="#{dataItem.description}">

				</h:outputText>
			</t:column>


		</t:dataTable>
	</t:div>
	<t:div id="FinancialAspectDivScroller" styleClass="contentDivFooter"
		style="width:99.1%">


		<t:panelGrid id="FinancialAspectRecNumTable" columns="2"
			cellpadding="0" cellspacing="0" width="100%"
			columnClasses="RECORD_NUM_TD,BUTTON_TD">
			<t:div id="FinancialAspectRecNumDiv" styleClass="RECORD_NUM_BG">
				<t:panelGrid id="FinancialAspectRecNumTbl" columns="3"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
					cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
					<h:outputText value="#{msg['commons.recordsFound']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$tabFamilyVillageFamilyAspects.recordSizeFamilyAspectFinancial}" />
				</t:panelGrid>

			</t:div>

			<CENTER>
				<t:dataScroller id="FinancialAspectScroller"
					for="dataTableFamilyAspectFinancial" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$tabFamilyVillageFamilyAspects.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="FinancialAspectlblFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="FinancialAspectlblFRRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="FinancialAspectlblFFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="FinancialAspectlblLRequestHistory"></t:graphicImage>
					</f:facet>

					<t:div id="FinancialAspectPageNumDiv" styleClass="PAGE_NUM_BG">
						<t:panelGrid id="FinancialAspectPageNumTable"
							styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
							cellspacing="0">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:panelGrid>

					</t:div>
				</t:dataScroller>
			</CENTER>

		</t:panelGrid>
	</t:div>



</t:div>


