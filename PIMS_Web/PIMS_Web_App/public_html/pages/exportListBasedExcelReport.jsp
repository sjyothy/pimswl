<%--
  - Author: Majid Hameed
  - Date: December 20, 2010
  - Copyright Notice:
  - @(#)
  - Description:: JSP for rendering XLS report in response if no record found then error code is set in session 
  --%>

<%@page import="com.avanza.pims.exporter.factory.ListBasedExporterFactory"%>
<%@page import="com.avanza.pims.exporter.IExporter"%>
<%@page import="com.avanza.pims.exporter.ListBasedExporterMeta"%>
<%@page import="com.avanza.core.util.Logger"%>
<%@page import="javax.faces.context.FacesContext"%>
<%@page import="com.avanza.pims.web.WebConstants"%>
<%@page import="com.avanza.pims.business.exceptions.PimsBusinessException"%>
<%@page import="com.avanza.pims.business.exceptions.ExceptionCodes"%>
<script type="text/javascript">
<!--
	function closeWindowSubmit(){
		window.opener.document.forms[0].submit();
		window.close();
	}
//-->
</script>
<%
	Logger logger = Logger.getLogger(this.getClass());
	
	try {
		logger.logInfo(" --- STARTED --- ");
		
		if (session.getAttribute("REPORT_EXPORTER_META") != null ) {
			ListBasedExporterMeta exporterMeta = (ListBasedExporterMeta) session.getAttribute("REPORT_EXPORTER_META");
			
			exporterMeta.setFacesContext(FacesContext.getCurrentInstance());
			exporterMeta.setExporterType(ListBasedExporterMeta.ExporterType.ExcelExporter);
			
			IExporter exporter = ListBasedExporterFactory.getExporter( exporterMeta );
		 	
			if( exporter != null ){
				exporter.export();
			}
		}
		logger.logInfo(" --- COMPLETED --- ");
	}
	catch (Exception e) {
		logger.LogException(" --- EXCEPTION --- ", e);
		if (e  instanceof PimsBusinessException) {
			logger.LogException(" --- PimsBusinessException --- ", e);
			PimsBusinessException pbe = (PimsBusinessException) e ;
			if (ExceptionCodes.REPORT_NO_RECORDS_FOUND.equals(pbe.getExceptionCode())) {
				session.setAttribute(WebConstants.ERROR,ExceptionCodes.REPORT_NO_RECORDS_FOUND);
			}
		}	
%>				
<body onLoad="javascript:closeWindowSubmit()">
</body>
<%				
	}
%>