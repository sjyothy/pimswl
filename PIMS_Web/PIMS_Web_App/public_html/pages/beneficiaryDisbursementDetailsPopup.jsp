<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for listing disbursement details of person 
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">



			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">





				<tr width="100%">



					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['commons.disbursementDetails']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="searchFrm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$beneficiaryDisbursementDetailsPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">

												<div class="DETAIL_SECTION" style="width: 99.8%;">
													<%--<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>--%>
													<t:div styleClass="contentDiv" style="width:95%">


														<t:dataTable id="disbursementDT" rows="150" width="100%"
															value="#{pages$beneficiaryDisbursementDetailsPopup.list}"
															binding="#{pages$beneficiaryDisbursementDetailsPopup.dataTable}"
															preserveDataModel="false" preserveSort="false"
															var="unitDataItem" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true">
															
															<t:column id="reqNumTd" sortable="true"
																style="white-space: normal;width:3%;">
																<f:facet name="header">
																	<t:outputText id="lblreqNumTd"
																		value="#{msg['request.numberCol']}" />
																</f:facet>
																<t:outputText style="white-space: normal;"
																	id="txtReqNumTd" styleClass="A_LEFT"
																	value="#{unitDataItem.reqNumber}" />
															
															</t:column>
															
															<t:column id="idReqType" sortable="true"
																style="white-space: normal;width:2%;">
																<f:facet name="header">
																	<t:outputText id="hdidReqType"
																		style="white-space: normal;"
																		value="#{msg['request.requestType']}" />
																</f:facet>
																<t:outputText id="hdidReqTypertx" styleClass="A_LEFT"
																	value="#{pages$beneficiaryDisbursementDetailsPopup.englishLocale?unitDataItem.reqTypeEn:unitDataItem.reqTypeAr}" />
															</t:column>
															<t:column id="refNumTd" sortable="true"
																style="white-space: normal;width:3%;">
																<f:facet name="header">
																	<t:outputText id="lblRefNumTd"
																		value="#{msg['commons.refNum']}" />
																</f:facet>
																<t:outputText style="white-space: normal;"
																	id="txtRefNumTd" styleClass="A_LEFT"
																	value="#{unitDataItem.refNum}" />
															</t:column>
															<t:column id="idPmtType" sortable="true"
																style="white-space: normal;width:2%;">
																<f:facet name="header">
																	<t:outputText id="hdPmtType"
																		style="white-space: normal;"
																		value="#{msg['mems.normaldisb.label.paymentType']}" />
																</f:facet>
																<t:outputText id="hdInsPmtType" styleClass="A_LEFT"
																	value="#{pages$beneficiaryDisbursementDetailsPopup.englishLocale?unitDataItem.paymentTypeEn:unitDataItem.paymentTypeAr}" />
															</t:column>
															<t:column id="idCurStatus" sortable="true"
																style="white-space: normal;width:3%;"
																>
																<f:facet name="header">
																	<t:outputText id="hdCurStatus"
																		value="#{msg['commons.status']}" />
																</f:facet>
																<t:outputText id="hdInsCurStatus" styleClass="A_LEFT"
																	value="#{pages$beneficiaryDisbursementDetailsPopup.englishLocale?unitDataItem.statusEn:unitDataItem.statusAr}" />
															</t:column>
															<t:column id="beneficiary" sortable="true"
																style="white-space: normal;width:10%;">
																<f:facet name="header">
																	<t:outputText id="hdBenef"
																		value="#{msg['mems.normaldisb.label.beneficiary']}" />
																</f:facet>
																<t:commandLink
																	actionListener="#{pages$beneficiaryDisbursementDetailsPopup.openManageBeneficiaryPopUp}"
																	value="#{unitDataItem.beneficiariesName}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															<t:column id="amnt" sortable="true"
																style="white-space: normal;width:4%;">
																<f:facet name="header">
																	<t:outputText id="hdAmnt"
																		value="#{msg['commons.amount']}" />
																</f:facet>
																<t:outputText id="amountt" styleClass="A_LEFT"
																	value="#{unitDataItem.amount}" />
															</t:column>
															<t:column id="pmtSrc" sortable="true"
																style="white-space: normal;width:6%;">
																<f:facet name="header">
																	<t:outputText id="hdPmtSrc"
																		value="#{msg['mems.normaldisb.label.paymentSrc']}" />
																</f:facet>
																<t:outputText id="paymentSrc" styleClass="A_LEFT"
																	value="#{pages$beneficiaryDisbursementDetailsPopup.englishLocale?unitDataItem.srcTypeEn:unitDataItem.srcTypeAr}" />
															</t:column>
															<t:column id="desc" sortable="true"
																style="white-space: normal;">
																<f:facet name="header">
																	<t:outputText id="hdTo"
																		value="#{msg['commons.description']}" />
																</f:facet>
																<div style="width: 70%; white-space: normal;">
																	<t:outputText id="p_w" styleClass="A_LEFT"
																		style="white-space: normal;"
																		value="#{unitDataItem.description}" />
																</div>
															</t:column>
															<t:column id="idReason" sortable="true"
																style="white-space: normal;width:8%;">
																<f:facet name="header">
																	<t:outputText id="hdReason"
																		value="#{msg['commons.reasons']}" />
																</f:facet>
																<t:outputText id="idInsReason" styleClass="A_LEFT"
																	value="#{pages$beneficiaryDisbursementDetailsPopup.englishLocale?unitDataItem.reasonEn:unitDataItem.reasonAr}" />
															</t:column>
															<t:column id="idItems" sortable="true"
																style="white-space: normal;width:8%;">
																<f:facet name="header">
																	<t:outputText id="hdItems"
																		value="#{msg['mems.normaldisb.label.items']}" />
																</f:facet>
																<t:outputText id="hdInsItems" styleClass="A_LEFT"
																	value="#{pages$beneficiaryDisbursementDetailsPopup.englishLocale?unitDataItem.itemEn:unitDataItem.itemAr}" />
															</t:column>
															<t:column id="idPaidTo" sortable="true"
																style="white-space: normal;width:5%;">
																<f:facet name="header">
																	<t:outputText id="hdPaidTo"
																		value="#{msg['mems.normaldisb.label.paidTo']}" />
																</f:facet>
																<div style="width: 70%; white-space: normal;">
																	<t:outputText id="hdInsPaidTo" styleClass="A_LEFT"
																		value="#{'0'==unitDataItem.paidTo?unitDataItem.vendorName:msg['mems.normaldisb.label.msg.paidToBenef']}" />
																</div>
															</t:column>
															<t:column id="idDisbSrcName" sortable="true"
																style="white-space: normal;width:6%;">
																<f:facet name="header">
																	<t:outputText id="hdDisbSrcName"
																		value="#{msg['mems.normaldisb.label.disbSrc']}" />
																</f:facet>
																<t:outputText id="hdInsDisbSrcName" styleClass="A_LEFT"
																	value="#{pages$beneficiaryDisbursementDetailsPopup.englishLocale?unitDataItem.disburseSrcNameEn:unitDataItem.disburseSrcNameAr}" />
															</t:column>




														</t:dataTable>
													</t:div>
												</div>
											</div>

										</div>


										</div>
									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>


		</body>
	</html>
</f:view>