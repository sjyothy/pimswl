<%-- 
  - Author: Munit Chagani
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Auction
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		
	function setFolderName(){
		var skillsSelect = document.getElementById("searchFrm:cmbParentFolders");
		if(skillsSelect.options[skillsSelect.selectedIndex].value != -1)
			document.getElementById("searchFrm:txtObjectStoreName").value = skillsSelect.options[skillsSelect.selectedIndex].innerHTML;
	}		  
	
	function performSave(control)
	{
			

		document.getElementById("searchFrm:lnkSave").onclick();
		return;
		
	}
	
	function resetFormValues(){
		resetForm();
	
	}
	
	function resetForm(){
	
			document.getElementById("searchFrm:txtDocClass").value="";
			document.getElementById("searchFrm:txtMetaColumns").value="";
			document.getElementById("searchFrm:txtMetaColumnValues").value="";
			document.getElementById("searchFrm:txtObjectStoreName").value="";
			document.getElementById("searchFrm:cmbParentFolders").selectedIndex=0;
			document.getElementById("searchFrm:txtDocSearchQuery").value="";
			document.getElementById("searchFrm:txtDocSearchWhereClause").value="";
			document.getElementById("searchFrm:txtFolderName").value="";
			document.getElementById("searchFrm:txtMetaQuery").value="";
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
		<div class="containerDiv">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>
				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['paymentConfiguration.ListTitle']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
									<div class="SCROLLABLE_SECTION AUC_SCH_SS">
										<h:form id="searchFrm" style="WIDTH: 97.6%;">
											<div style="height: 25px">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:messages></h:messages>
															<h:outputText value="#{pages$fileNetConfiguration.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
															<h:outputText value="#{pages$fileNetConfiguration.successMessages}"  escape="false" styleClass="INFO_FONT"/>
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN">
												<div>

													<table cellpadding="1px" cellspacing="2px">
														<tr>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['feeConfiguration.procedureType']}:"></h:outputLabel>
															</td>

															<td width="25%">
																<h:selectOneMenu id="selectProcedureType" style="width:250px;"
																	value="#{pages$fileNetConfiguration.procedureTypeVO.externalId}"
																	onchange="javascript: resetFormValues();" 
																	>
																	<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
																								  itemValue="-1" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.fileNetProcedureType}" />
																</h:selectOneMenu>

															</td>
															<td width="25%">
																<h:commandLink 
																	action="#{pages$fileNetConfiguration.btnSearchClick}">
																	<h:graphicImage 
																		title="#{msg['paymentConfiguration.getPayments']}"
																		url="../resources/images/magnifier.gif" />
																</h:commandLink>

															</td>
															<td width="25%">

															</td>
														</tr>

														<tr>
															<td width="25%">

															</td>
															<td width="25%">

															</td>
															<td style="PADDING: 5px; PADDING-TOP: 5px;">

															</td>
														</tr>
													</table>
												</div>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">

												<div >
												
													<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER" width="100%">
																		
																		<tr>
																			
																			<td width="25%">
																				<h:outputLabel
																					value="#{msg['procedureType.lbl.objectStoreName']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="txtObjectStoreName"
																					value="#{pages$fileNetConfiguration.procedureTypeVO.objectStoreName}">
																				</h:inputText>
																			</td>
																			
																			
																			

																			<td >
																				<h:selectOneMenu id="cmbParentFolders" style="width:250px;" 
																								
																					value="#{pages$fileNetConfiguration.procedureTypeVO.selectedFolderName}">
																					<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
																								  itemValue="-1" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.fileNetFolderName}" />
																				</h:selectOneMenu>
																				</td>
																				<td>
																				<h:commandButton type="BUTTON" styleClass="BUTTON"
																					value="#{msg['commons.add']}"
																					onclick="javascript:setFolderName();" style="width: 75px" />
				
																			</td>
																			
																		
																		</tr>
																		
																		<tr>
																			<td width="25%">
																				<h:outputLabel
																					value="#{msg['procedureType.lbl.docClass']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="txtDocClass"
																					value="#{pages$fileNetConfiguration.procedureTypeVO.docClass}">
																				</h:inputText>
																			</td>
																			<td >
																				<h:outputLabel styleClass="LABEL" 
																							   value="eg: {78BB9C12-9A4C-4B3C-BCD5-3E1AFE7C2D64} OR /MinorArchive ">
																				</h:outputLabel>
																			</td>
																		</tr>
																		
																		<tr>
																			<td width="25%">
																				<h:outputLabel
																					value="#{msg['procedureType.lbl.metaColumns']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="txtMetaColumns"
																					value="#{pages$fileNetConfiguration.procedureTypeVO.metaColumns}" />
																			</td>
																			
																			<td >
																				<h:outputLabel styleClass="LABEL" 
																							   value="eg: FileNumber ">
																				</h:outputLabel>
																			</td>

																			

																		</tr>
																		
																		<tr>
																			
																			<td width="25%">
																				<h:outputLabel
																					value="#{msg['procedureType.lbl.metaColumnsValue']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputTextarea id="txtMetaColumnValues" rows="10" cols="50"   
																					value="#{pages$fileNetConfiguration.procedureTypeVO.metaColumnsValue}">
																				</h:inputTextarea>
																			</td>
																			
																			<td >
																				<h:outputLabel styleClass="LABEL" 
																							   value="eg: select FILE_NUMBER from inheritance_file where inheritance_file_id=">
																				</h:outputLabel>
																			</td>
																			 
																		
																		</tr>
																		
																		
																		
																		
																		
																		<tr>
																			
																			<td width="25%">
																				<h:outputLabel
																					value="#{msg['procedureType.lbl.docSearchQuery']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputTextarea id="txtDocSearchQuery" rows="10" cols="50" 
																					value="#{pages$fileNetConfiguration.procedureTypeVO.docSearchQuery}">
																				</h:inputTextarea>
																			</td>
																			
																			<td >
																				<h:outputLabel styleClass="LABEL" 
																							   value="e.g: select '[FileNumber]=''' ||FILE_NUMBER ||'''' from inheritance_file where inheritance_file_id=">
																				</h:outputLabel>
																			</td>
																		
																		</tr>
																		
																		<tr>
																			
																			<td width="25%">
																				<h:outputLabel
																					value="#{msg['procedureType.lbl.docSearchWhereClause']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputTextarea id="txtDocSearchWhereClause" rows="10" cols="50" 
																					value="#{pages$fileNetConfiguration.procedureTypeVO.docSearchWhereClause}">
																				</h:inputTextarea>
																			</td>
																			
																			<td >
																				<h:outputLabel styleClass="LABEL" 
																							   value="e.g: SELECT * FROM [MinorsArchive] WHERE ">
																				</h:outputLabel>
																			</td>
																		
																		</tr>
																		
																		<tr>
																			
																			<td width="25%">
																				<h:outputLabel
																					value="#{msg['procedureType.lbl.folderName']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="txtFolderName"
																					value="#{pages$fileNetConfiguration.procedureTypeVO.folderName}">
																				</h:inputText>
																			</td>
																			<td >
																				<h:outputLabel styleClass="LABEL" 
																							   value="e.g: Test">
																				</h:outputLabel>
																			</td>
																		</tr>
																		
																		<tr>
																			
																			<td width="25%">
																				<h:outputLabel
																					value="#{msg['procedureType.lbl.metaQuery']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputTextarea id="txtMetaQuery" rows="10" cols="50"   
																					value="#{pages$fileNetConfiguration.procedureTypeVO.metaQuery}">
																				</h:inputTextarea>
																			</td>
																			<td >
																				<h:outputLabel styleClass="LABEL" 
																							   value="e.g: select request_number from request where request_id = {0}">
																				</h:outputLabel>
																			</td>
																		
																		</tr>
																		
																		<tr>
																			<td></td>
																			<td></td>
																			<td></td>
																			<td>
																				
																				<h:commandButton styleClass="BUTTON"
																								 value="#{msg['commons.saveButton']}"
																								 action="#{pages$fileNetConfiguration.saveRecord}">
																				</h:commandButton>
																								 
																										 
																			</td>
																		</tr>
													
													</table>
												
												</div>

											</div>
										</h:form>
									</div>




								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
	</html>
</f:view>