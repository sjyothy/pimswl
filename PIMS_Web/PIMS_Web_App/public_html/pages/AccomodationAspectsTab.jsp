<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%-- Please remove above taglibs after completing your tab contents--%>
<t:div styleClass="contentDiv" style="width:95%">
<t:dataTable id="historyGrid"
																				value="#{pages$AccomodationAspectsTab.dataListResearchGridView}"
																				binding="#{pages$AccomodationAspectsTab.dataTableResearchGrid}"
																				width="100%"
																				rows="#{pages$PeriodicDisbursements.paginatorRows}"
																				preserveDataModel="false" preserveSort="false"
																				var="dataItem" rowClasses="row1,row2" rules="all"
																				renderedIfEmpty="true">

																				<%--<t:column id="selectMany" width="10%"
																					sortable="false" style="white-space: normal;">
																					<f:facet name="header">
																						<t:outputText value="#{msg['commons.select']}" />
																					</f:facet>
																					<h:selectBooleanCheckbox id="select"
																						value="#{dataItem.selected}" />
																				</t:column>--%>
																				<t:column id="beneficiaryOfProperty" width="10%"
																					sortable="false" style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['ResearchFormBenef.beneficiaryOfProp']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.beneficiaryOfPropertyDesc}"
																						style="white-space: normal;" styleClass="A_LEFT" />

																				</t:column>

																				<t:column  id="homeType" width="10%"
																					defaultSorted="true" style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['ResearchFormBenef.typeOfHome']}" />

																					</f:facet>
																					<t:outputText value="#{dataItem.typesOfHomeCSV}"
																						style="white-space: normal;"  />

																				</t:column>
																				<t:column  id="accountType" width="10%"
																					defaultSorted="true" style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['ResearchFormBenef.conditionOfHome']} " />

																					</f:facet>
																					<t:outputText value="#{dataItem.conditionOfHomeDesc}"
																						style="white-space: normal;" styleClass="A_LEFT" />

																				</t:column>

																				<t:column id="accountOwnership" width="10%"
																					sortable="false" style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['ResearchFormBenef.accOwnership']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.accomodationOwnership}"
																						style="white-space: normal;" styleClass="A_LEFT" />

																				</t:column>
																				<t:column id="accomodationType" width="10%"
																					sortable="false" style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['ResearchFormBenef.accType']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.accomodationType}"
																						style="white-space: normal;" styleClass="A_LEFT" />

																				</t:column>
																			<t:column id="resTypeDesc" width="10%"
																					sortable="false" style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['ResearchFormBenef.resType']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.resTypeDesc}"
																						style="white-space: normal;" styleClass="A_LEFT" />

																				</t:column>
																				<t:column id="date" width="10%"
																					sortable="false" style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['commons.date']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.createdOn}"
																						style="white-space: normal;" styleClass="A_LEFT" />

																				</t:column>
																				<t:column id="rentingReason" width="10%"
																					sortable="false" style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['ResearchFormBenef.reasonForRentingFlat']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.reasonForRentDesc}"
																						style="white-space: normal;" styleClass="A_LEFT" />

																				</t:column>
																				
																			</t:dataTable>			
</t:div>
<%--Column 3,4 Ends--%>
