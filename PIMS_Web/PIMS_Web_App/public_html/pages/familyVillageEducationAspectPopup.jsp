<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
		
	function closeWindow()
	{
		 window.opener.onMessageFromAddEducationAspectPopup();
		 window.close();
		 
	}
	        
	function disableButtons(control)
	{
			
			var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
			
			control.nextSibling.nextSibling.onclick();
			
			return 
		
	}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">

				<tr width="100%">
					<td width="100%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">

									<h:outputLabel value="#{msg['educationAspects.tabName']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="frm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION" style="width: 95%;">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:messages></h:messages>
															<h:outputText
																value="#{pages$familyVillageEducationAspectPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$familyVillageEducationAspectPopup.successMessages}"
																escape="false" styleClass="INFO_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">

												<t:panelGrid id="tabBDetailsab" cellpadding="5px"
													width="100%" cellspacing="10px" columns="4"
													styleClass="TAB_DETAIL_SECTION_INNER"
													columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

													<h:outputLabel
														value="#{msg['educationAspects.instituteName']} :"
														style="font-weight:normal;" styleClass="TABLE_LABEL" />
													<t:panelGroup colspan="3">
														<h:inputText
															value="#{pages$familyVillageEducationAspectPopup.pageView.instituteName}"></h:inputText>
													</t:panelGroup>

													<h:panelGroup>
														<%--<h:outputLabel styleClass="mandatory" value="*" />--%>
														<h:outputLabel id="gradeLbl" styleClass="LABEL"
															value="#{msg['educationAspects.grade']}:"></h:outputLabel>
													</h:panelGroup>
													<h:selectOneMenu id="Grade"
														value="#{pages$familyVillageEducationAspectPopup.pageView.gradeId}">
														<f:selectItem itemValue="-1"
															itemLabel="#{msg['commons.combo.PleaseSelect']}" />
														<f:selectItems value="#{pages$ApplicationBean.allGrades}" />
													</h:selectOneMenu>

													<h:outputLabel
														value="#{msg['educationAspects.gradeDesc']} :"
														style="font-weight:normal;" styleClass="TABLE_LABEL" />
													<h:inputTextarea
														onkeyup="javaScript:removeExtraCharacter(this,50)"
														onkeypress="javaScript:removeExtraCharacter(this,50)"
														onkeydown="javaScript:removeExtraCharacter(this,50)"
														onblur="javaScript:removeExtraCharacter(this,50)"
														onchange="javaScript:removeExtraCharacter(this,50)"
														value="#{pages$familyVillageEducationAspectPopup.pageView.gradeDesc}" />

													<h:outputLabel value="#{msg['educationAspects.level']} :"
														style="font-weight:normal;" styleClass="TABLE_LABEL" />
													<h:selectOneMenu id="level"
														value="#{pages$familyVillageEducationAspectPopup.pageView.educationLevelId}">
														<f:selectItem itemValue="-1"
															itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
														<f:selectItems
															value="#{pages$ApplicationBean.allEducationLevel}" />
													</h:selectOneMenu>

													<h:outputLabel
														value="#{msg['educationAspects.educationLevelDesc']} :"
														style="font-weight:normal;" styleClass="TABLE_LABEL" />
													<h:inputTextarea
														onkeyup="javaScript:removeExtraCharacter(this,500)"
														onkeypress="javaScript:removeExtraCharacter(this,500)"
														onkeydown="javaScript:removeExtraCharacter(this,500)"
														onblur="javaScript:removeExtraCharacter(this,500)"
														onchange="javaScript:removeExtraCharacter(this,500)"
														value="#{pages$familyVillageEducationAspectPopup.pageView.educationLevelDesc}" />

													<h:outputLabel
														value="#{msg['researchFamilyVillageBeneficiary.lbl.EducationAspect.Attendancestatus']} :"
														style="font-weight:normal;" styleClass="TABLE_LABEL" />
													<h:selectOneMenu id="attendanceStatus"
														value="#{pages$familyVillageEducationAspectPopup.pageView.attendanceStatusId}">
														<f:selectItem itemValue="-1"
															itemLabel="#{msg['commons.combo.PleaseSelect']}" />
														<f:selectItems
															value="#{pages$ApplicationBean.attendanceStatusList}" />
													</h:selectOneMenu>
													<h:outputLabel value="#{msg['commons.description']} :"
														style="font-weight:normal;" styleClass="TABLE_LABEL" />
													<h:inputTextarea
														onkeyup="javaScript:removeExtraCharacter(this,500)"
														onkeypress="javaScript:removeExtraCharacter(this,500)"
														onkeydown="javaScript:removeExtraCharacter(this,500)"
														onblur="javaScript:removeExtraCharacter(this,500)"
														onchange="javaScript:removeExtraCharacter(this,500)"
														value="#{pages$familyVillageEducationAspectPopup.pageView.attendanceStatusDesc}" />

													<h:outputLabel
														value="#{msg['researchFamilyVillageBeneficiary.lbl.EducationAspect.NoOfYearsFailed']} :"
														style="font-weight:normal;" styleClass="TABLE_LABEL" />
													<h:inputText onkeyup="javaScript:removeNonInteger(this)"
														onkeypress="javaScript:removeNonInteger(this)"
														onkeydown="javaScript:removeNonInteger(this)"
														onblur="javaScript:removeNonInteger(this)"
														onchange="javaScript:removeNonInteger(this)"
														value="#{pages$familyVillageEducationAspectPopup.pageView.noOfYearsFailed}"></h:inputText>
													<h:outputLabel value="#{msg['commons.description']} :"
														style="font-weight:normal;" styleClass="TABLE_LABEL" />
													<h:inputTextarea
														onkeyup="javaScript:removeExtraCharacter(this,500)"
														onkeypress="javaScript:removeExtraCharacter(this,500)"
														onkeydown="javaScript:removeExtraCharacter(this,500)"
														onblur="javaScript:removeExtraCharacter(this,500)"
														onchange="javaScript:removeExtraCharacter(this,500)"
														value="#{pages$familyVillageEducationAspectPopup.pageView.noOfYearsFailedDesc}" />

													<h:outputLabel
														value="#{msg['researchFamilyVillageBeneficiary.lbl.EducationAspect.EnrollmentYear']} :"
														style="font-weight:normal;" styleClass="TABLE_LABEL" />
													<h:inputText onkeyup="javaScript:removeNonInteger(this)"
														onkeypress="javaScript:removeNonInteger(this)"
														onkeydown="javaScript:removeNonInteger(this)"
														onblur="javaScript:removeNonInteger(this)"
														onchange="javaScript:removeNonInteger(this)"
														value="#{pages$familyVillageEducationAspectPopup.pageView.enrollmentAge}"></h:inputText>
													<h:outputLabel value="#{msg['commons.description']} :"
														style="font-weight:normal;" styleClass="TABLE_LABEL" />
													<h:inputTextarea
														onkeyup="javaScript:removeExtraCharacter(this,500)"
														onkeypress="javaScript:removeExtraCharacter(this,500)"
														onkeydown="javaScript:removeExtraCharacter(this,500)"
														onblur="javaScript:removeExtraCharacter(this,500)"
														onchange="javaScript:removeExtraCharacter(this,500)"
														value="#{pages$familyVillageEducationAspectPopup.pageView.enrollementAgeDesc}" />

													<h:outputLabel styleClass="LABEL"
														value="#{msg['educationAspect.lbl.isDropout']}:" />
													<h:selectBooleanCheckbox
														style="width: 15%;height: 22px; border: 0px;"
														value="#{pages$familyVillageEducationAspectPopup.isDropOut}">
													</h:selectBooleanCheckbox>
													<h:outputLabel value="#{msg['commons.description']} :"
														style="font-weight:normal;" styleClass="TABLE_LABEL" />
													<h:inputTextarea
														onkeyup="javaScript:removeExtraCharacter(this,500)"
														onkeypress="javaScript:removeExtraCharacter(this,500)"
														onkeydown="javaScript:removeExtraCharacter(this,500)"
														onblur="javaScript:removeExtraCharacter(this,500)"
														onchange="javaScript:removeExtraCharacter(this,500)"
														value="#{pages$familyVillageEducationAspectPopup.pageView.isDropOutDesc}" />

												</t:panelGrid>

											</div>
											<t:div styleClass="BUTTON_TD"
												style="padding-top:10px; padding-right:21px;">

												<h:commandButton styleClass="BUTTON"
													value="#{msg['commons.saveButton']}"
													action="#{pages$familyVillageEducationAspectPopup.onSave}">
												</h:commandButton>
												<h:commandButton styleClass="BUTTON" type="button"
													value="#{msg['commons.done']}"
													onclick="javaScript:closeWindow();">
												</h:commandButton>
											</t:div>



										</div>
									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>
		</body>
	</html>
</f:view>