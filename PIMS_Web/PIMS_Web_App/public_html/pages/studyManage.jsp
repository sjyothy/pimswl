<%-- 
  - Author: Anil Verani
  - Date: 03/07/2009
  - Copyright Notice:
  - @(#)
  - Description: Used for Adding, Updating and Viewing Intial / Feasibility Studies 
  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

	function openSearchInvestmentPlanPopup()
	{
		var screen_width = 980;
		var screen_height = 350;
        var popup_width = screen_width;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('strategicPlanSearch.jsf?PAGE_MODE=MODE_SELECT_ONE_POPUP','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
	
	function receiveSelectedStrategicPlanView()
	{
		document.forms[0].submit();
	}
	
	function receiveSelectedStudies()
	{	
		document.getElementById('frm:lnkReceiveSelectedStuides').onclick();
	}	
	
	function openStudyViewPopup() 
	{
		var screen_width = 980;
		var screen_height = 350;
        var popup_width = screen_width;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('studyManage.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
	}	
	
	function openStudySearchPopup()
	{
		var screen_width = 990;
		var screen_height = 475;
        var popup_width = screen_width;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('studySearch.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
        
	}
	
	function openRecommendationManagePopup()
	{
		var screen_width = 980;
		var screen_height = 300;
        var popup_width = screen_width;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('recommendationManage.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
	
	function receiveRecommendation()
	{
		document.getElementById('frm:lnkReceiveRecommendation').onclick();
	}
	
	function SearchMemberPopup(){
			   var screen_width = 1024;
			   var screen_height = 450;
			   var popup_width = screen_width-150;
		       var popup_height = screen_height-50;
		       var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		       var popup = window.open('SearchMemberPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
		       popup.focus();
			}
	function showExternalMemberPopUp()
	  {
    	var screen_width = 1024;
	    var screen_height = 450;
	    window.open('SearchPerson.jsf?viewMode=popup&selectMode=many' ,'_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	  }
		function showUploadPopup()
		{
		      var screen_width = 1024;
		      var screen_height = 450;
		      var popup_width = screen_width-530;
		      var popup_height = screen_height-150;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
						<c:when test="${! pages$studyManage.isPopupViewOnlyMode}">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</c:when>
					</c:choose>
				</tr>

				<tr>
					<c:choose>
						<c:when test="${! pages$studyManage.isPopupViewOnlyMode}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['study.manage.heading']}" styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" >

										<h:form id="frm" style="width:97%" enctype="multipart/form-data">
											<table border="0" class="layoutTable">

												<tr>

													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$studyManage.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$studyManage.successMessages}" />
													</td>
												</tr>
											</table>
											
											<t:div rendered="#{pages$studyManage.isApproveRejectStudyMode}" style="width:97%; margin:10px;">
												<t:panelGrid cellpadding="1px" width="100%" cellspacing="5px" columns="4">																
													<h:outputLabel styleClass="LABEL" value="#{msg['commons.approval.date']}: "/>
											        <rich:calendar inputStyle="width: 186px; height: 18px" locale="#{pages$studyManage.locale}" popup="true" datePattern="#{pages$studyManage.dateFormat}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px"/>
											                    
													<h:outputLabel styleClass="LABEL"  value="#{msg['commons.comments']}: " />
													<t:inputTextarea styleClass="TEXTAREA" value="#{pages$studyManage.remarks}" rows="6" style="width: 200px; height:30px;" />
																
													<t:div style="height:10px;"/>
											   	</t:panelGrid>
											   	<table border="0" width="100%">
											   		<tr>
											   			<td class="BUTTON_TD">
											   				<h:commandButton value="#{msg['commons.approve']}" action="#{pages$studyManage.onTaskApprove}" styleClass="BUTTON"></h:commandButton>
											   				<h:commandButton value="#{msg['commons.reject']}" action="#{pages$studyManage.onTaskReject}" styleClass="BUTTON"></h:commandButton>
											   				<h:commandButton value="#{msg['commons.cancel']}" action="#{pages$studyManage.onTaskCancel}" styleClass="BUTTON"></h:commandButton>
											   			</td>
											   		</tr>
											   	</table>
											</t:div>
											
											<t:div rendered="#{pages$studyManage.isCompleteStudyMode}" style="width:97%; margin:10px;">
												<t:panelGrid cellpadding="1px" width="100%" cellspacing="5px" columns="4">																
													<h:outputLabel styleClass="LABEL" value="#{msg['commons.completion.date']}: "/>
											        <rich:calendar inputStyle="width: 186px; height: 18px" locale="#{pages$studyManage.locale}" popup="true" datePattern="#{pages$studyManage.dateFormat}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px"/>
											                    
													<h:outputLabel styleClass="LABEL"  value="#{msg['commons.comments']}: " />
													<t:inputTextarea styleClass="TEXTAREA" value="#{pages$studyManage.remarks}" rows="6" style="width: 200px; height:30px;" />
																
													<t:div style="height:10px;"/>
											   	</t:panelGrid>
											   	<table border="0" width="100%">
											   		<tr>
											   			<td class="BUTTON_TD">
											   				<h:commandButton value="#{msg['commons.ok']}" action="#{pages$studyManage.onTaskComplete}" styleClass="BUTTON"></h:commandButton>											   				
											   				<h:commandButton value="#{msg['commons.cancel']}" action="#{pages$studyManage.onTaskCancel}" styleClass="BUTTON"></h:commandButton>
											   			</td>
											   		</tr>
											   	</table>
											</t:div>
											
											<t:div rendered="#{pages$studyManage.isAddToInvestmentPlanMode}" style="width:97%; margin:10px;">
												<t:panelGrid cellpadding="1px" width="100%" cellspacing="5px" columns="4">																
													<h:outputLabel styleClass="LABEL" value="#{msg['BPM.WorkList.PIMSStrategicPlanBPEL.RequestTitle']}: "/>
											        <h:panelGroup>
												        <h:inputText value="#{pages$studyManage.strategicPlanStudyView.strategicPlanView.strategicPlanNumber}" style="width: 186px;" readonly="true" />
												        <h:outputLink onclick="openSearchInvestmentPlanPopup(); return false;">
												        	<h:graphicImage style="vertical-align: bottom;" title="#{msg['strategicPlanSearch.header']}" url="../resources/images/magnifier.gif" />
												        </h:outputLink>
											        </h:panelGroup>
											                    
													<h:outputLabel styleClass="LABEL"  value="#{msg['commons.comments']}: " />
													<t:inputTextarea styleClass="TEXTAREA" value="#{pages$studyManage.remarks}" rows="6" style="width: 200px; height:30px;" />
																
													<t:div style="height:10px;"/>
											   	</t:panelGrid>
											   	<table border="0" width="100%">
											   		<tr>
											   			<td class="BUTTON_TD">
											   				<h:commandButton value="#{msg['commons.Add']}" action="#{pages$studyManage.onAddToInvesmentPlan}" styleClass="BUTTON"></h:commandButton>											   				
											   				<h:commandButton value="#{msg['commons.cancel']}" action="#{pages$studyManage.onCancel}" styleClass="BUTTON"></h:commandButton>
											   			</td>
											   		</tr>
											   	</table>
											</t:div>

											<div class="MARGIN" style="width: 98%">
												
												<table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
										        </table>
												
												<rich:tabPanel style="width:100%;height:235px;" headerSpacing="0">
												
														<!-- Study Details Tab - Start -->
														<rich:tab label="#{msg['study.manage.tab.studyDetails.heading']}">
															<t:div  style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid id="followUpDetailsTable" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="2px" columns="4">																	
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['study.number']}:"></h:outputLabel>
																	<h:inputText value="#{pages$studyManage.studyDetailView.studyNumber}"  maxlength="20" styleClass="READONLY" readonly="true" />
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['study.status']}:"></h:outputLabel>
																	<h:selectOneMenu readonly="true" styleClass="READONLY" value="#{pages$studyManage.studyDetailView.statusId}" >
																		<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
																		<f:selectItems value="#{pages$ApplicationBean.studyStatusList}" />
																	</h:selectOneMenu>
																	
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['study.startDate']}:"></h:outputLabel>
																	</h:panelGroup>
																	<rich:calendar value="#{pages$studyManage.studyDetailView.startDate}"
																				   inputStyle="width: 170px; height: 14px"
											                        			   locale="#{pages$studyManage.locale}" 
											                        			   popup="true" 
											                        			   datePattern="#{pages$studyManage.dateFormat}" 
											                        			   showApplyButton="false" 
											                        			   enableManualInput="false" 
											                        			  />
											                        			   
											                        <h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
											                        	<h:outputLabel styleClass="LABEL" value="#{msg['study.endDate']}:" />
											                        </h:panelGroup>
											                        <rich:calendar value="#{pages$studyManage.studyDetailView.endDate}"
																				   inputStyle="width: 170px; height: 14px"
											                        			   locale="#{pages$studyManage.locale}" 
											                        			   popup="true" 
											                        			   datePattern="#{pages$studyManage.dateFormat}" 
											                        			   showApplyButton="false" 
											                        			   enableManualInput="false" 
											                        			   />
											                        			   
											                        <h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
											                        	<h:outputLabel styleClass="LABEL" value="#{msg['study.type']}:"></h:outputLabel>
											                        </h:panelGroup>
											                        <h:selectOneMenu value="#{pages$studyManage.studyDetailView.studyTypeId}" >
																		<f:selectItem itemValue="" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																		<f:selectItems value="#{pages$ApplicationBean.studyTypeList}" />
																	</h:selectOneMenu>
																	
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['study.subject']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:selectOneMenu value="#{pages$studyManage.studyDetailView.studySubjectId}" >
																		<f:selectItem itemValue="" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																		<f:selectItems value="#{pages$ApplicationBean.studySubjectList}" />
																	</h:selectOneMenu>
																	
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['study.title']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText value="#{pages$studyManage.studyDetailView.studyTitle}"  maxlength="50" />
																	
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['study.purpose']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText value="#{pages$studyManage.studyDetailView.studyPurpose}" maxlength="50" />
																	
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['study.requestedBy']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputText value="#{pages$studyManage.studyDetailView.requestedBy}"  maxlength="50" />
																	
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
																		<h:outputLabel styleClass="LABEL" value="#{msg['study.description']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:inputTextarea value="#{pages$studyManage.studyDetailView.description}"  styleClass="TEXTAREA"/>
																	
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Study Details Tab - End -->
														
														<!-- Related Studies Tab - Start -->
														<rich:tab label="#{msg['study.manage.tab.relatedStudies.heading']}">														
															
															<h:panelGrid rendered="#{! (pages$studyManage.isPopupViewOnlyMode || pages$studyManage.isViewMode || pages$studyManage.isApproveRejectStudyMode || pages$studyManage.isCompleteStudyMode || pages$studyManage.isAddToInvestmentPlanMode)}" styleClass="BUTTON_TD" width="100%">
																<h:commandButton styleClass="BUTTON"
																		value="#{msg['study.manage.tab.relatedStudies.btn.addStudies']}"
																		action="#{pages$studyManage.onAddStudies}">
																</h:commandButton>
																
																<a4j:commandLink id="lnkReceiveSelectedStuides" action="#{pages$studyManage.receiveSelectedStudies}" reRender="dataTable,gridInfo" />
																	
																<%-- <h:commandLink id="lnkReceiveSelectedStuides" action="#{pages$studyManage.receiveSelectedStudies}" /> --%>
															</h:panelGrid>
																													
															<t:div styleClass="contentDiv" style="width:99%">																
						                                       <t:dataTable id="dataTable"  
																	rows="#{pages$studyManage.paginatorRows}"
																	value="#{pages$studyManage.relatedStudiesViewList}"
																	binding="#{pages$studyManage.relatedStudiesDataTable}"																	
																	preserveDataModel="false" preserveSort="false" var="dataItem"
																	rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
																																		
																	<t:column width="15%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['study.number']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.studyByChildStudyId.studyNumber}" rendered="#{dataItem.isDeleted == 0}" />																		
																	</t:column>
																	<t:column width="10%" sortable="true">
																		<f:facet  name="header">
																			<t:outputText value="#{msg['study.startDate']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.studyByChildStudyId.startDate}" rendered="#{dataItem.isDeleted == 0}">
																			<f:convertDateTime timeZone="#{pages$studyManage.timeZone}" pattern="#{pages$studyManage.dateFormat}" />
																		</t:outputText>
																	</t:column>
																	<t:column width="15%" sortable="true">
																		<f:facet  name="header">
																			<t:outputText value="#{msg['study.endDate']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.studyByChildStudyId.endDate}" rendered="#{dataItem.isDeleted == 0}">
																			<f:convertDateTime timeZone="#{pages$studyManage.timeZone}" pattern="#{pages$studyManage.dateFormat}" />
																		</t:outputText>
																	</t:column>
																	<t:column width="12%" sortable="true" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.typeCol']}" />
																		</f:facet>
																		<t:outputText value="#{pages$studyManage.isEnglishLocale ? dataItem.studyByChildStudyId.studyTypeEn : dataItem.studyByChildStudyId.studyTypeAr}" rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>												
																	<t:column width="13%" sortable="true" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['TaskList.DataTable.Subject']}" />
																		</f:facet>
																		<t:outputText value="#{pages$studyManage.isEnglishLocale ? dataItem.studyByChildStudyId.studySubjectEn : dataItem.studyByChildStudyId.studySubjectAr}" rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>												
																	<t:column width="10%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['bidder.title']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.studyByChildStudyId.studyTitle}" rendered="#{dataItem.isDeleted == 0}" />																		
																	</t:column>												
																	<t:column width="10%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.status']}" />
																		</f:facet>
																		<t:outputText value="#{pages$studyManage.isEnglishLocale ? dataItem.studyByChildStudyId.statusEn : dataItem.studyByChildStudyId.statusAr}" rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	
																	<t:column rendered="#{! (pages$studyManage.isPopupViewOnlyMode || pages$studyManage.isViewMode || pages$studyManage.isApproveRejectStudyMode || pages$studyManage.isCompleteStudyMode || pages$studyManage.isAddToInvestmentPlanMode)}" sortable="false" width="15%">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<t:commandLink action="#{pages$studyManage.onStudyView}" rendered="#{dataItem.isDeleted == 0}">
																				<h:graphicImage title="#{msg['commons.view']}" url="../resources/images/app_icons/Approve-Request.png" />&nbsp;
																		</t:commandLink>
																		
																		<t:outputLabel value=" " rendered="#{dataItem.isDeleted == 0}"></t:outputLabel>
																		
																		<%-- <t:commandLink onclick="if (!confirm('#{msg['study.confirm.delete']}')) return" action="#{pages$studyManage.onStudyDelete}" rendered="#{dataItem.isDeleted == 0}">																			
																			<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
																		</t:commandLink> --%>
																		
																		<a4j:commandLink onclick="if (!confirm('#{msg['study.confirm.delete']}')) return" action="#{pages$studyManage.onStudyDelete}" rendered="#{dataItem.isDeleted == 0}" reRender="dataTable,gridInfo">
																			<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
																		</a4j:commandLink>
																															
																	</t:column>
																	
																  </t:dataTable>										
																</t:div>
														
				                                           		<t:div id="gridInfo" styleClass="contentDivFooter" style="width:100%">
																			<t:panelGrid id="rqtkRecNumTable"  columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
																				<t:div id="rqtkRecNumDiv"  styleClass="RECORD_NUM_BG">
																					<t:panelGrid id="rqtkRecNumTbl" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
																						<h:outputText value="#{msg['commons.recordsFound']}"/>
																						<h:outputText value=" : "/>
																						<h:outputText value="#{pages$studyManage.relatedStudiesRecordSize}"/>																						
																					</t:panelGrid>
																				</t:div>
																				  
																			      <CENTER>
																				   <t:dataScroller id="rqtkscroller" for="dataTable" paginator="true"
																					fastStep="1"  paginatorMaxPages="#{pages$studyManage.paginatorMaxPages}" immediate="false"
																					paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true" 
																				    pageIndexVar="pageNumber"
																				    styleClass="SCH_SCROLLER"
																				    paginatorActiveColumnStyle="font-weight:bold;"
																				    paginatorRenderLinkForActive="false" 
																					paginatorTableStyle="grid_paginator" layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">
								
																					    <f:facet  name="first">
																							<t:graphicImage    url="../#{path.scroller_first}"  id="lblFRequestHistory"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastrewind">
																							<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRRequestHistory"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastforward">
																							<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFRequestHistory"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="last">
																							<t:graphicImage  url="../#{path.scroller_last}"  id="lblLRequestHistory"></t:graphicImage>
																						</f:facet>
									
								                                                      <t:div id="rqtkPageNumDiv"   styleClass="PAGE_NUM_BG">
																					   <t:panelGrid id="rqtkPageNumTable"  styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0" >
																					 					<h:outputText  styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																										<h:outputText   styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																						</t:panelGrid>
																							
																						</t:div>
																				 </t:dataScroller>
																				 </CENTER>
																		      
																	        </t:panelGrid>
								                          			 </t:div>														
															</rich:tab>
														<!-- Related Studies Tab - End -->
														
														<!-- Study Participant Tab - Start -->
														<rich:tab label="#{msg['study.manage.tab.studyParticipant.heading']}">														
															
															<h:panelGrid rendered="#{! (pages$studyManage.isPopupViewOnlyMode || pages$studyManage.isViewMode || pages$studyManage.isApproveRejectStudyMode || pages$studyManage.isCompleteStudyMode || pages$studyManage.isAddToInvestmentPlanMode)}" styleClass="BUTTON_TD" width="100%">
																<h:panelGroup >
																	<h:commandButton styleClass="BUTTON" style="width:170px;"
																			value="#{msg['study.manage.tab.studyParticipant.btn.addInternalParticipants']}"
																			action="#{pages$studyManage.onAddMember}">
																	</h:commandButton>
																	<h:outputText value=" "  ></h:outputText>
																	<h:commandButton type="button" styleClass="BUTTON" style="width:170px;"
																			value="#{msg['study.manage.tab.studyParticipant.btn.addExternalParticipants']}"
																			onclick="javascript:showExternalMemberPopUp();">
																	</h:commandButton>
																</h:panelGroup>
															</h:panelGrid>
																													
															<t:div styleClass="contentDiv" style="width:99%">																
						                                       <t:dataTable id="dataTableParticipants"  
																	rows="#{pages$studyManage.paginatorRows}"
																	value="#{pages$studyManage.studyParticipantViewList}"
																	binding="#{pages$studyManage.studyParticipantDataTable}"																	
																	preserveDataModel="false" preserveSort="false" var="dataItemParticipant"
																	rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
																																		
																	<t:column width="40%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['studyManage.NameCol']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"  value="#{dataItemParticipant.personFullName}" rendered="#{dataItemParticipant.isDeleted == 0}" />																		
																	</t:column>
																	<t:column width="50%" sortable="true">
																		<f:facet  name="header">
																			<t:outputText value="#{msg['studyManage.emailCol']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"  value="#{dataItemParticipant.email}" rendered="#{dataItemParticipant.isDeleted == 0}">
																		</t:outputText>
																	</t:column>
																	
																	<t:column rendered="#{! (pages$studyManage.isPopupViewOnlyMode || pages$studyManage.isViewMode || pages$studyManage.isApproveRejectStudyMode || pages$studyManage.isCompleteStudyMode || pages$studyManage.isAddToInvestmentPlanMode)}" sortable="false" width="10%">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<t:outputLabel value=" " rendered="#{dataItemParticipant.isDeleted == 0}"></t:outputLabel>
																		
																		<t:commandLink onclick="if (!confirm('#{msg['study.confirm.delete']}')) return" action="#{pages$studyManage.onDeleteParticipant}" rendered="#{dataItemParticipant.isDeleted == 0}">
																			<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
																		</t:commandLink>													
																	</t:column>
																	
																  </t:dataTable>										
																</t:div>
														
				                                           		<t:div styleClass="contentDivFooter" style="width:100%">
																			<t:panelGrid id="rqtkRecNumTablesa"  columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
																				<t:div id="rqtkRecNumDivasdsad"  styleClass="RECORD_NUM_BG">
																					<t:panelGrid id="rqtkRecNumTblwqewq" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
																						<h:outputText value="#{msg['commons.recordsFound']}"/>
																						<h:outputText value=" : "/>
																						<h:outputText value="#{pages$studyManage.participantsGridRecordSize}"/>																						
																					</t:panelGrid>
																				</t:div>
																				  
																			      <CENTER>
																				   <t:dataScroller id="rqtkscrollerasdda" for="dataTableParticipants" paginator="true"
																					fastStep="1"  paginatorMaxPages="#{pages$studyManage.paginatorMaxPages}" immediate="false"
																					paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true" 
																				    pageIndexVar="pageNumber"
																				    styleClass="SCH_SCROLLER"
																				    paginatorActiveColumnStyle="font-weight:bold;"
																				    paginatorRenderLinkForActive="false" 
																					paginatorTableStyle="grid_paginator" layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">
								
																					    <f:facet  name="first">
																							<t:graphicImage    url="../#{path.scroller_first}"  id="lblFRequestHistorywqw"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastrewind">
																							<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRRequestHistorywqw"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastforward">
																							<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFRequestHistoryqw"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="last">
																							<t:graphicImage  url="../#{path.scroller_last}"  id="lblLRequestHistoryqww"></t:graphicImage>
																						</f:facet>
									
								                                                      <t:div id="rqtkPageNumDiv123"   styleClass="PAGE_NUM_BG">
																					   <t:panelGrid id="rqtkPageNumTable3123"  styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0" >
																					 					<h:outputText  styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																										<h:outputText   styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																						</t:panelGrid>
																							
																						</t:div>
																				 </t:dataScroller>
																				 </CENTER>
																		      
																	        </t:panelGrid>
								                          			 </t:div>														
															</rich:tab>
														
														<!-- Study Participant Tab - End -->
														
														
														<!-- Recommendation Tab - Start -->
														<rich:tab label="#{msg['study.manage.tab.recommendations.heading']}">														
															
															<h:panelGrid rendered="#{! (pages$studyManage.isPopupViewOnlyMode || pages$studyManage.isViewMode || pages$studyManage.isApproveRejectStudyMode || pages$studyManage.isCompleteStudyMode || pages$studyManage.isAddToInvestmentPlanMode)}" styleClass="BUTTON_TD" width="100%">
																<h:commandButton style="width: 170px;" styleClass="BUTTON"
																		value="#{msg['study.manage.tab.recommendations.btn.addRecommendations']}"
																		action="#{pages$studyManage.onAddRecommendations}">
																</h:commandButton>																
																<a4j:commandLink id="lnkReceiveRecommendation" action="#{pages$studyManage.receiveRecommendation}" reRender="dataTable2,gridInfo2" />
																<%-- <h:commandLink id="lnkReceiveRecommendation" action="#{pages$studyManage.receiveRecommendation}" /> --%>																
															</h:panelGrid>
																													
															<t:div styleClass="contentDiv" style="width:99%">																
						                                       <t:dataTable id="dataTable2"  
																	rows="#{pages$studyManage.paginatorRows}"
																	value="#{pages$studyManage.recommendationViewList}"
																	binding="#{pages$studyManage.recommendationDataTable}"																	
																	preserveDataModel="false" preserveSort="false" var="dataItem"
																	rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
																																		
																	<t:column width="20%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['study.manage.tab.recommendations.date']}" />
																		</f:facet>																		
																		<t:outputText value="#{dataItem.recommendationDate}" rendered="#{dataItem.isDeleted == 0}">
																			<f:convertDateTime timeZone="#{pages$studyManage.timeZone}" pattern="#{pages$studyManage.dateFormat}" />
																		</t:outputText>																		
																	</t:column>
																	
																	<t:column width="20%" sortable="true" >
																		<f:facet name="header">
																			<t:outputText value="#{msg['study.manage.tab.recommendations.user']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.recommendationUser}" rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>												
																																														
																	<t:column width="50%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['study.manage.tab.recommendations.details']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.remark}" rendered="#{dataItem.isDeleted == 0}" />																		
																	</t:column>
																	
																	<t:column rendered="#{! (pages$studyManage.isPopupViewOnlyMode || pages$studyManage.isViewMode || pages$studyManage.isApproveRejectStudyMode || pages$studyManage.isCompleteStudyMode || pages$studyManage.isAddToInvestmentPlanMode)}" sortable="false" width="10%">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<t:commandLink action="#{pages$studyManage.onEditRecommendation}" rendered="#{dataItem.isDeleted == 0}">
																				<h:graphicImage title="#{msg['commons.edit']}" url="../resources/images/edit-icon.gif" />&nbsp;
																		</t:commandLink>
																		
																		<t:outputLabel value=" " rendered="#{dataItem.isDeleted == 0}"></t:outputLabel>
																		
																		<%-- <t:commandLink onclick="if (!confirm('#{msg['study.manage.tab.recommendations.confirm.delete']}')) return" action="#{pages$studyManage.onDeleteRecommendation}" rendered="#{dataItem.isDeleted == 0}">
																			<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
																		</t:commandLink> --%>
																		
																		<a4j:commandLink onclick="if (!confirm('#{msg['study.manage.tab.recommendations.confirm.delete']}')) return" action="#{pages$studyManage.onDeleteRecommendation}" rendered="#{dataItem.isDeleted == 0}" reRender="dataTable2,gridInfo2">
																			<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
																		</a4j:commandLink>													
																	</t:column>
																	
																  </t:dataTable>										
																</t:div>
														
				                                           		<t:div id="gridInfo2" styleClass="contentDivFooter" style="width:100%">
																			<t:panelGrid id="rqtkRecNumTable2"  columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
																				<t:div id="rqtkRecNumDiv2"  styleClass="RECORD_NUM_BG">
																					<t:panelGrid id="rqtkRecNumTbl2" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
																						<h:outputText value="#{msg['commons.recordsFound']}"/>
																						<h:outputText value=" : "/>
																						<h:outputText value="#{pages$studyManage.recommendationsRecordSize}"/>																						
																					</t:panelGrid>
																				</t:div>
																				  
																			      <CENTER>
																				   <t:dataScroller id="rqtkscroller2" for="dataTable2" paginator="true"
																					fastStep="1"  paginatorMaxPages="#{pages$studyManage.paginatorMaxPages}" immediate="false"
																					paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true" 
																				    pageIndexVar="pageNumber"
																				    styleClass="SCH_SCROLLER"
																				    paginatorActiveColumnStyle="font-weight:bold;"
																				    paginatorRenderLinkForActive="false" 
																					paginatorTableStyle="grid_paginator" layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">
								
																					    <f:facet  name="first">
																							<t:graphicImage    url="../#{path.scroller_first}"  id="lblFRequestHistory2"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastrewind">
																							<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRRequestHistory2"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastforward">
																							<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFRequestHistory2"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="last">
																							<t:graphicImage  url="../#{path.scroller_last}"  id="lblLRequestHistory2"></t:graphicImage>
																						</f:facet>
									
								                                                      <t:div id="rqtkPageNumDiv2"   styleClass="PAGE_NUM_BG">
																					   <t:panelGrid id="rqtkPageNumTable2"  styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0" >
																					 					<h:outputText  styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																										<h:outputText   styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																						</t:panelGrid>
																							
																						</t:div>
																				 </t:dataScroller>
																				 </CENTER>
																		      
																	        </t:panelGrid>
								                          			 </t:div>								                          			 														
															</rich:tab>														
														
														<!-- Recommendation Tab - End -->
                                                        
                                                        <!-- Attachments Tab - Start -->
														<rich:tab label="#{msg['contract.attachmentTabHeader']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<!-- Attachments Tab - End -->
														
														<!-- Comments Tab - Start -->
                                                         <rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														<!-- Comments Tab - End -->
																												
												</rich:tabPanel>
												
												<table cellpadding="0" cellspacing="0"  style="width:100%;">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
												</table>
												
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td colspan="6" class="BUTTON_TD"  style="padding-top:10px;">
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$studyManage.onSave}"
																	rendered="#{! (pages$studyManage.isPopupViewOnlyMode || pages$studyManage.isViewMode || pages$studyManage.isApproveRejectStudyMode || pages$studyManage.isCompleteStudyMode || pages$studyManage.isAddToInvestmentPlanMode)}">
															</h:commandButton>															
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.send']}"
																	action="#{pages$studyManage.onSend}"
																	rendered="#{pages$studyManage.isEditMode}">
															</h:commandButton>
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	action="#{pages$studyManage.onCancel}"
																	rendered="#{! (pages$studyManage.isPopupViewOnlyMode || pages$studyManage.isApproveRejectStudyMode || pages$studyManage.isCompleteStudyMode || pages$studyManage.isAddToInvestmentPlanMode) }">
															</h:commandButton>																														
														</td>
													</tr>
												</table>
											</div>								
								
									    </h:form>
									</div>
								

									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
           </div>
		</body>
	</html>
</f:view>
