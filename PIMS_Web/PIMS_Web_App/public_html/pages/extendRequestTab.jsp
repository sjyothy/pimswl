
	<script language="JavaScript" type="text/javascript"> 
	
      
		function showContractorPopup()
		{
			var screen_width = screen.width;
			var screen_height = screen.height;
			var popup_width = screen_width-180;
			var popup_height = screen_height-265;
			var leftPos = (screen_width-popup_width)/2 -5, topPos = (screen_height-popup_height)/2 - 20;
			var popup = window.open('contractorSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
			popup.focus();
		}

	</script>
<t:div styleClass="TAB_DETAIL_SECTION" style="width:100%" >
	<t:panelGrid id="applicationDetailsTable" cellpadding="1px" width="100%" cellspacing="5px" styleClass="TAB_DETAIL_SECTION_INNER"  columns="4">
					
			        <h:outputLabel styleClass="LABEL" value="#{msg['application.number']}" />
				    <h:inputText maxlength="20" id="txtApplicationNum" readonly="true"  value="#{pages$extendRequestTab.applicationNumber}" style="width:190px; height: 18px"></h:inputText>
			        <h:outputLabel styleClass="LABEL" value="#{msg['application.status']}"></h:outputLabel>
					<h:inputText id="txtApplicationStatus" style="width:190px; height: 18px" readonly="true" maxlength="20" value="#{pages$extendRequestTab.applicationStatus}" />

					<h:outputLabel styleClass="LABEL" value="#{msg['applicationDetails.date']}"/>
                    <h:inputText id="txtApplicationDate" style="width:190px; height: 18px"   readonly="true" value="#{pages$extendRequestTab.applicationDate}"/>
                    <h:panelGroup>
						<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
       				<h:outputLabel  styleClass="LABEL" style="width: 160px;" value="#{msg['application.source']}:"/>
       				</h:panelGroup>
                    <h:selectOneMenu id="applicationSourceCombo" style="width: 192px;" rendered="#{!pages$extendRequestTab.readonlyMode}" required="false" value="#{pages$extendRequestTab.applicationSourceId}">
						<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
						<f:selectItems value="#{pages$ApplicationBean.applicationSourceList}" />
					</h:selectOneMenu>
                    <h:inputText id="txtSource" style="width:190px; height: 18px" readonly="true" rendered="#{pages$extendRequestTab.readonlyMode}" value="#{pages$extendRequestTab.applicationSource}" />
                    <h:panelGroup>
						<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
			        <h:outputLabel styleClass="LABEL" value="#{msg['contractor.name']}:"></h:outputLabel>
			        </h:panelGroup>
			        <t:panelGrid id="gridContractor" columns="2" width="100%" cellpadding="0" cellspacing="1" >
				        <h:inputText id="txtContractorName" readonly="true" style="width:190px; height: 18px" value="#{pages$extendRequestTab.contractorName}"></h:inputText>
					    <h:commandLink onclick="showContractorPopup();" rendered="#{!pages$extendRequestTab.readonlyMode}">
					   		<h:graphicImage id="imgPerson"  style="MARGIN: 0px 0px -4px" url="../resources/images/app_icons/Add-Person.png"></h:graphicImage>
					   </h:commandLink>
                    </t:panelGrid>
				    <h:outputLabel id="lblAppType" value="#{msg['contractor.type']}"></h:outputLabel>
					<h:inputText id="txtContractorType" style="width:190px; height:18px;" maxlength="15"  readonly="true" value="#{pages$extendRequestTab.contractorType}" ></h:inputText>
	                <h:panelGroup>
						<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
					<h:outputLabel styleClass="LABEL" value="#{msg['tender.number']}:"></h:outputLabel>
					</h:panelGroup>
					<t:panelGrid id="gridTender" columns="2" width="100%" cellpadding="0" cellspacing="1" >
				        <h:inputText id="txtTenderNumber" style="width:190px; height: 18px"  maxlength="20"  readonly="true" value="#{pages$extendRequestTab.tenderNumber}" ></h:inputText>
					    <h:commandLink  action="#{pages$extendRequestTab.onSearchTender}" rendered="#{!pages$extendRequestTab.readonlyMode}">
					   		<h:graphicImage id="imgTender"  style="MARGIN: 0px 0px -4px" url="../resources/images/app_icons/Add-Person.png"></h:graphicImage>
					   </h:commandLink>
                    </t:panelGrid>
			        <h:outputLabel  styleClass="LABEL"  value="#{msg['tender.description']}"></h:outputLabel>
			        <h:selectOneMenu id="tenderTypeCombo" style="width: 192px;" required="false" value="#{pages$extendRequestTab.tenderTypeId}" rendered="false">
						<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
						<f:selectItems value="#{pages$ApplicationBean.tenderTypeList}" />
					</h:selectOneMenu>
			        <h:inputText id="txtTenderDesc" readonly="true" style="width:190px; height: 18px" value="#{pages$extendRequestTab.tenderType}" rendered="true"></h:inputText>
			        
			        <h:outputLabel styleClass="LABEL"  value="#{msg['commons.startDate']}" />
				    <h:inputText maxlength="20" id="txtStartDate" readonly="true"  value="#{pages$extendRequestTab.tenderStartDate}" style="width:190px; height: 18px"></h:inputText>
			        <h:outputLabel styleClass="LABEL"  value="#{msg['commons.endDate']}"></h:outputLabel>
					<h:inputText id="txtEndDate" style="width:190px; height: 18px" readonly="true" maxlength="20" value="#{pages$extendRequestTab.tenderEndDate}" rendered="true"/>
					<rich:calendar id="endDate" value="#{pages$extendRequestTab.tenderEndDateVal}" locale="#{pages$extendRequestTab.locale}" popup="true" datePattern="#{pages$extendRequestTab.dateFormat}" showApplyButton="false" enableManualInput="false" rendered="false"/>
	                <h:panelGroup>
						<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>	        
			        <h:outputLabel styleClass="LABEL"  value="#{msg['commons.reasons']}:"></h:outputLabel>
			        </h:panelGroup>
			        <t:inputTextarea styleClass="TEXTAREA" value="#{pages$extendRequestTab.reasons}" readonly="#{pages$extendRequestTab.readonlyMode}" rows="6" style="width: 200px;"/>
			        <h:outputLabel value=""/>
			        <h:outputLabel value=""/>
   	</t:panelGrid>	              
</t:div>
