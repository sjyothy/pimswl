

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

		
<f:view>
        <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
       <f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		

			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			

	<script language="javascript" type="text/javascript">
    function clearWindow()
	{
       
        
            document.getElementById("formSearchPerson:txtfirstname").value="";
			document.getElementById("formSearchPerson:txtlastname").value="";	
			document.getElementById("formSearchPerson:txtpassportNumber").value="";
			document.getElementById("formSearchPerson:txtresidenseVisaNumber").value="";
			document.getElementById("formSearchPerson:txtsocialSecNumber").value="";
		
			
	}
	function addToContract()
	{
	   
	   
	  window.opener.document.forms[0].submit();
	  
	  window.close();
	}
	function sendToParent(PersonName,PersonId)
	{
	var controlName=document.getElementById("formSearchPerson:hdnControlName").value;
	
	var controlForId=document.getElementById("formSearchPerson:hdncontrolForId").value;
	var hdnDisplaycontrol=document.getElementById("formSearchPerson:hdndisplaycontrolName").value;
	window.opener.addSponsorManager(controlName,controlForId,PersonName,PersonId,hdnDisplaycontrol);
	window.close();
	}
	function closeWindow()
	{
	
	  window.opener="x";
	  window.close();
	
	}
	
       </SCRIPT>



	</head>
	<body dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
				

				<tr width="100%">
					
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr><td class="HEADER_TD">
										<h:outputLabel value="#{msg['commons.searchPerson']}" styleClass="HEADER_FONT"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
					        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
                                 <tr valign="top">
                                  <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
                                   </td>    
									<td width="100%" height="560px" valign="top" nowrap="nowrap">
								<div  class="SCROLLABLE_SECTION" height="560px"  >	
								 <h:form id="formSearchPerson" enctype="multipart/form-data" >
								 <div class="MARGIN" style="width:98%" >
								 
								<div class="DETAIL_SECTION" style="width:98%" >
								<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
								<table id="SearchCriteria" width="98%" cellpadding="1px" cellspacing="3px" class="DETAIL_SECTION_INNER"  >
												<tr>
													<td colspan="6">
														<h:inputHidden id="hdnPersonType"
																 value="#{pages$ContractPerson.requestForPersonType}"/>
													   <h:inputHidden id="hdnControlName"
																 value="#{pages$ContractPerson.controlName}"/>
													   <h:inputHidden id="hdncontrolForId"
																 value="#{pages$ContractPerson.controlForId}"/>
													   <h:inputHidden id="hdndisplaycontrolName"
																 value="#{pages$ContractPerson.hdndisplaycontrolName}"/>
														<h:messages></h:messages>		 
													</td>
												</tr>
												
												<tr>
												<td>
												&nbsp;&nbsp;
												</td>
												    <td width="20%">
															<h:outputLabel styleClass="LABEL"  value="#{msg['customer.firstname']}" ></h:outputLabel>:
													</td>
													<td>
															<h:inputText maxlength="15" id="txtfirstname"
																 value="#{pages$ContractPerson.firstName}">
															</h:inputText>
													</td>
													 <td>
												&nbsp;&nbsp;
												    </td>
												    <td>
												    &nbsp;&nbsp;
												   </td>
													  <td>
															<h:outputLabel styleClass="LABEL"  value="#{msg['customer.lastname']}" ></h:outputLabel>:
													</td>
												
													<td>
															<h:inputText maxlength="15" id="txtlastname"
																 value="#{pages$ContractPerson.lastName}">
															</h:inputText>
													</td>
													
													
												</tr>
												<tr>
												<td>
												&nbsp;&nbsp;
												</td>
												    <td>
															<h:outputLabel styleClass="LABEL"  value="#{msg['customer.socialSecNumber']}" ></h:outputLabel>:
													</td>
													<td>
															<h:inputText maxlength="15" id="txtsocialSecNumber" 
																 value="#{pages$ContractPerson.socialSecNumber}">
															</h:inputText>
													</td>  
													<td>
												    &nbsp;&nbsp;
												   </td>
												
												   <td>
												    &nbsp;&nbsp;
												   </td>
													 <td>
															<h:outputLabel styleClass="LABEL"  value="#{msg['customer.passportNumber']}" ></h:outputLabel>:
													</td>
													<td>
															<h:inputText maxlength="15" id="txtpassportNumber"
																 value= "#{pages$ContractPerson.passportNumber}" >
															</h:inputText>
													</td>  
													
												</tr>
												<tr>
												<td>
												&nbsp;&nbsp;
												</td>
												    <td>
															<h:outputLabel styleClass="LABEL"  value="#{msg['customer.residenseVisaNumber']}" ></h:outputLabel>:
													</td>
													<td>
															<h:inputText maxlength="15" id="txtresidenseVisaNumber"
																 value="#{pages$ContractPerson.residenseVisaNumber}" >
															</h:inputText>
													</td>
													 
													
												</tr>
												
												 <tr>
												 <td>
												&nbsp;&nbsp;
												</td>
                                              <td colspan="6" class="BUTTON_TD"
                                                > 
                                           			<h:commandButton type="submit" styleClass="BUTTON" value="#{msg['commons.search']}" action="#{pages$ContractPerson.doSearch}" ></h:commandButton>
                                           			 
                                           			<h:commandButton type="submit" styleClass="BUTTON" action="#{pages$ContractPerson.btnAdd_Click}" value="#{msg['commons.Add']}"/>
										              
										              <h:commandButton styleClass="BUTTON" type="button"
												                           value="#{msg['commons.reset']}"
												                          
												                           onclick="javascript:clearWindow();" 
												                             />
												      
                                		      </td>
                                		     
                                          </tr>
								    </table>
								    	</div>
							</div>
								    
								       <div style="padding:5px;">		    
                   		                  <div class="imag">&nbsp;</div>
                   			               <div class="contentDiv"  style="width:95%">
															<t:dataTable id="test3" 
																rows="#{pages$ContractPerson.paginatorRows}" 
																width="100%" styleClass="grid"
																value="#{pages$ContractPerson.propertyInquiryDataList}"
																binding="#{pages$ContractPerson.dataTable}"
																preserveDataModel="false" preserveSort="false" var="dataItem"
																rowClasses="row1,row2" rules="all" renderedIfEmpty="false"
															>
															
															<t:column id="col2"  >
			
			
																<f:facet name="header" >
			
																	<t:outputText  value="#{msg['customer.firstname']}" />
			
																</f:facet>
																<t:outputText styleClass="A_LEFT" value="#{dataItem.firstName} #{dataItem.middleName} #{dataItem.lastName}" />
															</t:column>
															<t:column id="col4" >
			
			
																<f:facet name="header">
			
																	<t:outputText  value="#{msg['customer.passportNumber']}" />
			
																</f:facet>
																<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.passportNumber}" />
															</t:column>
															
															<t:column id="col5" >
			
			
																<f:facet name="header">
			
																	<t:outputText  value="#{msg['customer.residenseVisaNumber']}" />
			
																</f:facet>
																<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.residenseVisaNumber}" />
															</t:column>
																											
															<t:column id="col6">
			
			
																<f:facet name="header">
			
																	<t:outputText  value="#{msg['customer.drivingLicenseNumber']}" />
			
																</f:facet>
																<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.drivingLicenseNumber}" />
															</t:column>
																											
															<t:column id="col7" >
																<f:facet name="header">
																	<t:outputText  value="#{msg['customer.socialSecNumber']}" />
																</f:facet>
																<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.socialSecNumber}" />
															</t:column>
														    <t:column id="col8"  >
																<f:facet name="header" >
																	<t:outputText value="#{msg['commons.select']}" />
			                                                    </f:facet>
			                                                    <h:graphicImage  style="width: 16;height: 16;border: 0" 
		                                         			                alt="#{msg['commons.select']}"
																			url="../resources/images/select-icon.gif"
																			onclick="javascript:sendToParent('#{dataItem.firstName} #{dataItem.middleName} #{dataItem.lastName}','#{dataItem.personId}');"
																		    rendered="#{pages$ContractPerson.isSelectOnePerson}"
																		     ></h:graphicImage>
																		<t:selectBooleanCheckbox rendered="#{pages$ContractPerson.isSelectManyPerson}"  
																		                         value="#{dataItem.selected}" />
																		
																	
															</t:column>
														  </t:dataTable>										
											</div>
											<t:div styleClass="contentDivFooter" style="width:96.7%;">
                                              <table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$ContractPerson.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:50%" align="right">
											     <CENTER>
												<t:dataScroller id="scrollerd" for="test3" paginator="true" 
													fastStep="1" paginatorMaxPages="15" immediate="false"
													paginatorTableClass="paginator" 
													pageIndexVar="pageNumber"
													renderFacetsIfSinglePage="true" paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
												    paginatorActiveColumnStyle="font-weight:bold;"
                                                                              paginatorRenderLinkForActive="false"
                                                                               styleClass="SCH_SCROLLER"
                                                  >
                                                                              
										               <f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
				                                    <t:div styleClass="PAGE_NUM_BG">
												 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
													</t:div>
                                                 </t:dataScroller>
                                               </CENTER>
											</td></tr>
											</table>
									
                                           </t:div>
											<table width="98%" >
												<tr>
													<td>
														&nbsp;&nbsp;&nbsp;
													</td>
													<td align="center" class="BUTTON_TD">
						                                <h:commandButton  styleClass="BUTTON" style="width:179px"
						                                                    
						                                                 value="#{msg['paidFacilities.sendToContract']}" 
						                                                 action ="#{pages$ContractPerson.sendInfoToParent}"
						                                                 rendered="#{pages$ContractPerson.isSelectManyPerson}"
						                                                 />
						                                                
													 </td>
												 </tr>
											 </table>
									</div>		 
									
										  									
				 				 </h:form>
				 				 </div>
							  </td>
							</tr>
							<tr >
				             <td>
				               &nbsp;
				             </td>
				           </tr>
							 
					      </table>  
					                 
					   
					 
					    </td>
					 </tr>
					 				 
				
			
				
				
				 </table> 
			
									
			</body>
			</html>
		</f:view>

