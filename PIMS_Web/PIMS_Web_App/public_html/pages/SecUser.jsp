<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript"> 

		function ads()
		{

			var objSourceElement =document.getElementById('UserForm:a1');
			var objTargetElement = document.getElementById('UserForm:a2');

  			for (var i = 0; i < objSourceElement.length; i++) 
  			{
  				document.getElementById('UserForm:gnbtu').value = objSourceElement.options[i].value +','+document.getElementById('UserForm:gnbtu').value;
			}
			for(var j =0;j<objTargetElement.length;j++)
			{
	 			document.getElementById('UserForm:gbtu').value = objTargetElement.options[j].value +','+document.getElementById('UserForm:gbtu').value;
			}
}
	function submitForm(ads){
					
			document.getElementById('NOLForm:hiddenValueNOLType').value = ads.value;
			document.forms[0].submit();
			
		}
	

function move(fbox, tbox) {
a = 'UserForm:'+fbox;
b = 'UserForm:'+tbox;
	var objSourceElement = document.getElementById(a);
	var objTargetElement = document.getElementById(b);   
    var aryTempSourceOptions = new Array();   
         var x = 0;                //looping through source element to find selected options 

         for (var i = 0; i < objSourceElement.length; i++) {     
         if (objSourceElement.options[i].selected) {      
         //need to move this option to target element  
               
                  var intTargetLen = objTargetElement.length++;
         objTargetElement.options[intTargetLen].text = objSourceElement.options[i].text;      
         objTargetElement.options[intTargetLen].value = objSourceElement.options[i].value;       
        
         }       
         else {       
         //storing options that stay to recreate select element  
         var objTempValues = new Object(); 
   
objTempValues.text = objSourceElement.options[i].text;
objTempValues.value = objSourceElement.options[i].value;
aryTempSourceOptions[x] = objTempValues;  
x++;       
}  
}      
//resetting length of source     
objSourceElement.length = aryTempSourceOptions.length;  
//looping through temp array to recreate source select element 
for (var i = 0; i < aryTempSourceOptions.length; i++) {     
objSourceElement.options[i].text = aryTempSourceOptions[i].text;    
objSourceElement.options[i].value = aryTempSourceOptions[i].value;
objSourceElement.options[i].selected = false;    
}   
}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">

				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['user.userCreateScreenTitle']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="UserForm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:outputText value="#{pages$SecUser.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																	
																	<h:outputText value="#{pages$SecUser.infoMessages}"
																	escape="false" styleClass="INFO_FONT" />
															</td>
														</tr>
													</table>
													<div class="AUC_DET_PAD">

														<div class="TAB_PANEL_INNER">

															<table cellpadding="1px" cellspacing="2px"
																style="font-weight: bold;">
																<tr>
																	<td class="DET_SEC_TD">
																		<h:outputText styleClass="LABEL" value="#{msg['user.userFullName']}" />
																	</td>
																	<td class="DET_SEC_TD">
																		<h:inputText 
																			binding="#{pages$SecUser.fullNameTextBox}"																			
																			styleClass="INPUT_TEXT A_LEFT"></h:inputText>
																	</td>

																	<td class="DET_SEC_TD">
																		<h:outputText styleClass="LABEL"
																			value="#{msg['user.userFullNameArabic']}" />
																	</td>
																	<td class="DET_SEC_TD">
																		<h:inputText
																			binding="#{pages$SecUser.fullNameSecTextBox}"																		
																			styleClass="INPUT_TEXT A_RIGHT"></h:inputText>

																	</td>

																</tr>
																<tr>
																	<td class="DET_SEC_TD">
																		<h:outputText styleClass="LABEL" value="#{msg['user.userFirstName']}" />
																	</td>
																	<td class="DET_SEC_TD">
																		<h:inputText
																			binding="#{pages$SecUser.firstNameTextBox}"																			
																			styleClass="INPUT_TEXT A_LEFT"></h:inputText>
																	</td>

																	<td class="DET_SEC_TD">
																		<h:outputText styleClass="LABEL" value="#{msg['user.userLastName']}" />
																	</td>
																	<td class="DET_SEC_TD">
																		<h:inputText
																			binding="#{pages$SecUser.lastNameTextBox}"																			
																			styleClass="INPUT_TEXT A_LEFT"></h:inputText>

																	</td>

																</tr>
																<tr>
																	<td class="DET_SEC_TD">
																		<h:outputText styleClass="LABEL" value="#{msg['user.userMiddleName']}" />
																	</td>
																	<td class="DET_SEC_TD">
																		<h:inputText
																			binding="#{pages$SecUser.middleNameTextBox}"
																			styleClass="INPUT_TEXT A_LEFT"></h:inputText>
																	</td>

																	<td class="DET_SEC_TD">
																		<h:outputText styleClass="LABEL" value="#{msg['user.email']}" />
																	</td>
																	<td class="DET_SEC_TD">
																		<h:inputText
																			binding="#{pages$SecUser.emailUrlTextBox}"																		
																			styleClass="INPUT_TEXT A_LEFT"></h:inputText>

																	</td>

																</tr>

																<tr>
																	<td class="DET_SEC_TD">
																		<h:outputText styleClass="LABEL" value="#{msg['user.loginId']}" />
																	</td>
																	<td class="DET_SEC_TD">

																		<h:selectOneMenu id="logins"  styleClass="SELECT_MENU"
																			value="#{pages$SecUser.loginId}">
																			<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1"/>
																			<f:selectItems value="#{pages$SecUser.loginIdList}" />

																		</h:selectOneMenu>
																	</td>

																	<td class="DET_SEC_TD">

																		<h:outputText styleClass="LABEL" value="#{msg['user.status']}" />
																	</td>
																	<td class="DET_SEC_TD">
																		<h:selectOneMenu id="a3" styleClass="SELECT_MENU"
																			value="#{pages$SecUser.userStatus}">
																			<%--<f:selectItems value="#{pages$SecUser.status}" />--%>
																			<f:selectItem itemLabel="#{msg['user.status.Active']}" itemValue="1"/>
																			<f:selectItem itemLabel="#{msg['user.status.InActive']}" itemValue="0"/>
																		</h:selectOneMenu>  


																	</td>

																</tr>
																<tr>
																	<td class="DET_SEC_TD">
																		<h:outputText styleClass="LABEL" value="#{msg['user.isAdmin']}" />
																	</td>
																	<td class="DET_SEC_TD">
																		<h:selectBooleanCheckbox
																			binding="#{pages$SecUser.adminCheck}">

																		</h:selectBooleanCheckbox>

																	</td>
																	<td class="DET_SEC_TD">
																	</td>
																	<td class="DET_SEC_TD">
																		<h:inputHidden binding="#{pages$SecUser.editMode}"></h:inputHidden>

																	</td>

																</tr>
															</table>

														</div>
														
														&nbsp;
														&nbsp;
														&nbsp;

														<div
															style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px; width: 95%">

															<div class="contentDiv" style="width: 98%">
																<t:dataTable id="dtGroups"
																	value="#{pages$SecUser.groups}" rows="#{pages$SecUser.paginatorRows}"
																	preserveDataModel="false" preserveSort="false"
																	var="dataItem" rowClasses="row1,row2"
																	renderedIfEmpty="true" width="100%">

																	<t:column id="col1" width="10%" sortable="false">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.select']}" />
																		</f:facet>
																		<h:selectBooleanCheckbox id="select"
																			value="#{dataItem.isAssignedToUser}" />
																	</t:column>

																	<t:column id="col2" width="20%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['userGroup.groupId']}" />
																		</f:facet>
																		<t:outputText  styleClass="A_LEFT"
																			value="#{dataItem.userGroup.userGroupId}" />
																	</t:column>

																	<t:column id="col3" width="35%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['userGroup.groupName']}" />
																		</f:facet>
																		<t:outputText  styleClass="A_LEFT"
																			value="#{dataItem.userGroup.primaryName}" />
																	</t:column>

																	<t:column id="col4" width="35%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['userGroup.groupNameArabic']}" />
																		</f:facet>
																		<t:outputText styleClass="A_RIGHT"
																			value="#{dataItem.userGroup.secondaryName}" />
																	</t:column>

																</t:dataTable>
															</div>
															<t:div styleClass="contentDivFooter" style="width:99%;">
																<table cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td class="RECORD_NUM_TD">
																			<div class="RECORD_NUM_BG">
																				<table cellpadding="0" cellspacing="0"
																					style="width: 182px; # width: 150px;">
																					<tr>
																						<td class="RECORD_NUM_TD">
																							<h:outputText
																								value="#{msg['commons.recordsFound']}" />
																						</td>
																						<td class="RECORD_NUM_TD">
																							<h:outputText value=" : " />
																						</td>
																						<td class="RECORD_NUM_TD">
																							<h:outputText
																								value="#{pages$SecUser.recordSize}" />
																						</td>
																					</tr>
																				</table>
																			</div>
																		</td>
																		<td class="BUTTON_TD"
																			style="width: 53%; # width: 50%;" align="right">
																			<CENTER>
																				<t:dataScroller id="scroller" for="dtGroups"
																					paginator="true" fastStep="1"
																					paginatorMaxPages="#{pages$SecUser.paginatorMaxPages}"
																					immediate="false" paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true"
																					paginatorTableStyle="grid_paginator"
																					layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																					paginatorActiveColumnStyle="font-weight:bold;"
																					paginatorRenderLinkForActive="false"
																					pageIndexVar="pageNumber" styleClass="SCH_SCROLLER">
																					<f:facet name="first">
																						<t:graphicImage url="../#{path.scroller_first}"
																							id="lblF"></t:graphicImage>
																					</f:facet>
																					<f:facet name="fastrewind">
																						<t:graphicImage
																							url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																					</f:facet>
																					<f:facet name="fastforward">
																						<t:graphicImage
																							url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																					</f:facet>
																					<f:facet name="last">
																						<t:graphicImage url="../#{path.scroller_last}"
																							id="lblL"></t:graphicImage>
																					</f:facet>
																					<t:div styleClass="PAGE_NUM_BG">
																						<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																						<table cellpadding="0" cellspacing="0">
																							<tr>
																								<td>
																									<h:outputText styleClass="PAGE_NUM"
																										value="#{msg['commons.page']}" />
																								</td>
																								<td>
																									<h:outputText styleClass="PAGE_NUM"
																										style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																										value="#{requestScope.pageNumber}" />
																								</td>
																							</tr>
																						</table>

																					</t:div>
																				</t:dataScroller>
																			</CENTER>

																		</td>
																	</tr>
																</table>
															</t:div>
														</div>
													</div>

													<table width="96%" border="0">
														<tr>
															<td class="BUTTON_TD JUG_BUTTON_TD_US">

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$SecUser.saveUser}" type="submit"
																	binding="#{pages$SecUser.saveButton}"
																	></h:commandButton>
																&nbsp;
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	binding="#{pages$SecUser.clearButton}"
																	action="#{pages$SecUser.cancel}"></h:commandButton>
															</td>

														</tr>


													</table>


												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>

				</table>
			</div>
		</body>
	</html>
</f:view>