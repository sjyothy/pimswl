<%-- 
  - Author: Munir chagani
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Auction
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<%
					response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
						response.setHeader("Pragma", "no-cache"); //HTTP 1.0
						response.setDateHeader("Expires", 0); //prevents caching at the proxy server
				%>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">


					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>


					<tr width="100%">

						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="83%" height="100%" valign="top"
							class="divBackgroundBody">
							<table class="greyPanelTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['userGroup.permission']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr valign="top">
									<td>

										<h:form id="searchFrm" enctype="multipart/form-data">
											<div class="SCROLLABLE_SECTION">
												<div>
													<table border="0" class="layoutTable">
														<tr>
															<td>
																<h:outputText
																	value="#{pages$secPermissionsTree.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
															</td>
														</tr>
													</table>
												</div>
												<div class="MARGIN" style="width: 95%;">
													<table cellpadding="0" cellspacing="0" width="100%">

														<tr>
															<td colspan="5">


																<t:tree2 showRootNode="false" id="clientTree"
																	value="#{pages$secPermissionsTree.permissionsTree}"
																	var="node" varNodeToggler="t">

																	<f:facet name="None">
																		<h:panelGroup style="width:100%;">
																			<h:panelGrid columns="8">
																				<h:outputLabel style="font-weight:bold;"
																					value="#{node.permissionName}">
																				</h:outputLabel>
																				<h:outputText style="font-weight:bold;"
																					value=" (#{node.childCount})"
																					rendered="#{!empty node.children}" />
																				<h:selectBooleanCheckbox id="personCheck1"
																					value="#{node.canView}"
																					title="#{node.permission.permissionId}"></h:selectBooleanCheckbox>

																			</h:panelGrid>
																		</h:panelGroup>
																	</f:facet>

																	<f:facet name="Module">
																		<h:panelGroup style="width:100%;">

																			<h:panelGrid columns="8">
																				<t:graphicImage
																					value="../resources/images/tree/FolderOpen.gif"
																					rendered="#{t.nodeExpanded}" border="0" />
																				<t:graphicImage
																					value="../resources/images/tree/folder.gif"
																					rendered="#{!t.nodeExpanded}" border="0" />
																				<h:outputLabel style="font-weight:bold;"
																					value="#{node.permissionName}">
																				</h:outputLabel>
																				<h:outputText style="font-weight:bold;"
																					value=" (#{node.childCount})"
																					rendered="#{!empty node.children}" />
																				<h:selectBooleanCheckbox id="personCheck2"
																					value="#{node.canView}"
																					title="#{node.permission.permissionId}"></h:selectBooleanCheckbox>

																			</h:panelGrid>
																		</h:panelGroup>
																	</f:facet>


																	<f:facet name="Transaction">
																		<h:panelGroup style="width:100%;">

																			<h:panelGroup style="width:100%;">

																				<h:panelGrid columns="6" >
																					<t:graphicImage
																						value="../resources/images/tree/Transaction.gif"
																						rendered="#{t.nodeExpanded}" border="0" />
																					<t:graphicImage
																						value="../resources/images/tree/Transaction.gif"
																						rendered="#{!t.nodeExpanded}" border="0" />
																					<h:outputLabel style="font-weight:bold;"
																						value="#{node.permissionName}">
																					</h:outputLabel>
																					<h:outputText style="font-weight:bold;"
																						value=" (#{node.childCount})"
																						rendered="#{!empty node.children}" />
																					<h:selectBooleanCheckbox id="transactionChkView"
																						value="#{node.canView}"
																						title="#{node.permission.permissionId}"></h:selectBooleanCheckbox>
																				</h:panelGrid>
																			</h:panelGroup>

																			<h:panelGroup style="width:100%;">

																				<h:panelGrid columns="6">

																					<t:outputText
																						value="#{msg['commons.group.permissions.create']}"></t:outputText>
																					<t:selectBooleanCheckbox id="transactionChkCreate"
																						value="#{node.canCreate}"
																						title="#{node.permission.permissionId}"
																						></t:selectBooleanCheckbox>
																					<t:outputText
																						value="#{msg['commons.group.permissions.update']}"></t:outputText>
																					<t:selectBooleanCheckbox id="transactionChkUpdate"
																						value="#{node.canUpdate}"
																						title="#{node.permission.permissionId}"
																						></t:selectBooleanCheckbox>
																					<t:outputText
																						value="#{msg['commons.group.permissions.delete']}"></t:outputText>
																					<t:selectBooleanCheckbox id="transactionChkDelete"
																						value="#{node.canDelete}"
																						title="#{node.permission.permissionId}"
																						></t:selectBooleanCheckbox>

																				</h:panelGrid>
																			</h:panelGroup>

																		</h:panelGroup>
																	</f:facet>


																	<f:facet name="Action">

																		<h:panelGroup style="width:100%;">

																			<h:panelGroup style="width:100%;">

																				<h:panelGrid columns="6" >
																					<t:graphicImage
																						value="../resources/images/tree/Action.png"
																						rendered="#{t.nodeExpanded}" border="0" />
																					<t:graphicImage
																						value="../resources/images/tree/Action.png"
																						rendered="#{!t.nodeExpanded}" border="0" />
																					<h:outputLabel style="font-weight:bold;"
																						value="#{node.permissionName}">
																					</h:outputLabel>
																					<h:outputText style="font-weight:bold;"
																						value=" (#{node.childCount})"
																						rendered="#{!empty node.children}" />
																					<h:selectBooleanCheckbox id="actionChkView"
																						value="#{node.canView}"
																						title="#{node.permission.permissionId}"></h:selectBooleanCheckbox>
																				</h:panelGrid>
																			</h:panelGroup>

																			<h:panelGroup style="width:100%;">

																				<h:panelGrid columns="6">

																					<t:outputText
																						value="#{msg['commons.group.permissions.create']}"></t:outputText>
																					<t:selectBooleanCheckbox id="actionChkCreate"
																						value="#{node.canCreate}"
																						title="#{node.permission.permissionId}"
																						></t:selectBooleanCheckbox>
																					<t:outputText
																						value="#{msg['commons.group.permissions.update']}"></t:outputText>
																					<t:selectBooleanCheckbox id="actionChkUpdate"
																						value="#{node.canUpdate}"
																						title="#{node.permission.permissionId}"
																						></t:selectBooleanCheckbox>
																					<t:outputText
																						value="#{msg['commons.group.permissions.delete']}"></t:outputText>
																					<t:selectBooleanCheckbox id="actionChkDelete"
																						value="#{node.canDelete}"
																						title="#{node.permission.permissionId}"
																						></t:selectBooleanCheckbox>

																				</h:panelGrid>
																			</h:panelGroup>

																		</h:panelGroup>
																	</f:facet>

																</t:tree2>
															</td>
														</tr>


														<tr align="right">
															<td colspan="5">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$secPermissionsTree.cmdSavePerm_action}"
																	type="submit" onclick="javascript:ads();">
																</h:commandButton>
																&nbsp;
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.reset']}"
																	action="#{pages$secPermissionsTree.reset}"></h:commandButton>
																&nbsp;


															</td>

														</tr>

													</table>

												</div>
											</div>

										</h:form>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>