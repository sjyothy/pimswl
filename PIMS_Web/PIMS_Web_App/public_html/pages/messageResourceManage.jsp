<%-- 
  - Author: M. Shiraz Siddiqui
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Search and Updating Message Resource  
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
 	function resetValues()
    {
        
		document.getElementById("searchFrm:txtKey").value="";
		document.getElementById("searchFrm:txtMessageEn").value="";
		document.getElementById("searchFrm:txtMessageAr").value="";
     }
	function openAssetManagePopup()
	{
	    var screen_width = 1024;
        var screen_height = 490;
        window.open('assetManage.jsf','_blank','width='+(screen_width)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	
	function closeAndPassSelected()
	{
		window.opener.receiveSelectedAsset();
		window.close();		
	}
		function updateChange( txtComponent )
	{	
	    document.getElementById('searchFrm:disablingDiv').style.display='block';	
		txtComponent.nextSibling.onclick();
	}
	
	function onChkChangeComplete()
    {
      document.getElementById('searchFrm:disablingDiv').style.display='none';
    }
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

</head>
	<body class="BODY_STYLE">
	         <div class="containerDiv">
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
			                <tr >
			            	  <td colspan="2">
								<jsp:include page="header.jsp" />
							  </td>
							</tr>
					
				<tr width="100%">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
					<td width="83%"  valign="top" class="divBackgroundBody">
					<h:form id="searchFrm" style="width:99.7%;height:95%"  >
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['messageRescourceManage.Header']}" styleClass="HEADER_FONT"/>
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
								
									
							 		<div class="SCROLLABLE_SECTION">
							 		       	<div class="MESSAGE">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$messageResourceManage.impMessage}"
																styleClass="INFO_FONT" />
															<h:outputText
																value="#{pages$messageResourceManage.informationMessages}"
																escape="false" styleClass="INFO_FONT" />
														</td>
													</tr>
												</table>
											</div>
							 		      	  <div class="MARGIN" style="width:94%;margin-bottom:0px;" >
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
													<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
													</tr>
												</table>
												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">

														<tr>
															<td class="DET_SEC_TD">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['messageRescourceManage.key']}"></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<h:inputText id="txtKey"
																	styleClass="INPUT_TEXT"
																	value="#{pages$messageResourceManage.messageResourceViewForSearch.messageKey}"></h:inputText>
															</td>
															<td class="DET_SEC_TD">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['messageRescourceManage.messageEn']}"></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<h:inputText id="txtMessageEn"
																	styleClass="INPUT_TEXT"
																	value="#{pages$messageResourceManage.messageResourceViewForSearch.messageDesEn}"></h:inputText>
															</td>
														</tr>

														<tr>
															<td class="DET_SEC_TD">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['messageRescourceManage.messageAr']}"></h:outputLabel>
															</td>
															<td class="DET_SEC_TD">
																<h:inputText id="txtMessageAr"
																	styleClass="INPUT_TEXT"
																	value="#{pages$messageResourceManage.messageResourceViewForSearch.messageDesAr}"></h:inputText>
															</td>
															
														</tr>
																												
														<tr>
															<td colspan="4" CLASS="BUTTON_TD">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.search']}"
																	action="#{pages$messageResourceManage.onSearch}"
																	style="width: 75px" />

																<h:commandButton id="btnClear" type="BUTTON" styleClass="BUTTON"
																	value="#{msg['commons.clear']}"
																	onclick="javascript:resetValues();" style="width: 75px" />
																	
																	
															</td>
														</tr>
													</table>
												</div>
											</div>
							 		      
																			
											<div id="grid"
												style="width:93.5%;padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
												<div class="imag">
													&nbsp;
												</div>
												<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
												<div class="contentDiv" style="width: 100%; # width: 100%;">
												<t:dataTable id="dt1" 
												value="#{pages$messageResourceManage.messageResourceViewList}"
												binding="#{pages$messageResourceManage.tbl_MessageResource}" 
												rows="#{pages$messageResourceManage.paginatorRows}"
												preserveDataModel="false" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												width="100%">

												<t:column id="messageKey" defaultSorted="true" sortable="true" style="white-space: normal;">
													<f:facet  name="header">
														<t:outputText value="#{msg['messageRescourceManage.key']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.messageKey}" />
												</t:column>
												<t:column id="messageDesEn" sortable="true" style="white-space: normal;">
													<f:facet  name="header">
														<t:outputText  value="#{msg['messageRescourceManage.messageEn']}" />
													</f:facet>
													<t:inputTextarea styleClass="A_LEFT" value="#{dataItem.messageDesEn}" onchange="updateChange(this);"  />
													<a4j:commandLink  reRender="searchFrm:btnApplyChanges" onbeforedomupdate="javascript:onChkChangeComplete();" action="#{pages$messageResourceManage.onChange}" />
												</t:column>
												 <t:column id="messageDesAr"  sortable="true" style="white-space: normal;" >
													<f:facet  name="header">
														<t:outputText value="#{msg['messageRescourceManage.messageAr']}" />
													</f:facet>
													<t:inputTextarea styleClass="A_LEFT" value="#{dataItem.messageDesAr}" onchange="updateChange(this);" />
													<a4j:commandLink  onbeforedomupdate="javascript:onChkChangeComplete();" action="#{pages$messageResourceManage.onChange}" />													
												</t:column>
											</t:dataTable>
												</div>
												<t:div styleClass="contentDivFooter"
													style="width:101%;#width:101%;">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$messageResourceManage.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">
																<CENTER>
																	<t:dataScroller id="scroller" for="dt1"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$messageResourceManage.paginatorMaxPages}"
																		immediate="false" paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false"
																		pageIndexVar="pageNumber" styleClass="SCH_SCROLLER">
																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL"></t:graphicImage>
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
				
				
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.pageNumber}" />
																					</td>
																				</tr>
																			</table>

																		</t:div>
																	</t:dataScroller>
																</CENTER>

															</td>
														</tr>
													</table>
												</t:div>
													<div>
													<table width="100%">
														<tr>
															<td class="BUTTON_TD" colspan="4">															
																<h:commandButton id="btnApplyChanges" action="#{pages$messageResourceManage.onApplyChanges}"  value="#{msg['messageRescourceManage.btn.ApplyChanges']}" styleClass="BUTTON" style="width:auto;"/>															
															</td>
														</tr>
													</table> 
												</div>
										   </div>
									   </div> 
								   </td>
							   </tr>
						 </table>
					</h:form>
				</td>
				</tr>
				<tr style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
					<td colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
					</td>
				</tr>
			</table>
		</body>
</html>
</f:view>