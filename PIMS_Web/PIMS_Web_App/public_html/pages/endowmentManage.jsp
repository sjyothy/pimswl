<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	function onMessageFromDistributeEndowmentRevenue()
	{
	  
	  disableInputs();
	  document.getElementById("detailsFrm:onMessageFromDistributeEndowmentRevenue").onclick();
	}
	 
	
	
	function performSave(control)
	{
			
		disableInputs();
		document.getElementById("detailsFrm:lnkSave").onclick();
		return;
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
	}	
	function   showFilePersonPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('SearchPerson.jsf?persontype=FILE_PERSON&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
	function showFilePersonDetailsIcon()
	{
	    
		return document.getElementById("detailsFrm:hdnFilePersonId").length > 0
	}	
	function onMessageFromEndowmentFileBen()
	{
	     disableInputs();
	     document.getElementById("detailsFrm:onMessageFromEndowmentFileBen").onclick();
	}
	function onMessageFromEndowmentFileAsso()
	{
	     disableInputs();
	     document.getElementById("detailsFrm:onMessageFromEndowmentFileAsso").onclick();
	}
	function onMessageFromEvaluateEndowmentPopup()
	{
	 //    disableInputs();
	     document.getElementById("detailsFrm:onMessageFromEvaluateEndowmentPopup").onclick();
	}	
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		disableInputs();	
		if( hdnPersonType=='APPLICANT' ) 
		{			    			 
			 document.getElementById("detailsFrm:hdnPersonId").value=personId;
			 document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
			 document.getElementById("detailsFrm:hdnPersonName").value=personName;
			 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
			 document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;
		}
		else if(hdnPersonType=='FILE_PERSON')
	    {
	    	 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
	    	 document.getElementById("detailsFrm:hdnFilePersonId").value=personId;
	    	 document.getElementById("detailsFrm:hdnFilePersonName").value=personName;
	    	 document.getElementById("detailsFrm:txtFilePersonName").value=personName;
	    	 
	    } 
					
	     document.getElementById("detailsFrm:onMessageFromSearchPerson").onclick();
	} 
	function   openEndowmentFileBenPopup()
	{
	   var screen_width   = 0.55 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   window.open('endowmentFileBenPopup.jsf?','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	    
	}
	
	function   openThirdPartyUnitPopup()
	{
	   var screen_width   = 0.55 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   
	   window.open('thirdPartyPropUnitsPopup.jsf?','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	    
	}
	
	function openEvaluateEndowmentsPopup()
	{
	   
	   var screen_width   = 0.60 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   
	   window.open('evaluateEndowmentsPopup.jsf?','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	}
	function   openEndowmentFileAssoPopup()
	{
	   var screen_width   = 0.60 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   
	   window.open('endowmentFileAssoPopup.jsf?','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              ); 
	}
	function   openFollowupPopup()
	{
	   var screen_width = 900;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('memsFollowup.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width)+',height='+(screen_height)+
	     			 ',left=120,top=150,scrollbars=yes,status=yes,resizeable=yes');
	    
	}

	
	function onMessageFromSearchEndowments()
		{
			document.getElementById("detailsFrm:onMessageFromSearchEndowments").onclick();
		
		}

	
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<c:choose>
						<c:when test="${pages$endowmentManage.pageMode != 'POPUP'}">

							<tr
								style="height: 84px; width: 100%; # height: 84px; # width: 100%;">

								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>
					<tr width="100%">
						<c:choose>
							<c:when test="${pages$endowmentManage.pageMode != 'POPUP'}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>

						</c:choose>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['endowment.lbl.heading']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>

																<h:outputText id="errorId"
																	value="#{pages$endowmentManage.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$endowmentManage.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$endowmentManage.pageMode}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$endowmentManage.hdnPersonId}" />
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$endowmentManage.hdnPersonName}" />

																<h:commandLink
																	id="onMessageFromDistributeEndowmentRevenue"
																	action="#{pages$endowmentManage.onMessageFromDistributeEndowmentRevenue}" />

															</td>
														</tr>
													</table>

													<div class="AUC_DET_PAD">

														<div class="TAB_PANEL_INNER">
															<rich:tabPanel style="width: 100%">

																<rich:tab id="endowmentTab"
																	label="#{msg['endowmentFile.tab.Endowments']}">
																	<%@ include file="tabEndowmentManageDetails.jsp"%>
																</rich:tab>
																<rich:tab id="endowmentFile"
																	action="#{pages$endowmentManage.onEndowmentFileAssoTab }"
																	label="#{msg['endowment.lbl.endowmentFile']}">
																	<%@ include file="tabEndowmentManageFiles.jsp"%>
																</rich:tab>

																<rich:tab id="endowmentBeneficiaries"
																	action="#{pages$endowmentManage.onEndowmentFileBenTab}"
																	label="#{msg['commons.Beneficiary']}">
																	<%@ include file="tabEndowmentManageBeneficiaries.jsp"%>
																</rich:tab>
																<%--	
																<rich:tab id="revenueAndExpensesList"
																	action="#{pages$endowmentManage.onRevenueAndExpensesListTab}"
																	label="#{msg['revenueandexpenseslist.lbl.revenuesAndExpenses']}">
																	<%@ include file="tabRevenueAndExpensesList.jsp"%>
																</rich:tab>
																--%>
																<rich:tab id="thirdPartyPayments"
																	action="#{pages$endowmentManage.onThirdPartyPaymentsTab}"
																	label="#{msg['endowment.tab.thirdPartyPayments']}">
																	<%@ include file="thirdPartyPaymentsTab.jsp"%>
																</rich:tab>
																<rich:tab id="expensesTab"
																	action="#{pages$endowmentManage.onExpensesTab}"
																	label="#{msg['commons.expenses']}">
																	<%@ include file="expensesTab.jsp"%>
																</rich:tab>

																<rich:tab id="newEndowmentCompensateTab"
																	label="#{msg['commons.endowmentCompensateTabHeading']}"
																	action="#{pages$endowmentManage.onNewCompensateTabClick}">
																	<%@  include file="newEndowmentCompensate.jsp"%>
																</rich:tab>


																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>

																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>


																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$endowmentManage.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>




															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">
															<pims:security screen="Pims.EndowMgmt.EndowmentFile.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performSave(this);">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$endowmentManage.onSave}" />


															</pims:security>
															<h:commandButton styleClass="BUTTON" style="width:auto;"
																value="#{msg['report.heading.statementOfAccount']}"
																action="#{pages$endowmentManage.onOpenEndowmentStatementOfAccountCriteriaPopup}">
															</h:commandButton>
														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>


					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<c:choose>
								<c:when test="${pages$endowmentManage.pageMode != 'POPUP'}">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td class="footer">
												<h:outputLabel id="ftrMssg"
													value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
								</c:when>
							</c:choose>
						</td>
					</tr>

				</table>

			</div>
		</body>
	</html>
</f:view>