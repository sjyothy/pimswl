<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<script language="javascript" type="text/javascript">
		function disableInputs()
		{
		    var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
		}
		
		function onRegisterForEjari()
		{

		  disableInputs();
		  document.getElementById("formRequest:onRegisterForEjari").onclick();
		}
		function openMaintenanceHistoryPopUp(){
		 var screen_width = 1024;
	       var screen_height = 450;
	        window.open('MaintenanceRequestHistory.jsf?contractId='+document.getElementById("formRequest:hdnContractId").value,'_blank','width='+(screen_width-100)+',height='+(screen_height)+',left=50,top=150,scrollbars=yes,status=yes');
		}
		
		function openRequestHistoryPopUp()
        {
           
           var screen_width = 1024;
	       var screen_height = 450;
	        window.open('RequestHistoryPopUp.jsf?contractId='+document.getElementById("formRequest:hdnContractId").value,'_blank','width='+(screen_width-100)+',height='+(screen_height)+',left=50,top=150,scrollbars=yes,status=yes'); 
        }
		function openContractHistoryPopUp()
        {
           var screen_width = 1024;
	       var screen_height = 450;
	        window.open('ContractHistoryPopUp.jsf?contractId='+document.getElementById("formRequest:hdnContractId").value,'_blank','width='+(screen_width-100)+',height='+(screen_height)+',left=50,top=150,scrollbars=yes,status=yes'); 
        }
    
	function showSponsorManagerReadOnlyPopup(personIdName)
	{
	   //ID:NAME
	   var personIdNameArr=personIdName.split(":");
	   showPersonReadOnlyPopup(personIdNameArr[0]);
	}    
	function showPopup(personType)
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   //window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
	    window.open('SearchPerson.jsf?persontype='+personType+'&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	
	function showPartnerOccupierPopUp(persontype)
	{
    	var screen_width = 1024;
	    var screen_height = 470;
	    window.open('SearchPerson.jsf?persontype='+persontype+'&viewMode=popup&selectMode=many' ,'_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	
	}
	function showPersonManagerPopUp(persontype,displaycontrolname,hdncontrolname,hdndisplaycontrolname,clickable)
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   if(clickable=='true')
	   {
	     window.open('SearchPerson.jsf?persontype='+persontype+'&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	   }
	}
	
	
	function showPaymentSchedulePopUp()
	{
	 var screen_width = screen.width;
	   var screen_height = screen.height;
	   
	   window.open('PaymentSchedule.jsf?totalContractValue='+document.getElementById("formRequest:hdntotalContract").value,'_blank','width='+(screen_width-500)+',height='+(screen_height-300)+',left=300,top=250,scrollbars=yes,status=yes');
	   
	}
    
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		    
		   
		    if(hdnPersonType=='TENANT')
		    {
			 //document.getElementById("formRequest:txtTenantName").value=personName;
			 document.getElementById("formRequest:hdntenantId").value=personId;
			if(document.getElementById("formRequest:hdnApplicantId").value=="")
			 	document.getElementById("formRequest:hdnApplicantId").value=personId;
	         //document.getElementById("formRequest:hdntenanttypeId").value=typeId;
	         
	        }
	        else if(hdnPersonType=='SPONSOR')
	        {
	         document.getElementById("formRequest:sponsorPerson").value=personName;
			 document.getElementById("formRequest:hdnSponsorPersonId").value=personId+":"+personName;
	         
	        }
	        else if(hdnPersonType=='MANAGER')
	        {
	         document.getElementById("formRequest:managerPerson").value=personName;
			 document.getElementById("formRequest:hdnManagerPersonId").value=personId+":"+personName;
	         
	        }
	        else if(hdnPersonType=='APPLICANT')
	        {
	         document.getElementById("formRequest:txtName").value=personName;
			 document.getElementById("formRequest:hdnApplicantId").value=personId;
	         //document.getElementById("formRequest:hdnApplicantPerson").value=personName;
	       
	        }
	        document.forms[0].submit();
	       
	}
	function deleteContractPerson(PersonType)
	{
	
	        if(PersonType=='SPONSOR')
	        {
	         document.getElementById("formRequest:sponsorPerson").value="";
			 document.getElementById("formRequest:hdnSponsorPersonId").value="";

	        }
	        else if(PersonType=='MANAGER')
	        {
	         document.getElementById("formRequest:managerPerson").value="";
			 document.getElementById("formRequest:hdnManagerPersonId").value="";

	         
	        }
	
	}
	function isDeleteContractPersonShow(PersonType)
	{
	       
		    if(PersonType=='SPONSOR'  && document.getElementById("formRequest:hdnSponsorPersonId").value.length<=0)
	        {
			 return false;
	        }
	        else if(PersonType=='MANAGER' && document.getElementById("formRequest:hdnManagerPersonId").value.length<=0)
	        {
	         return false;
	        }
	        return true;
	}
    
	function populateUnit(unitId,unitNum,usuageTypeId,unitRentAmount,unitUsuageType,propertyCommercialName,unitAddress,unitStatusId,unitStatus)
	{
	  document.getElementById("formRequest:hdnunitId").value=unitId;
	  document.forms[0].submit();
	}
	function populateAuctionUnit(unitId,unitNum,usuageTypeId,unitRentAmount,auctionUnitId,auctionNumber,unitAddress,propertyCommercialName,
	unitUsuageType,unitStatus,unitStatusId,bidderId)
	{
	 //alert( document.getElementById("formRequest:hdnunitRentAmount") );
	 //document.getElementById("formRequest:hdnunitRentAmount").value=unitRentAmount;
	 document.getElementById("formRequest:hdnunitId").value=unitId;
	  
	 document.getElementById("formRequest:hdntenantId").value=bidderId;
	}
	function isSponsorMangerDisplay()
    {
        return document.getElementById("formRequest:hdnSponsorMangerDisplay").value;
    }
    function receiveRemarks()
	{
		document.getElementById("formRequest:cancelLink").onclick();
	}
      
    </script>
		<style type="text/css">
.test {
	width: 22px;
}
</style>

		<!-- Header -->

		<body class="BODY_STYLE">
			<div class="containerDiv">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<c:choose>
							<c:when test="${!pages$LeaseContract.isViewModePopUp}">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</c:when>
						</c:choose>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${!pages$LeaseContract.isViewModePopUp}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>
						<td width="83%" valign="top" class="divBackgroundBody">
							<h:form id="formRequest" style="width:99%;HEIGHT:95%;"
								enctype="multipart/form-data">
								<table class="greyPanelTable" cellpadding="0" cellspacing="0"
									border="0">
									<tr>
										<td class="HEADER_TD" style="width: 70%;">
											<h:outputLabel value="#{pages$LeaseContract.pageTitle}"
												styleClass="HEADER_FONT" />
											<h:commandLink id="cancelLink"
												action="#{pages$LeaseContract.cancelContract}"></h:commandLink>
										</td>
										<td>
											&nbsp;
										</td>

										<td width="30%">
											<table>
												<tr>
													<td style="width: 23px">
														<h:commandLink
															rendered="#{pages$LeaseContract.isContrctStatusTerminated && (pages$LeaseContract.isPageModeAdd || pages$LeaseContract.isPageModeUpdate) }"
															action="#{pages$LeaseContract.issueCLCmdLink_Click}">
															<h:graphicImage title="#{msg['clearanceLetter.issueCL']}"
																url="../resources/images/app_icons/Clearance-letter.png"
																onmouseover="enlargePic(this)"
																onmouseout="releasePic(this)" />
														</h:commandLink>
													</td>
					
													<td style="width: 23px;">
														<h:commandLink
															rendered="#{pages$LeaseContract.isContrctStatusActive  && (pages$LeaseContract.isPageModeAdd || pages$LeaseContract.isPageModeUpdate)}"
															action="#{pages$LeaseContract.issueNOLCmdLink_Click}">
															<h:graphicImage
																title="#{msg['contract.screenName.issueNOL']}"
																onmouseover="enlargePic(this)"
																onmouseout="releasePic(this)"
																url="../resources/images/app_icons/No-Objection-letter.png" />
														</h:commandLink>
													</td>
													<td style="width: 23px;">
														<pims:security screen="Pims.ChangeTenantName"
															action="create">
															<h:commandLink
																action="#{pages$LeaseContract.cmdChangeTenant_Click}"
																rendered="#{pages$LeaseContract.isContrctStatusActive   && (pages$LeaseContract.isPageModeAdd || pages$LeaseContract.isPageModeUpdate)}">
																<h:graphicImage
																	title="#{msg['contractList.changeTenant']}"
																	alt="#{msg['contractList.changeTenant']}"
																	url="../resources/images/app_icons/Change-the-tenant-name.png"
																	onmouseover="enlargePic(this)"
																	onmouseout="releasePic(this)" />

															</h:commandLink>

														</pims:security>
													</td>
													
													<td style="width: 23px;">
														<pims:security screen="Pims.Contract.Cancel" action="view">
															<h:commandLink
																rendered="#{pages$LeaseContract.isContrctStatusActive  && (pages$LeaseContract.isPageModeAdd || pages$LeaseContract.isPageModeUpdate)}"
																action="#{pages$LeaseContract.cmdLinkCancelContract_Click}">
																<h:graphicImage
																	title="#{msg['contract.screenName.cancelContract']}"
																	onmouseover="enlargePic(this)"
																	onmouseout="releasePic(this)"
																	url="../resources/images/app_icons/Cancel-Contract.png" />
															</h:commandLink>

														</pims:security>
													</td>
													<td style="width: 23px;">
														<h:commandLink
															action="#{pages$LeaseContract.cmdLinkSettleContract_Click}"
															rendered="#{pages$LeaseContract.isContrctStatusActive  && (pages$LeaseContract.isPageModeAdd || pages$LeaseContract.isPageModeUpdate)}">
															<h:graphicImage
																title="#{msg['contract.screenName.settleContract']}"
																onmouseover="enlargePic(this)"
																onmouseout="releasePic(this)"
																url="../resources/images/app_icons/Settle-Contract.png" />
														</h:commandLink>
													</td>
													<td style="width: 23px;">
														<h:commandLink
															rendered="#{pages$LeaseContract.isContrctStatusActive  && (pages$LeaseContract.isPageModeAdd || pages$LeaseContract.isPageModeUpdate)}"
															action="#{pages$LeaseContract.cmdTransferContract_Click}">
															<h:graphicImage
																title="#{msg['transferContract.heading']}"
																onmouseover="enlargePic(this)"
																onmouseout="releasePic(this)"
																url="../resources/images/app_icons/Transfer-Contract.png" />
														</h:commandLink>
													</td>
													<td style="width: 23px;">
														<pims:security screen="Pims.Contract.Renew"
															action="create">
															<h:commandLink
																action="#{pages$LeaseContract.renewContract}"
																rendered="#{(pages$LeaseContract.isContrctStatusActive || pages$LeaseContract.isContrctStatusExpired || pages$LeaseContract.isContrctStatusRenewDraft || pages$LeaseContract.isContrctStatusComplete)  && (pages$LeaseContract.isPageModeAdd || pages$LeaseContract.isPageModeUpdate)}">
																<h:graphicImage title="#{msg['renewContract.header']}"
																	url="../resources/images/app_icons/Renew-A-Contract.png"
																	onmouseover="enlargePic(this)"
																	onmouseout="releasePic(this)" />
															</h:commandLink>
														</pims:security>
													</td>
													<td style="width: 23px;">
														<h:commandLink
															rendered="#{pages$LeaseContract.isContrctStatusActive  && (pages$LeaseContract.isPageModeAdd || pages$LeaseContract.isPageModeUpdate)}"
															action="#{pages$LeaseContract.img_PrintConnectServicesLetter}">
															<h:graphicImage
																title="#{msg['contract.lbl.printConnectElectricServicesLetter']}"
																onmouseover="enlargePic(this)"
																onmouseout="releasePic(this)"
																url="../resources/images/app_icons/No-Objection-letter.png" />
														</h:commandLink>
													</td>
													<td style="width: 23px;">
														<h:commandLink
															rendered="#{pages$LeaseContract.isContrctStatusActive  && (pages$LeaseContract.isPageModeAdd || pages$LeaseContract.isPageModeUpdate)}"
															action="#{pages$LeaseContract.img_PrintConnectWaterServicesLetter}">
															<h:graphicImage
																title="#{msg['contract.lbl.printConnectWaterServicesLetter']}"
																onmouseover="enlargePic(this)"
																onmouseout="releasePic(this)"
																url="../resources/images/app_icons/No-Objection-letter.png" />
														</h:commandLink>
													</td>
													<td style="width: 23px;">
														<h:commandLink
															rendered="#{pages$LeaseContract.isContrctStatusActive  && pages$LeaseContract.isContractCommercial &&(pages$LeaseContract.isPageModeAdd || pages$LeaseContract.isPageModeUpdate)}"
															action="#{pages$LeaseContract.img_PrintActivityPracticingLetter}">
															<h:graphicImage
																title="#{msg['contract.lbl.printPracticingActivityLetter']}"
																onmouseover="enlargePic(this)"
																onmouseout="releasePic(this)"
																url="../resources/images/app_icons/No-Objection-letter.png" />
														</h:commandLink>
													</td>

												</tr>
											</table>
											<rich:jQuery name="enlargePic" timing="onJScall"
												query="addClass('test')" />
											<rich:jQuery name="releasePic" timing="onJScall"
												query="removeClass('test')" />
										</td>
									</tr>

								</table>
								<table width="99%" height="95%" class="greyPanelMiddleTable"
									cellpadding="0" cellspacing="0" border="0">
									<tr valign="top">
										<td height="100%" valign="top" nowrap="nowrap"
											background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
											width="1">
										</td>
										<td height="100%" valign="top" nowrap="nowrap">
											<div class="SCROLLABLE_SECTION" style="width: 815px;">

												<table border="0" class="layoutTable" width="90%"
													style="margin-left: 15px; margin-right: 15px;">
													<tr>
														<td>
															<h:outputText id="successmsg" escape="false"
																styleClass="INFO_FONT"
																value="#{pages$LeaseContract.successMessages}" />
															<h:outputText id="errormsg" escape="false"
																styleClass="ERROR_FONT"
																value="#{pages$LeaseContract.errorMessages}" />
															<h:inputHidden id="hdnTenantBlackListed"
																value="#{pages$LeaseContract.hdnTenantBlackListed}" />

															<h:inputHidden id="hdnContractId"
																value="#{pages$LeaseContract.contractId}" />
															<h:inputHidden id="hdnContractCreatedOn"
																value="#{pages$LeaseContract.contractCreatedOn}" />
															<h:inputHidden id="hdnContractCreatedBy"
																value="#{pages$LeaseContract.contractCreatedBy}" />
															<h:inputHidden id="hdntenantId"
																value="#{pages$LeaseContract.hdnTenantId}" />

															<h:inputHidden id="hdnManagerPersonId"
																value="#{pages$LeaseContract.hdnManagerId}" />
															<h:inputHidden id="hdntotalContract"
																value="#{pages$LeaseContract.txttotalContractValue}" />
															<h:inputHidden id="pageMode"
																value="#{pages$LeaseContract.pageMode}" />
															<h:inputHidden id="hdnunitId"
																value="#{pages$LeaseContract.hdnUnitId}" />
															<h:inputHidden id="hdnSponsorPersonId"
																value="#{pages$LeaseContract.hdnSponsorId}" />
															<h:inputHidden id="hdnApplicantId"
																value="#{pages$LeaseContract.hdnApplicantId}" />
																<h:commandLink
																	id="onRegisterForEjari"
																	action="#{pages$LeaseContract.onRegisterForEjari}" />
														</td>
													</tr>
												</table>

												<div class="MARGIN"
													style="height: 410px; margin-bottom: 0px;">

													<table id="table_1" cellpadding="0" cellspacing="0"
														width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<rich:tabPanel id="tabPanel"
														binding="#{pages$LeaseContract.tabPanel}"
														style="height:320px;" headerSpacing="0">

														<rich:tab id="appDetailsTab" name="appDetailsTab"
															binding="#{pages$LeaseContract.tabApplicationDetails}"
															title="#{msg['commons.tab.applicationDetails']}"
															label="#{msg['contract.tabHeading.ApplicationDetails']}">
															<%@ include file="ApplicationDetails.jsp"%>

														</rich:tab>
														<rich:tab id="tabBasicInfo"
															action="#{pages$LeaseContract.tabBasicInfo_Click}"
															title="#{msg['contract.tab.BasicInfo']}"
															label="#{msg['contract.tab.BasicInfo']}">

															<t:panelGrid columnClasses="BUTTON_TD" columns="2"
																cellpadding="1px" width="100%" cellspacing="3px"
																rendered="#{!pages$LeaseContract.isPageModeAdd}">
																<t:panelGroup colspan="10">
																	<h:commandButton type="button" styleClass="BUTTON"
																		style="width:150px;"
																		rendered="#{!pages$LeaseContract.isViewModePopUp}"
																		value="#{msg['contract.contractHistory']}"
																		title="#{msg['contract.tabtooltip.ContractHistory']}"
																		onclick="javascript:openContractHistoryPopUp();" />
																	<h:commandButton type="button" styleClass="BUTTON"
																		style="width:150px;"
																		rendered="#{!pages$LeaseContract.isViewModePopUp}"
																		title="#{msg['contract.tabtooltip.PendingRequests']}"
																		value="#{msg['contract.tabHeading.PendingRequests']}"
																		onclick="javascript:openRequestHistoryPopUp();" />
																	<h:commandButton type="button" styleClass="BUTTON"
																		style="width:150px;"
																		rendered="#{!pages$LeaseContract.isViewModePopUp}"
																		title="#{msg['contract.tabtooltip.MaintenanceHistory']}"
																		value="#{msg['contract.tabHeading.MaintenanceHistory']}"
																		onclick="javascript:openMaintenanceHistoryPopUp();" />
																</t:panelGroup>
															</t:panelGrid>
															<t:div styleClass="TAB_DETAIL_SECTION" style="width:100%">


																<t:panelGrid id="basicInfoTable" cellpadding="1px"
																	width="100%" cellspacing="3px"
																	styleClass="TAB_DETAIL_SECTION_INNER" columns="4"
																	columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.referencenumber']}:"
																		binding="#{pages$LeaseContract.lblRefNum}" />
																	<h:inputText styleClass="READONLY" maxlength="20"
																		id="txtRefNum" readonly="true"
																		binding="#{pages$LeaseContract.txtRefNumber}"
																		value="#{pages$LeaseContract.refNum}"
																		style="width:78%;"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.status']}:"></h:outputLabel>
																	<h:inputText styleClass="READONLY"
																		id="txtcontractStatus" style="width:78%;"
																		readonly="true" maxlength="20"
																		value="#{pages$LeaseContract.contractStatus}" />
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['contract.tenantName']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:panelGroup>
																		<h:inputText styleClass="READONLY" id="txtTenantName"
																			style="width:71%" readonly="true"
																			title="#{pages$LeaseContract.txtTenantName}"
																			value="#{pages$LeaseContract.txtTenantName}"></h:inputText>
																		<h:graphicImage
																			url="../resources/images/app_icons/Search-tenant.png"
																			binding="#{pages$LeaseContract.imgTenant}"
																			onclick="showPopup('#{pages$LeaseContract.personTenant}');"
																			style="MARGIN: 0px 0px -4px; CURSOR: hand;"
																			alt="#{msg[contract.searchTenant]}"></h:graphicImage>
																		<h:graphicImage
																			binding="#{pages$LeaseContract.imgViewTenant}"
																			title="#{msg['commons.view']}"
																			url="../resources/images/app_icons/Tenant.png"
																			style="MARGIN: 0px 0px -4px; CURSOR: hand;"
																			onclick="javaScript:showPersonReadOnlyPopup('#{pages$LeaseContract.hdnTenantId}');" />
																	</h:panelGroup>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['tenants.tenantsType']}:"></h:outputLabel>
																	<h:inputText styleClass="READONLY" id="txtTenantType"
																		readonly="true" style="width:78%"
																		value="#{pages$LeaseContract.txtTenantType}"
																		title="#{pages$LeaseContract.txtTenantType}"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['person.blacklisted']}:"></h:outputLabel>
																	<h:inputText styleClass="READONLY"
																		id="txtTenantBlackListed" readonly="true"
																		style="width:78%"
																		value="#{pages$LeaseContract.hdnTenantBlackListed=='1'?msg['commons.true']:msg['commons.true']}"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contract.issueDate']}:"></h:outputLabel>
																	<rich:calendar id="contractIssueDate"
																		value="#{pages$LeaseContract.contractIssueDate}"
																		popup="true"
																		datePattern="#{pages$LeaseContract.dateFormat}"
																		binding="#{pages$LeaseContract.issueDateCalendar}"
																		timeZone="#{pages$LeaseContract.timeZone}"
																		showApplyButton="false" disabled="true"
																		enableManualInput="false" cellWidth="24px"
																		inputStyle="width:71%"
																		locale="#{pages$LeaseContract.locale}"></rich:calendar>
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel id="lblContractType" styleClass="LABEL"
																			value="#{msg['contract.contractType']}:"></h:outputLabel>
																	</h:panelGroup>
																	<h:selectOneMenu id="selectcontractType"
																		style="width: 80%; "
																		binding="#{pages$LeaseContract.cmbContractType}"
																		value="#{pages$LeaseContract.selectOneContractType}">
																		<f:selectItem itemValue="-1"
																			itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																		<f:selectItems
																			value="#{pages$LeaseContract.contractTypeList}" />
																		<a4j:support event="onchange"
																			action="#{pages$LeaseContract.selectContractType_Change}"
																			reRender="tabPanel,contractStartDate,datetimeContractEndDate" />
																	</h:selectOneMenu>


																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contract.date.Start']}:"></h:outputLabel>

																	<rich:calendar id="contractStartDate"
																		value="#{pages$LeaseContract.contractStartDate}"
																		popup="true"
																		datePattern="#{pages$LeaseContract.dateFormat}"
																		binding="#{pages$LeaseContract.startDateCalendar}"
																		timeZone="#{pages$LeaseContract.timeZone}"
																		showApplyButton="false" enableManualInput="false"
																		cellWidth="24px" inputStyle="width:71%"
																		locale="#{pages$LeaseContract.locale}"
																		todayControlMode="hidden">
																		<a4j:support event="onchanged"
																			action="#{pages$LeaseContract.onContractDurationChanged}"
																			reRender="totalContract,hdntotalContract,txtactualRent,tbl_PaymentSchedule,errormsg" />
																	</rich:calendar>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contract.date.expiry']}:"></h:outputLabel>

																	<rich:calendar id="datetimeContractEndDate"
																		value="#{pages$LeaseContract.contractEndDate}"
																		binding="#{pages$LeaseContract.endDateCalendar}"
																		popup="true"
																		datePattern="#{pages$LeaseContract.dateFormat}"
																		timeZone="#{pages$LeaseContract.timeZone}"
																		showApplyButton="false" enableManualInput="false"
																		cellWidth="24px" inputStyle="width:71%"
																		todayControlMode="hidden">
																		<a4j:support event="onchanged"
																			action="#{pages$LeaseContract.onContractDurationChanged}"
																			reRender="totalContract,hdntotalContract,txtactualRent,tbl_PaymentSchedule,errormsg" />

																	</rich:calendar>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contract.TotalContractValue']}:"></h:outputLabel>
																	<h:inputText id="totalContract" style="width:78%;"
																		styleClass="A_RIGHT_NUM READONLY" maxlength="15"
																		readonly="true"
																		value="#{pages$LeaseContract.txttotalContractValue}">

																	</h:inputText>
																	<h:outputLabel id="lblSponsor" styleClass="LABEL"
																		rendered="#{pages$LeaseContract.isContractCommercial}"
																		value="#{msg['contract.person.sponsor']}:"></h:outputLabel>

																	<h:panelGroup id="grpSponsorIcons"
																		rendered="#{pages$LeaseContract.isContractCommercial}">
																		<h:inputText id="sponsorPerson" readonly="true"
																			style="width:78%"
																			title="#{pages$LeaseContract.txtSponsor}"
																			value="#{pages$LeaseContract.txtSponsor}">

																		</h:inputText>
																		<h:graphicImage id="imgSearchSpnsr"
																			alt="#{msg['contract.person.sponsor']}"
																			url="../resources/images/app_icons/Search-tenant.png"
																			onclick="javascript:showPersonManagerPopUp('#{pages$LeaseContract.personSponsor}','formRequest:sponsorPerson','formRequest:hdnSponsorPersonId','formRequest:hdnSponsorPerson','#{pages$LeaseContract.isItemEditable}')"
																			style="MARGIN: 0px 0px -4px;CURSOR: hand;"
																			binding="#{pages$LeaseContract.imgSponsor}"></h:graphicImage>
																		<h:graphicImage id="imgViewSpnsr"
																			binding="#{pages$LeaseContract.imgViewSponsor}"
																			onclick="javaScript:showSponsorManagerReadOnlyPopup('#{pages$LeaseContract.hdnSponsorId}');"
																			title="#{msg['commons.view']}"
																			url="../resources/images/app_icons/Tenant.png"
																			style="MARGIN: 0px 0px -4px; CURSOR: hand;" />
																		<h:graphicImage id="imgDeleteSponsor"
																			style="MARGIN: 0px 0px -4px;CURSOR: hand;"
																			title="#{msg['commons.delete']}"
																			url="../resources/images/delete_icon.png"
																			binding="#{pages$LeaseContract.imgDeleteSponsor}"
																			onclick="javascript:deleteContractPerson('#{pages$LeaseContract.personSponsor}')">
																			<a4j:support event="onclick"
																				reRender="grpSponsorIcons,hdnSponsorPersonId,sponsorPerson,imgSearchSpnsr,imgViewSpnsr,imgDeleteSponsor"
																				action="#{pages$LeaseContract.imgDelSponsor_Click}" />
																		</h:graphicImage>

																	</h:panelGroup>

																	<h:outputLabel id="lblManager" styleClass="LABEL"
																		rendered="#{pages$LeaseContract.isContractCommercial}"
																		value="#{msg['contract.person.Manager']}:"></h:outputLabel>
																	<h:panelGroup id="grpManagerIcons"
																		rendered="#{pages$LeaseContract.isContractCommercial}">
																		<h:inputText id="managerPerson" readonly="true"
																			style="width:83%"
																			value="#{pages$LeaseContract.txtManager}"
																			title="#{pages$LeaseContract.txtManager}">
																		</h:inputText>
																		<h:graphicImage id="imgManager"
																			style="MARGIN: 0px 0px -4px;CURSOR: hand;"
																			alt="#{msg['contract.person.Manager']}"
																			url="../resources/images/app_icons/Search-tenant.png"
																			onclick="javascript:showPersonManagerPopUp('#{pages$LeaseContract.personManager}',
																				                   'formRequest:managerPerson',
																				                   'formRequest:hdnManagerPersonId',
																				                   'formRequest:hdnManagerPerson',
																				                   '#{pages$LeaseContract.isItemEditable}')"
																			binding="#{pages$LeaseContract.imgManager}">
																		</h:graphicImage>
																		<h:graphicImage id="imgViewMgr"
																			onclick="javaScript:showSponsorManagerReadOnlyPopup('#{pages$LeaseContract.hdnManagerId}');"
																			binding="#{pages$LeaseContract.imgViewManager}"
																			title="#{msg['commons.view']}"
																			url="../resources/images/app_icons/Tenant.png"
																			style="MARGIN: 0px 0px -4px; CURSOR: hand;"></h:graphicImage>
																		<h:graphicImage id="imgDeleteManager"
																			style="MARGIN: 0px 0px -4px;CURSOR: hand;"
																			title="#{msg['commons.delete']}"
																			url="../resources/images/delete_icon.png"
																			onclick="javascript:deleteContractPerson('#{pages$LeaseContract.personManager}')"
																			binding="#{pages$LeaseContract.imgDeleteManager}">
																			<a4j:support event="onclick"
																				reRender="grpManagerIcons,hdnManagerPersonId,managerPerson,imgManager,imgDeleteManager,imgViewMgr"
																				action="#{pages$LeaseContract.imgDelManager_Click}" />
																		</h:graphicImage>

																	</h:panelGroup>

																	<h:outputLabel id="lblBlockComment" styleClass="LABEL"
																		value="#{msg['contract.label.BlockComment']}"></h:outputLabel>
																	<h:inputText id="txtBlockComment" readonly="true"
																		style="width:78%" styleClass="READONLY"
																		value="#{pages$LeaseContract.blockComment}">
																	</h:inputText>

																	<h:outputLabel id="lblUnblockComment"
																		styleClass="LABEL"
																		value="#{msg['contract.label.UnBlockComment']}"></h:outputLabel>
																	<h:inputText id="txtUnblockComment" readonly="true"
																		style="width:78%" styleClass="READONLY"
																		value="#{pages$LeaseContract.unblockComment}">
																	</h:inputText>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.EjariNum']}:" />

																	<h:panelGroup id="grpEjariPrinting">
																		<h:inputText styleClass="READONLY" maxlength="20"
																			id="txtEjariNum" readonly="true"
																			binding="#{pages$LeaseContract.txtEjariNum}"
																			style="width:78%;" />
																			
																		<h:outputLink id="linkPrintEjariCertificate"
																			binding="#{pages$LeaseContract.linkPrintEjariCertificate}"  
																			
																			>
																			
																			<h:graphicImage id="imgPrintCertificate"
																			
																				title="#{msg['commons.printEjariCertificate']}"
																				url="../resources/images/app_icons/print.gif"
																				style="MARGIN: 0px 0px -4px; CURSOR: hand;" />
																		</h:outputLink >
																		<h:outputLink id="linkPrintEjariContract"  
																			binding="#{pages$LeaseContract.linkPrintEjariContract}"
																			>
																			<h:graphicImage id="imgPrintEjariContract"
																				title="#{msg['commons.printEjariContract']}"
																				url="../resources/images/app_icons/print.gif"
																				style="MARGIN: 0px 0px -4px; CURSOR: hand;" />
																		</h:outputLink >
																	</h:panelGroup>
																</t:panelGrid>

															</t:div>
														</rich:tab>

														<rich:tab id="tabUnit"
															title="#{msg['contract.tab.UnitInfo']}"
															label="#{msg['contract.tab.UnitInfo']}">
															<t:div style="width:100%;MARGIN: 0px 4px 0px 0px;">
																<t:panelGrid width="100%" border="0" columns="3"
																	columnClasses="BUTTON_TD">
																	<t:panelGroup colspan="10">
																		<h:commandButton id="auctionUnitspop"
																			styleClass="BUTTON"
																			action="#{pages$LeaseContract.openAuctionUnitsPopUp}"
																			value="#{msg['contract.auctionUnits']}"
																			binding="#{pages$LeaseContract.btnAddAuctionUnit}"
																			style="width:110px;" />
																		<h:commandButton id="unit" styleClass="BUTTON"
																			action="#{pages$LeaseContract.openUnitsPopUp}"
																			value="#{msg['units.unit']}"
																			binding="#{pages$LeaseContract.btnAddUnit}"
																			style="width:75px;" />

																	</t:panelGroup>
																</t:panelGrid>
															</t:div>
															<%@ include file="../pages/contractUnit.jsp"%>
														</rich:tab>

														<rich:tab id="tabPersonOccupier"
															title="#{msg['contract.person.Occupier']}"
															label="#{msg['contract.person.Occupier']}"
															binding="#{pages$LeaseContract.tabOccupier}"
															rendered="#{pages$LeaseContract.isCompanyTenant}">

															<t:div style="width:100%;MARGIN: 0px 4px 0px 0px;">
																<t:panelGrid width="100%" border="0" columns="3"
																	columnClasses="BUTTON_TD">
																	<t:panelGroup colspan="12">

																		<h:commandButton id="btnOccupiers" styleClass="BUTTON"
																			value="#{msg['contract.AddOccupier']}"
																			binding="#{pages$LeaseContract.btnOccuipers}"
																			style="width:150px;"
																			onclick="javascript:showPartnerOccupierPopUp('#{pages$LeaseContract.personOccupier}')" />

																	</t:panelGroup>
																</t:panelGrid>
															</t:div>


															<t:div styleClass="contentDiv" style="width:99%">
																<t:dataTable id="tbl_occupier" rows="59" width="100%"
																	value="#{pages$LeaseContract.prospectiveOccupierDataList}"
																	binding="#{pages$LeaseContract.propspectiveOccupierDataTable}"
																	preserveDataModel="true" preserveSort="true"
																	var="prospectiveOccupierDataItem"
																	rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true">
																	<t:column id="PersonName">


																		<f:facet name="header">

																			<t:outputText value="#{msg['customer.firstname']}" />

																		</f:facet>
																		<t:outputText styleClass="A_LEFT" id="txt_name"
																			value="#{prospectiveOccupierDataItem.firstName} #{prospectiveOccupierDataItem.middleName} #{prospectiveOccupierDataItem.lastName}" />
																	</t:column>
																	<t:column id="Passport">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['customer.passportNumber']}" />

																		</f:facet>
																		<t:outputText styleClass="A_RIGHT_NUM" title=""
																			value="#{prospectiveOccupierDataItem.passportNumber}" />
																	</t:column>

																	<t:column id="Visa">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['customer.residenseVisaNumber']}" />

																		</f:facet>
																		<t:outputText styleClass="A_RIGHT_NUM"
																			value="#{prospectiveOccupierDataItem.residenseVisaNumber}" />
																	</t:column>

																	<t:column id="License">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['customer.drivingLicenseNumber']}" />

																		</f:facet>
																		<t:outputText styleClass="A_RIGHT_NUM" title=""
																			value="#{prospectiveOccupierDataItem.drivingLicenseNumber}" />
																	</t:column>

																	<t:column id="SSN">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['customer.socialSecNumber']}" />
																		</f:facet>
																		<t:outputText styleClass="A_RIGHT_NUM" title=""
																			value="#{prospectiveOccupierDataItem.socialSecNumber}" />
																	</t:column>
																	<t:column id="DeleteOccupier">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<h:graphicImage style="padding-left:4px;"
																			title="#{msg['commons.view']}"
																			url="../resources/images/app_icons/Tenant.png"
																			onclick="javaScript:showPersonReadOnlyPopup('#{prospectiveOccupierDataItem.personId}');" />
																		<a4j:commandLink id="del_cmdLink_tblOccupier"
																			reRender="tbl_occupier"
																			action="#{pages$LeaseContract.btnOccupierDelete_Click}">
																			<h:graphicImage id="deleteOccupier"
																				binding="#{pages$LeaseContract.btnOccupierDelete}"
																				title="#{msg['commons.delete']}"
																				url="../resources/images/delete_icon.png" />
																		</a4j:commandLink>

																	</t:column>
																</t:dataTable>
															</t:div>
														</rich:tab>
														<rich:tab id="tabPersonPartnr"
															title="#{msg['contract.person.Partners']}"
															label="#{msg['contract.person.Partners']}"
															binding="#{pages$LeaseContract.tabPartner}"
															rendered="#{pages$LeaseContract.isContractCommercial}">
															<t:div style="width:100%;MARGIN: 0px 4px 0px 0px;">
																<t:panelGrid width="100%" border="0" columns="3"
																	columnClasses="BUTTON_TD">
																	<t:panelGroup colspan="12">

																		<h:commandButton id="btnPartners" styleClass="BUTTON"
																			value="#{msg['contract.AddPartner']}"
																			binding="#{pages$LeaseContract.btnPartners}"
																			style="width:150px;"
																			onclick="javascript:showPartnerOccupierPopUp('#{pages$LeaseContract.personPartner}')" />
																	</t:panelGroup>
																</t:panelGrid>
															</t:div>
															<t:div styleClass="contentDiv" style="width:99%">
																<t:dataTable id="partnerDataList" rows="20" width="100%"
																	value="#{pages$LeaseContract.prospectivepartnerDataList}"
																	binding="#{pages$LeaseContract.propspectivepartnerDataTable}"
																	preserveDataModel="true" preserveSort="true"
																	var="prospectivepartnerDataItem" rowClasses="row1,row2"
																	rules="all" renderedIfEmpty="true">
																	<t:column id="PersonName">


																		<f:facet name="header">

																			<t:outputText id="lblpartnerFirstName"
																				value="#{msg['customer.firstname']}" />

																		</f:facet>
																		<t:outputText id="txtpartnerFirstName"
																			styleClass="A_LEFT"
																			value="#{prospectivepartnerDataItem.firstName} #{prospectivepartnerDataItem.middleName} #{prospectivepartnerDataItem.lastName}" />
																	</t:column>
																	<t:column id="Passport">


																		<f:facet name="header">

																			<t:outputText id="lblpartnerPassport"
																				value="#{msg['customer.passportNumber']}" />

																		</f:facet>
																		<t:outputText id="txtpartnerPassport"
																			styleClass="A_RIGHT_NUM" title=""
																			value="#{prospectivepartnerDataItem.passportNumber}" />
																	</t:column>

																	<t:column id="Visa">


																		<f:facet name="header">

																			<t:outputText id="lblpartnerVisa"
																				value="#{msg['customer.residenseVisaNumber']}" />

																		</f:facet>
																		<t:outputText id="txtpartnerVisa"
																			styleClass="A_RIGHT_NUM"
																			value="#{prospectivepartnerDataItem.residenseVisaNumber}" />
																	</t:column>

																	<t:column id="License">


																		<f:facet name="header">

																			<t:outputText id="lblpartnerLicense"
																				value="#{msg['customer.drivingLicenseNumber']}" />

																		</f:facet>
																		<t:outputText id="txtpartnerLicense"
																			styleClass="A_RIGHT_NUM" title=""
																			value="#{prospectivepartnerDataItem.drivingLicenseNumber}" />
																	</t:column>

																	<t:column id="SSN">
																		<f:facet name="header">
																			<t:outputText id="lblSSN"
																				value="#{msg['customer.socialSecNumber']}" />
																		</f:facet>
																		<t:outputText id="TXTSSN" styleClass="A_RIGHT_NUM"
																			title=""
																			value="#{prospectivepartnerDataItem.socialSecNumber}" />
																	</t:column>
																	<t:column id="DeletePartner">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<h:graphicImage style="padding-left:4px;"
																			title="#{msg['commons.view']}"
																			url="../resources/images/app_icons/Tenant.png"
																			onclick="javaScript:showPersonReadOnlyPopup('#{prospectivepartnerDataItem.personId}');" />
																		<a4j:commandLink reRender="partnerDataList"
																			action="#{pages$LeaseContract.btnPartnerDelete_Click}">
																			<h:graphicImage id="deletePartner"
																				binding="#{pages$LeaseContract.btnPartnerDelete}"
																				title="#{msg['commons.delete']}"
																				url="../resources/images/delete_icon.png" />
																		</a4j:commandLink>

																	</t:column>


																</t:dataTable>
															</t:div>




														</rich:tab>
														<rich:tab id="tabCommercialActivity"
															rendered="#{pages$LeaseContract.isContractCommercial}"
															binding="#{pages$LeaseContract.tabCommercialActivity}"
															action="#{pages$LeaseContract.tabCommercialActivity_Click}"
															title="#{msg['contract.tab.CommercialActivity']}"
															label="#{msg['contract.tab.CommercialActivity']}">


															<t:div styleClass="contentDiv" style="align:center;">
																<t:dataTable id="commercialActivityDataTable" rows="85"
																	width="100%"
																	value="#{pages$LeaseContract.commercialActivityList}"
																	binding="#{pages$LeaseContract.commercialActivityDataTable}"
																	preserveDataModel="false" preserveSort="false"
																	var="commercialActivityItem" rowClasses="row1,row2"
																	rules="all" renderedIfEmpty="true">

																	<t:column id="colActivityName">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['contract.tab.CommercialActivity.Name']}" />

																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{commercialActivityItem.commercialActivityName}" />
																	</t:column>
																	<t:column id="colActivityDescEn"
																		rendered="#{pages$LeaseContract.isEnglishLocale}">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['contract.tab.CommercialActivity.description']}" />

																		</f:facet>
																		<t:outputText title="" styleClass="A_LEFT"
																			value="#{commercialActivityItem.commercialActivityDescEn}" />
																	</t:column>
																	<t:column id="colActivityDescAr"
																		rendered="#{pages$LeaseContract.isArabicLocale}">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['contract.tab.CommercialActivity.description']}" />

																		</f:facet>
																		<t:outputText title="" styleClass="A_LEFT"
																			value="#{commercialActivityItem.commercialActivityDescAr}" />
																	</t:column>
																	<t:column id="colActivitySelected">


																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.select']}" />
																		</f:facet>
																		<t:selectBooleanCheckbox
																			binding="#{pages$LeaseContract.chkCommercial}"
																			value="#{commercialActivityItem.isSelected}" />

																	</t:column>

																</t:dataTable>
															</t:div>

														</rich:tab>
														<rich:tab id="tabPaidFacilities"
															binding="#{pages$LeaseContract.tabFacilities}"
															title="#{msg['contract.Facilities']}"
															label="#{msg['contract.Facilities']}"
															action="#{pages$LeaseContract.tabPaidFacility_Click}">
															<t:div styleClass="contentDiv" style="align:center;">
																<t:dataTable id="paidFaciliites" rows="55" width="100%"
																	value="#{pages$LeaseContract.paidFacilitiesDataList}"
																	binding="#{pages$LeaseContract.paidFacilitiesDataTable}"
																	preserveDataModel="true" preserveSort="true"
																	var="paidFacilitiesDataItem" rowClasses="row1,row2"
																	rules="all" renderedIfEmpty="true">

																	<t:column id="col2">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['paidFacilities.facilityName']}" />

																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paidFacilitiesDataItem.facilityName}" />
																	</t:column>
																	<t:column id="col4">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['paidFacilities.facilityDescription']}" />

																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paidFacilitiesDataItem.facilityDescription}" />
																	</t:column>

																	<t:column id="col5">
																		<f:facet name="header">
																			<t:outputText value="#{msg['paidFacilities.Type']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paidFacilitiesDataItem.isMandatory=='N'?msg['paidFacilities.IsMandatory.Optional']:msg['paidFacilities.IsMandatory.Mandatory']}" />
																	</t:column>

																	<t:column id="col7">


																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paidFacilities.rentValue']}" />
																		</f:facet>
																		<t:outputText title="" styleClass="A_RIGHT_NUM"
																			value="#{paidFacilitiesDataItem.rent}">
																			<f:convertNumber minFractionDigits="2"
																				maxFractionDigits="2" pattern="##,###,###.##" />
																		</t:outputText>
																	</t:column>
																	<t:column id="colFacilitySelected">


																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.select']}" />
																		</f:facet>
																		<t:selectBooleanCheckbox
																			disabled="#{paidFacilitiesDataItem.isMandatory=='Y'}"
																			binding="#{pages$LeaseContract.chkPaidFacilities}"
																			value="#{paidFacilitiesDataItem.selected}">
																			<a4j:support event="onclick"
																				reRender="totalContract,hdntotalContract"
																				action="#{pages$LeaseContract.chkPaidFacilities_Changed}" />
																		</t:selectBooleanCheckbox>
																	</t:column>


																</t:dataTable>
															</t:div>





														</rich:tab>
														<rich:tab id="tabPaymentSchedule"
															binding="#{pages$LeaseContract.tabPaymentTerms}"
															action="#{pages$LeaseContract.tabPaymentTerms_Click}"
															title="#{msg['contract.paymentTerms']}"
															label="#{msg['contract.paymentTerms']}">
															<t:div style="width:100%;MARGIN: 0px 4px 0px 0px;">
																<t:panelGrid width="100%" border="0" columns="3"
																	columnClasses="BUTTON_TD">
																	<t:panelGroup colspan="12">


																		<h:commandButton id="pmt_sch_add" styleClass="BUTTON"
																			value="#{msg['contract.AddpaymentSchedule']}"
																			binding="#{pages$LeaseContract.btnAddPayments}"
																			action="#{pages$LeaseContract.btnAddPayments_Click}"
																			style="width:150px;" />
																		<h:commandButton id="pmt_sch_gen" styleClass="BUTTON"
																			value="#{msg['contract.paymentSchedule.GeneratePayments']}"
																			rendered="#{pages$LeaseContract.isPageModeAdd || pages$LeaseContract.isPageModeAmendAdd}"
																			style="width:150px;"
																			action="#{pages$LeaseContract.btnGenPayments_Click}"
																			binding="#{pages$LeaseContract.btnGenPayments}" />
																	</t:panelGroup>
																</t:panelGrid>
															</t:div>
															<t:div styleClass="contentDiv" style="width:98.2%">
																<t:dataTable id="tbl_PaymentSchedule" rows="7"
																	width="100%"
																	value="#{pages$LeaseContract.paymentScheduleDataList}"
																	binding="#{pages$LeaseContract.paymentScheduleDataTable}"
																	preserveDataModel="true" var="paymentScheduleDataItem"
																	rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true">
																	<t:column id="select" style="width:5%;"
																		rendered="#{(pages$LeaseContract.isContrctStatusActive || pages$LeaseContract.isContractStatusApproved) && (pages$LeaseContract.isPageModeAdd || pages$LeaseContract.isPageModeUpdate)}">
																		<f:facet name="header">
																			<t:outputText id="selectTxt"
																				value="#{msg['commons.select']}" />
																		</f:facet>
																		<t:selectBooleanCheckbox id="selectChk"
																			value="#{paymentScheduleDataItem.selected}"
																			rendered="#{paymentScheduleDataItem.isReceived == 'N'  && paymentScheduleDataItem.statusId==pages$LeaseContract.paymentSchedulePendingStausId && paymentScheduleDataItem.paymentScheduleId>0}" />
																	</t:column>
																	<t:column id="col_PaymentNumber" style="width:15%;"
																		rendered="#{!pages$LeaseContract.isPageModeAdd}">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentNumber']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentNumber}" />
																	</t:column>
																	<t:column id="col_ReceiptNumber" style="width:15%;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.ReceiptNumber']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentReceiptNumber}" />
																	</t:column>
																	<t:column id="colTypeEn" style="width:8%;"
																		rendered="#{pages$LeaseContract.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentType']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.typeEn}" />
																	</t:column>
																	<t:column id="colTypeAr" style="width:8%;"
																		rendered="#{pages$LeaseContract.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentType']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.typeAr}" />
																	</t:column>
																	<%-- 
																	<t:column id="colDesc" rendered="false">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.description']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.description}" />
																	</t:column>
																	 --%>
																	<t:column id="colDueOn" style="width:8%;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.paymentDueOn']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentDueOn}">
																			<f:convertDateTime
																				pattern="#{pages$LeaseContract.dateFormat}"
																				timeZone="#{pages$LeaseContract.timeZone}" />
																		</t:outputText>
																	</t:column>
																	<t:column id="colPaymentDate" style="width:8%;">
																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['paymentSchedule.paymentDate']}">
																				<f:convertDateTime
																					pattern="#{pages$LeaseContract.dateFormat}"
																					timeZone="#{pages$LeaseContract.timeZone}" />
																			</t:outputText>
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentDate}">
																			<f:convertDateTime
																				pattern="#{pages$LeaseContract.dateFormat}"
																				timeZone="#{pages$LeaseContract.timeZone}" />
																		</t:outputText>
																	</t:column>

																	<t:column id="colModeEn" style="width:8%;"
																		rendered="#{pages$LeaseContract.isEnglishLocale}">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['paymentSchedule.paymentMode']}" />

																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentModeEn}" />
																	</t:column>
																	<t:column id="colModeAr" style="width:8%;"
																		rendered="#{pages$LeaseContract.isArabicLocale}">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['paymentSchedule.paymentMode']}" />

																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentModeAr}" />
																	</t:column>
																	<t:column id="colStatusEn"
																		rendered="#{pages$LeaseContract.isEnglishLocale}">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['paymentSchedule.paymentStatus']}" />

																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.statusEn}" />
																	</t:column>
																	<t:column id="colStatusAr"
																		rendered="#{pages$LeaseContract.isArabicLocale}">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['paymentSchedule.paymentStatus']}" />

																		</f:facet>
																		<t:outputText styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.statusAr}" />
																	</t:column>
																	<t:column id="colAmount">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.amount']}" />
																		</f:facet>
																		<t:outputText title="" styleClass="A_RIGHT_NUM"
																			value="#{paymentScheduleDataItem.amount}">
																			<f:convertNumber minFractionDigits="2"
																				maxFractionDigits="2" pattern="##,###,###.##" />
																		</t:outputText>
																	</t:column>
																	<%-- Temporarily eplaced with btnEditPaymentSchedule_Click--%>
																	<t:column id="col8">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		<jsp:include page="imageForLeaseContract.jsp"></jsp:include>
																	</t:column>
																</t:dataTable>

															</t:div>
															<t:div id="pagingDivPaySch" styleClass="contentDivFooter"
																style="width:99.2%;">

																<t:dataScroller id="scrollerPaySch"
																	for="tbl_PaymentSchedule" paginator="true" fastStep="1"
																	paginatorMaxPages="15" immediate="false"
																	paginatorTableClass="paginator"
																	renderFacetsIfSinglePage="true"
																	pageIndexVar="pageNumber"
																	paginatorTableStyle="grid_paginator"
																	layout="singleTable"
																	paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																	paginatorActiveColumnStyle="font-weight:bold;"
																	paginatorRenderLinkForActive="false"
																	styleClass="SCH_SCROLLER">
																	<f:facet name="first">
																		<t:graphicImage url="../#{path.scroller_first}"
																			id="lblFPaySch"></t:graphicImage>
																	</f:facet>

																	<f:facet name="fastrewind">
																		<t:graphicImage url="../#{path.scroller_fastRewind}"
																			id="lblFRPaySch"></t:graphicImage>
																	</f:facet>

																	<f:facet name="fastforward">
																		<t:graphicImage url="../#{path.scroller_fastForward}"
																			id="lblFFPaySch"></t:graphicImage>
																	</f:facet>

																	<f:facet name="last">
																		<t:graphicImage url="../#{path.scroller_last}"
																			id="lblLPaySch"></t:graphicImage>
																	</f:facet>


																</t:dataScroller>

															</t:div>
															<f:verbatim>
																<br></br>
															</f:verbatim>
															<t:div id="divCollectPayment" styleClass="BUTTON_TD"
																style="padding:10px">
																<h:commandButton id="btnCollectPayment"
																	binding="#{pages$LeaseContract.btnCollectPayment}"
																	value="#{msg['settlement.actions.collectpayment']}"
																	styleClass="BUTTON"
																	action="#{pages$LeaseContract.openReceivePaymentsPopUp}"
																	style="width: 150px"
																	rendered="#{pages$LeaseContract.isContractStatusApproved || pages$LeaseContract.isContrctStatusActive}" />
															</t:div>
															<f:verbatim>
																<br></br>
															</f:verbatim>
															<t:div styleClass="TAB_DETAIL_SECTION" style="width:99%;">
																<t:panelGrid id="paymentSummary" cellpadding="1px"
																	width="100%" cellspacing="3px"
																	styleClass="TAB_DETAIL_SECTION_INNER" columns="4"
																	columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">

																	<t:outputLabel styleClass="A_LEFT"
																		value="#{msg['payments.totalCash']} :" />
																	<t:outputLabel styleClass="A_LEFT"
																		value="#{pages$LeaseContract.totalCashAmount}">
																		<f:convertNumber minFractionDigits="2"
																			maxFractionDigits="2" pattern="##,###,###.##" />
																	</t:outputLabel>

																	<t:outputLabel styleClass="A_LEFT"
																		value="#{msg['payments.totalChq']} :" />
																	<t:outputLabel styleClass="A_LEFT"
																		value="#{pages$LeaseContract.totalChqAmount}">
																		<f:convertNumber minFractionDigits="2"
																			maxFractionDigits="2" pattern="##,###,###.##" />
																	</t:outputLabel>

																	<t:outputLabel styleClass="A_LEFT"
																		value="#{msg['payments.totalCashCheque']} :" />
																	<t:outputLabel styleClass="A_LEFT"
																		value="#{pages$LeaseContract.totalChqAmount  + pages$LeaseContract.totalCashAmount}">
																		<f:convertNumber minFractionDigits="2"
																			maxFractionDigits="2" pattern="##,###,###.##" />
																	</t:outputLabel>

																	<t:outputLabel styleClass="A_LEFT"
																		value="#{msg['payments.totalDeposit']} :" />
																	<t:outputLabel styleClass="A_LEFT"
																		value="#{pages$LeaseContract.totalDepositAmount}">
																		<f:convertNumber minFractionDigits="2"
																			maxFractionDigits="2" pattern="##,###,###.##" />
																	</t:outputLabel>

																	<t:outputLabel styleClass="A_LEFT"
																		value="#{msg['payments.totalRent']} :" />
																	<t:outputLabel styleClass="A_LEFT"
																		value="#{pages$LeaseContract.totalRentAmount}">
																		<f:convertNumber minFractionDigits="2"
																			maxFractionDigits="2" pattern="##,###,###.##" />
																	</t:outputLabel>


																	<t:outputLabel styleClass="A_LEFT"
																		value="#{msg['payments.totalFees']} :" />
																	<t:outputLabel styleClass="A_LEFT"
																		value="#{pages$LeaseContract.totalFees}">
																		<f:convertNumber minFractionDigits="2"
																			maxFractionDigits="2" pattern="##,###,###.##" />
																	</t:outputLabel>

																	<t:outputLabel styleClass="A_LEFT"
																		value="#{msg['payments.totalFines']} :" />
																	<t:outputLabel styleClass="A_LEFT"
																		value="#{pages$LeaseContract.totalFines}">
																		<f:convertNumber minFractionDigits="2"
																			maxFractionDigits="2" pattern="##,###,###.##" />
																	</t:outputLabel>

																	<t:outputLabel styleClass="A_LEFT"
																		value="#{msg['payments.noOfInstallments']} :" />
																	<t:outputLabel styleClass="A_LEFT"
																		value="#{pages$LeaseContract.installments}" />
																	<t:outputLabel
																		value="#{msg['payments.totalRealizedRentAmount']} :" />
																	<t:outputLabel
																		value="#{pages$LeaseContract.totalRealizedRentAmount}" />

																</t:panelGrid>
															</t:div>

														</rich:tab>


														<rich:tab id="attachmentTab"
															label="#{msg['commons.attachmentTabHeading']}"
															action="#{pages$LeaseContract.tabAttachmentsComments_Click}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}"
															action="#{pages$LeaseContract.tabAttachmentsComments_Click}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														<rich:tab
															label="#{msg['contract.tabHeading.RequestHistory']}"
															title="#{msg['commons.tab.requestHistory']}"
															action="#{pages$LeaseContract.tabAuditTrail_Click}">
															<%@ include file="../pages/requestTasks.jsp"%>
														</rich:tab>

													</rich:tabPanel>



													<table cellpadding="0" cellspacing="0" style="width: 100%;">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>




													<table cellpadding="1px" cellspacing="0px" width="100%">
														<tr>
															<td colspan="12" class="BUTTON_TD"
																style="padding-top: 10px;">

																<pims:security
																	screen="Pims.LeaseContractManagement.LeaseContract.AddNewLease"
																	action="create">
																	<h:commandButton type="submit" styleClass="BUTTON"
																		action="#{pages$LeaseContract.btnSubmit_Click}"
																		binding="#{pages$LeaseContract.btnSaveNewLease}"
																		value="#{msg['commons.saveButton']}" />
																	<h:commandButton type="submit" styleClass="BUTTON"
																		action="#{pages$LeaseContract.btnSendNewLeaseForApproval}"
																		binding="#{pages$LeaseContract.btnSendNewLeaseForApproval}"
																		value="#{msg['commons.sendButton']}" />
																	<h:commandButton id="btnActivateContract"
																		binding="#{pages$LeaseContract.btnComplete}"
																		value="#{msg['commons.complete']}" styleClass="BUTTON"
																		action="#{pages$LeaseContract.onComplete}"
																		style="width: 150px"
																		rendered="#{pages$LeaseContract.isContractStatusApproved }" />
																	<h:commandButton id="btnRegisterEjari" type="submit"
																		styleClass="BUTTON"
																		onclick="javaScript:onRegisterForEjari();"
																		
																		binding="#{pages$LeaseContract.btnRegisterEjari}"
																		value="#{msg['contract.btn.Ejari']}"
																		style="width: auto;" />
																		
																	<h:commandButton id="btnTerminateInEjari" type="submit"
																		styleClass="BUTTON"
																		action="#{pages$LeaseContract.onTerminateInEjari}"
																		binding="#{pages$LeaseContract.btnTerminateInEjari}"
																		value="#{msg['cancelContract.btn.TerminateEjari']}"
																		style="width: auto;" />
																</pims:security>

																<pims:security
																	screen="Pims.LeaseContractManagement.LeaseContract.Approve"
																	action="create">
																	<h:commandButton type="submit" styleClass="BUTTON"
																		action="#{pages$LeaseContract.btnApprove_Click}"
																		value="#{msg['commons.approve']}"
																		rendered="#{(pages$LeaseContract.isPageUpdateStatusNew) && !pages$LeaseContract.isContractStatusApproved}"
																		binding="#{pages$LeaseContract.btnApprove}" />
																	<h:commandButton type="submit" styleClass="BUTTON"
																		rendered="#{pages$LeaseContract.isPageUpdateStatusNew && !pages$LeaseContract.isContractStatusApproved}"
																		action="#{pages$LeaseContract.btnReject_Click}"
																		value="#{msg['commons.reject']}"
																		binding="#{pages$LeaseContract.btnReject}" />
																</pims:security>
																<h:commandButton id="btnCancelContract"
																	binding="#{pages$LeaseContract.btnCancelContract}"
																	value="#{msg['commons.cancelContract']}"
																	styleClass="BUTTON"
																	action="#{pages$LeaseContract.btnCancelContract_Click}"
																	style="width: 150px" />
																<pims:security
																	screen="Pims.LeaseContractManagement.LeaseContract.PritnNewLease"
																	action="create">
																	<h:commandButton id="btnPrint" type="submit"
																		styleClass="BUTTON"
																		rendered="#{pages$LeaseContract.isContrctStatusActive && !pages$LeaseContract.isViewModePopUp}"
																		action="#{pages$LeaseContract.btnPrint_Click}"
																		binding="#{pages$LeaseContract.btnPrint}"
																		value="#{msg['commons.print']}" />
																	<h:commandButton id="btnPrintDisclosure" type="submit"
																		styleClass="BUTTON"
																		rendered="#{pages$LeaseContract.isContrctStatusActive && !pages$LeaseContract.isViewModePopUp}"
																		style="width:auto;"
																		action="#{pages$LeaseContract.btnPrintDisclosure_Click}"
																		value="#{msg['commons.button.printDisclosure']}" />
																</pims:security>
																<h:commandButton id="popUpCancel" styleClass="BUTTON"
																	type="button" onclick="javascript:window.close();"
																	value="#{msg['commons.cancel']}"
																	rendered="#{pages$LeaseContract.isViewModePopUp}">
																</h:commandButton>
																<h:commandButton id="btnContractPmnt"
																	styleClass="BUTTON" style="width:auto;"
																	binding="#{pages$LeaseContract.btnAllPayment}"
																	action="#{pages$LeaseContract.printContractPaymentReport}"
																	value="#{msg['infoMsg.printAllPayments']}">
																</h:commandButton>
															</td>
														</tr>

													</table>

												</div>
											</div>
										</td>
									</tr>
								</table>
							</h:form>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>

