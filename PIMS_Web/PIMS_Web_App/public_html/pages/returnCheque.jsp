<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html style="overflow:hidden;" dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<script language="javascript" type="text/javascript">
	function GenerateFloorsPopup()
	{
	   var screen_width = screen.width;
	   var screen_height = screen.height;
	   var screen_top = screen.width/4;
	   var screen_left = screen.height/4;
	   window.open('GenerateFloors.jsf','_blank','width='+(screen_width-300)+',height='+(screen_height-500)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}
		function OwnerSearchPopup()
	{
	   var screen_width = screen.width;
	   var screen_height = screen.height;
	   var screen_top = screen.width/4;
	   var screen_left = screen.height/4;
	   window.open('SearchPersonPopup.jsf','_blank','width='+(screen_width-280)+',height='+(screen_height-380)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}
	function GenerateUnitsPopup()
	{
	   var screen_width = screen.width;
	   var screen_height = screen.height;
	   var screen_top = screen.width/4;
	   var screen_left = screen.height/4;
	   window.open('GenerateUnits.jsf','_blank','width='+(screen_width-300)+',height='+(screen_height-500)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}
	       function submitForm()
	   {
          document.getElementById('ReceivePropertyForm').submit();
	   }
	function FacilitiesPopup()
	{
   var screen_width = screen.width;
	   var screen_height = screen.height;
	   var screen_top = screen.width/4;
	   var screen_left = screen.height/4;
	   window.open('FacilityListPopup.jsf','_blank','width='+(screen_width-280)+',height='+(screen_height-400)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=no,status=yes,location=no,directories=no,resizable=no');	
	}
	function  showPersonPopup()
	{
	 var screen_width = 1024;
	 var screen_height = 470;
	 window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
      {
		    document.getElementById("returnChequeForm:hdnPersonId").value=personId;
		    document.getElementById("returnChequeForm:hdnCellNo").value=cellNumber;
		    document.getElementById("returnChequeForm:hdnPersonName").value=personName;
		    document.getElementById("returnChequeForm:hdnPersonType").value=hdnPersonType;
		    document.getElementById("returnChequeForm:hdnIsCompany").value=isCompany;
			
	       document.forms[0].submit();
		}

		function closeWindow(){
		window.opener.btnSearch_Click();
		window.close();
		 
		}
</script>
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<style>
.COL1 {
	width: 18%;
}

input {
	width: 200px;
}

label {
	PADDING-RIGHT: 5px;
	PADDING-LEFT: 5px;
}
</style>


		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">

					<tr width="100%">
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['returnCheque.Heading']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0" height="88%">
								<tr valign="top">
									<td height="70%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<td width="99%" valign="top" nowrap="nowrap">
										<h:form id="returnChequeForm" enctype="multipart/form-data">




											<div>
												<table border="0" class="layoutTable"
													style="margin-left: 15px; margin-right: 15px;">
													<tr>
														<td>

															<h:outputText id="successMsg_2"
																value="#{pages$returnCheque.successMessages}"
																escape="false" styleClass="INFO_FONT" />

														</td>
													</tr>
												</table>
											</div>

											<table cellpadding="1px" cellspacing="6px"
												class="layoutTable" width="100%">

												<tr>
													<td>
														&nbsp;&nbsp;&nbsp;
													</td>
													<td>
														<h:outputText id="Label_Date" styleClass="LABEL"
															value="#{msg['commons.date']}:" />
													</td>
													<td>
														<h:outputText id="txt_Date" styleClass="LABEL"
															value="#{pages$returnCheque.toDate}">
															<f:convertDateTime
																pattern="#{pages$returnCheque.dateFormat}"
																timeZone="#{pages$returnCheque.timeZone}" />
														</h:outputText>
													</td>
													<td>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														&nbsp;&nbsp;
													</td>
													<td>
														<h:outputText id="Label_Remarks" styleClass="LABEL"
															value="#{msg['commons.reasons']}:" />
													</td>
													<td>
														<t:inputTextarea id="txt_Reasons" cols="100"
															value="#{pages$returnCheque.txtRemarks}" />
													</td>
												</tr>

											</table>

											<div id="divheader" class="TAB_PANEL_MARGIN"
												style="padding-top: 7px;">
												<div class="imag">
													&nbsp;
												</div>

												<div id="conDiv" class="contentDiv" style="width: 88.2%">

													<t:dataTable id="chequeDetaildataTable" width="100%"
														value="#{pages$returnCheque.bounceChequeList}"
														binding="#{pages$returnCheque.dataTableChequeDetail}"
														preserveDataModel="false" preserveSort="false"
														var="chequeDetailDataItem" rowClasses="row1,row2"
														renderedIfEmpty="true">

														<t:column id="ChequeContractNumber" width="20%"
															style="white-space:normal;">
															<f:facet name="header">
																<t:outputText id="ac201"
																	value="#{msg['bouncedChequesList.contractNumberCol']}" />
															</f:facet>
															<t:outputText id="ColContNumber" styleClass="A_LEFT"
																value="#{chequeDetailDataItem.number}" />
														</t:column>
														<t:column id="Gpaymuber" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['chequeList.paymentNumber']}" />
															</f:facet>
															<t:outputText id="cmdlink" styleClass="A_LEFT"
																value="#{chequeDetailDataItem.paymentNumber}"
																style="white-space: normal;" />
															<t:outputText />
														</t:column>
														
														<t:column id="GpaymentTypeEn"
															rendered="#{pages$returnCheque.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['cancelContract.payment.paymenttype']}" />
															</f:facet>
															<t:outputText id="payTypeEn" styleClass="A_LEFT"
																value="#{chequeDetailDataItem.paymentTypeDescriptionEn}"
																style="white-space: normal;" />
														</t:column>
														<t:column id="GpaymentTypeAr"
															rendered="#{pages$returnCheque.isArabicLocale}">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['cancelContract.payment.paymenttype']}" />
															</f:facet>
															<t:outputText id="payTypeAr" styleClass="A_LEFT"
																value="#{chequeDetailDataItem.paymentTypeDescriptionAr}"
																style="white-space: normal;" />
														</t:column>
														<t:column id="chequeNumberEn" width="10%"
															style="white-space:normal;">
															<f:facet name="header">
																<t:outputText id="ac171"
																	value="#{msg['bouncedChequesList.chequeNumberCol']}" />
															</f:facet>
															<t:outputText id="ac161" style="white-space:normal;"
																styleClass="A_LEFT" title=""
																value="#{chequeDetailDataItem.chequeNumber}" />
														</t:column>
														<t:column id="chequeAmount" width="10%"
															style="white-space:normal;">
															<f:facet name="header">
																<t:outputText id="descHead1"
																	value="#{msg['chequeList.amount']}" />
															</f:facet>
															<t:outputText id="descText1" style="white-space:normal;"
																styleClass="A_LEFT" title=""
																value="#{chequeDetailDataItem.paymentDetailAmount}" />
														</t:column>
														<t:column id="chequeNumberAr" width="10%"
															style="white-space:normal;"
															rendered="#{pages$returnCheque.isArabicLocale}">
															<f:facet name="header">
																<t:outputText id="ac151"
																	value="#{msg['bouncedChequesList.dueDateCol']}" />
															</f:facet>
															<t:outputText id="ac141" style="white-space:normal;"
																styleClass="A_LEFT" title=""
																value="#{chequeDetailDataItem.methodReferenceDate}" />
														</t:column>
														<t:column id="ChequeReceiptNumber" width="20%"
															style="white-space:normal;">
															<f:facet name="header">
																<t:outputText id="ac201222"
																	value="#{msg['chequeList.receiptNumber']}" />
															</f:facet>
															<t:outputText id="ac191" styleClass="A_LEFT"
																value="#{chequeDetailDataItem.receiptNumber}" />
														</t:column>
														<t:column id="GBankEn"
															rendered="#{pages$returnCheque.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText value="#{msg['chequeList.bankNameCol']}" />
															</f:facet>
															<t:outputText id="bankEn" styleClass="A_LEFT"
																value="#{chequeDetailDataItem.bankNameEn}"
																style="white-space: normal;" />
														</t:column>
														<t:column id="GBankAr"
															rendered="#{pages$returnCheque.isArabicLocale}">
															<f:facet name="header">
																<t:outputText value="#{msg['chequeList.bankNameCol']}" />
															</f:facet>
															<t:outputText id="BankAr" styleClass="A_LEFT"
																value="#{chequeDetailDataItem.bankNameAr}"
																style="white-space: normal;" />
														</t:column>
														<t:column id="ChequeStatusEn" width="10%"
															style="white-space:normal;"
															rendered="#{pages$returnCheque.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText id="ac111"
																	value="#{msg['commons.status']}" />
															</f:facet>
															<t:outputText id="ac101" style="white-space:normal;"
																styleClass="A_LEFT" title=""
																value="#{chequeDetailDataItem.statusEn}" />
														</t:column>
														<t:column id="ChequeStatusAr" width="10%"
															style="white-space:normal;"
															rendered="#{pages$returnCheque.isArabicLocale}">
															<f:facet name="header">
																<t:outputText id="ac9" value="#{msg['commons.status']}" />
															</f:facet>
															<t:outputText id="ac8" style="white-space:normal;"
																styleClass="A_LEFT" title=""
																value="#{chequeDetailDataItem.statusAr}" />
														</t:column>
														<t:column id="TenantNameCol" width="10%"
															style="white-space:normal;">
															<f:facet name="header">
																<t:outputText id="ac1111212"
																	value="#{msg['bouncedChequesList.tenantNameCol']}" />
															</f:facet>
															<t:outputText id="ac82321213" style="white-space:normal;"
																styleClass="A_LEFT" title=""
																value="#{chequeDetailDataItem.tenantName}" />
														</t:column>
														<t:column id="bouncedReason" >
															<f:facet name="header">
																<t:outputText value="#{msg['commons.label.bouncedReason']}" />
															</f:facet>
															<t:inputTextarea id="txtbouncedReason" styleClass="TEXTAREA"
																value="#{chequeDetailDataItem.bouncedReason}"
																rows="1" style="width: 150px;height:35px;" />
														</t:column>
														<t:column id="ActionsCol" width="10%"
															style="white-space:normal;">
															<f:facet name="header">
																<t:outputText id="lblAction"
																	value="#{msg['commons.action']}" />
															</f:facet>
															<h:commandLink id="followUpLink"
																rendered="#{!chequeDetailDataItem.terminateRequestPresent}"
																action="#{pages$returnCheque.onBounceAndCreateFollowup}">
																<h:graphicImage  title="#{msg['returnCheque.bounceFollowUp']}"
																	url="../resources/images/app_icons/Pay-Fine.png" />
															</h:commandLink>
														</t:column>
													</t:dataTable>
												</div>
											</div>
											<table width="90%">
												<tr>
													<td class="BUTTON_TD" colspan="4">

														<h:commandButton styleClass="BUTTON" type="submit"
															style="width: 119px"
															binding="#{pages$returnCheque.saveBinding}"
															action="#{pages$returnCheque.btn_save}"
															value="#{msg['commons.saveButton']}" />

														<h:commandButton styleClass="BUTTON" type="submit"
															style="width: 119px" rendered="false"
															binding="#{pages$returnCheque.sendForLegalDepartment}"
															action="#{pages$returnCheque.btn_SendToDepartment}"
															value="#{msg['returnCheque.sendToLegalDepartment']}" />

														<h:commandButton styleClass="BUTTON" type="button"
															onclick="javascript:closeWindow();"
															value="#{msg['commons.cancel']}" />




													</td>
												</tr>
											</table>
											</div>
										</h:form>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>