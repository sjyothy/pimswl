<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for listing blocking details of person 
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">



			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">





				<tr width="100%">



					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['zakat.heading.zakatTrx']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="searchFrm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$ZakatPersonalAccountTransLinkPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">

												<div class="DETAIL_SECTION" style="width: 99.8%;">
													<%--<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>--%>
													<t:div styleClass="contentDiv" style="width:95%">


														<t:dataTable id="dataTablePayments" rows="15" width="100%"
															value="#{pages$ZakatPersonalAccountTransLinkPopup.list}"
															binding="#{pages$ZakatPersonalAccountTransLinkPopup.dataTable}"
															preserveDataModel="false" preserveSort="false"
															var="dataItem" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true">
															<%-- 
															<t:column id="refNumcolblocking" sortable="true">
																<f:facet name="header">
																	<t:outputText id="refNumHeadingblocking"
																		value="#{msg['commons.refNum']}" />
																</f:facet>
																<t:outputText id="refNumblocking" styleClass="A_LEFT"
																	value="#{dataItem.refNum}" />
															</t:column>
															--%>

															<t:column id="desc" sortable="true"
																style="white-space: normal;">
																<f:facet name="header">
																	<t:outputText id="hdTo"
																		value="#{msg['commons.description']}" />
																</f:facet>
																<div style="width: 70%; white-space: normal;">
																	<t:outputText id="p_w" styleClass="A_LEFT"
																		style="white-space: normal;"
																		value="#{pages$ZakatPersonalAccountTransLinkPopup.englishLocale? dataItem.transDescEn:dataItem.transDescAr}" />
																</div>
															</t:column>
															<t:column id="transDateCol" sortable="true">
																<f:facet name="header">
																	<t:outputText id="hdtransDateCol"
																		value="#{msg['zakat.lbl.transDate']}" />
																</f:facet>
																<t:outputText id="transDateColt" styleClass="A_LEFT"
																	value="#{dataItem.transDate}">
																	<f:convertDateTime
																		timeZone="#{pages$zakatSearch.timeZone}"
																		pattern="#{pages$zakatSearch.dateFormat}" />
																</t:outputText>
															</t:column>

															<t:column id="amntCol" sortable="true">
																<f:facet name="header">
																	<t:outputText id="hdAmnt"
																		value="#{msg['commons.amount']}" />
																</f:facet>
																<t:outputText id="amountt" styleClass="A_LEFT"
																	value="#{dataItem.amount}" />
															</t:column>
															<%-- 
															<t:column id="balance" sortable="true">
																<f:facet name="header">
																	<t:commandSortHeader columnName="balance"
																		actionListener="#{pages$zakatSearch.sort}"
																		value="#{msg['mems.normaldisb.label.balance']}"
																		arrow="true">
																		<f:attribute name="sortField" value="balance" />
																	</t:commandSortHeader>
																</f:facet>
																<t:outputText value="#{dataItem.balance}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															--%>
														</t:dataTable>
													</t:div>
												</div>
											</div>

										</div>


										</div>
									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>


		</body>
	</html>
</f:view>