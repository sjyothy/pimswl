
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<f:view>
   <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
   <f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>						
            <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
			

			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			


		<script language="javascript" type="text/javascript">

	function closeWindow()
	{
	  window.opener="x";
	  window.close();
	}
	
	//function Calendar(control,control2,x,y)
     // {

       // var ctl2Name="formContractList:"+control2;
       // var ctl2=document.getElementById(ctl2Name);
       // showCalendar(control,ctl2,'dd/mm/yyyy','','',x,y); 
      //  return false;
      //}

	
</SCRIPT>

	</head>

	<body>
		
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['renewLeaseContractList.header']}" styleClass="HEADER_FONT"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="99%" valign="top" nowrap="nowrap">

									<div class="SCROLLABLE_SECTION" style="height: 450px;">

										<h:form id="renewContractListForm"
											enctype="multipart/form-data">

											<h:outputText escape="false"
												value="#{pages$inspectionDetails.errorMessages}" />

											<div class="MARGIN" style="width: 95%">
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td>
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" style="vertical-align: bottom;"/>
														</td>
														<td width="100%">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" style="vertical-align: bottom;" />
														</td>
														<td>
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" style="vertical-align: bottom;" />
														</td>
													</tr>
												</table>

												<div class="DETAIL_SECTION">
													<h:outputLabel
														value="#{msg['renewLeaseContractList.header']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">


														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.date.fromExpiry']}"></h:outputLabel>
																:
															</td>
															<td width="25%">


																<rich:calendar id="fromContractDateRichCalander"
																	datePattern="MM/dd/yyyy"
																	value="#{pages$RenewContractListBackingBean.fromDateDate}" />
															</td>
															<td width="25%"><h:outputLabel styleClass="LABEL" value="#{msg['contract.date.toExpiry']}"></h:outputLabel> 
																
																:
															</td>



															<td width="25%"><rich:calendar id="toContractDateRichCalander" datePattern="MM/dd/yyyy" value="#{pages$RenewContractListBackingBean.toDateDate}"/></td>
														</tr>
														<tr>
															<td><h:outputLabel styleClass="LABEL" value="#{msg['contract.contractStatus']}"></h:outputLabel> 
																:
															</td>

															<td><t:selectOneMenu id="contractStatus" value="#{pages$RenewContractListBackingBean.contractStatus}" style="width: 127px">
																	<f:selectItem id="selectAllType" itemValue="-1" itemLabel="#{msg['commons.All']}" />
																	<f:selectItems value="#{pages$RenewContractListBackingBean.statusList}" />

																</t:selectOneMenu>



																





															</td>
															<td>
																&nbsp;
															</td>




															<td>&nbsp;</td>
														</tr>

														<tr>
															<td>&nbsp;</td>

															<td>
																&nbsp;

															</td>

															<td >
																
																&nbsp;
															</td>

															<td><h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.search']}"
																	action="#{pages$RenewContractListBackingBean.searchContracts}" />
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	onclick="javascript:closeWindow();"></h:commandButton></td>
														</tr>


													</table>

												</div>
											</div>

											<br>
											<br>

											<div style="padding: 5px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" align="center" style="width: 95%">

													<t:dataTable id="test2" preserveDataModel="false"
														preserveSort="false" var="dataItem" rules="all"
														renderedIfEmpty="true" width="100%" rows="15"
														value="#{pages$RenewContractListBackingBean.contractViewList}"
														binding="#{pages$RenewContractListBackingBean.dataTable}">

														<t:column id="colTenetName" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['tenants.name']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.tenantFirstName}" />
														</t:column>

														<t:column id="coContractNumber" sortable="true">
															<f:facet name="header">
																<t:commandLink
																	action="#{pages$RenewContractListBackingBean.getContractDetails}"
																	value="#{msg['contract.contractNumber']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.contractNumber}" />
														</t:column>

														<t:column id="colRentAmouont" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.rentAmount']}" />
															</f:facet>
															<t:outputText styleClass="A_RIGHT_NUM"
																value="#{dataItem.rentAmount}" />
														</t:column>

														<t:column id="col3" sortable="true">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['renewLeaseContractList.seggestedRentAmount']}" />
															</f:facet>
															<t:outputText styleClass="A_RIGHT_NUM"
																value="#{dataItem.suggestedRentAmount}" />
														</t:column>

														<t:column id="col4" sortable="true">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['renewLeaseContractList.fine']}" />
															</f:facet>


															<t:outputText styleClass="A_LEFT"
																value="#{msg['commons.yes']}"
																rendered="#{dataItem.hasFine}" />
															<t:outputText styleClass="A_LEFT"
																value="#{msg['commons.no']}"
																rendered="#{dataItem.hasNotFine}" />


														</t:column>

														<t:column id="statusEn" sortable="true"
															rendered="#{pages$RenewContractListBackingBean.isEnglishLocale}">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.status']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.statusEn}" />
														</t:column>

														<t:column id="statusAr" sortable="true"
															rendered="#{pages$RenewContractListBackingBean.isArabicLocale}">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.status']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.statusAr}" />
														</t:column>

														<t:column id="expieryDate" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['contract.endDate']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.originalEndDateString}" />
														</t:column>
														
														<t:column id="col6Notification" >
															<f:facet name="header">
																<t:outputText value="#{msg['renewLeaseContractList.sendNotification']}" />
															</f:facet>
															<h:selectBooleanCheckbox></h:selectBooleanCheckbox>
														
														</t:column>

														<t:column id="col6" width="50" >
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>
															
															<pims:security screen="Pims.Contract.Renew"	action="create">
																<h:commandLink
																	action="#{pages$RenewContractListBackingBean.renewContract}">
																	<h:graphicImage title="#{msg['renewContract.header']}"
																		url="../resources/images/app_icons/Renew-A-Contract.png" />
																</h:commandLink>
															</pims:security>
														</t:column>
													</t:dataTable>


												</div>

												<div class="contentDivFooter" style="width: 97%"
													align="right">
													<t:dataScroller id="scroller" for="test2" paginator="true"
														fastStep="1" paginatorMaxPages="2" immediate="false"
														styleClass="scroller" paginatorTableClass="paginator"
														renderFacetsIfSinglePage="true"
														paginatorTableStyle="grid_paginator" layout="singleTable"
														paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
														paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

														<f:facet name="first">
															<t:graphicImage url="../resources/images/first_btn.gif"
																id="lblF"></t:graphicImage>
														</f:facet>

														<f:facet name="last">
															<t:graphicImage url="../resources/images/last_btn.gif"
																id="lblL"></t:graphicImage>

														</f:facet>

														<f:facet name="fastforward">
															<t:graphicImage url="../resources/images/next_btn.gif"
																id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage
																url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>
														</f:facet>

													</t:dataScroller>

												</div>
											</div>





											<div class="A_RIGHT" style="width: 97%">

												<h:commandButton styleClass="BUTTON"
													value="#{msg['renewLeaseContractList.sendNotification']}"
													action="" style="width: 107px" />
											</div>
										</h:form>
									</div>

								</td>
								<td></td>
							</tr>

						</table>
					</td></tr>
							<tr>
    <td colspan="2">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
       <tr><td class="footer"><h:outputLabel value="#{msg['commons.footer.message']}" /></td></tr>
    </table>
        </td>
    </tr>
						</table>					
												
			</body>
		
	</html>
</f:view>