<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	    var selectedUnits;


	function clearWindow()
	{
	       
	       document.getElementById("formPerson:txtUnitNumber").value="";
		    document.getElementById("formPerson:txtRentMin").value="";
		    document.getElementById("formPerson:txtRentMax").value="";
		    document.getElementById("formPerson:txtBedsMin").value="";
		    document.getElementById("formPerson:txtBedsMax").value="";
		    
		   
		    
		     var cmbUnitType=document.getElementById("formPerson:selectUnitType");
		     cmbUnitType.selectedIndex = 0;
		    
		    var cmbUnitUsage=document.getElementById("formPerson:selectUnitUsage");
		     cmbUnitUsage.selectedIndex = 0;
		    
		    var cmdCountry=document.getElementById("formPerson:country");
		     cmdCountry.selectedIndex = 0;
		     
		      var cmdCity=document.getElementById("formPerson:state");
		     cmdCity.selectedIndex = 0;
		    
		    var cmdState=document.getElementById("formPerson:selectcity");
		     cmdState.selectedIndex = 0;
	}

  function closeWindow(){
   window.close();
     
     }
    
    	function Calendar(control,control2,x,y)
		{
		  var ctl2Name="formUnits:"+control2;
		  
		  var ctl2=document.getElementById(ctl2Name);
		  showCalendar(control,ctl2,'dd/mm/yyyy','','',x,y); 
		  return false;
		
		
		}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
	<div class="containerDiv">
		
		
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">

<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>

				<tr width="100%">

					<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>

					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table  class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['inquiry.Person.inquiryPersonSearch']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" >
							<tr valign="top">
								<td>

											<h:form id="formPerson" enctype="multipart/form-data" >
											<div class="SCROLLABLE_SECTION" >
												<div>
													<table border="0" class="layoutTable">
														<tr>
															<td>
																<h:outputText value="#{pages$PersonList.errorMessages}" escape="false" styleClass="ERROR_FONT" />
														<h:outputText value="#{pages$PersonList.successMessages}" escape="false" styleClass="INFO_FONT" />
															</td>
														</tr>
													</table>
												</div>
												<div class="MARGIN" style="width:95%;">
													<table cellpadding="0" cellspacing="0" width="100%">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
											<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
													<div class="DETAIL_SECTION" style="width:99.6%;">
														<h:outputLabel value="#{msg['inquiry.searchcriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER">
                                                           <tr>
													<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.unitType']}:"></h:outputLabel>
													</td>
													<td >
														<h:selectOneMenu  id="selectUnitType" value="#{pages$PersonList.selectOneUnitType}"
														style="width: 192px; height: 24px" 
														>
															 <f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1"/>
															<f:selectItems value="#{pages$ApplicationBean.unitType}"/>
														</h:selectOneMenu>
													</td>
													<td >
													    <h:outputLabel styleClass="LABEL" value="#{msg['inquiry.unitUsage']}:"></h:outputLabel>
													 </td>   
													 <td>
													     <h:selectOneMenu  id="selectUnitUsage" value="#{pages$PersonList.selectOneUnitUsage}"
													     style="width: 192px; height: 24px" 
													     >
															 <f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1"/>
   															<f:selectItems value="#{pages$ApplicationBean.propertyUsageType}"/>
														</h:selectOneMenu>
														
													</td>
																																							
												</tr>
												<tr>
													 <td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.unitNumber']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText id="txtUnitNumber" value="#{pages$PersonList.unitNumber}" style="width: 186px; height: 16px">
												        </h:inputText>
													</td>
												     <td>
												        <h:outputLabel  styleClass="LABEL" value="#{msg['contact.country']}:"></h:outputLabel>
												     </td>   
												        <td>
															<h:selectOneMenu id="country"  value="#{pages$PersonList.countryId}"
															style="width: 192px; height: 24px"
															> 
																<f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems value="#{pages$PersonList.countryList}" />
                                                             <a4j:support event="onchange" action="#{pages$PersonList.loadState}" reRender="state,selectcity" />
															</h:selectOneMenu>
														</td>
																								    
											   </tr>
											   <tr>
											   	<td>	
													     	<h:outputLabel styleClass="LABEL"  value="#{msg['contact.city']}:"></h:outputLabel>
													    </td>
													    <td> 	
														    <h:selectOneMenu id="state"
																style="width: 192px; height: 24px"  required="false"
																value="#{pages$PersonList.stateId}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems value="#{pages$PersonList.stateList}" />
															<a4j:support event="onchange" action="#{pages$PersonList.loadCity}" reRender="selectcity" />
															</h:selectOneMenu>
														</td>
												     
	
											    <td>
															<h:outputLabel  styleClass="LABEL" value="#{msg['commons.area']}:"></h:outputLabel>
												    </td>
												    <td>
														    <h:selectOneMenu id="selectcity"  value="#{pages$PersonList.cityId}"
														    style="width: 192px; height: 24px"
														    >
														                <f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1"/>
															            <f:selectItems value="#{pages$PersonList.cityList}"/>
														    </h:selectOneMenu>
												
													</td>
												
													
														    
											   </tr>
											   <tr>
											   	<td>
														<h:outputLabel styleClass="LABEL"  value="#{msg['inquiry.rentValueMin']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText id="txtRentMin" value="#{pages$PersonList.rentValueMin}" style="width: 186px; height: 16px">
												        
												        </h:inputText>
													</td>
												
													<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.rentValueMax']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText id="txtRentMax" value="#{pages$PersonList.rentValueMax}"
												        style="width: 186px; height: 16px"
												        >
												        
												        </h:inputText>
													</td>
											   
													 	
												</tr>
												
												<tr>
												<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.noOfBedsMin']}:"></h:outputLabel>
												  </td>
												  <td>
												        <h:inputText id="txtBedsMin" value="#{pages$PersonList.noOfBedsMin}"
												        style="width: 186px; height: 16px"
												        >
												        </h:inputText>
											     </td>
										                     <td>
																	<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.noOfBedsMax']}:"></h:outputLabel>
															</td>
															<td>
															        <h:inputText id="txtBedsMax" value="#{pages$PersonList.noOfBedsMax}"
															        style="width: 186px; height: 16px"
															        >
															        </h:inputText>
														    </td>
												</tr>
												
												<tr>
													
													<td colspan="6" class="BUTTON_TD">
														<h:commandButton styleClass="BUTTON" type="submit"
															value="#{msg['commons.search']}" 
															action="#{pages$PersonList.doSearch}">
														</h:commandButton>
														<h:commandButton styleClass="BUTTON" type="button"
															onclick="javascript:clearWindow();"
															value="#{msg['commons.reset']}" >
														</h:commandButton>
														<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}"
												         actionListener="#{pages$PersonList.cancel}"
												         onclick="if (!confirm('#{msg['auction.cancelConfirm']}')) return false"
												         tabindex="16"/>

													</td>
												</tr>

														</table>
													</div>
												</div>
												<div
													style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;width:95%;">
													<div class="imag">
														&nbsp;
													</div>
													<div class="contentDiv" style="width:99%" >
														<t:dataTable id="personGrid"
													value="#{pages$PersonList.propertyInquiryDataList}"
													binding="#{pages$PersonList.dataTable}" rows="#{pages$PersonList.paginatorRows}"
													width="100%" preserveDataModel="false" preserveSort="false"
													var="dataItem" rowClasses="row1,row2" rules="all"
													renderedIfEmpty="true">

													<t:column id="col1"  sortable="true">

														<f:facet name="header">

															<t:outputText value="#{msg['customer.firstname']}" />

														</f:facet>
														<t:outputText styleClass="A_LEFT"  value="#{dataItem.personView.personFullName}" />
													</t:column>

													<t:column id="col4" >

														<f:facet name="header">

															<t:outputText value="#{msg['customer.passportNumber']}" />
														</f:facet>
														<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.personView.passportNumber}" />
													</t:column>
													<t:column  id="col5" >

														<f:facet name="header">

															<t:outputText value="#{msg['customer.residenseVisaNumber']}" />
														</f:facet>
														<t:outputText styleClass="A_RIGHT_NUM"  value="#{dataItem.personView.residenseVisaNumber}" />
													</t:column>
													

													

											      
											       <t:column id="col8" >

														<f:facet name="header">

															<t:outputText value="#{msg['customer.cell']}" />
														</f:facet>
														<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.personView.cellNumber}" />
													</t:column>

											<t:column id="col9" sortable="true" >
														<f:facet name="header">
															<t:outputText value="#{msg['inquiry.priority']}" />
														</f:facet>
														<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.priorityEn}" />
													</t:column>
							                 <t:column id="col10"  sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['inquiry.inquiryDate']}" />
														</f:facet>
														<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.inquiriesDate}">
														<f:convertDateTime pattern="#{pages$PersonList.dateFormat}" timeZone="#{pages$PersonList.timeZone}" />
												    </t:outputText>
														
													</t:column>        
												 	<t:column id="col11" >

														<f:facet name="header">
															<t:outputText value="#{msg['contact.email']}" />
														</f:facet>
														<h:selectBooleanCheckbox id="email" value="#{dataItem.selectedEmail}" disabled="#{dataItem.personView.isEmail}" />
														
													</t:column>
											        <t:column id="col12" >
														<f:facet name="header">
															<t:outputText value="#{msg['commons.sms']}" />
														</f:facet>
														<h:selectBooleanCheckbox id="sms" value="#{dataItem.selectedSms}" disabled="#{dataItem.personView.isSms}" />
														
													</t:column> 

												</t:dataTable>
													</div>
													<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:100%;#width:100%;" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$PersonList.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
												
													
																	<CENTER>
												<t:dataScroller id="scroller" for="personGrid" paginator="true"
													fastStep="1" paginatorMaxPages="#{pages$PersonList.paginatorMaxPages}" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 
												    pageIndexVar="pageNumber"
												    styleClass="SCH_SCROLLER"
												    paginatorActiveColumnStyle="font-weight:bold;"
												    paginatorRenderLinkForActive="false" 
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

													<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
															<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>
												</t:dataScroller>
												</CENTER>
																	</td></tr>
															</table>
													</t:div>
												</div>
											<table width="96%" border="0">
												<tr>
													<td class="BUTTON_TD JUG_BUTTON_TD_US" >

														<h:commandButton type="submit" styleClass="BUTTON" style="width: 120px" 
													         value="#{msg['commons.sendnotification']}" 
													         action="#{pages$PersonList.generateNotification}"/>
													</td>

												</tr>


											</table></div>
										
										
               </div>
										</h:form>

									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
					<td colspan="2">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
</div>
		
		</body>
	</html>
</f:view>