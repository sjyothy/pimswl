<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript"> 

function openEvaluationPopup()
{
			var screen_width = screen.width;
			var screen_height = screen.height;
			var popup_width = screen_width-120; 
			var popup_height = screen_height-380;
			var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
			
	        var popup = window.open('evaluationApplicationDetails.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
			popup.focus();	
}
		function showTeamMemberPopup()
		{
			var screen_width = screen.width;
			var screen_height = screen.height;
			var popup_width = screen_width-180;
			var popup_height = screen_height-240;
			var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
			
	        var popup = window.open('SearchMemberPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
			popup.focus();
		}
		 function setEvaluationById()
		{
			window.document.forms[0].submit();
		}	

		function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		
		function populateProperty(propertyId)
		{
			document.getElementById("detailsFrm:propertyId").value=propertyId;
			document.getElementById("detailsFrm:loadProperty").value='true';
		    document.forms[0].submit();
		}
		
		function submitForm()
		{
			window.document.forms[0].submit();
		}
	 function openNewAmountPopupJS()
		{
			  var screen_width = screen.width;
			 var screen_height = screen.height;
		      var popup_width = screen_width/3+250;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      var popup = window.open('evaluationUnitValuePopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		}
	function receiveNewRentAmount()
	{
		window.document.forms[0].submit();
	}
	function populateUnit(unitId,unitNum,usuageTypeId,unitRentAmount,unitUsuageType,propertyCommercialName,unitAddress,unitStatusId,unitStatus)
	{
		document.getElementById("detailsFrm:unitId").value=unitId;
		document.getElementById("detailsFrm:loadUnit").value='true';
		document.forms[0].submit();
	}
	
	function openLoadReportPopup(pageName) {
	   		var screen_width = screen.width;
			var screen_height = screen.height;
        	var popup_width = screen_width-250;
        	var popup_height = screen_height-500;
        	var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        	window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<c:choose>
					<c:when test="${!pages$evaluationApplicationDetails.popUp}">
                		<div class="containerDiv">
				   </c:when>
		     </c:choose>
				<%	
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
			%>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<c:choose>
					<c:when test="${!pages$evaluationApplicationDetails.popUp}">
                		<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
				   </c:when>
		    	 </c:choose>
					
					<tr width="100%">
					<c:choose>
					<c:when test="${!pages$evaluationApplicationDetails.popUp}">
                		<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
				   </c:when>
		    	 </c:choose>
						
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel
											value="#{pages$evaluationApplicationDetails.heading}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td  height="450px" valign="top" nowrap="nowrap">
										<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
										<div class="SCROLLABLE_SECTION" >
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 98%;">
												<div style="height: 60px">
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
														<h:messages></h:messages>
															<td>
																<h:outputText
																	value="#{pages$evaluationApplicationDetails.infoMessage}"
																	escape="false" styleClass="INFO_FONT" />
																<h:outputText
																	value="#{pages$evaluationApplicationDetails.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
															</td>
														</tr>
													</table>
												</div>
												<div>
													<h:inputHidden id="propertyId"
														value="#{pages$evaluationApplicationDetails.hiddenPropertyId}"></h:inputHidden>
													<h:inputHidden id="loadProperty"
														value="#{pages$evaluationApplicationDetails.hiddenLoadProperty}"></h:inputHidden>
													<h:inputHidden id="unitId"
														value="#{pages$evaluationApplicationDetails.hiddenUnitId}"></h:inputHidden>
													<h:inputHidden id="loadUnit"
														value="#{pages$evaluationApplicationDetails.hiddenLoadUnit}"></h:inputHidden>														
													<t:div styleClass="TAB_DETAIL_SECTION"
														style="width:99%; margin:13px;">
														<t:panelGrid id="evaluationActionGrid" cellpadding="1px"
															width="100%" cellspacing="5px"
															styleClass="TAB_DETAIL_SECTION_INNER" columns="4">
															<h:panelGroup 
															rendered="#{pages$evaluationApplicationDetails.pageModeHOS || 
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.statusHOS ||
																pages$evaluationApplicationDetails.pageModeInspector ||
																pages$evaluationApplicationDetails.statusExec ||
																pages$evaluationApplicationDetails.pageModeExec}">
															<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
																<h:outputLabel id="evaluationDateLabel"
																styleClass="LABEL" value="#{msg['evaluation.date']}: " />
															</h:panelGroup>
															
															<rich:calendar id="evaluationDate"
															
															rendered="#{pages$evaluationApplicationDetails.pageModeHOS || 
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.statusHOS ||
																pages$evaluationApplicationDetails.pageModeInspector ||
																pages$evaluationApplicationDetails.statusExec ||
																pages$evaluationApplicationDetails.pageModeExec}"
															
																value="#{pages$evaluationApplicationDetails.evaluationDate}"
																
																disabled="#{pages$evaluationApplicationDetails.pageModeHOS || 
																pages$evaluationApplicationDetails.HOSDone ||
																pages$evaluationApplicationDetails.pageModeView ||
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.statusHOS ||
																pages$evaluationApplicationDetails.pageModeExec||
																pages$evaluationApplicationDetails.execDone||
																pages$evaluationApplicationDetails.inspectorDone}"
																
																inputStyle="width: 190px; height: 18px"
																locale="#{pages$evaluationApplicationDetails.locale}"
																popup="true"
																datePattern="#{pages$evaluationApplicationDetails.dateFormat}"
																showApplyButton="false" enableManualInput="false"
																cellWidth="24px" cellHeight="22px" />
																
															<h:panelGroup
																rendered="#{pages$evaluationApplicationDetails.pageModeHOS || 
																pages$evaluationApplicationDetails.statusExec ||	
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.pageModeInspector ||
																pages$evaluationApplicationDetails.statusHOS ||
																pages$evaluationApplicationDetails.pageModeExec}">
															<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
																<h:outputLabel id="engineerLabel" styleClass="LABEL"
																value="#{msg['evaluation.engineer']}: " />
															</h:panelGroup>
															<h:inputText maxlength="20" id="txtEngineerName"
																readonly="#{pages$evaluationApplicationDetails.pageModeHOS || 
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.pageModeView ||
																pages$evaluationApplicationDetails.pageModeExec ||
																pages$evaluationApplicationDetails.execDone||
																pages$evaluationApplicationDetails.statusHOS ||
																pages$evaluationApplicationDetails.inspectorDone}"
																
																rendered="#{pages$evaluationApplicationDetails.pageModeHOS || 
																pages$evaluationApplicationDetails.statusHOS ||
																pages$evaluationApplicationDetails.statusExec ||
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.pageModeInspector ||
																pages$evaluationApplicationDetails.pageModeExec}"
																value="#{pages$evaluationApplicationDetails.evaluationEngr}"
																style="width:190px; height: 18px"></h:inputText>

															<h:panelGroup 
															rendered="#{!pages$evaluationApplicationDetails.hidePropertyRent&& 
															(pages$evaluationApplicationDetails.pageModeHOS || 
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.statusHOS ||
																pages$evaluationApplicationDetails.pageModeInspector ||
																pages$evaluationApplicationDetails.statusExec ||
																pages$evaluationApplicationDetails.pageModeExec)}">
															<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
																<h:outputLabel id="propertyValueLabel" styleClass="LABEL"
																value="#{msg['property.evaluationNewValue']}: " />
															</h:panelGroup>
															<h:inputText maxlength="20" id="txtPropertyValue"
																rendered="#{
																!pages$evaluationApplicationDetails.hidePropertyRent && 
																(pages$evaluationApplicationDetails.pageModeHOS || 
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.statusHOS ||
																pages$evaluationApplicationDetails.pageModeInspector ||
																pages$evaluationApplicationDetails.statusExec ||
																pages$evaluationApplicationDetails.pageModeExec)
																}"
																
																readonly="#{pages$evaluationApplicationDetails.pageModeHOS || 
																pages$evaluationApplicationDetails.HOSDone ||
																pages$evaluationApplicationDetails.pageModeView ||
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.statusHOS ||
																pages$evaluationApplicationDetails.pageModeExec||
																pages$evaluationApplicationDetails.execDone||
																pages$evaluationApplicationDetails.inspectorDone}"
																
																binding="#{pages$evaluationApplicationDetails.rentValueTxt}"
																value="#{pages$evaluationApplicationDetails.newPropertyValue}"
																style="width:190px; height: 18px;text-align:right;">
															</h:inputText>
															
															
															
															<%--  
															rendered="#{pages$evaluationApplicationDetails.pageModeHOS || 
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.statusExec ||
																pages$evaluationApplicationDetails.pageModeExec}"
															--%>
															<h:panelGroup
															rendered="false"																
															>
															<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
															<h:outputLabel id="recommendationOfHOS"
																styleClass="LABEL"
																value="#{msg['propertyEvaluation.hosRecommendation']}: " />
															</h:panelGroup>
															<%--
																rendered="#{pages$evaluationApplicationDetails.pageModeHOS || 
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.statusExec ||
																pages$evaluationApplicationDetails.pageModeExec}"
															--%>
															<t:inputTextarea styleClass="TEXTAREA" id="hosRecommendations"
																readonly="#{pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.pageModeView ||
																pages$evaluationApplicationDetails.HOSDone ||
																pages$evaluationApplicationDetails.pageModeExec ||
																pages$evaluationApplicationDetails.execDone}"
																rendered="false"
																value="#{pages$evaluationApplicationDetails.hosRecommendations}"
																rows="2" style="width: 190px; height: 26px;" />
															<%--
																rendered="#{pages$evaluationApplicationDetails.pageModeHOS ||
																pages$evaluationApplicationDetails.statusExec || 
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.pageModeExec}"
															--%>
															<h:panelGroup
															rendered = "false">
															<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
															<h:outputLabel id="recommendationOfRentCommittee"
																styleClass="LABEL"
																value="#{msg['propertyEvaluation.rentCommitteeRecommendation']}: " />
															</h:panelGroup>
															<%-- rendered="#{pages$evaluationApplicationDetails.pageModeHOS ||
																pages$evaluationApplicationDetails.statusExec || 
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.pageModeExec}"
																value="#{pages$evaluationApplicationDetails.rentCommRecommendations}"
															 --%>
															<t:inputTextarea styleClass="TEXTAREA" id="rentCommRecommendations"
															readonly="#{pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.pageModeView ||
																pages$evaluationApplicationDetails.HOSDone ||
																pages$evaluationApplicationDetails.pageModeExec ||
																pages$evaluationApplicationDetails.execDone}"
															rendered = "false"
															rows="2" style="width: 190px; height: 26px;" />
															<%-- 
																rendered="#{pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.pageModeExec}" 
															--%>
															<h:panelGroup 
															rendered="false" 
															>
															<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
															<h:outputLabel id="recommendationOfExective"
																styleClass="LABEL"
																value="#{msg['evaluationApplication.label.executiveRecommendations']}: " />
															</h:panelGroup>
															<%--
																rendered="#{pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.pageModeExec ||
																pages$evaluationApplicationDetails.execDone}"
															--%>
															<t:inputTextarea styleClass="TEXTAREA" id="executeRecommendation"
																rendered="false"
																readonly="#{pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.execDone}"
																value="#{pages$evaluationApplicationDetails.execRecommendations}"
																rows="2" style="width: 190px; height: 26px;" />
																
															<h:panelGroup 
															rendered="#{pages$evaluationApplicationDetails.pageModeInspector}">
															<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
															<h:outputLabel id="attachLabel" styleClass="LABEL"
																value="#{msg['commons.attachReport']}: " />
															</h:panelGroup>
															<t:inputFileUpload id="fileupload" 
															rendered="#{pages$evaluationApplicationDetails.pageModeInspector}"
																storage="file"
																binding="#{pages$evaluationApplicationDetails.fileUploadCtrl}"
																value="#{pages$evaluationApplicationDetails.selectedFile}"
																style="width:261px;height: 20px; margin-right: 5px;"></t:inputFileUpload>
																
															<h:panelGroup 
																rendered="#{pages$evaluationApplicationDetails.pageModeHOS || 
																pages$evaluationApplicationDetails.statusHOS ||
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.pageModeInspector ||
																pages$evaluationApplicationDetails.statusExec ||
																pages$evaluationApplicationDetails.pageModeExec}">
															<h:outputLabel styleClass="mandatory"
																	value="#{msg['commons.mandatory']}" />
															<h:outputLabel id="recommendationLabel"
																styleClass="LABEL"
																value="#{msg['evaluationApplication.label.inspectorRecomm']}: " />
															</h:panelGroup>	
															<t:panelGroup colspan="3">
															<t:inputTextarea
																readonly="#{pages$evaluationApplicationDetails.pageModeHOS || 
																pages$evaluationApplicationDetails.HOSDone ||
																pages$evaluationApplicationDetails.pageModeView ||
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.statusHOS ||
																pages$evaluationApplicationDetails.pageModeExec||
																pages$evaluationApplicationDetails.execDone||
																pages$evaluationApplicationDetails.inspectorDone}"
																rendered="#{pages$evaluationApplicationDetails.pageModeHOS || 
																pages$evaluationApplicationDetails.statusCompleted ||
																pages$evaluationApplicationDetails.statusHOS ||
																pages$evaluationApplicationDetails.pageModeInspector ||
																pages$evaluationApplicationDetails.statusExec ||
																pages$evaluationApplicationDetails.pageModeExec
																}"
																 styleClass="TEXTAREA" id="evaluationRecommendation"
																value="#{pages$evaluationApplicationDetails.evaluationRecomm}"
																rows="3" style="width: 550px; height:70px" />
															</t:panelGroup>		
																		
															<t:div style="height:10px;" />
														</t:panelGrid>
													</t:div>
													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%%">
															<tr>
															<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
															<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
															<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
															</tr>
												       </table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$evaluationApplicationDetails.tabPanel}"
																id="tabPanel" switchType="server"
																style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%">
																<rich:tab id="requestDetailsTab"
																	label="#{msg['requestDetailsTab.header']}">
																	<%@  include file="evaluationRequestTab.jsp"%>
																</rich:tab>
																<rich:tab id="evaluationRecommendationTab" label="#{msg['study.manage.tab.recommendations.heading']}"
																>
																	<%@  include file="evaluationRecommendationTab.jsp"%>
																</rich:tab>
																<rich:tab id="propertyDetailsTab" rendered="#{pages$evaluationApplicationDetails.showProperty}"
																	 label="#{msg['commons.propertyDetailsTabHeading']}">
																	<%@ include file="propertyEvaluationDetailTab.jsp"%>
																</rich:tab>
																<rich:tab id="unitDetailsTab"
																	rendered="#{!pages$evaluationApplicationDetails.showProperty}"
																	label="#{msg['unit.editUnit']}">
																	<%@  include file="unitEvaluationDetailTab.jsp"%>
																</rich:tab>
																<%--
																This condition was use before
																rendered="#{pages$evaluationApplicationDetails.showSiteVisitResults}" 
																--%>
																<rich:tab id="siteVisitResultsTab"
																	label="#{msg['siteVisit.results.tabHeader']}"
																	rendered="false">
																	<%@  include file="siteVisitResultsTab.jsp"%>
																</rich:tab>
																<rich:tab id="evaluationRequestDetailsTab"
																	rendered="#{!pages$evaluationApplicationDetails.popUp}"
																	label="#{msg['property.evaluationRequestHistory']}">
																	<%@  include file="evaluationRequestHistoryTab.jsp"%>
																</rich:tab>
																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>
																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>
																<%-- rendered="#{pages$evaluationApplicationDetails.showHistory}"--%>
																<rich:tab label="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$evaluationApplicationDetails.requestHistoryTabClick}"
																	>
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>
															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
															<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT" /></td>
															<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID" style="width:100%;" /></td>
															<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT" /></td>
															</tr>
														</table>
														<t:div styleClass="BUTTON_TD" style="padding-top:10px;">
															
															<t:commandButton styleClass="BUTTON" style="width:auto;"
																	value="#{msg['propertyEvaluation.label.saveSendRequestForEvaluation']}"
																	rendered="#{pages$evaluationApplicationDetails.pageModeNew &&
																				!pages$evaluationApplicationDetails.requestCreated}"
																	action="#{pages$evaluationApplicationDetails.saveAndSendForInspector}"
																	tabindex="13"/>
															<t:commandButton styleClass="BUTTON" style="width:auto;"
																	rendered="#{pages$evaluationApplicationDetails.pageModeHOS && !pages$evaluationApplicationDetails.HOSDone}"
																    title="#{msg['propertyEvaluation.label.sendForExecRecommendation']}"
																	value="#{msg['propertyEvaluation.label.sendForExecRecommendation']}"
																	action="#{pages$evaluationApplicationDetails.sendForExecutives}"
																	tabindex="13"
																	/>
															<t:commandButton styleClass="BUTTON" id="reject"
																	value="#{msg['propertyEvaluation.label.rejectRequest']}"
																	action="#{pages$evaluationApplicationDetails.rejectEvaluation}"
																	tabindex="14" 
																	rendered="false"
																	style="width:auto;" />	
															<t:commandButton styleClass="BUTTON" id="completeRequest"
																	value="#{msg['propertyEvaluation.label.completeRequest']}"
																	action="#{pages$evaluationApplicationDetails.completeRequest}"
																	tabindex="14"
																	rendered="false"
																	style="width:auto;" />			
															<t:commandButton styleClass="BUTTON" style="width:auto;"
																	rendered="#{pages$evaluationApplicationDetails.pageModeExec && !pages$evaluationApplicationDetails.execDone}"
																    title="#{msg['propertyEvaluation.label.saveExecRecomm']}"
																	value="#{msg['propertyEvaluation.label.saveExecRecomm']}"
																	action="#{pages$evaluationApplicationDetails.saveExecutiveRecommendations}"
																	tabindex="13"
																	 />
															<t:commandButton styleClass="BUTTON" style="width:auto;"
																value="#{msg['propertyEvaluation.label.sendForHOSRecomm']}"
																action="#{pages$evaluationApplicationDetails.sendForHOS}"
																rendered="#{pages$evaluationApplicationDetails.pageModeInspector && !pages$evaluationApplicationDetails.inspectorDone}" 
																/>
															<pims:security
																screen="Pims.PropertyManagement.EvaluationDetails.CancelEvaluation"
																action="create">
																<t:commandButton styleClass="BUTTON" id="cancel1"
																	value="Cancel Evaluation"
																	action="#{pages$evaluationApplicationDetails.cancelEvaluation}"
																	tabindex="14"
																	rendered="false"
																	style="width:auto;" />
															</pims:security>
															
															<t:commandButton styleClass="BUTTON" id="evaluationReport"
																	value="#{msg['report.evaluation.label.evaluation']}"
																	action="#{pages$evaluationApplicationDetails.viewPropertyUnitEvaluationReport}"
																	tabindex="15"
																	rendered="#{pages$evaluationApplicationDetails.propertyUnitEvaluationReportViewable}"
																	style="width:auto;" />
															
															<pims:security 
																screen="Pims.PropertyManagement.EvaluationDetails.SiteVisit"
																action="create">
																<t:commandButton styleClass="BUTTON"
																	value="#{msg['siteVisit.title']}"
																	action="#{pages$evaluationApplicationDetails.doSiteVisit}"
																	tabindex="16"
																	rendered="false" />
															</pims:security>
															<pims:security
																screen="Pims.PropertyManagement.EvaluationDetails.SaveSiteVisit"
																action="create">
																<t:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	tabindex="17"
																	rendered="false" />
															</pims:security>
															<t:commandButton styleClass="BUTTON"
																id="cancel2"
																value="#{msg['commons.cancel']}"
																onclick="if (!confirm('#{msg['auction.cancelConfirm']}')) return false"
																action="#{pages$evaluationApplicationDetails.cancel}"
																tabindex="12"
																rendered="#{!pages$evaluationApplicationDetails.popUp}">
															</t:commandButton>
																<t:commandButton styleClass="BUTTON"
																value="#{msg['bouncedChequesList.close']}"
																onclick="window.close();"
																rendered="#{pages$evaluationApplicationDetails.popUp}">
															</t:commandButton>

														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<c:choose>
						<c:when test="${!pages$evaluationApplicationDetails.popUp}">
							<tr style="height:10px;width:100%;#height:10px;#width:100%;">
								<td colspan="2">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
<%

%>										
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</c:when>
					</c:choose>
				</table>
			</div>
		</body>
	</html>
</f:view>

<%
	
	if( session.getAttribute("CLEAR_FIELD")!=null )
	{
		session.removeAttribute("CLEAR_FIELD");
%>
		<script type="text/javascript">
			if(document.getElementById('detailsFrm:txtPropertyValue')!=null)
				document.getElementById('detailsFrm:txtPropertyValue').value = '';
		</script>

<%
	}
%>