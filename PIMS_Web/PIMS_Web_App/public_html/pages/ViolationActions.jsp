<%-- 
  - Author: Afzal Zulfiqar
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Auction
  --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>		
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
		<title>PIMS</title>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>		
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
	 		
			<script type="text/javascript" src="../resources/<h:outputFormat value="#{path.js_prototype}"/>"></script>

			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			<script type="text/javascript">
				function onclickBlackList(){
				    
					var url = 'Reason.jsf?';
					url = url + 'unitId='+document.getElementById('violationActionForm:unitId').value;
					url =  url + '&' + 'inspectionId='+document.getElementById('violationActionForm:inspectionId').value;
					url =  url + '&' + 'violationId='+document.getElementById('violationActionForm:violationId').value;
					
				
					window.open(url,'_blank','width=420,height=210,left=300,top=150,scrollbars=no,status=yes');
					
				
				}
			</script>
						
	</head>

	<body>
		
			
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				

				<tr width="100%">
					
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['violationAction.actions']}" styleClass="HEADER_FONT"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" 
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="99%" valign="top" nowrap="nowrap">
								
								<div class="SCROLLABLE_SECTION" style="height:450px;">
									<h:form  id="violationActionForm">
									
									
										<h:messages id="aass222a">
															<h:outputText id="bb332323b" value="" escape="false" />
														</h:messages>
														
														<h:outputText id="ccc32323" escape="false"
															value="#{pages$ViolationActions.errorMessages}" />
															
															<div id="BlackListMsg23423">
															
															</div>
															
											<div id="divMarginMain1111" class="MARGIN">
									<table cellpadding="0" cellspacing="0" >
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>

										<div id="divDetailSectionId22222"  class="DETAIL_SECTION">
										<h:outputLabel id="olDetailSectionLabel111" value="#{msg['violationAction.actions']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td colspan="4">

															</td>
														</tr>
														<tr>
															<td colspan="1" width="25%">
																<h:outputLabel id="olUnitRefNo11" styleClass="LABEL"
																	value="#{msg['unit.unitNumber']}:"></h:outputLabel>
															</td>

															<td colspan="1" width="25%">

																<h:inputText id="unitReferenceNo" readonly="true"
																	value="#{pages$ViolationActions.unitRefNo}"
																	style="width: 85%; height: 18px"></h:inputText>

															</td>
															<td colspan="1" width="25%">
																<h:outputLabel id="olUnitType11" styleClass="LABEL"
																	value="#{msg['violation.category']}:"></h:outputLabel>
															</td>
															<td colspan="1" width="25%">
																<h:inputText id="otUnitType11" readonly="true"
																	value="#{pages$ViolationActions.violationCategory}"
																	style="width: 85%; height: 18px" maxlength="250"></h:inputText>
															</td>
														</tr>


														<tr>
															<td colspan="1">
																<h:outputLabel id="olPropertyName22" styleClass="LABEL"
																	value="#{msg['property.name']}:"></h:outputLabel>
															</td>
															<td colspan="1" width="10%">
																<h:inputText id="otPropertyName" readonly="true"
																	value="#{pages$ViolationActions.propertyName}"
																	style="width: 85%; height: 18px" maxlength="20"></h:inputText>
															</td>
															<td colspan="1">

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.replaceCheque.status']}:" />
															</td>
															<td colspan="1">

																<h:inputText id="otUnitStatus" readonly="true"
																	value="#{pages$ViolationActions.violationStatus}"
																	style="width: 85%; height: 18px"></h:inputText>
																</td>
														</tr>
														<tr>
															<td>
																<h:outputLabel id="olContractNumber" styleClass="LABEL"
																	value="#{msg['contract.contractNumber']}:" />
															</td>
															<td>
																<h:inputText id="contractNumber" readonly="true"
																	value="#{pages$ViolationActions.contractRefNo}"
																	style="width: 85%; height: 18px"></h:inputText>
															</td>
															<td>
															<h:outputLabel id="olViolationDate" styleClass="LABEL" value="#{msg['violation.Date']}:"/>
															</td>
															<td>
																<h:inputText id="violationDate" readonly="true" value="#{pages$ViolationActions.violationDate}"
															style="width: 85%; height: 18px" maxlength="20"></h:inputText>
															</td>

														</tr>
														<tr>
															<td><h:outputLabel styleClass="LABEL" value="#{msg['tenants.name']}:"></h:outputLabel></td>
															<td><h:inputText id="txttenantName" readonly="true" value="#{pages$ViolationActions.tenentName}" style="width: 85%; height: 18px"></h:inputText>
															
															</td>
															<td><h:outputLabel styleClass="LABEL" value="#{msg['violation.type']}:"/></td>
															<td><h:inputText id="violationStatus" readonly="true" value="#{pages$ViolationActions.violationType}"
															style="width: 85%; height: 18px" maxlength="20"></h:inputText></td>
														
														</tr>
																									<tr>
												<td>
													<h:outputLabel id="olActions" value="#{msg['commons.actions']}:"  styleClass="LABEL"/>
												</td>

												<td colspan="1">
													
													<h:selectManyCheckbox id="smChkActions" layout="pageDirection" value="#{pages$ViolationActions.selectedActions}" >
													
														<f:selectItems value="#{pages$ViolationActions.actionList}"/>

													</h:selectManyCheckbox>
													
												</td>
												<td></td>
												<td></td>
												
											</tr>
													</table>
												</div>
													
								</div>
									<h:inputHidden id="reasonHidden" value="#{pages$ViolationActions.reason}"/>
									<h:inputHidden id="unitRefHidden" value="#{pages$ViolationActions.unitRefNo}"/>
									<h:inputHidden id="propertyNameHidden" value="#{pages$ViolationActions.propertyName}"/>
									<h:inputHidden id="tenantHidden" value="#{pages$ViolationActions.tenentName}"/>
									<h:inputHidden id="damageAmountHidden" value="#{pages$ViolationActions.damageAmount}"/>
									<h:inputHidden id="contractRefHiddenHidden" value="#{pages$ViolationActions.contractRefNo}"/>
									<h:inputHidden id="unitTypeHiddenHidden" value="#{pages$ViolationActions.unitType}"/>
									<h:inputHidden id="unitStatusHiddenHidden" value="#{pages$ViolationActions.unitStatus}"/>
									<h:inputHidden id="unitIdHidden" value="#{pages$ViolationActions.unitId}"/>
									<h:inputHidden id="inspectionIdHidden" value="#{pages$ViolationActions.inspectionId}"/>
									<h:inputHidden id="violationIdHidden" value="#{pages$ViolationActions.violationId}"/>								
									<h:inputHidden id="violationDateHidden" value="#{pages$ViolationActions.violationDate}"/>
								
								<div class="A_RIGHT">
								<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}" action="#{pages$ViolationActions.cancel}"/>
								<h:commandButton id="blacklistBtn" value="#{msg['violationAction.blackList']}" styleClass="BUTTON" rendered="#{pages$ViolationActions.hasTenant}" disabled="#{pages$ViolationActions.tenantBlakListed}" onclick="onclickBlackList();return false;" />
															
								<h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" action="#{pages$ViolationActions.save}" /> 
								</div>				
								
									</h:form>
								</td>
							</tr>
									
						</table>
					</td>
				</tr>
			<tr>
    <td >
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
       <tr><td class="footer"><h:outputLabel value="#{msg['commons.footer.message']}" /></td></tr>
    </table>
        </td>
    </tr>
			</table>
		
	</body>
</html>
</f:view>