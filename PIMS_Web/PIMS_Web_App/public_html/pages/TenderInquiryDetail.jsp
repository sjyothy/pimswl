<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
</script>

<f:view>
	
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
	
	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
		<head>
		 <title>PIMS</title>
		 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
		 <meta http-equiv="pragma" content="no-cache">
		 <meta http-equiv="cache-control" content="no-cache">
		 <meta http-equiv="expires" content="0">
		 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
	 	 <script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>	
		</head>
	</html>
	
	<body class="BODY_STYLE">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			
			<!-- Header Row Start -->
			<tr>
				<td colspan="2">
					<jsp:include page="header.jsp" />
				</td>
			</tr>
			<!-- Header Row End -->
						
			<!-- Main Body Row Start -->
			<tr width="100%">
				<td class="divLeftMenuWithBody" width="17%">
					<jsp:include page="leftmenu.jsp" />
				</td>
				<td width="83%" valign="top" class="divBackgroundBody">
					<table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="HEADER_TD">
								<h:outputLabel value="Heading goes here" styleClass="HEADER_FONT"/>
							</td>
						</tr>
					</table>
					<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%">
						<tr valign="top">
							<td width="100%" height="100%" valign="top" nowrap="nowrap">
								<div class="SCROLLABLE_SECTION" style="height:475px;width:100%;#height:466px;">
									<h:form id="detailsFrm" enctype="multipart/form-data" style="WIDTH: 97.6%;">
										<div style="height:45px">
											<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
												<tr>
													<td>
														<h:outputText value="#{pages$TenderInquiryDetailBacking.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
														<h:outputText value="#{pages$TenderInquiryDetailBacking.infoMessages}"  escape="false" styleClass="INFO_FONT"/>
													</td>
												</tr>
											</table>
										</div>
										<div>
											<div class="AUC_DET_PAD">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"  /></td>
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
														<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT" /></td>
													</tr>
												</table>
												<div class="TAB_PANEL_INNER">
													<rich:tabPanel binding="#{pages$auctionDetails.tabPanel}" id="tabPanel" switchType="server" style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%">
														<rich:tab id="tabDetails" label="#{msg['auction.details']}">
														</rich:tab>
														<rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>														
													</rich:tabPanel>													
												</div>
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT" /></td>
														<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID" style="width:100%;" /></td>
														<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT" /></td>
													</tr>
												</table>
											</div>
										</div>
									</h:form>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- Main Body Row End -->
			
			<!-- Footer Row Start -->
			<tr>
				<td colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="footer">
								<h:outputLabel value="#{msg['commons.footer.message']}" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- Footer Row End -->
			
		</table>
	</body>
	
</f:view>
