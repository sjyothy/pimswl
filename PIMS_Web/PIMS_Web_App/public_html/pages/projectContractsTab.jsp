<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
	<script type="text/javascript">
	</script>
	<t:div style="width:100%;">
		<t:panelGrid columns="1" cellpadding="0" cellspacing="0">			
			<t:div styleClass="contentDiv" style="width:98%;">
				<t:dataTable id="dtProjectContracts" renderedIfEmpty="true" width="100%" cellpadding="0" cellspacing="0" var="dataItem" 
					binding="#{pages$projectContractsTab.dataTable}" value="#{pages$projectContractsTab.projectContractViewList}"
				 	preserveDataModel="false" preserveSort="false" rowClasses="row1,row2" rules="all" rows="9">
					
					<t:column sortable="true" width="200" id="contractNumberCol" defaultSorted="true">
						<f:facet name="header">
							<t:outputText value="#{msg['contract.contractNumber']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.contractNumber}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="contractTypeEnCol" rendered="#{pages$projectContractsTab.isEnglishLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['contract.contractType']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.contractTypeEn}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="contractTypeArCol" rendered="#{pages$projectContractsTab.isArabicLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['contract.contractType']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.contractTypeAr}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="contractDateCol">
						<f:facet name="header">
							<t:outputText value="#{msg['contract.contractDate']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.contractDate}" styleClass="A_LEFT" style="white-space: normal;">
							<f:convertDateTime pattern="#{pages$projectContractsTab.dateFormat}" timeZone="#{pages$projectContractsTab.timeZone}" />
						</t:outputText>
					</t:column>
					<t:column sortable="true" width="200" id="contractAmountCol">
						<f:facet name="header">
							<t:outputText value="#{msg['payment.amount']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.rentAmount}" styleClass="A_RIGHT_NUM" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="contractStatusEnCol" rendered="#{pages$projectContractsTab.isEnglishLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['contract.contractStatus']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.statusEn}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="contractStatusArCol" rendered="#{pages$projectContractsTab.isArabicLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['contract.contractStatus']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.statusAr}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
				<t:column id="actionCol" sortable="false" width="80"
					styleClass="ACTION_COLUMN">
					<f:facet name="header">
						<t:outputText value="#{msg['commons.action']}" />
					</f:facet>
					<t:commandLink action="#{pages$projectContractsTab.onView}">
						<h:graphicImage id="viewIcon" title="#{msg['commons.view']}"
							url="../resources/images/app_icons/Lease-contract.png" />&nbsp;
					</t:commandLink>
					<t:commandLink action="#{pages$projectContractsTab.onAddDesignPayment}"
					               rendered="#{pages$projectContractsTab.onRenderedPaymentLink}">
						<h:graphicImage id="addDesignAmountIcon"
						                title="#{msg['projectContractTab.AddDesignAmount']}"
							            url="../resources/images/app_icons/Add-Paid-Facility.png" />
					</t:commandLink>
					<t:commandLink action="#{pages$projectContractsTab.onAddSupervisionPayment}"
					               rendered="#{pages$projectContractsTab.onRenderedPaymentLink}">
						<h:graphicImage id="addSupervisionAmountIcon"
						                title="#{msg['projectContractTab.AddSupervisionAmount']}"
							            url="../resources/images/app_icons/Add-Paid-Facility.png" />
					</t:commandLink>
					<t:commandLink  action="#{pages$projectContractsTab.onAddContractAmount}"
					                rendered="#{!pages$projectContractsTab.onRenderedPaymentLink}">
						<h:graphicImage id="addTotalAmountIcon"
						                title="#{msg['projectContractTab.AddAmount']}"
							            url="../resources/images/app_icons/Add-Paid-Facility.png" />
					</t:commandLink>
					
					
				</t:column>
			</t:dataTable>
			</t:div>
			<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99.1%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']}"/>
						<h:outputText value=" : "/>
						<h:outputText value="#{pages$projectContractsTab.recordSize}" />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="dtProjectContractsScroller" for="dtProjectContracts" paginator="true"  
						fastStep="1" paginatorMaxPages="5" immediate="false"
						paginatorTableClass="paginator"
						renderFacetsIfSinglePage="true" 												
						paginatorTableStyle="grid_paginator" layout="singleTable" 
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" 
						pageIndexVar="projectContractPageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
							<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.projectContractPageNumber}"/>
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
		</t:div>
		</t:panelGrid>
	</t:div>