<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%-- Please remove above taglibs after completing your tab contents--%>
<t:div styleClass="contentDiv" style="width:95%;">
<t:dataTable id="collectionGrid" width="100%" styleClass="grid"
	value="#{pages$CollectionsTab.collectionDataList}" rows="10000"
	preserveDataModel="true" preserveSort="false" var="dataItem"
	rowClasses="row1,row2">




	<t:column id="requestNumber" width="10%" sortable="false"
		style="white-space: normal;">
		<f:facet name="header">
			<t:outputText value="#{msg['AuctionDepositRefundApproval.DataTable.RequestNumber']}" />
		</f:facet>
		<t:outputText value="#{dataItem.requestNumber}" />
	</t:column>
	
	<t:column id="status" width="10%" sortable="false"	 style="white-space: normal;">
		<f:facet name="header">
			<t:outputText value="#{msg['application.status.gridHeader']}" />
		</f:facet>
		<t:outputText value="#{dataItem.requestStatus}" />
	</t:column>
	
	<t:column id="createdBy" width="10%" sortable="false"
		style="white-space: normal;">
		<f:facet name="header">
			<t:outputText value="#{msg['socialProgram.lbl.CreatedBy']}" />
		</f:facet>
		<t:outputText value="#{dataItem.createdBy}" />
	</t:column>
	
	<t:column id="createdOn" width="10%" sortable="false"
		style="white-space: normal;">
		<f:facet name="header">
			<t:outputText value="#{msg['commons.createdOn']}" />
		</f:facet>
		<t:outputText value="#{dataItem.createdOn}" />
	</t:column>
	
	<t:column id="amount" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['commons.replaceCheque.oldChequeAmount']}" />
		</f:facet>
		<t:outputText value="#{dataItem.amount}" />
	</t:column>
	
	<t:column id="collectedAs" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['collectionProcedure.collectedAs']}" />
		</f:facet>
		<t:outputText value="#{dataItem.collectedAs}" />
	</t:column>
	<t:column id="desc" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['socialProgram.Budget.lbl.Desc']}" />
		</f:facet>
		<t:outputText value="#{dataItem.description}" />
	</t:column>
	



</t:dataTable>
</t:div>
<%--Column 3,4 Ends--%>
