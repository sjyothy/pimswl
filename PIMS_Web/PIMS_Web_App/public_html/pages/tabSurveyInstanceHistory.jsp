
<t:div id="tabSurveyInstanceHistory" style="width:100%;">

	<t:panelGrid id="tabSurveyInstanceHistoryGrid" cellpadding="3px"
		width="100%" cellspacing="2px" columns="1"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:outputLabel styleClass="LABEL" value="#{msg['commons.typeCol']}:"></h:outputLabel>
		<h:selectOneMenu id="surveyTypes"
			valueChangeListener="#{pages$tabSurveyInstanceHistory.onSurveyTypeChanged}"
			onchange="submit()"
			value="#{pages$tabSurveyInstanceHistory.selectOneSurveyType }"
			tabindex="1">
			<f:selectItem itemValue="-1"
				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
			<f:selectItems value="#{pages$tabSurveyInstanceHistory.surveyTypes}" />

		</h:selectOneMenu>

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">

		<h:commandButton id="btnAddSurveyInstance" styleClass="BUTTON"
			type="button" value="#{msg['commons.Add']}"
			onclick="javaScript:openSurveyForm('#{pages$tabSurveyInstanceHistory.personId}','#{pages$tabSurveyInstanceHistory.surveyTypeId}','-1');">


		</h:commandButton>

	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableSurveyInstanceHistory" rows="5" width="100%"
			value="#{pages$tabSurveyInstanceHistory.dataListSurveyInstances}"
			binding="#{pages$tabSurveyInstanceHistory.dataTableSurveyInstances}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="SurveyInstanceHistoryAspectsCreatedBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="SurveyInstanceHistoryAspectsCreatedByOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabSurveyInstanceHistory.englishLocale? 
																				dataItem.createdByEn:
																				dataItem.createdByAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="SurveyInstanceHistoryAspectsCreatedOn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabSurveyInstanceHistory.dateFormat}"
						timeZone="#{pages$tabSurveyInstanceHistory.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="actions">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.action']}" />
				</f:facet>
				<h:graphicImage title="#{msg['commons.view']}"
					onclick="javaScript:openSurveyForm('#{pages$tabSurveyInstanceHistory.personId}','#{dataItem.surveyTypeId}','#{dataItem.surveyInstanceId}');"
					url="../resources/images/app_icons/No-Objection-letter.png" />
			</t:column>

		</t:dataTable>
	</t:div>
	<t:div id="SurveyInstanceHistoryAspectsDivScroller"
		styleClass="contentDivFooter" style="width:99.1%">


		<t:panelGrid id="SurveyInstanceHistoryAspectsRecNumTable" columns="2"
			cellpadding="0" cellspacing="0" width="100%"
			columnClasses="RECORD_NUM_TD,BUTTON_TD">
			<t:div id="SurveyInstanceHistoryAspectsRecNumDiv"
				styleClass="RECORD_NUM_BG">
				<t:panelGrid id="SurveyInstanceHistoryAspectsRecNumTbl" columns="3"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
					cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
					<h:outputText value="#{msg['commons.recordsFound']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$tabSurveyInstanceHistory.recordSizeSurveyInstances}" />
				</t:panelGrid>

			</t:div>

			<CENTER>
				<t:dataScroller id="SurveyInstanceHistoryAspectsScroller"
					for="dataTableSurveyInstanceHistory" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$tabSurveyInstanceHistory.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="SurveyInstanceHistoryAspectslblFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="SurveyInstanceHistoryAspectslblFRRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="SurveyInstanceHistoryAspectslblFFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="SurveyInstanceHistoryAspectslblLRequestHistory"></t:graphicImage>
					</f:facet>

					<t:div id="SurveyInstanceHistoryAspectsPageNumDiv"
						styleClass="PAGE_NUM_BG">
						<t:panelGrid id="SurveyInstanceHistoryAspectsPageNumTable"
							styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
							cellspacing="0">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:panelGrid>

					</t:div>
				</t:dataScroller>
			</CENTER>

		</t:panelGrid>
	</t:div>
</t:div>

