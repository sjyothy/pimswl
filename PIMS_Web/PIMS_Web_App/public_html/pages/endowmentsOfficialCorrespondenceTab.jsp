<%-- Author: Kamran Ahmed--%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<t:div style="height:240px;overflow-y:scroll;width:100%;">
	<t:panelGrid border="0" width="97%" cellpadding="1" cellspacing="1"
		rendered="#{pages$endowmentFile.showRegistration}"
		id="offcialCorrespondencesTab">
		<t:panelGroup>
			<%--Column 1,2 Starts--%>
			<t:panelGrid columns="4" cellpadding="5px"
				columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">


				<h:outputLabel
					value="#{msg['officialCorrespondence.lbl.endowment']}" />
				<t:panelGroup colspan="3">
					<h:selectOneMenu styleClass="SELECT_MENU" style="width:260px;"
						value="#{pages$endowmentsOfficialCorrespondenceTab.seletedEndFileAssoId}">
						<f:selectItem itemValue="-1"
							itemLabel="#{msg['commons.combo.PleaseSelect']}" />
						<f:selectItems
							value="#{pages$endowmentsOfficialCorrespondenceTab.endowmentsList}" />
					</h:selectOneMenu>
				</t:panelGroup>
				<h:outputLabel value="#{msg['customer.govtDepartmant']}" />
				<h:selectOneMenu styleClass="SELECT_MENU"
					binding="#{pages$endowmentsOfficialCorrespondenceTab.cboGovtDepttId}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['commons.combo.PleaseSelect']}" />
					<f:selectItems value="#{pages$ApplicationBean.govtDepttList}" />
				</h:selectOneMenu>

				<h:outputLabel
					value="#{msg['mems.inheritanceFile.fieldLabel.bank']}" />
				<h:selectOneMenu
					binding="#{pages$endowmentsOfficialCorrespondenceTab.cboBankId}"
					styleClass="SELECT_MENU">
					<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
						itemValue="-1" />
					<f:selectItems value="#{pages$ApplicationBean.bankList}" />
				</h:selectOneMenu>

				<h:panelGroup>
					<h:outputLabel styleClass="mandatory"
						value="#{msg['commons.mandatory']}" />
					<h:outputLabel value="#{msg['commons.description']}"
						styleClass="LABEL" />
				</h:panelGroup>
				<t:panelGroup colspan="3">
					<h:inputTextarea style="width:570px"
						binding="#{pages$endowmentsOfficialCorrespondenceTab.txtArDescription}"></h:inputTextarea>
				</t:panelGroup>
				<h:outputLabel
					value="#{msg['mems.inheritanceFile.officialCoresTab.attacment']}" />
				<t:inputFileUpload id="fileupload" storage="file"
					binding="#{pages$endowmentsOfficialCorrespondenceTab.fileUploadCtrl}"
					value="#{pages$endowmentsOfficialCorrespondenceTab.selectedFile}"
					style="width:261px;height: 20px; margin-right: 5px;"></t:inputFileUpload>
				<t:div style="height:10px;" />
			</t:panelGrid>

		</t:panelGroup>
		<%--Column 3,4 Ends--%>
	</t:panelGrid>

	<t:div style="width:97%;">
		<t:div styleClass="A_RIGHT">
			<h:commandButton
				rendered="#{pages$endowmentFile.showRegistration}"
				value="#{msg['mems.inheritanceFile.officialCoresTab.save']}"
				action="#{pages$endowmentsOfficialCorrespondenceTab.saveCorrespondence}"
				styleClass="BUTTON" style="width:10%;"></h:commandButton>
		</t:div>
		<t:div style="height:5px;"></t:div>
		<t:div styleClass="contentDiv" style="width:98%">
			<t:dataTable id="offcialCorrespondencesGrid" rows="200"
				value="#{pages$endowmentsOfficialCorrespondenceTab.officialCorresList}"
				binding="#{pages$endowmentsOfficialCorrespondenceTab.officialCorresDataTable}"
				preserveSort="false" var="dataItem" rowClasses="row1,row2"
				rules="all" renderedIfEmpty="true" width="100%">

				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText />
					</f:facet>
					<h:selectBooleanCheckbox rendered="#{dataItem.opened}"
						value="#{dataItem.selected}"></h:selectBooleanCheckbox>
				</t:column>

				<t:column id="colendowmentNumber">
					<f:facet name="header">
						<h:outputText id="endowmentNumberf"
							value="#{msg['officialCorrespondence.lbl.endowment']}" />
					</f:facet>
					<h:outputText id="endowmentNumbere"
						value="#{dataItem.endowmentNumber}" />

				</t:column>

				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['mems.inheritanceFile.officialCoresTab.deptBank']}" />
					</f:facet>
					<h:outputText
						value="#{pages$endowmentsOfficialCorrespondenceTab.englishLocale?dataItem.govtDeptt.govtDepttEn:dataItem.govtDeptt.govtDepttAr}" />
					<h:outputText
						value="#{pages$endowmentsOfficialCorrespondenceTab.englishLocale?dataItem.bank.bankEn:dataItem.bank.bankAr}" />
				</t:column>

				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText value="#{msg['commons.description']}" />
					</f:facet>
					<h:outputText value="#{dataItem.description}" />
				</t:column>

				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['mems.inheritanceFile.officialCoresTab.status']}" />
					</f:facet>
					<h:outputText
						value="#{pages$endowmentsOfficialCorrespondenceTab.englishLocale?dataItem.statusEn:dataItem.statusAr}" />
				</t:column>

				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['mems.inheritanceFile.officialCoresTab.createdBy']}" />
					</f:facet>
					<h:outputText value="#{dataItem.createdByName}" />
				</t:column>
				<t:column id="actioCol" sortable="false">
					<f:facet name="header">
						<t:outputText
							value="#{msg['mems.inheritanceFile.columnLabel.action']}" />
					</f:facet>
					<t:commandLink id="lnkDownload" rendered="#{dataItem.downloadable}"
						action="#{pages$endowmentsOfficialCorrespondenceTab.downloadFile}">
						<h:graphicImage id="fileDownloadIcon" style="cursor:hand;"
							title="#{msg['attachment.buttons.download']}"
							url="../resources/images/app_icons/downloadAttachment.png" />
					</t:commandLink>

					<t:commandLink rendered="#{dataItem.opened}"
						onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToDeleteOfficialCorrespondence']}')) return false;"
						action="#{pages$endowmentsOfficialCorrespondenceTab.deleteOffCorrspndnc}">
						<h:graphicImage id="delete" title="#{msg['commons.delete']}"
							style="cursor:hand;" url="../resources/images/delete.gif" />&nbsp;
					</t:commandLink>


					<t:commandLink
						action="#{pages$endowmentsOfficialCorrespondenceTab.openFollowupPopup}">
						<h:graphicImage title="#{msg['memsFollowupTab.addFollowup']}"
							style="cursor:hand;"
							url="../resources/images/app_icons/application_form_edit.png" />&nbsp;
					</t:commandLink>


				</t:column>
			</t:dataTable>
		</t:div>
		<t:div style="height:10px;" />
		<t:div styleClass="A_RIGHT">
			<h:commandButton
				rendered="#{pages$endowmentFile.showRegistration}"
				value="#{msg['mems.inheritanceFile.officialCoresTab.close']}"
				onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToCloseCorres']}')) return false;"
				action="#{pages$endowmentsOfficialCorrespondenceTab.closeCorrespondence}"
				styleClass="BUTTON" style="width:auto;"></h:commandButton>
		</t:div>
	</t:div>
</t:div>