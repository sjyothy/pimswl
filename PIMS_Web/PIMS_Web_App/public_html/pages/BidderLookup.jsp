

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="javascript" type="text/javascript">
	function closeWindow()
	{
	  window.close();
	}
	function RowDoubleClick(bidderID, bidderFName, bidderLName, bidderSocialSecNo, bidderPassportNo)
	{
      window.opener.document.getElementById("auctionRequestForm:bidderID").value = bidderID;
	  window.opener.document.getElementById("auctionRequestForm:bidderNameInputText").value = bidderFName + ' ' + bidderLName;	  
	  window.opener.document.getElementById("auctionRequestForm:bidderName").value = bidderFName + ' ' + bidderLName;
	  window.opener.document.getElementById("auctionRequestForm:bidderSocialSec").value = bidderSocialSecNo;
	  window.opener.document.getElementById("auctionRequestForm:bidderPassport").value = bidderPassportNo;	  
      window.opener.document.getElementById("auctionRequestForm:auctionUnitsOfRequest").value = "";
      window.opener.document.forms[0].submit();
      closeWindow();
	}
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
	
  <html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
  <head>     
		<title>PIMS</title>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
								
    </head>

    
    <body>
    
    <!-- Header --> 
    <table width="100%" cellpadding="0" cellspacing="0"  border="0">
    <tr>
    <td colspan="2">        
    </td>
    </tr>

<tr width="100%">
    <!--td class="divLeftMenuWithBody" width="17%">
        < jsp:include page="leftmenu.jsp"/>
    </td-->
    <td width="100%" valign="top" class="divBackgroundBody" colspan="2">
        <table width="100%" class="greyPanelTable" cellpadding="0" cellspacing="0"  border="0">
	        <tr>
				<td class="HEADER_TD">
					<h:outputLabel value="#{msg['bidder.searchbidder']}" styleClass="HEADER_FONT"/>
				</td>    
	            <td width="100%">
			        &nbsp;
	            </td>
			</tr>
        </table>
        <table width="100%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" style="height:600px">
          <tr valign="top">
             <td height="100%" valign="top" nowrap="nowrap" background ="../../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
             </td>    
             <!--Use this below only for desiging forms Please do not touch other sections -->             
    
             <td width="100%" height="100%" valign="top" nowrap="nowrap">
             	<h:form id="conductAuctionForm">
             	<table width="100%">					
					<tr>
						<td colspan="6">
						<div class="MARGIN"> 
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
							<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
							<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
							<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
							</tr>
						</table>
						<div class="DETAIL_SECTION">
							<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
							<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" border="0">
								<tr>
									<td colspan="4">
										
									</td>
								</tr>
								<tr>
									<td>
										<h:outputLabel value="#{msg['tenants.socialSecNo']}"></h:outputLabel>
									</td>
									<td>
										<h:inputText id="socialSecNo" 
										binding="#{pages$BidderLookup.inputTextSocialSecNo}"></h:inputText>
									</td>
									<td>
										<h:outputLabel value="#{msg['bidder.bFirstName']}"></h:outputLabel>
									</td>
									<td>
										<h:inputText id="firstname"
										binding="#{pages$BidderLookup.inputTextFirstName}"></h:inputText>
									</td>
								</tr>
								<tr>
									<td>
										<h:outputLabel value="#{msg['commons.passport.number']}"></h:outputLabel>
									</td>
									<td>
										<h:inputText id="passport" 
										binding="#{pages$BidderLookup.inputTextPassport}"></h:inputText>
									</td>
									<td>
										<h:outputLabel value="#{msg['tenants.lastname']}"></h:outputLabel>
									</td>
									<td>
										<h:inputText id="lastname"
										binding="#{pages$BidderLookup.inputTextLastName}"></h:inputText>
									</td>								
								</tr>
								<tr>
									<td colspan="4">
									&nbsp;
									</td>
								</tr>
								<tr>								
									<td class="BUTTON_TD" colspan="4">
										<h:commandButton styleClass="BUTTON" value="#{msg['commons.search']}"
												action="#{pages$BidderLookup.search}"></h:commandButton>
		
										<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}"
												onclick="javascript:closeWindow();"></h:commandButton>
									</td>

								</tr>
							</table>
							</div></div>																											
						</td>													
					</tr>
					</table>
					
							<br><br>														
							<div class="MARGIN">
							<div class="imag">&nbsp;</div>
							<div class="contentDiv" style="width: 97%">
							<t:dataTable id="bidderGrid"
								value="#{pages$BidderLookup.bidderDataList}"													
								binding="#{pages$BidderLookup.dataTable}"													
								rows="10"
								preserveDataModel="false" preserveSort="false" var="dataItem"
								rules="all" renderedIfEmpty="true" width="100%">
									
								<t:column id="col2" sortable="true" >
									<f:facet name="header">
										<t:outputText value="#{msg['customer.firstname']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT" value="#{dataItem.firstName}"/>
								</t:column>
								
								<t:column id="col3" >
									<f:facet name="header">
										<t:outputText value="#{msg['customer.lastname']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT" value="#{dataItem.lastName}" />
								</t:column>
	
								<t:column id="col4" >
									<f:facet name="header">
										<t:outputText value="#{msg['commons.passport.number']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT" value="#{dataItem.passportNumber}" />
								</t:column>
								
								<t:column id="col5" >
									<f:facet name="header">
										<t:outputText value="#{msg['tenants.socialSecNo']}" />
									</f:facet>
									<t:outputText styleClass="A_LEFT" value="#{dataItem.socialSecNumber}" />
								</t:column>
	
								<t:column id="col6" width="50" >
									<f:facet name="header">
										<t:outputText value="#{msg['commons.select']}" />
									</f:facet>
										<t:commandLink
											action="#{pages$BidderLookup.theCall}">&nbsp;
											<h:graphicImage id="selectCmdLink" 
											title="#{msg['commons.select']}" 
											url="../resources/images/select-icon.gif"
											onmousedown="javascript:RowDoubleClick(#{dataItem.bidderId}, '#{dataItem.firstName}', '#{dataItem.lastName}', '#{dataItem.socialSecNumber}', '#{dataItem.passportNumber}');"/>
										</t:commandLink>																									
								</t:column>
							</t:dataTable>		
							</div>
											
							<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99.5%;" >
								<table cellpadding="0" cellspacing="0" width="100%">
									<tr>
									<td class="RECORD_NUM_TD">
										<div class="RECORD_NUM_BG">
											<table cellpadding="0" cellspacing="0">
											<tr><td class="RECORD_NUM_TD">
											<h:outputText value="#{msg['commons.recordsFound']}"/>
											</td><td class="RECORD_NUM_TD">
											<h:outputText value=" : "/>
											</td><td class="RECORD_NUM_TD">
											<h:outputText value="#{pages$BidderLookup.recordSize}"/>
											</td></tr>
											</table>
										</div>
									</td>
									<td class="BUTTON_TD" style="width:50%">
										<t:dataScroller id="scroller" for="bidderGrid" paginator="true"
											fastStep="1" paginatorMaxPages="5" immediate="false"
											styleClass="scroller" paginatorTableClass="paginator"
											renderFacetsIfSinglePage="true" 												
											paginatorTableStyle="grid_paginator" layout="singleTable" 
											paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
											paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
											pageIndexVar="pageNumber">
		
											<f:facet name="first">												
												<t:graphicImage url="../resources/images/first_btn.gif" id="lblF"></t:graphicImage>
											</f:facet>		
											<f:facet name="fastrewind">
												<t:graphicImage url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>
											</f:facet>		
											<f:facet name="fastforward">
												<t:graphicImage url="../resources/images/next_btn.gif" id="lblFF"></t:graphicImage>
											</f:facet>		
											<f:facet name="last">
												<t:graphicImage url="../resources/images/last_btn.gif" id="lblL"></t:graphicImage>
											</f:facet>
											<t:div styleClass="PAGE_NUM_BG">
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td>	
															<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
														</td>
														<td>	
															<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
														</td>
													</tr>					
												</table>										
											</t:div>		
										</t:dataScroller>											
									</td>
									</tr>
								</table>
							</t:div>							
						</div>
				
                </h:form>
                                   
         	</td>
         </tr>

        </table>
    
    
    
    
    
    </td>
    </tr>
    </table>
    </body>
</html>
</f:view>
