<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Assets
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	    function resetValues()
      	{
      	  
      		document.getElementById("searchFrm:txtFloorNo").value="";
      		document.getElementById("searchFrm:txtPropertyNumber").value="";
      		document.getElementById("searchFrm:txtEndowedName").value="";
      		document.getElementById("searchFrm:txtEndowedNumber").value="";
			document.getElementById("searchFrm:txtUnitNo").value="";
			document.getElementById("searchFrm:txtUnitCostCenter").value="";        	
			document.getElementById("searchFrm:txtBedRooms").value="";
			document.getElementById("searchFrm:txtDewaNumber").value="";
			document.getElementById("searchFrm:txtRentAmountFrom").value="";
			document.getElementById("searchFrm:txtRentAmountTo").value="";
			document.getElementById("searchFrm:txtPropertyName").value="";    
      	    document.getElementById("searchFrm:selectUnitUsage").selectedIndex=0;
      	    document.getElementById("searchFrm:selectUnitType").selectedIndex=0;
      	    document.getElementById("searchFrm:selectInvestmentType").selectedIndex=0;
      	    document.getElementById("searchFrm:selectUnitSide").selectedIndex=0;
      	    document.getElementById("searchFrm:selectPropertyOwnerShip").selectedIndex=0;
      	    document.getElementById("searchFrm:selectStatus").selectedIndex=0;
			document.getElementById("searchFrm:state").selectedIndex=0;
			document.getElementById("searchFrm:txtUnitDesc").value="";
        }        
		
		function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	function closeWindow()
		{
		  window.close();
		}
        
        	  function	openPopUp()
		{
		
		    var width = 300;
            var height = 300;
            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + ",menubar=yes,toolbar=yes,scrollbars=yes,resizable=yes,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

            var child1 = window.open("about:blank", "subWind", windowFeatures);

		      child1.focus();
		}
		
		
			function GenerateUnitsPopup()
		{
		      var screen_width = 1024;
		      var screen_height = 450;
		      var popup_width = screen_width-250;
		      var popup_height = screen_height;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('editUnitPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<c:choose>
				<c:when test="${!pages$SearchAssets.isViewModePopUp}">
					<div class="containerDiv">
				</c:when>
			</c:choose>


			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<c:choose>
					<c:when test="${!pages$SearchAssets.isViewModePopUp}">
						<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>
					</c:when>
				</c:choose>


				<tr width="100%">

					<c:choose>
						<c:when test="${!pages$SearchAssets.isViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>

					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['searchAssets.header']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="searchFrm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="font-size: 0px;">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="font-size: 0px;">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="font-size: 0px;">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width: 99.8%;">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px" border="0"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">

																	<tr>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchAssets.assetNumber']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:inputText id="txtPropertyNumber"
																				binding="#{pages$SearchAssets.htmlAssetNumber}"
																				tabindex="10">
																			</h:inputText>
																		</td>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchAssets.assetType']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:selectOneMenu id="assetTypes"
																				binding="#{pages$SearchAssets.assetTypeSelectMenu}"
																				tabindex="3">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$SearchAssets.assetTypesList}" />
																			</h:selectOneMenu>
																		</td>

																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchAssets.assetNameEn']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="txtEndowedName"
																				binding="#{pages$SearchAssets.htmlAssetNameNameEn}"
																				tabindex="10">
																			</h:inputText>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchAssets.assetNameAr']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="assetNameAr"
																				binding="#{pages$SearchAssets.htmlAssetNameAr}"
																				tabindex="10">
																			</h:inputText>
																		</td>

																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchAssets.assetDesc']}:"></h:outputLabel>

																		</td>
																		<td>
																			<h:inputText id="txtAssetsDesc"
																				binding="#{pages$SearchAssets.htmlAssetDesc}"
																				tabindex="10">
																			</h:inputText>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchAssets.filePersonName']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="txtFilePersonName"
																				binding="#{pages$SearchAssets.htmlFilePersonName}">
																			</h:inputText>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchAssets.fileNumber']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="htmlFileNumber"
																				binding="#{pages$SearchAssets.htmlFileNumber}"
																				tabindex="1">
																			</h:inputText>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchAssets.fileStatus']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="fileStatusList"
																				binding="#{pages$SearchAssets.fileStatusSelectMenu}"
																				tabindex="3">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$SearchAssets.fileStatusList}" />
																			</h:selectOneMenu>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['beneficiaryGuardianTab.BeneficiaryGuardian']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="beneficiary"
																				binding="#{pages$SearchAssets.htmlBeneficiary}"
																				tabindex="1">
																			</h:inputText>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchAssets.fileType']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="fileTypeList"
																				binding="#{pages$SearchAssets.fileTypeSelectMenu}"
																				tabindex="3">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$SearchAssets.fileTypeList}" />
																			</h:selectOneMenu>

																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchAssets.isFinal']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="cmbIsFinal"
																				binding="#{pages$SearchAssets.isFinalSelectMenu}"
																				tabindex="3">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItem itemLabel="#{msg['commons.no']}"
																					itemValue="0" />
																				<f:selectItem itemLabel="#{msg['commons.yes']}"
																					itemValue="1" />
																			</h:selectOneMenu>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['searchAssets.isConfirm']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="cmbIsConfirm"
																				binding="#{pages$SearchAssets.isConfirmSelectMenu}"
																				tabindex="3">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItem itemLabel="#{msg['commons.no']}"
																					itemValue="0" />
																				<f:selectItem itemLabel="#{msg['commons.yes']}"
																					itemValue="1" />
																			</h:selectOneMenu>

																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['inheritanceFile.inheritedAssetTab.label.isManagerAmaf']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="manager" tabindex="3"
																				binding="#{pages$SearchAssets.managerType}"
																				>
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItem
																					itemLabel="#{msg['commons.managerthirdParty']}"
																					itemValue="0" />
																				<f:selectItem
																					itemLabel="#{msg['commons.managerAmaf']}"
																					itemValue="1" />

																			</h:selectOneMenu>
																		</td>

																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['inheritanceFile.inheritedAssetTab.label.manager']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="managerName"
																				binding="#{pages$SearchAssets.htmlManageName}"
																				tabindex="1">
																			</h:inputText>
																		</td>
																	</tr>








																	<tr>
																		<td class="BUTTON_TD" colspan="4">



																			<h:commandButton styleClass="BUTTON" type="submit"
																				value="#{msg['commons.search']}"
																				action="#{pages$SearchAssets.doSearch}" />
																			<h:commandButton styleClass="BUTTON" type="button"
																				onclick="javascript:resetValues();"
																				value="#{msg['commons.clear']}" />
																			<h:commandButton styleClass="BUTTON" rendered="false"
																				value="#{msg['unit.addButton']}" />
																			<h:commandButton styleClass="BUTTON" type="button"
																				value="#{msg['commons.cancel']}"
																				onclick="javascript:window.close();">
																			</h:commandButton>
																		</td>

																	</tr>
																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>

													</table>
												</div>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px; width: 95%;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" style="width: 99%">
													<t:dataTable id="test2"
														value="#{pages$SearchAssets.dataList}"
														binding="#{pages$SearchAssets.dataTable}" width="100%"
														rows="#{pages$SearchAssets.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true">

														<t:column id="col1" width="10%" defaultSorted="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:commandSortHeader columnName="col1"
																	actionListener="#{pages$SearchAssets.sort}"
																	value="#{msg['searchAssets.assetNumber']}" arrow="true">
																	<f:attribute name="sortField" value="assetNumber" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.assetNumber}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column id="assetType" width="10%" sortable="false"
															style="white-space: normal;">

															<f:facet name="header">
																<t:commandSortHeader columnName="assetTypeId"
																	actionListener="#{pages$SearchAssets.sort}"
																	value="#{msg['searchAssets.assetType']}" arrow="true">
																	<f:attribute name="sortField" value="assetTypeId" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.assetType}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column id="assetNameEn" width="10%" sortable="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText value="#{msg['searchAssets.assetNameEn']}" />
															</f:facet>
															<t:outputText value="#{dataItem.assetNameEn}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column id="assetNameAr" width="10%" sortable="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText value="#{msg['searchAssets.assetNameAr']}" />
															</f:facet>
															<t:outputText value="#{dataItem.assetNameAr}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column id="managerNameGrid" width="10%" sortable="true"
															style="white-space: normal;">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['inheritanceFile.inheritedAssetTab.manager']}" />
															</f:facet>
															<t:outputText
																value="#{dataItem.amafManager?msg['inheritanceFile.inheritedAssetTab.managerAmaf']:dataItem.managerName}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column id="assetDesc" width="10%" sortable="false"
															style="white-space: normal;">

															<f:facet name="header">
																<t:commandSortHeader columnName="description"
																	actionListener="#{pages$SearchAssets.sort}"
																	value="#{msg['searchAssets.assetDesc']}" arrow="true">
																	<f:attribute name="sortField" value="description" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.assetDesc}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<%--<t:column id="fileNumber" width="10%" sortable="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:commandSortHeader columnName="fileNumber"
																	actionListener="#{pages$SearchAssets.sort}"
																	value="#{msg['searchAssets.fileNumber']}" arrow="true">
																	<f:attribute name="sortField" value="fileNumber" />
																</t:commandSortHeader>

															</f:facet>

															<t:outputText value="#{dataItem.fileNumber}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>--%>

														<t:column id="assetFinal" width="10%" sortable="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText value="#{msg['searchAssets.final']}" />
															</f:facet>
															<%-- value="#{!empty dataItem.finalCond ?(dataItem.finalCond=='0'? msg['commons.false']:msg['commons.true']): msg['commons.false'] }"--%>
															<t:outputText
																value="#{dataItem.finalCond=='0'? msg['commons.false']:msg['commons.true']}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>

														<t:column id="assetStatus" width="10%" sortable="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText value="#{msg['searchAssets.status']}" />
															</f:facet>
															<t:outputText value="#{dataItem.status}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="selectMany" width="10%" sortable="false"
															style="white-space: normal;"
															rendered="#{pages$SearchAssets.isPageModeSelectManyPopUp}">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.select']}" />
															</f:facet>
															<h:selectBooleanCheckbox id="select"
																value="#{dataItem.selected}" />
														</t:column>

														<t:column id="selectOne" sortable="false" width="10%"
															style="white-space: normal;"
															rendered="#{pages$SearchAssets.isPageModeSelectOnePopUp}">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.select']}" />
															</f:facet>
															<t:commandLink
																actionListener="#{pages$SearchAssets.sendAssetInfoToParent}">
																<h:graphicImage style="width: 16;height: 16;border: 0"
																	alt="#{msg['commons.select']}"
																	url="../resources/images/select-icon.gif"></h:graphicImage>
															</t:commandLink>
														</t:column>

														<t:column id="editCol" width="5%"
															style="white-space: normal;"
															rendered="#{!pages$SearchAssets.isPageModeSelectManyPopUp && !pages$SearchAssets.isPageModeSelectOnePopUp}"
															sortable="false">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>
															<t:commandLink action="#{pages$SearchAssets.edit}"
																rendered="#{!pages$SearchAssets.isPageModeSelectManyPopUp}">
																<h:graphicImage id="viewIcon"
																	title="#{msg['manageAsset.toolTip']}"
																	url="../resources/images/app_icons/View_Icon.png" />
															</t:commandLink>
															<t:commandLink
																action="#{pages$SearchAssets.openCollectionProc}"
																rendered="#{!pages$SearchAssets.isPageModeSelectManyPopUp && dataItem.showCollectionIcon}">
																<h:graphicImage id="collectionProcedureIcon"
																	title="Collection Procedure"
																	url="../resources/images/app_icons/Add-Paid-Facility.png" />
															</t:commandLink>
														</t:column>


														<%--<t:column id="editCol" width="5%" style="white-space: normal;"	sortable="false">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.edit']}" />
																</f:facet>
																<t:commandLink action="#{pages$UnitSearch.editUnit}"
																	>
																	<h:graphicImage id="editIcon"
																		title="#{msg['commons.edit']}"
																		url="../resources/images/edit-icon.gif" />
																</t:commandLink>
															</t:column> 
														--%>

													</t:dataTable>
												</div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:100%;#width:100%;">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$SearchAssets.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">

																<t:div styleClass="PAGE_NUM_BG" style="#width:20%">
																	<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																			</td>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{pages$SearchAssets.currentPage}" />
																			</td>
																		</tr>
																	</table>
																</t:div>
																<TABLE border="0" class="SCH_SCROLLER">
																	<tr>
																		<td>
																			<t:commandLink
																				action="#{pages$SearchAssets.pageFirst}"
																				disabled="#{pages$SearchAssets.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$SearchAssets.pagePrevious}"
																				disabled="#{pages$SearchAssets.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFR"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:dataList value="#{pages$SearchAssets.pages}"
																				var="page">
																				<h:commandLink value="#{page}"
																					actionListener="#{pages$SearchAssets.page}"
																					rendered="#{page != pages$SearchAssets.currentPage}" />
																				<h:outputText value="<b>#{page}</b>" escape="false"
																					rendered="#{page == pages$SearchAssets.currentPage}" />
																			</t:dataList>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$SearchAssets.pageNext}"
																				disabled="#{pages$SearchAssets.firstRow + pages$SearchAssets.rowsPerPage >= pages$SearchAssets.totalRows}">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$SearchAssets.pageLast}"
																				disabled="#{pages$SearchAssets.firstRow + pages$SearchAssets.rowsPerPage >= pages$SearchAssets.totalRows}">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblL"></t:graphicImage>
																			</t:commandLink>
																		</td>
																	</tr>
																</TABLE>
															</td>
														</tr>
													</table>
												</t:div>
											</div>
											<table width="96.6%" border="0">
												<tr>
													<td class="BUTTON_TD JUG_BUTTON_TD_US">

														<h:commandButton type="submit"
															rendered="#{pages$SearchAssets.isPageModeSelectManyPopUp}"
															styleClass="BUTTON"
															actionListener="#{pages$SearchAssets.sendManyAssetsInfoToParent}"
															value="#{msg['commons.select']}" />
													</td>

												</tr>


											</table>
										</div>


										</div>
									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
					<td colspan="2">
						<c:choose>
							<c:when test="${!pages$SearchAssets.isViewModePopUp}">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</tr>
								</table>
							</c:when>
						</c:choose>
					</td>
				</tr>
			</table>
			<c:choose>
				<c:when test="${!pages$SearchAssets.isViewModePopUp}">
					</div>
				</c:when>
			</c:choose>

		</body>
	</html>
</f:view>