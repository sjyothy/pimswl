<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
	<script type="text/javascript">		
	</script>
	<t:div style="width:100%;">
		<t:panelGrid columns="1" cellpadding="0" cellspacing="0">			
			<t:div styleClass="contentDiv" style="width:98%;">
				<t:dataTable id="dtProjectTenders" renderedIfEmpty="true" width="100%" cellpadding="0" cellspacing="0" var="dataItem" 
					binding="#{pages$projectTendersTab.dataTable}" value="#{pages$projectTendersTab.projectTendersViewList}"
				 	preserveDataModel="false" preserveSort="false" rowClasses="row1,row2" rules="all" rows="9">
					
					<t:column sortable="true" width="200" id="tendertNumberCol">
						<f:facet name="header">
							<t:outputText value="#{msg['tender.number.gridHeader']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.tenderNumber}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="tenderTypeEnCol" rendered="#{pages$projectTendersTab.isEnglishLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['tender.type.gridHeader']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.tenderTypeView.descriptionEn}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="tenderTypeArCol" rendered="#{pages$projectTendersTab.isArabicLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['tender.type.gridHeader']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.tenderTypeView.descriptionAr}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="tenderDescCol">
						<f:facet name="header">
							<t:outputText value="#{msg['commons.description']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.remarks}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="tenderDateCol">
						<f:facet name="header">
							<t:outputText value="#{msg['tender.date']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.issueDate}" styleClass="A_LEFT" style="white-space: normal;">
							<f:convertDateTime pattern="#{pages$projectTendersTab.dateFormat}" timeZone="#{pages$projectTendersTab.timeZone}" />
						</t:outputText>
					</t:column>
					<t:column sortable="true" width="200" id="sessionDateCol">
						<f:facet name="header">
							<t:outputText value="#{msg['tenderManagement.openTender.sessionDate']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.sessionDate}" styleClass="A_LEFT" style="white-space: normal;">
							<f:convertDateTime pattern="#{pages$projectTendersTab.dateFormat}" timeZone="#{pages$projectTendersTab.timeZone}" />
						</t:outputText>
					</t:column>
					<t:column sortable="true" width="200" id="tenderStatusEnCol" rendered="#{pages$projectTendersTab.isEnglishLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['commons.status']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.tenderTypeView.descriptionEn}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
					<t:column sortable="true" width="200" id="tenderStatusArCol" rendered="#{pages$projectTendersTab.isArabicLocale}">
						<f:facet name="header">
							<t:outputText value="#{msg['commons.status']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.tenderTypeView.descriptionAr}" styleClass="A_LEFT" style="white-space: normal;"/>
					</t:column>
				<t:column id="actionCol" sortable="false" width="15" rendered="false"
					styleClass="ACTION_COLUMN">
					<f:facet name="header">
						<t:outputText value="#{msg['commons.action']}" />
					</f:facet>

					<t:commandLink action="#{pages$projectTendersTab.onView}">
						<h:graphicImage id="viewIcon" title="#{msg['commons.view']}"
							url="../resources/images/app_icons/Lease-contract.png" />&nbsp;
					</t:commandLink>
				</t:column>

			</t:dataTable>
			</t:div>
			<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99.1%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']}"/>
						<h:outputText value=" : "/>
						<h:outputText value="#{pages$projectTendersTab.recordSize}" />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="dtProjectTendersScroller" for="dtProjectTenders" paginator="true"  
						fastStep="1" paginatorMaxPages="5" immediate="false"
						paginatorTableClass="paginator"
						renderFacetsIfSinglePage="true" 												
						paginatorTableStyle="grid_paginator" layout="singleTable" 
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" 
						pageIndexVar="projectTendersPageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
							<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.projectTendersPageNumber}"/>
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
		</t:div>
		</t:panelGrid>
	</t:div>