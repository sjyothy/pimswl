<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript">
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>

		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2"><jsp:include page="header.jsp" /></td>
					</tr>

					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['statsScreen.header']}"
											styleClass="HEADER_FONT" style="padding:10px" />
									</td>
									
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="5px">
									</td>



									<td width="100%" height="99%" valign="top" nowrap="nowrap">
										<div style="height: 100;">
											<div class="SCROLLABLE_SECTION"
												style="height: 445px; # height: 450px;">
												<h:form id="TaskList"
													style="width:99%;#width:96%; padding-left:5px;padding-right:5px;">
													<table border="0" class="layoutTable" style="width: 95%;">
														<tr>
															<td>
																<h:outputText
																	value="#{pages$StatsScreen.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
															</td>
														</tr>
													</table>

													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td colspan="3" style="padding: 10px;">
																<t:commandLink value="#{pages$StatsScreen.remaingTask}"
																	action=" #{pages$StatsScreen.sendToTaskList}"></t:commandLink>
															</td>
														</tr>
														<tr>



															<!-- First Column -->
															<td width="49%">


																<div class="DETAIL_SECTION">
																	<table cellpadding="0" cellspacing="0" border="0">
																		<tr>
																			<td>
																				<IMG
																					src="../resources/images/app_icons/Request-detail.png"
																					border="0" />
																			</td>
																			<td>
																				<h:outputLabel
																					value="#{msg['statsScreen.undistributedCheques']}"
																					styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>

																			</td>
																		</tr>
																	</table>
																	<div class="portlet_div" style="height: 150px;">
																		<table class="portlet_detail_section" cellspacing="0"
																			cellpadding="0" width="370">
																			<tr>
																				<td>

																					<t:dataTable
																						value="#{pages$StatsScreen.undistributedChequeStats}"
																						binding="#{pages$StatsScreen.dataTableUndistributedCheques}"
																						rowClasses="row1,row2" width="95%" rules="all"
																						renderedIfEmpty="true" sortable="true"
																						var="undistributeChq">
																						<t:column width="2%" sortable="false">
																							<f:facet name="header">
																								<t:outputText value="" />
																							</f:facet>
																							<t:outputText value="#{undistributeChq.rowNum}"
																								styleClass="A_LEFT" />
																						</t:column>

																						<t:column>
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['statsScreen.fileNum']}" />
																							</f:facet>

																							<t:commandLink
																								value="#{undistributeChq.fileNumber}"
																								action="#{pages$StatsScreen.onFileClicked}" />
																						</t:column>




																						<t:column width="3%" sortable="false">
																							<f:facet name="header">
																								<t:outputText value="#{msg['commons.total']}" />
																							</f:facet>
																							<t:outputText
																								value="#{undistributeChq.totalPayments}"
																								styleClass="A_RIGHT" />
																						</t:column>
																						<t:column>
																							<f:facet name="header">
																								<t:outputText value="#{msg['commons.amount']}" />
																							</f:facet>
																							<t:outputText value="#{undistributeChq.amount}"
																								styleClass="A_RIGHT" />
																						</t:column>
																					</t:dataTable>
																				</td>
																			</tr>
																		</table>
																	</div>
																</div>
																<table>
																	<tr height="3px">
																		<td></td>
																	</tr>
																</table>





																<div class="DETAIL_SECTION">
																	<table cellpadding="0" cellspacing="0" border="0">
																		<tr>
																			<td>
																				<IMG
																					src="../resources/images/app_icons/Request-detail.png"
																					border="0" />
																			</td>
																			<td>
																				<h:outputLabel
																					value="#{msg['statsScreen.rentContracts']}"
																					styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>

																			</td>
																		</tr>
																	</table>
																	<div class="portlet_div">
																		<table class="portlet_detail_section" cellspacing="0"
																			cellpadding="0" width="370">
																			<tr>
																				<td>

																					<t:dataTable
																						value="#{pages$StatsScreen.rentContractStats}"
																						rowClasses="row1,row2" width="100%" rules="all"
																						renderedIfEmpty="true" sortable="true"
																						var="rentContract">

																						<t:column>
																							<f:facet name="header">
																								<t:outputText value="#{msg['commons.typeCol']}" />
																							</f:facet>
																							<t:outputText
																								value="#{pages$StatsScreen.isArabicLocale ? rentContract.nameAR : rentContract.nameEN}"
																								styleClass="A_LEFT" />
																						</t:column>

																						<t:column>
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['statsScreen.status.active']}" />
																							</f:facet>
																							<t:outputText value="#{rentContract.active}"
																								styleClass="A_RIGHT" />
																						</t:column>

																						<t:column>
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['statsScreen.status.expired']}" />
																							</f:facet>
																							<t:outputText value="#{rentContract.expired}"
																								styleClass="A_RIGHT" />
																						</t:column>

																						<t:column>
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['statsScreen.status.renewApproved']}" />
																							</f:facet>
																							<t:outputText
																								value="#{rentContract.renewApproved}"
																								styleClass="A_RIGHT" />
																						</t:column>

																						<t:column>
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['statsScreen.status.renewDraft']}" />
																							</f:facet>
																							<t:outputText value=" #{rentContract.renewDraft}"
																								styleClass="A_RIGHT" />
																						</t:column>
																					</t:dataTable>
																				</td>
																			</tr>
																		</table>
																	</div>
																</div>
																<table>
																	<tr height="3px">
																		<td></td>
																	</tr>
																</table>
																<!-- Request Section Starts Here -->

																<div class="DETAIL_SECTION" width="100%">
																	<table cellpadding="0" cellspacing="0" border="0">
																		<tr>
																			<td>
																				<IMG
																					src="../resources/images/app_icons/Request-detail.png" />
																			</td>
																			<td>
																				<h:outputLabel
																					value="#{msg['statsScreen.requests']}"
																					styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
																			</td>
																		</tr>
																	</table>
																	<div class="portlet_div">
																		<table class="portlet_detail_section" cellspacing="0"
																			cellpadding="0" width="370">
																			<tr>
																				<td>
																					<t:dataTable
																						value="#{pages$StatsScreen.requestStats}"
																						rowClasses="row1,row2" rules="all"
																						renderedIfEmpty="true" sortable="true"
																						width="100%" var="request">

																						<t:column width="120px">
																							<f:facet name="header">
																								<t:outputText value="#{msg['commons.typeCol']}" />
																							</f:facet>
																							<t:outputText
																								value="#{pages$StatsScreen.isArabicLocale ? request.nameAR : request.nameEN}"
																								styleClass="A_LEFT" />
																						</t:column>


																						<t:column width="40px">
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['statsScreen.requestOrigination.internet']}"
																									style="align:center" />
																							</f:facet>
																							<t:outputText
																								value=" #{request.orginatedFromOther}"
																								styleClass="A_RIGHT" />
																						</t:column>

																						<t:column width="40px">
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['statsScreen.requestOrigination.csc']}" />
																							</f:facet>
																							<t:outputText
																								value=" #{request.orginatedFromCSC}"
																								styleClass="A_RIGHT" />
																						</t:column>

																					</t:dataTable>
																				</td>
																			</tr>
																		</table>
																	</div>
																</div>

																<!-- Request Section Ends here -->

															</td>
															<!-- end of first column -->
															<td width="1%">
																&nbsp;&nbsp;
															</td>
															<!-- Second Column -->
															<td valign="top" width="49%">
																<div class="DETAIL_SECTION">
																	<table cellpadding="0" cellspacing="0" border="0">
																		<tr>
																			<td>
																				<IMG
																					src="../resources/images/app_icons/Request-detail.png"
																					border="0" />
																			</td>
																			<td>
																				<h:outputLabel
																					value="#{msg['statsScreen.undistributedNonCheques']}"
																					styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>

																			</td>
																		</tr>
																	</table>
																	<div class="portlet_div" style="height: 150px;">
																		<table class="portlet_detail_section" cellspacing="0"
																			cellpadding="0" width="370">
																			<tr>
																				<td>

																					<t:dataTable
																						value="#{pages$StatsScreen.undistributedNonChequeStats}"
																						binding="#{pages$StatsScreen.dataTableUndistributedNonCheques}"
																						rowClasses="row1,row2" width="95%" rules="all"
																						renderedIfEmpty="true" sortable="true"
																						var="undistributeChq">
																						<t:column width="2%" sortable="false">
																							<f:facet name="header">
																								<t:outputText value="" />
																							</f:facet>
																							<t:outputText value="#{undistributeChq.rowNum}"
																								styleClass="A_LEFT" />
																						</t:column>

																						<t:column style="white-space: normal;width:18%;">
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['commons.requestNumber']}" />
																							</f:facet>

																							<t:commandLink style="white-space: normal;"
																								value="#{undistributeChq.requestNumber}"
																								action="#{pages$StatsScreen.onRequestClicked}" />
																						</t:column>
																						<t:column style="white-space: normal;width:3%;">
																							<f:facet name="header">
																								<t:outputText value="#{msg['commons.status']}" />
																							</f:facet>
																							<t:outputText style="white-space: normal;"
																								value="#{undistributeChq.requestStatusAr}"
																								styleClass="A_LEFT" />
																						</t:column>
																						<%-- 
																						<t:column style="white-space: normal;width:18%;">
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['commons.createdOn']}" />
																							</f:facet>
																							<t:outputText style="white-space: normal;"
																								value="#{undistributeChq.createdOn}"
																								styleClass="A_LEFT" />
																						</t:column>
																						--%>


																						<t:column style="white-space: normal;width:18%;">
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['statsScreen.fileNum']}" />
																							</f:facet>
																							<t:outputText style="white-space: normal;"
																								value="#{undistributeChq.fileNumber}"
																								styleClass="A_LEFT" />
																						</t:column>

																						<t:column width="3%" sortable="false">
																							<f:facet name="header">
																								<t:outputText value="#{msg['commons.total']}" />
																							</f:facet>
																							<t:outputText
																								value="#{undistributeChq.totalPayments}"
																								styleClass="A_RIGHT" />
																						</t:column>
																						<t:column style="white-space: normal;width:15%;">
																							<f:facet name="header">
																								<t:outputText value="#{msg['commons.amount']}" />
																							</f:facet>
																							<t:outputText value="#{undistributeChq.amount}"
																								style="white-space: normal;"
																								styleClass="A_RIGHT" />
																						</t:column>
																					</t:dataTable>
																				</td>
																			</tr>
																		</table>
																	</div>
																</div>
																<table>
																	<tr height="3px">
																		<td></td>
																	</tr>
																</table>


																<!-- Other contract Starts here -->

																<div class="DETAIL_SECTION" width="100%">
																	<table cellpadding="0" cellspacing="0" border="0">
																		<tr>
																			<td>
																				<IMG
																					src="../resources/images/app_icons/Request-detail.png" />
																			</td>
																			<td>
																				<h:outputLabel
																					value="#{msg['statsScreen.otherContracts']}"
																					styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
																			</td>
																		</tr>
																	</table>


																	<div class="portlet_div">
																		<table class="portlet_detail_section" cellspacing="0"
																			cellpadding="0" width="370">
																			<tr>
																				<td>
																					<t:dataTable
																						value="#{pages$StatsScreen.otherContractStats}"
																						rowClasses="row1,row2" width="100%" rules="all"
																						sortable="true" renderedIfEmpty="true"
																						var="otherContract">

																						<t:column width="90px">
																							<f:facet name="header">
																								<t:outputText value="#{msg['commons.typeCol']}" />
																							</f:facet>
																							<t:outputText
																								value="#{pages$StatsScreen.isArabicLocale ? otherContract.nameAR : otherContract.nameEN}"
																								styleClass="A_LEFT" />
																						</t:column>

																						<t:column width="15px">
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['statsScreen.status.active']}" />
																							</f:facet>
																							<t:outputText value=" #{otherContract.active}"
																								styleClass="A_RIGHT" />
																						</t:column>

																						<t:column width="15px">
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['statsScreen.status.expired']}"
																									style="text-align:center" />
																							</f:facet>
																							<t:outputText value=" #{otherContract.expired}"
																								styleClass="A_RIGHT" />
																						</t:column>

																						<t:column width="45px">
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['statsScreen.status.renewApproved']}" />
																							</f:facet>
																							<t:outputText
																								value=" #{otherContract.renewApproved}"
																								styleClass="A_RIGHT" />
																						</t:column>

																						<t:column width="55px">
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['statsScreen.status.renewDraft']}" />
																							</f:facet>
																							<t:outputText
																								value=" #{otherContract.renewDraft}"
																								styleClass="A_RIGHT" />
																						</t:column>

																					</t:dataTable>

																				</td>
																			</tr>
																		</table>
																	</div>
																</div>
																<!--  Other Contract ends here -->
																<table>
																	<tr height="3px">
																		<td></td>
																	</tr>
																</table>
														<!--  Undistributed Profit Start Here -->
															<div class="DETAIL_SECTION">
																	<table cellpadding="0" cellspacing="0" border="0">
																		<tr>
																			<td>
																				<IMG
																					src="../resources/images/app_icons/Request-detail.png"
																					border="0" />
																			</td>
																			<td>
																				<h:outputLabel
																					value="#{msg['statsScreen.lbl.undistributeProfits']}"
																					styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>

																			</td>
																		</tr>
																	</table>
																	<div class="portlet_div" style="height: 150px;">
																		<table class="portlet_detail_section" cellspacing="0"
																			cellpadding="0" width="370">
																			<tr>
																				<td>

																					<t:dataTable
																						value="#{pages$StatsScreen.undistributedProfitStats}"
																						binding="#{pages$StatsScreen.dataTableUndistributedProfits}"
																						rowClasses="row1,row2" width="95%" rules="all"
																						renderedIfEmpty="true" sortable="true"
																						var="undistributeChq">
																						<t:column width="2%" sortable="false">
																							<f:facet name="header">
																								<t:outputText value="" />
																							</f:facet>
																							<t:outputText value="#{undistributeChq.rowNum}"
																								styleClass="A_LEFT" />
																						</t:column>

																						<t:column>
																							<f:facet name="header">
																								<t:outputText
																									value="#{msg['statsScreen.fileNum']}" />
																							</f:facet>

																							<t:commandLink
																								value="#{undistributeChq.fileNumber}"
																								action="#{pages$StatsScreen.onProfitClicked}" />
																						</t:column>




																						<t:column width="3%" sortable="false">
																							<f:facet name="header">
																								<t:outputText value="#{msg['commons.description']}" />
																							</f:facet>
																							<t:outputText
																								value="#{undistributeChq.description}"
																								styleClass="A_RIGHT" />
																						</t:column>
																						<t:column>
																							<f:facet name="header">
																								<t:outputText value="#{msg['commons.amount']}" />
																							</f:facet>
																							<t:outputText value="#{undistributeChq.amount}"
																								styleClass="A_RIGHT" />
																						</t:column>
																					</t:dataTable>
																				</td>
																			</tr>
																		</table>
																	</div>
																</div>
																<!--  Undistributed Profit Start Here -->
																<table>
																	<tr height="3px">
																		<td></td>
																	</tr>
																</table>
															</td>
															<!-- End of Second Column -->
															
														</tr>
														<tr>
															<td colspan="6" height="8px"></td>
														</tr>

													</table>
												</h:form>
											</div>
										</div>
									</td>
								</tr>
							</table>

						</td>


					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>
