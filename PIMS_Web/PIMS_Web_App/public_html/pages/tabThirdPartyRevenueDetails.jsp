
<t:div id="tabDDetails" style="width:100%;">
	<t:panelGrid id="ddTab" cellpadding="5px" width="100%"
		rendered="#{pages$thirdPartyRevenue.showSaveButton || pages$thirdPartyRevenue.showResubmitButton}"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblBenef" styleClass="LABEL"
				value="#{msg['revenueFollowup.lbl.endowment']}" />
		</h:panelGroup>
		<h:panelGroup>

			<h:inputText id="endowmentName" maxlength="20" readonly="true"
				styleClass="READONLY"
				value="#{pages$thirdPartyRevenue.paymentView.endowmentView.endowmentName}" />

			<h:graphicImage id="endowmentProgramsSearchpopup"
				style="margin-right:5px;" title="#{msg['endowment.search.title']}"
				url="../resources/images/magnifier.gif"
				onclick="javaScript:openSearchEndowmentsPopup('thirdPartyRevenueReq');" />

			<h:graphicImage id="endowmentDetailspopup"
				rendered="#{
			    			!empty pages$thirdPartyRevenue.paymentView.endowmentView && 
			    			!empty pages$thirdPartyRevenue.paymentView.endowmentView.endowmentId
			    		   }"
				style="margin-right:5px;" title="#{msg['commons.details ']}"
				url="../resources/images/app_icons/No-Objection-letter.png"
				onclick="javaScript:openEndowmentManagePopup('#{pages$thirdPartyRevenue.paymentView.endowmentView.endowmentId}');" />


		</h:panelGroup>


		<h:outputLabel id="manager" styleClass="LABEL"
			value="#{msg['contract.person.Manager']}:"></h:outputLabel>

		<h:inputText id="txtManager" readonly="true" styleClass="READONLY"
			value="#{
					 !empty pages$thirdPartyRevenue.paymentView.endowmentView && 
					 !empty pages$thirdPartyRevenue.paymentView.endowmentView.manager ? pages$thirdPartyRevenue.paymentView.endowmentView.manager.personFullName:
					 																	'' 
					}" />

		<h:outputLabel styleClass="LABEL"
			value="#{msg['revenueFollowup.lbl.units']}:" />
		<h:selectOneMenu id="cmbUnits"
			disabled="#{!pages$thirdPartyRevenue.showSaveButton}"
			value="#{pages$thirdPartyRevenue.paymentView.thirdPartyPropUnitId}">
			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems value="#{pages$thirdPartyRevenue.thirdPartyUnits}" />
		</h:selectOneMenu>


		<h:outputLabel styleClass="LABEL"
			value="#{msg['thirdPartRevenue.lbl.For']}:" />
		<h:selectOneMenu id="cmbFor"
			disabled="#{!pages$thirdPartyRevenue.showSaveButton && 
			    		!pages$thirdPartyRevenue.showResubmitButton 
			           }"
			value="#{pages$thirdPartyRevenue.paymentView.forRebuild}">
			
			<f:selectItems		value="#{pages$thirdPartyRevenue.selectThirdPartyPaymentForTypes}" />/>

		</h:selectOneMenu>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="payModeField" styleClass="LABEL"
				value="#{msg['cancelContract.payment.paymentmethod']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="selectPaymentMethod"
			value="#{pages$thirdPartyRevenue.paymentView.paymentModeIdString}">

			<f:selectItems value="#{pages$ApplicationBean.paymentMethodList}" />
		</h:selectOneMenu>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblpaymentDueDate" styleClass="LABEL"
				value="#{msg['addMissingMigratedPayments.fieldName.paymentDueDate']}:"></h:outputLabel>
		</h:panelGroup>
		<rich:calendar id="paymentDueDatecal"
			value="#{pages$thirdPartyRevenue.paymentView.paymentDueOn}"
			popup="true" datePattern="dd/MM/yyyy" showApplyButton="false"
			locale="#{pages$donationRequest.locale}" enableManualInput="false"
			inputStyle="width: 170px; height: 14px" />



		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="amnt" styleClass="LABEL"
				value="#{msg['commons.amount']}:"></h:outputLabel>
		</h:panelGroup>
		<h:inputText id="txtAmount"
			readonly="#{!pages$thirdPartyRevenue.showSaveButton && 
			    		!pages$thirdPartyRevenue.showResubmitButton 
			           }"
			value="#{pages$thirdPartyRevenue.paymentView.amountStr }"
			onkeyup="removeNonNumeric(this)" onkeypress="removeNonNumeric(this)"
			onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
			onchange="removeNonNumeric(this)" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;">

		<h:commandButton styleClass="BUTTON"
			rendered="#{pages$thirdPartyRevenue.showSaveButton ||  
										 pages$thirdPartyRevenue.showResubmitButton 
					   				    }"
			value="#{msg['commons.Add']}" onclick="performTabClick('lnkAdd');">
			<h:commandLink id="lnkAdd" action="#{pages$thirdPartyRevenue.onAdd}" />
		</h:commandButton>


	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTablePayments" rows="15" width="100%"
			value="#{pages$thirdPartyRevenue.paymentList}"
			binding="#{pages$thirdPartyRevenue.dataTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="payNumber" sortable="true">
				<f:facet name="header">
					<t:outputText id="payNum"
						value="#{msg['chequeList.paymentNumber']}" />
				</f:facet>
				<t:outputText id="payNoField" styleClass="A_LEFT"
					value="#{dataItem.paymentNumber}" />
			</t:column>
			<t:column id="endProgram" sortable="true">
				<f:facet name="header">
					<t:outputText id="endProg"
						value="#{msg['revenueFollowup.lbl.endowment']}" />
				</f:facet>
				<t:commandLink
					onclick="javaScript:openEndowmentManagePopup('#{dataItem.endowmentView.endowmentId}');"
					value="#{dataItem.endowmentView.endowmentName}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>

			<t:column id="unit" sortable="true">
				<f:facet name="header">
					<t:outputText id="unitHeader"
						value="#{msg['revenueFollowup.lbl.units']}" />
				</f:facet>
				<t:outputText id="unitRow" styleClass="A_LEFT"
					value="#{dataItem.thirdPartyPropUnitNumber}" />
			</t:column>

			<t:column id="forRebuildcol" sortable="true">
				<f:facet name="header">
					<t:outputText id="forRebuildHeader"
						value="#{msg['thirdPartRevenue.lbl.For']}" />
				</f:facet>
				<t:outputText id="forRebuildRow" styleClass="A_LEFT"
					value="#{pages$thirdPartyRevenue.englishLocale?dataItem.forRebuildEn:dataItem.forRebuildAr}" />
			</t:column>

			<t:column id="payMode" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdMethod"
						value="#{msg['cancelContract.payment.paymentmethod']}" />
				</f:facet>
				<t:outputText id="paymentMode" styleClass="A_LEFT"
					value="#{dataItem.paymentModeAr}" />
			</t:column>
			<t:column id="paymentDueDatettcol" sortable="true">
				<f:facet name="header">
					<t:outputText id="paymentDueDateCOloutput"
						value="#{msg['addMissingMigratedPayments.fieldName.paymentDueDate']}" />
				</f:facet>
				<t:outputText id="GpayDate" styleClass="A_LEFT"
					value="#{dataItem.paymentDueOn}" style="white-space: normal;">
					<f:convertDateTime pattern="#{pages$thirdPartyRevenue.dateFormat}"
						timeZone="#{pages$thirdPartyRevenue.timeZone}" />
				</t:outputText>
			</t:column>

			<t:column id="amntCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAmnt"
						value="#{msg['mems.payment.request.label.Amount']}" />
				</f:facet>
				<t:outputText id="amountt" styleClass="A_LEFT"
					value="#{dataItem.amountStr}" />
			</t:column>
			<t:column id="statusCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdStatus" value="#{msg['commons.status']}" />
				</f:facet>
				<t:outputText id="psStatus" styleClass="A_LEFT"
					value="#{pages$thirdPartyRevenue.englishLocale?dataItem.statusEn: dataItem.statusAr}" />
			</t:column>
			<t:column id="distributedByAr" sortable="true">
				<f:facet name="header">
					<t:outputText id="distributedBy"
						value="#{msg['donationBox.lbl.distributedBy']}" />
				</f:facet>
				<t:outputText id="distributedByOT" styleClass="A_LEFT"
					value="#{dataItem.distributedByAr}" />
			</t:column>
			<t:column id="distributedOn" sortable="true">
				<f:facet name="header">
					<t:outputText id="distributedOnF"
						value="#{msg['donationBox.lbl.distributedOn']}" />
				</f:facet>
				<t:outputText id="distributedOnOT" styleClass="A_LEFT"
					value="#{dataItem.distributedOn}" />
			</t:column>

			<t:column id="action" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAction" value="#{msg['commons.action']}" />
				</f:facet>
				<pims:security
					screen="Pims.EndowMgmt.ThirdPartyRevenue.DistributeAndComplete"
					action="view">

					<h:commandLink id="lnkDistribute"
						rendered="#{
									pages$thirdPartyRevenue.showComplete && 
									dataItem.isDistributed!='1' && 
									dataItem.statusId==36006 &&
									dataItem.forRebuild=='2'	
								   }"
						action="#{pages$thirdPartyRevenue.onDistribute}">
						<h:graphicImage id="distributeIcon" style="cursor:hand;"
							title="#{msg['commons.distributeAmongBeneficiaries']}"
							url="../resources/images/app_icons/Add-New-Tenant.png"
							width="18px;" />
					</h:commandLink>
				</pims:security>
				<h:commandLink id="lnkViewDistribution"
					rendered="#{
								 dataItem.isDistributed == '1' && 
								 dataItem.forRebuild=='2'
							   }"
					action="#{pages$thirdPartyRevenue.onDistribute}">
					<h:graphicImage id="viewDistributionIcon" style="cursor:hand;"
						title="#{msg['distributeEndRevenue.tooltip.viewDistribution']}"
						url="../resources/images/app_icons/view_tender.png" width="18px;" />
				</h:commandLink>
				<h:commandLink id="lnkDelete"
					onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableInputs();"
					rendered="#{pages$thirdPartyRevenue.showSaveButton || pages$thirdPartyRevenue.showResubmitButton}"
					action="#{pages$thirdPartyRevenue.onDelete}">
					<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
						style="cursor:hand;" url="../resources/images/delete.gif"
						width="18px;" />
				</h:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>
</t:div>


