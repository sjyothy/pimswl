<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="javascript" type="text/javascript">
    function onAfterChangeNewRentAmount()
    {
          
      document.getElementById('transferContractForm:disablingDiv').style.display='none';
    }
	function onBeforeChangeNewRentAmount( field )
	{
	    document.getElementById('transferContractForm:disablingDiv').style.display='block';
		document.getElementById('transferContractForm:lnkNewRentAmountChange').onclick();
	}
	
	function showPaymentSchedulePopUp()
	{
	 var screen_width = screen.width;
	   var screen_height = screen.height;

	   window.open('PaymentSchedule.jsf?totalContractValue='+document.getElementById("transferContractForm:newRentAmount").value,'_blank','width='+(screen_width-600)+',height='+(screen_height-450)+',left=300,top=250,scrollbars=yes,status=yes');
	   
	}

	    function   showPersonPopup()
		{
		   var screen_width = 1024;
		   var screen_height = 450;
		   window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup','_blank','width='+(screen_width)+',height='+(screen_height)+',left=0,top=40,scrollbars=yes,status=yes');
		    
		}
		function   showTransferPersonPopup()
		{
		   var screen_width = 1024;
		   var screen_height = 450;
		   window.open('SearchPerson.jsf?persontype=TENANT&viewMode=popup','_blank','width='+(screen_width)+',height='+(screen_height)+',left=0,top=40,scrollbars=yes,status=yes');
		    
		}
					
        function   showContractPopup(clStatus)
		{
		   var screen_width = 1024;
		   var screen_height = 450;
		   window.open('ContractListPopUp.jsf?clStatus='+clStatus,'_blank','width='+(screen_width)+',height='+(screen_height)+',left=0,top=40,scrollbars=yes,status=yes');
		}    

		function showPopup()
		{
		   var screen_width = 1024;
		   var screen_height = 450;
		   window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width)+',height='+(screen_height)+',left=0,top=40,scrollbars=no,status=yes');
		}
		
		function addBasicInfo()
		{
		    var screen_width = 1024;
		    var screen_height = 450;
		    window.open('AddPerson.jsf?viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
		}

	    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
		{
		    
		    if(hdnPersonType=='APPLICANT'){
		    	document.getElementById("transferContractForm:hdnPersonId").value=personId;
			    document.getElementById("transferContractForm:hdnCellNo").value=cellNumber;
			    document.getElementById("transferContractForm:hdnPersonName").value=personName;
			    document.getElementById("transferContractForm:hdnPersonType").value=hdnPersonType;
			    document.getElementById("transferContractForm:hdnIsCompany").value=isCompany;
			    document.forms[0].submit();
		    }
		     if(hdnPersonType=='TENANT'){

		    	document.getElementById("transferContractForm:hdnTDPersonId").value=personId;
			    document.getElementById("transferContractForm:hdnTDCellNo").value=cellNumber;
			    document.getElementById("transferContractForm:hdnTDPersonName").value=personName;
			    document.getElementById("transferContractForm:hdnTDPersonType").value=hdnPersonType;
			    document.getElementById("transferContractForm:hdnTDIsCompany").value=isCompany;
			    document.getElementById("transferContractForm:hdnNewTenantLnk").onclick();
		    }
	     
		}
		
		 function  loadUpdatedFees()
	      {
	         document.getElementById('transferContractForm:lnkLoadNewPayment').onclick();
	      }
</script>


<f:view>

	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

			<style>
.rich-calendar-input {
	width: 150px;
}
</style>

		</head>

		<!-- Header -->

		<body class="BODY_STYLE">
			<c:choose>
				<c:when test="${!pages$transferContract.isPagePopUp}">

					<div class="containerDiv">
				</c:when>
			</c:choose>

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<c:choose>
					<c:when test="${!pages$transferContract.isPagePopUp}">

						<tr>
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>

					</c:when>
				</c:choose>



				<tr width="100%">
					<c:choose>
						<c:when test="${!pages$transferContract.isPagePopUp}">

							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>

						</c:when>
					</c:choose>

					<td width="83%" height="84%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['transferContract.heading']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">


									<h:form id="transferContractForm"
										styleClass="TransferContracttransferContractForm"
										enctype="multipart/form-data">

										<div class="SCROLLABLE_SECTION">
											<table id="layouttable" border="0" class="layoutTable"
												style="margin-left: 10px; margin-right: 10px;">
												<tr>
													<td>
														<h:outputText
															value="#{pages$transferContract.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />
														<h:outputText
															value="#{pages$transferContract.successMessages}"
															escape="false" styleClass="INFO_FONT" />
														<h:inputHidden id="hdnContractId"
															value="#{pages$transferContract.hdnContractId}"></h:inputHidden>
														<h:inputHidden id="hdnContactNumber"
															value="#{pages$transferContract.hdnContractNumber}"></h:inputHidden>
														<h:inputHidden id="hdnTenantId"
															value="#{pages$transferContract.hdnTenantId}"></h:inputHidden>
														<h:inputHidden id="hdnPersonId"
															value="#{pages$transferContract.hdnPersonId}"></h:inputHidden>
														<h:inputHidden id="hdnPersonType"
															value="#{pages$transferContract.hdnPersonType}"></h:inputHidden>
														<h:inputHidden id="hdnPersonName"
															value="#{pages$transferContract.hdnPersonName}"></h:inputHidden>
														<h:inputHidden id="hdnCellNo"
															value="#{pages$transferContract.hdnCellNo}"></h:inputHidden>
														<h:inputHidden id="hdnIsCompany"
															value="#{pages$transferContract.hdnIsCompany}"></h:inputHidden>
														<h:inputHidden id="hdnTDPersonId"
															value="#{pages$transferContract.hdnTDPersonId}"></h:inputHidden>
														<h:inputHidden id="hdnTDPersonType"
															value="#{pages$transferContract.hdnTDPersonType}"></h:inputHidden>
														<h:inputHidden id="hdnTDPersonName"
															value="#{pages$transferContract.hdnTDPersonName}"></h:inputHidden>
														<h:inputHidden id="hdnTDCellNo"
															value="#{pages$transferContract.hdnTDCellNo}"></h:inputHidden>
														<h:inputHidden id="hdnTDIsCompany"
															value="#{pages$transferContract.hdnTDIsCompany}"></h:inputHidden>
														<a4j:commandLink id="hdnNewTenantLnk"
															reRender="layouttable,tabpanel"
															action="#{pages$transferContract.populateTenantInfo}" />
													</td>
												</tr>
											</table>

											<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
											<div class="MARGIN">
												<table width="100%">
													<tr>
														<td>
															<h:outputLabel
																rendered="#{pages$transferContract.showHeaderControls}"
																value="#{msg['transferContract.oldRentAmount']}:"></h:outputLabel>
														</td>
														<td>
															<h:inputText id="oldRentValue" readonly="true"
																styleClass="READONLY"
																rendered="#{pages$transferContract.showHeaderControls}"
																style="width: 170px; height: 18px"
																binding="#{pages$transferContract.oldRentValue}">
																<f:convertNumber minFractionDigits="2"
																	maxFractionDigits="2" pattern="##,###,###.##" />
															</h:inputText>
														</td>
														<td>
															<h:outputLabel styleClass="mandatory" value="*"
																rendered="#{pages$transferContract.showHeaderControls}" />
															<h:outputLabel
																rendered="#{pages$transferContract.showHeaderControls}"
																value="#{msg['transferContract.newRentAmount']}:"></h:outputLabel>
														</td>
														<td>
															<h:inputText id="newRentAmount"
																rendered="#{pages$transferContract.showHeaderControls}"
																style="width: 170px; height: 18px"
																styleClass="#{pages$transferContract.readOnlyStyleClass}"
																readonly="#{pages$transferContract.readonlyHeaderControls}"
																binding="#{pages$transferContract.newRentAmount}"
																onchange="onBeforeChangeNewRentAmount(this);">
																<f:convertNumber minFractionDigits="2"
																	maxFractionDigits="2" pattern="##,###,###.##" />
																<%--
																<a4j:support event="onchange"
																	action="#{pages$transferContract.onLoadUpdatedPayment}"
																	reRender="tabpanel,layouttable" />
 --%>
															</h:inputText>
															<a4j:commandLink id="lnkNewRentAmountChange"
																onbeforedomupdate="javascript:onAfterChangeNewRentAmount();"
																action="#{pages$transferContract.onLoadUpdatedPayment}"
																reRender="tabpanel,layouttable" />

														</td>
													</tr>
													<tr>
														<td>
															<h:outputLabel styleClass="mandatory" value="*"
																rendered="#{pages$transferContract.showHeaderControls}" />

															<h:outputLabel
																rendered="#{pages$transferContract.showHeaderControls}"
																value="#{msg['transferContract.newStartDate']}:"></h:outputLabel>
														</td>
														<td>
															<rich:calendar id="newStartDate"
																rendered="#{pages$transferContract.showHeaderControls}"
																value="#{pages$transferContract.newStartDate}"
																datePattern="#{pages$transferContract.dateFormat}"
																disabled="#{pages$transferContract.readonlyHeaderControls}"
																styleClass="#{pages$transferContract.readOnlyStyleClass}"
																locale="#{pages$transferContract.locale}" popup="true"
																showApplyButton="false" enableManualInput="false" />
														</td>
														<td>
															<h:outputLabel styleClass="mandatory" value="*"
																rendered="#{pages$transferContract.showHeaderControls}" />
															<h:outputLabel
																value="#{msg['transferContract.newEndDate']}:"
																rendered="#{pages$transferContract.showHeaderControls}"></h:outputLabel>
														</td>
														<td>
															<rich:calendar id="newEndDate"
																rendered="#{pages$transferContract.showHeaderControls}"
																value="#{pages$transferContract.newEndDate}"
																disabled="#{pages$transferContract.readonlyHeaderControls}"
																datePattern="#{pages$transferContract.dateFormat}"
																styleClass="#{pages$transferContract.readOnlyStyleClass}"
																locale="#{pages$transferContract.locale}" popup="true"
																showApplyButton="false" enableManualInput="false" />

														</td>
													</tr>
													<tr>
														<td>
															<h:outputLabel
																value="#{msg['transferContract.comments']}:"
																rendered="#{pages$transferContract.showHeaderControls}"></h:outputLabel>
														</td>
														<td colspan="2">
															<h:inputText id="comments"
																style="width: 170px; height: 18px"
																readonly="#{pages$transferContract.readonlyHeaderControls}"
																binding="#{pages$transferContract.comments}"
																rendered="#{pages$transferContract.showHeaderControls}"
																styleClass="#{pages$transferContract.readOnlyStyleClass}" />
														</td>
													</tr>
												</table>



												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>


												<t:div styleClass="TAB_PANEL_INNER">

													<rich:tabPanel id="tabpanel"
														style="width:100%;height:235px;" headerSpacing="0"
														binding="#{pages$transferContract.tabPanel}">
														<rich:tab id="applicationTab"
															label="#{msg['commons.tab.applicationDetails']}">
															<%@ include file="ApplicationDetails.jsp"%>
														</rich:tab>
														<rich:tab id="contractDetailTab"
															label="#{msg['contract.Contract.Details']}">
															<t:panelGrid columns="4">

																<h:outputText styleClass="LABEL"
																	value="#{msg['contract.contractNumber']}" />
																<t:panelGroup>

																	<h:inputText id="aaa" readonly="true"
																		styleClass="READONLY"
																		value="#{pages$transferContract.contractView.contractNumber}"
																		style="width: 170px; height: 18px"></h:inputText>
																	<h:commandLink
																		action="#{pages$transferContract.btnContract_Click}">
																		<h:graphicImage style="padding-left:4px;"
																			title="#{msg['commons.view']}"
																			url="../resources/images/app_icons/Lease-contract.png" />
																	</h:commandLink>

																</t:panelGroup>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.contractType']}" />
																<h:inputText id="fds" readonly="true"
																	styleClass="READONLY"
																	value="#{pages$transferContract.contractView.contractTypeEn}"
																	style="width: 170px; height: 18px"></h:inputText>


																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.startDate']}" />
																<h:inputText readonly="true" styleClass="READONLY"
																	id="aadsa"
																	value="#{pages$transferContract.contractView.startDateString}"
																	style="width: 170px; height: 18px"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.endDate']}" />
																<h:inputText id="fad" readonly="true"
																	styleClass="READONLY"
																	value="#{pages$transferContract.contractView.endDateString}"
																	style="width: 170px; height: 18px"></h:inputText>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.tenantName']}" />

																<t:panelGroup>
																	<h:inputText readonly="true" styleClass="READONLY"
																		id="aaad"
																		value="#{pages$transferContract.contractView.tenantView.personFullName}"
																		style="width: 170px; height: 18px"></h:inputText>
																	<h:commandLink
																		action="#{pages$transferContract.btnTenant_Click}">
																		<h:graphicImage style="padding-left:4px;"
																			title="#{msg['commons.view']}"
																			url="../resources/images/app_icons/Tenant.png" />
																	</h:commandLink>
																</t:panelGroup>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.tenanttype']}" />


																<h:inputText readonly="true" styleClass="READONLY"
																	id="aadsddad"
																	value="#{pages$transferContract.contractView.tenantView.personTypeEn}"
																	style="width: 170px; height: 18px"></h:inputText>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['cancelContract.tab.unit.propertyname']}" />
																<h:inputText readonly="true" styleClass="READONLY"
																	value="#{pages$transferContract.contractView.contractUnit.unitView.propertyCommercialName}"
																	style="width: 170px; height: 18px"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['property.type']}" />
																<h:inputText id="drag" readonly="true"
																	styleClass="READONLY"
																	value="#{pages$transferContract.contractView.contractUnit.unitView.propertyTypeEn}"
																	style="width: 170px; height: 18px"></h:inputText>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['unit.unitNumber']}" />
																<h:inputText readonly="true" styleClass="READONLY"
																	value="#{pages$transferContract.contractView.contractUnit.unitView.unitNumber}"
																	style="width: 170px; height: 18px"></h:inputText>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['unit.rentValue']}" />
																<h:inputText readonly="true" styleClass="READONLY"
																	value="#{pages$transferContract.contractView.contractUnit.rentValue}"
																	style="width: 170px; height: 18px"></h:inputText>
																<t:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel
																		value="#{msg['transferContract.newTenantName']}:"></h:outputLabel>
																</t:panelGroup>
																<t:panelGroup>
																	<h:inputText id="newTenantName" readonly="true"
																		styleClass="READONLY"
																		style="width: 170px; height: 18px"
																		binding="#{pages$transferContract.newTenantName}" />
																	<h:graphicImage id="imgPerson2"
																		style="MARGIN: 0px 0px -4px; CURSOR: hand;"
																		url="../resources/images/app_icons/Search-tenant.png"
																		rendered="#{pages$transferContract.renderAddTenantLink}"
																		onclick="showTransferPersonPopup();">
																	</h:graphicImage>
																	<h:commandLink
																		action="#{pages$transferContract.imgAddPerson_Click}"
																		rendered="#{pages$transferContract.renderAddNewTenantBasicInfoLink}">
																		<h:graphicImage
																			title="#{msg['changeTenantName.msg.addPerson']}"
																			url="../resources/images/app_icons/Add-New-Tenant.png"
																			style="MARGIN: 0px 0px -4px; CURSOR: hand;" />
																	</h:commandLink>

																	<h:commandLink
																		action="#{pages$transferContract.btnNewTenant_Click}"
																		rendered="#{pages$transferContract.renderViewNewTenantLink}">
																		<h:graphicImage
																			style="MARGIN: 0px 0px -4px; CURSOR: hand;"
																			title="#{msg['commons.view']}"
																			url="../resources/images/app_icons/Tenant.png" />
																	</h:commandLink>


																</t:panelGroup>


																<t:panelGroup>
																	<h:outputLabel styleClass="mandatory" value="*" />
																	<h:outputLabel
																		value="#{msg['transferContract.evacuationDate']}:"></h:outputLabel>
																</t:panelGroup>
																<rich:calendar id="evacuationDate"
																	rendered="#{pages$transferContract.showEvacuationClndr}"
																	value="#{pages$transferContract.evacuationDate}"
																	timeZone="#{pages$transferContract.timeZone}"
																	datePattern="#{pages$transferContract.dateFormat}"
																	styleClass="#{pages$transferContract.readOnlyStyleClass}"
																	locale="#{pages$transferContract.locale}" popup="true"
																	showApplyButton="false" enableManualInput="false" />

																<rich:calendar id="evacuationDate2"
																	rendered="#{!pages$transferContract.showEvacuationClndr}"
																	value="#{pages$transferContract.contractView.settlementDate}"
																	timeZone="#{pages$transferContract.timeZone}"
																	disabled="true"
																	datePattern="#{pages$transferContract.dateFormat}"
																	styleClass="#{pages$transferContract.readOnlyStyleClass}"
																	locale="#{pages$transferContract.locale}" popup="true"
																	showApplyButton="false" enableManualInput="false" />
																	









															</t:panelGrid>


														</rich:tab>
														<rich:tab id="requestHistoryTab"
															rendered="#{pages$transferContract.initiationTask}"
															label="#{msg['commons.tab.requestHistory']}"
															action="#{pages$transferContract.tabRequestHistory_Click}">
															<t:div styleClass="contentDiv" style="width:97.2%">
																<t:dataTable id="requestTasksDataTable" width="100%"
																	rows="5"
																	value="#{pages$RequestHistory.requestTasksViewDataList}"
																	binding="#{pages$RequestHistory.requestHistoryDataTable}"
																	preserveDataModel="false" preserveSort="false"
																	var="notesDataItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true">
																	<t:column id="colUser">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['commons.tab.requestHistory.user']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{notesDataItem.createdBy}" />
																	</t:column>
																	<t:column id="colCreatedOn" style="width:15%">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.date']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{notesDataItem.notesDate}" />

																	</t:column>
																	<t:column id="colActivityNameEn"
																		rendered="#{pages$RequestHistory.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['commons.tab.requestHistory.activity']}" />
																		</f:facet>
																		<t:outputText title="" styleClass="A_LEFT"
																			value="#{notesDataItem.systemNoteEn}" />
																	</t:column>
																	<t:column id="colActivityNameAr"
																		rendered="#{pages$RequestHistory.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['commons.tab.requestHistory.activity']}" />
																		</f:facet>
																		<t:outputText title="" styleClass="A_LEFT"
																			value="#{notesDataItem.systemNoteAr}" />
																	</t:column>
																</t:dataTable>
															</t:div>

															<t:div styleClass="contentDivFooter" style="width:98.2%">


																<t:panelGrid columns="2" cellpadding="0" cellspacing="0"
																	width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD">
																	<t:div styleClass="RECORD_NUM_BG">
																		<t:panelGrid columns="3"
																			columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
																			cellpadding="0" cellspacing="0"
																			style="width:182px;#width:150px;FONT-WEIGHT: 100;">
																			<h:outputText value="#{msg['commons.recordsFound']}" />
																			<h:outputText value=" : " />
																			<h:outputText
																				value="#{pages$RequestHistory.recordSize}" />
																		</t:panelGrid>

																	</t:div>

																	<CENTER>
																		<t:dataScroller id="scroller"
																			for="requestTasksDataTable" paginator="true"
																			fastStep="1"
																			paginatorMaxPages="#{pages$RequestHistory.paginatorMaxPages}"
																			immediate="false" paginatorTableClass="paginator"
																			renderFacetsIfSinglePage="true"
																			pageIndexVar="pageNumber" styleClass="SCH_SCROLLER"
																			paginatorActiveColumnStyle="font-weight:bold;"
																			paginatorRenderLinkForActive="false"
																			paginatorTableStyle="grid_paginator"
																			layout="singleTable"
																			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

																			<f:facet name="first">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblFRequestHistory"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastrewind">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFRRequestHistory"></t:graphicImage>
																			</f:facet>

																			<f:facet name="fastforward">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}"
																					id="lblFFRequestHistory"></t:graphicImage>
																			</f:facet>

																			<f:facet name="last">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblLRequestHistory"></t:graphicImage>
																			</f:facet>

																			<t:div styleClass="PAGE_NUM_BG">
																				<t:panelGrid align="left" columns="2"
																					cellpadding="0" cellspacing="0">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</t:panelGrid>

																			</t:div>
																		</t:dataScroller>
																	</CENTER>

																</t:panelGrid>
															</t:div>
														</rich:tab>
														<rich:tab id="transferDetailsTab" rendered="false"
															label="#{msg['commons.tab.transferDetails']}">
															<t:div id="divTransferDetail" styleClass="DETAIL_SECTION">
																<t:panelGrid columns="4" cellpadding="1"
																	styleClass="DETAIL_SECTION_INNER" cellspacing="5">
																	<t:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel
																			value="#{msg['transferContract.newTenantName']}:"></h:outputLabel>
																	</t:panelGroup>
																	<t:panelGroup>

																	</t:panelGroup>
																	<h:outputLabel
																		value="#{msg['transferContract.country']}:"></h:outputLabel>

																	<h:inputText id="country"
																		style="width: 170px; height: 18px"
																		binding="#{pages$transferContract.county}" />
																	<h:outputLabel
																		value="#{msg['transferContract.address']}:"></h:outputLabel>

																	<h:inputText id="address"
																		style="width: 170px; height: 18px"
																		binding="#{pages$transferContract.address}" />
																	<h:outputLabel
																		value="#{msg['transferContract.emirate']}:"></h:outputLabel>

																	<h:inputText id="emirate"
																		style="width: 170px; height: 18px"
																		binding="#{pages$transferContract.emirate}" />
																	<h:outputLabel
																		value="#{msg['transferContract.cellNo']}:"></h:outputLabel>

																	<h:inputText id="cellNo"
																		style="width: 170px; height: 18px"
																		binding="#{pages$transferContract.cellNo}" />
																	<h:outputLabel
																		value="#{msg['transferContract.email']}:"></h:outputLabel>

																	<h:inputText id="email"
																		style="width: 170px; height: 18px"
																		binding="#{pages$transferContract.email}" />
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<rich:tab id="pendingAppTab"
															label="#{msg['commons.tab.pendingApp']}">

															<t:div style="text-align:center;">
																<t:div styleClass="contentDiv" style="width:83.9%">
																	<t:dataTable id="test8" rows="5" width="100%"
																		value="#{pages$transferContract.requestViewList}"
																		binding="#{pages$transferContract.pendingAppTable}"
																		preserveDataModel="true" preserveSort="true"
																		var="dataItemRCV" rowClasses="row1,row2" rules="all"
																		renderedIfEmpty="true">




																		<t:column id="dataItemRCV2" sortable="true">


																			<f:facet name="header">

																				<t:outputText value="#{msg['request.requestType']}" />

																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItemRCV.requestTypeEn}" />
																		</t:column>
																		<t:column id="dataItemRCV4" sortable="true">


																			<f:facet name="header">

																				<t:outputText value="#{msg['request.requestDate']}" />

																			</f:facet>
																			<t:outputText styleClass="A_LEFT" title=""
																				value="#{dataItemRCV.requestDateString}" />
																		</t:column>
																		<t:column id="dataItemRCV5" sortable="true">


																			<f:facet name="header">

																				<t:outputText
																					value="#{msg['request.requestStatus']}" />

																			</f:facet>
																			<t:outputText styleClass="A_LEFT" title=""
																				value="#{dataItemRCV.statusEn}" />
																		</t:column>



																	</t:dataTable>
																</t:div>



																<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																	style="width:85%;">
																	<t:panelGrid cellpadding="0" cellspacing="0"
																		width="100%" columns="2" columnClasses="RECORD_NUM_TD">

																		<t:div styleClass="RECORD_NUM_BG">
																			<t:panelGrid cellpadding="0" cellspacing="0"
																				columns="3" columnClasses="RECORD_NUM_TD">

																				<h:outputText value="#{msg['commons.recordsFound']}" />

																				<h:outputText value=" : " styleClass="RECORD_NUM_TD" />

																				<h:outputText
																					value="#{pages$transferContract.requestViewRecordSize}"
																					styleClass="RECORD_NUM_TD" />

																			</t:panelGrid>
																		</t:div>

																		<t:dataScroller id="scrollert" for="test8"
																			paginator="true" fastStep="1" paginatorMaxPages="2"
																			immediate="false" paginatorTableClass="paginator"
																			renderFacetsIfSinglePage="true"
																			paginatorTableStyle="grid_paginator"
																			layout="singleTable"
																			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																			paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																			pageIndexVar="receivingTeamPageNumber">

																			<f:facet name="firstdataItemRCV">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblFdataItemRCV"></t:graphicImage>
																			</f:facet>
																			<f:facet name="fastrewinddataItemRCV">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFRdataItemRCV"></t:graphicImage>
																			</f:facet>
																			<f:facet name="fastforwarddataItemRCV">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}"
																					id="lblFFdataItemRCV"></t:graphicImage>
																			</f:facet>
																			<f:facet name="lastdataItemRCV">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblLdataItemRCV"></t:graphicImage>
																			</f:facet>
																			<t:div styleClass="PAGE_NUM_BG">
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																				<h:outputText styleClass="PAGE_NUM"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{requestScope.receivingTeamPageNumber}" />
																			</t:div>
																		</t:dataScroller>

																	</t:panelGrid>
																</t:div>

															</t:div>


														</rich:tab>
														<rich:tab id="attachmentTab"
															label="#{msg['commons.attachmentTabHeading']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>



														<rich:tab id="PaymentTab"
															label="#{msg['cancelContract.tab.payments']}" action="">


															<t:div styleClass="A_RIGHT" style="padding:10px">
																<h:commandButton id="generatePayments"
																	rendered="#{pages$transferContract.renderPaymentButtons}"
																	styleClass="BUTTON" style="width:125px"
																	action="#{pages$transferContract.btnGenPayments_Click}"
																	value="#{msg['renewContract.generatePayments']}" />
																<h:commandButton id="pmt_sch_add" styleClass="BUTTON"
																	rendered="#{pages$transferContract.renderPaymentButtons}"
																	value="#{msg['contract.AddpaymentSchedule']}"
																	action="#{pages$transferContract.btnAddPayments_Click}"
																	style="width:150px;" />
															</t:div>



															<t:div styleClass="contentDiv" style="width:98.1%">

																<t:dataTable id="ttbb" width="100%"
																	value="#{pages$transferContract.paymentSchedules}"
																	binding="#{pages$transferContract.dataTablePaymentSchedule}"
																	preserveDataModel="false" preserveSort="false"
																	var="paymentScheduleDataItem" rowClasses="row1,row2"
																	renderedIfEmpty="true">

																	<t:column id="select" rendered="false">
																		<f:facet name="header">
																			<t:outputText id="selectTxt"
																				value="#{msg['commons.select']}" />
																		</f:facet>
																		<t:selectBooleanCheckbox id="selectChk"
																			value="#{paymentScheduleDataItem.selected}"
																			disabled="#{paymentScheduleDataItem.isReceived == 'Y'}" />
																	</t:column>

																	<t:column id="col_ContractNumber">
																		<f:facet name="header">
																			<t:outputText id="contractNumber99"
																				value="#{msg['contract.contractNumber']}" />
																		</f:facet>
																		<t:outputText id="ac55" styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.contractNumber}" />
																	</t:column>
																	<t:column id="col_PaymentNumber">
																		<f:facet name="header">
																			<t:outputText id="ac20"
																				value="#{msg['paymentSchedule.paymentNumber']}" />
																		</f:facet>
																		<t:outputText id="ac19" styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentNumber}" />
																	</t:column>
																	
																	
																	<t:column id="col_ReceiptNumber">
																		<f:facet name="header">
																			<t:outputText id="ac21"
																				value="#{msg['paymentSchedule.ReceiptNumber']}" />
																		</f:facet>
																		<t:outputText id="ac22" styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentReceiptNumber}" />
																	</t:column>
																	


																	<t:column id="colTypeEnt"
																		rendered="#{pages$transferContract.isEnglishLocale}">


																		<f:facet name="header">

																			<t:outputText id="ac17"
																				value="#{msg['paymentSchedule.paymentType']}" />

																		</f:facet>
																		<t:outputText id="ac16" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.typeEn}" />
																	</t:column>
																	<t:column id="colTypeArt"
																		rendered="#{pages$transferContract.isArabicLocale}">


																		<f:facet name="header">

																			<t:outputText id="ac15"
																				value="#{msg['paymentSchedule.paymentType']}" />

																		</f:facet>
																		<t:outputText id="ac14" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.typeAr}" />
																	</t:column>

																	<t:column id="colDesc">
																		<f:facet name="header">
																			<t:outputText id="descHead"
																				value="#{msg['renewContract.description']}" />
																		</f:facet>
																		<t:outputText id="descText" styleClass="A_LEFT"
																			title="#{paymentScheduleDataItem.description}"
																			value="#{paymentScheduleDataItem.description}" />
																	</t:column>


																	<t:column id="colDueOnt">


																		<f:facet name="header">

																			<t:outputText id="ac13"
																				value="#{msg['paymentSchedule.paymentDueOn']}" />

																		</f:facet>
																		<t:outputText id="ac12" styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentDueOn}">
																			<f:convertDateTime
																				timeZone="#{pages$transferContract.timeZone}"
																				pattern="#{pages$transferContract.dateFormatForDataTable}" />
																		</t:outputText>
																	</t:column>

																	<t:column id="colStatusEnt"
																		rendered="#{pages$transferContract.isEnglishLocale}">


																		<f:facet name="header">

																			<t:outputText id="ac11"
																				value="#{msg['commons.status']}" />

																		</f:facet>
																		<t:outputText id="ac10" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.statusEn}" />
																	</t:column>
																	<t:column id="colStatusArt"
																		rendered="#{pages$transferContract.isArabicLocale}">


																		<f:facet name="header">

																			<t:outputText id="ac9"
																				value="#{msg['commons.status']}" />

																		</f:facet>
																		<t:outputText id="ac8" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.statusAr}" />
																	</t:column>

																	<t:column id="colModeEnt"
																		rendered="#{pages$transferContract.isEnglishLocale}">


																		<f:facet name="header">

																			<t:outputText id="ac7"
																				value="#{msg['paymentSchedule.paymentMode']}" />

																		</f:facet>
																		<t:outputText id="ac6" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentModeEn}" />
																	</t:column>
																	<t:column id="colModeArt"
																		rendered="#{pages$transferContract.isArabicLocale}">


																		<f:facet name="header">

																			<t:outputText id="ac5"
																				value="#{msg['paymentSchedule.paymentMode']}" />

																		</f:facet>
																		<t:outputText id="ac4" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentModeAr}" />
																	</t:column>


																	<t:column id="colAmountt">


																		<f:facet name="header">

																			<t:outputText id="ac1"
																				value="#{msg['paymentSchedule.amount']}" />

																		</f:facet>
																		<t:outputText id="ac2" styleClass="A_RIGHT_NUM"
																			value="#{paymentScheduleDataItem.amount}">
																			<f:convertNumber minFractionDigits="2"
																				maxFractionDigits="2" pattern="##,###,###.##" />
																		</t:outputText>
																	</t:column>


																	<t:column id="ActionCol"
																		rendered="#{true || pages$transferContract.isDeletable || pages$transferContract.collectingNewCntrctPmnt}"
																		>
																		<f:facet name="header">
																			<t:outputText id="ac3"
																				value="#{msg['commons.action']}" />
																		</f:facet>
																		<h:commandLink
																			rendered="#{pages$transferContract.isDeletable}">
																			action="#{pages$transferContract.btnPrintPaymentSchedule_Click}">
																			<h:graphicImage id="printPayments"
																				title="#{msg['commons.print']}"
																				url="../resources/images/app_icons/print.gif"
																				rendered="#{paymentScheduleDataItem.isReceived == 'Y'}" />
																		</h:commandLink>
																		<h:commandLink action="#{pages$transferContract.btnViewPaymentDetails_Click}">
																				<h:graphicImage id="viewPayments" 
																				title="#{msg['commons.group.permissions.view']}" 
																				url="../resources/images/detail-icon.gif"
																				/>
																		</h:commandLink> 
																		
																		<h:commandLink
																			rendered="#{pages$transferContract.isDeletable || pages$transferContract.collectingNewCntrctPmnt || paymentScheduleDataItem.isReceived == 'N'}"
																			action="#{pages$transferContract.btnEditPaymentSchedule_Click}">
																			<h:graphicImage id="editPayments"
																				title="#{msg['commons.edit']}"
																				url="../resources/images/edit-icon.gif" />
																		</h:commandLink>
																		<h:commandLink
																					action="#{pages$transferContract.onSplitPayment}">
																					<h:graphicImage id="splitPayments"
																						title="#{msg['replaceChequeLabel.btnSplit']}"
																						url="../resources/images/app_icons/Add-fee.png"
																						rendered="#{paymentScheduleDataItem.isReceived == 'N' && 
																						            paymentScheduleDataItem.statusId != 36019 &&
																				                    pages$transferContract.showSplitPaymentsIcon
																				                   }" />
                  
																				</h:commandLink>
																		<h:commandLink id="deleteLink"
																			rendered="#{pages$transferContract.isDeletable}"
																			action="#{pages$transferContract.deletePayment}">
																			
																			<h:graphicImage style="margin-left:13px;"
																				title="#{msg['commons.delete']}"
																				rendered="#{paymentScheduleDataItem.showSelect && 
																				            paymentScheduleDataItem.isReceived == 'N' &&
																				            paymentScheduleDataItem.isSystem==0 
																				           }"
																				url="../resources/images/delete_icon.png" />
																		</h:commandLink>
																	</t:column>



																</t:dataTable>
															</t:div>
															<t:div>
															
															</t:div>
														</rich:tab>

													</rich:tabPanel>

												</t:div>
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<table cellpadding="1px" cellspacing="3px" width="100%">



													<tr>
														<td colspan="6" class="BUTTON_TD">

															<pims:security screen="Pims.Contract.Transfer.Approve"
																action="create">
																<t:commandButton styleClass="BUTTON"
																	value="#{msg['commons.approve']}"
																	onclick="if (!confirm('#{msg['transferContract.messages.confirmApproveRent']}')) return false"
																	action="#{pages$transferContract.btnApprove_Click}"
																	binding="#{pages$transferContract.btnApprove}"
																	style="width: 90px">
																</t:commandButton>
																<t:commandButton styleClass="BUTTON"
																	value="#{msg['commons.verify']}"
																	onclick="if (!confirm('#{msg['transferContract.messages.confirmApproveRent']}')) return false"
																	action="#{pages$transferContract.verifyAndSend}"
																	binding="#{pages$transferContract.verifySuggestion}"
																	style="width: 90px">
																</t:commandButton>
																<t:commandButton styleClass="BUTTON"
																	value="#{msg['commons.reject']}"
																	onclick="if (!confirm('#{msg['transferContract.messages.confirmRejectRent']}')) return false"
																	action="#{pages$transferContract.btnReject_Click}"
																	binding="#{pages$transferContract.btnReject}"
																	style="width: 90px">
																</t:commandButton>
															</pims:security>

															<pims:security screen="Pims.Contract.Transfer.Complete"
																action="create">
															</pims:security>
															<pims:security
																screen="Pims.Contract.Transfer.IssueNewContract"
																action="create">
																<t:commandButton styleClass="BUTTON"
																	onclick="if (!confirm('#{msg['transferContract.messages.confirmIssueNewContract']}')) return false"
																	value="#{msg['transferContract.buttons.issueContractLabel']}"
																	action="#{pages$transferContract.issueNewContract}"
																	binding="#{pages$transferContract.createNewLeaseContract}"
																	style="width: 90px">
																</t:commandButton>
															</pims:security>
															<pims:security screen="Pims.Contract.Transfer.Submit"
																action="create">
																<t:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['transferContract.messages.submitButton']}')) return false"
																	action="#{pages$transferContract.btnSave_Click}"
																	binding="#{pages$transferContract.btnSave}"
																	style="width: 90px">
																</t:commandButton>
																<t:commandButton styleClass="BUTTON"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['transferContract.messages.submitButton']}')) return false"
																	action="#{pages$transferContract.btnSubmit_Click}"
																	binding="#{pages$transferContract.btnSubmit}"
																	style="width: 90px">
																</t:commandButton>
															</pims:security>
															<t:commandButton styleClass="BUTTON"
																value="#{msg['commons.reviewButton']}"
																onclick="if (!confirm('#{msg['transferContract.messages.submitButton']}')) return false"
																action="#{pages$transferContract.reviewContract}"
																binding="#{pages$transferContract.reviewed}"
																style="width: 90px">
															</t:commandButton>
															<t:commandButton styleClass="BUTTON"
																value="#{msg['commons.approveButton']}"
																action="#{pages$transferContract.approveReview}"
																binding="#{pages$transferContract.approveReview}"
																style="width: 90px">
															</t:commandButton>
															<t:commandButton styleClass="BUTTON"
																value="#{msg['commons.rejectButton']}"
																onclick="if (!confirm('#{msg['transferContract.messages.submitButton']}')) return false"
																binding="#{pages$transferContract.rejectReview}"
																action="#{pages$transferContract.rejectReview}"
																style="width: 90px">
															</t:commandButton>
															<pims:security screen="Pims.Contract.Transfer.Pay"
																action="create">

																<t:commandButton styleClass="BUTTON"
																	value="#{msg['commons.collectAppFee']}"
																	binding="#{pages$transferContract.collectAppFee}"
																	action="#{pages$transferContract.openReceivePaymentsPopUp}"
																	style="width: 90px">
																</t:commandButton>
																<t:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancelAppFee']}"
																	onclick="if (!confirm('#{msg['transferContract.messages.submitButton']}')) return false"
																	binding="#{pages$transferContract.cancelAppFee}"
																	action="#{pages$transferContract.cancelCollection}"
																	style="width: 90px">
																</t:commandButton>
																<t:commandButton styleClass="BUTTON"
																	value="#{msg['commons.setteled']}"
																	onclick="if (!confirm('#{msg['transferContract.messages.submitButton']}')) return false"
																	binding="#{pages$transferContract.setteled}"
																	style="width: 90px">
																</t:commandButton>
																<t:commandButton styleClass="BUTTON"
																	value="#{msg['commons.collectOldContractPayment']}"
																	onclick="if (!confirm('#{msg['transferContract.messages.submitButton']}')) return false"
																	binding="#{pages$transferContract.collectOldContractPayment}"
																	action="#{pages$transferContract.openReceivePaymentsPopUp}"
																	style="width: 90px">
																</t:commandButton>
																<t:commandButton styleClass="BUTTON"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['transferContract.messages.submitButton']}')) return false"
																	binding="#{pages$transferContract.completeOldContractPaymentTask}"
																	action="#{pages$transferContract.completeOldContractPaymentTask}"
																	style="width: 90px">
																</t:commandButton>

																<t:commandButton styleClass="BUTTON"
																	value="#{msg['commons.collectNewContractPayment']}"
																	onclick="if (!confirm('#{msg['transferContract.messages.submitButton']}')) return false"
																	binding="#{pages$transferContract.collectNewContractPayment}"
																	action="#{pages$transferContract.openReceivePaymentsPopUp}"
																	style="width: 90px">
																</t:commandButton>

															</pims:security>
															<t:commandButton styleClass="BUTTON"
																value="#{msg['common.completeRequest']}"
																binding="#{pages$transferContract.btnComplete}"
																action="#{pages$transferContract.completeRequest}"
																style="width: auto;">
															</t:commandButton>
															<h:commandButton id="printSett" style="width:150px;" styleClass="BUTTON" action="#{pages$transferContract.reprintReport}" 
											                            value="#{msg['cancelContract.btn.reprintSettlement']}" />
															<c:choose>
																<c:when test="${!pages$transferContract.isPagePopUp}">

																	<t:commandButton styleClass="BUTTON"
																		value="#{msg['commons.back']}"
																		action="#{pages$transferContract.btnBack_Click}"
																		style="width: 90px">
																	</t:commandButton>

																</c:when>
															</c:choose>



														</td>

													</tr>


												</table>
											</div>
										</div>
									</h:form>


								</td>
							</tr>
						</table>


					</td>
				</tr>
				<c:choose>
					<c:when test="${!pages$transferContract.isPagePopUp}">

						<tr>
							<td colspan="2">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</tr>
								</table>
							</td>
						</tr>

					</c:when>
				</c:choose>

			</table>

			<c:choose>
				<c:when test="${!pages$transferContract.isPagePopUp}">

					</div>
				</c:when>
			</c:choose>


		</body>
	</html>
</f:view>
