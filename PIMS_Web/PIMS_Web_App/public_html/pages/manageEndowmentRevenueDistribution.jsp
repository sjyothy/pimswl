<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
	}	

	
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel
											value="#{msg['endowment.title.revenueDistributionManage']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>

																<h:outputText id="errorId"
																	value="#{pages$manageEndowmentRevenueDistribution.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$manageEndowmentRevenueDistribution.successMessages}"
																	escape="false" styleClass="INFO_FONT" />

															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<t:div rendered="true" style="width:100%;">
														<t:panelGrid cellpadding="1px" width="100%"
															cellspacing="5px" columns="4">

															<h:outputLabel styleClass="LABEL" id="lblNum"
																value="#{msg['endowment.lbl.num']}:" />
															<h:inputText id="txtendowmentNum" readonly="true"
																value="#{pages$manageEndowmentRevenueDistribution.pageObject.endowmentNumber}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL" id="lblName"
																value="#{msg['endowment.lbl.name']}:" />
															<h:inputText id="txtendowmentName" readonly="true"
																value="#{pages$manageEndowmentRevenueDistribution.pageObject.endowmentName}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL" id="lblCostCenter"
																value="#{msg['unit.costCenter']}:" />
															<h:inputText id="txtendowmentCostCenter" readonly="true"
																value="#{pages$manageEndowmentRevenueDistribution.pageObject.endowmentCostCenter}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL" id="lblPropNum"
																value="#{msg['commons.report.propertyNumber']}:" />
															<h:inputText id="txtpropertyNumber" readonly="true"
																value="#{pages$manageEndowmentRevenueDistribution.pageObject.propertyNumber}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL" id="lbltotalRevenue"
																value="#{msg['commons.totalRevenue']}:" />
															<h:inputText id="txttotalRevenue" readonly="true"
																value="#{pages$manageEndowmentRevenueDistribution.pageObject.amountToDistribute}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																id="lbloutstandingExpenses"
																value="#{msg['commons.outstandingExpenses']}:" />
															<h:inputText id="txttotalExpenseAmount" readonly="true"
																value="#{pages$manageEndowmentRevenueDistribution.totalExpenseAmount}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																id="totalAvailableRevenueAfterExpenseDeduction"
																value="#{msg['commons.totalAvailableRevenueAfterExpenseDeduction']}:" />
															<h:inputText
																id="txttotalAvailableRevenueAfterExpenseDeduction"
																readonly="true"
																value="#{pages$manageEndowmentRevenueDistribution.totalAvailableRevenueAfterExpenseDeduction}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																id="totalFeesToBeDeductedAfterExpenseDeduction"
																value="#{msg['commons.Fees']}:" />
															<h:inputText
																id="txttotalFeesToBeDeductedAfterExpenseDeduction"
																readonly="true"
																value="#{pages$manageEndowmentRevenueDistribution.totalFeesToBeDeductedAfterExpenseDeduction}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																id="totalAvailableRevenueAfterFeesDeduction"
																value="#{msg['commons.totalAvailableRevenueAfterFeesDeduction']}:" />
															<h:inputText
																id="txttotalAvailableRevenueAfterFeesDeduction"
																readonly="true"
																value="#{pages$manageEndowmentRevenueDistribution.totalAvailableRevenueAfterFeesDeduction}"
																styleClass="READONLY" />


															<h:outputLabel styleClass="LABEL" id="totalRebuildAmount"
																value="#{msg['commons.totalRebuildAmount']}:" />
															<h:inputText id="txttotalRebuildAmount" readonly="true"
																value="#{pages$manageEndowmentRevenueDistribution.totalRebuildRevenue}"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																id="totalDistributableAmount"
																value="#{msg['commons.totalDistributableAmount']}:" />
															<h:inputText id="txttotalDistributableAmount"
																readonly="true"
																value="#{pages$manageEndowmentRevenueDistribution.totalRevenueToDistribute}"
																styleClass="READONLY" />

														</t:panelGrid>
													</t:div>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">

														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$manageEndowmentRevenueDistribution.tabPanel}"
																style="width: 100%">
																<rich:tab id="tabPropertyOutstandingExpenses"
																	action="#{pages$manageEndowmentRevenueDistribution.onExpensesTab}"
																	label="#{msg['commons.outstandingExpenses']}">
																	<%@ include file="tabOutStandingExpenses.jsp"%>
																</rich:tab>

																<rich:tab
																	id="tabPropertyRevenueDistributionContractWise"
																	label="#{msg['commons.details']}">
																	<%@ include
																		file="tabPropertyRevenueDistributionContractWise.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>

														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">

															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.distributeAll']}"
																onclick="if (!confirm('#{msg['endowmentFile.msg.alertEndowmentFileCompletion']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkDistributeAll"
																action="#{pages$manageEndowmentRevenueDistribution.onDistributeAll}" />

														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">

									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>