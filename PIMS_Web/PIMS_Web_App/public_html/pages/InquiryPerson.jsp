

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	<title>PIMS</title>
	<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
	
		<script language="javascript" type="text/javascript">
		
		
		
		
		function clearWindow()
	{
	       //var cmbPriority=document.getElementById("formInquiry:selectPriority");
	       //var cmbMethod=document.getElementById("formUnits:selectInquiryMethod");
	       //var selectedManyUnits = document.getElementById("formUnits:selectManyUnits");
	        document.getElementById("formPerson:txtfirstName").value="";
		    document.getElementById("formPerson:txtmiddleName").value="";
		    document.getElementById("formPerson:txtlastName").value="";
		    //document.getElementById("formPerson:txtpersonType").value="";
		    //document.getElementById("formPerson:txtgender").value="";
		    document.getElementById("formPerson:dateOfBirth").value="";
		    document.getElementById("formPerson:txtcellNo").value="";
		    document.getElementById("formPerson:txtDesignation").value="";
		    document.getElementById("formPerson:txtnationality").value="";
		    document.getElementById("formPerson:txtpassportNumber").value="";
		    document.getElementById("formPerson:datePassportIssueDate").value="";
		    document.getElementById("formPerson:datePassportExpiryDate").value="";
		    document.getElementById("formPerson:txtresidenseVisaNumber").value="";
		    document.getElementById("formPerson:dateVisaExpiryDate").value="";
		    document.getElementById("formPerson:txtpersonalSecCardNo").value="";
		    document.getElementById("formPerson:txtdrivingLicenseNumber").value="";
		    document.getElementById("formPerson:txtsocialSecNumber").value="";
		    document.getElementById("formPerson:txtaddress1").value="";
		    document.getElementById("formPerson:txtaddress2").value="";
            document.getElementById("formPerson:txtemail").value="";
            document.getElementById("formPerson:txtfax").value="";
            document.getElementById("formPerson:txtofficePhone").value="";
            document.getElementById("formPerson:txthomePhone").value="";
            document.getElementById("formPerson:txtpostCode").value="";
            document.getElementById("formPerson:txtstreet").value="";
            
            var cmbTitle=document.getElementById("formPerson:selectTitle");
		     cmbTitle.selectedIndex = 0;
		    
		    var cmbGender =document.getElementById("formPerson:selectGender");
		     cmbGender.selectedIndex = 0; 
		     
		    
		   // var cmbpassportIssuePlace=document.getElementById("formPerson:selectpassportIssuePlace");
		   //  cmbpassportIssuePlace.selectedIndex = 0;
		    
		  //  var cmbresidenseVisaIssuePlace=document.getElementById("formPerson:selectresidenseVisaIssuePlace");
		  //   cmbresidenseVisaIssuePlace.selectedIndex = 0;
		    
		    var cmbcity=document.getElementById("formPerson:selectcity");
		     cmbcity.selectedIndex = 0;
		    
		    var cmbstate=document.getElementById("formPerson:selectstate");
		     cmbstate.selectedIndex = 0;
		    
		    var cmbcountry=document.getElementById("formPerson:selectcountry");
		     cmbcountry.selectedIndex = 0; 
		    
		    



		    //cmbPriority.selectedIndex = 0;
		    //cmbMethod.selectedIndex = 0;
		    }
		    
     function submitForm()
	   {
          document.getElementById('formPerson').submit();
          document.getElementById("formPerson:selectcountry").selectedIndex=0;
          document.getElementById("formPerson:selectstate").selectedIndex=0;
	   }
		    
		    function submitFormState(){
       
		    // alert("In submitFormState");    
		    var cmbState=document.getElementById("formPerson:selectstate");
		    var j = 0;
		    j=cmbState.selectedIndex;
		   // alert("value of j"+j);
		    document.getElementById("formPerson:hdnCityValue").value=cmbState.options[j].value ;
		     
		    document.forms[0].submit();
		   // alert("End submitFormState");
		    
		    }
		
		function getPersonInfo()
		{
		//alert("IngetPersonInfo");
		//alert(document.getElementById("personId"));
		    if(document.getElementById("personId")!=null)
			{
			setTimeout("3200");
		//	alert("IngetpersonId::::If");
		window.opener.document.getElementById("formInquiry:txtPerson").value=document.getElementById("personName").value;
	    window.opener.document.getElementById("formInquiry:hdnPersonId").value=document.getElementById("personId").value;
	    if(document.getElementById("passportNumber")!=null){
	    window.opener.document.getElementById("formInquiry:txtPassportNo").value=document.getElementById("passportNumber").value;}
	    else {
	    window.opener.document.getElementById("formInquiry:txtPassportNo").value="";
	    }
	    if(document.getElementById("cellNumber")!=null){
	    window.opener.document.getElementById("formInquiry:txtCellNo").value=document.getElementById("cellNumber").value;}
	    else{
	    window.opener.document.getElementById("formInquiry:txtCellNo").value="";
	    }
	    if(document.getElementById("designation")!=null){
	    window.opener.document.getElementById("formInquiry:txtDesignation").value=document.getElementById("designation").value;}
	    else {
	    window.opener.document.getElementById("formInquiry:txtDesignation").value="";
	    }
		window.close();
			}
		}
    </script>
    <style>
   .rich-calendar-input{
    width:132px;
     }
</style>
</head>

<body dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" onload="getPersonInfo()" class="BODY_STYLE">  
    <!-- Header --> 
<table width="100%" cellpadding="0" cellspacing="0"  border="0">
    

<tr width="100%">
   
    <td width="100%"  class="divBackgroundBody" height="580px">
        <table cellpadding="0" cellspacing="0" height="100%" width="100%">
		<tr height="1%">
			<td>
				<table width="100%" class="greyPanelTable" cellpadding="0"
					cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
								<h:outputLabel value="#{msg['inquiry.personinformation']}" styleClass="HEADER_FONT"/>
						</td>
						<td width="100%">
							&nbsp;
						</td>
					</tr>
				</table>
			</td>
		</tr>    
		<tr width="100%" height="99%" valign="top">
			<td>
        <table width="100%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%" style="height:98%;">
          <tr valign="top">
             <td height="100%"  valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
             </td> 
	
  <td width="100%" height="500px" valign="top" nowrap="nowrap">
			
			
	 <div >
			<h:form id="formPerson" style="width:100%">
		<div style="height:25px">	
			<table border="0" class="layoutTable" >
			<%if (request.getAttribute("personName")!=null) {%>
			<input type="hidden" id ="personName" value="<%=request.getAttribute("personName") %>" >
			<%} %>
			<%if (request.getAttribute("person")!=null) {%>
			<input type="hidden" id ="personId" value="<%=request.getAttribute("person") %>" >
			<%} %>
			
			<%if (request.getAttribute("passportNumber")!=null) {%>
			<input type="hidden" id ="passportNumber" value="<%=request.getAttribute("passportNumber") %>" >
			<%} %>
			
			<%if (request.getAttribute("cellNumber")!=null) {%>
			<input type="hidden" id ="cellNumber" value="<%=request.getAttribute("cellNumber") %>" >
			<%} %>
			
			<%if (request.getAttribute("designation")!=null) {%>
			<input type="hidden" id ="designation" value="<%=request.getAttribute("designation") %>" >
			<%} %>
			<h:inputHidden id="requestForPersonType" value="#{pages$InquiryPerson.personTypeToAdd}" />
			
			<tr>
				<td colspan="6">
				<h:outputText value="#{pages$InquiryPerson.errorMessages}" escape="false" styleClass="ERROR_FONT" style="padding:10px;"/>
				<h:outputText value="#{pages$InquiryPerson.successMessages}" escape="false" styleClass="INFO_FONT" style="padding:10px;"/>
				</td>
			</tr>
			</table>
		</div>
			<div class="MARGIN">
		       						<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
				<div class="DETAIL_SECTION" >
				<h:outputLabel value="#{msg['person.personDetail']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>					
											<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
												 	
												
												<tr>
													<td  >
														<h:outputLabel styleClass="LABEL" value="#{msg['customer.title']}:"></h:outputLabel>
													</td>
													
													<td style="padding-right:2px;" >
															
													    <h:selectOneMenu   id="selectTitle" style="width: 155px" value="#{pages$InquiryPerson.selectOneTitle}" >
															<f:selectItems value="#{pages$ApplicationBean.title}"/>
														</h:selectOneMenu>
													
													</td>
													<td >
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL" value="#{msg['customer.firstname']}:"></h:outputLabel>
													</td>
													
													<td >
												        <h:inputText styleClass="A_LEFT"  style="width:150px;" maxlength="50" id = "txtfirstName"  value="#{pages$InquiryPerson.firstName}">
												        
												        </h:inputText>
													</td>
													<td >
														<h:outputLabel styleClass="LABEL" value="#{msg['customer.middlename']}:"></h:outputLabel>
													</td>
		 											
													<td >
												        <h:inputText styleClass="A_LEFT" style="width:150px;"   maxlength="10" id="txtmiddleName" value="#{pages$InquiryPerson.middleName}">
												        
												        </h:inputText>
													</td>
																										</tr>
											    
											    <tr>
											    	<td >
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL" value="#{msg['customer.lastname']}:"></h:outputLabel>
													</td>
		 											
													<td >
												        <h:inputText styleClass="A_LEFT" style="width:150px;"   maxlength="50" id="txtlastName"  value="#{pages$InquiryPerson.lastName}">
												        
												        </h:inputText>
													</td>
											    
											    
													<td ><h:outputLabel styleClass="LABEL"  value="#{msg['customer.gender']}:"></h:outputLabel>
														
													</td>
													
													<td style="padding-right:2px;">
													<h:selectOneMenu id="selectGender" style="width: 155px" value="#{pages$InquiryPerson.gender1}">
														<f:selectItem itemLabel="#{msg['tenants.gender.male']}" itemValue="M" />
														<f:selectItem itemLabel="#{msg['tenants.gender.female']}" itemValue="F" />	
														</h:selectOneMenu>
												        
													</td>
													<td ><h:outputLabel styleClass="LABEL" value="#{msg['customer.dateOfBirth']}:"></h:outputLabel>
														
														
													</td>
													
													<td>
													
												        
												        <rich:calendar  id="DateOfBirth" value="#{pages$InquiryPerson.dateOfBirth}" 
							                          popup="true" datePattern="#{pages$InquiryPerson.dateFormat}" showApplyButton="false"
							                          locale="#{pages$InquiryPerson.locale}" style="width:133px"
							                          enableManualInput="false" cellWidth="24px" 
							                          />
													</td>
													
    													
													
												
												</tr>
											<tr>
											<td>
														
													<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"  value="#{msg['customer.cell']}:"/></td>
													
													<td><h:inputText styleClass="A_RIGHT_NUM" style="width:152px;"  maxlength="15" id="txtcellNo" value="#{pages$InquiryPerson.cellNo}">
												        
												        </h:inputText>
															
															
														   
														
														</td>
													<td><h:outputLabel styleClass="LABEL" value="#{msg['customer.designation']}:"/>
														
													</td>
													<td><h:inputText styleClass="A_LEFT"   style="width:150px;" maxlength="50" id="txtDesignation" value="#{pages$InquiryPerson.designation}">
												        
												        </h:inputText>
												        
													</td>
    													
													
												
												
													<td ><h:outputLabel styleClass="LABEL" value="#{msg['customer.nationality']}:"></h:outputLabel>
														
													</td>
													<td><h:inputText styleClass="A_LEFT" style="width:150px;"   maxlength="20" id="txtnationality" value="#{pages$InquiryPerson.nationality}">
												        
												        </h:inputText>
												        
													</td>
	 											
													
												</tr>
												<tr>
												
												<td ><h:outputLabel styleClass="LABEL"  value="#{msg['customer.passportNumber']}:"></h:outputLabel>
														
													</td>
													<td>
												        
													<h:inputText styleClass="A_LEFT" style="width:150px;" maxlength="15" id="txtpassportNumber" value="#{pages$InquiryPerson.passportNumber}">
												        
												        </h:inputText></td>
	 											
													<td >
														
													<h:outputLabel styleClass="LABEL"  value="#{msg['customer.passportIssueDate']}:"></h:outputLabel></td>
													<td>
													
													<rich:calendar  id="passportIssueDate" value="#{pages$InquiryPerson.passportIssueDate}" 
							                          popup="true" datePattern="#{pages$InquiryPerson.dateFormat}" showApplyButton="false"
							                          locale="#{pages$InquiryPerson.locale}" 
							                          enableManualInput="false" cellWidth="24px" 
							                          />
												        
													</td>
	 											
														<td><h:outputLabel styleClass="LABEL"  style="width:150px;"  value="#{msg['customer.passportExpiryDate']}:"></h:outputLabel>
														
													</td>
													<td>
														
														<rich:calendar  id="passportExpiryDate" value="#{pages$InquiryPerson.passportExpiryDate}" 
							                          popup="true" datePattern="#{pages$InquiryPerson.dateFormat}" showApplyButton="false"
							                          locale="#{pages$InquiryPerson.locale}" 
							                          enableManualInput="false" cellWidth="24px" 
							                          />
												</tr>
												<tr>
												
													
													<td ><h:outputLabel styleClass="LABEL" value="#{msg['customer.residenseVisaNumber']}:"></h:outputLabel>
														
													</td>
													<td ><h:inputText styleClass="A_LEFT" style="width:150px;"   maxlength="15" id="txtresidenseVisaNumber" value="#{pages$InquiryPerson.residenseVisaNumber}">
												        
												        </h:inputText>
															
													</td>
												
													<td ><h:outputLabel styleClass="LABEL"   value="#{msg['customer.residenseVisaExpDate']}:"></h:outputLabel>
														
													</td>
													<td>
													
													<rich:calendar  id="residenceVidaExpDate" value="#{pages$InquiryPerson.residenseVidaExpDate}" 
							                          popup="true" datePattern="#{pages$InquiryPerson.dateFormat}" showApplyButton="false"
							                          locale="#{pages$InquiryPerson.locale}" 
							                          enableManualInput="false" cellWidth="24px" 
							                          />
													</td>
													
													<td >
													<h:outputLabel styleClass="LABEL"  value="#{msg['customer.drivingLicenseNumber']}:"></h:outputLabel></td>
														
													<td><h:inputText  styleClass="A_LEFT" style="width:150px;"  maxlength="15" id="txtdrivingLicenseNumber" value="#{pages$InquiryPerson.drivingLicenseNumber}">
												        
												        </h:inputText>
												        
													</td>	
																									
												</tr>
											
															
												<tr>
												
				
												<td ><h:outputLabel  styleClass="LABEL" value="#{msg['customer.personalSecCardNo']}:"></h:outputLabel>
														
													</td>
													<td ><h:inputText    styleClass="A_LEFT" style="width:150px;" maxlength="15" id="txtpersonalSecCardNo" value="#{pages$InquiryPerson.personalSecCardNo}">
												        
												        </h:inputText>
															
													</td>	
													
													
													<td >
														
													<h:outputLabel  styleClass="LABEL" value="#{msg['customer.socialSecNumber']}:"></h:outputLabel></td>
													<td>
												        
													<h:inputText styleClass="A_LEFT" style="width:150px;"  maxlength="15" id="txtsocialSecNumber" value="#{pages$InquiryPerson.socialSecNumber}">
												        
												        </h:inputText></td>
														
																
		
												
												</tr>		
														
												
															
														
								
											
											</table>
                                              </div>
                                            </div>
                                  <div class="MARGIN">
									<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
									<div class="DETAIL_SECTION">
										<h:outputLabel value="#{msg['contact.contactinformation']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
                                            <table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
												<tr>
													<td>
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel  styleClass="LABEL" value="#{msg['contact.address1']}:"></h:outputLabel>
													</td>
													<td  >
												        <h:inputText  styleClass="A_LEFT" style="width:150px;"   maxlength="150" id = "txtaddress1" value="#{pages$InquiryPerson.address1}">
												        
												        </h:inputText>
													</td>
													<td >
														<h:outputLabel styleClass="LABEL" value="#{msg['contact.address2']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT" style="width:150px;"  maxlength="150" id = "txtaddress2" value="#{pages$InquiryPerson.address2}">
												        
												        </h:inputText>
													</td>
													<td >
														<h:outputLabel styleClass="LABEL"  value="#{msg['contact.street']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT"  style="width:150px;"  maxlength="150" id = "txtstreet"   value="#{pages$InquiryPerson.street}">
												        
												        </h:inputText>
													</td>
													
												</tr>
                                                 
                                                 <tr>
                                                 <td>
														<h:outputLabel styleClass="LABEL"  value="#{msg['contact.postcode']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_RIGHT_NUM" style="width:150px;"  maxlength="10" id= "txtpostCode"  value="#{pages$InquiryPerson.postCode}">
												        
												        </h:inputText>
													</td>
                                                 <td >
													<h:outputLabel  styleClass="LABEL" value="#{msg['contact.country']}:"></h:outputLabel>
													</td>
												
													<td >
																		<h:selectOneMenu id="selectcountry" tabindex="2" style="width: 140px" 
																		value="#{pages$InquiryPerson.selectOneCountry}" 
																		onchange="javascript:submitForm();" 
																		valueChangeListener="#{pages$InquiryPerson.loadStates}" >
																		<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
																		<f:selectItems value="#{pages$InquiryPerson.countries}" />
																		</h:selectOneMenu>
																		</td>
													<td>
														<h:outputLabel styleClass="LABEL"  value="#{msg['contact.state']}:"></h:outputLabel>
													</td>
													<td >
														<h:selectOneMenu id="selectstate"  valueChangeListener="#{pages$InquiryPerson.loadCities}"  
														onchange="javascript:submitForm();" style="width: 155px" value="#{pages$InquiryPerson.selectOneState}">
														
														<f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1"/>
															<f:selectItems value="#{pages$InquiryPerson.states}"/>
															
														</h:selectOneMenu>	
													</td>
													
													
													
													
												</tr>
												
												<tr>
												<td>
														<h:outputLabel  styleClass="LABEL" value="#{msg['contact.city']}:"></h:outputLabel>
													</td>
												        <td >
														<h:selectOneMenu id="selectcity" style="width: 155px"  value="#{pages$InquiryPerson.selectOneCity}">
														<f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1"/>
															<f:selectItems value="#{pages$InquiryPerson.cities}"/>
														</h:selectOneMenu>	
														</td>
													<td>
														<h:outputLabel  styleClass="LABEL" value="#{msg['contact.homephone']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_RIGHT_NUM" style="width:152px;"   maxlength="15" id = "txthomePhone" value="#{pages$InquiryPerson.homePhone}">
												        
												        </h:inputText>
													</td>
													<td >
														<h:outputLabel styleClass="LABEL" value="#{msg['contact.officephone']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_RIGHT_NUM" style="width:150px;" maxlength="15" id  ="txtofficePhone" value="#{pages$InquiryPerson.officePhone}">
												        
												        </h:inputText>
													</td>
													
												</tr>
												<tr>
												<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['contact.fax']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_RIGHT_NUM" style="width:152px;"  maxlength="15" id = "txtfax"  value="#{pages$InquiryPerson.fax}">
												        
												        </h:inputText>
													</td>
													<td >
														<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL"  value="#{msg['contact.email']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText styleClass="A_LEFT" style="width:150px;"  maxlength="50" id = "txtemail"  value="#{pages$InquiryPerson.email}">
												        
												        </h:inputText>
													</td>
												</tr>
												<h:inputHidden id="hdnStateValue" binding="#{pages$InquiryPerson.hiddenStateValue}"  />
												<h:inputHidden id="hdnCityValue" binding="#{pages$InquiryPerson.hiddenCityValue}"  />
											
											
											
                                		        
                                            </table>
                                           </div>
                                           </div>
                                           
                                           <table width="100%">
                                             <tr>
        										<td colspan="10" class="BUTTON_TD" >
                                      				    <h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" type= "submit"   action="#{pages$InquiryPerson.btnAdd_Click}" >  </h:commandButton>
											     	    <h:commandButton styleClass="BUTTON" type="button" onclick="javascript:clearWindow();" value="#{msg['commons.reset']}" >  </h:commandButton>
												        <h:commandButton styleClass="BUTTON" type= "submit" value="#{msg['commons.cancel']}" action="#{pages$InquiryPerson.btnBack_Click}" >	</h:commandButton>
                                		        </td>
                                		     </tr>
                                           </table>
                                            

 										
												          
               
			</h:form>
			</div>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
			</table>
			
			</body>
		</f:view>
	
</html>
