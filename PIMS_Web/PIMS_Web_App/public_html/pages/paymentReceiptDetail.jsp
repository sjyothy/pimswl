<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<body dir="${sessionScope.CurrentLocale.dir}"
			lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">

			<div class="containerDiv">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="87%" height="470px" valign="top"
							class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['paymentReceiptDetail.header']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>

									<td width="100%" height="440px" valign="top">
										<div class="SCROLLABLE_SECTION"
											style="padding-bottom: 7px; padding-left: 10px; padding-right: 0; padding-top: 7px;">
											<h:form id="formChequeList" style="width:96%">

												<div>
													<table border="0" class="layoutTable">
														<tr>
															<td colspan="6">
																<h:outputText
																	value="#{pages$paymentReceiptDetail.errorMessages}"
																	escape="false" styleClass="INFO_FONT"
																	style="padding:10px;" />
															</td>
														</tr>
													</table>
												</div>

												<div class="MARGIN">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px"> 
																<IMG
																	src="../<h:outputText value="#{path.img_section_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>

													<div class="DETAIL_SECTION">
														<h:outputLabel
															value="#{msg['paymentReceiptDetail.receiptSectionHeading']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER" width="100%">


															<tr>
																<td>
																	<h:outputLabel style="font-weight:normal;"
																		styleClass="TABLE_LABEL"
																		value="#{msg['chequeList.receiptNumber']} :"></h:outputLabel>
																</td>
																<td style="width: 30%">
																	<h:inputText style="font-weight:normal;"
																		id="txtreceiptNumber"
																		styleClass="A_LEFT INPUT_TEXT READONLY"
																		value="#{pages$paymentReceiptDetail.chequeView.receiptNumber}"
																		readonly="true">
																	</h:inputText>
																</td>
																<td>
																	<h:outputLabel style="font-weight:normal;"
																		styleClass="TABLE_LABEL"
																		value="#{msg['chequeList.receiptDate']} :"></h:outputLabel>
																</td>
																<td style="width: 30%">
																	<h:inputText id="txtReceiptDate"
																		styleClass="A_RIGHT_NUM READONLY"
																		value="#{pages$paymentReceiptDetail.receiptDate}"
																		readonly="true" style="width: 185px;">
																	</h:inputText>
																</td>

															</tr>

															<tr>
																<td>
																	<h:outputLabel style="font-weight:normal;"
																		styleClass="TABLE_LABEL"
																		value="#{msg['paymentReceiptDetail.totalPaymentSchedules']} :"></h:outputLabel>
																</td>
																<td style="width: 30%">
																	<h:inputText id="txtPaymentSchedules"
																		styleClass="A_RIGHT_NUM READONLY"
																		value="#{pages$paymentReceiptDetail.recordSize}"
																		readonly="true" style="width: 185px;">
																	</h:inputText>
																</td>
																<td>
																	<h:outputLabel style="font-weight:normal;"
																		styleClass="TABLE_LABEL"
																		value="#{msg['paymentReceiptDetail.totalAmount']} :"></h:outputLabel>
																</td>
																<td>
																	<h:inputText id="txtTotalAmount"
																		styleClass="A_RIGHT_NUM READONLY"
																		value="#{pages$paymentReceiptDetail.totalReceiptAmount}"
																		readonly="true" style="width: 185px;"></h:inputText>
																</td>

															</tr>

															<tr>
																<td>
																	<h:outputLabel style="font-weight:normal;"
																		styleClass="TABLE_LABEL"
																		value="#{msg['contract.contractNumber']} :"></h:outputLabel>
																</td>
																<td>
																	<h:inputText id="txtContractNumber"
																		styleClass="A_LEFT INPUT_TEXT READONLY"
																		value="#{pages$paymentReceiptDetail.chequeView.contractNumber}"
																		readonly="true"></h:inputText>
																</td>
																<td>
																	<h:outputLabel style="font-weight:normal;"
																		styleClass="TABLE_LABEL"
																		value="#{msg['request.number']} :"></h:outputLabel>
																</td>
																<td>
																	<h:inputText id="txtRequestNumber"
																		styleClass="A_LEFT INPUT_TEXT READONLY"
																		value="#{pages$paymentReceiptDetail.chequeView.requestView.requestNumber}"
																		readonly="true"></h:inputText>
																</td>

															</tr>
															<tr>
																<td>
																	<h:outputLabel style="font-weight:normal;"
																		styleClass="TABLE_LABEL"
																		value="#{msg['receiptDetail.CollectedBy']} :"></h:outputLabel>
																</td>
																<td>
																	<h:inputText id="txtCollectedBy"
																		styleClass="A_LEFT INPUT_TEXT READONLY"
																		value="#{pages$paymentReceiptDetail.isEnglishLocale ? pages$paymentReceiptDetail.chequeView.collectedByName:pages$paymentReceiptDetail.chequeView.collectedByNameAr}"
																		readonly="true"></h:inputText>
																</td>
																<td>
																	<h:outputLabel style="font-weight:normal;"
																		styleClass="TABLE_LABEL"
																		value="#{msg['receiptDetail.PaidBy']} :"></h:outputLabel>
																</td>
																<td>
																	<h:inputText id="txtPaidBy"
																		styleClass="A_LEFT INPUT_TEXT READONLY"
																		value="#{pages$paymentReceiptDetail.chequeView.paidByFullName}"
																		readonly="true"></h:inputText>
																</td>

															</tr>



														</table>
													</div>
												</div>

												<div style="padding: 5px; width: 98%;">
													<rich:tabPanel id="tabPanel" style="width:100%;"
														headerSpacing="0">
														<rich:tab id="detailsTab"
															label="#{msg['paymentReceiptDetail.DetailTab']}">
															<t:div style=" width: 98%;">


																<t:panelGrid id="rcptDetailHeading" columns="1">

																	<h:outputLabel
																		value="#{msg['paymentReceiptDetail.receiptDetailTable']}"
																		style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px;" />

																</t:panelGrid>

																<t:div id="conDiv2" styleClass="contentDiv"
																	style="width: 99%;">
																	<t:dataTable id="dt2"
																		value="#{pages$paymentReceiptDetail.receiptDetailList}"
																		binding="#{pages$paymentReceiptDetail.receiptDetaildataTable}"
																		rows="#{pages$paymentReceiptDetail.paginatorRows}"
																		preserveDataModel="false" preserveSort="false"
																		var="dataItem" rowClasses="row1,row2" rules="all"
																		renderedIfEmpty="true" width="100%">

																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel
																					value="#{msg['chequeList.message.paymentAmount']}" />
																			</f:facet>
																			<t:outputText styleClass="A_RIGHT_NUM"
																				value="#{dataItem.amount}"
																				style="white-space: normal;" />
																		</t:column>
																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel
																					value="#{msg['cancelContract.payment.paymentmethod']}" />
																			</f:facet>
																			<t:outputText
																				value="#{pages$paymentReceiptDetail.isEnglishLocale ? dataItem.modeOfPaymentEn : dataItem.modeOfPaymentAr}"
																				styleClass="A_LEFT" style="white-space: normal;" />
																		</t:column>
																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel
																					value="#{msg['chequeList.chequeType']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{pages$paymentReceiptDetail.isEnglishLocale ? dataItem.chequeTypeEn: dataItem.chequeTypeAr}"
																				style="white-space: normal;" />
																		</t:column>
																		 
																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel
																					value="#{msg['bouncedChequesList.chequeNumberCol']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.methodRefNo}"
																				style="white-space: normal;" />
																		</t:column>

																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel value="#{msg['commons.date']}" />
																			</f:facet>
																			<t:outputText id="GpayDate" styleClass="A_LEFT"
																				value="#{dataItem.methodRefDate}"
																				style="white-space: normal;">
																				<f:convertDateTime
																					pattern="#{pages$paymentReceiptDetail.dateFormat}"
																					timeZone="#{pages$paymentReceiptDetail.timeZone}" />
																			</t:outputText>

																		</t:column>


																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel
																					value="#{msg['feeConfiguration.accountNo']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.accountNo}"
																				style="white-space: normal;" />
																		</t:column>

																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel
																					value="#{msg['bouncedChequesList.bankNameCol']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{pages$paymentReceiptDetail.isEnglishLocale ? dataItem.bankEn : dataItem.bankAr}"
																				style="white-space: normal;" />
																		</t:column>

																	<t:column id="bouncedReason" >
																		<f:facet name="header">
																		<h:outputLabel value="#{msg['commons.label.bouncedReason']}">
																		</h:outputLabel>
																		</f:facet>
																		<h:outputLabel value="#{dataItem.bouncedReason}">
																		</h:outputLabel>
																	</t:column>
																	</t:dataTable>
																</t:div>
																
															<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																		style="width:100%;">
																		<t:panelGrid cellpadding="0" cellspacing="0"
																			width="100%" columns="2"
																			columnClasses="RECORD_NUM_TD">

																			<t:div styleClass="RECORD_NUM_BG">
																				<t:panelGrid cellpadding="0" cellspacing="0"
																					columns="3" columnClasses="RECORD_NUM_TD">

																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />

																					<h:outputText value=" : "
																						styleClass="RECORD_NUM_TD" />

																					<h:outputText
																						value="#{pages$paymentReceiptDetail.receiptRecordSize}"
																						styleClass="RECORD_NUM_TD" />

																				</t:panelGrid>
																			</t:div>

																			<t:dataScroller id="paymentReceiptScrollerId"
																				for="dt2" paginator="true" fastStep="1"
																				paginatorMaxPages="2" immediate="false"
																				paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true"
																				paginatorTableStyle="grid_paginator"
																				layout="singleTable"
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																				paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																				pageIndexVar="paymentReceiptScroller">

																				<f:facet name="first">
																					<t:graphicImage
																						url="../resources/images/first_btn.gif"
																						id="first_btn"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastrewind">
																					<t:graphicImage
																						url="../resources/images/previous_btn.gif"
																						id="previous_btn"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastforward">
																					<t:graphicImage
																						url="../resources/images/next_btn.gif"
																						id="next_btn"></t:graphicImage>
																				</f:facet>
																				<f:facet name="last">
																					<t:graphicImage
																						url="../resources/images/last_btn.gif"
																						id="last_btn"></t:graphicImage>
																				</f:facet>
																				<t:div styleClass="PAGE_NUM_BG">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.paymentReceiptScroller}" />
																				</t:div>
																			</t:dataScroller>
																		</t:panelGrid>
																	</t:div>
																<t:panelGrid>
																	<h:outputLabel
																		value="#{msg['paymentReceiptDetail.payScheduleTable']}"
																		style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px;" />
																</t:panelGrid>

																<t:div id="conDiv" styleClass="contentDiv"
																	style="width: 99%;">
																	<t:dataTable id="dt1"
																		value="#{pages$paymentReceiptDetail.psList}"
																		binding="#{pages$paymentReceiptDetail.dataTable}"
																		rows="#{pages$paymentReceiptDetail.paginatorRows}"
																		preserveDataModel="false" preserveSort="false"
																		var="dataItem" rowClasses="row1,row2" rules="all"
																		renderedIfEmpty="true" width="100%">

																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel
																					value="#{msg['bouncedChequesList.paymentNumberCol']}" />
																			</f:facet>
																			<t:outputText value="#{dataItem.paymentNumber}"
																				styleClass="A_LEFT" style="white-space: normal;" />
																		</t:column>

																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel
																					value="#{msg['cancelContract.payment.paymenttype']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{pages$paymentReceiptDetail.isEnglishLocale ? dataItem.typeEn : dataItem.typeAr}"
																				style="white-space: normal;" />
																		</t:column>


																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel
																					value="#{msg['cancelContract.payment.paymentdate']}" />
																			</f:facet>
																			<t:outputText id="GpayDate" styleClass="A_LEFT"
																				value="#{dataItem.paymentDate}"
																				style="white-space: normal;">
																				<f:convertDateTime
																					pattern="#{pages$paymentReceiptDetail.dateFormat}"
																					timeZone="#{pages$paymentReceiptDetail.timeZone}" />
																			</t:outputText>

																		</t:column>

																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel
																					value="#{msg['cancelContract.payment.paymentmethod']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{pages$paymentReceiptDetail.isEnglishLocale ? dataItem.paymentModeEn : dataItem.paymentModeAr }"
																				style="white-space: normal;" />
																		</t:column>
																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel
																					value="#{msg['bouncedChequesList.chequeNumberCol']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.chequeNumber}"
																				style="white-space: normal;" />
																		</t:column>



																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel
																					value="#{msg['chequeList.message.paymentAmount']}" />
																			</f:facet>
																			<t:outputText styleClass="A_RIGHT_NUM"
																				value="#{dataItem.amount}"
																				style="white-space: normal;" />
																		</t:column>

																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel
																					value="#{msg['chequeList.message.paymentStatus']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{pages$paymentReceiptDetail.isEnglishLocale ? dataItem.statusEn : dataItem.statusAr}"
																				style="white-space: normal;" />
																		</t:column>

																		<t:column sortable="true">
																			<f:facet name="header">
																				<t:outputLabel value="#{msg['commons.description']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.description}"
																				style="white-space: normal;" />
																		</t:column>

																	</t:dataTable>
																</t:div>
																	<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																		style="width:100%;">
																		<t:panelGrid cellpadding="0" cellspacing="0"
																			width="100%" columns="2"
																			columnClasses="RECORD_NUM_TD">

																			<t:div styleClass="RECORD_NUM_BG">
																				<t:panelGrid cellpadding="0" cellspacing="0"
																					columns="3" columnClasses="RECORD_NUM_TD">

																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />

																					<h:outputText value=" : "
																						styleClass="RECORD_NUM_TD" />

																					<h:outputText
																						value="#{pages$paymentReceiptDetail.recordSize}"
																						styleClass="RECORD_NUM_TD" />

																				</t:panelGrid>
																			</t:div>

																			<t:dataScroller id="paymentScheduleScrollerId"
																				for="dt1" paginator="true" fastStep="1"
																				paginatorMaxPages="2" immediate="false"
																				paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true"
																				paginatorTableStyle="grid_paginator"
																				layout="singleTable"
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																				paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																				pageIndexVar="paymentScheduleScroller">

																				<f:facet name="first">
																					<t:graphicImage
																						url="../resources/images/first_btn.gif"
																						id="lblfirst_btn"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastrewind">
																					<t:graphicImage
																						url="../resources/images/previous_btn.gif"
																						id="lblprevious_btn"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastforward">
																					<t:graphicImage
																						url="../resources/images/next_btn.gif"
																						id="lblnext_btn"></t:graphicImage>
																				</f:facet>
																				<f:facet name="last">
																					<t:graphicImage
																						url="../resources/images/last_btn.gif"
																						id="lbllast_btn"></t:graphicImage>
																				</f:facet>
																				<t:div styleClass="PAGE_NUM_BG">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.paymentScheduleScroller}" />
																				</t:div>
																			</t:dataScroller>
																		</t:panelGrid>
																	</t:div>

															</t:div>





														</rich:tab>
														<!-- Attachment Tab - Start -->
														<rich:tab id="attachmentTab"
															label="#{msg['commons.attachmentTabHeading']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<!-- Attachment Tab - Finish -->
														<!-- Request History Tab - Start -->
														<rich:tab label="#{msg['commons.tab.requestHistory']}"
															action="#{pages$paymentReceiptDetail.tabRequestHistory_Click}">
															<%@ include file="../pages/requestTasks.jsp"%>
														</rich:tab>
														<!-- Request History Tab - End -->


													</rich:tabPanel>

												</div>































												<div id="divButton" class="TAB_PANEL_MARGIN"
													style="padding-top: 7px;">
													<table width="100%" border="0">
														<tr>
															<td colspan="6" class="BUTTON_TD">

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.back']}"
																	action="#{pages$paymentReceiptDetail.btnBack_Click}">
																</h:commandButton>

															</td>
														</tr>
													</table>
												</div>

											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>

			</div>


		</body>
</f:view>
</html>
