<%-- 
  - Author: Afzal Zulfiqar
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Auction
  --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
		
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
		<title>PIMS</title>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>		

			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			
	</head>

	<body>
		
			
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<c:choose>
			<c:when test="${sessionScope.CurrentLocale.languageCode eq 'en'}">
				 <td background ="../resources/images/Grey panel/Grey-Panel-left-1.jpg" height="38" style="background-repeat:no-repeat;" width="100%">
				 <font
					style="font-family: Trebuchet MS; font-weight: regular; font-size: 19px; margin-left: 15px; top: 30px;">
					<h:outputLabel value="#{msg['blacklistSearch.header']}"></h:outputLabel>
				</font>
                <!--<IMG src="../resources/images/Grey panel/Grey-Panel-left-1.jpg"/>-->
			</c:when>
			<c:otherwise>
				<td background ="../resources/images/ar/Detail/Grey Panel/Grey-Panel-right-1.jpg" height="38" style="background-repeat:no-repeat;" width="100%">
				<font
					style="font-family: Trebuchet MS; font-weight: regular; font-size: 19px; margin-left: 15px; top: 30px;">
					<h:outputLabel value="#{msg['blacklistSearch.header']}"></h:outputLabel>
				</font>
  
			</c:otherwise>
		</c:choose>
						</td>	
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" 
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="99%" valign="top" nowrap="nowrap">
									<div style="height:450;overflow:auto;" >
									<h:form  id="searchFrm">
									
									
			
			
										<table width="100%" border="0">
											<tr>
												<td colspan="7">
													<h:messages>
														<h:outputText value=""
															escape="false" />
													</h:messages>
													<h:outputText  escape="false" value="#{pages$ViolationDetails.errorMessages}"/>
													
													
												</td>
											</tr>
											<tr>
												<td colspan="1" align="right" style="width: 62px">
													<h:outputLabel value="#{msg['contract.tenantName']}:"></h:outputLabel>
												</td>

												<td colspan="1" width="10%">

													<h:inputText id="unitReferenceNo" 
														value="#{pages$BlacklistSearch.tenantFirstName}"
														style="width: 117px; height: 16px" maxlength="50"></h:inputText>

												</td>
												<td colspan="1" align="right" width="37%">
													<h:outputLabel value="#{msg['blacklistSearch.contractorName']}:"></h:outputLabel>
												</td>
												<td colspan="1" align="left" width="30%">
													<h:inputText id="unitType" 
														value="#{pages$BlacklistSearch.contractorFirstName}"
														style="width: 117px; height: 16px" maxlength="50"></h:inputText>
												</td>
											</tr>
											
											
											<tr>
												<td colspan="1" align="right">
													<h:outputLabel value="#{msg['blacklistSearch.addedBy']}:"></h:outputLabel>
												</td>
												<td colspan="1" width="10%" style="width: 219px">
													<h:inputText id="propertyName" 
														value="#{pages$BlacklistSearch.addedBy}"
														style="width: 117px; height: 16px" maxlength="50" ></h:inputText>
												</td>
												
												<tr>
												<td colspan="3" align="right">
												<br><br>
												<h:commandButton styleClass="BUTTON" value="#{msg['commons.search']}" action="#{pages$BlacklistSearch.search}" style="width: 100px"/>
												</td>
												</tr>
												
										</table>
										
						<br><br>
								
										
										<!-- Panel group is here -->
										
										<br>
										<br><br>
										<div class="imag">&nbsp;</div>	
										<div class="contentDiv">
											<t:dataTable id="dt1" rows="20" preserveDataModel="true"
														preserveSort="true" var="dataItem" rules="all"
														renderedIfEmpty="true" width="100%"
														value="#{pages$BlacklistSearch.blaclistViews}"
														binding="#{pages$BlacklistSearch.dataTable}">

												<t:column id="col1" width="100" sortable="true">
													<f:facet  name="header">
														<t:outputText value="#{msg['blacklistSearch.contractor/Teneant']}" />
													</f:facet>
													<t:outputText value="#{dataItem.contractorName}" />
													<t:outputText value=" #{dataItem.tenantName}" />
												</t:column>
												<t:column id="col2" width="85" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['blackList.reason']}" />
													</f:facet>
													<t:outputText value="#{dataItem.lastReason}" />
													
												</t:column>
												<t:column id="col3" width="135" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['blackList.remarks']}" />
													</f:facet>
													<t:outputText value="#{dataItem.remarks}" />
													
													
												</t:column>
												<t:column id="col4" width="85" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['blacklistSearch.addedBy']}" />
													</f:facet>
													<t:outputText value="#{dataItem.lastAddedBy}" />
													
													
												</t:column>
												
													<t:column id="col5" width="85" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['blackList.removedBy']}" />
													</f:facet>
													<t:outputText value="#{dataItem.removedBy}" />
													
													
												</t:column>
												
												<t:column id="col10" sortable="false" width="30">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.remove']}" />
													</f:facet>
													
													
													
													<h:commandLink action="#{pages$BlacklistSearch.delete}" >
													<h:graphicImage url="../resources/images/delete_icon.png" />
													</h:commandLink>
													
													
													
													
												</t:column>
											</t:dataTable>
											</div>
											<div class="contentDivFooter">

												<t:dataScroller id="scroller" for="dt1" paginator="true"
													fastStep="1" paginatorMaxPages="2" immediate="false"
													styleClass="scroller" paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 												paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

													<f:facet name="first">
														
														<t:graphicImage url="../resources/images/first_btn.gif" id="lblF"></t:graphicImage>
													</f:facet>

													<f:facet name="fastrewind">
														<t:graphicImage url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>
													</f:facet>

													<f:facet name="fastforward">
														<t:graphicImage url="../resources/images/next_btn.gif" id="lblFF"></t:graphicImage>
													</f:facet>

													<f:facet name="last">
														<t:graphicImage url="../resources/images/last_btn.gif" id="lblL"></t:graphicImage>
													</f:facet>

												</t:dataScroller>
									
                                           </div>
										<br>
										
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</f:view>
	</body>
</html>