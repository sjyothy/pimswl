<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function closePopupAndRefreshOpener() 
	{
		window.opener.document.forms[0].submit();
		window.close();
	}	 
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>					
					<td width="100%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['returnedChequeFollowUp.Heading']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						 </table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" >
										<h:form id="frm" style="width:97%" enctype="multipart/form-data">
											<table border="0" width="80%" class="layoutTable">
												<tr>
													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$addFollowUpProgress.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$addFollowUpProgress.successMessages}" />
													</td>
												</tr>
											</table>
											<table border="0" width="80%" class="layoutTable">
												<tr>
													<td width="5%">&nbsp;</td>
													<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['commons.date']}:" />
													</td>
													<td>
														<rich:calendar  
																   value="#{pages$addFollowUpProgress.followUpProgressView.followUpProgressDate}"
																   datePattern="#{pages$addFollowUpProgress.dateFormat}"
																   locale="#{pages$addFollowUpProgress.locale}" 
																   popup="true"
																   showApplyButton="false" 
																   enableManualInput="false"
																   todayControlMode="hidden">
														</rich:calendar>
													</td>
													<td width="5%">&nbsp;</td>
													<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['application.status.gridHeader']}:" />
													</td>
													<td>
														<h:inputText styleClass="READONLY" rendered="#{!pages$addFollowUpProgress.isFollowUpProgressAction}"
																	 readonly="true"																	 
																	 value="#{pages$addFollowUpProgress.isEnglishLocale ? pages$addFollowUpProgress.followUpProgressView.statusEn : pages$addFollowUpProgress.followUpProgressView.statusAr}" />
													   <h:selectOneMenu id = "cmbStatus" rendered="#{pages$addFollowUpProgress.isFollowUpProgressAction}" readonly="true" value="#{pages$addFollowUpProgress.followUpProgressView.statusId}" 
																		 styleClass="SELECT_MENU">
															<f:selectItem itemValue="-1" itemLabel="--" />
															<f:selectItems value="#{pages$ApplicationBean.allFollowUpActionsStatus}" />
														</h:selectOneMenu>
													
													</td>
													<td width="5%">&nbsp;</td>
												</tr>
												
												<tr>
													<td width="5%">&nbsp;</td>
													<td>
														<h:outputLabel rendered="#{! pages$addFollowUpProgress.isFollowUpProgressAction}" styleClass="LABEL" value="#{msg['returnedChequeFollowUp.courtResultsTab.grid.courtResults']}:" />
														<h:outputLabel rendered="#{pages$addFollowUpProgress.isFollowUpProgressAction}" styleClass="LABEL" value="#{msg['returnedChequeFollowUp.actionsHistoryTab.grid.actionType']}:" />														
													</td>
													<td>
														<h:inputTextarea rendered="#{! pages$addFollowUpProgress.isFollowUpProgressAction}" value="#{pages$addFollowUpProgress.followUpProgressView.courtResult}" />
														<h:selectOneMenu rendered="#{pages$addFollowUpProgress.isFollowUpProgressAction}" value="#{pages$addFollowUpProgress.followUpProgressView.followUpActionIdString}" 
																		 styleClass="SELECT_MENU">
															<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
															<f:selectItems value="#{pages$ApplicationBean.allFollowUpActions}" />
															<a4j:support action="#{pages$addFollowUpProgress.cmbFollowUpActions_Changed}" reRender="cmbStatus" event="onchange"></a4j:support>
														</h:selectOneMenu>
													</td>
													<td width="5%">&nbsp;</td>
													<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['commons.commentsTabHeading']}:" />
													</td>
													<td>
														<h:inputTextarea value="#{pages$addFollowUpProgress.followUpProgressView.comments}" />
													</td>
													<td width="5%">&nbsp;</td>
												</tr>
												
												<tr>
													<td width="5%">&nbsp;</td>
													<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['commons.reminder.date']}:" rendered="#{pages$addFollowUpProgress.isFollowUpProgressAction}" />
													</td>
													<td>
														<rich:calendar  
																   value="#{pages$addFollowUpProgress.followUpProgressView.reminderScheduled}"
																   datePattern="#{pages$addFollowUpProgress.dateFormat}"
																   locale="#{pages$addFollowUpProgress.locale}" 
																   popup="true"
																   showApplyButton="false" 
																   enableManualInput="false"
																   todayControlMode="hidden"
																   rendered="#{pages$addFollowUpProgress.isFollowUpProgressAction}">
														</rich:calendar>
													</td>
													<td width="5%">&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td width="5%">&nbsp;</td>
												</tr>
												
												<tr>
													<td class="BUTTON_TD" colspan="6" width="95%">
														<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$addFollowUpProgress.btnSave_Click}">
														</h:commandButton>
														<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	onclick="window.close();">
														</h:commandButton>																												
													</td>
													<td width="5%">&nbsp;</td>
												</tr>
												
											</table>
									    </h:form>										
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>				
			</table>
           </div>
		</body>
	</html>
</f:view>
