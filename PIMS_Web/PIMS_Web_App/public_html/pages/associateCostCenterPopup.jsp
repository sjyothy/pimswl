<%-- 
  - Author:Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - 
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	function disableAddControl( cntrl )
	{
		  
		  document.getElementById('frm:btnAdd').disabled = "true";
		  document.getElementById('frm:addLink').onclick();
	}
	            
	function onChkChange()
    {
      document.getElementById('frm:disablingDiv').style.display='none';
    }
    function onSelectionChanged(changedChkBox)
	{
	    document.getElementById('frm:disablingDiv').style.display='block';
		document.getElementById('frm:lnkUnitSelectionChanged').onclick();
	}
	</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">


			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">




				<tr width="100%">


					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel
										value="#{msg['associateCostCenter.lbl.heading']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>
									<h:form id="frm" enctype="multipart/form-data">
										<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$associateCostCenterPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$associateCostCenterPopup.successMessages}"
																escape="false" styleClass="INFO_FONT" />
															<h:messages></h:messages>

															<h:commandLink id="addLink"
																action="#{pages$associateCostCenterPopup.onAdd}" />

															<a4j:commandLink id="lnkUnitSelectionChanged"
																onbeforedomupdate="javascript:onChkChange();"
																action="#{pages$associateCostCenterPopup.onChangeFileCombo}"
																reRender="costCenter" />




														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width: 100%">
													<h:outputLabel value="" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table id="tableForm" cellpadding="1px" width="100%"
														cellspacing="2px" class="DETAIL_SECTION_INNER" columns="4">

														<tr>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['associateCostCenter.lbl.personName']} :"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="personName" readonly="true"
																	styleClass="INPUT_TEXT READONLY"
																	value="#{pages$associateCostCenterPopup.disbursementRequestBeneficiary.name}"
																	style="width:190px;">
																</h:inputText>

															</td>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['associateCostCenter.lbl.amountToDisburse']} :"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="amountToDisburse" readonly="true"
																	styleClass="INPUT_TEXT READONLY"
																	value="#{pages$associateCostCenterPopup.disbursementDetails.amount}"
																	style="width:190px;">
																</h:inputText>


															</td>


														</tr>

														<tr>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['associateCostCenter.lbl.file']} :"></h:outputLabel>
															</td>
															<td>
																<h:selectOneMenu id="cmbfileNumber"
																	value="#{pages$associateCostCenterPopup.data.inheritanceFileId}"
																	tabindex="3" onchange="onSelectionChanged(this);">
																	<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																		itemValue="-1" />

																	<f:selectItems
																		value="#{pages$associateCostCenterPopup.itemList}" />


																</h:selectOneMenu>


															</td>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['associateCostCenter.lbl.costCenter']} :"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="costCenter" readonly="true"
																	styleClass="INPUT_TEXT READONLY"
																	value="#{pages$associateCostCenterPopup.data.costCenter}"
																	style="width:190px;">
																</h:inputText>


															</td>


														</tr>
														<tr>
															<td>
																<h:outputLabel style="font-weight:normal;"
																	styleClass="TABLE_LABEL"
																	value="#{msg['associateCostCenter.lbl.amount']} :"></h:outputLabel>
															</td>
															<td>
																<h:inputText id="amount"
																	value="#{pages$associateCostCenterPopup.data.amount}"
																	style="width:190px;">
																</h:inputText>


															</td>


														</tr>


													</table>
												</div>
												<t:div>&nbsp;</t:div>
												<table class="BUTTON_TD" cellpadding="1px" width="100%"
													cellspacing="3px">
													<tr>
														<td class="BUTTON_TD" colspan="10">
															<h:commandButton id="btnAdd" styleClass="BUTTON"
																value="#{msg['commons.Add']}"
																binding="#{pages$associateCostCenterPopup.btnAdd}"
																onclick="disableAddControl();" style="width: 135px">
															</h:commandButton>

															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.done']}"
																binding="#{pages$associateCostCenterPopup.btnDone}"
																action="#{pages$associateCostCenterPopup.onDone}"
																style="width: 135px">
															</h:commandButton>
														</td>
													</tr>
												</table>


												<br></br>
												<table id="imageTable" cellpadding="0" cellspacing="0"
													width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG id="image1"
																src="../<%=ResourceUtil.getInstance().getPathProperty(
									"img_section_left")%>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td style="FONT-SIZE: 0px" width="100%">
															<IMG id="image2"
																src="../<%=ResourceUtil.getInstance().getPathProperty(
									"img_section_mid")%>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG id="image3"
																src="../<%=ResourceUtil.getInstance().getPathProperty(
									"img_section_right")%>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="contentDiv" style="width: 99%">
													<t:dataTable id="test2"
														value="#{pages$associateCostCenterPopup.dataList}"
														binding="#{pages$associateCostCenterPopup.dataTable}"
														width="100%"
														rows="#{pages$associateCostCenterPopup.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true">



														<t:column id="costCenter" width="10%" defaultSorted="true"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText
																	value="#{msg['associateCostCenter.lbl.costCenter']}" />

															</f:facet>
															<t:outputText
																rendered="#{dataItem.deleted == 'false' && dataItem.editing == 'false'}"
																value="#{dataItem.costCenter}"
																style="white-space: normal;" />

														</t:column>

														<t:column id="fileName" width="10%" sortable="false"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText
																	value="#{msg['associateCostCenter.lbl.file']}" />
															</f:facet>
															<h:commandLink
																rendered="#{dataItem.deleted == 'false' && dataItem.editing == 'false'}"
																action="#{pages$associateCostCenterPopup.onOpenFileFromDataTable}"
																value="#{dataItem.fileNumber}"
																style="white-space: normal;" styleClass="A_LEFT" />

														</t:column>

														<t:column id="fileStatus" width="10%" sortable="false"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText
																	value="#{msg['associateCostCenter.lbl.fileStatus']}" />
															</f:facet>
															<t:outputText
																rendered="#{dataItem.deleted == 'false' && dataItem.editing == 'false'}"
																value="#{pages$associateCostCenterPopup.englishLocale? dataItem.fileStatusEn:dataItem.fileStatusAr}"
																style="white-space: normal;" styleClass="A_LEFT" />

														</t:column>


														<t:column id="actionGrid" width="5%"
															style="white-space: normal;" sortable="false">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>
															<t:commandLink
																rendered="#{dataItem.deleted == 'false' && dataItem.editing == 'false'}"
																action="#{pages$associateCostCenterPopup.onEdit}">
																<h:graphicImage id="editIcon"
																	title="#{msg['commons.edit']}"
																	url="../resources/images/edit-icon.gif" />
															</t:commandLink>

															<t:commandLink
																rendered="#{dataItem.deleted == 'false' && dataItem.editing == 'false'}"
																onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;"
																action="#{pages$associateCostCenterPopup.onDelete}">
																<h:graphicImage id="deleteIcon"
																	title="#{msg['commons.delete']}"
																	url="../resources/images/delete_icon.png" />
															</t:commandLink>


														</t:column>





													</t:dataTable>




												</div>

											</div>
									</h:form>

								</td>
							</tr>


						</table>

						</div>

						</div>



						</div>




					</td>
				</tr>
			</table>
			</td>
			</tr>
			</table>



		</body>
	</html>
</f:view>