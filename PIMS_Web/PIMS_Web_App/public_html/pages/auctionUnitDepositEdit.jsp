<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Editing an Auction Unit Deposit Amount
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		function closeWindowSubmit()
		{
			window.opener.document.forms[0].submit();
		  	window.close();	  
		}
		function windowSubmit()
		{
			document.getElementById('detailsFrm').submit();
		}
</script>
<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
		 <title>PIMS</title>
		 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
		 <meta http-equiv="pragma" content="no-cache">
		 <meta http-equiv="cache-control" content="no-cache">
		 <meta http-equiv="expires" content="0">
		 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
		 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 <script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
	</head>

	<body class="BODY_STYLE">
			<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
			%>
	
				<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="100%" valign="top" class="divBackgroundBody">
						<table width="99.1%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['auctionUnit.applyOpeningPriceHeading']}"
												styleClass="HEADER_FONT" rendered="#{pages$auctionUnitDepositEdit.isOPeningPriceMode}"/>
										<h:outputLabel value="#{msg['auctionUnit.applyDepositHeading']}"
												styleClass="HEADER_FONT" rendered="#{!pages$auctionUnitDepositEdit.isOPeningPriceMode}"/>
									</td>
								</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" 
							cellspacing="0" border="0">
								<tr valign="top">
								<td width="100%" height="100%" valign="top" >
								<div class="SCROLLABLE_SECTION" style="display:block;height:700px;width:100%">
										<h:form id="detailsFrm" style="width:88%">
										<div style="height:35px; padding-left:9px;">
										<table border="0" class="layoutTable">
											<tr>
												<td>
													<h:outputText value="#{pages$auctionUnitDepositEdit.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
												</td>
											</tr>
										</table>
										</div>
										<t:panelGrid rendered="false">
										<t:div style="width:100%;padding-left:10px;">
										<t:div styleClass="contentDiv" style="width:98.2%; height:200px;">
											<t:dataTable id="dt1"
												value="#{pages$auctionUnitDepositEdit.dataList}"
												binding="#{pages$auctionUnitDepositEdit.dataTable}"
												rows="5"
												preserveDataModel="false" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
												<t:column id="propertyNumberCol" width="100" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['property.propertyNumberCol']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.propertyNumber}" />
												</t:column>
												<t:column id="propertyCommercialNameCol" width="100" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['property.commercialName']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.propertyCommercialName}" style="white-space: normal;"/>
												</t:column>
												<t:column id="propertyEndowedNameCol" width="120" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['property.endowedName']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.propertyEndowedName}" style="white-space: normal;"/>
												</t:column>
												<t:column id="landNumberCol" width="80" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['property.landNumber']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.landNumber}" style="white-space: normal;"/>
												</t:column>
												<t:column id="emirateEnCol" width="100" sortable="true" rendered="#{pages$auctionUnitDepositEdit2.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.Emirate']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText value="#{dataItem.emirateEn}" styleClass="A_LEFT" style="white-space: normal;"/>
												</t:column>
												<t:column id="emirateArCol" width="100" sortable="true" rendered="#{pages$auctionUnitDepositEdit2.isArabicLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.Emirate']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText value="#{dataItem.emirateAr}" styleClass="A_LEFT" style="white-space: normal;"/>
												</t:column>
												<t:column id="unitNumberCol" width="60" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['unit.numberCol']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.unitNumber}" style="white-space: normal;"/>
												</t:column>
												<t:column id="unitAreaCol" width="80" sortable="true" rendered="#{pages$auctionUnitDepositEdit2.advertisementMode}">
													<f:facet name="header">
														<t:outputText value="#{msg['unit.unitArea']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText value="#{dataItem.area}" styleClass="A_RIGHT_NUM" style="white-space: normal;"/>
												</t:column>
												<t:column id="unitTypeEnCol" width="60" sortable="true" rendered="#{pages$auctionUnitDepositEdit2.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['unit.type']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.unitTypeEn}" style="white-space: normal;"/>
												</t:column>
												<t:column id="unitTypeArCol" width="60" sortable="true" rendered="#{pages$auctionUnitDepositEdit2.isArabicLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['unit.type']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.unitTypeAr}" style="white-space: normal;"/>
												</t:column>
												<t:column id="unitUsageEnCol" width="80" sortable="true" rendered="#{pages$auctionUnitDepositEdit2.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['inquiry.unitUsage']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.unitUsageTypeEn}" style="white-space: normal;"/>
												</t:column>
												<t:column id="unitUsageTypeArCol" width="80" sortable="true" rendered="#{pages$auctionUnitDepositEdit2.isArabicLocale}">
													<f:facet name="header">
														<t:outputText value="#{msg['inquiry.unitUsage']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.unitUsageTypeAr}" style="white-space: normal;"/>
												</t:column>
												<t:column id="rentValueCol" width="70" sortable="true">
													<f:facet name="header"  >
														<t:outputText value="#{msg['unit.rentValueCol']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText value="#{dataItem.rentValue}"  styleClass="A_RIGHT_NUM" style="white-space: normal;">
														<f:convertNumber pattern="#{pages$auctionUnitDepositEdit2.numberFormat}"/>
													</t:outputText>
												</t:column>
												<t:column id="openingPriceCol" width="80" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['auctionUnit.openingPrice']}" style="white-space: normal;"/>
													</f:facet>
													<t:outputText value="#{dataItem.openingPrice}" styleClass="A_RIGHT_NUM" style="white-space: normal;">
														<f:convertNumber pattern="#{pages$auctionUnitDepositEdit2.numberFormat}"/>
													</t:outputText>
												</t:column>
												<t:column id="depositAmountCol" width="70" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['auctionUnit.depositColumn']}" style="white-space: normal;"/>
													<%-- 	<t:outputText value="#{msg['unit.depositAmount']}" />  --%>
													</f:facet>
													<t:outputText value="#{dataItem.depositAmount}" styleClass="A_RIGHT_NUM" style="white-space: normal;">
														<f:convertNumber pattern="#{pages$auctionUnitDepositEdit2.numberFormat}"/>
													</t:outputText>
												</t:column>
											</t:dataTable>
										</t:div>
										</t:div>
										</t:panelGrid>
									<div class="MARGIN" style="padding-top:10px;"> 
									<div style="width:680px;">
										
										<table cellpadding="1px" cellspacing="2px" style="width:680px;">
											<tr>
													<td style="PADDING: 5px; PADDING-TOP: 5px;">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['auctionUnit.openingPriceMinAmount']}"
																	rendered="#{pages$auctionUnitDepositEdit.isOPeningPriceMode}"></h:outputLabel>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['auctionUnit.depositMinAmount']}"
																	rendered="#{!pages$auctionUnitDepositEdit.isOPeningPriceMode}"></h:outputLabel>
													</td>
													<td style="PADDING: 5px; PADDING-TOP: 5px;">
													<h:inputText id="minDeposit" maxlength="20" binding="#{pages$auctionUnitDepositEdit.minDepositField}"  styleClass="#{pages$auctionUnitDepositEdit.readonlyStyleClassPerc}">
													</h:inputText>
												</td>
													<td style="PADDING: 5px; PADDING-TOP: 5px;">
															<h:outputLabel styleClass="LABEL"
																value="#{msg['auctionUnit.openingPriceMaxAmount']}"
																rendered="#{pages$auctionUnitDepositEdit.isOPeningPriceMode}"></h:outputLabel>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['auctionUnit.depositMaxAmount']}"
																rendered="#{!pages$auctionUnitDepositEdit.isOPeningPriceMode}"></h:outputLabel>
													</td>
													<td style="PADDING: 5px; PADDING-TOP: 5px;">
														<h:inputText id="maxDeposit" maxlength="20" binding="#{pages$auctionUnitDepositEdit.maxDepositField}"  styleClass="#{pages$auctionUnitDepositEdit.readonlyStyleClassPerc}">
														</h:inputText>
													</td>
											</tr>
											<tr>
												<td style="PADDING: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['auctionUnit.openingPriceType']}"
															rendered="#{pages$auctionUnitDepositEdit.isOPeningPriceMode}"></h:outputLabel>
													<h:outputLabel styleClass="LABEL" value="#{msg['auctionUnit.depositType']}"
															rendered="#{!pages$auctionUnitDepositEdit.isOPeningPriceMode}"></h:outputLabel>
												</td>
												<td colspan="1" style="PADDING: 5px; PADDING-TOP: 25px;" >
													  <h:selectOneRadio id="depositTypeRadio" layout="pageDirection" binding="#{pages$auctionUnitDepositEdit.depositTypeRadio}" value="#{pages$auctionUnitDepositEdit.selectedDepositType}" valueChangeListener="#{pages$auctionUnitDepositEdit.radioValueChanged}" onclick="javascript:windowSubmit();">
													  	<f:selectItems value="#{pages$auctionUnitDepositEdit.depositTypeList}"/>
													  </h:selectOneRadio>
												</td>
												<td colspan="1" style="PADDING-TOP: 25px;">
													<table>
														<tr>
															<td>
																<h:inputText maxlength="20" value="#{pages$auctionUnitDepositEdit.fixedValue}" binding="#{pages$auctionUnitDepositEdit.fixedValueField}" tabindex="1"  styleClass="#{pages$auctionUnitDepositEdit.readonlyStyleClassFixed}">
																</h:inputText>
															</td>
														</tr>
														<tr>
															<td>
																<h:inputText maxlength="20" value="#{pages$auctionUnitDepositEdit.percentage}" binding="#{pages$auctionUnitDepositEdit.percentageField}" tabindex="2" styleClass="#{pages$auctionUnitDepositEdit.readonlyStyleClassPerc}"></h:inputText>
															</td>
															<td>
																<h:outputLabel value="%" styleClass="LABEL"></h:outputLabel>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<h:outputLabel styleClass="LABEL" value=""></h:outputLabel>
												</td>
												<td>
													<h:outputLabel styleClass="LABEL RADIO_SUB" value="#{msg['commons.from']}"></h:outputLabel>
												</td>
												<td>
													<h:selectOneMenu id="basedOnCombo" styleClass="#{pages$auctionUnitDepositEdit.readonlyStyleClassPerc}"
														style="width: 140px; height: 24px" required="false" onchange="javascript:windowSubmit();" 
														value="#{pages$auctionUnitDepositEdit.selectedBasedOn}" valueChangeListener="#{pages$auctionUnitDepositEdit.comboValueChanged}"
														binding="#{pages$auctionUnitDepositEdit.basedOnCombo}">
														<f:selectItem itemValue="none" itemLabel="#{msg['commons.select']}" />
														<f:selectItem itemValue="BASED_ON_ANNUAL_RENT" itemLabel="#{msg['auctionUnit.annualRent']}" />

																	<c:choose>
																		<c:when test="${!pages$auctionUnitDepositEdit.isOPeningPriceMode}">
																			<f:selectItem itemValue="BASED ON OPENING AMOUNT" itemLabel="#{msg['auctionUnit.openingPrice']}" />
																		</c:when>
																	</c:choose>
																	
														
													</h:selectOneMenu>
												</td>
											</tr>
											<tr>
												<td>
													<h:outputLabel rendered="false" styleClass="LABEL" value=""></h:outputLabel>
												</td>
												<td>
													<h:outputLabel rendered="false" styleClass="LABEL RADIO_SUB" value="#{msg['commons.amount']}" ></h:outputLabel>
												</td>
												<td>
													<h:inputText maxlength="20" value="#{pages$auctionUnitDepositEdit.basedOnAmount}" binding="#{pages$auctionUnitDepositEdit.basedOnAmountField}" readonly="true" styleClass="A_RIGHT_NUM">
													</h:inputText>
												</td>
											</tr>
											<tr>
												<td>
													<h:outputLabel rendered="false" styleClass="LABEL" value=""></h:outputLabel>
												</td>
												<td>
													<h:outputLabel rendered="false" styleClass="LABEL RADIO_SUB" value="#{msg['auctionUnit.depositAmount']}" ></h:outputLabel>
												</td>
												<td>
													<h:inputText maxlength="20" value="#{pages$auctionUnitDepositEdit.depositAmount}" binding="#{pages$auctionUnitDepositEdit.depositAmountField}" readonly="true" styleClass="A_RIGHT_NUM">
													</h:inputText>
												</td>
											</tr>
											<tr>
												<td class="BUTTON_TD" colspan="4">
													<table>
													<tr>
													<td>
															<pims:security screen="Pims.AuctionManagement.PrepareAuction.AuctionUnitAction" action="create">
																<h:commandButton styleClass="BUTTON" value="#{msg['commons.setValue']}"
																	action="#{pages$auctionUnitDepositEdit.saveAuctionUnitPrices}"
																	/>
															</pims:security>
														</td>
														<td>
															<h:commandButton styleClass="BUTTON"  value="#{msg['commons.cancel']}"
																actionListener="#{pages$auctionUnitDepositEdit.cancel}"
																/>
														</td>
														
													</tr>
													</table>
												</td>
											</tr>
											</table>
											</div>
											</div>
										</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
        		</td>
		    </tr>
		</table>
	</body>
</html>
</f:view>
