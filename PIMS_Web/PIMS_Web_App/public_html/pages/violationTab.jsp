

					<h:panelGrid columns="2" columnClasses="presidentDiscussionColumn"
   						rendered="#{pages$RenewContractBackingBean.currentViolation}" >

											<t:dataTable id="ViolationTable"
													value="#{pages$RenewContractBackingBean.violations}"
													rows="20"
													preserveDataModel="true" preserveSort="true" var="dataItem"
													rules="all" renderedIfEmpty="true" width="80%" align="center">
						
													<t:column id="colViolationNameEn" sortable="true" rendered="#{pages$RenewContractBackingBean.isEnglishLocale}">
														<f:facet name="header">
															<t:outputText value="#{msg['renewContract.name']}" />
														</f:facet>
														<t:outputText value="#{dataItem.nameEn}"/>
													</t:column>
													
													<t:column id="colViolationNameAr" sortable="true" rendered="#{pages$RenewContractBackingBean.isArabicLocale}">
														<f:facet name="header">
															<t:outputText value="#{msg['renewContract.name']}" />
														</f:facet>
														<t:outputText value="#{dataItem.nameAr}"/>
													</t:column>
													
													<t:column id="colViolationDesc" sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['renewContract.description']}" />
														</f:facet>
														<t:outputText value="#{dataItem.description}"/>
													</t:column>
													
												
												</t:dataTable>

	<t:dataScroller id="violationScroller" for="ViolationTable" paginator="true" fastStep="1"
		paginatorMaxPages="2" immediate="true" styleClass="scroller"
		paginatorTableClass="paginator" renderFacetsIfSinglePage="false"
		paginatorTableStyle="grid_paginator" layout="singleTable"
		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
		paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

		<f:facet name="first">

			<t:outputLabel value="First" id="lblFV" />

		</f:facet>

		<f:facet name="last">

			<t:outputLabel value="Last" id="lblLV" />

		</f:facet>

		<f:facet name="fastforward">

			<t:outputLabel value="FFwd" id="lblFFV" />

		</f:facet>

		<f:facet name="fastrewind">

			<t:outputLabel value="FRev" id="lblFRV" />

		</f:facet>

	</t:dataScroller>
</h:panelGrid>