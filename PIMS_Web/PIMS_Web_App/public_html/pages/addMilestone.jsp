<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Popup Used for Adding/Editing milestone
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

    function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:auto;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

</head>
	<body class="BODY_STYLE" >
	<div class="containerDiv">
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<td width="83%" height="200px" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{pages$addMilestone.heading}" styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
									<div class="SCROLLABLE_SECTION"
										style="height: 70%; width: 100%; #height: 290px; #width: 780px;">
										<h:form id="searchFrm" style="WIDTH: 98%;#WIDTH: 100%;">
											<t:div styleClass="MESSAGE">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$addMilestone.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</t:div>
											<div class="MARGIN">
													<table cellpadding="1px" cellspacing="2px">
														<tr>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="LABEL" value="#{msg['projectMilestone.milestoneNumber']}: "></h:outputLabel>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:inputText id="milestoneNumber" styleClass="INPUT_TEXT READONLY" value="#{pages$addMilestone.milestoneNumber}"></h:inputText>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL" value="#{msg['projectMilestone.milestoneName']}: "></h:outputLabel>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:inputText id="milestoneName" styleClass="INPUT_TEXT" value="#{pages$addMilestone.milestoneName}"></h:inputText>
															</td>
														</tr>
														<tr>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL" value="#{msg['projectMilestone.completionPercentage']}: "></h:outputLabel>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:inputText id="completionPercent" styleClass="INPUT_TEXT A_RIGHT_NUM" value="#{pages$addMilestone.completionPercent}"></h:inputText>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL" value="#{msg['projectMilestone.completionDate']}: "></h:outputLabel>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<rich:calendar id="completionDate" value="#{pages$addMilestone.completionDate}" locale="#{pages$addMilestone.locale}" popup="true" datePattern="#{pages$addMilestone.dateFormat}" showApplyButton="false" enableManualInput="false" cellWidth="22px" cellHeight="24px"/>
															</td>
														</tr>
														<tr>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<h:outputLabel styleClass="mandatory" value="*"/><h:outputLabel styleClass="LABEL" value="#{msg['projectMilestone.milestoneDescription']}: "></h:outputLabel>
															</td>
															<td style="PADDING: 5px; PADDING-TOP: 0px;">
																<t:inputTextarea id="description" styleClass="TEXTAREA" value="#{pages$addMilestone.description}" rows="2" style="width: 200px; height: 26px;" />
															</td>
														</tr>
														<tr>
															<td class="JUG_BUTTON_TD_AUC_POP BUTTON_TD" colspan="4">
																<table cellpadding="1px" cellspacing="1px">
																	<tr>
																		<td colspan="2">
																			<pims:security screen="Pims.ConstructionMgmt.ProjectMilestones.SaveMilestone" action="create">
																				<h:commandButton styleClass="BUTTON"
																					rendered="#{pages$addMilestone.isAddMode}"
																					value="#{msg['commons.Add']}"
																					actionListener="#{pages$addMilestone.saveProjectMilestone}"
																					tabindex="7">
																				</h:commandButton>
																				<h:commandButton styleClass="BUTTON"
																					rendered="#{pages$addMilestone.isEditMode}"
																					value="#{msg['commons.saveButton']}"
																					actionListener="#{pages$addMilestone.saveProjectMilestone}"
																					tabindex="7">
																				</h:commandButton>
																			</pims:security>
																			<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.closeButton']}"
																				onclick="javascript:window.close();"
																				tabindex="8">
																			</h:commandButton>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</div>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
</html>
</f:view>