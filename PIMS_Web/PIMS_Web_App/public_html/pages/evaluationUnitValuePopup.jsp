<%-- 
  - Author:Kamran Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Uploading a file
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
function sendNewRentAmount()
{
	window.opener.receiveNewRentAmount();
	window.close();
}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>
		<body class="BODY_STYLE">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="100%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['property.addNewAmount']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td width="100%" height="100%" valign="top">
									<div style="display: block; height: 700px; width: 100%">
										<h:form id="NewRentAmountPopup" enctype="multipart/form-data">
											<t:div styleClass="MESSAGE">
												<t:panelGrid columns="1"
													style="margin-left:5px;margin-right:5px;">
													<h:outputText
														value="#{pages$evaluationUnitValuePopup.errorMessages}"
														escape="false" styleClass="ERROR_FONT" />
												</t:panelGrid>
												<t:panelGrid columns="4">
													<h:outputLabel
														value="#{msg['commons.replaceCheque.unitType']}"></h:outputLabel>
													<h:inputText id="unitType" styleClass="READONLY"
														readonly="true"
														value="#{pages$evaluationUnitValuePopup.unitType}"></h:inputText>
													<h:outputLabel value="#{msg['receiveProperty.noOfUnits']}"></h:outputLabel>
													<h:inputText id="noOfUnits" readonly="true"
														styleClass="READONLY" 
														value="#{pages$evaluationUnitValuePopup.noOfUnits}"></h:inputText>
													<h:outputLabel
														value="#{msg['transferContract.newRentAmount']}"></h:outputLabel>
													<h:inputText id="newRentAmount"
														value="#{pages$evaluationUnitValuePopup.newRentAmount}"
														styleClass="A_RIGHT"></h:inputText>
												</t:panelGrid>
											</t:div>

											<t:panelGrid columns="1" cellpadding="2" cellspacing="0" styleClass="A_RIGHT"
												border="0" style="margin-bottom:20px;">
												<t:panelGroup>
													<t:div styleClass="BUTTON_TD"  >
														<t:commandButton id="btnSubmit" styleClass="BUTTON"
															action="#{pages$evaluationUnitValuePopup.sendNewAmount}"
															value="#{msg['cancelContract.submit']}"
															style="width: 75px;margin-right: 2px;margin-left: 2px;" />
														<t:commandButton id="btnCancel" styleClass="BUTTON"
															value="#{msg['commons.cancel']}"
															onclick="javascript:window.close();"
															style="width: 75px; margin-right: 2px;margin-left: 2px;" />
													</t:div>
												</t:panelGroup>
											</t:panelGrid>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				</td>
				</tr>
			</table>
		</body>
	</html>
</f:view>
