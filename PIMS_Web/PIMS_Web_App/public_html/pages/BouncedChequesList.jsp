<%-- 
  - Author: Syed Asif Iqbal
  - Date: 17 Mar 2009
  - Copyright Notice:
  - @(#)\
  - Description: Used for Updating Cheque status to bounced 
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	
	

</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>

			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>

		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0"
							style="margin-left: 1px; margin-right: 1px;">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['bouncedChequesList.heading']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION_POPUP"
										style="height: 500px; overflow: hidden;">
										<h:form id="bouncedChequesListFrm" style="width:96%;">
											<div style="height: 25px">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText value="#{pages$bouncedChequesList.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div
												style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 900px; PADDING-TOP: 10px">

												<div class="">
													<table cellpadding="0px" cellspacing="0px"
														class="">
														<tr>
															<td style="PADDING: 5px; padding-bottom: 0px;">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['bouncedChequesList.reasons']}:"></h:outputLabel>
															</td>
															<td style="PADDING: 5px; padding-bottom: 0px;">
															</td>
															<td style="PADDING: 5px; padding-bottom: 0px;">
															</td>
															<td style="PADDING: 5px; padding-bottom: 0px;">
																<h:inputTextarea
																	value="#{pages$bouncedChequesList.reasons}"
																	style="width: 186px; height: 30px" tabindex="1" rows="2" id="txtReasons">
																</h:inputTextarea>
															</td>
															<td style="PADDING: 5px; padding-bottom: 0px;">
																&nbsp;
															</td>
														</tr>
													</table>
												</div>
												<div
													style="PADDING-BOTTOM: 5px; WIDTH: 100%; PADDING-TOP: 10px">
													<div class="contentDiv"
														style="width: 98.2%; height: 151px; # height: 137px;">
														<t:dataTable id="dt1"
															value="#{pages$bouncedChequesList.dataList}"
															binding="#{pages$bouncedChequesList.dataTable}"
															preserveDataModel="false" preserveSort="false"
															var="dataItem" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true" width="100%">

															<t:column id="paymentReceiptDetailIdCol" width="100"
																sortable="true" rendered="false">
																<f:facet name="header">
																	<t:outputText
																		value="#{msg['bouncedChequesList.receiptDetailIdCol']}" />
																</f:facet>
																<t:outputText value="#{dataItem.paymentReceiptDetailId}"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="contractNumberCol" width="100"
																sortable="true">
																<f:facet name="header">
																	<t:outputText value="#{msg['bouncedChequesList.contractNumberCol']}" />
																</f:facet>
																<t:outputText value="#{dataItem.contractNumber}"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="paymentNumberCol" width="100"
																sortable="true">
																<f:facet name="header">
																	<t:outputText value="#{msg['bouncedChequesList.paymentNumberCol']}" />
																</f:facet>
																<t:outputText value="#{dataItem.paymentNumber}"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="paymentTypeColEn" width="80" sortable="true" rendered="#{pages$bouncedChequesList.isEnglishLocale}">
																<f:facet name="header">
																	<t:outputText value="#{msg['bouncedChequesList.paymentTypeCol']}" />
																</f:facet>
																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.paymentTypeEn}" />
															</t:column>
															<t:column id="paymentTypeColAr" width="80" sortable="true" rendered="#{pages$bouncedChequesList.isArabicLocale}">
																<f:facet name="header">
																	<t:outputText value="#{msg['bouncedChequesList.paymentTypeCol']}" />
																</f:facet>
																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.paymentTypeAr}" />
															</t:column>
															<t:column id="chequeNumberCol" width="100" sortable="true">
																<f:facet name="header">
																	<t:outputText value="#{msg['bouncedChequesList.chequeNumberCol']}" />
																</f:facet>
																<t:outputText value="#{dataItem.chequeNumber}"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="amountCol" width="100"
																sortable="true">
																<f:facet name="header">
																	<t:outputText value="#{msg['bouncedChequesList.amountCol']}" />
																</f:facet>
																<t:outputText value="#{dataItem.amount}"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="dueDateCol" width="100"
																sortable="true">
																<f:facet name="header">
																	<t:outputText value="#{msg['bouncedChequesList.dueDateCol']}" />
																</f:facet>
																<t:outputText value="#{dataItem.dueDate}"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="bankNameColEn" width="100" sortable="true" rendered="#{pages$bouncedChequesList.isEnglishLocale}">
																<f:facet name="header">
																	<t:outputText value="#{msg['bouncedChequesList.bankNameCol']}" />
																</f:facet>
																<t:outputText value="#{dataItem.bankNameEn}"
																	styleClass="A_LEFT" />
															</t:column>
															<t:column id="bankNameColAr" width="100" sortable="true" rendered="#{pages$bouncedChequesList.isArabicLocale}">
																<f:facet name="header">
																	<t:outputText value="#{msg['bouncedChequesList.bankNameCol']}" />
																</f:facet>
																<t:outputText value="#{dataItem.bankNameAr}"
																	styleClass="A_LEFT" />
															</t:column>															
															<t:column id="tenantNameCol" width="100" sortable="true">
																<f:facet name="header">
																	<t:outputText value="#{msg['bouncedChequesList.tenantNameCol']}" />
																</f:facet>
																<t:outputText value="#{dataItem.tenantName}"
																	styleClass="A_LEFT" />
															</t:column>
														</t:dataTable>
													</div>
													<table width="100%" cellpadding="0" cellspacing="0"
														border="0">
														<tr>
															<td class="BUTTON_TD" colspan="4">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['bouncedChequesList.close']}"
																	actionListener="#{pages$bouncedChequesList.btnCancel}"
																	style="width: 107px" tabindex="7">
																</h:commandButton>&nbsp;
																<pims:security
																	screen="Pims.PaymentManagement.PaymentDetails.ChequeList.BounceChequesList.Save"
																	action="create">
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['bouncedChequesList.save']}"
																		actionListener="#{pages$bouncedChequesList.btnSave}"
																		style="width: 107px" tabindex="7">
																	</h:commandButton>&nbsp;
																</pims:security>
															</td>
														</tr>
													</table>
												</div>

											</div>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</body>
	</html>
</f:view>