<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Auction
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<%
					response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
						response.setHeader("Pragma", "no-cache"); //HTTP 1.0
						response.setDateHeader("Expires", 0); //prevents caching at the proxy server
				%>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>


					<tr width="100%">

						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="83%" height="100%" valign="top"
							class="divBackgroundBody">
							<table class="greyPanelTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['user.userSearchScreenTitle']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr valign="top">
									<td>

										<h:form id="searchFrm" enctype="multipart/form-data">
											<div class="SCROLLABLE_SECTION">
												<div>
													<table border="0" class="layoutTable">
														<tr>
															<td>
																<h:outputText escape="false" styleClass="ERROR_FONT" />
															</td>
														</tr>
													</table>
												</div>
												<div class="MARGIN" style="width: 95%;">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td>
																<IMG
																	src="../<h:outputText value="#{path.img_section_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%">
																<IMG
																	src="../<h:outputText value="#{path.img_section_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td>
																<IMG
																	src="../<h:outputText value="#{path.img_section_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="DETAIL_SECTION" style="width: 99.6%;">
														<h:outputLabel value="#{msg['commons.searchCriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER">
															<tr>
																<td class="DET_SEC_TD">
																	<h:outputText styleClass="LABEL"
																		value="#{msg['user.userFullName']}" />
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText styleClass="INPUT_TEXT A_LEFT"
																		binding="#{pages$SearchSecUser.fullNameTextBox}"
																		value="#{pages$SearchSecUser.fullName}"></h:inputText>

																</td>
																<td class="DET_SEC_TD">
																	<h:outputText styleClass="LABEL"
																		value="#{msg['user.userFullNameArabic']}" />

																</td>
																<td class="DET_SEC_TD">
																	<h:inputText styleClass="INPUT_TEXT A_RIGHT"
																		binding="#{pages$SearchSecUser.fullNameSecTextBox}"></h:inputText>

																</td>

															</tr>
															<tr>
																<td class="DET_SEC_TD">
																	<h:outputText styleClass="LABEL"
																		value="#{msg['user.userFirstName']}" />

																</td>
																<td class="DET_SEC_TD">
																	<h:inputText styleClass="INPUT_TEXT A_LEFT"
																		binding="#{pages$SearchSecUser.firstNameTextBox}"></h:inputText>

																</td>
																<td class="DET_SEC_TD">
																	<h:outputText styleClass="LABEL"
																		value="#{msg['user.userLastName']}" />

																</td>
																<td class="DET_SEC_TD">
																	<h:inputText styleClass="INPUT_TEXT A_LEFT"
																		binding="#{pages$SearchSecUser.lastNameTextBox}"></h:inputText>

																</td>

															</tr>
															<tr>
																<td class="DET_SEC_TD">
																	<h:outputText styleClass="LABEL"
																		value="#{msg['user.userMiddleName']}" />


																</td>
																<td class="DET_SEC_TD">
																	<h:inputText styleClass="INPUT_TEXT A_LEFT"
																		binding="#{pages$SearchSecUser.middleNameTextBox}"></h:inputText>

																</td>
																<td class="DET_SEC_TD">
																	<h:outputText styleClass="LABEL"
																		value="#{msg['user.email']}" />

																</td>
																<td class="DET_SEC_TD">
																	<h:inputText styleClass="INPUT_TEXT A_LEFT"
																		binding="#{pages$SearchSecUser.emailUrlTextBox}"></h:inputText>

																</td>
															</tr>

															<tr>
																<td class="DET_SEC_TD">
																	<h:outputText styleClass="LABEL"
																		value="#{msg['user.loginId']}" />

																</td>
																<td class="DET_SEC_TD">
																	<h:inputText styleClass="INPUT_TEXT A_LEFT"
																		binding="#{pages$SearchSecUser.loginIdTextBox}" />

																</td>
																<td class="DET_SEC_TD">
																	<h:outputText styleClass="LABEL"
																		value="#{msg['user.status']}" />

																</td>
																<td class="DET_SEC_TD">
																	<h:selectOneMenu styleClass="SELECT_MENU" id="a3"
																		value="#{pages$SearchSecUser.userStatus}">
																		<%--<f:selectItems value="#{pages$SearchSecUser.status}" />--%>
																		<f:selectItem itemLabel="#{msg['commons.All']}"
																			itemValue="-1" />
																		<f:selectItem itemLabel="#{msg['user.status.Active']}"
																			itemValue="1" />
																		<f:selectItem
																			itemLabel="#{msg['user.status.InActive']}"
																			itemValue="0" />
																	</h:selectOneMenu>


																</td>
															</tr>

															<tr>
																<td class="DET_SEC_TD">
																	<h:outputText styleClass="LABEL"
																		value="#{msg['user.isAdmin']}" />

																</td>
																<td class="DET_SEC_TD">
																	<h:selectBooleanCheckbox>
																	</h:selectBooleanCheckbox>

																</td>
																<td class="DET_SEC_TD">

																</td>
																<td class="DET_SEC_TD">

																</td>
															</tr>



															<tr>
																<td class="BUTTON_TD JUG_BUTTON_TD_US" colspan="4">

																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.search']}"
																		action="#{pages$SearchSecUser.searchUser}"></h:commandButton>
																	&nbsp;
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.add']}"
																		action="#{pages$SearchSecUser.addUser}"></h:commandButton>
																	&nbsp;
																	<h:commandButton styleClass="BUTTON"
																		value="#{msg['commons.cancel']}"
																		action="#{pages$SearchSecUser.reset}"></h:commandButton>


																</td>

															</tr>
														</table>
													</div>
												</div>
												<div
													style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px; width: 95%">
													<div class="imag">
														&nbsp;
													</div>
													<div class="contentDiv" style="width: 98%">
														<t:dataTable id="test2"
															value="#{pages$SearchSecUser.secUserDataList}"
															binding="#{pages$SearchSecUser.dataTable}"
															rows="#{pages$SearchSecUser.paginatorRows}"
															preserveDataModel="false" preserveSort="false"
															var="dataItem" rowClasses="row1,row2"
															renderedIfEmpty="true" width="100%">

															<t:column id="col1" width="10%">
																<f:facet name="header">
																	<t:outputText value="#{msg['user.loginId']}" />
																</f:facet>
																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.loginId}" />
															</t:column>


															<t:column id="col2" sortable="true" width="20%">
																<f:facet name="header">


																	<t:outputText value="#{msg['user.userFullName']}" />

																</f:facet>
																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.fullName}" />
															</t:column>


															<t:column id="col3" sortable="true">


																<f:facet name="header">

																	<t:outputText value="#{msg['user.userFullNameArabic']}" />

																</f:facet>
																<t:outputText styleClass="A_RIGHT"
																	value="#{dataItem.secondaryFullName}" />
															</t:column>
															<t:column id="col4" sortable="true">
																<f:facet name="header">

																	<t:outputText value="#{msg['user.userFirstName']}" />

																</f:facet>
																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.firstName}" />
															</t:column>

															<t:column id="col5" sortable="true">


																<f:facet name="header">

																	<t:outputText value="#{msg['user.userMiddleName']}" />

																</f:facet>
																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.middleName}" />
															</t:column>


															<t:column id="col6">


																<f:facet name="header">

																	<t:outputText value="#{msg['user.userLastName']}" />

																</f:facet>
																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.lastName}" />
															</t:column>

															<t:column id="col7">


																<f:facet name="header">

																	<t:outputText value="#{msg['user.email']}" />

																</f:facet>
																<t:outputText styleClass="A_LEFT"
																	value="#{dataItem.emailUrl}" />
															</t:column>
															<%-- 	<t:column id="col8" >


														<f:facet name="header">

															<t:outputText value="#{msg['user.isAdmin']}" />

														</f:facet>
														<h:selectBooleanCheckbox value="true" />
													</t:column>
													--%>

															<t:column id="col9">

																<f:facet name="header">

																	<t:outputText value="#{msg['commons.edit']}" />
																</f:facet>

																<h:commandLink
																	action="#{pages$SearchSecUser.editDataItem}">
																	<h:graphicImage title="#{msg['commons.edit']}"
																		url="../resources/images/edit-icon.gif" />
																</h:commandLink>
															</t:column>
														</t:dataTable>
													</div>
													<t:div styleClass="contentDivFooter" style="width:99%;">
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td class="RECORD_NUM_TD">
																	<div class="RECORD_NUM_BG">
																		<table cellpadding="0" cellspacing="0"
																			style="width: 182px; # width: 150px;">
																			<tr>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText value=" : " />
																				</td>
																				<td class="RECORD_NUM_TD">
																					<h:outputText
																						value="#{pages$SearchSecUser.recordSize}" />
																				</td>
																			</tr>
																		</table>
																	</div>
																</td>
																<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																	align="right">
																	<CENTER>
																		<t:dataScroller id="scroller" for="test2"
																			paginator="true" fastStep="1"
																			paginatorMaxPages="#{pages$SearchSecUser.paginatorMaxPages}"
																			immediate="false" paginatorTableClass="paginator"
																			renderFacetsIfSinglePage="true"
																			paginatorTableStyle="grid_paginator"
																			layout="singleTable"
																			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																			paginatorActiveColumnStyle="font-weight:bold;"
																			paginatorRenderLinkForActive="false"
																			pageIndexVar="pageNumber" styleClass="SCH_SCROLLER">
																			<f:facet name="first">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblF"></t:graphicImage>
																			</f:facet>
																			<f:facet name="fastrewind">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFR"></t:graphicImage>
																			</f:facet>
																			<f:facet name="fastforward">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																			</f:facet>
																			<f:facet name="last">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblL"></t:graphicImage>
																			</f:facet>
																			<t:div styleClass="PAGE_NUM_BG">
																				<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																				<table cellpadding="0" cellspacing="0">
																					<tr>
																						<td>
																							<h:outputText styleClass="PAGE_NUM"
																								value="#{msg['commons.page']}" />
																						</td>
																						<td>
																							<h:outputText styleClass="PAGE_NUM"
																								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																								value="#{requestScope.pageNumber}" />
																						</td>
																					</tr>
																				</table>

																			</t:div>
																		</t:dataScroller>
																	</CENTER>

																</td>
															</tr>
														</table>
													</t:div>
												</div>
											</div>


											</div>
										</h:form>


									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

		</body>
	</html>
</f:view>