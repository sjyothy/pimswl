
<t:div id="tabDDetails" style="width:100%;">
	<t:panelGrid id="ddTab" cellpadding="5px" width="100%"
		rendered="#{pages$donationRequest.showSaveButton || pages$donationRequest.showResubmitButton}"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblBenef" styleClass="LABEL"
				value="#{msg['donationRequest.lbl.endowmentProgram']}" />
		</h:panelGroup>
		<h:panelGroup>

			<h:inputText id="program" maxlength="20" readonly="true"
				styleClass="READONLY"
				value="#{pages$donationRequest.paymentView.endowmentProgramView.progName}" />

			<h:graphicImage id="endowmentProgramsSearchpopup"
				style="margin-right:5px;"
				title="#{msg['endowmentProgram.title.search']}"
				url="../resources/images/app_icons/Settle-Contract.png"
				onclick="javaScript:openEndowmentProgramsPopup();" />
		</h:panelGroup>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['endowmentProgram.lbl.targetedAmount']}:" />
		<h:inputText id="txtTargetAmount" readonly="true"
			styleClass="READONLY"
			value="#{pages$donationRequest.paymentView.endowmentProgramView.targetAmount}" />


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="paymenttypelbl" styleClass="LABEL"
				value="#{msg['cancelContract.payment.paymenttype']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="selectPaymentType"
			value="#{pages$donationRequest.paymentView.typeIdStr}">
			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems value="#{pages$donationRequest.programPaymentTypes}" />
		</h:selectOneMenu>


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="donationSrcField" styleClass="LABEL"
				value="#{msg['donationRequest.lbl.donationSrc']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="selectdonationSrc"
			value="#{pages$donationRequest.paymentView.donationSourceIdString}">
			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems value="#{pages$ApplicationBean.donationSource}" />
		</h:selectOneMenu>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="payModeField" styleClass="LABEL"
				value="#{msg['cancelContract.payment.paymentmethod']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="selectPaymentMethod"
			value="#{pages$donationRequest.paymentView.paymentModeIdString}">

			<f:selectItems value="#{pages$ApplicationBean.paymentMethodList}" />
		</h:selectOneMenu>


		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblpaymentDueDate" styleClass="LABEL"
				value="#{msg['addMissingMigratedPayments.fieldName.paymentDueDate']}:"></h:outputLabel>
		</h:panelGroup>
		<rich:calendar id="paymentDueDatecal"
			value="#{pages$donationRequest.paymentView.paymentDueOn}"
			popup="true" datePattern="dd/MM/yyyy"
			showApplyButton="false" locale="#{pages$donationRequest.locale}"
			enableManualInput="false" inputStyle="width: 170px; height: 14px" />



		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="amnt" styleClass="LABEL"
				value="#{msg['commons.amount']}:"></h:outputLabel>
		</h:panelGroup>
		<h:inputText id="txtAmount"
			readonly="#{!pages$donationRequest.showSaveButton && 
			    		!pages$donationRequest.showResubmitButton 
			           }"
			value="#{pages$donationRequest.paymentView.amountStr }"
			onkeyup="removeNonNumeric(this)" onkeypress="removeNonNumeric(this)"
			onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
			onchange="removeNonNumeric(this)" />

		<h:panelGroup>
<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="lblDescriptionamnt" styleClass="LABEL"
				value="#{msg['commons.description']}:"></h:outputLabel>
		</h:panelGroup>
		<t:inputTextarea id="txtDescription"
			readonly="#{!pages$donationRequest.showSaveButton && 
			    		!pages$donationRequest.showResubmitButton 
			           }"
			value="#{pages$donationRequest.paymentView.description }" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;">

		<h:commandButton styleClass="BUTTON"
			rendered="#{pages$donationRequest.showSaveButton ||  
										 pages$donationRequest.showResubmitButton 
					   				    }"
			value="#{msg['commons.Add']}" onclick="performTabClick('lnkAdd');">
			<h:commandLink id="lnkAdd" action="#{pages$donationRequest.onAdd}" />
		</h:commandButton>


	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTablePayments" rows="15" width="100%"
			value="#{pages$donationRequest.paymentList}"
			binding="#{pages$donationRequest.dataTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
			<t:column id="payNumber" sortable="true">
				<f:facet name="header">
					<t:outputText id="payNum"
						value="#{msg['chequeList.paymentNumber']}" />
				</f:facet>
				<t:outputText id="payNoField" styleClass="A_LEFT"
					value="#{dataItem.paymentNumber}" />
			</t:column>
			<t:column id="endProgram" sortable="true">
				<f:facet name="header">
					<t:outputText id="endProg"
						value="#{msg['endowmentProgram.lbl.name']}" />
				</f:facet>
				<t:commandLink
					onclick="javaScript:openManageEndowmentPrograms('#{dataItem.endowmentProgramView.endowmentProgramId}');"
					value="#{dataItem.endowmentProgramView.progName}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>

			<t:column id="paymentTypettcol" sortable="true">
				<f:facet name="header">
					<t:outputText id="paymentTypeCOloutput"
						value="#{msg['cancelContract.payment.paymenttype']}" />
				</f:facet>
				<t:outputText id="paymentTypettcolFfdsield" styleClass="A_LEFT"
					value="#{pages$donationRequest.englishLocale? dataItem.typeEn:dataItem.typeEn}" />
			</t:column>
			<t:column id="paymentDueDatettcol" sortable="true">
				<f:facet name="header">
					<t:outputText id="paymentDueDateCOloutput"
						value="#{msg['addMissingMigratedPayments.fieldName.paymentDueDate']}" />
				</f:facet>
				<t:outputText id="GpayDate" styleClass="A_LEFT"
					value="#{dataItem.paymentDueOn}" style="white-space: normal;">
					<f:convertDateTime pattern="#{pages$donationRequest.dateFormat}"
						timeZone="#{pages$donationRequest.timeZone}" />
				</t:outputText>
			</t:column>

			<t:column id="donationSrcCOl" sortable="true">
				<f:facet name="header">
					<t:outputText id="donationSrcCOloutput"
						value="#{msg['donationRequest.lbl.donationSrc']}" />
				</f:facet>
				<t:outputText id="donationSrcdonationField" styleClass="A_LEFT"
					value="#{pages$donationRequest.englishLocale? dataItem.donationSourceEn:dataItem.donationSourceAr}" />
			</t:column>
			<t:column id="statusCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="statusAmnt" value="#{msg['commons.status']}" />
				</f:facet>
				<t:outputText id="status" styleClass="A_LEFT"
					value="#{pages$donationRequest.englishLocale? dataItem.statusEn:dataItem.statusAr}" />
			</t:column>

			<t:column id="payMode" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdMethod"
						value="#{msg['cancelContract.payment.paymentmethod']}" />
				</f:facet>
				<t:outputText id="paymentMode" styleClass="A_LEFT"
					value="#{dataItem.paymentModeAr}" />
			</t:column>
			<t:column id="paymentDescription" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdpaymentDescription"
						value="#{msg['commons.description']}" />
				</f:facet>
				<t:outputText id="paymentDescriptionField" styleClass="A_LEFT"
					value="#{dataItem.description}" />
			</t:column>

			<t:column id="amntCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAmnt"
						value="#{msg['mems.payment.request.label.Amount']}" />
				</f:facet>
				<t:outputText id="amountt" styleClass="A_LEFT"
					value="#{dataItem.amountStr}" />
			</t:column>

			<t:column id="action" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAmoun" value="#{msg['commons.action']}" />
				</f:facet>

				<h:commandLink id="lnkDelete"
					onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableInputs();"
					rendered="#{pages$donationRequest.showSaveButton || pages$donationRequest.showResubmitButton}"
					action="#{pages$donationRequest.onDelete}">
					<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
						url="../resources/images/delete.gif" width="18px;" />
				</h:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>
</t:div>


