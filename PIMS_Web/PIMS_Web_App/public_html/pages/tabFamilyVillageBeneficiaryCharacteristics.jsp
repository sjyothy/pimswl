
<t:div id="tabBCharacteristicsDetails" style="width:100%;">
	<t:panelGrid id="tabCharacteristicsDetailsGrid" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['commons.description']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu
			value="#{pages$tabFamilyVillageBeneficiaryCharacteristics.pageView.characteristicTypeIdString}">
			<f:selectItem itemLabel="#{msg['commons.select']}" itemValue="" />
			<f:selectItems value="#{pages$ApplicationBean.beneficiaryCharacteristics}" />
		</h:selectOneMenu>
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['commons.description']}:"></h:outputLabel>
		</h:panelGroup>
		<t:panelGroup colspan="3" style="width: 100%">
			<t:inputTextarea style="width: 90%;" id="txtcharacteristicDescription"
				value="#{pages$tabFamilyVillageBeneficiaryCharacteristics.pageView.characteristicDescription}"
				onblur="javaScript:removeExtraCharacter(txtcharacteristicDescription,1200);"
				onkeyup="javaScript:removeExtraCharacter(txtcharacteristicDescription,1200);"
				onmouseup="javaScript:removeExtraCharacter(txtcharacteristicDescription,1200);"
				onmousedown="javaScript:removeExtraCharacter(txtcharacteristicDescription,1200);"
				onchange="javaScript:removeExtraCharacter(txtcharacteristicDescription,1200);"
				onkeypress="javaScript:removeExtraCharacter(txtcharacteristicDescription,1200);" />
		</t:panelGroup>

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;">

		<h:commandButton styleClass="BUTTON" value="#{msg['commons.Add']}"
			action="#{pages$tabFamilyVillageBeneficiaryCharacteristics.onAdd}">
		</h:commandButton>
	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableCharacteristics" rows="15" width="100%"
			value="#{pages$tabFamilyVillageBeneficiaryCharacteristics.dataList}"
			binding="#{pages$tabFamilyVillageBeneficiaryCharacteristics.dataTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="CharacteristicscreatedBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="CharacteristicsCreatedOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageBeneficiaryCharacteristics.englishLocale? 
																				dataItem.createdBy:
																				dataItem.createdBy}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="CharacteristicsCreatedOn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageBeneficiaryCharacteristics.dateFormat}"
						timeZone="#{pages$tabFamilyVillageBeneficiaryCharacteristics.timeZone}" />
				</h:outputText>
			</t:column>
						<t:column id="commonscharacteristicsType" sortable="true">
				<f:facet name="header">
					<t:outputText id="commonscharacteristicsTypeOT"
						value="#{msg['commons.characteristicsType']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageBeneficiaryCharacteristics.englishLocale? 
																				dataItem.characteristicTypeEn:
																				dataItem.characteristicTypeAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			
			
			<t:column id="characteristicDetails" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.description']}" />
				</f:facet>
				<h:outputText value="#{dataItem.characteristicDescription}">

				</h:outputText>
			</t:column>


		</t:dataTable>
	</t:div>

</t:div>


