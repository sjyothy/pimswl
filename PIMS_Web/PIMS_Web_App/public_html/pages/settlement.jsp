
<%@page import="com.avanza.ui.util.ResourceUtil"%><%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript" src="../resources/jscripts/commons.js"></script>
<script type="text/javascript">
      function onSettlementClick( control )
	  {
	    control.disabled  = true;
	     
	    return oamSubmitForm('settlementForm','settlementForm:lnkSettlement');
	  }
	function printSettlement()
	{
      var screen_width = screen.width;
      var screen_height = screen.height;
      var popup_width = screen_width/3+150;
      var popup_height = screen_height/3-10;
      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
      
      var popup = window.open('printSettlement.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=yes,resizable=yes,titlebar=yes,dialog=yes');
      popup.focus();
	}
	function onAddEvacFines()
	{
		window.open('exemptionReason.jsf?entityId=' + $F('requestId') + '&noteowner=EXEMPTION_REASON', 'Exemption', 'width=500,height=400,toolbar=0,resizable=0,status=0,location=0');
	}
	function onCollectPayment()
	{
		window.open('receivePayment.jsf', 'ReceivePayment', 'toolbar=0,resizable=0,status=0,location=0');
		return false;					
	}
	
	function onDateChange()
	{
		window.document.forms[0].submit();	
	}
	function openAddPaymentsPopup(arg1, arg2)
	{
	      var screen_width = screen.width;
	      var screen_height = screen.height;
	      var popup_width = screen_width/3+150;
	      var popup_height = screen_height/3+150;
	      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
	      var popup = window.open('PaymentSchedule.jsf?'+arg1+'='+arg2+',_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,resizable=yes,titlebar=yes,dialog=yes');
	      popup.focus();
	}
	function openCancelRequest()
	{
	   var  screen_width = 1024;
	   var screen_height = 450;
	   window.open('CancelContract.jsf?pageMode=popup','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=20,top=150,scrollbars=yes,status=yes');
	}
	function openTransferRequest()
	{
	   var  screen_width = 1024;
	   var screen_height = 450;
	   window.open('transferContract.jsf?pageMode=popup','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=20,top=150,scrollbars=yes,status=yes');
	}
	function openLoadReportPopup(pageName) 
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}	
	
		
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg" />
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"	CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.js_popup_calendar}"/>" />
		</head>
		<body class="BODY_STYLE">
		
		<div class="containerDiv">
			<h:inputHidden id="requestId" value="#{pages$Settlement.requestId}"/>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>
				<tr >
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table  class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD"><h:outputLabel value="#{msg['cancelContract.title']}" styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
							<tr valign="top">
								<td valign="top" nowrap="nowrap" background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
								</td>
								<td width="100%" valign="top" nowrap="nowrap">
									<h:form id="settlementForm" enctype="multipart/form-data">
									<div class="SCROLLABLE_SECTION" >
										<div>
											<h:outputText value="#{pages$Settlement.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
											<h:outputText value="#{pages$Settlement.successMessages}" escape="false" styleClass="INFO_FONT"/>
										</div>
										
										<div class="MARGIN" style="width:95%;">
											<table class="BUTTON_TD"  width="100%">
												<tr>
													<td>
													  <h:commandButton id="openRequest" styleClass="BUTTON" rendered="#{!pages$Settlement.readOnly }"  action="#{pages$Settlement.btnOpenRequest_Click}" value="#{msg['settlement.Request']}"/>
													  <h:commandButton id="save" style="width:150px;" styleClass="BUTTON" onclick="javaScript:onSettlementClick(this);" value="#{msg['settlement.actions.save']}"
											           rendered="#{!pages$Settlement.readOnly && pages$Settlement.fromTaskList}"/>
											           <h:commandLink id="lnkSettlement"  action="#{pages$Settlement.onSave}"/>
											           <h:commandButton id="RejectRequest" style="width:150px;" styleClass="BUTTON" 
											           binding="#{pages$Settlement.btnRejectBtn}"
											           onclick="if (!confirm('#{msg['common.msgDialog.askForRejection']}')) return false;"
											           action="#{pages$Settlement.onRejectRequest}" value="#{msg['commons.reject']}" 
											           rendered="#{(!pages$Settlement.readOnly && pages$Settlement.fromTaskList && pages$Settlement.fromCancelContract) || pages$Settlement.transferSettlementTask }"/>
											          <h:commandButton id="print" style="width:150px;" styleClass="BUTTON" action="#{pages$Settlement.printReport}" 
											           value="#{msg['commons.print']}" rendered="#{!pages$Settlement.readOnly && pages$Settlement.fromTaskList}"/>
                             			 
													</td>
												</tr>
											</table>
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
											</table>
											
											
											
											<div class="DETAIL_SECTION" style="width:99.6%;">
												<h:outputLabel value="#{msg['contract.Contract.Details']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
												<t:panelGrid columns="4" styleClass="DETAIL_SECTION_INNER" columnClasses="GRID_C1, GRID_C2, GRID_C1, GRID_C2">
													 <h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['commons.Contract.Number']}:"/>
											        <h:inputText styleClass="A_RIGHT_NUM READONLY" id="txtContractNumber"  value="#{pages$Settlement.contractBean.contractNo}" />
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['contract.tenantName']}:"/>
											        <h:inputText styleClass="A_RIGHT_NUM READONLY"   id="txtTenentName" value="#{pages$Settlement.contractBean.tenantName}" />
													 <h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.grpCustNo']}:"/>
											        <h:inputText styleClass="A_RIGHT_NUM READONLY"  id="txtGRPCustomerNumber"  value="#{pages$Settlement.contractBean.grpCustomerNumber}" />
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.costCenter']}:"/>
											        <h:inputText styleClass="A_RIGHT_NUM READONLY"   id="txtGRPCostCenter" value="#{pages$Settlement.contractBean.grpUnitCostCenter}" />
													
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.startdate']}"/>
											        <h:inputText id="txtStartDate" readonly="true" value="#{pages$Settlement.contractBean.contractStartDate}" styleClass="READONLY"/>
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.enddate']}"/>
											        <h:inputText id="txtEndDate" readonly="true" value="#{pages$Settlement.contractBean.contractExpiryDate}" styleClass="READONLY"/>
													
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.rentalamount']}"/>
													<h:inputText styleClass="A_RIGHT_NUM READONLY" id="txtUnitRentValue" readonly="true" value="#{pages$Settlement.contractBean.rentalAmount}">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
													</h:inputText>
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.evacdate']}"/>
													<t:panelGrid cellpadding="0" cellspacing="0" columns="2">
														<rich:calendar  id="evacdate" binding="#{pages$Settlement.evacuationClnder}"  value="#{pages$Settlement.contractBean.evacDate}" datePattern="#{pages$Settlement.dateFormat}" />
														<h:commandLink  action="#{pages$Settlement.onCalculateSettlement}" rendered="#{pages$Settlement.parentContractList}">
															<h:graphicImage 
																title="#{msg['settlement.label.calculate']}"
																url="../resources/images/app_icons/Payment-Management.png">
															</h:graphicImage>
														</h:commandLink>
													</t:panelGrid>
													
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.actualrentperiod']}"/>
													<t:panelGrid cellpadding="0" cellspacing="0" columns="4">
														<h:inputText style="width:20px;" id="months" readonly="true" value="#{pages$Settlement.contractBean.months}" styleClass="READONLY"/>
														<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.months']}"/>
														<h:inputText style="width:20px;" id="days" readonly="true" value="#{pages$Settlement.contractBean.days}" styleClass="READONLY"/>
														<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.days']}"/>
													</t:panelGrid>
													<h:outputLabel styleClass="TABLE_LABEL" value="#{msg['settlement.label.actualrentamt']}"/>
													<h:inputText styleClass="A_RIGHT_NUM READONLY" id="actualRentalAmount" readonly="true" value="#{pages$Settlement.contractBean.actualRentalAmount}">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
													</h:inputText>

													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.totaldepositamt']}"/>
											        <h:inputText styleClass="A_RIGHT_NUM READONLY" id="totalDepositAmount" readonly="true" value="#{pages$Settlement.contractBean.totalDepositAmount}">
											        	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											        </h:inputText>
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.totalrealizedamt']}"/>
											        <h:inputText styleClass="A_RIGHT_NUM READONLY" id="totalRealizedAmount" readonly="true" value="#{pages$Settlement.contractBean.totalRealizedAmount}">
											        	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											        </h:inputText>

													<t:div>&nbsp;</t:div>
													<t:div>&nbsp;</t:div>
													<h:outputLabel styleClass="TABLE_LABEL" value="#{msg['settlement.label.paidamt']}" style="font-weight:bold;"/>
											        <h:inputText styleClass="A_RIGHT_NUM READONLY" id="totalPaidAmount" readonly="true" value="#{pages$Settlement.contractBean.totalPaidAmount}">
											        	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											        </h:inputText>

													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.unrealizedamt']}"/>
											        <h:inputText styleClass="A_RIGHT_NUM READONLY" id="totalUnrealizedAmount" readonly="true" value="#{pages$Settlement.contractBean.totalUnrealizedAmount}">
											        	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											        </h:inputText>
											        <h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.evacfines']}:" rendered="#{pages$Settlement.parentTransferContract}" />
											        <h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.evacfinesApplied']}:" rendered="#{ (pages$Settlement.exempted == 0) && (!pages$Settlement.parentTransferContract)}" />
												    <h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.evacfinesExempted']}:" rendered="#{ (pages$Settlement.exempted == 1)&& (!pages$Settlement.parentTransferContract)}" />
													
											        <t:panelGrid cellpadding="0" cellspacing="0" columns="4">
											        	<h:inputText styleClass="A_RIGHT_NUM READONLY"  id="totalEvacFines" value="#{pages$Settlement.contractBean.totalEvacFines}" disabled="#{pages$Settlement.readOnly}" readonly="true"/>
											        	<pims:security screen="Pims.Settlement.EvacuationOptions" action="view">
											        	<h:commandLink   onclick="javascript: onAddEvacFines();"  rendered="#{(!pages$Settlement.readOnly) && (!pages$Settlement.parentTransferContract)}">
											        		<h:graphicImage 
											        			title="#{msg['tenderManagement.approveTender.comments']}"
											        			url="../resources/images/app_icons/No-Objection-letter.png">											        		
											        		</h:graphicImage>
											        	</h:commandLink>
											        	<h:commandLink  actionListener="#{pages$Settlement.onEvacFinesExempted}" rendered="#{ (pages$Settlement.exempted == 0) &&  (!pages$Settlement.readOnly) && (!pages$Settlement.parentTransferContract)}">
											        		<h:graphicImage 
											        			title="#{msg['settlement.label.exempt']}"
											        			url="../resources/images/app_icons/No-Objection-letter.png">											        		
											        		</h:graphicImage>
											        	</h:commandLink>
											        	<h:commandLink  actionListener="#{pages$Settlement.onEvacFinesApplied}"  rendered="#{(pages$Settlement.exempted == 1) &&  (!pages$Settlement.readOnly) && (!pages$Settlement.parentTransferContract) }">
											        		<h:graphicImage 
											        			title="#{msg['settlement.label.apply']}"
											        			url="../resources/images/app_icons/No-Objection-letter.png">											        		
											        		</h:graphicImage>
											        	</h:commandLink>
											        	</pims:security>
											        </t:panelGrid>
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.unpaidfinesEnterByUser']}"/>
											        <h:inputText styleClass="A_RIGHT_NUM" id="otherUnpaidfinesExtra"  value="#{pages$Settlement.contractBean.unpaidfinesEnteredByUser}">
											        	
											        </h:inputText>
													<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL" value="#{msg['settlement.label.unpaidfines']}"/>
											        <h:inputText styleClass="A_RIGHT_NUM READONLY" id="otherUnpaidfines" readonly="true" value="#{pages$Settlement.contractBean.otherUnpaidfines}">
											        	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											        </h:inputText>
													<h:outputLabel rendered="false" styleClass="TABLE_LABEL" value="#{msg['settlement.label.unpaidamt']}" style="font-weight:bold;"/>
											        <h:inputText rendered="false" styleClass="A_RIGHT_NUM READONLY" id="totalUnpaidAmount" readonly="true" value="#{pages$Settlement.contractBean.totalUnpaidAmount}">
											        	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											        </h:inputText>
											        <h:outputLabel  rendered="#{pages$Settlement.showCreditMemo}" styleClass="TABLE_LABEL" value="#{msg['settlement.label.creditMemo']}" style="font-weight:bold;"/>
											        <h:inputText    rendered="#{pages$Settlement.showCreditMemo}" styleClass="A_RIGHT_NUM READONLY" id="creditMemo" readonly="true" value="#{pages$Settlement.contractBean.creditMemo}">
											        	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
											        </h:inputText>
												</t:panelGrid>
											</div>
											<t:div>&nbsp;</t:div>
											<table>
											  <tr>
											      <td >
											        <h:outputLabel value="#{msg['settlement.lbl.rentPaymentsDetails']}" style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px;" />
											      </td>
											  </tr>
											</table>
											<t:div>
												<table id="imageTable" cellpadding="0" cellspacing="0" width="100%" style ="margin-right:4px;">
													<tr>
													<td><IMG id="image1" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_left")%>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG id="image2" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_mid")%>" class="TAB_PANEL_MID"/></td>
													<td><IMG id="image3" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_right")%>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>
												<t:div styleClass="contentDiv" style="width:99%">
													<t:dataTable id="dt1" var="paymentItem" rows="#{pages$Settlement.paginatorRows}" binding="#{pages$Settlement.paymentTable}" value="#{pages$Settlement.payments}"  width="100%">
														
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymenttype']}"/></f:facet>
															<h:outputText value="#{paymentItem.paymentType}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymentdate']}"/></f:facet>
															<h:outputText value="#{paymentItem.dueDate}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymentmethod']}"/></f:facet>
															<h:outputText value="#{paymentItem.paymentMethod}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['commons.amount']}"/></f:facet>
															<h:outputText styleClass="A_RIGHT_NUM" value="#{paymentItem.amountVal}">
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
															</h:outputText>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['commons.status']}"/></f:facet>
															<h:outputText value="#{paymentItem.status}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.receiptno']}"/></f:facet>
															<h:outputText value="#{paymentItem.recieptNo}"/>
														</t:column>
														<!-- hide this colume bcoz payment detail screen is not made yet -->
														<t:column width="10%" rendered="false">
															<f:facet name="header"><h:outputText value="#{msg['commons.action']}"/></f:facet>
															<t:commandLink actionListener="#{pages$Settlement.onPaymentDetail}" rendered="#{paymentItem.showDetail}">
																<t:graphicImage title="#{msg['commons.details']}" url="../resources/images/app_icons/replaceCheque.png">																	
																</t:graphicImage>
															</t:commandLink>
														</t:column>
													</t:dataTable>
												</t:div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:100%;" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$Settlement.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
												<CENTER>
													<t:dataScroller id="scroller" for="dt1" paginator="true"  
														fastStep="1" paginatorMaxPages="#{pages$Settlement.paginatorMaxPages}" immediate="false"
														paginatorTableClass="paginator"
														renderFacetsIfSinglePage="true" 												
														paginatorTableStyle="grid_paginator" layout="singleTable" 
														paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
														paginatorActiveColumnStyle="font-weight:bold;"
														paginatorRenderLinkForActive="false" 
														pageIndexVar="pageNumber"
														styleClass="SCH_SCROLLER">
														<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblFSettle"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFRSettle"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFSettle"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblLSettle"></t:graphicImage>
														</f:facet>
														<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>
													</t:dataScroller>
													</CENTER>
													
												</td></tr>
											</table>
									</t:div>
												
												
											</t:div>
												<%--Other PAYMENTS  start--%>
											<t:div>&nbsp;</t:div>
										<table>
										  <tr>
										      <td >
										        <h:outputLabel value="#{msg['settlement.lbl.otherPayments']}" style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px;" />
										      </td>
										  </tr>
										</table>
											
											<t:div>
												<table id="imageTable" cellpadding="0" cellspacing="0" width="100%" style ="margin-right:4px;">
													<tr>
													<td><IMG id="image1" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_left")%>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG id="image2" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_mid")%>" class="TAB_PANEL_MID"/></td>
													<td><IMG id="image3" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_right")%>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>
												<t:div styleClass="contentDiv" style="width:99%">
													<t:dataTable id="dt3" var="paymentItem" rows="#{pages$Settlement.paginatorRows}" binding="#{pages$Settlement.paymentTableOther}" value="#{pages$Settlement.otherPayments}"  width="100%">
														
														<t:column id="select" style="width:5%;" >
															<f:facet name="header">
																<t:outputText id="selectTxt" value="#{msg['commons.select']}" />
															</f:facet>
															<t:selectBooleanCheckbox id="selectChk" value="#{paymentItem.selected}" rendered="#{paymentItem.paymentScheduleView.showSelect && paymentItem.paymentScheduleView.isReceived == 'N'  && paymentItem.paymentScheduleView.paymentScheduleId>0}"/>
												        </t:column>
														<t:column width="10%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymenttype']}"/></f:facet>
															<h:outputText value="#{paymentItem.paymentType}"/>
														</t:column>
														<t:column width="10%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymentdate']}"/></f:facet>
															<h:outputText value="#{paymentItem.dueDate}"/>
														</t:column>
														<t:column width="10%">
															<f:facet name="header"><h:outputText value="#{msg['paymentSchedule.description']}"/></f:facet>
															<h:outputText value="#{paymentItem.paymentScheduleView.description}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymentmethod']}"/></f:facet>
															<h:outputText value="#{paymentItem.paymentMethod}"/>
														</t:column>
														<t:column width="12%">
															<f:facet name="header"><h:outputText value="#{msg['commons.amount']}"/></f:facet>
															<h:outputText styleClass="A_RIGHT_NUM" value="#{paymentItem.amountVal}">
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
															</h:outputText>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['commons.status']}"/></f:facet>
															<h:outputText value="#{paymentItem.status}"/>
														</t:column>
														<t:column width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.receiptno']}"/></f:facet>
															<h:outputText value="#{paymentItem.recieptNo}"/>
														</t:column>
														<!-- hide this colume bcoz payment detail screen is not made yet -->
														<t:column width="10%" rendered="false">
															<f:facet name="header"><h:outputText value="#{msg['commons.action']}"/></f:facet>
															<t:commandLink actionListener="#{pages$Settlement.onPaymentDetail}" rendered="#{paymentItem.showDetail}">
																<t:graphicImage title="#{msg['commons.details']}" url="../resources/images/app_icons/replaceCheque.png">																	
																</t:graphicImage>
															</t:commandLink>
														</t:column>
													</t:dataTable>
												</t:div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:100%;" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$Settlement.recordSizeOtherPayments}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
												<CENTER>
													<t:dataScroller id="scroller3" for="dt3" paginator="true"  
														fastStep="1" paginatorMaxPages="#{pages$Settlement.paginatorMaxPages}" immediate="false"
														paginatorTableClass="paginator"
														renderFacetsIfSinglePage="true" 												
														paginatorTableStyle="grid_paginator" layout="singleTable" 
														paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
														paginatorActiveColumnStyle="font-weight:bold;"
														paginatorRenderLinkForActive="false" 
														pageIndexVar="pageNumber"
														styleClass="SCH_SCROLLER">
														<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblFOther"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFROther"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFOther"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblLOther"></t:graphicImage>
														</f:facet>
														<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>
													</t:dataScroller>
													</CENTER>
													
												</td></tr>
											</table>
									</t:div>
												
												
											</t:div>
											<%-- Other Payments End--%>
											<t:div>&nbsp;</t:div>
											<t:div>
												<t:panelGrid cellpadding="0" cellspacing="10" columns="6" width="70%">
													<h:outputLabel id="lblRefund" style="font-weight:bold;" styleClass="TABLE_LABEL" value="#{msg['settlement.payment.totalrefundamount']}"/>
												    <h:outputText styleClass="A_RIGHT_NUM" id="totalrefundAmount" value="#{pages$Settlement.contractBean.totalRefundAmount}">
												      	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
												    </h:outputText>
												    <h:outputLabel style="font-weight:bold;" styleClass="TABLE_LABEL" value="#{msg['settlement.payment.currency']}"/>
													<h:outputLabel id="lblDue" style="font-weight:bold;" styleClass="TABLE_LABEL" value="#{msg['settlement.payment.totaldueamount']}"/>
													<h:outputText  styleClass="A_RIGHT_NUM" id="totalDueAmount" value="#{pages$Settlement.contractBean.totalDueAmount}">
												      	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
												    </h:outputText>
												    <h:outputLabel style="font-weight:bold;" styleClass="TABLE_LABEL" value="#{msg['settlement.payment.currency']}"/>
												</t:panelGrid>
											</t:div>
											<t:div styleClass="BUTTON_TD">
											
											
												<h:commandButton  id="addPaymentSchedule" style="width:150px;" styleClass="BUTTON" action="#{pages$Settlement.btnAddPayments_Click}" value="#{msg['contract.AddpaymentSchedule']}" rendered="#{pages$Settlement.showAddPayment}"/>&nbsp;
												<h:commandButton id="collectPayment" style="width:150px;" styleClass="BUTTON" action="#{pages$Settlement.onCollectPayment}" value="#{msg['settlement.actions.collectpayment']}" rendered="#{pages$Settlement.showCollectPayment}" />&nbsp;
												<h:commandButton id="printsettlement" style="width:150px;" styleClass="BUTTON" onclick="if (!confirm('#{msg['settlement.actions.confirmSettlementPrint']}')) return false;" action="#{pages$Settlement.onPrintSettlement}" value="#{msg['settlement.actions.printsettlement']}" rendered="#{pages$Settlement.showPrintSettlement}"/>&nbsp;
												<h:commandButton id="printreceipt" style="width:150px;" styleClass="BUTTON" onclick="if (!confirm('#{msg['settlement.actions.confirmPaymentReceiptPrint']}')) return false;" action="#{pages$Settlement.onPrintReceipt}" value="#{msg['settlement.actions.printreceipt']}" rendered="#{pages$Settlement.showPrintReceipt}"/>&nbsp;
												<h:commandButton id="cancelPayment" style="width:150px;" styleClass="BUTTON"  action="#{pages$Settlement.onCancelPayment}" value="#{msg['settlement.actions.cancelPayment']}" rendered="false" />&nbsp;
											</t:div>
											<%--Cheques to be returned start--%>
											<t:div>&nbsp;</t:div>
											<t:div>&nbsp;</t:div>
										
										<table>
										  <tr>
										      <td >
										        <h:outputLabel value="#{msg['settlement.lbl.returnCheques']}" style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px;" />
										      </td>
										  </tr>
										</table>
											<t:div>
												<table id="imageTable" cellpadding="0" cellspacing="0" width="100%" style ="margin-right:4px;">
													<tr>
													<td><IMG id="image1" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_left")%>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG id="image2" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_mid")%>" class="TAB_PANEL_MID"/></td>
													<td><IMG id="image3" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_right")%>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>
												<t:div styleClass="contentDiv" style="width:99%">
													<t:dataTable id="dt2" var="paymentItem" rows="#{pages$Settlement.paginatorRows}" binding="#{pages$Settlement.paymentChequestToReturnTable}" value="#{pages$Settlement.paymentsChequesToBeReturned}"  width="100%">
														
														
														<t:column id = "returnChequDate" width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymentdate']}"/></f:facet>
															<h:outputText value="#{paymentItem.dueDate}"/>
														</t:column>
														<t:column id = "returnChequeMethod" width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.payment.paymentmethod']}"/></f:facet>
															<h:outputText value="#{paymentItem.paymentMethod}"/>
														</t:column>
														<t:column id = "returnChequeAmount" width="15%">
															<f:facet name="header"><h:outputText value="#{msg['commons.amount']}"/></f:facet>
															<h:outputText styleClass="A_RIGHT_NUM" value="#{paymentItem.amountVal}">
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
															</h:outputText>
														</t:column>
														<t:column id = "returnChequeStatus" width="15%">
															<f:facet name="header"><h:outputText value="#{msg['commons.status']}"/></f:facet>
															<h:outputText value="#{paymentItem.status}"/>
														</t:column>
													
														<t:column id = "HasWithDrawlPrinted" width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.withdrawlPrinted']}"/></f:facet>
															<h:outputText value="#{paymentItem.hasWithDrawPrinted== '1'? msg['settlement.withdrawlPrinted.yes']:msg['settlement.withdrawlPrinted.no']}"/>
														</t:column>
														<t:column id = "withDrawlPrintedBy" width="15%">
															<f:facet name="header"><h:outputText value="#{msg['settlement.withdrawlPrintedBy']}"/></f:facet>
															<h:outputText value="#{paymentItem.withDrawPrintedBy}"/>
														</t:column>
														<!-- hide this colume bcoz payment detail screen is not made yet -->
														<t:column width="10%">
															<f:facet name="header"></f:facet>
															<h:selectBooleanCheckbox value="#{paymentItem.selected}">
															</h:selectBooleanCheckbox>
														</t:column>
														<t:column width="10%" rendered="false">
															<f:facet name="header"><h:outputText value="#{msg['commons.action']}"/></f:facet>
															<t:commandLink actionListener="#{pages$Settlement.onPaymentDetail}" rendered="#{paymentItem.showDetail}">
																<t:graphicImage title="#{msg['commons.details']}" url="../resources/images/app_icons/replaceCheque.png">																	
																</t:graphicImage>
															</t:commandLink>
														</t:column>
													</t:dataTable>
												</t:div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:100%;" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$Settlement.recordSizeReturnCheque}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
												<CENTER>
													<t:dataScroller id="scroller2" for="dt2" paginator="true"  
														fastStep="1" paginatorMaxPages="#{pages$Settlement.paginatorMaxPages}" immediate="false"
														paginatorTableClass="paginator"
														renderFacetsIfSinglePage="true" 												
														paginatorTableStyle="grid_paginator" layout="singleTable" 
														paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
														paginatorActiveColumnStyle="font-weight:bold;"
														paginatorRenderLinkForActive="false" 
														pageIndexVar="pageNumber"
														styleClass="SCH_SCROLLER">
														<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblFReturn"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFRReturn"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFReturn"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblLReturn"></t:graphicImage>
														</f:facet>
														<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>
													</t:dataScroller>
													</CENTER>
													
												</td></tr>
											</table>
											
									</t:div>
										<t:div styleClass="BUTTON_TD">
											
											<h:messages></h:messages>
											<h:commandButton value="#{msg['commons.print']}" 
											action="#{pages$Settlement.printWithdrawlLetter}"
											styleClass="BUTTON">
											</h:commandButton>
											</t:div>
											
												
											</t:div>
											
										<%-- Cheques to be returned End--%>
										<%-- Attachment/Comment START--%>
											<t:div>&nbsp;</t:div>
											<t:div>&nbsp;</t:div>
											<t:div>
										<table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
										</table>	
						 				  <rich:tabPanel id="tabPanel" style="width:100%;height:320px;" headerSpacing="0">
						 				           
													<rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}" action= "#{pages$Settlement.tabAttachmentsComments_Click}">
															   <%@  include file="attachment/attachment.jsp"%>
													</rich:tab>
											        <rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}" action= "#{pages$Settlement.tabAttachmentsComments_Click}">
															   <%@ include file="notes/notes.jsp"%>
													</rich:tab>
				                            </rich:tabPanel>
                                        <table cellpadding="0" cellspacing="0"  style="width:100%">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
										</t:div>
										<%-- Attachment/Comment finish--%>
										
								
											
										</div>
									</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
	</html>
</f:view>
