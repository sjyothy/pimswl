<%-- 
  - Author: Jawwad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: this popup wil save the remarks of user while block/unblock the contract.
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%-- JAVA SCRIPT FUNCTIONS START --%>
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script type="text/javascript">

function closeWindow()
{
    
	window.opener.onMessageFromManage();
	window.close();
}

</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<body class="BODY_STYLE">

			<table width="100%" cellpadding="0" cellspacing="0" border="0">

				<tr width="100%">

					<td width="100%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel
										value="#{msg['endowmentExpenseType.heading.manage']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99.1%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" valign="top" nowrap="nowrap">
									<h:form id="searchFrm" style="width:100%;">
										<div class="SCROLLABLE_SECTION">

											<table border="0" class="layoutTable" width="90%"
												style="margin-left: 15px; margin-right: 15px;">
												<tr>

													<td>
														<h:messages></h:messages>
														<h:outputText id="errorMsg" escape="false"
															styleClass="ERROR_FONT"
															value="#{pages$manageEndowmentExpenseType.errorMessages}" />
													</td>

												</tr>
											</table>
											<div
												style="width: 97%; margin-left: 15px; margin-right: 15px;">
												<table cellpadding="1px" cellspacing="2px">
													<tr>
														<td>
															<h:outputLabel
																value="#{msg['endowmentExpenseType.lbl.typeNameEn']}">
															</h:outputLabel>

														</td>
														<td>
															<h:inputText id="typeNameEn"
																value="#{pages$manageEndowmentExpenseType.pageObject.typeNameEn}" />
														</td>
														<td>
															<h:outputLabel
																value="#{msg['endowmentExpenseType.lbl.typeNameAr']}">
															</h:outputLabel>

														</td>
														<td>
															<h:inputText id="typeNameAr"
																value="#{pages$manageEndowmentExpenseType.pageObject.typeNameAr}" />
														</td>
													</tr>
													<tr>
														<td>
															<h:outputLabel value="#{msg['commons.description']}">
															</h:outputLabel>

														</td>
														<td>
															<h:inputText id="description"
																value="#{pages$manageEndowmentExpenseType.pageObject.description}" />
														</td>
													</tr>

												</table>
											</div>
											<TABLE width="97%">
												<tr>
													<td class="BUTTON_TD" colspan="6">
														<h:commandButton styleClass="BUTTON" type="submit"
															action="#{pages$manageEndowmentExpenseType.onDone}"
															value="#{msg['commons.done']}" />
													</td>
												</tr>
											</TABLE>
									</h:form>
								</td>
							</tr>

						</table>
					</td>
				</tr>
			</table>


		</body>
	</html>
</f:view>