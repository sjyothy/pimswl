
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<script language="javascript" type="text/javascript">

            function RowDoubleClick(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
			{
			    
			  
			    window.opener.populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany);
			    
			    window.close();
			  
			}	
       </SCRIPT>



<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="Cache-Control" content="no-cache,must-revalidate,no-store">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />

			<script language="JavaScript" type="text/javascript"
				src="../resources/jscripts/commons.js"></script>

			<script language="JavaScript" type="text/javascript"
				src="../resources/jscripts/base64.js"></script>

			<script language="JavaScript" type="text/javascript"
				src="../resources/jscripts/jquery-1.8.2.js"></script>

			<script language="JavaScript" type="text/javascript"
				src="../resources/jscripts/errors.js"></script>

			<script language="JavaScript" type="text/javascript"
				src="../resources/jscripts/zfcomponent.js"></script>


			<script language="JavaScript" type="text/javascript"
				src="../resources/jscripts/jquery-ui-1.9.0.js"></script>


			<script language="JavaScript" type="text/javascript">
			
			function doReadPublicData(argument) 
			{
			    clearWindow();
			    var Ret = Initialize();
			    
			    if(Ret == false)
			        return;
			    
			    Ret = ReadPublicData(true, true, true, true, true, true);
			    
			    if(Ret == false)
			        return;
			     
			    var ef_idn_cn = GetEF_IDN_CN();
			    
			    //$("#ef_idn_cn").val(ef_idn_cn );
			    document.getElementById('formSearchPerson:ef_idn_cn').value=ef_idn_cn ;
			
			    var nonModData =GetEF_NonModifiableData();
			    $("#ef_non_mod_data").val(nonModData );
			    document.getElementById('formSearchPerson:ef_non_mod_data').value=nonModData;
			
			    var ef_mod_data = GetEF_ModifiableData();
			    $("#ef_mod_data").val(ef_mod_data);
			    document.getElementById('formSearchPerson:ef_mod_data').value=ef_mod_data;
			
			    var ef_home_address= GetEF_HomeAddressData();
			    $("#ef_home_address").val(ef_home_address);
			    document.getElementById('formSearchPerson:ef_home_address').value=ef_home_address;
			    
			    var ef_work_address= GetEF_WorkAddressData();
			    $("#ef_work_address").val(ef_work_address);
			    document.getElementById('formSearchPerson:ef_work_address').value=ef_work_address;
			    
			    
				document.getElementById('formSearchPerson:onScanEmiratesCard').onclick();			    
			    alert("Card Data Read!")
			    
			}
	
			function clearWindow()
			{
		            document.getElementById("formSearchPerson:txtfirstname").value="";
					document.getElementById("formSearchPerson:txtpassportNumber").value="";
					document.getElementById("formSearchPerson:txtresidenseVisaNumber").value="";
					document.getElementById("formSearchPerson:txtsocialSecNumber").value="";
					document.getElementById("formSearchPerson:txtContractNo").value="";
					document.getElementById("formSearchPerson:txtCellNum").value="";
			}
			
			function closeWindow()
			{
			
			  window.opener="x";
			  window.close();
			
			}
				
		    function closeWindow(){
			
			window.close();
			
			}
			function styleBlackListRow( id)
			{
			    alert( document.getElementById(id) );
				if( true)
				{
				  
				  document.getElementById(id).style.backgroundColor = '#f9a63c';
				}
				return true;
			}
       </SCRIPT>



		</head>
		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
					String userAgent = request.getHeader("user-agent");
					if (userAgent.indexOf("MSIE") != -1
							&& userAgent.indexOf("x64") != -1) {
			%>
			<object id="ZFComponent" width="0" height="0"
				classid="CLSID:502A94C0-E6CB-4910-846D-6F4F261E98C0"
				codebase="EIDA_ZF_ActiveX64.CAB">
				<strong style="color: red">ActiveX is not supported by this
					browser, please use Internet Explorer</strong>
			</object>
			<%
				} else {
			%>

			<object id="ZFComponent" width="0" height="0"
				classid="CLSID:502A94C0-E6CB-4910-846D-6F4F261E98C0"
				codebase="EIDA_ZF_ActiveX.CAB">
				<strong style="color: red">ActiveX is not supported by this
					browser, please use Internet Explorer</strong>
			</object>

			<%
				}
			%>

			<c:choose>
				<c:when test="${!pages$SearchPerson.isViewModePopUp}">
					<div class="containerDiv">
				</c:when>
			</c:choose>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">

				<tr>
					<c:choose>
						<c:when test="${!pages$SearchPerson.isViewModePopUp}">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</c:when>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!pages$SearchPerson.isViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>

					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['person.personList']}"
										styleClass="HEADER_FONT" />
								</td>

							</tr>

						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="3">
								</td>
								<td width="100%" height="450px" valign="top" nowrap="nowrap">
									<h:form id="formSearchPerson" style="width:100%">
										<div class="SCROLLABLE_SECTION">

											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td colspan="6">
															<h:outputText value="#{pages$SearchPerson.errorMessages}"
																escape="false" styleClass="ERROR_FONT"
																style="padding:10px;" />
															<h:inputHidden id="hdnPersonType"
																value="#{pages$SearchPerson.requestForPersonType}" />
															<h:inputHidden id="ef_idn_cn"
																value="#{pages$SearchPerson.ef_idn_cn}" />
															<h:inputHidden id="ef_mod_data"
																value="#{pages$SearchPerson.ef_mod_data}" />
															<h:inputHidden id="ef_non_mod_data"
																value="#{pages$SearchPerson.ef_non_mod_data}" />
															<h:inputHidden id="ef_sign_image"
																value="#{pages$SearchPerson.ef_sign_image}" />
															<h:inputHidden id="ef_home_address"
																value="#{pages$SearchPerson.ef_home_address}" />
															<h:inputHidden id="ef_work_address"
																value="#{pages$SearchPerson.ef_work_address}" />

															<h:commandLink id="onScanEmiratesCard"
																action="#{pages$SearchPerson.onScanEmiratesCard}" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 97%;">

												<div class="DETAIL_SECTION" style="width: 99.6%;">
													<h:outputLabel value="#{msg['inquiry.searchcriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">
																	<tr>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['person.Type']}:" />
																		</td>
																		<td width="25%">
																			<h:selectOneMenu id="selectPersonKind"
																				value="#{pages$SearchPerson.selectedPersonKind}">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItem
																					itemLabel="#{msg['tenants.tpye.company']}"
																					itemValue="1" />
																				<f:selectItem
																					itemLabel="#{msg['tenants.tpye.individual']}"
																					itemValue="0" />
																			</h:selectOneMenu>
																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['contract.unit.isBlackListed']}:" />
																		</td>
																		<td width="25%">
																			<h:selectBooleanCheckbox
																				style="width: 15%;height: 22px; border: 0px;"
																				value="#{pages$SearchPerson.isBlackListed}" />
																		</td>
																	</tr>
																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['person.Name']}:"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText maxlength="110" id="txtfirstname"
																				value="#{pages$SearchPerson.firstName}">
																			</h:inputText>
																			<h:commandLink
																				id="onScanEmiratesCardAndSearchByNameArabic"
																				action="#{pages$SearchPerson.onScanEmiratesCardAndSearchByNameArabic}">
																				<h:graphicImage
																					id="searchByEmiratesIdCardNameArabic"
																					style="MARGIN: 0px 0px -4px;"
																					title="#{msg['searchPerson.lbl.searchByEmiratesIdCardNameArabic']}"
																					url="../resources/images/app_icons/cardlogo.png" />
																			</h:commandLink>
																			<h:commandLink
																				id="onScanEmiratesCardAndSearchByNameEnglish"
																				action="#{pages$SearchPerson.onScanEmiratesCardAndSearchByNameEnglish}">
																				<h:graphicImage
																					id="searchByEmiratesIdCardNameEnglish"
																					style="MARGIN: 0px 0px -4px;"
																					title="#{msg['searchPerson.lbl.searchByEmiratesIdCardNameEnglish']}"
																					url="../resources/images/app_icons/cardlogo.png" />
																			</h:commandLink>
																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['customer.socialSecNumber']}:"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText maxlength="15" id="txtsocialSecNumber"
																				value="#{pages$SearchPerson.socialSecNumber}">
																			</h:inputText>
																			<h:commandLink
																				id="onScanEmiratesCardAndSearchByCardNum"
																				action="#{pages$SearchPerson.onScanEmiratesCardAndSearchByCardNum}">

																				<h:graphicImage id="searchByEmiratesIdNum"
																					style="MARGIN: 0px 0px -4px;"
																					title="#{msg['searchPerson.lbl.searchByEmiratesIdNum']}"
																					url="../resources/images/app_icons/cardlogo.png" />

																			</h:commandLink>

																		</td>

																	</tr>
																	<tr>
																		<td width="20%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['customer.passportNumber']}:"></h:outputLabel>
																		</td>
																		<td width="30%">
																			<h:inputText maxlength="15" id="txtpassportNumber"
																				value="#{pages$SearchPerson.passportNumber}">
																			</h:inputText>
																			<h:commandLink
																				id="onScanEmiratesCardAndSearchByPassport"
																				action="#{pages$SearchPerson.onScanEmiratesCardAndSearchByPassport}">
																				<h:graphicImage id="searchByPassportNum"
																					style="MARGIN: 0px 0px -4px;"
																					title="#{msg['searchPerson.lbl.searchByEmiratesIdPassportNum']}"
																					url="../resources/images/app_icons/cardlogo.png" />


																			</h:commandLink>

																		</td>
																		<td width="20%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['customer.residenseVisaNumber']}:"></h:outputLabel>
																		</td>
																		<td width="30%">
																			<h:inputText maxlength="15"
																				id="txtresidenseVisaNumber"
																				value="#{pages$SearchPerson.residenseVisaNumber}">
																			</h:inputText>
																		</td>



																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['tenants.license.licenseno']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText maxlength="50" id="txtLicenseno"
																				value="#{pages$SearchPerson.licenseNumber}"></h:inputText>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['tenants.license.issueplace']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="selectLicenseIssuePlace"
																				value="#{pages$SearchPerson.licenseSource}">
																				<f:selectItem itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$SearchPerson.countryList}" />
																			</h:selectOneMenu>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['addPerson.grpNumber']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText maxlength="100" id="txtGRPCustNo"
																				value="#{pages$SearchPerson.grpNumber}"></h:inputText>
																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.Contract.Number']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText maxlength="100" id="txtContractNo"
																				value="#{pages$SearchPerson.contractNumber}"></h:inputText>
																		</td>
																	</tr>
																	<tr>

																		<td width="20%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['SearchBeneficiary.cellNumber']}:"></h:outputLabel>
																		</td>
																		<td width="30%">
																			<h:inputText maxlength="15" id="txtCellNum"
																				value="#{pages$SearchPerson.cellNumber}">
																			</h:inputText>
																			<h:commandLink
																				id="onScanEmiratesCardAndSearchByCellNum"
																				action="#{pages$SearchPerson.onScanEmiratesCardAndSearchByCellNum}">
																				<h:graphicImage id="searchByCellNum"
																					style="MARGIN: 0px 0px -4px;"
																					title="#{msg['searchPerson.lbl.searchPerson.lbl.searchByEmiratesIdCellNum']}"
																					url="../resources/images/app_icons/cardlogo.png" />
																			</h:commandLink>
																		</td>
																		
																	<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['user.loginId']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:inputText maxlength="100" id="txtloginId"
																				value="#{pages$SearchPerson.loginID}"></h:inputText>
																		</td>																		
																	</tr>
																	<tr>
																		<td colspan="4">
																			&nbsp;
																		</td>
																	</tr>

																	<tr>

																		<td colspan="4" class="BUTTON_TD">
																			<h:commandButton type="submit" styleClass="BUTTON"
																				value="#{msg['commons.search']}"
																				action="#{pages$SearchPerson.doSearch}"></h:commandButton>
																			<h:commandButton styleClass="BUTTON" type="button"
																				value="#{msg['commons.clear']}"
																				onclick="javascript:clearWindow();" />

																			<h:commandButton type="button" styleClass="BUTTON"
																				value="#{msg['searchPerson.lbl.scanEmiratesCard']}"
																				onclick="javaScript:doReadPublicData();"></h:commandButton>


																			<h:commandButton type="submit" styleClass="BUTTON"
																				action="#{pages$SearchPerson.btnAdd_Click}"
																				value="#{msg['commons.Add']}" />

																			<h:commandButton styleClass="BUTTON" type="button"
																				onclick="javascript:closeWindow();"
																				value="#{msg['commons.cancel']}">
																			</h:commandButton>
																		</td>
																	</tr>
																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>

													</table>
												</div>
											</div>


											<div style="padding: 5px; width: 98%;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv">
													<t:dataTable id="test3"
														rows="#{pages$SearchPerson.paginatorRows}" width="100%"
														value="#{pages$SearchPerson.propertyInquiryDataList}"
														binding="#{pages$SearchPerson.dataTable}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rules="all" renderedIfEmpty="true"
														rowClasses="row1,row2"
														columnClasses="A_LEFT_GRID,A_LEFT_GRID,A_LEFT_GRID,A_LEFT_GRID,A_LEFT_GRID,A_LEFT_GRID"
														rowId="#{dataItem.personId}">


														<t:column id="col2" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader immediate="false" columnName="col2"
																	actionListener="#{pages$SearchPerson.sort}"
																	value="#{msg['person.Name']}" arrow="true">
																	<f:attribute name="sortField" value="firstName" />
																</t:commandSortHeader>

															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.personFullName}" />

														</t:column>
														<t:column id="grpCustomerNo" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader immediate="false"
																	columnName="grpCustomerNo"
																	actionListener="#{pages$SearchPerson.sort}"
																	value="#{msg['addPerson.grpNumber']}" arrow="true">
																	<f:attribute name="sortField" value="grpCustomerNo" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{dataItem.grpCustomerNo}" />
														</t:column>
														<t:column id="col5">
															<f:facet name="header">
																<t:commandSortHeader immediate="false" columnName="col5"
																	actionListener="#{pages$SearchPerson.sort}"
																	value="#{msg['customer.residenseVisaNumber']}"
																	arrow="true">
																	<f:attribute name="sortField"
																		value="residenseVisaNumber" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText styleClass="A_LEFT" title=""
																value="#{dataItem.residenseVisaNumber}" />
														</t:column>
														<t:column id="col7">


															<f:facet name="header">
																<t:commandSortHeader immediate="false" columnName="col7"
																	actionListener="#{pages$SearchPerson.sort}"
																	value="#{msg['customer.socialSecNumber']}" arrow="true">
																	<f:attribute name="sortField" value="socialSecNumber" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																title="#{dataItem.socialSecNumber}"
																value="#{dataItem.socialSecNumber}" />
														</t:column>
														<t:column id="colCell">

															<f:facet name="header">
																<t:commandSortHeader immediate="false"
																	columnName="colCell"
																	actionListener="#{pages$SearchPerson.sort}"
																	value="#{msg['customer.cell']}" arrow="true">
																	<f:attribute name="sortField" value="cellNumber" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																title="#{dataItem.cellNumber}"
																value="#{dataItem.cellNumber}" />
														</t:column>
														
														
														<t:column id="colLoginId">

															<f:facet name="header">
																<t:outputText value="#{msg['user.loginId']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																title="#{dataItem.loginId}"
																value="#{dataItem.loginId}" />
														</t:column>
														
																												
														<t:column id="colEID">

															<f:facet name="header">
																<t:outputText value="#{msg['person.msg.SynchedWithEid']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																
																value="#{! empty dataItem.synchedWithEID && dataItem.synchedWithEID ==1 ?msg['commons.true']:msg['commons.false']}" />
														</t:column>
														<t:column id="col8">
															<f:facet name="header">

																<t:outputText value="#{msg['commons.select']}" />
															</f:facet>
															<h:commandLink
																action="#{pages$SearchPerson.sendSinglePersonToParent}"
																rendered="#{pages$SearchPerson.isRenderSendToParent && !pages$SearchPerson.selectManyPersonType && pages$SearchPerson.isViewModePopUp}">
																<h:graphicImage style="width: 16;height: 16;border: 0"
																	alt="#{msg['commons.select']}"
																	url="../resources/images/select-icon.gif" />
															</h:commandLink>
															<t:selectBooleanCheckbox
																rendered="#{pages$SearchPerson.isRenderSendToParent && pages$SearchPerson.selectManyPersonType && pages$SearchPerson.isViewModePopUp}"
																value="#{dataItem.selected}" />
																		
																			
																		&nbsp;&nbsp;&nbsp;
																		<h:commandLink id="cmdEdit"
																action="#{pages$SearchPerson.cmdEdit_Click}">
																<h:graphicImage style="width: 16;height: 16;border: 0"
																	alt="#{msg['commons.select']}"
																	url="../resources/images/edit-icon.gif" />
															</h:commandLink>


															<t:outputLabel
																rendered="#{! empty dataItem.isBlacklisted && dataItem.isBlacklisted ==1 ?true:false}"
																style="font-weight:bold;color:red;"
																value="#{msg['person.blacklisted']}">
															</t:outputLabel>
														</t:column>
													</t:dataTable>
												</div>

												<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:99%;#width:99%;">
													<table cellpadding="0" cellspacing="0" width="99%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$SearchPerson.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">

																<t:div styleClass="PAGE_NUM_BG" style="#width:20%">
																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																			</td>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{pages$SearchPerson.currentPage}" />
																			</td>
																		</tr>
																	</table>
																</t:div>
																<TABLE border="0" class="SCH_SCROLLER">
																	<tr>
																		<td>
																			<t:commandLink
																				action="#{pages$SearchPerson.pageFirst}"
																				disabled="#{pages$SearchPerson.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$SearchPerson.pagePrevious}"
																				disabled="#{pages$SearchPerson.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFR"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:dataList value="#{pages$SearchPerson.pages}"
																				var="page">
																				<h:commandLink value="#{page}"
																					actionListener="#{pages$SearchPerson.page}"
																					rendered="#{page != pages$SearchPerson.currentPage}" />
																				<h:outputText value="<b>#{page}</b>" escape="false"
																					rendered="#{page == pages$SearchPerson.currentPage}" />
																			</t:dataList>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$SearchPerson.pageNext}"
																				disabled="#{pages$SearchPerson.firstRow + pages$SearchPerson.rowsPerPage >= pages$SearchPerson.totalRows}">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$SearchPerson.pageLast}"
																				disabled="#{pages$SearchPerson.firstRow + pages$SearchPerson.rowsPerPage >= pages$SearchPerson.totalRows}">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblL"></t:graphicImage>
																			</t:commandLink>
																		</td>
																	</tr>
																</TABLE>
															</td>
														</tr>
													</table>
												</t:div>

											</div>
											<table width="98%">
												<tr>
													<td>
														&nbsp;&nbsp;&nbsp;
													</td>
													<td align="center" class="BUTTON_TD">
														<h:commandButton styleClass="BUTTON" style="width:125px"
															value="#{msg['paidFacilities.sendToContract']}"
															action="#{pages$SearchPerson.sendInfoToParent}"
															rendered="#{pages$SearchPerson.selectManyPersonType && pages$SearchPerson.isViewModePopUp}" />

													</td>
												</tr>
											</table>

										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>

			</td>
			</tr>
			</table>
			<c:choose>
				<c:when test="${!pages$SearchPerson.isViewModePopUp}">
					</div>
				</c:when>
			</c:choose>

		</body>
	</html>
</f:view>

