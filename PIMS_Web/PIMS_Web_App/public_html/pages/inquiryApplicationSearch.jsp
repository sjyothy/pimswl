


<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
		
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
		<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>	
	
		<script language="javascript" type="text/javascript">
  
    var selectedUnits;


	function clearWindow()
	{
	       
	       document.getElementById("formPerson:txtUnitNumber").value="";
		    document.getElementById("formPerson:txtRentMin").value="";
		    document.getElementById("formPerson:txtRentMax").value="";
		    document.getElementById("formPerson:txtBedsMin").value="";
		    document.getElementById("formPerson:txtBedsMax").value="";
		    
		   
		    
		     var cmbUnitType=document.getElementById("formPerson:selectUnitType");
		     cmbUnitType.selectedIndex = 0;
		    
		    var cmbUnitUsage=document.getElementById("formPerson:selectUnitUsage");
		     cmbUnitUsage.selectedIndex = 0;
		    
		    var cmdCountry=document.getElementById("formPerson:country");
		     cmdCountry.selectedIndex = 0;
		     
		      var cmdCity=document.getElementById("formPerson:state");
		     cmdCity.selectedIndex = 0;
		    
		    var cmdState=document.getElementById("formPerson:selectcity");
		     cmdState.selectedIndex = 0;
	}

  function closeWindow(){
   window.close();
     
     }
    
    	
    </script>

		<%
			response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1

			response.setHeader("Pragma", "no-cache"); //HTTP 1.0

			response.setDateHeader("Expires", 0); //prevents caching at the proxy server
		%>



	</head>
	<body dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">

				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
    <td colspan="2">
        <jsp:include page="header.jsp"/>
    </td>
    </tr>

<tr width="100%">
    <td class="divLeftMenuWithBody" width="17%">
        <jsp:include page="leftmenu.jsp"/>
    </td>
    <td width="83%" height="505px" valign="top" class="divBackgroundBody">
 <table width="99.2%" class="greyPanelTable" cellpadding="0"
					cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
								<h:outputLabel value="#{msg['inquiryApplication.header']}" styleClass="HEADER_FONT"/>
						</td>
						<td width="100%">
							&nbsp;
						</td>
					</tr>
				</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									
									<td width="100%" height="440px" valign="top" nowrap="nowrap">
										
									<div class="SCROLLABLE_SECTION" style="height:420px;#height:445px;">	
										<h:form id="formPerson" style="width:97%;">
									<div style = "padding-left:10px;">	
										<table border="0" class="layoutTable">
												<tr>
      									    	      <td colspan="6">
														<h:outputText value="#{pages$inquiryApplicationSearch.errorMessages}" escape="false" styleClass="ERROR_FONT" />
														<h:outputText value="#{pages$inquiryApplicationSearch.successMessages}" escape="false" styleClass="INFO_FONT" />
													  </td>
												</tr>
									     </table>
									</div>     
                                 <div class= "MARGIN">
				                   <table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
				                  </table>
				 <div class="DETAIL_SECTION">
             		<h:outputLabel value="#{msg['inquiry.searchcriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>				
							<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER" width="100%" >
												<tr>
												    <td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.applicationNumber']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText id="txtApplicationNumber" value="#{pages$inquiryApplicationSearch.applicationNumber}">
												        </h:inputText>
													</td>
													<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.status']}:"></h:outputLabel>
													</td>
													<td >
														<h:selectOneMenu  id="selectApplicationStatus" style="width: 122px" value="#{pages$inquiryApplicationSearch.selectOneApplicationStatus}" >
															 <f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1"/>
															 <f:selectItems value="#{pages$inquiryApplicationSearch.requestStatusList}"/>
														</h:selectOneMenu>
													</td>
												</tr>
												<tr>
												    <td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.receivedBy']}:"></h:outputLabel>
													</td>
													<td >                                                                                                                     
														<h:selectOneMenu  id="selectApplicationMethod" style="width: 122px" value="#{pages$inquiryApplicationSearch.selectOneApplicationMethod}" >
															 <f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$ApplicationBean.inquiryMethod}"/>
														</h:selectOneMenu>
													</td>
													 <td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.priority']}:"></h:outputLabel>
													</td>
													<td >
														<h:selectOneMenu  id="selectApplicationPriority" style="width: 122px" value="#{pages$inquiryApplicationSearch.selectOneApplicationPriority}" >
															 <f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$ApplicationBean.inquiryPriority}"/>
														</h:selectOneMenu>
													</td>
													
											   </tr>
											   <tr>
	                                               <td>
														<h:outputLabel styleClass="LABEL"  value="#{msg['inquiryApplication.inquirerName']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText id="txtInquirerName" value="#{pages$inquiryApplicationSearch.inquirerName}">
												        
												        </h:inputText>
													</td>    
												    <td>
														<h:outputLabel styleClass="LABEL"  value="#{msg['inquiryApplication.cellPhone']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText id="txtCellPhone" value="#{pages$inquiryApplicationSearch.cellPhone}">
												        
												        </h:inputText>
													</td>    
											   </tr>
											   <tr>
												   	<td>
												       <h:outputLabel value="#{msg['inquiryApplication.inquiryDateFrom']} :"></h:outputLabel>
											        </td>
											        <td>                                                                                      
														 <rich:calendar  id="applicattionDateFrom" value="#{pages$inquiryApplicationSearch.applicationDateFrom}" 
								                          popup="true" datePattern="#{pages$inquiryApplicationSearch.dateFormat}" showApplyButton="false"
								                          locale="#{pages$inquiryApplicationSearch.locale}" 
								                          enableManualInput="false" cellWidth="24px" 
								                          />
											        </td>
											        <td>
												       <h:outputLabel value="#{msg['inquiryApplication.inquiryDateTo']} :"></h:outputLabel>
											        </td>
											        <td>
														 <rich:calendar  id="applicattionDateTo" value="#{pages$inquiryApplicationSearch.applicationDateTo}" 
								                          popup="true" datePattern="#{pages$inquiryApplicationSearch.dateFormat}" showApplyButton="false"
								                          locale="#{pages$inquiryApplicationSearch.locale}" 
								                          enableManualInput="false" cellWidth="24px" 
								                          />
											        </td>
											 </tr>
												
												<tr>
												     <td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.personIdNumber']}:"></h:outputLabel>
												     </td>
												     <td>
												        <h:inputText id="txtPersonIdNumber" value="#{pages$inquiryApplicationSearch.personIdNumber}">
												        </h:inputText>
											         </td>
								                     <td>
															<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.passportNumber']}:"></h:outputLabel>
													 </td>
													 <td>
													        <h:inputText id="txtPassportNumber" value="#{pages$inquiryApplicationSearch.passportNumber}">
													        </h:inputText>
												     </td>
												</tr>
												<tr> 
												    <td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.unitNumber']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText id="txtUnitNumber" value="#{pages$inquiryApplicationSearch.unitNumber}">
												        </h:inputText>
													</td>
													<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.unitType']}:"></h:outputLabel>
													</td>
													<td >
														<h:selectOneMenu  id="selectUnitType" style="width: 122px" value="#{pages$inquiryApplicationSearch.selectOneUnitType}" >
															 <f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$ApplicationBean.unitType}"/>
														</h:selectOneMenu>
													</td>
												</tr>
												<tr> 
												    <td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.propertyName']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText id="txtPropertyName" value="#{pages$inquiryApplicationSearch.propertyName}">
												        </h:inputText>
													</td>
													<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.propertyType']}:"></h:outputLabel>
													</td>
													<td >
														<h:selectOneMenu  id="selectPropertyType" style="width: 122px" value="#{pages$inquiryApplicationSearch.selectOnePropertyType}" >
															 <f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1"/>
															<f:selectItems value="#{pages$ApplicationBean.propertyTypeList}"/>
														</h:selectOneMenu>
													</td>
												</tr>
												<tr>
												    <td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.numberOfRooms']}:"></h:outputLabel>
												   </td>
												   <td>
												        <h:inputText id="txtRoomsMin" value="#{pages$inquiryApplicationSearch.noOfBedsMin}">
												        </h:inputText>
											       </td>
											       <td>
												        <h:outputLabel  styleClass="LABEL" value="#{msg['inquiryApplication.propertyArea']}:"></h:outputLabel>
												     </td>   
												        <td>
															<h:selectOneMenu id="selectPropertyArea" style="width: 122px;" value="#{pages$inquiryApplicationSearch.selectOnePropertyArea}"> 
																<f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems value="#{pages$inquiryApplicationSearch.propertyAreaList}"/>
															</h:selectOneMenu>
														</td>
												</tr>
												<tr>
											   	     <td>
														<h:outputLabel styleClass="LABEL"  value="#{msg['inquiryApplication.fromRentValue']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText id="txtRentMin" value="#{pages$inquiryApplicationSearch.rentValueMin}">
												        
												        </h:inputText>
													</td>
												
													<td>
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.toRentValue']}:"></h:outputLabel>
													</td>
													<td >
												        <h:inputText id="txtRentMax" value="#{pages$inquiryApplicationSearch.rentValueMax}">
												        
												        </h:inputText>
													</td>
												</tr>
												
												<tr>
													
													<td colspan="6" class="BUTTON_TD">
														<h:commandButton styleClass="BUTTON" type="submit"
															value="#{msg['commons.search']}" 
															action="#{pages$inquiryApplicationSearch.doSearch}">
														</h:commandButton>
														<h:commandButton styleClass="BUTTON" type="button"
															onclick="javascript:clearWindow();"
															value="#{msg['commons.reset']}" >
														</h:commandButton>
														<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}"
												         actionListener="#{pages$inquiryApplicationSearch.cancel}"
												         onclick="if (!confirm('#{msg['auction.cancelConfirm']}')) return false"
												         tabindex="16"/>

													</td>
												</tr>

											</table>
											</div>
											</div>


											 <div style="width:99%;padding-left:8px;">		    
                   		                       <div class="imag">&nbsp;</div>
											
											<div class="contentDiv">


												<t:dataTable id="personGrid"
													value="#{pages$inquiryApplicationSearch.propertyInquiryDataList}"
													binding="#{pages$inquiryApplicationSearch.dataTable}" rows="4"
													width="100%" preserveDataModel="false" preserveSort="false"
													var="dataItem" rowClasses="row1,row2" rules="all"
													renderedIfEmpty="true">

													<t:column id="applicationCol"  sortable="true">

														<f:facet name="header">

															<t:outputText value="#{msg['inquiryApplication.applicationNumber']}" />

														</f:facet>
														<t:outputText styleClass="A_LEFT"  value="#{dataItem.applicationNumber}" />
													</t:column>
													
													
													<t:column id="col1"  sortable="true">

														<f:facet name="header">

															<t:outputText value="#{msg['inquiryApplication.inquirerName']}" />

														</f:facet>
														<t:outputText styleClass="A_LEFT"  value="#{dataItem.personView.personFullName}" />
													</t:column>
                                                    <t:column id="col10"  sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['inquiry.inquiryDate']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.inquiriesDate}">
														<f:convertDateTime pattern="#{pages$inquiryApplicationSearch.dateFormat}" timeZone="#{pages$inquiryApplicationSearch.timeZone}" />
												    </t:outputText>
										            </t:column> 
													<t:column id="col4" >

														<f:facet name="header">

															<t:outputText value="#{msg['inquiryApplication.personIdNumber']}" />
														</f:facet>
														<t:outputText styleClass="A_LEFT" value="#{dataItem.personView.socialSecNumber}" />
													</t:column>

											         <t:column id="col9" sortable="true" >
														<f:facet name="header">
															<t:outputText value="#{msg['inquiry.priority']}" />
														</f:facet>
														<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.priorityEn}" />
													</t:column>
													<t:column id="GRequestStatusEn"  rendered="#{pages$inquiryApplicationSearch.isEnglishLocale}" >
													<f:facet name="header">
														<t:outputText value= "#{msg['commons.status']}" />
													</f:facet>
													<t:outputText id="ColRequestStatusEn" styleClass="A_LEFT" value="#{dataItem.applicationStatusEn}"
													style="white-space: normal;" />
												</t:column>
												
												<t:column id="GRequestStatusAr" rendered="#{pages$inquiryApplicationSearch.isArabicLocale}"  >
													<f:facet name="header">
														<t:outputText value= "#{msg['commons.status']}" />
													</f:facet>
													<t:outputText id="ColRequestStatusAr" styleClass="A_LEFT" value="#{dataItem.applicationStatusAr}" 
													style="white-space: normal;"/>
												</t:column>
							                         
													<t:column id="col11" >

														<f:facet name="header">
															<t:outputText value="#{msg['contact.email']}" />
														</f:facet>
														<h:selectBooleanCheckbox id="email" value="#{dataItem.selectedEmail}" disabled="#{dataItem.personView.isEmail}" />
														
													</t:column>
											        <t:column id="col12" >
														<f:facet name="header">
															<t:outputText value="#{msg['commons.sms']}" />
														</f:facet>
														<h:selectBooleanCheckbox id="sms" value="#{dataItem.selectedSms}" disabled="#{dataItem.personView.isSms}" />
														
													</t:column>
													
													<t:column>
														<f:facet name="header">
															<t:outputText value="#{msg['commons.action']}" />
														</f:facet>				
														<h:commandLink action="#{pages$inquiryApplicationSearch.onFindMatchingProperties}">
															<h:graphicImage title="#{msg['searchInquiry.actions.viewMatchingProperties']}" url="../resources/images/detail-icon.gif" />
														</h:commandLink>										
													</t:column>

												</t:dataTable>

												
											</div>
											<div class="contentDivFooter" style="width: 99%">
												<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$inquiryApplicationSearch.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
											<CENTER>
												<t:dataScroller id="scroller" for="personGrid" paginator="true"
													fastStep="1" paginatorMaxPages="#{pages$inquiryApplicationSearch.paginatorMaxPages}" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 
												    pageIndexVar="pageNumber"
												    styleClass="SCH_SCROLLER"
												    paginatorActiveColumnStyle="font-weight:bold;"
												    paginatorRenderLinkForActive="false" 
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

													<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
															<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>
												</t:dataScroller>
												</CENTER>
												</td></tr>
												</table>
                                           </div>
								   </div>
											

											<table width="98%" border="0">
												<tr>
													
													<td colspan="6" class="BUTTON_TD">
													
														<h:commandButton type="submit" styleClass="BUTTON" style="width: 120px" 
													         value="#{msg['commons.sendnotification']}" 
													         action="#{pages$inquiryApplicationSearch.generateNotification}"/>
													         
													
													</td>

												</tr>
											</table>


										</h:form>
										</div>
										</td>
										</tr>
										</table>
										</td>
										</tr>
										<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
										</table>
										
			</body>
		</f:view>
</html>
