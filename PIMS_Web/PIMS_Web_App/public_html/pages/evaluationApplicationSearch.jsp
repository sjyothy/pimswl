<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Property Evaluation Requests
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="javascript" type="text/javascript">
	
</script>
<script language="JavaScript" type="text/javascript">
	    function resetValues()
      	{
      	    document.getElementById("searchFrm:requestNumber").value="";
      	    document.getElementById("searchFrm:applicationStatuses").selectedIndex=0;
      	    $('searchFrm:requestDateFrom').component.resetSelectedDate();
			$('searchFrm:requestDateTo').component.resetSelectedDate();
			document.getElementById("searchFrm:propertyName").value="";
			document.getElementById("searchFrm:propertyTypeCombo").selectedIndex=0;
			document.getElementById("searchFrm:propertyOwnershipTypeCombo").value="";
			document.getElementById("searchFrm:landNumber").value="";
        }
        
       function submitForm()
	   {
          document.getElementById('searchFrm').submit();
	   }
	  
	   function openLoadReportPopup(pageName) {
	   		var screen_width = screen.width;
			var screen_height = screen.height;
        	var popup_width = screen_width-250;
        	var popup_height = screen_height-500;
        	var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        	window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	   }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			 <meta http-equiv="pragma" content="no-cache">
			 <meta http-equiv="cache-control" content="no-cache">
			 <meta http-equiv="expires" content="0">
			 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
			 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
	</head>

	<body class="BODY_STYLE">
	      <div class="containerDiv">
			<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
			%>
	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
										<h:outputLabel value="#{msg['evaluationApplication.search.heading']}" styleClass="HEADER_FONT"/>
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td width="100%" height="466px" valign="top" >
									<div class="SCROLLABLE_SECTION  AUC_SCH_SS">
									<h:form id="searchFrm" style="WIDTH: 97.6%;">
										<table border="0" class="layoutTable">
											<tr>
												<td>
													<h:outputText value="#{pages$evaluationApplicationSearch.errorMessages}"  escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$evaluationApplicationSearch.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
										<div class="MARGIN"> 
										<table cellpadding="0" cellspacing="0" width="100%">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
											<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
										<div class="DETAIL_SECTION">
											<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
											<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
												<tr>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['application.number']}:"></h:outputLabel>
												</td>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<h:inputText id="requestNumber"
														value="#{pages$evaluationApplicationSearch.searchFilterView.requestNumber}"
														style="width: 186px;" maxlength="20"></h:inputText>
												</td>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['application.status']}:"></h:outputLabel>
												</td>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<h:selectOneMenu id="applicationStatuses" binding="#{pages$evaluationApplicationSearch.applicationStatusCombo}" style="width: 192px;" required="false" value="#{pages$evaluationApplicationSearch.searchFilterView.applicationStatusId}">
														<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
														<f:selectItems value="#{pages$ApplicationBean.requestStatusList}" />
													</h:selectOneMenu>
												</td>
											</tr>
											<tr>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['application.fromDate']}:"></h:outputLabel>
												</td>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<rich:calendar id="requestDateFrom" inputStyle="width: 186px; height: 18px" value="#{pages$evaluationApplicationSearch.searchFilterView.requestDateFrom}"
							                        locale="#{pages$evaluationApplicationSearch.locale}" popup="true" datePattern="#{pages$evaluationApplicationSearch.dateFormat}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px" style="width:186px; height:16px"/>
												</td>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['application.toDate']}:" />
												</td>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<rich:calendar id="requestDateTo" inputStyle="width: 186px; height: 18px" value="#{pages$evaluationApplicationSearch.searchFilterView.requestDateTo}"
							                        locale="#{pages$evaluationApplicationSearch.locale}" popup="true" datePattern="#{pages$evaluationApplicationSearch.dateFormat}" showApplyButton="false" enableManualInput="false" cellWidth="24px" cellHeight="22px" style="width:186px; height:16px"/>
							                    </td>
											</tr>
											<tr>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['property.name']}:"></h:outputLabel>
												</td>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<h:inputText id="propertyName"
														value="#{pages$evaluationApplicationSearch.searchFilterView.propertyName}"
														style="width: 186px;"></h:inputText>
												</td>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['property.type']}:"></h:outputLabel>
												</td>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<h:selectOneMenu id="propertyTypeCombo" binding="#{pages$evaluationApplicationSearch.propertyTypeCombo}" style="width: 192px;" required="false" value="#{pages$evaluationApplicationSearch.searchFilterView.propertyTypeId}">
														<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
														<f:selectItems value="#{pages$ApplicationBean.propertyTypeList}" />
													</h:selectOneMenu>
												</td>
											</tr>
											<tr>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['applicationDetails.ownershipType']}:"></h:outputLabel>
												</td>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<h:selectOneMenu id="propertyOwnershipTypeCombo" binding="#{pages$evaluationApplicationSearch.propertyOwnershipTypeCombo}" style="width: 192px;" required="false" value="#{pages$evaluationApplicationSearch.searchFilterView.propertyOwnershipTypeId}">
														<f:selectItem itemValue="0" itemLabel="#{msg['commons.All']}" />
														<f:selectItems value="#{pages$ApplicationBean.propertyOwnershipType}" />
													</h:selectOneMenu>
												</td>
												<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
													<h:outputLabel styleClass="LABEL" value="#{msg['property.landNumber']}:"></h:outputLabel>
												</td>
												 <td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px;">
														<h:inputText id="landNumber"
															value="#{pages$evaluationApplicationSearch.searchFilterView.landNumber}"
															style="width: 186px;" maxlength="20">
														</h:inputText>
												 </td>
											</tr>
											<tr>
												<td class="BUTTON_TD JUG_BUTTON_TD" colspan="4">
												<table cellpadding="1px" cellspacing="1px">
												<tr>
													<td>
														<pims:security screen="Pims.PropertyManagement.EvaluationSearch.Search" action="create">
					     									<h:commandButton styleClass="BUTTON" value="#{msg['commons.search']}" action="#{pages$evaluationApplicationSearch.searchRequests}" style="width: 80px" tabindex="7"></h:commandButton>
					     								</pims:security>
													</td>
													<td>
														<pims:security screen="Pims.PropertyManagement.EvaluationSearch.AddRequest" action="create">
					     									<h:commandButton styleClass="BUTTON" value="#{msg['evaluationApplication.search.addRequestButton']}" action="#{pages$evaluationApplicationSearch.onAddRequest}" style="width: 165px" tabindex="7"></h:commandButton>
					     								</pims:security>
													</td>
													<td>
				     									<h:commandButton styleClass="BUTTON" value="#{msg['commons.clear']}" onclick="javascript:resetValues();" style="width: 80px" tabindex="7"></h:commandButton>
													</td>
												</tr>
												</table>	
												</td>
											</tr>
										</table>
									
										</div>	
										</div>
										<div style="padding-bottom:7px;padding-left:10px;padding-right:7px;padding-top:7px;">
										<div class="imag">&nbsp;</div>
										<div class="contentDiv">
											<t:dataTable id="dt1"
												value="#{pages$evaluationApplicationSearch.dataList}"
												binding="#{pages$evaluationApplicationSearch.dataTable}" 
												rows="#{pages$evaluationApplicationSearch.paginatorRows}"
												preserveDataModel="false" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												width="100%">

												<t:column id="requestNumberCol" width="15%" sortable="true">
													<f:facet  name="header">
														<t:outputText value="#{msg['application.number.gridHeader']}" />
													</f:facet>
													<t:outputText value="#{dataItem.requestNumber}" />
												</t:column>
												<t:column id="requestDateCol" width="14%" sortable="true">
													<f:facet  name="header">
														<t:outputText value="#{msg['application.date.gridHeader']}" />
													</f:facet>
													<t:outputText value="#{dataItem.requestDateStr}" />
												</t:column>
												<t:column id="propertyNameCol" width="20%" sortable="true">
													<f:facet  name="header">
														<t:outputText value="#{msg['property.name']}" />
													</f:facet>
													<t:outputText value="#{dataItem.propertyName}" />
												</t:column>
												<t:column id="landNumberCol" width="12%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['applicationDetails.ownershipType']}" />
													</f:facet>
													<t:outputText value="#{dataItem.ownershipType}" />
												</t:column>
												<t:column id="receivedDateCol" width="12%" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['evaluationApplication.search.grid.receivedDate']}" />
													</f:facet>
													<t:outputText value="#{dataItem.receivedDateStr}" />
												</t:column>
												<t:column id="applicationStatusCol" width="12%" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['application.status.gridHeader']}" />
													</f:facet>
													<t:outputText value="#{dataItem.applicationStatus}" />
												</t:column>
												<t:column id="actionCol" sortable="false" width="15%">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.action']}" />
													</f:facet>
													<t:commandLink
														action="#{pages$evaluationApplicationSearch.onView}"
														rendered="#{dataItem.showView}" >
														<h:graphicImage id="viewIcon" title="#{msg['commons.view']}" url="../resources/images/app_icons/Approve-Request.png" />&nbsp;
													</t:commandLink>
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													<t:commandLink 
														action="#{pages$evaluationApplicationSearch.onEdit}"
														rendered="#{dataItem.showEdit}" >
														<h:graphicImage id="editIcon" title="#{msg['commons.edit']}" url="../resources/images/app_icons/Provide-Tecnical-Comments.png" />&nbsp;
													</t:commandLink>
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													<t:commandLink 
														action="#{pages$evaluationApplicationSearch.onSiteVisit}"
														rendered="#{dataItem.showSiteVisit}" >
													<h:graphicImage id="siteVisitIcon" title="#{msg['evaluationApplication.search.toolTips.siteVisit']}" url="../resources/images/app_icons/Request-detail.png" />&nbsp;
													</t:commandLink>
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													<t:commandLink 
														action="#{pages$evaluationApplicationSearch.onEvaluate}"
														rendered="#{dataItem.showEvaluate}" >
													<h:graphicImage id="evaluateIcon" title="#{msg['evaluationApplication.search.tooltips.evaluate']}" url="../resources/images/app_icons/Request-detail.png" />&nbsp;
													</t:commandLink>
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													<t:commandLink 
														action="#{pages$evaluationApplicationSearch.onCancel}"
														rendered="#{dataItem.showCancel}" >
													<h:graphicImage id="cancelIcon" title="#{msg['commons.cancel']}" url="../resources/images/app_icons/Request-detail.png" />&nbsp;
													</t:commandLink>
													<t:outputLabel value=" " rendered="true"></t:outputLabel>
													<t:commandLink 
														onclick="if (!confirm('#{msg['evaluationApplication.search.confirm.delete']}')) return"
														action="#{pages$evaluationApplicationSearch.onDelete}"
														rendered="#{dataItem.showDelete}" >
													<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />&nbsp;
													</t:commandLink>
													<t:commandLink
						rendered="#{pages$evaluationApplicationSearch.propertyUnitEvaluationReportViewable}"
						action="#{pages$evaluationApplicationSearch.viewPropertyUnitEvaluationReport}">
						<h:graphicImage title="#{msg['report.evaluation.label.evaluation']}"
						url="../resources/images/detail-icon.gif" />
													</t:commandLink>
												</t:column>
											</t:dataTable>
										</div>
										<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99%;" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$evaluationApplicationSearch.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
											<CENTER>
												<t:dataScroller id="scroller" for="dt1" paginator="true"
													fastStep="1" paginatorMaxPages="#{pages$evaluationApplicationSearch.paginatorMaxPages}" immediate="false"
													paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true" 
												    pageIndexVar="pageNumber"
												    styleClass="SCH_SCROLLER"
												    paginatorActiveColumnStyle="font-weight:bold;"
												    paginatorRenderLinkForActive="false" 
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

								                    	<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
													
													<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>
													
												</t:dataScroller>
												</CENTER>
												</td>
                                        		</tr>
											</table>
											</t:div>
                                           </div>
                                           </div>
                                           
									</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>	
	</body>
</html>
</f:view>