


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script type="text/javascript">
		function disableButtons(control,siblingLevel)
		{
			var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
			
			if(siblingLevel==2)
			{
				control.nextSibling.nextSibling.onclick();
			}
			else 
			{
				control.nextSibling.onclick();
			}
			return 
		
		}
		function populateAsset(aasetDesc,assetNameEn,assetNameAr,assetType,fileNumber,personName,assetId,status)
		{
			document.getElementById("formRequest:hdnAssetId").value = assetId;
			document.getElementById("formRequest:lnkAssetId").onclick();
		}
		function   showAssetSearchPopup()
		{
		   var screen_width = 1024;
		   var screen_height = 470;
		   var screen_top = screen.top;
		   
		   var fileId  = '';
		   if( document.getElementById("formRequest:hdnFileId").value.length > 0 )
		   {
		   		fileId += 'INH_FILE_ID='+document.getElementById("formRequest:hdnFileId").value+'&FILE_NUMBER='+document.getElementById("formRequest:hdnFileNumber").value ;
		   }
		    window.open('SearchAssets.jsf?pageMode=MODE_SELECT_ONE_POPUP&context=MAINTENANCE_REQUEST&'+fileId,
		    			'_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes'
		    		   );
		    
		}
		function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
        
		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />


	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />


		</head>

		<script language="javascript" type="text/javascript">



	function validateMaintenanceDetails() {
      maxlength=250;
       if(document.getElementById("formRequest:txtMaintenanceDetails").value.length>=maxlength) 
       {
          document.getElementById("formRequest:txtMaintenanceDetails").value=
          document.getElementById("formRequest:txtMaintenanceDetails").value.substr(0,maxlength);  
      }
    }
	
	function validate() {
      maxlength=500;
       if(document.getElementById("formRequest:txt_remarks").value.length>=maxlength) 
       {
          document.getElementById("formRequest:txt_remarks").value=
          document.getElementById("formRequest:txt_remarks").value.substr(0,maxlength);  
      }
    }
    function validateEvaRemarks() {
      maxlength=250;
       if(document.getElementById("formRequest:evaRemarks").value.length>=maxlength) 
       {
          document.getElementById("formRequest:evaRemarks").value=
          document.getElementById("formRequest:evaRemarks").value.substr(0,maxlength);  
      }
    }
    function populateContract(contractId)
    {
    
      document.getElementById("formRequest:hdnContractId").value=contractId;
      document.getElementById("formRequest:cmdAddContract_Click").onclick();
    
    }
    function SearchMemberPopup(){
			   var screen_width = 1024;
			   var screen_height = 450;
			   var popup_width = screen_width-150;
		       var popup_height = screen_height-50;
		       var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		       var popup = window.open('SearchMemberPopup.jsf?singleselectmode=true','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
		       popup.focus();
	}
    function populateUnit(unitId,unitNum,usuageTypeId,unitRentAmount,unitUsuageType,propertyCommercialName,unitAddress,unitStatusId,unitStatus)
	{
	
	
	}
     function showUnitSearchPopUp()
	 {
		var screen_width = 1024;
		var screen_height = 460;
		window.open('UnitSearch.jsf?pageMode=MODE_SELECT_ONE_POPUP&context=MaintenanceRequest','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=10,top=150,scrollbars=yes,status=yes');
	 }
	 function populateProperty(propertyId)
	 {
	  
	  document.getElementById("formRequest:hdnPropertyId").value=propertyId;
	  document.getElementById("formRequest:cmdAddProperty_Click").onclick();
	 }
	 function showPropertySearchPopUp()
	 {
		var screen_width = 1024;
		var screen_height = 450;
		window.open('propertyList.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	 }
 
	function showSearchContractorPopUp(viewMode,popUp)
	{
	     
	     var  screen_width = 1024;
	     var screen_height = 450;
	     
	     window.open('contractorSearch.jsf?'+viewMode+'='+popUp,'_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	  
	}
	function populateContractor(contractorId, contractorNumber, contractorNameEn, contractorNameAr, licenseNumber, licenseSource, officeNumber){
		    document.getElementById("formRequest:hdnContractorId").value=contractorId;
		    document.getElementById("formRequest:cmdAddPerson_Click").onclick();
		}
	
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
            if(hdnPersonType=='APPLICANT')
	        {
	         
			 document.getElementById("formRequest:hdnApplicantId").value=personId;
	         document.getElementById("formRequest:cmdAddApplicant_Click").onclick();
	       
	        }
		    
	}
	
	
	function showPersonReadOnlyPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 450;
	    window.open('AddPerson.jsf?personId='+document.getElementById("formRequest:hdnContractorId").value+'&viewMode=popup&readOnlyMode=true','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
    </script>


		<!-- Header -->

		<body class="BODY_STYLE">
			<div class="containerDiv">

				<table width="99%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<c:choose>
							<c:when test="${!pages$minorMaintenanceRequest.isViewModePopUp}">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</c:when>
						</c:choose>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${!pages$minorMaintenanceRequest.isViewModePopUp}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>
						<td width="83%" height="84%" valign="top"
							class="divBackgroundBody">
							<h:form id="formRequest" style="width:99.7%;height:95%"
								enctype="multipart/form-data">
								<table class="greyPanelTable" cellpadding="0" cellspacing="0"
									border="0">
									<tr>
										<td class="HEADER_TD" style="width: 70%;">
											<h:outputLabel
												value="#{pages$minorMaintenanceRequest.pageTitle}"
												styleClass="HEADER_FONT" />
										</td>
										<td>
											&nbsp;
										</td>


									</tr>
								</table>
								<table height="95%" width="99%" class="greyPanelMiddleTable"
									cellpadding="0" cellspacing="0" border="0">
									<tr valign="top">
										<td height="100%" valign="top" nowrap="nowrap"
											background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
											width="1">
										</td>
										<td height="100%" valign="top" nowrap="nowrap">
											<div class="SCROLLABLE_SECTION">
												<t:panelGrid id="hiddenFields" border="0"
													styleClass="layoutTable" width="94%"
													style="margin-left:15px;margin-right:15px;" columns="1">
													<h:messages></h:messages>
													<h:outputText id="successmsg" escape="false"
														styleClass="INFO_FONT"
														value="#{pages$minorMaintenanceRequest.successMessages}" />
													<h:outputText id="errormsg" escape="false"
														styleClass="ERROR_FONT"
														value="#{pages$minorMaintenanceRequest.errorMessages}" />
													<h:inputHidden id="hdnCreatedOn"
														value="#{pages$minorMaintenanceRequest.contractCreatedOn}" />
													<h:inputHidden id="hdnCreatedBy"
														value="#{pages$minorMaintenanceRequest.contractCreatedBy}" />
													<h:inputHidden id="hdnRequestId"
														value="#{pages$minorMaintenanceRequest.requestId}" />
													<h:inputHidden id="hdnFileId"
														value="#{pages$minorMaintenanceRequest.hdnFileId}" />
													<h:inputHidden id="hdnFileNumber"
														value="#{pages$minorMaintenanceRequest.hdnFileNumber}" />
													<h:inputHidden id="hdnPropertyId"
														value="#{pages$minorMaintenanceRequest.hdnPropertyId}" />
													<h:inputHidden id="hdnContractorId"
														value="#{pages$minorMaintenanceRequest.hdnContractorId}" />
													<h:inputHidden id="hdnApplicantId"
														value="#{pages$minorMaintenanceRequest.hdnApplicantId}" />
													<h:inputHidden id="hdnContractId"
														value="#{pages$minorMaintenanceRequest.hdnContractId}" />
													<h:inputHidden id="hdnoldActualCost"
														value="#{pages$minorMaintenanceRequest.oldActualCost}" />

													<a4j:commandLink id="cmdAddPerson_Click"
														reRender="tbl_Action,hiddenFields"
														action="#{pages$minorMaintenanceRequest.populateContractor}" />
													<a4j:commandLink id="cmdAddProperty_Click"
														reRender="hiddenFields,unitInfoTable"
														action="#{pages$minorMaintenanceRequest.populatePropertyInfoInTab}" />
													<a4j:commandLink id="cmdAddApplicant_Click"
														reRender="applicationDetailsTable,hiddenFields"
														action="#{pages$minorMaintenanceRequest.findApplicantById}" />
													<a4j:commandLink id="cmdAddContract_Click"
														reRender="tbl_Action,hiddenFields"
														action="#{pages$minorMaintenanceRequest.populateContract}" />
													<h:inputHidden id="hdnAssetId"
														value="#{pages$minorMaintenanceRequest.hdnAssetId}" />
													<h:commandLink id="lnkAssetId"
														action="#{pages$minorMaintenanceRequest.receiveAsset}"></h:commandLink>

												</t:panelGrid>
												<div class="MARGIN" style="width: 94%; margin-bottom: 0px;">
													<t:div styleClass="BUTTON_TD">
														<t:commandButton styleClass="BUTTON"
															rendered="#{! empty pages$minorMaintenanceRequest.hdnFileId && 
																		!pages$minorMaintenanceRequest.isViewModePopUp
															           }"
															value="#{msg['reportName.fileDetails']}"
															onclick="javascript:disableButtons(this,'2');"
															style="width: 135px">
														</t:commandButton>
														<h:commandLink id="lnkOpenFile"
															action="#{pages$minorMaintenanceRequest.onOpenFile}" />
													</t:div>
													<t:panelGrid id="tbl_Action" cellpadding="1px" width="100%"
														border="0"
														columnClasses="MINOR_MAINTENANCE_FIELDS_GRID_C1, MINOR_MAINTENANCE_FIELDS_GRID_C2,
														               MINOR_MAINTENANCE_FIELDS_GRID_C1, MINOR_MAINTENANCE_FIELDS_GRID_C2"
														cellspacing="5px" columns="4"
														binding="#{pages$minorMaintenanceRequest.tbl_Action}">

														<!-- Top Fields - Start -->
														<h:outputLabel styleClass="LABEL"
															value="#{msg['searchInheritenceFile.fileNo']}:" />
														<h:inputText styleClass="READONLY" readonly="true"
															value="#{pages$minorMaintenanceRequest.inheritanceFileView.fileNumber}" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['report.memspaymentreceiptreport.fileOwner']}:" />
														<h:inputText styleClass="READONLY" readonly="true"
															value="#{pages$minorMaintenanceRequest.inheritanceFileView.filePerson}" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['searchInheritenceFile.fileType']}:" />
														<h:inputText styleClass="READONLY" readonly="true"
															value="#{pages$minorMaintenanceRequest.englishLocale ? 
																		pages$minorMaintenanceRequest.inheritanceFileView.fileTypeEn : 
																		pages$minorMaintenanceRequest.inheritanceFileView.fileTypeAr}" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['commons.status']}:" />
														<h:inputText styleClass="READONLY" readonly="true"
															value="#{pages$minorMaintenanceRequest.englishLocale ? pages$minorMaintenanceRequest.inheritanceFileView.statusEn : pages$minorMaintenanceRequest.inheritanceFileView.statusAr}" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['mems.normaldisb.label.vendor']}" />
														<h:panelGroup>
															<h:inputText id="txtContractorNumber" style="width:190px"
																readonly="true"
																title="#{pages$minorMaintenanceRequest.contractor.personFullName}"
																binding="#{pages$minorMaintenanceRequest.txtVendor}"
																value="#{pages$minorMaintenanceRequest.contractor.personFullName}"></h:inputText>
															<h:graphicImage
																url="../resources/images/app_icons/Search-tenant.png"
																onclick="javaScript:showSearchContractorPopUp('#{pages$minorMaintenanceRequest.contractorScreenQueryStringViewMode}','#{pages$minorMaintenanceRequest.contractorScreenQueryStringPopUpMode}');"
																style="MARGIN: 0px 0px -4px;"
																binding="#{pages$minorMaintenanceRequest.imgSearchContractor}"
																alt="#{msg[maintenanceRequest.searchContractor]}"></h:graphicImage>
														</h:panelGroup>

														<%--
														<h:outputLabel styleClass="LABEL"
															value="#{msg['maintenanceRequest.requestPriority']}:" />
														<h:selectOneMenu style="width:193px"
															id="RequestPriorityMenu"
															binding="#{pages$minorMaintenanceRequest.cmbRequestPriority}"
															value="#{pages$minorMaintenanceRequest.selectOneRequestPriority}">
															<f:selectItems
																value="#{pages$minorMaintenanceRequest.requestPriority}" />
														</h:selectOneMenu>
 --%>


														<h:outputLabel styleClass="LABEL"
															style="white-space: normal;"
															value="#{msg['maintenanceRequest.estimatedCost']}"></h:outputLabel>

														<h:inputText maxlength="20" style="width:190px;"
															id="txtEstimatedCost"
															binding="#{pages$minorMaintenanceRequest.txtEstimatedCost}"
															value="#{pages$minorMaintenanceRequest.estimatedCost}"
															onblur="javaScript:removeNonNumeric(this);"
															onkeyup="javaScript:removeNonNumeric(this);"
															onmouseup="javaScript:removeNonNumeric(this);"
															onmousedown="javaScript:removeNonNumeric(this);"
															onchange="javaScript:removeNonNumeric(this);"
															onkeypress="javaScript:removeNonNumeric(this);" />

														<h:outputLabel styleClass="LABEL"
															value="#{msg['maintenanceRequest.actualCost']}" />
														<h:inputText maxlength="20" style="width: 190px"
															id="txtActualCost"
															binding="#{pages$minorMaintenanceRequest.txtActualCost}"
															value="#{pages$minorMaintenanceRequest.actualCost}"
															onblur="javaScript:removeNonNumeric(this);"
															onkeyup="javaScript:removeNonNumeric(this);"
															onmouseup="javaScript:removeNonNumeric(this);"
															onmousedown="javaScript:removeNonNumeric(this);"
															onchange="javaScript:removeNonNumeric(this);"
															onkeypress="javaScript:removeNonNumeric(this);" />
														<h:outputLabel styleClass="LABEL" value="" />
														<h:outputLabel styleClass="LABEL" value="" />
														<h:outputLabel styleClass="LABEL"
															value="#{msg['fieldLabel.evaRemarks']}" />
														<t:panelGroup colspan="3">
															<t:inputTextarea style="width: 575px" wrap="true"
																id="evaRemarks"
																value="#{pages$minorMaintenanceRequest.txtRemarks}"
																binding="#{pages$minorMaintenanceRequest.txtEvaRemarks}"
																onblur="javaScript:validateEvaRemarks();"
																onkeyup="javaScript:validateEvaRemarks();"
																onmouseup="javaScript:validateEvaRemarks();"
																onmousedown="javaScript:validateEvaRemarks();"
																onchange="javaScript:validateEvaRemarks();"
																onkeypress="javaScript:validateEvaRemarks();" />
														</t:panelGroup>

														<h:outputText id="idSendTo" styleClass="LABEL"
															value="#{msg['mems.investment.label.sendTo']}"
															binding="#{pages$minorMaintenanceRequest.lblSendTo}" />

														<h:selectOneMenu id="idUserGrps"
															binding="#{pages$minorMaintenanceRequest.cmbUserGroups}"
															style="width: 200px;">
															<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
																itemValue="-1" />
															<f:selectItems
																value="#{pages$ApplicationBean.userGroups}" />
														</h:selectOneMenu>

														<h:outputText id="idRevComments" styleClass="LABEL"
															binding="#{pages$minorMaintenanceRequest.lblRevComments}"
															value="#{msg['commons.comments']}" />
														<h:inputTextarea id="txtReviewCmts" style="width: 300px;"
															binding="#{pages$minorMaintenanceRequest.txtReviewComments}">
														</h:inputTextarea>
													</t:panelGrid>
													<t:panelGrid id="followupremarks" cellpadding="1px"
														width="92.5%" border="0"
														columnClasses="MINOR_MAINTENANCE_FIELDS_GRID_C1, MINOR_MAINTENANCE_FIELDS_GRID_C2,
														               MINOR_MAINTENANCE_FIELDS_GRID_C1, MINOR_MAINTENANCE_FIELDS_GRID_C2"
														cellspacing="5px"
														binding="#{pages$minorMaintenanceRequest.tbl_FollowupRemarks}"
														columns="4">
														<h:outputLabel styleClass="LABEL"
															rendered="#{pages$minorMaintenanceRequest.isPageModeFollowUp}"
															value="#{msg['followup.remarks']}" />
														<t:panelGroup colspan="3" style="width:100%;">
															<t:inputTextarea id="txt_remarks" style="width: 550px"
																binding="#{pages$minorMaintenanceRequest.txtboxRemarks}"
																onblur="javaScript:validate();"
																onkeyup="javaScript:validate();"
																onmouseup="javaScript:validate();"
																onmousedown="javaScript:validate();"
																onchange="javaScript:validate();"
																onkeypress="javaScript:validate();" />
														</t:panelGroup>
													</t:panelGrid>
													<t:panelGrid id="tbl_ActionBtn" cellpadding="1px"
														cellspacing="3px" width="100%" border="0" columns="1"
														columnClasses="BUTTON_TD">
														<t:panelGroup>
															<pims:security
																screen="Pims.MinorMgmt.MinorMaintenanceRequest.approve"
																action="view">
																<h:commandButton type="submit" styleClass="BUTTON"
																	style="width:auto;"
																	onclick="if (!confirm('#{msg['minorMaintenance.warning.sureToApprove']}')) return false;else disableButtons(this,'1')"
																	binding="#{pages$minorMaintenanceRequest.btnApprove}"
																	value="#{msg['commons.approve']}" />
																<h:commandLink id="lnkApprove"
																	action="#{pages$minorMaintenanceRequest.btnApprove_Click}" />
																<h:outputLabel value=" " />

																<h:commandButton type="submit" styleClass="BUTTON"
																	style="width:auto;"
																	onclick="if (!confirm('#{msg['mems.common.alertReview']}')) return false;else disableButtons(this,'1')"
																	binding="#{pages$minorMaintenanceRequest.btnRevReq}"
																	value="#{msg['commons.review']}" />
																<h:commandLink id="lnkRevReq"
																	action="#{pages$minorMaintenanceRequest.onSentForReview}" />
																<h:outputLabel value=" " />

															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.MinorMaintenanceRequest.reject"
																action="view">
																<h:commandButton type="submit" styleClass="BUTTON"
																	style="width:auto;"
																	onclick="if (!confirm('#{msg['minorMaintenance.warning.sureToReject']}')) return false;else disableButtons(this,'1');"
																	binding="#{pages$minorMaintenanceRequest.btnReject}"
																	value="#{msg['commons.reject']}" />
																<h:commandLink id="lnkReject"
																	action="#{pages$minorMaintenanceRequest.btnReject_Click}" />
																<h:outputLabel value=" " />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.MinorMaintenanceRequest.sendForEavluation"
																action="view">
																<h:commandButton type="submit" styleClass="BUTTON"
																	style="width:auto;"
																	onclick="if (!confirm('#{msg['minorMaintenance.warning.sureToSendForEvaluation']}')) return false;else disableButtons(this,'1');"
																	binding="#{pages$minorMaintenanceRequest.btnSendForEval}"
																	value="#{msg['commons.send.evaluation']}" />
																<h:commandLink id="lnkSendForEvaluation"
																	action="#{pages$minorMaintenanceRequest.btnSendForEvaluation_Click}" />
																<h:outputLabel value=" " />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.MinorMaintenanceRequest.doneEvaluation"
																action="view">
																<h:commandButton type="submit" styleClass="BUTTON"
																	style="width:auto;"
																	binding="#{pages$minorMaintenanceRequest.btnDone}"
																	onclick="if (!confirm('#{msg['minorMaintenance.warning.sureToDoneEvaluation']}')) return false;else disableButtons(this,'1');"
																	value="#{msg['commons.done']}" />
																<h:commandLink id="lnkDoneEvaluation"
																	action="#{pages$minorMaintenanceRequest.btnDoneEvaluation_Click}" />
																<h:outputLabel value=" " />
															</pims:security>
															<h:commandButton type="submit" styleClass="BUTTON"
																style="width:auto;"
																onclick="if (!confirm('#{msg['mems.common.alertReviewDone']}')) return false;else disableButtons(this,'1')"
																binding="#{pages$minorMaintenanceRequest.btnReviewed}"
																value="#{msg['commons.done']}" />
															<h:commandLink id="lnkReviewed"
																action="#{pages$minorMaintenanceRequest.onReviewDone}" />
															<h:outputLabel value=" " />
															<pims:security
																screen="Pims.MinorMgmt.MinorMaintenanceRequest.saveFollowup"
																action="view">
																<h:commandButton type="submit" styleClass="BUTTON"
																	style="width:auto;"
																	onclick="if (!confirm('#{msg['confirmMsg.areuSureToSave']}')) return false;else disableButtons(this,'1');"
																	binding="#{pages$minorMaintenanceRequest.btnSaveFollowUp}"
																	value="#{msg['maintenanceRequest.saveFollowUp']}" />
																<h:commandLink id="lnkSaveFollowUp"
																	action="#{pages$minorMaintenanceRequest.btnSaveFollowUp_Click}" />
																<h:outputLabel value=" " />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.MinorMaintenanceRequest.sendForFollowup"
																action="view">
																<h:commandButton type="submit" styleClass="BUTTON"
																	style="width:auto;"
																	onclick="if (!confirm('#{msg['minorMaintenance.warning.sureToSendFollowUp']}')) return false;else disableButtons(this,'1');"
																	binding="#{pages$minorMaintenanceRequest.btnSendForFollowup}"
																	value="#{msg['btnLabel.sendForFollowup']}" />
																<h:commandLink id="lnkSendForFollowup"
																	action="#{pages$minorMaintenanceRequest.btnSendForFollowup_Click}" />
																<h:outputLabel value=" " />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.MinorMaintenanceRequest.completeRequest"
																action="view">
																<h:commandButton type="submit" styleClass="BUTTON"
																	style="width:auto;"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else disableButtons(this,'1');"
																	binding="#{pages$minorMaintenanceRequest.btnSendForBlocking}"
																	value="#{msg['mems.maintenance.message.block']}" />
																<h:commandLink id="lnkSendForBlocking"
																	action="#{pages$minorMaintenanceRequest.onSendForBlocking}" />

																<h:outputLabel value=" " />

																<h:commandButton type="submit" styleClass="BUTTON"
																	style="width:auto;"
																	onclick="if (!confirm('#{msg['minorMaintenance.warning.sureToComplete']}')) return false;else disableButtons(this,'1');"
																	binding="#{pages$minorMaintenanceRequest.btnComplete}"
																	value="#{msg['maintenanceRequest.completeRequest']}" />
																<h:commandLink id="lnkComplete"
																	action="#{pages$minorMaintenanceRequest.btnComplete_Click}" />
															</pims:security>
															<h:commandButton styleClass="BUTTON" id="btnPrint"
																value="#{msg['commons.print']}"
																binding="#{pages$minorMaintenanceRequest.btnPrint}"
																onclick="disableButtons(this);">
															</h:commandButton>
															<h:commandLink id="lnkPrint"
																action="#{pages$minorMaintenanceRequest.onPrint}" />

														</t:panelGroup>
													</t:panelGrid>


													<table id="table_1" cellpadding="0" cellspacing="0"
														width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<rich:tabPanel id="tabPanel"
														binding="#{pages$minorMaintenanceRequest.tabPanel}"
														style="width:100%;" headerSpacing="0">

														<rich:tab id="appDetailsTab"
															binding="#{pages$minorMaintenanceRequest.tabApplicationDetails}"
															title="#{msg['commons.tab.applicationDetails']}"
															label="#{msg['contract.tabHeading.ApplicationDetails']}">

															<%@ include file="ApplicationDetails.jsp"%>

														</rich:tab>
														<rich:tab id="tabBasicInfo"
															binding="#{pages$minorMaintenanceRequest.tabBasicInfo}"
															title="#{msg['contract.tab.BasicInfo']}"
															label="#{msg['contract.tab.BasicInfo']}">
															<t:panelGroup>
																<t:panelGrid columns="4"
																	columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
																	width="100%">

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<t:outputLabel styleClass="LABEL"
																			value="#{msg['assetManage.assetNumber']}:" />
																	</h:panelGroup>
																	<h:panelGroup>
																		<h:inputText styleClass="READONLY" readonly="true"
																			value="#{pages$minorMaintenanceRequest.assetView.assetNumber}"></h:inputText>
																		<h:graphicImage
																			binding="#{pages$minorMaintenanceRequest.imgSearchAsset}"
																			url="../resources/images/app_icons/Search-Unit.png"
																			style="MARGIN: 1px 1px -5px;cursor:hand;"
																			onclick="javascript:showAssetSearchPopup();"></h:graphicImage>
																		<h:commandLink
																			rendered="#{! empty pages$minorMaintenanceRequest.assetView.assetNumber }"
																			action="#{pages$minorMaintenanceRequest.openManageAsset}">
																			<h:graphicImage title="#{msg['manageAsset.toolTip']}"
																				binding="#{pages$minorMaintenanceRequest.imgManageAsset}"
																				url="../resources/images/app_icons/View_Icon.png"
																				style="MARGIN: 1px 1px -5px;cursor:hand;"></h:graphicImage>
																		</h:commandLink>
																	</h:panelGroup>

																	<t:outputLabel styleClass="LABEL"
																		value="#{msg['assetSearch.assetName']}:" />
																	<h:inputText styleClass="READONLY" readonly="true"
																		value="#{pages$minorMaintenanceRequest.englishLocale?pages$minorMaintenanceRequest.assetView.assetNameEn:pages$minorMaintenanceRequest.assetView.assetNameAr}"></h:inputText>

																	<t:outputLabel styleClass="LABEL"
																		value="#{msg['searchAssets.assetType']}:" />
																	<h:inputText
																		value="#{pages$minorMaintenanceRequest.assetView.assetType}"
																		styleClass="READONLY" readonly="true"></h:inputText>

																	<t:outputLabel styleClass="LABEL"
																		value="#{msg['searchInheritenceFile.assetDesc']}:" />
																	<h:inputText
																		value="#{pages$minorMaintenanceRequest.assetView.assetDesc}"
																		styleClass="READONLY" readonly="true"></h:inputText>

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<t:outputLabel styleClass="LABEL"
																			value="#{msg['scopeOfWork.workType']}:" />
																	</h:panelGroup>

																	<t:div
																		style="overflow-y:auto;height: 100px; overflow-x: hidden; border-width: 1px; border-style: solid; border-color: #a2a2a2;">
																		<h:panelGrid
																			style="border-width: 1px; border-style: solid; border-color: #a2a2a2; background: white; overflow-x: auto; overflow-y: scroll; width: 80%; height: 60px;font-weight:400 ">
																			<h:selectManyCheckbox id="workTypeMenu"
																				binding="#{pages$minorMaintenanceRequest.cmbWorkType}"
																				layout="pageDirection"
																				value="#{pages$minorMaintenanceRequest.selectManyWorkType}">

																				<f:selectItems
																					value="#{pages$ApplicationBean.workTypesList}" />
																			</h:selectManyCheckbox>
																		</h:panelGrid>
																	</t:div>

																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<t:outputLabel styleClass="LABEL"
																			value="#{msg['maintenanceRequest.lbl.maintenanceType']}:" />
																	</h:panelGroup>
																	<h:selectOneMenu id="maintenanceTypeMenu"
																		binding="#{pages$minorMaintenanceRequest.cmbMaintenanceType}"
																		value="#{pages$minorMaintenanceRequest.selectOneMaintenanceType}">
																		<f:selectItems
																			value="#{pages$ApplicationBean.maintenanceTypesList}" />
																	</h:selectOneMenu>
																	<h:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel
																			value="#{msg['maintenanceRequest.tab.MaintenanceDetails']}" />
																	</h:panelGroup>
																	<t:panelGroup colspan="3" style="width:100%">
																		<h:inputTextarea id="txtMaintenanceDetails"
																			style="width:90%"
																			binding="#{pages$minorMaintenanceRequest.txtMaintenaceDetails}"
																			onblur="javaScript:validateMaintenanceDetails();"
																			onkeyup="javaScript:validateMaintenanceDetails();"
																			onmouseup="javaScript:validateMaintenanceDetails();"
																			onmousedown="javaScript:validateMaintenanceDetails();"
																			onchange="javaScript:validateMaintenanceDetails();"
																			onkeypress="javaScript:validateMaintenanceDetails();" />
																	</t:panelGroup>
																</t:panelGrid>
															</t:panelGroup>
														</rich:tab>
														<rich:tab label="#{msg['tabLabel.deductFrom']}"
															id="deductionListId"
															binding="#{pages$minorMaintenanceRequest.tabDeductionFrom}"
															title="#{msg['tabLabel.deductFrom']}">
															<t:panelGrid border="0" width="100%" cellpadding="1"
																cellspacing="1">

																<t:panelGroup>
																	<t:panelGrid columns="4"
																		columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
																		width="100%">
																		<h:outputLabel id="lblPaymentSrc" styleClass="LABEL"
																			value="#{msg['mems.normaldisb.label.paymentSrc']}" />
																		<h:selectOneMenu id="paymentSrc"
																			binding="#{pages$minorMaintenanceRequest.cmbPaymentSource}"
																			style="width: 200px;">
																			<f:selectItem
																				itemLabel="#{msg['commons.pleaseSelect']}"
																				itemValue="-1" />
																			<f:selectItems
																				value="#{pages$minorMaintenanceRequest.paymentSource}" />
																			<a4j:support event="onchange"
																				reRender="disbSrcId,cmbDisbSrc,errormsg,cmbDeductCrit,deductFromGrid,btnApplyDeduction"
																				action="#{pages$minorMaintenanceRequest.onChangePaymentSource}" />
																		</h:selectOneMenu>
																		<h:panelGroup>
																			<h:outputLabel styleClass="mandatory" value="*" />
																			<h:outputLabel id="lblDisSrc" styleClass="LABEL"
																				value="#{msg['commons.source']}" />
																		</h:panelGroup>
																		<h:selectOneMenu id="cmbDisbSrc"
																			binding="#{pages$minorMaintenanceRequest.cmbDisbursementSource}"
																			style="width: 200px;">
																			<f:selectItem
																				itemLabel="#{msg['commons.pleaseSelect']}"
																				itemValue="-1" />
																			<f:selectItems
																				value="#{pages$minorMaintenanceRequest.disbursementSources}" />
																		</h:selectOneMenu>


																		<h:outputLabel
																			value="#{msg['commons.distributePopUp']}"></h:outputLabel>
																		<h:selectOneMenu id="cmbDeductCrit"
																			binding="#{pages$minorMaintenanceRequest.cmbDeductCrit}">
																			<f:selectItem
																				itemLabel="#{msg['commons.combo.PleaseSelect']}"
																				itemValue="-1" />
																			<f:selectItems
																				value="#{pages$minorMaintenanceRequest.deductionCriteriaList}" />
																		</h:selectOneMenu>
																		<pims:security
																			screen="Pims.MinorMgmt.MinorMaintenanceRequest.approve"
																			action="view">
																			<h:commandButton styleClass="BUTTON" type="submit"
																				id="btnApplyDeduction"
																				action="#{pages$minorMaintenanceRequest.btnApply_Click}"
																				binding="#{pages$minorMaintenanceRequest.btnApplyDeduction}"
																				value="#{msg['settlement.label.apply']}"></h:commandButton>
																		</pims:security>
																	</t:panelGrid>
																	<t:div style="height:5px;" />
																	<t:div styleClass="contentDiv" style="width:98%">
																		<t:dataTable id="deductFromGrid" rows="400"
																			binding="#{pages$minorMaintenanceRequest.deductFromDataTable}"
																			value="#{pages$minorMaintenanceRequest.deductFromList}"
																			preserveDataModel="true" preserveSort="false"
																			var="dataItem" rowClasses="row1,row2" rules="all"
																			renderedIfEmpty="true" width="100%">

																			<t:column sortable="true" id="col1">
																				<f:facet name="header">
																					<h:outputText value="" />
																				</f:facet>
																				<h:selectBooleanCheckbox
																					binding="#{pages$minorMaintenanceRequest.cboDeduction}"
																					value="#{dataItem.selected}" />
																			</t:column>

																			<t:column sortable="true" id="col2">
																				<f:facet name="header">
																					<h:outputText
																						value="#{msg['PeriodicDisbursements.columns.beneficiary']}" />
																				</f:facet>
																				<h:outputText value="#{dataItem.beneficiaryName}" />
																			</t:column>

																			<t:column sortable="true" id="col3">
																				<f:facet name="header">
																					<h:outputText
																						value="#{msg['searchInheritenceFile.fileNo']}" />
																				</f:facet>
																				<t:commandLink id="colfileNumber"
																					action="#{pages$minorMaintenanceRequest.openFilePopUp}"
																					styleClass="A_LEFT"
																					value="#{dataItem.inheritanceFileNumber}" />
																			</t:column>
																			<t:column sortable="true" id="col8">
																				<f:facet name="header">
																					<h:outputText
																						value="#{msg['distribution.totalFileShare']}" />
																				</f:facet>
																				<h:outputText value="#{dataItem.totalFileShares}" />
																			</t:column>
																			<t:column sortable="true" id="col7">
																				<f:facet name="header">
																					<h:outputText value="#{msg['label.shariaShares']}" />
																				</f:facet>
																				<h:outputText value="#{dataItem.shariaPercent}" />
																			</t:column>
																			<t:column sortable="true" id="col6">
																				<f:facet name="header">
																					<h:outputText
																						value="#{msg['distribution.totalAssetShares']}" />
																				</f:facet>
																				<h:outputText value="#{dataItem.totalAssetShares}" />
																			</t:column>
																			<t:column sortable="true" id="col5">
																				<f:facet name="header">
																					<h:outputText value="#{msg['label.assetShares']}" />
																				</f:facet>
																				<h:outputText value="#{dataItem.assetPercent}" />
																			</t:column>
																			<t:column sortable="true" id="colBalance">
																				<f:facet name="header">
																					<h:outputText
																						value="#{msg['mems.normaldisb.label.balance']}" />
																				</f:facet>
																				<h:outputText value="#{dataItem.remainingBalance}" />
																			</t:column>
																			<t:column sortable="true" id="col4">
																				<f:facet name="header">
																					<h:outputText
																						value="#{msg['ReviewAndConfirmDisbursements.amount']}" />
																				</f:facet>
																				<h:outputText value="#{dataItem.amountToBeDeducted}" />
																			</t:column>

																		</t:dataTable>
																	</t:div>
																</t:panelGroup>
															</t:panelGrid>
														</rich:tab>
														<rich:tab id="reviewTab"
															label="#{msg['commons.tab.reviewDetails']}"
															binding="#{pages$minorMaintenanceRequest.tabReview}">
															<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
														</rich:tab>
														<rich:tab id="tabFollowUp"
															action="#{pages$minorMaintenanceRequest.tabFollowUp_Click}"
															binding="#{pages$minorMaintenanceRequest.tabFollowUp}"
															title="#{msg['maintenanceRequest.tab.FollowUp']}"
															label="#{msg['maintenanceRequest.tab.FollowUp']}">
															<t:div styleClass="contentDiv" style="width:98%;">
																<t:dataTable id="followUpTable" renderedIfEmpty="true"
																	width="100%" cellpadding="0" cellspacing="0"
																	var="followUpDataItem"
																	binding="#{pages$minorMaintenanceRequest.followUpDataTable}"
																	value="#{pages$minorMaintenanceRequest.followUpList}"
																	preserveDataModel="false" preserveSort="false"
																	rowClasses="row1,row2" rules="all" rows="9">
																	<t:column width="200">
																		<f:facet name="header">
																			<t:outputText value=" Date " />
																		</f:facet>
																		<t:outputText value="#{followUpDataItem.followupDate}"
																			styleClass="A_LEFT" />

																	</t:column>
																	<t:column width="createByEn"
																		rendered="#{pages$minorMaintenanceRequest.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['maintenanceRequest.followUpBy']}" />
																		</f:facet>
																		<t:outputText
																			value="#{followUpDataItem.createdByFullNameEn}"
																			styleClass="A_LEFT" />
																	</t:column>
																	<t:column width="createByEnAr"
																		rendered="#{!pages$minorMaintenanceRequest.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['maintenanceRequest.followUpBy']}" />
																		</f:facet>
																		<t:outputText
																			value="#{followUpDataItem.createdByFullNameAr}"
																			styleClass="A_LEFT" />
																	</t:column>


																	<t:column>
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['maintenanceRequest.actualCost']}" />
																		</f:facet>
																		<t:outputText value="#{followUpDataItem.actualCost}"
																			styleClass="A_LEFT" />

																	</t:column>
																	<t:column>
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['maintenanceRequest.remarks']}" />
																		</f:facet>
																		<t:outputText value="#{followUpDataItem.remarks}"
																			styleClass="A_LEFT" />

																	</t:column>



																</t:dataTable>
															</t:div>
															<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																style="width:100%;">
																<t:panelGrid columns="2" border="0" width="100%"
																	cellpadding="1" cellspacing="1">
																	<t:panelGroup
																		styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																		<t:div styleClass="JUG_NUM_REC_ATT">
																			<h:outputText value="#{msg['commons.records']}" />
																			<h:outputText value=" : " />
																			<h:outputText
																				value="#{pages$minorMaintenanceRequest.recordSize}" />
																		</t:div>
																	</t:panelGroup>
																	<t:panelGroup>
																		<t:dataScroller id="FoollowupScroller1"
																			for="followUpTable" paginator="true" fastStep="1"
																			paginatorMaxPages="5" immediate="false"
																			paginatorTableClass="paginator"
																			renderFacetsIfSinglePage="true"
																			paginatorTableStyle="grid_paginator"
																			layout="singleTable"
																			paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																			paginatorActiveColumnStyle="font-weight:bold;"
																			paginatorRenderLinkForActive="false"
																			pageIndexVar="pageNumber" styleClass="JUG_SCROLLER">
																			<f:facet name="first">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblFFU"></t:graphicImage>
																			</f:facet>
																			<f:facet name="fastrewind">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFRFU"></t:graphicImage>
																			</f:facet>
																			<f:facet name="fastforward">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFFFU"></t:graphicImage>
																			</f:facet>
																			<f:facet name="last">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblLFU"></t:graphicImage>
																			</f:facet>
																			<t:div styleClass="PAGE_NUM_BG" rendered="true">
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																				<h:outputText styleClass="PAGE_NUM"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{requestScope.pageNumber}" />
																			</t:div>
																		</t:dataScroller>
																	</t:panelGroup>
																</t:panelGrid>
															</t:div>

														</rich:tab>

														<rich:tab
															label="#{msg['contract.tabHeading.RequestHistory']}"
															title="#{msg['commons.tab.requestHistory']}"
															action="#{pages$minorMaintenanceRequest.tabAuditTrail_Click}">
															<%@ include file="../pages/requestTasks.jsp"%>
														</rich:tab>
														<rich:tab id="attachmentTab"
															label="#{msg['commons.attachmentTabHeading']}"
															action="#{pages$minorMaintenanceRequest.tabAttachmentsComments_Click}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}"
															action="#{pages$minorMaintenanceRequest.tabAttachmentsComments_Click}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>

													</rich:tabPanel>
												</div>


												<table cellpadding="0" cellspacing="0"
													style="width: 94%; margin-right: 10px;">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>

												<table cellpadding="1px" cellspacing="3px" width="95%">
													<tr>

														<td colspan="12" class="BUTTON_TD">

															<pims:security
																screen="Pims.MinorMgmt.MinorMaintenanceRequest.save"
																action="view">
																<h:commandButton type="submit" styleClass="BUTTON"
																	style="width:10%;"
																	onclick="if (!confirm('#{msg['minorMaintenance.warning.sureToSave']}')) return false;else disableButtons(this,'2');"
																	binding="#{pages$minorMaintenanceRequest.btnSave}"
																	value="#{msg['commons.saveButton']}" />
																<h:commandLink id="lnkSave"
																	action="#{pages$minorMaintenanceRequest.btnSave_Click}" />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.MinorMaintenanceRequest.submit"
																action="view">
																<h:commandButton id="btnSendForApproval" type="submit"
																	styleClass="BUTTON" style="width:10%;"
																	onclick="if (!confirm('#{msg['minorMaintenance.warning.sureToSendRequestForApproval']}')) return false;else disableButtons(this,'2');"
																	binding="#{pages$minorMaintenanceRequest.btnSend_For_Approval}"
																	value="#{msg['commons.sendButton']}" />
																<h:commandLink id="lnkSubmit"
																	action="#{pages$minorMaintenanceRequest.btnSendForApproval_Click}" />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.MinorMaintenanceRequest.completeFollowup"
																action="view">
																<h:commandButton type="submit" styleClass="BUTTON"
																	style="width:auto;"
																	onclick="if (!confirm('#{msg['minorMaintenance.warning.sureToDoneFollowUp']}')) return false;else disableButtons(this,'2');"
																	binding="#{pages$minorMaintenanceRequest.btnDoneFollowup}"
																	value="#{msg['btnLabel.doneFollowup']}" />
																<h:commandLink id="lnkCompleteFollowup"
																	action="#{pages$minorMaintenanceRequest.btnDoneFollowup_Click}" />
															</pims:security>

														</td>
													</tr>
												</table>

											</div>
											</div>
										</td>
									</tr>
								</table>
							</h:form>
						</td>
					</tr>
					<c:choose>
						<c:when test="${!pages$minorMaintenanceRequest.isViewModePopUp}">

							<tr>
								<td colspan="2">

									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</c:when>
					</c:choose>
				</table>
			</div>
		</body>
	</html>
</f:view>

