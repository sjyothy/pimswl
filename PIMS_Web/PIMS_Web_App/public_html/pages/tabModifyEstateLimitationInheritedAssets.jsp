<%-- Author: Kamran Ahmed--%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<t:div style="height:310px;overflow-y:scroll;width:100%;">

	<t:panelGrid style="width: 96%;" columns="1"
		columnClasses="BUTTON_TD MARGIN">
		<t:panelGroup colspan="12" styleClass="BUTTON_TD">
			<h:commandButton id="idAddBtn" value="#{msg['commons.Add']}"
				rendered="#{pages$modifyEstateLimitation.showSaveButton || pages$modifyEstateLimitation.showApproveRejectButton}"
				styleClass="BUTTON"
				action="#{pages$modifyEstateLimitation.onOpenModifyAssetPopupForAdd}">
			</h:commandButton>
		</t:panelGroup>
	</t:panelGrid>

	<t:div styleClass="contentDiv" style="width:96%;margin-top:15px;">

		<t:dataTable id="dataTableInheritedAsset" rows="500"
			value="#{pages$modifyEstateLimitation.inheritedAssetsList}"
			binding="#{pages$modifyEstateLimitation.dataTableInheritedAsset}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
			width="100%">

			<t:column width="2%">
				<f:facet name="header" />
				<h:selectBooleanCheckbox value="#{dataItem.isSelected}"
					rendered="#{empty  dataItem.inheritedAssetId && dataItem.isDeleted == 0}" />

			</t:column>

			<t:column width="5%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['searchAssets.assetNumber']}" />
				</f:facet>
				<h:commandLink
					rendered="#{!empty dataItem.assetMemsView.assetId  && dataItem.isDeleted == 0}" 
					onclick="openManageAssetPopup('#{dataItem.assetMemsView.assetId}')"
					value="#{dataItem.assetMemsView.assetNumber}"
					/>
				<h:commandLink
					rendered="#{empty dataItem.assetMemsView.assetId   && !empty dataItem.assetMemsView.oldAssetId && dataItem.isDeleted == 0}" 
					onclick="openManageAssetPopup('#{dataItem.assetMemsView.oldAssetId}')"
					value="#{dataItem.assetMemsView.assetNumber}"
					/>
			</t:column>

			<t:column width="5%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['searchAssets.assetType']}" />
				</f:facet>
				<t:outputText
					value="#{pages$modifyEstateLimitation.englishLocale ? dataItem.assetMemsView.assetTypeView.assetTypeNameEn : dataItem.assetMemsView.assetTypeView.assetTypeNameAr}"
					rendered="#{dataItem.isDeleted == 0}" />
			</t:column>

			<t:column width="15%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['assetSearch.assetName']}" />
				</f:facet>
				<t:outputText
					value="#{pages$modifyEstateLimitation.englishLocale ? dataItem.assetMemsView.assetNameEn : dataItem.assetMemsView.assetNameAr}"
					rendered="#{dataItem.isDeleted == 0}" />
			</t:column>
			<t:column sortable="true" width="7%">
				<f:facet name="header">
					<h:outputText value="#{msg['contract.person.Manager']}" />
				</f:facet>
				<h:outputText value="#{dataItem.assetMemsView.managerName}"
					rendered="#{dataItem.isDeleted == 0}" />
			</t:column>

			<t:column width="7%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['searchAssets.assetDesc']}" />
				</f:facet>
				<t:outputText value="#{dataItem.assetMemsView.description}"
					rendered="#{dataItem.isDeleted == 0}" />
			</t:column>
			<t:column sortable="true" width="3%">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.status']}" />
				</f:facet>
				<h:outputText rendered="#{dataItem.isDeleted == 0}"
					value="#{dataItem.statusAr}">
				</h:outputText>
			</t:column>

			<t:column id="shareValue" sortable="true" width="4%">
				<f:facet name="header">
					<h:outputText
						value="#{msg['inheritanceFile.label.totalAssetShare']}" />
				</f:facet>
				<h:outputText value="#{dataItem.totalShareValueString}"
					rendered="#{dataItem.isDeleted == 0}" />
			</t:column>

			<t:column id="action" width="4%">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.action']}" />
				</f:facet>
				<t:commandLink
					action="#{pages$modifyEstateLimitation.onOpenModifyAssetPopup}">
					<h:graphicImage title="#{msg['commons.edit']}" style="cursor:hand;"
						rendered="#{
										dataItem.isDeleted == 0
									}"
						url="../resources/images/edit-icon.gif" />
				</t:commandLink>
				<t:commandLink
					action="#{pages$modifyEstateLimitation.onDeleteAssetFromRequest}">
					<h:graphicImage title="#{msg['commons.delete']}"
						style="cursor:hand;"
						rendered="#{ !empty dataItem.assetMemsView.assetId && 
									  dataItem.isDeleted == 0 && 
									  (	 pages$modifyEstateLimitation.showApproveRejectButton )
								}"
						url="../resources/images/delete.gif" />
				</t:commandLink>
				<t:commandLink
					rendered="#{
								!empty dataItem.assetMemsView.assetId &&
								dataItem.isDeleted == 0  &&
								dataItem.status!=212002  && 
								(  pages$modifyEstateLimitation.showApproveRejectButton )
							   }"
					onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;"
					style="margin-left:5px; margin-right:5px;"
					action="#{pages$modifyEstateLimitation.onDisableAsset}">
					<h:graphicImage title="#{msg['commons.clickDisable']}"
						style="cursor:hand;"
						url="../resources/images/app_icons/file_block.gif" />&nbsp;
				</t:commandLink>
				<t:commandLink
					rendered="#{
								!empty dataItem.assetMemsView.assetId &&
								dataItem.isDeleted == 0  && 
								dataItem.status!=212001  && 
								(  pages$modifyEstateLimitation.showApproveRejectButton )
							   }"
					onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;"
					style="margin-left:5px; margin-right:5px;"
					action="#{pages$modifyEstateLimitation.onEnableAsset}">
					<h:graphicImage title="#{msg['commons.clickEnable']}"
						style="cursor:hand;"
						url="../resources/images/app_icons/file_allow.gif" />&nbsp;
				</t:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>

</t:div>