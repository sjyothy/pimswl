<%-- 
  - Author: Anil Verani
  - Date: 03/07/2009
  - Copyright Notice:
  - @(#)
  - Description: Used for Adding, Updating and Viewing Intial / Feasibility Studies 
  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
	function  receiveSubClassList()
	{
	  document.getElementById('frm:lnkReceiveAssetSubClass').onclick();
	}
	
	function openAssetSubClassManagePopup()
	{
	 var screen_width = 1024;
     var screen_height = 220;
     window.open('assetSubClassManage.jsf','_blank','width='+(screen_width-250)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	function closeWindow() 
	{
	 window.close();
	}
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
						<c:when test="${! pages$assetClassManage.isPopupViewOnlyMode}">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</c:when>
					</c:choose>
				</tr>

				<tr>
					<c:choose>
						<c:when test="${! pages$assetClassManage.isPopupViewOnlyMode}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['assetManage.heading']}" styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99.1%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:460px">

										<h:form id="frm" style="width:97%" enctype="multipart/form-data" >
											<table border="0" class="layoutTable">

												<tr>

													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$assetClassManage.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$assetClassManage.successMessages}" />
													</td>
												</tr>
											</table>

											<div class="MARGIN" style="width: 98%">
												
												<table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
										        </table>
												
												<rich:tabPanel style="width:100%;height:235px;" headerSpacing="0">
												
														<!-- Asset Class Tab - Start -->
														<rich:tab label="#{msg['assetManage.AssetClass.Tab.heading']}">
															<t:div  style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid id="followUpDetailsTable" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4">																	
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['assetManage.assetNumber']}:"></h:outputLabel>
																	<h:inputText value="#{pages$assetClassManage.assetClassView.assetClassNumber}" style="width: 186px;" maxlength="20" styleClass="READONLY" readonly="true" />
																  <h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>
																	<h:outputLabel styleClass="LABEL" value="#{msg['assetManage.status']}:"></h:outputLabel>
																  </h:panelGroup>	
																	<h:selectOneMenu readonly="#{(!pages$assetClassManage.isEditMode ||  pages$assetClassManage.isViewMode) }" styleClass="#{pages$assetClassManage.isStatusReadOnly}" value="#{pages$assetClassManage.assetClassView.statusIdForUI}" style="width: 192px;">
																		<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
																		<f:selectItems value="#{pages$ApplicationBean.assetClassStatus}" />
																	</h:selectOneMenu>
																 <h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel  styleClass="LABEL" value="#{msg['assetManage.assetClassNameAr']}:"></h:outputLabel>
																</h:panelGroup>	
																	<h:inputText readonly="#{pages$assetClassManage.isViewMode}" styleClass="#{pages$assetClassManage.isViewReadOnly}" value="#{pages$assetClassManage.assetClassView.assetClassNameAr}" style="width: 186px;" maxlength="50" />
																 <h:panelGroup>	
																	<h:outputLabel styleClass="mandatory" value="*"/>	
																	<h:outputLabel styleClass="LABEL" value="#{msg['assetManage.assetClassNameEn']}:"></h:outputLabel>
																 </h:panelGroup>	
																	<h:inputText readonly="#{pages$assetClassManage.isViewMode}" styleClass="#{pages$assetClassManage.isViewReadOnly}" value="#{pages$assetClassManage.assetClassView.assetClassNameEn}" style="width: 186px;" maxlength="50" />
																	
																	<h:outputLabel styleClass="LABEL" value="#{msg['assetManage.assetClassDescription']}:"></h:outputLabel>
																	<h:inputTextarea readonly="#{pages$assetClassManage.isViewMode}" styleClass="#{pages$assetClassManage.isViewReadOnly}" value="#{pages$assetClassManage.assetClassView.description}" style="width: 186px;" />
																	                                                                 
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Asset Class Tab - End -->
														
														<!-- Asset Sub Class Tab - Start -->
														<rich:tab label="#{msg['assetManage.AssetSubClass.Tab.heading']}">														
															
															<h:panelGrid rendered="#{! (pages$assetClassManage.isPopupViewOnlyMode || pages$assetClassManage.isViewMode)}" styleClass="BUTTON_TD" width="100%">
																<h:commandButton style="width:auto;" styleClass="BUTTON"
																		value="#{msg['assetManage.AssetSubClass.btn.AddSubClass']}"
																		action="#{pages$assetClassManage.onAddSubClass}">
																</h:commandButton>																
																 <a4j:commandLink id="lnkReceiveAssetSubClass" action="#{pages$assetClassManage.receiveAssetSubClass}"   reRender="dataTable2,gridInfo2" /> 
																<%-- <h:commandLink id="lnkReceiveRecommendation" action="#{pages$studyManage.receiveRecommendation}" /> --%>																
															</h:panelGrid>
																													
															<t:div styleClass="contentDiv" style="width:99%">																
						                                       <t:dataTable id="dataTable2"  
																	rows="#{pages$assetClassManage.paginatorRows}"
																	value="#{pages$assetClassManage.assetSubClassViewList}"
																	binding="#{pages$assetClassManage.assetSubClassDataTable}"																	
																	preserveDataModel="false" preserveSort="false" var="dataItem"
																	rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
																																		
																	<t:column id="assetNameArCol" sortable="true" >
																		<f:facet  name="header">
																			<t:outputText  value="#{msg['assetClassesSearch.assetClassNameAr']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.assetClassNameAr}" rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	<t:column id="AssetNameEnCol" sortable="true" >
																		<f:facet  name="header">
																			<t:outputText value="#{msg['assetClassesSearch.assetClassNameEn']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.assetClassNameEn}" rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	<t:column id="AssetDescriptionCol" sortable="true" >
																		<f:facet  name="header">
																			<t:outputText value="#{msg['assetClassesSearch.assetClassDescription']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.description}" rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	<t:column id="AssetStatusEnCol" sortable="true" rendered="#{pages$assetClassManage.isEnglishLocale}">
																		<f:facet  name="header">
																			<t:outputText value="#{msg['assetClassesSearch.assetClassStatus']}"  />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.statusEn}" rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	<t:column id="AssetStatusArCol" sortable="true" rendered="#{! pages$assetClassManage.isEnglishLocale}">
																		<f:facet  name="header">
																			<t:outputText value="#{msg['assetClassesSearch.assetClassStatus']}"  />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT" value="#{dataItem.statusAr}"  rendered="#{dataItem.isDeleted == 0}" />
																	</t:column>
																	
																	<t:column id="actions" rendered="#{!pages$assetClassManage.isViewMode}">
																		<f:facet  name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
																		
																		<t:outputText escape="false" value="&nbsp;" rendered="#{dataItem.isDeleted == 0}"></t:outputText>
																		<t:commandLink   action="#{pages$assetClassManage.cmdEditSubClass_Click}" rendered="#{dataItem.isDeleted == 0}" >
																		   <h:graphicImage title="#{msg['commons.edit']}" 
																				                alt="#{msg['commons.edit']}"
																				                 url="../resources/images/edit-icon.gif"
																				                />
																	    </t:commandLink>
																	    <t:outputText escape="false" value="&nbsp;" rendered="#{dataItem.isDeleted == 0}"></t:outputText>
																		<t:commandLink   action="#{pages$assetClassManage.cmdDeleteSubClass_Click}"  rendered="#{dataItem.isDeleted == 0}">
																		   <h:graphicImage title="#{msg['commons.delete']}" 
																				                alt="#{msg['commons.delete']}"
																				                 url="../resources/images/delete.gif"
																				                />
																	    </t:commandLink>
																	</t:column>
																	
																  </t:dataTable>										
																</t:div>
														
				                                           		<t:div id="gridInfo2" styleClass="contentDivFooter" style="width:100%">
																			<t:panelGrid id="rqtkRecNumTable2"  columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
																				<t:div id="rqtkRecNumDiv2"  styleClass="RECORD_NUM_BG">
																					<t:panelGrid id="rqtkRecNumTbl2" columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
																						<h:outputText value="#{msg['commons.recordsFound']}"/>
																						<h:outputText value=" : "/>
																						<h:outputText value="#{pages$assetClassManage.subClassesRecordSize}"/>																						
																					</t:panelGrid>
																				</t:div>
																				  
																			      <CENTER>
																				   <t:dataScroller id="rqtkscroller2" for="dataTable2" paginator="true"
																					fastStep="1"  paginatorMaxPages="#{pages$assetClassManage.paginatorMaxPages}" immediate="false"
																					paginatorTableClass="paginator"
																					renderFacetsIfSinglePage="true" 
																				    pageIndexVar="pageNumber"
																				    styleClass="SCH_SCROLLER"
																				    paginatorActiveColumnStyle="font-weight:bold;"
																				    paginatorRenderLinkForActive="false" 
																					paginatorTableStyle="grid_paginator" layout="singleTable"
																					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">
								
																					    <f:facet  name="first">
																							<t:graphicImage    url="../#{path.scroller_first}"  id="lblFRequestHistory2"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastrewind">
																							<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRRequestHistory2"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="fastforward">
																							<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFRequestHistory2"></t:graphicImage>
																						</f:facet>
									
																						<f:facet name="last">
																							<t:graphicImage  url="../#{path.scroller_last}"  id="lblLRequestHistory2"></t:graphicImage>
																						</f:facet>
									
								                                                      <t:div id="rqtkPageNumDiv2"   styleClass="PAGE_NUM_BG">
																					   <t:panelGrid id="rqtkPageNumTable2"  styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0" >
																					 					<h:outputText  styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																										<h:outputText   styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																						</t:panelGrid>
																							
																						</t:div>
																				 </t:dataScroller>
																				 </CENTER>
																		      
																	        </t:panelGrid>
								                          			 </t:div>								                          			 														
															</rich:tab>										
														
														<!-- Asset Sub Class Tab - End -->
														
														                                                        
                                                        <!-- Attachments Tab - Start -->
														<rich:tab label="#{msg['contract.attachmentTabHeader']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<!-- Attachments Tab - End -->
														
														<!-- Comments Tab - Start -->
                                                         <rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														<!-- Comments Tab - End -->
																												
												</rich:tabPanel>
												
												<table cellpadding="0" cellspacing="0"  style="width:100%;">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
												</table>
												
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td colspan="6" class="BUTTON_TD">
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$assetClassManage.onSave}"
																	rendered="#{! (pages$assetClassManage.isPopupViewOnlyMode || pages$assetClassManage.isViewMode)}">
															</h:commandButton>
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.reset']}"
																	action="#{pages$assetClassManage.onReset}"
																	rendered="#{! (pages$assetClassManage.isPopupViewOnlyMode || pages$assetClassManage.isViewMode) && !pages$assetClassManage.isEditMode}">
															</h:commandButton>															
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	action="#{pages$assetClassManage.onCancel}"
																	rendered="#{! pages$assetClassManage.isPopupViewOnlyMode}">
															</h:commandButton>	
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	onclick="closeWindow();"
																	rendered="#{pages$assetClassManage.isViewMode}">
															</h:commandButton>	
																																												
														</td>
													</tr>
												</table>
											</div>								
								
									    </h:form>
									</div>
								

									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
           </div>
		</body>
	</html>
</f:view>
