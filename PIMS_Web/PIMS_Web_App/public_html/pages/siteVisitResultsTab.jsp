<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

	<t:div style="width:100%;">
		<t:panelGrid columns="1" cellpadding="0" cellspacing="0" width="100%">
			<t:div styleClass="contentDiv" style="width:98%;">
				<t:dataTable id="siteVisitResultsTable" renderedIfEmpty="true" width="100%" cellpadding="0" 
							cellspacing="0" var="dataItem" binding="#{pages$siteVisitResultsTab.dataTable}" 
							value="#{pages$siteVisitResultsTab.siteVisitResultList}" preserveDataModel="false"
							preserveSort="false" rowClasses="row1,row2" rules="all" rows="9">
					<t:column width="300">
						<f:facet name="header">
							<t:outputText value="#{msg['siteVisit.date']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.visitDate}" styleClass="A_LEFT">
							<f:convertDateTime pattern="#{pages$siteVisitResultsTab.dateFormat}" timeZone="#{pages$siteVisitResultsTab.timeZone}"/>
						</t:outputText>
					</t:column>
					<t:column width="330">
						<f:facet name="header">
							<t:outputText value="#{msg['siteVisit.result']}"/>
						</f:facet>
						<t:outputText value="#{dataItem.result}" styleClass="A_LEFT"/>
					</t:column>
				</t:dataTable>
			</t:div>
			<t:div styleClass="contentDivFooter AUCTION_SCH_RF" style="width:99.1%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1" cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']}"/>
						<h:outputText value=" : "/>
						<h:outputText value="#{pages$siteVisitResultsTab.recordSize}" />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="siteVisitResultScroller" for="siteVisitResultsTable" paginator="true"  
						fastStep="1" paginatorMaxPages="5" immediate="false"
						paginatorTableClass="paginator"
						renderFacetsIfSinglePage="true" 												
						paginatorTableStyle="grid_paginator" layout="singleTable" 
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false" 
						pageIndexVar="siteVisitResultsPageNumber"
						styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}" ></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}" ></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}" ></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}" ></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
							<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.siteVisitResultsPageNumber}"/>
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
		</t:div>
		</t:panelGrid>
	</t:div>
	