
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="javascript" type="text/javascript">
    function onDateChangeComplete()
    {
      document.getElementById('formRenewContract:disablingDiv').style.display='none';
    }
	function onDateChangeStart( )
	{
	    document.getElementById('formRenewContract:disablingDiv').style.display='block';
		document.getElementById('formRenewContract:onContractDurationChangedId').onclick();
	}
	
		
	
	function closeWindow()
	{
	  window.opener="x";
	  window.close();
	}
	
	fields = 1;
	
	var deleteImg = '<img src="../resources/images/delete_icon.png">';
	
	function addElement(editable) 
	{
	 
		
		if (fields>1)
		{
			var row=document.getElementById('chTr').rows[fields+5];
			row.deleteCell(4);
		}
		
		var row=document.getElementById('chTr').insertRow(fields+6);
		row.id = 'ROWCHQ'+fields;
		var a=row.insertCell(0);
		var b=row.insertCell(1);
		var c=row.insertCell(2);
		var d=row.insertCell(3);
		var e=row.insertCell(4);
		
		
		
		var chqName = document.getElementById('chequeName').value;
		var dateName = document.getElementById('dateName').value;
		var amountName = document.getElementById('amountName').value;
		var removeName = document.getElementById('removeName').value;
		
		var richCal = '';
		
		//richCal = '<SPAN id="renewContractListForm:toContractDateRichCalanderPopup"><INPUT class="rich-calendar-input " id="renewContractListForm:toContractDateRichCalanderInputDate" style="VERTICAL-ALIGN: middle" readOnly name="renewContractListForm:toContractDateRichCalanderInputDate" _eventID="5" value="" /><IMG class="rich-calendar-button " id="renewContractListForm:toContractDateRichCalanderPopupButton" style="VERTICAL-ALIGN: middle" src="http://localhost:8988/PIMS_Web_App/faces/a4j_3_1_6.GAorg.richfaces.renderkit.html.iconimages.CalendarIcon/DATB/eAH7cW0fw6znAA8XBA4_" _eventID="4" /><INPUT id="renewContractListForm:toContractDateRichCalanderInputCurrentDate" style="DISPLAY: none" type="hidden" name="renewContractListForm:toContractDateRichCalanderInputCurrentDate" value="03/2009" /></SPAN>';
		
		if (editable == true)
		{
		a.innerHTML=chqName+' # '+fields+' '+amountName+' :';
		b.innerHTML='<input type="text" style="width:120px"  style="wid" class="A_RIGHT_NUM" id="CHQ'+fields+'">';
		c.innerHTML=dateName+':';
		d.innerHTML='<input type="text" id="DATECHQ'+fields+'">'+richCal;
 		//e.innerHTML='<a href=\'#\' onclick=\'removeElement()\'>'+removeName+'</a>';
 		e.innerHTML='<a href=\'#\' onclick=\'removeElement()\'>'+deleteImg+'</a>';
 		} else
 		{
 		a.innerHTML=chqName+' # '+fields+' '+amountName+' :';
		b.innerHTML='<input type="text" style="width:120px" class="A_RIGHT_NUM" readonly="true" id="CHQ'+fields+'">';
		c.innerHTML=dateName+':';
		d.innerHTML='<input type="text" readonly="true" id="DATECHQ'+fields+'">';
 		}
 		
 		
 		fields += 1;
 		
 		
	}
	
	function removeElement() {
 		
 		var d = document.getElementById('chTr');
  		d.deleteRow(fields+5);
  		var removeName = document.getElementById('removeName').value;
  		if (fields>2)
		{
			var row=document.getElementById('chTr').rows[fields+4];
			var e=row.insertCell(4);
			//e.innerHTML='<a href=\'#\' onclick=\'removeElement()\'>'+removeName+'</a>';
			e.innerHTML='<a href=\'#\' onclick=\'removeElement()\'>'+deleteImg+'</a>';
		}
  		
  		
  		fields-=1;
  		
	}
	
	
	
	function generateSeperatedValues() {
	var chques = '';
	var dates = '';
	
		for (var i=1;i<fields;i++)
		{
			var chName = 'CHQ'+i;
			var dtName = 'DATECHQ'+i;	
			
			chques += document.getElementById(chName).value+'|';
			dates +=  document.getElementById(dtName).value+'|';
		}
	
	document.getElementById('formRenewContract:dates').value = dates;
	document.getElementById('formRenewContract:chaques').value = chques;
	
	
	//alert(document.getElementById('formRenewContract:dates').value);
	//alert(document.getElementById('formRenewContract:chaques').value);
	
	}
	
	function showPaymentShchedule(){
	
	//alert(document.getElementById('formRenewContract:dates').value);
	//alert(document.getElementById('formRenewContract:chaques').value);
	
		
			var datesAll = document.getElementById('formRenewContract:dates').value;
			var amountAll = document.getElementById('formRenewContract:chaques').value;
			
			
			var datesArray = datesAll.split("|");
			var amountArray = amountAll.split("|");
			
			
			
			var statusNotSentandApproved = document.getElementById('formRenewContract:statusNotSentAndApproved').value;
			
			for (var i=1;i<datesArray.length;i++)
			{
				if (statusNotSentandApproved == "true")
				addElement(true);
				else
				addElement(false);
				
				var chName = 'CHQ'+i;
				var dtName = 'DATECHQ'+i;
				
				document.getElementById(chName).value = amountArray[i-1];
				document.getElementById(dtName).value = datesArray[i-1];
				
			}
		
	}
	
	function conirm(){
	
	var isViolation = document.getElementById('formRenewContract:finesOrVioExists').value;
	
		if (isViolation == "true")
		{
			var mesg = document.getElementById('confrmMsg').value;
			var x=window.confirm(mesg);
			
			if (x)
				return true;
			else
				return false;
		}
	
	}
	
	
	
	var selectedPaidFacilites = "";
	var rentAmount = 0;
	
	
	//For Adding PaidFacilities
	function chkPaidFacility(field,paidFacilityId,facilityAmount)
	{

	//alert(document.getElementById('formRenewContract:paidFacilitiesAll').value);
	
	selectedPaidFacilites=document.getElementById('formRenewContract:paidFacilitiesAll').value;
	rentAmount = document.getElementById('formRenewContract:newRentAmountHid').value;
		
      if(field.checked)
	  {
	   selectedPaidFacilites = selectedPaidFacilites+","+paidFacilityId;
	   rentAmount = parseFloat(rentAmount)+parseFloat(facilityAmount);
	  }
	  else
	  {
	   	   var delimiters=",";
		   var pArr=selectedPaidFacilites.split(delimiters);
		   selectedPaidFacilites="";
		   for(i=0;i<pArr.length;i++)
		   {
		   if(pArr[i]!=paidFacilityId)
		    {
	       	   if(selectedPaidFacilites.length>0 )
		       {
		             selectedPaidFacilites=selectedPaidFacilites+","+pArr[i];
	
               }
		       else
		             selectedPaidFacilites=pArr[i];
		     
		    }
		    rentAmount = document.getElementById('formRenewContract:newRentAmountHid').value;
		    
		    rentAmount = parseFloat(rentAmount)-parseFloat(facilityAmount);
		   }
	  }
	  
	  document.getElementById('formRenewContract:paidFacilitiesAll').value = selectedPaidFacilites;
	  //document.getElementById('formRenewContract:rentAmount').value = rentAmount;
	  document.getElementById('formRenewContract:newRentAmountHid').value = rentAmount;
	 //alert(selectedPaidFacilites+" -RENT-> "+rentAmount);
	  
	 }
	 
	  function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
      {
               document.getElementById("formRenewContract:hdnPersonId").value=personId;
               document.getElementById("formRenewContract:hdnCellNo").value=cellNumber;
               document.getElementById("formRenewContract:hdnPersonName").value=personName;
               document.getElementById("formRenewContract:hdnPersonType").value=hdnPersonType;
               document.getElementById("formRenewContract:hdnIsCompany").value=isCompany;
               document.forms[0].submit();
     }
            
	function showPaymentSchedulePopUp()
	{
	 var screen_width = screen.width;
	   var screen_height = screen.height;
	   
	   window.open('PaymentSchedule.jsf?totalContractValue='+document.getElementById("formRenewContract:newRentAmountHid").value+
	   '&ownerShipTypeId='+document.getElementById("formRenewContract:ownerShipTypeIdHid").value,'_blank','width='+(screen_width-500)+',height='+(screen_height-300)+',left=300,top=250,scrollbars=yes,status=yes');
	
	}
		
		

</SCRIPT>

<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden">

		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />




		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr>
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table class="greyPanelTable" cellpadding="0" cellspacing="0"
								border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['renewContract.header']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<td width="100%" height="99%" valign="top" nowrap="nowrap">

										<h:form id="formRenewContract" style="width:99.7%;HEIGHT:95%;"
											enctype="multipart/form-data">
											<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
											<div class="SCROLLABLE_SECTION">

												<table id="layoutTable" border="0" class="layoutTable"
													width="96%" style="margin-left: 15px; margin-right: 15px;">
													<tr>
														<td>
															<h:outputText id="errormsg" escape="false"
																styleClass="ERROR_FONT"
																value="#{pages$RenewContract.errorMessages}" />
															<h:outputText id="mmsg" escape="false"
																styleClass="INFO_FONT"
																value="#{pages$RenewContract.msgs}" />

															<h:inputHidden id="contractIdHidden"
																value="#{pages$RenewContract.contractId}" />
															<h:inputHidden id="suggestedRentAmountHid"
																value="#{pages$RenewContract.suggestedRentAmount}" />
															<h:inputHidden id="newRentAmountHid"
																value="#{pages$RenewContract.newRentAmount}" />
															<h:inputHidden id="cashAmountHid"
																value="#{pages$RenewContract.cash}" />
															<h:inputHidden id="ownerShipTypeIdHid"
																value="#{pages$RenewContract.ownerShipTypeIdHid}" />

															<h:inputHidden id="finesOrVioExists"
																value="#{pages$RenewContract.finesOrViolationsExisits}" />
															<h:inputHidden id="dateFormat"
																value="#{pages$RenewContract.dateFormatForDataTable}" />
															<h:inputHidden id="tenatNamehid"
																value="#{pages$RenewContract.tenantName}" />
															<h:inputHidden id="oldRentValueHid"
																value="#{pages$RenewContract.oldRentValue}" />
															<h:inputHidden id="contType"
																value="#{pages$RenewContract.contractType}" />
															<h:inputHidden id="contractstatus"
																value="#{pages$RenewContract.contractStatus}" />
															<h:inputHidden id="contactStartdate"
																value="#{pages$RenewContract.contractStartDateString}" />
															<h:inputHidden id="contactEnddate"
																value="#{pages$RenewContract.contractEndDateString}" />
															<h:inputHidden id="paidFacilitiesAll"
																value="#{pages$RenewContract.paidFacilitiesAll}" />
															<h:inputHidden id="tenantId"
																value="#{pages$RenewContract.tenantId}" />
															<h:inputHidden id="reqId"
																value="#{pages$RenewContract.requestId}" />
															<h:inputHidden id="hdnPersonId"
																value="#{pages$RenewContract.hdnPersonId}"></h:inputHidden>
															<h:inputHidden id="hdnPersonType"
																value="#{pages$RenewContract.hdnPersonType}"></h:inputHidden>
															<h:inputHidden id="hdnPersonName"
																value="#{pages$RenewContract.hdnPersonName}"></h:inputHidden>
															<h:inputHidden id="hdnCellNo"
																value="#{pages$RenewContract.hdnCellNo}"></h:inputHidden>
															<h:inputHidden id="hdnIsCompany"
																value="#{pages$RenewContract.hdnIsCompany}"></h:inputHidden>
															<h:inputHidden id="hdncntrno"
																value="#{pages$RenewContract.contractNumber}" />
															<a4j:commandLink id="onContractDurationChangedId"
																onbeforedomupdate="javaScrip:onDateChangeComplete();"
																action="#{pages$RenewContract.onContractDurationChanged}"
																reRender="rentAmount,suggestedRentAmount,txtactualRent,ttbb,errormsg" />

														</td>
													</tr>
												</table>

												<div class="MARGIN"
													style="width: 96%; height: 80%; margin-bottom: 0px;">

													<table id="table_1" cellpadding="0" cellspacing="0"
														width="100%">

														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText  value="#{path.img_tab_top_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText  value="#{path.img_tab_top_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
														</tr>
													</table>
													<rich:tabPanel id="one1" style="width:100%;height:90%;"
														binding="#{pages$RenewContract.tabPanel}">

														<rich:tab id="ApplicationDetailsTab"
															name="ApplicationDetailsTab"
															label="#{msg['commons.tab.applicationDetails']}">
															<jsp:directive.include
																file="../pages/ApplicationDetails.jsp" />
														</rich:tab>

														<rich:tab id="contractDetailsTab"
															label="#{msg['contract.Contract.Details']}">
															<t:div styleClass="BUTTON_TD">
																<h:commandButton id="btnOldContractDetails"
																	styleClass="BUTTON" style="width:125px"
																	action="#{pages$RenewContract.btnContract_Click}"
																	value="#{msg['renewContract.OldContractDetail']}" />

															</t:div>
															<t:div styleClass="TAB_DETAIL_SECTION" style="width:100%">
																<t:panelGrid id="contractdetailsection"
																	cellpadding="1px" width="100%" cellspacing="3px"
																	styleClass="TAB_DETAIL_SECTION_INNER" columns="4"
																	columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">

																	<h:outputText styleClass="LABEL"
																		value="#{msg['contract.contractNumber']} :" />
																	<h:inputText styleClass="READONLY"
																		value="#{pages$RenewContract.contractNumber}"
																		readonly="true" style="width: 85%; height: 16px" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.status']} :" />
																	<h:inputText styleClass="READONLY"
																		style="width: 85%; height: 16px" id="tabStatus"
																		readonly="true"
																		value="#{pages$RenewContract.contractStatus}" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contract.contractType']} :" />
																	<h:inputText styleClass="READONLY" readonly="true"
																		value="#{pages$RenewContract.contractType}"
																		style="width: 85%; height: 16px" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contract.tenantName']} :" />
																	<t:div>
																		<h:inputText styleClass="READONLY" readonly="true"
																			value="#{pages$RenewContract.tenantName}"
																			style="width: 85%; height: 16px" />
																		<h:graphicImage style="margin-left:13px;"
																			title="#{msg['cancelContract.tenantView']}"
																			url="../resources/images/app_icons/Tenant.png"
																			onclick="javascript:showPersonReadOnlyPopup('#{pages$RenewContract.tenantId}')" />

																	</t:div>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contract.startDate']} :" />
																	<rich:calendar id="contractStartDate"
																		value="#{pages$RenewContract.contractStartDate}"
																		popup="true"
																		datePattern="#{pages$RenewContract.dateFormat}"
																		timeZone="#{pages$RenewContract.timeZone}"
																		showApplyButton="false"
																		disabled="#{!pages$RenewContract.startDateEditPermission && 
											                                        !( pages$RenewContract.pageModeAdd &&
											                                           pages$RenewContract.pageModeEdit && 
											                                           pages$RenewContract.pageModeApproveReject
											                                          )
											                                         }"
																		enableManualInput="false" cellWidth="24px"
																		inputStyle="width:85%"
																		locale="#{pages$RenewContract.locale}"
																		onchanged="onDateChangeStart();" />

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contract.endDate']} :" />
																	<rich:calendar id="contractEndDate"
																		value="#{pages$RenewContract.contractEndDate}"
																		popup="true"
																		datePattern="#{pages$RenewContract.dateFormat}"
																		disabled="#{!pages$RenewContract.pageModeAdd && 
																		!pages$RenewContract.pageModeEdit && 
																		!pages$RenewContract.pageModeApproveReject }"
																		timeZone="#{pages$RenewContract.timeZone}"
																		showApplyButton="false" enableManualInput="false"
																		cellWidth="24px" inputStyle="width:85%"
																		locale="#{pages$RenewContract.locale}"
																		todayControlMode="hidden"
																		onchanged="onDateChangeStart();">

																	</rich:calendar>
																	<h:outputLabel id="tt14tab" styleClass="LABEL"
																		value="#{msg['contract.TotalContractValue']} :" />
																	<h:inputText style="width: 85%; height: 16px"
																		styleClass="A_RIGHT_NUM READONLY" id="rentAmount"
																		readonly="true"
																		binding="#{pages$RenewContract.rentValueTxt}"
																		value="#{pages$RenewContract.newRentAmount}" />
																	<h:outputLabel id="tt12" styleClass="LABEL"
																		rendered="false"
																		value="#{msg['renewContract.DepositCashAmount']} :" />
																	<h:inputText rendered="false"
																		style="width: 85%; height: 16px"
																		styleClass="A_RIGHT_NUM READONLY" id="cashAmount"
																		binding="#{pages$RenewContract.cashValueTxt}"
																		value="#{pages$RenewContract.cash}" readonly="true" />
																	<h:outputLabel styleClass="LABEL" rendered="true"
																		id="oldr"
																		value="#{msg['renewContract.oldRentAmount']} :" />
																	<h:inputText rendered="true"
																		style="width: 85%; height: 16px"
																		styleClass="A_RIGHT_NUM READONLY" readonly="true"
																		id="sugestedamounttxttab"
																		value="#{pages$RenewContract.oldRentValue}" />
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<rich:tab binding="#{pages$RenewContract.tabUnit}"
															id="tabUnit" title="#{msg['contract.tab.UnitInfo']}"
															label="#{msg['contract.tab.UnitInfo']}">

															<t:div styleClass="TAB_DETAIL_SECTION" style="width:100%">
																<t:panelGrid id="unitInfoTable" cellpadding="1px"
																	width="100%" cellspacing="3px"
																	styleClass="TAB_DETAIL_SECTION_INNER" columns="4"
																	columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.unitNumber']}:" />
																	<h:inputText maxlength="20" styleClass="READONLY"
																		id="txtunitNumber" readonly="true"
																		value="#{pages$RenewContract.unitView.unitNumber}"
																		style="width:85%;"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['floor.floorNo']}:"></h:outputLabel>
																	<h:inputText id="txtfloorNumber" styleClass="READONLY"
																		style="width:85%;" readonly="true" maxlength="20"
																		value="#{pages$RenewContract.unitView.floorNumber}" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.endowed/minorName']}:"></h:outputLabel>
																	<h:inputText id="txtEndowedMinorName"
																		style="width:85%;" styleClass="READONLY"
																		readonly="true"
																		value="#{pages$RenewContract.unitView.propertyEndowedName}"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['property.propertyNumber']}:"></h:outputLabel>
																	<h:inputText styleClass="READONLY"
																		id="txtpropertyNumber" style="width:85%;"
																		readonly="true"
																		value="#{pages$RenewContract.unitView.propertyNumber}"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.usage']}:"></h:outputLabel>
																	<h:inputText styleClass="READONLY" id="txtunitusageEn"
																		style="width:85%;"
																		rendered="#{pages$RenewContract.isEnglishLocale}"
																		readonly="true"
																		value="#{pages$RenewContract.unitView.usageTypeEn}"></h:inputText>
																	<h:inputText styleClass="READONLY" id="txtunitusageAr"
																		style="width:85%;"
																		rendered="#{pages$RenewContract.isArabicLocale}"
																		readonly="true"
																		value="#{pages$RenewContract.unitView.usageTypeAr}"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contact.country']}:"></h:outputLabel>
																	<h:inputText styleClass="READONLY" id="txtCountryEn"
																		style="width:85%;"
																		rendered="#{pages$RenewContract.isEnglishLocale}"
																		readonly="true"
																		value="#{pages$RenewContract.unitView.countryEn}"></h:inputText>
																	<h:inputText styleClass="READONLY" id="txtCountryAr"
																		style="width:85%;"
																		rendered="#{pages$RenewContract.isArabicLocale}"
																		readonly="true"
																		value="#{pages$RenewContract.unitView.countryAr}"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.Emirate']}:"></h:outputLabel>
																	<h:inputText styleClass="READONLY" id="txtEmirateEn"
																		style="width:85%;"
																		rendered="#{pages$RenewContract.isEnglishLocale}"
																		readonly="true"
																		value="#{pages$RenewContract.unitView.stateEn}"></h:inputText>
																	<h:inputText styleClass="READONLY" id="txtEmirateAr"
																		style="width:85%;"
																		rendered="#{pages$RenewContract.isArabicLocale}"
																		readonly="true"
																		value="#{pages$RenewContract.unitView.stateAr}"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.area']}:" />
																	<h:inputText styleClass="READONLY" id="txtunitArea"
																		readonly="true"
																		value="#{pages$RenewContract.unitView.unitArea}"
																		style="width:85%;"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['contact.street']}:" />
																	<h:inputText styleClass="READONLY" id="txtstreet"
																		readonly="true"
																		value="#{pages$RenewContract.unitView.street}"
																		style="width:85%;"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.address']}:" />
																	<h:inputText styleClass="READONLY" id="txtaddress"
																		readonly="true"
																		value="#{pages$RenewContract.unitView.unitAddress}"
																		style="width:85%;"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['renewContract.oldUnitRentAmount']}:" />
																	<h:inputText id="txtOldRent" styleClass="READONLY"
																		readonly="true"
																		value="#{pages$RenewContract.oldUnitRentValue}"
																		style="width:85%;"></h:inputText>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.rentValue']}:" />
																	<h:inputText id="txtactualRent"
																		binding="#{pages$RenewContract.txtUnitRentValue}"
																		readonly="#{!pages$RenewContract.unitRentAmountEditable}"
																		value="#{pages$RenewContract.unitRentAmount}"
																		style="width:85%;"></h:inputText>


																</t:panelGrid>
															</t:div>




														</rich:tab>

														<!-- Paid Fcilities Tab Here -->
														<rich:tab id="tabPaidFacilities"
															label="#{msg['contract.Facilities']}"
															action="#{pages$RenewContract.tabPaidFacility_Click}">

															<t:div styleClass="contentDiv" style="align:center;">
																<t:dataTable id="paidFaciliites" rows="9" width="100%"
																	value="#{pages$RenewContract.paidFacilitiesDataList}"
																	binding="#{pages$RenewContract.paidFacilitiesDataTable}"
																	preserveDataModel="true" preserveSort="true"
																	var="paidFacilitiesDataItem" rowClasses="row1,row2"
																	rules="all" renderedIfEmpty="true">
																	<t:column id="colFacilitySelected">


																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.select']}" />
																		</f:facet>
																		<t:selectBooleanCheckbox
																			disabled="#{pages$RenewContract.disableChkBoxes || paidFacilitiesDataItem.isMandatory=='Y'}"
																			value="#{paidFacilitiesDataItem.selected}"
																			onclick="javascript:chkPaidFacility(this,'#{paidFacilitiesDataItem.facilityId}','#{paidFacilitiesDataItem.rent}');">



																		</t:selectBooleanCheckbox>
																	</t:column>

																	<t:column id="col2">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['paidFacilities.facilityName']}" />

																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paidFacilitiesDataItem.facilityName}" />
																	</t:column>
																	<t:column id="col4">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['paidFacilities.facilityDescription']}" />

																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paidFacilitiesDataItem.facilityDescription}" />
																	</t:column>

																	<t:column id="col5">
																		<f:facet name="header">
																			<t:outputText value="#{msg['paidFacilities.Type']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			value="#{paidFacilitiesDataItem.isMandatory=='N'?msg['paidFacilities.IsMandatory.Optional']:msg['commons.tab.requiredDocuments.IsMandatory']}" />
																	</t:column>

																	<t:column id="col7">


																		<f:facet name="header">

																			<t:outputText
																				value="#{msg['paidFacilities.rentValue']}" />

																		</f:facet>
																		<t:outputText title="" styleClass="A_RIGHT_NUM"
																			value="#{paidFacilitiesDataItem.rent}" />
																	</t:column>

																</t:dataTable>
															</t:div>
															<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																style="width:100%;">
																<t:panelGrid cellpadding="0" cellspacing="0"
																	width="100%" columns="2" columnClasses="RECORD_NUM_TD">

																	<t:div styleClass="RECORD_NUM_BG">
																		<t:panelGrid cellpadding="0" cellspacing="0"
																			columns="3" columnClasses="RECORD_NUM_TD">

																			<h:outputText value="#{msg['commons.recordsFound']}" />

																			<h:outputText value=" : " styleClass="RECORD_NUM_TD" />

																			<h:outputText
																				value="#{pages$RenewContract.recordSize}"
																				styleClass="RECORD_NUM_TD" />

																		</t:panelGrid>
																	</t:div>

																	<t:dataScroller id="scroller1" for="paidFaciliites"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$RenewContract.paginatorMaxPages}"
																		immediate="false" paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																		styleClass="SCH_SCROLLER" pageIndexVar="pageNumber"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false">

																		<f:facet name="first">
																			<t:graphicImage
																				url="../resources/images/first_btn.gif"
																				id="UnitExclusionlblF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage
																				url="../resources/images/previous_btn.gif"
																				id="UnitExclusionlblFR"></t:graphicImage>
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage
																				url="../resources/images/next_btn.gif"
																				id="UnitExclusionlblFF"></t:graphicImage>
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage
																				url="../resources/images/last_btn.gif"
																				id="UnitExclusionlblL"></t:graphicImage>
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
																			<h:outputText styleClass="PAGE_NUM"
																				value="#{msg['commons.page']}" />
																			<h:outputText styleClass="PAGE_NUM"
																				style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																				value="#{requestScope.paginatorMaxPages}" />
																		</t:div>
																	</t:dataScroller>
																</t:panelGrid>
															</t:div>
														</rich:tab>

														<rich:tab id="PaymentTab"
															label="#{msg['cancelContract.tab.payments']}">
															<t:div styleClass="BUTTON_TD">
																<h:commandButton id="generatePayments"
																	styleClass="BUTTON" style="width:125px"
																	binding="#{pages$RenewContract.btnGeneratePayments}"
																	action="#{pages$RenewContract.generatePayments}"
																	value="#{msg['renewContract.generatePayments']}" />
																<h:commandButton id="pmt_sch_add" styleClass="BUTTON"
																	value="#{msg['contract.AddpaymentSchedule']}"
																	binding="#{pages$RenewContract.btnAddPayments}"
																	action="#{pages$RenewContract.btnAddPayments_Click}"
																	style="width:150px;" />
															</t:div>


															<t:div id="divtt" styleClass="contentDiv"
																style="width:99%">

																<t:dataTable id="ttbb" width="100%"
																	value="#{pages$RenewContract.paymentSchedules}"
																	binding="#{pages$RenewContract.dataTablePaymentSchedule}"
																	preserveDataModel="false" preserveSort="false"
																	var="paymentScheduleDataItem" rowClasses="row1,row2"
																	renderedIfEmpty="true">
																	<t:column id="select"
																		rendered="#{pages$RenewContract.pageModeCollectPayments}">
																		<f:facet name="header">
																			<t:outputText id="selectTxt"
																				value="#{msg['commons.select']}" />
																		</f:facet>
																		<t:selectBooleanCheckbox id="selectChk"
																			value="#{paymentScheduleDataItem.selected}"
																			rendered="#{ paymentScheduleDataItem.isReceived == 'N' && paymentScheduleDataItem.isDeleted==0 && 
													           ( paymentScheduleDataItem.statusId==pages$RenewContract.paymentSchedulePendingStausId ||
													             paymentScheduleDataItem.statusId==pages$RenewContract.paymentScheduleDraftStausId ) }" />
																	</t:column>
																	<t:column id="col_PaymentNumber"
																		rendered="#{!pages$RenewContract.pageModeAdd}">
																		<f:facet name="header">
																			<t:outputText id="ac20"
																				value="#{msg['paymentSchedule.paymentNumber']}" />
																		</f:facet>
																		<t:outputText id="ac19" styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentNumber}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>
																	<t:column id="col_ReceiptNumber" style="width:15%;">
																		<f:facet name="header">
																			<t:outputText
																				value="#{msg['paymentSchedule.ReceiptNumber']}" />
																		</f:facet>
																		<t:outputText styleClass="A_LEFT"
																			title="#{paymentScheduleDataItem.paymentReceiptNumber}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}"
																			value="#{paymentScheduleDataItem.paymentReceiptNumber}" />
																	</t:column>
																	<t:column id="colTypeEnt"
																		rendered="#{pages$RenewContract.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac17"
																				value="#{msg['paymentSchedule.paymentType']}" />
																		</f:facet>
																		<t:outputText id="ac16" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.typeEn}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>
																	<t:column id="colTypeArt"
																		rendered="#{pages$RenewContract.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac15"
																				value="#{msg['paymentSchedule.paymentType']}" />
																		</f:facet>
																		<t:outputText id="ac14" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.typeAr}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>
																	<t:column id="colDesc" rendered="false">
																		<f:facet name="header">
																			<t:outputText id="descHead"
																				value="#{msg['renewContract.description']}" />
																		</f:facet>
																		<t:outputText id="descText" styleClass="A_LEFT"
																			title=""
																			value="#{paymentScheduleDataItem.description}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>
																	<t:column id="colDueOnt">
																		<f:facet name="header">
																			<t:outputText id="ac13"
																				value="#{msg['paymentSchedule.paymentDueOn']}" />
																		</f:facet>
																		<t:outputText id="ac12" styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentDueOn}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}">
																			<f:convertDateTime
																				pattern="#{pages$RenewContract.dateFormat}"
																				timeZone="#{pages$RenewContract.timeZone}" />
																		</t:outputText>
																	</t:column>
																	<t:column id="colStatusEnt"
																		rendered="#{pages$RenewContract.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac11"
																				value="#{msg['commons.status']}" />
																		</f:facet>
																		<t:outputText id="ac10" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.statusEn}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>
																	<t:column id="colStatusArt"
																		rendered="#{pages$RenewContract.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac9"
																				value="#{msg['commons.status']}" />
																		</f:facet>
																		<t:outputText id="ac8" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.statusAr}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>
																	<t:column id="colModeEnt"
																		rendered="#{pages$RenewContract.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac7"
																				value="#{msg['paymentSchedule.paymentMode']}" />
																		</f:facet>
																		<t:outputText id="ac6" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentModeEn}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>
																	<t:column id="colModeArt"
																		rendered="#{pages$RenewContract.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac5"
																				value="#{msg['paymentSchedule.paymentMode']}" />
																		</f:facet>
																		<t:outputText id="ac4" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentModeAr}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>
																	<t:column id="colAmountt">
																		<f:facet name="header">
																			<t:outputText id="ac1"
																				value="#{msg['paymentSchedule.amount']}" />
																		</f:facet>
																		<t:outputText id="ac2" styleClass="A_RIGHT_NUM"
																			value="#{paymentScheduleDataItem.amount}"
																			rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																	</t:column>
																	<t:column id="ActionPaymentCol">
																		<f:facet name="header">
																			<t:outputText id="ac3"
																				value="#{msg['commons.action']}" />
																		</f:facet>
																		<h:commandLink
																			action="#{pages$RenewContract.btnEditPaymentSchedule_Click}">
																			<h:graphicImage id="editPayments"
																				title="#{msg['commons.edit']}"
																				url="../resources/images/edit-icon.gif"
																				rendered="#{paymentScheduleDataItem.isReceived == 'N' && 
																		            !pages$RenewContract.pageModeComplete && 
																		            !pages$RenewContract.pageModeView &&
																		            paymentScheduleDataItem.isDeleted==0
																		               
																		 }" />
																		</h:commandLink>
																		<h:commandLink
																			action="#{pages$RenewContract.btnPrintPaymentSchedule_Click}">
																			<h:graphicImage id="printPayments"
																				title="#{msg['commons.print']}"
																				url="../resources/images/app_icons/print.gif"
																				rendered="#{paymentScheduleDataItem.isReceived == 'Y'}" />
																		</h:commandLink>
																		<h:commandLink
																			action="#{pages$RenewContract.btnViewPaymentDetails_Click}">
																			<h:graphicImage id="viewPayments"
																				title="#{msg['commons.group.permissions.view']}"
																				url="../resources/images/detail-icon.gif"
																				rendered="#{paymentScheduleDataItem.isDeleted==0}" />
																		</h:commandLink>




																		<h:commandLink id="deleteLink"
																			action="#{pages$RenewContract.deletePayment}">
																			<h:graphicImage style="margin-left:13px;"
																				title="#{msg['commons.delete']}"
																				url="../resources/images/delete_icon.png"
																				rendered="#{paymentScheduleDataItem.typeId==pages$RenewContract.rentIdFromDomainData && 
																		            paymentScheduleDataItem.isReceived == 'N'  && 
																		            paymentScheduleDataItem.isDeleted==0 &&
																		            (  
																		               pages$RenewContract.pageModeAdd ||
											                                           pages$RenewContract.pageModeEdit || 
											                                           pages$RenewContract.pageModeApproveReject
											                                           )
											                                           }" />
																		</h:commandLink>
																	</t:column>
																</t:dataTable>
															</t:div>
															<t:div>
																<t:div styleClass="BUTTON_TD">
																	<h:commandButton
																		binding="#{pages$RenewContract.btnCollectPayment}"
																		value="#{msg['settlement.actions.collectpayment']}"
																		styleClass="BUTTON"
																		action="#{pages$RenewContract.openReceivePaymentsPopUp}"
																		style="width: 119px" />
																</t:div>
																<t:div styleClass="DETAIL_SECTION">
																	<t:panelGrid width="100%" columns="4" cellpadding="1px"
																		cellspacing="5px" styleClass="DETAIL_SECTION_INNER">
																		<t:outputLabel value="#{msg['payments.totalCash']} :" />
																		<t:outputLabel
																			value="#{pages$RenewContract.totalCashAmount}" />
																		<t:outputLabel value="#{msg['payments.totalChq']} :" />
																		<t:outputLabel
																			value="#{pages$RenewContract.totalChqAmount}" />
																		<t:outputLabel
																			value="#{msg['payments.totalDeposit']} :" />
																		<t:outputLabel
																			value="#{pages$RenewContract.totalDepositAmount}" />
																		<t:outputLabel value="#{msg['payments.totalRent']} :" />
																		<t:outputLabel
																			value="#{pages$RenewContract.totalRentAmount}" />
																		<t:outputLabel value="#{msg['payments.totalFees']} :" />
																		<t:outputLabel
																			value="#{pages$RenewContract.totalFees}" />
																		<t:outputLabel value="#{msg['payments.totalFines']} :" />
																		<t:outputLabel
																			value="#{pages$RenewContract.totalFines}" />
																		<t:outputLabel
																			value="#{msg['payments.noOfInstallments']} :" />
																		<t:outputLabel
																			value="#{pages$RenewContract.installments}" />
																		<t:outputLabel
																			value="#{msg['payments.totalRealizedRentAmount']} :" />
																		<t:outputLabel
																			value="#{pages$RenewContract.totalRealizedRentAmount}" />

																	</t:panelGrid>
																</t:div>
															</t:div>
														</rich:tab>



														<rich:tab id="reqHist"
															label="#{msg['commons.tab.requestHistory']}"
															action="#{pages$RenewContract.tabRequestHistory_Click}">

															<t:div id="reqHistDiv" styleClass="A_LEFT">
																<%@ include file="requestTasks.jsp"%>
															</t:div>

														</rich:tab>


														<rich:tab label="#{msg['contract.attachmentTabHeader']}">

															<t:div styleClass="A_LEFT">
																<%@ include file="../pages/attachment/attachment.jsp"%>
															</t:div>

														</rich:tab>

														<rich:tab label="#{msg['contract.commentsTabHeader']}">


															<t:div styleClass="A_LEFT">
																<%@ include file="notes/notes.jsp"%>
															</t:div>

														</rich:tab>
														<%--  Violation Tab Start --%>
														<rich:tab label="#{msg['violation.violations']}"
															action="#{pages$RenewContract.populateViolations}"
															binding="#{pages$RenewContract.violationsTab}">
															<t:div style="height:240px;overflow-y:scroll;width:100%;">
																<t:div styleClass="contentDiv">
																	<t:dataTable id="dt1" width="100%"
																		value="#{pages$RenewContract.violations}" rows="5"
																		preserveDataModel="false" preserveSort="false"
																		var="dataItem" rowClasses="row1,row2" rules="all"
																		renderedIfEmpty="true">
																		<t:column id="colVioContract" sortable="true"
																			style="width:15%;">
																			<f:facet name="header">
																				<t:outputText
																					value="#{msg['contract.contractNumber']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				style="WHITE-SPACE: normal;"
																				value="#{dataItem.contractNumber}" />
																		</t:column>
																		<t:column id="colViolUnitNumber" sortable="true"
																			style="width:15%;">
																			<f:facet name="header">
																				<t:outputText id="otxtUnitNumbet"
																					value="#{msg['unit.unitNumber']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				style="WHITE-SPACE: normal;"
																				value="#{dataItem.unitNumber}" />
																		</t:column>
																		<t:column id="colViolationDate" sortable="true"
																			style="width:15%;">
																			<f:facet name="header">
																				<t:outputText id="otxtVolDate"
																					value="#{msg['violation.Date']}" />
																			</f:facet>
																			<t:outputText id="otxtVolDateValue"
																				styleClass="A_LEFT"
																				value="#{dataItem.violationDateString}" />

																		</t:column>
																		<t:column id="description" sortable="true"
																			style="width:15%;">
																			<f:facet name="header">
																				<t:outputText
																					value="#{msg['violationDetails.description']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.description}" />
																		</t:column>
																		<t:column id="typeEn" sortable="true"
																			rendered="#{pages$RenewContract.isEnglishLocale}"
																			style="width:15%;">
																			<f:facet name="header">
																				<t:outputText value="#{msg['inspection.type']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.typeEn}" />
																		</t:column>
																		<t:column id="typeAr" sortable="true"
																			rendered="#{pages$RenewContract.isArabicLocale}"
																			style="width:15%;">
																			<f:facet name="header">
																				<t:outputText value="#{msg['inspection.type']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.typeAr}" />
																		</t:column>
																		<t:column id="categoryEn" sortable="true"
																			rendered="#{pages$RenewContract.isEnglishLocale}"
																			style="width:15%;">
																			<f:facet name="header">
																				<t:outputText value="#{msg['violation.category']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.categoryEn}" />
																		</t:column>
																		<t:column id="categoryAr" sortable="true"
																			rendered="#{pages$RenewContract.isArabicLocale}"
																			style="width:15%;">
																			<f:facet name="header">
																				<t:outputText value="#{msg['violation.category']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.categoryAr}" />
																		</t:column>
																		<t:column id="StatusEn" sortable="true"
																			rendered="#{pages$RenewContract.isEnglishLocale}"
																			style="width:15%;">
																			<f:facet name="header">
																				<t:outputText value="#{msg['inspection.status']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.statusEn}" />
																		</t:column>
																		<t:column id="StatusAr" sortable="true"
																			rendered="#{pages$RenewContract.isArabicLocale}"
																			style="width:15%;">
																			<f:facet name="header">
																				<t:outputText value="#{msg['inspection.status']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{dataItem.statusAr}" />
																		</t:column>

																	</t:dataTable>
																</t:div>
															</t:div>

														</rich:tab>
														<%--  Violation Tab Finish --%>





													</rich:tabPanel>


													<table id="table_BottomTab" cellpadding="0" cellspacing="0"
														width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>


													<BR />

													<table id="tablr_Btn" cellpadding="1px" cellspacing="3px"
														width="100%">
														<tr>

															<td colspan="12" class="BUTTON_TD">

																<pims:security screen="Pims.Contract.Renew"
																	action="create">
																	<h:commandButton
																		binding="#{pages$RenewContract.saveButton}"
																		styleClass="BUTTON"
																		value="#{msg['commons.saveButton']}"
																		action="#{pages$RenewContract.saveDraft}"
																		style="width: 70px" />
																	<h:commandButton
																		binding="#{pages$RenewContract.sendForApprovalButton }"
																		styleClass="BUTTON"
																		value="#{msg['commons.submitButton']}"
																		action="#{pages$RenewContract.sendForApproval}"
																		style="width: 70px" />

																	<h:commandButton
																		onclick="if (!confirm('#{msg['confirmMsg.cancelRenewContract']}')) return false;"
																		binding="#{pages$RenewContract.btnCancelRenewContractButton}"
																		styleClass="BUTTON" value="#{msg['commons.cancel']}"
																		action="#{pages$RenewContract.cancelRenewContract}"
																		style="width: 70px" />

																</pims:security>
																<pims:security
																	screen="Pims.Contract.Renew.ApproveReject"
																	action="create">
																	<h:commandButton
																		binding="#{pages$RenewContract.approveButton}"
																		styleClass="BUTTON" value="#{msg['commons.approve']}"
																		action="#{pages$RenewContract.appoveTask}"
																		style="width: 70px" />
																	<h:commandButton
																		binding="#{pages$RenewContract.rejectButton}"
																		styleClass="BUTTON" value="#{msg['commons.reject']}"
																		action="#{pages$RenewContract.rejectTask}"
																		style="width: 70px" />
																</pims:security>
																<h:commandButton id="completReq"
																	binding="#{pages$RenewContract.reqCompleteButton}"
																	action="#{pages$RenewContract.completeReq}"
																	styleClass="BUTTON" value="#{msg['commons.complete']}"
																	style="width:70px;" />
																<h:commandButton
																	binding="#{pages$RenewContract.btnRegisterInEjari}"
																	styleClass="BUTTON" value="#{msg['contract.btn.Ejari']}"
																	action="#{pages$RenewContract.onRenewInEjari}"
																	style="width: auto;" />
																<h:commandButton id="btnPrint" styleClass="BUTTON"
																	action="#{pages$RenewContract.btnPrint_Click}"
																	binding="#{pages$RenewContract.btnPrint}"
																	value="#{msg['commons.print']}" />
																<h:commandButton id="btnPrintDisclosure"
																	styleClass="BUTTON"
																	binding="#{pages$RenewContract.btnPrintDisclosure}"
																	style="width:auto;"
																	action="#{pages$RenewContract.btnPrintDisclosure_Click}"
																	value="#{msg['commons.button.printDisclosure']}" />

															</td>
														</tr>
													</table>


												</div>



											</div>


										</h:form>

									</td>



									<h:inputHidden id="chequeName"
										value="#{msg['renewContract.cheque']}" />
									<h:inputHidden id="dateName" value="#{msg['commons.date']}" />
									<h:inputHidden id="amountName" value="#{msg['commons.amount']}" />
									<h:inputHidden id="removeName" value="#{msg['commons.remove']}" />
									<h:inputHidden id="confrmMsg"
										value="#{msg['renewContract.finesAndViolationsExisitsContinue']}" />

									</td>
								</tr>

								</td>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">

							<table id="table_footer" width="100%" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel id="footman"
											value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

		</body>

	</html>
</f:view>