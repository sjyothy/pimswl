
<t:div
	style="height:200px;overflow-y:scroll;overflow-x:hidden;width:100%;
																			margin: 0 auto;
																			margin-left: 0px;
																			margin-right: 0px;">
	<t:panelGrid border="0" width="100%" cellpadding="1" cellspacing="1">
		<t:panelGroup>
			<%--Column 1,2,3,4 Starts--%>
			<t:panelGrid columns="4"
				columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2">

				<h:outputLabel value="#{msg['educationAspects.level']} :"
					style="font-weight:normal;" styleClass="TABLE_LABEL" />
				<h:selectOneMenu id="level"
					value="#{pages$ResearchFormBeneficiary.view.educationAspects.educationLevelId}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
					<f:selectItems value="#{pages$ApplicationBean.allEducationLevel}" />
				</h:selectOneMenu>

				<h:outputLabel
					value="#{msg['educationAspects.educationLevelDesc']} :"
					style="font-weight:normal;" styleClass="TABLE_LABEL" />
				<h:inputTextarea onkeyup="javaScript:removeExtraCharacter(this,50)"
					onkeypress="javaScript:removeExtraCharacter(this,50)"
					onkeydown="javaScript:removeExtraCharacter(this,50)"
					onblur="javaScript:removeExtraCharacter(this,50)"
					onchange="javaScript:removeExtraCharacter(this,50)"
					value="#{pages$ResearchFormBeneficiary.view.educationAspects.educationLevelDesc}" />

				<h:outputLabel value="#{msg['educationAspects.grade']} :"
					style="font-weight:normal;" styleClass="TABLE_LABEL" />
				<h:selectOneMenu id="grade"
					value="#{pages$ResearchFormBeneficiary.view.educationAspects.gradeId}">
					<f:selectItem itemValue="-1"
						itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
					<f:selectItems value="#{pages$ApplicationBean.allGrades}" />
				</h:selectOneMenu>

				<h:outputLabel value="#{msg['educationAspects.gradeDesc']} :"
					style="font-weight:normal;" styleClass="TABLE_LABEL" />
				<h:inputTextarea onkeyup="javaScript:removeExtraCharacter(this,50)"
					onkeypress="javaScript:removeExtraCharacter(this,50)"
					onkeydown="javaScript:removeExtraCharacter(this,50)"
					onblur="javaScript:removeExtraCharacter(this,50)"
					onchange="javaScript:removeExtraCharacter(this,50)"
					value="#{pages$ResearchFormBeneficiary.view.educationAspects.gradeDesc}" />


				<h:outputLabel value="#{msg['educationAspects.instituteName']} :"
					style="font-weight:normal;" styleClass="TABLE_LABEL" />
				<h:inputText
					value="#{pages$ResearchFormBeneficiary.view.educationAspects.instituteName}"></h:inputText>
			</t:panelGrid>
		</t:panelGroup>
		<%--Column 1,2,3,4 Ends--%>
	</t:panelGrid>
	<t:div styleClass="contentDiv"
		style="width: 95%;overflow-y:hidden;overflow-x:hidden;">
		<t:dataTable width="100%"
			rows="#{pages$ResearchFormBeneficiary.paginatorRows}"
			binding="#{pages$ResearchFormBeneficiary.educationAspectDT}"
			value="#{pages$ResearchFormBeneficiary.view.educationAspectsList}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="edudate" width="10%" sortable="false"
				style="white-space: normal;">

				<f:facet name="header">
					<t:outputText value="#{msg['commons.date']}" />
				</f:facet>
				<t:outputText value="#{dataItem.createdOn}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>

			<t:column sortable="false" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText value="#{msg['educationAspects.instituteName']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" styleClass="A_LEFT"
					value="#{dataItem.instituteName}" />
			</t:column>
			<t:column sortable="false" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText value="#{msg['educationAspects.grade']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" styleClass="A_LEFT"
					value="#{pages$ResearchFormBeneficiary.englishLocale?dataItem.gradeEn:dataItem.gradeAr}" />
			</t:column>

			<t:column sortable="false" style="white-space: normal;">
				<f:facet name="header">
					<t:outputText value="#{msg['educationAspects.level']}" />
				</f:facet>
				<t:outputText style="white-space: normal;" styleClass="A_LEFT"
					value="#{pages$ResearchFormBeneficiary.englishLocale?dataItem.educationLevelEn:dataItem.educationLevelAr}" />
			</t:column>
			<t:column id="Education" sortable="false"
				style="white-space: normal;">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.action']}" />
				</f:facet>
				<t:commandLink
					onclick="if (!confirm('#{msg['migrationProject.confirmMsg.sureToDelete']}')) return false;"
					action="#{pages$ResearchFormBeneficiary.deleteEducationAspects}">
					<h:graphicImage title="#{msg['commons.delete']}"
						style="cursor:hand;" url="../resources/images/delete.gif" />&nbsp;
																	</t:commandLink>
			</t:column>

		</t:dataTable>
	</t:div>
</t:div>