
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="javascript" type="text/javascript">	
		function  closeWindow(parentVar)
		{
		
		
			window.opener.document.forms[0].submit();
			window.close();
		}
		
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg" />
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />

			<style>
span {
	PADDING-RIGHT: 5px;
	PADDING-LEFT: 5px;
}
</style>


		</head>
	
		<body  dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">
<script>
function verifyBeforePost()
		{
		
		  
		  document.getElementById('formSearchMember:postToGrp').disabled = "true";
		  
		  document.getElementById('formSearchMember:lnkPostToGRP').onclick();
		}
</script>


			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<tr width="100%">

					<td width="100%" valign="top" class="divBackgroundBody"
						height="610px">
						<table width="100%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr >
								<td class="HEADER_TD">
											<h:outputLabel value="#{msg['grp.verificationHeading']}"
												styleClass="HEADER_FONT" />
									</td>
								</tr>
						 </table>
						  <table width="100%" class="greyPanelMiddleTable"
										cellpadding="0" cellspacing="0" border="0">
										<tr valign="top">
											<td height="100%" valign="top" nowrap="nowrap"
												background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
												width="1">
											</td>

											<td width="100%" height="576px" valign="top" nowrap="nowrap">




												<div>
													<h:form id="formSearchMember" style="width:98%">
			                                            <t:panelGrid id="messagestbl"  border="0" styleClass="layoutTable" width="94%" style="margin-left:15px;margin-right:15px;" columns="1" >
															<h:outputText id="errormsg" escape="false" styleClass="ERROR_FONT" value="#{pages$verifyGRPPosting.errorMessages}"/>
                                                            <h:inputHidden id="finId" value = "#{pages$verifyGRPPosting.financialId}"/>  								
                                                            <h:commandLink id="lnkPostToGRP" action="#{pages$verifyGRPPosting.confirmPosting}" /> 						
														</t:panelGrid>
														<div class="MARGIN">
															<table cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<td style="FONT-SIZE: 0px;">
																		<IMG
																			src="../<h:outputText value="#{path.img_section_left}"/>"
																			class="TAB_PANEL_LEFT" />
																	</td>
																	<td width="100%" style="FONT-SIZE: 0px;">
																		<IMG
																			src="../<h:outputText value="#{path.img_section_mid}"/>"
																			class="TAB_PANEL_MID" />
																	</td>
																	<td style="FONT-SIZE: 0px;">
																		<IMG
																			src="../<h:outputText value="#{path.img_section_right}"/>"
																			class="TAB_PANEL_RIGHT" />
																	</td>
																</tr>
															</table>
															<div class="contentDiv"  style="width: 99.2%;">
																<t:dataTable id="dt1"
																	value="#{pages$verifyGRPPosting.transactionsDataList}"
																	binding="#{pages$verifyGRPPosting.transactionsGrid}"
																	rows="#{pages$verifyGRPPosting.paginatorRows}"
																	preserveDataModel="false" preserveSort="false"
																	var="dataItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true" width="100%"  rowId="#{dataItem.financialTransactionId}">
																	
																	<t:column id="tenderDescColen" width="20%"
																		sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['grp.ownership']}" />
																		</f:facet>
																		
																		<t:outputText 
																		value="#{dataItem.ownerShipTypeEn}"
																		rendered="#{pages$verifyGRPPosting.isEnglishLocale}" />
																		
																		<t:outputText value="#{dataItem.ownerShipTypeAr}" 
																		rendered="#{pages$verifyGRPPosting.isArabicLocale}" />
																	</t:column>
																
																	<t:column id="tenderTypeCol" width="20%"
																		sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['grp.paymentType']}" />
																		</f:facet>
																		<t:outputText  
																		value="#{dataItem.paymentTypeEn}"
																		rendered="#{pages$verifyGRPPosting.isEnglishLocale}" />
																		
																		<t:outputText  
																		value="#{dataItem.paymentTypeAr}"
																		rendered="#{pages$verifyGRPPosting.isArabicLocale}" />
																	
																		
																	</t:column>
																		<t:column id="paymentMethodCol" width="20%"
																sortable="true">
																<f:facet name="header">
																	<t:outputText style="white-space: normal;"
																		value="#{msg['grp.paymentMethod']}" />
																</f:facet>
																<t:outputText style="white-space: normal;"
																	value="#{dataItem.paymentMethodName}" />
															</t:column>
																	<t:column id="grpAccnum" width="20%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['grp.grpAccountNo']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.grpAccountNumber}" />
																	</t:column>
																	<t:column id="colAmount" width="20%" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['grp.amount']}" />
																		</f:facet>
																		<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.amount}" >
																		<f:convertNumber pattern="#{pages$verifyGRPPosting.numberFormat}"/>
																	</t:outputText>
																
																	</t:column>
																	

																</t:dataTable>
															</div>
															<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																style="width:99.8%;">
																<table cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td class="RECORD_NUM_TD">
																			<div class="RECORD_NUM_BG">
																				<table cellpadding="0" cellspacing="0">
																					<tr>
																						<td class="RECORD_NUM_TD">
																							<h:outputText
																								value="#{msg['commons.recordsFound']}" />
																						</td>
																						<td class="RECORD_NUM_TD">
																							<h:outputText value=" : " />
																						</td>
																						<td class="RECORD_NUM_TD">
																							<h:outputText
																								value="#{pages$verifyGRPPosting.recordSize}" />
																						</td>
																					</tr>
																				</table>
																			</div>
																		</td>
																		<td class="BUTTON_TD" style="width: 50%">
																			<t:dataScroller id="scroller" for="dt1"
																				paginator="true" fastStep="1"
																				paginatorMaxPages="#{pages$verifyGRPPosting.paginatorMaxPages}"
																				immediate="false" paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true"
																				paginatorTableStyle="grid_paginator"
																				layout="singleTable"
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																				paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																				pageIndexVar="pageNumber" styleClass="SCH_SCROLLER">

																				<f:facet name="first">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblF"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastrewind">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastforward">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																				</f:facet>
																				<f:facet name="last">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblL"></t:graphicImage>
																				</f:facet>
																				<t:div styleClass="PAGE_NUM_BG" >
																					<table cellpadding="0" cellspacing="0">
																						<tr>
																							<td>
																								<h:outputText styleClass="PAGE_NUM"
																									value="#{msg['commons.page']}" />
																							</td>
																							<td>
																								<h:outputText styleClass="PAGE_NUM"
																									style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																									value="#{requestScope.pageNumber}" />
																							</td>
																						</tr>
																					</table>
																					</t:div>
																			</t:dataScroller>

																		</td>
																	</tr>
																</table>
															</t:div>
	                                                        <table width="99%" border="0">
																<tr>
																	<td colspan="6" class="BUTTON_TD">
																		<h:commandButton id="postToGrp" styleClass="BUTTON" 
																		 type="button"
				
				
																		value="#{msg['grp.post']}"
																		onclick="verifyBeforePost();"
																		 
																		 />
																	</td>
																</tr>
															</table>
														</div>

													</h:form>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
	<script language="javascript" type="text/javascript">
	
		     var finId = document.getElementById('formSearchMember:finId').value;
		     if(finId.length>0)
		     {
	           	var tbl =document.getElementById('formSearchMember:dt1');
	            var trs = tbl.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
	            for (var i = 0; i < trs.length; i++) 
	            {
	               trs[i].style.backgroundColor = '';
	                if(trs[i].getAttribute("id")== finId)
		              	trs[i].style.backgroundColor = '#f9a60c';
	           	}
           	}
	
	</script>
						
		</body>
</html>
</f:view>

