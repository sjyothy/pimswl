<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Amaf Distribution percentage
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

    function closeWindow()
    {
    	window.close();
    
    }
    
    function onMessageFromManage()
    {
      document.getElementById("searchFrm:onMessageFromManage").onclick(); 
    }
    
    function openPopupAdd()
	{
		var screen_width = screen.width;
       	var screen_height = screen.height;
       	var popup_width = screen_width/2;
       	var popup_height = screen_height/4;
       	var leftPos = (screen_width-popup_width)/2, 
       		topPos 	=  (screen_height-popup_height)/2;
       	window.open('ManageEndowmentExpenseType.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top='+topPos +',resizable=yes,scrollbars=yes,status=yes');
	
	}   
        
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<c:choose>
				<c:when test="${!pages$searchExpenseTypes.sViewModePopUp}">
					<div class="containerDiv">
				</c:when>
			</c:choose>

			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<c:choose>
					<c:when test="${!pages$searchExpenseTypes.sViewModePopUp}">
						<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>

					</c:when>
				</c:choose>

				<tr width="100%">
					<c:choose>
						<c:when test="${!pages$searchExpenseTypes.sViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>

						</c:when>
					</c:choose>
					<td width="83%" height="470px" valign="top"
						class="divBackgroundBody">

						<h:form id="searchFrm"
							style="WIDTH: 99%;min-height: 100%; #WIDTH: 99%;">
							<div class="SCROLLABLE_SECTION AUC_SCH_SS"
								style="min-height: 100%; width: 100%; # width: 100%;">
								<table width="99%" class="greyPanelTable" cellpadding="0"
									cellspacing="0" border="0">
									<tr>
										<td class="HEADER_TD">
											<h:outputLabel
												value="#{msg['endowmentExpenseType.heading.search']}"
												styleClass="HEADER_FONT" />
										</td>
									</tr>
								</table>

								<table width="99%" style="margin-left: 1px;"
									class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
									border="0" height="100%">
									<tr valign="top">
										<td width="90%" height="100%">

											<t:div id="layoutTable" styleClass="MESSAGE">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:messages></h:messages>
															<h:outputText id="errorMessage"
																value="#{pages$searchExpenseTypes.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText id="SUCCMessage"
																value="#{pages$searchExpenseTypes.successMessages}"
																escape="false" styleClass="INFO_FONT" />

														</td>
													</tr>
												</table>
												<h:commandLink id="onMessageFromManage"
													action="#{pages$searchExpenseTypes.onAddToList}">
												</h:commandLink>
											</t:div>

											<div class="BUTTON_TD"
												style="width: 91%; margin-bottom: 15px;">

												<h:commandButton styleClass="BUTTON" type="button"
													value="#{msg['commons.Add']}"
													onclick="javaScript:openPopupAdd();"
													rendered="#{!pages$searchExpenseTypes.sViewModePopUp}"
													style="width: 75px" />


											</div>

											<div class="contentDiv"
												style="width: 90%; # width: 90%; margin-left: 10px; margin-right: 10px;">

												<t:dataTable id="dt1"
													value="#{pages$searchExpenseTypes.dataList}"
													binding="#{pages$searchExpenseTypes.dataTable}"
													rows="#{pages$searchExpenseTypes.paginatorRows}"
													preserveDataModel="false" preserveSort="false"
													var="dataItem" rowClasses="row1,row2" rules="all"
													renderedIfEmpty="true" width="100%">

													<t:column id="typeNameEn" sortable="true">
														<f:facet name="header">
															<t:commandSortHeader columnName="typeNameEn"
																actionListener="#{pages$searchExpenseTypes.sort}"
																value="#{msg['endowmentExpenseType.lbl.typeNameEn']}"
																arrow="true">
																<f:attribute name="sortField" value="typeNameEn" />
															</t:commandSortHeader>
														</f:facet>
														<t:outputText value="#{dataItem.typeNameEn}"
															style="white-space: normal;" styleClass="A_LEFT" />
													</t:column>
													<t:column id="typeNameAr" sortable="true">
														<f:facet name="header">
															<t:commandSortHeader columnName="typeNameAr"
																actionListener="#{pages$searchExpenseTypes.sort}"
																value="#{msg['endowmentExpenseType.lbl.typeNameAr']}"
																arrow="true">
																<f:attribute name="sortField" value="typeNameAr" />
															</t:commandSortHeader>
														</f:facet>
														<t:outputText value="#{dataItem.typeNameAr}"
															style="white-space: normal;" styleClass="A_LEFT" />
													</t:column>
													<t:column id="description" sortable="true">
														<f:facet name="header">
															<t:commandSortHeader columnName="description"
																actionListener="#{pages$searchExpenseTypes.sort}"
																value="#{msg['commons.description']}" arrow="true">
																<f:attribute name="sortField" value="description" />
															</t:commandSortHeader>
														</f:facet>
														<t:outputText value="#{dataItem.description}"
															style="white-space: normal;" styleClass="A_LEFT" />
													</t:column>

													<t:column id="createdOn" sortable="true">
														<f:facet name="header">
															<t:commandSortHeader columnName="createdOn"
																actionListener="#{pages$searchExpenseTypes.sort}"
																value="#{msg['commons.createdOn']}" arrow="true">
																<f:attribute name="sortField" value="createdOn" />
															</t:commandSortHeader>
														</f:facet>
														<t:outputText value="#{dataItem.createdOn}"
															style="white-space: normal;" styleClass="A_LEFT" />
													</t:column>

													<t:column id="createdBy" sortable="true">
														<f:facet name="header">
															<t:commandSortHeader columnName="createdBy"
																actionListener="#{pages$searchExpenseTypes.sort}"
																value="#{msg['commons.createdBy']}" arrow="true">
																<f:attribute name="sortField" value="createdBy" />
															</t:commandSortHeader>
														</f:facet>
														<t:outputText value="#{dataItem.createdByName}"
															style="white-space: normal;" styleClass="A_LEFT" />
													</t:column>

													<t:column id="actionCol" sortable="false" width="100"
														style="TEXT-ALIGN: center;">
														<f:facet name="header">
															<t:outputText value="#{msg['commons.action']}" />
														</f:facet>

														<t:commandLink
															rendered="#{!pages$searchExpenseTypes.sViewModePopUp}"
															action="#{pages$searchExpenseTypes.onEdit}">

															<h:graphicImage title="#{msg['commons.edit']}"
																url="../resources/images/edit-icon.gif" />
														</t:commandLink>
														<t:commandLink
															rendered="#{pages$searchExpenseTypes.pageModeSelectOnePopUp}"
															action="#{pages$searchExpenseTypes.onSingleSelect}">

															<h:graphicImage title="#{msg['commons.edit']}"
																url="../resources/images/select-icon.gif" />
														</t:commandLink>
													</t:column>



												</t:dataTable>
											</div>



										</td>
									</tr>
								</table>

							</div>
						</h:form>


					</td>
				</tr>
			</table>
			</td>
			</tr>

			<tr style="height: 10px; width: 100%; # height: 10px; # width: 100%;">

				<td colspan="2">
					<c:choose>
						<c:when test="${!pages$searchExpenseTypes.sViewModePopUp}">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>

						</c:when>
					</c:choose>

				</td>
			</tr>

			</table>

			<c:choose>
				<c:when test="${!pages$searchExpenseTypes.sViewModePopUp}">
					</div>
				</c:when>
			</c:choose>
		</body>
	</html>
</f:view>