<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	function performTabClick(elementid)
	{
			
		disableInputs();
		
		document.getElementById("detailsFrm:"+elementid).onclick();
		
		return 
		
	}
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
	}	
	
	
		
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		disableInputs();	
		if( hdnPersonType=='APPLICANT' ) 
		{			    			 
			 document.getElementById("detailsFrm:hdnPersonId").value=personId;
			 document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
			 document.getElementById("detailsFrm:hdnPersonName").value=personName;
			 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
			 document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;
		}
				
	     document.getElementById("detailsFrm:onMessageFromSearchPerson").onclick();
	} 
	
		
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{pages$blockingRequest.pageTitle}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>

																<h:outputText id="errorId"
																	value="#{pages$blockingRequest.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$blockingRequest.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$blockingRequest.pageMode}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$blockingRequest.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$blockingRequest.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$blockingRequest.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$blockingRequest.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$blockingRequest.hdnIsCompany}"></h:inputHidden>


															</td>
														</tr>
													</table>
													<table cellpadding="0" cellspacing="6px" width="100%">
														<tr>
															<td class="BUTTON_TD MARGIN" colspan="6">

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['attachment.file']}"
																	action="#{pages$blockingRequest.onOpenInheritanceFile}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$blockingRequest.isFromMaintenanceRequest}"
																	value="#{msg['blocking.maintenanceRequest']}"
																	onclick="javaScript:openMinorMaintenancePopup('#{pages$blockingRequest.maintenanceRequest.requestId}')" />


															</td>
														</tr>


													</table>

													<!-- Top Fields - Start -->
													<div style="width: 100%;">
														<table cellpadding="1px" cellspacing="5px" width="100%">
															<tr>
																<td>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['searchInheritenceFile.fileNo']}:" />
																</td>
																<td>
																	<h:inputText styleClass="READONLY" readonly="true"
																		value="#{pages$blockingRequest.inheritanceFile.fileNumber}" >
																		
																		</h:inputText>
																</td>
																<td>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['searchInheritenceFile.fileType']}:" />
																</td>
																<td>
																	<h:inputText styleClass="READONLY" readonly="true"
																		value="#{pages$blockingRequest.englishLocale ? pages$blockingRequest.inheritanceFile.fileTypeEn : pages$blockingRequest.inheritanceFile.fileTypeAr}" />
																</td>
															</tr>
															<tr>
																<td>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.status']}:" />
																</td>
																<td>
																	<h:inputText styleClass="READONLY" readonly="true"
																		value="#{pages$blockingRequest.englishLocale ? pages$blockingRequest.inheritanceFile.statusEn : pages$blockingRequest.inheritanceFile.statusAr}" />
																</td>
																<td>
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.createdBy']}:" />
																</td>
																<td>
																	<h:inputText styleClass="READONLY" readonly="true"
																		value="#{pages$blockingRequest.inheritanceFile.createdBy}" />
																</td>

															</tr>
															<tr>
																<td>
																	<h:outputText id="idSendTo" styleClass="LABEL"
																		rendered="#{pages$blockingRequest.showApproveButton }"
																		value="#{msg['mems.investment.label.sendTo']}" />

																</td>
																<td colspan="3">
																	<h:selectOneMenu id="idUserGrps"
																		rendered="#{pages$blockingRequest.showApproveButton }"
																		binding="#{pages$blockingRequest.cmbReviewGroup}"
																		style="width: 200px;">
																		<f:selectItem
																			itemLabel="#{msg['commons.pleaseSelect']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.userGroups}" />
																	</h:selectOneMenu>

																</td>
															</tr>
															<tr>
																<td>
																	<h:outputLabel styleClass="LABEL"
																		rendered="#{pages$blockingRequest.showApproveButton || pages$blockingRequest.showComplete}"
																		value="#{msg['maintenanceRequest.remarks']}" />

																</td>
																<td colspan="3">
																	<t:inputTextarea style="width: 90%;" id="txt_remarks"
																		rendered="#{pages$blockingRequest.showApproveButton || pages$blockingRequest.showComplete}"
																		value="#{pages$blockingRequest.txtRemarks}"
																		onblur="javaScript:validate();"
																		onkeyup="javaScript:validate();"
																		onmouseup="javaScript:validate();"
																		onmousedown="javaScript:validate();"
																		onchange="javaScript:validate();"
																		onkeypress="javaScript:validate();" />
																</td>
															</tr>

														</table>
													</div>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$blockingRequest.tabPanel}"
																style="width: 100%">

																<rich:tab id="applicationTab"
																	label="#{msg['commons.tab.applicationDetails']}">
																	<%@ include file="ApplicationDetails.jsp"%>
																</rich:tab>
																<rich:tab id="paymentsTab"
																	label="#{msg['BlockingRequest.lbl.basicDetails']}">
																	<%@ include file="tabBlockingDetails.jsp"%>
																</rich:tab>
																<rich:tab id="reviewTab"
																	label="#{msg['commons.tab.reviewDetails']}">
																	<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
																</rich:tab>
																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>
																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>
																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$blockingRequest.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">
															<pims:security screen="Pims.MinorMgmt.Block.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$blockingRequest.showSaveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$blockingRequest.onSave}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$blockingRequest.showSubmitButton}"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSubmit"
																	action="#{pages$blockingRequest.onSubmit}" />
																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$blockingRequest.showResubmitButton && pages$blockingRequest.isTaskPresent}"
																	value="#{msg['socialResearch.btn.resubmit']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkResubmit"
																	action="#{pages$blockingRequest.onResubmit}" />
															</pims:security>


															<pims:security
																screen="Pims.MinorMgmt.Block.ApproveReject"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$blockingRequest.showApproveButton && pages$blockingRequest.isTaskPresent}"
																	value="#{msg['commons.approveButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">

																</h:commandButton>
																<h:commandLink id="lnkCollect"
																	action="#{pages$blockingRequest.onApproved}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$blockingRequest.showApproveButton && pages$blockingRequest.isTaskPresent}"
																	value="#{msg['commons.reject']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkRejected"
																	action="#{pages$blockingRequest.onRejected}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$blockingRequest.showApproveButton && pages$blockingRequest.isTaskPresent}"
																	value="#{msg['commons.sendBack']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSendBack"
																	action="#{pages$blockingRequest.onSendBack}" />


																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$blockingRequest.showApproveButton && pages$blockingRequest.isTaskPresent}"
																	value="#{msg['commons.review']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkReviewReq"
																	action="#{pages$blockingRequest.onReviewRequired}" />

															</pims:security>

															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$blockingRequest.showReviewDone && pages$blockingRequest.isTaskPresent}"
																value="#{msg['commons.done']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkReviewDone"
																action="#{pages$blockingRequest.onReviewed}" />

															<pims:security screen="Pims.MinorMgmt.Block.Complete"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$blockingRequest.showComplete && pages$blockingRequest.isTaskPresent}"
																	value="#{msg['commons.complete']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkComplete"
																	action="#{pages$blockingRequest.onComplete}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$blockingRequest.showComplete && pages$blockingRequest.isTaskPresent}"
																	value="#{msg['commons.sendBack']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkRejectBackFromFinance"
																	action="#{pages$blockingRequest.onRejectedFromFinance}" />
															</pims:security>
															<pims:security screen="Pims.MinorMgmt.Unblock.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$blockingRequest.showUnblockButton}"
																	value="#{msg['UnBlockingRequest.title.heading']}"
																	onclick="performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkNavigateToUnblocking"
																	action="#{pages$blockingRequest.onNavigateToUnblocking}" />
															</pims:security>
														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>