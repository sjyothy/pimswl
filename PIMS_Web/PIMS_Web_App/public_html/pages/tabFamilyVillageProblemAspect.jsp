
<t:div id="tabProblem" style="width:100%;">

	<t:panelGrid id="tabProblemGrid" cellpadding="3px" width="100%"
		cellspacing="2px" columns="1" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.tabProblemAspect']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">

		<h:commandButton id="btnAddProblem" styleClass="BUTTON" type="button"
			value="#{msg['commons.Add']}"
			onclick="javaScript:openFamilyVillageProblemAspectPopup('#{pages$tabFamilyVillageProblemAspect.personId}');">


		</h:commandButton>

	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableProblemAspect" rows="5" width="100%"
			value="#{pages$tabFamilyVillageProblemAspect.dataListProblemAspect}"
			binding="#{pages$tabFamilyVillageProblemAspect.dataTableProblemAspect}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="ProblemAspectsCreatedBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="ProblemAspectsCreatedByOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageProblemAspect.englishLocale? 
																				dataItem.createdByEn:
																				dataItem.createdByAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="ProblemAspectsCreatedOn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageProblemAspect.dateFormat}"
						timeZone="#{pages$tabFamilyVillageProblemAspect.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="ProblemAspectsType" sortable="true">
				<f:facet name="header">
					<t:outputText id="ProblemAspectsTypeOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.ProblemAspect.ProblemType']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageProblemAspect.englishLocale? 
																				dataItem.problemTypeEn:
																				dataItem.problemTypeAr}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="problemStartDate" sortable="true">
				<f:facet name="header">
					<h:outputText
						value="#{msg['researchFamilyVillageBeneficiary.lbl.ProblemAspect.ProblemStartDate']}" />
				</f:facet>
				<h:outputText value="#{dataItem.problemStartDate}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageProblemAspect.dateFormat}"
						timeZone="#{pages$tabFamilyVillageProblemAspect.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="ProblemAspectsRiskLevel" sortable="true">
				<f:facet name="header">
					<t:outputText id="ProblemAspectsRisklevelOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.ProblemAspect.RiskLevel']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageProblemAspect.englishLocale? 
																				dataItem.riskLevelEn:
																				dataItem.riskLevelAr}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="ProblemAspectsCommunicatedToOthers" sortable="true">
				<f:facet name="header">
					<t:outputText id="CommunicatedToOthersOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.ProblemAspect.CommunicatedToOthers']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageProblemAspect.englishLocale? 
																				dataItem.communicatedToOthersEn:
																				dataItem.communicatedToOthersAr}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="ProblemAspectsmovementProblems" sortable="true">
				<f:facet name="header">
					<t:outputText id="movementProblemsOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.ProblemAspect.movementProblems']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageProblemAspect.englishLocale? 
																				dataItem.movementProblemsEn:
																				dataItem.movementProblemsAr}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="ProblemAspectsmentalProblems" sortable="true">
				<f:facet name="header">
					<t:outputText id="mentalProblemsOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.ProblemAspect.psychologicalProblem']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageProblemAspect.englishLocale? 
																				dataItem.mentalProblemsEn:
																				dataItem.mentalProblemsAr}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="ProblemAspectspersonalCaring" sortable="true">
				<f:facet name="header">
					<t:outputText id="personalCaringOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.ProblemAspect.personalCaring']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageProblemAspect.englishLocale? 
																				dataItem.personalCaringEn:
																				dataItem.personalCaringAr}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="ProblemAspectsDetails" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.description']}" />
				</f:facet>
				<h:outputText value="#{dataItem.description}">

				</h:outputText>
			</t:column>


		</t:dataTable>
	</t:div>
	<t:div id="ProblemAspectsDivScroller" styleClass="contentDivFooter"
		style="width:99.1%">


		<t:panelGrid id="ProblemAspectsRecNumTable" columns="2"
			cellpadding="0" cellspacing="0" width="100%"
			columnClasses="RECORD_NUM_TD,BUTTON_TD">
			<t:div id="ProblemAspectsRecNumDiv" styleClass="RECORD_NUM_BG">
				<t:panelGrid id="ProblemAspectsRecNumTbl" columns="3"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
					cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
					<h:outputText value="#{msg['commons.recordsFound']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$tabFamilyVillageProblemAspect.recordSizeProblemAspect}" />
				</t:panelGrid>

			</t:div>

			<CENTER>
				<t:dataScroller id="ProblemAspectsScroller"
					for="dataTableProblemAspect" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$tabFamilyVillageProblemAspect.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="ProblemAspectslblFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="ProblemAspectslblFRRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="ProblemAspectslblFFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="ProblemAspectslblLRequestHistory"></t:graphicImage>
					</f:facet>

					<t:div id="ProblemAspectsPageNumDiv" styleClass="PAGE_NUM_BG">
						<t:panelGrid id="ProblemAspectsPageNumTable"
							styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
							cellspacing="0">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:panelGrid>

					</t:div>
				</t:dataScroller>
			</CENTER>

		</t:panelGrid>
	</t:div>
</t:div>

