<%-- Author: Kamran Ahmed--%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<t:div style="height:310px;overflow-y:scroll;width:100%;">
	<t:collapsiblePanel value="true" title="testTitle" var="test2collapsed">
		<f:facet name="header">
			<t:div>
				<h:outputText value="" />
				<t:headerLink immediate="true">

					<h:commandButton styleClass="BUTTON" style="width:auto;"
						value="#{msg['portfolio.manage.tab.assets.btn.add']}"
						onclick="javaScript:onProcessStart();"
						rendered="#{test2collapsed}" />
					<h:commandButton styleClass="BUTTON" style="width:auto;"
						value="#{msg['inheritedAsset.hideAssetFields']}"
						rendered="#{!test2collapsed}" />
				</t:headerLink>
			</t:div>
		</f:facet>
		<f:facet name="closedContent">
			<h:panelGroup>
			</h:panelGroup>
		</f:facet>
		<%-- Inherited Assets Tab --%>
		<t:panelGrid border="0" width="95%" cellpadding="1" cellspacing="1"
			id="basicAssetsInfoGrid">
			<t:panelGroup>
				<%--Column 1,2 Starts--%>
				<t:panelGrid columns="4"
					columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%"
					rendered="#{!(pages$inheritanceFile.viewModePopUp ||pages$inheritanceFile.pageModeView )}">

					<h:outputText value="#{msg['searchAssets.assetNumber']}" />
					<h:panelGroup>
						<h:inputText readonly="true" styleClass="READONLY"
							value="#{pages$inheritedAssetsTab.assetMemsView.assetNumber}" />
						<h:graphicImage title="#{msg['commons.search']}"
							rendered="#{pages$inheritanceFile.showAddAssetButton}"
							style="MARGIN: 1px 1px -5px;cursor:hand;"
							onclick="searchAssetClicked();"
							url="../resources/images/app_icons/Search-Unit.png">
						</h:graphicImage>
						<h:commandLink id="lnkSeachAsset"
							action="#{pages$inheritedAssetsTab.showAssetSearchPopup}"></h:commandLink>
					</h:panelGroup>

					<h:panelGroup>
						<h:outputLabel styleClass="mandatory" value="*" />
						<h:outputText value="#{msg['searchAssets.assetType']}" />
					</h:panelGroup>

					<%--value="#{pages$inheritedAssetsTab.assetMemsView.assetTypeView.assetTypeIdString}"
			This will not work here, it returns the default value i.e= -1 --%>
					<h:selectOneMenu
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.assetTypeView.assetTypeIdString}"
						styleClass="SELECT_MENU">
						<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
							itemValue="-1" />
						<f:selectItems value="#{pages$ApplicationBean.assetTypesList}" />
						<a4j:support event="onchange"
							reRender="ExtraDetailWithEachAssetType,errorMesssages,layoutTable,verifyFromGrid,cncrndDepttGrid"
							action="#{pages$inheritedAssetsTab.assetTypeChanged}" />
					</h:selectOneMenu>

					<h:outputLabel value="#{msg['searchAssets.assetNameEn']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.assetNameEn}"></h:inputText>

					<h:outputLabel value="#{msg['searchAssets.assetNameAr']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.assetNameAr}"></h:inputText>


					<h:outputLabel value="#{msg['searchAssets.assetDesc']}" />
					<h:inputText maxlength="250"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.description}"></h:inputText>

					<h:outputLabel id="lblAssetTotalshare"
						value="#{msg['inheritanceFile.label.totalAssetShare']}" />
					<h:inputText id="txtAssetTotalShare"
						onkeyup="removeNonInteger(this)"
						onkeypress="removeNonInteger(this)"
						onkeydown="removeNonInteger(this)" onblur="removeNonInteger(this)"
						onchange="removeNonInteger(this)"
						binding="#{pages$inheritedAssetsTab.txtAssetTotalShare}"></h:inputText>

					<h:outputLabel
						value="#{msg['inheritanceFile.inheritedAssetTab.label.isManagerAmaf']}" />
					<h:selectBooleanCheckbox id="isManagerAmafId"
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						styleClass="SELECT_MENU"
						binding="#{pages$inheritedAssetsTab.chkIsManagerAmaf}">
						<a4j:support action="#{pages$inheritedAssetsTab.managerAmafClick}"
							event="onclick" reRender="basicAssetsInfoGrid,managerId"></a4j:support>
					</h:selectBooleanCheckbox>

					<h:outputLabel
						value="#{msg['inheritanceFile.inheritedAssetTab.label.manager']}" />
					<h:panelGroup>
						<h:inputText readonly="true"
							binding="#{pages$inheritedAssetsTab.txtManagerName}"
							styleClass="READONLY" />
						<h:graphicImage
							rendered="#{pages$inheritedAssetsTab.showSearchManagerImage}"
							title="#{msg['inheritanceFile.inheritedAssetTab.tooltip.searchManager']}"
							style="MARGIN: 1px 1px -5px;cursor:hand;"
							onclick="searchManagerClicked();"
							url="../resources/images/app_icons/Search-tenant.png">
						</h:graphicImage>
						<h:commandLink id="lnkSeachManager"
							action="#{pages$inheritedAssetsTab.openManagerSearchPopup}"></h:commandLink>
					</h:panelGroup>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.genRev']}" />
					<h:selectBooleanCheckbox id="isRevGenId"
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						binding="#{pages$inheritedAssetsTab.revChk}"
						value="#{pages$inheritedAssetsTab.assetMemsView.incomeExpbool}">
						<a4j:support action="#{pages$inheritedAssetsTab.genRevClicked}"
							event="onclick"
							reRender="basicAssetsInfoGrid,cboRevType,txtExpRev,clndrFrom"></a4j:support>
					</h:selectBooleanCheckbox>

					<h:outputText id="lblRevType"
						value="#{msg['mems.inheritanceFile.fieldLabel.revType']}" />
					<h:selectOneMenu id="cboRevType"
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						binding="#{pages$inheritedAssetsTab.cboRevType}"
						value="#{pages$inheritedAssetsTab.assetMemsView.revenueTypeIdString}"
						styleClass="SELECT_MENU">
						<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
							itemValue="-1" />
						<f:selectItems value="#{pages$ApplicationBean.revenueTypeList}" />
					</h:selectOneMenu>

					<h:outputLabel id="lblExpRev"
						value="#{msg['mems.inheritanceFile.fieldLabel.expRev']}" />
					<h:inputText onkeyup="removeNonNumeric(this)"
						onkeypress="removeNonNumeric(this)"
						onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
						onchange="removeNonNumeric(this)"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						id="txtExpRev" binding="#{pages$inheritedAssetsTab.txtExpRev}"
						value="#{pages$inheritedAssetsTab.assetMemsView.expectedRevenueString}"></h:inputText>

					<h:outputLabel id="lblFrom"
						value="#{msg['mems.inheritanceFile.fieldLabel.from']}" />
					<rich:calendar id="clndrFrom"
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						binding="#{pages$inheritedAssetsTab.clndrFromDate}"
						locale="#{pages$inheritedAssetsTab.locale}" popup="true"
						datePattern="#{pages$inheritedAssetsTab.dateFormat}"
						showApplyButton="false" enableManualInput="false"
						inputStyle="width:185px;height:17px" />
				</t:panelGrid>

			</t:panelGroup>
			<%--Column 3,4 Ends--%>
		</t:panelGrid>

		<t:panelGrid border="0" width="95%" cellpadding="1" cellspacing="1"
			rendered="#{!(pages$inheritanceFile.viewModePopUp ||pages$inheritanceFile.pageModeView )}"
			id="ExtraDetailWithEachAssetType">
			<t:panelGroup>
				<t:div style="width:100%;background-color:#636163;"
					rendered="#{pages$inheritedAssetsTab.landProperties}">
					<h:outputText value="#{msg['inhFile.label.LandnProp']}"
						styleClass="GROUP_LABEL"></h:outputText>
				</t:div>
				<t:panelGrid columns="4" id="landAndPropertiesPanel"
					rendered="#{pages$inheritedAssetsTab.landProperties}"
					columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.govDept']}" />
					<h:selectOneMenu
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.govtDepttView.govtDepttIdString}"
						styleClass="SELECT_MENU">
						<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
							itemValue="-1" />
						<f:selectItems value="#{pages$ApplicationBean.govtDepttList}" />
					</h:selectOneMenu>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.land']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldNumber}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.region']}" />
					<h:inputText maxlength="30"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.regionName}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.devolution']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.devolution}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.landStatus']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldStatus}"></h:inputText>
				</t:panelGrid>
				<t:div style="width:100%;background-color:#636163;"
					rendered="#{pages$inheritedAssetsTab.animals}">
					<h:outputText value="#{msg['inhFile.label.animal']}"
						styleClass="GROUP_LABEL"></h:outputText>
				</t:div>
				<t:panelGrid columns="4" id="animalsPanel"
					rendered="#{pages$inheritedAssetsTab.animals}"
					columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.nofAnimal']}" />
					<h:inputText maxlength="150" onkeyup="removeNonInteger(this)"
						onkeypress="removeNonInteger(this)"
						onkeydown="removeNonInteger(this)" onblur="removeNonInteger(this)"
						onchange="removeNonInteger(this)"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldNumber}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.animalType']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldType}"></h:inputText>
				</t:panelGrid>
				<t:div style="width:100%;background-color:#636163;"
					rendered="#{pages$inheritedAssetsTab.vehciles}">
					<h:outputText value="#{msg['inhFile.label.vehicles']}"
						styleClass="GROUP_LABEL"></h:outputText>
				</t:div>
				<t:panelGrid columns="4" id="vehiclePanel"
					columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%"
					rendered="#{pages$inheritedAssetsTab.vehciles}">

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.govDept']}" />
					<h:selectOneMenu
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.govtDepttView.govtDepttIdString}"
						styleClass="SELECT_MENU">
						<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
							itemValue="-1" />
						<f:selectItems value="#{pages$ApplicationBean.govtDepttList}" />
					</h:selectOneMenu>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.plateNmber']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldNumber}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.vehicleCat']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.vehicleCategory}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.vehicleType']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldType}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.vehicleStatus']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldStatus}"></h:inputText>
				</t:panelGrid>
				<t:div style="width:100%;background-color:#636163;"
					rendered="#{pages$inheritedAssetsTab.transportation}">
					<h:outputText value="#{msg['inhFile.label.transportation']}"
						styleClass="GROUP_LABEL"></h:outputText>
				</t:div>
				<t:panelGrid columns="4" id="transportationPanel"
					rendered="#{pages$inheritedAssetsTab.transportation}"
					columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.plateNmber']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldNumber}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.income']}" />
					<h:inputText maxlength="50" onkeyup="removeNonNumeric(this)"
						onkeypress="removeNonNumeric(this)"
						onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
						onchange="removeNonNumeric(this)"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.amountString}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.transferParty']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldParty}"></h:inputText>
				</t:panelGrid>
				<t:div style="width:100%;background-color:#636163;"
					rendered="#{pages$inheritedAssetsTab.stockshares}">
					<h:outputText value="#{msg['inhFile.label.stocknShare']}"
						styleClass="GROUP_LABEL"></h:outputText>
				</t:div>
				<t:panelGrid columns="4" id="stockAndSharesPanel"
					rendered="#{pages$inheritedAssetsTab.stockshares}"
					columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.company']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.company}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.nofShare']}" />
					<h:inputText maxlength="50" onkeyup="removeNonNumeric(this)"
						onkeypress="removeNonNumeric(this)"
						onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
						onchange="removeNonNumeric(this)"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldNumber}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.listingParty']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldParty}"></h:inputText>

				</t:panelGrid>
				<t:div style="width:100%;background-color:#636163;"
					rendered="#{pages$inheritedAssetsTab.licenses}">
					<h:outputText value="#{msg['inhFile.label.license']}"
						styleClass="GROUP_LABEL"></h:outputText>
				</t:div>
				<t:panelGrid columns="4" id="licensePanel"
					rendered="#{pages$inheritedAssetsTab.licenses}"
					columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.licenseName']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.licenseName}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.licenseNmber']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldNumber}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.licenseStatus']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldStatus}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.rentValue']}" />
					<h:inputText onkeyup="removeNonNumeric(this)"
						onkeypress="removeNonNumeric(this)"
						onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
						onchange="removeNonNumeric(this)"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.amountString}"></h:inputText>
				</t:panelGrid>
				<t:div style="width:100%;background-color:#636163;"
					rendered="#{pages$inheritedAssetsTab.loanLiabilities}">
					<h:outputText value="#{msg['inhFile.label.loanliability']}"
						styleClass="GROUP_LABEL"></h:outputText>
				</t:div>
				<t:panelGrid columns="4" id="loanAndLiabilities"
					rendered="#{pages$inheritedAssetsTab.loanLiabilities}"
					columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.party']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldParty}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
					<h:inputText onkeyup="removeNonNumeric(this)"
						onkeypress="removeNonNumeric(this)"
						onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
						onchange="removeNonNumeric(this)"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.amountString}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.dueDate']}" />
					<rich:calendar
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.dueDate}"
						locale="#{pages$inheritedAssetsTab.locale}" popup="true"
						datePattern="#{pages$inheritedAssetsTab.dateFormat}"
						showApplyButton="false" enableManualInput="false"
						inputStyle="width:185px;height:17px" />
				</t:panelGrid>
				<t:div style="width:100%;background-color:#636163;"
					rendered="#{pages$inheritedAssetsTab.jewellery}">
					<h:outputText value="#{msg['inhFile.label.jewellery']}"
						styleClass="GROUP_LABEL"></h:outputText>
				</t:div>
				<t:panelGrid columns="4" id="jewelleryPanel"
					rendered="#{pages$inheritedAssetsTab.jewellery}"
					columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.jewelType']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldType}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.wightInGms']}" />
					<h:inputText onkeyup="removeNonNumeric(this)"
						onkeypress="removeNonNumeric(this)"
						onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
						onchange="removeNonNumeric(this)"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.weightString}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.nofJewel']}" />
					<h:inputText maxlength="50" onkeyup="removeNonInteger(this)"
						onkeypress="removeNonInteger(this)"
						onkeydown="removeNonInteger(this)" onblur="removeNonInteger(this)"
						onchange="removeNonInteger(this)"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldNumber}"></h:inputText>
				</t:panelGrid>
				<t:div style="width:100%;background-color:#636163;"
					rendered="#{pages$inheritedAssetsTab.cash}">
					<h:outputText value="#{msg['inhFile.label.cash']}"
						styleClass="GROUP_LABEL"></h:outputText>
				</t:div>
				<t:panelGrid columns="4" id="cashPanel"
					rendered="#{pages$inheritedAssetsTab.cash}"
					columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
					<h:inputText onkeyup="removeNonNumeric(this)"
						onkeypress="removeNonNumeric(this)"
						onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
						onchange="removeNonNumeric(this)"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.amountString}"></h:inputText>
				</t:panelGrid>

				<t:div style="width:100%;background-color:#636163;"
					rendered="#{pages$inheritedAssetsTab.bankTransfer}">
					<h:outputText value="#{msg['inhFile.label.bankTransfer']}"
						styleClass="GROUP_LABEL"></h:outputText>
				</t:div>
				<t:panelGrid columns="4" id="bankTransferPanel"
					rendered="#{pages$inheritedAssetsTab.bankTransfer}"
					columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.transferNmber']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldNumber}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
					<h:inputText onkeyup="removeNonNumeric(this)"
						onkeypress="removeNonNumeric(this)"
						onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
						onchange="removeNonNumeric(this)"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.amountString}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.bank']}" />
					<h:selectOneMenu
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.bankView.bankIdString}"
						styleClass="SELECT_MENU">
						<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
							itemValue="-1" />
						<f:selectItems value="#{pages$ApplicationBean.bankList}" />
					</h:selectOneMenu>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.expTransferDate']}" />
					<rich:calendar
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.dueDate}"
						locale="#{pages$inheritedAssetsTab.locale}" popup="true"
						datePattern="#{pages$inheritedAssetsTab.dateFormat}"
						showApplyButton="false" enableManualInput="false"
						inputStyle="width:185px;height:17px" />


				</t:panelGrid>

				<t:div style="width:100%;background-color:#636163;"
					rendered="#{pages$inheritedAssetsTab.cheque}">
					<h:outputText value="#{msg['inhFile.label.cheque']}"
						styleClass="GROUP_LABEL"></h:outputText>
				</t:div>
				<t:panelGrid columns="4" id="chequePanel"
					rendered="#{pages$inheritedAssetsTab.cheque}"
					columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
					<h:inputText onkeyup="removeNonNumeric(this)"
						onkeypress="removeNonNumeric(this)"
						onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
						onchange="removeNonNumeric(this)"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.amountString}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.bank']}" />
					<h:selectOneMenu
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.bankView.bankIdString}"
						styleClass="SELECT_MENU">
						<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
							itemValue="-1" />
						<f:selectItems value="#{pages$ApplicationBean.bankList}" />
					</h:selectOneMenu>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.chequeNmbr']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.fieldNumber}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.dueDate']}" />
					<rich:calendar
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.dueDate}"
						locale="#{pages$inheritedAssetsTab.locale}" popup="true"
						datePattern="#{pages$inheritedAssetsTab.dateFormat}"
						showApplyButton="false" enableManualInput="false"
						inputStyle="width:185px;height:17px" />

				</t:panelGrid>
				<t:div style="width:100%;background-color:#636163;"
					rendered="#{pages$inheritedAssetsTab.pension}">
					<h:outputText value="#{msg['inhFile.label.Pension']}"
						styleClass="GROUP_LABEL"></h:outputText>
				</t:div>
				<t:panelGrid columns="4" id="pensionPanel"
					rendered="#{pages$inheritedAssetsTab.pension}"
					columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
					<h:inputText onkeyup="removeNonNumeric(this)"
						onkeypress="removeNonNumeric(this)"
						onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
						onchange="removeNonNumeric(this)"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.amountString}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.govDept']}" />
					<h:selectOneMenu
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.govtDepttView.govtDepttIdString}"
						styleClass="SELECT_MENU">
						<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
							itemValue="-1" />
						<f:selectItems value="#{pages$ApplicationBean.govtDepttList}" />
					</h:selectOneMenu>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.bank']}" />
					<h:selectOneMenu
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.bankView.bankIdString}"
						styleClass="SELECT_MENU">
						<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
							itemValue="-1" />
						<f:selectItems value="#{pages$ApplicationBean.bankList}" />
					</h:selectOneMenu>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.accountName']}" />
					<h:inputText maxlength="150"
						readonly="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.accountName}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.amafAccount']}" />
					<h:selectBooleanCheckbox
						disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
						value="#{pages$inheritedAssetsTab.assetMemsView.isAmafAccountBool}"></h:selectBooleanCheckbox>
				</t:panelGrid>

			</t:panelGroup>
		</t:panelGrid>
		<t:div style="width:95%;background-color:#636163;"
			rendered="#{!(pages$inheritanceFile.viewModePopUp || pages$inheritanceFile.pageModeView )}">
			<h:outputText value="#{msg['inhFile.label.concerndDeptt']}"
				styleClass="GROUP_LABEL"></h:outputText>
		</t:div>
		<t:panelGrid border="0" width="95%" cellpadding="1" cellspacing="1"
			rendered="#{!(pages$inheritanceFile.viewModePopUp ||pages$inheritanceFile.pageModeView )}"
			id="verifyFromGrid">
			<t:panelGroup>
				<t:panelGrid columns="4" id="cncrndDepttGrid"
					columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.govDept']}" />
					<h:selectOneMenu
						value="#{pages$inheritedAssetsTab.memsAssetCncrndDeptView.govtDepttView.govtDepttIdString}"
						styleClass="SELECT_MENU">
						<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
							itemValue="-1" />
						<f:selectItems value="#{pages$ApplicationBean.govtDepttList}" />
					</h:selectOneMenu>


					<h:outputText
						value="#{msg['mems.inheritanceFile.fieldLabel.bank']}" />

					<h:selectOneMenu
						value="#{pages$inheritedAssetsTab.memsAssetCncrndDeptView.bankView.bankIdString}"
						styleClass="SELECT_MENU">
						<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
							itemValue="-1" />
						<f:selectItems value="#{pages$ApplicationBean.bankList}" />
					</h:selectOneMenu>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.contactPerson']}" />
					<h:inputText maxlength="500"
						value="#{pages$inheritedAssetsTab.memsAssetCncrndDeptView.contactPersonName}"></h:inputText>

					<h:outputLabel value="#{msg['inquiryApplication.cellPhone']}" />
					<h:inputText maxlength="15"
						value="#{pages$inheritedAssetsTab.memsAssetCncrndDeptView.phone1}"></h:inputText>

					<h:outputLabel value="#{msg['contact.officephone']}" />
					<h:inputText maxlength="15"
						value="#{pages$inheritedAssetsTab.memsAssetCncrndDeptView.phone2}"></h:inputText>

					<h:outputText value="#{msg['mems.inheritanceFile.fieldLabel.fax']}" />
					<h:inputText maxlength="15"
						value="#{pages$inheritedAssetsTab.memsAssetCncrndDeptView.fax}"></h:inputText>

					<h:outputLabel
						value="#{msg['mems.inheritanceFile.fieldLabel.email']}" />
					<h:inputText maxlength="50"
						value="#{pages$inheritedAssetsTab.memsAssetCncrndDeptView.email}"></h:inputText>

				</t:panelGrid>
			</t:panelGroup>
		</t:panelGrid>
		<t:div style="width:95%;">
			<t:div styleClass="A_RIGHT">
				<pims:security screen="Pims.MinorMgmt.InheritanceFile.Save" action="view">
					<h:commandButton
					rendered="#{pages$inheritanceFile.showAddAssetButton}"
					action="#{pages$inheritedAssetsTab.addAsset}"
					onclick="javaScript:onProcessStart();"
					value="#{msg['commons.saveButton']}" styleClass="BUTTON"
					style="width:10%;"></h:commandButton>
				</pims:security>
				<pims:security
					screen="Pims.MinorMgmt.InheritanceFile.SaveAfterFinalize" action="view">
					<h:commandButton
					rendered="#{pages$inheritanceFile.showAddAssetButtonAfterFinalize}"
					action="#{pages$inheritedAssetsTab.addAsset}"
					onclick="javaScript:onProcessStart();"
					value="#{msg['commons.saveButton']}" styleClass="BUTTON"
					style="width:10%;"></h:commandButton></pims:security>
				<h:outputText value=" "></h:outputText>
				<h:commandButton
					rendered="#{!(pages$inheritanceFile.viewModePopUp ||pages$inheritanceFile.pageModeView )}"
					action="#{pages$inheritedAssetsTab.clearAllFields}"
					value="#{msg['commons.clear']}" styleClass="BUTTON"
					style="width:10%;"></h:commandButton>
			</t:div>
		</t:div>
	</t:collapsiblePanel>
	<t:div style="height:5px;"></t:div>
	<t:div style="width:95%;">
		<t:div style="height:5px;"></t:div>
		<t:div styleClass="contentDiv" style="width:98%">
			<t:dataTable id="beneficiaryAssetsGrid" rows="1000"
				binding="#{pages$inheritedAssetsTab.inheritedAssetsDataTable}"
				value="#{pages$inheritedAssetsTab.inheritedAssetList}"
				preserveDataModel="false" preserveSort="false" var="dataItem"
				rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
				width="100%">
				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText value="#{msg['assetManage.assetNumber']}" />
					</f:facet>
					<h:commandLink action="#{pages$inheritedAssetsTab.openManageAsset}"
						value="#{dataItem.assetMemsView.assetNumber}" />
				</t:column>

<%-- 
				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText value="#{msg['searchAssets.assetNameEn']}" />
					</f:facet>
					<h:outputText value="#{dataItem.assetMemsView.assetNameEn}" />
				</t:column>
				--%>
				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText value="#{msg['searchAssets.assetNameAr']}" />
					</f:facet>
					<h:outputText value="#{dataItem.assetMemsView.assetNameAr}" />
				</t:column>

				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText value="#{msg['searchAssets.assetType']}" />
					</f:facet>
					<h:outputText
						value="#{pages$inheritedAssetsTab.englishLocale?dataItem.assetMemsView.assetTypeView.assetTypeNameEn:dataItem.assetMemsView.assetTypeView.assetTypeNameAr}" />
				</t:column>
				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['commons.status']}" />
					</f:facet>
					<h:outputText value="#{dataItem.statusAr}">
					</h:outputText>
				</t:column>
				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText value="#{msg['contract.person.Manager']}" />
					</f:facet>
					<h:outputText value="#{dataItem.assetMemsView.managerName}" />
				</t:column>

				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText value="#{msg['study.description']}" />
					</f:facet>
					<h:outputText value="#{dataItem.assetMemsView.description}" />
				</t:column>

				<t:column id="shareValue" sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['inheritanceFile.label.totalAssetShare']}" />
					</f:facet>
					<h:outputText
						value="#{dataItem.assetMemsView.totalAssetShareInFile}" />
				</t:column>

				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['mems.inheritanceFile.fieldLabel.revType']}" />
					</f:facet>
					<h:outputText value="#{dataItem.assetMemsView.revenueTypeString}" />
				</t:column>

				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText value="#{msg['inhDetails.reportLabel.revenue']}" />
					</f:facet>
					<h:outputText
						value="#{dataItem.assetMemsView.expectedRevenueString}" />
				</t:column>

				<t:column sortable="true">
					<f:facet name="header">
						<h:outputText
							value="#{msg['PeriodicDisbursements.columns.amount']}" />
					</f:facet>
					<h:outputText value="#{dataItem.assetMemsView.amountString}">
					</h:outputText>
				</t:column>

				<t:column id="actionCol" sortable="false"
					rendered="#{pages$inheritanceFile.showInheritedAssetTabGridEditItems || 
					            pages$inheritanceFile.showInheritedAssetTabGridConfirmActionItems||
					            pages$inheritanceFile.showInheritedAssetTabGridEditItemsAfterFinalize|| 
					            pages$inheritanceFile.showInheritedAssetTabGridConfirmActionItemsAfterFinalize
					           }">
					<f:facet name="header">
						<t:outputText
							value="#{msg['mems.inheritanceFile.columnLabel.action']}" />
					</f:facet>

					<pims:security screen="Pims.MinorMgmt.InheritanceFile.Save"
						action="view">
						<t:commandLink
							rendered="#{
										!dataItem.isSelected && 
										 
										pages$inheritanceFile.showInheritedAssetTabGridEditItems
									   }"
							action="#{pages$inheritedAssetsTab.editAsset}">
							<h:graphicImage title="#{msg['commons.edit']}"
								style="cursor:hand;" url="../resources/images/edit-icon.gif" />&nbsp;
						</t:commandLink>
						<t:commandLink
							rendered="#{!dataItem.isSelected && pages$inheritanceFile.showInheritedAssetTabGridEditItems}"
							onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToDeleteAsset']}')) return false;"
							style="margin-left:5px; margin-right:5px;"
							action="#{pages$inheritedAssetsTab.deleteAsset}">
							<h:graphicImage title="#{msg['commons.delete']}"
								style="cursor:hand;" url="../resources/images/delete.gif" />&nbsp;
						</t:commandLink>
						<t:commandLink
						rendered="#{!dataItem.isConfirmBool && pages$inheritanceFile.showInheritedAssetTabGridConfirmActionItems}"
						onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToConfirmAsset']}')) return false;"
						action="#{pages$inheritedAssetsTab.confirmAsset}">
						<h:graphicImage
							title="#{msg['mems.inheritanceFile.toolTip.confirmAsset']}"
							style="cursor:hand;" url="../resources/images/conduct.gif" />&nbsp;
						</t:commandLink>
						

					</pims:security>
					<pims:security screen="Pims.MinorMgmt.InheritanceFile.SaveAfterFinalize"
						action="view">
						<t:commandLink
							rendered="#{!dataItem.isSelected && dataItem.status!=212002 && dataItem.isFinal ==1 &&  pages$inheritanceFile.showInheritedAssetTabGridEditItemsAfterFinalize}"
							action="#{pages$inheritedAssetsTab.editAsset}">
							<h:graphicImage title="#{msg['commons.edit']}"
								style="cursor:hand;" url="../resources/images/edit-icon.gif" />&nbsp;
						</t:commandLink>
						<t:commandLink
						rendered="#{!dataItem.isConfirmBool && pages$inheritanceFile.showInheritedAssetTabGridConfirmActionItemsAfterFinalize}"
						onclick="if (!confirm('#{msg['confirmMsg.areuSureTouWantToConfirmAsset']}')) return false;"
						action="#{pages$inheritedAssetsTab.confirmAsset}">
						<h:graphicImage
							title="#{msg['mems.inheritanceFile.toolTip.confirmAsset']}"
							style="cursor:hand;" url="../resources/images/conduct.gif" />&nbsp;
						</t:commandLink>
						<t:commandLink
							rendered="#{!dataItem.isSelected && dataItem.isConfirmBool && dataItem.status!=212002 && pages$inheritanceFile.showInheritedAssetTabGridEditItemsAfterFinalize}"
							onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;"
							style="margin-left:5px; margin-right:5px;"
							action="#{pages$inheritedAssetsTab.disableAsset}">
							<h:graphicImage title="#{msg['commons.clickDisable']}"
								style="cursor:hand;" url="../resources/images/app_icons/file_block.gif" />&nbsp;
						</t:commandLink>
						<t:commandLink
							rendered="#{!dataItem.isSelected && dataItem.isConfirmBool && dataItem.status==212002 && pages$inheritanceFile.showInheritedAssetTabGridEditItemsAfterFinalize}"
							onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;"
							style="margin-left:5px; margin-right:5px;"
							action="#{pages$inheritedAssetsTab.enableAsset}">
							<h:graphicImage title="#{msg['commons.clickEnable']}"
								style="cursor:hand;" url="../resources/images/app_icons/file_allow.gif" />&nbsp;
						</t:commandLink>
						
					</pims:security>
					
					
				</t:column>
			</t:dataTable>
		</t:div>
	</t:div>
</t:div>