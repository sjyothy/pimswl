<%-- 
  - Author: Anil Verani
  - Date: 23/12/2010
  - Description: Used for Managing Takharuj Details 
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	function openSearchPersonPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	   window.open('SearchPerson.jsf?viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');	    
	}
	
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		document.getElementById('frm:selectedToPersonId').value = personId; 
		document.getElementById('frm:lnkReceiveToBeneficiary').onclick();
	}
	
	function closeAndPassTakharujInheritedAssetView()
	{
		window.opener.receiveTakharujInheritedAssetView();
		window.close();
	}	
	
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				
				<tr>
					<td width="100%" valign="top" class="divBackgroundBody">
						<table width="100%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['takharuj.details.heading']}" styleClass="HEADER_FONT" />
								</td>								
							</tr>
						</table>
						<table width="100%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">								
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" >
										<h:form id="frm" style="width:97%" enctype="multipart/form-data">
											<table border="0" class="layoutTable">
												<tr>
													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$takharujDetailManage.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$takharujDetailManage.successMessages}" />													    
													</td>
												</tr>
											</table>

												<t:panelGrid cellpadding="1px" width="100%"
													cellspacing="5px" columns="4"
													rendered="#{!  pages$takharujDetailManage.popModeView}">
													<h:panelGroup>
														<h:outputLabel styleClass="mandatory" value="*" />
														<h:outputLabel styleClass="LABEL"
															value="#{msg['takharuj.details.fromBeneficiary']}:" />
													</h:panelGroup>
													<h:selectOneMenu
														value="#{pages$takharujDetailManage.selectedFromInheritedAssetSharesId}">
														<f:selectItem itemValue=""
															itemLabel="#{msg['commons.select']}" />
														<f:selectItems
															value="#{pages$takharujDetailManage.beneficiarySelectItems}" />
														<a4j:support
															action="#{pages$takharujDetailManage.onFromBeneficiaryChange}"
															event="onchange"
															reRender="selectedFromShare,hdnSelectedFromShare,remainingShare" />
													</h:selectOneMenu>

													<h:panelGroup>
														<h:outputLabel styleClass="mandatory" value="*" />
														<h:outputLabel styleClass="LABEL"
															value="#{msg['takharuj.details.fromShare']}:" />
													</h:panelGroup>
													<h:panelGroup>
														<h:inputText id="selectedFromShare" readonly="true"
															styleClass="READONLY"
															value="#{pages$takharujDetailManage.selectedFromShare}" />
														<h:inputHidden id="hdnSelectedFromShare"
															value="#{pages$takharujDetailManage.selectedFromShare}" />
													</h:panelGroup>

													<h:panelGroup>
														<h:outputLabel styleClass="mandatory" value="*" />
														<h:outputLabel styleClass="LABEL"
															value="#{msg['takharuj.details.toBeneficiary']}:" />
													</h:panelGroup>
													<h:panelGroup>
														<h:inputHidden id="selectedToPersonId"
															value="#{pages$takharujDetailManage.toPersonView.personId}" />
														<a4j:commandLink id="lnkReceiveToBeneficiary"
															action="#{pages$takharujDetailManage.onToBeneficiaryChange}"
															reRender="selectedToPerson,selectedToShare,hdnSelectedToShare,selectedToBeneficiary" />
														<h:inputText id="selectedToPerson" readonly="true"
															styleClass="READONLY"
															value="#{pages$takharujDetailManage.toPersonView.personFullName}" />
														<h:graphicImage title="#{msg['commons.search']}"
															style="MARGIN: 1px 1px -5px;cursor:hand"
															onclick="openSearchPersonPopup();"
															url="../resources/images/app_icons/Search-tenant.png">
														</h:graphicImage>
													</h:panelGroup>


													<h:panelGroup>
														<h:outputLabel styleClass="mandatory" value="*" />
														<h:outputLabel styleClass="LABEL"
															value="#{msg['takharuj.details.toShare']}:" />
													</h:panelGroup>
													<h:panelGroup>
														<h:inputText id="selectedToShare" readonly="true"
															styleClass="READONLY"
															value="#{pages$takharujDetailManage.selectedToShare}" />
														<h:inputHidden id="hdnSelectedToShare"
															value="#{pages$takharujDetailManage.selectedToShare}" />
													</h:panelGroup>

													<h:panelGroup>
														<h:outputLabel styleClass="mandatory" value="*" />
														<h:outputLabel styleClass="LABEL"
															value="#{msg['takharuj.details.takharujShare']}:" />
													</h:panelGroup>
													<h:inputText onkeyup="removeNonInteger(this)"
														onkeypress="removeNonInteger(this)"
														onkeydown="removeNonInteger(this)"
														onblur="removeNonInteger(this)"
														onchange="removeNonInteger(this)"
														value="#{pages$takharujDetailManage.takharujShare}" />

													<h:panelGroup rendered="false">
														<h:outputLabel styleClass="mandatory" value="*" />
														<h:outputLabel styleClass="LABEL"
															value="#{msg['takharuj.details.toBeneficiary']}:" />
													</h:panelGroup>
													<h:selectOneMenu rendered="false"
														id="selectedToBeneficiary"
														value="#{pages$takharujDetailManage.selectedToInheritedAssetSharesId}">
														<f:selectItem itemValue=""
															itemLabel="#{msg['commons.select']}" />
														<f:selectItems
															value="#{pages$takharujDetailManage.beneficiarySelectItems}" />
													</h:selectOneMenu>

													<h:outputLabel styleClass="LABEL"
														value="#{msg['takharuj.label.remainingShare']}:" />
													<h:inputText id="remainingShare" readonly="true"
														styleClass="READONLY"
														value="#{pages$takharujDetailManage.remainingShare}" />
												</t:panelGrid>

												<t:panelGrid styleClass="BUTTON_TD" cellpadding="1px" cellspacing="5px" width="100%" rendered ="#{!  pages$takharujDetailManage.popModeView}">
												<h:panelGroup>
													<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.done']}"
																	action="#{pages$takharujDetailManage.onTakharujDetailDone}" />
												
													<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.Add']}"
																	action="#{pages$takharujDetailManage.onTakharujDetailAdd}" />
												</h:panelGroup>
											</t:panelGrid>
											
											<t:div styleClass="contentDiv" style="width:99%">																
												<t:dataTable id="takharujDetailViewDataTable"  
															rows="#{pages$takharujDetailManage.paginatorRows}"
															value="#{pages$takharujDetailManage.takharujDetailViewList}"
															binding="#{pages$takharujDetailManage.takharujDetailViewDataTable}"																	
															preserveDataModel="false" preserveSort="false" var="dataItem"
															rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">
																												
													<t:column width="20%" sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['takharuj.details.fromBeneficiary']}" />
														</f:facet>
														<t:outputText rendered="#{dataItem.isDeleted == 0}" value="#{dataItem.fromInheritedAssetSharesView.inheritanceBeneficiaryView.beneficiary.personFullName}" />
													</t:column>
																																													
													<t:column width="15%" sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['takharuj.details.fromShare']}" />
														</f:facet>
														<t:outputText rendered="#{dataItem.isDeleted == 0}" value="#{dataItem.fromSharePercentage}" />																		
													</t:column>
													
													<t:column width="15%" sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['takharuj.details.takharujShare']}" />
														</f:facet>
														<t:outputText rendered="#{dataItem.isDeleted == 0}" value="#{dataItem.takharujShare}" />																		
													</t:column>
																												
													<t:column width="20%" sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['takharuj.details.toBeneficiary']}" />
														</f:facet>
														<t:outputText rendered="#{dataItem.isDeleted == 0}" value="#{dataItem.toPersonView.personFullName}" />																		
													</t:column>
													
													<t:column width="15%" sortable="true">
														<f:facet name="header">
															<t:outputText value="#{msg['takharuj.details.toShare']}" />
														</f:facet>
														<t:outputText rendered="#{dataItem.isDeleted == 0}" value="#{dataItem.toSharePercentage}" />
													</t:column>
													
													<t:column width="15%" sortable="false">
														<f:facet name="header">
															<t:outputText value="#{msg['commons.action']}" />
														</f:facet>
														
														<a4j:commandLink onclick="if (!confirm('#{msg['takharujDetailView.confirm.delete']}')) return" action="#{pages$takharujDetailManage.onTakharujDetailDelete}" rendered="#{dataItem.isDeleted == 0 && !pages$takharujDetailManage.popModeView}" reRender="takharujDetailViewDataTable,takharujDetailViewGridInfo" >
															<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />
														</a4j:commandLink>
													</t:column>
												</t:dataTable>										
											</t:div>
											
											<t:div id="takharujDetailViewGridInfo" styleClass="contentDivFooter" style="width:100%">
												<t:panelGrid columns="2" cellpadding="0" cellspacing="0" width="100%" columnClasses="RECORD_NUM_TD,BUTTON_TD" >
													<t:div styleClass="RECORD_NUM_BG">
														<t:panelGrid columns="3" columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD" cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
															<h:outputText value="#{msg['commons.recordsFound']}"/>
															<h:outputText value=" : "/>
															<h:outputText value="#{pages$takharujDetailManage.takharujDetailViewListRecordSize}"/>																						
														</t:panelGrid>
													</t:div>
																															  
													<CENTER>
														<t:dataScroller for="takharujDetailViewDataTable" paginator="true"
																		fastStep="1"  
																		paginatorMaxPages="#{pages$takharujDetailManage.paginatorMaxPages}" 
																		immediate="false"
																		paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true" 
																		pageIndexVar="pageNumber"
																		styleClass="SCH_SCROLLER"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false" 
																		paginatorTableStyle="grid_paginator" layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;">
																			
																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
																		</f:facet>
																				
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
																		</f:facet>
																				
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
																		</f:facet>
																				
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
																		</f:facet>
																				
																		<t:div styleClass="PAGE_NUM_BG">
																			<t:panelGrid styleClass="PAGE_NUM_BG_TABLE"  columns="2" cellpadding="0" cellspacing="0">
																				<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																				<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"  value="#{requestScope.pageNumber}"/>
																			</t:panelGrid>																							
																		</t:div>
														</t:dataScroller>
													</CENTER>																		      
												</t:panelGrid>
											</t:div>
									    </h:form>
									</div>									
								</td>								
							</tr>
						</table>
					</td>
				</tr>				
			</table>
           </div>
		</body>
	</html>
</f:view>