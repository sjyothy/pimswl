<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
<script language="javascript" type="text/javascript">
function closePopup()
	{
		window.close();
	}
</script>
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />



			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

		</head>

		<script language="javascript" type="text/javascript">
   </script>


		<!-- Header -->
	<t:div styleClass="TAB_DETAIL_SECTION" style="width:100%;overflow-y: scroll;" >
	
		<body class="BODY_STYLE">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">

					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel 
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="99%" valign="top" nowrap="nowrap">
									
											
										<h:form id="formPaymentSchedule" style="width:92%"
											enctype="multipart/form-data">
											<div class="SCROLLABLE_SECTION">
											<table border="0" class="layoutTable">

												<tr>
												<td>
																&nbsp;&nbsp;
															</td>

													<td colspan="3">

														<h:outputText escape="false" styleClass="ERROR_FONT"
															value="#{pages$splitPaymentSchedule.errorMessages}" />

													</td>
												</tr>
											</table>

											<div class="MARGIN" style="width: 100%">
												<table cellpadding="0" cellspacing="0" width="90.3%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG 
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width: 90%">
													<h:outputLabel
														value="#{msg['paymentScheduleSplit.oldpaymentDetails']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="1px" class="DETAIL_SECTION_INNER">
														<tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td >
																<h:outputLabel  value="#{msg['bouncedChequesList.paymentNumberCol']}"></h:outputLabel>
															</td>
															<td>
																<h:inputText styleClass="READONLY INPUT_TEXT" readonly="true" value="#{pages$splitPaymentSchedule.paymentScheduleView.paymentNumber}"></h:inputText>
															</td>
															<td >
																<h:outputLabel value="#{msg['cancelContract.payment.paymenttype']}"></h:outputLabel>
															</td>
															<td>
																<h:inputText  styleClass="READONLY INPUT_TEXT"  readonly="true" value="#{pages$splitPaymentSchedule.paymentScheduleView.typeEn}"></h:inputText>
															</td>
														</tr>
                                                        <tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td >
																<h:outputLabel value="#{msg['cancelContract.settlement.description']}"></h:outputLabel>
															</td>
															<td>
																<h:inputTextarea readonly="true" styleClass="READONLY INPUT_TEXT" value="#{pages$splitPaymentSchedule.paymentScheduleView.description}"/>
															</td>
															<td >
																<h:outputLabel  value="#{msg['paymentSchedule.paymentDueOn']}"></h:outputLabel>
															</td>
															<td>
															
															<rich:calendar id="dateTimePaymentDueOnOld" disabled="true" styleClass="READONLY INPUT_TEXT"
																	value="#{pages$splitPaymentSchedule.paymentScheduleView.paymentDueOn}"
																	popup="true" showApplyButton="false"
																	datePattern="#{pages$splitPaymentSchedule.shortDateFormat}"
																	enableManualInput="false" cellWidth="24px"
																	locale="#{pages$splitPaymentSchedule.currentLocale}"																	
																	 />
															</td>
															
															
														</tr>


														<tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td >
																<h:outputLabel value="#{msg['cancelContract.payment.paymentmethod']}"></h:outputLabel>
															</td>
															<td>
															<h:inputText styleClass="READONLY INPUT_TEXT" readonly="true" value="#{pages$splitPaymentSchedule.paymentScheduleView.paymentModeEn}"></h:inputText>
															</td>
															<td >
																<h:outputLabel value="#{msg['chequeList.amount']}"></h:outputLabel>
															</td>
															<td>
																<h:inputText readonly="true" styleClass="READONLY INPUT_TEXT A_RIGHT_NUM" value="#{pages$splitPaymentSchedule.paymentScheduleView.amount}"></h:inputText>
															</td>
														</tr>
														
													
													</table>

												</div>
												&nbsp;
												
												
												<table cellpadding="0" cellspacing="0" width="90.3%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG 
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div tyle="width: 90%">
												
											<div class="DETAIL_SECTION" style="width: 90%">
													<h:outputLabel
														value="#{msg['paymentScheduleSplit.newpaymentDetails']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														
														<table cellpadding="1px" cellspacing="1px" class="DETAIL_SECTION_INNER">
														
															<tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td >
																<h:outputLabel value="#{msg['cancelContract.payment.paymentmethod']}"></h:outputLabel>
															</td>
															<td>
															
															<h:selectOneMenu id="selectpaymentMode"														
																	binding ="#{pages$splitPaymentSchedule.htmlPaymentMethod}" >
															<f:selectItems 
																		value="#{pages$ApplicationBean.paymentMethodList}" />
															</h:selectOneMenu>																
																
															
															</td>
															<td >
																<h:outputLabel value="#{msg['chequeList.amount']}"></h:outputLabel>
															</td>
															<td>
																<h:inputText styleClass="INPUT_TEXT A_RIGHT_NUM" value="#{pages$splitPaymentSchedule.splitAmount}"></h:inputText>
															</td>
														</tr>
														
														<tr>
															<td>
																&nbsp;&nbsp;
															</td>
															<td >
																<h:outputLabel value="#{msg['paymentSchedule.paymentDueOn']}"></h:outputLabel>
															</td>
															<td>
															<rich:calendar id="dateTimePaymentDueOn"
																	binding="#{pages$splitPaymentSchedule.htmlCalendar}"
																	popup="true" showApplyButton="false"
																	datePattern="#{pages$splitPaymentSchedule.shortDateFormat}"
																	enableManualInput="false" cellWidth="24px"
																	locale="#{pages$splitPaymentSchedule.currentLocale}"																	
																	styleClass="READONLY INPUT_TEXT" />
															</td>
															<td >
																
															</td>
															<td>
																
															</td>
														</tr>
														</table>
														</div>
														<table width="90%">
													<tr>

														<td colspan="6" class="BUTTON_TD">
															<h:commandButton styleClass="BUTTON" value="#{msg['commons.Add']}"
															action="#{pages$splitPaymentSchedule.btnAdd_Click}"															
															/>
															&nbsp;
															<h:commandButton styleClass="BUTTON" value="#{msg['commons.done']}"
															action="#{pages$splitPaymentSchedule.btnDone_Click}"															
															/>
															&nbsp;
															<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancelAppFee']}"
															onclick="if (!confirm('#{msg['replaceCheque.warngMsg.areYouSureToCancel']}')) return else closePopup();"
															/>

														</td>
													</tr>
												</table>
												
												
												<t:div id="divtt" styleClass="contentDiv"
																style="width:90%">
																<h:dataTable id="ttbb" width="100%"
																	value="#{pages$splitPaymentSchedule.paymentScheduleViewList}"	
																	binding="#{pages$splitPaymentSchedule.dataTablePaymentSchedule}"
																	var="paymentScheduleDataItem" rowClasses="row1,row2"
																	>
																	
																	<t:column id="colModeEnt" width="30%"
																		rendered="#{pages$ReplaceCheque.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac7"
																				value="#{msg['paymentSchedule.paymentMode']}" />
																		</f:facet>
																		<t:outputText id="ac6" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentModeEn}" />
																	</t:column>
																	<t:column id="colModeArt" width="30%"
																		rendered="#{pages$ReplaceCheque.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac5"
																				value="#{msg['paymentSchedule.paymentMode']}" />
																		</f:facet>
																		<t:outputText id="ac4" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentModeAr}" />
																	</t:column>


																	<t:column id="colAmountt" width="30%">
																		<f:facet name="header">
																			<t:outputText id="ac1"
																				value="#{msg['paymentSchedule.amount']}" />
																		</f:facet>
																		<t:outputText id="ac2" styleClass="A_RIGHT_NUM"
																			value="#{paymentScheduleDataItem.amount}" />
																	</t:column>
																	
																		<t:column id="colDueOnt" width="30%">
																		<f:facet name="header">
																			<t:outputText id="ac13"
																				value="#{msg['paymentSchedule.paymentDueOn']}" />
																		</f:facet>
																		<t:outputText id="ac12" styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentDueOn}">
																			<f:convertDateTime
																				pattern="#{pages$splitPaymentSchedule.shortDateFormat}" />
																		</t:outputText>
																	</t:column>
																	

																	<t:column id="col8" width="10%">
																		<f:facet name="header">
																		
																		</f:facet>

																		<h:commandLink
																			action="#{pages$splitPaymentSchedule.deleteRow}"
																			>
																			<h:graphicImage id="delete"
																				title="#{msg['commons.delete']}"
																				url="../resources/images/delete.gif"
																				 />
																		</h:commandLink>
																		

																	</t:column>




																</h:dataTable>
															</t:div>
											</div>
														
												
												
												
												
														
															
											</div>
											</div>
						                </h:form>
						
						
						
					          </td>
				         </tr>
			          </table>

			</td>
			</tr>

			</table>

		</body>
		</t:div>
	</html>
</f:view>

