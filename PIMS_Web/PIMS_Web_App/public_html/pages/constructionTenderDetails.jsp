
<script type="text/javascript">
	function showSearchProjectPopUp(pageModeKey,selectOnePopKey)
    {
	     //var  screen_width = screen.width;
	     //var screen_height = screen.height;	     
	     //window.open('projectSearch.jsf?'+pageModeKey+'='+selectOnePopKey+'&context=ConstructionTender','_blank','width='+(screen_width-300)+',height='+(screen_height-350)+',left=0,top=40,scrollbars=no,status=yes');
				var screen_width = 1024;
				var screen_height = 450;
				window.open('projectSearch.jsf?'+pageModeKey+'='+selectOnePopKey+'&context=ConstructionTender','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	     
    }
    
    function populateProject(projectId, projectName, projectNumber, projectEstimatedCost)
    {    	
    	document.getElementById("frm:hdnProjectId").value = projectId;
    	document.getElementById("frm:hdnProjectNumber").value = projectNumber;    			    
    	document.getElementById("frm:hdnProjectName").value = projectName;
    	document.getElementById("frm:hdnProjectEstimatedCost").value = projectEstimatedCost;
    	document.getElementById("frm:txtProjectNumber").value = projectNumber;    			    
    	document.getElementById("frm:txtProjectName").value = projectName;
    	document.getElementById("frm:txtProjectEstimatedCost").value = projectEstimatedCost;
    }
</script>

	
	<h:inputHidden id="hdnProjectId"
				   value="#{pages$constructionTenderDetails.constructionTenderDetailsView.projectId}">
	</h:inputHidden>	
	<h:inputHidden id="hdnProjectNumber"
				   value="#{pages$constructionTenderDetails.constructionTenderDetailsView.projectNumber}">
	</h:inputHidden>
	<h:inputHidden id="hdnProjectName"
				   value="#{pages$constructionTenderDetails.constructionTenderDetailsView.projectName}">
	</h:inputHidden>	
	<h:inputHidden id="hdnProjectEstimatedCost"
				   value="#{pages$constructionTenderDetails.constructionTenderDetailsView.projectEstimatedCost}">
	</h:inputHidden>
	

	<t:panelGrid width="100%" columns="4">	
		
		
		<h:outputLabel styleClass="LABEL" 
						   value="#{msg['tender.number']}:">
		</h:outputLabel>						
		<h:inputText id="txtTenderNumber" styleClass="READONLY"			 
					 binding="#{pages$constructionTenderDetails.txtTenderNumber}"
					 value="#{pages$constructionTenderDetails.constructionTenderDetailsView.tenderNumber}">
		</h:inputText>
		
		
		
		<h:outputLabel styleClass="LABEL" 
					   value="#{msg['constructionTender.search.tender.status']}:">
		</h:outputLabel>			
		<h:selectOneMenu id="ddnTenderStatus" styleClass="READONLY" readonly="true" 
						 binding="#{pages$constructionTenderDetails.ddnTenderStatus}"
						 value="#{pages$constructionTenderDetails.constructionTenderDetailsView.tenderStatusId}">
			<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
			<f:selectItems value="#{pages$ApplicationBean.tenderStatusList}" />
		</h:selectOneMenu>
		
		
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
			<h:outputLabel styleClass="LABEL" value="#{msg['tender.type']}:" />
		</h:panelGroup>	
		<h:selectOneMenu id="ddnTenderType" styleClass="READONLY" readonly="true" 
						 binding="#{pages$constructionTenderDetails.ddnTenderType}"						  
						 value="#{pages$constructionTenderDetails.constructionTenderDetailsView.tenderTypeId}">
			<f:selectItem itemValue="" itemLabel="#{msg['commons.select']}" />
			<f:selectItems value="#{pages$ApplicationBean.tenderTypeNewList}" />
		</h:selectOneMenu>
		
		
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
			<h:outputLabel styleClass="LABEL" value="#{msg['serviceContract.serviceType']}:" />
		</h:panelGroup>
		<h:selectOneMenu id="ddnConstructionServiceType" 
						 binding="#{pages$constructionTenderDetails.ddnConstructionServiceType}"
						 value="#{pages$constructionTenderDetails.constructionTenderDetailsView.constructionServiceTypeId}">
			<f:selectItem itemValue="" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
			<f:selectItems value="#{pages$ApplicationBean.constructionServiceTypeList}" />
		</h:selectOneMenu>
		
			
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
			<h:outputLabel styleClass="LABEL" value="#{msg['constructionTender.search.project.number']}:" />
		</h:panelGroup>		
		<h:panelGroup>
			<h:inputText id="txtProjectNumber" styleClass="READONLY"					 
					 binding="#{pages$constructionTenderDetails.txtProjectNumber}" 
					 value="#{pages$constructionTenderDetails.constructionTenderDetailsView.projectNumber}">
			</h:inputText>
				
			<h:graphicImage url="../resources/images/app_icons/Search-tenant.png"
							binding="#{pages$constructionTenderDetails.imgProjectSearch}" 
							onclick="javaScript:showSearchProjectPopUp('#{pages$constructionTenderDetails.projectKey_PageMode}','#{pages$constructionTenderDetails.projectKey_PageModeSelectOnPopUp}');"  
							alt="#{msg[commons.searchProject]}">
			</h:graphicImage>
		</h:panelGroup>
		
				
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
			<h:outputLabel styleClass="LABEL" value="#{msg['constructionTender.search.project.name']}:" />
		</h:panelGroup>						
		<h:inputText id="txtProjectName" styleClass="READONLY"					 
					 binding="#{pages$constructionTenderDetails.txtProjectName}"
					 value="#{pages$constructionTenderDetails.constructionTenderDetailsView.projectName}">
		</h:inputText>
		
		
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
			<h:outputLabel styleClass="LABEL" 
						   value="#{msg['maintenanceRequest.estimatedCost']}:">
			</h:outputLabel>				
		</h:panelGroup>
		<h:inputText id="txtProjectEstimatedCost" styleClass="A_RIGHT_NUM READONLY"					 
					 binding="#{pages$constructionTenderDetails.txtProjectEstimatedCost}"
					 value="#{pages$constructionTenderDetails.constructionTenderDetailsView.projectEstimatedCost}">
		<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="#{pages$constructionTenderDetails.numberFormat}"/>			 
		</h:inputText>
		
			<h:outputLabel styleClass="LABEL" 
						   value="#{msg['constructionTender.search.docSellingPrice']}:">
			</h:outputLabel>				
		<h:inputText id="txtTenderDocSellingPrice"	styleClass="A_RIGHT_NUM"				 
					 binding="#{pages$constructionTenderDetails.txtTenderDocSellingPrice}"
					 value="#{pages$constructionTenderDetails.constructionTenderDetailsView.tenderDocumentSellingPrice}">
		</h:inputText>
		
		
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
			<h:outputLabel styleClass="LABEL" 
						   value="#{msg['constructionTender.search.tender.issueDate']}:">
			</h:outputLabel>
		</h:panelGroup>
		<rich:calendar id="calTenderIssueDate"	
					   binding="#{pages$constructionTenderDetails.calTenderIssueDate}"				    
					   value="#{pages$constructionTenderDetails.constructionTenderDetailsView.tenderIssueDate}"
					   datePattern="#{pages$constructionTenderDetails.dateFormat}"
					   locale="#{pages$constructionTenderDetails.locale}" 
					   popup="true" inputStyle="height:14px;"
					   showApplyButton="false" 
					   enableManualInput="false">
		</rich:calendar>
		
		
		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>
			<h:outputLabel styleClass="LABEL" 
						   value="#{msg['constructionTender.search.tender.endDate']}:">
			</h:outputLabel>
		</h:panelGroup>
		<rich:calendar id="calTenderEndDate"	
					   binding="#{pages$constructionTenderDetails.calTenderEndDate}"				    
					   value="#{pages$constructionTenderDetails.constructionTenderDetailsView.tenderEndDate}"
					   datePattern="#{pages$constructionTenderDetails.dateFormat}"
					   locale="#{pages$constructionTenderDetails.locale}" 
					   popup="true" inputStyle="height:14px;"
					   showApplyButton="false" 
					   enableManualInput="false">
		</rich:calendar>
		<h:outputLabel
				value="#{msg['tenderManagement.tenderDescription']}:"></h:outputLabel>
			<h:inputText id="tenderDescription" 
				
				value="#{pages$constructionTenderDetails.constructionTenderDetailsView.tenderDescription}"/>
		
	</t:panelGrid>
	