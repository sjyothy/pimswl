
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
<!--
var selectedFacilities;

function SelectFacility(field,facilityId){

	if (field.checked){
	
		if (selectedFacilities != null && selectedFacilities.length > 0){
			
			selectedFacilities = selectedFacilities+","+facilityId;
			} else
			selectedFacilities = facilityId;
	
	}
	else
	{
	
	   var delimiters=",";
		   var allSelectedFacilites = selectedFacilities.split(delimiters);
		   selectedFacilities = "";
		   
		   for(i=0;i<allSelectedFacilites.length;i++)
		   {
		   if(allSelectedFacilites[i]!= facilityId)
		    {
	       	   if(selectedFacilities.length>0 )
		       {
		             selectedFacilities = selectedFacilities+","+allSelectedFacilites[i];
	
               }
		       else
		       {
		             selectedFacilities = allSelectedFacilites[i];
		       }
		      }
		     }
	
	}
	
	
	document.getElementById("propertyFacilityForm:selectedFacilitiesHidden").value = selectedFacilities;
}
//-->
</script>
	
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
		<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="Cache-Control"
			content="no-cache,must-revalidate,no-store">
		<meta http-equiv="expires" content="0">

		<link rel="stylesheet" type="text/css"
			href="../resources/css/simple_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../resources/css/amaf_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../resources/css/table.css" />
		<title>PIMS</title>
	</head>
	
	
	<f:view>
	<%
				UIViewRoot view = (UIViewRoot) session.getAttribute("view");
					if (view.getLocale().toString().equals("en")) {
			%>
			<body dir="ltr">
				<f:loadBundle
					basename="com.avanza.pims.web.messageresource.Messages" var="msg" />
				<%
					} else {
				%>
			
			<body dir="rtl">
				<f:loadBundle
					basename="com.avanza.pims.web.messageresource.Messages_ar"
					var="msg" />
				<%
					}
				%>

		
			
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr>
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td
										background="../../resources/images/Grey panel/Grey-Panel-left-1.jpg"
										height="38" style="background-repeat: no-repeat;" width="100%">
										<font
											style="font-family: Trebuchet MS; font-weight: regular; font-size: 19px; margin-left: 15px; top: 30px;">
										<h:outputLabel
												value="#{msg['propertyFacility.header']}"></h:outputLabel></font>
										<!--<IMG src="../resources/images/Grey panel/Grey-Panel-left-1.jpg"/>-->
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<td width="500px" height="100%" valign="top" nowrap="nowrap">
										<h:form id="propertyFacilityForm">
											<table style="width: 100%">
												<tr>
													<td width="25%">
														<h:outputText value="#{msg['propertyFacility.facilityName']}"></h:outputText>:
													</td>
													<td width="25%">
														<h:inputText value="#{pages$PropertyFacilityBackingBean.facilitySearch}"></h:inputText>
													</td>
													<td width="25%">
              <h:commandButton styleClass="BUTTON" value="#{msg['propertyFacility.searchFacility']}" action="#{pages$PropertyFacilityBackingBean.SearchFacilities}" style="width: 142px; height: 20px"></h:commandButton>
              <h:commandButton value="Cancel"/>
             </td>
													<td width="25%">&nbsp;<br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
												</tr>
												
												<tr>
													<td colspan="4">
                                                 <t:dataTable id="test2"
													value="#{pages$PropertyFacilityBackingBean.facilities}"
													binding="#{pages$PropertyFacilityBackingBean.dataTable}"
													rows="7"
													preserveDataModel="true" preserveSort="true" var="dataItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">

													<t:column id="col1" width="20">

														<f:facet name="header">

															<t:outputText value="#{msg['propertyFacility.facilityId']}" />

														</f:facet>
														<t:outputText value="#{dataItem.facilityId}" />
													</t:column>


												<t:column id="col2" sortable="true" width="20%" >
													<f:facet name="header">


														<t:outputText value="#{msg['propertyFacility.facilityName']}" />

													</f:facet>
													<t:outputText value="#{dataItem.facilityName}" />
												</t:column>


												<t:column id="col3" sortable="true">


													<f:facet name="header">

														<t:outputText value="#{msg['propertyFacility.description']}" />

													</f:facet>
													<t:outputText value="#{dataItem.description}" />
												</t:column>



													<t:column id="col9" width="15%">

														<f:facet name="header" >
															
																<t:outputText value="#{msg['propertyFacility.select']}" />
															
														</f:facet>
																<h:selectBooleanCheckbox onclick="javascript:SelectFacility(this,'#{dataItem. facilityId}')" />
															</t:column>
												</t:dataTable>
												<p align="right">
														<h:commandButton styleClass="BUTTON" value="Add" action="#{pages$PropertyFacilityBackingBean.AddFacilities}" style="width: 106px"></h:commandButton>
              									</p>
													</td>
												</tr>
											</table>
											<h:inputHidden id="selectedFacilitiesHidden" binding="#{pages$PropertyFacilityBackingBean.allSelectedFacilities}"/>
										</h:form>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</body>
			
		</f:view>
</html>
