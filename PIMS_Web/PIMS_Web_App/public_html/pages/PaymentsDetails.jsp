<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">




<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />



			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

		</head>

		<script language="javascript" type="text/javascript">
    </script>


		<!-- Header -->

		<body class="BODY_STYLE">

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['chequeList.chequeList']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="85%" valign="top" nowrap="nowrap">
									<h:form id="formPaymentSchedule" style="width:100%"
										enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION" style="width: 100%">
											<table border="0" class="layoutTable">

												<tr>

													<td colspan="1">

													</td>
												</tr>
											</table>

											<div class="MARGIN" style="width: 100%;">
												<table cellpadding="0" cellspacing="0" width="95%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
													<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width: 95%">
													<h:outputLabel
														value="#{msg['paymentSchedule.paymentDetails']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>

													<t:panelGrid styleClass="DETAIL_SECTION_INNER" width="100%"
														columns="4">
														<h:outputLabel
															value="#{msg['bouncedChequesList.paymentNumberCol']} :">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															value="#{pages$PaymentsDetails.paymentScheduleView.paymentNumber}">
														</h:outputText>

														<h:outputLabel
															value="#{msg['bouncedChequesList.contractNumberCol']} :">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															value="#{pages$PaymentsDetails.paymentScheduleView.contractNumber}">
														</h:outputText>
														<h:outputLabel
															value="#{msg['cancelContract.payment.paymenttype']} :">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															value="#{!pages$PaymentsDetails.isEnglishLocale?pages$PaymentsDetails.paymentScheduleView.typeAr:pages$PaymentsDetails.paymentScheduleView.typeAr}">
														</h:outputText>
														<h:outputLabel
															value="#{msg['paymentSchedule.paymentMode']} :">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															value="#{!pages$PaymentsDetails.isEnglishLocale?pages$PaymentsDetails.paymentScheduleView.paymentModeAr:pages$PaymentsDetails.paymentScheduleView.paymentModeEn}">
														</h:outputText>

														<h:outputLabel
															value="#{msg['paymentSchedule.paymentDueOn']} :">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															value="#{pages$PaymentsDetails.paymentScheduleView.paymentDueOn}">
															<f:convertDateTime
																pattern="#{pages$PaymentsDetails.dateFormat}"
																timeZone="#{pages$PaymentsDetails.timeZone}" />
														</h:outputText>
														<h:outputLabel
															value="#{msg['purchaseTenderDocument.paymentDate']} :">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															value="#{pages$PaymentsDetails.paymentScheduleView.paymentDate}">
															<f:convertDateTime
																pattern="#{pages$PaymentsDetails.dateFormat}"
																timeZone="#{pages$PaymentsDetails.timeZone}" />
														</h:outputText>
														<h:outputLabel
															value="#{msg['chequeList.paymentAmountCol']} :">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															value="#{pages$PaymentsDetails.paymentScheduleView.amount}">
														</h:outputText>
														<h:outputLabel
															value="#{msg['chequeList.message.paymentStatus']} :">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															value="#{!pages$PaymentsDetails.isEnglishLocale?pages$PaymentsDetails.paymentScheduleView.statusAr:pages$PaymentsDetails.paymentScheduleView.statusEn}">
														</h:outputText>
														<h:outputLabel value="#{msg['commons.description']} :">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															value="#{pages$PaymentsDetails.paymentScheduleView.description}">
														</h:outputText>
														<h:outputLabel value="#{msg['paymentSchedule.systemPaymentChangeReason']} :">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															value="#{pages$PaymentsDetails.paymentScheduleView.comments}">
														</h:outputText>
														<h:outputLabel
															value="#{msg['common.msg.paymentCategory']} :">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															value="#{pages$PaymentsDetails.paymentCategory}">
														</h:outputText>
														<h:outputLabel value="#{msg['payment.accountNo']} :"
															rendered="#{pages$PaymentsDetails.paymentCollected}">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															value="#{pages$PaymentsDetails.paymentReceiptDetailsView.accountNo}"
															rendered="#{pages$PaymentsDetails.paymentCollected}">
														</h:outputText>
														<h:outputLabel
															rendered="#{pages$PaymentsDetails.paymentCollected}"
															value="#{msg['payment.receiptAmount']} :">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															rendered="#{pages$PaymentsDetails.paymentCollected}"
															value="#{pages$PaymentsDetails.paymentReceiptDetailsView.amount}">
														</h:outputText>
														<h:outputLabel
															rendered="#{pages$PaymentsDetails.paymentCollected}"
															value="#{msg['settlement.payment.receiptno']} :">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															rendered="#{pages$PaymentsDetails.paymentCollected}"
															value="#{pages$PaymentsDetails.paymentReceiptDetailsView.paymentReceiptNumber}">
														</h:outputText>
														<h:outputLabel
															rendered="#{pages$PaymentsDetails.paymentCollected}"
															value="#{msg['financialAccConfiguration.bankAccName']} :">
														</h:outputLabel>
														<h:outputText styleClass="TEXT_DATA"
															rendered="#{pages$PaymentsDetails.paymentCollected}"
															value="#{!pages$PaymentsDetails.isEnglishLocale?pages$PaymentsDetails.paymentReceiptDetailsView.bankAr:pages$PaymentsDetails.paymentReceiptDetailsView.bankEn}">
														</h:outputText>

													</t:panelGrid>

												</div>
												<table width="96.0%">
													<tr>
														<td colspan="6" class="BUTTON_TD">
															<h:commandButton id="btnSendToParent"
																onclick="window.close();" style="width: auto"
																styleClass="BUTTON"
																value="#{msg['commons.closeButton']}" />
														</td>
													</tr>
												</table>
											</div>
										</div>
									</h:form>

								</td>
							</tr>
						</table>

					</td>
				</tr>

			</table>

		</body>
	</html>
</f:view>

