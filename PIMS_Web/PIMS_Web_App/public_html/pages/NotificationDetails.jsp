<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)\
  - Description: Used for Adding Auction Units to an Auction
  --%>
<%@page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	    
	    function resetValues()
      	{
	      	document.getElementById("unitSearchFrm:investmentPurposeList").selectedIndex=0;
	      	document.getElementById("unitSearchFrm:propertyCategoryList").selectedIndex=0;
		    document.getElementById("unitSearchFrm:propertyUsageList").selectedIndex=0;
      	    document.getElementById("unitSearchFrm:propertyTypeList").selectedIndex=0;
		    document.getElementById("unitSearchFrm:propertyNumber").value="";
			document.getElementById("unitSearchFrm:commercialName").value="";
			document.getElementById("unitSearchFrm:endowedName").value="";
        }
	    

</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>

			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>

		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>



			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="100%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0"
							style="margin-left: 1px; margin-right: 1px;">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['notificationDetails.heading']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99.2%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0" style="margin-right: 1px;"  >
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap" 
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION_POPUP"
										style="height: 400px; overflow: hidden;">
										<h:form id="unitSearchFrm" style="width:90%;">
										<h:inputHidden id="notificationScreenMode"
																	value="#{pages$NotificationDetails.screenModeValue}"
																	binding="#{pages$NotificationDetails.screenMode}" ></h:inputHidden>
									<h:inputHidden id="notificationidhidden"
																	value="#{pages$NotificationDetails.notificationId}"
																	binding="#{pages$NotificationDetails.notificationIdHidden}" ></h:inputHidden>
											<div style="height: 25px">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText value="#{pages$NotificationDetails.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div
												style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 600px; PADDING-TOP: 10px">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG 
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px" >
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>

												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notificationDetails.type']}:"></h:outputLabel>
															<br /></td>
															<td >
																<h:selectOneMenu
																	value="#{pages$NotificationDetails.notificationTypeId}"
																	binding="#{pages$NotificationDetails.notificationTypeSelectMenu}"
																	id="motificationTypeList"
																	required="false"
																	tabindex="2">
																	<f:selectItem itemValue="SMTP" itemLabel="SMTP" />
																	<f:selectItem itemValue="SMS" itemLabel="SMS" />
																</h:selectOneMenu>
															<br /></td>
															
															
															
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notificationDetails.subject']}:"></h:outputLabel>
															<br /></td>
															<td >
																<h:inputText id="notificationSubject"
																	value="#{pages$NotificationDetails.notificationSubject}"
																	binding="#{pages$NotificationDetails.subjectInputText}"
																	maxlength="170"
																	tabindex="3"></h:inputText>
															</td>
														</tr>
														<tr>
															<td >
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['notificationDetails.body']}:"></h:outputLabel>
															<br /></td>
															<td >
																<br />
															</td>
															
															
														</tr>
														<tr>
															<td style="PADDING: 3px; padding-bottom: 0px;" colspan="2" >
																<h:inputTextarea id="notificationBodyText"
																	value="#{pages$NotificationDetails.notificationBody}"
																	binding="#{pages$NotificationDetails.bodyInputText}"
																	style="width: 550px; height: 100px" rows="5" cols="75"
																	tabindex="3"></h:inputTextarea>
															<br /></td>
															
															
															
														</tr>
														<tr>
															<td class="BUTTON_TD" colspan="2" style="padding-right: 34px;">
																<table cellpadding="1" cellspacing="1">
																	<tr>
																		<td>
																			<h:commandButton  id="generateButtonId" styleClass="BUTTON"
																				binding="#{pages$NotificationDetails.generateButton}"
																				value="#{msg['commons.add']}"
																				action="#{pages$NotificationDetails.generateNotification}"
																				style="width: 75px" tabindex="10"></h:commandButton>
																		</td>
																		<td>
																			<h:commandButton id="updateButtonId"  styleClass="BUTTON"
																				binding="#{pages$NotificationDetails.saveButton}"
																				value="#{msg['commons.send']}"
																				action="#{pages$NotificationDetails.saveNotification}"
																				style="width: 75px" tabindex="10"></h:commandButton>
																		</td>
																		<td>
																			<h:commandButton id="cancelButtonId" styleClass="BUTTON"
																				value="#{msg['commons.cancel']}"
																				onclick="window.close();"
																				style="width: 75px" tabindex="11"></h:commandButton>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</div>
											</div>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</body>
	</html>
</f:view>