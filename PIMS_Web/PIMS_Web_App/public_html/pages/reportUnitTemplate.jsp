<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">

	function openLoadReportPopup(pageName) 	{	
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />


			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />





		</head>

		<body>
			<div style="width: 1024px;">


				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>


					</tr>
					<tr width="100%">

						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel
											value="#{msg['listReport.unitTemplate.reportSearchPage.heading']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg">
									</td>
									<td width="100%" height="99%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION" style="height: 450px;">

											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$reportUnitTemplate.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>


											<h:form id="searchFrm">

												<div class="MARGIN" style="width: 96%">
													<table cellpadding="0" cellspacing="0">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td style="FONT-SIZE: 0px" width="100%">
																<IMG
																	src="../<h:outputText value="#{path.img_section_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="DETAIL_SECTION" style="width: 99.6%">
														<h:outputLabel value="#{msg['commons.report.criteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="0px" cellspacing="0px"
															class="DETAIL_SECTION_INNER" border="0">
															<tr>
																<td width="97%">
																	<table width="100%">
																		<tr>
																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.property.Name']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:inputText id="txPropertyName"
																					value="#{pages$reportUnitTemplate.criteria.txtPropertyName}"
																					styleClass="INPUT_TEXT" maxlength="250"></h:inputText>
																			</td>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['inspection.propertyRefNum']} :"></h:outputLabel>
																			</td>

																			<td>


																				<h:inputText id="txtPropertyNumber"
																					value="#{pages$reportUnitTemplate.criteria.txtPropertyNumber}"
																					styleClass="INPUT_TEXT" maxlength="20"></h:inputText>

																			</td>


																		</tr>

																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['property.type']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:selectOneMenu id="cboPropertyType"
																					required="false"
																					value="#{pages$reportUnitTemplate.criteria.cboPropertyType}">

																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyTypeList}" />
																				</h:selectOneMenu>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.property.usageType']} :" />

																			</td>
																			<td>
																				<h:selectOneMenu id="cboPropertyUsage"
																					required="false"
																					value="#{pages$reportUnitTemplate.criteria.cboPropertyUsage}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyUsageList}" />
																				</h:selectOneMenu>

																			</td>



																		</tr>

																		<tr>



																			<td width="25%">
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.unitType']} :"></h:outputLabel>
																			</td>
																			<td width="25%">
																				<h:selectOneMenu id="unitType" required="false"
																					value="#{pages$reportUnitTemplate.criteria.cboUnittype}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.unitTypeList}" />
																				</h:selectOneMenu>
																			</td>


																			<td>


																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.floorNumber']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtFloorNumber"
																					value="#{pages$reportUnitTemplate.criteria.txtFloorNumber}"
																					maxlength="20"></h:inputText>
																			</td>
																		</tr>

																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.unitTemplate.unitUsage']} :"></h:outputLabel>
																			</td>

																			<td>
																				<h:selectOneMenu id="cboUnitUsage" required="false"
																					value="#{pages$reportUnitTemplate.criteria.cboUnitUsage}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.unitUsageTypeList}" />
																				</h:selectOneMenu>

																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.unitTemplate.unitSubType']} :"></h:outputLabel>
																			</td>

																			<td>
																				<h:selectOneMenu id="cboUnitSubType"
																					required="false"
																					value="#{pages$reportUnitTemplate.criteria.cboUnitSubType}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.unitSideType}" />
																				</h:selectOneMenu>

																			</td>

																		</tr>


																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.unitTemplate.ownerName']} :"></h:outputLabel>


																			</td>
																			<td>


																				<h:inputText id="txtTenantName"
																					value="#{pages$reportUnitTemplate.criteria.txtOwnerName}"
																					maxlength="20"></h:inputText>
																			</td>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.unitTemplate.ownerNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtOwnerNo"
																					value="#{pages$reportUnitTemplate.criteria.txtOwnerNo}"
																					maxlength="20"></h:inputText>
																			</td>


																		</tr>


																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.fromDate']} :"></h:outputLabel>
																			</td>
																			<td>




																				<rich:calendar id="fromDate" popup="true"
																					datePattern="#{pages$reportUnitTemplate.dateFormat}"
																					locale="#{pages$reportUnitTemplate.locale}"
																					value="#{pages$reportUnitTemplate.criteria.clndrFromDate}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width: 170px; height: 14px"></rich:calendar>
																			</td>


																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.toDate']} :"></h:outputLabel>
																			</td>
																			<td>

																				<rich:calendar id="toDate"
																					datePattern="#{pages$reportUnitTemplate.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$reportUnitTemplate.locale}"
																					value="#{pages$reportUnitTemplate.criteria.cldrToDate}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px">

																				</rich:calendar>
																			</td>

																		</tr>
																		<tr>
																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.unitTemplate.community']} :" />

																			</td>
																			<td>
																				<h:selectOneMenu id="cboPropertyCommunity"
																					required="false"
																					value="#{pages$reportUnitTemplate.criteria.cboPropertyCommunity}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems value="#{view.attributes['city']}" />
																				</h:selectOneMenu>
																			</td>

																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['property.landNumber']} :"></h:outputLabel>
																			</td>
																			<td>

																				<h:inputText id="landNmuberInput"
																					value="#{pages$reportUnitTemplate.criteria.txtLandNumber}"></h:inputText>
																			</td>

																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.costCenter']} :" />
																			</td>
																			<td>
																				<h:inputText id="txtunitCostCenter"
																					value="#{pages$reportUnitTemplate.criteria.txtCostCenter}"
																					maxlength="250"></h:inputText>
																			</td>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.unitStatus']} :" />
																			</td>
																			<td>
																				<h:selectOneMenu id="cboUnitStatus" required="false"
																					value="#{pages$reportUnitTemplate.criteria.cboUnitStatus}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.unitStatusList}" />
																				</h:selectOneMenu>
																			</td>

																		</tr>
																		<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.unitRentAmountFrom']} :"></h:outputLabel>
																			</td>

																			<td>


																				<h:inputText id="txtUnitRentAmountFrom"
																					value="#{pages$reportUnitTemplate.criteria.unitRentAmountFrom}"
																					styleClass="INPUT_TEXT" maxlength="20"></h:inputText>

																			</td>


																			<td>


																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.unitRentAmountTo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtUnitRentAmountTo"
																					value="#{pages$reportUnitTemplate.criteria.unitRentAmountTo}"
																					maxlength="20"></h:inputText>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.contractAmountFrom']} :"></h:outputLabel>
																			</td>

																			<td>


																				<h:inputText id="txtContractAmountFrom"
																					value="#{pages$reportUnitTemplate.criteria.contractRentAmountFrom}"
																					styleClass="INPUT_TEXT" maxlength="20"></h:inputText>

																			</td>


																			<td>


																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.contractAmountTo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtContractAmountTo"
																					value="#{pages$reportUnitTemplate.criteria.contractRentAmountTo}"
																					maxlength="20"></h:inputText>
																			</td>
																		</tr>

																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.unitTemplate.nationality']} :" />

																			</td>
																			<td>
																				<h:selectOneMenu id="cboOwnerNationality"
																					required="false"
																					value="#{pages$reportUnitTemplate.criteria.cboOwnerNationality}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.countryList}" />
																				</h:selectOneMenu>
																			</td>



																			<td>


																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.unitTemplate.visaNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtOwnerVisaNo"
																					value="#{pages$reportUnitTemplate.criteria.txtOwnerVisaNo}"
																					maxlength="30"></h:inputText>
																			</td>


																		</tr>

																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.unitTemplate.tradeLNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtOwnerTradeLNo"
																					value="#{pages$reportUnitTemplate.criteria.txtOwnerTradeLNo}"
																					maxlength="10"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.unitTemplate.phoneNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtOwnerPhoneNo"
																					value="#{pages$reportUnitTemplate.criteria.txtOwnerPhoneNo}"
																					maxlength="30"></h:inputText>
																			</td>


																		</tr>


																		<tr>

																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.contractNumber']} :"></h:outputLabel>
																			</td>
																			<td>

																				<h:inputText id="txtContractNumber"
																					value="#{pages$reportUnitTemplate.criteria.txtContractNumber}"
																					maxlength="20"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.unitTemplate.nidNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtOwnerNidNo"
																					value="#{pages$reportUnitTemplate.criteria.txtOwnerNidNo}"
																					maxlength="20"></h:inputText>
																			</td>


																		</tr>


																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.unitTemplate.dmNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtPropertyDmNo"
																					value="#{pages$reportUnitTemplate.criteria.txtPropertyDmNo}"
																					maxlength="7"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.unitTemplate.dewaAccNo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtPropertyDewaAccNo"
																					value="#{pages$reportUnitTemplate.criteria.txtPropertyDewaAccNo}"
																					maxlength="15"></h:inputText>
																			</td>


																		</tr>
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['inquiry.unitAreaMin']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtUnitAreaSqFtFrom"
																					value="#{pages$reportUnitTemplate.criteria.txtUnitAreaSqFtFrom}"
																					maxlength="12"></h:inputText>
																			</td>


																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['inquiry.unitAreaMax']} :"></h:outputLabel>
																			</td>

																			<td>

																				<h:inputText id="txtUnitAreaSqFtTo"
																					value="#{pages$reportUnitTemplate.criteria.txtUnitAreaSqFtTo}"
																					maxlength="12"></h:inputText>
																			</td>

																		</tr>

																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['receiveProperty.ownershipType']} :" />

																			</td>
																			<td>
																				<h:selectOneMenu id="cboPropertyOwnershipType"
																					required="false"
																					value="#{pages$reportUnitTemplate.criteria.cboPropertyOwnershipType}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.propertyOwnershipType}" />
																				</h:selectOneMenu>
																			</td>






																		</tr>


																		<tr>
																			<td class="BUTTON_TD" colspan="4">
																				<h:commandButton styleClass="BUTTON"
																					id="submitButton"
																					action="#{pages$reportUnitTemplate.cmdView_Click}"
																					immediate="false" value="#{msg['commons.view']}"
																					tabindex="7"></h:commandButton>
																			</td>
																		</tr>

																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>
															</tr>

														</table>


													</div>
												</div>



											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

		</body>
	</html>
</f:view>
