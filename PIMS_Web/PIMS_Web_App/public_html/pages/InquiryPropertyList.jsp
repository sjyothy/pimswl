<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<script language="JavaScript" type="text/javascript" src="../resources/jscripts/commons.js"></script>
<script language="javascript" type="text/javascript">
  
	function openUnitSearchPopup()
	{
		var screen_width = 1024;
        var screen_height = 480;
        var popup_width = screen_width-25;
        var popup_height = screen_height;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
              
        var popup = window.open('UnitSearch.jsf?pageMode=MODE_SELECT_MANY_POPUP&context=PropertyInquiryUnits','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,resizable=no,titlebar=no,dialog=yes');
        popup.focus();
	}
	
	function   showPersonPopup()
						{
						   var screen_width = 1024;
						   var screen_height = 470;
						   window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
						    
						    
						}
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
		{
		    
		    // alert("in populatePerson");
		    
		    document.getElementById("formInquiry:hdnPersonId2").value=personId;
		    document.getElementById("formInquiry:hdnCellNo").value=cellNumber;
		    document.getElementById("formInquiry:hdnPersonName").value=personName;
		    document.getElementById("formInquiry:hdnPersonType").value=hdnPersonType;
		    document.getElementById("formInquiry:hdnIsCompany").value=isCompany;
		    
			//alert("PersonID is :::::"+document.getElementById("formInquiry:hdnPersonId2").value);
	       // document.forms[0].submit();
	       return oamSubmitForm('formInquiry','formInquiry:lnkLoadPerson');
		}						

	    function showPopup()
		{
		//alert("In show PopUp");
		   var screen_width = screen.width;
		   var screen_height = screen.height;
		   window.open('UnitList.jsf?','_blank','width='+(screen_width-10)+',height='+(screen_height-150)+',left=0,top=40,scrollbars=yes,status=yes');
		    
		}

        function  isUnitAlreadyPresent(unitId)
        {
        
                    
                     var selectedManyUnits = document.getElementById("formInquiry:selectManyUnits");
                    //alert("isUnitAlready"+parseInt(selectedManyUnits.length));
                    //alert("unit id "+unitId);
                     for( var s=0 ; s < selectedManyUnits.length ;  s++ )
				     {   
				     //   alert("isUnitAlready inside for loop  "+s);
				       // alert("selectedManyUnits.options[s].value  "+selectedManyUnits.options[s].value);
					    if(selectedManyUnits.options[s].value== unitId)
					    return true;
					 }
					 
					// alert("isUnitAlready4");
					 return false;
        }

	    function populateSelectManyUsers(selectOneUnitType,selectOneUnitUsage,
                                         unitNumber,rentValueMin,rentValueMax,noOfBedsMax,
                                         noOfBedsMin,countryId,stateId,cityId) 
		{
   
                 var hdnSelectUnitType = document.getElementById("formInquiry:hdnSelectUnitType");
		         var hdnSelectUnitUsage = document.getElementById("formInquiry:hdnSelectUnitUsage");
		         var hdnUnitNumber = document.getElementById("formInquiry:hdnUnitNumber");
		         var hdnRentValueMin = document.getElementById("formInquiry:hdnRentValueMin");
		         var hdnRentValueMax = document.getElementById("formInquiry:hdnRentValueMax");
		         var hdnNoOfbedsMax = document.getElementById("formInquiry:hdnNoOfbedsMax");
		         var hdnNoOfbedsMin = document.getElementById("formInquiry:hdnNoOfbedsMin");
		         var hdnCountryId = document.getElementById("formInquiry:hdnCountryId");
		         var hdnStateId = document.getElementById("formInquiry:hdnStateId");
		         var hdnCityId = document.getElementById("formInquiry:hdnCityId");
		                      
                hdnSelectUnitType.value = selectOneUnitType;
                hdnSelectUnitUsage.value = selectOneUnitUsage;
                hdnUnitNumber.value = unitNumber;
                hdnRentValueMin.value = rentValueMin;
                hdnRentValueMax.value = rentValueMax;
                hdnNoOfbedsMax.value= noOfBedsMax;
                hdnNoOfbedsMin.value= noOfBedsMin;
                hdnCountryId.value= countryId;
                hdnStateId.value= stateId;
                hdnCityId.value= cityId;
                

		}  
		
		function removeSelectedItems()
            {
                  var objTargetElement = document.getElementById("formInquiry:selectManyUnits");
//                  var tempObjTargetElement = new Array();
                    
                  var hdnUserIdsElement = document.getElementById("formInquiry:hdnUnitIds");
//                          alert(objTargetElement.length);
                           hdnUserIdsElement.value="";
//                  tempObjTargetElement.text = null;                           
                
//                for(i=0;i<tempObjTargetElement.length;i++){
//                 tempObjTargetElement.options[i].text= null;
 //               }
  
                  for(i=0;i<objTargetElement.length;i++)
              {   
//                    alert("Select Items"+objTargetElement.options[i].text);
                    if(objTargetElement.options[i].selected)
                    {
                    objTargetElement.options[i]= null;
                    i = i-1;
                    
                    
                    }
                    else
                    {
                      hdnUserIdsElement.value=hdnUserIdsElement.value+","+objTargetElement[i].value;

  //                   alert("Unselected Item:"+hdnUserIdsElement.value);

                    }
                     


              }
            }
		
		
		function clearWindow()
		{  
		    
		    document.getElementById("formInquiry:hdnUnitIds").value="";
		    document.getElementById("formInquiry:datetimeIquiry").value="";
		    var cmbPriority=document.getElementById("formInquiry:selectPriority");
		     cmbPriority.selectedIndex = 0;
		    var cmbMethod=document.getElementById("formInquiry:selectInquiryMethod");
		     cmbMethod.selectedIndex = 0;
		    document.getElementById("formInquiry:txtPerson").value="";
		    document.getElementById("formInquiry:hdnPersonId").value="";
		    document.getElementById("formInquiry:txtPassportNo").value="";
		    document.getElementById("formInquiry:txtCellNo").value="";
		    document.getElementById("formInquiry:txtDesignation").value="";
		    
		    


		    var selectedManyUnits = document.getElementById("formInquiry:selectManyUnits");
             for( var s=0 ; s < selectedManyUnits.length ;  s++ )
		     {   
		        selectedManyUnits.options[s].value="";
		        selectedManyUnits.options[s].text="";
			    
			 }
		
		}
		
		
		
    
     </script>
<style>

   .rich-calendar-input{
    width:160px;
    height:20px; 
     }

</style>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />


<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" >
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
				<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table_ff}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
	 </head>
	
	

<body>
	<div class="containerDiv">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0" height="100%">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
    <td class="divLeftMenuWithBody" width="17%">
        <jsp:include page="leftmenu.jsp"/>
    </td>
    <td width="83%" height="505px" valign="top" class="divBackgroundBody">
 <table width="99.2%" class="greyPanelTable" cellpadding="0"
					cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
								<h:outputLabel value="#{msg['inquiry.propertyinquiry']}" styleClass="HEADER_FONT"/>
						</td>
						<td width="100%">
							&nbsp;
						</td>
					</tr>
				</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
								<!--Use this below only for desiging forms Please do not touch other sections -->


								<td height="100%" width="100%" valign="top" nowrap="nowrap">

									<div class="SCROLLABLE_SECTION" style="height:420px;#height:445px;">
									<h:form id="formInquiry" >
			                                         <h:inputHidden id="hdnUnitIds" value="#{pages$InquiryPropertyList.hdnUnitIds}"    binding="#{pages$InquiryPropertyList.ctlHdnUnitId}"  />					
							                         <h:inputHidden id="hdnPersonId" value="#{pages$InquiryPropertyList.hdnPersonIds}" binding="#{pages$InquiryPropertyList.ctlHdnPersonId}" />
							                         <h:inputHidden id="hdnUnitAreaMin"  binding="#{pages$InquiryPropertyList.ctlHdnUnitAreaMin}"  />					
							                         <h:inputHidden id="hdnUnitAreaMax"  binding="#{pages$InquiryPropertyList.ctlHdnUnitAreaMax}" />
							                         <h:inputHidden id="hdnLivingMax"  binding="#{pages$InquiryPropertyList.ctlHdnLivingMax}"  />					
							                         <h:inputHidden id="hdnBedsMax"  binding="#{pages$InquiryPropertyList.ctlHdnBedsMax}" />
							                         <h:inputHidden id="hdnBathMax"  binding="#{pages$InquiryPropertyList.ctlHdnBathMax}"  />					
							                         <h:inputHidden id="hdnRentMin"  binding="#{pages$InquiryPropertyList.ctlHdnRentMin}" />
							                         <h:inputHidden id="hdnRentMax"  binding="#{pages$InquiryPropertyList.ctlHdnRentMax}"  />					
							                         <h:inputHidden id="hdnFloorMin"  binding="#{pages$InquiryPropertyList.ctlHdnFloorMin}" />
							                         <h:inputHidden id="hdnObjSelectUnitType"  binding="#{pages$InquiryPropertyList.ctlHdnObjSelectUnitType}" />
							                         <h:inputHidden id="hdnObjSelectUnitUsage"  binding="#{pages$InquiryPropertyList.ctlHdnObjSelectUnitUsage}" />
							                         <h:inputHidden id="hdnObjSelectUnitSide"  binding="#{pages$InquiryPropertyList.ctlHdnObjSelectUnitSide}" />
							                         
							                         <h:inputHidden id="hdnPersonId2"  value="#{pages$InquiryPropertyList.hdnPersonId}" />
							                         <h:inputHidden id="hdnPersonName"  value="#{pages$InquiryPropertyList.hdnPersonName}"  />					
							                         <h:inputHidden id="hdnPersonType"  value="#{pages$InquiryPropertyList.hdnPersonType}" />
							                         <h:inputHidden id="hdnCellNo"  value="#{pages$InquiryPropertyList.hdnCellNo}"  />					
							                         <h:inputHidden id="hdnIsCompany"  value="#{pages$InquiryPropertyList.hdnIsCompany}" />
							                         
							                         <h:inputHidden id="hdnSelectUnitType"  value="#{pages$InquiryPropertyList.selectOneUnitType}" />
							                         <h:inputHidden id="hdnSelectUnitUsage"  value="#{pages$InquiryPropertyList.selectOneUnitUsage}"  />					
							                         <h:inputHidden id="hdnUnitNumber"  value="#{pages$InquiryPropertyList.unitNumber}" />
							                         <h:inputHidden id="hdnRentValueMin"  value="#{pages$InquiryPropertyList.rentValueMin}"  />					
							                         <h:inputHidden id="hdnRentValueMax"  value="#{pages$InquiryPropertyList.rentValueMax}" />
							                         <h:inputHidden id="hdnNoOfbedsMax"  value="#{pages$InquiryPropertyList.noOfBedsMax}" />
							                         <h:inputHidden id="hdnNoOfbedsMin"  value="#{pages$InquiryPropertyList.noOfBedsMin}"  />					
							                         <h:inputHidden id="hdnCountryId"  value="#{pages$InquiryPropertyList.countryId}" />
							                         <h:inputHidden id="hdnStateId"  value="#{pages$InquiryPropertyList.stateId}"  />					
							                         <h:inputHidden id="hdnCityId"  value="#{pages$InquiryPropertyList.cityId}" />
							                         
						            
			                            <div>
										<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
											<tr>
												<td>
													<h:outputText id="successMsg_2" value="#{pages$InquiryPropertyList.successMessages}" escape="false"  styleClass="INFO_FONT"/>
													<h:outputText id="errorMsg_1" value="#{pages$InquiryPropertyList.errorMessages}"  escape="false" styleClass="ERROR_FONT"/>
												</td>
											</tr>
										</table>
									</div>
											
								 		<table  class="TAB_PANEL_MARGIN" width="98%" border="0">

											<tr>
												<td colspan="2">
												<div >
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
													<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
										</table>
													
											
									<rich:tabPanel style="HEIGHT: 330px;width: 100%">
				                          <rich:tab id="InquiryTab" label="#{msg['commons.tab.inquiry']}">
				                          
												<t:div id="divInquiry" styleClass="TAB_DETAIL_SECTION"> 
												
												
												  <t:panelGrid cellpadding="1px" width="100%" cellspacing="5px" styleClass="TAB_DETAIL_SECTION_INNER"  columns="4">
												  
												  	<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.inquiryNumber']}:" ></h:outputLabel>
												  	<h:inputText readonly="true" style="width: 160px;height:20px;" styleClass="READONLY"></h:inputText>
												  	
												  	<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.status']}:"></h:outputLabel>
												  	<h:selectOneMenu readonly="true"  id="selectApplicationStatus" style="width: 160px;height:20px;" 
												  					value="#{pages$InquiryPropertyList.selectOneApplicationStatus}" styleClass="READONLY">
															 <f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1"/>
															 <f:selectItems value="#{pages$InquiryPropertyList.requestStatusList}"/>
													</h:selectOneMenu>
												  												           
												      
												 	<h:outputLabel styleClass="LABEL" value="#{msg['inquiryApplication.receivedBy']}:" ></h:outputLabel>
													<h:selectOneMenu  styleClass="select" id="selectInquiryMethod"  style="width: 160px;height:20px;" binding="#{pages$InquiryPropertyList.prioritySelectMenu}"
														value="#{pages$InquiryPropertyList.selectOneInquiryMethod}">
														<f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
														<f:selectItems value="#{pages$ApplicationBean.inquiryMethod}"/>
													</h:selectOneMenu>
												
													<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.priority']}:"></h:outputLabel>
													<h:selectOneMenu id="selectPriority" style="width: 160px;height:20px;" binding="#{pages$InquiryPropertyList.inquiryMethodSelectMenu}"
															value="#{pages$InquiryPropertyList.selectOnePriority}">
														<f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}" />	
														<f:selectItems value="#{pages$ApplicationBean.inquiryPriority}"/>
													</h:selectOneMenu>
													
													<h:outputLabel styleClass="LABEL"   value="#{msg['inquiryApplication.inquirerName']}:"></h:outputLabel>
													<t:panelGrid id="gridApplicant" columns="3" width="70%" cellpadding="0" cellspacing="1" columnClasses="LEASE_CONTRACT_BASIC_INFO_SPONSORMANGER_GRID_C1,LEASE_CONTRACT_BASIC_INFO_SPONSORMANGER_GRID_C2">
														<h:inputText id="txtName" readonly="true"  style="width: 160px;height:20px;" value="#{pages$InquiryPropertyList.name}" binding="#{pages$InquiryPropertyList.txtName}"></h:inputText>
														<h:graphicImage id="imgPerson"  style="MARGIN: 0px 0px -4px" url="../resources/images/app_icons/Search-tenant.png" onclick="showPersonPopup();"></h:graphicImage>
														<h:graphicImage id="imgViewPerson" title="#{msg['commons.details']}"  rendered="#{pages$InquiryPropertyList.hdnPersonId != null}" onclick="javascript:showPersonReadOnlyPopup('#{pages$InquiryPropertyList.hdnPersonId}')" style="MARGIN: 0px 0px -4px; CURSOR: hand;" url="../resources/images/app_icons/Add-Person.png"></h:graphicImage>													        
													</t:panelGrid>
												
												    <h:outputLabel styleClass="LABEL" value="#{msg['inquiry.inquiryDate']}:"></h:outputLabel>												      
													<t:panelGroup>
														<h:inputHidden  id="datetimeIquiryhdn" value="#{pages$InquiryPropertyList.inquiryDate}"  binding="#{pages$InquiryPropertyList.ctlInquiryDate}"/>
														<rich:calendar  id="inquiryDate" value="#{pages$InquiryPropertyList.inquiryDate}" binding="#{pages$InquiryPropertyList.richCalendar}"
														                popup="true" datePattern="#{pages$InquiryPropertyList.dateFormat}" showApplyButton="false"
														                locale="#{pages$InquiryPropertyList.locale}" 
														                enableManualInput="false" cellWidth="24px"/>
													</t:panelGroup> 
													
													<h:outputLabel  styleClass="LABEL"  value="#{msg['inquiryApplication.personNumber']}:"></h:outputLabel>
													<h:inputText styleClass="READONLY" id="txtPersonNumber" value="#{pages$InquiryPropertyList.personNumber}" readonly="true"  style="width: 160px;height:20px;"></h:inputText>
													
													<h:outputLabel  styleClass="LABEL"  value="#{msg['inquiryApplication.passportNumber']}:"></h:outputLabel>
													<h:inputText styleClass="READONLY" id="txtPassportNumber" value="#{pages$InquiryPropertyList.passportNumber}" readonly="true" style="width: 160px;height:20px;"></h:inputText>
													
													<h:outputLabel  styleClass="LABEL"  value="#{msg['inquiryApplication.passportIssueDate']}:"></h:outputLabel>
													<h:inputText styleClass="READONLY" id="txtPassportIssueDate" value="#{pages$InquiryPropertyList.passportIssueDate}" readonly="true" style="width: 160px;height:20px;"></h:inputText>
													
													<h:outputLabel  styleClass="LABEL"  value="#{msg['inquiryApplication.passportExpiryDate']}:"></h:outputLabel>
													<h:inputText styleClass="READONLY" id="txtPassportExpiryDate" value="#{pages$InquiryPropertyList.passportExpiryDate}" readonly="true" style="width: 160px;height:20px;"></h:inputText>
													
													<h:outputLabel  styleClass="LABEL"  value="#{msg['inquiryApplication.profession']}:"></h:outputLabel>
													<h:inputText styleClass="READONLY" id="txtProfession" value="#{pages$InquiryPropertyList.profession}" readonly="true" style="width: 160px;height:20px;"></h:inputText>
													
													<h:outputLabel  styleClass="LABEL"  value="#{msg['inquiryApplication.company']}:"></h:outputLabel>
													<h:inputText styleClass="READONLY" id="txtCompany" value="#{pages$InquiryPropertyList.company}" readonly="true" style="width: 160px;height:20px;"></h:inputText>
													
													<h:outputLabel  styleClass="LABEL"  value="#{msg['inquiryApplication.phoneNumber']}:"></h:outputLabel>
													<h:inputText styleClass="READONLY" id="txtPhoneNumber" value="#{pages$InquiryPropertyList.phoneNumber}" readonly="true" style="width: 160px;height:20px;"></h:inputText>
													
													<h:outputLabel  styleClass="LABEL"   value="#{msg['applicationDetails.cell']}:"></h:outputLabel>
													<h:inputText styleClass="READONLY" id="txtCell" maxlength="15" style="width: 160px;height:20px;"  readonly="true" binding="#{pages$InquiryPropertyList.txtCellNo}"  value="#{pages$InquiryPropertyList.cellNo}" ></h:inputText>
													
													<h:outputLabel  styleClass="LABEL"  value="#{msg['inquiryApplication.poBox']}:"></h:outputLabel>
													<h:inputText styleClass="READONLY" id="txtPOBox" value="#{pages$InquiryPropertyList.poBox}" readonly="true" style="width: 160px;height:20px;"></h:inputText>
													
													<h:outputLabel  styleClass="LABEL"  value="#{msg['applicationDetails.email']}:"></h:outputLabel>
													<h:inputText styleClass="READONLY" id="txtemail" readonly="true"  style="width: 160px;height:20px;" binding="#{pages$InquiryPropertyList.txtEmail}" value="#{pages$InquiryPropertyList.email}" ></h:inputText>
													
													<h:outputLabel rendered="false"  styleClass="LABEL" id="lblAppType"   value="#{msg['applicationDetails.applicantType']}:"></h:outputLabel>
													<h:inputText styleClass="READONLY" rendered="false" id="txtAppType"  maxlength="15"  style="width: 160px;height:20px;" readonly="true" binding="#{pages$InquiryPropertyList.txtApplicantType}" value="#{pages$InquiryPropertyList.applicantType}" ></h:inputText>
													
													<h:commandLink id="lnkLoadPerson" action="#{pages$InquiryPropertyList.loadPerson}" />
												
												</t:panelGrid>
												</t:div>
												
											
											</rich:tab>

												
												<rich:tab id="InquiryUnitsTab" label="#{msg['commons.tab.inquiryUnits']}">
											
										                <t:div rendered="#{! pages$InquiryPropertyList.enableDisableControl}" id="divUnit" styleClass="TAB_DETAIL_SECTION"> 
											               <t:panelGrid cellpadding="1px" width="100%" cellspacing="3px" styleClass="TAB_DETAIL_SECTION_INNER"  columns="4">
												   

												   
												        
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.unitType']}:"></h:outputLabel>
														<h:selectOneMenu  readonly="#{pages$InquiryPropertyList.enableDisableControl}" id="selectUnitType" style="width: 160px;height:20px;" value="#{pages$InquiryPropertyList.selectOneUnitType}" >
															 <f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1"/>
															 <f:selectItems value="#{pages$ApplicationBean.unitType}"/>
														</h:selectOneMenu>
													
													    <h:outputLabel styleClass="LABEL" value="#{msg['inquiry.unitUsage']}:"></h:outputLabel>
													    <h:selectOneMenu  readonly="#{pages$InquiryPropertyList.enableDisableControl}" id="selectUnitUsage" style="width: 160px;height:20px;" value="#{pages$InquiryPropertyList.selectOneUnitUsage}" >
															 <f:selectItem itemLabel="#{msg['commons.All']}" itemValue="-1"/>
   															 <f:selectItems value="#{pages$ApplicationBean.propertyUsageType}"/>
														</h:selectOneMenu>
														
												        
												        <h:outputLabel  styleClass="LABEL" value="#{msg['contact.country']}:"></h:outputLabel>
												 		 <h:selectOneMenu readonly="#{pages$InquiryPropertyList.enableDisableControl}" id="country" style="width: 160px;height:20px;" value="#{pages$InquiryPropertyList.countryId}"> 
															 <f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
															 <f:selectItems value="#{pages$InquiryPropertyList.countryList}" />
                                                          <a4j:support event="onchange" action="#{pages$InquiryPropertyList.loadState}" reRender="state,selectcity" />
														  </h:selectOneMenu>
															
													      <h:outputLabel styleClass="LABEL"  value="#{msg['contact.city']}:"></h:outputLabel>
														  <h:selectOneMenu readonly="#{pages$InquiryPropertyList.enableDisableControl}" id="state" style="width: 160px;height:20px;"  required="false"
																value="#{pages$InquiryPropertyList.stateId}">
																<f:selectItem itemValue="-1"
																	itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																<f:selectItems value="#{pages$InquiryPropertyList.stateList}" />
															<a4j:support event="onchange" action="#{pages$InquiryPropertyList.loadCity}" reRender="selectcity" />
															</h:selectOneMenu>
													
														<h:outputLabel  styleClass="LABEL" value="#{msg['commons.area']}:"></h:outputLabel>
											    	    <h:selectOneMenu readonly="#{pages$InquiryPropertyList.enableDisableControl}" id="selectcity" style="width: 160px;height:20px;"  value="#{pages$InquiryPropertyList.cityId}">
													                <f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1"/>
														            <f:selectItems value="#{pages$InquiryPropertyList.cityList}"/>
													    </h:selectOneMenu>
												
														<h:outputLabel styleClass="LABEL"  value="#{msg['inquiry.rentValueMin']}:"></h:outputLabel>
												        <h:inputText readonly="#{pages$InquiryPropertyList.enableDisableControl}" id="txtRentMin" styleClass="A_RIGHT_NUM" style="width: 160px;height:20px;" value="#{pages$InquiryPropertyList.rentValueMin}">
												        </h:inputText>
												
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.rentValueMax']}:"></h:outputLabel>
												        <h:inputText readonly="#{pages$InquiryPropertyList.enableDisableControl}" id="txtRentMax" styleClass="A_RIGHT_NUM" style="width: 160px;height:20px;" value="#{pages$InquiryPropertyList.rentValueMax}">
												        </h:inputText>
												          
												        <h:outputLabel styleClass="LABEL" value="#{msg['inquiry.noOfBedsMin']}:"></h:outputLabel>
												        <h:inputText readonly="#{pages$InquiryPropertyList.enableDisableControl}" id="txtBedsMin" styleClass="A_RIGHT_NUM" style="width: 160px;height:20px;" value="#{pages$InquiryPropertyList.noOfBedsMin}">
												        </h:inputText>
												         
														<h:outputLabel styleClass="LABEL" value="#{msg['inquiry.noOfBedsMax']}:"></h:outputLabel>
												        <h:inputText readonly="#{pages$InquiryPropertyList.enableDisableControl}" id="txtBedsMax"  styleClass="A_RIGHT_NUM" style="width: 160px;height:20px;" value="#{pages$InquiryPropertyList.noOfBedsMax}">
												        </h:inputText>
											             
														</t:panelGrid>
													</t:div>
											
												
												<t:div styleClass="BUTTON_TD"> 
												    <t:div styleClass="MARGIN" >
												    	
												    	  <h:commandButton styleClass="BUTTON" 
												    	  					value="#{msg['tenderManagement.AddUnit']}" 
												    	  					action="#{pages$InquiryPropertyList.openUnitSearchPopUp}" />
												    	  																	    
														  <h:commandButton rendered="false" styleClass="BUTTON"  type= "submit" style="width:128px"
																                           value="#{msg['commons.searchUnits']}" 
																                           binding="#{pages$InquiryPropertyList.openUnitListButton}" 
																                           action="#{pages$InquiryPropertyList.openUnitListPopUp}"/>
														  <h:commandButton rendered="false" styleClass="BUTTON"  type= "submit" style="width:128px"
																                           value="#{msg['common.clearUnits']}" 
																                           binding="#{pages$InquiryPropertyList.clearUnitListButton}" 
																                           action="#{pages$InquiryPropertyList.clearUnits}"/>	                           
													</t:div>
												</t:div>	
												   
												                           
											<t:div styleClass="contentDiv" style="width:100%" rendered="#{pages$InquiryPropertyList.enableDisableControl}">
										         <t:dataTable id="test2"
													value="#{pages$InquiryPropertyList.propertyInquiryDataList}"
													binding="#{pages$InquiryPropertyList.dataTable}"
													rows="15" width="100%" 
													preserveDataModel="false" preserveSort="false" var="dataItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="false" >

													<t:column id="col1"  sortable="true">
														<f:facet name="header" >
															<t:outputText value="#{msg['unit.unitNumber']}" />
														</f:facet>
														<t:outputText value="#{dataItem.unitNumber}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
													</t:column>
                                     	

												<t:column id="col2"  sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['cancelContract.tab.unit.propertyname']}"  />
													</f:facet>
													<t:outputText value="#{dataItem.propertyCommercialName}" styleClass="A_LEFT" style="white-space: normal;" />
												</t:column>
												
												<t:column sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['inquiryApplication.propertyType']}"  />
													</f:facet>
													<t:outputText value="#{pages$InquiryPropertyList.isEnglishLocale ? dataItem.propertyTypeEn : dataItem.propertyTypeAr}" styleClass="A_LEFT" style="white-space: normal;" />
												</t:column>
	

												<t:column id="col3"  >
													<f:facet name="header">
														<t:outputText value= "#{msg['unit.unitArea']}" />
													</f:facet>
													<t:outputText value="#{dataItem.unitArea}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
												</t:column>

												<t:column id="col4" >
													<f:facet name="header">
														<t:outputText value="#{msg['unit.noOfBed']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.noOfBed}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
												</t:column>
									
										        <t:column id="col5" >
													<f:facet name="header">
														<t:outputText value="#{msg['unit.noOfLiving']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.noOfLiving}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
												</t:column>
					     
	        								    <t:column id="col6" >
													<f:facet name="header">
														<t:outputText value="#{msg['unit.noOfBath']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.noOfBath}" styleClass="A_RIGHT_NUM" style="white-space: normal;" />
												</t:column>
						
										        <t:column id="col7" >
													<f:facet name="header">
													 <t:outputText value="#{msg['unit.rentValue']}" />
													</f:facet>
													<t:outputText value ="#{dataItem.rentValue}" styleClass="A_RIGHT_NUM" style="white-space: normal;"/>
												</t:column>
												
												<t:column>
													<f:facet name="header">
														<t:outputText value="#{msg['commons.action']}" />
													</f:facet>				
													<h:commandLink action="#{pages$InquiryPropertyList.onUnitDelete}">
														<h:graphicImage title="#{msg['commons.delete']}" url="../resources/images/delete.gif" />
													</h:commandLink>										
												</t:column>
						
                                            
                                               </t:dataTable>										
											</t:div>
										
											                           	

									</rich:tab>
									
									<rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}">
										<%@  include file="attachment/attachment.jsp"%>
									</rich:tab>
									
									<rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}">
										<%@ include file="notes/notes.jsp"%>
									</rich:tab>

										</rich:tabPanel>
											
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
													<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
												</table>
												</div>
												</td>
											</tr>
											<tr>
												<td class="BUTTON_TD MARGIN" colspan="8">												
												 
                              	                          <h:commandButton styleClass="BUTTON" value="#{msg['commons.saveButton']}" type= "submit"  binding="#{pages$InquiryPropertyList.saveButton}"  action="#{pages$InquiryPropertyList.btnAdd_Click}" /> 
                              	                  
												           
												          <h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}"
																           actionListener="#{pages$InquiryPropertyList.cancel}"
																           onclick="if (!confirm('#{msg['auction.cancelConfirm']}')) return false"
																           tabindex="16"/>                  												
												</td>
											</tr>
										</table>
									</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
</f:view>


