

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="Cache-Control"
			content="no-cache,must-revalidate,no-store">
		<meta http-equiv="expires" content="0">

		<link rel="stylesheet" type="text/css"
			href="../resources/css/simple_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../resources/css/amaf_en.css" />
		<link rel="stylesheet" type="text/css"
			href="../resources/css/table.css" />
		<title>PIMS</title>

		<script language="javascript" type="text/javascript">
  
       



function move(fbox, tbox) {
a = '_id0:'+fbox;
b = '_id0:'+tbox;
	var objSourceElement = document.getElementById(a);
	var objTargetElement = document.getElementById(b);   
    var aryTempSourceOptions = new Array();   
         var x = 0;                //looping through source element to find selected options 

         for (var i = 0; i < objSourceElement.length; i++) {     
         if (objSourceElement.options[i].selected) {      
         //need to move this option to target element  
               
                  var intTargetLen = objTargetElement.length++;
         objTargetElement.options[intTargetLen].text = objSourceElement.options[i].text;      
         objTargetElement.options[intTargetLen].value = objSourceElement.options[i].value;       
         }       
         else {       
         //storing options that stay to recreate select element  
         var objTempValues = new Object(); 
   
objTempValues.text = objSourceElement.options[i].text;
objTempValues.value = objSourceElement.options[i].value;
aryTempSourceOptions[x] = objTempValues;  
x++;       
}  
}      
//resetting length of source     
objSourceElement.length = aryTempSourceOptions.length;  
//looping through temp array to recreate source select element 
for (var i = 0; i < aryTempSourceOptions.length; i++) {     
objSourceElement.options[i].text = aryTempSourceOptions[i].text;    
objSourceElement.options[i].value = aryTempSourceOptions[i].value;
objSourceElement.options[i].selected = false;    
}   
}    //-->    </SCRIPT>



	</head>
	<body>

		<f:view>



			<%
				UIViewRoot view = (UIViewRoot) session.getAttribute("view");
				if (view.getLocale().toString().equals("en")) {
			%>
			<body dir="ltr">
				<%
				} else {
				%>
			
			<body dir="rtl">
				<%
				}
				%>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<%
									if (view.getLocale().toString().equals("en")) {
									%>
									<td
										background="../resources/images/Grey panel/Grey-Panel-left-1.jpg"
										height="38" style="background-repeat:no-repeat;" width="100%">
										<font
											style="font-family:Trebuchet MS;font-weight:regular;font-size:19px;margin-left:15px;top:30px;">Policy</font>
										<!--<IMG src="../resources/images/Grey panel/Grey-Panel-left-1.jpg"/>-->
										<%
										} else {
										%>
									
									<td width="100%">
										<IMG
											src="../resources/images/ar/Detail/Grey Panel/Grey-Panel-right-1.jpg"></img>
										<%
										}
										%>
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>

									<td width="500px" height="100%" valign="top" nowrap="nowrap">
										<h:form>
											<table>
												<tr>
													<td colspan="6">
														<h:messages></h:messages>
													</td>
												</tr>
												<tr>
													<td>
														<h:outputLabel value="Policy Name:"></h:outputLabel>
													</td>
													<td>
														<h:inputText binding="#{SecPolicy.policyNamePriTextBox}"
															value="#{SecPolicy.policyNamePrm}"
															style="width: 195px; height: 30px"></h:inputText>
													</td>
													<td>
														&nbsp;
													</td>
													<td>
														<h:outputLabel value="Policy Name Arabic:"></h:outputLabel>
													</td>
													<td>
														<h:inputText value="#{SecPolicy.policyNameSec}"
															style="width: 195px; height: 30px"></h:inputText>

													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														<h:outputLabel value="Time Expression:"></h:outputLabel>
													</td>
													<td>
														<h:inputText binding="#{SecPolicy.timeExpressionTextBox}"
															value="#{SecPolicy.timeExpression}"
															style="width: 195px; height: 30px"></h:inputText>
													</td>
													<td>
														&nbsp;
													</td>
													<td>
														<h:outputLabel value="Password Expression:"></h:outputLabel>
													</td>
													<td>
														<h:inputText value="#{SecPolicy.passwordExpression}"
															style="width: 195px; height: 30px"></h:inputText>

													</td>
													<td>
														&nbsp;
													</td>
												</tr>



												<tr>
													<td colspan="5">
														<h:selectBooleanCheckbox value="#{SecPolicy.autoReset}">Auto Reset</h:selectBooleanCheckbox>
														&nbsp;
														<h:selectBooleanCheckbox value="#{SecPolicy.multLogin}">Mulit Login</h:selectBooleanCheckbox>
														&nbsp;
														<h:selectBooleanCheckbox value="#{SecPolicy.default}">Deafult</h:selectBooleanCheckbox>
													</td>

												</tr>



												<tr>
													<td>
														<h:commandButton styleClass="BUTTON" value="Submit"
															action="#{SecPolicy.savePolicy}"></h:commandButton>
													</td>

													<td>
														<h:commandButton styleClass="BUTTON" value="Reset"
															action="#{SecPolicy.reset}"></h:commandButton>
													</td>

												</tr>
											</table>
											<div class="contentDiv">



												<t:dataTable id="test2"
													value="#{SecPolicy.secPolicyDataList}"
													binding="#{SecPolicy.dataTable}" rows="8"
													preserveDataModel="true" preserveSort="true" var="dataItem"
													rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">



													<t:column id="col2" sortable="true" width="20%">
														<f:facet name="header">


															<t:outputText value="Pri" />

														</f:facet>
														<t:outputText value="#{dataItem.strPolicyNamePrm}" />
													</t:column>


													<t:column id="col3" sortable="true">


														<f:facet name="header">

															<t:outputText value="Value" />

														</f:facet>
														<t:outputText value="#{dataItem.strPolicyNameSec}" />
													</t:column>

													<t:column id="col4" sortable="true">


														<f:facet name="header">

															<t:outputText value="Time Expression" />

														</f:facet>
														<t:outputText value="#{dataItem.strTimeExpression}" />
													</t:column>

													<t:column id="col5" sortable="true">


														<f:facet name="header">

															<t:outputText value="Password Expression" />

														</f:facet>
														<t:outputText value="#{dataItem.strPasswordExpression}" />
													</t:column>
													
													
																	<t:column id="col6" >


														<f:facet name="header">

															<t:outputText value="Default" />

														</f:facet>
														<h:selectBooleanCheckbox value="#{dataItem.default}" />
													</t:column>
													
																					<t:column id="col7" >


														<f:facet name="header">

															<t:outputText value="Multi Login" />

														</f:facet>
														<h:selectBooleanCheckbox value="#{dataItem.multiLogin}" />
													</t:column>
																		<t:column id="col8" >


														<f:facet name="header">

															<t:outputText value="Auto Reset" />

														</f:facet>
														<h:selectBooleanCheckbox value="#{dataItem.autoReset}" />
													</t:column>
													

													<t:column id="col9">

														<f:facet name="header">
															<t:outputText value="Edit" />
														</f:facet>
														<h:commandLink value="Edit"
															action="#{SecPolicy.editDataItem}" />

													</t:column>
												</t:dataTable>
											</div>
											<div class="contentDivFooter">
												<t:dataScroller id="scroller" for="test2" paginator="true"
													fastStep="1" paginatorMaxPages="2" immediate="true"
													styleClass="scroller" paginatorTableClass="paginator"
													renderFacetsIfSinglePage="true"
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
													paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

													<f:facet name="first">

														<t:outputLabel value="First" id="lblF" />

													</f:facet>

													<f:facet name="last">

														<t:outputLabel value="Last" id="lblL" />

													</f:facet>

													<f:facet name="fastforward">

														<t:outputLabel value="FFwd" id="lblFF" />

													</f:facet>

													<f:facet name="fastrewind">

														<t:outputLabel value="FRev" id="lblFR" />

													</f:facet>

												</t:dataScroller>

											</div>


										</h:form>
			</body>
		</f:view>
</html>
