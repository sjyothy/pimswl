
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="javascript" type="text/javascript">
	
	
		
		function submitForm(ads){
					
			document.getElementById('NOLForm:hiddenValueNOLType').value = ads.value;
			document.forms[0].submit();
		}
		
		function   showContractPopup(clStatus)
		{
	     // alert("In show PopUp");
		 var screen_width = 1024;
         var screen_height = 470;

		    
		   window.open('ContractListPopUp.jsf?clStatus='+clStatus,'_blank','width='+(screen_width-220)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
		    
		}
	
		function populateContract(contractNumber,contractId)
		{
     
        document.getElementById("NOLForm:hdnContractId").value=contractId;
	    document.getElementById("NOLForm:hdnContactNumber").value=contractNumber;
	    
     }
    
     
     function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		    
		  //  alert("in populatePerson");
		    
		    document.getElementById("NOLForm:hdnPersonId").value=personId;
		    document.getElementById("NOLForm:hdnCellNo").value=cellNumber;
		    document.getElementById("NOLForm:hdnPersonName").value=personName;
		    document.getElementById("NOLForm:hdnPersonType").value=hdnPersonType;
		    document.getElementById("NOLForm:hdnIsCompany").value=isCompany;
			
	       document.forms[0].submit();
	}
     
     
		
	  function	openPopUp()
		{
		
		    var width = 300;
            var height = 300;
            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + ",menubar=yes,toolbar=yes,scrollbars=yes,resizable=yes,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;

            var child1 = window.open("about:blank", "subWind", windowFeatures);

		      child1.focus();
		}

		
		function   showPersonPopup()
		{
		   var screen_width = 1024;
		   var screen_height = 470;
		   window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
						    
		}
	</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />


	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table_ff}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
		</head>




		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['contract.nolScreenTitle']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<!--Use this below only for desiging forms Please do not touch other sections -->


									<td width="100%" height="100%" valign="top" nowrap="nowrap">

										<h:form id="NOLForm" enctype="multipart/form-data">
											<div class="SCROLLABLE_SECTION">

												<table border="0" class="layoutTable"
													style="margin-left: 15px; margin-right: 15px;">
													<tr>
														<td>
															<h:outputText id="successMsg_2"
																value="#{pages$issueNOL.successMessages}" escape="false"
																styleClass="INFO_FONT" />
															<h:outputText id="errorMsg_1"
																value="#{pages$issueNOL.errorMessages}" escape="false"
																styleClass="ERROR_FONT" />
															<h:inputHidden id="hdnRequestId"
																value="#{pages$issueNOL.requestId}"></h:inputHidden>
															<h:inputHidden id="hdnContractId"
																value="#{pages$issueNOL.hdnContractId}"></h:inputHidden>
															<h:inputHidden id="hdnContactNumber"
																value="#{pages$issueNOL.hdnContractNumber}"></h:inputHidden>
															<h:inputHidden id="hdnTenantId"
																value="#{pages$issueNOL.hdnTenantId}"></h:inputHidden>
															<h:inputHidden id="hdnPersonId"
																value="#{pages$issueNOL.hdnPersonId}"></h:inputHidden>
															<h:inputHidden id="hdnPersonType"
																value="#{pages$issueNOL.hdnPersonType}"></h:inputHidden>
															<h:inputHidden id="hdnPersonName"
																value="#{pages$issueNOL.hdnPersonName}"></h:inputHidden>
															<h:inputHidden id="hdnCellNo"
																value="#{pages$issueNOL.hdnCellNo}"></h:inputHidden>
															<h:inputHidden id="hdnIsCompany"
																value="#{pages$issueNOL.hdnIsCompany}"></h:inputHidden>
															<h:inputHidden id="hdnSendStatus"
																value="#{pages$issueNOL.hdnSendStatus}"></h:inputHidden>

														</td>
													</tr>

												</table>

												<table>
													<tr>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['maintenanceRequest.requestPriority']}:" />
														</td>
														<td>
															<h:selectOneMenu style="width:193px"
																id="RequestPriorityMenu"
																binding="#{pages$issueNOL.cmbRequestPriority}"
																valueChangeListener="#{pages$issueNOL.onRequestPriorityChanged}"
																onchange="submit();">
																<f:selectItems
																	value="#{pages$ApplicationBean.maintenancePriorityList}" />

															</h:selectOneMenu>
														</td>
													</tr>
												</table>

												<t:panelGrid id="tbl_Action" width="95%" border="0"
													columns="1" binding="#{pages$issueNOL.tbl_Action}">
													<t:panelGroup>
														<h:outputText style="width:10%" id="Label_Remarks"
															styleClass="LABEL"
															value="#{msg['tenderManagement.openTender.sessionRemarks']}" />
														<t:inputTextarea id="txt_remarks" cols="100"
															value="#{pages$issueNOL.txtRemarks}" />
													</t:panelGroup>
												</t:panelGrid>

												<div class="MARGIN" style="width: 98%" border="0">



													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="TAB_PANEL_INNER">
														<rich:tabPanel id="issueNOLTabPanel" headerSpacing="0"
															binding="#{pages$issueNOL.richTabPanel}"
															style="HEIGHT: 330px;width: 100%">

															<rich:tab id="applicationTab"
																label="#{msg['commons.tab.applicationDetails']}">
																<%@ include file="ApplicationDetails.jsp"%>
															</rich:tab>


															<rich:tab id="contractTab"
																label="#{msg['contract.Contract.Details']}">
																<t:div id="divContract" styleClass="TAB_DETAIL_SECTION">
																	<t:panelGrid cellpadding="1px" width="100%"
																		cellspacing="3px"
																		styleClass="TAB_DETAIL_SECTION_INNER" columns="4">

																		<t:panelGroup>
																			<t:panelGrid columns="2" cellpadding="1"
																				cellspacing="5">

																				<h:outputText styleClass="LABEL"
																					value="#{msg['contract.contractNumber']}" />
																				<t:panelGroup>

																					<h:inputText styleClass="READONLY" readonly="true"
																						binding="#{pages$issueNOL.contractNoText}"
																						style="width: 186px; height: 18px"></h:inputText>
																					<h:commandLink
																						binding="#{pages$issueNOL.populateContract}"
																						onclick="javascript:showContractPopup('#{pages$issueNOL.clStatus}')">
																						<h:graphicImage id="populateIcon"
																							style="padding-left:5px;"
																							title="#{msg['commons.populate']}"
																							url="../resources/images/magnifier.gif" />
																					</h:commandLink>
																					<h:commandLink
																						action="#{pages$issueNOL.btnContract_Click}">
																						<h:graphicImage style="padding-left:4px;"
																							title="#{msg['commons.view']}"
																							url="../resources/images/app_icons/Lease-contract.png" />
																					</h:commandLink>

																				</t:panelGroup>


																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.tenantName']}:" />

																				<t:panelGroup>
																					<h:inputText styleClass="READONLY" readonly="true"
																						binding="#{pages$issueNOL.tenantNameText}"
																						style="width: 186px; height: 18px"></h:inputText>
																					<h:commandLink
																						action="#{pages$issueNOL.btnTenant_Click}">
																						<h:graphicImage style="padding-left:4px;"
																							title="#{msg['commons.view']}"
																							url="../resources/images/app_icons/Tenant.png" />
																					</h:commandLink>
																				</t:panelGroup>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.startDate']}:" />
																				<h:inputText styleClass="READONLY" readonly="true"
																					binding="#{pages$issueNOL.contractStartDateText}"
																					style="width: 186px; height: 18px"></h:inputText>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['cancelContract.tab.unit.propertyname']}:" />
																				<h:inputText styleClass="READONLY" readonly="true"
																					binding="#{pages$issueNOL.txtpropertyName}"
																					style="width: 186px; height: 18px"></h:inputText>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.unitNumber']}:" />
																				<h:inputText styleClass="READONLY" readonly="true"
																					binding="#{pages$issueNOL.txtunitRefNum}"
																					style="width: 186px; height: 18px"></h:inputText>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.rentValue']}:" />
																				<h:inputText styleClass="READONLY" readonly="true"
																					binding="#{pages$issueNOL.txtUnitRentValue}"
																					style="width: 186px; height: 18px"></h:inputText>

																			</t:panelGrid>
																		</t:panelGroup>


																		<t:div
																			style="padding-bottom:21px;#padding-bottom:25px;">
																			<t:panelGroup>
																				<t:panelGrid columns="2" cellpadding="1"
																					cellspacing="5">

																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['contract.contractType']}:" />
																					<h:inputText styleClass="READONLY" readonly="true"
																						binding="#{pages$issueNOL.contractTypeText}"
																						style="width: 186px; height: 18px"></h:inputText>

																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['transferContract.oldTenant.Number']}:" />
																					<h:inputText styleClass="READONLY"
																						style="width: 186px; height: 18px" readonly="true"
																						binding="#{pages$issueNOL.tenantNumberType}"></h:inputText>

																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['contract.endDate']}:" />
																					<h:inputText styleClass="READONLY" readonly="true"
																						binding="#{pages$issueNOL.contractEndDateText}"
																						style="width: 186px; height: 18px"></h:inputText>

																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['property.type']}:" />
																					<h:inputText styleClass="READONLY" readonly="true"
																						binding="#{pages$issueNOL.txtpropertyType}"
																						style="width: 186px; height: 18px"></h:inputText>

																					<h:outputLabel styleClass="LABEL"
																						value="#{msg['commons.replaceCheque.unitType']}:" />
																					<h:inputText styleClass="READONLY" readonly="true"
																						binding="#{pages$issueNOL.txtUnitType}"
																						style="width: 186px; height: 18px"></h:inputText>


																				</t:panelGrid>
																			</t:panelGroup>
																		</t:div>
																	</t:panelGrid>
																</t:div>
															</rich:tab>


															<rich:tab id="NOLTab"
																label="#{msg['contract.nolTabHeader']}">
																<t:div id="divNOLTab" styleClass="TAB_DETAIL_SECTION">
																	<t:panelGrid id="NOLDetailTab" cellpadding="1px"
																		width="100%" cellspacing="5px"
																		styleClass="TAB_DETAIL_SECTION_INNER" columns="4">
																		<h:panelGrid columns="2">
																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputText styleClass="LABEL"
																					value="#{msg['contract.nolType']}:" />
																			</h:panelGroup>
																			<h:selectOneMenu id="nolTypeSelectOneMenu"
																				styleClass="#{pages$issueNOL.nolTabReadOnlyMode}"
																				style="width: 200px;"
																				value="#{pages$issueNOL.strNolTypeId}"
																				binding="#{pages$issueNOL.nolTypeSelectOneMenu}"
																				valueChangeListener="#{pages$issueNOL.onNolTypeChanged}"
																				onchange="submit();">
																				<f:selectItem
																					itemLabel="#{msg['commons.pleaseSelect']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$issueNOL.nolTypeItems}" />
																			</h:selectOneMenu>
																			<h:panelGroup>
																				<h:outputLabel styleClass="mandatory" value="*" />
																				<h:outputText styleClass="LABEL"
																					value="#{msg['contract.nolReason']}:" />
																			</h:panelGroup>
																			<h:inputTextarea
																				styleClass="#{pages$issueNOL.nolTabReadOnlyMode}"
																				binding="#{pages$issueNOL.reasontForNol}"
																				style="width:198px;height:100px;"></h:inputTextarea>
																		</h:panelGrid>
																	</t:panelGrid>
																</t:div>
															</rich:tab>
															<%--tabCommercialActivity --%>
															<rich:tab id="tabCommercialActivity"
																binding="#{pages$issueNOL.tabCommercialActivity}"
																action="#{pages$issueNOL.tabCommercialActivity_Click}"
																title="#{msg['contract.tab.CommercialActivity']}"
																label="#{msg['contract.tab.CommercialActivity']}">


																<t:div styleClass="contentDiv" style="align:center;">
																	<t:dataTable id="commercialActivityDataTable" rows="15"
																		width="100%"
																		value="#{pages$issueNOL.commercialActivityList}"
																		binding="#{pages$issueNOL.commercialActivityDataTable}"
																		preserveDataModel="false" preserveSort="false"
																		var="commercialActivityItem" rowClasses="row1,row2"
																		rules="all" renderedIfEmpty="true">

																		<t:column id="colActivityName">


																			<f:facet name="header">

																				<t:outputText
																					value="#{msg['contract.tab.CommercialActivity.Name']}" />

																			</f:facet>
																			<t:outputText styleClass="A_LEFT"
																				value="#{commercialActivityItem.commercialActivityName}" />
																		</t:column>
																		<t:column id="colActivityDescEn"
																			rendered="#{pages$issueNOL.isEnglishLocale}">


																			<f:facet name="header">

																				<t:outputText
																					value="#{msg['contract.tab.CommercialActivity.description']}" />

																			</f:facet>
																			<t:outputText title="" styleClass="A_LEFT"
																				value="#{commercialActivityItem.commercialActivityDescEn}" />
																		</t:column>
																		<t:column id="colActivityDescAr"
																			rendered="#{pages$issueNOL.isArabicLocale}">


																			<f:facet name="header">

																				<t:outputText
																					value="#{msg['contract.tab.CommercialActivity.description']}" />

																			</f:facet>
																			<t:outputText title="" styleClass="A_LEFT"
																				value="#{commercialActivityItem.commercialActivityDescAr}" />
																		</t:column>
																		<t:column id="colActivitySelected">


																			<f:facet name="header">
																				<t:outputText value="#{msg['commons.select']}" />
																			</f:facet>
																			<t:selectBooleanCheckbox
																				binding="#{pages$issueNOL.chkCommercial}"
																				value="#{commercialActivityItem.isSelected}" />

																		</t:column>

																	</t:dataTable>
																</t:div>
																<t:div styleClass="contentDivFooter"
																	style="width:99%;align:center;MARGIN:0px 4px 0px 0px;">

																	<t:dataScroller id="scrollerd"
																		for="commercialActivityDataTable" paginator="true"
																		fastStep="1" paginatorMaxPages="2" immediate="false"
																		paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		pageIndexVar="pageNumber"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false"
																		styleClass="SCH_SCROLLER">
																		<f:facet name="first">

																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblFComAct"></t:graphicImage>
																		</f:facet>

																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFRComAct"></t:graphicImage>
																		</f:facet>

																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFFComAct"></t:graphicImage>
																		</f:facet>

																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblLComAct"></t:graphicImage>
																		</f:facet>

																	</t:dataScroller>

																</t:div>

															</rich:tab>
															<%-- --%>
															<rich:tab id="PaymentTab"
																label="#{msg['cancelContract.tab.payments']}">
																<t:div id="divtt" styleClass="contentDiv"
																	style="width:100%">
																	<t:dataTable id="ttbb" width="100%"
																		value="#{pages$issueNOL.paymentSchedules}"
																		binding="#{pages$issueNOL.dataTablePaymentSchedule}"
																		preserveDataModel="false" preserveSort="false"
																		var="paymentScheduleDataItem" rowClasses="row1,row2"
																		renderedIfEmpty="true">

																		<t:column id="col_PaymentNumber">
																			<f:facet name="header">
																				<t:outputText id="ac20"
																					value="#{msg['paymentSchedule.paymentNumber']}" />
																			</f:facet>
																			<t:outputText id="ac19" styleClass="A_LEFT"
																				value="#{paymentScheduleDataItem.paymentNumber}" />
																		</t:column>
																		<t:column id="col_ReceiptNumber">
																			<f:facet name="header">
																				<t:outputText
																					value="#{msg['paymentSchedule.ReceiptNumber']}" />
																			</f:facet>
																			<t:outputText styleClass="A_LEFT" title=""
																				value="#{paymentScheduleDataItem.paymentReceiptNumber}" />
																		</t:column>

																		<t:column id="colTypeEnt"
																			rendered="#{pages$CLRequest.isEnglishLocale}">
																			<f:facet name="header">
																				<t:outputText id="ac17"
																					value="#{msg['paymentSchedule.paymentType']}" />

																			</f:facet>
																			<t:outputText id="ac16" styleClass="A_LEFT" title=""
																				value="#{paymentScheduleDataItem.typeEn}" />
																		</t:column>
																		<t:column id="colTypeArt"
																			rendered="#{pages$issueNOL.isArabicLocale}">
																			<f:facet name="header">
																				<t:outputText id="ac15"
																					value="#{msg['paymentSchedule.paymentType']}" />
																			</f:facet>
																			<t:outputText id="ac14" styleClass="A_LEFT" title=""
																				value="#{paymentScheduleDataItem.typeAr}" />
																		</t:column>
																		<t:column id="colDesc" style="width:25%;">
																			<f:facet name="header">
																				<t:outputText id="descHead"
																					value="#{msg['renewContract.description']}" />
																			</f:facet>
																			<t:outputText id="descText" styleClass="A_LEFT"
																				title=""
																				value="#{paymentScheduleDataItem.description}" />
																		</t:column>
																		<t:column id="colDueOnt">
																			<f:facet name="header">
																				<t:outputText id="ac13"
																					value="#{msg['paymentSchedule.paymentDueOn']}" />
																			</f:facet>
																			<t:outputText id="ac12" styleClass="A_LEFT"
																				value="#{paymentScheduleDataItem.paymentDueOn}">
																				<f:convertDateTime
																					timeZone="#{pages$issueNOL.timeZone}"
																					pattern="#{pages$issueNOL.dateFormatForDataTable}" />
																			</t:outputText>
																		</t:column>
																		<t:column id="colStatusEnt"
																			rendered="#{pages$issueNOL.isEnglishLocale}">
																			<f:facet name="header">
																				<t:outputText id="ac11"
																					value="#{msg['commons.status']}" />
																			</f:facet>
																			<t:outputText id="ac10" styleClass="A_LEFT" title=""
																				value="#{paymentScheduleDataItem.statusEn}" />
																		</t:column>
																		<t:column id="colStatusArt"
																			rendered="#{pages$issueNOL.isArabicLocale}">
																			<f:facet name="header">
																				<t:outputText id="ac9"
																					value="#{msg['commons.status']}" />
																			</f:facet>
																			<t:outputText id="ac8" styleClass="A_LEFT" title=""
																				value="#{paymentScheduleDataItem.statusAr}" />
																		</t:column>
																		<t:column id="colModeEnt"
																			rendered="#{pages$issueNOL.isEnglishLocale}">
																			<f:facet name="header">
																				<t:outputText id="ac7"
																					value="#{msg['paymentSchedule.paymentMode']}" />
																			</f:facet>
																			<t:outputText id="ac6" styleClass="A_LEFT" title=""
																				value="#{paymentScheduleDataItem.paymentModeEn}" />
																		</t:column>
																		<t:column id="colModeArt"
																			rendered="#{pages$issueNOL.isArabicLocale}">
																			<f:facet name="header">
																				<t:outputText id="ac5"
																					value="#{msg['paymentSchedule.paymentMode']}" />
																			</f:facet>
																			<t:outputText id="ac4" styleClass="A_LEFT" title=""
																				value="#{paymentScheduleDataItem.paymentModeAr}" />
																		</t:column>
																		<t:column id="colAmountt">
																			<f:facet name="header">
																				<t:outputText id="ac1"
																					value="#{msg['paymentSchedule.amount']}" />
																			</f:facet>
																			<t:outputText id="ac2" styleClass="A_RIGHT_NUM"
																				value="#{paymentScheduleDataItem.amount}" />
																		</t:column>
																		<t:column id="DelPaymentCol">
																			<f:facet name="header">
																				<t:outputText id="ac3"
																					value="#{msg['commons.action']}" />
																			</f:facet>
																			<h:commandLink
																				action="#{pages$issueNOL.btnEditPaymentSchedule_Click}"
																				rendered="#{pages$issueNOL.editPaymentEnable}">
																				<h:graphicImage id="editPayments"
																					title="#{msg['commons.edit']}"
																					url="../resources/images/edit-icon.gif"
																					rendered="#{paymentScheduleDataItem.isReceived == 'N'}" />
																			</h:commandLink>
																			<h:commandLink
																				action="#{pages$issueNOL.btnPrintPaymentSchedule_Click}">
																				<h:graphicImage id="printPayments"
																					title="#{msg['commons.print']}"
																					url="../resources/images/app_icons/print.gif"
																					rendered="#{paymentScheduleDataItem.isReceived == 'Y'}" />
																			</h:commandLink>

																			<h:commandLink
																				action="#{pages$issueNOL.btnViewPaymentDetails_Click}">
																				<h:graphicImage id="viewPayments"
																					title="#{msg['commons.group.permissions.view']}"
																					url="../resources/images/detail-icon.gif" />
																			</h:commandLink>


																		</t:column>
																	</t:dataTable>
																</t:div>
																<t:div>
																	<t:div id="cmdDiv" styleClass="A_RIGHT"
																		style="padding:10px">
																		<h:commandButton id="cmdCollect"
																			value="#{msg['settlement.actions.collectpayment']}"
																			styleClass="BUTTON"
																			binding="#{pages$issueNOL.btnCollectPayment}"
																			action="#{pages$issueNOL.openReceivePaymentsPopUp}"
																			style="width: 119px" />
																	</t:div>
																</t:div>
															</rich:tab>


															<rich:tab id="requestHistoryTab"
																label="#{msg['commons.tab.requestHistory']}"
																action="#{pages$issueNOL.tabRequestHistory_Click}">
																<t:div>
																	<%@ include file="../pages/requestTasks.jsp"%>
																</t:div>
															</rich:tab>

															<rich:tab id="attachmentTab"
																label="#{msg['commons.attachmentTabHeading']}">
																<%@  include file="attachment/attachment.jsp"%>
															</rich:tab>

															<rich:tab id="commentsTab"
																label="#{msg['commons.commentsTabHeading']}">
																<%@ include file="notes/notes.jsp"%>
															</rich:tab>


														</rich:tabPanel>

													</div>
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																	class="TAB_PANEL_MID" style="width: 100%;" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="BUTTON_TD MARGIN" colspan="6">

																<h:commandButton
																	value="#{msg['AuctionDepositRefundApproval.Buttons.Save']}"
																	styleClass="BUTTON"
																	binding="#{pages$issueNOL.saveButton}"
																	action="#{pages$issueNOL.onSave}">
																</h:commandButton>
																<pims:security
																	screen="Pims.LeaseContractManagement.issueNOL.print"
																	action="create">
																	<h:commandButton styleClass="BUTTON"
																		binding="#{pages$issueNOL.printButton}"
																		action="#{pages$issueNOL.printReceipt}"
																		onclick="return confirm('#{msg['noObjectionLetter.message.confirmComplete']}');">

																	</h:commandButton>
																</pims:security>
																<pims:security
																	screen="Pims.LeaseContractManagement.issueNOL.sendForApproval"
																	action="create">
																	<h:commandButton styleClass="BUTTON"
																		style="width:auto;"
																		binding="#{pages$issueNOL.sendForApprovalButton}"
																		action="#{pages$issueNOL.sendForApproval}"
																		onclick="return confirm('#{msg['noObjectionLetter.message.confirmTechnical']}');"></h:commandButton>
																</pims:security>
																<pims:security
																	screen="Pims.LeaseContractManagement.issueNOL.approve"
																	action="create">
																	<h:commandButton styleClass="BUTTON"
																		binding="#{pages$issueNOL.approveButton}"
																		action="#{pages$issueNOL.Approved}"
																		onclick="return confirm('#{msg['noObjectionLetter.message.confirmApprove']}');"></h:commandButton>
																</pims:security>
																<pims:security
																	screen="Pims.LeaseContractManagement.issueNOL.reject"
																	action="create">
																	<h:commandButton styleClass="BUTTON"
																		binding="#{pages$issueNOL.rejectButton}"
																		action="#{pages$issueNOL.reject}"
																		onclick="return confirm('#{msg['noObjectionLetter.message.confirmReject']}');"></h:commandButton>
																</pims:security>
																<pims:security
																	screen="Pims.LeaseContractManagement.issueNOL.send"
																	action="create">
																	<h:commandButton styleClass="BUTTON"
																		style="width:auto;"
																		binding="#{pages$issueNOL.sendButton}"
																		action="#{pages$issueNOL.initiateFlowWithTechComments}"
																		onclick="return confirm('#{msg['noObjectionLetter.message.confirmSend']}');"></h:commandButton>
																</pims:security>
																<h:commandButton styleClass="BUTTON" style="width:auto;"
																	binding="#{pages$issueNOL.sendButtonWithoutTechComments}"
																	action="#{pages$issueNOL.initiateFlowWithoutTechComments}"
																	onclick="return confirm('#{msg['noObjectionLetter.message.confirmSendApproval']}');"></h:commandButton>

																<h:commandButton styleClass="BUTTON" style="width:auto;"
																	binding="#{pages$issueNOL.completeRequest}"
																	action="#{pages$issueNOL.completed}"
																	onclick="return confirm('#{msg['noObjectionLetter.message.confirmComplete']}');"></h:commandButton>

																<h:commandButton styleClass="BUTTON" style="width:auto;"
																	binding="#{pages$issueNOL.cancelRequest}"
																	action="#{pages$issueNOL.canceled}"
																	onclick="return confirm('#{msg['noObjectionLetter.message.confirmCancel']}');"></h:commandButton>

																<h:commandButton styleClass="BUTTON" style="width:auto;"
																	binding="#{pages$issueNOL.printNol}"
																	value="#{msg['commons.btn.printNol']}"
																	action="#{pages$issueNOL.printNolReport}">
																</h:commandButton>

																<h:commandButton styleClass="BUTTON"
																	binding="#{pages$issueNOL.cancelButton}"
																	action="#{pages$issueNOL.cancel}"></h:commandButton>

															</td>
														</tr>
													</table>

												</div>


											</div>
										</h:form>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td class="footer">
													<h:outputLabel value="#{msg['commons.footer.message']}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>


						</td>
					</tr>
				</table>
			</div>
		</body>

	</html>
</f:view>
