<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
function managerAmafClicked(control){
	if(control.checked){
	 document.getElementById("inheritanceFileForm:searchManagerImageId").style.visibility = 'hidden';
	 document.getElementById("inheritanceFileForm:managerNameId").value = '';
	}
	else{
	document.getElementById("inheritanceFileForm:searchManagerImageId").style.visibility = 'visible';
	document.getElementById("inheritanceFileForm:managerNameId").value = '';
	}
}

function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
       document.getElementById("inheritanceFileForm:hdnManagerId").value=personId;
       document.forms[0].submit();
	}
	function   showManagerSearchPopup()
	{
  var screen_width = 1024;
   var screen_height = 470;
   var screen_top = screen.top;
   document.getElementById("inheritanceFileForm:isManagerAmafId").value = false;
     window.open('SearchPerson.jsf?viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');	    
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">

					<c:choose>
						<c:when test="${!pages$ManageAssets.isViewModePopUp}">
							<tr
								style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
								<td colspan="2">
									<jsp:include page="header.jsp" />
								</td>
							</tr>
						</c:when>
					</c:choose>
					<tr width="100%">


						<c:choose>
							<c:when test="${!pages$ManageAssets.isViewModePopUp}">
								<td class="divLeftMenuWithBody" width="17%">
									<jsp:include page="leftmenu.jsp" />
								</td>
							</c:when>
						</c:choose>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['manageAsset.header']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>

							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<h:outputText value="#{pages$ManageAssets.errorMessages}"
											escape="false" styleClass="ERROR_FONT" />
										<h:outputText value="#{pages$ManageAssets.successMessages}"
											escape="false" styleClass="INFO_FONT" />
										<h:messages></h:messages>
										<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="inheritanceFileForm"
												enctype="multipart/form-data" style="WIDTH: 97.6%;">
												<h:inputHidden id="hdnManagerId"
													value="#{pages$ManageAssets.hdnManagerId}" />

												<div class="AUC_DET_PAD">

													<div class="TAB_PANEL_INNER">
														<rich:tabPanel switchType="server"
															style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%">

															<rich:tab
																label="#{msg['mems.inheritanceFile.tabName.basicInfo']}"
																style="height:100px;"
																title="#{msg['mems.inheritanceFile.tabToolTip.basicInfo']}">
																<%@  include file="ManageAssetBasicInfoTab.jsp"%>
															</rich:tab>
															<rich:tab label="#{msg['inheritanceFile.tabHeader']}">
																<%@  include file="InheritanceFileTab.jsp"%>
															</rich:tab>
															<rich:tab
																label="#{msg['PeriodicDisbursements.columns.beneficiary']}">
																<%@  include file="BeneficiaryTab.jsp"%>
															</rich:tab>
															<rich:tab style="min-height:10px;"
																label="#{msg['commons.report.name.collectionReport.jsp']}">
																<t:div
																	style="min-height:10px;overflow-y:scroll;overflow-x:hidden;width:99%;">
																	<%@  include file="CollectionsTab.jsp"%>
																</t:div>
															</rich:tab>
															<rich:tab label="#{msg['thirdPartyUnit.tabTitle']}">
																<%@  include file="thirdPartyPropUnitsTab.jsp"%>
															</rich:tab>
															<rich:tab
																label="#{msg['revenueFollowup.lbl.revenueFollowup']}"
																action="#{pages$ManageAssets.onRevenueFollowupTab}">
																<%@  include file="tabAssetRevenueFollowup.jsp"%>
															</rich:tab>

															<rich:tab
																label="#{msg['mems.inheritanceFile.tabHeadingShort.history']}"
																action="#{pages$ManageAssets.requestHistoryTabClick}">
																<%@ include file="../pages/requestTasks.jsp"%>
															</rich:tab>
														</rich:tabPanel>

													</div>

													<t:div styleClass="BUTTON_TD"
														style="padding-top:10px; padding-right:21px;">
														<h:commandButton styleClass="BUTTON"
															value="#{msg['commons.saveButton']}" style="width: auto"
															action="#{pages$ManageAssets.saveManager}">
														</h:commandButton>
														<h:commandButton styleClass="BUTTON"
															value="#{msg['bouncedChequesList.close']}"
															style="width: auto" onclick="javascript:window.close();">
														</h:commandButton>

													</t:div>
												</div>
										</div>

										</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>


					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">

								<c:choose>
									<c:when test="${!pages$ManageAssets.isViewModePopUp}">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</c:when>
								</c:choose>

							</table>
						</td>
					</tr>
				</table>
			</div>
			+
		</body>
	</html>
</f:view>