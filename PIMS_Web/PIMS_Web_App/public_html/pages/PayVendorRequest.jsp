<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

function onChkChangeStart(  )
		{
		    
		    
	        document.getElementById('detailsFrm:onClickMarkUnMark').onclick();
			
			
		}

	function disableButtons(control)
	{
		var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
		
		
		if (typeof control.nextSibling.nextSibling.id !== "undefined" && control.nextSibling.nextSibling.id.indexOf("lnk") != -1 )
		{
		
			control.nextSibling.nextSibling.onclick();
		}
		else if (typeof control.nextSibling.nextSibling.nextSibling.id !== "undefined" &&  control.nextSibling.nextSibling.nextSibling.id.indexOf("lnk") != -1 )
		{
		
			control.nextSibling.nextSibling.nextSibling.onclick();
		}
	}
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{		    			 
		if( hdnPersonType=='APPLICANT' ) {			    			 
			 document.getElementById("detailsFrm:hdnPersonId").value=personId;
			 document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
			 document.getElementById("detailsFrm:hdnPersonName").value=personName;
			 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
			 document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;
		} else {
			document.getElementById("detailsFrm:hdnVendorId").value=personId;		
			document.getElementById("detailsFrm:hdnVendorName").value=personName;		
		}		
	     document.forms[0].submit();
	} 
	function populatePaymentDetails()
	{	
	  	document.getElementById("detailsFrm:populatePayments").onclick();	  	 
	}
	function showVendorPopup()
	{	
   		var screen_width = 1024;
   		var screen_height = 470;
   		var screen_top = screen.top;
     	window.open('SearchPerson.jsf?isCompany=true&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');         		  
	}
	
	function openPayVendorNotificationPopup( )
	{
	
		var screen_width = screen.width;
        var screen_height = screen.height;
        var popup_width = screen_width-600;
        var popup_height = 500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
        var popup 	= window.open(
        							'PayVendorNotificationPopup.jsf?',
        						 	'_blank',
        						 	'width='+popup_width  +
        						 	',height='+popup_height+
        						 	',left='+ leftPos +
        						 	',top=' + topPos + 
        						 	',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes'
        						 );
        popup.focus();
	
	}
	
								
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['mems.payvendor.label.title']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>

																<h:outputText
																	value="#{pages$PayVendorRequest.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText
																	value="#{pages$PayVendorRequest.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="hdnRequestId"
																	value="#{pages$PayVendorRequest.hdnRequestId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$PayVendorRequest.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$PayVendorRequest.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$PayVendorRequest.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$PayVendorRequest.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$PayVendorRequest.hdnIsCompany}"></h:inputHidden>
																<h:inputHidden id="hdnVendorId"
																	value="#{pages$PayVendorRequest.hdnVendorId}"></h:inputHidden>
																<h:inputHidden id="hdnVendorName"
																	value="#{pages$PayVendorRequest.hdnVendorName}"></h:inputHidden>

																<h:commandLink id="onClickMarkUnMark"
																	action="#{pages$PayVendorRequest.onMarkUnMarkChanged}" />
															</td>
														</tr>
													</table>
													<%-- 
													<table cellpadding="0" cellspacing="6px" width="100%">
														<tr>
															<td class="BUTTON_TD MARGIN" colspan="6">

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['mems.payvendor.label.request']}"
																	action="#{pages$PayVendorRequest.showRequestPopup}">
																</h:commandButton>
															</td>
														</tr>
													</table>--%>
													<t:panelGrid id="applicationDetailsTable1"
														cellpadding="5px" width="100%" cellspacing="10px"
														columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
														columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">


														<h:outputLabel
															value="#{msg['mems.payvendor.label.vendor']}"></h:outputLabel>
														<h:panelGroup>
															<h:inputText id="inVendor" styleClass="READONLY"
																readonly="true"
																binding="#{pages$PayVendorRequest.txtVendor}"
																maxlength="20" />
															<h:graphicImage id="imgVendor"
																title="#{msg['commons.search']}"
																binding="#{pages$PayVendorRequest.imgVendor}"
																onclick="showVendorPopup();"
																style="MARGIN: 0px 0px -4px; cursor:hand;horizontal-align:right;"
																url="../resources/images/app_icons/Search-tenant.png">
															</h:graphicImage>
														</h:panelGroup>
														<h:outputLabel value="" />
														<h:outputLabel value="" />
														<h:outputLabel styleClass="LABEL"
															rendered="#{pages$PayVendorRequest.isApprovalRequired ||
																		pages$PayVendorRequest.isFinanceRejected || 
																		pages$PayVendorRequest.isReviewed 
																		}"
															value="#{msg['mems.investment.label.sendTo']}:" />
														<t:panelGroup
															rendered="#{pages$PayVendorRequest.isApprovalRequired ||
																		pages$PayVendorRequest.isFinanceRejected || 
																		pages$PayVendorRequest.isReviewed 
																		}">
															<h:selectOneMenu
																binding="#{pages$PayVendorRequest.cmbReviewGroup}">
																<f:selectItem itemLabel="#{msg['commons.select']}"
																	itemValue="" />
																<f:selectItems
																	value="#{pages$ApplicationBean.allUserGroups}" />
															</h:selectOneMenu>
														</t:panelGroup>
														<h:outputLabel
															rendered="#{pages$PayVendorRequest.isApprovalRequired ||
																		pages$PayVendorRequest.isFinanceRejected || 
																		pages$PayVendorRequest.isReviewed 
																		}"
															value="" />
														<h:outputLabel
															rendered="#{pages$PayVendorRequest.isApprovalRequired ||
																		pages$PayVendorRequest.isFinanceRejected || 
																		pages$PayVendorRequest.isReviewed 
																		}"
															value="" />
														<h:outputLabel styleClass="LABEL"
															rendered="#{
															             pages$PayVendorRequest.isApprovalRequired || 
																		 pages$PayVendorRequest.isFinanceRejected ||
																		 pages$PayVendorRequest.isReviewed ||
																		 pages$PayVendorRequest.isDisbursementRequired
																	   }"
															value="#{msg['commons.comments']}:" />
														<t:panelGroup colspan="3"
															rendered="#{
															             pages$PayVendorRequest.isApprovalRequired ||
																		 pages$PayVendorRequest.isFinanceRejected || 
																		 pages$PayVendorRequest.isReviewed ||
																		 pages$PayVendorRequest.isDisbursementRequired
																	   }">
															<h:inputTextarea
																binding="#{pages$PayVendorRequest.htmlRFC}"
																style="width: 550px;" />
														</t:panelGroup>




													</t:panelGrid>
													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel
																binding="#{pages$PayVendorRequest.richPanel}"
																style="HEIGHT: 350px;#HEIGHT: 300px;width: 100%">

																<rich:tab id="applicationTab"
																	label="#{msg['commons.tab.applicationDetails']}">
																	<%@ include file="ApplicationDetails.jsp"%>
																</rich:tab>

																<rich:tab id="disbursementTab"
																	label="#{msg['mems.normaldisb.label.disbdetail']}">
																	<%@ include file="tabPayVendorDisbursements.jsp"%>
																</rich:tab>

																<rich:tab id="reviewTab"
																	rendered="#{!pages$PayVendorRequest.isNewStatus }"
																	label="#{msg['commons.tab.reviewDetails']}">
																	<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
																</rich:tab>
																<rich:tab id="financeTab"
																	label="#{msg['mems.finance.label.tab']}"
																	rendered="#{pages$PayVendorRequest.isDisbursementRequired}">
																	<%@  include file="tabFinance.jsp"%>
																</rich:tab>

																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>

																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>

																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$PayVendorRequest.getRequestHistory}"
																	rendered="#{!pages$PayVendorRequest.isInvalidStatus}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" />
																</td>
															</tr>
														</table>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">

															<pims:security
																screen="Pims.MinorMgmt.PayVendorRequest.SaveRequest"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else disableButtons(this);"
																	rendered="#{pages$PayVendorRequest.isNewStatus}">
																</h:commandButton>
																<h:commandLink id="lnksave"
																	action="#{pages$PayVendorRequest.save}" />
															</pims:security>

															<pims:security
																screen="Pims.MinorMgmt.PayVendorRequest.SubmitRequest"
																action="view">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSentForDisbursement']}')) return false;else disableButtons(this);"
																	rendered="#{pages$PayVendorRequest.isNewStatus}">
																</h:commandButton>
																<h:commandLink id="lnksend"
																	action="#{pages$PayVendorRequest.submitQuery}" />
															</pims:security>

															<h:commandButton
																rendered="#{
																				pages$PayVendorRequest.isTaskPresent &&
																				 (
																				  pages$PayVendorRequest.isApprovalRequired ||
																				  pages$PayVendorRequest.isReviewed ||
																				  pages$PayVendorRequest.isFinanceRejected
																				 )
																		   }"
																styleClass="BUTTON"
																onclick="if (!confirm('#{msg['mems.common.alertOperation']}')) return false;else disableButtons(this);"
																value="#{msg['commons.approve']}" />
															<h:commandLink id="lnkApprove"
																action="#{pages$PayVendorRequest.onApproved}" />

															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.reject']}"
																rendered="#{
																				 pages$PayVendorRequest.isTaskPresent &&
																				 (
																				  pages$PayVendorRequest.isApprovalRequired ||
																				  pages$PayVendorRequest.isReviewed ||
																				  pages$PayVendorRequest.isFinanceRejected
																				 )
																		   }"
																onclick="if (!confirm('#{msg['mems.common.alertOperation']}')) return false;else disableButtons(this);">
															</h:commandButton>
															<h:commandLink id="lnkReject"
																action="#{pages$PayVendorRequest.onReject}" />

															<h:commandButton
																rendered="#{
																				 pages$PayVendorRequest.isTaskPresent &&
																				 (
																				  pages$PayVendorRequest.isApprovalRequired ||
																				  pages$PayVendorRequest.isReviewed ||
																				  pages$PayVendorRequest.isFinanceRejected
																				 )
																		   }"
																styleClass="BUTTON"
																onclick="if (!confirm('#{msg['mems.common.alertComplete']}')) return false;else disableButtons(this);"
																value="#{msg['commons.review']}" />
															<h:commandLink id="lnkReview"
																action="#{pages$PayVendorRequest.review}" />


															<h:commandButton
																rendered="#{
 																				 pages$PayVendorRequest.isTaskPresent &&
																				 (
																				  pages$PayVendorRequest.isApprovalRequired ||
																				  pages$PayVendorRequest.isReviewed ||
																				  pages$PayVendorRequest.isFinanceRejected
																				 )
																		   }"
																styleClass="BUTTON" onclick="disableButtons(this);"
																value="#{msg['applicationDetails.email']}" />
															<h:commandLink id="lnkOpenPayVendorNotificationPopup"
																action="#{pages$PayVendorRequest.onOpenPayVendorNotificationPopup}" />

															<h:commandButton
																rendered="#{	 pages$PayVendorRequest.isTaskPresent && 
																				pages$PayVendorRequest.isReviewRequired
																			}"
																styleClass="BUTTON"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else disableButtons(this);"
																value="#{msg['commons.done']}">
															</h:commandButton>
															<h:commandLink id="lnkreviewDone"
																action="#{pages$PayVendorRequest.reviewDone}" />
															<pims:security
																screen="Pims.MinorMgmt.PayVendorRequest.CompleteRequest"
																action="view">
																<h:commandButton
																	rendered="#{
																				 pages$PayVendorRequest.isDisbursementRequired &&
																				 pages$PayVendorRequest.isTaskPresent
																				}"
																	styleClass="BUTTON"
																	onclick="if (!confirm('#{msg['mems.common.alertComplete']}')) return false;else disableButtons(this);"
																	value="#{msg['commons.complete']}">

																</h:commandButton>
																<h:commandLink id="lnkcomplete"
																	action="#{pages$PayVendorRequest.complete}" />


																<h:commandButton
																	rendered="#{
																				 pages$PayVendorRequest.isDisbursementRequired  &&
																				 pages$PayVendorRequest.isTaskPresent
																				}"
																	onclick="if (!confirm('#{msg['mems.common.alertReject']}')) return false;else disableButtons(this); "
																	styleClass="BUTTON" value="#{msg['commons.reject']}">
																</h:commandButton>
																<h:commandLink id="lnkFinanceReject"
																	action="#{pages$PayVendorRequest.rejectFinance}" />
															
															</pims:security>
															
															<h:commandButton styleClass="BUTTON" id="btnPrint"
																value="#{msg['commons.print']}"
																rendered="#{!pages$PayVendorRequest.isNewStatus}"
																onclick="disableButtons(this);">
															</h:commandButton>
															<h:commandLink id="lnkPrint"
																action="#{pages$PayVendorRequest.onPrint}" />


															<h:commandLink id="populatePayments" styleClass="BUTTON"
																style="width: auto;visibility: hidden;"
																actionListener="#{pages$PayVendorRequest.loadPaymentDetails}">
															</h:commandLink>
														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>