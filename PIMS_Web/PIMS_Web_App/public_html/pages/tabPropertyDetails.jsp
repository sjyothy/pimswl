<h:inputHidden binding="#{pages$ReceiveProperty.floorHiddenId}" />
<h:inputHidden binding="#{pages$ReceiveProperty.hiddenCounter}" />
<h:inputHidden binding="#{pages$ReceiveProperty.unitHiddenId}" />
<h:inputHidden binding="#{pages$ReceiveProperty.unitStatusId}" />
<h:inputHidden binding="#{pages$ReceiveProperty.contactInfoId}" />
<t:panelGrid width="100%" columns="4">


	<h:outputLabel
		value="#{msg['receiveProperty.propertyReferenceNumber']}:"></h:outputLabel>

	<h:inputText id="propertyRefNo"
		binding="#{pages$ReceiveProperty.propertyReferenceNumber}"
		styleClass="READONLY"></h:inputText>
	<h:outputLabel value="#{msg['receiveProperty.propertyStatus']}:"></h:outputLabel>

	<h:inputText id="propertyStatus" readonly="true" styleClass="READONLY"
		binding="#{pages$ReceiveProperty.statusText}" />

	<t:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel value="#{msg['receiveProperty.commercialName']}:"></h:outputLabel>
	</t:panelGroup>
	<h:inputText id="commercialName" maxlength="150"
		binding="#{pages$ReceiveProperty.commercialName}" />

	<t:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel value="#{msg['receiveProperty.lbl.nameEn']}:"></h:outputLabel>
	</t:panelGroup>
	<h:inputText id="propertyNameEn" maxlength="150"
		binding="#{pages$ReceiveProperty.propertyNameEn}" />


	<h:outputLabel value="#{msg['receiveProperty.projectNumber']}:"></h:outputLabel>
	<h:panelGroup>
		<h:inputText id="projectNumber" maxlength="50" disabled="true"
			binding="#{pages$ReceiveProperty.projectNumber}" />
		<h:graphicImage url="../resources/images/app_icons/Search-tenant.png"
			binding="#{pages$ReceiveProperty.projectImage}"
			onclick="javaScript:showSearchProjectPopUp();"
			alt="#{msg[commons.searchProject]}">
		</h:graphicImage>
	</h:panelGroup>
	<h:outputLabel value="#{msg['receiveProperty.endowedName']}:"></h:outputLabel>

	<h:inputText id="endowedName" maxlength="150"
		binding="#{pages$ReceiveProperty.endowedName}" />
	<h:outputLabel value="#{msg['receiveProperty.endowedMinorNo']}:"></h:outputLabel>

	<h:inputText id="endowedNo" maxlength="50"
		binding="#{pages$ReceiveProperty.endowedMinorNo}" />
	<t:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel value="#{msg['receiveProperty.ownershipType']}:"></h:outputLabel>
	</t:panelGroup>
	<h:selectOneMenu id="owner"
		binding="#{pages$ReceiveProperty.ownershipType}"
		onchange="javascript:submitForm();"
		valueChangeListener="#{pages$ReceiveProperty.ownershipTypeChanged}">>
																			<f:selectItem itemValue="-1"
			itemLabel="#{msg['commons.combo.PleaseSelect']}" />

		<f:selectItems value="#{pages$ReceiveProperty.ownershipItems}" />
	</h:selectOneMenu>
	<t:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel value="#{msg['receiveProperty.investmentPurpose']}:"></h:outputLabel>
	</t:panelGroup>
	<h:selectOneMenu id="purpose"
		binding="#{pages$ReceiveProperty.proeprtyInvestmentPurpose}">
		<f:selectItem itemValue="-1"
			itemLabel="#{msg['commons.combo.PleaseSelect']}" />

		<f:selectItems value="#{pages$ReceiveProperty.investmentPurposes}" />
	</h:selectOneMenu>

	<t:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel value="#{msg['receiveProperty.propertyUsage']}:"></h:outputLabel>
	</t:panelGroup>
	<h:selectOneMenu id="usage"
		binding="#{pages$ReceiveProperty.propertyUsage}">
		<f:selectItem itemValue="-1"
			itemLabel="#{msg['commons.combo.PleaseSelect']}" />

		<f:selectItems value="#{pages$ReceiveProperty.usageItems}" />
	</h:selectOneMenu>


	<t:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel value="#{msg['receiveProperty.propertyType']}:"></h:outputLabel>
	</t:panelGroup>
	<h:selectOneMenu id="propertyType"
		binding="#{pages$ReceiveProperty.propertyType}">
		<f:selectItem itemValue="-1"
			itemLabel="#{msg['commons.combo.PleaseSelect']}" />

		<f:selectItems value="#{pages$ReceiveProperty.propertyItems}" />
	</h:selectOneMenu>
	<h:outputLabel value="#{msg['receiveProperty.country']}:"></h:outputLabel>
	<h:selectOneMenu id="country"
		value="#{pages$ReceiveProperty.countryId}">
		<f:selectItem itemValue="-1"
			itemLabel="#{msg['commons.combo.PleaseSelect']}" />
		<f:selectItems value="#{pages$ReceiveProperty.countryList}" />
		<a4j:support event="onchange"
			action="#{pages$ReceiveProperty.loadState}"
			reRender="state,selectcity" />
	</h:selectOneMenu>
	<h:outputLabel value="#{msg['receiveProperty.emirate']}:"></h:outputLabel>
	<h:selectOneMenu id="state" required="false"
		value="#{pages$ReceiveProperty.stateId}">
		<f:selectItem itemValue="-1"
			itemLabel="#{msg['commons.combo.PleaseSelect']}" />
		<f:selectItems value="#{pages$ReceiveProperty.stateList}" />
		<a4j:support event="onchange"
			action="#{pages$ReceiveProperty.loadCity}" reRender="selectcity" />
	</h:selectOneMenu>

	<h:outputLabel value="#{msg['receiveProperty.city']}:"></h:outputLabel>
	<h:selectOneMenu id="selectcity"
		binding="#{pages$ReceiveProperty.cityMenu}"
		value="#{pages$ReceiveProperty.cityId}">
		<f:selectItem itemLabel="#{msg['commons.All']}" itemValue="1" />
		<f:selectItems value="#{pages$ReceiveProperty.cityList}" />
	</h:selectOneMenu>
	<t:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel value="#{msg['receiveProperty.landNumber']}:"></h:outputLabel>
	</t:panelGroup>
	<h:inputText id="landNumber" maxlength="50"
		binding="#{pages$ReceiveProperty.landNumber}" />

	<t:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel value="#{msg['receiveProperty.addressLineOne']}:"></h:outputLabel>
	</t:panelGroup>
	<h:inputText id="addressLineOne" maxlength="150"
		binding="#{pages$ReceiveProperty.addressLineOne}" />
	<h:outputLabel value="#{msg['receiveProperty.streetInformation']}:"></h:outputLabel>
	<h:inputText id="streetInfo" maxlength="150"
		binding="#{pages$ReceiveProperty.streetInformation}" />
	<h:outputLabel value="#{msg['receiveProperty.EWUtilityNo']}:"></h:outputLabel>
	<h:inputText id="eqewUtilityNo" maxlength="50"
		binding="#{pages$ReceiveProperty.ewUtilityNo}" />

	<h:outputLabel value="#{msg['receiveProperty.buildingCoordinates']}:"></h:outputLabel>
	<h:inputText id="coordinates"
		binding="#{pages$ReceiveProperty.buildingCoordinates}" maxlength="50" />
	
	<h:outputLabel value="#{msg['receiveProperty.landArea']}:"></h:outputLabel>
	<h:inputText id="landArea" style="text-align: right;"
		binding="#{pages$ReceiveProperty.landArea}" />

	<h:outputLabel value="#{msg['receiveProperty.builtInArea']}:"></h:outputLabel>
	<h:inputText id="builtInAreaNumber" style="text-align: right;"
		binding="#{pages$ReceiveProperty.builtInArea}" />
	
	<h:outputLabel value="#{msg['receiveProperty.totalFloors']}:"></h:outputLabel>
	<h:inputText id="totalNoOfFloors" readonly="true"
		binding="#{pages$ReceiveProperty.totalNoOfFloors}">
	</h:inputText>
	
	<h:outputLabel value="#{msg['receiveProperty.totalUnits']}:"></h:outputLabel>
	<h:inputText id="totalNoOfUnits" readonly="true"
		binding="#{pages$ReceiveProperty.totalNoOfUnits}">
	</h:inputText>
	
	<h:outputLabel
		value="#{msg['inheritanceFile.fieldLabel.oldCostCenter']}:"></h:outputLabel>
	<h:inputText id="oldCostCenter" styleClass="READONLY" readonly="true"
		value="#{pages$ReceiveProperty.oldCC}" />

	<h:outputLabel
		value="#{msg['inheritanceFile.fieldLabel.newCostCenter']}:"></h:outputLabel>
	<h:inputText id="newCostCenter" styleClass="READONLY" readonly="true"
		value="#{pages$ReceiveProperty.newCC}" />


	<h:outputLabel id="lblEndowmentnum"
		value="#{msg['endowment.lbl.num']}:"></h:outputLabel>
	<h:panelGroup id="grpEndowment">
		<h:inputText styleClass="READONLY" readonly="true"
			style="width:187px;"
			binding="#{pages$ReceiveProperty.txtEndowmentRef}" />

		<h:graphicImage alt="#{msg['receiveProperty.tooltip.viewEndowments']}"
			url="../resources/images/app_icons/view_tender.png"
			style="MARGIN: 1px 1px -5px;cursor:hand;"
			onclick="openEndowmentManagePopup('#{pages$ReceiveProperty.endowmentId}')"></h:graphicImage>
	</h:panelGroup>


	<t:panelGroup id="lblAsstetNumber"
		rendered="#{pages$ReceiveProperty.showAssetControls}">
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel value="#{msg['assetManage.assetNumber']}:"></h:outputLabel>
	</t:panelGroup>

	<h:panelGroup id="txtAsstetNumber">
		<h:inputText styleClass="READONLY" readonly="true"
			rendered="#{pages$ReceiveProperty.showAssetControls}"
			style="width:187px;"
			binding="#{pages$ReceiveProperty.txtAssetNumber}" />
		<h:graphicImage alt="#{msg['tooltip.receiveProperty.searchAsset']}"
			rendered="#{pages$ReceiveProperty.showAssetControls}"
			binding="#{pages$ReceiveProperty.imgSearchAsset}"
			url="../resources/images/app_icons/Search-Unit.png"
			style="MARGIN: 1px 1px -5px;cursor:hand;"
			onclick="showAssetSearchPopup();"></h:graphicImage>

		<h:graphicImage rendered="#{pages$ReceiveProperty.showAssetControls}"
			alt="#{msg['manageAsset.toolTip']}"
			url="../resources/images/app_icons/view_tender.png"
			style="MARGIN: 1px 1px -5px;cursor:hand;"
			onclick="showAssetDetailsPopup();"></h:graphicImage>
	</h:panelGroup>

	<h:outputLabel id="lblAssetDesc"
		rendered="#{pages$ReceiveProperty.showAssetControls}"
		value="#{msg['searchAssets.assetDesc']}:"></h:outputLabel>

	<h:inputText styleClass="A_LEFT READONLY" id="txtAssetDesc"
		readonly="true" rendered="#{pages$ReceiveProperty.showAssetControls}"
		style="width:187px;" binding="#{pages$ReceiveProperty.txtAssetDesc}" />

	<h:outputLabel id="lblAssetNameEn"
		rendered="#{pages$ReceiveProperty.showAssetControls}"
		value="#{msg['searchAssets.assetNameEn']}:"></h:outputLabel>

	<h:inputText styleClass="A_LEFT READONLY" id="txtAssetNameEn"
		readonly="true" rendered="#{pages$ReceiveProperty.showAssetControls}"
		style="width:187px;" binding="#{pages$ReceiveProperty.txtAssetNameEn}" />

	<h:outputLabel id="lblAssetNameAr"
		rendered="#{pages$ReceiveProperty.showAssetControls}"
		value="#{msg['searchAssets.assetNameAr']}:"></h:outputLabel>

	<h:inputText styleClass="A_LEFT READONLY" id="txtAssetNameAr"
		readonly="true" rendered="#{pages$ReceiveProperty.showAssetControls}"
		style="width:187px;" binding="#{pages$ReceiveProperty.txtAssetNameAr}" />


</t:panelGrid>

