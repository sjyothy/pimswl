<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="javascript">
	function   showContractorsPopup()
	{
	   var screen_width = screen.width;
	   var screen_height = screen.height;
	   window.open('SearchPerson.jsf?persontype=CONTRACTOR&viewMode=popup','_blank','width='+(screen_width-10)+',height='+(screen_height-270)+',left=0,top=40,scrollbars=yes,status=yes');
	}
    function showViewTenderProposalPopup() 
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open('ViewProposalDetails.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
	}			
	function showPopup()
	{
	   var screen_width = screen.width;
	   var screen_height = screen.height;
	   window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
	}
	function populateTenant(TenantId,TenantNumber,TypeId,tenantNames)
    {
    }
</script>


<f:view>
	<script>
	function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
</script>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

			<style>
.rich-calendar-input {
	width: 150px;
}
</style>

		</head>

		<!-- Header -->

		<body class="BODY_STYLE">
			<div class="containerDiv">

				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>

					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['tenderManagement.heading']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<td width="100%" height="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION" style="height: 465px">

											<h:form id="manageTenderForm" styleClass="manageTenderForm"
												enctype="multipart/form-data">

												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 10px; margin-right: 10px;">
														<tr>
															<td>
																
																<h:outputText
																	value="#{pages$manageTender.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText
																	value="#{pages$manageTender.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="hdnTDTenderID"
																	binding="#{pages$manageTender.hdnTenderId}" />
															</td>
														</tr>
													</table>
												</div>


												<div class="MARGIN" style="width: 98%">




													<table cellpadding="0" cellspacing="0" width="98%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>

													<t:div styleClass="TAB_PANEL_INNER">

														<rich:tabPanel style="width:98%;height:235px;"
															headerSpacing="0"
															binding="#{pages$manageTender.tabPanel}">
															<rich:tab id="proposalsTab"
																label="#{msg['commons.tab.proposals']}">
																<t:div style="width:95%;text-align:center;">
																	<t:panelGrid width="100%" columns="1" cellpadding="0"
																		cellspacing="0">
																		<t:div styleClass="contentDiv" style="width:99%">
																			<t:dataTable id="proposalDataTable"
																				renderedIfEmpty="true" var="tenderProposal"
																				binding="#{pages$manageTender.proposalDataTable}"
																				value="#{pages$manageTender.proposalDataList}"
																				preserveDataModel="false" preserveSort="false"
																				rowClasses="row1,row2" rules="all"
																				rows="#{pages$manageTender.paginatorRows}"
																				width="100%">
																				<t:column width="10%" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderManagement.openProposal.proposalNumber']}" />
																					</f:facet>
																					<t:outputText
																						value="#{tenderProposal.proposalNumber}" />
																				</t:column>
																				<t:column width="12%" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderManagement.openProposal.proposalDate']}" />
																					</f:facet>
																					<t:outputText
																						value="#{tenderProposal.proposalDate}" />
																				</t:column>
																				<t:column width="15%" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderManagement.openProposal.proposalContractor']}" />
																					</f:facet>
																					<t:outputText
																						value="#{pages$manageTender.isEnglishLocale ? tenderProposal.contractorNameEn : tenderProposal.contractorNameAr}" />
																				</t:column>
																				<t:column width="14%" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderManagement.openProposal.proposalAmount']}" />
																					</f:facet>
																					<t:outputText
																						value="#{tenderProposal.proposalAmount}" />
																				</t:column>
																				<t:column width="20%" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderManagement.openProposal.recommendation']}" />
																					</f:facet>
																					<t:outputText
																						value="#{tenderProposal.recommendation}" />
																				</t:column>
																				<t:column width="10%" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderManagement.openProposal.proposalStatus']}" />
																					</f:facet>
																					<t:outputText
																						value="#{pages$manageTender.isEnglishLocale ? tenderProposal.statusEn : tenderProposal.statusAr}" />
																				</t:column>
																				<t:column width="19%" >
																					<!-- rendered="#{pages$manageTender.markWinnerLink}"-->
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderManagement.openProposal.proposalActions']}" />
																					</f:facet>
																					<h:commandLink id="markLink"
																						onclick="if (!confirm('#{msg['winnerContractor.msg']}')) return false"
																						action="#{pages$manageTender.onWinnerSelected}"
																						rendered="#{!tenderProposal.isWinner}">
																						<h:graphicImage id="markIcon"
																							title="#{msg['commons.markAsWinner']}"
																							url="../resources/images/app_icons/Dclare-Auction-Winner.png" />
																					</h:commandLink>
																					<t:outputLabel value=" " rendered="true"></t:outputLabel>
																					<h:commandLink id="viewLink" 
																						action="#{pages$manageTender.viewLink_action}">
																						<h:graphicImage id="viewIcon"
																							title="#{msg['commons.view']}"
																							url="../resources/images/app_icons/View-Contract.png" />
																					</h:commandLink>

																				</t:column>
																			</t:dataTable>
																		</t:div>
																		<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																			style="width:100%;">
																			<t:panelGrid columns="2" border="0" width="96%"
																				cellpadding="1" cellspacing="1">
																				<t:panelGroup
																					styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																					<t:div styleClass="JUG_NUM_REC_ATT">
																						<h:outputText value="#{msg['commons.records']}" />
																						<h:outputText value=" : " />
																						<h:outputText
																							value="#{pages$manageTender.recordSize}" />
																					</t:div>
																				</t:panelGroup>
																				<t:panelGroup>
																					<t:dataScroller id="proposalScroller"
																						for="proposalDataTable" paginator="true"
																						fastStep="1"
																						paginatorMaxPages="#{pages$manageTender.paginatorMaxPages}"
																						immediate="false" paginatorTableClass="paginator"
																						renderFacetsIfSinglePage="true"
																						paginatorTableStyle="grid_paginator"
																						layout="singleTable"
																						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																						paginatorActiveColumnStyle="font-weight:bold;"
																						paginatorRenderLinkForActive="false"
																						pageIndexVar="pageNumber"
																						styleClass="JUG_SCROLLER">
																						<f:facet name="first">
																							<t:graphicImage url="../#{path.scroller_first}"
																								id="lblF1"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastrewind">
																							<t:graphicImage
																								url="../#{path.scroller_fastRewind}" id="lblFR1"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastforward">
																							<t:graphicImage
																								url="../#{path.scroller_fastForward}"
																								id="lblFF1"></t:graphicImage>
																						</f:facet>
																						<f:facet name="last">
																							<t:graphicImage url="../#{path.scroller_last}"
																								id="lblL1"></t:graphicImage>
																						</f:facet>
																						<t:div styleClass="PAGE_NUM_BG" rendered="true">
																							<h:outputText styleClass="PAGE_NUM"
																								value="#{msg['commons.page']}" />
																							<h:outputText styleClass="PAGE_NUM"
																								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																								value="#{requestScope.pageNumber}" />
																						</t:div>
																					</t:dataScroller>
																				</t:panelGroup>
																			</t:panelGrid>
																		</t:div>
																	</t:panelGrid>
																</t:div>

																<t:div style="width:95%;text-align:right; margin-top:5px;">
																<h:commandButton id="unWinnerButton" styleClass="BUTTON" style="width:120px;"
																	value="#{msg['tenderProposal.DeselectWinners']}"
																	action="#{pages$manageTender.onDeselectWinners}"
																	onclick="return confirm('#{msg['tenderProposal.UnmarkAllConfirmation']}');">
																</h:commandButton>
																</t:div>
															</rich:tab>
															<rich:tab id="tenderDetailsTab"
																label="#{msg['manageTender.tab.TenderDetail']}">
																<t:div id="divTenderDetail"
																	styleClass="TAB_DETAIL_SECTION">
																	<t:panelGrid columns="4" cellpadding="1"
																		styleClass="TAB_DETAIL_SECTION_INNER" cellspacing="5"
																		width="100%">

																		<h:outputLabel
																			value="#{msg['tenderManagement.tenderNumber']}:"></h:outputLabel>
																		<t:panelGroup>

																			<h:inputText id="tenderNumber" readonly="true"
																				style="width: 170px; height: 18px"
																				binding="#{pages$manageTender.tenderNumber}" />
																		</t:panelGroup>
																		<h:outputLabel
																			value="#{msg['tenderManagement.tenderStatus']}:"></h:outputLabel>
																		<h:inputText id="tenderStatus" readonly="true"
																			style="width: 170px; height: 18px"
																			binding="#{pages$manageTender.tenderStatue}" />

																		<h:outputLabel
																			value="#{msg['tenderManagement.tenderType']}:"></h:outputLabel>

																		<h:selectOneMenu id="tenderType"
																			style="width: 170px; height: 18px"
																			binding="#{pages$manageTender.tenderType}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />

																			<f:selectItems
																				value="#{pages$manageTender.tenderTypeList}" />

																		</h:selectOneMenu>
																		<h:outputLabel
																			value="#{msg['tenderManagement.tenderDescription']}:"></h:outputLabel>
																		<h:inputText id="tenderDescription" readonly="true"
																			style="width: 170px; height: 18px"
																			binding="#{pages$manageTender.tenderDescription}" />


																		<h:outputLabel
																			value="#{msg['tenderManagement.startDate']}:"></h:outputLabel>

																		<rich:calendar id="startDate" rendered="true"
																			disabled="true"
																			value="#{pages$manageTender.startDate}"
																			datePattern="#{pages$manageTender.dateFormat}"
																			locale="#{pages$manageTender.locale}" popup="true"
																			showApplyButton="false" enableManualInput="false" />
																		<h:outputLabel
																			value="#{msg['tenderManagement.endDate']}:"></h:outputLabel>

																		<rich:calendar id="endDate" rendered="true"
																			disabled="true" value="#{pages$manageTender.endDate}"
																			datePattern="#{pages$manageTender.dateFormat}"
																			locale="#{pages$manageTender.locale}" popup="true"
																			showApplyButton="false" enableManualInput="false" />


																	</t:panelGrid>
																</t:div>
															</rich:tab>
															<rich:tab id="propertyDetailsTab"
																label="#{msg['manageTender.tab.propertyDetail']}">
																<t:panelGrid id="propertyDetailsButtonGrid" width="97%"
																	columnClasses="BUTTON_TD,BUTTON_TD" columns="2">
																	<t:panelGroup>
																		<h:commandButton styleClass="BUTTON" rendered="false"
																			binding="#{pages$manageTender.addUnit}"
																			value="#{msg['tenderManagement.AddUnit']}"
																			action="#{pages$manageTender.showUnitsPopup}" />
																		<h:commandButton styleClass="BUTTON" rendered="false"
																			binding="#{pages$manageTender.addProperty}"
																			value="#{msg['tenderManagement.AddProperty']}"
																			action="#{pages$manageTender.showPropertyPopup}" />
																	</t:panelGroup>
																</t:panelGrid>
																<t:div style="text-align:center;">
																	<t:div styleClass="contentDiv" style="width:95%">
																		<t:dataTable id="test8" rows="5" width="100%"
																			value="#{pages$manageTender.propertyDataList}"
																			binding="#{pages$manageTender.propertyGrid}"
																			preserveDataModel="false" preserveSort="false"
																			var="dataItemProperty" rowClasses="row1,row2"
																			rules="all" renderedIfEmpty="true">




																			<t:column id="dataItemProperty2" sortable="true">


																				<f:facet name="header">

																					<t:outputText
																						value="#{msg['property.propertyNumber']}" />

																				</f:facet>
																				<t:outputText styleClass="A_LEFT" id="a1"
																					value="#{dataItemProperty.propertyNumber}" />
																			</t:column>
																			<t:column id="dataItemProperty4" sortable="true">


																				<f:facet name="header">

																					<t:outputText
																						value="#{msg['property.commercialName']}" />

																				</f:facet>
																				<t:outputText styleClass="A_LEFT" title="" id="a2"
																					value="#{dataItemProperty.commercialName}" />
																			</t:column>
																			<t:column id="dataItemProperty5" sortable="true">


																				<f:facet name="header">

																					<t:outputText value="#{msg['property.type']}" />

																				</f:facet>
																				<t:outputText styleClass="A_LEFT" title="" id="a3"
																					value="#{dataItemProperty.propertyTypeEn}" />
																			</t:column>
																			<t:column id="dataItemProperty6" sortable="true">


																				<f:facet name="header">

																					<t:outputText value="#{msg['property.unitNumber']}" />

																				</f:facet>
																				<t:outputText styleClass="A_LEFT" title="" id="a34"
																					value="#{dataItemProperty.noOfUnits}" />
																			</t:column>
																			<t:column id="dataItemProperty7" sortable="true">


																				<f:facet name="header">

																					<t:outputText value="#{msg['property.emirates']}" />

																				</f:facet>
																				<t:outputText styleClass="A_LEFT" title="" id="a14"
																					value="#{dataItemProperty.emirateNameEn}" />
																			</t:column>
																			<t:column id="dataItemProperty8" sortable="true">


																				<f:facet name="header">

																					<t:outputText value="#{msg['property.area']}" />

																				</f:facet>
																				<t:outputText styleClass="A_LEFT" title="" id="a15"
																					value="#{dataItemProperty.stateEn}" />
																			</t:column>

																			<t:column
																				rendered="#{pages$manageTender.canDeleteTenderProperty}">
																				<f:facet name="header">
																					<t:outputText id="lbl_delete_Property"
																						value="#{msg['commons.delete']}" />
																				</f:facet>
																				<h:commandLink id="deleteIconProperty"
																					rendered="#{pages$manageTender.renderAction}"
																					action="#{pages$manageTender.cmdPropertyDelete_Click}">
																					<h:graphicImage id="delete_IconProperty"
																						title="#{msg['commons.delete']}"
																						url="../resources/images/delete_icon.png" />
																				</h:commandLink>

																			</t:column>


																		</t:dataTable>
																	</t:div>



																	<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																		style="width:96%;">
																		<t:panelGrid cellpadding="0" cellspacing="0"
																			width="100%" columns="2"
																			columnClasses="RECORD_NUM_TD">

																			<t:div styleClass="RECORD_NUM_BG">
																				<t:panelGrid cellpadding="0" cellspacing="0"
																					columns="3" columnClasses="RECORD_NUM_TD">

																					<h:outputText
																						value="#{msg['commons.recordsFound']}" />

																					<h:outputText value=" : "
																						styleClass="RECORD_NUM_TD" />

																					<h:outputText
																						value="#{pages$manageTender.propertyRecordSize}"
																						styleClass="RECORD_NUM_TD" />

																				</t:panelGrid>
																			</t:div>

																			<t:dataScroller id="extendscroller12" for="test8"
																				paginator="true" fastStep="1" paginatorMaxPages="2"
																				immediate="false" paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true"
																				paginatorTableStyle="grid_paginator"
																				layout="singleTable"
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																				paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																				pageIndexVar="pageNumber" styleClass="SCH_SCROLLER">

																				<f:facet name="first">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblFinquiry"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastrewind">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}"
																						id="lblFRinquiry"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastforward">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}"
																						id="lblFFinquiry"></t:graphicImage>
																				</f:facet>
																				<f:facet name="last">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblLinquiry"></t:graphicImage>
																				</f:facet>
																				<t:div styleClass="PAGE_NUM_BG" rendered="true">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</t:div>
																			</t:dataScroller>


																		</t:panelGrid>
																	</t:div>

																</t:div>
															</rich:tab>
															<rich:tab id="contractorsTab"
																label="#{msg['manageTender.tab.contractor']}">
																<t:panelGrid id="contractDetailsButtonGrid" width="97%"
																	columnClasses="BUTTON_TD" columns="1">
																	<h:commandButton value="#{msg['commons.addContractor']"
																		styleClass="BUTTON" rendered="false"
																		onclick="javascript:showContractorsPopup();"></h:commandButton>
																</t:panelGrid>
																<%@  include file="Contractors.jsp"%>

															</rich:tab>
															<rich:tab id="scopeOfWorkTab"
																label="#{msg['commons.tab.scopeOfWork']}">
																<%@  include file="ScopeOfWork.jsp"%>

															</rich:tab>
															<rich:tab id="inquiryAppTab"
																label="#{msg['tender.inquiryApp']}">
																<t:div style="width:100%;">
																	<t:panelGrid columns="1" cellpadding="0"
																		cellspacing="0" width="100%">
																		<t:div styleClass="contentDiv" style="width:99%;">
																			<t:dataTable id="dtInquiry"
																				value="#{pages$manageTender.tenderInquiryList}"
																				binding="#{pages$manageTender.inquiryDataTable}"
																				rows="5" preserveDataModel="false"
																				preserveSort="false" var="TenderInquiry"
																				rowClasses="row1,row2" rules="all"
																				renderedIfEmpty="true" width="100%">

																				<t:column id="colInquiryNumber" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderInquirySearch.inquiryNumber']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.requestNumber}" />
																				</t:column>

																				<t:column id="colInquiryDate" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderInquirySearch.inquiryDate']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.requestDate}">
																						<f:convertDateTime
																							pattern="#{pages$manageTender.dateFormat}"
																							timeZone="#{pages$manageTender.timeZone}" />
																					</t:outputText>
																				</t:column>

																				<t:column id="colSourceType" rendered="false"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderInquirySearch.sourceType']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.requestSourceEn}"
																						rendered="#{pages$manageTender.isEnglishLocale}" />
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.requestSourceAr}"
																						rendered="#{!pages$manageTender.isEnglishLocale}" />
																				</t:column>

																				<t:column id="colContractorNumber" rendered="false"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['ContractorSearch.contractorNumber']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.contractorNumber}" />
																				</t:column>

																				<t:column id="colContractorName" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['ContractorSearch.contractorName']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.contractorNameEn}"
																						rendered="#{pages$manageTender.isEnglishLocale}" />
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.contractorNameAr}"
																						rendered="#{!pages$manageTender.isEnglishLocale}" />
																				</t:column>
																				<t:column id="colTenderNumber" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderInquirySearch.TenderNo']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.tenderNumber}" />
																				</t:column>

																				<t:column id="colTenderStatus" sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tenderInquirySearch.inquiryStatus']}" />
																					</f:facet>
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.statusEn}"
																						rendered="#{pages$manageTender.isEnglishLocale}" />
																					<t:outputText styleClass="A_LEFT"
																						value="#{TenderInquiry.statusAr}"
																						rendered="#{!pages$manageTender.isEnglishLocale}" />
																				</t:column>
																				<t:column id="colTenderAction" rendered="false"
																					sortable="false">
																					<f:facet name="header">
																						<t:outputText value="#{msg['commons.action']}" />
																					</f:facet>

																					<h:commandLink
																						action="#{pages$manageTender.btnManage_Click}">
																						<h:graphicImage style="margin-left:5px;"
																							title="#{msg['commons.Manage']}"
																							url="../resources/images/detail-icon.gif" />
																					</h:commandLink>

																					<h:commandLink
																						action="#{pages$manageTender.btnDeleteInquiry_Click}">
																						<h:graphicImage style="margin-left:5px;"
																							title="#{msg['commons.delete']}"
																							url="../resources/images/delete.gif" />
																					</h:commandLink>
																					<h:commandLink
																						action="#{pages$manageTender.btnViewInquiry_Click}">
																						<h:graphicImage style="margin-left:5px;"
																							title="#{msg['commons.view']}"
																							url="../resources/images/app_icons/Contrect.png" />
																					</h:commandLink>
																				</t:column>

																			</t:dataTable>
																		</t:div>
																		<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																			style="width:100%;">
																			<t:panelGrid columns="2" border="0" width="100%"
																				cellpadding="1" cellspacing="1">
																				<t:panelGroup
																					styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																					<t:div styleClass="JUG_NUM_REC_ATT">
																						<h:outputText value="#{msg['commons.records']}" />
																						<h:outputText value=" : " />
																						<h:outputText
																							value="#{pages$manageTender.inquiryAppRecordSize}" />
																					</t:div>
																				</t:panelGroup>
																				<t:panelGroup>
																					<t:dataScroller id="Inquiryscroller"
																						for="dtInquiry" paginator="true" fastStep="1"
																						paginatorMaxPages="#{pages$manageTender.paginatorMaxPages}"
																						immediate="false" paginatorTableClass="paginator"
																						renderFacetsIfSinglePage="true"
																						paginatorTableStyle="grid_paginator"
																						layout="singleTable"
																						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																						paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																						pageIndexVar="pageNumber"
																						styleClass="SCH_SCROLLER">

																						<f:facet name="first">
																							<t:graphicImage url="../#{path.scroller_first}"
																								id="lblFinqend"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastrewind">
																							<t:graphicImage
																								url="../#{path.scroller_fastRewind}"
																								id="lblFRinqend"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastforward">
																							<t:graphicImage
																								url="../#{path.scroller_fastForward}"
																								id="lblFFinqe"></t:graphicImage>
																						</f:facet>
																						<f:facet name="last">
																							<t:graphicImage url="../#{path.scroller_last}"
																								id="lblLInqend"></t:graphicImage>
																						</f:facet>
																						<t:div styleClass="PAGE_NUM_BG" rendered="true">
																							<h:outputText styleClass="PAGE_NUM"
																								value="#{msg['commons.page']}" />
																							<h:outputText styleClass="PAGE_NUM"
																								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																								value="#{requestScope.pageNumber}" />
																						</t:div>
																					</t:dataScroller>
																				</t:panelGroup>
																			</t:panelGrid>
																		</t:div>
																	</t:panelGrid>
																</t:div>
															</rich:tab>
															<rich:tab id="extendsAppTab"
																label="#{msg['manageTender.tab.extendRequest']}">
																<t:div style="width:100%;">
																	<t:panelGrid columns="1" cellpadding="0"
																		cellspacing="0" width="100%">
																		<t:div styleClass="contentDiv" style="width:99.1%;">

																			<t:dataTable id="dt1"
																				value="#{pages$manageTender.extendedDataList}"
																				binding="#{pages$manageTender.extendedDataTable}"
																				rows="#{pages$manageTender.paginatorRows}"
																				preserveDataModel="false" preserveSort="false"
																				var="dataItem" rowClasses="row1,row2" rules="all"
																				renderedIfEmpty="true" width="100%">

																				<t:column id="requestNumberCol" width="15%"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['application.number.gridHeader']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.requestNumber}" />
																				</t:column>
																				<t:column id="requestDateCol" width="14%"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['application.date.gridHeader']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.requestDateStr}" />
																				</t:column>
																				<t:column id="contractorNameCol" width="20%"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['contractor.name.gridHeader']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.contractorName}" />
																				</t:column>
																				<t:column id="tenderNumberCol" width="12%"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tender.number.gridHeader']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.tenderNumber}" />
																				</t:column>
																				<t:column id="tenderTypeCol" width="12%"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['tender.type.gridHeader']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.tenderType}" />
																				</t:column>
																				<t:column id="applicationStatusCol" width="12%"
																					sortable="true">
																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['application.status.gridHeader']}" />
																					</f:facet>
																					<t:outputText value="#{dataItem.applicationStatus}" />
																				</t:column>
																				<t:column id="actionCol" rendered="false"
																					sortable="false" width="15%">
																					<f:facet name="header">
																						<t:outputText value="#{msg['commons.action']}" />
																					</f:facet>
																					<t:commandLink
																						action="#{pages$manageTender.onView}"
																						rendered="#{dataItem.showView}">
																						<h:graphicImage id="viewIcon"
																							title="#{msg['extendApplication.search.toolTips.view']}"
																							url="../resources/images/app_icons/Approve-Request.png" />&nbsp;
												                                 	</t:commandLink>
																					<t:outputLabel value=" " rendered="true"></t:outputLabel>
																					<t:commandLink
																						action="#{pages$manageTender.onFollowUp}"
																						rendered="#{dataItem.showFollowUp}">
																						<h:graphicImage id="followUpIcon"
																							title="#{msg['extendApplication.search.toolTips.followUp']}"
																							url="../resources/images/app_icons/Provide-Tecnical-Comments.png" />&nbsp;
													                                </t:commandLink>
																					<t:outputLabel value=" " rendered="true"></t:outputLabel>
																					<t:commandLink
																						action="#{pages$manageTender.onManage}"
																						rendered="#{dataItem.showManage}">
																						<h:graphicImage id="manageIcon"
																							title="#{msg['extendApplication.search.toolTips.manage']}"
																							url="../resources/images/app_icons/Request-detail.png" />&nbsp;
													                                </t:commandLink>
																					<t:outputLabel value=" " rendered="true"></t:outputLabel>
																					<t:commandLink
																						action="#{pages$manageTender.onPublish}"
																						rendered="#{dataItem.showPublish}">
																						<h:graphicImage id="publishIcon"
																							title="#{msg['extendApplication.search.toolTips.publish']}"
																							url="../resources/images/app_icons/Request-detail.png" />&nbsp;
													                                </t:commandLink>
																					<t:outputLabel value=" " rendered="true"></t:outputLabel>
																					<t:commandLink
																						onclick="if (!confirm('#{msg['extendApplication.search.confirm.delete']}')) return"
																						action="#{pages$manageTender.onDelete}"
																						rendered="#{dataItem.showDelete}">
																						<h:graphicImage id="deleteAppIcon"
																							title="#{msg['extendApplication.search.toolTips.delete']}"
																							url="../resources/images/app_icons/Request-detail.png" />&nbsp;
													                                 </t:commandLink>
																				</t:column>
																			</t:dataTable>
																		</t:div>
																		<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																			style="width:100%;">
																			<t:panelGrid columns="2" border="0" width="100%"
																				cellpadding="1" cellspacing="1">
																				<t:panelGroup
																					styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																					<t:div styleClass="JUG_NUM_REC_ATT">
																						<h:outputText value="#{msg['commons.records']}" />
																						<h:outputText value=" : " />
																						<h:outputText
																							value="#{pages$manageTender.recordSize}" />
																					</t:div>
																				</t:panelGroup>
																				<t:panelGroup>
																					<t:dataScroller id="extendscroller" for="dt1"
																						paginator="true" fastStep="1"
																						paginatorMaxPages="#{pages$manageTender.paginatorMaxPages}"
																						immediate="false" paginatorTableClass="paginator"
																						renderFacetsIfSinglePage="true"
																						paginatorTableStyle="grid_paginator"
																						layout="singleTable"
																						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																						paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																						pageIndexVar="pageNumber"
																						styleClass="SCH_SCROLLER">

																						<f:facet name="first">
																							<t:graphicImage url="../#{path.scroller_first}"
																								id="lblFextend"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastrewind">
																							<t:graphicImage
																								url="../#{path.scroller_fastRewind}"
																								id="lblFRextend"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastforward">
																							<t:graphicImage
																								url="../#{path.scroller_fastForward}"
																								id="lblFFexte"></t:graphicImage>
																						</f:facet>
																						<f:facet name="last">
																							<t:graphicImage url="../#{path.scroller_last}"
																								id="lblLextend"></t:graphicImage>
																						</f:facet>
																						<t:div styleClass="PAGE_NUM_BG" rendered="true">
																							<h:outputText styleClass="PAGE_NUM"
																								value="#{msg['commons.page']}" />
																							<h:outputText styleClass="PAGE_NUM"
																								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																								value="#{requestScope.pageNumber}" />
																						</t:div>
																					</t:dataScroller>
																				</t:panelGroup>
																			</t:panelGrid>
																		</t:div>
																	</t:panelGrid>
																</t:div>
															</rich:tab>

															<rich:tab id="attachmentTab"
																label="#{msg['commons.attachmentTabHeading']}">
																<%@  include file="attachment/attachment.jsp"%>
															</rich:tab>

															<rich:tab id="commentsTab"
																label="#{msg['commons.commentsTabHeading']}">
																<%@ include file="notes/notes.jsp"%>
															</rich:tab>
															
															<rich:tab id="activityLog" label="#{msg['tenders.activityLog']}"
														          action="#{pages$manageTender.requestHistoryTabClick}">
														          <!-- rendered="#{!empty pages$manageTender.tenderView && !empty pages$manageTender.tenderView.tenderId}" -->
														          
							                                   <%@ include file="../pages/requestTasks.jsp"%>
														     </rich:tab>

														</rich:tabPanel>
													</t:div>
													<table cellpadding="0" cellspacing="0" width="98%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<table cellpadding="1px" cellspacing="3px" width="98%">



														<tr>
															<td colspan="6" class="BUTTON_TD">
																<h:commandButton id="cancelButton" styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	action="#{pages$manageTender.cancel}">
																</h:commandButton>


															</td>

														</tr>


													</table>
												</div>
										</div>
										</h:form>

										</div>
									</td>
								</tr>
							</table>


						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>
