
<t:div id="tabDDetails" style="width:100%;">
	<t:panelGrid id="ddTab" cellpadding="5px" width="100%"
		rendered="#{pages$generalCollectionRequest.showSaveButton || pages$generalCollectionRequest.showResubmitButton}"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="paymentTypeField" styleClass="LABEL"
				value="#{msg['addMissingMigratedPayments.fieldName.paymentType']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="selectPaymenType"
			value="#{pages$generalCollectionRequest.paymentView.typeIdStr}">
			<f:selectItem itemValue="-1"
				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
			<f:selectItems value="#{pages$ApplicationBean.paymentTypeList}" />
		</h:selectOneMenu>

		<h:panelGroup>
			<h:outputLabel id="mandCostCenter" styleClass="mandatory" value="*" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['settlement.label.costCenter']}:"></h:outputLabel>
		</h:panelGroup>
		<h:panelGroup>
			<h:inputText id="txtCostCenter" styleClass="READONLY" readonly="true"
				binding="#{pages$generalCollectionRequest.txtCostCenter }"
				value="#{pages$generalCollectionRequest.paymentView.costCenter}" />

			<h:selectOneMenu id="selectOneCostCenter" rendered="false"
				binding="#{pages$generalCollectionRequest.htmlSelectOneFixCostCenter }"
				value="#{pages$generalCollectionRequest.selectOneCostCenter}">

				<f:selectItems
					value="#{pages$generalCollectionRequest.fixedCostCenters}" />
			</h:selectOneMenu>
			<h:selectBooleanCheckbox id="chkCostCenterFixed"
				value="#{pages$generalCollectionRequest.chkCostCenterFixed}">
				<a4j:support event="onclick" reRender="tabDDetails"
					action="#{pages$generalCollectionRequest.onCheckFixCostCenter}" />
			</h:selectBooleanCheckbox>
		</h:panelGroup>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['cancelContract.tab.unit.propertyname']}" />
		<t:panelGroup>
			<h:inputText styleClass="READONLY" style="width:170px;"
				readonly="true"
				value="#{pages$generalCollectionRequest.paymentView.property.commercialName}" />
			<h:commandLink id="lnkSearchProperty">
				<h:graphicImage id="imgSearchProperty"
					rendered="#{pages$generalCollectionRequest.showSaveButton || 
				    		pages$generalCollectionRequest.showResubmitButton 
				           }"
					title="#{msg['maintenanceRequest.searchProperties']}"
					style="MARGIN: 0px 0px -4px;" onclick="showPropertySearchPopUp();"
					url="../resources/images/magnifier.gif">
				</h:graphicImage>
			</h:commandLink>
		</t:panelGroup>

		<h:outputLabel styleClass="LABEL" value="#{msg['grp.UnitNumber']}" />
		<h:panelGroup>
			<h:inputText styleClass="READONLY" id="txtunitNumber"
				style="width:170px;" readonly="true"
				value="#{pages$generalCollectionRequest.paymentView.unit.unitNumber}"></h:inputText>
			<h:graphicImage url="../resources/images/magnifier.gif"
				rendered="#{pages$generalCollectionRequest.showSaveButton || 
				    		pages$generalCollectionRequest.showResubmitButton 
				           }"
				onclick="javaScript:showUnitSearchPopUp();"
				style="MARGIN: 0px 0px -4px;"
				title="#{msg['maintenanceRequest.searchUnits']}"
				alt="#{msg['maintenanceRequest.searchUnits']}"></h:graphicImage>

		</h:panelGroup>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['commons.inheritanceFile']}" />
		<h:panelGroup>
			<h:inputText styleClass="READONLY" id="inheritanceFile"
				style="width:170px;" readonly="true"
				value="#{pages$generalCollectionRequest.paymentView.inheritanceFile.fileNumber}"></h:inputText>
			<h:graphicImage url="../resources/images/magnifier.gif"
				rendered="#{pages$generalCollectionRequest.showSaveButton || 
				    		pages$generalCollectionRequest.showResubmitButton 
				           }"
				onclick="javaScript:openInheritenceFileSearch( 'generalCollectionRequest','MODE_SELECT_ONE_POPUP');"
				style="MARGIN: 0px 0px -4px;"
				title="#{msg['commons.tooltip.searchInheritanceFile']}"
				alt="#{msg['commons.tooltip.searchInheritanceFile']}"></h:graphicImage>

		</h:panelGroup>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['commons.inheritanceBen']}" />
		<h:panelGroup>
			<h:inputText maxlength="20" styleClass="READONLY" id="inheritanceben"
				style="width:170px;" readonly="true"
				value="#{pages$generalCollectionRequest.paymentView.inheritanceBeneficiary.beneficiary.personFullName}"></h:inputText>
			<h:graphicImage url="../resources/images/magnifier.gif"
				rendered="#{pages$generalCollectionRequest.showSaveButton || 
				    		pages$generalCollectionRequest.showResubmitButton 
				           }"
				onclick="javaScript:openInheritenceBeneficiarySearch( 'generalCollectionRequest','MODE_SELECT_ONE_POPUP');"
				style="MARGIN: 0px 0px -4px;"
				title="#{msg['commons.tooltip.searchInheritanceBen']}"
				alt="#{msg['commons.tooltip.searchInheritanceBen']}"></h:graphicImage>

		</h:panelGroup>

		<h:outputLabel id="lblEndowment" styleClass="LABEL"
			value="#{msg['revenueFollowup.lbl.endowment']}" />
		<h:panelGroup>

			<h:inputText id="endowment" readonly="true" styleClass="READONLY"
				style="width:170px;"
				value="#{pages$generalCollectionRequest.paymentView.endowmentView.endowmentName}" />

			<h:graphicImage id="endowmentPopup" style="MARGIN: 0px 0px -4px;"
				title="#{msg['endowment.search.title']}"
				url="../resources/images/magnifier.gif"
				onclick="javaScript:openSearchEndowmentsPopup('generalCollectionRequest');" />
		</h:panelGroup>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="payModeField" styleClass="LABEL"
				value="#{msg['cancelContract.payment.paymentmethod']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="selectPaymentMethod"
			value="#{pages$generalCollectionRequest.paymentView.paymentModeIdString}">

			<f:selectItems value="#{pages$ApplicationBean.paymentMethodList}" />
		</h:selectOneMenu>

		<h:panelGroup>
			<h:outputLabel id="mandownerShipMenu" styleClass="mandatory"
				value="*" />
			<h:outputLabel styleClass="LABEL"
				value="#{msg['grp.ownerShipMenu']}:"></h:outputLabel>
		</h:panelGroup>

		<h:selectOneMenu id="ownerShipMenu"
			binding="#{pages$generalCollectionRequest.cmbOwnershipType}"
			value="#{pages$generalCollectionRequest.paymentView.ownerShipTypeIdStr}"
			required="false">
			<f:selectItem itemValue="-1" itemLabel="#{msg['commons.All']}" />
			<f:selectItems value="#{pages$ApplicationBean.propertyOwnershipType}" />
		</h:selectOneMenu>


		<h:outputLabel styleClass="LABEL"
			value="#{msg['paymentSchedule.paymentDueOn']}:"></h:outputLabel>

		<rich:calendar id="createdOnDateId" styleClass="READONLY"
			disabled="false"
			value="#{pages$generalCollectionRequest.paymentView.paymentDueOn}"
			locale="#{pages$generalCollectionRequest.locale}" popup="true"
			datePattern="#{pages$generalCollectionRequest.dateFormat}"
			showApplyButton="false" enableManualInput="false"
			inputStyle="width:185px;height:17px" />



		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="amnt" styleClass="LABEL"
				value="#{msg['commons.amount']}:"></h:outputLabel>
		</h:panelGroup>
		<h:inputText id="txtAmount"
			readonly="#{!pages$generalCollectionRequest.showSaveButton && 
			    		!pages$generalCollectionRequest.showResubmitButton 
			           }"
			value="#{pages$generalCollectionRequest.paymentView.amountStr }"
			onkeyup="removeNonNumeric(this)" onkeypress="removeNonNumeric(this)"
			onkeydown="removeNonNumeric(this)" onblur="removeNonNumeric(this)"
			onchange="removeNonNumeric(this)" />

		<h:outputLabel id="emptyCol1" styleClass="mandatory" value="" />
		<h:outputLabel id="emptyCol2" styleClass="mandatory" value="" />


		<h:outputLabel style="font-weight:normal;" styleClass="TABLE_LABEL"
			value="#{msg['collectionProcedure.description']} :"></h:outputLabel>
		<t:panelGroup colspan="4">
			<h:inputTextarea id="descriptionPayments"
				value="#{pages$generalCollectionRequest.paymentView.description}"
				style="width:570px;">
			</h:inputTextarea>
		</t:panelGroup>



	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:10px; padding-right:21px;">

		<h:commandButton styleClass="BUTTON" id="btnAdd"
			rendered="#{pages$generalCollectionRequest.showSaveButton ||  
										 pages$generalCollectionRequest.showResubmitButton 
					   				    }"
			value="#{msg['commons.Add']}" onclick="performTabClick('lnkAdd');">
			<h:commandLink id="lnkAdd"
				action="#{pages$generalCollectionRequest.onAdd}" />
		</h:commandButton>


	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTablePaymentsSchedule" rows="15" width="100%"
			value="#{pages$generalCollectionRequest.paymentList}"
			binding="#{pages$generalCollectionRequest.dataTable}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">


			<t:column id="payNumber" sortable="true">
				<f:facet name="header">
					<t:outputText id="payNum"
						value="#{msg['chequeList.paymentNumber']}" />
				</f:facet>
				<t:outputText id="payNoField" styleClass="A_LEFT"
					value="#{dataItem.paymentNumber}" />
			</t:column>


			<t:column id="typepaymetn" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdtypepaymetn"
						value="#{msg['addMissingMigratedPayments.fieldName.paymentType']}" />
				</f:facet>
				<t:outputText id="typepaymetnField" styleClass="A_LEFT"
					value="#{dataItem.typeAr}" />
			</t:column>
			

			<t:column id="costCenter" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdcostCenter"
						value="#{msg['settlement.label.costCenter']}" />
				</f:facet>
				<t:outputText id="costCenterField" styleClass="A_LEFT"
					value="#{dataItem.costCenter}" />
			</t:column>
			<t:column id="name" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdname" value="#{msg['commons.Name']}" />
				</f:facet>
				<t:outputText id="nameField" styleClass="A_LEFT"
					value="#{dataItem.name}" />
			</t:column>
			<t:column id="statusCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="statusAmnt" value="#{msg['commons.status']}" />
				</f:facet>
				<t:outputText id="status" styleClass="A_LEFT"
					value="#{pages$generalCollectionRequest.englishLocale? dataItem.statusEn:dataItem.statusAr}" />
			</t:column>

			<t:column id="payMode" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdMethod"
						value="#{msg['cancelContract.payment.paymentmethod']}" />
				</f:facet>
				<t:outputText id="paymentMode" styleClass="A_LEFT"
					value="#{dataItem.paymentModeAr}" />
			</t:column>


			<t:column id="amntCol" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAmnt"
						value="#{msg['mems.payment.request.label.Amount']}" />
				</f:facet>
				<t:outputText id="amountt" styleClass="A_LEFT"
					value="#{dataItem.amountStr}" />
			</t:column>

			<t:column id="grpownerShipMenu" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdgrpownerShipMenu"
						value="#{msg['grp.ownerShipMenu']}" />
				</f:facet>
				<t:outputText id="ownerShipTypeA" styleClass="A_LEFT"
					value="#{dataItem.ownerShipTypeAr}" />
			</t:column>
			<t:column id="collectionProcedureDescription" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdcollectionProceduredescription"
						value="#{msg['collectionProcedure.description']}" />
				</f:facet>
				<div style="width: 70%; white-space: normal;">
					<t:outputText id="descColGrid" styleClass="A_LEFT"
						value="#{dataItem.description}" />
				</div>
			</t:column>
			<t:column id="action" sortable="true">
				<f:facet name="header">
					<t:outputText id="hdAmoun" value="#{msg['commons.action']}" />
				</f:facet>

				<h:commandLink id="lnkDelete"
					onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableInputs();"
					rendered="#{pages$generalCollectionRequest.showSaveButton || pages$generalCollectionRequest.showResubmitButton}"
					action="#{pages$generalCollectionRequest.onDelete}">
					<h:graphicImage id="deleteIcon" title="#{msg['commons.delete']}"
						url="../resources/images/delete.gif" width="18px;" />
				</h:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>
</t:div>


