<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Adding Family Village Beneficiary 
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
		function closeWindow()
        {
        	window.opener.onMessageFromAddFamilyVillageBeneficiary();
        	window.close();
        
        }
	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">



			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">





				<tr width="100%">



					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['commons.Beneficiary']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="searchFrm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:messages></h:messages>
															<h:outputText
																value="#{pages$AddFamilyVillageBeneficiary.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$AddFamilyVillageBeneficiary.successMessages}"
																escape="false" styleClass="INFO_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">
												<div style="width: 99.8%;">
													<t:panelGrid id="tabBDetailsab" cellpadding="5px"
														width="100%" cellspacing="10px" columns="4"
														styleClass="TAB_DETAIL_SECTION_INNER"
														columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
														<h:panelGroup>
															<h:outputLabel id='firstNamelbl' styleClass="mandatory"
																value="*" />
															<h:outputLabel id="firstnameText" styleClass="LABEL"
																value="#{msg['commons.Name']}:"></h:outputLabel>
														</h:panelGroup>

														<h:inputText maxlength="500" id="firstnameTextIT"
															value="#{pages$AddFamilyVillageBeneficiary.pageView.firstName}" />

														<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*" />
															<h:outputLabel id="genderLblVilla" styleClass="LABEL"
																value="#{msg['customer.gender']}:"></h:outputLabel>
														</h:panelGroup>
														<h:selectOneMenu id="selectGender"
															value="#{pages$AddFamilyVillageBeneficiary.pageView.gender}">
															<f:selectItem itemLabel="#{msg['tenants.gender.male']}"
																itemValue="M" />
															<f:selectItem itemLabel="#{msg['tenants.gender.female']}"
																itemValue="F" />
														</h:selectOneMenu>

														<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*" />
															<h:outputLabel styleClass="LABEL"
																id="DateOfBirthVillalbl"
																value="#{msg['customer.dateOfBirth']}:" />
														</h:panelGroup>
														<rich:calendar id="DateOfBirthVilla"
															value="#{pages$AddFamilyVillageBeneficiary.pageView.dateOfBirth}"
															popup="true"
															datePattern="#{pages$AddFamilyVillageBeneficiary.dateFormat}"
															showApplyButton="false"
															locale="#{pages$AddFamilyVillageBeneficiary.locale}"
															enableManualInput="false"
															inputStyle="width: 170px; height: 14px">
															<a4j:support event="onchanged"
																action="#{pages$AddFamilyVillageBeneficiary.onDateOfBirthChange}"
																reRender="age,errormsg" />

														</rich:calendar>

														<h:outputLabel id="age" styleClass="LABEL"
															value="#{msg['familyVillageManageBeneficiary.lbl.age']}:"></h:outputLabel>

														<h:inputText readonly="true" id="ageTextIT"
															styleClass="READONLY"
															value="#{pages$AddFamilyVillageBeneficiary.pageView.age}" />

														<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*" />
															<h:outputLabel styleClass="LABEL"
																value="#{msg['customer.nationality']}:"></h:outputLabel>
														</h:panelGroup>
														<h:selectOneMenu id="nationality" immediate="false"
															
															value="#{pages$AddFamilyVillageBeneficiary.pageView.nationalityIdString}">
															<f:selectItem itemValue="-1"
																itemLabel="#{msg['commons.combo.PleaseSelect']}" />
															<f:selectItems
																value="#{pages$ApplicationBean.countryList}" />
														</h:selectOneMenu>

														<h:outputLabel styleClass="LABEL"
															value="#{msg['commons.lbl.emiratesId']}:"></h:outputLabel>

														<h:inputText maxlength="15" id="txtsocialSecNumber"
															value="#{pages$AddFamilyVillageBeneficiary.pageView.socialSecNumber}"></h:inputText>

														<h:outputText
															value="#{msg['mems.inheritanceFile.fieldLabel.passportType']}" />
														<h:selectOneMenu id="isMasroom"
															binding="#{pages$AddFamilyVillageBeneficiary.cmbPassportType}">
															<f:selectItem itemValue="-1"
																itemLabel="#{msg['commons.combo.PleaseSelect']}" />
															<f:selectItems
																value="#{pages$ApplicationBean.passportTypeList}" />
															<a4j:support event="onchange"
																reRender="passportNumberId,masroomNumberId,passportNumberIdLbl,masroomNumberIdLbl"
																action="#{pages$AddFamilyVillageBeneficiary.onPassportTypeChanged}" />
														</h:selectOneMenu>
														<h:panelGroup id="passportNumberIdLbl">
															<h:outputText
																value="#{msg['mems.inheritanceFile.fieldLabel.passportNumber']}" />
														</h:panelGroup>
														<h:panelGroup id="passportNumberId">
															<h:inputText
																disabled="#{pages$AddFamilyVillageBeneficiary.pageView.masroom}"
																value="#{pages$AddFamilyVillageBeneficiary.pageView.passportNumber}" />
														</h:panelGroup>

														<h:panelGroup id="masroomNumberId">
															<h:outputText
																value="#{msg['mems.inheritanceFile.fieldLabel.masroomNumber']}" />
														</h:panelGroup>

														<h:panelGroup id="masroomNumberIdLbl">
															<h:inputText
																disabled="#{!pages$AddFamilyVillageBeneficiary.pageView.masroom}"
																value="#{pages$AddFamilyVillageBeneficiary.pageView.masroomNumber}" />
														</h:panelGroup>




														<h:panelGroup>
															<h:outputLabel styleClass="mandatory" value="*" />
															<h:outputLabel styleClass="LABEL"
																id="DateOfBirthJOININGlbl"
																value="#{msg['familyVillageManageBeneficiary.lbl.joiningDate']}:" />
														</h:panelGroup>
														<rich:calendar id="DateOfBirthJoining"
															value="#{pages$AddFamilyVillageBeneficiary.pageView.dateOfJoining}"
															popup="true"
															datePattern="#{pages$AddFamilyVillageBeneficiary.dateFormat}"
															showApplyButton="false"
															locale="#{pages$AddFamilyVillageBeneficiary.locale}"
															enableManualInput="false"
															inputStyle="width: 170px; height: 14px">
														</rich:calendar>


														<h:outputLabel styleClass="LABEL"
															value="#{msg['lbl.requestReason']}:"></h:outputLabel>
														<div class="SCROLLABLE_SECTION"
															style="height: 100px; width: 298px; overflow-x: hidden; border-width: 1px; border-style: solid; border-color: #a2a2a2;">
															<h:panelGroup
																style="border-width: 1px; border-style: solid; border-color: #a2a2a2; background: white; overflow-x: auto; overflow-y: scroll; width: 90%; height: 60px; ">

																<h:selectManyCheckbox layout="pageDirection"
																	id="housingRequestReasons"
																	value="#{pages$AddFamilyVillageBeneficiary.housingRequestReasons}">
																	<f:selectItems
																		value="#{pages$ApplicationBean.housingRequestReason}" />
																</h:selectManyCheckbox>
															</h:panelGroup>
														</div>
														<h:outputLabel styleClass="LABEL"
															value="#{msg['lbl.requestReasonDesc']}:"></h:outputLabel>
														<t:panelGroup colspan="6">
															<h:inputTextarea rows="2" style="width: 90%;"
																id="housingReasonDescription"
																value="#{pages$AddFamilyVillageBeneficiary.pageView.housingReasonDescription}" />
														</t:panelGroup>

														<h:outputLabel
															value="#{msg['educationAspects.instituteName']} :"
															style="font-weight:normal;" styleClass="TABLE_LABEL" />
														<h:inputText id="instituteName"
															value="#{pages$AddFamilyVillageBeneficiary.pageView.educationAspects.instituteName}"></h:inputText>

														<h:outputLabel value="#{msg['educationAspects.grade']} :"
															style="font-weight:normal;" styleClass="TABLE_LABEL" />
														<h:selectOneMenu id="grade"
															value="#{pages$AddFamilyVillageBeneficiary.pageView.educationAspects.gradeId}">
															<f:selectItem itemValue="-1"
																itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
															<f:selectItems value="#{pages$ApplicationBean.allGrades}" />
														</h:selectOneMenu>

														<h:outputLabel
															value="#{msg['educationAspects.gradeDesc']} :"
															style="font-weight:normal;" styleClass="TABLE_LABEL" />
														<h:inputTextarea id="gradeDesc"
															onkeyup="javaScript:removeExtraCharacter(this,50)"
															onkeypress="javaScript:removeExtraCharacter(this,50)"
															onkeydown="javaScript:removeExtraCharacter(this,50)"
															onblur="javaScript:removeExtraCharacter(this,50)"
															onchange="javaScript:removeExtraCharacter(this,50)"
															value="#{pages$AddFamilyVillageBeneficiary.pageView.educationAspects.gradeDesc}" />



														<h:outputLabel value="#{msg['educationAspects.level']} :"
															style="font-weight:normal;" styleClass="TABLE_LABEL" />
														<h:selectOneMenu id="level"
															value="#{pages$AddFamilyVillageBeneficiary.pageView.educationAspects.educationLevelId}">
															<f:selectItem itemValue="-1"
																itemLabel="#{msg['mems.inheritanceFile.fieldLabel.pleaseSelect']}" />
															<f:selectItems
																value="#{pages$ApplicationBean.allEducationLevel}" />
														</h:selectOneMenu>

														<h:outputLabel
															value="#{msg['educationAspects.educationLevelDesc']} :"
															style="font-weight:normal;" styleClass="TABLE_LABEL" />
														<h:inputTextarea id="eduLevelDesc"
															onkeyup="javaScript:removeExtraCharacter(this,50)"
															onkeypress="javaScript:removeExtraCharacter(this,50)"
															onkeydown="javaScript:removeExtraCharacter(this,50)"
															onblur="javaScript:removeExtraCharacter(this,50)"
															onchange="javaScript:removeExtraCharacter(this,50)"
															value="#{pages$AddFamilyVillageBeneficiary.pageView.educationAspects.educationLevelDesc}" />



													</t:panelGrid>
												</div>

												<t:div styleClass="BUTTON_TD"
													style="padding-top:10px; padding-right:21px;">

													<h:commandButton styleClass="BUTTON"
														value="#{msg['commons.saveButton']}" style="width: auto"
														action="#{pages$AddFamilyVillageBeneficiary.onSave}">
													</h:commandButton>
													<h:commandButton styleClass="BUTTON" type="button"
														value="#{msg['commons.done']}" style="width: auto"
														onclick="javaScript:closeWindow();">
													</h:commandButton>
												</t:div>

											</div>

										</div>


										</div>
									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>


		</body>
	</html>
</f:view>