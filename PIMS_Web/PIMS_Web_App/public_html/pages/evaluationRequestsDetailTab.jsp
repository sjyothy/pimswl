<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<script type="text/javascript">
</script>
<t:div style="text-align:left;">
	<t:div styleClass="contentDiv" style="width:94.5%">
		<t:dataTable id="dt1"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
			width="100%">

			<t:column id="requestNumberCol" width="15%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['application.number.gridHeader']}" />
				</f:facet>
				<t:outputText value="" />
			</t:column>
			<t:column id="requestDateCol" width="14%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['application.date.gridHeader']}" />
				</f:facet>
				<t:outputText value="" />
			</t:column>
			<t:column id="propertyNameCol" width="20%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['property.name']}" />
				</f:facet>
				<t:outputText value="" />
			</t:column>
			<t:column id="landNumberCol" width="12%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['applicationDetails.ownershipType']}" />
				</f:facet>
				<t:outputText value="" />
			</t:column>
			<t:column id="receivedDateCol" width="12%" sortable="true">
				<f:facet name="header">
					<t:outputText
						value="#{msg['evaluationApplication.search.grid.receivedDate']}" />
				</f:facet>
				<t:outputText value="" />
			</t:column>
			<t:column id="applicationStatusCol" width="12%" sortable="true">
				<f:facet name="header">
					<t:outputText value="#{msg['application.status.gridHeader']}" />
				</f:facet>
				<t:outputText value="" />
			</t:column>
			<t:column id="actionCol" sortable="false" width="15%">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.action']}" />
				</f:facet>
				<t:commandLink >
					<h:graphicImage id="viewIcon" title="#{msg['commons.view']}"
						url="../resources/images/app_icons/View_Icon.png" />&nbsp;
				</t:commandLink>
			</t:column>
		</t:dataTable>
	</t:div>

</t:div>
