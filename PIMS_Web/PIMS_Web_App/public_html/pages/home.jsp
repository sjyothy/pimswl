
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<script language="javascript" type="text/javascript">
	function GenerateFloorsPopup()
	{
	   var screen_width = screen.width;
	   var screen_height = screen.height;
	   var screen_top = screen.width/4;
	   var screen_left = screen.height/4;
	   window.open('GenerateFloors.jsf','_blank','width='+(screen_width-300)+',height='+(screen_height-500)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}
	function GenerateUnitsPopup()
	{
	   var screen_width = screen.width;
	   var screen_height = screen.height;
	   var screen_top = screen.width/4;
	   var screen_left = screen.height/4;
	   window.open('GenerateUnits.jsf','_blank','width='+(screen_width-300)+',height='+(screen_height-500)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}

</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />


	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
		<head>
			<title>PIMS</title>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />


			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table_ff}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

		</head>

		<body dir="${sessionScope.CurrentLocale.dir}"
			lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">

            <div class="containerDiv">

			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
			
				<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99.2%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<c:choose>
									<c:when
										test="${sessionScope.CurrentLocale.languageCode eq 'en'}">
										<td 
											background="../resources/images/Grey panel/Grey-Panel-left-1.jpg"
											height="38" style="background-repeat: no-repeat;"
											width="100%">
											<font
												style="font-family: Trebuchet MS; font-weight: regular; font-size: 19px; margin-left: 15px; top: 30px;">
												</font>
											<!--<IMG src="../resources/images/Grey panel/Grey-Panel-left-1.jpg"/>-->
									</c:when>
									<c:otherwise>
										<td width="100%" >
											<IMG height="35"
												src="../resources/images/ar/Detail/Grey Panel/Grey-Panel-right-1.jpg"></img>
									</c:otherwise>
								</c:choose>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" height="450"
							cellspacing="0" border="0">
							<tr valign="top">
									<td width="500px" height="100%" valign="top" nowrap="nowrap">

									
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<h:form>
<!-- 
										<h:commandButton styleClass="BUTTON" style="width:100px"
											value="Generate Floors"
											actionListener="#{pages$GenerateFloors.showGenerateFloorsPopup}"></h:commandButton>
											
											<h:commandButton styleClass="BUTTON" style="width:100px"
											value="Generate Units"
											actionListener="#{pages$GenerateUnits.showGenerateUnitsPopup}"></h:commandButton>
											
											<h:commandButton styleClass="BUTTON" style="width:100px"
											value="Add Unit"
											action="#{pages$UnitDetails.NavigateToAddUnit}"></h:commandButton>
											
	 -->																			
									</h:form>

								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
	</html>
</f:view>
