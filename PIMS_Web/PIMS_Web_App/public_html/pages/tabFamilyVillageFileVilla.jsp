<t:div id="tabVillaDetails" style="width:100%;">
	<t:panelGrid id="ddVillaTab" cellpadding="5px" width="100%"
		cellspacing="10px" columns="4" styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="villaNumber" styleClass="LABEL"
				value="#{msg['familyVillagefile.lbl.villaNumber']}:"></h:outputLabel>
		</h:panelGroup>
		<h:panelGroup>
			<h:inputText
				value="#{pages$familyVillageRequest.requestView.inheritanceFileView.familyVillageVillaView.villaNumber}" />
			<h:graphicImage url="../resources/images/magnifier.gif"
				onclick="javaScript:openFamilyVillageVillaSearchPopup();"
				style="MARGIN: 0px 0px -4px;" title="#{msg['commons.search']}"
				alt="#{msg['commons.search']}"></h:graphicImage>
		</h:panelGroup>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="selectOneVillaCategoryll" styleClass="LABEL"
				value="#{msg['familyVillagefile.lbl.category']}:"></h:outputLabel>
		</h:panelGroup>
		<h:selectOneMenu id="selectOneVillaCategory"
			disabled="#{pages$familyVillageRequest.pageMode=='PAGE_MODE_REJECTED'}"
			value="#{pages$familyVillageRequest.selectOneVillaCategory}">

			<f:selectItem itemLabel="#{msg['commons.pleaseSelect']}"
				itemValue="-1" />
			<f:selectItems value="#{pages$ApplicationBean.villaCategory}" />
		</h:selectOneMenu>

		<h:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*" />
			<h:outputLabel id="address" styleClass="LABEL"
				value="#{msg['familyVillagefile.lbl.address']}:"></h:outputLabel>
		</h:panelGroup>
		<t:panelGroup colspan="3" style="width: 100%">
			<t:inputTextarea style="width: 90%;" id="txtAddress"
				value="#{pages$familyVillageRequest.requestView.inheritanceFileView.familyVillageVillaView.villaAddress}"
				onblur="javaScript:validate();" onkeyup="javaScript:validate();"
				onmouseup="javaScript:validate();"
				onmousedown="javaScript:validate();"
				onchange="javaScript:validate();"
				onkeypress="javaScript:validate();" />
		</t:panelGroup>


	</t:panelGrid>

</t:div>


