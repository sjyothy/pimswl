  
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">        
        
	    function resetValues()
      	{
      		document.getElementById("searchFrm:txtPlanNumber").value="";
      	    document.getElementById("searchFrm:txtPlanNameAr").value="";
      	    document.getElementById("searchFrm:txtPlanNameEn").value="";
			document.getElementById("searchFrm:cmbPlanStatus").selectedIndex=0;	
			$('searchFrm:createDateto').component.resetSelectedDate();
      	    $('searchFrm:createDatefrom').component.resetSelectedDate();
        }
        
        function passSelectedStrategicPlanView()
        {
        	window.opener.receiveSelectedStrategicPlanView();
        	window.close();
        }
        
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>

</head>
	<body class="BODY_STYLE">
	<div class="containerDiv">
	<%
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header --> 
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			  <c:choose>
			<c:when test="${!pages$strategicPlanSearch.isViewModePopUp}">
			<td colspan="2">
				<jsp:include page="header.jsp" />
			</td>
			
		     </c:when>
		     </c:choose>
		</tr>
		<tr >
		      <c:choose>
			<c:when test="${!pages$strategicPlanSearch.isViewModePopUp}">
			<td class="divLeftMenuWithBody" width="17%">
				<jsp:include page="leftmenu.jsp" />
			</td>
		     </c:when>
		     </c:choose>
			
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['strategicPlanSearch.header']}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>
				<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%">
					<tr valign="top">
						<td width="100%" height="100%">
							<div class="SCROLLABLE_SECTION AUC_SCH_SS">
									<h:form id="searchFrm" style="WIDTH: 97.6%;">
									<t:div styleClass="MESSAGE">
										<table border="0" class="layoutTable">
											<tr>
												<td>
													<h:outputText value="#{pages$strategicPlanSearch.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$strategicPlanSearch.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
									</t:div>
								<div class="MARGIN"> 
									<table cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID" /></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>
									<div class="DETAIL_SECTION">
										<h:outputLabel value="#{msg['commons.searchCriteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
										<table cellpadding="1px" cellspacing="2px" class="DETAIL_SECTION_INNER">
															<tr>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['strategicPlanSearch.searchCriteria.planNumber']}: "></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="txtPlanNumber"
																		styleClass="INPUT_TEXT"
																		binding="#{pages$strategicPlanSearch.htmlInputPlanNumber}"
																		tabindex="1">
																	</h:inputText>
																</td>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['strategicPlanSearch.searchCriteria.planStatus']}: "></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:selectOneMenu id="cmbPlanStatus" tabindex="2"
																		binding="#{pages$strategicPlanSearch.htmlSelectOnePlanStatus}"
																		styleClass="SELECT_MENU">
																		<f:selectItem itemLabel="#{msg['commons.All']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.strategicPlanStatus}" />
																	</h:selectOneMenu>
																</td>
															</tr>
															<tr>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['strategicPlanSearch.searchCriteria.planNameEn']}: "></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="txtPlanNameEn"
																		styleClass="INPUT_TEXT"
																		binding="#{pages$strategicPlanSearch.htmlInputPlanameEn}"
																		tabindex="3">
																	</h:inputText>
																</td>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['strategicPlanSearch.searchCriteria.planNameAr']}: "></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<h:inputText id="txtPlanNameAr"
																		styleClass="INPUT_TEXT"
																		binding="#{pages$strategicPlanSearch.htmlInputPlanameAr}"
																		tabindex="4">
																	</h:inputText>
																</td>
															</tr>
															<tr>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['strategicPlanSearch.searchCriteria.creationDateFrom']}: "></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<rich:calendar id="createDatefrom" popup="true"
																  	binding="#{pages$strategicPlanSearch.htmlCalendarCreationDateFrom}"
																	datePattern="#{pages$strategicPlanSearch.dateFormat}"
																	timeZone="#{pages$strategicPlanSearch.timeZone}"																	
																	showApplyButton="false"																	
																	enableManualInput="false" cellWidth="24px"
																	cellHeight="22px" />
																</td>
																<td class="DET_SEC_TD">
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['strategicPlanSearch.searchCriteria.creationDateTo']}: "></h:outputLabel>
																</td>
																<td class="DET_SEC_TD">
																	<rich:calendar id="createDateto" popup="true"
																  	binding="#{pages$strategicPlanSearch.htmlCalendarCreationDateTo}"
																	datePattern="#{pages$strategicPlanSearch.dateFormat}"																	
																	timeZone="#{pages$strategicPlanSearch.timeZone}"
																	showApplyButton="false"																	
																	enableManualInput="false" cellWidth="24px"
																	cellHeight="22px" />
																</td>
															</tr>

															<tr>
												<td class="BUTTON_TD" colspan="10" >
												
												<pims:security screen="Pims.InvestmentManagement.SearchStrategicPlan.Search" action="create">
												<h:commandButton styleClass="BUTTON" value="#{msg['commons.search']}" action="#{pages$strategicPlanSearch.btnSearch_Click}" style="width: 75px" ></h:commandButton>
												</pims:security>		
											   	<h:commandButton styleClass="BUTTON" type="button" value="#{msg['commons.clear']}" onclick="javascript:resetValues();" style="width: 75px" ></h:commandButton>
											   	<pims:security screen="Pims.InvestmentManagement.SearchStrategicPlan.AddPlan" action="create">
												<h:commandButton styleClass="BUTTON" value="#{msg['strategicPlanSearch.buttons.addPlan']}" action="#{pages$strategicPlanSearch.onAddPlan}" 
												style="width: 90px" tabindex="6"></h:commandButton>							
												</pims:security>				   		
												</td>
											</tr>
										</table>
								</div>	
								</div>
								 <div style="padding-bottom:7px;padding-left:10px;padding-right:7px;padding-top:7px;">   
                                      <div class="imag">&nbsp;</div>
										<div class="contentDiv">
											<t:dataTable id="dt1"
												value="#{pages$strategicPlanSearch.strategicPlanViewList}"
												binding="#{pages$strategicPlanSearch.dataTable}"
												rows="#{pages$strategicPlanSearch.paginatorRows}" 
												preserveDataModel="false" preserveSort="false" var="dataItem"
												rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
												width="100%">

												<t:column id="planNumberCol" sortable="true" defaultSorted="true" >
													<f:facet  name="header">
														<t:outputText value="#{msg['strategicPlanSearch.grid.planNumber']}" />
													</f:facet>
													<t:outputText value="#{dataItem.strategicPlanNumber}" styleClass="A_LEFT"/>
												</t:column>
												<t:column id="planNameAr" sortable="true" >
													<f:facet  name="header">
														<t:outputText value="#{msg['strategicPlanSearch.grid.planNameAr']}"  />
													</f:facet>
													<t:outputText value="#{dataItem.strategicPlanNameAr}" styleClass="A_RIGHT"/>
												</t:column>
												<t:column id="planNameEn" sortable="true" >
													<f:facet  name="header">
														<t:outputText value="#{msg['strategicPlanSearch.grid.planNameEn']}"  />
													</f:facet>
													<t:outputText value="#{dataItem.strategicPlanNameEn}" styleClass="A_LEFT"/>
												</t:column>
												<t:column id="creationDate"  sortable="true" width="85">
													<f:facet name="header">
														<t:outputText value="#{msg['strategicPlanSearch.grid.creationDate']}" />														
														
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.strategicPlanCreationDate}" >
													<f:convertDateTime
																	pattern="#{pages$strategicPlanSearch.dateFormat}"																	
																	timeZone="#{pages$strategicPlanSearch.timeZone}" />
														</t:outputText>
													
												</t:column>
																								
												<t:column id="statusEn" sortable="true" rendered="#{pages$strategicPlanSearch.isEnglishLocale}">
													<f:facet  name="header">
														<t:outputText value="#{msg['strategicPlanSearch.grid.status']}" />
													</f:facet>
													<t:outputText value="#{dataItem.statusEn}" styleClass="A_LEFT"/>
												</t:column>
													
												<t:column id="statusAr" sortable="true" rendered="#{pages$strategicPlanSearch.isArabicLocale}">
													<f:facet  name="header">
														<t:outputText value="#{msg['strategicPlanSearch.grid.status']}" />
													</f:facet>
													<t:outputText value="#{dataItem.statusAr}" styleClass="A_LEFT"/>
												</t:column>											
												
												<t:column id="actionCol" sortable="false" width="15%"
																rendered="#{pages$strategicPlanSearch.isFullMode}"
																styleClass="ACTION_COLUMN">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.action']}" />
																</f:facet>
																
																<pims:security screen="Pims.InvestmentManagement.SearchStrategicPlan.ViewPlan" action="create">
														<t:commandLink
																	action="#{pages$strategicPlanSearch.onView}">
																	<h:graphicImage id="viewIcon"
																		title="#{msg['commons.view']}"
																		url="../resources/images/app_icons/Lease-contract.png" />&nbsp;
														</t:commandLink>
														</pims:security>

																<t:outputLabel value=" " rendered="true"></t:outputLabel>
																
																<pims:security screen="Pims.InvestmentManagement.SearchStrategicPlan.EditPlan" action="create">
																<t:commandLink
																	action="#{pages$strategicPlanSearch.onEdit}"
																	rendered="#{dataItem.canEdit}">
																	<h:graphicImage id="editIcon"
																		title="#{msg['commons.edit']}"
																		url="../resources/images/application_form_edit.png" />&nbsp;
														</t:commandLink>
														</pims:security>
																<t:outputLabel value=" " rendered="true"></t:outputLabel>
																<pims:security screen="Pims.InvestmentManagement.SearchStrategicPlan.ClosePlan" action="create">
																<t:commandLink
																	action="#{pages$strategicPlanSearch.onClose}"
																	rendered="#{dataItem.canClose}">
																	<h:graphicImage title="#{msg['strategicPlanSearch.grid.actions.close']}"
																		url="../resources/images/app_icons/Cancel-Contract.png" />&nbsp;
														</t:commandLink>
														</pims:security>
																<t:outputLabel value=" " rendered="true"></t:outputLabel>
																<pims:security screen="Pims.InvestmentManagement.SearchStrategicPlan.DeletePlan" action="create">
																<t:commandLink
																	onclick="if (!confirm('#{msg['strategicPlanSearch.grid.actions.delete.confirm']}')) return"
																	rendered="#{dataItem.canDelete}"
																	action="#{pages$strategicPlanSearch.onDelete}">
																	<h:graphicImage id="deleteIcon"
																		title="#{msg['commons.delete']}"
																		url="../resources/images/delete_icon.png" />&nbsp;
														</t:commandLink>
														</pims:security>
															</t:column>
															
															<t:column rendered="#{pages$strategicPlanSearch.isPageModeSelectOnePopUp}" sortable="false" width="15%">
																<f:facet name="header">
																	<t:outputText value="#{msg['commons.select']}" />
																</f:facet>
																<t:commandLink action="#{pages$strategicPlanSearch.onSingleSelect}">															
																	<h:graphicImage title="#{msg['commons.select']}" url="../resources/images/select-icon.gif" />&nbsp;
																</t:commandLink>
															</t:column>
															
														</t:dataTable>
										</div>
										<t:div styleClass="contentDivFooter" style="width:99%;" >
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td class="RECORD_NUM_TD">
													<div class="RECORD_NUM_BG">
														<table cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
														<tr><td class="RECORD_NUM_TD">
														<h:outputText value="#{msg['commons.recordsFound']}"/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value=" : "/>
														</td><td class="RECORD_NUM_TD">
														<h:outputText value="#{pages$strategicPlanSearch.recordSize}"/>
														</td></tr>
														</table>
													</div>
												</td>
												<td class="BUTTON_TD" style="width:53%;#width:50%;" align="right">  
												<CENTER>
													<t:dataScroller id="scroller" for="dt1" paginator="true"  
														fastStep="1" paginatorMaxPages="#{pages$strategicPlanSearch.paginatorMaxPages}" immediate="false"
														paginatorTableClass="paginator"
														renderFacetsIfSinglePage="true" 												
														paginatorTableStyle="grid_paginator" layout="singleTable" 
														paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
														paginatorActiveColumnStyle="font-weight:bold;"
														paginatorRenderLinkForActive="false" 
														pageIndexVar="pageNumber"
														styleClass="SCH_SCROLLER">
														<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblF"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblL"></t:graphicImage>
														</f:facet>
														<t:div styleClass="PAGE_NUM_BG">
												<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>	 		<table cellpadding="0" cellspacing="0">
																<tr>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" value="#{msg['commons.page']}"/>
																	</td>
																	<td>	
																		<h:outputText styleClass="PAGE_NUM" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px" value="#{requestScope.pageNumber}"/>
																	</td>
																</tr>					
															</table>
															
														</t:div>
													</t:dataScroller>
													</CENTER>
													
												</td></tr>
											</table>
									</t:div>
									</div>	
									</div>
								
							</h:form>
							
					  </div> 
						</td>			
					</tr>
				</table>
			</td>
		</tr>
		<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
	</table>
</div>
</body>
</html>
</f:view>