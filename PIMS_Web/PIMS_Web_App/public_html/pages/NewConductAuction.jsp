<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript" src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript"> 
		
 		
 		function onProcessStart()
		{
		    document.getElementById('conductAuctionForm:disablingDiv').style.display='block';
			document.getElementById('conductAuctionForm:bidderChanged').onclick();
			return true;
		}
 		function onProcessComplete()
	    {
	      document.getElementById('conductAuctionForm:disablingDiv').style.display='none';
	       if ( document.getElementById('conductAuctionForm:Attendance:selectAll') != null && document.getElementById('conductAuctionForm:Attendance:selectAll').checked )
				document.getElementById('conductAuctionForm:Attendance:selectAll').checked = !document.getElementById('conductAuctionForm:Attendance:selectAll').checked;
	    }
 		function showBidderUnitPopup()
        {
              var screen_width = 1024;
              var screen_height = 450;
              var popup_width = screen_width-110;
              var popup_height = screen_height-120;
              var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
              
              var popup = window.open('bidderAuctionUnitDetails.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,resizable=no,titlebar=no,dialog=yes');
              popup.focus();
        }
		
		

		function checkAllChkBox()
		{	
			var inputElements = document.getElementsByTagName("input");
			
			if (inputElements == null)
				return;
    
		    for ( var counter = 0; counter < inputElements.length; counter++ )    
		        if ( inputElements[counter].type == 'checkbox' && ! inputElements[counter].readOnly && ! inputElements[counter].disabled )            
		            if(document.getElementById("conductAuctionForm:Attendance:selectAll")!=null)
		            	inputElements[counter].checked = document.getElementById("conductAuctionForm:Attendance:selectAll").checked;
		}
		
		

		function showVenuePopup()
        {
              var screen_width = screen.width;
              var screen_height = screen.height;
              var popup_width = screen_width-220;
              var popup_height = screen_height-228;
              var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
              window.open('auctionVenue.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
        }
        function openUnitsPopup(unitId)
		{
		      var screen_width = 1024;
		      var screen_height = 450;
		      var popup_width = screen_width-250;
		      var popup_height = screen_height;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('editUnitPopup.jsf?UNIT_ID='+unitId+'&readOnlyMode=true','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
				
			<script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>
			<script language="javascript" type="text/javascript">
					var attendanceIds;	
					var unitIdsToExclude;
					
					function SelectBidder(field,attId)
					{
				    	//alert(field.checked); = true print hota hai	   
					   var attFlag = "";
				   		if(field.checked)
				   			attFlag = "1";
				   		else
				   			attFlag = "0";
				   			
						var newID = ":" + attId;
						
						if(attendanceIds == null)
						{
							attendanceIds = attFlag + ":" + attId;
						}
						else
						{
							if(attendanceIds.indexOf(newID) != -1)
							{
								var delimiters=",";
								var attArr=attendanceIds.split(delimiters);
								attendanceIds="";
								for(i=0;i<attArr.length;i++)
								{
									   var att = newID;
									   if(attFlag == "1")
									   		att = "0"+att;	
									   else
									   		att = "1"+att;
									   		
									   if(attArr[i] == att)
									    {
								       	   if(attendanceIds.length>0)
					       	       	   			attendanceIds = attendanceIds + "," + attFlag + newID;
				       	       	   			else
				       	       	   				attendanceIds = attFlag + newID;
									    }
									    else
									    {
									    	if(attendanceIds.length>0)
									    		attendanceIds = attendanceIds + ","  + attArr[i];
									    	else
									    		attendanceIds = attArr[i];
									    }		    
								}		
							}
							else
							{
								attendanceIds = attendanceIds + "," + attFlag + ":" + attId;
							}
						}
				   	   document.getElementById("conductAuctionForm:attendanceIds").value = attendanceIds;
				   	   //alert('attendance to update:'+document.getElementById("conductAuctionForm:attendanceIds").value);
					}
					
					function exclude(field,unitId)
					{
						var exclusionFlag = "";
				   		if(field.checked)
				   			exclusionFlag = "1";
				   		else
				   			exclusionFlag = "0";
				   			
						var newID = ":" + unitId;
						
						if(unitIdsToExclude == null)
						{
							unitIdsToExclude = exclusionFlag + ":" + unitId;
						}
						else
						{
							if(unitIdsToExclude.indexOf(newID) != -1)
							{
								var delimiters=",";
								var Arr=unitIdsToExclude.split(delimiters);
								unitIdsToExclude="";
								for(i=0;i<unitsArr.length;i++)
								{
									   var unit = newID;
									   if(exclusionFlag == "1")
									   		unit = "0"+unit;	
									   else
									   		unit = "1"+unit;
									   		
									   if(unitsArr[i] == unit)
									    {
								       	   if(unitIdsToExclude.length>0)
					       	       	   			unitIdsToExclude = unitIdsToExclude + "," + exclusionFlag + newID;
				       	       	   			else
				       	       	   				unitIdsToExclude = exclusionFlag + newID;
									    }
									    else
									    {
									    	if(unitIdsToExclude.length>0)
									    		unitIdsToExclude = unitIdsToExclude + ","  + unitsArr[i];
									    	else
									    		unitIdsToExclude = unitsArr[i];
									    }		    
								}		
							}
							else
							{
								unitIdsToExclude = unitIdsToExclude + "," + exclusionFlag + ":" + unitId;
							}
						}		
				   	   document.getElementById("conductAuctionForm:unitIdsToExclude").value = unitIdsToExclude;   	   
				   	   //alert('units to update:'+document.getElementById("conductAuctionForm:unitIdsToExclude").value);
					}
					
					function showconfirm(msg)
					{
						alert(msg);
					}
				</script>
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
				response.setHeader("Pragma", "no-cache"); //HTTP 1.0
				response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
		</head>
		<body class="BODY_STYLE">
		<div class="containerDiv">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>
				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody" height="470px">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{pages$NewConductAuction.heading}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0" >
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" valign="top" nowrap="nowrap">




									
										<h:form id="conductAuctionForm" enctype="multipart/form-data">
										<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
										<div class="SCROLLABLE_SECTION" >
											<h:inputHidden id="attendanceIds"
												value="#{pages$NewConductAuction.hiddenAttendanceIds}"></h:inputHidden>
											<h:inputHidden id="unitIdsToExclude"
												value="#{pages$NewConductAuction.hiddenUnitsToExclude}"></h:inputHidden>
											<h:inputHidden id="isResultPopupCalled"
												value="#{pages$NewConductAuction.hdnResultPopupCalled}"></h:inputHidden>
											<h:inputHidden id="tabIndex"
												value="#{pages$NewConductAuction.index}"></h:inputHidden>
											<div >
												<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
													<tr>
														<td>
															<h:outputText value="#{pages$NewConductAuction.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
															<h:outputText value="#{pages$NewConductAuction.infoMessage}"  escape="false" styleClass="INFO_FONT"/>
														</td>
													</tr>
												</table>
											</div>
											<br>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="98%">
													<tr>
														<td style="font-size: 0px;">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td style="font-size: 0px;" width="100%">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="font-size: 0px;">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>

												<rich:tabPanel style="width:98%;height:240px" binding="#{pages$NewConductAuction.tabPanel}">

													<!--  Auction Details - Tab (Start) -->
													<rich:tab id="auctionDetailsTab" label="#{msg['auction.details']}"
														action="#{pages$NewConductAuction.auctionDetailsAction}">
														<t:panelGrid columns="4" cellspacing="10px">
															<h:outputLabel value="#{msg['auction.number']}" />
															<h:inputText id="txtAuctionNo" readonly="true"
																styleClass="INPUT_TEXT"
																value="#{pages$NewConductAuction.auctionNo}" />

															<h:outputLabel value="#{msg['auction.status']}" />
															<h:inputText id="txtAuctionStatus"
																styleClass="INPUT_TEXT"
																readonly="true"
																value="#{pages$NewConductAuction.auctionStatus}" />

															<h:outputLabel value="#{msg['auction.title']}" />
															<h:inputText id="txtAuctionDescription" readonly="true"
																styleClass="INPUT_TEXT"
																value="#{pages$NewConductAuction.auctionTitle}" />

															<h:outputLabel value="#{msg['auction.venue']}" />
															<t:panelGroup>
																<h:inputText id="txtAuctionVenue" styleClass="INPUT_TEXT" readonly="true" value="#{pages$NewConductAuction.auctionVenueName}" maxlength="50" tabindex="4"/>
																<h:commandButton actionListener="#{pages$NewConductAuction.selectVenue}" rendered="#{!pages$NewConductAuction.isViewMode && !pages$NewConductAuction.isRefundMode}"
																	style="width: 25px; height:20px; border-width:0px;" tabindex="8" image="../resources/images/magnifier.gif" />
															</t:panelGroup>

															<h:outputLabel value="#{msg['auction.date']}" />
															<rich:calendar id="txtAuctionDate" value="#{pages$NewConductAuction.auctionDate}" disabled="#{pages$NewConductAuction.isViewMode || pages$NewConductAuction.isRefundMode}"
																popup="true" datePattern="#{pages$NewConductAuction.dateFormat}" showApplyButton="false"
																locale="#{pages$NewConductAuction.locale}"
																enableManualInput="false" cellWidth="24px"/>

															<h:outputLabel value="#{msg['auction.time']}" />
															<h:inputText id="txtAuctionTime"
																readonly="true"
																styleClass="INPUT_TEXT" 
																value="#{pages$NewConductAuction.auctionTime}" />

															<h:outputLabel value="#{msg['auction.outbiddingValue']}" />
															<h:inputText id="txtAuctionOutbiddingValue"
																readonly="true"
																styleClass="INPUT_TEXT" 
																value="#{pages$NewConductAuction.outbiddingValue}" />

															<h:outputLabel
																value="#{msg['auction.confiscationPercentage']}" />
															<h:inputText id="txtAuctionConfiscationPercentage"
																readonly="true"
																styleClass="INPUT_TEXT" 
																value="#{pages$NewConductAuction.confiscationPercentage}" />

															<h:outputLabel value="#{msg['auction.publishingDate']}" />

															<h:inputText id="txtAuctionPublishingDate"
																styleClass="INPUT_TEXT"
																readonly="true"
																value="#{pages$NewConductAuction.publishingDate}">
															</h:inputText>

															<h:outputLabel
																value="#{msg['auction.registrationPeriod']}" />
															<t:panelGroup>
																<h:inputText id="txtAuctionRegistrationStartDate"
																	readonly="true"
																	styleClass="INPUT_TEXT"
																	style="width: 85px;"
																	value="#{pages$NewConductAuction.auctionStartDate}"/>
																<h:outputLabel value=" - " />
																<h:inputText id="txtAuctionRegistrationEndDate"
																	styleClass="INPUT_TEXT"
																	style="width: 85px;"
																	readonly="true"
																	value="#{pages$NewConductAuction.auctionEndDate}"/>
															</t:panelGroup>
														</t:panelGrid>
													</rich:tab>
													<!--  Auction Details - Tab (End) -->

													<!--  Units - Tab (Start) -->
													<rich:tab id="auctionUnitsTab"
														label="#{msg['auction.conductAuction.unitToExclude']}"
														action="#{pages$NewConductAuction.unitExclusionAction}">
														<t:div id="exclusionDiv" styleClass="contentDiv"
															style="width: 97.9%">

															<t:dataTable id="UnitExclusion"
																value="#{pages$NewConductAuction.dataListUnitsToExclude}"
																binding="#{pages$NewConductAuction.dataTableUnitsToExclude}"
																rows="20" preserveDataModel="true" preserveSort="false"
																var="dataItemUnitsToExclude" rowClasses="row1,row2"
																rules="all" renderedIfEmpty="true" width="100%">

																<t:column id="colSelect"  rendered="#{!pages$NewConductAuction.isViewMode && !pages$NewConductAuction.isRefundMode}">
																	<h:selectBooleanCheckbox styleClass="checkBox" id="select"
																		value="#{dataItemUnitsToExclude.excluded}"
																		readonly="#{pages$NewConductAuction.isViewMode}"
																		disabled="#{dataItemUnitsToExclude.excluded}"																		
																		/>
																</t:column>
                                                                 <t:column  id ="colStatusEn" rendered="#{pages$NewConductAuction.isEnglishLocale}">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.status']}" />
																	</f:facet>
                                                                        <t:outputText styleClass="A_LEFT" value="#{dataItemUnitsToExclude.auctionUnitStatusEn}" style="white-space: normal;" />																	
																</t:column>
																<t:column  id ="colStatusAr"rendered="#{!pages$NewConductAuction.isEnglishLocale}">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.status']}" />
																	</f:facet>
                                                                        <t:outputText styleClass="A_LEFT" value="#{dataItemUnitsToExclude.auctionUnitStatusAr}" style="white-space: normal;" />																	
																</t:column>
																
																<t:column id="colPropertyRefNo" >
																	<f:facet name="header">
																		<t:outputText value="#{msg['property.refNumber']}" style="white-space: normal;" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{dataItemUnitsToExclude.propertyRefNumber}" style="white-space: normal;" />
																</t:column>

																<t:column id="colPropertyCommName" >
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['property.commercialName']}"  />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{dataItemUnitsToExclude.commercialName}"  style="white-space: normal;"/>
																</t:column>

																<t:column id="colUnitNo" >
																	<f:facet name="header">
																		<t:outputText value="#{msg['unit.number']}"  />
																	</f:facet>
																	<h:commandLink onclick="javascript:openUnitsPopup('#{dataItemUnitsToExclude.unitId}');" >
																	  <t:outputText styleClass="A_LEFT"
																		value="#{dataItemUnitsToExclude.unitNumber}" style="white-space: normal;" />
																	</h:commandLink>
																</t:column>
																<t:column id="colUnitAccNumber" sortable="true" >
																	<f:facet name="header">
																		<t:outputText value="#{msg['auction.unitTab.columnUnitAccountNumber']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"  value="#{dataItemUnitsToExclude.unitAccountNumber}" style="white-space: normal;"/>
																</t:column>
																<t:column id="colUnitDesc" sortable="true" >
																	<f:facet name="header">
																		<t:outputText value="#{msg['unit.unitDescription']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" value="#{dataItemUnitsToExclude.unitDesc}" style="white-space: normal;"/>
																</t:column>
																<t:column id="colUnitTypeEn"  rendered="#{pages$NewConductAuction.isEnglishLocale}">
																	<f:facet name="header">
																		<t:outputText value="#{msg['unit.type']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{dataItemUnitsToExclude.unitTypeEn}" style="white-space: normal;"/>
																</t:column>
																<t:column id="colUnitTypeAr"  rendered="#{pages$NewConductAuction.isArabicLocale}">
																	<f:facet name="header">
																		<t:outputText value="#{msg['unit.type']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{dataItemUnitsToExclude.unitTypeAr}" style="white-space: normal;" />
																</t:column>
																<t:column >
																	<f:facet name="header">
																		<t:outputText value="#{msg['facility.rentValue']}" />
																	</f:facet>
																	<t:outputText styleClass="A_RIGHT_NUM"
																		value="#{dataItemUnitsToExclude.rentValue}" />
																</t:column>
																<t:column >
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['auctionUnit.openingPrice']}" />
																	</f:facet>
																	<t:outputText styleClass="A_RIGHT_NUM"
																		value="#{dataItemUnitsToExclude.openingPrice}" />
																</t:column>
																<t:column id="colDepositAmount" >
																	<f:facet name="header">
																		<t:outputText value="#{msg['unit.depositAmount']}" />
																	</f:facet>
																	<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItemUnitsToExclude.depositAmount}" style="white-space: normal;">
																		<f:convertNumber pattern="#{pages$NewConductAuction.numberFormat}"/>
																	</t:outputText>
																</t:column>
																<t:column >
																	<f:facet name="header">
																		<t:outputText value="#{msg['auctionUnit.winnerName']}" />
																	</f:facet>
																	<h:commandLink onclick="javascript:showPersonReadOnlyPopup('#{dataItemUnitsToExclude.winnerId}');" >
																	   <t:outputText styleClass="A_LEFT" value="#{dataItemUnitsToExclude.winner}" style="white-space: normal;" />
																	</h:commandLink>
																</t:column>
																<t:column >
																	<f:facet name="header">
																		<t:outputText value="#{msg['auctionUnit.grid.exclusionReason']}" />
																	</f:facet>
                                                                        <t:inputTextarea disabled="#{pages$NewConductAuction.isViewMode || pages$NewConductAuction.isRefundMode}" styleClass="TEXTAREA" value="#{dataItemUnitsToExclude.exclusionReason}" rows="1" style="width: 100px;height:35px;"/>																	
																	   
																	
																</t:column>
																<t:column id="colBidPrice" >
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['auction.conductAuction.bidPrice']}" />
																	</f:facet>
																	<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItemUnitsToExclude.bidPrice}" />
																</t:column>
																<t:column id="colEditWinner"  rendered="#{!pages$NewConductAuction.isRefundMode && !pages$NewConductAuction.isViewMode}">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.action']}" />
																	</f:facet>
																	<t:commandLink
																		action="#{pages$NewConductAuction.editRequest}"
																		rendered= "#{!dataItemUnitsToExclude.excluded && dataItemUnitsToExclude.registered}"
																		onclick="if (!confirm('#{msg['auction.messages.confirmWinner']}')) return false">
																	<h:graphicImage id="selectWinnerIcon"
																			title="#{msg['auction.conductAuction.editBidWinner']}"
																			url="../resources/images/app_icons/Dclare-Auction-Winner.png" />
																	</t:commandLink>
																</t:column>
															</t:dataTable>

														</t:div>
														<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
															style="width:98.8%;">
															<t:panelGrid cellpadding="0" cellspacing="0" width="100%"
																columns="2" columnClasses="RECORD_NUM_TD">

																<t:div styleClass="RECORD_NUM_BG">
																	<t:panelGrid cellpadding="0" cellspacing="0"
																		columns="3" columnClasses="RECORD_NUM_TD">

																		<h:outputText value="#{msg['commons.recordsFound']}" />

																		<h:outputText value=" : " styleClass="RECORD_NUM_TD" />

																		<h:outputText
																			value="#{pages$NewConductAuction.unitExclusionRecordSize}"
																			styleClass="RECORD_NUM_TD" />

																	</t:panelGrid>
																</t:div>

																<t:dataScroller id="unitExclusionScroller"
																	for="UnitExclusion" paginator="true" fastStep="1"
																	paginatorMaxPages="2" immediate="false"
																	paginatorTableClass="paginator"
																	renderFacetsIfSinglePage="true"
																	paginatorTableStyle="grid_paginator"
																	layout="singleTable"
																	paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																	paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																	pageIndexVar="unitExclusionPageNumber">

																	<f:facet name="first">
																		<t:graphicImage
																			url="../resources/images/first_btn.gif"
																			id="UnitExclusionlblF"></t:graphicImage>
																	</f:facet>
																	<f:facet name="fastrewind">
																		<t:graphicImage
																			url="../resources/images/previous_btn.gif"
																			id="UnitExclusionlblFR"></t:graphicImage>
																	</f:facet>
																	<f:facet name="fastforward">
																		<t:graphicImage url="../resources/images/next_btn.gif"
																			id="UnitExclusionlblFF"></t:graphicImage>
																	</f:facet>
																	<f:facet name="last">
																		<t:graphicImage url="../resources/images/last_btn.gif"
																			id="UnitExclusionlblL"></t:graphicImage>
																	</f:facet>
																	<t:div styleClass="PAGE_NUM_BG">
																		<h:outputText styleClass="PAGE_NUM"
																			value="#{msg['commons.page']}" />
																		<h:outputText styleClass="PAGE_NUM"
																			style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																			value="#{requestScope.unitExclusionPageNumber}" />
																	</t:div>
																</t:dataScroller>
															</t:panelGrid>
														</t:div>
														<h:panelGrid id="actionGridUnitStatus" columns="1" style="width:100%">
															<h:commandButton id="excludeUnits" type="submit" rendered="#{!pages$NewConductAuction.isViewMode && !pages$NewConductAuction.isRefundMode}"
																styleClass="BUTTON"	value="#{msg['auctionUnit.excludeUnits']}" 
																action="#{pages$NewConductAuction.saveUnitExclusion}" style="width:auto;"
																onclick="if (!confirm('#{msg['auction.messages.confirmExclusion']}')) return false">
															</h:commandButton>
														</h:panelGrid>
														<%-- 
														<t:panelGrid width="100%" rendered="#{!pages$NewConductAuction.isViewMode && !pages$NewConductAuction.isRefundMode}">
														      <h:outputLabel value="#{msg['auctionUnit.lbl.exclusionReason']}" />
														      <t:inputTextarea styleClass="TEXTAREA" value="#{pages$NewConductAuction.exclusionReason}" rows="6" style="width: 320px;"/>
														</t:panelGrid>
														--%>
													</rich:tab>
													<!-- Units - End -->

													<!-- Attendance Tab - Start -->
													<rich:tab id="biddersTab"
														label="#{pages$NewConductAuction.bidderTabHeading}"
														action="#{pages$NewConductAuction.attendanceAction}">
														<t:div>
														<h:outputLabel value="#{msg['bidder.firstName']} : "></h:outputLabel>
														<h:messages></h:messages>
														<h:selectOneMenu id="selectBidder" onchange="onProcessStart();"
																		value="#{pages$NewConductAuction.selectedBidder}">
																		<f:selectItem
																			itemLabel="#{msg['commons.combo.PleaseSelect']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$NewConductAuction.bidderList}"/>
																	</h:selectOneMenu>
																	<a4j:commandLink id="bidderChanged"
																			onbeforedomupdate="javascript:onProcessComplete();"
																			action="#{pages$NewConductAuction.onBidderChange}"   reRender="attDiv,record"/>
														<h:messages></h:messages>
															
														</t:div>			
														<t:div id="attDiv" styleClass="contentDiv"
															style="width: 97.9%"
															rendered="true">
															<t:dataTable id="Attendance"
																value="#{pages$NewConductAuction.dataListBidder}"
																binding="#{pages$NewConductAuction.dataTableBidder}"
																rows="20" preserveDataModel="false" preserveSort="false"
																var="dataItemBidder" rowClasses="row1,row2" rules="all"
																renderedIfEmpty="true" width="100%">

																<t:column id="col7" rendered="#{!pages$NewConductAuction.isViewMode && !pages$NewConductAuction.isRefundMode}">
																	<f:facet name="header">
																		<h:selectBooleanCheckbox styleClass="checkBox"
																		binding="#{pages$NewConductAuction.selectAll}" 
																		id="selectAll" onclick="checkAllChkBox();"/>
																	</f:facet>
																	<h:selectBooleanCheckbox styleClass="checkBox" id="select" 
																		value="#{dataItemBidder.attendance}" disabled="#{dataItemBidder.anyWin == 'Y'}" />
																</t:column>

																<t:column sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['bidder.name']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{dataItemBidder.name}" />
																</t:column>

																<t:column sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['tenants.cellnumber']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" value="#{dataItemBidder.cellNumber}" />
																</t:column>

																<t:column sortable="true">
																	<f:facet name="header">
																		<t:outputText value="#{msg['bidder.idNumber']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" value="#{dataItemBidder.socialSecNumber}" />
																</t:column>
																
																<t:column sortable="true">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['AuctionDepositRefundApproval.DataTable.TotalUnits']}" />
																	</f:facet>
																	<t:commandLink styleClass="A_RIGHT_NUM" value="#{dataItemBidder.totalAuctionUnits}" actionListener="#{pages$NewConductAuction.openBidderUnitPopup}" style="white-space: normal;" title="#{msg['auction.bidderUnitPopup.heading']}">
																	</t:commandLink>
																</t:column>
																
																<t:column sortable="true">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['AuctionDepositRefundApproval.DataTable.DepositAmount']}" />
																	</f:facet>
																	<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItemBidder.totalDepositAmount}" style="white-space: normal;">
																		<f:convertNumber pattern="#{pages$NewConductAuction.numberFormat}"/>
																	</t:outputText>
																</t:column>
																<t:column sortable="true" rendered="#{pages$NewConductAuction.isRefundMode}">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['AuctionDepositRefundApproval.DataTable.ConfiscatedAmount']}" />
																	</f:facet>
																	<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItemBidder.confiscatedAmount}" style="white-space: normal;">
																		<f:convertNumber pattern="#{pages$NewConductAuction.numberFormat}"/>
																	</t:outputText>
																</t:column>
																<t:column sortable="true" id="hasRefundedCol" width="100" rendered="#{pages$NewConductAuction.isRefundAuctionFinalMode}">
																	<f:facet name="header">
																		<t:outputText value="#{msg['auction.bidderUnitPopup.grid.hasRefundedCol']}" />
																	</f:facet>
																	<t:outputText value="#{dataItemBidder.hasRefunded}" styleClass="A_LEFT"/>
																</t:column>	
																
																<t:column sortable="true">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['commons.status']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" value="#{dataItemBidder.statusEn}" rendered="#{pages$NewConductAuction.isEnglishLocale}" />
																	<t:outputText styleClass="A_LEFT" value="#{dataItemBidder.statusAr}" rendered="#{pages$NewConductAuction.isArabicLocale}"/>
																</t:column>																	
																<t:column>
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.action']}" />
																	</f:facet>
																	<h:commandLink id="cmdBidderDetail" action="#{pages$NewConductAuction.cmdBidderEdit_Click}">
																		<h:graphicImage id="imgPerson" title="#{msg['bidder.details']}" style="MARGIN: 0px 0px -4px" url="../resources/images/app_icons/searchBidder.png"/>
																	</h:commandLink>
																	<t:commandLink actionListener="#{pages$NewConductAuction.markAsRefunded}" onclick="if (!confirm('#{msg['auction.refund.messages.confirmRefund']}')) return false" rendered="#{pages$NewConductAuction.isRefundAuctionFinalMode && dataItemBidder.hasRefunded=='No'}">
																		<h:graphicImage id="markAsRefundIcon" title="#{msg['auction.bidderUnitPopup.grid.action.markAsRefunded']}" url="../resources/images/app_icons/Refund-Deposit-Amount.png" />
																	</t:commandLink>
																</t:column>
															</t:dataTable>
														</t:div>
														<t:div id="record" styleClass="contentDivFooter AUCTION_SCH_RF"
															style="width:98.8%;">
															<t:panelGrid cellpadding="0" cellspacing="0" width="100%"
																columns="2" columnClasses="RECORD_NUM_TD">

																<t:div styleClass="RECORD_NUM_BG">
																	<t:panelGrid cellpadding="0" cellspacing="0"
																		columns="3" columnClasses="RECORD_NUM_TD">

																		<h:outputText value="#{msg['commons.recordsFound']}" />

																		<h:outputText value=" : " styleClass="RECORD_NUM_TD" />

																		<h:outputText
																			value="#{pages$NewConductAuction.attendanceRecordSize}"
																			styleClass="RECORD_NUM_TD" />

																	</t:panelGrid>
																</t:div>

																<t:dataScroller id="attendanceScroller" for="Attendance"
																	paginator="true" fastStep="1" paginatorMaxPages="10"
																	immediate="false" styleClass="scroller"
																	paginatorTableClass="paginator"
																	renderFacetsIfSinglePage="true"
																	paginatorTableStyle="grid_paginator"
																	layout="singleTable"
																	paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																	paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;"
																	pageIndexVar="attPageNumber">

																	<f:facet name="first">
																		<t:graphicImage
																			url="../resources/images/first_btn.gif"
																			id="AttendancelblF"></t:graphicImage>
																	</f:facet>
																	<f:facet name="previous">
																		<t:graphicImage
																			url="../resources/images/previous_btn.gif"
																			id="AttendancelblFR"></t:graphicImage>
																	</f:facet>
																	<f:facet name="fastforward">
																		<t:graphicImage url="../resources/images/next_btn.gif"
																			id="AttendancelblFF"></t:graphicImage>
																	</f:facet>
																	<f:facet name="last">
																		<t:graphicImage url="../resources/images/last_btn.gif"
																			id="AttendancelblL"></t:graphicImage>
																	</f:facet>
																	<t:div styleClass="PAGE_NUM_BG">
																		<h:outputText styleClass="PAGE_NUM"
																			value="#{msg['commons.page']}" />
																		<h:outputText styleClass="PAGE_NUM"
																			style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																			value="#{requestScope.attPageNumber}" />
																	</t:div>
																</t:dataScroller>

															</t:panelGrid>
														</t:div>
														<h:panelGrid id="actionGridAtt" columns="3" style="width:40%">
															<h:commandButton type="submit" styleClass="BUTTON" rendered="#{!pages$NewConductAuction.isViewMode && !pages$NewConductAuction.isRefundMode}"
																value="#{msg['bidder.markAbsent']}" action="#{pages$NewConductAuction.markAbsent}"
																style="width:auto;" />
																
															<h:commandButton type="submit" styleClass="BUTTON" rendered="#{!pages$NewConductAuction.isViewMode && !pages$NewConductAuction.isRefundMode}"
																value="#{msg['bidder.markPresent']}" action="#{pages$NewConductAuction.markPresent}"
																style="width:auto;" />
																<h:commandButton styleClass="BUTTON" 
																style="width:auto;"
																value="#{msg['auctionConduct.buttonTitle.showAllBidders']}"  
																action="#{pages$NewConductAuction.showAllBidder}">
																</h:commandButton>
														</h:panelGrid>
													</rich:tab>
													
													<f:verbatim>
														<script language="JavaScript" type="text/javascript">
															checkAllChkBox();
														</script>
													</f:verbatim>
													<!-- Attendance Tab - End -->
													
													

												    <rich:tab id="attachmentTab" label="#{msg['commons.attachmentTabHeading']}">
														<%@  include file="attachment/attachment.jsp"%>
													</rich:tab>
													<rich:tab id="commentsTab" label="#{msg['commons.commentsTabHeading']}">
														<%@ include file="notes/notes.jsp"%>
													</rich:tab>
													<rich:tab id="requestHistoryTab"
															label="#{msg['contract.tabHeading.RequestHistory']}"
															action="#{pages$NewConductAuction.tabRequestHistory_Click}">
															<t:div>
																<%@ include file="../pages/requestTasks.jsp"%>
															</t:div>
													</rich:tab>
													
													
													
												</rich:tabPanel>
												<table cellpadding="0" cellspacing="0" width="98%">
													<tr>
														<td style="font-size: 0px;">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="font-size: 0px;">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="font-size: 0px;">
															<IMG
																src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>

											</div>
											<div class="MARGIN">
												<table style="width: 98%">
													<tr>
														<td class="BUTTON_TD">
															<h:commandButton id="saveUnits" type="submit"
																styleClass="BUTTON" style="width: 90px"
																value="#{msg['commons.saveButton']}"
																action="#{pages$NewConductAuction.save}"
																rendered="#{pages$NewConductAuction.saveEnable && !pages$NewConductAuction.isViewMode && !pages$NewConductAuction.isRefundMode}">
															</h:commandButton>
															<h:commandButton id="btnSaveDocNNotes"
																style="width: 90px" type="submit" styleClass="BUTTON"
																value="#{msg['commons.saveDocNNotes']}"
																action="#{pages$NewConductAuction.save}" 
																rendered="#{pages$NewConductAuction.saveDocNNotes && !pages$NewConductAuction.isViewMode && !pages$NewConductAuction.isRefundMode}">
															</h:commandButton>
															<h:commandButton id="btnComplete" type="submit"
																styleClass="BUTTON" style="width: 120px"
																value="#{msg['auction.conductAuction.completeAuction']}"
																action="#{pages$NewConductAuction.completeAuction}"
																rendered="#{!pages$NewConductAuction.isViewMode && !pages$NewConductAuction.isRefundMode}"
																onclick="return confirm('#{msg['auction.conductAuction.confirmComplete']}');">
															</h:commandButton>
															<h:commandButton styleClass="BUTTON" value="#{msg['commons.submitButton']}"
																action="#{pages$NewConductAuction.approveRefund}" style="width: 90px"
																onclick="if (!confirm('#{msg['auction.refund.messages.confirmApprove']}')) return false"
																tabindex="19" rendered="#{!pages$NewConductAuction.isViewMode && pages$NewConductAuction.isRefundAuctionApprovalMode}">
															</h:commandButton>
															<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}" 
																 onclick="if (!confirm('#{msg['auction.refund.messages.confirmCancel']}')) return false"
													        	 action="#{pages$NewConductAuction.cancel}" style="width: 90px">
													       	</h:commandButton>
														</td>
													</tr>
												</table>
											</div>
											</div>
										</h:form>
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
	</html>	
</f:view>

