
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<SCRIPT>	
function closeWindow()
	{
	  //window.opener.document.getElementById('conductAuctionForm:isResultPopupCalled').value = "Y";
	  window.opener.document.forms[0].submit();
	  window.close();
	}
       </SCRIPT>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />

			<style>
span {
	PADDING-RIGHT: 5px;
	PADDING-LEFT: 5px;
}
</style>
			<script language="javascript" type="text/javascript">

	

	
</script>



		</head>
		<body dir="${sessionScope.CurrentLocale.dir}"
			lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<tr width="100%">

					<td width="100%" valign="top" class="divBackgroundBody"
						height="610px">
						<table width="100%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr height="1%">
								<td>
									<table width="100%" class="greyPanelTable" cellpadding="0"
										cellspacing="0" border="0">
										<tr>
											<td class="HEADER_TD">
												<h:outputLabel value="#{msg['unit.editUnit']}"
													styleClass="HEADER_FONT" />
											</td>
											<td width="100%">
												&nbsp;
											</td>
										</tr>

									</table>

									<table width="100%" class="greyPanelMiddleTable"
										cellpadding="0" cellspacing="0" border="0">
										<tr valign="top">
											<td height="100%" valign="top" nowrap="nowrap"
												background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
												width="1">
											</td>

											<td width="100%" height="576px" valign="top" nowrap="nowrap">

												<h:form id="formEditUnitPopup">
													<h:inputHidden id="floorId"
														binding="#{pages$editUnitPopup.hiddenFloorId}" />
													<h:inputHidden id="propertyId"
														binding="#{pages$editUnitPopup.hiddenPropertyId}" />
													<h:inputHidden id="unitId"
														binding="#{pages$editUnitPopup.hiddenUnitId}" />
													<h:inputHidden id="statusId"
														binding="#{pages$editUnitPopup.hiddenStatusId}" />
													<div>
														<table border="0" class="layoutTable">
															<tr>
																<td>
																	<h:outputText
																		value="#{pages$editUnitPopup.errorMessages}"
																		escape="false" styleClass="ERROR_FONT" />
																	<h:outputText
																		value="#{pages$editUnitPopup.infoMessage}"
																		escape="false" styleClass="INFO_FONT" />
																</td>
															</tr>
														</table>
													</div>


													<div class="MARGIN">

														<rich:tabPanel style="width: 100%">

															<rich:tab id="unitDetailstab"
																label="#{msg['unit.editUnit']}">

																<t:panelGrid id="ddTab" width="100%" cellpadding="1px"
																	cellspacing="2px" columns="4"
																	styleClass="TAB_DETAIL_SECTION_INNER"
																	columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.propertyReferenceNumber']}" />
																	<h:inputText id="propretyNo"
																		styleClass="READONLY INPUT_TEXT" readonly="true"
																		binding="#{pages$editUnitPopup.propertyNumber}" />

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.unitAccountNumber']}" />
																	<h:inputText id="accountNo" styleClass="INPUT_TEXT"
																		maxlength="50"
																		binding="#{pages$editUnitPopup.accountNumber}" />

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.commercialName']}"></h:outputLabel>
																	<h:inputText id="propertyCommercialName"
																		styleClass="READONLY INPUT_TEXT" readonly="true"
																		binding="#{pages$editUnitPopup.propCommercialName}" />


																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.address']}"></h:outputLabel>
																	<h:inputText id="propertyAddress"
																		styleClass="READONLY INPUT_TEXT" readonly="true"
																		binding="#{pages$editUnitPopup.propAddress}" />

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.floorNumber']}"></h:outputLabel>
																	<h:inputText id="floorNo"
																		styleClass="READONLY INPUT_TEXT" readonly="true"
																		binding="#{pages$editUnitPopup.floorNumber}" />

																	<t:panelGroup colspan="2">
																		<h:selectBooleanCheckbox readonly="true"
																			style="height: 22px; border: 0px;"
																			binding="#{pages$editUnitPopup.includeFloorNumber}"
																			value="#{pages$editUnitPopup.includeFloorNumberVar}">
																			<h:outputText
																				value="#{msg['receiveProperty.unit.addWithFloors']}" />
																		</h:selectBooleanCheckbox>
																	</t:panelGroup>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.unitPrefix']}"></h:outputLabel>
																	<h:inputText id="unitPrefix" styleClass="INPUT_TEXT"
																		binding="#{pages$editUnitPopup.unitPrefix}" />

																	<t:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unitNo']}"></h:outputLabel>
																	</t:panelGroup>
																	<h:inputText id="unitNo" readonly="true"
																		styleClass="READONLY INPUT_TEXT"
																		binding="#{pages$editUnitPopup.unitNo}" />

																	<t:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unitSide']}"></h:outputLabel>
																	</t:panelGroup>
																	<h:selectOneMenu id="unitSideMenu"
																		value="#{pages$editUnitPopup.unitSide}"
																		readonly="#{pages$editUnitPopup.isReadOnlyMode}">
																		<f:selectItem itemValue="-1"
																			itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.unitSideType}" />

																	</h:selectOneMenu>
																	<t:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unitType']}"></h:outputLabel>
																	</t:panelGroup>
																	<h:selectOneMenu id="unitTypeMenu"
																		readonly="#{pages$editUnitPopup.isReadOnlyMode}"
																		value="#{pages$editUnitPopup.unitType}">
																		<f:selectItem itemValue="-1"
																			itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.unitType}" />

																	</h:selectOneMenu>
																	<t:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unitUsageType']}"></h:outputLabel>
																	</t:panelGroup>
																	<h:selectOneMenu
																		readonly="#{pages$editUnitPopup.isReadOnlyMode}"
																		id="unitUsageTypeMenu"
																		value="#{pages$editUnitPopup.unitUsage}">
																		<f:selectItem itemValue="-1"
																			itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.unitUsageTypeList}" />

																	</h:selectOneMenu>

																	<t:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.unitInvestmentType']}"></h:outputLabel>
																	</t:panelGroup>
																	<h:selectOneMenu id="unitInvestmentTypeMenu"
																		readonly="#{pages$editUnitPopup.isReadOnlyMode}"
																		value="#{pages$editUnitPopup.investmentType}">
																		<f:selectItem itemValue="-1"
																			itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.unitInvestmentTypeList}" />

																	</h:selectOneMenu>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.areaInSquare']}"></h:outputLabel>
																	<h:inputText id="areaInSquare"
																		style="text-align: right;" styleClass="INPUT_TEXT"
																		binding="#{pages$editUnitPopup.areaInSquare}" />

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.bedRooms']}"></h:outputLabel>
																	<h:inputText id="bedRooms" style="text-align: right;"
																		styleClass="INPUT_TEXT"
																		binding="#{pages$editUnitPopup.bedrooms}" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.livingRooms']}"></h:outputLabel>

																	<h:inputText id="livingRooms"
																		style="text-align: right;" styleClass="INPUT_TEXT"
																		binding="#{pages$editUnitPopup.livingRooms}" />

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.bathRooms']}"></h:outputLabel>

																	<h:inputText id="bathRooms" style="text-align: right;"
																		styleClass="INPUT_TEXT"
																		binding="#{pages$editUnitPopup.bathrooms}" />

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.unitEWUtilityNo']}"></h:outputLabel>
																	<h:inputText id="unitEWUtilityNumber"
																		styleClass="INPUT_TEXT"
																		binding="#{pages$editUnitPopup.EWUtilityNo}" />
																	<t:panelGroup>
																		<h:outputLabel styleClass="mandatory" value="*" />
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['receiveProperty.rentValueForUnits']}"></h:outputLabel>
																	</t:panelGroup>
																	<h:inputText id="rentValueForUnits"
																		style="text-align: right;" styleClass="INPUT_TEXT"
																		binding="#{pages$editUnitPopup.rent}" />


																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.unitDescription']}"></h:outputLabel>
																	<h:inputText id="unitDescription"
																		styleClass="INPUT_TEXT"
																		binding="#{pages$editUnitPopup.description}" />

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['receiveProperty.unitRemarks']}"></h:outputLabel>
																	<h:inputText id="unitRemarks" styleClass="INPUT_TEXT"
																		binding="#{pages$editUnitPopup.unitRemarks}" />
																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['unit.ejariRefNum']}"></h:outputLabel>
																	<h:inputText id="unitEjariRefNum" styleClass="INPUT_TEXT"
																		binding="#{pages$editUnitPopup.ejariRefNum}" />

																	<pims:security
																		screen="Pims.PropertyManagement.ChangeUnitStatus"
																		action="view">

																		<h:outputLabel styleClass="LABEL"
																			value="Old Unit Status"></h:outputLabel>

																		<h:inputText styleClass="READONLY"
																			binding="#{pages$editUnitPopup.unitStatus}">
																		</h:inputText>
																		<h:outputLabel styleClass="LABEL"
																			value="#{msg['contract.unit.status']}"></h:outputLabel>

																		<h:selectOneMenu id="unitStatusMenu"
																			binding="#{pages$editUnitPopup.selectedUnitStaus}">
																			<f:selectItem itemValue="-1"
																				itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																			<f:selectItems
																				value="#{pages$ApplicationBean.unitStatusList}" />

																		</h:selectOneMenu>

																	</pims:security>

																</t:panelGrid>
																<t:panelGrid id="ddTabButton" width="100%"
																	cellpadding="1px" columns="2" styleClass="BUTTON_TD">

																	<t:panelGroup></t:panelGroup>
																	<t:panelGroup>
																		<h:commandButton styleClass="BUTTON"
																			binding="#{pages$editUnitPopup.updateButton}"
																			value="#{msg['commons.Update']}"
																			action="#{pages$editUnitPopup.updateUnit}" />

																		<h:commandButton styleClass="BUTTON"
																			value="#{msg['commons.closeButton']}"
																			action="#{pages$editUnitPopup.closePopup}" />
																	</t:panelGroup>
																</t:panelGrid>
															</rich:tab>
															<rich:tab
																label="#{msg['contract.tabHeading.RequestHistory']}"
																title="#{msg['commons.tab.requestHistory']}"
																action="#{pages$editUnitPopup.onActivityLogTab}">
																<%@ include file="../pages/requestTasks.jsp"%>
															</rich:tab>
														</rich:tabPanel>
													</div>
												</h:form>

											</td>
										</tr>
									</table>
								</td>
							</tr>


						</table>

					</td>
				</tr>
			</table>
		</body>
	</html>
</f:view>

