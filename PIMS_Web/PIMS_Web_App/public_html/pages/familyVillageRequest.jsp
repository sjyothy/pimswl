<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
function performTabClick(elementid)
	{
			
		disableInputs();
		
		document.getElementById("detailsFrm:"+elementid).onclick();
		
		return 
		
	}
	function onMessageFromSearchVilla()
	{
		
		disableInputs();
		
		document.getElementById('detailsFrm:onMessageFromSearchVilla').onclick();
	}
	function onMessageFromAddFamilyVillageBeneficiary()
	{
		disableInputs();
		document.getElementById('detailsFrm:onMessageFromAddFamilyVillageBeneficiary').onclick();
	}
	function performClick(control)
	{
			
		disableInputs();
		control.nextSibling.nextSibling.onclick();
		return 
		
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++) {
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		        )
		     {
		        inputs[i].disabled = true;
		    }
		}
	}	
		
    function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		disableInputs();	
		if( hdnPersonType=='APPLICANT' ) 
		{			    			 
			 document.getElementById("detailsFrm:hdnPersonId").value=personId;
			 document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
			 document.getElementById("detailsFrm:hdnPersonName").value=personName;
			 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
			 document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;
		}
				
	     document.getElementById("detailsFrm:onMessageFromSearchPerson").onclick();
	} 
	
	function onBenefChangeStart()
	{
	    document.getElementById('detailsFrm:disablingDiv').style.display='block';
		document.getElementById('detailsFrm:lnkBenefChange').onclick();
	}
	function onBenefChangeComplete()
	{
	 document.getElementById('detailsFrm:disablingDiv').style.display='none';
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{msg['familyVillagefile.lbl.heading']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">
												<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>

																<h:outputText id="errorId"
																	value="#{pages$familyVillageRequest.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$familyVillageRequest.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$familyVillageRequest.pageMode}"></h:inputHidden>
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$familyVillageRequest.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$familyVillageRequest.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$familyVillageRequest.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$familyVillageRequest.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$familyVillageRequest.hdnIsCompany}"></h:inputHidden>

																<h:commandLink id="onMessageFromSearchPerson"
																	action="#{pages$familyVillageRequest.onMessageFromSearchPerson}" />
																<h:commandLink id="onMessageFromSearchVilla"
																	action="#{pages$familyVillageRequest.onMessageFromSearchVilla}" />
																	<h:commandLink id="onMessageFromAddFamilyVillageBeneficiary"
																	action="#{pages$familyVillageRequest.onMessageFromAddFamilyVillageBeneficiary}" />
																

															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<t:div rendered="true" style="width:100%;">


														<t:panelGrid cellpadding="1px" width="100%"
															cellspacing="5px" columns="4">

															<!-- Top Fields - Start -->
															<h:outputLabel styleClass="LABEL"
																value="#{msg['searchInheritenceFile.fileNo']}:" />
															<h:inputText styleClass="READONLY" readonly="true"
																value="#{pages$familyVillageRequest.requestView.inheritanceFileView.fileNumber}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['searchInheritenceFile.fileType']}:" />
															<h:inputText styleClass="READONLY" readonly="true"
																value="#{pages$familyVillageRequest.englishLocale ? 
																					pages$familyVillageRequest.requestView.inheritanceFileView.fileTypeEn : 
																					pages$familyVillageRequest.requestView.inheritanceFileView.fileTypeAr}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.status']}:" />
															<h:inputText styleClass="READONLY" readonly="true"
																value="#{pages$familyVillageRequest.englishLocale ? 
																						pages$familyVillageRequest.requestView.inheritanceFileView.statusEn : 
																						pages$familyVillageRequest.requestView.inheritanceFileView.statusAr}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.createdBy']}:" />
															<h:inputText styleClass="READONLY" readonly="true"
																value="#{pages$familyVillageRequest.requestView.inheritanceFileView.createdBy}" />


															<%--
															<h:outputLabel styleClass="LABEL"
																rendered="#{pages$familyVillageRequest.statusApprovalRequired ||
																			pages$familyVillageRequest.statusReviewDone  
																		}"
																value="#{msg['mems.investment.label.sendTo']}:" />
															<t:panelGroup
																rendered="#{pages$familyVillageRequest.statusApprovalRequired ||
																			pages$familyVillageRequest.statusReviewDone 
																		   }">
																<h:selectOneMenu
																	binding="#{pages$familyVillageRequest.cmbReviewGroup}">
																	<f:selectItem itemLabel="#{msg['commons.select']}"
																		itemValue="" />
																	<f:selectItems
																		value="#{pages$ApplicationBean.allUserGroups}" />
																</h:selectOneMenu>
															</t:panelGroup>
															
															<h:outputLabel styleClass="LABEL"
																rendered="#{pages$familyVillageRequest. statusApprovalRequired ||
																			pages$familyVillageRequest.statusReviewDone 
																		}"
																value="" />
															<h:outputLabel styleClass="LABEL"
																rendered="#{pages$familyVillageRequest. statusApprovalRequired ||
																			pages$familyVillageRequest.statusReviewDone 
																		}"
																value="" />
																--%>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['maintenanceRequest.remarks']}"
																rendered="#{
																				pages$familyVillageRequest.showApprovalButton  ||
																				pages$familyVillageRequest.showResubmitButton
																			}" />
															<t:panelGroup colspan="3" style="width: 100%">
																<t:inputTextarea style="width: 90%;" id="txt_remarks"
																	rendered="#{pages$familyVillageRequest.showApprovalButton  ||
																				pages$familyVillageRequest.showResubmitButton}"
																	value="#{pages$familyVillageRequest.txtRemarks}"
																	onblur="javaScript:validate();"
																	onkeyup="javaScript:validate();"
																	onmouseup="javaScript:validate();"
																	onmousedown="javaScript:validate();"
																	onchange="javaScript:validate();"
																	onkeypress="javaScript:validate();" />
															</t:panelGroup>

															<h:outputLabel value="" />
															<h:outputLabel value="" />
															<!-- Top Fields - End -->
														</t:panelGrid>
													</t:div>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">

														<div class="TAB_PANEL_INNER">

															<rich:tabPanel
																binding="#{pages$familyVillageRequest.tabPanel}"
																style="width: 100%">

																<rich:tab id="applicationTab"
																	label="#{msg['commons.tab.applicationDetails']}">
																	<%@ include file="ApplicationDetails.jsp"%>
																</rich:tab>
																<!-- villa Tab - Start -->
																<rich:tab id="villaDetailsTab"
																	label="#{msg['familyVillagefile.lbl.villaDetails']}"
																	action="#{pages$familyVillageRequest.onVillaDetailsTab}">
																	<%@ include file="tabFamilyVillageFileVilla.jsp"%>
																</rich:tab>
																<!-- villa Tab - Finish -->
																<!-- villaStaffTab Tab - Start -->
																<rich:tab id="villaStaffTab"
																	label="#{msg['familyVillagefile.tab.VillaStaff']}"
																	action="#{pages$familyVillageRequest.onVillaStaffTab}">
																	<%@ include file="tabFamilyVillageVillaStaff.jsp"%>
																</rich:tab>
																<!-- villaStaffTab Tab - Finish -->
																<!-- beneficiaries  Tab - Start -->
																<rich:tab id="beneficiaryTab"
																			  
																	label="#{msg['mems.investment.label.beneficiaries']}"
																	action="#{pages$familyVillageRequest.onBeneficiariesTab}">
																	<%@ include file="tabFamilyVillageFileBeneficiary.jsp"%>
																</rich:tab>
																<!-- beneficiaries  Tab - Finish -->
																<!-- Review Details Tab - Start -->
																<%-- 
																<rich:tab id="tabReview"
																	rendered="#{!pages$familyVillageRequest.statusNew}"
																	label="#{msg['commons.tab.reviewDetails']}">
																	<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
																</rich:tab>
																--%>
																<!-- Review Details Tab - End -->
																<rich:tab id="commentsTab"
																	action="#{pages$familyVillageRequest.onAttachmentsCommentsClick}"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>

																<rich:tab id="attachmentTab"
																	action="#{pages$familyVillageRequest.onAttachmentsCommentsClick}"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>

																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$familyVillageRequest.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>

														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">
															
																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$familyVillageRequest.showSaveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$familyVillageRequest.onSave}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$familyVillageRequest.showSaveAfterApproveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSaveAfterApprove"
																	action="#{pages$familyVillageRequest.onSaveAfterApprove}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$familyVillageRequest.showSubmitButton}"
																	value="#{msg['commons.submitButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSubmit"
																	action="#{pages$familyVillageRequest.onSubmit}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$familyVillageRequest.showResubmitButton}"
																	value="#{msg['socialResearch.btn.resubmit']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkResubmit"
																	action="#{pages$familyVillageRequest.onResubmitted}" />



																<%-- 
																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$familyVillageRequest.showApprovalButton}"
																	value="#{msg['commons.complete']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkComplete"
																	action="#{pages$familyVillageRequest.onComplete}" />
--%>
																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$familyVillageRequest.showApprovalButton}"
																	value="#{msg['commons.approve']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkApprove"
																	action="#{pages$familyVillageRequest.onApproved}" />
																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$familyVillageRequest.showApprovalButton}"
																	value="#{msg['commons.reject']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkReject"
																	action="#{pages$familyVillageRequest.onRejected}" />
																<%-- 
																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$familyVillageRequest.showApprovalButton}"
																	value="#{msg['commons.sendBack']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkSendBack"
																	action="#{pages$familyVillageRequest.onSentBack}" />

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$familyVillageRequest.showApprovalButton}"
																	value="#{msg['commons.reviewButton']}"
																	onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
																</h:commandButton>
																<h:commandLink id="lnkReviewButton"
																	action="#{pages$familyVillageRequest.onReview}" />
--%>
															
															<h:commandButton styleClass="BUTTON"
																rendered="#{pages$familyVillageRequest.showReviewDoneButton}"
																value="#{msg['commons.done']}"
																onclick="if (!confirm('#{msg['auction.messages.confirmSubmit']}')) return false;else performClick(this);">
															</h:commandButton>
															<h:commandLink id="lnkReviewDone"
																action="#{pages$familyVillageRequest.onReviewDone}" />


														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>