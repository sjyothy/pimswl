<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript"> 

		function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
		
		
		function submitForm()
		{
			window.document.forms[0].submit();
		}	
	 	function callOpenerFn()
	    {
	      window.opener.receiveReplies();
	      window.close();
	   }
	 
	    
       function closeWindow() 
       { 
	      window.close();
	   }
		

</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
	 <title>PIMS</title>
	 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
	 <meta http-equiv="pragma" content="no-cache">
	 <meta http-equiv="cache-control" content="no-cache">
	 <meta http-equiv="expires" content="0">
	 <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>				
	 <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
 	 <script type="text/javascript" src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>


	
</head>
	<body class="BODY_STYLE">
	<%	
		   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		   response.setHeader("Pragma","no-cache"); //HTTP 1.0
		   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
    <!-- Header --> 
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		
		<tr width="100%">
			<td width="83%" valign="top" class="divBackgroundBody">
				<table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="HEADER_TD">
							<h:outputLabel value="#{msg['tenderInquiryAddAll.heading']}" styleClass="HEADER_FONT"/>
						</td>
					</tr>
				</table>
				<table width="99%" style="margin-left:1px;" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" height="100%">
					<tr valign="top">
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
						<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
							<div class="SCROLLABLE_SECTION" style="height:475px;width:100%;#height:466px;">
									<h:form id="tenderInquiryReplayFrm" enctype="multipart/form-data" style="WIDTH: 96%;">
								
								<div >
										<table border="0" class="layoutTable" style="margin-left:15px;margin-right:15px;">
											<tr>
												<td>
													<h:outputText value="#{pages$tenderInquiryReplayAll.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
													<h:outputText value="#{pages$tenderInquiryReplayAll.infoMessages}"  escape="false" styleClass="INFO_FONT"/>
												</td>
											</tr>
										</table>
								 </div>
									   <div  class="MARGIN">
										<div class="TAB_DETAIL_SECTION">
											<table cellpadding="1px" style="width:70%;" cellspacing="2px" class="TAB_DETAIL_SECTION_INNER">
														<tr>
															<td>
															<h:outputLabel styleClass="LABEL" value="#{msg['tenderInquiryAdd.inquiryDate']}"/>
															</td>
															<td>
                                                            <h:inputText id="txtReplyDate" style="width:190px; height: 18px"  styleClass="READONLY"  readonly="true" value="#{pages$tenderInquiryReplayAll.applicationDate}">
                                                            <f:convertDateTime pattern="#{pages$tenderInquiryReplayAll.dateFormat}"
													                          timeZone="#{pages$tenderInquiryReplayAll.timeZone}" />
                                                            </h:inputText>
															</td>
														</tr>
														<tr>
															<td >
														     <h:outputLabel styleClass="LABEL"  value="#{msg['tenderInquiryAdd.inquiryDetails']}: #{pages$tenderInquiryReplayAll.asterisk}"></h:outputLabel>
															</td>
															<td >
													         <t:inputTextarea id ="txtReplies" styleClass="TEXTAREA" value="#{pages$tenderInquiryReplayAll.replayText}"  rows="6" style="width: 200px;"/>
												            </td>
														</tr>
														<tr>
															<td >
																<h:outputLabel id="attachLabel" styleClass="LABEL" value="#{msg['commons.attachReport']}: "/>
															</td>
															<td width="30%">
																<t:inputFileUpload id="fileupload" size="35" storage="file" 
																value="#{pages$tenderInquiryReplayAll.selectedFile}" 
																binding="#{pages$tenderInquiryReplayAll.fileUploadCtrl}" 
																style="height: 16px; margin-right: 5px;" >
																</t:inputFileUpload>
															</td>
														</tr>
														<tr>
															<td colspan="6" class="BUTTON_TD">
																<h:commandButton styleClass="BUTTON" type="button"
																	value="#{msg['commons.cancel']}"
																	onclick="javascript:closeWindow();" />
																<h:commandButton type="submit" styleClass="BUTTON" style="width:auto;"
																	value="#{msg['common.replayRequest']}"
																	action="#{pages$tenderInquiryReplayAll.replied}" />
																	
															</td>
														</tr>
													</table>
												</div>
											</div>
									           
							</h:form>
					  </div> 
					  
						</td>			
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
</f:view>