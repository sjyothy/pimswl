
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<SCRIPT>	
function closeWindow()
	{
	  //window.opener.document.getElementById('conductAuctionForm:isResultPopupCalled').value = "Y";
	  window.opener.document.forms[0].submit();
	  window.close();
	}
       </SCRIPT>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />

			<style>
span {
	PADDING-RIGHT: 5px;
	PADDING-LEFT: 5px;
}
</style>
			<script language="javascript" type="text/javascript">
	function clearWindow()
	{
     //  alert("In clear");
        
            document.getElementById("formSearchMemberPopup:txtfirstname").value="";
			document.getElementById("formSearchMemberPopup:txtlastname").value="";	
			document.getElementById("formSearchMemberPopup:txtpassportNumber").value="";
			document.getElementById("formSearchMemberPopup:txtresidenseVisaNumber").value="";
			//document.getElementById("formSearchMemberPopup:txtpersonalSecCardNo").value="";
			//document.getElementById("formSearchMemberPopup:txtdrivingLicenseNumber").value="";
			document.getElementById("formSearchMemberPopup:txtsocialSecNumber").value="";
			//var cmbRequestType=document.getElementById("formSearchMemberPopup:selectPersonType");
			//cmbRequestType.selectedIndex = 0;
			//var cmbRequestType1=document.getElementById("formSearchMemberPopup:selectnationality");
			//cmbRequestType1.selectedIndex = 0;
	   //   cmbRequestType.selectedIndex = 0;
	  //      alert(cmbRequestType);
			
	}
	

	
</script>



		</head>
		<body dir="${sessionScope.CurrentLocale.dir}"
			lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<tr width="100%">

					<td width="100%" valign="top" class="divBackgroundBody">
									<table width="100%" class="greyPanelTable" cellpadding="0"
										cellspacing="0" border="0">
										<tr>
											<td class="HEADER_TD">
												<h:outputLabel value="#{msg['member.searchMember']}"
													styleClass="HEADER_FONT" />
											</td>
											<td width="100%">
												&nbsp;
											</td>
										</tr>

									</table>

									<table width="100%" class="greyPanelMiddleTable"
										cellpadding="0" cellspacing="0" border="0">
										<tr valign="top">
											<td height="100%" valign="top" nowrap="nowrap"
												background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
												width="1">
											</td>

											<td width="100%" height="576px" valign="top" nowrap="nowrap">




												<div>
													<h:form id="formSearchMember" style="width:98%">

														<div class="MARGIN">
															<table cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<td style="FONT-SIZE: 0px;">
																		<IMG
																			src="../<h:outputText value="#{path.img_section_left}"/>"
																			class="TAB_PANEL_LEFT" />
																	</td>
																	<td width="100%" style="FONT-SIZE: 0px;">
																		<IMG
																			src="../<h:outputText value="#{path.img_section_mid}"/>"
																			class="TAB_PANEL_MID" />
																	</td>
																	<td style="FONT-SIZE: 0px;">
																		<IMG
																			src="../<h:outputText value="#{path.img_section_right}"/>"
																			class="TAB_PANEL_RIGHT" />
																	</td>
																</tr>
															</table>
															<div class="DETAIL_SECTION">
																<h:outputLabel value="#{msg['inquiry.searchcriteria']}"
																	styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
																<table cellpadding="1px" cellspacing="2px"
																	class="DETAIL_SECTION_INNER">

																	<tr>
																	<td width="97%">
																	<table width="100%">

																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['member.fullName']}:"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText  id="txtfirstname"
																				binding="#{pages$SearchMemberPopup.fullName}">
																			</h:inputText>
																		</td>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['member.loginName']}:"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText  id="txtlastname"
																				binding="#{pages$SearchMemberPopup.userName}">
																			</h:inputText>
																		</td>


																	</tr>
																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['member.groupName']}:"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText  
																				id="txtsocialSecNumber"
																				binding="#{pages$SearchMemberPopup.groupName}">
																			</h:inputText>
																		</td>

																	</tr>
																	<tr>

																		<td colspan="4" class="BUTTON_TD">
																			<h:commandButton type="submit" styleClass="BUTTON"
																				value="#{msg['commons.search']}"
																				action="#{pages$SearchMemberPopup.searchUser}"></h:commandButton>


																			<h:commandButton styleClass="BUTTON" type="button"
																				value="#{msg['commons.clear']}"
																				onclick="javascript:clearWindow();" />

																		</td>
																	</tr>

																		</table>
																	</td>
																	<td width="3%">
																		&nbsp;
																	</td>
																</tr>


																</table>
															</div>
														</div>
														<div style="padding: 10px;">
															<div class="imag">
															</div>
															<div class="contentDiv" style="width:99%;">
																<t:dataTable id="test3" rows="5" width="100%"
																	value="#{pages$SearchMemberPopup.userDataList}"
																	binding="#{pages$SearchMemberPopup.dataTable}"
																	preserveDataModel="true" preserveSort="true"
																	var="dataItem" rowClasses="row1,row2" rules="all"
																	renderedIfEmpty="true">
																	<t:column style="white-space:nowrap; width:25%"  id="col2" sortable="true" rendered="#{pages$SearchMemberPopup.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['member.fullName']}" />
																		</f:facet>
																		<t:outputText value="#{dataItem.fullNamePrimary}" styleClass="A_LEFT" style="padding:0px;"/>
																	</t:column>
																	<t:column style="white-space:nowrap; width:25%"   id="col2Ar" sortable="true" rendered="#{pages$SearchMemberPopup.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['member.fullName']}" />
																		</f:facet>
																			<t:outputText value="#{dataItem.fullNameSecondary}" styleClass="A_LEFT" style="padding:0px;"/>
																	</t:column>
																	<t:column style="white-space:nowrap; width:25%"  id="col4" sortable="true">
																		<f:facet name="header">
																			<t:outputText value="#{msg['member.loginName']}"/>
																		</f:facet>
																		<t:outputText title="" value="#{dataItem.userName}" styleClass="A_LEFT" style="padding:0px;"/>
																	</t:column>
																	<t:column style="white-space:pre-line; width:25%"  id="col5" sortable="true" rendered="#{pages$SearchMemberPopup.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['member.groupName']}"/>
																		</f:facet>
																		<t:outputText  title="" value="#{dataItem.groupNamePrimary}" styleClass="A_LEFT" style="padding:0px;"/>
																	</t:column>
																	<t:column style="white-space:nowrap; width:25%" id="col5Ar" sortable="true" rendered="#{pages$SearchMemberPopup.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText value="#{msg['member.groupName']}"/>
																		</f:facet>
																			<t:outputText value="#{dataItem.groupNameSecondary}" styleClass="A_LEFT" style="padding:0px;"/>
																	</t:column>
																	<t:column style="white-space:nowrap; width:25%"  id="GpaymentDetail">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.select']}" />
																		</f:facet>
																		<h:selectBooleanCheckbox styleClass="checkBox" id="select" rendered="#{!pages$SearchMemberPopup.isSingleSelectMode}" value="#{dataItem.selected}" />
																		<h:commandLink rendered="#{pages$SearchMemberPopup.isSingleSelectMode}" action="#{pages$SearchMemberPopup.cmdSelectPerson_Click}">
																		   <h:graphicImage   style="width: 16;height: 16;border: 0" alt="#{msg['commons.select']}" url="../resources/images/select-icon.gif"/>
																		</h:commandLink>
																	</t:column>
																</t:dataTable>
															</div>

															<div class="contentDivFooter" style="width: 100%">
																<t:dataScroller id="scroller" for="test3"
																	paginator="true" fastStep="1" paginatorMaxPages="15"
																	immediate="false" paginatorTableClass="paginator"
																	renderFacetsIfSinglePage="true"
																	paginatorTableStyle="grid_paginator"
																	layout="singleTable"
																	styleClass="SCH_SCROLLER"
																	paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																	paginatorActiveColumnStyle="font-weight:bold;"
																	>

																	<f:facet name="first">
																		<t:graphicImage url="../#{path.scroller_first}"
																			id="lblFowner"></t:graphicImage>
																	</f:facet>
																	<f:facet name="fastrewind">
																		<t:graphicImage url="../#{path.scroller_fastRewind}"
																			id="lblFRowner"></t:graphicImage>
																	</f:facet>
																	<f:facet name="fastforward">
																		<t:graphicImage url="../#{path.scroller_fastForward}"
																			id="lblFFowner"></t:graphicImage>
																	</f:facet>
																	<f:facet name="last">
																		<t:graphicImage url="../#{path.scroller_last}"
																			id="lblLowner"></t:graphicImage>
																	</f:facet>
																</t:dataScroller>

															</div>
															<table width="100%" border="0">
																<tr>
																	<td colspan="4" class="BUTTON_TD">

																		<h:commandButton styleClass="BUTTON" type="submit"
																			value="#{msg['member.select']}"
																			binding="#{pages$SearchMemberPopup.selectButton}"
																			action="#{pages$SearchMemberPopup.editDataItem}">
																		</h:commandButton>
																		
																	</td>
																	
																</tr>
															</table>
													</h:form>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
		</body>
</f:view>
</html>
