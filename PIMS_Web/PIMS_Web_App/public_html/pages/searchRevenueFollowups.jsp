<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Endowment Programs
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	    function resetValues()
      	{
      		document.getElementById("searchFrm:txtrefNum").value="";
      		
      		document.getElementById("searchFrm:txtcontactNumber").value="";
      		document.getElementById("searchFrm:txtcontactName").value="";
      		document.getElementById("searchFrm:txtassetName").value="";
      		document.getElementById("searchFrm:txtassetNumber").value="";
      		document.getElementById("searchFrm:txtendNum").value="";
      		document.getElementById("searchFrm:txtendName").value="";
      		
      		
		    document.getElementById("searchFrm:cmbstatus").selectedIndex=0;
		    document.getElementById("searchFrm:cmbclosedBy").selectedIndex=0;
		    document.getElementById("searchFrm:cmbcreatedBy").selectedIndex=0;
      	    
      	    
			$('searchFrm:followUpTo').component.resetSelectedDate();
			$('searchFrm:followUpFrom').component.resetSelectedDate();
			$('searchFrm:createdFrom').component.resetSelectedDate();
			$('searchFrm:createdTo').component.resetSelectedDate();
			
        }
        
        function closeWindow()
        {
        	window.opener.onMessageFromEndowmentPrograms();
        	window.close();
        
        }
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<c:choose>
				<c:when test="${!pages$searchRevenueFollowups.sViewModePopUp}">
					<div class="containerDiv">
				</c:when>
			</c:choose>

			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<c:choose>
					<c:when test="${!pages$searchRevenueFollowups.sViewModePopUp}">
						<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>

					</c:when>
				</c:choose>



				<tr width="100%">
					<c:choose>
						<c:when test="${!pages$searchRevenueFollowups.sViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>

						</c:when>
					</c:choose>



					<td width="83%" height="470px" valign="top"
						class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['revenueFollowup.heading.search']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
									<div class="SCROLLABLE_SECTION AUC_SCH_SS"
										style="height: 95%; width: 100%; # height: 95%; # width: 100%;">
										<h:form id="searchFrm"
											style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
											<t:div id="layoutTable" styleClass="MESSAGE">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:messages></h:messages>
															<h:outputText id="errorMessage"
																value="#{pages$searchRevenueFollowups.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</t:div>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">
																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['revenueFollowup.lbl.RefNum']}"></h:outputLabel>
																		</td>

																		<td width="25%">
																			<h:inputText id="txtrefNum"
																				value="#{pages$searchRevenueFollowups.criteria.refNum}"
																				maxlength="20"></h:inputText>

																		</td>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.status']}"></h:outputLabel>
																		</td>

																		<td width="25%" colspan="1">
																			<h:selectOneMenu id="cmbstatus"
																				value="#{pages$searchRevenueFollowups.criteria.statusId}"
																				tabindex="3">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.revenueFollowupStatus}" />
																			</h:selectOneMenu>
																		</td>
																	</tr>
																	<tr>


																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.createdBy']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:selectOneMenu id="cmbcreatedBy"
																				value="#{pages$searchRevenueFollowups.criteria.createdBy}"
																				tabindex="3">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.secUserList}" />
																			</h:selectOneMenu>
																		</td>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['revenueFollowup.lbl.closedBy']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<h:selectOneMenu id="cmbclosedBy"
																				value="#{pages$searchRevenueFollowups.criteria.closedBy}"
																				tabindex="3">
																				<f:selectItem itemLabel="#{msg['commons.All']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.secUserList}" />
																			</h:selectOneMenu>
																		</td>
																	</tr>
																	<tr>

																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['inheritanceFileSearch.createdOnFrom']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<rich:calendar id="createdFrom"
																				locale="#{pages$searchRevenueFollowups.locale}"
																				value="#{pages$searchRevenueFollowups.criteria.createdFrom}"
																				popup="true"
																				datePattern="#{pages$searchRevenueFollowups.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:185px;height:17px" />

																		</td>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.to']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<rich:calendar id="createdTo"
																				locale="#{pages$searchRevenueFollowups.locale}"
																				value="#{pages$searchRevenueFollowups.criteria.createdTo}"
																				popup="true"
																				datePattern="#{pages$searchRevenueFollowups.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:185px;height:17px" />

																		</td>

																	</tr>
																	<tr>

																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['revenueFollowup.lbl.followupOnFrom']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<rich:calendar id="followUpFrom"
																				locale="#{pages$searchRevenueFollowups.locale}"
																				value="#{pages$searchRevenueFollowups.criteria.followUpFrom}"
																				popup="true"
																				datePattern="#{pages$searchRevenueFollowups.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:185px;height:17px" />
																		</td>
																		<td width="25%" colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.to']}:"></h:outputLabel>
																		</td>
																		<td width="25%" colspan="1">
																			<rich:calendar id="followUpTo"
																				locale="#{pages$searchRevenueFollowups.locale}"
																				value="#{pages$searchRevenueFollowups.criteria.followUpTo}"
																				popup="true"
																				datePattern="#{pages$searchRevenueFollowups.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:185px;height:17px" />

																		</td>

																	</tr>
																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['revenueFollowup.lbl.endowmentNum']}"></h:outputLabel>
																		</td>

																		<td width="25%">
																			<h:inputText id="txtendNum"
																				value="#{pages$searchRevenueFollowups.criteria.endowment.endowmentNum}"
																				maxlength="20"></h:inputText>

																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['revenueFollowup.lbl.endowmentName']}"></h:outputLabel>
																		</td>

																		<td width="25%">
																			<h:inputText id="txtendName"
																				value="#{pages$searchRevenueFollowups.criteria.endowment.endowmentName}"></h:inputText>

																		</td>
																	</tr>
																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['revenueFollowup.lbl.assetNum']}"></h:outputLabel>
																		</td>

																		<td width="25%">
																			<h:inputText id="txtassetNumber"
																				value="#{pages$searchRevenueFollowups.criteria.assetMems.assetNumber}"
																				maxlength="20"></h:inputText>

																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['revenueFollowup.lbl.assetName']}"></h:outputLabel>
																		</td>

																		<td width="25%">
																			<h:inputText id="txtassetName"
																				value="#{pages$searchRevenueFollowups.criteria.assetMems.assetNameEn}"></h:inputText>

																		</td>
																	</tr>
																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['revenueFollowup.lbl.contactName']}"></h:outputLabel>
																		</td>

																		<td width="25%">
																			<h:inputText id="txtcontactName"
																				value="#{pages$searchRevenueFollowups.criteria.contactName}"
																				maxlength="20"></h:inputText>

																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['person.contactNumber']}"></h:outputLabel>
																		</td>

																		<td width="25%">
																			<h:inputText id="txtcontactNumber"
																				value="#{pages$searchRevenueFollowups.criteria.contactNumber}"
																				maxlength="20"></h:inputText>

																		</td>
																	</tr>

																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>

													</table>


												</div>
											</div>
											<div class="BUTTON_TD">
												<table>
													<tr>

														<td style="width: 100" class="BUTTON_TD" colspan="4">
															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.search']}"
																action="#{pages$searchRevenueFollowups.onSearch}"
																style="width: 75px" />

															<h:commandButton styleClass="BUTTON" type="button"
																value="#{msg['commons.clear']}"
																onclick="javascript:resetValues();" style="width: 75px" />
															<pims:security
																screen="Pims.RevenueFollowupMgmt.RevenueFollowup.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.Add']}"
																	action="#{pages$searchRevenueFollowups.onAdd}"
																	rendered="#{!pages$searchRevenueFollowups.sViewModePopUp}"
																	style="width: 75px" />

															</pims:security>
														</td>
													</tr>
												</table>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" style="width: 100%; # width: 99%;">
													<t:dataTable id="dt1"
														value="#{pages$searchRevenueFollowups.dataList}"
														binding="#{pages$searchRevenueFollowups.dataTable}"
														rows="#{pages$searchRevenueFollowups.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%">

														<t:column id="refNum" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="refNum"
																	actionListener="#{pages$searchRevenueFollowups.sort}"
																	value="#{msg['revenueFollowup.lbl.RefNum']}"
																	arrow="true">
																	<f:attribute name="sortField" value="refNum" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.refNum}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>


														<t:column id="statusId" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="domainDataId"
																	actionListener="#{pages$searchRevenueFollowups.sort}"
																	value="#{msg['commons.status']}" arrow="true">
																	<f:attribute name="sortField" value="domainDataId" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText
																value="#{pages$searchRevenueFollowups.englishLocale? 
																	         dataItem.status.dataDescEn:
																	         dataItem.status.dataDescAr
																	        }"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="followUpOn" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="followUpOn"
																	actionListener="#{pages$searchRevenueFollowups.sort}"
																	value="#{msg['revenueFollowup.lbl.followupOn']}"
																	arrow="true">
																	<f:attribute name="sortField" value="followUpOn" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.followUpOn}"
																style="white-space: normal;" styleClass="A_LEFT">
																<f:convertDateTime
																	timeZone="#{pages$searchRevenueFollowups.timeZone}"
																	pattern="#{pages$searchRevenueFollowups.dateFormat}" />
															</t:outputText>
														</t:column>
														<t:column id="createdBy" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="createdBy"
																	actionListener="#{pages$searchRevenueFollowups.sort}"
																	value="#{msg['commons.createdBy']}" arrow="true">
																	<f:attribute name="sortField" value="followUpOn" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.createdByName}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="assetNameEn" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.refNum']}" />

															</f:facet>
															<t:outputText
																value="#{dataItem.assetRefNumForSearchGrid}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:column id="assetNameForSearchGrid" sortable="true">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.Name']}" />

															</f:facet>
															<t:outputText
																value="#{  pages$searchRevenueFollowups.englishLocale?
																									                    dataItem.assetNameForSearchGrid:
																									                    dataItem.assetNameArForSearchGrid
																								
																				  }"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>


														<t:column id="actionCol" sortable="false" width="100"
															style="TEXT-ALIGN: center;">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.action']}" />
															</f:facet>

															<t:commandLink
																rendered="#{!pages$searchRevenueFollowups.sViewModePopUp}"
																action="#{pages$searchRevenueFollowups.onEdit}">

																<h:graphicImage title="#{msg['commons.edit']}"
																	url="../resources/images/edit-icon.gif" />
															</t:commandLink>
															<t:commandLink
																rendered="#{pages$searchRevenueFollowups.pageModeSelectOnePopUp}"
																action="#{pages$searchRevenueFollowups.onSingleSelect}">

																<h:graphicImage title="#{msg['commons.select']}"
																	url="../resources/images/select-icon.gif" />
															</t:commandLink>
														</t:column>



													</t:dataTable>
												</div>
												<t:div id="contentDivFooter"
													styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:100%;#width:100%;">
													<table id="RECORD_NUM_TD" cellpadding="0" cellspacing="0"
														width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$searchRevenueFollowups.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td id="contentDivFooterColumnTwo" class="BUTTON_TD"
																style="width: 53%; # width: 50%;" align="right">

																<t:div styleClass="PAGE_NUM_BG" style="#width:20%">

																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																			</td>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{pages$searchRevenueFollowups.currentPage}" />
																			</td>
																		</tr>
																	</table>
																</t:div>
																<TABLE border="0" class="SCH_SCROLLER">
																	<tr>
																		<td>
																			<t:commandLink
																				action="#{pages$searchRevenueFollowups.pageFirst}"
																				disabled="#{pages$searchRevenueFollowups.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$searchRevenueFollowups.pagePrevious}"
																				disabled="#{pages$searchRevenueFollowups.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFR"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:dataList
																				value="#{pages$searchRevenueFollowups.pages}"
																				var="page">
																				<h:commandLink value="#{page}"
																					actionListener="#{pages$searchRevenueFollowups.page}"
																					rendered="#{page != pages$searchRevenueFollowups.currentPage}" />
																				<h:outputText value="<b>#{page}</b>" escape="false"
																					rendered="#{page == pages$searchRevenueFollowups.currentPage}" />
																			</t:dataList>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$searchRevenueFollowups.pageNext}"
																				disabled="#{pages$searchRevenueFollowups.firstRow + pages$searchRevenueFollowups.rowsPerPage >= pages$searchRevenueFollowups.totalRows}">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$searchRevenueFollowups.pageLast}"
																				disabled="#{pages$searchRevenueFollowups.firstRow + pages$searchRevenueFollowups.rowsPerPage >= pages$searchRevenueFollowups.totalRows}">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblL"></t:graphicImage>
																			</t:commandLink>
																		</td>
																	</tr>
																</TABLE>
															</td>
														</tr>
													</table>
												</t:div>
											</div>

										</h:form>

									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">

					<td colspan="2">
						<c:choose>
							<c:when test="${!pages$searchRevenueFollowups.sViewModePopUp}">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</tr>
								</table>

							</c:when>
						</c:choose>

					</td>
				</tr>

			</table>

			<c:choose>
				<c:when test="${!pages$searchRevenueFollowups.sViewModePopUp}">
					</div>
				</c:when>
			</c:choose>
		</body>
	</html>
</f:view>