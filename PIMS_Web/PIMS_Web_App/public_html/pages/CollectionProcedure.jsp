<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">


	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		//disableInputs();	
		if( hdnPersonType=='APPLICANT' ) 
		{			    			 
			 document.getElementById("detailsFrm:hdnPersonId").value=personId;
			 document.getElementById("detailsFrm:hdnCellNo").value=cellNumber;
			 document.getElementById("detailsFrm:hdnPersonName").value=personName;
			 document.getElementById("detailsFrm:hdnPersonType").value=hdnPersonType;
			 document.getElementById("detailsFrm:hdnIsCompany").value=isCompany;
		}
				
	     document.getElementById("detailsFrm:onMessageFromSearchPerson").onclick();
	}
	
		function openSearchPayments(fileId )
		{
			var screen_width = screen.width;
	        var screen_height = screen.height;
	        var popup_width = screen_width-300;
	        var popup_height = 450;
	        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
	        var popup 	= window.open(
	        						  'chequeList.jsf?modeSelectOnePopUp=1&context=MINOR_COLLECTIONS&fileId='+fileId,
	        						  '_blank',
	        						  'width='+popup_width+
	        						  ',height='+popup_height+
	        						  ',left='+ leftPos +
	        						  ',top=' + topPos + 
	        						  ',scrollbars=yes,status=yes,resizable=yes,titlebar=no,dialog=yes'
	        						 );
	        popup.focus();
		
		}

	  
		function openCollectionDetails()
		{
		        var screen_width = screen.width;
				var screen_height = screen.height;
		        var popup_width = screen_width-250;
		        var popup_height = screen_height-100;
		        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
		        window.open('collectionDetailsPopup.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
		  
		}
        function populatePaymentDetails()
		{
		  document.getElementById("detailsFrm:testing").onclick();
		  
		}
		function onMessageFromChequeList( )
		{
		 	
		  	document.getElementById("detailsFrm:openFromChequeList").onclick();
		}	
		function populateDistributionDetails()
		{
		  document.getElementById("detailsFrm:distPop").onclick();
		  
		}
		function populateCollectionPaymentDetails()
		{
		  document.getElementById("detailsFrm:collectionPaymentDetailsPop").onclick();
		  
		}
		function onActionDone()
	    {
	      document.getElementById('detailsFrm:disablingDiv').style.display='none';
	    }
	    function onCollectedAsChange( control )
		{
		    document.getElementById('detailsFrm:disablingDiv').style.display='block';
			document.getElementById('detailsFrm:lnkSelectionChanged').onclick();
		}
		function onChkChange()
	    {
	      document.getElementById('detailsFrm:disablingDiv').style.display='none';
	    }
	    function onSelectionChanged(changedChkBox)
		{
		    document.getElementById('detailsFrm:disablingDiv').style.display='block';
			
			document.getElementById('detailsFrm:lnkUnitSelectionChanged').onclick();
		}
		function disableButtons(control)
		{
			var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
			
			control.nextSibling.nextSibling.onclick();
			return 
		
		}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div>

				<div class="containerDiv">
					<!-- Header -->
					<table width="100%" height="100%" cellpadding="0" cellspacing="0"
						border="0">
						<c:choose>
							<c:when test="${!pages$CollectionProcedure.isViewModePopUp}">
								<tr
									style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
									<td colspan="2">
										<jsp:include page="header.jsp" />
									</td>
								</tr>
							</c:when>
						</c:choose>
						<tr width="100%">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
							<td width="83%" valign="top" class="divBackgroundBody">
								<table width="99.2%" class="greyPanelTable" cellpadding="0"
									cellspacing="0" border="0">
									<tr>
										<td class="HEADER_TD">
											<h:outputLabel value="#{msg['collectionProcedure.header']}"
												styleClass="HEADER_FONT" />
										</td>
									</tr>
								</table>
								<table width="99%" style="margin-left: 1px;"
									class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
									border="0" height="100%">
									<tr valign="top">
										<td width="100%" valign="top" nowrap="nowrap">
											<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
											<div class="SCROLLABLE_SECTION"
												style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
												<h:form id="detailsFrm" enctype="multipart/form-data"
													style="WIDTH: 97.6%;">
													<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>



													<table id="layoutTable" border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>
																<h:outputText
																	value="#{pages$CollectionProcedure.errorMessages}"
																	escape="false" styleClass="ERROR_FONT" />
																<h:outputText
																	value="#{pages$CollectionProcedure.successMessages}"
																	escape="false" styleClass="INFO_FONT" />
																<h:inputHidden id="hdnPersonId"
																	value="#{pages$CollectionProcedure.hdnPersonId}"></h:inputHidden>
																<h:inputHidden id="hdnPersonType"
																	value="#{pages$CollectionProcedure.hdnPersonType}"></h:inputHidden>
																<h:inputHidden id="hdnPersonName"
																	value="#{pages$CollectionProcedure.hdnPersonName}"></h:inputHidden>
																<h:inputHidden id="hdnCellNo"
																	value="#{pages$CollectionProcedure.hdnCellNo}"></h:inputHidden>
																<h:inputHidden id="hdnIsCompany"
																	value="#{pages$CollectionProcedure.hdnIsCompany}"></h:inputHidden>



																<h:commandLink id="collectionPaymentDetailsPop"
																	action="#{pages$CollectionProcedure.onMessageFromCollectionPaymentDetailsPopup}" />
																<h:commandLink id="openFromChequeList"
																	action="#{pages$CollectionProcedure.openFromChequeList}" />

																<h:commandLink id="onMessageFromSearchPerson"
																	action="#{pages$CollectionProcedure.onMessageFromSearchPerson}" />

																<a4j:commandLink id="lnkSelectionChanged"
																	onbeforedomupdate="javascript:onActionDone();"
																	action="#{pages$CollectionProcedure.onCollectedAsChanged}"
																	reRender="basicInfoFields,layoutTable" />
																<h:commandLink id="distPop"
																	action="#{pages$CollectionProcedure.onMessageFromDistributePopUp}" />
																<a4j:commandLink id="lnkUnitSelectionChanged"
																	onbeforedomupdate="javascript:onChkChange();"
																	action="#{pages$CollectionProcedure.onAssetNameChange}"
																	reRender="basicInfoFields" />
																<h:inputHidden id="rowId"
																	value="#{pages$CollectionProcedure.rowId}" />

															</td>
														</tr>
													</table>
													<div>
														<div class="AUC_DET_PAD">
															<t:div styleClass="BUTTON_TD">
																<h:commandButton styleClass="BUTTON"
																	value="#{msg['attachment.file']}"
																	action="#{pages$CollectionProcedure.onOpenFile}"
																	rendered="#{pages$CollectionProcedure.isInheritanceFileAvailable}"
																	style="width: 135px">
																</h:commandButton>
															</t:div>

															<t:panelGrid cellpadding="1px" width="100%"
																cellspacing="5px" columns="4">
																<!-- Top Fields - Start -->

																<h:outputLabel styleClass="LABEL"
																	rendered="#{pages$CollectionProcedure.isInheritanceFileAvailable}"
																	value="#{msg['collectionProcedure.fileNumber']} :" />
																<h:inputText styleClass="READONLY" readonly="true"
																	rendered="#{pages$CollectionProcedure.isInheritanceFileAvailable}"
																	binding="#{pages$CollectionProcedure.htmlFileNumber}" />

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['report.memspaymentreceiptreport.fileOwner']}:" />
																<h:inputText styleClass="READONLY" readonly="true"
																	binding="#{pages$CollectionProcedure.htmlFilePerson}" />

																<h:outputLabel styleClass="LABEL"
																	rendered="#{pages$CollectionProcedure.isInheritanceFileAvailable}"
																	value="#{msg['searchInheritenceFile.fileType']}:" />
																<h:inputText styleClass="READONLY" readonly="true"
																	rendered="#{pages$CollectionProcedure.isInheritanceFileAvailable}"
																	binding="#{pages$CollectionProcedure.htmlFileType}" />

																<h:outputLabel styleClass="LABEL"
																	rendered="#{pages$CollectionProcedure.isInheritanceFileAvailable}"
																	value="#{msg['commons.status']}:" />
																<h:inputText styleClass="READONLY" readonly="true"
																	rendered="#{pages$CollectionProcedure.isInheritanceFileAvailable}"
																	binding="#{pages$CollectionProcedure.htmlFileStatus}" />


																<h:outputLabel styleClass="LABEL"
																	value="#{msg['request.number']} :" />
																<h:inputText styleClass="READONLY" readonly="true"
																	value="#{pages$CollectionProcedure.requestView.requestNumber}" />
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['request.requestStatus']} :" />
																<h:inputText styleClass="READONLY" readonly="true"
																	value="#{pages$CollectionProcedure.englishLocale?pages$CollectionProcedure.requestView.statusEn:pages$CollectionProcedure.requestView.statusAr}" />
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.createdBy']} :" />
																<h:inputText styleClass="READONLY" readonly="true"
																	value="#{pages$CollectionProcedure.requestView.createdByFullName}" />
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.createdOn']} :" />
																<rich:calendar inputStyle="width:170px; height:14px"
																	disabled="true" inputClass="READONLY"
																	value="#{pages$CollectionProcedure.requestView.createdOn}" />

																<h:outputLabel styleClass="LABEL"
																	rendered="#{
																				(
																					pages$CollectionProcedure.pageModeConfirm  || 
																					pages$CollectionProcedure.pageModeApproveReject 
																				)  &&  
																	            !pages$CollectionProcedure.confirmBankTransfer
																	           }"
																	value="#{msg['commons.comments']}:" />
																<t:panelGroup colspan="3"
																	rendered="#{ 
																	            (
																					pages$CollectionProcedure.pageModeConfirm  || 
																					pages$CollectionProcedure.pageModeApproveReject 
																				)  && 
																	            !pages$CollectionProcedure.confirmBankTransfer
																	           }">
																	<h:inputTextarea
																		binding="#{pages$CollectionProcedure.htmlRFC}"
																		style="width: 600px;" />
																</t:panelGroup>
																<!-- Top Fields - End -->


															</t:panelGrid>

															<div class="TAB_PANEL_INNER">
																<rich:tabPanel style="width: 100%"
																	binding="#{pages$CollectionProcedure.richPanel}">

																	<rich:tab id="applicationTab"
																		label="#{msg['commons.tab.applicationDetails']}">
																		<%@ include file="ApplicationDetails.jsp"%>
																	</rich:tab>

																	<rich:tab id="basicInfoTab"
																		label="#{msg['mems.inheritanceFile.tabName.basicInfo']}">


																		<t:panelGrid id="basicInfoFieldsOnDistribution"
																			border="0" width="100%" cellpadding="1"
																			cellspacing="1"
																			rendered="#{ 
																						(
																							pages$CollectionProcedure.pageModeConfirm  || 
																							pages$CollectionProcedure.pageModeApproveReject 
																						)  &&  
																			            !pages$CollectionProcedure.confirmBankTransfer &&
																			            pages$CollectionProcedure.taskPresent
																			           }"
																			columns="4">

																			<h:outputLabel style="font-weight:normal;"
																				styleClass="TABLE_LABEL"
																				value="#{msg['collectionProcedure.assetName']} :"></h:outputLabel>
																			<h:selectOneMenu id="assetNameOnDistribution"
																				value="#{pages$CollectionProcedure.selectOneAssetId}">
																				<f:selectItems
																					value="#{pages$CollectionProcedure.assetNameList}" />
																			</h:selectOneMenu>

																			<h:commandButton id="btnChangeAsset"
																				value="#{msg['commons.change']}"
																				action="#{pages$CollectionProcedure.onChangeAsset}"
																				styleClass="BUTTON" style="width:auto;"></h:commandButton>
																		</t:panelGrid>



																		<t:panelGrid id="basicInfoFields" border="0"
																			width="100%" cellpadding="1" cellspacing="1"
																			rendered="#{ pages$CollectionProcedure.pageModeAddEdit ||  
 																						     pages$CollectionProcedure.pageModeCollect 
 																						   }"
																			columns="4">
																			<h:outputLabel style="font-weight:normal;"
																				styleClass="TABLE_LABEL"
																				value="#{msg['collectionProcedure.assetName']} :"></h:outputLabel>
																			<h:selectOneMenu id="assetName"
																				binding="#{pages$CollectionProcedure.cmbAssets}"
																				disabled="#{
																							pages$CollectionProcedure.confirmBankTransfer || 
																							!pages$CollectionProcedure.isInheritanceFileAvailable
																						   }"
																				value="#{pages$CollectionProcedure.collectionPaymentDetailsView.singleCollectionTrxView.selectOneAssetId}"
																				onchange="onSelectionChanged(this);">
																				<f:selectItems
																					value="#{pages$CollectionProcedure.assetNameList}" />
																			</h:selectOneMenu>
																			<h:outputLabel style="font-weight:normal;"
																				styleClass="TABLE_LABEL"
																				value="#{msg['searchAssets.assetType']} :"></h:outputLabel>
																			<h:inputText id="assetTypeName" readonly="true"
																				styleClass="INPUT_TEXT READONLY"
																				value="#{pages$CollectionProcedure.englishLocale? 
																						pages$CollectionProcedure.collectionPaymentDetailsView.singleCollectionTrxView.assetTypeEn: 
																						pages$CollectionProcedure.collectionPaymentDetailsView.singleCollectionTrxView.assetTypeAr}">
																			</h:inputText>
																			<h:outputLabel style="font-weight:normal;"
																				value="#{msg['collectionProcedure.collectedAs']} :"></h:outputLabel>

																			<h:selectOneMenu id="paymentMethodId"
																				disabled="#{pages$CollectionProcedure.confirmBankTransfer }"
																				binding="#{pages$CollectionProcedure.collectedAsSelectMenu}"
																				value="#{pages$CollectionProcedure.collectionPaymentDetailsView.paymentMethod}"
																				tabindex="3" onchange="onCollectedAsChange(this);">
																				<f:selectItem
																					itemLabel="#{msg['commons.pleaseSelect']}"
																					itemValue="-1" />

																				<f:selectItems
																					value="#{pages$CollectionProcedure.collectedAsList}" />
																			</h:selectOneMenu>
																			<h:outputLabel style="font-weight:normal;"
																				value="#{msg['collectionProcedure.amount']} :"></h:outputLabel>
																			<h:inputText id="collectionamount"
																				binding="#{pages$CollectionProcedure.txtAmount}"
																				value="#{pages$CollectionProcedure.collectionPaymentDetailsView.amount}">
																			</h:inputText>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['paymentDetailsPopUp.referenceNumber']}:"></h:outputLabel>
																			<h:inputText id="referenceNum"
																				value="#{pages$CollectionProcedure.collectionPaymentDetailsView.referenceNo}"
																				binding="#{pages$CollectionProcedure.txtRefNum}"
																				tabindex="10">
																			</h:inputText>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['paymentDetailsPopUp.bank']}:"></h:outputLabel>
																			<h:selectOneMenu id="bank"
																				value="#{pages$CollectionProcedure.collectionPaymentDetailsView.bankId}"
																				tabindex="3"
																				binding="#{pages$CollectionProcedure.cmbBank}">
																				<f:selectItem
																					itemLabel="#{msg['commons.pleaseSelect']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$ApplicationBean.bankList}" />
																			</h:selectOneMenu>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['paymentDetailsPopUp.date']}:"></h:outputLabel>

																			<rich:calendar id="createdOnDateId"
																				styleClass="READONLY" disabled="false"
																				value="#{pages$CollectionProcedure.collectionPaymentDetailsView.refDate}"
																				locale="#{pages$CollectionProcedure.locale}"
																				popup="true"
																				datePattern="#{pages$CollectionProcedure.dateFormat}"
																				showApplyButton="false" enableManualInput="false"
																				inputStyle="width:185px;height:17px"
																				binding="#{pages$CollectionProcedure.refDate}" />
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['paymentDetailsPopUp.accountNumber']}:"></h:outputLabel>

																			<h:inputText id="accountNumber"
																				value="#{pages$CollectionProcedure.collectionPaymentDetailsView.accountNo}"
																				tabindex="10" maxlength="23"
																				binding="#{pages$CollectionProcedure.txtAccNum}">
																			</h:inputText>


																			<h:outputLabel style="font-weight:normal;"
																				styleClass="TABLE_LABEL"
																				value="#{msg['collectionProcedure.description']} :"></h:outputLabel>
																			<t:panelGroup colspan="4">
																				<h:inputTextarea id="description"
																					value="#{pages$CollectionProcedure.collectionPaymentDetailsView.description}"
																					style="width:570px;">
																				</h:inputTextarea>
																			</t:panelGroup>


																			<h:outputLabel style="font-weight:normal;"
																				styleClass="TABLE_LABEL"
																				value="#{msg['collectionProcedure.oldPaymentNo']} :"></h:outputLabel>
																			<t:panelGroup colspan="4">
																				<h:inputText id="oldPaymentNo"
																					value="#{pages$CollectionProcedure.collectionPaymentDetailsView.singleCollectionTrxView.oldCollectionTrxNumber}"
																					readonly="true" styleClass="READONLY">
																				</h:inputText>
																				<h:graphicImage id="searchPayments"
																					onclick="javaScript:openSearchPayments('#{pages$CollectionProcedure.inheritanceFileId}')"
																					url="../resources/images/magnifier.gif" />
																			</t:panelGroup>

																		</t:panelGrid>

																		<t:panelGrid style="width:100%;" columns="4"
																			rendered="#{ pages$CollectionProcedure.pageModeAddEdit ||  
 																						     pages$CollectionProcedure.pageModeCollect 
 																						   }"
																			styleClass="BUTTON_TD">

																			<h:commandButton styleClass="BUTTON"
																				value="#{msg['commons.Add']}"
																				action="#{pages$CollectionProcedure.onAdd}"
																				style="width: 135px">
																			</h:commandButton>
																		</t:panelGrid>



																		<t:div styleClass="contentDiv" style="width: 99%">
																			<t:dataTable id="dtCollectedPayemnt"
																				value="#{pages$CollectionProcedure.collectionList}"
																				binding="#{pages$CollectionProcedure.dtCollection}"
																				width="100%" rows="200" preserveDataModel="false"
																				preserveSort="false" var="dataItem"
																				rowClasses="row1,row2" rules="all"
																				renderedIfEmpty="true"
																				rowId="#{dataItem.paymentDetailsId}">

																				<t:column id="selected" width="10%" sortable="false"
																					rendered="#{
 
																										!pages$CollectionProcedure.pageModeCollect && 
																							            !pages$CollectionProcedure.pageModeAddEdit &&
																							            !pages$CollectionProcedure.pageModeView
																							          }"
																					style="white-space: normal;">
																					<f:facet name="header">
																						<t:outputText value="" />
																					</f:facet>
																					<h:selectBooleanCheckbox id="select"
																						value="#{dataItem.selected}" />
																				</t:column>



																				<t:column id="collectionTrxNumber" width="10%"
																					style="white-space: normal;"
																					rendered="#{pages$CollectionProcedure.requestView.requestId != null and  not empty pages$CollectionProcedure.requestView.requestId }">

																					<f:facet name="header">
																						<t:outputText id="lblTrxNo"
																							value="#{msg['collectionProcedure.TrxNumber']}" />

																					</f:facet>
																					<t:outputText id="valTrxNo"
																						value="#{dataItem.singleCollectionTrxView.trxNumber}"
																						rendered="#{dataItem.isDeleted == 0 && dataItem.isBeingEdited==0}"
																						style="white-space: normal;" />

																				</t:column>

																				<t:column id="collectionPaymentMETHOD" width="10%"
																					defaultSorted="true" style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['collectionProcedure.collectedAs']}" />

																					</f:facet>
																					<t:outputText
																						rendered="#{dataItem.isDeleted == 0 && dataItem.isBeingEdited==0}"
																						value="#{ pages$CollectionProcedure.englishLocale ?dataItem.paymentMethodEn:dataItem.paymentMethodAr}"
																						style="white-space: normal;" />

																				</t:column>
																				<t:column id="collectionPaymentAmount" width="10%"
																					style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['collectionProcedure.amount']}" />

																					</f:facet>
																					<t:outputText value="#{dataItem.amount}"
																						rendered="#{dataItem.isDeleted == 0 && dataItem.isBeingEdited==0}"
																						style="white-space: normal;" />

																				</t:column>
																				<t:column id="collectionPaymentRefNum" width="10%"
																					style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['paymentDetailsPopUp.referenceNumber']}" />

																					</f:facet>
																					<t:outputText value="#{dataItem.referenceNo}"
																						rendered="#{dataItem.isDeleted == 0 && dataItem.isBeingEdited==0}"
																						style="white-space: normal;" />

																				</t:column>
																				<t:column id="collectionPaymentRefDate" width="10%"
																					style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['paymentDetailsPopUp.date']}" />

																					</f:facet>
																					<t:outputText value="#{dataItem.refDate}"
																						rendered="#{dataItem.isDeleted == 0 && dataItem.isBeingEdited==0}"
																						style="white-space: normal;">
																						<f:convertDateTime
																							pattern="#{pages$CollectionProcedure.dateFormat}"
																							timeZone="#{pages$CollectionProcedure.timeZone}" />

																					</t:outputText>

																				</t:column>


																				<t:column id="collectionBank" width="10%"
																					defaultSorted="true" style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['paymentDetailsPopUp.bank']}" />

																					</f:facet>
																					<t:outputText
																						rendered="#{dataItem.isDeleted == 0 && dataItem.isBeingEdited==0}"
																						value="#{ pages$CollectionProcedure.englishLocale ?dataItem.bankEn:dataItem.bankAr}"
																						style="white-space: normal;" />

																				</t:column>
																				<t:column id="AssetName" width="10%"
																					sortable="false" style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['collectionProcedure.assetName']}" />
																					</f:facet>
																					<h:commandLink
																						rendered="#{dataItem.isDeleted == 0 && dataItem.isBeingEdited == 0}"
																						action="#{pages$CollectionProcedure.openManageAsset}"
																						value="#{pages$CollectionProcedure.englishLocale? dataItem.singleCollectionTrxView.assetNameEn:dataItem.singleCollectionTrxView.assetNameAr}"
																						style="white-space: normal;" styleClass="A_LEFT" />

																				</t:column>

																				<t:column id="AssetType" width="10%"
																					sortable="false" style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['collectionProcedure.assetType']}" />
																					</f:facet>
																					<t:outputText
																						rendered="#{dataItem.isDeleted == 0 && dataItem.isBeingEdited == 0}"
																						value="#{pages$CollectionProcedure.englishLocale? dataItem.singleCollectionTrxView.assetTypeEn:dataItem.singleCollectionTrxView.assetTypeAr}"
																						style="white-space: normal;" styleClass="A_LEFT" />

																				</t:column>
																				<t:column id="description" width="10%"
																					sortable="false" style="white-space: normal;">

																					<f:facet name="header">
																						<t:outputText
																							value="#{msg['collectionProcedure.description']}" />
																					</f:facet>
																					<t:div
																						style="white-space: normal;word-wrap: break-word;width:80%;">
																						<t:outputText
																							rendered="#{dataItem.isDeleted == 0 && dataItem.isBeingEdited == 0}"
																							value="#{dataItem.description}"
																							style="white-space: normal;" styleClass="A_LEFT" />
																					</t:div>

																				</t:column>





																				<t:column id="actionGrid" width="5%"
																					style="white-space: normal;" sortable="false">
																					<f:facet name="header">
																						<t:outputText value="#{msg['commons.action']}" />
																					</f:facet>
																					<t:commandLink
																						rendered="#{ dataItem.isDeleted == 0 && dataItem.isBeingEdited==0  && 
																						            ( pages$CollectionProcedure.pageModeAddEdit || 
 																									  pages$CollectionProcedure.pageModeCollect 
 																									 )
 																									}"
																						action="#{pages$CollectionProcedure.onEdit}">
																						<h:graphicImage id="editIcon"
																							title="#{msg['commons.edit']}"
																							binding="#{pages$CollectionProcedure.imgEditCollection}"
																							url="../resources/images/edit-icon.gif" />
																					</t:commandLink>
																					<t:commandLink
																						rendered="#{
																									dataItem.isDeleted == 0 && dataItem.isBeingEdited==0 && 
																									!pages$CollectionProcedure.pageModeCollect &&
																									!pages$CollectionProcedure.pageModeApproveReject &&  
																						            !pages$CollectionProcedure.pageModeAddEdit &&
																						            dataItem.paymentMethod != '1'
																						            }"
																						action="#{pages$CollectionProcedure.onOpenDistributePopUp}">
																						<h:graphicImage id="addIcon"
																							title="#{msg['common.addDCollectionDetails']}"
																							url="../resources/images/app_icons/Add-New-Tenant.png" />
																					</t:commandLink>

																					<t:commandLink
																						rendered="#{dataItem.isDeleted == 0 && dataItem.isBeingEdited==0 && pages$CollectionProcedure.pageModeAddEdit && 
																						            !pages$CollectionProcedure.confirmBankTransfer}"
																						onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this);"
																						action="#{pages$CollectionProcedure.onDelete}">
																						<h:graphicImage id="deleteIcon"
																							binding="#{pages$CollectionProcedure.imgDeleteCollection}"
																							title="#{msg['commons.delete']}"
																							url="../resources/images/delete_icon.png" />
																					</t:commandLink>

																					<h:commandLink
																						rendered="#{dataItem.isDeleted == 0 && dataItem.isBeingEdited==0 &&
																									pages$CollectionProcedure.requestView.statusId==9005  
																									||pages$CollectionProcedure.requestView.statusId==90054
																									
																								
																						 			}"
																						action="#{pages$CollectionProcedure.openPaymentReceipt}">
																						<h:graphicImage id="printPayments"
																							rendered="#{!empty dataItem.paymentDetailsId}"
																							binding="#{pages$CollectionProcedure.imgPrintPaymentReceipt}"
																							title="#{msg['commons.print']}"
																							url="../resources/images/app_icons/print.gif" />
																					</h:commandLink>

																				</t:column>
																			</t:dataTable>
																		</t:div>
																		<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																			style="width:99%;">
																			<t:panelGrid columns="2" border="0" width="100%"
																				cellpadding="1" cellspacing="1">
																				<t:panelGroup
																					styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																					<t:div styleClass="JUG_NUM_REC_ATT">
																						<h:outputText value="#{msg['commons.records']}" />
																						<h:outputText value=" : " />
																						<h:outputText
																							value="#{pages$CollectionProcedure.totalCollectionCount}" />
																					</t:div>
																				</t:panelGroup>
																				<t:panelGroup>
																					<t:dataScroller id="shareScrollers"
																						for="dtCollectedPayemnt" paginator="true"
																						fastStep="1" immediate="false"
																						paginatorTableClass="paginator"
																						renderFacetsIfSinglePage="true"
																						paginatorTableStyle="grid_paginator"
																						layout="singleTable"
																						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																						paginatorActiveColumnStyle="font-weight:bold;"
																						paginatorRenderLinkForActive="false"
																						pageIndexVar="pageNumber"
																						styleClass="JUG_SCROLLER">
																						<f:facet name="first">
																							<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastrewind">
																							<t:graphicImage
																								url="../#{path.scroller_fastRewind}"></t:graphicImage>
																						</f:facet>
																						<f:facet name="fastforward">
																							<t:graphicImage
																								url="../#{path.scroller_fastForward}"></t:graphicImage>
																						</f:facet>
																						<f:facet name="last">
																							<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
																						</f:facet>

																						<t:div>
																							<table cellpadding="0" cellspacing="0">
																								<tr>
																									<td>
																										<h:outputText styleClass="PAGE_NUM"
																											style="font-size:9;color:black;"
																											value="#{msg['mems.normaldisb.label.totalAmount']}:" />
																									</td>
																									<td>
																										<h:outputText styleClass="PAGE_NUM"
																											style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;font-size:9;font-weight:bold;color:black"
																											value="#{pages$CollectionProcedure.totalCollectionAmount}" />
																									</td>
																								</tr>
																							</table>
																						</t:div>
																					</t:dataScroller>
																				</t:panelGroup>
																			</t:panelGrid>
																		</t:div>
																	</rich:tab>
																	<!-- Review Details Tab - Start -->
																	<rich:tab id="reviewTab"
																		rendered="#{ (
																		      	     	pages$CollectionProcedure.pageModeConfirm ||
																		        		pages$CollectionProcedure.pageModeReview ||
																		           		pages$CollectionProcedure.pageModeView
																	             		) 
																	              && 
																	            !pages$CollectionProcedure.confirmBankTransfer
																	           }"
																		label="#{msg['commons.tab.reviewDetails']}">
																		<jsp:include page="reviewDetailsTab.jsp"></jsp:include>
																	</rich:tab>
																	<!-- Review Details Tab - End -->
																	<rich:tab id="attachmentTab"
																		label="#{msg['commons.attachmentTabHeading']}">
																		<%@  include file="attachment/attachment.jsp"%>
																	</rich:tab>
																	<rich:tab label="#{msg['attachment.grid.commentsCol']}"
																		action="#{pages$CollectionProcedure.tabAttachmentsComments_Click}">
																		<%@ include file="notes/notes.jsp"%>
																	</rich:tab>
																	<rich:tab
																		rendered="#{pages$CollectionProcedure.reqestPresent}"
																		label="#{msg['mems.inheritanceFile.tabHeadingShort.history']}"
																		action="#{pages$CollectionProcedure.onRequestHistoryTabClick}">
																		<%@ include file="../pages/requestTasks.jsp"%>
																	</rich:tab>








																</rich:tabPanel>


															</div>
															<table cellpadding="0" cellspacing="0" width="100%">

																<tr>
																	<td style="FONT-SIZE: 0px">
																		<IMG
																			src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																			class="TAB_PANEL_LEFT" />
																	</td>
																	<td width="100%" style="FONT-SIZE: 0px">
																		<IMG
																			src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																			class="TAB_PANEL_MID" style="width: 100%;" />
																	</td>
																	<td style="FONT-SIZE: 0px">
																		<IMG
																			src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																			class="TAB_PANEL_RIGHT" />
																	</td>
																</tr>
															</table>
															<t:div styleClass="BUTTON_TD"
																style="padding-top:10px; padding-right:21px;">
																<pims:security
																	screen="Pims.MinorMgmt.CollectionRequest.Create"
																	action="create">
																	<h:commandButton styleClass="BUTTON" id="saveButton"
																		value="#{msg['commons.saveButton']}"
																		binding="#{pages$CollectionProcedure.btnSave}"
																		rendered="#{ 
																		             ( pages$CollectionProcedure.pageModeAddEdit || 
																		               pages$CollectionProcedure.pageModeConfirm 
																		             )
																		            && 
																		           !pages$CollectionProcedure.confirmBankTransfer 
																	           	   }"
																		onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this);"
																		style="width: auto">
																	</h:commandButton>

																	<h:commandLink id="lnkSave"
																		action="#{pages$CollectionProcedure.btnSave_Click}" />
																</pims:security>
																<pims:security
																	screen="Pims.MinorMgmt.CollectionRequest.Create"
																	action="create">
																	<h:commandButton styleClass="BUTTON" id="btnSubmit"
																		value="#{msg['commons.submitButton']}"
																		binding="#{pages$CollectionProcedure.btnSubmit}"
																		rendered="#{ pages$CollectionProcedure.pageModeAddEdit && 
																	            !pages$CollectionProcedure.confirmBankTransfer && 
																	             pages$CollectionProcedure.reqestPresent
																	           }"
																		onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this);">
																	</h:commandButton>
																	<h:commandLink id="lnkSubmit"
																		action="#{pages$CollectionProcedure.btnSubmit_Click}" />
																</pims:security>

																<pims:security
																	screen="Pims.MinorMgmt.CollectionRequest.CollectPayments"
																	action="create">

																	<h:commandButton styleClass="BUTTON" id="btnCollect"
																		value="#{msg['commons.collectAppFee']}"
																		rendered="#{ pages$CollectionProcedure.pageModeCollect && 
																	            !pages$CollectionProcedure.confirmBankTransfer
																	            &&
																		            pages$CollectionProcedure.taskPresent
																	           }"
																		onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this);" />
																	<h:commandLink id="lnkCollect"
																		action="#{pages$CollectionProcedure.onCollect}" />

																	<h:commandButton styleClass="BUTTON"
																		id="btnConfirmBankTransfer"
																		value="#{msg['buttons.Confirm']}"
																		rendered="#{pages$CollectionProcedure.pageModeAddEdit && pages$CollectionProcedure.confirmBankTransfer}"
																		onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this);"
																		style="width: auto" />
																	<h:commandLink id="lnkConfirmBankTransfer"
																		action="#{pages$CollectionProcedure.onConfirmingBankTransfer}" />

																	<h:commandButton styleClass="BUTTON"
																		id="btnMoveCollect"
																		value="#{msg['commons.collectAppFee']}"
																		rendered="#{ pages$CollectionProcedure.pageModeMoveCollectionTask && 
																	            !pages$CollectionProcedure.confirmBankTransfer
																	           }"
																		onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this);" />
																	<h:commandLink id="lnkMoveCollect"
																		action="#{pages$CollectionProcedure.onMoveCollect}" />
																</pims:security>
																<pims:security
																	screen="Pims.MinorMgmt.CollectionRequest.ApproveReject"
																	action="create">

																	<h:commandButton styleClass="BUTTON" id="btnApprove"
																		value="#{msg['commons.approve']}"
																		rendered="#{ 
																					pages$CollectionProcedure.pageModeApproveReject &&
																		            pages$CollectionProcedure.taskPresent
																	           	   }"
																		onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this);" />
																	<h:commandLink id="lnkApprove"
																		action="#{pages$CollectionProcedure.onApprove}" />

																	<h:commandButton styleClass="BUTTON" id="btnReject"
																		value="#{msg['commons.reject']}"
																		rendered="#{ 
																					pages$CollectionProcedure.pageModeApproveReject &&
																		            pages$CollectionProcedure.taskPresent
																	           	   }"
																		onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this);" />
																	<h:commandLink id="lnkReject"
																		action="#{pages$CollectionProcedure.onReject}" />

																</pims:security>
																<pims:security
																	screen="Pims.MinorMgmt.CollectionRequest.Distribute"
																	action="create">
																	<h:commandButton styleClass="BUTTON" id="btnDistribute"
																		value="#{msg['distribution.distribute']}"
																		rendered="#{ pages$CollectionProcedure.pageModeConfirm && 
																	            !pages$CollectionProcedure.confirmBankTransfer &&
																		            pages$CollectionProcedure.taskPresent
																	           }"
																		onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this);" />
																	<h:commandLink id="lnkDistribute"
																		action="#{pages$CollectionProcedure.onDistribute}" />

																	<h:commandButton styleClass="BUTTON"
																		id="btnMoveDistribute"
																		value="#{msg['distribution.distribute']}"
																		rendered="#{ pages$CollectionProcedure.pageModeMoveDistributionTask && 
																	            !pages$CollectionProcedure.confirmBankTransfer
																	           }"
																		onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this);" />
																	<h:commandLink id="lnkMOveDistribute"
																		action="#{pages$CollectionProcedure.onMOveDistribute}" />

																	<h:commandButton styleClass="BUTTON" id="btnReview"
																		value="#{msg['commons.reviewButton']}"
																		rendered="#{ pages$CollectionProcedure.pageModeConfirm && 
																	            !pages$CollectionProcedure.confirmBankTransfer &&
																		            pages$CollectionProcedure.taskPresent
																	           }"
																		onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this);" />
																	<h:commandLink id="lnkReview"
																		action="#{pages$CollectionProcedure.onReview}" />
																</pims:security>
																<pims:security
																	screen="Pims.MinorMgmt.CollectionRequest.Review"
																	action="create">
																	<h:commandButton styleClass="BUTTON" id="btnReviewDone"
																		value="#{msg['commons.done']}"
																		rendered="#{ pages$CollectionProcedure.pageModeReview  &&
																		            pages$CollectionProcedure.taskPresent}"
																		onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false;else disableButtons(this);" />
																	<h:commandLink id="lnkReviewDone"
																		action="#{pages$CollectionProcedure.reviewDone}" />
																</pims:security>
																<h:commandButton styleClass="BUTTON" id="btnPrint"
																	value="#{msg['commons.print']}"
																	rendered="#{!pages$CollectionProcedure.pageModeAddEdit }"
																	onclick="disableButtons(this);">
																</h:commandButton>
																<h:commandLink id="lnkPrint"
																	action="#{pages$CollectionProcedure.onPrint}" />

															</t:div>
												</h:form>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<tr
							style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
							<td colspan="2">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<script language="javascript" type="text/javascript">
	
		     var finId = document.getElementById('detailsFrm:rowId').value;
		     if(finId.length>0)
		     {
	           	var tbl =document.getElementById('detailsFrm:dtCollectedPayemnt');
	            var trs = tbl.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
	            for (var i = 0; i < trs.length; i++) 
	            {
	               trs[i].style.backgroundColor = '';
	                if(trs[i].getAttribute("id")== finId)
		              	trs[i].style.backgroundColor = '#f9a60c';
	           	}
           	}
	
	</script>
		</body>
	</html>
</f:view>