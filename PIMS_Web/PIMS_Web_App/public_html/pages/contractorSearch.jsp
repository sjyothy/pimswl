<%@ page import="javax.faces.component.UIViewRoot;"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="javascript" type="text/javascript">
				function closeWindowSubmit()
				{
					window.opener.document.forms[0].submit();
				  	window.close();	  
				}
				
				function clearWindow()
				 {
					document.getElementById("frmSearchContractor:txtGrpNumber").value="";
					//document.getElementById("frmSearchContractor:txtContractorNo").value="";	
					document.getElementById("frmSearchContractor:txtCommercialName").value="";
				    //document.getElementById("frmSearchContractor:selectContractorType").value="-1";
					document.getElementById("frmSearchContractor:txtContractorNameEn").value="";
					document.getElementById("frmSearchContractor:txtContractorNameAr").value="";
					document.getElementById("frmSearchContractor:txtOfficeNumber").value="";
					document.getElementById("frmSearchContractor:txtLicenseNumber").value="";
					document.getElementById("frmSearchContractor:selectLicenseSource").value="-1";
					document.getElementById("frmSearchContractor:selectCountry").value="-1";
					document.getElementById("frmSearchContractor:selectState").value="-1";
					document.getElementById("frmSearchContractor:selectCity").value="-1";
				}
				
								
				function closeWindow() {
					window.opener="x";
				  	window.close();
				}
				
				function sendToParent(contractorId, contractorNumber, contractorNameEn, contractorNameAr, licenseNumber, licenseSource, officeNumber, contractorTypeId) {				    
				    window.opener.populateContractor(contractorId, contractorNumber, contractorNameEn, 
				    								contractorNameAr, licenseNumber, licenseSource, officeNumber, contractorTypeId);
				    window.close();
				}
					

					
					
			    function closeWindow() {
					window.close();
				}
				
				  function showContractorAddPopUp(contractorTypeId)
                 	{
				     var  screen_width = screen.width;
				     var screen_height = screen.height;
				     
				     window.open('addContractor.jsf?MODE_POPUP=popup'+'&contractorTypeId='+contractorTypeId
				      ,'_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
				     window.close(); 
	                }
	                
	                function showContractorDetailPopUp(contractorTypeId)
                 	{
				     var  screen_width = screen.width;
				     var screen_height = screen.height;
				     
				     window.open('contractorDetails.jsf?pageMode=popUp'+'&contractorTypeId='+contractorTypeId
				      ,'_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
				     window.close(); 
	                }
	                
	    
	                
			</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />

			
		</head>

		<body class="BODY_STYLE">
		<c:choose>
		  <c:when test="${!pages$contractorSearch.isViewModePopUp}">
		   <div class="containerDiv">
		  </c:when>
		 </c:choose>  
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
						<c:when test="${!pages$contractorSearch.isViewModePopUp}">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</c:when>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!pages$contractorSearch.isViewModePopUp}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>

					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['ContractorSearch.heading']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="3" />
								<td width="100%" height="450px" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION">
										<h:form id="frmSearchContractor" style="width:96%">
											<div style="height: 25px;">
												<table border="0" class="layoutTable">
													<tr>
														<td colspan="6">
														<h:outputText value="#{pages$contractorSearch.errorMessages}" escape="false" styleClass="ERROR_FONT" style="padding:10px;"/>
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
													<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
													<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
				                                 </table>
												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['inquiry.searchcriteria']}"
														styleClass="DETAIL_SECTION_LABEL" />
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
													<%--	<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.GRPNumber']}:" />
															</td>
															<td>
																<h:inputText styleClass="A_LEFT_SMALL" maxlength="15"
																	id="txtGrpNumber" style="width:200px;"
																	value="#{pages$contractorSearch.grpNumber}" />
															</td>

														 <td width="20%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.contractorNumber']}:" />
															</td>
															<td width="30%">
																<h:inputText styleClass="A_LEFT_SMALL" maxlength="15"
																	id="txtContractorNo" style="width:200px;"
																	value="#{pages$contractorSearch.contractorNumber}" />
															</td> 
														</tr> --%>
														<tr>
														<td width="97%">
														<table width="100%">
														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.commercialName']}:" />
															</td>
															<td width="25%">
																<h:inputText maxlength="100"
																	id="txtCommercialName" 
																	value="#{pages$contractorSearch.commercialName}" />
															</td>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.contractorType']}:" />
															</td>
															<td width="25%">
																<h:selectOneMenu id="selectContractorType"
																	binding="#{pages$contractorSearch.cmbContractorType}"
																	value="#{pages$contractorSearch.contractorType}">
																	<f:selectItem  itemValue="-1" itemLabel="#{msg['commons.All']}" />
																	<f:selectItems value="#{pages$contractorSearch.contractorTypeList}" />
																</h:selectOneMenu>
															</td>
														</tr>
														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.contractorNameEn']}:" />
															</td>
															<td width="35%">
																<h:inputText  maxlength="100"
																	id="txtContractorNameEn" 
																	value="#{pages$contractorSearch.contractorNameEn}" />
															</td>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.contractorNameAr']}:" />
															</td>
															<td width="25%">
																<h:inputText maxlength="100"
																	id="txtContractorNameAr" 
																	value="#{pages$contractorSearch.contractorNameAr}" />
															</td>
														</tr>
														<tr>
															
															
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.GRPNumber']}:" />
															</td>
															<td>
																<h:inputText  maxlength="15"
																	id="txtGrpNumber" 
																	value="#{pages$contractorSearch.grpNumber}" />
															</td>															
												
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.officeNumber']}:" />
															</td>
															<td width="25%">
																<h:inputText maxlength="15"
																	id="txtOfficeNumber" 
																	value="#{pages$contractorSearch.officeNumber}" />
															</td>

														</tr>
														<tr>
																									
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.licenseNumber']}:" />
															</td>
															<td width="25%">
																<h:inputText maxlength="15"
																	id="txtLicenseNumber" 
																	value="#{pages$contractorSearch.licenseNumber}" />
															</td>															
															
															
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.licenseSource']}:" />
															</td>
															<td>
																<h:selectOneMenu id="selectLicenseSource"
																	
																	value="#{pages$contractorSearch.licenseSource}">
																	<f:selectItem id="selectAllSource" itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems
																		value="#{pages$contractorSearch.licenseSourceList}" />
																</h:selectOneMenu>
															</td>

														</tr>
														<tr>
														
												     		<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.country']}:" />
															</td>
															<td>
																<h:selectOneMenu id="selectCountry" 
																	value="#{pages$contractorSearch.country}">
																	<f:selectItem id="selectAllCountry" itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems
																		value="#{pages$contractorSearch.countryList}" />
																	<a4j:support event="onchange"
																		action="#{pages$contractorSearch.loadState}"
																		reRender="selectState,selectCity" />
																</h:selectOneMenu>
															</td>														
														
															<td width="20%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contact.state']}:" />
															</td>
															<td>
																<h:selectOneMenu id="selectState" 
																	value="#{pages$contractorSearch.state}">
																	<f:selectItem id="selectAllState" itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems
																		value="#{pages$contractorSearch.stateList}" />
																	<a4j:support event="onchange"
																		action="#{pages$contractorSearch.loadCity}"
																		reRender="selectCity" />
																</h:selectOneMenu>
															</td>
														</tr>
															
														<tr>
															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['ContractorSearch.city']}:" />
															</td>
															<td>
																<h:selectOneMenu id="selectCity" 
																	value="#{pages$contractorSearch.city}">
																	<f:selectItem id="selectAllCity" itemValue="-1"
																		itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																	<f:selectItems
																		value="#{pages$contractorSearch.cityList}" />
																</h:selectOneMenu>
															</td>
														</tr>

														<tr>
															<td colspan="6" class="BUTTON_TD">
															
																<h:commandButton id="btnContractorSearch" type="submit" styleClass="BUTTON"
																	value="#{msg['commons.search']}"
																	action="#{pages$contractorSearch.searchContractors}" />
																	
																<h:commandButton styleClass="BUTTON" type="button"
																	value="#{msg['commons.clear']}"
																	onclick="javascript:clearWindow();" />
															
																<h:commandButton id="btnContractorAdd" type="submit" styleClass="BUTTON"
																	rendered="#{!pages$contractorSearch.isViewModePopUp}"
																	value="#{msg['commons.Add']}" 
																	action="#{pages$contractorSearch.openAddContractorPage}" />
																
																<h:commandButton id="btnCloseWindow" styleClass="BUTTON" type="button"
																	onclick="javascript:closeWindow();"
																	value="#{msg['commons.cancel']}"
																	rendered="#{pages$contractorSearch.isViewModePopUp}" />
															</td>
														</tr>
														</table>
														</td>
														<td width="3%"></td>
														</tr>
													</table>
												</div>
											</div>

											<div style="padding: 10px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" style="widht:99.6%">
													<t:dataTable id="dtContractors" width="100%"
														value="#{pages$contractorSearch.contractorsList}"
														binding="#{pages$contractorSearch.dataTable}"
														rows="#{pages$contractorSearch.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="contractor" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true"
														columnClasses="A_LEFT_GRID,A_LEFT_GRID,A_LEFT_GRID,A_LEFT_GRID,A_LEFT_GRID">

														<t:column id="col1" sortable="true"  style="white-space:normal" rendered="false" >
															<f:facet name="header">
																<t:outputText
																	value="#{msg['ContractorSearch.contractorNumber']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{contractor.contractorNumber}" />
														</t:column>


														<t:column id="col0" sortable="true"  style="white-space:normal" >
															<f:facet name="header">
																<t:outputText
																	value="#{msg['ContractorSearch.commercialName']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{contractor.commercialName}" />
														</t:column>


														<t:column id="col2" sortable="true"  style="white-space:normal">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['ContractorSearch.contractorName']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{contractor.contractorNameEn}"
																rendered="#{pages$contractorSearch.isEnglishLocale}" />
															<t:outputText styleClass="A_LEFT"
																value="#{contractor.contractorNameAr}"
																rendered="#{!pages$contractorSearch.isEnglishLocale}" />
														</t:column>

														<t:column id="col3" sortable="true"  style="white-space:normal">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['ContractorSearch.contractorType']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{contractor.contractorTypeEn}"
																rendered="#{pages$contractorSearch.isEnglishLocale}" />
															<t:outputText styleClass="A_LEFT"
																value="#{contractor.contractorTypeAr}"
																rendered="#{!pages$contractorSearch.isEnglishLocale}" />
														</t:column>

														<t:column id="col4" sortable="true"  style="white-space:normal">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['ContractorSearch.licenseNumber']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{contractor.licenseNumber}" />
														</t:column>

														<t:column id="col5" sortable="true"  style="white-space:normal">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['ContractorSearch.licenseSource']}" />
															</f:facet>
															<t:outputText styleClass="A_LEFT"
																value="#{contractor.licenseSource}" />
														</t:column>

														<t:column id="col7"
															rendered="#{!pages$contractorSearch.isViewModePopUp}"  style="white-space:normal">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.actions']}" />
															</f:facet>
															<h:commandLink id="viewLink" action="#{pages$contractorSearch.viewDetails}" >
																<h:graphicImage id="viewIcon" title="#{msg['commons.view']}" url="../resources/images/detail-icon.gif" />
															</h:commandLink>
																														
															<h:commandLink id="editLink" action="#{pages$contractorSearch.editDetails}" >
																<h:graphicImage id="editIcon" title="#{msg['commons.edit']}" url="../resources/images/edit-icon.gif" />
															</h:commandLink>
															
														<%--	<a4j:commandLink id="deleteLink" reRender="dtContractors,record_Size"
																action="#{pages$contractorSearch.removeContractor}">
																<h:graphicImage id="deleteIcon"
																	title="#{msg['commons.delete']}"
																	url="../resources/images/delete_icon.png" />
															</a4j:commandLink> --%>
															
															<h:commandLink id="deleteLink" 
																action="#{pages$contractorSearch.removeContractor}">
																<h:graphicImage id="deleteIcon"
																	title="#{msg['commons.delete']}"
																	url="../resources/images/delete_icon.png" />
															</h:commandLink>
														</t:column>
														
														
															<t:column style="width:60px" sortable="false"
																rendered="#{pages$contractorSearch.isPageModeSelectManyPopUp}">
																<f:facet name="header">
																	<t:outputLabel value="#{msg['commons.select']}" style="font-size:10px;width:60px"/>
																</f:facet>
																<h:selectBooleanCheckbox id="select" style="width:13px"
																styleClass="A_CENTER"
																	value="#{contractor.isSelected}" />
															</t:column>

														<t:column id="col8"
															rendered="#{pages$contractorSearch.isPageModeSelectOnePopUp}"  style="white-space:normal">
															<f:facet name="header">
																<t:outputText value="#{msg['commons.select']}" />
															</f:facet>
															<h:graphicImage style="width: 16;height: 16;border: 0"
																alt="#{msg['commons.select']}"
																url="../resources/images/select-icon.gif"
																onclick="javascript:sendToParent('#{contractor.personId}','#{contractor.contractorNumber}','#{contractor.contractorNameEn}','#{contractor.contractorNameAr}','#{contractor.licenseNumber}','#{contractor.licenseSource}','#{contractor.officeNumber}','#{contractor.contractorTypeId}');" />
														</t:column>
													</t:dataTable>
												</div>

												<div class="contentDivFooter" style="width: 99.1%">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText 
																					id="record_Size"
																					value="#{pages$contractorSearch.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">
																<CENTER>
																	<t:dataScroller id="scroller" for="dtContractors"
																		paginator="true" fastStep="1"
																		paginatorMaxPages="#{pages$contractorSearch.paginatorMaxPages}"
																		immediate="false" paginatorTableClass="paginator"
																		renderFacetsIfSinglePage="true"
																		pageIndexVar="pageNumber" styleClass="SCH_SCROLLER"
																		paginatorActiveColumnStyle="font-weight:bold;"
																		paginatorRenderLinkForActive="false"
																		paginatorTableStyle="grid_paginator"
																		layout="singleTable"
																		paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

																		<f:facet name="first">
																			<t:graphicImage url="../#{path.scroller_first}"
																				id="lblF" />
																		</f:facet>
																		<f:facet name="fastrewind">
																			<t:graphicImage url="../#{path.scroller_fastRewind}"
																				id="lblFR" />
																		</f:facet>
																		<f:facet name="fastforward">
																			<t:graphicImage url="../#{path.scroller_fastForward}"
																				id="lblFF" />
																		</f:facet>
																		<f:facet name="last">
																			<t:graphicImage url="../#{path.scroller_last}"
																				id="lblL" />
																		</f:facet>
																		<t:div styleClass="PAGE_NUM_BG">
																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{requestScope.pageNumber}" />
																					</td>
																				</tr>
																			</table>
																		</t:div>
																	</t:dataScroller>
																</center>
															</td>
														</tr>
													</table>
												</div>
												
										<div>
											<table width="99.6%" border="0">
												<tr>
													<td class="BUTTON_TD JUG_BUTTON_TD_US" >

														<h:commandButton type="submit"
															rendered="#{pages$contractorSearch.isPageModeSelectManyPopUp}"
															styleClass="BUTTON"
															actionListener="#{pages$contractorSearch.sendManyContractorInfoToParent}"
															value="#{msg['commons.select']}" />
													</td>

												</tr>


											</table>
												
										</div>		
												
												
												
											</div>
										</h:form>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		<c:choose>
			<c:when test="${!pages$contractorSearch.isViewModePopUp}">	
		 </div>
		 </c:when>
		</c:choose> 	
		</body>
	</html>
</f:view>