
<t:panelGrid id="progRecommendationsGrdFields"
	styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%"
	cellspacing="5px" columns="4">
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['socialProgram.lbl.Recommendations']}" />
	</h:panelGroup>
	<h:inputTextarea
		value="#{pages$programRecommendations.programRecommendationsView.recommendations}"
		style="width: 185px;" />
</t:panelGrid>
<t:panelGrid id="progRecommendationsGrdActions" styleClass="BUTTON_TD"
	cellpadding="1px" width="100%" cellspacing="5px">
	<h:panelGroup>
		<h:commandButton styleClass="BUTTON"
			value="#{msg['commons.saveButton']}"
			action="#{pages$programRecommendations.onSave}" />
	</h:panelGroup>
</t:panelGrid>
<t:div styleClass="contentDiv" style="width:98%"
	id="progRecommendationsContentDiv">
	<t:dataTable id="progRecommendationssDataTable" rows="100"
		value="#{pages$programRecommendations.list}"
		binding="#{pages$programRecommendations.dataTable}"
		preserveDataModel="false" preserveSort="false" var="dataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">

		<t:column id="progRecommendationsdate" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.PurposeGrid.lbl.Date']}"
					style="white-space: normal;" />
			</f:facet>

			<t:outputText style="white-space: normal;"
				value="#{dataItem.createdOn}" 				rendered="#{dataItem.isDeleted != '1' }"> 
				<f:convertDateTime
					pattern="#{pages$programRecommendations.dateFormat}"
					timeZone="#{pages$programRecommendations.timeZone}" />
			</t:outputText>
		</t:column>
		<t:column id="progRecommendationsCreatedBy" width="15%"
			sortable="true">
			<f:facet name="header">
				<t:outputText
					value="#{msg['socialProgram.Requirement.lbl.CreatedBy']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;" 				rendered="#{dataItem.isDeleted != '1' }"
				value="#{pages$programRecommendations.englishLocale? dataItem.createdEn:dataItem.createdAr}" />
		</t:column>
		<t:column id="progRecommendationsdesc"  width="55%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.lbl.Recommendations']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText value="#{dataItem.recommendations}" 				rendered="#{dataItem.isDeleted != '1' }"
				style="white-space: normal;" />
		</t:column>
		<t:column id="programActionColumn" width="15%">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.action']}" />
			</f:facet>
			<h:graphicImage id="imgDeleteReq"
				rendered="#{dataItem.isDeleted != '1' }"
				title="#{msg['commons.delete']}"
				url="../resources/images/delete_icon.png"
				onclick="if (!confirm('#{msg['commons.confirmDelete']}')) return false; else onDeleted(this);">
			</h:graphicImage>
			<a4j:commandLink id="onReqDeleted"
				reRender="hiddenFields,progRecommendationsContentDiv,progRecommendationssDataTable,successmsg,errormsg"
				onbeforedomupdate="javaScript:onRemoveDisablingDiv(); "
				action="#{pages$programRecommendations.onDelete}" />
		</t:column>
	</t:dataTable>
</t:div>

