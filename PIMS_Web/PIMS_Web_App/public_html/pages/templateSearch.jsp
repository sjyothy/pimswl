<%-- 
  - Author: imran.mirza
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Search screen template
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>


<script language="JavaScript" type="text/javascript">
	    function resetValues()
      	{
      		document.getElementById("searchFrm:myName").value="";			
      	    document.getElementById("searchFrm:myAge").selectedIndex=0;
        }
        
        </script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>#{msg['application.title']}"</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<%
					response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
						response.setHeader("Pragma", "no-cache"); //HTTP 1.0
						response.setDateHeader("Expires", 0); //prevents caching at the proxy server
				%>
				<!-- Header -->
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr width="100%">
						<td width="83%" height="470px" valign="top"
							class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="SearchHeading"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" height="100%">										
										<h:form id="searchFrm"
											style="WIDTH: 98%;height: 428px; #WIDTH: 96%;">
											<t:div styleClass="MESSAGE">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText value="infoMessages"
																escape="false" styleClass="INFO_FONT" />
															<h:outputText
																value="errorMessages"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</t:div>
											<div class="MARGIN">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION">
													<h:outputLabel value="#{msg['commons.searchCriteria']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">
																	<tr>

																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['template.myName']}:"></h:outputLabel>
																		</td>

																		<td width="25%">
																			<h:inputText id="myName"
																				value="#{pages$templateSearch.templateView.myName}"
																				maxlength="20"></h:inputText>

																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['template.myAge']}:"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:selectOneMenu id="myAGe" style="width: 40px" required="false"
																				value="#{pages$templateSearch.templateView.myAge}"
																				maxlength="100">
																					<f:selectItem itemValue="" itemLabel="12" />
																					<f:selectItems itemValue="" itemLabel="15"/>
																			</h:selectOneMenu>
																		</td>
																	  </tr>
																	
																	<tr>																		
																		<table cellpadding="1px" cellspacing="1px">
																			<tr>
																			<td colspan="2">
																					<pims:security
																						screen="Pims.AuctionManagement.Auction.SearchAuctions"
																						action="create">
																						<h:commandButton styleClass="BUTTON"
																							value="#{msg['commons.search']}"
																							action="#{pages$templateSearch.doSearch}"
																							style="width: 75px" tabindex="7">
																						</h:commandButton>
																					</pims:security>
																					<h:commandButton styleClass="BUTTON"
																						value="#{msg['commons.clear']}"
																						onclick="javascript:resetValues();"
																						style="width: 75px" tabindex="7"></h:commandButton>
																				</td>
																			</tr>
																		</table>
																		</td>
																	</tr>
																</table>
															</td>
															<td width="3%">
																&nbsp;
															</td>
														</tr>

													</table>
												</div>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">
												<div class="imag">
													&nbsp;
												</div>
												<div class="contentDiv" style="width: 100%; # width: 99%;">
													<t:dataTable id="dt1"
														value="#{pages$templateSearch.dataList}"
														binding="#{pages$templateSearch.dataTable}"
														rows="#{pages$templateSearch.paginatorRows}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" width="100%">

														<t:column id="auctionNumberCol" sortable="true">
															<f:facet name="header">
																<t:commandSortHeader columnName="myNameCol"
																	actionListener="#{pages$templateSearch.sort}"
																	value="#{msg['template.myName']}" arrow="true">
																	<f:attribute name="sortField" value="myName" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.myName}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														<t:commandSortHeader columnName="myNameCol"
																	actionListener="#{pages$templateSearch.sort}"
																	value="#{msg['template.myName']}" arrow="true">
																	<f:attribute name="sortField" value="myName" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.myName}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>
														
														<t:commandSortHeader columnName="myNameCol"
																	actionListener="#{pages$templateSearch.sort}"
																	value="#{msg['template.myName']}" arrow="true">
																	<f:attribute name="sortField" value="myName" />
																</t:commandSortHeader>
															</f:facet>
															<t:outputText value="#{dataItem.myName}"
																style="white-space: normal;" styleClass="A_LEFT" />
														</t:column>	
																												
													</t:dataTable>
												</div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:100%;#width:100%;">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="RECORD_NUM_TD">
																<div class="RECORD_NUM_BG">
																	<table cellpadding="0" cellspacing="0"
																		style="width: 182px; # width: 150px;">
																		<tr>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value="#{msg['commons.recordsFound']}" />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText value=" : " />
																			</td>
																			<td class="RECORD_NUM_TD">
																				<h:outputText
																					value="#{pages$auctionSearch.recordSize}" />
																			</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																align="right">

																<t:div styleClass="PAGE_NUM_BG" style="#width:20%">
																	<%--			<h:outputFormat styleClass="PAGE_NUM" value="#{msg['commons.pageNum']}">
																<f:param value="#{requestScope.pageNumber}"/>
															</h:outputFormat>
												--%>
																	<table cellpadding="0" cellspacing="0">
																		<tr>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					value="#{msg['commons.page']}" />
																			</td>
																			<td>
																				<h:outputText styleClass="PAGE_NUM"
																					style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																					value="#{pages$templateSearch.currentPage}" />
																			</td>
																		</tr>
																	</table>
																</t:div>
																<TABLE border="0" class="SCH_SCROLLER">
																	<tr>
																		<td>
																			<t:commandLink
																				action="#{pages$templateSearch.pageFirst}"
																				disabled="#{pages$templateSearch.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_first}"
																					id="lblF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$templateSearch.pagePrevious}"
																				disabled="#{pages$templateSearch.firstRow == 0}">
																				<t:graphicImage url="../#{path.scroller_fastRewind}"
																					id="lblFR"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:dataList value="#{pages$templateSearch.pages}"
																				var="page">
																				<h:commandLink value="#{page}"
																					actionListener="#{pages$templateSearch.page}"
																					rendered="#{page != pages$templateSearch.currentPage}" />
																				<h:outputText value="<b>#{page}</b>" escape="false"
																					rendered="#{page == pages$templateSearch.currentPage}" />
																			</t:dataList>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$templateSearch.pageNext}"
																				disabled="#{pages$templateSearch.firstRow + pages$auctionSearch.rowsPerPage >= pages$auctionSearch.totalRows}">
																				<t:graphicImage
																					url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																			</t:commandLink>
																		</td>
																		<td>
																			<t:commandLink
																				action="#{pages$templateSearch.pageLast}"
																				disabled="#{pages$templateSearch.firstRow + pages$templateSearch.rowsPerPage >= pages$templateSearch.totalRows}">
																				<t:graphicImage url="../#{path.scroller_last}"
																					id="lblL"></t:graphicImage>
																			</t:commandLink>
																		</td>
																	</tr>
																</TABLE>
															</td>
														</tr>
													</table>
												</t:div>
											</div>

										</h:form>

										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<c:choose>
								<c:when test="${!pages$templateSearch.isViewModePopUp}">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td class="footer">
												<h:outputLabel value="#{msg['commons.footer.message']}" />
											</td>
										</tr>
									</table>
								</c:when>
							</c:choose>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>