<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
	<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>Test JSP Title</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
			<script type="text/javascript" src="../../<h:outputFormat value="#{path.js_prototype}"/>"></script>
		</head>
		<BODY>
			<h:form id="tableForm">


				<div class="contentDiv">
					<t:dataTable id="testTable"
						value="#{testController.tablelist}"
						rows="3"
						preserveDataModel="true" preserveSort="false" var="listitem"
						rowClasses="row1,row2" rules="all" renderedIfEmpty="true"
						width="100%">
						<h:column>
							<f:facet name="header"><f:verbatim>id</f:verbatim></f:facet>
							<h:outputText value="#{listitem.id}"/>
						</h:column>
						<h:column>
							<f:facet name="header"><f:verbatim>name</f:verbatim></f:facet>
							<h:outputText value="#{listitem.name}"/>
						</h:column>
					</t:dataTable>
					</div>
					<rich:datascroller id="scroller" for="testTable" maxPages="5" 
						pageIndexVar="scrollerPageIndex" pagesVar="scrollerPageCount"/>
			</h:form>
		</BODY>
	</html>
</f:view>