
<t:div id="tabInterventionPlan" style="width:100%;">

	<t:panelGrid id="tabInterventionPlanGrid" cellpadding="3px"
		width="100%" cellspacing="2px" columns="1"
		styleClass="TAB_DETAIL_SECTION_INNER"
		columnClasses="APP_DETAIL_GRID_C1, APP_DETAIL_GRID_C2,APP_DETAIL_GRID_C3,APP_DETAIL_GRID_C4">
		<h:outputLabel
			value="#{msg['researchFamilyVillageBeneficiary.lbl.tabInterventionPlan']}"
			style="font-family: Trebuchet MS;font-weight: bold;font-size: 14px" />

	</t:panelGrid>
	<t:div styleClass="BUTTON_TD"
		style="padding-top:5px; padding-right:21px;">

		<h:commandButton id="btnAddInterventionPlan" styleClass="BUTTON"
			type="button" value="#{msg['commons.Add']}"
			onclick="javaScript:openFamilyVillageInterventionPlanPopup('#{pages$tabFamilyVillageInterventionPlan.personId}');">


		</h:commandButton>

	</t:div>
	<t:div styleClass="contentDiv" style="width:98%;margin-top: 5px;">
		<t:dataTable id="dataTableInterventionPlan" rows="5" width="100%"
			value="#{pages$tabFamilyVillageInterventionPlan.dataListInterventionPlan}"
			binding="#{pages$tabFamilyVillageInterventionPlan.dataTableInterventionPlan}"
			preserveDataModel="false" preserveSort="false" var="dataItem"
			rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

			<t:column id="InterventionPlanAspectsCreatedBy" sortable="true">
				<f:facet name="header">
					<t:outputText id="InterventionPlanAspectsCreatedByOT"
						value="#{msg['commons.createdBy']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageInterventionPlan.englishLocale? 
																				dataItem.createdByEn:
																				dataItem.createdByAr}"
					style="white-space: normal;" styleClass="A_LEFT" />

			</t:column>
			<t:column id="InterventionPlanAspectsCreatedOn" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.createdOn']}" />
				</f:facet>
				<h:outputText value="#{dataItem.createdOn}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageInterventionPlan.dateFormat}"
						timeZone="#{pages$tabFamilyVillageInterventionPlan.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="InterventionPlanAspectsCaring" sortable="true">
				<f:facet name="header">
					<t:outputText id="InterventionPlanAspectsCaringOT"
						value="#{msg['researchFamilyVillageBeneficiary.lbl.InterventionPlan.CaringType']}" />
				</f:facet>
				<h:outputText
					value="#{pages$tabFamilyVillageInterventionPlan.englishLocale? 
																				dataItem.caringTypeEn:
																				dataItem.caringTypeAr}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>
			<t:column id="interventionAspectFromDate" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.fromDate']}" />
				</f:facet>
				<h:outputText value="#{dataItem.fromDate}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageInterventionPlan.dateFormat}"
						timeZone="#{pages$tabFamilyVillageInterventionPlan.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="interventtionAspectsToDate" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.toDate']}" />
				</f:facet>
				<h:outputText value="#{dataItem.toDate}">
					<f:convertDateTime
						pattern="#{pages$tabFamilyVillageInterventionPlan.dateFormat}"
						timeZone="#{pages$tabFamilyVillageInterventionPlan.timeZone}" />
				</h:outputText>
			</t:column>
			<t:column id="InterventionPlanAspectsTarget" sortable="true">
				<f:facet name="header">
					<h:outputText
						value="#{msg['researchFamilyVillageBeneficiary.lbl.InterventionPlan.Target']}" />
				</f:facet>
				<h:outputText value="#{dataItem.target}">

				</h:outputText>
			</t:column>

			<t:column id="InterventionPlanAspectsDetails" sortable="true">
				<f:facet name="header">
					<h:outputText value="#{msg['commons.description']}" />
				</f:facet>
				<h:outputText value="#{dataItem.description}">

				</h:outputText>
			</t:column>


		</t:dataTable>
	</t:div>
	<t:div id="InterventionPlanAspectsDivScroller"
		styleClass="contentDivFooter" style="width:99.1%">


		<t:panelGrid id="InterventionPlanAspectsRecNumTable" columns="2"
			cellpadding="0" cellspacing="0" width="100%"
			columnClasses="RECORD_NUM_TD,BUTTON_TD">
			<t:div id="InterventionPlanAspectsRecNumDiv"
				styleClass="RECORD_NUM_BG">
				<t:panelGrid id="InterventionPlanAspectsRecNumTbl" columns="3"
					columnClasses="RECORD_NUM_TD,RECORD_NUM_TD,RECORD_NUM_TD"
					cellpadding="0" cellspacing="0" style="width:182px;#width:150px;">
					<h:outputText value="#{msg['commons.recordsFound']}" />
					<h:outputText value=" : " />
					<h:outputText
						value="#{pages$tabFamilyVillageInterventionPlan.recordSizeInterventionPlan}" />
				</t:panelGrid>

			</t:div>

			<CENTER>
				<t:dataScroller id="InterventionPlanAspectsScroller"
					for="dataTableInterventionPlan" paginator="true" fastStep="1"
					paginatorMaxPages="#{pages$tabFamilyVillageInterventionPlan.paginatorMaxPages}"
					immediate="false" paginatorTableClass="paginator"
					renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
					styleClass="SCH_SCROLLER"
					paginatorActiveColumnStyle="font-weight:bold;"
					paginatorRenderLinkForActive="false"
					paginatorTableStyle="grid_paginator" layout="singleTable"
					paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; ">

					<f:facet name="first">
						<t:graphicImage url="../#{path.scroller_first}"
							id="InterventionPlanAspectslblFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastrewind">
						<t:graphicImage url="../#{path.scroller_fastRewind}"
							id="InterventionPlanAspectslblFRRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="fastforward">
						<t:graphicImage url="../#{path.scroller_fastForward}"
							id="InterventionPlanAspectslblFFRequestHistory"></t:graphicImage>
					</f:facet>

					<f:facet name="last">
						<t:graphicImage url="../#{path.scroller_last}"
							id="InterventionPlanAspectslblLRequestHistory"></t:graphicImage>
					</f:facet>

					<t:div id="InterventionPlanAspectsPageNumDiv"
						styleClass="PAGE_NUM_BG">
						<t:panelGrid id="InterventionPlanAspectsPageNumTable"
							styleClass="PAGE_NUM_BG_TABLE" columns="2" cellpadding="0"
							cellspacing="0">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.pageNumber}" />
						</t:panelGrid>

					</t:div>
				</t:dataScroller>
			</CENTER>

		</t:panelGrid>
</t:div>
</t:div>

