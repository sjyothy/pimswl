
<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
		
	function calculateAmount(quantityControl)
	{
			removeNonInteger(quantityControl);
			quantityControl.nextsibling;
			var denControl  = document.getElementById(quantityControl.id.replace('quantity','denomination'));
			var id =quantityControl.id.replace('quantity','amountId');
			var amountControl  = document.getElementById(id );
			
			amountControl.innerText =denControl.outerText * quantityControl.value;
			countRows();
           	
	}
	function countRows()
	{
			var total = document.getElementById('total');
			var totalAmount= 0;
	       	var tbl =document.getElementById('frm:test2');
            var trs = tbl.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
            for (var i = 0; i < trs.length; i++) 
            {
               totalAmount+= Number(document.getElementById('amountId['+i+']').innerText);
           	}
           	total.innerText = totalAmount;
        }
	            
	function onChkChange()
    {
      document.getElementById('frm:disablingDiv').style.display='none';
    }
    function onSelectionChanged(changedChkBox)
	{
	    document.getElementById('frm:disablingDiv').style.display='block';
		changedChkBox.nextSibling.onclick();
	}
	function closeWindowSubmit()
	{
	    
		window.opener.onMessageFromAmountDetailsPopUp();
	  	window.close();	  
	}	
	function closeWindow()
	{
		  window.close();
	}

</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['openBox.heading.amountDetails']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="frm" enctype="multipart/form-data">
										<div>
											<table border="0" class="layoutTable">
												<tr>
													<td>
														<h:messages></h:messages>
														<h:outputText
															value="#{pages$addOpenBoxAmountDetails.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />
														<h:outputText
															value="#{pages$addOpenBoxAmountDetails.successMessages}"
															escape="false" styleClass="INFO_FONT" />


													</td>
												</tr>
											</table>
										</div>
										<div class="SCROLLABLE_SECTION">

											<div class="MARGIN" style="width: 95%;">

												<t:div styleClass="DETAIL_SECTION" style="width: 99.8%;"
													rendered="#{! ( pages$addOpenBoxAmountDetails.pageModeAdd || 
													                pages$addOpenBoxAmountDetails.pageModeView
													              )
													           }">
													<h:outputLabel
														value="#{msg['openBox.heading.amountDetails']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<t:panelGrid cellpadding="1px" width="100%"
														styleClass="DETAIL_SECTION_INNER" id="basicInfoFields"
														cellspacing="5px" columns="4">

														<h:outputLabel id="lblTrxNo"
															value="#{msg['commons.refNum']}:" />
														<h:inputText id="trxNumber" styleClass="READONLY"
															readonly="true"
															value="#{pages$addOpenBoxAmountDetails.openBox.refNum}" />

														<h:outputLabel id="lblTrxMethod"
															value="#{msg['commons.status']}:" />
														<h:inputText id="trxMethod" styleClass="READONLY"
															readonly="true"
															value="#{pages$addOpenBoxAmountDetails.englishLocale ?
																												  pages$addOpenBoxAmountDetails.openBox.statusEn:pages$addOpenBoxAmountDetails.openBox.statusAr}" />

														<h:outputLabel id="lblcurrentAmount"
															value="#{msg['openBox.lbl.currentAmount']}:" />
														<h:inputText id="txtcurrentAmount" styleClass="READONLY"
															readonly="true"
															value="#{pages$addOpenBoxAmountDetails.amountDetails.currentAmount}" />

														<h:outputLabel id="lblnewAmount"
															value="#{msg['openBox.lbl.newAmount']}:" />
														<h:inputText id="txtnewAmount" styleClass="READONLY"
															readonly="true"
															value="#{pages$addOpenBoxAmountDetails.amountDetails.newAmount}" />

														<h:outputLabel id="changeReason"
															value="#{msg['commons.reasons']}:" />
														<t:panelGroup colspan="3" style="width: 100%">
															<t:inputTextarea style="width: 90%;" id="txt_desc"
																value="#{pages$addOpenBoxAmountDetails.amountDetails.changeReason}"
																onblur="javaScript:validate();"
																onkeyup="javaScript:validate();"
																onmouseup="javaScript:validate();"
																onmousedown="javaScript:validate();"
																onchange="javaScript:validate();"
																onkeypress="javaScript:validate();" />
														</t:panelGroup>
													</t:panelGrid>
												</t:div>

												<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
												<t:div styleClass="BUTTON_TD">
													<h:commandButton id="btnDone" styleClass="BUTTON"
														value="#{msg['commons.done']}"
														rendered="#{! pages$addOpenBoxAmountDetails.pageModeView}"
														action="#{pages$addOpenBoxAmountDetails.onDone}"
														style="width: auto" />

													<h:commandButton styleClass="BUTTON"
														value="#{msg['generateFloors.Close']}"
														onclick="javascript:window.close();" style="width: auto">
													</h:commandButton>
												</t:div>

												<t:div styleClass="contentDiv" id="parentDivs"
													style="width:95%">
													<t:dataTable id="test2" width="100%" styleClass="grid"
														value="#{pages$addOpenBoxAmountDetails.list}" rows="200"
														preserveDataModel="true" preserveSort="false"
														var="dataItem" rowClasses="row1,row2"
														rowId="#{dataItem.denomination}">

														<t:column id="denomination" width="10%" sortable="false"
															style="white-space: normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['openBox.dirhams']}" />
															</f:facet>

															<t:outputText value="#{dataItem.denominationStr}"
																forceId="true" id="denomination" />
														</t:column>


														<t:column id="quantity" width="10%" sortable="false"
															style="white-space: normal;">
															<f:facet name="header">
																<t:outputText value="#{msg['openBox.quantity']}" />
															</f:facet>
															<t:inputText style="width:100%;border:0;" forceId="true"
																id="quantity"
																rendered="#{!pages$addOpenBoxAmountDetails.pageModeView}"
																onkeyup="calculateAmount(this);"
																value="#{dataItem.quantityStr}" />
															<t:outputText
																rendered="#{pages$addOpenBoxAmountDetails.pageModeView}"
																value="#{dataItem.quantityStr}" />

														</t:column>


														<t:column id="amount" width="10%" sortable="false"
															style="white-space: normal;">

															<f:facet name="header">
																<t:outputText value="#{msg['commons.amount']}" />
															</f:facet>
															<t:outputText value="#{dataItem.amountStr}"
																forceId="true" id="amountId" />
														</t:column>

													</t:dataTable>


												</t:div>
												<t:div styleClass="BUTTON_TD" style="width:82%;font-weight:bold;">
													<t:outputText value="0.0" forceId="true" id="total" />
												</t:div>



											</div>
										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<script language="javascript" type="text/javascript">
	
		     	countRows();
	</script>
		</body>
	</html>
</f:view>