<%-- 
  - Author: Syed Hammad Ahmed
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching Assets
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">
	function closeWindowSubmit()
	{
		window.opener.document.forms[0].submit();
	  	window.close();	  
	}	
	function closeWindow()
	{
		 window.close();
	}        
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<c:choose>
				<c:when test="${!pages$addBeneficiaryPopup.viewMode}">
					<div class="containerDiv">
				</c:when>
			</c:choose>
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">


				<c:choose>
					<c:when test="${!pages$addBeneficiaryPopup.viewMode}">
						<tr
							style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</tr>
					</c:when>
				</c:choose>


				<tr width="100%">

					<c:choose>
						<c:when test="${!pages$addBeneficiaryPopup.viewMode}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>

					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['paymentDetailsPopUp.header']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="searchFrm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText id="errorMessages"
																value="#{pages$addBeneficiaryPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$addBeneficiaryPopup.successMessages}"
																escape="false" styleClass="INFO_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_left}"/>"
																class="TAB_PANEL_LEFT" />
														</td>
														<td width="100%" style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_mid}"/>"
																class="TAB_PANEL_MID" />
														</td>
														<td style="FONT-SIZE: 0px">
															<IMG
																src="../<h:outputText value="#{path.img_section_right}"/>"
																class="TAB_PANEL_RIGHT" />
														</td>
													</tr>
												</table>
												<div class="DETAIL_SECTION" style="width: 100%;">
													<h:outputLabel
														value="#{msg['mems.normaldisb.label.beneficiarypopup.heading']}"
														styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td width="97%">
																<table width="100%">
																	<tr>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['mems.normaldisb.label.paymentType']}" />
																		</td>
																		<td width="25%">
																			<h:inputText
																				value="#{pages$addBeneficiaryPopup.paymentType}"
																				readonly="true" styleClass="READONLY">

																			</h:inputText>
																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['mems.normaldisb.label.totalAmount']}:" />
																		</td>
																		<td width="25%">
																			<h:inputText
																				value="#{pages$addBeneficiaryPopup.totalAmount}"
																				readonly="true" styleClass="READONLY">
																			</h:inputText>
																		</td>
																	</tr>
																	<tr>
																		<td width="25%">

																			<h:outputLabel id="labelNoofBenef" styleClass="LABEL"
																				value="#{msg['mems.normaldisb.label.noOfBenef']}:" />
																		</td>
																		<td width="25%">
																			<h:inputText id="noOfBenef" readonly="true"
																				styleClass="READONLY"
																				value="#{pages$addBeneficiaryPopup.noofBeneficiaries}" />
																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['mems.normaldisb.label.beneficiary']}:" />
																		</td>
																		<td width="25%">
																			<h:selectOneMenu id="benefList"
																				binding="#{pages$addBeneficiaryPopup.cmbBeneficiaryList}"
																				disabled="#{! pages$addBeneficiaryPopup.isEditable}"
																				style="width: 200px;">
																				<f:selectItem
																					itemLabel="#{msg['commons.pleaseSelect']}"
																					itemValue="-1" />
																				<f:selectItems
																					value="#{pages$addBeneficiaryPopup.beneficiaries}" />
																				<a4j:support event="onchange"
																					reRender="accountBalance,blockinBalance,availableBalance,requestedAmount,shariaShares,errorMessages"
																					action="#{pages$addBeneficiaryPopup.handleBeneficiaryChange}" />
																			</h:selectOneMenu>
																		</td>

																	</tr>
																	<tr>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['commons.amount']}" />
																		</td>
																		<td width="25%">
																			<h:inputText maxlength="20"
																				readonly="#{!pages$addBeneficiaryPopup.isEditable}"
																				binding="#{pages$addBeneficiaryPopup.txtAmount}" />
																		</td>
																		<td>
																			<h:outputLabel id="labelBalance" styleClass="LABEL"
																				value="#{msg['mems.normaldisb.label.balance']}:">
																			</h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="accountBalance" readonly="true"
																				styleClass="READONLY"
																				binding="#{pages$addBeneficiaryPopup.accountBalance}">
																			</h:inputText>
																		</td>

																	</tr>
																	<tr>
																		<td width="25%">
																			<h:outputLabel id="lblBlockBalance"
																				styleClass="LABEL"
																				value="#{msg['blocking.lbl.blockedamount']}"></h:outputLabel>
																		</td>
																		<td width="25%">
																			<h:inputText id="blockinBalance"
																				styleClass="READONLY" readonly="true"
																				binding="#{pages$addBeneficiaryPopup.blockingBalance}"
																				maxlength="20" />
																			<h:commandLink id="lnkBlockingDetails"
																				action="#{pages$addBeneficiaryPopup.onOpenBlockingDetailsPopup}">
																				<h:graphicImage id="openBlockingDetailsPopup"
																					style="margin-right:5px;"
																					title="#{msg['blocking.lbl.blockingDetails']}"
																					url="../resources/images/app_icons/manage_tender.png" />
																			</h:commandLink>
																		</td>
																		<td>
																			<h:outputLabel id="labelrequestedAmount"
																				styleClass="LABEL"
																				value="#{msg['commons.requestedAmount']}:">
																			</h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="requestedAmount" readonly="true"
																				styleClass="READONLY"
																				binding="#{pages$addBeneficiaryPopup.requestedAmount}">
																			</h:inputText>
																			<h:commandLink id="lnkDisbDetails"
																				action="#{pages$addBeneficiaryPopup.onOpenBeneficiaryDisbursementDetailsPopup}">
																				<h:graphicImage
																					id="onOpenBeneficiaryDisbursementDetailsPopup"
																					style="margin-right:5px;"
																					title="#{msg['commons.disbursementDetails']}"
																					url="../resources/images/app_icons/manage_tender.png" />
																			</h:commandLink>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel id="labelAVBalance" styleClass="LABEL"
																				value="#{msg['commons.availableBalance']}:">

																			</h:outputLabel>
																		</td>
																		<td>
																			<h:inputText id="availableBalance" readonly="true"
																				styleClass="READONLY"
																				binding="#{pages$addBeneficiaryPopup.availableBalance}">
																			</h:inputText>
																		</td>

																		<td colspan="4" class="BUTTON_TD">
																			<h:commandButton type="submit" styleClass="BUTTON"
																				rendered="#{ pages$addBeneficiaryPopup.isEditable}"
																				action="#{pages$addBeneficiaryPopup.saveBeneficiary}"
																				value="#{msg['commons.Add']}" />


																			<h:commandButton type="submit" styleClass="BUTTON"
																				rendered="#{ pages$addBeneficiaryPopup.isEditable}"
																				action="#{pages$addBeneficiaryPopup.onDistribute}"
																				value="#{msg['normalDisbursement.distribute']}" />
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</div>
												<t:div styleClass="contentDiv"
													style="width:98%;margin-top: 5px;">
													<t:dataTable id="beneficiaryId" rows="15" width="100%"
														value="#{pages$addBeneficiaryPopup.beneficiaryList}"
														binding="#{pages$addBeneficiaryPopup.beneficiariesTable}"
														preserveDataModel="false" preserveSort="false"
														var="unitDataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true">
														<t:column id="beneficiary" sortable="true">
															<f:facet name="header">
																<t:outputText id="hdBenef"
																	value="#{msg['mems.investment.label.beneficiaries']}" />
															</f:facet>

															<t:commandLink id="s_b"
																onclick="openBeneficiaryPopup(#{unitDataItem.personId})"
																value="#{unitDataItem.name}"
																style="white-space: normal;" styleClass="A_LEFT" />

														</t:column>
														<t:column id="amnt" sortable="true">
															<f:facet name="header">
																<t:outputText id="hdAmnt"
																	value="#{msg['commons.amount']}" />
															</f:facet>
															<t:outputText id="amountt" styleClass="A_LEFT"
																value="#{unitDataItem.amount}" />
														</t:column>

														<t:column id="srcType" sortable="true">
															<f:facet name="header">
																<t:outputText id="hdTo"
																	value="#{msg['mems.payment.request.lable.Desc']}" />
															</f:facet>
															<t:outputText id="p_w" styleClass="A_LEFT"
																value="#{pages$addBeneficiaryPopup.isEnglishLocale?pages$addBeneficiaryPopup.disbursementDetails.srcTypeEn:pages$addBeneficiaryPopup.disbursementDetails.srcTypeAr}" />
														</t:column>

														<t:column id="actionId" sortable="true"
															rendered="#{pages$addBeneficiaryPopup.isEditable}">
															<f:facet name="header">
																<t:outputText id="hdAmoun"
																	value="#{msg['commons.action']}" />
															</f:facet>
															<h:commandLink
																action="#{pages$addBeneficiaryPopup.deleteBeneficiary}">
																<h:graphicImage id="deleteIcon"
																	title="#{msg['commons.delete']}"
																	url="../resources/images/delete.gif" width="18px;" />
															</h:commandLink>
														</t:column>
													</t:dataTable>
												</t:div>
												<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
													style="width:99.1%;">
													<t:panelGrid columns="2" border="0" width="100%"
														cellpadding="1" cellspacing="1">
														<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
															<t:div styleClass="JUG_NUM_REC_ATT">
																<h:outputText value="#{msg['commons.records']}" />
																<h:outputText value=" : " />
																<h:outputText
																	value="#{pages$addBeneficiaryPopup.totalRows}" />
															</t:div>
														</t:panelGroup>
														<t:panelGroup>
															<t:dataScroller id="shareScrollers" for="beneficiaryId"
																paginator="true" fastStep="1" immediate="false"
																paginatorTableClass="paginator"
																renderFacetsIfSinglePage="true"
																paginatorTableStyle="grid_paginator"
																layout="singleTable"
																paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																paginatorActiveColumnStyle="font-weight:bold;"
																paginatorRenderLinkForActive="false"
																pageIndexVar="pageNumber" styleClass="JUG_SCROLLER">
																<f:facet name="first">
																	<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
																</f:facet>
																<f:facet name="fastrewind">
																	<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
																</f:facet>
																<f:facet name="fastforward">
																	<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
																</f:facet>
																<f:facet name="last">
																	<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
																</f:facet>
																<t:div styleClass="PAGE_NUM_BG" rendered="true">
																	<h:outputText styleClass="PAGE_NUM"
																		value="#{msg['commons.page']}" />
																	<h:outputText styleClass="PAGE_NUM"
																		style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																		value="#{requestScope.pageNumber}" />
																</t:div>
															</t:dataScroller>
														</t:panelGroup>
													</t:panelGrid>
												</t:div>
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td height="5px"></td>
													</tr>
													<tr>
														<td class="BUTTON_TD MARGIN" colspan="6">
															<h:commandButton id="idSaveBtn"
																rendered="#{pages$addBeneficiaryPopup.isEditable}"
																value="#{msg['commons.saveButton']}" styleClass="BUTTON"
																action="#{pages$addBeneficiaryPopup.done}">
															</h:commandButton>
														</td>
													</tr>
												</table>
											</div>
										</div>
									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr
					style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
					<td colspan="2">
						<c:choose>
							<c:when test="${!pages$addBeneficiaryPopup.viewMode}">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="footer">
											<h:outputLabel value="#{msg['commons.footer.message']}" />
										</td>
									</tr>
								</table>
							</c:when>
						</c:choose>
					</td>
				</tr>
			</table>
			<c:choose>
				<c:when test="${!pages$addBeneficiaryPopup.viewMode}">
					</div>
				</c:when>
			</c:choose>
		</body>
	</html>
</f:view>