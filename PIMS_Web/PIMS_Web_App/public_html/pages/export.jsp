<%@page import="com.avanza.pims.exporter.ExporterFactory"%>
<%@page import="com.avanza.pims.exporter.IExporter"%>
<%@page import="com.avanza.pims.exporter.ExporterMeta"%>
<%@page import="com.avanza.core.util.Logger"%>
<%@page import="javax.faces.context.FacesContext"%>

<%
	Logger logger = Logger.getLogger(this.getClass());
	
	try
	{
		logger.logInfo(" --- STARTED --- ");
		
		if ( session.getAttribute("exporterMeta") != null )
		{
			ExporterMeta exporterMeta = (ExporterMeta) session.getAttribute("exporterMeta");
			
			exporterMeta.setFacesContext( FacesContext.getCurrentInstance() );
			
			IExporter exporter = ExporterFactory.getExporter( exporterMeta );
			
			if ( exporter != null )
			{
				exporter.export();
			}
		}
		
		logger.logInfo(" --- COMPLETED --- ");
	}
	catch ( Exception e )
	{
		logger.LogException(" --- EXCEPTION --- ", e);
	}
%>