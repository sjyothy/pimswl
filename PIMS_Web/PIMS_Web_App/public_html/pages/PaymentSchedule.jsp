<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



    
<f:view>
        <f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
       <f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
 
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden;">
	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>						
            <link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
			


			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			
 	</head>
 	        
		<script language="javascript" type="text/javascript">
	function clearWindow()
	{
            document.getElementById("formPaymentSchedule:dateTimePaymentDueOn").value="";
			document.getElementById("formPaymentSchedule:txtPaymentDescription").value="";
			document.getElementById("formPaymentSchedule:txtAmount").value="";
			var cmbPaymentMode=document.getElementById("formPaymentSchedule:selectPaymentMode");
			cmbPaymentMode.selectedIndex = 0;
			var  cmbPaymentType=document.getElementById("formPaymentSchedule:selectpaymentType");
			cmbPaymentType.selectedIndex = 0;
			$('formPaymentSchedule:dateTimePaymentDueOn').component.resetSelectedDate();
	        
	}
 	function addToContract()
    {
        window.opener.document.forms[0].submit();
        window.close();
    }
    function onCheckChanged(chkboxNum)
    {
      
      if(chkboxNum==1)
      {
          
            document.getElementById("formPaymentSchedule:chkCompletion").checked=false;
			document.getElementById("formPaymentSchedule:chkMileStone").checked=false;
			document.getElementById("formPaymentSchedule:txtPercentage").value="";
			document.getElementById("formPaymentSchedule:selectMileStones").selectedIndex = 0;
			
			
      }
      else if(chkboxNum==2)
      {
            document.getElementById("formPaymentSchedule:chkSpecificDate").checked=false;
			document.getElementById("formPaymentSchedule:chkMileStone").checked=false;
			document.getElementById("formPaymentSchedule:selectMileStones").selectedIndex = 0;
			$('formPaymentSchedule:seconddateTimePaymentDueOn').component.resetSelectedDate();
			
			    
      }
      else if(chkboxNum==3)
      {
            document.getElementById("formPaymentSchedule:chkSpecificDate").checked=false;
            document.getElementById("formPaymentSchedule:chkCompletion").checked=false;
            document.getElementById("formPaymentSchedule:txtPercentage").value="";
            $('formPaymentSchedule:seconddateTimePaymentDueOn').component.resetSelectedDate();
			
      }
    }
	
    </script>
 	

				<!-- Header -->
	
		<body class="BODY_STYLE">
	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr><td class="HEADER_TD">
										<h:outputLabel value="#{msg['contract.paymentSchedule']}" styleClass="HEADER_FONT"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
					        <table width="99%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
                                 <tr valign="top">
                                  <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
                                   </td>    
									<td width="100%" height="99%" valign="top" nowrap="nowrap">
									<div  class="SCROLLABLE_SECTION"  >
									  <h:form id="formPaymentSchedule" style="width:92%"  enctype="multipart/form-data">
											<table border="0" class="layoutTable" >
											
												<tr>
												
													<td colspan="3">
												
														<h:outputText  styleClass="ERROR_FONT" escape="false" value="#{pages$PaymentSchedule.errorMessages}"/>
												        <h:inputHidden value="#{pages$PaymentSchedule.totalContractValue}"></h:inputHidden>
												        <h:inputHidden id="hdnDateTimeDueOn" value="#{pages$PaymentSchedule.paymentDueOn}" />
												        <h:inputHidden id="hdntxtPercentage" value="#{pages$PaymentSchedule.txtCompletionPercentage}" />
												        <h:inputHidden id="hdnSelectedMileStone" value="#{pages$PaymentSchedule.selectedMileStone}" />
													</td>
												</tr>
											</table>
										
										<div class="MARGIN" style="width:100%" >
											<table cellpadding="0" cellspacing="0" width="99.3%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
											</table>
										<div class="DETAIL_SECTION" style="width:99%" >
										   <h:outputLabel value="#{msg['paymentSchedule.paymentDetails']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
											<t:panelGrid id="psTable" cellpadding="1px" width="100%" cellspacing="2px" styleClass="DETAIL_SECTION_INNER"  columns="4"
	                                                     columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
                                                    
                                                    <h:outputLabel  value="#{msg['contract.TotalContractValue']}:" rendered="#{! empty pages$PaymentSchedule.totalContractValue && pages$PaymentSchedule.totalContractValue>0}"/>
													<h:inputText style="width:85%" styleClass="A_RIGHT_NUM READONLY" readonly="true" 
													             maxlength="15" id="txtTotalContractValue"
													             rendered="#{! empty pages$PaymentSchedule.totalContractValue && pages$PaymentSchedule.totalContractValue>0}" 
																 value="#{pages$PaymentSchedule.totalContractValue}">
													<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
													</h:inputText>

													
													
													
													<h:outputLabel  value="#{msg['paymentSchedule.paymentType']}:"></h:outputLabel>
													<h:selectOneMenu id="selectpaymentType" style="width: 85%" 
												          value="#{pages$PaymentSchedule.selectedPaymentType}">
												          <f:selectItems value="#{pages$PaymentSchedule.paymentTypeList}"/>
			                                         </h:selectOneMenu>
			                                         
			                                         <h:outputLabel  value="#{msg['paymentSchedule.paymentMode']}:"></h:outputLabel>
													 <h:selectOneMenu id="selectpaymentMode"
												          style="width: 85%" value="#{pages$PaymentSchedule.selectedPaymentMode}">
			                                              <f:selectItems value="#{pages$PaymentSchedule.paymentModeList}"/>
			                                         </h:selectOneMenu>
			                                         
			                                         <h:outputLabel  rendered="#{!pages$PaymentSchedule.multiplePaymentDueOn}" value="#{msg['paymentSchedule.paymentDueOn']}:" ></h:outputLabel>
													 <rich:calendar rendered="#{!pages$PaymentSchedule.multiplePaymentDueOn}" id="dateTimePaymentDueOn" value="#{pages$PaymentSchedule.paymentDueOn}"  
														            popup="true" showApplyButton="false"
														            datePattern="#{pages$PaymentSchedule.dateformat}"  
							                                        enableManualInput="false" cellWidth="24px" 
							                                        locale="#{pages$PaymentSchedule.locale}" inputStyle="width: 85%"/>
							                         <t:panelGroup colspan="2" rendered="#{pages$PaymentSchedule.multiplePaymentDueOn}">
			                                         
			                                         </t:panelGroup>
			                                         <t:panelGroup colspan="4" rendered="#{pages$PaymentSchedule.multiplePaymentDueOn}">
			                                        
			                                         <t:panelGrid cellspacing="2px" columns="4" style="width:100%"
			                                         columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2"
			                                         >
							                                         <h:outputLabel   value="#{msg['paymentSchedule.paymentDueOn']}:" ></h:outputLabel>
							                                         <t:panelGrid cellspacing="2px" columns="2" >
								                                         <h:selectBooleanCheckbox id="chkSpecificDate" onclick="javascript:onCheckChanged(1);" value="#{pages$PaymentSchedule.chkSpecificDate}">
								                                         </h:selectBooleanCheckbox >
								                                         <h:outputLabel  id="lblspecifdate" value="#{msg['paymentSchedule.specificDate']}:" ></h:outputLabel>
							                                         </t:panelGrid>
							                                         <h:outputLabel  value="#{msg['paymentSchedule.paymentDueOn']}" >:</h:outputLabel>
																	 <rich:calendar  id="seconddateTimePaymentDueOn" value="#{pages$PaymentSchedule.paymentDueOn}"  
																		            popup="true" showApplyButton="false"
																		            datePattern="#{pages$PaymentSchedule.dateformat}"  
											                                        enableManualInput="false" cellWidth="24px" 
											                                        locale="#{pages$PaymentSchedule.locale}" inputStyle="width: 85%"/>
								                                     <t:panelGroup colspan="1">
							                                         
							                                         </t:panelGroup>
							                                         <t:panelGrid cellspacing="2px" columns="2" >
									                                         <h:selectBooleanCheckbox id="chkCompletion" onclick="javascript:onCheckChanged(2);" value="#{pages$PaymentSchedule.chkPercentage}">
									                                         </h:selectBooleanCheckbox >
									                                         <h:outputLabel   value="#{msg['paymentSchedule.completionPercentage']}:" ></h:outputLabel>
								                                     </t:panelGrid>
							                                         <h:outputLabel  value="#{msg['paymentSchedule.percentage']}:" ></h:outputLabel>
																      <h:inputText id="txtPercentage" value="#{pages$PaymentSchedule.txtCompletionPercentage}" style="width: 85%" styleClass="A_RIGHT_NUM" maxlength="3" ></h:inputText>
			                                         
			                                                           <t:panelGroup colspan="1">
							                                         
							                                         </t:panelGroup>
							                                         <t:panelGrid cellspacing="2px" columns="2" >
									                                         <h:selectBooleanCheckbox id="chkMileStone" onclick="javascript:onCheckChanged(3);" value="#{pages$PaymentSchedule.chkMileStone}">
									                                         </h:selectBooleanCheckbox >
									                                         <h:outputLabel  id="lblMileStone" value="#{msg['paymentSchedule.mileStone']}:" ></h:outputLabel>
								                                     </t:panelGrid>
							                                         <h:outputLabel  value="#{msg['paymentSchedule.mileStones']}:" ></h:outputLabel>
																     <h:selectOneMenu id="selectMileStones" style="width: 85%" value="#{pages$PaymentSchedule.selectedMileStone}">
																          <f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}" itemValue="-1" />
														                  <f:selectItems value="#{pages$PaymentSchedule.mileStoneList}"/>
							                                         </h:selectOneMenu>
			                                         
			                                         </t:panelGrid>
			                                         </t:panelGroup>
			                                                     
			                                          
			                                         <h:outputLabel value="#{msg['paymentSchedule.amount']}:" ></h:outputLabel>
													 <h:inputText style="width: 85%" styleClass="A_RIGHT_NUM" maxlength="15" id="txtAmount" 
																 value="#{pages$PaymentSchedule.amount}">
													 </h:inputText>
			                                         
			                                         <h:outputLabel  value="#{msg['paymentSchedule.description']}:"></h:outputLabel>
													 <h:inputText id = "txtPaymentDescription" style="width: 85%" maxlength="500" value="#{pages$PaymentSchedule.description}"/>
	                                         </t:panelGrid>
                                             
										
											
                                          </div>
                                          <t:div>&nbsp;</t:div>
                                          <t:panelGrid styleClass="BUTTON_TD" cellpadding="1px" width="100%" cellspacing="3px">
                                              <t:panelGroup colspan="10"> 
                                           			<h:commandButton type="submit" styleClass="BUTTON" action="#{pages$PaymentSchedule.btnAdd_Click}" style="width: 150px" value="#{msg['commons.Add']}"/>
										            <h:commandButton styleClass="BUTTON" type="button" value="#{msg['commons.reset']}" style="width: 150px" onclick="javascript:clearWindow();"/>
								              </t:panelGroup> 
                                		  </t:panelGrid>
                                		  <t:div>&nbsp;</t:div>
                                		  <table id="imageTable" cellpadding="0" cellspacing="0" width="100%" >
													<tr>
													<td><IMG id="image1" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_left")%>" class="TAB_PANEL_LEFT"/></td>
													<td width="100%"><IMG id="image2" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_mid")%>" class="TAB_PANEL_MID"/></td>
													<td><IMG id="image3" src="../<%=ResourceUtil.getInstance().getPathProperty("img_section_right")%>" class="TAB_PANEL_RIGHT"/></td>
													</tr>
											</table>
                                            <t:div id="dataListDiv"  styleClass="contentDiv"   style="width:98.7%;margin-right:0px;" >
												
												<t:dataTable id="test3" rows="#{pages$PaymentSchedule.paginatorRows}" width="100%" value="#{pages$PaymentSchedule.gridDataList}" binding="#{pages$PaymentSchedule.tbl_Facility}" 
												 preserveDataModel="true" preserveSort="true" var="dataItem" rowClasses="row1,row2" rules="all" renderedIfEmpty="true">
												<t:column id="colDueOn" width="15%">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentDueOn']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.paymentDueOnString}" >
														
													</t:outputText>
												</t:column>
												<t:column id="colCompletionPerccent" width="15%" rendered="#{pages$PaymentSchedule.multiplePaymentDueOn}">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.percentage']}"  />
													</f:facet>
													<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.completionPercentage}" >
														
													</t:outputText>
												</t:column>
												<t:column id="colMileStone" width="15%" rendered="#{pages$PaymentSchedule.multiplePaymentDueOn}">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.mileStoneNum']}"  />
													</f:facet>
													<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.projectMileStoneNumber}" >
														
													</t:outputText>
												</t:column>
												
												<t:column id="colTypeEn" rendered="#{pages$PaymentSchedule.isEnglishLocale}">


													<f:facet name="header">

														<t:outputText  value="#{msg['paymentSchedule.paymentType']}" />

													</f:facet>
													<t:outputText     styleClass="A_LEFT"  title="" value="#{dataItem.typeEn}" />
												</t:column>
												<t:column id="colTypeAr" rendered="#{pages$PaymentSchedule.isArabicLocale}">


													<f:facet name="header">

														<t:outputText  value="#{msg['paymentSchedule.paymentType']}" />

													</f:facet>
													<t:outputText     styleClass="A_LEFT"  title="" value="#{dataItem.typeAr}" />
												</t:column>
												<t:column id="colDesc" >
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.description']}" />
													</f:facet>
													<t:outputText     styleClass="A_LEFT"  title="" value="#{dataItem.description}" />
												</t:column>
												<t:column id="colModeEn" rendered="#{pages$PaymentSchedule.isEnglishLocale}">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentMode']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.paymentModeEn}" />
												</t:column>
												<t:column id="colModeAr" rendered="#{pages$PaymentSchedule.isArabicLocale}">
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.paymentMode']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" title="" value="#{dataItem.paymentModeAr}" />
												</t:column>
												<t:column id="colAmount" >
													<f:facet name="header">
														<t:outputText  value="#{msg['paymentSchedule.amount']}" >
														
														</t:outputText>
													</f:facet>
													<t:outputText styleClass="A_RIGHT_NUM" title="" value="#{dataItem.amount}" >
													<f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="##,###,###.##"/>
													</t:outputText>
												</t:column>
													 <t:column id="col8" >
													<f:facet name="header">
														<t:outputText  value="#{msg['commons.action']}" />
													</f:facet>
													<h:commandLink action="#{pages$PaymentSchedule.btnDelPaymentSchedule_Click}">&nbsp;
																	       <h:graphicImage id="deletePaySc" 
																		title="#{msg['commons.delete']}" 
																		rendered="#{dataItem.isReceived == 'N'}"
																		url="../resources/images/delete_icon.png"/>														
													</h:commandLink>             
												</t:column>
											  </t:dataTable>										
											</t:div>
											  <t:div id="pagingDivPaySch" styleClass="contentDivFooter" style="width:99.8%;margin-right:0px;" rendered="#{pages$PaymentSchedule.showProspectiveGrid}" >

												<t:dataScroller id="scrollerPaySch" for="test3" paginator="true"
													fastStep="1" paginatorMaxPages="#{pages$PaymentSchedule.paginatorMaxPages}" immediate="false"
													paginatorTableClass="paginator" renderFacetsIfSinglePage="true" pageIndexVar="pageNumber"
													paginatorTableStyle="grid_paginator" layout="singleTable"
													paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
													paginatorActiveColumnStyle="font-weight:bold;"
													paginatorRenderLinkForActive="false"
													 
													styleClass="SCH_SCROLLER"
													>
                                                	<f:facet  name="first">
														
														<t:graphicImage  url="../#{path.scroller_first}" id="lblFPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="fastrewind">
														<t:graphicImage  url="../#{path.scroller_fastRewind}" id="lblFRPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="fastforward">
														<t:graphicImage  url="../#{path.scroller_fastForward}" id="lblFFPaySch"></t:graphicImage>
													</f:facet>

													<f:facet name="last">
														<t:graphicImage   url="../#{path.scroller_last}"  id="lblLPaySch"></t:graphicImage>
													</f:facet>

												</t:dataScroller>
									
                                           </t:div>
                                           <table width="100%" >
											<tr>
												<td>
														&nbsp;&nbsp;
												</td>
												<td colspan="6" class="BUTTON_TD"> 
	                                           		<h:commandButton rendered="#{pages$PaymentSchedule.showProspectiveGrid}" style="width:179px" 
												     styleClass="BUTTON" value="#{msg['paidFacilities.sendToContract']}"  action="#{pages$PaymentSchedule.sendToParent}"/>
												</td>
											</tr>
											</table> 
                                         
                                          </div>
			    </h:form>
			 </div>
									</td>
									</tr>
									</table>
			
			</td>
    </tr>
    
    </table>
			
		</body>
	</html>
</f:view>

