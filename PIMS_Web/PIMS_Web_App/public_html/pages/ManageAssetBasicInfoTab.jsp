<%-- Author: Kamran Ahmed--%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%-- Inherited Assets Tab --%>
<t:panelGrid border="0" width="100%" cellpadding="1" cellspacing="1"
	id="basicAssetsInfoGrid">
	<t:panelGroup>
		<%--Column 1,2 Starts--%>
		<t:panelGrid columns="4" columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%" rendered="#{!(pages$inheritanceFile.viewModePopUp ||pages$inheritanceFile.pageModeView )}">
			
			<h:outputText
					value="#{msg['searchAssets.assetNumber']}" />
			<h:panelGroup>
				<h:inputText
					readonly="true"
					value="#{pages$ManageAssetBasicInfoTab.assetMemsView.assetNumber}" />
				<h:graphicImage title="#{msg['commons.search']}"
					rendered="#{pages$inheritanceFile.showAddAssetButton}"
					style="MARGIN: 1px 1px -5px;cursor:hand;" onclick="searchAssetClicked();"
					url="../resources/images/app_icons/Search-Unit.png">
				</h:graphicImage>
				<h:commandLink id="lnkSeachAsset" action="#{pages$ManageAssetBasicInfoTab.showAssetSearchPopup}"></h:commandLink>
			</h:panelGroup>
			
			<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="*" />
				<h:outputText
				value="#{msg['searchAssets.assetType']}" />
			</h:panelGroup>
			
			<%--value="#{pages$ManageAssetBasicInfoTab.assetMemsView.assetTypeView.assetTypeIdString}"
			This will not work here, it returns the default value i.e= -1 --%>
			<h:selectOneMenu 
				disabled="true"
				value="#{pages$ManageAssetBasicInfoTab.assetMemsView.assetTypeView.assetTypeIdString}"
				styleClass="SELECT_MENU">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$ApplicationBean.assetTypesList}"/>	
				<a4j:support event="onchange"
					reRender="ExtraDetailWithEachAssetType"
					action="#{pages$ManageAssetBasicInfoTab.assetTypeChanged}" />
			</h:selectOneMenu>

			<h:outputLabel value="#{msg['searchAssets.assetNameEn']}"/>
			<h:inputText maxlength="50"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.assetNameEn}"></h:inputText>
			
			<h:outputLabel value="#{msg['searchAssets.assetNameAr']}"/>
			<h:inputText maxlength="50"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.assetNameAr}"></h:inputText>

			
			<h:outputLabel value="#{msg['searchAssets.assetDesc']}"/>
			<h:inputText maxlength="250"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.description}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.genRev']}"/>
			<h:selectBooleanCheckbox id="isRevGenId"
			disabled="true"  
			binding="#{pages$ManageAssetBasicInfoTab.revChk}"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.incomeExpbool}">
			<a4j:support action="#{pages$ManageAssetBasicInfoTab.genRevClicked}" event="onclick" reRender="basicAssetsInfoGrid,cboRevType,txtExpRev,clndrFrom"></a4j:support>
			</h:selectBooleanCheckbox>
			
			<h:outputText id="lblRevType"
				value="#{msg['mems.inheritanceFile.fieldLabel.revType']}" />
			<h:selectOneMenu id="cboRevType"
				disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
				binding="#{pages$ManageAssetBasicInfoTab.cboRevType}"
				value="#{pages$ManageAssetBasicInfoTab.assetMemsView.revenueTypeIdString}"
				styleClass="SELECT_MENU">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1"/>
				<f:selectItems value="#{pages$ApplicationBean.revenueTypeList}"/>	
			</h:selectOneMenu>
			
			<h:outputLabel 
			id="lblExpRev"
			value="#{msg['mems.inheritanceFile.fieldLabel.expRev']}"/>
			<h:inputText 
			readonly="true"
			id="txtExpRev"
			binding="#{pages$ManageAssetBasicInfoTab.txtExpRev}"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.expectedRevenueString}"></h:inputText>
			
			<h:outputLabel 
			id="lblFrom"
			value="#{msg['mems.inheritanceFile.fieldLabel.from']}"/>
			<rich:calendar id="clndrFrom"
			disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
			binding="#{pages$ManageAssetBasicInfoTab.clndrFromDate}"
			locale="#{pages$ManageAssetBasicInfoTab.locale}" popup="true"
			datePattern="#{pages$ManageAssetBasicInfoTab.dateFormat}"
			showApplyButton="false" enableManualInput="false"
			inputStyle="width:185px;height:17px" />
			
			<h:outputLabel
				value="#{msg['inheritanceFile.inheritedAssetTab.label.isManagerAmaf']}" />
			<h:selectBooleanCheckbox id="isManagerAmafId"
				onclick="managerAmafClicked(this);"
				binding="#{pages$ManageAssets.chkIsManagerAmaf}">
			</h:selectBooleanCheckbox>

			<h:outputLabel
				value="#{msg['inheritanceFile.inheritedAssetTab.label.manager']}" />
			<h:panelGroup>
				<h:inputText readonly="true"
					id="managerNameId"
					binding="#{pages$ManageAssets.txtManagerName}"
					styleClass="READONLY" />
				<h:graphicImage
					id="searchManagerImageId"
					binding="#{pages$ManageAssets.searchImage}"	
					title="#{msg['inheritanceFile.inheritedAssetTab.tooltip.searchManager']}"
					style="MARGIN: 1px 1px -5px;cursor:hand;"
					onclick="showManagerSearchPopup();"
					url="../resources/images/app_icons/Search-tenant.png">
				</h:graphicImage>
			</h:panelGroup>
		</t:panelGrid>
	</t:panelGroup>
	<%--Column 3,4 Ends--%>
</t:panelGrid>

<t:panelGrid border="0" width="100%" cellpadding="1" cellspacing="1"  
	rendered="#{!(pages$inheritanceFile.viewModePopUp ||pages$inheritanceFile.pageModeView )}" 
	id="ExtraDetailWithEachAssetType">
	<t:panelGroup>
<t:div style="width:100%;background-color:#636163;" rendered="#{pages$ManageAssetBasicInfoTab.landProperties}">
<h:outputText value="#{msg['inhFile.label.LandnProp']}" styleClass="GROUP_LABEL"></h:outputText>
</t:div>
		<t:panelGrid columns="4" id="landAndPropertiesPanel" rendered="#{pages$ManageAssetBasicInfoTab.landProperties}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.govDept']}"/>
			<h:selectOneMenu 
				disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"	
				value="#{pages$ManageAssetBasicInfoTab.assetMemsView.govtDepttView.govtDepttIdString}"
				styleClass="SELECT_MENU">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$ApplicationBean.govtDepttList}"/>		
			</h:selectOneMenu>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.land']}"/>
			<h:inputText 
			maxlength="50"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldNumber}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.region']}"/>
			<h:inputText 
			maxlength="30"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.regionName}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.devolution']}"/>
			<h:inputText 
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.devolution}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.landStatus']}"/>
			<h:inputText 
			maxlength="50"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldStatus}"></h:inputText>
		</t:panelGrid>
<t:div style="width:100%;background-color:#636163;" rendered="#{pages$ManageAssetBasicInfoTab.animals}">
<h:outputText value="#{msg['inhFile.label.animal']}" styleClass="GROUP_LABEL"></h:outputText>
</t:div>		
		<t:panelGrid columns="4" id="animalsPanel" rendered="#{pages$ManageAssetBasicInfoTab.animals}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.nofAnimal']}"/>
			<h:inputText 
			maxlength="50"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldNumber}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.animalType']}"/>
			<h:inputText 
			maxlength="50"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldType}"></h:inputText>
		</t:panelGrid>
<t:div style="width:100%;background-color:#636163;" rendered="#{pages$ManageAssetBasicInfoTab.vehciles}">
<h:outputText value="#{msg['inhFile.label.vehicles']}" styleClass="GROUP_LABEL"></h:outputText>
</t:div>		
		<t:panelGrid columns="4" id="vehiclePanel"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%" rendered="#{pages$ManageAssetBasicInfoTab.vehciles}">
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.govDept']}"/>
			<h:selectOneMenu 
				disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
				value="#{pages$ManageAssetBasicInfoTab.assetMemsView.govtDepttView.govtDepttIdString}"
				styleClass="SELECT_MENU">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$ApplicationBean.govtDepttList}"/>		
			</h:selectOneMenu>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.plateNmber']}"/>
			<h:inputText 
			maxlength="50"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldNumber}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.vehicleCat']}"/>
			<h:inputText 
			maxlength="50"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.vehicleCategory}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.vehicleType']}"/>
			<h:inputText 
			maxlength="50"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldType}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.vehicleStatus']}"/>
			<h:inputText 
			maxlength="50"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldStatus}"></h:inputText>
		</t:panelGrid>
<t:div style="width:100%;background-color:#636163;" rendered="#{pages$ManageAssetBasicInfoTab.transportation}">
<h:outputText value="#{msg['inhFile.label.transportation']}" styleClass="GROUP_LABEL"></h:outputText>
</t:div>		
		<t:panelGrid columns="4" id="transportationPanel" rendered="#{pages$ManageAssetBasicInfoTab.transportation}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
			
		<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.plateNmber']}"/>
		<h:inputText  
		maxlength="50"
		readonly="true"
		value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldNumber}"></h:inputText>
		
		<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.income']}"/>
		<h:inputText 
		maxlength="50"
		readonly="true"
		value="#{pages$ManageAssetBasicInfoTab.assetMemsView.amountString}"></h:inputText>
		
		<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.transferParty']}"/>
		<h:inputText 
		maxlength="50"
		readonly="true"
		value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldParty}"></h:inputText>
		</t:panelGrid>
<t:div style="width:100%;background-color:#636163;" rendered="#{pages$ManageAssetBasicInfoTab.stockshares}">
<h:outputText value="#{msg['inhFile.label.stocknShare']}" styleClass="GROUP_LABEL"></h:outputText>
</t:div>		
		<t:panelGrid columns="4" id="stockAndSharesPanel" rendered="#{pages$ManageAssetBasicInfoTab.stockshares}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
			
		<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.company']}"/>
		<h:inputText 
		maxlength="50"
		readonly="true"
		value="#{pages$ManageAssetBasicInfoTab.assetMemsView.company}"></h:inputText>
		
		<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.nofShare']}"/>
		<h:inputText
		maxlength="50"
		readonly="true"
		 value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldNumber}"></h:inputText>
		
		<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.listingParty']}"/>
		<h:inputText 
		maxlength="50"
		readonly="true"
		value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldParty}"></h:inputText>
		
		</t:panelGrid>
<t:div style="width:100%;background-color:#636163;" rendered="#{pages$ManageAssetBasicInfoTab.licenses}">
<h:outputText value="#{msg['inhFile.label.license']}" styleClass="GROUP_LABEL"></h:outputText>
</t:div>		
		<t:panelGrid columns="4" id="licensePanel" rendered="#{pages$ManageAssetBasicInfoTab.licenses}" 
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.licenseName']}"/>
			<h:inputText 
			maxlength="50" 
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.licenseName}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.licenseNmber']}"/>
			<h:inputText  
			maxlength="50"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldNumber}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.licenseStatus']}"/>
			<h:inputText 
			maxlength="50" 
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldStatus}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.rentValue']}"/>
			<h:inputText 
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.amountString}"></h:inputText>
		</t:panelGrid>
<t:div style="width:100%;background-color:#636163;" rendered="#{pages$ManageAssetBasicInfoTab.loanLiabilities}">
<h:outputText value="#{msg['inhFile.label.loanliability']}" styleClass="GROUP_LABEL"></h:outputText>
</t:div>		
		<t:panelGrid columns="4" id="loanAndLiabilities" rendered="#{pages$ManageAssetBasicInfoTab.loanLiabilities}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.party']}"/>
			<h:inputText 
			maxlength="50"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldParty}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.amount']}"/>
			<h:inputText 
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.amountString}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.dueDate']}"/>
			<rich:calendar 
			disabled="true"				
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.dueDate}"
			locale="#{pages$ManageAssetBasicInfoTab.locale}" popup="true"
			datePattern="#{pages$ManageAssetBasicInfoTab.dateFormat}"
			showApplyButton="false" enableManualInput="false"
			inputStyle="width:185px;height:17px" />
		</t:panelGrid>
<t:div style="width:100%;background-color:#636163;" rendered="#{pages$ManageAssetBasicInfoTab.jewellery}">
<h:outputText value="#{msg['inhFile.label.jewellery']}" styleClass="GROUP_LABEL"></h:outputText>
</t:div>		
		<t:panelGrid columns="4" id="jewelleryPanel" rendered="#{pages$ManageAssetBasicInfoTab.jewellery}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.jewelType']}"/>
			<h:inputText
			maxlength="50" 
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldType}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.wightInGms']}"/>
			<h:inputText 
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.weightString}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.nofJewel']}"/>
			<h:inputText 
			maxlength="50"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldNumber}"></h:inputText>
		</t:panelGrid>
<t:div style="width:100%;background-color:#636163;" rendered="#{pages$ManageAssetBasicInfoTab.cash}">
<h:outputText value="#{msg['inhFile.label.cash']}" styleClass="GROUP_LABEL"></h:outputText>
</t:div>		
		<t:panelGrid columns="4" id="cashPanel" rendered="#{pages$ManageAssetBasicInfoTab.cash}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.amount']}"/>
			<h:inputText 
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.amountString}"></h:inputText>
		</t:panelGrid>
		
<t:div style="width:100%;background-color:#636163;" rendered="#{pages$ManageAssetBasicInfoTab.bankTransfer}">
<h:outputText value="#{msg['inhFile.label.bankTransfer']}" styleClass="GROUP_LABEL"></h:outputText>
</t:div>		
		<t:panelGrid columns="4" id="bankTransferPanel" rendered="#{pages$ManageAssetBasicInfoTab.bankTransfer}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.transferNmber']}"/>
			<h:inputText
			maxlength="50" 
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldNumber}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.amount']}"/>
			<h:inputText 
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.amountString}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.bank']}"/>
			<h:selectOneMenu 
				disabled="true"
				value="#{pages$ManageAssetBasicInfoTab.assetMemsView.bankView.bankIdString}"
				styleClass="SELECT_MENU">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$ApplicationBean.bankList}"/>				
			</h:selectOneMenu>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.expTransferDate']}"/>
			<rich:calendar
			disabled="true"	 
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.dueDate}"
			locale="#{pages$ManageAssetBasicInfoTab.locale}" popup="true"
			datePattern="#{pages$ManageAssetBasicInfoTab.dateFormat}"
			showApplyButton="false" enableManualInput="false"
			inputStyle="width:185px;height:17px" />
			
			
		</t:panelGrid>		
		
<t:div style="width:100%;background-color:#636163;" rendered="#{pages$ManageAssetBasicInfoTab.cheque}">
<h:outputText value="#{msg['inhFile.label.cheque']}" styleClass="GROUP_LABEL"></h:outputText>
</t:div>		
		<t:panelGrid columns="4" id="chequePanel" rendered="#{pages$ManageAssetBasicInfoTab.cheque}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.amount']}"/>
			<h:inputText
			readonly="true"
			 value="#{pages$ManageAssetBasicInfoTab.assetMemsView.amountString}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.bank']}"/>
			<h:selectOneMenu 
				disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
				value="#{pages$ManageAssetBasicInfoTab.assetMemsView.bankView.bankIdString}"
				styleClass="SELECT_MENU">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$ApplicationBean.bankList}"/>				
			</h:selectOneMenu>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.chequeNmbr']}"/>
			<h:inputText 
			maxlength="50"
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.fieldNumber}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.dueDate']}"/>
			<rich:calendar 
			
			disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.dueDate}"
			locale="#{pages$ManageAssetBasicInfoTab.locale}" popup="true"
			datePattern="#{pages$ManageAssetBasicInfoTab.dateFormat}"
			showApplyButton="false" enableManualInput="false"
			inputStyle="width:185px;height:17px" />
			
		</t:panelGrid>
<t:div style="width:100%;background-color:#636163;" rendered="#{pages$ManageAssetBasicInfoTab.pension}">
<h:outputText value="#{msg['inhFile.label.Pension']}" styleClass="GROUP_LABEL"></h:outputText>
</t:div>	
		<t:panelGrid columns="4" id="pensionPanel" rendered="#{pages$ManageAssetBasicInfoTab.pension}"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.amount']}"/>
			<h:inputText 
			readonly="true"
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.amountString}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.govDept']}"/>
			<h:selectOneMenu
				disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}" 
				value="#{pages$ManageAssetBasicInfoTab.assetMemsView.govtDepttView.govtDepttIdString}"
				styleClass="SELECT_MENU">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$ApplicationBean.govtDepttList}"/>		
			</h:selectOneMenu>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.bank']}"/>
			<h:selectOneMenu 
				disabled="#{pages$inheritanceFile.assetMemsFieldReadonly}"
				value="#{pages$ManageAssetBasicInfoTab.assetMemsView.bankView.bankIdString}"
				styleClass="SELECT_MENU">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
					<f:selectItems value="#{pages$ApplicationBean.bankList}"/>				
			</h:selectOneMenu>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.accountName']}"/>
			<h:inputText
			readonly="true"
			maxlength="50" 
			value="#{pages$ManageAssetBasicInfoTab.assetMemsView.accountName}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.amafAccount']}"/>
			<h:selectBooleanCheckbox
			disabled="true"
			rendered="#{pages$ManageAssetBasicInfoTab.assetMemsView.isAmafAccountBool}"
			 value="true"></h:selectBooleanCheckbox>			
		</t:panelGrid>
		
	</t:panelGroup>
</t:panelGrid>	
<t:div style="width:100%;background-color:#636163;"  rendered="#{!(pages$inheritanceFile.viewModePopUp || pages$inheritanceFile.pageModeView )}">
<h:outputText value="#{msg['inhFile.label.concerndDeptt']}" styleClass="GROUP_LABEL"></h:outputText>
</t:div>
<t:panelGrid border="0" width="100%" cellpadding="1" cellspacing="1"  rendered="#{!(pages$inheritanceFile.viewModePopUp ||pages$inheritanceFile.pageModeView )}"
	id="verifyFromGrid">
	<t:panelGroup>
		<t:panelGrid columns="4"
			columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2" width="100%">
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.govDept']}"/>
			<h:selectOneMenu 
				disabled="true"
				value="#{pages$ManageAssetBasicInfoTab.memsAssetCncrndDeptView.govtDepttView.govtDepttIdString}"
				styleClass="SELECT_MENU">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$ApplicationBean.govtDepttList}"/>		
			</h:selectOneMenu>
			

			<h:outputText
				value="#{msg['mems.inheritanceFile.fieldLabel.bank']}" />
			
			<h:selectOneMenu 
				disabled="true"
				value="#{pages$ManageAssetBasicInfoTab.memsAssetCncrndDeptView.bankView.bankIdString}"
				styleClass="SELECT_MENU">
				<f:selectItem itemLabel="#{msg['commons.combo.PleaseSelect']}"
					itemValue="-1" />
				<f:selectItems value="#{pages$ApplicationBean.bankList}"/>			
			</h:selectOneMenu>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.contactPerson']}"/>
			<h:inputText 
			readonly="true"
			maxlength="500"
			value="#{pages$ManageAssetBasicInfoTab.memsAssetCncrndDeptView.contactPersonName}"></h:inputText>
			
			<h:outputLabel value="#{msg['inquiryApplication.cellPhone']}"/>
			<h:inputText 
			readonly="true"
			maxlength="15"
			value="#{pages$ManageAssetBasicInfoTab.memsAssetCncrndDeptView.phone1}"></h:inputText>
			
			<h:outputLabel value="#{msg['contact.officephone']}"/>
			<h:inputText 
			readonly="true"
			maxlength="15"
			value="#{pages$ManageAssetBasicInfoTab.memsAssetCncrndDeptView.phone2}"></h:inputText>
			
			<h:outputText
			
				value="#{msg['mems.inheritanceFile.fieldLabel.fax']}" />
			<h:inputText  
			readonly="true"
			maxlength="15"
			value="#{pages$ManageAssetBasicInfoTab.memsAssetCncrndDeptView.fax}"></h:inputText>
			
			<h:outputLabel value="#{msg['mems.inheritanceFile.fieldLabel.email']}"/>
			<h:inputText
			readonly="true"
			maxlength="50"
			value="#{pages$ManageAssetBasicInfoTab.memsAssetCncrndDeptView.email}"></h:inputText>

		</t:panelGrid>
	</t:panelGroup>
</t:panelGrid>	
	<t:div style="height:5px;"></t:div>
