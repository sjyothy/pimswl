
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<script type="text/javascript">

						
</script>
<t:div styleClass="TAB_DETAIL_SECTION" style="width:100%" >
	<t:panelGrid id="projectDetailsTable" cellpadding="1px" width="100%" cellspacing="5px" styleClass="TAB_DETAIL_SECTION_INNER"  columns="4"
	columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
		<h:panelGroup>
				<h:outputLabel styleClass="mandatory" value="#{msg['commons.mandatory']}"/>			
            <h:outputLabel styleClass="LABEL"  value="#{msg['project.projectNumber']}" />
            </h:panelGroup>
		    <h:panelGroup>
              <h:inputText maxlength="20"   id="txtprojectNumber" styleClass="READONLY" readonly="true" value="#{pages$projectDetailsTab.projectNumber}" ></h:inputText>
	          <h:graphicImage  url="../resources/images/app_icons/Search-tenant.png" binding="#{pages$projectDetailsTab.imgSearchProject}" 
	           onclick="javaScript:showSearchProjectPopUp('#{pages$projectDetailsTab.projectKey_PageMode}','#{pages$projectDetailsTab.projectKey_PageModeSelectOnPopUp}','#{pages$projectDetailsTab.projectKey_AllowedStatus}','#{pages$projectDetailsTab.projectValue_AllowedStatuses}','#{pages$projectDetailsTab.projectKey_AllowedType}','#{pages$projectDetailsTab.projectValue_AllowedTypes}');"  
	           style ="MARGIN: 0px 0px -4px;" alt="#{msg[commons.searchProject]}" ></h:graphicImage>
	         </h:panelGroup> 
	        <h:outputLabel id="lblprojectName" styleClass="LABEL"  value="#{msg['project.projectName']}"></h:outputLabel>
	        <h:inputText maxlength="150"   id="txtprojectName" styleClass="READONLY" readonly="true" value="#{pages$projectDetailsTab.projectName}" ></h:inputText>
	        
	        <h:outputText	value="#{msg['project.projectStatus']}:"></h:outputText>
			<h:selectOneMenu id="selectprojectStatus"  styleClass="READONLY" readonly="true"  value="#{pages$projectDetailsTab.projectStatus}"  >
					<f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
		            <f:selectItems value="#{pages$ApplicationBean.projectStatus}" />
			</h:selectOneMenu>
			<h:outputLabel styleClass="LABEL" value="#{msg['project.projectType']}:" />
			<h:selectOneMenu  id="projectType" style="height: 24px" styleClass="READONLY" readonly="true" 
					value="#{pages$projectDetailsTab.projectType}" >
					<f:selectItem itemValue="-1" itemLabel="#{msg['commons.combo.PleaseSelect']}:" />
					<f:selectItems  value="#{pages$ApplicationBean.projectType}"  />
			</h:selectOneMenu>
			
			<h:outputLabel styleClass="LABEL"  value="#{msg['project.projectEstimatedCost']}"></h:outputLabel>
		    <h:inputText id="txtprojectEstimatedCost"  readonly="true"    styleClass="A_RIGHT_NUM READONLY" maxlength="15" binding="#{pages$projectDetailsTab.txtprojectEstimatedCost}"   value="#{pages$projectDetailsTab.projectEstimatedCost}">		    
		    <f:convertNumber minFractionDigits="2" maxFractionDigits="2" pattern="#{pages$projectDetailsTab.numberFormat}"/>
		    </h:inputText>
																        
   </t:panelGrid>		              
</t:div>
