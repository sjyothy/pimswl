
<script language="JavaScript" type="text/javascript">	
</script>

<t:panelGrid id="progrequiremntGrdFields"
	styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%"
	cellspacing="5px" columns="4">

	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['socialProgram.lbl.Requirement']}" />
	</h:panelGroup>
	<h:inputTextarea
		value="#{pages$programRequirement.programRequirements.description}"
		style="width: 185px;" />



</t:panelGrid>
<t:panelGrid id="progrequiremntGrdActions" styleClass="BUTTON_TD"
	cellpadding="1px" width="100%" cellspacing="5px">
	<h:panelGroup>
		<h:commandButton styleClass="BUTTON"
			value="#{msg['commons.saveButton']}"
			action="#{pages$programRequirement.onSave}" />
	</h:panelGroup>
</t:panelGrid>


<t:div styleClass="contentDiv" style="width:98%"
	id="progrequiremntContentDiv">
	<t:dataTable id="progrequiremntsDataTable" rows="200"
		value="#{pages$programRequirement.list}"
		binding="#{pages$programRequirement.dataTable}"
		preserveDataModel="false" preserveSort="false" var="dataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">

		<t:column id="progrequiremntdate" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.Requirement.lbl.Date']}"
					style="white-space: normal;" />
			</f:facet>

			<t:outputText style="white-space: normal;"
				value="#{dataItem.createdOn}"
				rendered="#{dataItem.isDeleted != '1' }">
				<f:convertDateTime pattern="#{pages$programRequirement.dateFormat}"
					timeZone="#{pages$programRequirement.timeZone}" />
			</t:outputText>
		</t:column>
		<t:column id="progrequiremntCreatedBy" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText
					value="#{msg['socialProgram.Requirement.lbl.CreatedBy']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"
				value="#{dataItem.createdEn}"
				rendered="#{dataItem.isDeleted != '1' }" />
		</t:column>
		<t:column id="progrequiremntdesc" width="55%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.Requirement.lbl.Desc']}"
					style="white-space: normal;" />
			</f:facet>
			<t:outputText value="#{dataItem.description}" rendered="#{dataItem.isDeleted != '1' }"
				style="white-space: normal;" />
		</t:column>
		<t:column id="programActionColumn" width="15%">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.action']}" />
			</f:facet>
			<h:graphicImage id="imgDeleteReq"
				rendered="#{dataItem.isDeleted != '1' }"
				title="#{msg['commons.delete']}"
				url="../resources/images/delete_icon.png"
				onclick="if (!confirm('#{msg['commons.confirmDelete']}')) return false; else onDeleted(this);">
			</h:graphicImage>
			<a4j:commandLink id="onReqDeleted"
				reRender="hiddenFields,progrequiremntContentDiv,progrequiremntsDataTable,successmsg,errormsg"
				onbeforedomupdate="javaScript:onRemoveDisablingDiv(); "
				action="#{pages$programRequirement.onDelete}" />
		</t:column>
	</t:dataTable>
</t:div>
