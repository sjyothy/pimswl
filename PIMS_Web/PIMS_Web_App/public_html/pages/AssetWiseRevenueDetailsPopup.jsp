<%-- 
  - Author: Danish Farooq
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for listing blocking details of person 
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">

	
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">

			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr width="100%">
					<td width="83%" height="100%" valign="top"
						class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['assetwiserevenue.heading']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="searchFrm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$AssetWiseRevenueDetailsPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN" style="width: 95%;">

												<div class="DETAIL_SECTION" style="width: 99.8%;">

													<t:div styleClass="contentDiv" style="width:95%">


														<t:dataTable id="dataTablePayments" rows="500"
															width="100%"
															value="#{pages$AssetWiseRevenueDetailsPopup.dataList}"
															binding="#{pages$AssetWiseRevenueDetailsPopup.dataTable}"
															preserveDataModel="false" preserveSort="false"
															var="dataItem" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true">


															<t:column sortable="true">
																<f:facet name="header">
																	<h:outputText value="#{msg['assetManage.assetNumber']}" />
																</f:facet>
																<h:outputText value="#{dataItem.inheritedAssetNumber}" />
															</t:column>


															<t:column sortable="false">
																<f:facet name="header">
																	<h:outputText
																		value="#{msg['searchAssets.assetNameEn']}" />
																</f:facet>
																<h:outputText value="#{dataItem.inheritedAssetNameEn}" />
															</t:column>


															<t:column sortable="false">
																<f:facet name="header">
																	<h:outputText
																		value="#{msg['searchAssets.assetNameAr']}" />
																</f:facet>
																<h:outputText value="#{dataItem.inheritedAssetNameAr}" />
															</t:column>


															<t:column sortable="false">
																<f:facet name="header">
																	<h:outputText value="#{msg['searchAssets.assetType']}" />
																</f:facet>
																<h:outputText
																	value="#{pages$AssetWiseRevenueDetailsPopup.englishLocale?dataItem.inheritedAssetTypeEn:dataItem.inheritedAssetTypeAr}" />
															</t:column>


															<t:column id="col1" width="10%" sortable="false"
																style="white-space: normal;">

																<f:facet name="header">
																	<h:outputText
																		value="#{msg['searchInheritenceFile.fileNo']}" />

																</f:facet>
																<t:outputText value="#{dataItem.fileNumber}"
																	style="white-space: normal;" styleClass="A_LEFT" />
															</t:column>
															
															<t:column sortable="false">
																<f:facet name="header">
																	<h:outputText value="#{msg['SearchBeneficiary.fileType']}" />
																</f:facet>
																<h:outputText
																	value="#{pages$AssetWiseRevenueDetailsPopup.englishLocale?dataItem.fileTypeEn:dataItem.fileTypeAr}" />
															</t:column>

															

															<t:column sortable="false">
																<f:facet name="header">
																	<h:outputText
																		value="#{msg['distribution.totalFileShare']}" />
																</f:facet>
																<t:outputText forceId="true" id="totalfileShares"
																	value="#{dataItem.fileShare}" />
															</t:column>


															<t:column sortable="false">
																<f:facet name="header">
																	<h:outputText
																		value="#{msg['inheritanceFile.label.totalAssetShare']}" />
																</f:facet>
																<t:outputText forceId="true" id="totalShares"
																	value="#{dataItem.totalShare}" />
															</t:column>


															<t:column id="shareValue" sortable="false">
																<f:facet name="header">
																	<h:outputText
																		value="#{msg['inheritanceFile.label.beneShareValue']}" />
																</f:facet>

																<t:outputText forceId="true" id="enteredValue"
																	value="#{dataItem.inheritedBeneficiaryShariaShare}" />
															</t:column>


															<t:column sortable="true">
																<f:facet name="header">
																	<h:outputText
																		value="#{msg['mems.inheritanceFile.fieldLabel.revType']}" />
																</f:facet>
																<h:outputText
																	value="#{dataItem.revenueTypeString}" />
															</t:column>

															<t:column sortable="true">
																<f:facet name="header">
																	<h:outputText
																		value="#{msg['inhDetails.reportLabel.revenue']}" />
																</f:facet>
																<h:outputText
																	value="#{dataItem.expectedRevenueString}" />
															</t:column>


															<t:column id="monthlyIncomeCol" sortable="true">
																<f:facet name="header">
																	<t:outputText id="monthlyIncomeT"
																		value="#{msg['researchFormBenef.monthlyIncome']}" />
																</f:facet>
																<t:outputText id="monthlyIncomeTT" styleClass="A_LEFT"
																	value="#{dataItem.monthlyRevenue}" />
															</t:column>


															<t:column id="annualIncomeCol" sortable="true">
																<f:facet name="header">
																	<t:outputText id="annualIncomeT"
																		value="#{msg['researchFormBenef.annualIncome']}" />
																</f:facet>
																<t:outputText id="annualIncomeTT" styleClass="A_LEFT"
																	value="#{dataItem.annualRevenue}" />
															</t:column>


														</t:dataTable>
													</t:div>
												</div>
											</div>
											<div>
												<table border="0">
													<tr>
														<td>
															<h:outputLabel styleClass="TABLE_LABEL"
																value="#{msg['assetwiserevenue.totalMonthlyIncome']}:"
																style="font-weight:bold;" />
														</td>
														<td>
															<h:outputLabel styleClass="TABLE_LABEL"
																value="#{pages$AssetWiseRevenueDetailsPopup.monthlyRevenue}"
																style="font-weight:bold;color:red;" />
														</td>
													</tr>
													<tr>
														<td>
															<h:outputLabel styleClass="TABLE_LABEL"
																value="#{msg['assetwiserevenue.totalAnnualIncome']}:"
																style="font-weight:bold;" />
														</td>
														<td>
															<h:outputLabel styleClass="TABLE_LABEL"
																value="#{pages$AssetWiseRevenueDetailsPopup.annualRevenue}"
																style="font-weight:bold;color:red;" />
														</td>
													</tr>
												</table>
											</div>
										</div>
									</h:form>

								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>


		</body>
	</html>
</f:view>