<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%-- Please remove above taglibs after completing your tab contents--%>
<t:div styleClass="contentDiv" style="width:95%">
<t:dataTable id="assetGrid" width="100%" styleClass="grid"
	value="#{pages$AssetTab.assetDataList}" rows="300"
	binding="#{pages$AssetTab.dataTable}"
	preserveDataModel="false" preserveSort="false" var="dataItem"
	rowClasses="row1,row2">




	<t:column id="assetNumber" width="10%" sortable="false"
		style="white-space: normal;">
		<f:facet name="header">
			<t:outputText value="#{msg['searchAssets.assetNumber']}" />
		</f:facet>
		<h:commandLink action="#{pages$AssetTab.openManageAsset}" value="#{dataItem.assetNumber}" />
	</t:column>
<%-- 	
	<t:column id="assetTypeEn" width="10%" sortable="false"
		style="white-space: normal;" rendered = "#{pages$AssetType.isEnglishLocale}">
		<f:facet name="header">
			<t:outputText value="#{msg['searchAssets.assetType']}" />
		</f:facet>
		<t:outputText value="#{dataItem.assetTypeView.assetTypeNameEn}" />
	</t:column>--%>
	<t:column id="assetTypeAr" width="10%" sortable="false"
		style="white-space: normal;" rendered = "#{!pages$AssetType.isEnglishLocale}">
		<f:facet name="header">
			<t:outputText value="#{msg['searchAssets.assetType']}" />
		</f:facet>
		<t:outputText value="#{dataItem.assetTypeView.assetTypeNameAr}" />
	</t:column>
<%-- 	
	<t:column id="assetNameEn" width="10%" sortable="false"
		style="white-space: normal;">
		<f:facet name="header">
			<t:outputText value="#{msg['searchAssets.assetNameEn']}" />
		</f:facet>
		<t:outputText value="#{dataItem.assetNameEn}" />
	</t:column>--%>
	<t:column id="assetNameAr" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['searchAssets.assetNameAr']}" />
		</f:facet>
		<t:outputText value="#{dataItem.assetNameAr}" />
	</t:column>
	<t:column id="assetDesc" width="10%" sortable="false"
		style="white-space: normal;" >
		<f:facet name="header">
			<t:outputText value="#{msg['searchAssets.assetDesc']}" />
		</f:facet>
		<t:outputText value="#{dataItem.description}" />
	</t:column>
	



</t:dataTable>
</t:div>
<%--Column 3,4 Ends--%>
