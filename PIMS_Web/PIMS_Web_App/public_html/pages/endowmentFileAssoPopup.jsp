

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>


<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	function sendDataToParent()
	{
		window.opener.onMessageFromEndowmentFileAsso();
	  	window.close();	  
	}	
	function closeWindow()
	{
		 window.close();
	}
	        
	function disableButtons(control)
	{
			
			var inputs = document.getElementsByTagName("INPUT");
			for (var i = 0; i < inputs.length; i++) {
			    if ( inputs[i] != null &&
			         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
			        )
			     {
			        inputs[i].disabled = true;
			    }
			}
			
			control.nextSibling.nextSibling.onclick();
			
			return 
		
	}
	
	function onMessageFromSearchEndowment()
	{	
	     document.getElementById("frm:onMessageFromSearchEndowment").onclick();
	}
	function   showPersonSearchPopup()
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   var screen_top = screen.top;
	     window.open('SearchPerson.jsf?viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	    
	}
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{	
	     
		 document.getElementById("frm:hdnPersonId").value=personId;
		 document.getElementById("frm:hdnPersonName").value=personName;
		 document.getElementById("frm:managerName").value=personName;
		 document.getElementById("frm:isManagerAmafId").checked=false;
	     //document.getElementById("frm:onMessageFromSearchPerson").onclick();
	}
	 
	function showPropertySearchPopUp()
	{
		var screen_width = 1024;
		var screen_height = 450;
		window.open('propertyList.jsf?pageMode=MODE_SELECT_ONE_POPUP&context=endowment','_blank','width='+(screen_width-50)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	 
	function populateProperty(propertyId)
	{
		document.getElementById("frm:hdnPropertyId").value=propertyId;
		document.getElementById("frm:onMessageFromSearchProperty").onclick();
	    
	}	
	
    
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">

				<tr>
					<td valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['endowment.lbl.AddEndowments']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0">
							<tr valign="top">
								<td>

									<h:form id="frm" enctype="multipart/form-data">
										<div class="SCROLLABLE_SECTION" style="width: 95%;">
											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:messages></h:messages>
															<h:outputText id="errorMessages"
																value="#{pages$endowmentFileAssoPopup.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
															<h:outputText
																value="#{pages$endowmentFileAssoPopup.successMessages}"
																escape="false" styleClass="INFO_FONT" />

															<h:inputHidden id="hdnPropertyId"
																value="#{pages$endowmentFileAssoPopup.hdnPropertyId}" />
															<h:inputHidden id="hdnPersonId"
																value="#{pages$endowmentFileAssoPopup.hdnPersonId}" />
															<h:inputHidden id="hdnPersonName"
																value="#{pages$endowmentFileAssoPopup.hdnPersonName}" />
															<h:commandLink id="onMessageFromSearchPerson"
																action="#{pages$endowmentFileAssoPopup.onMessageFromSearchPerson}" />
															<h:commandLink id="onMessageFromSearchEndowment"
																action="#{pages$endowmentFileAssoPopup.onMessageFromSearchEndowment}" />
															<h:commandLink id="onMessageFromSearchProperty"
																action="#{pages$endowmentFileAssoPopup.onMessageFromSearchProperty}" />

														</td>
													</tr>
												</table>
											</div>

											<div class="POPUP_DETAIL_SECTION" style="width: 95%;">

												<table width="100%" cellspacing="1px" cellpadding="5px">
													<tr>
														<td width="10%">
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowment.lbl.num']}" />
														</td>
														<td width="30%">
															<h:inputText
																value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.endowmentNum}"
																readonly="true" styleClass="READONLY">

															</h:inputText>
														</td>
														<td width="10%">
															<h:outputLabel styleClass="LABEL"
																value="#{msg['settlement.label.costCenter']}" />
														</td>
														<td width="30%">
															<h:inputText maxlength="6"
																value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.costCenter}">
															</h:inputText>
														</td>
													</tr>
													<tr>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowment.lbl.type']}" />
														</td>
														<td>
															<h:selectOneMenu
																value="#{pages$endowmentFileAssoPopup.endowmentTypeId}"
																disabled="#{! pages$endowmentFileAssoPopup.isPageModeUpdatable}"
																styleClass="SELECT_MENU"
																valueChangeListener="#{pages$endowmentFileAssoPopup.onAssetTypeChange}"
																onchange="submit();">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.endowmentAssetTypesList}" />
															</h:selectOneMenu>
														</td>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowment.lbl.name']}" />
														</td>
														<td id="endNameCol">
															<h:inputText
																readonly="#{ !pages$endowmentFileAssoPopup.isPageModeUpdatable}"
																styleClass="#{ !pages$endowmentFileAssoPopup.isPageModeUpdatable ?'READONLY':''}"
																value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.endowmentName}" />
															<h:commandLink id="lnkSearchProperty">
																<h:graphicImage id="imgSearchProperty"
																	rendered="#{ 
																		        pages$endowmentFileAssoPopup.isPageModeUpdatable &&
															    			    pages$endowmentFileAssoPopup.landProperties
															                    }"
																	title="#{msg['maintenanceRequest.searchProperties']}"
																	style="MARGIN: 1px 1px -3px;cursor:hand;"
																	onclick="showPropertySearchPopUp();"
																	url="../resources/images/app_icons/property.png">
																</h:graphicImage>
															</h:commandLink>
														</td>

													</tr>
													
													<tr>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.typeStatus']}:" />
															</td>
															<td>
															<h:selectOneMenu 
																disabled="#{ !pages$endowmentFileAssoPopup.isPageModeUpdatable}"
																value="#{pages$endowmentFileAssoPopup.endowmentTypeStatus}"
																styleClass="SELECT_MENU">
													
																<f:selectItems
																	value="#{pages$ApplicationBean.endowmentStatusList}" />
															</h:selectOneMenu>
														</td>
														<td></td>
														<td></td>
													</tr>
													<tr>

														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowment.lbl.maincategory']}" />
														</td>
														<td>
															<h:selectOneMenu
																disabled="#{ !pages$endowmentFileAssoPopup.isPageModeUpdatable}"
																value="#{pages$endowmentFileAssoPopup.endowmentMainCategoryId}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.endowmentMainCategory}" />

															</h:selectOneMenu>
														</td>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowment.lbl.category']}" />
														</td>
														<td>
															<h:selectOneMenu
																disabled="#{ !pages$endowmentFileAssoPopup.isPageModeUpdatable}"
																value="#{pages$endowmentFileAssoPopup.endowmentCategoryId}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.endowmentCategories}" />

															</h:selectOneMenu>
														</td>

													</tr>
													<tr>
														<td>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentfile.lbl.revenuePercentage']}" />
														</td>
														<td>
															<h:inputText
																value="#{pages$endowmentFileAssoPopup.revenuePercentageString}"
																onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)">
															</h:inputText>
														</td>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowmentfile.lbl.reconstructionPercentage']}" />
														</td>
														<td>
															<h:inputText
																value="#{pages$endowmentFileAssoPopup.reconstructionPercentageString}"
																onkeyup="removeNonNumeric(this)"
																onkeypress="removeNonNumeric(this)"
																onkeydown="removeNonNumeric(this)"
																onblur="removeNonNumeric(this)"
																onchange="removeNonNumeric(this)">
															</h:inputText>


														</td>
														<td>
														</td>
													</tr>
													<%-- 
													<tr>
														<td width="25%">
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowment.lbl.ownerShare']}" />
														</td>
														<td>
															<h:inputText
																readonly="#{ !pages$endowmentFileAssoPopup.isPageModeUpdatable}"
																styleClass="#{ !pages$endowmentFileAssoPopup.isPageModeUpdatable ?'READONLY':''}"
																value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.ownerShareString}"
																onkeyup="removeNonInteger(this)"
																onkeypress="removeNonInteger(this)"
																onkeydown="removeNonInteger(this)"
																onblur="removeNonInteger(this)"
																onchange="removeNonInteger(this)">

															</h:inputText>
														</td>
														
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowment.lbl.shareToEndow']}" />
														</td>
														<td>
															<h:inputText
																readonly="#{ !pages$endowmentFileAssoPopup.isPageModeUpdatable}"
																styleClass="#{ !pages$endowmentFileAssoPopup.isPageModeUpdatable ?'READONLY':''}"
																value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.shareToEndowString}"
																onkeyup="removeNonInteger(this)"
																onkeypress="removeNonInteger(this)"
																onkeydown="removeNonInteger(this)"
																onblur="removeNonInteger(this)"
																onchange="removeNonInteger(this)">
															</h:inputText>
														</td>
														--%>
													</tr>
													
													<tr>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['inheritanceFile.inheritedAssetTab.label.isManagerAmaf']}" />
														</td>
														<td>
															<h:selectBooleanCheckbox id="isManagerAmafId"
																disabled="#{ !pages$endowmentFileAssoPopup.isPageModeUpdatable}"
																styleClass="SELECT_MENU"
																binding="#{pages$endowmentFileAssoPopup.chkIsManagerAmaf}">
																<a4j:support
																	action="#{pages$endowmentFileAssoPopup.onManagerAmafClick}"
																	event="onclick"
																	reRender="managerName,hdnPersonName,hdnPersonId"></a4j:support>
															</h:selectBooleanCheckbox>
														</td>
														<td>
															<h:outputLabel
																value="#{msg['inheritanceFile.inheritedAssetTab.label.manager']}" />
														</td>
														<td>

															<h:inputText id="managerName" style="width:84%;"
																value="#{pages$endowmentFileAssoPopup.hdnPersonName}"
																styleClass="READONLY" />
															<h:graphicImage
																rendered="#{ pages$endowmentFileAssoPopup.isPageModeUpdatable}"
																title="#{msg['inheritanceFile.inheritedAssetTab.tooltip.searchManager']}"
																style="MARGIN: 1px 1px -3px;cursor:hand;"
																onclick="showPersonSearchPopup();"
																url="../resources/images/app_icons/Search-tenant.png">
															</h:graphicImage>

														</td>
													</tr>
													<tr>
														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowment.lbl.purpose']}" />
														</td>
														<td>
															<h:selectOneMenu
																disabled="#{ !pages$endowmentFileAssoPopup.isPageModeUpdatable}"
																value="#{pages$endowmentFileAssoPopup.endowmentPurposeId}"
																styleClass="SELECT_MENU">
																<f:selectItem
																	itemLabel="#{msg['commons.combo.PleaseSelect']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$ApplicationBean.endowmentPurposes}" />
															</h:selectOneMenu>
														</td>

														<td>
															<h:outputLabel styleClass="LABEL"
																value="#{msg['endowment.lbl.endowFor']}" />
														</td>
														<td id="endowForCol">
															<h:inputText maxlength="250"
																readonly="#{ !pages$endowmentFileAssoPopup.isPageModeUpdatable}"
																styleClass="#{ !pages$endowmentFileAssoPopup.isPageModeUpdatable ?'READONLY':''}"
																value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.endowFor}" />

														</td>
													</tr>

												</table>
											</div>

											<t:panelGrid border="0" width="95%" cellpadding="1"
												cellspacing="1" id="ExtraDetailWithEachAssetType">

												<t:div style="width:100%;background-color:#636163;"
													rendered="#{pages$endowmentFileAssoPopup.landProperties}">
													<h:outputText value="#{msg['inhFile.label.LandnProp']}"
														styleClass="GROUP_LABEL"></h:outputText>
												</t:div>

												<h:panelGrid columns="4" id="landAndPropertiesPanel"
													columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
													width="100%"
													binding="#{pages$endowmentFileAssoPopup.gridLandProperties}">

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.land']}" />
													<h:inputText maxlength="200" id="txtLandNumber"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials1}" />

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.devolution']}" />
													<h:inputText maxlength="200"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials2}" />

													<h:outputLabel
														value="#{msg['receiveProperty.addressLineOne']}" />
													<h:inputText maxlength="200"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials3}" />

													<h:outputLabel
														value="#{msg['botContractDetails.LandArea']}" />
													<h:inputText maxlength="200"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials4}"
														onkeyup="removeNonNumeric(this)"
														onkeypress="removeNonNumeric(this)"
														onkeydown="removeNonNumeric(this)"
														onblur="removeNonNumeric(this)"
														onchange="removeNonNumeric(this)" />

												</h:panelGrid>
												<t:div style="width:100%;background-color:#636163;"
													rendered="#{pages$endowmentFileAssoPopup.stockshares}">
													<h:outputText value="#{msg['inhFile.label.stocknShare']}"
														styleClass="GROUP_LABEL"></h:outputText>
												</t:div>
												<h:panelGrid columns="4" id="stockAndSharesPanel"
													binding="#{pages$endowmentFileAssoPopup.gridStockshares}"
													columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
													width="100%">

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.company']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials1}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.nofShare']}" />
													<h:inputText maxlength="50"
														onkeyup="removeNonNumeric(this)"
														onkeypress="removeNonNumeric(this)"
														onkeydown="removeNonNumeric(this)"
														onblur="removeNonNumeric(this)"
														onchange="removeNonNumeric(this)"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials2}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.listingParty']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials3}"></h:inputText>

												</h:panelGrid>
												<t:div style="width:100%;background-color:#636163;"
													rendered="#{pages$endowmentFileAssoPopup.licenses}">
													<h:outputText value="#{msg['inhFile.label.license']}"
														styleClass="GROUP_LABEL"></h:outputText>
												</t:div>
												<h:panelGrid columns="4" id="licensePanel"
													binding="#{pages$endowmentFileAssoPopup.gridLicenses}"
													columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
													width="100%">

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.licenseName']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials1}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.licenseNmber']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials2}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.licenseStatus']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials3}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.rentValue']}" />
													<h:inputText onkeyup="removeNonNumeric(this)"
														onkeypress="removeNonNumeric(this)"
														onkeydown="removeNonNumeric(this)"
														onblur="removeNonNumeric(this)"
														onchange="removeNonNumeric(this)"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials4}"></h:inputText>
												</h:panelGrid>
												<t:div style="width:100%;background-color:#636163;"
													rendered="#{pages$endowmentFileAssoPopup.animals}">

													<h:outputText value="#{msg['inhFile.label.animal']}"
														styleClass="GROUP_LABEL"></h:outputText>
												</t:div>

												<h:panelGrid columns="4" id="animalsPanel"
													columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
													width="100%"
													binding="#{pages$endowmentFileAssoPopup.gridAnimals}">

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.nofAnimal']}" />
													<h:inputText maxlength="150"
														onkeyup="removeNonInteger(this)"
														onkeypress="removeNonInteger(this)"
														onkeydown="removeNonInteger(this)"
														onblur="removeNonInteger(this)"
														onchange="removeNonInteger(this)"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials1}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.animalType']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials2}"></h:inputText>
												</h:panelGrid>
												<t:div style="width:100%;background-color:#636163;"
													rendered="#{pages$endowmentFileAssoPopup.vehicles}">
													<h:outputText value="#{msg['inhFile.label.vehicles']}"
														styleClass="GROUP_LABEL"></h:outputText>
												</t:div>
												<h:panelGrid columns="4" id="vehiclePanel"
													columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
													width="100%"
													binding="#{pages$endowmentFileAssoPopup.gridVehicles}">

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.govDept']}" />
													<h:selectOneMenu
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.govtDepttIdString}"
														styleClass="SELECT_MENU">
														<f:selectItem
															itemLabel="#{msg['commons.combo.PleaseSelect']}"
															itemValue="-1" />
														<f:selectItems
															value="#{pages$ApplicationBean.govtDepttList}" />
													</h:selectOneMenu>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.plateNmber']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials1}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.vehicleCat']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials2}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.vehicleType']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials3}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.vehicleStatus']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials4}"></h:inputText>
												</h:panelGrid>
												<t:div style="width:100%;background-color:#636163;"
													rendered="#{pages$endowmentFileAssoPopup.transportation}">
													<h:outputText
														value="#{msg['inhFile.label.transportation']}"
														styleClass="GROUP_LABEL"></h:outputText>
												</t:div>
												<h:panelGrid columns="4" id="transportationPanel"
													binding="#{pages$endowmentFileAssoPopup.gridTransportation}"
													columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
													width="100%">

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.plateNmber']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials1}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.income']}" />
													<h:inputText maxlength="50"
														onkeyup="removeNonNumeric(this)"
														onkeypress="removeNonNumeric(this)"
														onkeydown="removeNonNumeric(this)"
														onblur="removeNonNumeric(this)"
														onchange="removeNonNumeric(this)"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials2}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.transferParty']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials3}"></h:inputText>
												</h:panelGrid>
												<t:div style="width:100%;background-color:#636163;"
													rendered="#{pages$endowmentFileAssoPopup.cash}">
													<h:outputText value="#{msg['inhFile.label.cash']}"
														styleClass="GROUP_LABEL"></h:outputText>
												</t:div>
												<t:panelGrid columns="4" id="cashPanel"
													rendered="#{pages$endowmentFileAssoPopup.cash}"
													columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
													width="100%"
													binding="#{pages$endowmentFileAssoPopup.gridCash}">

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
													<h:inputText onkeyup="removeNonNumeric(this)"
														onkeypress="removeNonNumeric(this)"
														onkeydown="removeNonNumeric(this)"
														onblur="removeNonNumeric(this)"
														onchange="removeNonNumeric(this)"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials1}"></h:inputText>
												</t:panelGrid>

												<t:div style="width:100%;background-color:#636163;"
													rendered="#{pages$endowmentFileAssoPopup.bankTransfer}">
													<h:outputText value="#{msg['inhFile.label.bankTransfer']}"
														styleClass="GROUP_LABEL"></h:outputText>
												</t:div>
												<t:panelGrid columns="4" id="bankTransferPanel"
													rendered="#{pages$endowmentFileAssoPopup.bankTransfer}"
													columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
													width="100%"
													binding="#{pages$endowmentFileAssoPopup.gridBankTransfer}">

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.transferNmber']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials2}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
													<h:inputText onkeyup="removeNonNumeric(this)"
														onkeypress="removeNonNumeric(this)"
														onkeydown="removeNonNumeric(this)"
														onblur="removeNonNumeric(this)"
														onchange="removeNonNumeric(this)"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials1}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.bank']}" />
													<h:selectOneMenu
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.bankIdStr}"
														styleClass="SELECT_MENU">
														<f:selectItem
															itemLabel="#{msg['commons.combo.PleaseSelect']}"
															itemValue="-1" />
														<f:selectItems value="#{pages$ApplicationBean.bankList}" />
													</h:selectOneMenu>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.expTransferDate']}" />
													<rich:calendar
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.dueDate}"
														locale="#{pages$endowmentFileAssoPopup.locale}"
														popup="true"
														datePattern="#{pages$endowmentFileAssoPopup.dateFormat}"
														showApplyButton="false" enableManualInput="false"
														inputStyle="width:185px;height:17px" />


												</t:panelGrid>

												<t:div style="width:100%;background-color:#636163;"
													rendered="#{pages$endowmentFileAssoPopup.cheque}">
													<h:outputText value="#{msg['inhFile.label.cheque']}"
														styleClass="GROUP_LABEL"></h:outputText>
												</t:div>
												<t:panelGrid columns="4" id="chequePanel"
													rendered="#{pages$endowmentFileAssoPopup.cheque}"
													columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
													width="100%"
													binding="#{pages$endowmentFileAssoPopup.gridCheque}">

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
													<h:inputText onkeyup="removeNonNumeric(this)"
														onkeypress="removeNonNumeric(this)"
														onkeydown="removeNonNumeric(this)"
														onblur="removeNonNumeric(this)"
														onchange="removeNonNumeric(this)"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials1}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.bank']}" />
													<h:selectOneMenu
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.bankIdStr}"
														styleClass="SELECT_MENU">
														<f:selectItem
															itemLabel="#{msg['commons.combo.PleaseSelect']}"
															itemValue="-1" />
														<f:selectItems value="#{pages$ApplicationBean.bankList}" />
													</h:selectOneMenu>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.chequeNmbr']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials2}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.dueDate']}" />
													<rich:calendar
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.dueDate}"
														locale="#{pages$endowmentFileAssoPopup.locale}"
														popup="true"
														datePattern="#{pages$endowmentFileAssoPopup.dateFormat}"
														showApplyButton="false" enableManualInput="false"
														inputStyle="width:185px;height:17px" />

												</t:panelGrid>
												<t:div style="width:100%;background-color:#636163;"
													rendered="#{pages$endowmentFileAssoPopup.pension}">
													<h:outputText value="#{msg['inhFile.label.Pension']}"
														styleClass="GROUP_LABEL"></h:outputText>
												</t:div>
												<t:panelGrid columns="4" id="pensionPanel"
													rendered="#{pages$endowmentFileAssoPopup.pension}"
													columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
													width="100%"
													binding="#{pages$endowmentFileAssoPopup.gridPension}">

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.amount']}" />
													<h:inputText onkeyup="removeNonNumeric(this)"
														onkeypress="removeNonNumeric(this)"
														onkeydown="removeNonNumeric(this)"
														onblur="removeNonNumeric(this)"
														onchange="removeNonNumeric(this)"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials1}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.govDept']}" />
													<h:selectOneMenu
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.govtDepttIdString}"
														styleClass="SELECT_MENU">
														<f:selectItem
															itemLabel="#{msg['commons.combo.PleaseSelect']}"
															itemValue="-1" />
														<f:selectItems
															value="#{pages$ApplicationBean.govtDepttList}" />
													</h:selectOneMenu>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.bank']}" />
													<h:selectOneMenu
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.bankIdStr}"
														styleClass="SELECT_MENU">
														<f:selectItem
															itemLabel="#{msg['commons.combo.PleaseSelect']}"
															itemValue="-1" />
														<f:selectItems value="#{pages$ApplicationBean.bankList}" />
													</h:selectOneMenu>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.accountName']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials2}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.amafAccount']}" />
													<h:selectBooleanCheckbox
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.isAmafAccountBool}"></h:selectBooleanCheckbox>
												</t:panelGrid>

												<t:div style="width:100%;background-color:#636163;"
													rendered="#{pages$endowmentFileAssoPopup.jewellery}">
													<h:outputText value="#{msg['inhFile.label.jewellery']}"
														styleClass="GROUP_LABEL"></h:outputText>
												</t:div>
												<t:panelGrid columns="4" id="jewelleryPanel"
													rendered="#{pages$endowmentFileAssoPopup.jewellery}"
													columnClasses="COLUMN1,COLUMN2,COLUMN1,COLUMN2"
													width="100%"
													binding="#{pages$endowmentFileAssoPopup.gridJewellery}">

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.jewelType']}" />
													<h:inputText maxlength="150"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials2}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.wightInGms']}" />
													<h:inputText onkeyup="removeNonNumeric(this)"
														onkeypress="removeNonNumeric(this)"
														onkeydown="removeNonNumeric(this)"
														onblur="removeNonNumeric(this)"
														onchange="removeNonNumeric(this)"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials3}"></h:inputText>

													<h:outputLabel
														value="#{msg['mems.inheritanceFile.fieldLabel.nofJewel']}" />
													<h:inputText maxlength="50"
														onkeyup="removeNonInteger(this)"
														onkeypress="removeNonInteger(this)"
														onkeydown="removeNonInteger(this)"
														onblur="removeNonInteger(this)"
														onchange="removeNonInteger(this)"
														value="#{pages$endowmentFileAssoPopup.endowmentFileAsso.endowment.fielDetials1}"></h:inputText>
												</t:panelGrid>

											</t:panelGrid>
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<td height="5px"></td>
												</tr>
												<tr>
													<td class="BUTTON_TD MARGIN" colspan="6">
														<h:commandButton id="idSaveBtn"
															value="#{msg['commons.saveButton']}" styleClass="BUTTON"
															action="#{pages$endowmentFileAssoPopup.onSave}">
														</h:commandButton>
													</td>
												</tr>
											</table>
										</div>
									</h:form>


								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>
		</body>
	</html>
</f:view>