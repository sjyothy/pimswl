<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<script type="text/javascript">
	
		
	</script>
<t:div style="width:100%;">
	<t:panelGrid columns="1" cellpadding="0" cellspacing="0">
		<t:div styleClass="contentDiv" style="width:98%;">
			<t:dataTable id="projectFollowTable" renderedIfEmpty="true" width="100%"
				cellpadding="0" cellspacing="0" var="dataItem"
				binding="#{pages$projectFollowupDetailsTab.dataTable}"
				value="#{pages$projectFollowupDetailsTab.projectFollowupList}"
				preserveDataModel="false" preserveSort="false"
				rowClasses="row1,row2" rules="all" rows="#{pages$projectFollowupDetailsTab.paginatorRows}">
				<t:column sortable="true" width="200" id="followupDate">
					<f:facet name="header">
						<t:outputText value="#{msg['followup.date']}" />
					</f:facet>
					<t:outputText 
						value="#{dataItem.followupDate}"
						 style="white-space: normal;" >
						<f:convertDateTime pattern="#{pages$projectFollowupDetailsTab.dateFormat}"
							 timeZone="#{pages$projectFollowupDetailsTab.timeZone}" />
					</t:outputText>
				</t:column>
				<t:column sortable="true" width="300" id="followupRemarks">
					<f:facet name="header">
						<t:outputText
							value="#{msg['maintenanceRequest.remarks']}" />
					</f:facet>
					<t:outputText 
						value="#{dataItem.remark}"
						 style="white-space: normal;" />
				</t:column>
				<t:column sortable="true" width="200" id="actualStartDate">
					<f:facet name="header">
						<t:outputText value="#{msg['project.details.actualStartDate']}" />
					</f:facet>
					<t:outputText
						value="#{dataItem.actualProjectStartDate}"
						style="white-space: normal;">
							<f:convertDateTime pattern="#{pages$projectFollowupDetailsTab.dateFormat}"
							 timeZone="#{pages$projectFollowupDetailsTab.timeZone}" />
					</t:outputText>
				</t:column>
				<t:column sortable="true" width="250" id="newAchievedMilestone">
					<f:facet name="header">
						<t:outputText value="#{msg['project.details.newAchievedMilestone']}" />
					</f:facet>
					<t:outputText
						value="#{dataItem.newAchievedProjectMilestoneName}"
						style="white-space: normal;">
					</t:outputText>
				</t:column>
				<t:column sortable="true" width="50" id="newCompletionPercentage">
					<f:facet name="header">
						<t:outputText value="#{msg['project.details.newCompletionPercent']}" />
					</f:facet>
					<t:outputText
						value="#{dataItem.newCompletionPercentage}"
						style="white-space: normal;">
					</t:outputText>
				</t:column>
			</t:dataTable>
		</t:div>
		<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
			style="width:99.1%;">
			<t:panelGrid columns="2" border="0" width="100%" cellpadding="1"
				cellspacing="1">
				<t:panelGroup styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
					<t:div styleClass="JUG_NUM_REC_ATT">
						<h:outputText value="#{msg['commons.records']}" />
						<h:outputText value=" : " />
						<h:outputText value="#{pages$projectFollowupDetailsTab.recordSize}" />
					</t:div>
				</t:panelGroup>
				<t:panelGroup>
					<t:dataScroller id="projectFollowupScroller" for="projectFollowTable"
						paginator="true" fastStep="1" paginatorMaxPages="#{pages$projectFollowupDetailsTab.paginatorMaxPages}"
						immediate="false" paginatorTableClass="paginator"
						renderFacetsIfSinglePage="true"
						paginatorTableStyle="grid_paginator" layout="singleTable"
						paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
						paginatorActiveColumnStyle="font-weight:bold;"
						paginatorRenderLinkForActive="false"
						pageIndexVar="followUpPageNumber" styleClass="JUG_SCROLLER">
						<f:facet name="first">
							<t:graphicImage url="../#{path.scroller_first}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastrewind">
							<t:graphicImage url="../#{path.scroller_fastRewind}"></t:graphicImage>
						</f:facet>
						<f:facet name="fastforward">
							<t:graphicImage url="../#{path.scroller_fastForward}"></t:graphicImage>
						</f:facet>
						<f:facet name="last">
							<t:graphicImage url="../#{path.scroller_last}"></t:graphicImage>
						</f:facet>
						<t:div styleClass="PAGE_NUM_BG" rendered="true">
							<h:outputText styleClass="PAGE_NUM"
								value="#{msg['commons.page']}" />
							<h:outputText styleClass="PAGE_NUM"
								style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
								value="#{requestScope.followUpPageNumber}" />
						</t:div>
					</t:dataScroller>
				</t:panelGroup>
			</t:panelGrid>
		</t:div>
	</t:panelGrid>
</t:div>