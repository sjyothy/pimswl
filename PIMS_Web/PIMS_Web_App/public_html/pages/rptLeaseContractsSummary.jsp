<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>

<%@ page import="javax.faces.component.UIViewRoot;"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<script language="javascript" type="text/javascript">
	function openLoadReportPopup(pageName) 
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}	
	
	function showPopup(personType)
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   //window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
	    window.open('SearchPerson.jsf?persontype='+personType+'&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{		    	   
	    if(hdnPersonType=='TENANT')
	    {
			 document.getElementById("formLeaseContractsSummary:hdnTenantId").value = personId;
			 document.getElementById("formLeaseContractsSummary:tenantName").value = personName;
			 document.getElementById("formLeaseContractsSummary:retTenant").onclick();
        }
	}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>

<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}" style="overflow:hidden">

	<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache"/>
			<meta http-equiv="cache-control" content="no-cache"/>
			<meta http-equiv="expires" content="0"/>
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
			<meta http-equiv="description" content="This is my page"/>
		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>	
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>
	 								
			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

            <script language="JavaScript" type="text/javascript" src="../resources/jscripts/popcalendar_en.js"></script>
		

<script language="javascript" type="text/javascript">


	
</SCRIPT>

</head>
	<body class="BODY_STYLE">
		<div style="width:1024px;">
			
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td  colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>	<td class="HEADER_TD">
										<h:outputLabel value= "#{msg['commons.report.name.leaseContractsSummary.jsp']}" styleClass="HEADER_FONT" style="padding:10px"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						
					
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" 
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									>
								</td>
								<td width="0%" height="99%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:450px;">
									<h:form id="formLeaseContractsSummary">
											<h:inputHidden id="hdnTenantId" value="#{pages$LeaseContractsSummary.hdnTenantId}" />
											<h:commandLink id="retTenant" action="#{pages$LeaseContractsSummary.retrieveTenant}" />
											
									<div class="MARGIN" style="width:96%">
									<table cellpadding="0" cellspacing="0"  >
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>

									<div class="DETAIL_SECTION" style="width:99.6%;">
									<h:outputLabel value="#{msg['commons.report.criteria']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
										<table cellpadding="0px" cellspacing="0px"
												class="DETAIL_SECTION_INNER">
											<tr>
												<td width="97%">
												<table width="100%">

												<tr>
													<td width="25%">
														<h:outputLabel styleClass="LABEL"
															value="#{msg['commons.Contract.Number']}" />
														:
													</td>
													<td width="25%">
														<h:inputText id="contNum"   
															value="#{pages$LeaseContractsSummary.reportCriteria.contractNumber}"></h:inputText>
													</td>
													<td width="25%">
													<h:outputLabel styleClass="LABEL"
															value="#{msg['chequeList.tenantName']}"></h:outputLabel>
														:
													</td>
													<td width="25%">
													<h:inputText id="tenantName" 
															value="#{pages$LeaseContractsSummary.reportCriteria.tenantName}"></h:inputText><h:graphicImage
																			url="../resources/images/app_icons/Search-tenant.png"
																			onclick="showPopup('#{pages$LeaseContractsSummary.personTenant}');"
																			style="MARGIN: 0px 0px -4px; CURSOR: hand;"
																			alt="#{msg[contract.searchTenant]}"></h:graphicImage>
													</td>
												</tr>
												<tr>
													<td><h:outputLabel styleClass="LABEL"
															value="#{msg['contract.property.Name']}"></h:outputLabel>
														:
													</td>
													<td><h:inputText id="propertyName" 
															value="#{pages$LeaseContractsSummary.reportCriteria.propertyComercialName}"></h:inputText>
													</td>
													<td><h:outputLabel styleClass="LABEL"
															value="#{msg['unit.number']}"></h:outputLabel>
														:
													</td>
													<td><h:inputText id="unitRefNo" 
															value="#{pages$LeaseContractsSummary.reportCriteria.unitNum}"></h:inputText>
													</td>
												</tr>
												<tr>

																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.unitRentAmountFrom']} :"></h:outputLabel>
																			</td>

																			<td>


																				<h:inputText id="txtUnitRentAmountFrom"
																					value="#{pages$LeaseContractsSummary.reportCriteria.unitRentAmountFrom}"
																					styleClass="INPUT_TEXT" maxlength="20"></h:inputText>

																			</td>


																			<td>


																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.unitRentAmountTo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtUnitRentAmountTo"
																					value="#{pages$LeaseContractsSummary.reportCriteria.unitRentAmountTo}"
																					maxlength="20"></h:inputText>
																			</td>
																		</tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.contractAmountFrom']} :"></h:outputLabel>
																			</td>

																			<td>


																				<h:inputText id="txtContractAmountFrom"
																					value="#{pages$LeaseContractsSummary.reportCriteria.contractRentAmountFrom}"
																					styleClass="INPUT_TEXT" maxlength="20"></h:inputText>

																			</td>


																			<td>


																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.contractAmountTo']} :"></h:outputLabel>
																			</td>
																			<td>


																				<h:inputText id="txtContractAmountTo"
																					value="#{pages$LeaseContractsSummary.reportCriteria.contractRentAmountTo}"
																					maxlength="20"></h:inputText>
																			</td>
																		</tr>
												
												<tr>
													<td >
														<h:outputLabel styleClass="LABEL" 
															value="#{msg['contract.fromContractDate']}"> :</h:outputLabel>
													</td>
													<td >
														<rich:calendar id="fromContractD"
															datePattern="#{pages$LeaseContractsSummary.dateFormat}"
															value="#{pages$LeaseContractsSummary.reportCriteria.fromContractDateRich}" 
															inputStyle="width: 170px; height: 14px"
								                        	enableManualInput="false" 
								                        	 
															></rich:calendar>
													</td>
															<td>
														<h:outputLabel styleClass="LABEL"
															value="#{msg['contract.toContractDate']}" />
														:
													</td>
													<td>
														<rich:calendar id="toConractD" datePattern="#{pages$LeaseContractsSummary.dateFormat}"
															value="#{pages$LeaseContractsSummary.reportCriteria.toContractDateRich}"
															inputStyle="width: 170px; height: 14px"
								                        	enableManualInput="false" 
								                        	 
															></rich:calendar>
													</td>
												</tr>
												<tr>
													<td>
														<h:outputLabel styleClass="LABEL"
															value="#{msg['contract.date.fromExpiry']}" />
														:
													</td>
													<td>
														<rich:calendar id="fromExpD" 	datePattern="#{pages$LeaseContractsSummary.dateFormat}"
															value="#{pages$LeaseContractsSummary.reportCriteria.fromContractExpDateRich}"
															inputStyle="width: 170px; height: 14px"
								                        	enableManualInput="false" 
								                        	 
															></rich:calendar>
													</td>
													<td>
														<h:outputLabel styleClass="LABEL"
															value="#{msg['contract.date.toExpiry']}"></h:outputLabel>
														:
													</td>
													<td>
														<rich:calendar id="toExpD" datePattern="#{pages$LeaseContractsSummary.dateFormat}"
															value="#{pages$LeaseContractsSummary.reportCriteria.toContractExpDateRich}"
															inputStyle="width: 170px; height: 14px"
								                        	enableManualInput="false" 
								                        	 
															></rich:calendar>
													</td>
												</tr>
												<tr>
													<td><h:outputLabel styleClass="LABEL"
															value="#{msg['contract.contractType']}"></h:outputLabel>
														:
													</td>
													<td><h:selectOneMenu id="contractType"  
															value="#{pages$LeaseContractsSummary.reportCriteria.contractType}">
															<f:selectItem id="selectAllType" itemValue="-1"
																itemLabel="#{msg['commons.All']}" />
															<f:selectItems
																value="#{pages$ApplicationBean.contractType}" />
														</h:selectOneMenu>
													</td>
													<td><h:outputLabel styleClass="LABEL"
															value="#{msg['ContractorSearch.contractStatus']}"></h:outputLabel>
														:
													</td>
													<td><h:selectOneMenu id="contractStatus"  
															value="#{pages$LeaseContractsSummary.reportCriteria.contractStatus}"	>
															<f:selectItem id="selectAll" itemValue="-1"
																itemLabel="#{msg['commons.All']}" />
															<f:selectItems
																value="#{pages$ApplicationBean.contractStatus}" />
														</h:selectOneMenu>
													</td>
												</tr>
												<tr>
													<td><h:outputLabel styleClass="LABEL"
															value="#{msg['commons.report.costCenter']}"></h:outputLabel>
														:
													</td>
													<td>
														<h:inputText id="costCenter"
															value="#{pages$LeaseContractsSummary.reportCriteria.costCenter}"
															maxlength="20"></h:inputText>
													</td>
													<td><h:outputLabel styleClass="LABEL"
															value="#{msg['commons.report.propertyNumber']}"></h:outputLabel>
														:
													</td>
													<td>							
														<h:inputText id="propertyNumber"
															value="#{pages$LeaseContractsSummary.reportCriteria.propertyNumber}"
															maxlength="20"></h:inputText>
													</td>
												</tr>

													<tr>
														<td>
															<h:outputLabel styleClass="LABEL"
															value="#{msg['receiveProperty.city']}"></h:outputLabel> :
														</td>
														<td>
															<h:selectOneMenu id="selectcity"
																value="#{pages$LeaseContractsSummary.reportCriteria.city}">
																<f:selectItem itemLabel="#{msg['commons.All']}"
																	itemValue="-1" />
																<f:selectItems
																	value="#{pages$LeaseContractsSummary.cityList}" />
															</h:selectOneMenu>
														</td>
														
														<td>
														</td>
														
														<td>
														</td>
													</tr>

													<tr>
														<td class="BUTTON_TD" colspan="4">
												        		<pims:security screen="Pims.Reports.LeaseContractSummary"	action="create">
																	<h:commandButton id="btnPrint" type="submit" style="width:100px;"
																		styleClass="BUTTON" 
																		action="#{pages$LeaseContractsSummary.btnPrint_Click}" 
																		binding="#{pages$LeaseContractsSummary.btnPrint}"
																		value="#{msg['commons.report.name.leaseContractsSummary.view']}" />
																</pims:security>
												        		<pims:security screen="Pims.Reports.LeaseContractDetails"	action="create">
																	<h:commandButton id="btnPrint2" type="submit"
																		styleClass="BUTTON"
																		action="#{pages$LeaseContractsSummary.btnPrintDetail_Click}"
																		binding="#{pages$LeaseContractsSummary.btnPrintDetail}"
																		value="#{msg['commons.report.name.leaseContractsDetail.view']}" />
																</pims:security>
														</td>
													</tr>	
													</table>
													</td>
														<td width="3%">
														&nbsp;
													</td>
												</tr>
													
												</table>
											</div>
											</div>
										</h:form>
									</div>
								</td></tr>
							</table>
						</td>
						</tr>
						<tr>
						    <td colspan="2">
						    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
						       		<tr><td class="footer"><h:outputLabel value= "#{msg['commons.footer.message']}" />
						       				</td></tr>
					    		</table>
					        </td>
					    </tr> 
					</table>
					</div>
			</body>
		</html>
	</f:view> 