

<t:div styleClass="contentDiv" style="width:95%;margin-bottom:15px;">


	<t:dataTable id="dataTablePayments" rows="115" width="100%"
		value="#{pages$financialAspectsPopup.dataList}"
		binding="#{pages$financialAspectsPopup.dataTable}"
		preserveDataModel="false" preserveSort="false" var="dataItem"
		rowClasses="row1,row2" rules="all" renderedIfEmpty="true">

		<t:column id="assetTypeArCol" sortable="true">
			<f:facet name="header">
				<t:outputText id="assetTypeArAmnt"
					value="#{msg['searchAssets.assetType']}" />
			</f:facet>
			<h:commandLink id="assetType" styleClass="A_LEFT"
				action="#{pages$financialAspectsPopup.onOpenAssetWiseRevenueDetailsPopup}"
				value="#{ pages$financialAspectsPopup.englishLocale? dataItem.assetTypeEn:dataItem.assetTypeAr}" />
					      
		</t:column>

		<t:column id="monthlyIncomeCol" sortable="true">
			<f:facet name="header">
				<t:outputText id="monthlyIncomeT"
					value="#{msg['researchFormBenef.monthlyIncome']}" />
			</f:facet>
			<t:outputText id="monthlyIncomeTT" styleClass="A_LEFT"
				value="#{dataItem.monthlyRevenue}" />
		</t:column>

		<t:column id="annualIncomeCol" sortable="true">
			<f:facet name="header">
				<t:outputText id="annualIncomeT"
					value="#{msg['researchFormBenef.annualIncome']}" />
			</f:facet>
			<t:outputText id="annualIncomeTT" styleClass="A_LEFT"
				value="#{dataItem.annualRevenue}" />
		</t:column>

	</t:dataTable>
</t:div>


<t:div style="margin-bottom:10px; width: 100%;">

	<t:panelGrid columns="4" border="0" width="50%" cellpadding="1"
		cellspacing="1">
		<h:outputLabel styleClass="TABLE_LABEL"
			value="#{msg['assetwiserevenue.totalMonthlyIncome']}:"
			style="font-weight:bold;" />
		<h:outputLabel styleClass="TABLE_LABEL"
			value="#{pages$financialAspectsPopup.monthlyRevenue}"
			style="font-weight:bold;color:red;" />

		<h:outputLabel styleClass="TABLE_LABEL"
			value="#{msg['assetwiserevenue.totalAnnualIncome']}:"
			style="font-weight:bold;" />
		<h:outputLabel styleClass="TABLE_LABEL"
			value="#{pages$financialAspectsPopup.annualRevenue}"
			style="font-weight:bold;color:red;" />

	</t:panelGrid>
</t:div>

<h:outputLabel styleClass="TABLE_LABEL"
	value="#{msg['researchFormBenef.monthlyExpenses']}"
	style="font-weight:bold;" />


<t:div styleClass="contentDiv"
	style="width: 95%;overflow-y:hidden;overflow-x:hidden;"
	id="lastMonthlyExpenseDivId">

	<t:dataTable width="100%" id="lastMonthlyExpenseTableId"
		rows="#{pages$financialAspectsPopup.paginatorRows}"
		value="#{pages$financialAspectsPopup.view.lastMonthlyExpense}"
		preserveDataModel="false" preserveSort="false"
		var="lastMonthlyExpense" rowClasses="row1,row2" rules="all"
		renderedIfEmpty="true">

		<t:column sortable="false">
			<f:facet name="header">
				<t:outputText style="white-space: normal;"
					value="#{msg['researchFormBenef.food']}" />
			</f:facet>
			<h:inputText onchange="calculateMonthlyExpense(this);"
				style="width:100%;" value="#{lastMonthlyExpense.food}"></h:inputText>
		</t:column>

		<t:column sortable="false">
			<f:facet name="header">
				<t:outputText style="white-space: normal;"
					value="#{msg['researchFormBenef.stationary']}" />
			</f:facet>
			<h:inputText style="width:100%;"
				onchange="calculateMonthlyExpense(this);"
				value="#{lastMonthlyExpense.stationary}"></h:inputText>
		</t:column>

		<t:column sortable="false">
			<f:facet name="header">
				<t:outputText value="#{msg['researchFormBenef.telephone']}" />
			</f:facet>
			<h:inputText style="width:100%;"
				onchange="calculateMonthlyExpense(this);"
				value="#{lastMonthlyExpense.telephone}"></h:inputText>
		</t:column>

		<t:column sortable="false">
			<f:facet name="header">
				<t:outputText value="#{msg['researchFormBenef.electricity']}" />
			</f:facet>
			<h:inputText style="width:100%;"
				onchange="calculateMonthlyExpense(this);"
				value="#{lastMonthlyExpense.electricity}"></h:inputText>
		</t:column>

		<t:column sortable="false">
			<f:facet name="header">
				<t:outputText value="#{msg['researchFormBenef.emergency']}" />
			</f:facet>
			<h:inputText style="width:100%;"
				onchange="calculateMonthlyExpense(this);"
				value="#{lastMonthlyExpense.emergency}"></h:inputText>
		</t:column>

		<t:column sortable="false">
			<f:facet name="header">
				<t:outputText value="#{msg['researchFormBenef.servant']}" />
			</f:facet>
			<h:inputText style="width:100%;"
				onchange="calculateMonthlyExpense(this);"
				value="#{lastMonthlyExpense.servant}"></h:inputText>
		</t:column>

		<t:column sortable="false">
			<f:facet name="header">
				<t:outputText value="#{msg['researchFormBenef.clothes']}" />
			</f:facet>
			<h:inputText style="width:100%;"
				onchange="calculateMonthlyExpense(this);"
				value="#{lastMonthlyExpense.clothes}"></h:inputText>
		</t:column>

		<t:column sortable="false">
			<f:facet name="header">
				<t:outputText value="#{msg['researchFormBenef.personalExp']}" />
			</f:facet>
			<h:inputText style="width:100%;"
				onchange="calculateMonthlyExpense(this);"
				value="#{lastMonthlyExpense.personalExp}"></h:inputText>
		</t:column>

		<t:column sortable="false">
			<f:facet name="header">
				<t:outputText value="#{msg['researchFormBenef.mobile']}" />
			</f:facet>
			<h:inputText style="width:100%;"
				onchange="calculateMonthlyExpense(this);"
				value="#{lastMonthlyExpense.mobile}"></h:inputText>
		</t:column>

		<t:column sortable="false">
			<f:facet name="header">
				<t:outputText value="#{msg['researchFormBenef.petrol']}" />
			</f:facet>
			<h:inputText style="width:100%;"
				onchange="calculateMonthlyExpense(this);"
				value="#{lastMonthlyExpense.petrol}"></h:inputText>
		</t:column>

		<t:column sortable="false">
			<f:facet name="header">
				<t:outputText value="#{msg['researchFormBenef.entertainment']}" />
			</f:facet>
			<h:inputText style="width:100%;"
				onchange="calculateMonthlyExpense(this);"
				value="#{lastMonthlyExpense.entertainment}"></h:inputText>
		</t:column>

		<t:column sortable="false">
			<f:facet name="header">
				<t:outputText value="#{msg['researchFormBenef.total']}" />
			</f:facet>
			<h:inputText id="totalMonthlyExpenseId" style="width:100%;"
				readonly="true" styleClass="READONLY"
				value="#{lastMonthlyExpense.total}"></h:inputText>
		</t:column>
	</t:dataTable>
</t:div>



<t:collapsiblePanel value="true" title="testTitle" var="test2collapsed">
	<f:facet name="header">
		<t:div style="margin-bottom:5px;">
			<h:outputText value="" />
			<t:headerLink immediate="true">
				<h:outputText
					value="#{msg['financialAspects.monthlyExpense.detail']}"
					rendered="#{test2collapsed}" />
				<h:outputText
					value="#{msg['financialAspects.monthlyExpense.detail.hide']}"
					rendered="#{!test2collapsed}" />
			</t:headerLink>
		</t:div>
	</f:facet>
	<f:facet name="closedContent">
		<h:panelGroup>
		</h:panelGroup>
	</f:facet>
	<t:div styleClass="contentDiv"
		style="width: 95%;overflow-y:hidden;overflow-x:hidden;">
		<t:dataTable width="100%"
			rows="#{pages$financialAspectsPopup.paginatorRows}"
			value="#{pages$financialAspectsPopup.view.allMonthlyExpense}"
			preserveDataModel="false" preserveSort="false"
			var="allMonthlyExpense" rowClasses="row1,row2" rules="all"
			renderedIfEmpty="true">

			<t:column id="monthlyEXPdate" width="10%" sortable="false"
				style="white-space: normal;">
				<f:facet name="header">
					<t:outputText value="#{msg['commons.date']}" />
				</f:facet>
				<t:outputText value="#{allMonthlyExpense.createdOn}"
					style="white-space: normal;" styleClass="A_LEFT" />
			</t:column>

			<t:column sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['researchFormBenef.food']}" />
				</f:facet>
				<h:outputText value="#{allMonthlyExpense.food}"></h:outputText>
			</t:column>

			<t:column sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['researchFormBenef.stationary']}" />
				</f:facet>
				<h:outputText value="#{allMonthlyExpense.stationary}"></h:outputText>
			</t:column>

			<t:column sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['researchFormBenef.telephone']}" />
				</f:facet>
				<h:outputText value="#{allMonthlyExpense.telephone}"></h:outputText>
			</t:column>

			<t:column sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['researchFormBenef.electricity']}" />
				</f:facet>
				<h:outputText value="#{allMonthlyExpense.electricity}"></h:outputText>
			</t:column>

			<t:column sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['researchFormBenef.emergency']}" />
				</f:facet>
				<h:outputText value="#{allMonthlyExpense.emergency}"></h:outputText>
			</t:column>

			<t:column sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['researchFormBenef.servant']}" />
				</f:facet>
				<h:outputText value="#{allMonthlyExpense.servant}"></h:outputText>
			</t:column>

			<t:column sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['researchFormBenef.clothes']}" />
				</f:facet>
				<h:outputText value="#{allMonthlyExpense.clothes}"></h:outputText>
			</t:column>

			<t:column sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['researchFormBenef.personalExp']}" />
				</f:facet>
				<h:outputText value="#{allMonthlyExpense.personalExp}"></h:outputText>
			</t:column>

			<t:column sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['researchFormBenef.mobile']}" />
				</f:facet>
				<h:outputText value="#{allMonthlyExpense.mobile}"></h:outputText>
			</t:column>

			<t:column sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['researchFormBenef.petrol']}" />
				</f:facet>
				<h:outputText value="#{allMonthlyExpense.petrol}"></h:outputText>
			</t:column>

			<t:column sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['researchFormBenef.entertainment']}" />
				</f:facet>
				<h:outputText value="#{allMonthlyExpense.entertainment}"></h:outputText>
			</t:column>

			<t:column sortable="false">
				<f:facet name="header">
					<t:outputText value="#{msg['researchFormBenef.total']}" />
				</f:facet>
				<h:outputText value="#{allMonthlyExpense.total}"></h:outputText>
			</t:column>

		</t:dataTable>
	</t:div>
</t:collapsiblePanel>
