<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html style="overflow:hidden;" dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<script language="javascript" type="text/javascript">
		function openLoadReportPopup(pageName) 
	{
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}	
	function closePopup()
	{
		window.close();
	}
	
	function onChkChangeComplete()
    {
      document.getElementById('replaceChequeForm:disablingDiv').style.display='none';
    }
    function onProcessStart()
	{
	    document.getElementById('replaceChequeForm:disablingDiv').style.display='block';
		return true;
	}
	function onProcessComplete()
	{
	 document.getElementById('replaceChequeForm:disablingDiv').style.display='none';
	}
	function onChkChangeStart( changedChkBox, error, errorMsg   )
	{
	  if( changedChkBox.checked  )
	  {
	    if( confirmRepalceChequedaysExceed( error, errorMsg  ) )
	    {
	     document.getElementById('replaceChequeForm:disablingDiv').style.display='block';
		 changedChkBox.nextSibling.onclick();
	    }
	    else
	    {
	      changedChkBox.checked = !changedChkBox.checked; 
	    }
	  }
	  else if ( !changedChkBox.checked  )
	  {
	    document.getElementById('replaceChequeForm:disablingDiv').style.display='block';
		changedChkBox.nextSibling.onclick();
	  }
	}
    function submitForm()
    {
         document.getElementById('ReceivePropertyForm').submit();
    }

	function  showPersonPopup()
	{
	 var screen_width = 1024;
	 var screen_height = 470;
	 window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup&SELECT_BLACK_LIST=true','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
    {
      document.getElementById("replaceChequeForm:hdnPersonId").value=personId;
      document.getElementById("replaceChequeForm:hdnCellNo").value=cellNumber;
      document.getElementById("replaceChequeForm:hdnPersonName").value=personName;
      document.getElementById("replaceChequeForm:hdnPersonType").value=hdnPersonType;
      document.getElementById("replaceChequeForm:hdnIsCompany").value=isCompany;
      document.forms[0].submit();
	}
	
		
</script>
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">

			<c:choose>
				<c:when test="${! pages$ReplaceCheque.pageModePopup}">
					<div class="containerDiv">
				</c:when>
			</c:choose>


			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<c:choose>
					<c:when test="${!pages$ReplaceCheque.pageModePopup}">
						<tr>

							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>

						</tr>
					</c:when>
				</c:choose>
				<tr width="100%">
					<c:choose>
						<c:when test="${!pages$ReplaceCheque.pageModePopup}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp"  />
							</td>
						</c:when>
					</c:choose>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99.2%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['replaceCheque.Heading']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>

						<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
							cellspacing="0" border="0" height="460px">
							<tr valign="top">
								<td height="70%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" valign="top" nowrap="nowrap">

									<h:form id="replaceChequeForm" enctype="multipart/form-data">
										<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
										<h:inputHidden id="hdnPersonId"
											value="#{pages$ReplaceCheque.hdnPersonId}"></h:inputHidden>
										<h:inputHidden id="hdnPersonType"
											value="#{pages$ReplaceCheque.hdnPersonType}"></h:inputHidden>
										<h:inputHidden id="hdnPersonName"
											value="#{pages$ReplaceCheque.hdnPersonName}"></h:inputHidden>
										<h:inputHidden id="hdnCellNo"
											value="#{pages$ReplaceCheque.hdnCellNo}"></h:inputHidden>
										<h:inputHidden id="hdnIsCompany"
											value="#{pages$ReplaceCheque.hdnIsCompany}"></h:inputHidden>
										<h:inputHidden id="hdnSelectCheque"
											value="#{pages$ReplaceCheque.selectChequeDetail}"></h:inputHidden>
										<h:inputHidden id="hdnRequestId"
											value="#{pages$ReplaceCheque.requestId}"></h:inputHidden>


										<div>
											<table border="0" class="layoutTable"
												style="margin-left: 15px; margin-right: 15px;">
												<tr>
													<td>
														<h:outputText id="successMsg_2"
															value="#{pages$ReplaceCheque.successMessages}"
															escape="false" styleClass="INFO_FONT" />
														<h:outputText id="errorMsg_1"
															value="#{pages$ReplaceCheque.errorMessages}"
															escape="false" styleClass="ERROR_FONT" />
													</td>
												</tr>
											</table>
										</div>


										<div class="SCROLLABLE_SECTION" style="height: 350px;">
											<div class="MARGIN" style="width: 95%; height: 48%;">
												<div style="height: 300px;">

													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>

													<rich:tabPanel
														binding="#{pages$ReplaceCheque.richTabPanel}"
														style="HEIGHT: 300px;width: 100%">
														
														<rich:tab id="applicationTab"
															label="#{msg['commons.tab.applicationDetails']}">
															<%@ include file="ApplicationDetails.jsp"%>
														</rich:tab>
														<rich:tab id="contractDetailTab"
															label="#{msg['contract.Contract.Details']}">
															<t:div id="divContract" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid cellpadding="1px" width="100%"
																	cellspacing="3px" styleClass="TAB_DETAIL_SECTION_INNER"
																	columns="4">
																	<t:panelGroup>
																		<t:panelGrid columns="2" cellpadding="1"
																			cellspacing="5">

																			<h:outputText styleClass="LABEL"
																				value="#{msg['contract.contractNumber']}" />
																			<t:panelGroup>

																				<h:inputText styleClass="READONLY" readonly="true"
																					binding="#{pages$ReplaceCheque.contractNoText}"
																					style="width: 186px; height: 18px"></h:inputText>
																				<h:commandLink
																					action="#{pages$ReplaceCheque.btnContract_Click}">
																					<h:graphicImage style="padding-left:4px;"
																						title="#{msg['commons.view']}"
																						url="../resources/images/app_icons/Lease-contract.png" />
																				</h:commandLink>

																			</t:panelGroup>


																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['contract.tenantName']}" />

																			<t:panelGroup>
																				<h:inputText styleClass="READONLY" readonly="true"
																					binding="#{pages$ReplaceCheque.tenantNameText}"
																					style="width: 186px; height: 18px"></h:inputText>
																				<h:commandLink
																					action="#{pages$ReplaceCheque.btnTenant_Click}">
																					<h:graphicImage style="padding-left:4px;"
																						title="#{msg['commons.view']}"
																						url="../resources/images/app_icons/Tenant.png" />
																				</h:commandLink>
																			</t:panelGroup>

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['contract.startDate']}" />
																			<h:inputText styleClass="READONLY" readonly="true"
																				binding="#{pages$ReplaceCheque.contractStartDateText}"
																				style="width: 186px; height: 18px"></h:inputText>

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['cancelContract.tab.unit.propertyname']}" />
																			<h:inputText styleClass="READONLY" readonly="true"
																				binding="#{pages$ReplaceCheque.txtpropertyName}"
																				style="width: 186px; height: 18px"></h:inputText>

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['unit.unitNumber']}" />
																			<h:inputText styleClass="READONLY" readonly="true"
																				binding="#{pages$ReplaceCheque.txtunitRefNum}"
																				style="width: 186px; height: 18px"></h:inputText>

																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['unit.rentValue']}" />
																			<h:inputText styleClass="READONLY" readonly="true"
																				binding="#{pages$ReplaceCheque.txtUnitRentValue}"
																				style="width: 186px; height: 18px"></h:inputText>
																			<h:outputLabel styleClass="LABEL"
																			  rendered="#{pages$ReplaceCheque.showProcedureType}"	
																			  value="#{msg['feeConfiguration.procedureType']}" />
																			<h:selectOneMenu id="cboProcedureType"   disabled="true" readonly="true"
																				rendered="#{pages$ReplaceCheque.showProcedureType}"
																				value="#{pages$ReplaceCheque.selectedProcedureType}">
																				<f:selectItem itemLabel="#{msg['procedureType.replaceReceivedCheque']}"  itemValue="1"/>
																				<f:selectItem itemLabel="#{msg['procedureType.payBounceCheque']}" itemValue="2" />
																			</h:selectOneMenu>
																		</t:panelGrid>
																	</t:panelGroup>


																	<t:div
																		style="padding-bottom:21px;#padding-bottom:25px;">
																		<t:panelGroup>
																			<t:panelGrid columns="2" cellpadding="1"
																				cellspacing="5">

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.contractType']}" />
																				<h:inputText styleClass="READONLY" readonly="true"
																					binding="#{pages$ReplaceCheque.contractTypeText}"
																					style="width: 186px; height: 18px"></h:inputText>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['transferContract.oldTenant.Number']}" />
																				<h:inputText styleClass="READONLY"
																					style="width: 186px; height: 18px" readonly="true"
																					binding="#{pages$ReplaceCheque.tenantNumberType}"></h:inputText>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.endDate']}" />
																				<h:inputText styleClass="READONLY" readonly="true"
																					binding="#{pages$ReplaceCheque.contractEndDateText}"
																					style="width: 186px; height: 18px"></h:inputText>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['property.type']}" />
																				<h:inputText styleClass="READONLY" readonly="true"
																					binding="#{pages$ReplaceCheque.txtpropertyType}"
																					style="width: 186px; height: 18px"></h:inputText>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.replaceCheque.unitType']}" />
																				<h:inputText styleClass="READONLY" readonly="true"
																					binding="#{pages$ReplaceCheque.txtUnitType}"
																					style="width: 186px; height: 18px"></h:inputText>


																			</t:panelGrid>
																		</t:panelGroup>
																	</t:div>
																</t:panelGrid>
															</t:div>
														</rich:tab>

														<rich:tab id="chequeDetailTab"
															label="#{msg['common.tab.chequeDetail']}">

															<t:div id="chequeDetailDiv" styleClass="contentDiv"
																style="width:98%">

																<t:dataTable id="chequeDetaildataTable"
																	value="#{pages$ReplaceCheque.bounceChequeList}"
																	binding="#{pages$ReplaceCheque.dataTableChequeDetail}"
																	rows="10" preserveDataModel="true" preserveSort="false"
																	var="chequeDetailDataItem" rowClasses="row1,row2"
																	rules="all" renderedIfEmpty="true" width="100%">


																	<t:column id="Chequeselect"
																		rendered="#{pages$ReplaceCheque.selectChequeDetail}"
																		style="white-space:normal;">
																		<f:facet name="header">
																			<t:outputText id="selectTxt"
																				value="#{msg['commons.select']}" />
																		</f:facet>
																		<h:selectBooleanCheckbox id="selectChq"
																			style="white-space:normal;"
																			value="#{chequeDetailDataItem.selected}"
																			disabled="#{chequeDetailDataItem.enableDisableCheckBox}"
																			readonly="#{chequeDetailDataItem.readOnly}"
																			
																			onclick="onChkChangeStart(this,'#{chequeDetailDataItem.isExceedMaxDaysForWithDrawal}','#{msg['chequeList.msg.durationNotWithinLimit']}');"
																			rendered="#{chequeDetailDataItem.renderedCheckBox}">

																		</h:selectBooleanCheckbox>
																		<a4j:commandLink id="chkChanged"
																			onbeforedomupdate="javascript:onChkChangeComplete();"
																			action="#{pages$ReplaceCheque.chkSelectChq_Changed}" />
																	</t:column>
																	
																	<t:column id="ChequePaymentNumber"
																		style="white-space:normal;">
																		<f:facet name="header">
																			<t:outputText id="ac182"
																				value="#{msg['chequeList.paymentNumber']}" />
																		</f:facet>
																		<t:outputText id="ac181" styleClass="A_LEFT"
																			value="#{chequeDetailDataItem.paymentNumber}" />
																	</t:column>
																	

																	<t:column id="ChequeReceiptNumber"
																		style="white-space:normal;">
																		<f:facet name="header">
																			<t:outputText id="ac201"
																				value="#{msg['chequeList.receiptNumber']}" />
																		</f:facet>
																		<t:outputText id="ac191" styleClass="A_LEFT"
																			value="#{chequeDetailDataItem.receiptNumber}" />
																	</t:column>


																	<t:column id="chequeNumberEn"
																		style="white-space:normal;">
																		<f:facet name="header">
																			<t:outputText id="ac171"
																				value="#{msg['bouncedChequesList.chequeNumberCol']}" />
																		</f:facet>
																		<t:outputText id="ac161" style="white-space:normal;"
																			styleClass="A_LEFT" title=""
																			value="#{chequeDetailDataItem.chequeNumber}" />
																	</t:column>

																	<t:column id="chequeNumberAr"
																		style="white-space:normal;" rendered="false">
																		<f:facet name="header">
																			<t:outputText id="ac151"
																				value="#{msg['bouncedChequesList.dueDateCol']}" />
																		</f:facet>
																		<t:outputText id="ac141" style="white-space:normal;"
																			styleClass="A_LEFT" title=""
																			value="#{chequeDetailDataItem.methodReferenceDate}" />
																	</t:column>

																	<t:column id="chequeAmount" style="white-space:normal;">
																		<f:facet name="header">
																			<t:outputText id="descHead1"
																				value="#{msg['chequeList.amount']}" />
																		</f:facet>
																		<t:outputText id="descText1"
																			style="white-space:normal;" styleClass="A_LEFT"
																			title=""
																			value="#{chequeDetailDataItem.paymentDetailAmount}" />
																	</t:column>

																	<t:column id="ChequeStatusEn"
																		style="white-space:normal;"
																		rendered="#{pages$ReplaceCheque.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac111"
																				value="#{msg['commons.status']}" />
																		</f:facet>
																		<t:outputText id="ac101" style="white-space:normal;"
																			styleClass="A_LEFT" title=""
																			value="#{chequeDetailDataItem.paymentDetailStatusEn}" />
																	</t:column>
																	<t:column id="ChequeStatusAr"
																		style="white-space:normal;"
																		rendered="#{pages$ReplaceCheque.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac9"
																				value="#{msg['commons.status']}" />
																		</f:facet>
																		<t:outputText id="ac8" style="white-space:normal;"
																			styleClass="A_LEFT" title=""
																			value="#{chequeDetailDataItem.paymentDetailStatusAr}" />
																	</t:column>
																	<t:column id="ChequeAccount"
																		style="white-space:normal;">
																		<f:facet name="header">
																			<t:outputText id="ac711"
																				value="#{msg['feeConfiguration.accountNo']}" />
																		</f:facet>
																		<t:outputText id="ac611" style="white-space:normal;"
																			styleClass="A_LEFT" title=""
																			value="#{chequeDetailDataItem.accountNumber}" />
																	</t:column>

																	<t:column id="ChequeDescription"
																		style="white-space:normal;">
																		<f:facet name="header">
																			<t:outputText id="ac71"
																				value="#{msg['settlement.payment.paymentmethod']}" />
																		</f:facet>
																		<t:outputText id="ac61" style="white-space:normal;"
																			styleClass="A_LEFT" title=""
																			value="#{chequeDetailDataItem.paymentMethodDescription}" />
																	</t:column>
																	
																	
																	
																	<t:column id="ChequeMaturityDate"
																		style="white-space:normal;">
																		<f:facet name="header">
																			<t:outputText id="ChequeMaturityDate2"
																				value="#{msg['mems.inheritanceFile.fieldLabel.maturityDate']}" />
																		</f:facet>
																		<t:outputText id="ChequeMaturityDate3" style="white-space:normal;"
																			styleClass="A_LEFT" title=""
																			value="#{chequeDetailDataItem.paymentDueOn}">
																			<f:convertDateTime timeZone="#{pages$ReplaceCheque.localTimeZone}"
																				pattern="#{pages$ReplaceCheque.dateFormat}" />
																			</t:outputText>	
																	</t:column>

																</t:dataTable>
															</t:div>
															 <t:div id="pagingDivChqDetail" styleClass="contentDivFooter"
																style="width:99.1%;">

																<t:dataScroller id="scrollerChqDetail"
																	for="chequeDetaildataTable" paginator="true" fastStep="1"
																	paginatorMaxPages="15" immediate="false"
																	paginatorTableClass="paginator"
																	renderFacetsIfSinglePage="true"
																	pageIndexVar="pageNumberChequeDetail"
																	paginatorTableStyle="grid_paginator"
																	layout="singleTable"
																	paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																	paginatorActiveColumnStyle="font-weight:bold;"
																	paginatorRenderLinkForActive="false"
																	>
																	<f:facet name="first">
																		<t:graphicImage url="../#{path.scroller_first}"
																			id="lblFCD"></t:graphicImage>
																	</f:facet>

																	<f:facet name="fastrewind">
																		<t:graphicImage url="../#{path.scroller_fastRewind}"
																			id="lblFRCD"></t:graphicImage>
																	</f:facet>

																	<f:facet name="fastforward">
																		<t:graphicImage url="../#{path.scroller_fastForward}"
																			id="lblFFCD"></t:graphicImage>
																	</f:facet>

																	<f:facet name="last">
																		<t:graphicImage url="../#{path.scroller_last}"
																			id="lblLCD"></t:graphicImage>
																	</f:facet>


																</t:dataScroller>

															</t:div>
                                                                                     
															
															<t:div>
																<t:div id="cmdCheque" styleClass="A_RIGHT"
																	style="padding-top:7px;padding-left:3px">
																	<h:commandButton id="cmdChequeReplaced"
																		rendered="false"
																		value="#{msg['replaceCheque.replacePayment']}"
																		styleClass="BUTTON"
																		binding="#{pages$ReplaceCheque.replacePaymentButton}"
																		action="#{pages$ReplaceCheque.replace_Payment}"
																		style="width: 119px" />
																	<h:commandButton id="cmdMarkReturned" rendered="false"
																		value="#{msg['replace.btn.MarkReturned']}"
																		styleClass="BUTTON"
																		action="#{pages$ReplaceCheque.cheque_Returned}"
																		style="width: 119px" />
																</t:div>
															</t:div>
														</rich:tab>
														<rich:tab id="PaymentTab"
															label="#{msg['cancelContract.tab.payments']}">

															<t:div id="divtt" styleClass="contentDiv"
																style="width:97%">
																<t:dataTable id="ttbb" width="100%" rows="10"
																	value="#{pages$ReplaceCheque.paymentSchedules}"
																	binding="#{pages$ReplaceCheque.dataTablePaymentSchedule}"
																	preserveDataModel="false" preserveSort="false"
																	var="paymentScheduleDataItem" rowClasses="row1,row2"
																	renderedIfEmpty="true">


																	<t:column id="col_PaymentNumber" rendered="#{true || pages$ReplaceCheque.paymentNumderCol}">
																		<f:facet name="header">
																			<t:outputText id="ac20"
																				value="#{msg['paymentSchedule.paymentNumber']}" />
																		</f:facet>
																		<t:outputText id="ac19" styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentNumber}" />
																	</t:column>
																	
																	
																	<t:column id="col_ReceiptNumber">
																		<f:facet name="header">
																			<t:outputText id="ac21"
																				value="#{msg['paymentSchedule.ReceiptNumber']}" />
																		</f:facet>
																		<t:outputText id="ac22" styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentReceiptNumber}" />
																	</t:column>


																	<t:column id="colTypeEnt"
																		rendered="#{pages$ReplaceCheque.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac17"
																				value="#{msg['paymentSchedule.paymentType']}" />
																		</f:facet>
																		<t:outputText id="ac16" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.typeEn}" />
																	</t:column>
																	<t:column id="colTypeArt"
																		rendered="#{pages$ReplaceCheque.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac15"
																				value="#{msg['paymentSchedule.paymentType']}" />
																		</f:facet>
																		<t:outputText id="ac14" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.typeAr}" />
																	</t:column>

																	<t:column id="colDesc">
																		<f:facet name="header">
																			<t:outputText id="descHead"
																				value="#{msg['renewContract.description']}" />
																		</f:facet>
																		<t:outputText id="descText" styleClass="A_LEFT"
																			title=""
																			value="#{paymentScheduleDataItem.description}" />
																	</t:column>


																	<t:column id="colDueOnt">
																		<f:facet name="header">
																			<t:outputText id="ac13"
																				value="#{msg['paymentSchedule.paymentDueOn']}" />
																		</f:facet>
																		<t:outputText id="ac12" styleClass="A_LEFT"
																			value="#{paymentScheduleDataItem.paymentDueOn}">
																			<f:convertDateTime timeZone="#{pages$ReplaceCheque.localTimeZone}"
																				pattern="#{pages$ReplaceCheque.dateFormatForDataTable}" />
																		</t:outputText>
																	</t:column>

																	<t:column id="colStatusEnt"
																		rendered="#{pages$ReplaceCheque.isEnglishLocale}">


																		<f:facet name="header">
																			<t:outputText id="ac11"
																				value="#{msg['commons.status']}" />
																		</f:facet>
																		<t:outputText id="ac10" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.statusEn}" />
																	</t:column>
																	<t:column id="colStatusArt"
																		rendered="#{pages$ReplaceCheque.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac9"
																				value="#{msg['commons.status']}" />
																		</f:facet>
																		<t:outputText id="ac8" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.statusAr}" />
																	</t:column>

																	<t:column id="colModeEnt"
																		rendered="#{pages$ReplaceCheque.isEnglishLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac7"
																				value="#{msg['paymentSchedule.paymentMode']}" />
																		</f:facet>
																		<t:outputText id="ac6" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentModeEn}" />
																	</t:column>
																	<t:column id="colModeArt"
																		rendered="#{pages$ReplaceCheque.isArabicLocale}">
																		<f:facet name="header">
																			<t:outputText id="ac5"
																				value="#{msg['paymentSchedule.paymentMode']}" />
																		</f:facet>
																		<t:outputText id="ac4" styleClass="A_LEFT" title=""
																			value="#{paymentScheduleDataItem.paymentModeAr}" />
																	</t:column>


																	<t:column id="colAmountt">
																		<f:facet name="header">
																			<t:outputText id="ac1"
																				value="#{msg['paymentSchedule.amount']}" />
																		</f:facet>
																		<t:outputText id="ac2" styleClass="A_RIGHT_NUM"
																			value="#{paymentScheduleDataItem.amount}" />
																	</t:column>

																	<t:column id="col8">
																		<f:facet name="header">
																			<t:outputText value="#{msg['commons.action']}" />
																		</f:facet>
                                                                        <h:commandLink
																			action="#{pages$ReplaceCheque.btnEditPaymentSchedule_Click}"
																			rendered="#{pages$ReplaceCheque.editPaymentEnable}">
																			<h:graphicImage id="editPayments"
																				title="#{msg['commons.edit']}"
																				url="../resources/images/edit-icon.gif"
																				rendered="#{paymentScheduleDataItem.isReceived == 'N' && paymentScheduleDataItem.isSystem == '1'}" />
																		</h:commandLink>
																		<h:commandLink
																			action="#{pages$ReplaceCheque.openPopupToSplitAmount}"
																			rendered="#{pages$ReplaceCheque.editPaymentEnable}">
																			<h:graphicImage id="splitPayments"
																				title="#{msg['replaceChequeLabel.btnSplit']}"
																				url="../resources/images/app_icons/Add-fee.png"
																				rendered="#{paymentScheduleDataItem.isReceived == 'N' }" />
																		</h:commandLink>
																		<h:commandLink
																			action="#{pages$ReplaceCheque.btnPrintPaymentSchedule_Click}">
																			<h:graphicImage id="printPayments"
																				title="#{msg['commons.print']}"
																				url="../resources/images/app_icons/print.gif"
																				rendered="#{paymentScheduleDataItem.isReceived == 'Y'}" />
																		</h:commandLink>
																		
																		<h:commandLink action="#{pages$ReplaceCheque.btnViewPaymentDetails_Click}">
																			<h:graphicImage id="viewPayments" 
																				title="#{msg['commons.group.permissions.view']}" 
																				url="../resources/images/detail-icon.gif" />
																		</h:commandLink> 
																		
																		

																	</t:column>


																</t:dataTable>
															</t:div>
															

                                                             <t:div id="pagingDivPaySch" styleClass="contentDivFooter"
																style="width:98%;">

																<t:dataScroller id="scrollerPaySch"
																	for="ttbb" paginator="true" fastStep="1"
																	paginatorMaxPages="15" immediate="false"
																	paginatorTableClass="paginator"
																	renderFacetsIfSinglePage="true"
																	pageIndexVar="pageNumber"
																	paginatorTableStyle="grid_paginator"
																	layout="singleTable"
																	paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																	paginatorActiveColumnStyle="font-weight:bold;"
																	paginatorRenderLinkForActive="false"
																	>
																	<f:facet name="first">
																		<t:graphicImage url="../#{path.scroller_first}"
																			id="lblFPaySch"></t:graphicImage>
																	</f:facet>

																	<f:facet name="fastrewind">
																		<t:graphicImage url="../#{path.scroller_fastRewind}"
																			id="lblFRPaySch"></t:graphicImage>
																	</f:facet>

																	<f:facet name="fastforward">
																		<t:graphicImage url="../#{path.scroller_fastForward}"
																			id="lblFFPaySch"></t:graphicImage>
																	</f:facet>

																	<f:facet name="last">
																		<t:graphicImage url="../#{path.scroller_last}"
																			id="lblLPaySch"></t:graphicImage>
																	</f:facet>


																</t:dataScroller>

															</t:div>
                                                                                     

<t:div>
																<t:div id="cmdDiv" styleClass="A_RIGHT"
																	style="padding-top:7px;padding-left:3px">
																	<h:commandButton id="btnCollectPayment"
																		value="#{msg['settlement.actions.collectpayment']}"
																		styleClass="BUTTON"
																		binding="#{pages$ReplaceCheque.btnCollectPayment}"
																		action="#{pages$ReplaceCheque.openReceivePaymentsPopUp}"
																		style="width: 119px"
																		onclick="javaScript:onProcessStart();" />
																</t:div>


															</t:div>
														</rich:tab>
														<rich:tab id="requestHistoryTab"
															label="#{msg['commons.tab.requestHistory']}"
															action="#{pages$ReplaceCheque.tabRequestHistory_Click}">
															<t:div>
																<%@ include file="../pages/requestTasks.jsp"%>
															</t:div>
														</rich:tab>

														<rich:tab id="attachmentTab"
															label="#{msg['commons.attachmentTabHeading']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}">
															<%@  include file="notes/notes.jsp"%>
														</rich:tab>
													</rich:tabPanel>

													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																	class="TAB_PANEL_MID" style="width: 100%;" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
												</div>
												<div>




												</div>
											</div>
										</div>
										<table width="97%">
											<tr>
												<td class="BUTTON_TD" colspan="4">


													<h:commandButton styleClass="BUTTON" type="submit"
														style="width: 119px"
														binding="#{pages$ReplaceCheque.saveButton}"
														action="#{pages$ReplaceCheque.saveRequest}"
														value="#{msg['commons.saveButton']}"
														onclick="javaScript:onProcessStart();" />

													<h:commandButton styleClass="BUTTON" type="submit"
														style="width: 119px"
														binding="#{pages$ReplaceCheque.sendBtn}"
														action="#{pages$ReplaceCheque.send}"
														value="#{msg['replace.btn.send']}"
														onclick="javaScript:onProcessStart();" />
													<h:commandButton styleClass="BUTTON" type="submit"
														style="width: 119px"
														binding="#{pages$ReplaceCheque.reviewBtn}"
														action="#{pages$ReplaceCheque.review}"
														value="#{msg['commons.review']}"
														onclick="javaScript:onProcessStart();" />	
													<h:commandButton styleClass="BUTTON" 
														style="width: auto"
														rendered="#{pages$ReplaceCheque.showWithdrawPrintButton}"
														action="#{pages$ReplaceCheque.printWithdrawlLetter}"
														onclick="if (!confirm('#{msg['confirmMsg.sureToPrintWithdrawl']}')) return false"
														value="#{msg['report.title.chequeWithdrawlReport']}"/>	
													<h:commandButton styleClass="BUTTON" type="submit"
														style="width: 119px"
														binding="#{pages$ReplaceCheque.approvedBtn}"
														action="#{pages$ReplaceCheque.approve}"
														value="#{msg['replace.btn.approved']}"
														onclick="javaScript:onProcessStart();" />

													<h:commandButton styleClass="BUTTON" type="submit"
														style="width: 119px"
														binding="#{pages$ReplaceCheque.rejectBtn}"
														action="#{pages$ReplaceCheque.reject}"
														value="#{msg['replace.btn.reject']}"
														onclick="javaScript:onProcessStart();" />
												<%--binding="#{pages$ReplaceCheque.completeBtn}" --%>
													<h:commandButton styleClass="BUTTON" type="submit"
														style="width: auto;"
														rendered="#{pages$ReplaceCheque.showReturnOldChequeButton}"
														action="#{pages$ReplaceCheque.complete}"
														value="#{msg['replaceCheque.returnedOldCheque']}"
														onclick="if (!confirm('#{msg['confirmMsg.sureToReturnChequeAndCompeteRequest']}')) return false" />

													<h:commandButton styleClass="BUTTON" type="submit"
														style="width: 119px"
														binding="#{pages$ReplaceCheque.cancelBtn}"
														action="#{pages$ReplaceCheque.replaceCancel}"
														value="#{msg['replace.btn.cancel']}"
														onclick="javaScript:onProcessStart();" />

													<h:commandButton styleClass="BUTTON" type="submit"
														style="width: 119px"
														binding="#{pages$ReplaceCheque.replacedBtn}"
														action="#{pages$ReplaceCheque.replace}"
														value="#{msg['replace.btn.chequeReplaced']}"
														onclick="javaScript:onProcessStart();" />

													<h:commandButton id="returnedChequeBtn" styleClass="BUTTON"
														type="submit" style="width: 119px"
														rendered="#{pages$ReplaceCheque.showReturnCheque}"
														action="#{pages$ReplaceCheque.returned}"
														value="#{msg['replace.btn.chequeReturned']}"
														onclick="javaScript:onProcessStart();" />

													<%-- old control binding
													binding="#{pages$ReplaceCheque.returedChequeBtn}"--%>
													<h:commandButton styleClass="BUTTON" type="submit"
														style="width: auto;"
														rendered="#{pages$ReplaceCheque.withdrawn}"
														action="#{pages$ReplaceCheque.receiveChequeFromBank}"
														value="#{msg['replaceCheque.label.receiveCheque']}"
														onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceed']}')) return false" />

													<h:commandButton styleClass="BUTTON" type="submit"
														style="width: 119px"
														binding="#{pages$ReplaceCheque.closeButton}"
														action="#{pages$ReplaceCheque.close}"
														value="#{msg['commons.closeButton']}"
														onclick="javaScript:onProcessStart();" />
													<h:commandButton styleClass="BUTTON" 
														style="width: 119px"
														rendered="#{pages$ReplaceCheque.pageModePopup}"
														value="#{msg['commons.closeButton']}"
														onclick="javaScript:window.close();" />
													

												</td>
											</tr>
										</table>

									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>


			<c:choose>
				<c:when test="${!pages$ReplaceCheque.pageModePopup}">
					</div >
				</c:when>
			</c:choose>


		</body>
	</html>
</f:view>