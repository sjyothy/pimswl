<%-- 
  - Author: Muhammad Shiraz	
  - Date: 03/07/2009
  - Copyright Notice:
  - @(#)
  - Description: Used for Adding, Updating and Viewing Revenue and Expense  
  --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}

		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
	function  receiveSubClassList()
	{
	  document.getElementById('frm:lnkReceiveAssetSubClass').onclick();
	}
	
	function openAssetSubClassManagePopup()
	{
	 var screen_width = 1024;
     var screen_height = 220;
     window.open('assetSubClassManage.jsf','_blank','width='+(screen_width-250)+',height='+(screen_height)+',left=120,top=150,scrollbars=yes,status=yes');
	}
	function closeWindow() 
	{
	 window.close();
	}
	function openPortfolioSearchPopup()
    {
      var screen_width = 990;
      var screen_height = 475;
      var popup_width = screen_width;
      var popup_height = screen_height;
      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
      window.open('portfolioSearch.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=no,status=no,resizable=yes,titlebar=no,dialog=yes');
    }
	function receiveSelectedPortfolio() 
    { 
     document.forms[0].submit();
    }
	
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
		</head>

		<!-- Header -->
		<body class="BODY_STYLE">
          <div class="containerDiv">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<c:choose>
						<c:when test="${! pages$revenueAndExpensesManage.isPopupViewOnlyMode}">
							<td colspan="2">
								<jsp:include page="header.jsp" />
							</td>
						</c:when>
					</c:choose>
				</tr>

				<tr>
					<c:choose>
						<c:when test="${! pages$revenueAndExpensesManage.isPopupViewOnlyMode}">
							<td class="divLeftMenuWithBody" width="17%">
								<jsp:include page="leftmenu.jsp" />
							</td>
						</c:when>
					</c:choose>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{pages$revenueAndExpensesManage.screenName}"   styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<table width="99.1%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="100%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:460px">

										<h:form id="frm" style="width:97%" enctype="multipart/form-data" >
											<table border="0" class="layoutTable">

												<tr>

													<td>
														<h:outputText escape="false" styleClass="ERROR_FONT" value="#{pages$revenueAndExpensesManage.errorMessages}" />
													    <h:outputText escape="false" styleClass="INFO_FONT" value="#{pages$revenueAndExpensesManage.successMessages}" />
													</td>
												</tr>
											</table>

											<div class="MARGIN" style="width: 98%">
												
												<table  id="table_1" cellpadding="0" cellspacing="0" width="100%">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_top_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_top_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
										        </table>
												
												<rich:tabPanel style="width:100%;height:235px;" headerSpacing="0">
												
														<!-- Revenue And Expenses Tab - Start -->
														<rich:tab label="#{msg['revenueAndExpensesManage.Detail.Tab.heading']}">
															<t:div  style="width:100%" styleClass="TAB_DETAIL_SECTION">
																<t:panelGrid id="followUpDetailsTable" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4">
																     
																    <h:outputLabel     styleClass="LABEL" value="#{msg['revenueAndExpensesManage.Portfolio']}:"></h:outputLabel>
																	<t:panelGroup>
																		 <h:inputText      id="txtPortfolio"
																		                   readonly="true" 
																		                   styleClass="READONLY" 
																		                   value="#{pages$revenueAndExpensesManage.expenseAndRevenueView.portfolioView.portfolioNumber}" 
																		                   style="width: 186px;" maxlength="20"  />
																		
																		<h:commandLink     action="#{pages$revenueAndExpensesManage.onAddPortfolio}" >
														                  <h:graphicImage  
														                                   id="populateIcon" 
														                                   style="padding-left:5px;"
														                                   rendered="#{!pages$revenueAndExpensesManage.isViewMode}" 
														                                   title="#{msg['commons.populate']}" 
														                                   url="../resources/images/magnifier.gif" />
														               </h:commandLink>                   
																  	</t:panelGroup>																	
																  	
																	<h:outputLabel    styleClass="LABEL" value="#{msg['revenueAndExpensesManage.TransactionRefNo']}:"></h:outputLabel>
																	<h:selectOneMenu   id="selectTranstionNo"
																	                   readonly="#{pages$revenueAndExpensesManage.isViewMode}" 
																	                   styleClass="#{pages$revenueAndExpensesManage.isViewReadOnly}"
																	                   value="#{pages$revenueAndExpensesManage.expenseAndRevenueView.orderId}" 
																	                   style="width: 192px;">
																		<f:selectItem  itemValue="" itemLabel="#{msg['commons.select']}" />
																		<f:selectItems value="#{pages$revenueAndExpensesManage.transtionNoList}" />
																	</h:selectOneMenu>
																  <h:panelGroup>	
																	<h:outputLabel     styleClass="mandatory" value="*"/>
																	<h:outputLabel     styleClass="LABEL" value="#{msg['revenueAndExpensesManage.status']}:"></h:outputLabel>
																  </h:panelGroup>	
																	<h:selectOneMenu   id="selectStatus"
																	                   readonly="#{pages$revenueAndExpensesManage.isViewMode}" 
																	                   styleClass="#{pages$revenueAndExpensesManage.isViewReadOnly}"
																	                   value="#{pages$revenueAndExpensesManage.expenseAndRevenueView.statusId}" 
																	                   style="width: 192px;">
																		<f:selectItem  itemValue="" itemLabel="#{msg['commons.select']}" />
																		<f:selectItems value="#{pages$ApplicationBean.revenueAndExpenseStatus}" />
																	</h:selectOneMenu>
																 <h:panelGroup>	
																	<h:outputLabel     styleClass="mandatory" value="*"/>	
																	<h:outputLabel     styleClass="LABEL" value="#{msg['revenueAndExpensesManage.TransactionDate']}:"></h:outputLabel>
																</h:panelGroup>	
																    <t:panelGroup>
											  	                                      <rich:calendar id="transactionDate" 
											  	                                       disabled="#{pages$revenueAndExpensesManage.isViewMode}"
											  	                                       inputClass="#{pages$revenueAndExpensesManage.isViewReadOnly}"
										                   	                           locale="#{pages$revenueAndExpensesManage.locale}"
										                   	                           datePattern="#{pages$revenueAndExpensesManage.dateFormat}"
										                   	                           value="#{pages$revenueAndExpensesManage.expenseAndRevenueView.transectionDate}" 
										                   	                           popup="true" 
										                   	                           showApplyButton="false" 
										                   	                           enableManualInput="false" 
										                   	                           cellWidth="24px" 
										                   	                           cellHeight="22px" />
											                       </t:panelGroup>
																	
																	<h:outputLabel     styleClass="LABEL" value="#{msg['revenueAndExpensesManage.TransactionTime']}:"></h:outputLabel>
																	<t:panelGroup>
																	  	<h:selectOneMenu id="TimeHH" 
																	  	                 style="width: 40px; " 
																	  	                 required="false" 
																	  	                 readonly="#{pages$revenueAndExpensesManage.isViewMode}" 
																	                     styleClass="#{pages$revenueAndExpensesManage.isViewReadOnly}" 
																				         value="#{pages$revenueAndExpensesManage.expenseAndRevenueView.hhTime}" tabindex="6">
																				         <f:selectItem itemValue="" itemLabel="hh" />
																				         <f:selectItems value="#{pages$ApplicationBean.hours}" />
																		</h:selectOneMenu>
																		<h:selectOneMenu id="TimeMM" 
																		                 style="width: 40px;" 
																		                 required="false" 
																		                 readonly="#{pages$revenueAndExpensesManage.isViewMode}" 
																	                     styleClass="#{pages$revenueAndExpensesManage.isViewReadOnly}" 
																				         value="#{pages$revenueAndExpensesManage.expenseAndRevenueView.mmTime}" tabindex="7">
																				         <f:selectItem itemValue="" itemLabel="mm" />
																				         <f:selectItems value="#{pages$ApplicationBean.minutes}" />
																		</h:selectOneMenu>
																	</t:panelGroup>
																
																    <h:outputLabel styleClass="LABEL" value="#{msg['revenueAndExpensesManage.marker']}:"></h:outputLabel>
																	<h:inputText       id="txtMarker"
																	                   readonly="#{pages$revenueAndExpensesManage.isViewMode}" 
																	                   styleClass="#{pages$revenueAndExpensesManage.isViewReadOnly}" 
																	                   value="#{pages$revenueAndExpensesManage.expenseAndRevenueView.marker}" 
																	                   style="width: 186px;" maxlength="20"  />
																<h:panelGroup>	
																	<h:outputLabel     styleClass="mandatory" value="*"/>	
																	<h:outputLabel     styleClass="LABEL" value="#{msg['revenueAndExpensesManage.ActualValue']}:"></h:outputLabel>
																  </h:panelGroup>
																	<h:inputText       id="txtActualValue"
																	                   readonly="#{pages$revenueAndExpensesManage.isViewMode}" 
																	                   styleClass="#{pages$revenueAndExpensesManage.isViewReadOnly}" 
																	                   value="#{pages$revenueAndExpensesManage.expenseAndRevenueView.strActualValue}" 
																	                   style="width: 186px;" maxlength="20"  />
																
																
																	<h:outputLabel     styleClass="LABEL" value="#{msg['revenueAndExpensesManage.RealizeDate']}:"></h:outputLabel>
																	<t:panelGroup>
											  	                                      <rich:calendar id="realizeDate" 
											  	                                       disabled="#{pages$revenueAndExpensesManage.isViewMode}"
											  	                                       inputClass="#{pages$revenueAndExpensesManage.isViewReadOnly}"
																	                   datePattern="#{pages$revenueAndExpensesManage.dateFormat}"
										                   	                           locale="#{pages$revenueAndExpensesManage.locale}"
										                   	                           value="#{pages$revenueAndExpensesManage.expenseAndRevenueView.realizeDate}"  
										                   	                           popup="true" 
										                   	                           showApplyButton="false" 
										                   	                           enableManualInput="false" 
										                   	                           cellWidth="24px" 
										                   	                           cellHeight="22px" />
											                       </t:panelGroup>
																
																    <h:outputLabel styleClass="LABEL" value="#{msg['revenueAndExpensesManage.description']}:"></h:outputLabel>
																	<h:inputTextarea   id="txtDescription"
																	                   readonly="#{pages$revenueAndExpensesManage.isViewMode}" 
																	                   styleClass="#{pages$revenueAndExpensesManage.isViewReadOnly}" 
																	                   value="#{pages$revenueAndExpensesManage.expenseAndRevenueView.description}"
																	                   style="width: 186px;" />              
																	                   
																                    	                                                                 
																</t:panelGrid>
															</t:div>
														</rich:tab>
														<!-- Revenue And Expenses Tab - End -->
														
																				
														                                                        
                                                        <!-- Attachments Tab - Start -->
														<rich:tab label="#{msg['contract.attachmentTabHeader']}">
															<%@  include file="attachment/attachment.jsp"%>
														</rich:tab>
														<!-- Attachments Tab - End -->
														
														<!-- Comments Tab - Start -->
                                                         <rich:tab id="commentsTab"
															label="#{msg['commons.commentsTabHeading']}">
															<%@ include file="notes/notes.jsp"%>
														</rich:tab>
														<!-- Comments Tab - End -->
																												
												</rich:tabPanel>
												
												<table cellpadding="0" cellspacing="0"  style="width:100%;">
												<tr>
												<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
												<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
												<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
												</tr>
												</table>
												
												<table cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td colspan="6" class="BUTTON_TD">
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.saveButton']}"
																	action="#{pages$revenueAndExpensesManage.onSave}"
																	rendered="#{! (pages$revenueAndExpensesManage.isPopupViewOnlyMode || pages$revenueAndExpensesManage.isViewMode)}">
															</h:commandButton>
														<!-- 	
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.reset']}"
																	action="#{pages$revenueAndExpensesManage.onReset}"
																	rendered="#{! (pages$revenueAndExpensesManage.isPopupViewOnlyMode || pages$revenueAndExpensesManage.isViewMode) && !pages$revenueAndExpensesManage.isEditMode}">
															</h:commandButton>															
														 -->	
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	action="#{pages$revenueAndExpensesManage.onCancel}"
																	rendered="#{! pages$revenueAndExpensesManage.isPopupViewOnlyMode}">
															</h:commandButton>	
															
															<h:commandButton styleClass="BUTTON"
																	value="#{msg['commons.cancel']}"
																	onclick="closeWindow();"
																	rendered="#{pages$revenueAndExpensesManage.isViewMode}">
															</h:commandButton>	
																																												
														</td>
													</tr>
												</table>
											</div>								
								
									    </h:form>
									</div>
								

									</div>
								</td>
							</tr>
						</table>

					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
           </div>
		</body>
	</html>
</f:view>
