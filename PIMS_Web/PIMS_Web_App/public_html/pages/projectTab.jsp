<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
	<script type="text/javascript">
		function showPropertySearchPopup()
		{
			var screen_width = 1024;
			var screen_height = 470;
			var popup_width = screen_width-30;
			var popup_height = screen_height;
			var leftPos = (screen_width-popup_width)/2 -5, topPos = (screen_height-popup_height)/2 - 20;
			var popup = window.open('propertyList.jsf?pageMode=MODE_SELECT_ONE_POPUP','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
			popup.focus();
		}
		
		function showPropertyViewPopup()
		{
			alert('Problem in viewing the property! Please contact support staff...');
		}
	</script>
<t:div style="width:100%" >
	<t:panelGrid id="projectDetailsTable" cellpadding="1px" width="100%" cellspacing="5px"  columns="4">
					
			        <h:outputLabel styleClass="LABEL" value="#{msg['project.projectNumber']}: " />
				    <h:inputText styleClass="INPUT_TEXT READONLY" id="txtProjectNum"  value="#{pages$projectTab.projectNumber}" readonly="true"></h:inputText>
			        <h:outputLabel styleClass="LABEL" value="#{msg['project.projectStatus']}: " />
					<h:inputText styleClass="INPUT_TEXT READONLY" id="txtProjectStatus" value="#{pages$projectTab.projectStatus}" readonly="true"/>

					<h:panelGroup>
	                   
	       				<h:outputLabel styleClass="LABEL" value="#{msg['project.relatedStudy']}: " />
                    </h:panelGroup>	
					<h:panelGroup>
	                    <h:inputText styleClass="INPUT_TEXT READONLY" id="txtRelatedStudy" value="#{pages$projectTab.relatedStudy}" readonly="true"/>
	                    <h:outputLabel value=" "/>
					    <h:commandLink actionListener="#{pages$projectTab.openStudySearchPopup}" rendered="#{!pages$projectTab.readonlyMode}" title="#{msg['commons.search']}">
					   		<h:graphicImage style="MARGIN: 0px 0px -4px" url="../resources/images/magnifier.gif"></h:graphicImage>
					    </h:commandLink>
					    <h:commandLink actionListener="#{pages$projectTab.openStudyViewPopup}" rendered="#{pages$projectTab.relatedStudyId != null}" title="#{msg['commons.view']}">
					   		<h:graphicImage style="MARGIN: 0px 0px -2px" url="../resources/images/app_icons/Lease-contract.png"></h:graphicImage>
					    </h:commandLink>
                    </h:panelGroup>
	       			<h:outputLabel  styleClass="LABEL" value="#{msg['study.description']}: "/>
                    <h:inputText styleClass="INPUT_TEXT READONLY" id="txtStudyDesc" value="#{pages$projectTab.studyDescription}" readonly="true"/>
										
					<h:panelGroup>
	                    <h:outputLabel styleClass="mandatory" value="#{pages$projectTab.asterisk}"></h:outputLabel>
						<h:outputLabel styleClass="LABEL" value="#{msg['property.name']}: " />
					</h:panelGroup>
			        <h:panelGroup>
	                    <h:inputText styleClass="INPUT_TEXT READONLY" id="txtPropertyName"  value="#{pages$projectTab.propertyName}" readonly="true"></h:inputText>
	                    <h:outputLabel value=" "/>
	                    <h:commandLink onclick="showPropertySearchPopup();"  rendered="#{!pages$projectTab.readonlyMode}" title="#{msg['commons.search']}">
					   		<h:graphicImage style="MARGIN: 0px 0px -4px" url="../resources/images/magnifier.gif"></h:graphicImage>
					    </h:commandLink>
					    <h:commandLink style="padding-left: 2px; padding-right:2px;" action="#{pages$projectTab.openRecieveProperty}" rendered="#{pages$projectTab.showRecievePropertyImage}" title="#{msg['commons.view']}">
					   		<h:graphicImage style="MARGIN: 0px 0px -2px" url="../resources/images/app_icons/Lease-contract.png"></h:graphicImage>
					    </h:commandLink>
                    </h:panelGroup>
			        <h:panelGroup>
	       				<h:outputLabel styleClass="LABEL"  value="#{msg['property.landNumber']}: " />
                    </h:panelGroup>
					<h:inputText styleClass="INPUT_TEXT READONLY" id="txtLandNumber" value="#{pages$projectTab.landNumber}" readonly="true"/>
			        
			        
			        <h:panelGroup>
	       				<h:outputLabel styleClass="LABEL"  value="#{msg['property.type']}: " />
                    </h:panelGroup>
			        <h:selectOneMenu styleClass="SELECT_MENU" id="propertyTypeCombo" rendered="#{!pages$projectTab.readonlyMode && false}" required="false"
							value="#{pages$projectTab.propertyTypeId}">
							<f:selectItem itemValue="0" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
							<f:selectItems value="#{pages$ApplicationBean.propertyTypeList}" />
					</h:selectOneMenu>
					<h:inputText styleClass="INPUT_TEXT READONLY" id="txtPropertyType" rendered="#{pages$projectTab.readonlyMode || true}" readonly="true" value="#{pages$projectTab.propertyType}" />
								       
					<h:panelGroup>
	       				<h:outputLabel styleClass="LABEL"  value="#{msg['property.ownership']}: " />
                    </h:panelGroup>
			        <h:selectOneMenu styleClass="SELECT_MENU" id="propertyOwnershipCombo" rendered="#{!pages$projectTab.readonlyMode  && false}" required="false"
							value="#{pages$projectTab.propertyOwnershipId}">
							<f:selectItem itemValue="0" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
							<f:selectItems value="#{pages$ApplicationBean.propertyOwnershipType}" />
					</h:selectOneMenu>
					<h:inputText styleClass="INPUT_TEXT READONLY" id="txtpropertyOwnership" rendered="#{pages$projectTab.readonlyMode  || true}" readonly="true" value="#{pages$projectTab.propertyOwnership}" />
															
					<h:panelGroup>
	                    <h:outputLabel styleClass="mandatory" value="#{pages$projectTab.asterisk}"></h:outputLabel>
	       				<h:outputLabel styleClass="LABEL" value="#{msg['project.projectName']}: "></h:outputLabel>
                    </h:panelGroup>										
			        <h:inputText styleClass="INPUT_TEXT #{pages$projectTab.readonlyStyleClass}" id="txtProjectName"  value="#{pages$projectTab.projectName}" readonly="#{pages$projectTab.readonlyMode}" tabindex="1" maxlength="100"></h:inputText>
				    <h:panelGroup>
	                    <h:outputLabel styleClass="mandatory" value="#{pages$projectTab.asterisk}"></h:outputLabel>
	       				<h:outputLabel styleClass="LABEL" value="#{msg['project.projectType']}: "></h:outputLabel>
                    </h:panelGroup>		
				    <h:selectOneMenu styleClass="SELECT_MENU" id="projectTypeCombo" rendered="#{!pages$projectTab.readonlyMode}" required="false" value="#{pages$projectTab.projectTypeId}" tabindex="2">
						<f:selectItem itemValue="0" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
						<f:selectItems value="#{pages$ApplicationBean.projectType}" />
					</h:selectOneMenu>
					<h:inputText styleClass="INPUT_TEXT READONLY" id="txtProjectType" rendered="#{pages$projectTab.readonlyMode}"  value="#{pages$projectTab.projectType}" readonly="true"></h:inputText>
	   
					<h:panelGroup>
	                    <h:outputLabel styleClass="mandatory" value="#{pages$projectTab.asterisk}"></h:outputLabel>
	       				<h:outputLabel styleClass="LABEL" value="#{msg['project.size']}: "></h:outputLabel>
                    </h:panelGroup>	
				    <h:selectOneMenu styleClass="SELECT_MENU" id="projectSizeCombo" required="false" rendered="#{!pages$projectTab.readonlyMode}"
							value="#{pages$projectTab.projectSizeId}" tabindex="3">
							<f:selectItem itemValue="0" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
							<f:selectItems value="#{pages$ApplicationBean.projectSizeList}" />
					</h:selectOneMenu>
					<h:inputText styleClass="INPUT_TEXT READONLY" id="txtProjectSize" rendered="#{pages$projectTab.readonlyMode}" readonly="true" value="#{pages$projectTab.projectSize}" ></h:inputText>
			        <h:panelGroup>
	                    <h:outputLabel styleClass="mandatory" value="#{pages$projectTab.asterisk}"></h:outputLabel>
	       				<h:outputLabel styleClass="LABEL" value="#{msg['project.purpose']}: "></h:outputLabel>
                    </h:panelGroup>
				    <h:selectOneMenu styleClass="SELECT_MENU"  id="projectPurposeCombo"  rendered="#{!pages$projectTab.readonlyMode}" required="false"
							value="#{pages$projectTab.projectPurposeId}" tabindex="4">
							<f:selectItem itemValue="0" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
							<f:selectItems value="#{pages$ApplicationBean.projectPurposeList}" />
					</h:selectOneMenu>
					<h:inputText styleClass="INPUT_TEXT READONLY" id="txtProjectPurpose" rendered="#{pages$projectTab.readonlyMode}" readonly="true"  value="#{pages$projectTab.projectPurpose}" ></h:inputText>
			        
    				<h:panelGroup>
	                    <h:outputLabel styleClass="mandatory" value="#{pages$projectTab.asterisk}"></h:outputLabel>
	       				<h:outputLabel styleClass="LABEL"  value="#{msg['project.category']}: " />
                    </h:panelGroup>
			        <h:selectOneMenu styleClass="SELECT_MENU"  id="projectCategoryCombo" rendered="#{!pages$projectTab.readonlyMode}" required="false"
							value="#{pages$projectTab.projectCategoryId}" tabindex="5">
							<f:selectItem itemValue="0" itemLabel="#{msg['commons.combo.PleaseSelect']}" />
							<f:selectItems value="#{pages$ApplicationBean.projectCategoryList}" />
					</h:selectOneMenu>
					<h:inputText styleClass="INPUT_TEXT READONLY" id="txtProjectCategory" rendered="#{pages$projectTab.readonlyMode}" readonly="true" value="#{pages$projectTab.projectCategory}" />
						
					<h:panelGroup>
	                    <h:outputLabel styleClass="mandatory" value="#{pages$projectTab.asterisk}"></h:outputLabel>
	       				<h:outputLabel styleClass="LABEL"  value="#{msg['constructionTender.search.project.estimatedCost']}: " />
                    </h:panelGroup>	
					<h:inputText styleClass="INPUT_TEXT A_RIGHT #{pages$projectTab.readonlyStyleClass}" id="txtEstimatedCost" value="#{pages$projectTab.projectEstimatedCost}" readonly="#{pages$projectTab.readonlyMode}" tabindex="6">
					
					</h:inputText>
					
					<h:panelGroup>
	                    <h:outputLabel styleClass="mandatory" value="#{pages$projectTab.asterisk}"></h:outputLabel>
	       				<h:outputLabel styleClass="LABEL"  value="#{msg['project.expectedStartDate']}: " />
                    </h:panelGroup>
					<rich:calendar id="expectedStartDate" styleClass="#{pages$projectTab.readonlyStyleClass}" value="#{pages$projectTab.expectedStartDate}" locale="#{pages$projectTab.locale}" popup="true" datePattern="#{pages$projectTab.dateFormat}" disabled="#{pages$projectTab.readonlyMode}" showApplyButton="false" enableManualInput="false" cellWidth="22px" cellHeight="24px"/>
						
					<h:panelGroup>
	                    <h:outputLabel styleClass="mandatory" value="#{pages$projectTab.asterisk}"></h:outputLabel>
	       				<h:outputLabel styleClass="LABEL"  value="#{msg['project.expectedEndDate']}: " />
                    </h:panelGroup>
					<rich:calendar id="expectedEndDate" styleClass="#{pages$projectTab.readonlyStyleClass}" value="#{pages$projectTab.expectedEndDate}" locale="#{pages$projectTab.locale}" popup="true" datePattern="#{pages$projectTab.dateFormat}" disabled="#{pages$projectTab.readonlyMode}" showApplyButton="false" enableManualInput="false" cellWidth="22px" cellHeight="24px"/>
						
					<h:panelGroup>
	                    <h:outputLabel styleClass="mandatory" value="#{pages$projectTab.asterisk}"></h:outputLabel>
	       				<h:outputLabel styleClass="LABEL" value="#{msg['project.description']}: "></h:outputLabel>
                    </h:panelGroup>										
			        <t:inputTextarea styleClass="TEXTAREA #{pages$projectTab.readonlyStyleClass}" value="#{pages$projectTab.projectDescription}" readonly="#{pages$projectTab.readonlyMode}" rows="6" style="width: 185px; height:30px;"  tabindex="7"/>
	</t:panelGrid>
</t:div>
