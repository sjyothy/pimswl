<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">

	function openLoadReportPopup(pageName) 	{	
		var screen_width = screen.width;
		var screen_height = screen.height;
        var popup_width = screen_width-250;
        var popup_height = screen_height-500;
        var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;
        window.open(pageName,'_blank','width='+popup_width+',height='+popup_height+',left='+leftPos+',top='+topPos+ ',scrollbars=yes,status=no,resizable=yes,titlebar=no,dialog=yes');
	}
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />


			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />





		</head>

		<body>
			<div style="width: 1024px;">


				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>


					</tr>
					<tr width="100%">

						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>

						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel
											value="#{msg['reportPendingPaymentsAfterSettlement.reportSearchPage.heading']}"
											styleClass="HEADER_FONT" />
									</td>
									<td width="100%">
										&nbsp;
									</td>
								</tr>
							</table>

							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg">
									</td>
									<td width="100%" height="99%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION" style="height: 450px;">

											<div>
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText
																value="#{pages$reportPendingPaymentsAfterSettlement.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>
											</div>


											<h:form id="searchFrm">

												<div class="MARGIN" style="width: 96%">
													<table cellpadding="0" cellspacing="0">
														<tr>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td style="FONT-SIZE: 0px" width="100%">
																<IMG
																	src="../<h:outputText value="#{path.img_section_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="FONT-SIZE: 0px">
																<IMG
																	src="../<h:outputText value="#{path.img_section_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="DETAIL_SECTION" style="width: 99.6%">
														<h:outputLabel value="#{msg['commons.report.criteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="0px" cellspacing="0px"
															class="DETAIL_SECTION_INNER" border="0">
															<tr>
																<td width="97%">
																	<table width="100%">
																	<tr>
																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.contractNumber']} :"></h:outputLabel>
																			</td>
																			<td>

																				<h:inputText id="txtContractNumber"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.CONTRACT_NUMBER}"
																					maxlength="20"></h:inputText>
																			</td>
																			
																			
																			<td>
																			<h:outputLabel styleClass="LABEL"
																					value="#{msg['listReport.tenancyContractTemplate.tenantName']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtTenantName"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.TENANT_NAME}"
																					maxlength="20"></h:inputText>
																			</td>
																			
																	</tr>
																	
																	
																																			
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['reportPendingPaymentsAfterSettlement.reportSearchPage.settlementDateFrom']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="clndrSettlementDateFrom" popup="true"
																					datePattern="#{pages$reportPendingPaymentsAfterSettlement.dateFormat}"
																					locale="#{pages$reportPendingPaymentsAfterSettlement.locale}"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.SETTLEMENT_DATE_FROM}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width: 170px; height: 14px"></rich:calendar>
																			</td>


																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['reportPendingPaymentsAfterSettlement.reportSearchPage.settlementDateTo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="clndrSettlementDateTo"
																					datePattern="#{pages$reportPendingPaymentsAfterSettlement.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$reportPendingPaymentsAfterSettlement.locale}"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.SETTLEMENT_DATE_TO}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px">
																				</rich:calendar>
																			</td>
																		</tr>
																		
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.startDateFrom']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="clndrContractStartDateFrom" popup="true"
																					datePattern="#{pages$reportPendingPaymentsAfterSettlement.dateFormat}"
																					locale="#{pages$reportPendingPaymentsAfterSettlement.locale}"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.CONTRACT_START_DATE_FROM}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width: 170px; height: 14px"></rich:calendar>
																			</td>


																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.startDateTo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="clndrContractStartDateTo"
																					datePattern="#{pages$reportPendingPaymentsAfterSettlement.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$reportPendingPaymentsAfterSettlement.locale}"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.CONTRACT_START_DATE_TO}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px">
																				</rich:calendar>
																			</td>
																		</tr>
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.endDateFrom']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="clndrContractEndDateFrom" popup="true"
																					datePattern="#{pages$reportPendingPaymentsAfterSettlement.dateFormat}"
																					locale="#{pages$reportPendingPaymentsAfterSettlement.locale}"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.CONTRACT_END_DATE_FROM}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width: 170px; height: 14px"></rich:calendar>
																			</td>


																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['contract.endDateTo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="clndrContractEndDateTo"
																					datePattern="#{pages$reportPendingPaymentsAfterSettlement.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$reportPendingPaymentsAfterSettlement.locale}"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.CONTRACT_END_DATE_TO}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px">
																				</rich:calendar>
																			</td>
																		</tr>
																	
																	
																	
																	<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['settlement.label.grpCustNo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtGrpCustomerNo"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.GRP_CUSTOMER_NO}"
																					maxlength="20"></h:inputText>
																			</td>
																			
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['grp.propertyName']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtPropertyName"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.PROPERTY_NAME}"
																					maxlength="50"></h:inputText>
																			</td>
																			
																			
																			
																	</tr>
																	
																	
																	
																	<tr>
																			
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['unit.unitDescription']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtUnitDesc"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.UNIT_DESC}"
																					maxlength="100"></h:inputText>
																			</td>
																			
																			
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['reportPendingPaymentsAfterSettlement.reportSearchPage.lblUnitAccNo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtUnitAccNo"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.UNIT_ACC_NO}"
																					maxlength="50"></h:inputText>
																			</td>
																			
																		
																			
																	</tr>
																	
																	
																		
																	<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['reportPendingPaymentsAfterSettlement.reportSearchPage.amountFrom']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtAmountFrom"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.AMOUNT_FROM}"
																					maxlength="15"></h:inputText>
																			</td>
																			
																			
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['reportPendingPaymentsAfterSettlement.reportSearchPage.amountTo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<h:inputText id="txtAmountTo"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.AMOUNT_TO}"
																					maxlength="15"></h:inputText>
																			</td>
																		
																	</tr>
																	
																		

																		
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.name.paymentDueDateFrom']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="clndrPaymentDueDateFrom" popup="true"
																					datePattern="#{pages$reportPendingPaymentsAfterSettlement.dateFormat}"
																					locale="#{pages$reportPendingPaymentsAfterSettlement.locale}"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.PAYMENT_DUE_ON_FROM}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width: 170px; height: 14px"></rich:calendar>
																			</td>


																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['commons.report.name.paymentDueDateTo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="clndrPaymentDueDateTo"
																					datePattern="#{pages$reportPendingPaymentsAfterSettlement.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$reportPendingPaymentsAfterSettlement.locale}"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.PAYMENT_DUE_ON_TO}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px">
																				</rich:calendar>
																			</td>
																		</tr>
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['request.dateFrom']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="clndrRequestDateFrom" popup="true"
																					datePattern="#{pages$reportPendingPaymentsAfterSettlement.dateFormat}"
																					locale="#{pages$reportPendingPaymentsAfterSettlement.locale}"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.REQUEST_DATE_FROM}"
																					showApplyButton="false" enableManualInput="false"
																					inputStyle="width: 170px; height: 14px"></rich:calendar>
																			</td>


																			<td>

																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['request.dateTo']} :"></h:outputLabel>
																			</td>
																			<td>
																				<rich:calendar id="clndrRequestDateTo"
																					datePattern="#{pages$reportPendingPaymentsAfterSettlement.dateFormat}"
																					showApplyButton="false"
																					locale="#{pages$reportPendingPaymentsAfterSettlement.locale}"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.REQUEST_DATE_TO}"
																					enableManualInput="false"
																					inputStyle="width: 170px; height: 14px">
																				</rich:calendar>
																			</td>
																		</tr>
																		
																		<tr>
																			<td>
																				<h:outputLabel styleClass="LABEL"
																					value="#{msg['paymentConfiguration.paymentType']} :"></h:outputLabel>
																			</td>
																			<td>
																				 <h:selectOneMenu id="cboPaymentType"
																					required="false"
																					value="#{pages$reportPendingPaymentsAfterSettlement.criteria.PAYMENT_TYPE_ID}">
																					<f:selectItem itemValue="-1"
																						itemLabel="#{msg['commons.All']}" />
																					<f:selectItems
																						value="#{pages$ApplicationBean.paymentTypeList}" />
																				</h:selectOneMenu>	
																					
																			</td>
																		</tr>			
																			
																
																					
							
																		


																		<tr>
																			<td class="BUTTON_TD" colspan="4">
																				<h:commandButton styleClass="BUTTON"
																					id="submitButton"
																					action="#{pages$reportPendingPaymentsAfterSettlement.cmdView_Click}"
																					immediate="false" value="#{msg['commons.view']}"
																					tabindex="7"></h:commandButton>
																			</td>
																		</tr>

																	</table>
																</td>
																<td width="3%">
																	&nbsp;
																</td>
															</tr>

														</table>


													</div>
												</div>



											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>

		</body>
	</html>
</f:view>
