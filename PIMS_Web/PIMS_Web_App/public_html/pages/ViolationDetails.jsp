<%-- 
  - Author: Afzal Zulfiqar
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Auction
  --%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>		
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<f:view>
<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>
<html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
	<head>
		<title>PIMS</title>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is auction search page">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table_ff}"/>"/>	
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_calendar}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>		
	 		<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_tabPanel}"/>"/>		

			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			<style>
.rich-calendar-input{
width:85%;
HEIGHT: 18px;
}
</style>
	</head>



	<body class="BODY_STYLE">
		
			
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td  colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>

				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>	<td class="HEADER_TD">
										<h:outputLabel value="#{msg['violationDetails.header']}" styleClass="HEADER_FONT"/>
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						
					
						<table width="99%" class="greyPanelMiddleTable" cellpadding="0" 
							cellspacing="0" border="0">
							<tr valign="top">
								<td height="100%" valign="top" nowrap="nowrap"
									background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
									width="1">
								</td>
								<td width="100%" height="99%" valign="top" nowrap="nowrap">
									<div class="SCROLLABLE_SECTION" style="height:450px;">
									
									<h:form  id="searchFrm" style="width:99%">

										<h:messages id="aaa">
															<h:outputText id="bbb" value="" escape="false" />
														</h:messages>
														<h:outputText id="ccc" escape="false"
															value="#{pages$ViolationDetails.errorMessages}" />

							<div class="MARGIN">
									<table cellpadding="0" cellspacing="0"  >
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>

										<div class="DETAIL_SECTION">
										<h:outputLabel value="#{msg['unit.details']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
													<table cellpadding="1px" cellspacing="2px"
														class="DETAIL_SECTION_INNER">
														<tr>
															<td colspan="4">
																<!--<h:messages>
															<h:outputText value="" escape="false" />
														</h:messages>
														<h:outputText escape="false"
															value="#{pages$ViolationDetails.errorMessages}" />

-->
															</td>
														</tr>
														<tr>
															<td colspan="1" width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.unitRefNo']}:"></h:outputLabel>
															</td>

															<td colspan="1" width="25%">

																<h:inputText id="unitReferenceNo" readonly="true"
																	value="#{pages$ViolationDetails.unitRefNo}"
																	style="width:85%;HEIGHT: 18px"  maxlength="20"></h:inputText>

															</td>
															<td colspan="1" width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['unit.type']}:"></h:outputLabel>
															</td>
															<td colspan="1" width="25%">
																<h:inputText id="unitType" readonly="true"
																	value="#{pages$ViolationDetails.unitType}"
																	 style="width:85%;HEIGHT: 18px" maxlength="250"></h:inputText>
															</td>
														</tr>


														<tr>
															<td colspan="1">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['property.name']}:"></h:outputLabel>
															</td>
															<td colspan="1" width="10%">
																<h:inputText id="propertyName" readonly="true"
																	value="#{pages$ViolationDetails.propertyName}"
																	style="width:85%;HEIGHT: 18px" maxlength="20"></h:inputText>
															</td>
															<td colspan="1">

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.unit.status']}:" />
															</td>
															<td colspan="1">

																<h:inputText id="unitStatus" readonly="true"
																	value="#{pages$ViolationDetails.unitStatus}"
																	style="width:85%;HEIGHT: 18px"></h:inputText>
														</tr>
														<tr>
															<td>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['contract.contractNumber']}:" />
															</td>
															<td>
																<h:inputText id="contractNumber" readonly="true"
																style="width:85%;HEIGHT: 18px" value="#{pages$ViolationDetails.contractRefNo}"></h:inputText>
															</td>
															<td>
															</td>
															<td>
																
															</td>
														</tr>
														<tr>
															
															<td colspan="4">
															<div class="A_RIGHT">
															<h:commandButton styleClass="BUTTON" value="#{msg['inspection.details']}" action="#{pages$ViolationDetails.inspectionDetails}" style="width: 117px;" />
															</div>
															</td>
														</tr>
													</table>
												</div>
								</div>
								
								
							<pims:security screen="Pims.Property.Inspection.Violation" action="create"> 
							<c:if test="${requestScope.pages$ViolationDetails.editable}">
							<c:if test="${requestScope.pages$ViolationDetails.fromList}">
								<div class="MARGIN">
									<table cellpadding="0" cellspacing="0"  >
										<tr>
										<td><IMG src="../<h:outputText value="#{path.img_section_left}"/>" class="TAB_PANEL_LEFT"/></td>
										<td width="100%"><IMG src="../<h:outputText value="#{path.img_section_mid}"/>" class="TAB_PANEL_MID"/></td>
										<td><IMG src="../<h:outputText value="#{path.img_section_right}"/>" class="TAB_PANEL_RIGHT"/></td>
										</tr>
									</table>

									<div class="DETAIL_SECTION">
									<h:outputLabel value="#{msg['violation.addViolation']}" styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
																<table cellpadding="1px" cellspacing="2px"
																	class="DETAIL_SECTION_INNER">
																	<tr>
																		<td colspan="1">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['violationDetails.description']}" />
																			:
																		</td>
																		<td colspan="3">
																			<h:inputTextarea id="description"
																				value="#{pages$ViolationDetails.description}"
																				style="width: 557px; height: 48px" cols="130"
																				rows="2" />
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['violation.Date']}:"></h:outputLabel>
																		</td>
																		<td>

																			<rich:calendar id="vdate" datePattern="MM/dd/yyyy"
																				value="#{pages$ViolationDetails.violationDateRich}" />



																		</td>
																		<td>
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['violation.category']}:"></h:outputLabel>
																		</td>
																		<td>
																			<h:selectOneMenu id="violationCategory"
																				value="#{pages$ViolationDetails.violationCategory}"
																				style="width: 163px">
																				<f:selectItem id="plsSelectCategory" itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ViolationDetails.categoryList}" />

																			</h:selectOneMenu>
																		</td>
																	</tr>
																	<tr>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['violation.type']}:" />
																		</td>
																		<td width="25%">
																			<h:selectOneMenu id="violationType"
																				value="#{pages$ViolationDetails.violationType}"
																				style="width: 183px">
																				<f:selectItem id="plsSelectType" itemValue="-1"
																					itemLabel="#{msg['commons.combo.PleaseSelect']}" />
																				<f:selectItems
																					value="#{pages$ViolationDetails.typeList}" />

																			</h:selectOneMenu>
																		</td>
																		<td width="25%">
																			<h:outputLabel styleClass="LABEL"
																				value="#{msg['violation.damageAmount']}:" />
																		</td>
																		<td width="25%">
																			<h:inputText id="damageAmount"
																				value="#{pages$ViolationDetails.damageAmount}"
																				styleClass="A_RIGHT_NUM" style="width: 85%;;HEIGHT: 18px" />
																		</td>
																	</tr>
																	<tr>
																		
																		<td colspan="4">
																		<div class="A_RIGHT">
																		<h:commandButton styleClass="BUTTON ADD_VIOLATION_BUTTON" value="#{msg['violation.addViolation']}" action="#{pages$ViolationDetails.addViolation}" style="width: 100px" />
																		</div>
																		</td>
																	</tr>
																	<tr>
																		<td width="25%" />
																		<td width="25%" />
																		<td width="23%" />
																		<td width="25%">
																			
																		</td>

																	</tr>
																</table>

															</div>
									</div>
									</c:if>
									</c:if>
									</pims:security>
									
									<div style="padding:5px;">
								
										<div class="contentDiv" style="width:100%" align="center">
										
										<t:dataTable id="dt1" rows="5" preserveDataModel="false"
														preserveSort="false" var="dataItem" rules="all"
														renderedIfEmpty="true" width="100%"
														value="#{pages$ViolationDetails.violationView}"
														binding="#{pages$ViolationDetails.dataTable}">

												<t:column id="col1" width="100" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['violationDetails.description']}"  />
													</f:facet>
													<t:outputText styleClass="A_LEFT" style="WHITE-SPACE: normal"  value="#{dataItem.description}" />
												</t:column>
												<t:column id="col2" width="85" sortable="true">
													<f:facet name="header">
														<t:outputText value="#{msg['violation.type']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" style="WHITE-SPACE: normal" value="#{dataItem.typeEn}" rendered="#{pages$ViolationDetails.isEnglishLocale}"/>
													<t:outputText styleClass="A_LEFT" style="WHITE-SPACE: normal" value="#{dataItem.typeAr}" rendered="#{pages$ViolationDetails.isArabicLocale}"/>
												</t:column>
												<t:column id="col3" width="135" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['violation.category']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" style="WHITE-SPACE: normal" value="#{dataItem.categoryEn}" rendered="#{pages$ViolationDetails.isEnglishLocale}"/>
													<t:outputText styleClass="A_LEFT" style="WHITE-SPACE: normal" value="#{dataItem.categoryAr}" rendered="#{pages$ViolationDetails.isArabicLocale}"/>
													
												</t:column>
												<t:column id="col4" width="85" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['violation.status']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" style="WHITE-SPACE: normal"  value="#{dataItem.statusEn}" rendered="#{pages$ViolationDetails.isEnglishLocale}"/>
													<t:outputText styleClass="A_LEFT" style="WHITE-SPACE: normal"  value="#{dataItem.statusAr}" rendered="#{pages$ViolationDetails.isArabicLocale}"/>
													
												</t:column>
												<t:column id="colDamge" width="85" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['violation.damageAmount']}" />
													</f:facet>
													<t:outputText styleClass="A_RIGHT_NUM" value="#{dataItem.damageAmount}" />
													
												</t:column>
												<t:column id="colViolationDate" width="85" sortable="true" >
													<f:facet name="header">
														<t:outputText value="#{msg['violation.Date']}" />
													</f:facet>
													<t:outputText styleClass="A_LEFT" value="#{dataItem.violationDateString}" />
													
												</t:column>
												
												<c:if test="${!requestScope.pages$ViolationDetails.fromList}">
												<t:column  id="col11" sortable="false"  width="20">
													<f:facet name="header">
														<t:outputText  value="#{msg['commons.action']}" />
													</f:facet>
													<pims:security screen="Pims.Property.Inspection.Violation.Action" action="create">
													
													<h:commandLink  action="#{pages$ViolationDetails.addAction}" >
													<h:graphicImage style="margin-left:13px;"  title="#{msg['inspection.assignActionToViolation']}" url="../resources/images/app_icons/Assign-Action-to-Violation.png" />
													</h:commandLink>
													
													</pims:security>
													</t:column>
													</c:if>
													
													<c:if test="${requestScope.pages$ViolationDetails.editable}">
													<c:if test="${requestScope.pages$ViolationDetails.fromList}">
												<t:column  id="col10" sortable="false" width="20">
													<f:facet name="header">
														<t:outputText value="#{msg['commons.delete']}" />
													</f:facet>
													
													<pims:security screen="Pims.Property.Inspection.Violation" action="delete">
													
													<h:commandLink action="#{pages$ViolationDetails.deleteViolation}" >
													<h:graphicImage style="margin-left:13px;" title="#{msg['commons.delete']}" url="../resources/images/delete_icon.png" />
													</h:commandLink>
													</pims:security>
													</t:column>
													</c:if>
													</c:if>
												
											</t:dataTable>
							</div>
										<div class="contentDivFooter" align="right">
											<t:dataScroller id="scroller" for="dt1" paginator="true"
												fastStep="1" paginatorMaxPages="2" immediate="false"
												styleClass="scroller" paginatorTableClass="paginator"
												renderFacetsIfSinglePage="true" 
												paginatorTableStyle="grid_paginator" layout="singleTable" 
												paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
												paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

												<f:facet name="first">
													<t:graphicImage url="../resources/images/first_btn.gif" id="lblF"></t:graphicImage>
												</f:facet>

												<f:facet name="last">
												<t:graphicImage url="../resources/images/last_btn.gif" id="lblL"></t:graphicImage> 
                                                
												</f:facet>

												<f:facet name="fastforward">
                                                    <t:graphicImage url="../resources/images/next_btn.gif" id="lblFF"></t:graphicImage>
												</f:facet>
												<f:facet name="fastrewind">
                                                    <t:graphicImage url="../resources/images/previous_btn.gif" id="lblFR"></t:graphicImage>      
												</f:facet>

											</t:dataScroller>

										</div>     
								</div>
										<div class="A_RIGHT">
										
										<pims:security screen="Pims.Property.Inspection.Violation.Action" action="create">
											<c:if test="${!requestScope.pages$ViolationDetails.fromList}">
												<h:commandButton styleClass="BUTTON" value="#{msg['commons.complete']}" action="#{pages$ViolationDetails.okTask}" />
											</c:if>	
										</pims:security>
										
										<pims:security screen="Pims.Property.Inspection.Violation" action="create">
										<c:if test="${requestScope.pages$ViolationDetails.editable}">
											<c:if test="${requestScope.pages$ViolationDetails.fromList}">
												<h:commandButton styleClass="BUTTON" value="#{msg['commons.send']}" action="#{pages$ViolationDetails.sendTask}" />
												<h:commandButton styleClass="BUTTON" value="#{msg['commons.cancel']}" action="#{pages$ViolationDetails.btnCancel_Click}" /> 
											</c:if>
										</c:if>
										</pims:security>
										
										</div>
										<h:inputHidden id="contractNumberHidden" value="#{pages$ViolationDetails.contractRefNo}"/>
											<h:inputHidden id="unitNumberHidden" value="#{pages$ViolationDetails.unitRefNo}"/>
											<h:inputHidden id="unitTypeHidden" value="#{pages$ViolationDetails.unitType}"/>
											<h:inputHidden id="unitStatusHidden" value="#{pages$ViolationDetails.unitStatus}"/>
											<h:inputHidden id="propertyNameHidden" value="#{pages$ViolationDetails.propertyName}"/>
											<h:inputHidden id="unitId" value="#{pages$ViolationDetails.unitId}"/>
											<h:inputHidden id="inspectionId" value="#{pages$ViolationDetails.inspectionId}"/>
											<h:inputHidden id="formListHid" value="#{pages$ViolationDetails.fromList}"/>
											<h:inputHidden id="editHid" value="#{pages$ViolationDetails.editable}"/>
										
									</h:form>
									</div>	
								</td>
							</tr>
							
						</table>
					</td>
				</tr>
				<tr>
    <td colspan="2">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
       <tr><td class="footer"><h:outputLabel value="#{msg['commons.footer.message']}" /></td></tr>
    </table>
        </td>
    </tr>
			</table>
		  </f:view>
		
	</body>
</html>