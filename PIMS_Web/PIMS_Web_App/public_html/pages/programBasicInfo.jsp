
	<t:panelGrid id="basicInfoGrid" cellpadding="1px" width="100%"
		cellspacing="5px" styleClass="TAB_DETAIL_SECTION_INNER" columns="4">

		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['socialProgram.lbl.ProgramNum']}"></h:outputLabel>
		</t:panelGroup>
		<h:inputText id="txtProgramNumber" styleClass="READONLY"
			readonly="true"
			value="#{pages$SocialProgramBasicInfo.socialProgramView.programNum}"></h:inputText>

		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['socialProgram.lbl.ProgramName']}"></h:outputLabel>
		</t:panelGroup>
		<h:inputText id="txtProgramName"
			value="#{pages$SocialProgramBasicInfo.socialProgramView.programName}"></h:inputText>

		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['socialProgram.lbl.ProgramType']}"></h:outputLabel>
		</t:panelGroup>
		<h:selectOneMenu id="selectProgramType"
			value="#{pages$SocialProgramBasicInfo.socialProgramView.programType}">

			<f:selectItems value="#{pages$ApplicationBean.socialProgramTypeList}" />
		</h:selectOneMenu>
		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['socialProgram.lbl.assignedEmp']}"></h:outputLabel>
		</t:panelGroup>
		<h:panelGroup>

			<h:inputText id="txtEmpName" styleClass="READONLY" readonly="true"
				value="#{pages$SocialProgramBasicInfo.englishLocale? pages$SocialProgramBasicInfo.socialProgramView.assignedEmpEn:pages$SocialProgramBasicInfo.socialProgramView.assignedEmpAr}" />
			<h:graphicImage title="#{msg['commons.search']}"
				rendered="#{pages$SocialProgramBasicInfo.pageMode!= 'PAGE_MODE_VIEW'}"
				onclick="javaScript:onSearchMemberPopup();"
				style="MARGIN: 1px 1px -5px;cursor:hand"
				url="../resources/images/app_icons/Search-tenant.png">
			</h:graphicImage>

		</h:panelGroup>
		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['socialProgram.lbl.TargetGrp']}"></h:outputLabel>
		</t:panelGroup>
		<h:selectOneMenu id="selectTargetGroup"
			value="#{pages$SocialProgramBasicInfo.socialProgramView.targetGrp}">

			<f:selectItems
				value="#{pages$ApplicationBean.socialProgramTargetGrpList}" />
		</h:selectOneMenu>


		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['socialProgram.lbl.Duration']}"></h:outputLabel>
		</t:panelGroup>
		<t:panelGrid columns="2" style="width:100%;"
			columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">
			<h:inputText id="txtFreq" style="width:95%;" styleClass="A_RIGHT_NUM"
				maxlength="4" onchange="javaScript:onCalculateEndDate();"
				value="#{pages$SocialProgramBasicInfo.socialProgramView.periodFreq}">
				
			</h:inputText>
			<h:selectOneMenu id="selectPeriod" style="width:40%;" onchange="javaScript:onCalculateEndDate();"
				value="#{pages$SocialProgramBasicInfo.socialProgramView.periodDuration}">

				<f:selectItems value="#{pages$ApplicationBean.gracePeriodTypeList}" />
				
			</h:selectOneMenu>
		</t:panelGrid>
		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['socialProgram.lbl.StartDate']}"></h:outputLabel>
		</t:panelGroup>
		<rich:calendar id="startDate" 
			value="#{pages$SocialProgramBasicInfo.socialProgramView.startDate}"
			popup="true" datePattern="#{pages$SocialProgramBasicInfo.dateFormat}"
			timeZone="#{pages$SocialProgramBasicInfo.timeZone}"
			showApplyButton="false" enableManualInput="false" cellWidth="24px"
			locale="#{pages$SocialProgramBasicInfo.locale}"
			onchanged="javaScript:onCalculateEndDate();"
			>
			
		</rich:calendar>

		<h:outputLabel styleClass="LABEL"
			value="#{msg['socialProgram.lbl.EndDate']}"></h:outputLabel>
		<rich:calendar id="endDate" disabled="true"
			value="#{pages$SocialProgramBasicInfo.socialProgramView.endDate}"
			popup="true" datePattern="#{pages$SocialProgramBasicInfo.dateFormat}"
			timeZone="#{pages$SocialProgramBasicInfo.timeZone}"
			showApplyButton="false" enableManualInput="false" cellWidth="24px"
			locale="#{pages$SocialProgramBasicInfo.locale}">
		</rich:calendar>


		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['socialProgram.lbl.ExpectedBudget']}"></h:outputLabel>
		</t:panelGroup>

		<h:inputText maxlength="20" style="width:190px" id="txtExpectedBudget"
			value="#{pages$SocialProgramBasicInfo.socialProgramView.expectedBudget}" />
		<h:outputLabel styleClass="LABEL"
			value="#{msg['socialProgram.lbl.participantCount']}"></h:outputLabel>

		<h:inputText id="txtNoOfPresentParticipants" readonly="true"
			styleClass="READONLY"
			value="#{pages$SocialProgramBasicInfo.socialProgramView.presentParticipants}" />


		<t:panelGroup>
			<h:outputLabel styleClass="mandatory" value="*"></h:outputLabel>
			<h:outputLabel styleClass="LABEL"
				value="#{msg['socialProgram.lbl.Description']}"></h:outputLabel>
		</t:panelGroup>
		<t:panelGroup colspan="3">
			<t:inputTextarea style="width: 93%" id="txtDesc"
				value="#{pages$SocialProgramBasicInfo.socialProgramView.description}" />
		</t:panelGroup>
		<h:outputLabel styleClass="LABEL"
			value="#{msg['socialProgram.lbl.surveyResult']}"></h:outputLabel>

		<t:panelGroup colspan="3">
			<t:inputTextarea style="width: 93%" id="txtSurveyResult"
				value="#{pages$SocialProgramBasicInfo.socialProgramView.surveyResult}" />
		</t:panelGroup>
	</t:panelGrid>
