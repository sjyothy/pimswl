
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>


<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



<script language="javascript" type="text/javascript">
		function clearWindow()
	{
	
	




	        document.getElementById("formFacilityListPopup:txtName").value="";
		    document.getElementById("formFacilityListPopup:txtrentValue").value="";
		         
	        var cmbFacilityType=document.getElementById("formFacilityListPopup:selectFacilityType");
		     cmbFacilityType.selectedIndex = 0;       
    


	
		    }
	

	function closeWindow()
	{
	  //window.opener.document.getElementById('conductAuctionForm:isResultPopupCalled').value = "Y";
	  window.opener.document.forms[0].submit();
	  window.close();
	}
	
</script>


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />


			<%

   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1

   response.setHeader("Pragma","no-cache"); //HTTP 1.0

   response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

%>



		</head>
		<body dir="${sessionScope.CurrentLocale.dir}"
			lang="${sessionScope.CurrentLocale.languageCode}" class="BODY_STYLE">

			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<%--<jsp:include page="header.jsp"/>--%>
					</td>
				</tr>

				<tr style="width: 100%">
					<td width="100%" valign="top" class="divBackgroundBody" colspan="2">
						<table width="100%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['facilityPopup.PageHeader']}"
										styleClass="HEADER_FONT" />
								</td>
								<td width="100%">
									&nbsp;
								</td>
							</tr>
						</table>
						<div>
							<table width="100%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">

								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<!--Use this below only for desiging forms Please do not touch other sections -->

									<td valign="top" nowrap="nowrap" height="100%">

											<h:form id="formFacilityListPopup">
												<div class="MARGIN">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="font-size:0pt;">
																<IMG
																	src="../<h:outputText value="#{path.img_section_left}"/>"
																	class="TAB_PANEL_LEFT" />
															</td>
															<td width="100%" style="font-size:0pt;">
																<IMG
																	src="../<h:outputText value="#{path.img_section_mid}"/>"
																	class="TAB_PANEL_MID" />
															</td>
															<td style="font-size:0pt;">
																<IMG
																	src="../<h:outputText value="#{path.img_section_right}"/>"
																	class="TAB_PANEL_RIGHT" />
															</td>
														</tr>
													</table>
													<div class="DETAIL_SECTION">
														<h:outputLabel value="#{msg['inquiry.searchcriteria']}"
															styleClass="DETAIL_SECTION_LABEL"></h:outputLabel>
														<table cellpadding="1px" cellspacing="2px"
															class="DETAIL_SECTION_INNER" >

															<tr>
															<td width="97%">
															<table width="100%">

															<tr>
																<td>
																	<h:outputLabel value="#{msg['facility.facilityName']}"></h:outputLabel>
																</td>
																<td>
																	<h:inputText id="txtName"
																		value="#{pages$FacilityListPopup.name}" />

																</td>
																<td>
																	<h:outputLabel value="#{msg['facility.rentValue']}"></h:outputLabel>
																</td>
																<td>
																	<h:inputText id="txtrentValue"
																		value="#{pages$FacilityListPopup.rentValue}">

																	</h:inputText>
																</td>

															</tr>
															<tr>

																<td>
																	<h:outputLabel value="#{msg['facility.facilityType']}"></h:outputLabel>
																</td>
																<td>

																	<h:selectOneMenu id="selectFacilityType"
																		style="width: 136px"
																		value="#{pages$FacilityListPopup.selectOneFacilityType}">
																		<f:selectItem itemLabel="#{msg['commons.All']}"
																			itemValue="-1" />
																		<f:selectItems
																			value="#{pages$ApplicationBean.facilityType}" />
																	</h:selectOneMenu>

																</td>

															</tr>

															</td>

															</tr>


															<tr>

																<td colspan="4" class="BUTTON_TD">


																	<h:commandButton styleClass="BUTTON" type="submit"
																		value="#{msg['commons.search']}"
																		action="#{pages$FacilityListPopup.btnSearch_Click}">
																	</h:commandButton>
																	<h:commandButton styleClass="BUTTON" type="button"
																		onclick="javascript:clearWindow();"
																		value="#{msg['commons.clear']}">
																	</h:commandButton>

																</td>
															</tr>
														</table>
													</td>
													<td width="3%">
														&nbsp;
													</td>
												</tr>
															

															<h:inputHidden id="hdnMode"
																value="#{pages$FacilityListPopup.hdnMode}" />

														</table>
													</div>
												</div>

												<div style="padding:5px;">
													<div class="imag">
														&nbsp;
													</div>
													<div class="contentDiv" style="width:98%">

														<t:dataTable id="FacilityGrid"
															value="#{pages$FacilityListPopup.gridDataList}"
															binding="#{pages$FacilityListPopup.dataTable}" rows="5"
															preserveDataModel="true" preserveSort="false"
															var="dataItem" rowClasses="row1,row2" rules="all"
															renderedIfEmpty="true" width="100%">


															<t:column id="name" sortable="true">
																<f:facet name="header">
																	<t:outputText value="#{msg['facility.facilityName']}" />
																</f:facet>

																<t:commandLink immediate="true"
																	action="#{pages$FacilityListPopup.cmdFacilityName_Click}"
																	value="#{dataItem.facilityName}" />


															</t:column>




															<t:column id="description">
																<f:facet name="header">


																	<t:outputText value="#{msg['facility.description']}" />

																</f:facet>
																<t:outputText value="#{dataItem.description}" />
															</t:column>


															<t:column id="rentValue">


																<f:facet name="header">

																	<t:outputText value="#{msg['facility.rentValue']}" />

																</f:facet>
																<t:outputText value="#{dataItem.rent}" />
															</t:column>





														</t:dataTable>


													</div>
													<div class="contentDivFooter" style="width: 100%">
														<t:dataScroller id="scroller" for="FacilityGrid"
															paginator="true" fastStep="1" paginatorMaxPages="2"
															immediate="false" style="padding-left:708px"
															paginatorTableClass="paginator"
															renderFacetsIfSinglePage="true"
															paginatorTableStyle="grid_paginator" layout="singleTable"
															paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
															paginatorActiveColumnStyle="font-size: 10;font-family: Verdana;font-weight: bold;color: #0e48ac;">

<f:facet name="first">
															<t:graphicImage url="../#{path.scroller_first}" id="lblFowner"></t:graphicImage>
														</f:facet>
														<f:facet name="fastrewind">
															<t:graphicImage url="../#{path.scroller_fastRewind}" id="lblFRowner"></t:graphicImage>
														</f:facet>
														<f:facet name="fastforward">
															<t:graphicImage url="../#{path.scroller_fastForward}" id="lblFFowner"></t:graphicImage>
														</f:facet>
														<f:facet name="last">
															<t:graphicImage url="../#{path.scroller_last}" id="lblLowner"></t:graphicImage>
														</f:facet>		
														</t:dataScroller>
													</div>
												</div>



											</h:form>

									</td>
								</tr>
							</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

		</body>
</f:view>

</html>
