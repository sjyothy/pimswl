<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript">

	
	function performClick(lnkName)
	{
		disableInputs();
		document.getElementById(lnkName).onclick();
		return true;
	}
	function disableInputs()
	{
	    var inputs = document.getElementsByTagName("INPUT");
		for (var i = 0; i < inputs.length; i++)
		{
		    if ( inputs[i] != null &&
		         ( inputs[i].type == 'submit' ||  inputs[i].type  == "button" ||  inputs[i].type  == "reset") 
		       )
		    {
		        inputs[i].disabled = true;
		    }
		}
	}
	
	function onMessageFromAmountDetailsPopUp()
	{
	  disableInputs();
	  document.getElementById("detailsFrm:onMessageFromAmountDetailsPopUp").onclick();
	}
	
	function SearchMemberPopup()
	{
   		var screen_width = screen.width;
	   	var screen_height = screen.height;
	   	var screen_top = screen.width/6;
	   	var screen_left = screen.height/5;
	   	window.open('SearchMemberPopup.jsf','_blank','width='+(screen_width-280)+',height='+(screen_height-350)+',left='+(screen_left)+',top='+( screen_top)+',scrollbars=yes,status=yes,location=no,directories=no,resizable=no');
	}
	
	function openAmountDetailsPopup()
	{
	   var screen_width   = 0.65 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   window.open('addOpenBoxAmountDetails.jsf','_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	}
	
	function openAmountDetailsHistoryPopup( id )
	{
	 		
	 		
	   var screen_width   = 0.65 * screen.width ;
	   var screen_height  = screen.height-300;
	   var screen_left    = screen.width /3.6;
	   var screen_top     = screen.height/6;
	   window.open('openBoxAmountDetailsHistory.jsf?id='+id,'_blank',
	               ' width=' +(screen_width) +
	               ',height='+(screen_height)+
	               ',left='  +(screen_left)  +
	               ',top='   +(screen_top)   +
	               ',scrollbars=yes,status=yes'
	              );
	}
	
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />
			<script type="text/javascript"
				src="../<h:outputFormat value="#{path.js_prototype}"/>"></script>



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel value="#{pages$openBox.pageTitle}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">
												<t:div id="disablingDiv" styleClass="disablingDiv"></t:div>
												<div>
													<table border="0" class="layoutTable"
														style="margin-left: 15px; margin-right: 15px;">
														<tr>
															<td>
																<h:messages></h:messages>
																<h:outputText id="errorId"
																	value="#{pages$openBox.errorMessages}" escape="false"
																	styleClass="ERROR_FONT" />
																<h:outputText id="successId"
																	value="#{pages$openBox.successMessages}" escape="false"
																	styleClass="INFO_FONT" />
																<h:inputHidden id="pageMode"
																	value="#{pages$openBox.pageMode}"></h:inputHidden>
																<h:commandLink id="onMessageFromAmountDetailsPopUp"
																	action="#{pages$openBox.onMessageFromAmountDetailsPopUp}" />


															</td>
														</tr>
													</table>

													<!-- Top Fields - Start -->
													<t:div rendered="true" style="width:100%;">
														<t:panelGrid cellpadding="1px" width="100%"
															id="basicInfoFields" cellspacing="5px" columns="4">

															<h:outputLabel styleClass="LABEL"
																value="#{msg['request.number']}:" />
															<h:inputText id="txtReqNumber" maxlength="20"
																readonly="true" styleClass="READONLY"
																value="#{pages$openBox.openBox.request.requestNumber}" />
															<h:outputLabel styleClass="LABEL"
																value="#{msg['openBox.lbl.transactionNum']}:" />
															<h:inputText id="txtOBTNumber" maxlength="20"
																readonly="true" styleClass="READONLY"
																value="#{pages$openBox.openBox.refNum}" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['commons.status']}:" />
															<h:inputText id="txtStatus" readonly="true"
																value="#{pages$openBox.englishLocale? 
																	     pages$openBox.openBox.statusEn:
																	     pages$openBox.openBox.statusAr
																	    }"
																styleClass="READONLY" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['openBox.lbl.collectionDate']}:" />
															<rich:calendar id="txtDate"
																disabled="#{!pages$openBox.showSaveButton}"
																value="#{pages$openBox.openBox.collectedOn}"
																locale="#{pages$openBox.locale}" popup="true"
																datePattern="#{pages$openBox.dateFormat}"
																showApplyButton="false" enableManualInput="false"
																cellWidth="24px" cellHeight="22px" style="height:16px" />

															<h:outputLabel styleClass="LABEL"
																value="#{msg['openBox.lbl.collectionTime']}:" />
															<t:panelGroup>
																<h:selectOneMenu id="collectionTimeHH"
																	style="width: 40px; " required="false"
																	value="#{pages$openBox.openBox.hour}">
																	<f:selectItem itemValue="00" itemLabel="hh" />
																	<f:selectItems value="#{pages$ApplicationBean.hours}" />
																</h:selectOneMenu>
																<h:selectOneMenu id="collectionTimeMM"
																	style="width: 40px; " required="false"
																	value="#{pages$openBox.openBox.minute}">
																	<f:selectItem itemValue="00" itemLabel="mm" />
																	<f:selectItems value="#{pages$ApplicationBean.minutes}" />
																</h:selectOneMenu>
															</t:panelGroup>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['openBox.lbl.amount']}:" />
															<h:panelGroup>
																<h:inputText id="txtAmount" styleClass="READONLY"
																	readonly="true" onkeyup="removeNonNumeric(this)"
																	onkeypress="removeNonNumeric(this)"
																	onkeydown="removeNonNumeric(this)"
																	onblur="removeNonNumeric(this)"
																	onchange="removeNonNumeric(this)"
																	value="#{pages$openBox.openBox.amountCollectedStr}" />
																<h:commandLink
																	action="#{pages$openBox.openAmountDetailsPopup}">
																	<h:graphicImage id="openAmountDetailsPopup"
																		style="margin-right:5px;"
																		title="#{msg['openBox.heading.amountDetails']}"
																		url="../resources/images/app_icons/Add-Payment-schedule.png"
																		rendered="#{pages$openBox.showAddAmountDetails}" />
																	<h:graphicImage id="openAmountDetailsPopupForChange"
																		style="margin-right:5px;"
																		title="#{msg['openBox.heading.amountDetails']}"
																		url="../resources/images/app_icons/application_form_edit.png"
																		rendered="#{pages$openBox.showChangeAmount}" />


																</h:commandLink>
																<h:graphicImage id="openAmountDetailsHistoryPopup"
																	style="margin-right:5px;"
																	title="#{msg['openBox.heading.amountDetailsHistory']}"
																	url="../resources/images/app_icons/manage_tender.png"
																	onclick="javaScript:openAmountDetailsHistoryPopup('#{pages$openBox.openBox.openBoxId}')"
																	rendered="#{pages$openBox.showChangeAmount}" />
															</h:panelGroup>

															<h:outputLabel styleClass="LABEL"
																value="#{msg['donationBox.lbl.boxNum']}:" />
															<h:inputText id="txtNumber" maxlength="20"
																readonly="true" styleClass="READONLY"
																value="#{pages$openBox.openBox.donationBox.boxNum}" />

															<h:panelGroup>
																<h:outputLabel styleClass="mandatory" value="*" />
																<h:outputLabel id="lblBenef" styleClass="LABEL"
																	value="#{msg['donationRequest.lbl.endowmentProgram']}" />
															</h:panelGroup>
															<h:panelGroup>
																<h:inputText id="program" maxlength="20" readonly="true"
																	styleClass="READONLY"
																	value="#{pages$openBox.openBox.endowmentProgram.progName}" />
																<h:graphicImage id="endowmentProgramsDetailspopup"
																	style="margin-right:5px;"
																	title="#{msg['openBox.lbl.endowmentProrgams']}"
																	onclick="javaScript:openManageEndowmentPrograms('#{pages$openBox.openBox.endowmentProgram.endowmentProgramId}');"
																	url="../resources/images/app_icons/view_tender.png" />
															</h:panelGroup>


														</t:panelGrid>
													</t:div>
													<!-- Top Fields - End -->
													<div class="AUC_DET_PAD">
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_mid}"/>"
																		class="TAB_PANEL_MID" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_top_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<div class="TAB_PANEL_INNER">
															<rich:tabPanel binding="#{pages$openBox.tabPanel}"
																style="width: 100%">

																<rich:tab id="tabCommittee"
																	label="#{msg['openBox.lbl.committee']}">
																	<%@ include file="tabOpenBoxCommittee.jsp"%>
																</rich:tab>

																<rich:tab id="commentsTab"
																	label="#{msg['commons.commentsTabHeading']}">
																	<%@ include file="notes/notes.jsp"%>
																</rich:tab>

																<rich:tab id="attachmentTab"
																	label="#{msg['commons.attachmentTabHeading']}">
																	<%@  include file="attachment/attachment.jsp"%>
																</rich:tab>

																<rich:tab
																	label="#{msg['contract.tabHeading.RequestHistory']}"
																	title="#{msg['commons.tab.requestHistory']}"
																	action="#{pages$openBox.onActivityLogTab}">
																	<%@ include file="../pages/requestTasks.jsp"%>
																</rich:tab>

															</rich:tabPanel>
														</div>
														<table cellpadding="0" cellspacing="0" width="100%"
															border="0">
															<tr>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_left}"/>"
																		class="TAB_PANEL_LEFT" border="0" />
																</td>
																<td width="100%" style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>"
																		class="TAB_PANEL_MID" style="width: 100%;" border="0" />
																</td>
																<td style="FONT-SIZE: 0px">
																	<IMG
																		src="../<h:outputText value="#{path.img_tab_bottom_right}"/>"
																		class="TAB_PANEL_RIGHT" border="0" />
																</td>
															</tr>
														</table>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;padding-bottom: 10x;">


															<pims:security screen="Pims.EndowMgmt.OpenBox.Save"
																action="view">

																<h:commandButton styleClass="BUTTON" id="save"
																	rendered="#{pages$openBox.showSaveButton}"
																	value="#{msg['commons.saveButton']}"
																	onclick="if (!confirm('#{msg['mems.common.alertSave']}')) return false;else performClick('detailsFrm:lnkSave');">
																</h:commandButton>
																<h:commandLink id="lnkSave"
																	action="#{pages$openBox.onSave}" />
															</pims:security>
															<pims:security screen="Pims.EndowMgmt.OpenBox.Save"
																action="view">

																<h:commandButton styleClass="BUTTON"
																	rendered="#{pages$openBox.showCompletionButton}"
																	value="#{msg['commons.complete']}"
																	onclick="if (!confirm('#{msg['receiveProperty.messages.confirmAction']}')) return false;else performClick('detailsFrm:lnkComplete');">
																</h:commandButton>
																<h:commandLink id="lnkComplete"
																	action="#{pages$openBox.onCompleted}" />

															</pims:security>



														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			+
		</body>
	</html>
</f:view>