<%-- 
  - Author: Munit Chagani
  - Date:
  - Copyright Notice:
  - @(#)
  - Description: Used for Searching an Auction
  --%>

<%@page import="com.avanza.ui.util.ResourceUtil"%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script language="JavaScript" type="text/javascript">
	function showPaymentConfigurationPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = 800;
		      var popup_height = screen_height/3+200;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('PaymentConfigurationEdit.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}    
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />

		</head>
		<body class="BODY_STYLE">
		<div class="containerDiv">
			<%
				response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
					response.setHeader("Pragma", "no-cache"); //HTTP 1.0
					response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			%>
			<!-- Header -->
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>
				</tr>
				<tr width="100%">
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" valign="top" class="divBackgroundBody">
						<table width="99%" class="greyPanelTable" cellpadding="0"
							cellspacing="0" border="0">
							<tr>
								<td class="HEADER_TD">
									<h:outputLabel value="#{msg['paymentConfiguration.ListTitle']}"
										styleClass="HEADER_FONT" />
								</td>
							</tr>
						</table>
						<table width="99%" style="margin-left: 1px;"
							class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
							border="0" height="100%">
							<tr valign="top">
								<td width="100%" height="100%">
									<div class="SCROLLABLE_SECTION AUC_SCH_SS">
										<h:form id="searchFrm" style="WIDTH: 97.6%;">
											<div style="height: 25px">
												<table border="0" class="layoutTable">
													<tr>
														<td>
															<h:outputText value="#{pages$PaymentConfiguration.errorMessages}" escape="false" styleClass="ERROR_FONT"/>
															<h:outputText value="#{pages$PaymentConfiguration.infoMessages}"  escape="false" styleClass="INFO_FONT"/>
														</td>
													</tr>
												</table>
											</div>
											<div class="MARGIN">
												<div>

													<table cellpadding="1px" cellspacing="2px">
														<tr>

															<td width="25%">
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['paymentConfiguration.procedureType']}:"></h:outputLabel>
															</td>

															<td width="25%">
																<h:selectOneMenu id="selectProcedureType" style="width:250px;"
																	value="#{pages$PaymentConfiguration.selectOneProcedureType}">
																	<f:selectItems
																		value="#{pages$ApplicationBean.procedureType}" />
																</h:selectOneMenu>

															</td>
															<td width="25%">
																<h:commandLink
																	action="#{pages$PaymentConfiguration.btnSearch_Click}">
																	<h:graphicImage id="searchPayments"
																		title="#{msg['paymentConfiguration.getPayments']}"
																		url="../resources/images/magnifier.gif" />
																</h:commandLink>
																&nbsp;
																<!-- 
																<h:commandLink 
																	onclick="javascript:clearWindow();"
																	>
																	
																	
																	<h:graphicImage id="reset"
																		title="Clear"
																		url="../resources/images/delete.gif" />
																		
																</h:commandLink>
																-->

															</td>
															<td width="25%">

															</td>
														</tr>

														<tr>
															<td width="25%">

															</td>
															<td width="25%">

															</td>
															<td style="PADDING: 5px; PADDING-TOP: 5px;">

															</td>
															<td class="BUTTON_TD">
															
															<pims:security screen="Pims.PaymentManagement.PaymentConfiguration.AddPaymentConfiguration" action="create">															
																<h:commandButton styleClass="BUTTON"
																	style="width:100px;" type="submit" size="150px"
																	value="#{msg['paymentConfiguration.actions.AddPayment']}"
																	actionListener="#{pages$PaymentConfiguration.openPaymentConfigPopup}">
																</h:commandButton>
															</pims:security>


															</td>
														</tr>
													</table>
												</div>
											</div>
											<div
												style="padding-bottom: 7px; padding-left: 10px; padding-right: 7px; padding-top: 7px;">

												<div class="contentDiv">
													<t:dataTable id="test2"
														value="#{pages$PaymentConfiguration.gridDataList}"
														binding="#{pages$PaymentConfiguration.dataTable}"
														preserveDataModel="false" preserveSort="false"
														var="dataItem" rowClasses="row1,row2" rules="all"
														renderedIfEmpty="true" rows="150"
														columnClasses="A_LEFT_GRID,A_LEFT_GRID,1,1,A_LEFT_GRID,1,1"
														width="100%">

														<t:column id="paymentTypeEn" sortable="true"
															rendered="#{pages$PaymentConfiguration.isEnglishLocale}">
															<f:facet name="header">

																<t:outputText
																	value="#{msg['paymentConfiguration.paymentType']}" />

															</f:facet>
															<t:outputText value="#{dataItem.paymentTypeEn}"
																styleClass="A_LEFT" style="white-space: normal;" />
														</t:column>
														<t:column id="paymentTypeAr" sortable="true"
															rendered="#{pages$PaymentConfiguration.isArabicLocale}">


															<f:facet name="header">

																<t:outputText
																	value="#{msg['paymentConfiguration.paymentType']}" />

															</f:facet>
															<t:outputText value="#{dataItem.paymentTypeAr}" />
														</t:column>

														<t:column id="descriptionEn" sortable="true"
															rendered="#{pages$PaymentConfiguration.isEnglishLocale}">
															<f:facet name="header">

																<t:outputText
																	value="#{msg['paymentConfiguration.description']}" />

															</f:facet>
															<t:outputText value="#{dataItem.descriptionEn}"
																styleClass="A_LEFT" style="white-space: normal;" />
														</t:column>
														<t:column id="descriptionAr" sortable="true"
															rendered="#{pages$PaymentConfiguration.isArabicLocale}">


															<f:facet name="header">

																<t:outputText
																	value="#{msg['paymentConfiguration.description']}" />

															</f:facet>
															<t:outputText value="#{dataItem.descriptionAr}" />
														</t:column>

														<t:column id="nature" sortable="true">
															<f:facet name="header">

																<t:outputText
																	value="#{msg['paymentConfiguration.paymentNature']}" />


															</f:facet>

															<t:outputText value="Percentage"
																rendered="#{dataItem.isPercentPayment}" />
															<t:outputText value="Fixed"
																rendered="#{dataItem.isFixedPayment}" />
														</t:column>


														<t:column id="amount" sortable="true">
															<f:facet name="header">


																<t:outputText value="#{msg['paymentConfiguration.Amount']}" />

															</f:facet>
															<t:outputText value="#{dataItem.amount}"
																styleClass="A_RIGHT_NUM" >
																<f:convertNumber pattern="#{pages$PaymentConfiguration.numberFormat}"/>
																
																</t:outputText>
														</t:column>


														<t:column id="deletebtn">
															<f:facet name="header">
																<t:outputText
																	value="#{msg['paymentConfiguration.Actions']}">

																</t:outputText>
															</f:facet>
															<pims:security
																screen="Pims.PaymentManagement.PaymentConfiguration.EditPaymentConfiguration"
																action="create">
																<t:commandLink
																	actionListener="#{pages$PaymentConfiguration.openPaymentConfigPopup}">&nbsp;
														 <h:graphicImage id="editIcon"
																		title="#{msg['commons.edit']}"
																		url="../resources/images/edit-icon.gif" />&nbsp;
														  </t:commandLink>
															</pims:security>
														  &nbsp;
														  &nbsp;
														  &nbsp;
														  <pims:security
																screen="Pims.PaymentManagement.PaymentConfiguration.DeletePaymentConfiguration"
																action="create">
																<t:commandLink
																	onclick="if (!confirm('#{msg['paymentConfiguration.deleteConfirmation']}')) return false"
																	action="#{pages$PaymentConfiguration.cmdDelete_Click}">&nbsp;
														 <h:graphicImage id="deleteIcon"
																		title="#{msg['commons.delete']}"
																		url="../resources/images/delete.gif" />&nbsp;
														  </t:commandLink>
															</pims:security>
														</t:column>
														</t:dataTable>
												</div>

											</div>
										</h:form>
									</div>




								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</body>
	</html>
</f:view>