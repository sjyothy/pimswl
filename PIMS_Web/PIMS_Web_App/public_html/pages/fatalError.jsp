

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages" var="msg"/>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.pims-resource-path" var="path"/>


  <html dir="${sessionScope.CurrentLocale.dir}" lang="${sessionScope.CurrentLocale.languageCode}">
  <head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}"/>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<meta http-equiv="description" content="This is my page">
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_amaf}"/>"/>
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_simple}"/>"/>			
			<link rel="stylesheet" type="text/css" href="../<h:outputFormat value="#{path.css_table}"/>"/>						


			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table_ff}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->
			
 	</head>
    
    <body class="BODY_STYLE">
    <div class="containerDiv">
    

    <!-- Header --> 
    <table width="100%" cellpadding="0" cellspacing="0"  border="0">
    <tr>
    <td colspan="2">
        <jsp:include page="header.jsp"/>
    </td>
    </tr>

<tr width="100%">
    <td class="divLeftMenuWithBody" width="17%">
        <jsp:include page="leftmenu.jsp"/>
    </td>
    <td width="83%" valign="top" class="divBackgroundBody">
        <table width="99%" class="greyPanelTable" cellpadding="0" cellspacing="0"  border="0">
        <tr>
        <c:choose>
			<c:when test="${sessionScope.CurrentLocale.languageCode eq 'en'}">
				 <td background ="../resources/images/Grey panel/Grey-Panel-left-1.jpg" height="38" style="background-repeat:no-repeat;" width="100%">
	             <!-- <font style="font-family:Trebuchet MS;font-weight:regular;font-size:19px;margin-left:15px;top:30px;">Exception Occoured Contact Support Staff</font>  -->
	             <font style="font-family:Trebuchet MS;font-weight:regular;font-size:19px;margin-left:15px;top:30px;">Under Development</font> 
                <!--<IMG src="../resources/images/Grey panel/Grey-Panel-left-1.jpg"/>-->
			</c:when>
			<c:otherwise>
				<td width="100%">
                 <IMG src="../resources/images/ar/Detail/Grey Panel/Grey-Panel-right-1.jpg"></img>
			</c:otherwise>
		</c:choose>
           </td>           
            <td width="100%">
        &nbsp;
            </td>
	</tr>
        </table>
        <table width="99%" height="90%" class="greyPanelMiddleTable" cellpadding="0" cellspacing="0" border="0" >
          <tr valign="top">
             <td height="100%" valign="top" nowrap="nowrap" background ="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg" width="1">
             </td>    
             <!--Use this below only for desiging forms Please do not touch other sections -->
             
    
             <td width="500px" height="100%" valign="top" nowrap="nowrap">
             	<div class="SCROLLABLE_SECTION" style="white-space:normal;width:815px;height:450px;padding-left:10px;">
	             <!--   ${sessionScope.exceptionMessage} -->
	             Sorry for inconvenience, the feature is currently under development. 
	            </div>
             </td>
         </tr>
        </table>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
       <tr><td class="footer"><h:outputLabel value="#{msg['commons.footer.message']}" /></td></tr>
    </table>
        </td>
    </tr>
    </table>
    </div>
    </body>
</html>
</f:view>
