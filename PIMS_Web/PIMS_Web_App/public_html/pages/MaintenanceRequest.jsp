<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
function showUploadPopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+150;
		      var popup_height = screen_height/3-10;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('attachFile.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
        
		function showAddNotePopup()
		{
		      var screen_width = screen.width;
		      var screen_height = screen.height;
		      var popup_width = screen_width/3+120;
		      var popup_height = screen_height/3-80;
		      var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;
		      
		      var popup = window.open('notes/addNote.jsf','_blank','width='+popup_width+',height='+popup_height+',left='+ leftPos +',top=' + topPos + ',scrollbars=no,status=no,resizable=no,titlebar=no,dialog=yes');
		      popup.focus();
		}
</script>

<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />
	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<title>PIMS</title>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table_ff}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />



			<!--[if IE 7]>
			<link href="../<h:outputFormat value="#{path.css_table}"/>" type="text/css" rel="stylesheet" />
			<![endif]-->

		</head>

		<script language="javascript" type="text/javascript">
		
		function openRequestHistoryPopUp()
        {
           var screen_width = screen.width;
	       var screen_height = screen.height;
	        window.open('RequestHistoryPopUp.jsf?contractId='+document.getElementById("formRequest:hdnContractId").value,'_blank','width='+(screen_width-10)+',height='+(screen_height-460)+',left=0,top=40,scrollbars=yes,status=yes'); 
        }
		function openContractHistoryPopUp()
        {
           var screen_width = screen.width;
	       var screen_height = screen.height;
	        window.open('ContractHistoryPopUp.jsf?contractId='+document.getElementById("formRequest:hdnContractId").value,'_blank','width='+(screen_width-10)+',height='+(screen_height-460)+',left=0,top=40,scrollbars=yes,status=yes'); 
        }
	function showPopup(personType)
	{
	   var screen_width = 1024;
	   var screen_height = 470;
	   //window.open('TenantsList.jsf?modeSelectOnePopUp=modeSelectOnePopUp','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
	    window.open('SearchPerson.jsf?persontype='+personType+'&viewMode=popup','_blank','width='+(screen_width-200)+',height='+(screen_height)+',left=120,top=150,scrollbars=no,status=yes');
	}
	
	function showPartnerOccupierPopUp(persontype)
	{
	var screen_width = screen.width;
	   var screen_height = screen.height;
	   window.open('SearchPerson.jsf?persontype='+persontype+'&viewMode=popup&selectMode=many' ,'_blank','width='+(screen_width-10)+',height='+(screen_height-350)+',left=0,top=40,scrollbars=no,status=yes');
	
	}
	function showPersonManagerPopUp(persontype,displaycontrolname,hdncontrolname,hdndisplaycontrolname,clickable)
	{
	var screen_width = screen.width;
	   var screen_height = screen.height;
	  
	   
	   if(clickable=='true')
	   {
	   
	    //window.open('ContractPerson.jsf?persontype='+persontype+'&displaycontrolname='+displaycontrolname+'&hdncontrolname='+hdncontrolname+'&hdndisplaycontrolname='+hdndisplaycontrolname ,'_blank','width='+(screen_width-10)+',height='+(screen_height-350)+',left=0,top=40,scrollbars=no,status=yes');
	   window.open('SearchPerson.jsf?persontype='+persontype+'&viewMode=popup','_blank','width='+(screen_width-10)+',height='+(screen_height-300)+',left=0,top=40,scrollbars=no,status=yes');
	   }
	}
	
	
	function showPaymentSchedulePopUp()
	{
	 var screen_width = screen.width;
	   var screen_height = screen.height;
	   
	   window.open('PaymentSchedule.jsf?totalContractValue='+document.getElementById("formRequest:hdntotalContract").value,'_blank','width='+(screen_width-500)+',height='+(screen_height-300)+',left=300,top=250,scrollbars=yes,status=yes');
	   
	}
    
	function populatePerson(hdnPersonType,personName,personId,passportNumber,cellNumber,designation,isCompany)
	{
		    if(hdnPersonType=='TENANT')
		    {
			 //document.getElementById("formRequest:txtTenantName").value=personName;
			 document.getElementById("formRequest:hdntenantId").value=personId;
	         //document.getElementById("formRequest:hdntenanttypeId").value=typeId;
	         
	        }
	        else if(hdnPersonType=='SPONSOR')
	        {
	         document.getElementById("formRequest:sponsorPerson").value=personName;
			 document.getElementById("formRequest:hdnSponsorPersonId").value=personId+":"+personName;
	         
	        }
	        else if(hdnPersonType=='MANAGER')
	        {
	         document.getElementById("formRequest:managerPerson").value=personName;
			 document.getElementById("formRequest:hdnManagerPersonId").value=personId+":"+personName;
	         
	        }
	        else if(hdnPersonType=='APPLICANT')
	        {
	         document.getElementById("formRequest:txtName").value=personName;
			 document.getElementById("formRequest:hdnApplicantId").value=personId;
	         //document.getElementById("formRequest:hdnApplicantPerson").value=personName;
	       
	        }
	        document.forms[0].submit();
	       
	}
	function deleteContractPerson(PersonType)
	{
	
	        if(PersonType=='SPONSOR')
	        {
	         document.getElementById("formRequest:sponsorPerson").value="";
			 document.getElementById("formRequest:hdnSponsorPersonId").value="";
	         document.getElementById("formRequest:hdnSponsorPerson").value="";
	        }
	        else if(PersonType=='MANAGER')
	        {
	         document.getElementById("formRequest:managerPerson").value="";
			 document.getElementById("formRequest:hdnManagerPersonId").value="";
	         document.getElementById("formRequest:hdnManagerPerson").value="";
	         
	        }
	}
    
	function populateUnit(unitId,unitNum,usuageTypeId,unitRentAmount,unitUsuageType,propertyCommercialName,unitAddress,unitStatusId,unitStatus)
	{
	
	//document.getElementById("formRequest:hdnunitRefNumber").value=unitNum;
	document.getElementById("formRequest:hdnunitRentAmount").value=unitRentAmount;
	document.getElementById("formRequest:hdnunitId").value=unitId;
	//document.getElementById("formRequest:hdnunitUsuageTypeId").value=usuageTypeId;
	//document.getElementById("formRequest:hdnunitUsuageType").value=unitUsuageType;
	//document.getElementById("formRequest:hdnPropertyCommercialName").value=propertyCommercialName;
	//document.getElementById("formRequest:hdnUnitStatusId").value=unitStatusId;
	//document.getElementById("formRequest:hdnPropertyAddress").value=unitAddress;
	//document.getElementById("formRequest:hdnunitStatusDesc").value=unitStatus;
	}
	function populateAuctionUnit(unitId,unitNum,usuageTypeId,unitRentAmount,auctionUnitId,auctionNumber,unitAddress,propertyCommercialName,unitUsuageType,unitStatus,unitStatusId,bidderId)
	{
	 
	 //document.getElementById("formRequest:hdnunitRefNumber").value=unitNum;
	 document.getElementById("formRequest:hdnunitRentAmount").value=unitRentAmount;
	 document.getElementById("formRequest:hdnunitId").value=unitId;
	 document.getElementById("formRequest:hdntenantId").value=bidderId;
	 //document.getElementById("formRequest:hdnunitUsuageTypeId").value=usuageTypeId;
	//document.getElementById("formRequest:hdnunitUsuageType").value=unitUsuageType;
	//document.getElementById("formRequest:hdnPropertyCommercialName").value=propertyCommercialName;
	//document.getElementById("formRequest:hdnUnitStatusId").value=unitStatusId;
	//document.getElementById("formRequest:hdnPropertyAddress").value=unitAddress;
	//document.getElementById("formRequest:hdnunitStatusDesc").value=unitStatus;
	
	}
	
	  function isSponsorMangerDisplay()
      {
      
        return document.getElementById("formRequest:hdnSponsorMangerDisplay").value;
      }
      
    </script>
		<style type="text/css">
.test {
	width: 22px;
}
</style>

		<!-- Header -->

		<body class="BODY_STYLE">

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>

					<td colspan="2">
						<jsp:include page="header.jsp" />
					</td>

				</tr>
				<tr>
					<td class="divLeftMenuWithBody" width="17%">
						<jsp:include page="leftmenu.jsp" />
					</td>
					<td width="83%" height="45PX" valign="top"
						class="divBackgroundBody">
						<h:form id="formRequest" style="width:99.7%"
							enctype="multipart/form-data">
							<table width="99%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD" style="width: 70%;">
										<h:outputLabel value="#{pages$LeaseContract.pageTitle}"
											styleClass="HEADER_FONT" />
									</td>
									<td>
										&nbsp;
									</td>

									<td width="30%">


									</td>
								</tr>
							</table>
							<table width="99%" class="greyPanelMiddleTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr valign="top">
									<td height="100%" valign="top" nowrap="nowrap"
										background="../resources/images/Grey panel/Grey-Panel-Left-vertical-Middle-center.jpg"
										width="1">
									</td>
									<td height="450PX" valign="top" nowrap="nowrap">
										<div class="SCROLLABLE_SECTION" style="width: 850px;">

											<table border="0" class="layoutTable" width="100%"
												style="margin-left: 15px; margin-right: 15px;">
												<tr>
													<td>
														<h:outputText id="successmsg" escape="false"
															styleClass="INFO_FONT"
															value="#{pages$MaintenanceRequest.successMessages}" />
														<h:outputText id="errormsg" escape="false"
															styleClass="ERROR_FONT"
															value="#{pages$MaintenanceRequest.errorMessages}" />
														<h:inputHidden id="hdnContractId"
															value="#{pages$MaintenanceRequest.contractId}" />
														<h:inputHidden id="hdnContractCreatedOn"
															value="#{pages$MaintenanceRequest.contractCreatedOn}" />
														<h:inputHidden id="hdnContractCreatedBy"
															value="#{pages$MaintenanceRequest.contractCreatedBy}" />
														<h:inputHidden id="hdntenantId"
															value="#{pages$MaintenanceRequest.hdnTenantId}" />
														<h:inputHidden id="hdnunitRentAmount"
															value="#{pages$MaintenanceRequest.unitRentAmount}" />
														<h:inputHidden id="hdnManagerPersonId"
															value="#{pages$MaintenanceRequest.hdnManagerId}" />
														<h:inputHidden id="hdntotalContract"
															value="#{pages$MaintenanceRequest.txttotalContractValue}" />
														<h:inputHidden id="pageMode"
															value="#{pages$MaintenanceRequest.pageMode}" />
														<h:inputHidden id="hdnunitId"
															value="#{pages$MaintenanceRequest.hdnUnitId}" />
														<h:inputHidden id="hdnSponsorPersonId"
															value="#{pages$MaintenanceRequest.hdnSponsorId}" />
														<h:inputHidden id="hdnApplicantId"
															value="#{pages$MaintenanceRequest.hdnApplicantId}" />
													</td>
												</tr>
											</table>

											<div class="MARGIN" style="width: 99%; margin-bottom: 0px;">

												<div id="otherDivsForTasks">
												<rich:panel id="rpForOtherTasks"  binding="#{pages$MaintenanceRequest.taskPanel}"  >
												<t:panelGrid id="applicationDetailsTable123" cellpadding="1px" width="70%" 
												cellspacing="5px" styleClass="DETAIL_SECTION_INNER" columns="4" 
												columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2" >
																
																<h:outputLabel styleClass="LABEL" value="Maintenance Contracts"/>
																<h:selectOneMenu id="cmbMaintenanceContracts" style="width: 192px;" required="false" 
																value="#{pages$MaintenanceRequest.svContractId}" 
																readonly="#{!pages$MaintenanceRequest.isPageModeApprovalRequired}" >
																	<f:selectItem itemValue="0" itemLabel="#{msg['commons.none']}" />
																	
																	<f:selectItems
																		value="#{pages$MaintenanceRequest.svContractSelectList}" />
																 </h:selectOneMenu>
																
																<h:outputLabel styleClass="LABEL" value="Priority" />
																<h:selectOneMenu id="cmbPriority" style="width: 192px;" required="false"
																value="#{pages$MaintenanceRequest.priorityValue}" 
																readonly="#{!pages$MaintenanceRequest.isPageModeApprovalRequired}" >
																<f:selectItem itemValue="0"
																	itemLabel="#{msg['commons.All']}" />
																</h:selectOneMenu>
																
																
																
																<h:outputLabel styleClass="LABEL" value="Contractor" />
																<h:inputText maxlength="20" id="txtContractors"
																 value="#{pages$MaintenanceRequest.contractorName}"
																readonly="true" style="width:85%; height: 18px" />
																
																<h:outputLabel styleClass="LABEL" value="Estimated Cost"></h:outputLabel> 
																<h:inputText maxlength="20" id="txtEstimatedCost"  value="#{pages$MaintenanceRequest.estimatedCost}" 
																readonly="#{!pages$MaintenanceRequest.isPageModeOnSiteVisit}" style="width:85%; height: 18px"  />
																
																
																
																<h:outputLabel styleClass="LABEL" value="Visit Date"   /> 
																<rich:calendar id="txtVisitDate"   
																	disabled="#{!pages$MaintenanceRequest.isPageModeOnSiteVisit}"
																	value="#{pages$MaintenanceRequest.visitDate}"
																	locale="#{pages$requestSearch.locale}" popup="true"
																	datePattern="#{pages$MaintenanceRequest.dateFormat}"
																	showApplyButton="false" enableManualInput="false"
																	cellWidth="24px" cellHeight="22px"
																	style="width:186px; height:16px" />
																
																<h:outputLabel styleClass="LABEL" value="Engineer Name"   />
																<h:inputText maxlength="20" id="txtEngineerName"  style="width:85%; height: 18px" 
																value="#{pages$MaintenanceRequest.engineerName}"
																readonly="#{!pages$MaintenanceRequest.isPageModeOnSiteVisit}" 
																 />
																
																<h:outputLabel styleClass="LABEL" value="Actual Cost"  />
																<h:inputText maxlength="20" id="txtActualCost"  style="width:85%; height: 18px"
																value="#{pages$MaintenanceRequest.actualCost}" 
																readonly="#{!pages$MaintenanceRequest.isPageModeOnFollowUp}" 
																/>
																
																<h:outputLabel styleClass="LABEL" style="width: 160px;" value="Remarks" />
																<h:inputTextarea id="txtRemarks" style="width:90%; height: 40px"
																value="#{pages$MaintenanceRequest.remarksFor}" 
																readonly="#{!pages$MaintenanceRequest.isPageModeOnFollowUp && !pages$MaintenanceRequest.isPageModeOnSiteVisit}"
																   />
																 <t:panelGroup colspan="4">
																 <t:panelGrid columns="6" align="right">
																	 														
														<pims:security
															screen="Pims.LeaseContractManagement.LeaseContract.CompleteAmendLease"
															action="create">
															<h:commandButton type="submit" styleClass="BUTTON"
																action="#{pages$MaintenanceRequest.btnCompleteAmend_Click}"
																
																
																value="#{msg['commons.complete']}" />
														</pims:security>
														<pims:security
															screen="Pims.LeaseContractManagement.LeaseContract.ApproveAmendLease"
															action="create">

															<h:commandButton type="submit" styleClass="BUTTON"
																action="#{pages$MaintenanceRequest.btnSiteVisit_Click}"
																rendered="#{pages$MaintenanceRequest.isPageModeApprovalRequired}"
																value="Need Site Visit" />
															<h:commandButton type="submit" styleClass="BUTTON"
																action="#{pages$MaintenanceRequest.btnApproveAmend_Click}"
																rendered="#{pages$MaintenanceRequest.isPageModeApprovalRequired}"
																value="#{msg['commons.approve']}" />

															<h:commandButton type="submit" styleClass="BUTTON"
																action="#{pages$MaintenanceRequest.btnRejectAmend_Click}"
																rendered="#{pages$MaintenanceRequest.isPageModeApprovalRequired}"
																value="#{msg['commons.reject']}" />
														</pims:security>
														
														<pims:security
															screen="Pims.LeaseContractManagement.LeaseContract.CompleteAmendLease"
															action="create">
															<h:commandButton type="submit" styleClass="BUTTON"
																action="#{pages$MaintenanceRequest.btnSaveSiteVisit_Click}"
																rendered="#{pages$MaintenanceRequest.isPageModeOnSiteVisit}"
																value="#{msg['commons.save']}"  title="Save Site Visit"/>
																
															<h:commandButton type="submit" styleClass="BUTTON" title="Complete after the Site Visit"
																action="#{pages$MaintenanceRequest.btnCompleteTask_Click}"
																rendered="#{pages$MaintenanceRequest.isPageModeOnSiteVisit}"
																value="#{msg['commons.complete']}" />
														</pims:security>
														
														<pims:security
															screen="Pims.LeaseContractManagement.LeaseContract.CompleteAmendLease"
															action="create">
															
															<h:commandButton type="submit" styleClass="BUTTON"
																action="#{pages$MaintenanceRequest.btnSaveFollowup_Click}"
																rendered="#{pages$MaintenanceRequest.isPageModeOnFollowUp}"
																value="#{msg['commons.save']}"  title="Save Followup"/>
																
															<h:commandButton type="submit" styleClass="BUTTON" title="Complete after Followup"
																action="#{pages$MaintenanceRequest.btnCompleteRequestTask_Click}"
																rendered="#{pages$MaintenanceRequest.isPageModeOnFollowUp}"
																value="#{msg['commons.complete']}" />
														</pims:security>
														
														
																	 </t:panelGrid> 
																	 </t:panelGroup>
												
												</t:panelGrid>
												</rich:panel>
												<!-- 
												<TABLE>
												<tr>
												<td>
												
												</td>
												<td>
												
												</td>
												<td>
												
												</td>
												<td>
												
												</td>
												<tr>
												</TABLE> -->
												
																								
												
												
												
												</div>
												<rich:tabPanel id="tabPanel" style="width:95%;height:320px;"
													headerSpacing="0">

													<rich:tab id="appDetailsTab"
														title="#{msg['commons.tab.applicationDetails']}"
														label="#{msg['contract.tabHeading.ApplicationDetails']}">

					<script type="text/javascript">
						function   showPersonPopup()
						{
						   var screen_width = screen.width;
						   var screen_height = screen.height;
						   window.open('SearchPerson.jsf?persontype=APPLICANT&viewMode=popup','_blank','width='+(screen_width-10)+',height='+(screen_height-270)+',left=0,top=40,scrollbars=yes,status=yes');
						    
						}
					</script>

														<t:div styleClass="DETAIL_SECTION" style="width:100%">
															<t:panelGrid id="applicationDetailsTable"
																cellpadding="1px" width="100%" cellspacing="5px"
																styleClass="DETAIL_SECTION_INNER" columns="4"
																columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['applicationDetails.number']}" />
																<h:inputText maxlength="20" id="txtApplicationNum"
																	readonly="true"
																	value="#{pages$MaintenanceRequest.applicationNo}"
																	binding="#{pages$MaintenanceRequest.txtApplicationNo}"
																	style="width:85%; height: 18px"></h:inputText>
																<h:outputLabel styleClass="LABEL"
																	value="#{msg['applicationDetails.status']}"></h:outputLabel>

																<h:selectOneMenu id="requestStatuses"
																	binding="#{pages$MaintenanceRequest.requestStatusCombo}"
																	style="width: 192px;" required="false" readonly="true"
																	value="#{pages$MaintenanceRequest.statusId}">
																	<f:selectItems
																		value="#{pages$ApplicationBean.requestStatusList}" />
																</h:selectOneMenu>

																<h:outputLabel styleClass="LABEL" style="width: 160px;"
																	value="Date"></h:outputLabel>
																<rich:calendar id="txtApplicationDate"
																	value="#{pages$requestSearch.fromDate}"
																	locale="#{pages$requestSearch.locale}" popup="true"
																	datePattern="#{pages$MaintenanceRequest.dateFormat}"
																	showApplyButton="false" enableManualInput="false"
																	cellWidth="24px" cellHeight="22px"
																	style="width:186px; height:16px" />

																<h:outputLabel styleClass="LABEL" style="width: 160px;"
																	value="#{msg['application.source']}"></h:outputLabel>
																<h:selectOneMenu id="requestSources"
																	readonly="#{pages$MaintenanceRequest.applicationDetailsReadonlyMode}"
																	style="width: 192px;" required="false"
																	value="#{pages$MaintenanceRequest.source}">
																	<f:selectItem itemValue="1" itemLabel="CSC" />
																	<f:selectItem itemValue="2" itemLabel="Fax" />
																	<f:selectItem itemValue="3" itemLabel="Phone" />
																	<f:selectItem itemValue="4" itemLabel="Email" />
																</h:selectOneMenu>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['applicationDetails.applicantName']}"></h:outputLabel>
																<t:panelGrid id="gridApplicant" columns="2" width="100%"
																	cellpadding="0" cellspacing="1"
																	columnClasses="LEASE_CONTRACT_BASIC_INFO_SPONSORMANGER_GRID_C1,LEASE_CONTRACT_BASIC_INFO_SPONSORMANGER_GRID_C2">

																	<h:inputText id="txtName" readonly="true"
																		style="width:190px; height: 18px"
																		value="#{pages$MaintenanceRequest.name}"
																		binding="#{pages$MaintenanceRequest.txtName}"></h:inputText>
																	<h:commandLink onclick="showPersonPopup();"
																		rendered="#{!pages$MaintenanceRequest.applicationDetailsReadonlyMode}">
																		<h:graphicImage id="imgPerson"
																			style="MARGIN: 0px 0px -4px"
																			url="../resources/images/app_icons/Add-Person.png"></h:graphicImage>
																	</h:commandLink>


																</t:panelGrid>

																<h:outputLabel id="lblAppType"
																	value="#{msg['applicationDetails.maintenanceOn']}"></h:outputLabel>
																<h:selectOneRadio id="rdMaintenanceOn"
																	value="#{pages$MaintenanceRequest.maintenanceOn}">
																	<f:selectItem itemLabel="Unit" itemValue="1" />
																	<f:selectItem itemLabel="Property" itemValue="2" />
																</h:selectOneRadio>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['receiveProperty.rentUnitNumber']}"></h:outputLabel>
																<t:panelGrid id="gridApplicant5555" columns="2"
																	width="100%" cellpadding="0" cellspacing="1"
																	columnClasses="LEASE_CONTRACT_BASIC_INFO_SPONSORMANGER_GRID_C1,LEASE_CONTRACT_BASIC_INFO_SPONSORMANGER_GRID_C2">
																	<h:inputText id="txtUnitNumber"
																		style="width:85%; height: 18px"
																		styleClass="A_RIGHT_NUM" maxlength="15"
																		readonly="true"
																		binding="#{pages$MaintenanceRequest.txtUnitNo}"
																		value="#{pages$MaintenanceRequest.unitRefNum}"></h:inputText>
																	
																		
																		<h:commandLink  action="#{pages$MaintenanceRequest.openUnitsPopUp}" title="#{msg['units.unit']}"
																		rendered="#{!pages$MaintenanceRequest.applicationDetailsReadonlyMode}">
																		<h:graphicImage id="imgPerson2"
																			style="MARGIN: 0px 0px -4px"
																			url="../resources/images/app_icons/Add-Person.png"></h:graphicImage>
																		</h:commandLink>
																</t:panelGrid>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.replaceCheque.propertyName']}"></h:outputLabel>
																<h:inputText id="txtPropertyName" readonly="true"
																	style="width:85%; height: 18px"
																	binding="#{pages$MaintenanceRequest.txtPropertyName}"
																	value="#{pages$MaintenanceRequest.propertyName}"></h:inputText>

																<h:outputLabel styleClass="LABEL"
																	value="#{msg['commons.replaceCheque.tenantName']}"></h:outputLabel>

																<h:inputText id="txtTenantName" readonly="true"
																	style="width:85%; height: 18px"
																	binding="#{pages$MaintenanceRequest.txtInputTenantName}"
																	value="#{pages$MaintenanceRequest.tenantName}"></h:inputText>

																<h:outputLabel id="lblOwnershipType"
																	value="#{msg['applicationDetails.ownershipType']}"></h:outputLabel>

																<h:inputText id="txtOwnershipType" readonly="true"
																	style="width:85%; height: 18px"
																	
																	value="#{pages$MaintenanceRequest.ownershipType}" 
																	 ></h:inputText>
																

																<h:outputLabel id="lblWorkType"
																	value="#{msg['applicationDetails.workType']}"></h:outputLabel>
																<h:selectOneMenu id="selWorkTypeId"
																	readonly="#{pages$MaintenanceRequest.applicationDetailsReadonlyMode}"
																	value="#{pages$MaintenanceRequest.workType}"
																	style="width: 192px;" required="false">
																	<f:selectItem itemValue="0"
																		itemLabel="#{msg['commons.none']}" />
																		<f:selectItems
																		value="#{pages$MaintenanceRequest.workTypeSelectList}" />
																</h:selectOneMenu>

																<h:outputLabel id="lblMaintenanceType"
																	value="#{msg['applicationDetails.maintenanceType']}"></h:outputLabel>
																<h:selectOneMenu id="selMaintenanceType"
																	readonly="#{pages$MaintenanceRequest.applicationDetailsReadonlyMode}"
																	value="#{pages$MaintenanceRequest.maintenanceType}"
																	style="width: 192px;" required="false">
																	<f:selectItem itemValue="0"
																		itemLabel="#{msg['commons.none']}" />
																		<f:selectItems
																		value="#{pages$MaintenanceRequest.maintenanceTypeSelectList}" />
																</h:selectOneMenu>

																<h:outputLabel styleClass="LABEL" style="width: 160px;"
																	value="#{msg['maintenanceRequest.description']}"></h:outputLabel>
																<h:inputTextarea id="txtDesc"
																	style="width:100%; height: 40px"
																	readonly="#{pages$MaintenanceRequest.applicationDetailsReadonlyMode}"
																	value="#{pages$MaintenanceRequest.description}"
																	binding="#{pages$MaintenanceRequest.txtDescription}"></h:inputTextarea>

															</t:panelGrid>
														</t:div>

													</rich:tab>
													

													
													<rich:tab id="tabPaymentSchedule"
														binding="#{pages$MaintenanceRequest.tabPaymentTerms}"
														action="#{pages$MaintenanceRequest.tabPaymentTerms_Click}"
														title="#{msg['contract.paymentTerms']}"
														label="#{msg['contract.paymentTerms']}">
														<t:div style="width:100%;MARGIN: 0px 4px 0px 0px;">
															<t:panelGrid width="100%" border="0" columns="3"
																columnClasses="BUTTON_TD">
																<t:panelGroup colspan="12">


																	<h:commandButton id="pmt_sch_add" styleClass="BUTTON"
																		value="#{msg['contract.AddpaymentSchedule']}"
																		
																		action="#{pages$MaintenanceRequest.btnAddPayments_Click}"
																		rendered="false"
																		style="width:150px;" />
																	<h:commandButton id="pmt_sch_gen" styleClass="BUTTON"
																		value="#{msg['contract.paymentSchedule.GeneratePayments']}"
																		rendered="false"
																		style="width:150px;"
																		action="#{pages$MaintenanceRequest.btnGenPayments_Click}"
																		 />
																</t:panelGroup>
															</t:panelGrid>
														</t:div>
														<t:div styleClass="contentDiv" style="width:97.1%">
															<t:dataTable id="tbl_PaymentSchedule" rows="7"
																width="100%"
																value="#{pages$MaintenanceRequest.paymentScheduleDataList}"
																binding="#{pages$MaintenanceRequest.paymentScheduleDataTable}"
																preserveDataModel="true" var="paymentScheduleDataItem"
																rowClasses="row1,row2" rules="all"
																renderedIfEmpty="true">
																<t:column id="select" style="width:5%;"
																	rendered="#{(pages$MaintenanceRequest.isContrctStatusActive || pages$MaintenanceRequest.isContractStatusApproved) && (pages$MaintenanceRequest.isPageModeAdd || pages$MaintenanceRequest.isPageModeUpdate)}">
																	<f:facet name="header">
																		<t:outputText id="selectTxt"
																			value="#{msg['commons.select']}" />
																	</f:facet>
																	<t:selectBooleanCheckbox id="selectChk"
																		value="#{paymentScheduleDataItem.selected}"
																		rendered="#{paymentScheduleDataItem.isReceived == 'N'  && paymentScheduleDataItem.statusId==pages$MaintenanceRequest.paymentSchedulePendingStausId && paymentScheduleDataItem.paymentScheduleId>0}" />
																</t:column>
																<t:column id="col_PaymentNumber" style="width:15%;"
																	rendered="#{!pages$MaintenanceRequest.isPageModeAdd}">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['paymentSchedule.paymentNumber']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{paymentScheduleDataItem.paymentNumber}" />
																</t:column>
																<t:column id="colTypeEn" style="width:8%;"
																	rendered="#{pages$MaintenanceRequest.isEnglishLocale}">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['paymentSchedule.paymentType']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" title=""
																		value="#{paymentScheduleDataItem.typeEn}" />
																</t:column>
																<t:column id="colTypeAr" style="width:8%;"
																	rendered="#{pages$MaintenanceRequest.isArabicLocale}">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['paymentSchedule.paymentType']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" title=""
																		value="#{paymentScheduleDataItem.typeAr}" />
																</t:column>
																<t:column id="colDesc">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['paymentSchedule.description']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" title=""
																		value="#{paymentScheduleDataItem.description}" />
																</t:column>
																<t:column id="colDueOn" style="width:8%;">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['paymentSchedule.paymentDueOn']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT"
																		value="#{paymentScheduleDataItem.paymentDueOn}">
																		<f:convertDateTime
																			pattern="#{pages$LeaseContract.dateFormat}"
																			timeZone="#{pages$LeaseContract.timeZone}" />
																	</t:outputText>
																</t:column>
																<t:column id="colPaymentDate" style="width:8%;">
																	<f:facet name="header">

																		<t:outputText
																			value="#{msg['paymentSchedule.paymentDate']}">
																			<f:convertDateTime
																				pattern="#{pages$MaintenanceRequest.dateFormat}"
																				timeZone="#{pages$MaintenanceRequest.timeZone}" />
																		</t:outputText>
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" title=""
																		value="#{paymentScheduleDataItem.paymentDate}">
																		<f:convertDateTime
																			pattern="#{pages$MaintenanceRequest.dateFormat}"
																			timeZone="#{pages$MaintenanceRequest.timeZone}" />
																	</t:outputText>
																</t:column>

																<t:column id="colModeEn" style="width:8%;"
																	rendered="#{pages$MaintenanceRequest.isEnglishLocale}">


																	<f:facet name="header">

																		<t:outputText
																			value="#{msg['paymentSchedule.paymentMode']}" />

																	</f:facet>
																	<t:outputText styleClass="A_LEFT" title=""
																		value="#{paymentScheduleDataItem.paymentModeEn}" />
																</t:column>
																<t:column id="colModeAr" style="width:8%;"
																	rendered="#{pages$MaintenanceRequest.isArabicLocale}">


																	<f:facet name="header">

																		<t:outputText
																			value="#{msg['paymentSchedule.paymentMode']}" />

																	</f:facet>
																	<t:outputText styleClass="A_LEFT" title=""
																		value="#{paymentScheduleDataItem.paymentModeAr}" />
																</t:column>
																<t:column id="colStatusEn"
																	rendered="#{pages$LeaseContract.isEnglishLocale}">


																	<f:facet name="header">

																		<t:outputText
																			value="#{msg['paymentSchedule.paymentStatus']}" />

																	</f:facet>
																	<t:outputText styleClass="A_LEFT" title=""
																		value="#{paymentScheduleDataItem.statusEn}" />
																</t:column>
																<t:column id="colStatusAr"
																	rendered="#{pages$MaintenanceRequest.isArabicLocale}">


																	<f:facet name="header">

																		<t:outputText
																			value="#{msg['paymentSchedule.statusAr']}" />

																	</f:facet>
																	<t:outputText styleClass="A_LEFT" title=""
																		value="#{paymentScheduleDataItem.statusAr}" />
																</t:column>
																<t:column id="colReceiptNumber" style="width:15%;"
																	rendered="false">
																	<f:facet name="header">
																		<t:outputText
																			value="#{msg['paymentSchedule.ReceiptNumber']}" />
																	</f:facet>
																	<t:outputText styleClass="A_LEFT" title=""
																		value="#{paymentScheduleDataItem.paymentReceiptNumber}" />
																</t:column>
																<t:column id="colAmount">
																	<f:facet name="header">
																		<t:outputText value="#{msg['paymentSchedule.amount']}" />
																	</f:facet>
																	<t:outputText title="" styleClass="A_RIGHT_NUM"
																		value="#{paymentScheduleDataItem.amount}" />
																</t:column>
																<t:column id="col8">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.action']}" />
																	</f:facet>

																	<h:commandLink
																		action="#{pages$LeaseContract.btnEditPaymentSchedule_Click}">
																		<h:graphicImage id="editPayments"
																			title="#{msg['commons.edit']}"
																			url="../resources/images/edit-icon.gif"
																			rendered="#{paymentScheduleDataItem.isReceived == 'N'}" />

																	</h:commandLink>
																	<a4j:commandLink
																		reRender="tbl_PaymentSchedule,paymentSummary"
																		action="#{pages$LeaseContract.btnDelPaymentSchedule_Click}">
																		<h:graphicImage id="deletePaySc"
																			title="#{msg['commons.delete']}"
																			rendered="#{(paymentScheduleDataItem.isReceived == 'N' )
																		             }"
																			url="../resources/images/delete_icon.png" />
																	</a4j:commandLink>
																</t:column>
															</t:dataTable>

														</t:div>
														<t:div id="pagingDivPaySch" styleClass="contentDivFooter"
															style="width:99.1%;">

															<t:dataScroller id="scrollerPaySch"
																for="tbl_PaymentSchedule" paginator="true" fastStep="1"
																paginatorMaxPages="15" immediate="false"
																paginatorTableClass="paginator"
																renderFacetsIfSinglePage="true"
																pageIndexVar="pageNumber"
																paginatorTableStyle="grid_paginator"
																layout="singleTable"
																paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000; "
																paginatorActiveColumnStyle="font-weight:bold;"
																paginatorRenderLinkForActive="false"
																styleClass="SCH_SCROLLER">
																<f:facet name="first">
																	<t:graphicImage url="../#{path.scroller_first}"
																		id="lblFPaySch"></t:graphicImage>
																</f:facet>

																<f:facet name="fastrewind">
																	<t:graphicImage url="../#{path.scroller_fastRewind}"
																		id="lblFRPaySch"></t:graphicImage>
																</f:facet>

																<f:facet name="fastforward">
																	<t:graphicImage url="../#{path.scroller_fastForward}"
																		id="lblFFPaySch"></t:graphicImage>
																</f:facet>

																<f:facet name="last">
																	<t:graphicImage url="../#{path.scroller_last}"
																		id="lblLPaySch"></t:graphicImage>
																</f:facet>


															</t:dataScroller>

														</t:div>
														<f:verbatim>
															<br></br>
														</f:verbatim>
														<t:div id="divCollectPayment" styleClass="A_RIGHT"
															style="padding:10px">
															<h:commandButton id="btnCollectPayment"
																binding="#{pages$MaintenanceRequest.btnCollectPayment}"
																value="#{msg['settlement.actions.collectpayment']}"
																styleClass="BUTTON"
																action="#{pages$MaintenanceRequest.openReceivePaymentsPopUp}"
																style="width: 150px"
																rendered="#{pages$MaintenanceRequest.isContractStatusApproved || pages$MaintenanceRequest.isContrctStatusActive}" />
														</t:div>
														<f:verbatim>
															<br></br>
														</f:verbatim>
														<t:div styleClass="DETAIL_SECTION" style="width:99%;">
															<t:panelGrid id="paymentSummary" cellpadding="1px"
																width="100%" cellspacing="3px"
																styleClass="DETAIL_SECTION_INNER" columns="4"
																columnClasses="LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2,LEASE_CONTRACT_BASIC_INFO_GRID_C1, LEASE_CONTRACT_BASIC_INFO_GRID_C2">

																<t:outputLabel styleClass="A_LEFT"
																	value="#{msg['payments.totalCash']} :" />
																<t:outputLabel styleClass="A_LEFT"
																	value="#{pages$LeaseContract.totalCashAmount}" />

																<t:outputLabel styleClass="A_LEFT"
																	value="#{msg['payments.totalChq']} :" />
																<t:outputLabel styleClass="A_LEFT"
																	value="#{pages$LeaseContract.totalChqAmount}" />

																<t:outputLabel styleClass="A_LEFT"
																	value="#{msg['payments.totalDeposit']} :" />
																<t:outputLabel styleClass="A_LEFT"
																	value="#{pages$MaintenanceRequest.totalDepositAmount}" />

																<t:outputLabel styleClass="A_LEFT"
																	value="#{msg['payments.totalRent']} :" />
																<t:outputLabel styleClass="A_LEFT"
																	value="#{pages$MaintenanceRequest.totalRentAmount}" />


																<t:outputLabel styleClass="A_LEFT"
																	value="#{msg['payments.totalFees']} :" />
																<t:outputLabel styleClass="A_LEFT"
																	value="#{pages$MaintenanceRequest.totalFees}" />

																<t:outputLabel styleClass="A_LEFT"
																	value="#{msg['payments.totalFines']} :" />
																<t:outputLabel styleClass="A_LEFT"
																	value="#{pages$MaintenanceRequest.totalFines}" />

																<t:outputLabel styleClass="A_LEFT"
																	value="#{msg['payments.noOfInstallments']} :" />
																<t:outputLabel styleClass="A_LEFT"
																	value="#{pages$MaintenanceRequest.installments}" />


															</t:panelGrid>
														</t:div>

													</rich:tab>
													<rich:tab id="tabSiteVisitId" title="Site Visit" label="Site Visit" >


														<t:div style="width:100%;">
															<t:panelGrid columns="1" cellpadding="0" cellspacing="0">

																<t:div styleClass="contentDiv" style="width:98%;">
																	<t:dataTable id="siteVisitTable" renderedIfEmpty="true"
																		width="100%" cellpadding="0" cellspacing="0"
																		var="siteVisitDataItem"
																		binding="#{pages$MaintenanceRequest.siteVisitDataTable}"
																		value="#{pages$MaintenanceRequest.siteVisitList}"
																		preserveDataModel="false" preserveSort="false"
																		rowClasses="row1,row2" rules="all" rows="9">
																		<t:column width="200">
																			<f:facet name="header">
																				<t:outputText value="Visit Date" />
																			</f:facet>
																			<t:outputText value="#{siteVisitDataItem.visitDate}"
																				styleClass="A_LEFT" />

																		</t:column>
																		<t:column width="300">
																			<f:facet name="header">
																				<t:outputText value="Remarks" />
																			</f:facet>
																			<t:outputText value="#{siteVisitDataItem.remarks}"
																				styleClass="A_LEFT" />

																		</t:column>
																		<t:column width="200">
																			<f:facet name="header">
																				<t:outputText value="Engineer Name" />
																			</f:facet>
																			<t:outputText
																				value="#{siteVisitDataItem.engineerName}"
																				styleClass="A_LEFT" />
																		</t:column>
																		<t:column width="200">
																			<f:facet name="header">
																				<h:outputText value="Estimated Cost" />
																			</f:facet>
																			<t:outputText
																				value="#{siteVisitDataItem.estimatedCost}"
																				styleClass="A_LEFT" />
																		</t:column>


																	</t:dataTable>
																</t:div>
																<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																	style="width:100%;">
																	<t:panelGrid columns="2" border="0" width="100%"
																		cellpadding="1" cellspacing="1">
																		<t:panelGroup
																			styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																			<t:div styleClass="JUG_NUM_REC_ATT">
																				<h:outputText value="#{msg['commons.records']}" />
																				<h:outputText value=" : " />
																				<h:outputText
																					value="#{pages$MaintenanceRequest.recordSize}" />
																			</t:div>
																		</t:panelGroup>
																		<t:panelGroup>
																			<t:dataScroller id="siteVisitScroller"
																				for="siteVisitTable" paginator="true" fastStep="1"
																				paginatorMaxPages="5" immediate="false"
																				paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true"
																				paginatorTableStyle="grid_paginator"
																				layout="singleTable"
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																				paginatorActiveColumnStyle="font-weight:bold;"
																				paginatorRenderLinkForActive="false"
																				pageIndexVar="pageNumber" styleClass="JUG_SCROLLER">
																				<f:facet name="first">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblFSV"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastrewind">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFRSV"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastforward">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFFSV"></t:graphicImage>
																				</f:facet>
																				<f:facet name="last">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblLSV"></t:graphicImage>
																				</f:facet>
																				<t:div styleClass="PAGE_NUM_BG" rendered="true">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</t:div>
																			</t:dataScroller>
																		</t:panelGroup>
																	</t:panelGrid>
																</t:div>
															</t:panelGrid>
														</t:div>
													</rich:tab>

													<rich:tab id="tabFollowUpId" title="Follow Up"  label="Follow Up">


														<t:div style="width:100%;">
															<t:panelGrid columns="1" cellpadding="0" cellspacing="0">

																<t:div styleClass="contentDiv" style="width:98%;">
																	<t:dataTable id="followUpTable" renderedIfEmpty="true"
																		width="100%" cellpadding="0" cellspacing="0"
																		var="followUpDataItem"
																		binding="#{pages$MaintenanceRequest.followUpDataTable}"
																		value="#{pages$MaintenanceRequest.followUpList}"
																		preserveDataModel="false" preserveSort="false"
																		rowClasses="row1,row2" rules="all" rows="9">
																		<t:column width="200">
																			<f:facet name="header">
																				<t:outputText value=" Date " />
																			</f:facet>
																			<t:outputText value="#{followUpDataItem.followupDate}"
																				styleClass="A_LEFT" />

																		</t:column>
																		<t:column width="380">
																			<f:facet name="header">
																				<t:outputText value="Remarks" />
																			</f:facet>
																			<t:outputText value="#{followUpDataItem.remarks}"
																				styleClass="A_LEFT" />

																		</t:column>
																		
																		<t:column width="200">
																			<f:facet name="header">
																				<h:outputText value="Actual Cost" />
																			</f:facet>
																			<t:outputText
																				value="#{followUpDataItem.actualCost}"
																				styleClass="A_LEFT" />
																		</t:column>


																	</t:dataTable>
																</t:div>
																<t:div styleClass="contentDivFooter AUCTION_SCH_RF"
																	style="width:100%;">
																	<t:panelGrid columns="2" border="0" width="100%"
																		cellpadding="1" cellspacing="1">
																		<t:panelGroup
																			styleClass="RECORD_NUM_BG_ATT RECORD_NUM_TD">
																			<t:div styleClass="JUG_NUM_REC_ATT">
																				<h:outputText value="#{msg['commons.records']}" />
																				<h:outputText value=" : " />
																				<h:outputText
																					value="#{pages$MaintenanceRequest.recordSize}" />
																			</t:div>
																		</t:panelGroup>
																		<t:panelGroup>
																			<t:dataScroller id="FoollowupScroller1"
																				for="followUpTable" paginator="true" fastStep="1"
																				paginatorMaxPages="5" immediate="false"
																				paginatorTableClass="paginator"
																				renderFacetsIfSinglePage="true"
																				paginatorTableStyle="grid_paginator"
																				layout="singleTable"
																				paginatorColumnStyle="font-size: 10;font-family: Verdana;font-weight: regular;color: #000000;"
																				paginatorActiveColumnStyle="font-weight:bold;"
																				paginatorRenderLinkForActive="false"
																				pageIndexVar="pageNumber" styleClass="JUG_SCROLLER">
																				<f:facet name="first">
																					<t:graphicImage url="../#{path.scroller_first}"
																						id="lblFFU"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastrewind">
																					<t:graphicImage
																						url="../#{path.scroller_fastRewind}" id="lblFRFU"></t:graphicImage>
																				</f:facet>
																				<f:facet name="fastforward">
																					<t:graphicImage
																						url="../#{path.scroller_fastForward}" id="lblFFFU"></t:graphicImage>
																				</f:facet>
																				<f:facet name="last">
																					<t:graphicImage url="../#{path.scroller_last}"
																						id="lblLFU"></t:graphicImage>
																				</f:facet>
																				<t:div styleClass="PAGE_NUM_BG" rendered="true">
																					<h:outputText styleClass="PAGE_NUM"
																						value="#{msg['commons.page']}" />
																					<h:outputText styleClass="PAGE_NUM"
																						style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																						value="#{requestScope.pageNumber}" />
																				</t:div>
																			</t:dataScroller>
																		</t:panelGroup>
																	</t:panelGrid>
																</t:div>
															</t:panelGrid>
														</t:div>
													</rich:tab>

													<rich:tab id="attachmentTab"
														label="#{msg['commons.attachmentTabHeading']}"
														action="#{pages$MaintenanceRequest.tabAttachmentsComments_Click}">
														<%@  include file="attachment/attachment.jsp"%>
													</rich:tab>

													<rich:tab id="commentsTab"
														label="#{msg['commons.commentsTabHeading']}"
														action="#{pages$MaintenanceRequest.tabAttachmentsComments_Click}">
														<%@ include file="notes/notes.jsp"%>
													</rich:tab>

												</rich:tabPanel>

											</div>

											<%--
                                        <table cellpadding="0" cellspacing="0"  style="width:89.38%;margin-left:10px;">
											<tr>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_left}"/>" class="TAB_PANEL_LEFT"/></td>
											<td width="100%"><IMG src="../<h:outputText value="#{path.img_tab_bottom_mid}"/>" class="TAB_PANEL_MID"/></td>
											<td><IMG src="../<h:outputText value="#{path.img_tab_bottom_right}"/>" class="TAB_PANEL_RIGHT"/></td>
											</tr>
										</table>
										--%>
											<br />


											<table cellpadding="1px" cellspacing="3px" width="100%">
												<tr>
													<td colspan="12" class="BUTTON_TD">

														<pims:security
															screen="Pims.LeaseContractManagement.LeaseContract.AddNewLease"
															action="create">
															<h:commandButton type="submit" styleClass="BUTTON"
																action="#{pages$MaintenanceRequest.btnSubmit_Click}"
																binding="#{pages$MaintenanceRequest.btnSaveNewLease}"
																rendered="#{pages$MaintenanceRequest.isPageModeAdd || pages$MaintenanceRequest.isPageModeUpdate }"
																value="#{msg['commons.saveButton']}" />
															
														</pims:security>
														
			
														<pims:security
															screen="Pims.LeaseContractManagement.LeaseContract.CompleteAmendLease"
															action="create">
															<h:commandButton type="submit" styleClass="BUTTON"
																action="#{pages$MaintenanceRequest.btnCompleteAmend_Click}"
																binding="#{pages$MaintenanceRequest.btnCompleteAmendLease}"
																rendered="#{pages$MaintenanceRequest.isPageModeUpdate}"
																value="#{msg['commons.complete']}" />
														</pims:security>
													</td>
												</tr>

											</table>

										</div>

									</td>
								</tr>
							</table>
						</h:form>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="footer">
									<h:outputLabel value="#{msg['commons.footer.message']}" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

		</body>
	</html>
</f:view>

