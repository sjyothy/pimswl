
<script language="JavaScript" type="text/javascript">	
</script>

<t:panelGrid 

	id="pnlGrdFields" styleClass="TAB_DETAIL_SECTION_INNER" cellpadding="1px" width="100%" cellspacing="5px" columns="4">
	
	<h:panelGroup>
		<h:outputLabel styleClass="mandatory" value="*"/>
		<h:outputLabel styleClass="LABEL" value="#{msg['socialProgram.lbl.Purpose']}" />
	</h:panelGroup>
	<h:inputTextarea  
	value="#{pages$programPurposes.programPurposes.purpose}" style="width: 185px;" />
	
		
		
</t:panelGrid>
<t:panelGrid 
id="pnlGrdActions" styleClass="BUTTON_TD" cellpadding="1px" width="100%" cellspacing="5px">
	<h:panelGroup >
		<h:commandButton styleClass="BUTTON"
			value="#{msg['commons.saveButton']}"
			action="#{pages$programPurposes.onSave}" />
	</h:panelGroup>
</t:panelGrid>


<t:div styleClass="contentDiv" style="width:98%"  id= "purposeContentDiv">																
	<t:dataTable id="purposesDataTable"  
				rows="100"
				value="#{pages$programPurposes.list}"
				binding="#{pages$programPurposes.dataTable}"																	
				preserveDataModel="false" preserveSort="false" var="dataItem"
				rowClasses="row1,row2" rules="all" renderedIfEmpty="true" width="100%">


		
		<t:column id="purposedate" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.PurposeGrid.lbl.Date']}" style="white-space: normal;" />
			</f:facet>
			
			<t:outputText style="white-space: normal;"  value="#{dataItem.createdOn}" rendered="#{dataItem.isDeleted != '1' }">
			<f:convertDateTime pattern="#{pages$programPurposes.dateFormat}" timeZone="#{pages$programPurposes.timeZone}" />
			</t:outputText>
		</t:column>
		<t:column id="purposeCreatedBy" width="15%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.PurposeGrid.lbl.CreatedBy']}" style="white-space: normal;" />
			</f:facet>
			<t:outputText style="white-space: normal;"  value="#{dataItem.createdEn}" rendered="#{dataItem.isDeleted != '1' }"/>																		
		</t:column>																																
		<t:column id="purposedesc" width="55%" sortable="true">
			<f:facet name="header">
				<t:outputText value="#{msg['socialProgram.PurposeGrid.lbl.Purpose']}" style="white-space: normal;"  />
			</f:facet>
			<t:outputText value="#{dataItem.purpose}"  style="white-space: normal;" rendered="#{dataItem.isDeleted != '1' }"/>																		
		</t:column>
		<t:column id="purposeAction" width="15%">
			<f:facet name="header">
				<t:outputText value="#{msg['commons.action']}" />
			</f:facet>
			<h:graphicImage id="imgDeletePurpose"
				rendered="#{dataItem.isDeleted != '1' }"
				title="#{msg['commons.delete']}"
				url="../resources/images/delete_icon.png"
				onclick="if (!confirm('#{msg['commons.confirmDelete']}')) return false; else onDeleted(this);">
			</h:graphicImage>
			<a4j:commandLink id="onPurposeDeleted"
				reRender="hiddenFields,purposeContentDiv,purposeDataTable,successmsg,errormsg"
				onbeforedomupdate="javaScript:onRemoveDisablingDiv(); "
				action="#{pages$programPurposes.onDelete}" />


		</t:column>
	</t:dataTable>										
</t:div>
