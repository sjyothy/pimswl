<%@ page import="com.avanza.ui.util.ResourceUtil"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://avanza.pims/security" prefix="pims"%>
<%@ page import="javax.faces.component.UIViewRoot;"%>

<?xml version="1.0" encoding="${sessionScope.CurrentLocale.encoding}"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<script language="JavaScript" type="text/javascript"
	src="../resources/jscripts/commons.js"></script>
<script language="JavaScript" type="text/javascript"> 
</script>
<f:view>
	<f:loadBundle basename="com.avanza.pims.web.messageresource.Messages"
		var="msg" />
	<f:loadBundle
		basename="com.avanza.pims.web.messageresource.pims-resource-path"
		var="path" />

	<html dir="${sessionScope.CurrentLocale.dir}"
		lang="${sessionScope.CurrentLocale.languageCode}"
		style="overflow:hidden;">
		<head>
			<META HTTP-EQUIV="Content-Type"
				CONTENT="text/html; charset=${sessionScope.CurrentLocale.encoding}" />
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="0">
			<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_simple}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_amaf}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_table}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_tabPanel}"/>" />
			<link rel="stylesheet" type="text/css"
				href="../<h:outputFormat value="#{path.css_calendar}"/>" />



		</head>
		<body class="BODY_STYLE">
			<div class="containerDiv">
				<!-- Header -->
				<table width="100%" height="100%" cellpadding="0" cellspacing="0"
					border="0">
					<tr
						style="height: 84px; width: 100%; # height: 84px; # width: 100%;">
						<td colspan="2">
							<jsp:include page="header.jsp" />
						</td>
					</tr>
					<tr width="100%">
						<td class="divLeftMenuWithBody" width="17%">
							<jsp:include page="leftmenu.jsp" />
						</td>
						<td width="83%" valign="top" class="divBackgroundBody">
							<table width="99.2%" class="greyPanelTable" cellpadding="0"
								cellspacing="0" border="0">
								<tr>
									<td class="HEADER_TD">
										<h:outputLabel
											value="#{msg['mems.periodic.disb.lablel.heading']}"
											styleClass="HEADER_FONT" />
									</td>
								</tr>
							</table>
							<table width="99%" style="margin-left: 1px;"
								class="greyPanelMiddleTable" cellpadding="0" cellspacing="0"
								border="0" height="100%">
								<tr valign="top">
									<td width="100%" valign="top" nowrap="nowrap">
										<%-- 	<div class="SCROLLABLE_SECTION" >  --%>
										<div class="SCROLLABLE_SECTION"
											style="height: 470px; width: 100%; # height: 470px; # width: 100%;">
											<h:form id="detailsFrm" enctype="multipart/form-data"
												style="WIDTH: 97.6%;">
												<table border="0" class="layoutTable"
													style="margin-left: 15px; margin-right: 15px;">
													<tr>
														<td>
															<h:outputText id="successMsg_2"
																value="#{pages$managePeriodicDisbursement.successMessages}"
																escape="false" styleClass="INFO_FONT" />
															<h:outputText id="errorMsg_1"
																value="#{pages$managePeriodicDisbursement.errorMessages}"
																escape="false" styleClass="ERROR_FONT" />
														</td>
													</tr>
												</table>

												<div>

													<div class="AUC_DET_PAD">

														<table border="0" width="100%" cellspacing="5px"
															cellpadding="0" style="margin-bottom: 10px;">
															<tr>
																<td>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.requestNumber']}"> :</h:outputLabel>
																</td>
																<td>
																	<h:inputText
																		value="#{pages$managePeriodicDisbursement.requestView.requestNumber}"
																		styleClass="READONLY"></h:inputText>
																</td>
																<td>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.createdBy']}"> :</h:outputLabel>
																</td>
																<td>
																	<h:inputText
																		value="#{pages$managePeriodicDisbursement.isEnglishLocale?
										                             pages$managePeriodicDisbursement.requestView.createdByFullName:
										                             pages$managePeriodicDisbursement.requestView.createdByFullNameAr
										                     }"
																		styleClass="READONLY"></h:inputText>
																</td>
															</tr>
															<tr>
																<td>

																	<h:outputLabel styleClass="LABEL"
																		value="#{msg['commons.requestStatus']}"> :</h:outputLabel>
																</td>
																<td>
																	<h:inputText
																		value="#{pages$managePeriodicDisbursement.isEnglishLocale? 
										                                         pages$managePeriodicDisbursement.requestView.statusEn:
										                                         pages$managePeriodicDisbursement.requestView.statusAr}"
																		styleClass="READONLY"></h:inputText>
																</td>
																<td>

																	<h:outputLabel styleClass="LABEL"
																		rendered="#{! pages$managePeriodicDisbursement.requestStatusCompleted}"
																		value="#{msg['commons.amount']}" />
																</td>
																<td>

																	<h:inputText maxlength="200"
																		value="#{pages$managePeriodicDisbursement.amount}"
																		onkeyup="removeNonNumeric(this)"
																		onkeypress="removeNonNumeric(this)"
																		onkeydown="removeNonNumeric(this)"
																		onblur="removeNonNumeric(this)"
																		onchange="removeNonNumeric(this)" />
																</td>
															</tr>
															<tr>
																<td>

																	<h:outputLabel styleClass="LABEL"
																		rendered="#{! pages$managePeriodicDisbursement.requestStatusCompleted}"
																		value="#{msg['commons.startDate']}:" />
																</td>
																<td>
																	<rich:calendar id="fromContractD"
																		rendered="#{! pages$managePeriodicDisbursement.requestStatusCompleted}"
																		datePattern="MM/dd/yyyy"
																		value="#{pages$managePeriodicDisbursement.startDate}"
																		inputStyle="width:170px; height:14px"></rich:calendar>
																</td>
																<td>

																	<h:outputLabel styleClass="LABEL"
																		rendered="#{! pages$managePeriodicDisbursement.requestStatusCompleted}"
																		value="#{msg['commons.endDate']}:" />
																</td>
																<td>
																	<rich:calendar id="toContractD"
																		rendered="#{! pages$managePeriodicDisbursement.requestStatusCompleted}"
																		datePattern="MM/dd/yyyy"
																		value="#{pages$managePeriodicDisbursement.renewedDate}"
																		inputStyle="width:170px; height:14px"></rich:calendar>
																</td>
															</tr>
														</table>

														<t:div styleClass="contentDiv" style="width:100%">



															<t:dataTable id="prdc" rows="15" width="100%"
																value="#{pages$managePeriodicDisbursement.pageView}"
																binding="#{pages$managePeriodicDisbursement.periodicDisbursementTable}"
																preserveDataModel="false" preserveSort="false"
																var="unitDataItem" rowClasses="row1,row2" rules="all"
																renderedIfEmpty="true">
																<t:column id="checkbox"
																	rendered="#{! pages$managePeriodicDisbursement.requestStatusCompleted}">
																	<f:facet name="header">
																		<t:outputText value="#{msg['commons.select']}" />
																	</f:facet>
																	<h:selectBooleanCheckbox id="idbox" immediate="true"
																		value="#{unitDataItem.selected}">
																	</h:selectBooleanCheckbox>
																</t:column>
																<t:column id="singleBenf" sortable="true">
																	<f:facet name="header">
																		<t:outputText id="headerSingleBenf"
																			value="#{msg['mems.periodic.disb.lablel.heading']}" />
																	</f:facet>

																	<t:commandLink id="s_b"
																		actionListener="#{pages$managePeriodicDisbursement.openManageBeneficiaryPopUp}"
																		styleClass="A_LEFT"
																		value="#{unitDataItem.beneficiaryName}" />
																</t:column>
																<t:column id="comments" width="10%" sortable="false"
																	style="white-space: normal;word-wrap: hard;">

																	<f:facet name="header">

																		<h:outputText value="#{msg['commons.comments']}" />

																	</f:facet>
																	<t:div
																		style="white-space: normal;word-wrap: break-word;">
																		<t:outputText value="#{unitDataItem.comments}"
																			style="width:70px;white-space: normal;word-wrap: break-word;" />

																	</t:div>
																</t:column>

																<t:column id="from" sortable="true">
																	<f:facet name="header">
																		<t:outputText id="hdFrom"
																			value="#{msg['mems.periodic.disb.lable.fromDate']}" />
																	</f:facet>
																	<t:outputText id="b_n" styleClass="A_LEFT"
																		value="#{unitDataItem.dateFrom}">
																		<f:convertDateTime
																			timeZone="#{pages$managePeriodicDisbursement.timeZone}" />
																	</t:outputText>
																</t:column>
																<t:column id="pctWeight" sortable="true">
																	<f:facet name="header">
																		<t:outputText id="hdTo"
																			value="#{msg['mems.periodic.disb.lable.toDate']}" />
																	</f:facet>
																	<t:outputText id="p_w" styleClass="A_LEFT"
																		value="#{unitDataItem.dateTo}">
																		<f:convertDateTime
																			timeZone="#{pages$managePeriodicDisbursement.timeZone}" />
																	</t:outputText>
																</t:column>
																<t:column id="amount" sortable="true">
																	<f:facet name="header">
																		<t:outputText id="hdAmoun"
																			value="#{msg['mems.periodic.disb.label.amount']}" />
																	</f:facet>
																	<t:outputText id="amount_r" styleClass="A_LEFT"
																		value="#{unitDataItem.amount}" />
																</t:column>
																<t:column id="statis" sortable="true">
																	<f:facet name="header">
																		<t:outputText id="hdnStatus"
																			value="#{msg['mems.periodic.disb.label.status']}" />
																	</f:facet>
																	<t:outputText id="status" styleClass="A_LEFT"
																		value="#{pages$managePeriodicDisbursement.isEnglishLocale?unitDataItem.statusEn:unitDataItem.statusAr}" />
																</t:column>

															</t:dataTable>

														</t:div>

														<t:div id="footer"
															styleClass="contentDivFooter AUCTION_SCH_RF"
															style="width:100%;#width:100%;border: 4px solid #F9A63C;">
															<table cellpadding="0" cellspacing="0" width="99%">
																<tr>
																	<td class="RECORD_NUM_TD">
																		<div class="RECORD_NUM_BG">
																			<table cellpadding="0" cellspacing="0"
																				style="width: 182px; # width: 150px;">
																				<tr>
																					<td class="RECORD_NUM_TD">
																						<h:outputText
																							value="#{msg['commons.recordsFound']}" />
																					</td>
																					<td class="RECORD_NUM_TD">
																						<h:outputText value=" : " />
																					</td>
																					<td class="RECORD_NUM_TD">
																						<h:outputText
																							value="#{pages$managePeriodicDisbursement.totalRows}" />
																					</td>
																				</tr>
																			</table>
																		</div>
																	</td>
																	<td class="BUTTON_TD" style="width: 53%; # width: 50%;"
																		align="right">

																		<t:div styleClass="PAGE_NUM_BG" style="#width:20%">

																			<table cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							value="#{msg['commons.page']}" />
																					</td>
																					<td>
																						<h:outputText styleClass="PAGE_NUM"
																							style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px"
																							value="#{pages$managePeriodicDisbursement.currentPage}" />
																					</td>
																				</tr>
																			</table>
																		</t:div>
																		<TABLE border="0" class="SCH_SCROLLER">
																			<tr>
																				<td>
																					<t:commandLink
																						action="#{pages$managePeriodicDisbursement.pageFirst}"
																						disabled="#{pages$managePeriodicDisbursement.firstRow == 0}">
																						<t:graphicImage url="../#{path.scroller_first}"
																							id="lblF"></t:graphicImage>
																					</t:commandLink>
																				</td>
																				<td>
																					<t:commandLink
																						action="#{pages$managePeriodicDisbursement.pagePrevious}"
																						disabled="#{pages$managePeriodicDisbursement.firstRow == 0}">
																						<t:graphicImage
																							url="../#{path.scroller_fastRewind}" id="lblFR"></t:graphicImage>
																					</t:commandLink>
																				</td>
																				<td>
																					<t:dataList
																						value="#{pages$managePeriodicDisbursement.pages}"
																						var="page">
																						<h:commandLink value="#{page}"
																							actionListener="#{pages$managePeriodicDisbursement.page}"
																							rendered="#{page != pages$managePeriodicDisbursement.currentPage}" />
																						<h:outputText value="<b>#{page}</b>"
																							escape="false"
																							rendered="#{page == pages$managePeriodicDisbursement.currentPage}" />
																					</t:dataList>
																				</td>
																				<td>
																					<t:commandLink
																						action="#{pages$managePeriodicDisbursement.pageNext}"
																						disabled="#{pages$managePeriodicDisbursement.firstRow + pages$managePeriodicDisbursement.rowsPerPage >= pages$managePeriodicDisbursement.totalRows}">
																						<t:graphicImage
																							url="../#{path.scroller_fastForward}" id="lblFF"></t:graphicImage>
																					</t:commandLink>
																				</td>
																				<td>
																					<t:commandLink
																						action="#{pages$managePeriodicDisbursement.pageLast}"
																						disabled="#{pages$managePeriodicDisbursement.firstRow + pages$managePeriodicDisbursement.rowsPerPage >= pages$managePeriodicDisbursement.totalRows}">
																						<t:graphicImage url="../#{path.scroller_last}"
																							id="lblL"></t:graphicImage>
																					</t:commandLink>
																				</td>
																			</tr>
																		</TABLE>
																	</td>
																</tr>
															</table>

														</t:div>
														<t:div styleClass="BUTTON_TD"
															style="padding-top:10px; padding-right:21px;">
															<h:commandButton styleClass="BUTTON"
																action="#{pages$managePeriodicDisbursement.stop}"
																onclick="if (!confirm('#{msg['confirmMsg.areYouSureWantToStop']}')) return false;"
																value="#{msg['mems.periodic.disb.label.stop']}"
																rendered="#{! pages$managePeriodicDisbursement.requestStatusCompleted && 
																			  pages$managePeriodicDisbursement.isTaskPresent}">
															</h:commandButton>
															<h:commandButton styleClass="BUTTON"
																value="#{msg['mems.periodic.disb.label.renew']}"
																onclick="if (!confirm('#{msg['confirmMsg.areYouSureWantToRenew']}')) return false;"
																action="#{pages$managePeriodicDisbursement.renew}"
																rendered="#{! pages$managePeriodicDisbursement.requestStatusCompleted && 
																			  pages$managePeriodicDisbursement.isTaskPresent}">
															</h:commandButton>
															<h:commandButton styleClass="BUTTON"
																value="#{msg['commons.complete']}"
																onclick="if (!confirm('#{msg['confirmMsg.areYouSureToProceedWithComplete']}')) return false;"
																action="#{pages$managePeriodicDisbursement.complete}"
																rendered="#{! pages$managePeriodicDisbursement.requestStatusCompleted && 
																			  pages$managePeriodicDisbursement.isTaskPresent}">
															</h:commandButton>

														</t:div>
													</div>
												</div>
											</h:form>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr
						style="height: 10px; width: 100%; # height: 10px; # width: 100%;">
						<td colspan="2">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="footer">
										<h:outputLabel value="#{msg['commons.footer.message']}" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
</f:view>