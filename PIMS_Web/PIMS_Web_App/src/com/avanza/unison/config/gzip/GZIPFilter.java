package com.avanza.unison.config.gzip;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.avanza.core.util.Logger;

public class GZIPFilter implements Filter {

	private static Logger logger = Logger.getLogger(GZIPFilter.class);
	
	public GZIPFilter() {

	}

	public void doFilter(ServletRequest servletrequest,
			ServletResponse servletresponse, FilterChain filterchain)
			throws IOException, ServletException {

		if (servletrequest instanceof HttpServletRequest) {
			HttpServletRequest httpservletrequest = (HttpServletRequest) servletrequest;
			HttpServletResponse httpservletresponse = (HttpServletResponse) servletresponse;						
			String url = httpservletrequest.getRequestURL().toString().toLowerCase();
			if (url.indexOf("inboundCallAreaTabs") == -1 && url.indexOf("validatorResource") == -1 )
			if (url.indexOf(".jsf?") > 0 || url.indexOf(".jsp?") > 0 || url.indexOf(".html?") > 0 || url.endsWith(".jsf") || url.endsWith(".jsp") || url.endsWith(".html")) {
				String s = httpservletrequest.getHeader("accept-encoding");
				if (s != null && s.toLowerCase().indexOf("gzip") != -1) {
					logger.logInfo("GZIP supported, compressing for." + httpservletrequest.getRequestURL());
					httpservletresponse.setHeader("Content-Encoding", "gzip");
					GZIPResponseWrapper gzipresponsewrapper = new GZIPResponseWrapper(
							httpservletresponse);
					filterchain.doFilter(servletrequest, gzipresponsewrapper);
					gzipresponsewrapper.finishResponse();
					return;
				}
			}
			filterchain.doFilter(servletrequest, servletresponse);
		}
	}

	public void init(FilterConfig filterconfig) {
	}

	public void destroy() {
	}
}