package com.avanza.core.web.context.impl;

import java.util.Map;
import com.avanza.core.web.ViewContext;


@SuppressWarnings("serial")
public class ViewContextImpl implements ViewContext {

    private Map viewMap;
    
    public ViewContextImpl(Map viewMap){
        this.viewMap = viewMap;
    }
    
    public <T, U> U getAttribute(T key) {
        
        return (U) this.viewMap.get(key.toString());
    }

    public <T> void removeAttribute(T key) {

        this.viewMap.remove(key.toString());
    }

    public <T, U> void setAttribute(T key, U value) {
        
        this.viewMap.put(key.toString(), value);
    }

    public Map getAttributesMap() {
        return this.viewMap;
    }
}
