package com.avanza.core.jsf.appbase.dao.security;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.avanza.core.data.expression.Search;
import com.avanza.core.security.PermissionBinding;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.UserGroup;
import com.avanza.core.security.db.GroupRelationBinding;
import com.avanza.core.security.db.GroupUserBinding;
import com.avanza.core.security.db.UserGroupDbImpl;
import com.avanza.pims.dao.AbstractManager;
import com.avanza.pims.dao.ManagerException;


import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @author Nauman Bashir
 *
 */
public class GroupManager extends AbstractManager<UserGroup> {

	/* (non-Javadoc)
	 * @see com.avanza.eip.dao.AbstractManager#add(com.avanza.core.data.DbObject)
	 */
	@Override
	public void add(UserGroup item)
	{
			SecurityManager.persistBinding(item);
		
	}
		
	/**
	 * Persist the user group object and the relationship with the parent group Object
	 * @param item the child object to be persisted
	 * @param parent the Parent User group Object
	 * @throws ManagerException 
	 */
	public void add(UserGroup item,UserGroup parent) throws ManagerException {
		try {
			SecurityManager.persistBinding(item);
			addGroupRelation(parent.getUserGroupId(), item.getUserGroupId());	
		} catch (Exception e) {
			throw new ManagerException(e);
		}
			
	}
	
    /**
     * Persist the user group object and the relationship with the parent group Object
     * @param item the child object to be persisted
     * @param parent the Parent User group Id
     * @throws ManagerException 
     */
	public void add(UserGroup item,String parentId) throws ManagerException {
		try {
			SecurityManager.persistBinding(item);		
			addGroupRelation(parentId, item.getUserGroupId());	
		} catch (Exception e) {
			throw new ManagerException(e);
		}
	}

	/**
	 * Persist the user group object and the relationship with the parent group Object
	 * @param parentId  the Parent User group Object Id
	 * @param childId The Child User group Id
	 * @throws ManagerException 
	 */
	private void addGroupRelation(String parentId, String childId) throws Exception {
		try {
			GroupRelationBinding gbind = new GroupRelationBinding();
			gbind.setParentGroup(parentId);
			gbind.setChildGroup(childId);
			SecurityManager.persistBinding(gbind);
		} catch (Exception e) {
			throw new ManagerException(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.avanza.eip.dao.AbstractManager#delete(com.avanza.core.data.DbObject)
	 */
	@Override
	public void delete(UserGroup item) {
			SecurityManager.deleteBinding(item);
		
	}

	/* (non-Javadoc)
	 * @see com.avanza.eip.dao.AbstractManager#find(com.avanza.core.data.expression.Search)
	 */
	@Override
	public List<UserGroup> find(Search query) {
			return SecurityManager.findGroup(query);
		
	}
	
	
	/* (non-Javadoc)
	 * @see com.avanza.eip.dao.AbstractManager#update(com.avanza.core.data.DbObject)
	 */
	@Override
	public void update(UserGroup item)  {			
			SecurityManager.persistBinding(item);
	
	}
        
        public Map<String,String> getGroupSelectOptions(Locale locale) throws ManagerException
        {
            Search query = new Search();
            query.addFrom(UserGroup.class);
            List<UserGroup> grpList = this.find(query);
            HashMap<String, String> map = new HashMap<String, String>();            
            for (UserGroup object : grpList) {
           	String label = ( locale.equals( Locale.ENGLISH )  ) ? object.getPrimaryName() : object.getSecondaryName();
				map.put(label, object.getUserGroupId());
			}
            
            return map;
        }
	
        public GroupRelationBinding getParentGroup(UserGroupDbImpl group) throws ManagerException {
            try{
                Set<GroupRelationBinding> parents = group.getParentGroupBindings();
                if(parents == null || parents.size() == 0)
                        return null;
                else
                
                    return parents.iterator().next();
            } catch (Exception e) {
                    throw new ManagerException(e);
            }
            
        }
        
    public PermissionBinding PrepareBindings() {
        return SecurityManager.prepareBindings();
    }
        
    public void updatePermissions(UserGroupDbImpl userGroup) {
        
        UserGroupDbImpl usrGroup = (UserGroupDbImpl) FindUserGroupById(userGroup.getUserGroupId());
        usrGroup.getPermissions();
        Iterator<PermissionBinding> iterator = userGroup.getAddedPermissions().values().iterator();
        while (iterator.hasNext()) {
            PermissionBinding permissionBinding = (PermissionBinding) iterator.next();
            usrGroup.addPermission(permissionBinding);
        }
        iterator = userGroup.getRemovedPermissions().values().iterator();
        while (iterator.hasNext()) {
            PermissionBinding permissionBinding = (PermissionBinding) iterator.next();
            usrGroup.removePermission(permissionBinding);
        }
        
        SecurityManager.persistGroup(usrGroup);
    }

    public UserGroup FindUserGroupById(String userGroupId) {
       
        return SecurityManager.getGroup(userGroupId);
        
    }
    
}
