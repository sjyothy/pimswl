package com.avanza;

import com.avanza.pims.entity.EntityManager;

import java.util.ArrayList;
import java.util.List;


public class UtilityServiceAgent {
    List<Object> getAllDomainTypeData() {
                        List<Object> list = new ArrayList<Object>();
                        try {
                                String hql = " from DomainType dt "
                                                + " inner join fetch dt.domainDatas dd "
                                                + " order by lower(dd.dataDescEn) asc ";
                                list = EntityManager.getBroker().find(hql);

                                /*
                                 * Session session = EntityManager.getBroker().getSession();
                                 * Criteria domainTypeCriteria =
                                 * session.createCriteria(DomainType.class); Criteria
                                 * domainDataCriteria =
                                 * domainTypeCriteria.createCriteria("domainDatas");
                                 * domainTypeCriteria.setFetchMode("domainDatas", FetchMode.JOIN);
                                 * domainDataCriteria.addOrder(Order.asc("dataDescEn").ignoreCase());
                                 * list = domainTypeCriteria.list();
                                 */
                                return list;
                        }  catch (Exception e) {
                                e.printStackTrace();
                        } 
                        return list;
                }
}
