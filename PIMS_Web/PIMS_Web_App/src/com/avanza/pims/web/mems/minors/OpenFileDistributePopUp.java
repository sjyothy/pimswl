package com.avanza.pims.web.mems.minors;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlSelectOneMenu;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.collaxa.thirdparty.jaxen.function.SumFunction;

import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationBean;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.CollectionProcedureService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.OpenFileDistribute;
import com.avanza.pims.ws.vo.mems.DistributePopUpView;

public class OpenFileDistributePopUp extends AbstractMemsBean 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8948793371607545289L;
	private HtmlDataTable dataTable ;
	private List<DistributePopUpView> dataList = new ArrayList<DistributePopUpView>( 0 );
	private List<SelectItem> distributionCriteriaList = new ArrayList<SelectItem>();
	CollectionProcedureService collectionService = new CollectionProcedureService();
	private HtmlSelectOneMenu distCritSelectMenu = new HtmlSelectOneMenu();
	private HtmlCommandButton btnDone = new HtmlCommandButton();
	private Long inheritanceFileId;
	private ArrayList<String> errorMessages;
	private ArrayList<String> successMessages;
	OpenFileDistribute openFileDistribute ;
	@SuppressWarnings("unchecked")
	@Override
	public void init()
	{
		super.init();
		try
		{
			if( isPostBack() ){return;}
			
			getValuesFromSession();
			loadCombos();
			loadBeneficiariesList();	
		}
		catch( Exception e)
		{
			logger.LogException(" init--- CRASHED --- ", e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	@SuppressWarnings( "unchecked" )
	private void loadBeneficiariesList() throws Exception
	{
		List<DistributePopUpView> list= collectionService.getBeneficiariesForOpenFileDistribute( this.getOpenFileDistribute().getAssetId(), this.getInheritanceFileId() );
		HashMap <Long,DistributePopUpView> map = new HashMap<Long, DistributePopUpView>();
		for (DistributePopUpView distributePopUpView : this.getOpenFileDistribute().getDistributePopUpView() ) 
		{
			map.put( distributePopUpView.getBeneficiaryId() , distributePopUpView);
		}	
		Double sumAssetShares = 0.0d;
		Double sumShariaShares = 0.0d;
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		DistributePopUpView lastView = null; 
		for (DistributePopUpView distributePopUpView : list ) 
		{
	          if( map.containsKey( distributePopUpView.getBeneficiaryId() ) )
	          {
	        	  DistributePopUpView alreadyExisting  = (DistributePopUpView )map.get( distributePopUpView.getBeneficiaryId() );
	        	  distributePopUpView.setSelected(true);
	        	  distributePopUpView.setAmount( alreadyExisting.getAmount() );
	          }
	          if( distributePopUpView.getAssetPercent()  != null )
	          {
		          sumAssetShares  += Double.valueOf( oneDForm.format(  distributePopUpView.getAssetPercent() ) );
		          lastView = distributePopUpView;
		          sumAssetShares   = Double.valueOf( oneDForm.format(  sumAssetShares) );
	          }
	          if( distributePopUpView.getShariaPercent() != null )
	          {
	        	  sumShariaShares   += Double.valueOf( oneDForm.format(  distributePopUpView.getShariaPercent() ) );
		          lastView = distributePopUpView;
		          sumShariaShares   = Double.valueOf( oneDForm.format(  sumShariaShares) );
	          }
		}
		
		logger.logDebug("loadBeneficiariesList|sumShariaShares:%s|sumAssetShares:%s|",sumShariaShares,sumAssetShares);
		if( lastView  != null )
		{
			logger.logDebug("loadBeneficiariesList|lastView.getTotalAssetShares() :%s|",lastView.getTotalAssetShares() );
			if(lastView.getTotalAssetShares() != null &&  sumAssetShares.compareTo( lastView.getTotalAssetShares())!= 0 )
			{
				String msg = java.text.MessageFormat.format(  
															 CommonUtil.getBundleMessage("openFileDistribute.msg.totalAssetSharesNotEqualSumAssetShares"),
															 lastView.getTotalAssetShares(),
															 sumAssetShares
														   );
				
															 
				viewMap.put("totalAssetSharesNotEqualSumAssetShares", msg);
			}
			if(lastView.getTotalFileShares()  != null && sumShariaShares.compareTo( lastView.getTotalFileShares() )!= 0 )
			{
				String msg = java.text.MessageFormat.format(  
															 CommonUtil.getBundleMessage("openFileDistribute.msg.totalFileSharesNotEqualSumFileShares"),
															 lastView.getTotalFileShares(),
															 sumShariaShares
														   );
				
															 
				viewMap.put("totalFileSharesNotEqualSumFileShares", msg);
			}
		}
        		
		this.setDataList( list )  ;
	}
	
	@SuppressWarnings( "unchecked" )
	private void getValuesFromSession() throws Exception
	{
		if( sessionMap.get( WebConstants.OpenFileDistributePopUpKeys.FILE_ID ) != null )
		{
	      this.setInheritanceFileId( new Long( sessionMap.remove( WebConstants.OpenFileDistributePopUpKeys.FILE_ID ).toString() ) )	;
		}
		if( sessionMap.get( WebConstants.OpenFileDistributePopUpKeys.OBJECT ) != null )
		{
		  this.setOpenFileDistribute( (OpenFileDistribute) sessionMap.remove( WebConstants.OpenFileDistributePopUpKeys.OBJECT )  )	;
		}
	}
	@SuppressWarnings( "unchecked" )
	private void loadCombos() throws Exception
	{
	  ApplicationBean aBean = new ApplicationBean();
      this.setDistributionCriteriaList( aBean.getDistributionCriteriaWithoutGeneralAccount() );
	}
		
	
	@SuppressWarnings("unchecked")
	@Override
	public void prerender() 
	{
		super.prerender();
		
	}
	
	private boolean hasError() throws Exception
	{
		boolean hasError =false;
		
		for (DistributePopUpView obj : this.getDataList() )
		{
              if( obj.getSelected() != null && obj.getSelected() && ( obj.getCostCenter() == null || obj.getCostCenter().trim().length()<=0) )
              {
            	  errorMessages.add(CommonUtil.getBundleMessage("collectionProcedure.msg.beneficiaryCostCenterEmpty"));
            	  return true;
            	  
              }
		}
		
		return hasError;
	}
	@SuppressWarnings( "unchecked" )
	public void onDone()
	{
		final String methodName = "onDone|";
		try	
		{	
				errorMessages = new ArrayList<String>();
				successMessages = new ArrayList<String>();
			
				if( hasError() ){return;}
		
				openFileDistribute = this.getOpenFileDistribute();
				List<DistributePopUpView> selectedList= new ArrayList<DistributePopUpView>();
				
				for (DistributePopUpView obj : this.getDataList() )
				{
	                  if( 
	                	  obj.getSelected() != null && 
	                	  obj.getSelected() &&  
	                	  obj.getAmount() != null && 
	                	  obj.getAmount().length() > 0  &&  
	                	  Double.valueOf( obj.getAmount() ) > 0d )
	                  {
	                	  selectedList.add( obj );
	                  }
				}
				openFileDistribute.setDistributePopUpView( selectedList );
				sessionMap.put( WebConstants.OpenFileDistributePopUpKeys.OBJECT ,openFileDistribute );
				String javaScriptText = "javascript:closeWindowSubmit();";
				FacesContext facesContext = FacesContext.getCurrentInstance();
				AddResource addResource = AddResourceFactory.getInstance(facesContext);
				addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);

		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onCriteriaChanged()
	{
		try	
		{	
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			openFileDistribute = this.getOpenFileDistribute();
			btnDone.setRendered(true);
			Long criteria = new Long( distCritSelectMenu.getValue().toString() ); 
			Double amount = this.getOpenFileDistribute().getAmount();
			List<DistributePopUpView> selectedList = new ArrayList<DistributePopUpView>( 0 );
			
			if( 
					hasOnCriteriaChangedError(selectedList,criteria)
			  )
			{
				btnDone.setRendered(false);
				return;
			  	
			}
			divide( selectedList, criteria, amount  );	
			openFileDistribute.setDistributionCriteriaForBeneficiaries( distCritSelectMenu.getValue().toString()  );
			this.setOpenFileDistribute(openFileDistribute);
		}
		catch (Exception exception) 
		{
			logger.LogException( "onCriteriaChanged --- CRASHED --- ", exception);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	/**
	 * @param selectedList
	 */
	private boolean hasOnCriteriaChangedError( List<DistributePopUpView> selectedList, Long criteria) throws Exception 
	{
		boolean hasNoCostCenter=false;
		boolean hasError = false;
		for (DistributePopUpView item : this.getDataList() ) 
		{
			if(
				item.getCostCenter() == null ||
				item.getCostCenter().trim().length() <= 0 
			  )
			{
				hasNoCostCenter=true;	
			}
			if(
					( item.getSelected() != null && item.getSelected() ) || 
					criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_SHARIA_ID ) 	== 0 ||
					criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_ASSET_SHARES_ID) == 0 ||
					criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_SHARIA_SELECTED_ID) == 0||
					criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_ASSET_SHARES_SELECTED_ID ) == 0 ||
					criteria.compareTo( WebConstants.DistributionCriteria.EQUALLY_ID) == 0
			  )
			{
				   if(
						criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_SHARIA_ID ) 	== 0 
						//||
						//criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_SHARIA_SELECTED_ID) == 0  
				     )
					{
						if	 ( item.getShariaPercent() == null || item.getShariaPercent().compareTo(0D) == 0 )
						{
							item.setCheckBoxRender(1L);
							item.setSelected(false);
							item.setAmount("0");
						}
						else
						{
							item.setCheckBoxRender(0L);
							item.setSelected(true);
							selectedList.add( item );
						}
					}
					else if(
							criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_ASSET_SHARES_ID) == 0
							//||
							//criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_ASSET_SHARES_SELECTED_ID ) == 0  
						   )
					{
							if	 ( item.getAssetPercent()  == null || item.getAssetPercent().compareTo(0D) == 0 )
							{
								item.setCheckBoxRender(1L);
								item.setSelected(false);
								item.setAmount("0");
							}
							else
							{
								item.setCheckBoxRender(0L);
								item.setSelected(true);
								selectedList.add( item );
							}
				   }
				   else
				   {
					   
					  if(
						  	( criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_ASSET_SHARES_SELECTED_ID ) == 0 &&
							  (   item.getAssetPercent()  == null ||item.getAssetPercent().compareTo(0D) == 0)
							)
							||
							( criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_SHARIA_SELECTED_ID) == 0 &&
								  	( item.getShariaPercent() == null || item.getShariaPercent().compareTo(0D) == 0 )
							)
						)
					  {
						  	item.setCheckBoxRender(1L);
							item.setSelected(false);
							item.setAmount("0");
					  }
					  
					  else if( item.getSelected() != null && item.getSelected() )
					  {
						   item.setSelected(true);
						   selectedList.add( item );
					   }
					   else
					   {
						   item.setSelected(false);
					   }
						item.setAmount("0");
				   }
			}
			else
			{
				item.setAmount("0");
				item.setCheckBoxRender(0L);
				item.setSelected(false);
			}
		}
		if( hasNoCostCenter )
		{
			hasError = true;
			errorMessages.add(CommonUtil.getBundleMessage( "collectionProcedure.msg.beneficiaryCostCenterEmpty" ) );
		}
		return hasError;
	}
	
	@SuppressWarnings ( "unchecked" )
	private void divide( List<DistributePopUpView> list, Long criteria, Double amount ) throws Exception
	{
    	DecimalFormat oneDForm = new DecimalFormat("#.00");
    	

		double caluclatedAmount = 0.00d;
		double sumCaluclatedAmount = 0.00d;
		DistributePopUpView lasObj = new DistributePopUpView();
		btnDone.setRendered(true);
		for (DistributePopUpView obj : list) 
		{
		
			if ( 
				  ( 
					criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_SHARIA_ID ) == 0 ||
					criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_SHARIA_SELECTED_ID ) == 0
				  )
					//&&
				 //obj.getShariaPercent()!= null 	
				) 
			{
						
						if  (
								viewMap.get("totalFileSharesNotEqualSumFileShares")!= null
							)
							{
									errorMessages = new ArrayList<String>();
									errorMessages.add( viewMap.get("totalFileSharesNotEqualSumFileShares").toString() );
									btnDone.setRendered(false);
							}
					else if   (     
									obj.getTotalFileShares() != null && obj.getTotalFileShares() > 0 &&
									obj.getShariaPercent()   != null && obj.getShariaPercent()   > 0 
							  )
						   {
								caluclatedAmount = Double.valueOf( oneDForm.format(  obj.getFileShareRatio() * amount ) );
								obj.setAmount( new Double( caluclatedAmount ).toString());
								obj.setBasedOnCriteria(Long.parseLong(this.distCritSelectMenu.getValue().toString()));
						   }
					  else
						  {
							  errorMessages.add(CommonUtil.getBundleMessage("collection.msg.FileShareorShariaShareRequired"));
							  btnDone.setRendered(false);
						  }
			}
			else if ( 
						(
						criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_ASSET_SHARES_ID ) == 0 || 
						criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_ASSET_SHARES_SELECTED_ID ) == 0 
					 )//&&
					  //	obj.getAssetPercent() != null	
				    ) 
			{
				if(
					viewMap.get("totalAssetSharesNotEqualSumAssetShares")!= null
				  )
				{
						errorMessages = new ArrayList<String>();
						errorMessages.add( viewMap.get("totalAssetSharesNotEqualSumAssetShares").toString() );
						btnDone.setRendered(false);
				}
				else if( obj.getTotalAssetShares() != null && obj.getTotalAssetShares() > 0 &&
						obj.getAssetPercent()   != null && obj.getAssetPercent()   > 0 
					  )
				{

					caluclatedAmount = Double.valueOf( oneDForm.format( ( obj.getAssetShareRatio() * amount) ) );
					obj.setAmount( new Double( caluclatedAmount ).toString());
					obj.setBasedOnCriteria(Long.parseLong(this.distCritSelectMenu.getValue().toString()));
				}
				else
				{
				    errorMessages.add(CommonUtil.getBundleMessage("collection.msg.AssetShareorShariaShareRequired"));
				    btnDone.setRendered(false);
				}
			}
				
			else if ( criteria.compareTo( getDistributionCriteriaFromType(WebConstants.DistributionCriteria.EQUALLY).getDomainDataId() ) == 0 ) 
			{
					caluclatedAmount = Double.valueOf( oneDForm.format( (amount)/ new Double( list.size() ) ));
					obj.setAmount( new Double( caluclatedAmount ).toString());
					obj.setBasedOnCriteria(Long.parseLong(this.distCritSelectMenu.getValue().toString()));
			}
			sumCaluclatedAmount+=caluclatedAmount;
			if( sumCaluclatedAmount > amount )
			{
				Double newAmount = Double.valueOf( obj.getAmount()) - ( sumCaluclatedAmount-amount);  
				obj.setAmount( oneDForm.format( newAmount));
			}
			lasObj = obj;
		}
		
		if ( list.size() >0 && btnDone.isRendered() &&
			 (
				criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_SHARIA_SELECTED_ID ) == 0 || 
				criteria.compareTo( WebConstants.DistributionCriteria.BASED_ON_ASSET_SHARES_SELECTED_ID ) == 0 
			 ) && sumCaluclatedAmount < amount
		   )
		
		{
			Double amnt = Double.valueOf( oneDForm.format(  (amount-sumCaluclatedAmount )/ new Double( list.size() ) ));
			for (DistributePopUpView obj : list) 
			{
				if( obj.getAmount() == null ) continue;
				sumCaluclatedAmount +=amnt;
				obj.setAmount( 
								oneDForm.format(Double.valueOf( obj.getAmount())  + amnt ) 
							 );
				lasObj = obj;
			}
		}
		if( 	btnDone.isRendered() &&
				sumCaluclatedAmount < amount && lasObj.getAmount()!= null 
		  )
		{
			Double newAmount = Double.valueOf( lasObj.getAmount()) + ( amount-sumCaluclatedAmount);  
			lasObj.setAmount( oneDForm.format( newAmount));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onChkChange()
	{
		try	
		{	
			errorMessages = new ArrayList<String>();
			onCriteriaChanged();
			successMessages = new ArrayList<String>();
			
		}
		catch (Exception exception) 
		{
			logger.LogException( "onChkChange --- CRASHED --- ", exception);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onTemplateMethod()
	{
		String methodName = "onSave|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			
			//if( !hasAddDisSrcError() )
			{
				
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	@SuppressWarnings("unchecked")
	public List<DistributePopUpView> getDataList() {
		if( viewMap.get( "dataList" )!= null )
			dataList = (ArrayList<DistributePopUpView>) viewMap.get( "dataList" );
		return dataList;
	}
	@SuppressWarnings("unchecked")
	public void setDataList(List<DistributePopUpView> dataList) 
	{
		this.dataList = dataList;
		if( this.dataList != null  )
		{
			Collections.sort(dataList, new ListComparator("beneficiaryName",true));
			viewMap.put (  "dataList",this.dataList );
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getDistributionCriteriaList() {
		if( viewMap.get( "distributionCriteriaList" ) != null )
			distributionCriteriaList = ( ArrayList<SelectItem> )viewMap.get( "distributionCriteriaList" ) ;
		return distributionCriteriaList;
	}

	@SuppressWarnings("unchecked")
	public void setDistributionCriteriaList(
			List<SelectItem> distributionCriteriaList) {
		this.distributionCriteriaList = distributionCriteriaList;
		if( this.distributionCriteriaList != null )
			viewMap.put( "distributionCriteriaList",this.distributionCriteriaList );
	}
	@SuppressWarnings( "unchecked" )
	public Long getInheritanceFileId() {
		if ( viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null )
			inheritanceFileId = new Long ( viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID).toString()   );
		return inheritanceFileId;
	}
@SuppressWarnings( "unchecked" )
	public void setInheritanceFileId(Long inheritanceFileId) {
		this.inheritanceFileId = inheritanceFileId;
		if( this.inheritanceFileId!= null  )
			viewMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_ID,this.inheritanceFileId); 
	}
	@SuppressWarnings( "unchecked" )
	public OpenFileDistribute getOpenFileDistribute() {
		if ( viewMap.get( WebConstants.OpenFileDistributePopUpKeys.OBJECT ) != null )
			openFileDistribute= ( OpenFileDistribute )viewMap.get(WebConstants.OpenFileDistributePopUpKeys.OBJECT );
		return openFileDistribute;
	}
	@SuppressWarnings( "unchecked" )
	public void setOpenFileDistribute(OpenFileDistribute openFileDistribute) {
		this.openFileDistribute = openFileDistribute;
	
		if( this.openFileDistribute!= null  )
			viewMap.put(WebConstants.OpenFileDistributePopUpKeys.OBJECT,this.openFileDistribute);
	}

	public String getErrorMessages()
	{

		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}


	public HtmlSelectOneMenu getDistCritSelectMenu() {
		return distCritSelectMenu;
	}


	public void setDistCritSelectMenu(HtmlSelectOneMenu distCritSelectMenu) {
		this.distCritSelectMenu = distCritSelectMenu;
	}

	@SuppressWarnings( "unchecked" )
	 public DomainDataView getDistributionCriteriaFromType( String disCriteria)
	 {
		String viewKey = "DistributionCriteria"+disCriteria;
		 DomainDataView ddv;
		 if ( viewMap.get( viewKey ) != null )
		 {
			 ddv = (DomainDataView)viewMap.get( viewKey );
		 }
		 else
		 {
			 
			 ddv = CommonUtil.getIdFromType( this.getDistributionCriteria(), disCriteria );
			 viewMap.put( viewKey,ddv);
		 
		 } 
	 
	    return ddv;
	 }

	@SuppressWarnings( "unchecked" )
	 public List<DomainDataView> getDistributionCriteria()
	 {
		 List<DomainDataView >ddvList;
		 if ( viewMap.get( "DistributionCriteriaList" ) != null )
		 {
			 ddvList = (ArrayList<DomainDataView>)viewMap.get( "DistributionCriteriaList" );
		 }
		 else
		 {
			 
			 ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.DistributionCriteria.DISTRIBUTION_CRITERIA);
			 viewMap.put( "DistributionCriteriaList",ddvList );
		 
		 } 
	 
	    return ddvList;
	 }
	public HtmlCommandButton getBtnDone() {
		return btnDone;
	}
	public void setBtnDone(HtmlCommandButton btnDone) {
		this.btnDone = btnDone;
	}


	
}
