package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.SocialProgramService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.ProgramParticipantsView;
import com.avanza.ui.util.ResourceUtil;
@SuppressWarnings("unchecked")
public class ProgramParticipantWinnerPopupBean extends AbstractMemsBean
{
	private static final long serialVersionUID = 8517238816949483790L;
	
	private ProgramParticipantsView programParticipants;
	
	public ProgramParticipantWinnerPopupBean() 
	{	
	}
	
	@Override
	public void init() 
	{
		final String METHOD_NAME = "init()";
		
		try	
		{	
			errorMessages = new ArrayList<String>(0);
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			if(sessionMap.containsKey(WebConstants.SocialProgramParticipants.VO) )
			{
				setProgramParticipants((ProgramParticipantsView)sessionMap.get(WebConstants.SocialProgramParticipants.VO));
				sessionMap.remove(WebConstants.SocialProgramParticipants.VO);
			  
			}
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@Override
	public void prerender()
	{
		
	}
	@SuppressWarnings("unchecked")
	public void onDone( )
	{
	    final String METHOD_NAME = "onDone";
		try
		{
			errorMessages = new ArrayList<String>(0);
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			if( !hasErrors() )
			{
			  String javaScript=("javaScript:onDone();");
			  sessionMap.put(WebConstants.SocialProgramParticipants.VO,getProgramParticipants() );
			  execureJavaScript( javaScript );
			}
			logger.logInfo( METHOD_NAME + " --- Finish --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	public boolean hasErrors() throws Exception
	{
		errorMessages = new ArrayList<String>();
		if( getProgramParticipants().getWinnerComments() == null || getProgramParticipants().getWinnerComments().trim().length() <=0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.participantWinner.commentsRequired"));
			return true;
		}
		else
			return false;
	}
	public void execureJavaScript(String javaScript)
	{
        FacesContext facesContext = FacesContext.getCurrentInstance();
        AddResourceFactory.getInstance(facesContext).addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScript );
	}
	public ProgramParticipantsView getProgramParticipants() {
		if( viewMap.get(WebConstants.SocialProgramParticipants.VO) != null )
		{
			programParticipants= ( ProgramParticipantsView )viewMap.get(WebConstants.SocialProgramParticipants.VO);
		}
		return programParticipants;
	}

	public void setProgramParticipants(ProgramParticipantsView programParticipants) {
		this.programParticipants = programParticipants;
		if(this.programParticipants!= null)
		{
			viewMap.put(WebConstants.SocialProgramParticipants.VO,programParticipants); 
		}
	}

}
