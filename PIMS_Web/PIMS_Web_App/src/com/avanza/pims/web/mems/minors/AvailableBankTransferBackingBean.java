package com.avanza.pims.web.mems.minors;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.entity.Person;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationBean;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.AssetMemsService;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.AssetsGridView;
import com.avanza.pims.ws.vo.PaymentCriteriaView;
import com.avanza.pims.ws.vo.PaymentMethodView;
import com.avanza.pims.ws.vo.PeriodicDisbursementGridView;
import com.avanza.pims.ws.vo.mems.AssetMemsView;
import com.avanza.pims.ws.vo.mems.CollectionProcedureView;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.InheritedAssetView;
import com.avanza.pims.ws.vo.mems.PaymentDetailsView;
import com.avanza.pims.ws.vo.mems.PeriodicDisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.ResearchRecommendationView;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings("unchecked")
public class AvailableBankTransferBackingBean extends AbstractMemsBean {
	private transient Logger logger = Logger
			.getLogger(AvailableBankTransferBackingBean.class);

	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	Map viewRootMap;
	private HtmlDataTable dataTable;


	private HtmlSelectOneMenu bankSelectMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu fileTypeSelectMenu = new HtmlSelectOneMenu();
	private List<SelectItem> paidToList = new ArrayList<SelectItem>(0);
	private List<SelectItem> bankList = new ArrayList<SelectItem>(0);
	private List<SelectItem> paymentModeList = new ArrayList<SelectItem>(0);
	HashMap formParamMap = new HashMap();
	private List<AssetMemsView> dataList = new ArrayList<AssetMemsView>();
	private List<SelectItem> fileStatus = new ArrayList<SelectItem>();
	private String VIEW_MODE = "pageMode";
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;

	private String MODE_SELECT_ONE_POPUP = "MODE_SELECT_ONE_POPUP";
	private String MODE_SELECT_MANY_POPUP = "MODE_SELECT_MANY_POPUP";
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;

	private boolean isPageModeSelectOnePopUp = false;
	private boolean isPageModeSelectManyPopUp = false;

	private String CONTEXT = "context";
	private String personId = "";

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public FacesContext getContext() {
		return context;
	}

	public void setContext(FacesContext context) {
		this.context = context;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


	public HtmlSelectOneMenu getFileTypeSelectMenu() {
		return fileTypeSelectMenu;
	}

	public void setFileTypeSelectMenu(HtmlSelectOneMenu fileTypeSelectMenu) {
		this.fileTypeSelectMenu = fileTypeSelectMenu;
	}

	@SuppressWarnings("unchecked")
	public void init() {
		super.init();
		String methodName="init";
		viewRootMap = getFacesContext().getViewRoot().getAttributes();

		try {

			sessionMap = context.getExternalContext().getSessionMap();

			Map requestMap = context.getExternalContext().getRequestMap();
			HttpServletRequest request = (HttpServletRequest) this
					.getFacesContext().getExternalContext().getRequest();

			if (!isPostBack()) {
				if (request.getParameter(WebConstants.PERSON_ID) != null) 
				{

					if (request.getParameter(WebConstants.PERSON_ID) != null) {
						viewRootMap.put(WebConstants.PERSON_ID, request
								.getParameter(WebConstants.PERSON_ID)
								.toString());
						loadDataList(Long.valueOf(viewRootMap.get(WebConstants.PERSON_ID).toString()));

					}

				}
			}

		} catch (Exception es) {
			logger.LogException(methodName + " Crashed ", es);

		}

	}
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}
	
	private void loadDataList(Long beneficiaryId) {
		 String methodName="loadDataList";
		 if(dataList != null)
			 dataList.clear();
		 
		List<AssetMemsView> list = new ArrayList<AssetMemsView>();
		 
		try
		{
			logger.logInfo(methodName+ "Starts");
			AssetMemsService assetMems = new AssetMemsService();			
			int totalRows =  assetMems.displayAssetsByBeneficiaryId(beneficiaryId);
			setTotalRows(totalRows);
			
			
			list =  assetMems.displayBankTransferTypeAssetMemsByBeneficiaryId(beneficiaryId,getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending(),isEnglishLocale());
			viewRootMap.put("assetList", list);
	    }
		catch (Exception ex) 
		{
			logger.LogException(methodName+ " crashed!", ex);
	        ex.printStackTrace();
	    }
		
	}

	



	public Boolean getPageModeConfirm()
	{
		Boolean returnVal = false;
		if (viewRootMap.get("PageModeConfirm")!=null)
			returnVal=Boolean.valueOf(viewRootMap.get("PageModeConfirm").toString());
		return returnVal;
			
	}


	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		// Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if (recordSize == null)
			recordSize = 0;
		return recordSize;
	}

	public boolean getIsViewModePopUp() {
		boolean returnVal = false;

		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null) {
			if (getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())
				returnVal = true;
		}

		else {
			returnVal = false;
		}

		return returnVal;

	}

	public boolean getIsPageModeSelectOnePopUp() {
		isPageModeSelectOnePopUp = false;
		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null
				&& viewRootMap.get(VIEW_MODE).toString().trim()
						.equalsIgnoreCase(MODE_SELECT_ONE_POPUP)) {
			isPageModeSelectOnePopUp = true;
		}
		return isPageModeSelectOnePopUp;

	}

	public boolean getIsPageModeSelectManyPopUp() {
		isPageModeSelectManyPopUp = false;
		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null
				&& viewRootMap.get(VIEW_MODE).toString().trim()
						.equalsIgnoreCase(MODE_SELECT_MANY_POPUP)) {
			isPageModeSelectManyPopUp = true;
		}
		return isPageModeSelectManyPopUp;
	}



	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}




	
	public List<AssetMemsView> getAssetDataList()
	{
		dataList = (List<AssetMemsView>)viewRootMap.get("assetList");
		if(dataList==null)
			dataList = new ArrayList<AssetMemsView>();
		return dataList;
	}


}
