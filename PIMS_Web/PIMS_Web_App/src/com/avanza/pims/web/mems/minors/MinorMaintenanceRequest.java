package com.avanza.pims.web.mems.minors;

import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectManyCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlInputTextarea;
import org.apache.myfaces.component.html.ext.HtmlOutputLabel;
import org.apache.myfaces.component.html.ext.HtmlOutputText;
import org.apache.myfaces.component.html.ext.HtmlPanelGrid;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.bpel.proxy.MEMSBlockingBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.business.services.RequestServiceAgent;
import com.avanza.pims.entity.InheritanceBeneficiary;
import com.avanza.pims.entity.Request;
import com.avanza.pims.memsminormaintenancerequestproxy.proxy.MEMSMinorMaintenanceRequestBPELPortClient;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.ExceptionCodes;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationBean;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.ContractorSearch;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.backingbeans.ServiceContractListBackingBean;
import com.avanza.pims.web.backingbeans.construction.servicecontract.ServiceContractPaymentSchTab;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.AssetMemsService;
import com.avanza.pims.ws.mems.BlockingAmountService;
import com.avanza.pims.ws.mems.CollectionProcedureService;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.AssetsGridView;
import com.avanza.pims.ws.vo.ContractView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.FollowUpView;
import com.avanza.pims.ws.vo.InheritanceBeneficiaryView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestDetailView;
import com.avanza.pims.ws.vo.RequestTasksView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.SiteVisitView;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.pims.ws.vo.mems.AssetMemsView;
import com.avanza.pims.ws.vo.mems.DistributePopUpView;
import com.avanza.pims.ws.vo.mems.MaintenanceDeductionView;
import com.avanza.pims.ws.vo.mems.MemsFollowupView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings("serial")
public class MinorMaintenanceRequest extends AbstractMemsBean 
{
	
	private transient Logger logger = Logger.getLogger(MinorMaintenanceRequest.class);
	private HtmlSelectOneMenu cmbUserGroups = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cmbPaymentSource = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cmbDisbursementSource = new HtmlSelectOneMenu();
	private static final String DISBURSEMENT_SOURCES	= "DisbursementSources";
	private MemsFollowupView memsFollowup;
	public interface Page_Mode {
		public static final String PAGE_MODE="PAGE_MODE";
		public static final String ADD="PAGE_MODE_ADD";
		public static final String EDIT="PAGE_MODE_EDIT";
		public static final String APPROVAL_REQUIRED="PAGE_MODE_APPROVAL_REQUIRED";
		public static final String EVALUATED="PAGE_MODE_EVALUATED";
		public static final String SITE_VISIT_REQUIRED="PAGE_MODE_SITE_VISIT_REQUIRED";
		public static final String COMPLETE="PAGE_MODE_COMPLETE";
		public static final String POPUP="PAGE_MODE_POPUP";
		public static final String VIEW="PAGE_MODE_VIEW";
		public static final String FOLLOW_UP="PAGE_MODE_FOLLOW_UP";
		public static final String EVALUATION_REQ="EVALUATION_REQ";
		public static final String REVIEW_REQ="REVIEW_REQ";
		public static final String APPROVED="APPROVED";
		public static final String ON_FOLLOW_UP="PAGE_MODE_ON_FOLLOW_UP";
		public static final String FOLLOW_UP_DONE="PAGE_MODE_FOLLOW_UP_DONE";
	}
    public interface TAB_ID {
		public static final String ApplicationDetails="tabApplicationDetails";
		public static final String BasicInfo="tabBasicInfo";
		public static final String PaymentSchedule="tabPaymentSchedule";
		public static final String Attachment="attachmentTab";
		public static final String SiteVist="tabSiteVisit";
		public static final String FollowUp="tabFollowUp";
		
	}
//    private final String ASSET_DESC = "ASSET_DESC";
//    private final String ASSET_NUMBER = "ASSET_NUMBER";
//    private final String ASSET_NAME_EN = "ASSET_NAME_EN";
//    private final String ASSET_NAME_AR = "ASSET_NAME_AR";
//    private final String ASSET_ID = "ASSET_ID";
//    private final String ASSET_TYPE = "ASSET_TYPE";
//    private final String ASSET_STATUS = "ASSET_STATUS";
    
    
    
	protected  String pageMode="pageMode";
	HttpServletRequest request ;
	
	protected SystemParameters parameters = SystemParameters.getInstance();
	Map viewRootMap ;
	Map sessionMap ;
	FacesContext context ;
	protected List<String> errorMessages = new ArrayList<String>();
	protected List<String> successMessages = new ArrayList<String>();
	CommonUtil commonUtil ;
	private InheritanceFileView inheritanceFileView;
	private String hdnFileId;
	private String hdnFileNumber; 
	
	protected String PROCEDURE_TYPE;
	protected String EXTERNAL_ID;

	protected String createdOn;
	protected String createdBy;
	public PersonView  contractor = new PersonView();

	protected String DD_REQUEST_STATUS_LIST ="DD_REQUEST_STATUS_LIST";
	protected String CONTRACTOR_VIEW  ="CONTRACTOR_VIEW";
	protected String CONTRACT_VIEW    ="CONTRACT_VIEW";
	protected String REQUEST_VIEW     ="REQUEST_VIEW";
	protected String REQUEST_PRIORITY_COMBO     ="REQUEST_PRIORITY_COMBO";
	protected RequestView requestView = new RequestView();
	
	public String pageTitle;
	protected org.richfaces.component.html.HtmlTab tabAuditTrail=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabPaymentTerms=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabBasicInfo=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabApplicationDetails=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabDeductionFrom=new org.richfaces.component.html.HtmlTab();
	protected org.richfaces.component.html.HtmlTab tabReview=new org.richfaces.component.html.HtmlTab();
	
	
	protected org.richfaces.component.html.HtmlTab tabFollowUp=new org.richfaces.component.html.HtmlTab();
	private HtmlCommandButton btnRevReq = new HtmlCommandButton();
	private HtmlCommandButton btnReviewed = new HtmlCommandButton();
	private HtmlCommandButton btnSave = new HtmlCommandButton();
	private HtmlCommandButton btnPrint = new HtmlCommandButton();
	private HtmlCommandButton btnSend_For_Approval= new HtmlCommandButton();
	private HtmlCommandButton btnSendForEval= new HtmlCommandButton();
	protected HtmlCommandButton btnApprove = new HtmlCommandButton();
    protected HtmlCommandButton btnReject = new HtmlCommandButton();
	protected HtmlCommandButton btnSaveSiteVisit = new HtmlCommandButton();
	protected HtmlCommandButton btnCompleteSiteVisit = new HtmlCommandButton();
	protected HtmlCommandButton btnComplete = new HtmlCommandButton();
	protected HtmlCommandButton btnSaveFollowUp= new HtmlCommandButton();
	private HtmlCommandButton btnOtherSave = new HtmlCommandButton();
	private HtmlCommandButton btnDone = new HtmlCommandButton();
	private HtmlCommandButton btnSendForFollowup = new HtmlCommandButton();
	private HtmlCommandButton btnSendForBlocking = new HtmlCommandButton();
	private HtmlCommandButton btnApplyDeduction = new HtmlCommandButton();
	private HtmlOutputLabel lblRemarks=new HtmlOutputLabel();
	private HtmlOutputText lblRevComments =new HtmlOutputText();
	private HtmlOutputText lblSendTo =new HtmlOutputText();
	private HtmlInputText txtVendor = new HtmlInputText();
	private HtmlInputText txtEvaRemarks = new HtmlInputText();
	private HtmlInputText txtReviewComments = new HtmlInputText();
	private String oldActualCost;
	private HtmlInputText txtActualCost = new HtmlInputText();
	private HtmlInputText txtEstimatedCost = new HtmlInputText();
	private HtmlOutputLabel lblVendor=new HtmlOutputLabel();
	private HtmlOutputLabel lblEvaRemarks=new HtmlOutputLabel();
	private HtmlOutputLabel lblActualCost=new HtmlOutputLabel();
	private HtmlOutputLabel lblEstimatedCost=new HtmlOutputLabel();
	private HtmlSelectBooleanCheckbox cboDeduction = new HtmlSelectBooleanCheckbox();
	private HtmlCommandButton btnDoneFollowup = new HtmlCommandButton();
//	 private String hdnAssetDesc;
//	 private String hdnAssetNameEn;
//	 private String hdnAssetNameAr;
//	 private String hdnAssetType;
//	 private String hdnFileNumber;
//	 private String hdnPersonName;
	 private String hdnAssetId;
//	 private String hdnStatus;
//	 private String hdnAssetNumber;
	
	HtmlSelectOneMenu cmbMaintenanceType = new HtmlSelectOneMenu();
	private String selectOneMaintenanceType;
	
	private List<String> selectManyWorkType;
	HtmlSelectManyCheckbox  cmbWorkType = new HtmlSelectManyCheckbox();
	
	HtmlSelectOneMenu cmbRequestPriority = new HtmlSelectOneMenu();
	private String selectOneRequestPriority ;
	
	private HtmlInputText txtContractNum = new HtmlInputText();
	private HtmlInputText txtContractorNum = new HtmlInputText();
	private HtmlInputTextarea txtboxRemarks = new HtmlInputTextarea();
	protected HtmlPanelGrid tbl_Action = new HtmlPanelGrid();
	protected HtmlPanelGrid tbl_FollowupRemarks = new HtmlPanelGrid();
    protected HtmlTabPanel tabPanel = new HtmlTabPanel();
    
    private HtmlGraphicImage imgUnitSearch=new HtmlGraphicImage();
    private HtmlGraphicImage imgPropertySearch=new HtmlGraphicImage();
    private HtmlGraphicImage imgContractSearch=new HtmlGraphicImage();
    private HtmlGraphicImage imgSearchContractor=new HtmlGraphicImage();
    private HtmlGraphicImage imgSearchEngineer=new HtmlGraphicImage();
    private HtmlGraphicImage imgClearContract = new HtmlGraphicImage();
    private HtmlGraphicImage imgSearchAsset=new HtmlGraphicImage();
    private HtmlGraphicImage imgManageAsset=new HtmlGraphicImage();
    // Site Visit table 
	private HtmlDataTable siteVisitDataTable = new HtmlDataTable();
	private List<SiteVisitView> siteVisitList = new ArrayList<SiteVisitView>();
	
    
	// Follow Up History
	private HtmlDataTable followUpDataTable = new HtmlDataTable();
	private List<FollowUpView> followUpList = new ArrayList<FollowUpView>();

    
	private List<SelectItem> requestPriority = new ArrayList<SelectItem>();
    private String hdnPropertyId  = new String();
    private String hdnApplicantId = new String();
    public UnitView  unitView = new UnitView(); 
    public UserView userView = new UserView();
    public ContractView contractView = new ContractView();
    protected String txtRemarks;    
    protected String hdnContractorId;
    protected String  hdnContractId;
    
	protected String requestId;
	

	private String engineerName;
	private String actualCost;
	private String estimatedCost;
	private String priorityValue;
	private Date visitDate;
	
	private int recordSize=0;

	private AssetsGridView assetView = new AssetsGridView();
	private final String ASSET_VIEW = "ASSET_VIEW";
	private final String DEDUCT_FROM_LIST = "DEDUCT_FROM_LIST";
	private List<DistributePopUpView> deductFromList = new ArrayList<DistributePopUpView>();
	private HtmlDataTable deductFromDataTable;
	private final String DEDUCTION_CRIT_LIST = "DEDUCTION_CRIT_LIST";
	private List<SelectItem> deductionCriteriaList = new ArrayList<SelectItem>();
	private HtmlSelectOneMenu cmbDeductCrit = new HtmlSelectOneMenu();
	private  DomainDataView equallyDD = new DomainDataView();
	private  DomainDataView basedOnShariaDD = new DomainDataView();
	private  DomainDataView basedOnShareDD = new DomainDataView();
	private final String EQUALLY_DD = "EQUALLY_DD";
	private final String BASED_ON_SHARIA_DD = "BASED_ON_SHARIA_DD";
	private final String BASED_ON_SHARE_DD = "BASED_ON_SHARE_DD";
	private final String MAINTENANCE_DEDUCTION_VIEW_LIST = "MAINTENANCE_DEDUCTION_VIEW_LIST";
	private final String BENEFICIARY_AMOUNT_MAP = "BENEFICIARY_AMOUNT_MAP";
	private List<MaintenanceDeductionView> maintenanceDeductionList = new ArrayList<MaintenanceDeductionView>();
	HashMap<Long, Double> beneAmountMap = new HashMap<Long, Double>();
	private HtmlInputTextarea txtMaintenaceDetails = new HtmlInputTextarea();
	
	public String getEngineerName() {
		if(viewRootMap.containsKey("engineerName") && viewRootMap.get("engineerName")!=null)
			engineerName=  viewRootMap.get("engineerName").toString();
		return engineerName;
	}



	public void setEngineerName(String engineerName) {
		this.engineerName = engineerName;
		
		if(this.engineerName !=null)
			viewRootMap.put("engineerName",this.engineerName);
	}



	public Date getVisitDate() {
		if(viewRootMap.containsKey("visitDate") && viewRootMap.get("visitDate")!=null)
			visitDate= (Date) viewRootMap.get("visitDate");
		
		return visitDate;
	}



	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
		
		if(this.visitDate !=null)
			viewRootMap.put("visitDate",this.visitDate);
	}

	public String getActualCost() {
		if(viewRootMap.containsKey("actualCost") && viewRootMap.get("actualCost")!=null)
			actualCost = viewRootMap.get("actualCost").toString();
		
		return actualCost;
	}



	@SuppressWarnings("unchecked")
	public void setActualCost(String actualCost) 
	{
		this.actualCost = actualCost;
		if(this.actualCost !=null)
			viewRootMap.put("actualCost",this.actualCost);
	}


	public String getEstimatedCost() {
		if(viewRootMap.containsKey("estimatedCost") && viewRootMap.get("estimatedCost")!=null)
			estimatedCost= viewRootMap.get("estimatedCost").toString();
		
		return estimatedCost;
	}



	public void setEstimatedCost(String estimatedCost) {
		this.estimatedCost = estimatedCost;
		
		if(this.estimatedCost !=null)
			viewRootMap.put("estimatedCost",this.estimatedCost);
	}
	public String getPriorityValue() {
		if(viewRootMap.containsKey("priorityValue") && viewRootMap.get("priorityValue")!=null)
			priorityValue = viewRootMap.get("priorityValue").toString();
		return priorityValue;
	}

	public void setPriorityValue(String priorityValue) {
		this.priorityValue = priorityValue;
	}
	
	Map requestMap ;


	public String getHdnContractorId() {
		return hdnContractorId;
	}

	public void setHdnContractorId(String hdnContractorId) {
		this.hdnContractorId = hdnContractorId;
	}

	public MinorMaintenanceRequest(){
		request =(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		viewRootMap = getFacesContext().getViewRoot().getAttributes();
		sessionMap = getFacesContext().getExternalContext().getSessionMap();
		requestMap = getFacesContext().getExternalContext().getRequestMap();
		context = FacesContext.getCurrentInstance();
		commonUtil = new CommonUtil();
		PROCEDURE_TYPE ="procedureType";
		EXTERNAL_ID ="externalId";
	}
	
	@Override
	@SuppressWarnings( "unchecked" )
    public void init() 
    {
    	
    	try{
	    	
		       if( !isPostBack() )
		       {
		    	   initializeContext();
		       }
		       else
		       {
		    	   requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		    	   
		    	   //setPageModeBasedOnRequestStatus();
		       }
    	}
    	catch(Exception ex)
    	{
    		errorMessages = new ArrayList<String>(0);
    		logger.LogException( "init|Error Occured", ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		
    	}
    }


   @SuppressWarnings("unchecked") 
	private void initializeContext() throws Exception, PimsBusinessException 
	{
	   	   ApplicationBean application = new ApplicationBean();
	   	   loadDisbursementSourceList(application);
	   	   loadPaymentSourceList(application);
	   	   loadDeductionCriteriaList(application);
		   
	   	   viewRootMap.put(PROCEDURE_TYPE, WebConstants.PROCEDURE_TYPE_MINOR_MAINTENANCE);
		   viewRootMap.put(EXTERNAL_ID ,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		   viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.ADD);
		   viewRootMap.put("PERSON_TYPE_LIST", CommonUtil.getDomainDataListForDomainType(WebConstants.TENANT_TYPE));
		   
		   loadAttachmentsAndComments(null);
		   
			if(sessionMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
			{
		       getDataFromTaskList();
			}
			else if(requestMap.containsKey(WebConstants.REQUEST_VIEW) && requestMap.get(WebConstants.REQUEST_VIEW)!=null)
			{
				requestView = (RequestView)requestMap.get(WebConstants.REQUEST_VIEW);
				this.requestId = requestView.getRequestId().toString();
				viewRootMap.put(REQUEST_VIEW, requestView);
			}
			else if (sessionMap.containsKey( WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW ) )
			{
				memsFollowup   = 			(MemsFollowupView) sessionMap.remove(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW ) ;
				initFromMemsFollowup ( );
			}

			if ( request.getParameter("requestId") != null ) 
			{
			 this.requestId  = request.getParameter("requestId");
			 viewRootMap.put(WebConstants.VIEW_MODE, WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP);
			}
			// From File Search
			else if (sessionMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) != null) 
			{
				openFromFileSearch();
			}
			
			if(	this.requestId != null )
			{
				putViewValuesInControl();
				setPageModeBasedOnRequestStatus();
				
				if(
					viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EDIT)||
					viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD)||
					viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.APPROVAL_REQUIRED)
					
				  )
				{	
					if(requestView.getContractView()!=null && requestView.getContractView().getContractId()!=null)
				    {
				     imgSearchContractor.setRendered(false);
				     imgClearContract.setRendered(true);
				    }
				    else
				    {
					 imgSearchContractor.setRendered(true);
					 imgClearContract.setRendered(false);
				    }
				}
				else
				{
					 imgSearchContractor.setRendered(false);
					 imgClearContract.setRendered(false);
				}
				
				if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.VIEW) )
				{
					List<PaymentScheduleView> psList = new PropertyServiceAgent().getPaymentScheduleByRequestID(requestView.getRequestId());
				    if(psList!=null && psList.size()>0)
				    	viewRootMap.put(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST,psList);
				}
			}
	}

   private void initFromMemsFollowup( ) throws Exception
	{
		setMemsFollowUp( memsFollowup);
		openFromFileSearch();
	}
	public void onChangePaymentSource() throws Exception
	{
		try
		{
			if( 
					cmbPaymentSource.getValue() != null && 
					cmbPaymentSource.getValue().toString().equals( String.valueOf( WebConstants.PaymentSrcType.SELF_ACCOUNT_ID ) )
			   )
			{
				cmbDisbursementSource.setDisabled(true);
				cmbDisbursementSource.setValue("-1");
				cmbDeductCrit.setDisabled(false);
				cmbDeductCrit.setValue( "-1");
				btnApplyDeduction.setRendered(true);
				for (DistributePopUpView obj : getDeductFromList()) 
				{
				  obj.setSelected(false);
				  obj.setAmount(null);
				  obj.setAmountToBeDeducted( null );
				}
				
			}
			else if( 
					cmbPaymentSource.getValue() != null && 
					cmbPaymentSource.getValue().toString().equals( String.valueOf( WebConstants.PaymentSrcType.AID_ID ) )
			   )
			{
				cmbDeductCrit.setDisabled(true);
				cmbDisbursementSource.setDisabled(false);
				cmbDeductCrit.setValue(String.valueOf( WebConstants.DistributionCriteria.EQUALLY_ID  ) );
				//btnApplyDeduction.setRendered(false);
				for (DistributePopUpView obj : getDeductFromList()) 
				{
				  obj.setSelected(true);
				  obj.setAmount(null);
				  obj.setAmountToBeDeducted( null );
				}
				btnApply_Click();
			}
			
		}
		catch (Exception e)
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onChangePaymentSource|Error OCCURED", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	/**
	 * @param application
	 */
   	@SuppressWarnings("unchecked")
	private void loadPaymentSourceList(ApplicationBean application)throws Exception
	{
		List<SelectItem> srcList = application.getDomainDataList( WebConstants.MemsPaymentCategories.PAYMENT_SOURCE);
		for(SelectItem singleSrc : srcList) 
		{
			if( singleSrc.getValue().toString().equals( String.valueOf( WebConstants.PaymentSrcType.EXTERNAL_PARTY_ID ) ) ) 
			{
				srcList.remove(singleSrc);
				break;
			}
		}		    
	   viewMap.put( WebConstants.MemsPaymentCategories.PAYMENT_SOURCE, srcList);
	}

   	/**
	 * @param application
	 */
   	@SuppressWarnings("unchecked")
	private void loadDisbursementSourceList(ApplicationBean application)throws Exception
	{
   		List<SelectItem> sources = application.getDisbursementSourceList();
   		viewMap.put( DISBURSEMENT_SOURCES, sources);
	}


/**
 * @param application
 * @throws Exception
 */
 @SuppressWarnings("unchecked")
private void loadDeductionCriteriaList(ApplicationBean application)
		throws Exception {
	List<SelectItem> deductionCritList = application .getDistributionCriteriaWithoutGeneralAccount();
	   if(deductionCritList != null && deductionCritList.size() > 0)
	   {
		   viewMap.put(DEDUCTION_CRIT_LIST, deductionCritList);
	   }
	   
	   List<DomainDataView> deductionCritDDList = CommonUtil.getDomainDataListForDomainType(WebConstants.DistributionCriteria.DISTRIBUTION_CRITERIA);
	   if(deductionCritDDList != null && deductionCritDDList.size() > 0)
	   {
		  equallyDD = CommonUtil.getIdFromType(deductionCritDDList, WebConstants.DistributionCriteria.EQUALLY);
		  if(equallyDD != null && equallyDD.getDomainDataId() != null)
		  {
			  viewMap.put(EQUALLY_DD,equallyDD);  
		  }
		  
		  basedOnShariaDD = CommonUtil.getIdFromType(deductionCritDDList, WebConstants.DistributionCriteria.BASED_ON_SHARIA);
		  if(basedOnShariaDD != null && basedOnShariaDD.getDomainDataId() != null)
		  {
			  viewMap.put(BASED_ON_SHARIA_DD,basedOnShariaDD);  
		  }
		  
		  basedOnShareDD = CommonUtil.getIdFromType(deductionCritDDList, WebConstants.DistributionCriteria.BASED_ON_ASSET_SHARES);
		  if(basedOnShareDD != null && basedOnShareDD.getDomainDataId() != null)
		  {
			  viewMap.put(BASED_ON_SHARE_DD,basedOnShareDD);  
		  }
	   }
}

	

	@SuppressWarnings("unchecked")
	public void prerender()
	{
		super.prerender();
		try
		{
			
			 viewRootMap.put(ServiceContractPaymentSchTab.ViewRootKeys.DIRECTION_TYPE_INBOUND,"0");
			 enableDisableControls();
		}
		catch (Exception e)
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("prerender|Error OCCURED", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
	}
	private void createRequestPriorityCombo()
	{
		if(!viewRootMap.containsKey(REQUEST_PRIORITY_COMBO) )
		{
			List<DomainDataView> ddvRequestPriority = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_PRIORITY);
			List<SelectItem> listSelectItem = new ArrayList<SelectItem>(0);
			DomainDataView ddvNormal = CommonUtil.getIdFromType(ddvRequestPriority,WebConstants.REQUEST_PRIORITY_NORMAL);
			DomainDataView ddvUrgent = CommonUtil.getIdFromType(ddvRequestPriority,WebConstants.REQUEST_PRIORITY_URGENT);
		    SelectItem item1 = new SelectItem(ddvNormal.getDomainDataId().toString(),(getIsEnglishLocale()?ddvNormal.getDataDescEn():ddvNormal.getDataDescAr()));
			listSelectItem.add(item1);
			SelectItem item2 = new SelectItem(ddvUrgent.getDomainDataId().toString(),(getIsEnglishLocale()?ddvUrgent.getDataDescEn():ddvUrgent.getDataDescAr()));
			listSelectItem.add(item2);
			viewRootMap.put(REQUEST_PRIORITY_COMBO, listSelectItem);
		}
	}
	private void putViewValuesInControl()throws PimsBusinessException,Exception
	{
		getRequestView();
		
		populateApplicationDetailsTab();
		if(requestView.getMaintenanceDetails() != null)
		{
			txtMaintenaceDetails.setValue(requestView.getMaintenanceDetails());
		}
		
		if(requestView.getRequestDetailView()!=null && requestView.getRequestDetailView().iterator().hasNext())
		{
			RequestDetailView rdv = requestView.getRequestDetailView().iterator().next();
			if(rdv.getActualCost() != null)
			{
				this.setActualCost(rdv.getActualCost().toString());
				oldActualCost =  rdv.getActualCost().toString();
			}
			
			if(rdv.getEstimatedCost() != null)
				this.setEstimatedCost(rdv.getEstimatedCost().toString());
			
			if(rdv.getEvaluationRemarks() != null)
				this.setTxtRemarks(rdv.getEvaluationRemarks());
		}
//		if( !requestView.getStatusDataValue().equals( WebConstants.REQUEST_STATUS_NEW ) &&
//			!requestView.getStatusDataValue().equals( WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED ) 	
//	      )
//		{
			if( 
				requestView.getRequestPriorityId() != null && 
				requestView.getRequestPriorityId().compareTo(-1l)!=0  
			  )
			{
				cmbDisbursementSource.setValue(requestView.getRequestPriorityId().toString());
				cmbPaymentSource.setValue( String.valueOf( WebConstants.PaymentSrcType.AID_ID ) );
			}
			else 
			{
				cmbPaymentSource.setValue( String.valueOf( WebConstants.PaymentSrcType.SELF_ACCOUNT_ID) );
	
			}
		
//		}
		if(requestView.getMemsAssetId()!=null)
		{
			this.hdnAssetId =  requestView.getMemsAssetId().toString();
			AssetMemsView assetMems = null;
			assetMems = new InheritanceFileService().getAssetMemsById(requestView.getMemsAssetId());
			populateBasicInfoTab(assetMems, requestView);
			populateDeductionTab(requestView.getRequestId());
			List<DistributePopUpView> deductionList = null;
			deductionList = new CollectionProcedureService().getBeneficiariesByAssetId(assetMems.getAssetId(), null);
			beneAmountMap = getBeneAmountMap();
			if(deductionList  != null && deductionList.size() > 0 && beneAmountMap != null && beneAmountMap.size() > 0)
			{
				for(DistributePopUpView view : deductionList)
				{
					if(beneAmountMap.containsKey(view.getInheritedBeneficiaryId()))
					{
						view.setAmountToBeDeducted(beneAmountMap.get(view.getInheritedBeneficiaryId()).toString());
						
					}
//					PersonalAccountTrxService accountWS = new PersonalAccountTrxService();
//					Double amount =  accountWS.getAmountByPersonId( view.getBeneficiaryId() );
//					view.setRemainingBalance(amount);
					if( view.getAmountToBeDeducted() != null  )
					{ 
						view.setSelected(true);
					}
				}
				setDeductFromList(deductionList);
			}
			else
				setDeductFromList(deductionList);
		}
		
		this.setSelectOneMaintenanceType(requestView.getMaintenanceTypeId().toString());
		if( requestView.getMaintenanceWorkTypes()  != null && requestView.getMaintenanceWorkTypes().length >0 )
		{
			this.setSelectManyWorkType( Arrays.asList( requestView.getMaintenanceWorkTypes()  ) );
		}
		if(requestView.getContactorView()!=null && requestView.getContactorView().getPersonId()!= null)
			this.setContractor(requestView.getContactorView());
		if(requestView.getRequestPriorityId()!=null)
			this.setSelectOneRequestPriority(requestView.getRequestPriorityId().toString());
		if(requestView.getRequestId()!=null)
		loadAttachmentsAndComments(requestView.getRequestId());
		
		if( requestView.getInheritanceFileView() != null )
		{
			this.setInheritanceFileView( requestView.getInheritanceFileView() );
		}
		if(requestView.getDistributionCriteria() != null)
			cmbDeductCrit.setValue(requestView.getDistributionCriteria().toString());
		
		onChangePaymentSource();
	}
	public void populateContractor()
	{
		try
		{
			this.setContractor(getContractorInfo());
		}
		catch(PimsBusinessException e)
		{
			logger.LogException("populateContractor|Error Occured::", e);
		}
		
	}

	private void populateDeductionTab(Long requestId) 
	{
		successMessages = new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			List<MaintenanceDeductionView> maintenanceDeductionList =  new ArrayList<MaintenanceDeductionView>();
			maintenanceDeductionList = new AssetMemsService().getMaintenanceDeductionsByRequest(requestId);
			if(maintenanceDeductionList != null && maintenanceDeductionList.size() > 0)
			{
        		DecimalFormat df = new DecimalFormat("#.##");
				for(MaintenanceDeductionView mainDeduction : maintenanceDeductionList)
				{
					long id= mainDeduction.getInhBeneficiaryView().getInheritanceBeneficiaryId();
					double amount = mainDeduction.getAmount();
					beneAmountMap.put(id, new Double(df.format(amount)));
				}
				setBeneAmountMap(beneAmountMap);
			}
    	}
    	catch(Exception ex)
    	{
    		logger.LogException( "populateDeductionTab|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
	}



	private void populateBasicInfoTab(AssetMemsView assetMems,	RequestView requestView2) 
	{
		AssetsGridView assetGridView =  new AssetsGridView();
		assetGridView.setAssetId(assetMems.getAssetId());
		assetGridView.setAssetDesc(assetMems.getDescription());
		assetGridView.setAssetNameEn(assetMems.getAssetNameEn());
		assetGridView.setAssetNameAr(assetMems.getAssetNameAr());
		assetGridView.setAssetNumber(assetMems.getAssetNumber());
		if(assetMems.getAssetTypeView() != null && assetMems.getAssetTypeView().getAssetTypeId() != null)
			assetGridView.setAssetType(isEnglishLocale() ? assetMems.getAssetTypeView().getAssetTypeNameEn(): assetMems.getAssetTypeView().getAssetTypeNameAr());
		setAssetView(assetGridView);
	}


	private void hideAll()
	{
		btnSendForEval.setRendered(false);
		btnPrint.setRendered(true);
		btnSave.setRendered(false);
		btnSend_For_Approval.setRendered(false);
		btnOtherSave.setRendered(false);
        btnApprove.setRendered(false);
		btnReject.setRendered(false);
		btnSaveSiteVisit.setRendered(false);
		btnCompleteSiteVisit.setRendered(false);
		btnSaveFollowUp.setRendered(false);
        btnComplete.setRendered(false);
        btnReviewed.setRendered(false);
        btnRevReq.setRendered(false);
        imgContractSearch.setRendered(false);
        //imgSearchContractor.setRendered(false);
        imgPropertySearch.setRendered(false);
        imgUnitSearch.setRendered(false);
        imgSearchEngineer.setRendered(false);
        txtboxRemarks.setRendered(false);
        lblRemarks.setRendered(false);
        tbl_Action.setRendered(false);
        tabPaymentTerms.setRendered(false);
        //tabDeductionFrom.setRendered(false);
		tabFollowUp.setRendered(false);
		cmbRequestPriority.setDisabled(true);
		cmbWorkType.setDisabled(true);
		cmbMaintenanceType.setDisabled(true);
		txtMaintenaceDetails.setDisabled(true);
		lblSendTo.setRendered(false);
		lblRevComments.setRendered(false);
		txtReviewComments.setRendered(false);
		cmbUserGroups.setRendered(false);
	}
	@SuppressWarnings("unchecked")
	public void enableDisableControls()
	{
	    hideAll();
		viewRootMap.put("applicationDetailsReadonlyMode",true);
		if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
		{
		
			viewRootMap.put("applicationDetailsReadonlyMode",false);
			PageModeAdd();
			this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_ADD_MODE));
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EDIT))
		{
			PageModeEdit();
			viewRootMap.put("applicationDetailsReadonlyMode",false);
			this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_ADD_MODE));

		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.APPROVAL_REQUIRED))
		{
			PageModeApprovalRequired();
			this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_APPROVAL_REQUIRED_MODE));
            //createRequestPriorityCombo();
		}
		
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EVALUATED))
		{
			PageModeEvaluated();
			this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_APPROVAL_REQUIRED_MODE));
            //createRequestPriorityCombo();
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EVALUATION_REQ))
		{
			PageModeEvaluationRequired();
			this.setPageTitle(CommonUtil.getBundleMessage("maintenanceRequest.pageTitle.evaluationMaintenanceApp"));
		//	createRequestPriorityCombo();
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.REVIEW_REQ))
		{
			PageModeReviewRequired();
			this.setPageTitle(ResourceUtil.getInstance().getProperty("commons.review"));
            //createRequestPriorityCombo();
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.FOLLOW_UP))
		{
		    PageModeFollowUp();
		    this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_FOLLOWUP_MODE));
		  //  createRequestPriorityCombo();
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ON_FOLLOW_UP))
		{
		    PageModeOnFollowUp();
		    this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_VIEW_MODE));
		    //createRequestPriorityCombo();
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.FOLLOW_UP_DONE))
		{
		    PageModeFollowUpDone();
		    this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_VIEW_MODE));
		    //createRequestPriorityCombo();
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.APPROVED))
		{
		    PageModeApproved();
		    this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_VIEW_MODE));
		    //createRequestPriorityCombo();
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.COMPLETE))
		{
		    PageModeCompleted();
		    this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_VIEW_MODE));
		    //createRequestPriorityCombo();
		}
		else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.VIEW))
		{
			PageModeView();
			this.setPageTitle(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.PAGE_TITLE_VIEW_MODE));
			//createRequestPriorityCombo();
		}
		
	}
	private void PageModeCompleted()
	{
		imgSearchAsset.setRendered(false);
		tbl_Action.setRendered(true);
		btnComplete.setRendered(false);
		btnSaveFollowUp.setRendered(false);
		tabFollowUp.setRendered(true);
		imgContractSearch.setRendered(false);
		btnSendForFollowup.setRendered(false);
		btnSendForBlocking.setRendered(false);
		btnSendForEval.setRendered(false);
		txtActualCost.setReadonly( true);
		txtActualCost.setStyleClass("READONLY");
		txtEstimatedCost.setReadonly(true);
		txtEstimatedCost.setStyleClass("READONLY");
		txtEvaRemarks.setReadonly(true);
		txtEvaRemarks.setStyleClass("READONLY");
		cmbRequestPriority.setReadonly(true);
		btnDone.setRendered(false);
		cmbWorkType.setReadonly(true);
		txtMaintenaceDetails.setReadonly(true);
		cmbMaintenanceType.setReadonly(true);
		txtboxRemarks.setRendered(false);
		tabDeductionFrom.setRendered(true);
		btnApplyDeduction.setRendered(false);
		cmbDeductCrit.setReadonly(true);
		tbl_FollowupRemarks.setRendered(false);
		tabDeductionFrom.setRendered(true);
		txtVendor.setReadonly(true);
		txtVendor.setStyleClass("READONLY");
		canAddAttachmentAndComments(false);
	}



	private void PageModeOnFollowUp() 
	{
		
		imgSearchAsset.setRendered(false);
		tbl_Action.setRendered(true);
		btnComplete.setRendered(false);
		btnSaveFollowUp.setRendered(true);
		tabFollowUp.setRendered(true);
		imgContractSearch.setRendered(false);
		btnSendForFollowup.setRendered(false);
		btnSendForBlocking.setRendered(false);
		btnSendForEval.setRendered(false);
		txtActualCost.setReadonly( false);
		txtActualCost.setStyleClass("");
		txtEstimatedCost.setReadonly(true);
		txtEstimatedCost.setStyleClass("READONLY");
		txtEvaRemarks.setReadonly(true);
		txtEvaRemarks.setStyleClass("READONLY");
		cmbRequestPriority.setReadonly(true);
		btnDone.setRendered(false);
		cmbWorkType.setReadonly(true);
		txtMaintenaceDetails.setDisabled(true);
		cmbMaintenanceType.setReadonly(true);
		txtboxRemarks.setRendered(true);
		tabDeductionFrom.setRendered(true);
		btnApplyDeduction.setRendered(false);
		cmbDeductCrit.setReadonly(true);
		cboDeduction.setRendered(false);
		if (hasTask() )
		{
			btnDoneFollowup.setRendered(true);
		}
		txtVendor.setReadonly(true);
		txtVendor.setStyleClass("READONLY");
		imgSearchContractor.setRendered(true);
		canAddAttachmentAndComments(true);
	}



	private void PageModeApproved() 
	{
		cboDeduction.setRendered(false);
		tbl_FollowupRemarks.setRendered(false);
		tabDeductionFrom.setRendered(true);
		btnApplyDeduction.setRendered(false);
		cmbDeductCrit.setReadonly(true);
		imgSearchAsset.setRendered(false);
		tbl_Action.setRendered(true);
	
		btnSaveFollowUp.setRendered(false);
		tabFollowUp.setRendered(false);
		imgContractSearch.setRendered(false);
		
		btnSendForEval.setRendered(false);
		txtActualCost.setReadonly(  true );
		txtActualCost.setStyleClass("READONLY");
		txtEstimatedCost.setReadonly(true);
		txtEstimatedCost.setStyleClass("READONLY");
		txtVendor.setReadonly(true);
		txtVendor.setStyleClass("READONLY");
		txtEvaRemarks.setReadonly(true);
		txtEvaRemarks.setStyleClass("READONLY");
		cmbRequestPriority.setReadonly(true);
		btnDone.setRendered(false);
		cmbWorkType.setReadonly(true);
		txtMaintenaceDetails.setDisabled(true);
		cmbMaintenanceType.setReadonly(true);
		if (hasTask() )
		{
			btnSendForFollowup.setRendered(true);
			btnSendForBlocking.setRendered(false);
			btnComplete.setRendered(true);	
		}
		btnDoneFollowup.setRendered(false);
		canAddAttachmentAndComments(true);
	}

	

	@SuppressWarnings("unchecked")
	private void canAddAttachmentAndComments(boolean canAdd)
	{
		viewRootMap.put("canAddAttachment", canAdd);
		viewRootMap.put("canAddNote", canAdd);
	}
	protected void PageModeAdd()
	{
		tbl_FollowupRemarks.setRendered(false);
		imgSearchAsset.setRendered(true);
		btnPrint.setRendered(false);
		btnSave.setRendered(true);
		canAddAttachmentAndComments(true);
		imgPropertySearch.setRendered(true);
        imgUnitSearch.setRendered(true);
        cmbWorkType.setDisabled(false);
        txtMaintenaceDetails.setDisabled(false);
        cmbMaintenanceType.setDisabled(false);
        btnSendForEval.setRendered(false);
        btnDone.setRendered(false);
        btnSendForFollowup.setRendered(false);
        btnSendForBlocking.setRendered(false);
        btnDoneFollowup.setRendered(false);
	}
	protected void PageModeEdit()
	{
		tbl_FollowupRemarks.setRendered(false);
		btnPrint.setRendered(false);
		imgSearchAsset.setRendered(true);
		btnSendForFollowup.setRendered(false);
		btnSendForBlocking.setRendered(false);
		btnDone.setRendered(false);
		btnSave.setRendered(true);
		btnSend_For_Approval.setRendered(true);
		canAddAttachmentAndComments(true);
        cmbWorkType.setDisabled(false);
        txtMaintenaceDetails.setDisabled(false);
        cmbMaintenanceType.setDisabled(false);
        btnSendForEval.setRendered(false);
        btnDoneFollowup.setRendered(false);
	}
	protected void PageModeApprovalRequired()
	{
		btnPrint.setRendered(true);
		tbl_FollowupRemarks.setRendered(false);
		imgSearchAsset.setRendered(false);
		btnDone.setRendered(false);
		btnSendForFollowup.setRendered(false);
		btnSendForBlocking.setRendered(false);
		tabDeductionFrom.setRendered(true);
		btnSave.setRendered(true);
		if (hasTask() )
		{
			btnOtherSave.setRendered(true);
			btnApprove.setRendered(true);
			btnReject.setRendered(true);
			btnRevReq.setRendered(true);
			btnSendForEval.setRendered(true);
			btnSendForBlocking.setRendered(true);
		}
		tbl_Action.setRendered(true);
		cmbRequestPriority.setDisabled(false);
		txtboxRemarks.setRendered(false);
		lblRemarks.setRendered(false);
		
		txtEstimatedCost.setReadonly(true);
		txtEstimatedCost.setStyleClass("READONLY");
		txtEvaRemarks.setReadonly(true);
		txtEvaRemarks.setStyleClass("READONLY");
		btnDoneFollowup.setRendered(false);
		txtActualCost.setReadonly(false);
		txtActualCost.setStyleClass("");
		txtEstimatedCost.setReadonly(false);
		txtEstimatedCost.setStyleClass("");
		imgContractSearch.setRendered(false);
		imgSearchContractor.setRendered(true);
		canAddAttachmentAndComments(true);
		lblSendTo.setRendered(true);
		lblRevComments.setRendered(true);
		txtReviewComments.setRendered(true);
		cmbUserGroups.setRendered(true);
	}
	
	protected void PageModeEvaluated()
	{
		btnPrint.setRendered(true);
		tbl_FollowupRemarks.setRendered(false);
		imgSearchAsset.setRendered(false);
		btnDone.setRendered(false);
		btnSendForFollowup.setRendered(false);
		btnSendForBlocking.setRendered(false);
		tabDeductionFrom.setRendered(true);
		btnSave.setRendered(true);
		if (hasTask() )
		{
			btnOtherSave.setRendered(true);
			btnApprove.setRendered(true);
			btnReject.setRendered(true);
			btnRevReq.setRendered(true);
			btnSendForBlocking.setRendered(true);
		}
		tbl_Action.setRendered(true);
		
		cmbRequestPriority.setDisabled(false);
		txtboxRemarks.setRendered(false);
		lblRemarks.setRendered(false);
		btnSendForEval.setRendered(true);
		txtEstimatedCost.setReadonly(true);
		txtEstimatedCost.setStyleClass("READONLY");
		txtEvaRemarks.setReadonly(true);
		txtEvaRemarks.setStyleClass("READONLY");
		btnDoneFollowup.setRendered(false);
		txtActualCost.setReadonly(false);
		txtActualCost.setStyleClass("");
		txtEstimatedCost.setReadonly(false);
		txtEstimatedCost.setStyleClass("");
		imgContractSearch.setRendered(false);
		imgSearchContractor.setRendered(true);
		canAddAttachmentAndComments(true);
		canAddAttachmentAndComments(true);
		lblSendTo.setRendered(true);
		lblRevComments.setRendered(true);
		txtReviewComments.setRendered(true);
		cmbUserGroups.setRendered(true);
	}
	protected void PageModeEvaluationRequired()
	{
		txtActualCost.setReadonly(false);
		imgSearchAsset.setRendered(false);
		if (hasTask() )
		{

			btnSaveSiteVisit.setRendered(true);
			btnDone.setRendered(true);	
			btnCompleteSiteVisit.setRendered(true);
		}
		tbl_Action.setRendered(true);
		txtboxRemarks.setRendered(true);
		lblRemarks.setRendered(true);
		imgSearchEngineer.setRendered(true);
		btnSendForEval.setRendered(false);
		btnApprove.setRendered(false);
		btnReject.setRendered(false);
		
		tbl_FollowupRemarks.setRendered(false);
		btnSendForFollowup.setRendered(false);
		btnSendForBlocking.setRendered(false);
		tabDeductionFrom.setRendered(true);
		txtEstimatedCost.setReadonly(false);
		txtEstimatedCost.setStyleClass("");
		txtEvaRemarks.setReadonly(false);
		txtEvaRemarks.setStyleClass("");
		btnDoneFollowup.setRendered(false);
		canAddAttachmentAndComments(true);
		txtVendor.setReadonly(true);
		txtVendor.setStyleClass("READONLY");
		imgContractSearch.setRendered(true);
		imgSearchContractor.setRendered(true);
	}



	/**
	 * @return
	 */
	private boolean hasTask() 
	{
		return viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null;
	}
	
	@SuppressWarnings("unchecked")
	protected void PageModeReviewRequired()
	{
		tabDeductionFrom.setRendered(true);
		tbl_FollowupRemarks.setRendered(true);
		tbl_Action.setRendered(true);
		if ( hasTask() )
		{
			btnReviewed.setRendered(true);
			
		}
		txtActualCost.setReadonly(true);
		txtActualCost.setStyleClass("READONLY");
		txtEstimatedCost.setReadonly(true);
		txtEstimatedCost.setStyleClass("READONLY");
		btnSendForFollowup.setRendered(false);
		btnSendForBlocking.setRendered(false);
		btnDone.setRendered(false);
		btnDoneFollowup.setRendered(false);
		viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
	}
	protected void PageModeFollowUp()
	{
		imgSearchAsset.setRendered(false);
		tbl_Action.setRendered(true);
		if (hasTask() )
		{

			btnComplete.setRendered(true);
		}
		btnSaveFollowUp.setRendered(true);
		tabFollowUp.setRendered(true);
		tabPaymentTerms.setRendered(true);
		imgContractSearch.setRendered(false);
		txtboxRemarks.setRendered(true);
		lblRemarks.setRendered(true);
		btnSendForEval.setRendered(false);
		imgSearchContractor.setRendered(true);
		canAddAttachmentAndComments(true);
	}
	protected void PageModeFollowUpDone()
	{
		imgSearchAsset.setRendered(false);
		tbl_Action.setRendered(true);
		btnComplete.setRendered(true);
		btnSaveFollowUp.setRendered(true);
		tabFollowUp.setRendered(true);
		tabPaymentTerms.setRendered(true);
		imgContractSearch.setRendered(false);
		txtboxRemarks.setRendered(true);
		lblRemarks.setRendered(true);
		btnSendForEval.setRendered(false);
		btnDoneFollowup.setRendered(false);
		btnDone.setRendered(false);
		btnSaveFollowUp.setRendered(false);
		tbl_FollowupRemarks.setRendered(false);
		btnSendForFollowup.setRendered(false);
		btnSendForBlocking.setRendered(false);
		txtEstimatedCost.setReadonly(true);
		txtEstimatedCost.setStyleClass("READONLY");
		txtActualCost.setReadonly(true);
		txtEvaRemarks.setReadonly(true);
		txtEvaRemarks.setStyleClass("READONLY");
		tabDeductionFrom.setRendered(true);
		cmbDeductCrit.setReadonly(true);
		btnApplyDeduction.setRendered(false);
		cboDeduction.setRendered(false);
		canAddAttachmentAndComments(true);
	}
	protected void PageModeView()
	{
      imgSearchAsset.setRendered(false);
	  tabFollowUp.setRendered(true);
	  tbl_Action.setRendered(true);
	  txtboxRemarks.setRendered(false);
	  tbl_FollowupRemarks.setRendered(false);
	  lblRemarks.setRendered(false);
	  btnSendForEval.setRendered(false);
	  tabDeductionFrom.setRendered(true);
	  btnApplyDeduction.setRendered(false);
	  cmbDeductCrit.setReadonly(true);
	  btnDone.setRendered(false);
	  btnSendForFollowup.setRendered(false);
	  btnSendForBlocking.setRendered(false);
	  cboDeduction.setRendered(false);
	  txtActualCost.setReadonly(true);
	  txtEstimatedCost.setReadonly(true);
	  txtEstimatedCost.setStyleClass("READONLY");
	  txtEvaRemarks.setReadonly(true);
	  txtEvaRemarks.setStyleClass("READONLY");
	  btnDoneFollowup.setRendered(false);
	  canAddAttachmentAndComments(false);
	}
    public boolean getIsPageModeSiteVisitRequired()
    {
    	@SuppressWarnings("unused")
		boolean isPageModeSiteVisitRequired=true;
    	if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.SITE_VISIT_REQUIRED))
    		return true ;
    	else
    		return false;
    	
    }
    public boolean getIsPageModeFollowUp()
    {
    	
    	if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals("PAGE_MODE_ON_FOLLOW_UP"))
    		return true ;
    	else
    		return false;
    	
    }
    public boolean getIsPageModeEvaluated()
    {
    	
    	if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EVALUATED))
    		return true ;
    	else
    		return false;
    	
    }

	
	public void imgClearContract_Click()
	{
		this.setContractView(null);
		
		if(viewRootMap.containsKey(CONTRACT_VIEW))
		viewRootMap.remove(CONTRACT_VIEW);
		
		this.setHdnContractorId(null);
		this.setContractor(null);
		
		if(viewRootMap.containsKey(CONTRACTOR_VIEW))
		viewRootMap.remove(CONTRACTOR_VIEW);
		imgSearchContractor.setRendered(true);
		imgClearContract.setRendered(false);
		
	}

	private void saveCommentAndAttachements(String eventDesc)throws Exception 
	{
		requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		if(requestView != null && requestView.getRequestId()!=null)
		 {
		     saveComments(requestView.getRequestId());
			 saveAttachments(requestView.getRequestId().toString());
			 saveSystemComments(eventDesc);
		 }
		 
	}	
    private boolean hasError() throws Exception
	{
	 	boolean hasErrors =false;
	 	errorMessages = new ArrayList<String>(0);
	 		
	 		/////Validate Application Details Tab Start///////////////
		 	if(hdnApplicantId!=null && hdnApplicantId.trim().length()<=0)
		 	{ 
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonsMessages.MSG_APPLICANT_REQUIRED));
			   tabPanel.setSelectedTab(TAB_ID.ApplicationDetails);
			   return true;
		 	}
	 		/////Validate Application Details Tab Finish///////////////
		 	
		    /////Validate Basic Info Tab Start///////////////
		 	else if(getAssetView() == null || getAssetView().getAssetId() == null)
            {
		 		errorMessages.add(CommonUtil.getBundleMessage("errMsg.selectAsset"));
				tabPanel.setSelectedTab(TAB_ID.BasicInfo);
				return true;
            	
            }
            /////Validate Basic Info Tab Finish///////////////
	 		
	 		/////Validate Attachment Tab Start///////////////
		 	else if(!AttachmentBean.mandatoryDocsValidated())
	    	{
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS));
	    		hasErrors = true;
	    		tabPanel.setSelectedTab(TAB_ID.Attachment);
	        	return true;
	    	}
		    /////Validate Attachment Tab Finish///////////////
			if(  
					cmbPaymentSource.getValue() == null || 
					cmbPaymentSource.getValue().toString().equals( "-1" ) 
			    )
			{
				errorMessages.add(CommonUtil.getBundleMessage("mems.payment.err.msg.paymentSrcReq"));
				tabPanel.setSelectedTab("deductionListId");
				return true;
			}
		 	/////Validate if Contractor or Contract is present..Start///////////////
//		 	else if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.APPROVAL_REQUIRED)&&
//		 			 (this.getContractView()==null || this.getContractView().getContractId()==null) && 
//		 			 (this.getContractor()==null || this.getContractor().getPersonId()==null))
//		 	{
//		 			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.CONTRACT_OR_CONTACTOR_REQ));
//	    		    hasErrors = true;
//		 	}
		   /////Validate if Contractor or Contract is present..Finish///////////////
		 	if(txtMaintenaceDetails == null || txtMaintenaceDetails.getValue() == null || txtMaintenaceDetails.getValue().toString().trim().length() <= 0)
		 	{
		 		errorMessages.add(CommonUtil.getBundleMessage("errMsg.maintenanceDtlRequired"));
    		    hasErrors = true;
		 	}
	 	return hasErrors;
		
	}
	@SuppressWarnings("static-access")
	private void setRequestView() throws Exception
	{
		
		if(viewRootMap.containsKey(Page_Mode.PAGE_MODE) &&  viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
        {
			requestView = new RequestView();
            requestView.setCreatedOn(new Date());
            requestView.setCreatedBy(getLoggedInUserId());
            requestView.setIsDeleted(new Long(0));
            requestView.setRecordStatus(new Long(1));
            DomainDataView ddRequestStatus = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS), WebConstants.REQUEST_STATUS_NEW);
            requestView.setStatusId(ddRequestStatus.getDomainDataId());  
            requestView.setRequestTypeId(WebConstants.REQUEST_TYPE_MINOR_MAINTENANCE_REQUEST);
            requestView.setRequestDate(new Date());
            DomainDataView ddRequestOrgination= commonUtil.getIdFromType(commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_ORIGINATION), WebConstants.REQUEST_ORIGINATION_CSC);
            requestView.setRequestOrigination(ddRequestOrgination.getDomainDataId());
            requestView.setMemsAssetId(getAssetView().getAssetId());
            if(getMemsFollowUp() != null )
            {
            	
            	requestView.setFollowupId(getMemsFollowUp().getFollowupId() );
            }
        }
		else
		   {
				requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
				requestView.setMemsAssetId(getAssetView().getAssetId());
		   }
		PersonView applicantView =new PersonView();
	    applicantView.setPersonId(new Long(hdnApplicantId));
	    requestView.setApplicantView(applicantView);
		requestView.setMaintenanceTypeId(new Long(this.getSelectOneMaintenanceType()));
		if( this.getSelectManyWorkType() != null )
		{
			String[] workTypes=new String[ this.getSelectManyWorkType().size() ]; 
			workTypes = this.getSelectManyWorkType().toArray( workTypes); 
	        requestView.setMaintenanceWorkTypes( workTypes );
		}
        if(viewMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION) != null)
        	requestView.setDescription(viewMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION).toString())	;

        if(this.getContractor()!=null && this.getContractor().getPersonId()!=null)
        	requestView.setContactorView(this.getContractor());
        if(this.getInheritanceFileView() != null)
        {
        	requestView.setInheritanceFileId( this.getInheritanceFileView().getInheritanceFileId() );
        	requestView.setInheritanceFileView( this.getInheritanceFileView() );
        }
        if(this.getSelectOneRequestPriority()!=null && this.getSelectOneRequestPriority().trim().length()>0)
        	requestView.setRequestPriorityId(new Long(this.getSelectOneRequestPriority()));
		requestView.setUpdatedBy(getLoggedInUserId());
        requestView.setUpdatedOn(new Date());
        if( cmbDisbursementSource.getValue() != null && cmbDisbursementSource.getValue().toString().trim().length() > 0 )
        {
        	requestView.setRequestPriorityId( new Long(cmbDisbursementSource.getValue().toString()));
        }
        
	}
	 public Set<RequestDetailView> getRequestDetailSetForUnit(RequestView requestView)
	 {
	       Set<RequestDetailView> requestDetailSet=new HashSet<RequestDetailView>();
	       RequestDetailView requestDetailView ;
    	   
    	   if(requestView.getRequestId()==null )
    	   {
    		   requestDetailView = new RequestDetailView();
    		   requestDetailView.setCreatedOn(new Date());
        	   requestDetailView.setCreatedBy(getLoggedInUserId());
    	   }
    	   else
    	   {
    		   requestDetailView = (RequestDetailView)requestView.getRequestDetailView().iterator().next();
    	   }
    	   requestDetailView.setUnitView(this.getUnitView());
    	   requestDetailView.setUpdatedBy(getLoggedInUserId());
    	   requestDetailView.setUpdatedOn(new Date());
    	   requestDetailView.setRecordStatus(new Long(1));
    	   requestDetailView.setIsDeleted(new Long(0));
    	   if(requestView != null && requestView.getRequestId() != null && requestView.getRequestId().longValue()> 0)
    	   {
    		   requestDetailView.setRequestId( requestView.getRequestId() );
    	   }
		   requestDetailSet.add(requestDetailView);
		   requestView.setRequestDetailView(requestDetailSet);
		   
		   return requestDetailSet;
	 }
	private void putControlValuesinViews() throws Exception
	{
         //Set Request View 
         setRequestView();
	}
	@SuppressWarnings("unchecked")
	public void onPrint()
    {
    	try
    	{
    		if(this.requestId!=null && this.requestId.trim().length()>0)
    		{
    			CommonUtil.printMemsReport( Long.parseLong( this.requestId  ) );
    		}
    	}
		catch (Exception ex) 
		{
			logger.LogException( "onPrint|Error Occured:",ex);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
    	
    }
	@SuppressWarnings("unchecked")
	public void btnSendForApproval_Click()
	{
	
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
	    try {
		        String endPoint= parameters.getParameter(WebConstants.MINOR_MAINTENANCE_REQUEST_BPEL_ENDPOINT);
		        MEMSMinorMaintenanceRequestBPELPortClient port=new MEMSMinorMaintenanceRequestBPELPortClient();
		        
		   	    port.setEndpoint(endPoint);
		   		if(this.requestId!=null && this.requestId.trim().length()>0)
		   		{
		   		 new RequestService().setRequestAsApprovalRequired(new Long(this.requestId), getLoggedInUserId());
		   		 requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
			   	 sessionMap.put("REQUEST_DATE", requestView.getRequestDate());
			   	 String grp = getApprovalGroupName();
//			   	 port.initiate(requestView.getRequestId(),grp,getLoggedInUserId(), null, null);
			   	 saveCommentAndAttachements(MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL) ;
		   		 viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.VIEW);
	   	         successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.ContractAdd.MSG_REQ_SENT_FOR_APPROVAL));
	   	         imgSearchContractor.setRendered(true);
	   	         CommonUtil.printMemsReport( Long.parseLong( this.requestId  ) );
	   	         
		   		}
	    }
    	catch(Exception ex)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		logger.LogException( "btnSendForApproval_Click| Error Occured...",ex);
    	}
	}
	
	@SuppressWarnings("unchecked")
	private String getApprovalGroupName()throws Exception
	{
		String groupName = "Admin Group";
		String procedureTaskKey = "APPROVAL_TASK";
		UtilityService sservice = new UtilityService();
	
		if( cmbPaymentSource.getValue() != null && 
			cmbPaymentSource.getValue().toString().equals( String.valueOf( WebConstants.PaymentSrcType.AID_ID )   )
		  )
		{
			groupName = sservice.getProcedureTaskGroups(
															WebConstants.PROCEDURE_TYPE_MINOR_MAINTENANCE, 
															"AID_TASK"
														);
		}
		else if( cmbPaymentSource.getValue() != null && 
				 cmbPaymentSource.getValue().toString().equals( String.valueOf( WebConstants.PaymentSrcType.SELF_ACCOUNT_ID  )   )
			   )
		{
			groupName = sservice.getProcedureTaskGroups(
															WebConstants.PROCEDURE_TYPE_MINOR_MAINTENANCE, 
															"SELF_ACCOUNT_TASK"
														);
		}
		else
		{
			groupName = sservice.getProcedureTaskGroups(
														WebConstants.PROCEDURE_TYPE_MINOR_MAINTENANCE, 
														procedureTaskKey
													);
		}	
		
		return groupName;
	}

	@SuppressWarnings("unchecked")
	public void btnSave_Click()
	{
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		RequestServiceAgent rsa = new RequestServiceAgent(); 
		try 
		{
			String eventDesc =	 "";
			String successMsg =	 "";
			if(!hasError())
			{
				 putControlValuesinViews();
				 if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD))
				 {
				     eventDesc  = MessageConstants.RequestEvents.REQUEST_CREATED;
				     successMsg = ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.MAINTENANCE_REQUEST_ADDED); 
				 }
				 else 
				 {
				     eventDesc  = MessageConstants.RequestEvents.REQUEST_UPDATED;
				     successMsg = ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.MAINTENANCE_REQUEST_UPDATED); 
				 }
				 if(txtMaintenaceDetails != null && txtMaintenaceDetails.getValue() != null && txtMaintenaceDetails.getValue().toString().trim().length() > 0)
				 {
					 requestView.setMaintenanceDetails(txtMaintenaceDetails.getValue().toString());
				 }
		         requestView = rsa.persistMaintenanceRequest(requestView);		 
		         this.setRequestId(requestView.getRequestId().toString());
		         viewRootMap.put(REQUEST_VIEW,requestView);
		         sessionMap.put("REQUEST_DATE", requestView.getRequestDate());
				 populateApplicationDetailsTab();
				 saveCommentAndAttachements(eventDesc);
				 if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.ADD) || viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EDIT) )
				    viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.EDIT);
				 else
					 this.setTxtRemarks("");
				 successMessages.add(successMsg);			
			}
		}
		catch(Exception e)
		{
			logger.LogException("btnSave_Click|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public void tabFollowUp_Click()
	{
		
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		RequestServiceAgent rsa = new RequestServiceAgent(); 
		try 
		{
		     requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		     sessionMap.put("REQUEST_DATE", requestView.getRequestDate());
		     followUpList  =  rsa.getFollowupsByRequest(requestView.getRequestId());
			 this.setFollowUpList(followUpList);
		    this.setRecordSize(this.getFollowUpList().size());
		    tabPanel.setSelectedTab("tabFollowUp");
	    	
		}
		catch(Exception e)
		{
			logger.LogException("tabFollowUp_Click|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		

	}
	
	private void setFollowUpView(FollowUpView followUpView)
	{
		followUpView.setActualCost( new Double( this.txtActualCost.getValue().toString() ) );
		if(followUpView!=null && followUpView.getFollowUpId()!=null)
		{
			followUpView.setRemarks(this.txtboxRemarks.getValue().toString());
			
		}
		else
		{
			List<DomainDataView> ddvList = new ArrayList<DomainDataView>(0);
		    ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUpType.FOLLOW_UP_TYPE);
			DomainDataView ddFollowUpType = CommonUtil.getIdFromType(ddvList, WebConstants.FollowUpType.CUSTOM); 
	        ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUpStatus.FOLLOW_UP_STATUS);
	        DomainDataView ddFollowUpStatus = CommonUtil.getIdFromType(ddvList, WebConstants.FollowUpStatus.CLOSE);
			followUpView.setRemarks(this.txtboxRemarks.getValue().toString());
			RequestView requestView = (RequestView) viewMap.get(REQUEST_VIEW);
	        followUpView.setRequestId( Long.valueOf( requestView.getRequestId()) );
	        followUpView.setFollowupDate(new Date());
	        followUpView.setCreatedBy(getLoggedInUserId());
	        followUpView.setUpdatedBy(getLoggedInUserId());
	        followUpView.setCreatedOn(new Date());
	        followUpView.setUpdatedOn(new Date());
	        followUpView.setStartDate( new Date()); 
	        followUpView.setFollowUpTypeId(ddFollowUpType.getDomainDataId()); // Will not be used but setting the value due to not null
	        followUpView.setStatusId(ddFollowUpStatus.getDomainDataId() ); // Will not be used but setting the value due to not null
	        followUpView.setIsDeleted(0L);
	        followUpView.setRecordStatus(1L);
		}
		 if(viewRootMap.containsKey("SELECTED_FOLLOWUP"))
			 viewRootMap.remove("SELECTED_FOLLOWUP");
	}
	private void clearFollowUpFields()
	{
		txtboxRemarks.setValue(null);
		
		
	}
	private void makePaymentSchedule()
	{
		
       if(this.getFollowUpList().size()>0)
       {
    	   
    	    PaymentScheduleView paymentSchedule = new PaymentScheduleView();
    	    Double amount = 0D;
    	    for (FollowUpView fpView : this.getFollowUpList())
 			{
    	    	if(fpView.getActualCost()!=null)
    	    	amount +=fpView.getActualCost();
 			}
 			if(amount.compareTo(new Double(0))!=0)
 			{
				List<DomainDataView> ddvListStatus = CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS);
				DomainDataView ddvDraft = CommonUtil.getIdFromType(ddvListStatus, WebConstants.PAYMENT_SCHEDULE_STATUS_DRAFT);
				paymentSchedule.setRequestId(Long.valueOf(this.getRequestId()));
				paymentSchedule.setIsDeleted(0L);
				paymentSchedule.setRecordStatus(1L);
				paymentSchedule.setCreatedOn(new Date());
				paymentSchedule.setCreatedBy(getLoggedInUserId());
				paymentSchedule.setTypeId(WebConstants.PAYMENT_TYPE_RENT_ID);
				paymentSchedule.setAmount(amount);
			 	paymentSchedule.setUpdatedOn(new Date());
				paymentSchedule.setUpdatedBy(getLoggedInUserId());
				paymentSchedule.setDirectionTypeInbound(0L);
				paymentSchedule.setIsReceived("N");
				paymentSchedule.setDescription("Maintenance Request Payment");  
				paymentSchedule.setOwnerShipTypeId(this.getUnitView().getPropertyCategoryId());
				paymentSchedule.setStatusId(ddvDraft.getDomainDataId());
				paymentSchedule.setStatusAr(ddvDraft.getDataDescAr());
				paymentSchedule.setStatusEn(ddvDraft.getDataDescEn());
				List<PaymentScheduleView> psList =   new ArrayList<PaymentScheduleView>(0);
			    psList.add(paymentSchedule);	
			    viewRootMap.put(WebConstants.ServiceContractPaySchTab.PAYMENT_SCH_LIST,psList);
 			}
		}
       
	}
	private boolean hasSaveFollowUpErrors()
	{
		boolean hasSaveFollowUpErrors=false;
		if(txtboxRemarks.getValue() == null || txtboxRemarks.getValue().toString().length() <= 0)
		{
			hasSaveFollowUpErrors = true;
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.provideFollowupRemarks"));
		}
		if (this.getActualCost() == null || this.getActualCost().trim().length() <= 0) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.actualCoseReq"));
			return true;
		}
		return hasSaveFollowUpErrors;
	}
	@SuppressWarnings("unchecked")
	public void btnSaveFollowUp_Click()
	{
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		RequestServiceAgent rsa = new RequestServiceAgent(); 
		try 
		{
			String eventDesc =	 "";
			String successMsg =	 "";
			if(!hasSaveFollowUpErrors())
			{
				 tabFollowUp_Click();
				 FollowUpView followUpView = new FollowUpView();
				 requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
				 setFollowUpView(followUpView );
				 followUpView = rsa.addFollowup(followUpView);
				
			 	 List<FollowUpView> fvl=this.getFollowUpList();
			 	 fvl.add(0,followUpView);
			 	 viewRootMap.put("followUpList",fvl);
				 this.getFollowUpList();
				 viewRootMap.put("RECORD_SIZE", this.getFollowUpList().size());
				 eventDesc  = MessageConstants.RequestEvents.REQUEST_SAVED_FOLLOWUP;
				 successMsg = ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.FOLLOWUP_SAVED_SUCCESSFULLY); 
				 saveCommentAndAttachements( eventDesc);
				 successMessages.add(successMsg);
				 tabPanel.setSelectedTab(TAB_ID.FollowUp);
				 clearFollowUpFields();
				 
			}
		}
		catch(Exception e)
		{
			logger.LogException("btnSaveFollowUp_Click|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		
	}	
	@SuppressWarnings("unchecked")
	
	public void imgContractSearch_Click()
	{
		
		FacesContext facesContext =getFacesContext();
		if(this.getUnitView()!=null)
		{
		    if(this.getUnitView().getPropertyId()!=null && this.getUnitView().getPropertyCommercialName().trim().length()>0)
		    {
			sessionMap.put(ServiceContractListBackingBean.PROPERTY_COMM_NAME, this.getUnitView().getPropertyCommercialName());
			sessionMap.put(ServiceContractListBackingBean.PROPERTY_ID, this.getUnitView().getPropertyId());
		    }
		    if(this.getUnitView().getUnitId()!=null )
		    {
			sessionMap.put(ServiceContractListBackingBean.UNIT_ID, this.getUnitView().getUnitId());
			sessionMap.put(ServiceContractListBackingBean.UNIT_NUMBER, this.getUnitView().getUnitNumber());
		    }
		    String javaScriptText="showServiceContract();";
		    AddResource addResource = AddResourceFactory.getInstance(facesContext);
            addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);
		}
        
		
	}
	@SuppressWarnings("unchecked")
	public void getIncompleteRequestTasks()
	{
		logger.logInfo("getIncompleteRequestTasks started...");
		String taskType = "";
		 
		try{
			
			RequestView rv =(RequestView)viewRootMap.get(REQUEST_VIEW);
	   		Long requestId =rv.getRequestId();
	   		RequestServiceAgent requestServiceAgent = new RequestServiceAgent();
	   		RequestTasksView reqTaskView = requestServiceAgent.getIncompleteRequestTask(requestId);
	   		String taskId = reqTaskView.getTaskId();
	   		String user = getLoggedInUserId();
	   		
	   		if(taskId!=null)
	   		{
		   		BPMWorklistClient bpmWorkListClient = null;
		        String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
				bpmWorkListClient = new BPMWorklistClient(contextPath);
				UserTask userTask = bpmWorkListClient.getTaskForUser(taskId, user);
				 
				taskType = userTask.getTaskType();
				viewRootMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK,userTask);
				logger.logInfo("Task Type is:" + taskType);
	   		}
	   		else
	   		{
	   			logger.logInfo("getIncompleteRequestTasks |no task present for requestId..."+requestId);
				
	   		}
	   		logger.logInfo("getIncompleteRequestTasks  completed successfully!!!");
		}
		catch(PIMSWorkListException ex)
		{
			if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHORIZED_FOR_TASK)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authorized for task...");
	   		}
			else if(ex.getExceptionCode()==ExceptionCodes.USER_NOT_AUTHENTIC)
			{
				logger.logWarning("getIncompleteRequestTasks |user not authenticated...");
	   		}
			else
			{
				logger.LogException("getIncompleteRequestTasks |Exception...",ex);
			}
		}
		catch(PimsBusinessException ex)
		{
			logger.LogException("getIncompleteRequestTasks |No task found...",ex);
		}
		catch (Exception exception) {
			logger.LogException("getIncompleteRequestTasks |No task found...",exception);
		}	
		
		
		
	}
	
	@SuppressWarnings("unchecked")
	private void setPageModeBasedOnRequestStatus()
	{
		
		if(requestView != null && requestView.getStatusId()!=null)
		{
			DomainDataView ddRequestStatus = CommonUtil.getDomainDataFromId( getRequestStatusList(),requestView.getStatusId());
			if(ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_NEW))
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.EDIT);
				return;
			}
			else if (
					 ddRequestStatus.getDataValue().equals( WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED ) 
					 || 
					 ddRequestStatus.getDataValue().equals( WebConstants.REQUEST_STATUS_REVIEW_DONE)
					)
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.APPROVAL_REQUIRED);
				if( ddRequestStatus.getDataValue().equals( WebConstants.REQUEST_STATUS_REVIEW_DONE) )
				{
					tabPanel.setSelectedTab("reviewTab");
				}
				return;
			}
			else if ( ddRequestStatus.getDataValue().equals( WebConstants.REQUEST_STATUS_EVALUATED ) )
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.EVALUATED);
				return;
			}
			
			else if (ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_APPROVED))
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.APPROVED);
				return;
			}
			else if (ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_EVALUATION_REQUIRED))
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.EVALUATION_REQ);
				return;
			}
			else if (ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED))
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.REVIEW_REQ);
				tabPanel.setSelectedTab("reviewTab");
				return;
			}
			else if (ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_ON_FOLLOW_UP))
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.ON_FOLLOW_UP);
				tabPanel.setSelectedTab("tabFollowUp");
				tabFollowUp_Click();
				return;
			}
			else if (ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_FOLLOWUP_DONE))
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.FOLLOW_UP_DONE);
				return;
			}
			else if (ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_FOLLOW_UP))
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.FOLLOW_UP);
				return;
			}
			else if (ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_COMPLETE )|| ddRequestStatus.getDataValue().equals(WebConstants.REQUEST_STATUS_REJECTED ))
			{
				viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.VIEW);
				return;
			}
		}

	}

	@SuppressWarnings("unchecked")
	protected List<DomainDataView>  getRequestStatusList() {
		List<DomainDataView> ddvRequestStatusList = null;
		if(!viewRootMap.containsKey(DD_REQUEST_STATUS_LIST))
		{
		ddvRequestStatusList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
		viewRootMap.put(DD_REQUEST_STATUS_LIST, ddvRequestStatusList );
		}
		else
			ddvRequestStatusList =(ArrayList<DomainDataView>)viewRootMap.get(DD_REQUEST_STATUS_LIST);
		return ddvRequestStatusList ;
	}
	@SuppressWarnings("unchecked")
	private void getRequestView()throws PimsBusinessException 
	{
		PropertyServiceAgent psa = new PropertyServiceAgent();
		//RequestView rv = new RequestView();
		//rv.setRequestTypeId(WebConstants.REQUEST_TYPE_MINOR_MAINTENANCE_REQUEST);
		//rv.setRequestId(new Long(requestId));
		//List<RequestView>requestViewList= psa.getAllRequests(rv, null, null, null);
	    requestView = psa.getRequestById(new Long(requestId));
		//if(requestViewList.size()>0)
		//{
			//requestView =requestViewList.get(0); 
			viewRootMap.put(REQUEST_VIEW,requestView );
			sessionMap.put("REQUEST_DATE", requestView.getRequestDate());
			 
		//}
    }
	
	
	protected PersonView getContractorInfo()throws PimsBusinessException
    {
    	
    	
	    if(viewRootMap.containsKey(CONTRACTOR_VIEW))
    	{
    		contractor=(PersonView)viewRootMap.get(CONTRACTOR_VIEW);
	    	if(hdnContractorId!=null && hdnContractorId.trim().length()>0 && 
	    			contractor.getPersonId()!=null && contractor.getPersonId().compareTo(new Long(hdnContractorId))!=0)
	    	{
	    		getContractorInfoFromDB();
	    	
	    			
	    	}
	     
    	}
    	else if(hdnContractorId!=null && hdnContractorId.trim().length()>0 )
    	{
    		getContractorInfoFromDB();
    	    
    	 
    			
    	}
    	else 
    		return null;
	    
	    return contractor;	    
    }

	private void getContractorInfoFromDB() throws PimsBusinessException {
		PropertyServiceAgent psa=new PropertyServiceAgent();
		PersonView pv=new PersonView();
		pv.setPersonId(new Long(this.getHdnContractorId()));
		List<PersonView> investorsList =  psa.getPersonInformation(pv);
		//contractor =  csa.getContractorById(new Long(hdnContractorId), null);
		if(investorsList.size()>0)
		{
			viewRootMap.put(CONTRACTOR_VIEW, investorsList.get(0) );
		    contractor =  (PersonView)investorsList.get(0) ;
		}
	}
		
	private void refresh() throws PimsBusinessException {
		
		getRequestView();
		
	}
	
		
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
    	try
    	{
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = (UserTask) viewRootMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    	}
    	catch(PIMSWorkListException ex)
    	{
    		
    		logger.LogException("setTaskOutCome| Error Occured...",ex);
    		throw ex;
    	}
    	catch(Exception ex)
    	{
    		logger.LogException( "setTaskOutCome| Error Occured...",ex);
    		throw ex;
    	}
    }
    public String getErrorMessages()
	{

    	return CommonUtil.getErrorMessages(errorMessages);
	}
    public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}
    public void saveSystemComments(String sysNoteType) throws Exception
    {
    		String notesOwner = WebConstants.REQUEST;
    		if(requestId!=null && requestId.trim().length()>0)
    		{
    		  Long requestsId = new Long(requestId);
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, requestsId);
    		}
    }
	public Boolean saveComments(Long referenceId) throws Exception
    {
		Boolean success = false;
		String notesOwner = WebConstants.REQUEST;
		if(txtRemarks!=null && txtRemarks.length()>0)
		{
		   CommonUtil.saveRemarksAsComments(referenceId, txtRemarks, notesOwner) ;
		}
		NotesController.saveNotes(notesOwner, referenceId);
		success = true;
		return success;
    }
	public Boolean saveAttachments(String referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null){
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments(Long requestId){
    		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
    		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, viewRootMap.get(PROCEDURE_TYPE).toString());
    		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
        	String externalId = viewRootMap.get(EXTERNAL_ID).toString();
        	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
    		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
    		viewMap.put("noteowner", WebConstants.REQUEST);
    		if(requestId!= null){
    	    	String entityId = requestId.toString();
    			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
    			viewMap.put("entityId", entityId);
    		}
    	}	
	@SuppressWarnings( "unchecked" )
    public void tabAttachmentsComments_Click()
    {
		if( viewRootMap.get(REQUEST_VIEW) !=null )
		{
		  requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		  sessionMap.put("REQUEST_DATE", requestView.getRequestDate());
		  if(requestView != null && requestView.getRequestId()!=null)
			 loadAttachmentsAndComments(requestView.getRequestId());
		}
    }
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	   UserTask userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
      viewRootMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
      if(userTask.getTaskAttributes().get("REQUEST_ID")!=null)
	  this.requestId = userTask.getTaskAttributes().get("REQUEST_ID").toString();
       sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
    }
	@SuppressWarnings( "unchecked" )
	private void openFromFileSearch() throws Exception 
	{
		setInheritanceFileView( (InheritanceFileView)sessionMap.remove(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW)  );
	}
	public String getLocale(){
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	public boolean getIsArabicLocale()
	{
	    	return CommonUtil.getIsArabicLocale();
	}
	public boolean getIsEnglishLocale()
	{
	     	return CommonUtil.getIsEnglishLocale();
	}
	public String getDateFormat()
	{
		    return CommonUtil.getDateFormat();
	}
	public TimeZone getTimeZone()
	{
		    return CommonUtil.getTimeZone();
	}
    @SuppressWarnings("unchecked")
	public void findApplicantById()throws PimsBusinessException
    {
    	String methodName="cmdAddApplicant_Click";
    	logger.logInfo(methodName+"| Start");
    	PersonView applicantView;
    	if(viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICANT_VIEW) && 
    	   viewRootMap.get(WebConstants.ApplicationDetails.APPLICANT_VIEW)!=null		
    	   )
    	{
    		applicantView=(PersonView)viewRootMap.get(WebConstants.ApplicationDetails.APPLICANT_VIEW);
	    	if(hdnApplicantId!=null && applicantView.getPersonId().compareTo(new Long(hdnApplicantId))!=0)
	    	{
	    		PropertyServiceAgent psa=new PropertyServiceAgent();
	    		PersonView pv=new PersonView();
	    		pv.setPersonId(new Long(hdnApplicantId));
	    		List<PersonView> personsList =  psa.getPersonInformation(pv);
	    		if(personsList.size()>0)
	    		{
	    			pv = personsList.get(0);
	    			populateApplicantDetails(pv);
	    			viewRootMap.put(WebConstants.ApplicationDetails.APPLICANT_VIEW,pv);
	    		}
	    			
	    	}
    	}
    	else if(hdnApplicantId!=null && hdnApplicantId.trim().length()>0)
    	{
    		PropertyServiceAgent psa=new PropertyServiceAgent();
    		PersonView pv=new PersonView();
    		pv.setPersonId(new Long(hdnApplicantId));
    		List<PersonView> personsList=  psa.getPersonInformation(pv);
    		if(personsList.size()>0)
    		{
    			pv = personsList.get(0);
    			populateApplicantDetails(pv);
    			viewRootMap.put(WebConstants.ApplicationDetails.APPLICANT_VIEW,pv);
    		}	
    	}
    	
    	logger.logInfo(methodName+"| Finish");
    }

	@SuppressWarnings("unchecked")
	private void populateApplicationDetailsTab() {
		//PopulatingApplicationDetailsTab
		if(requestView!=null)
		{
			if(requestView.getRequestNumber()!=null)
			viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER, requestView.getRequestNumber());
			if(requestView.getRequestDate()!=null)
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE, requestView.getRequestDate());
			if(requestView.getStatusId()!=null)
			{
				if(getIsEnglishLocale())
				{
					if(requestView.getStatusEn()!=null)	
				    viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, requestView.getStatusEn());
				}else if(getIsArabicLocale())
				{
					if(requestView.getStatusAr()!=null)
					 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, requestView.getStatusAr());
				}
			}
			else
			{
				List<DomainDataView >ddvList= commonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
				DomainDataView ddv=commonUtil.getIdFromType(ddvList, WebConstants.REQUEST_STATUS_NEW);
				if(getIsEnglishLocale())
				{
						
				    viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, ddv.getDataDescEn());
				}else if(getIsArabicLocale())
				{
					
					 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS, ddv.getDataDescAr());
				}
			}
			
			if(requestView.getDescription() != null && requestView.getDescription().length() > 0)
			{
				viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION, requestView.getDescription());
			}
			if(requestView.getApplicantView()!=null)
				populateApplicantDetails(requestView.getApplicantView());
			loadAttachmentsAndComments(requestView.getRequestId());
			
		}
	}
    @SuppressWarnings("unchecked")
	private void populateApplicantDetails(PersonView pv)
    {
        List<DomainDataView>  ddl = (ArrayList<DomainDataView>)viewRootMap.get("PERSON_TYPE_LIST");
		DomainDataView ddvCompany = CommonUtil.getIdFromType(ddl,WebConstants.TENANT_TYPE_COMPANY);
		viewRootMap.put(WebConstants.TENANT_TYPE_COMPANY, ddvCompany);
		DomainDataView ddvIndividual = CommonUtil.getIdFromType(ddl,WebConstants.TENANT_TYPE_INDIVIDUAL);
		viewRootMap.put(WebConstants.TENANT_TYPE_INDIVIDUAL, ddvIndividual);
		
    	if(pv.getPersonId()!=null )
       	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,pv.getPersonId());
      if(pv.getPersonFullName()!=null && pv.getPersonFullName().trim().length()>0)
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,pv.getPersonFullName());
     else if(pv.getCompanyName()!=null && pv.getCompanyName().trim().length()>0)
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,pv.getCompanyName());
      if(!viewRootMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE) )
      {
	      String personType = getPersonType(pv);
	      if(personType !=null)
	         viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,personType);
      }
     if(pv.getCellNumber()!=null)
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,pv.getCellNumber());
     if(pv.getPersonId()!=null)
     {
    	 viewRootMap.put(WebConstants.ApplicationDetails.APPLICATION_ID,pv.getPersonId());
    	 hdnApplicantId = pv.getPersonId().toString();
     }
    	 
    }
    private String getPersonType(PersonView tenantView) {

		DomainDataView ddv;
		if (tenantView!= null &&  tenantView.getIsCompany().compareTo(new Long(1)) == 0) 
			ddv = (DomainDataView) viewRootMap.get(WebConstants.TENANT_TYPE_COMPANY);
		else
			ddv = (DomainDataView) viewRootMap.get(WebConstants.TENANT_TYPE_INDIVIDUAL);
		if (getIsArabicLocale())
			return ddv.getDataDescAr();
		else if (getIsEnglishLocale())
			return ddv.getDataDescEn();

		return "";

	}

	
	public RequestView getRequestViewFromViewRoot()
	{
		if(viewRootMap.get(REQUEST_VIEW)!=null)
			return (RequestView)viewRootMap.get(REQUEST_VIEW);
		else
			return null;
		
	}
	public void tabAuditTrail_Click()
	{
		String methodName="tabAuditTrail_Click";
    	logger.logInfo(methodName+"|"+"Start..");
    	errorMessages = new ArrayList<String>(0);
    	try	
    	{
    	  RequestHistoryController rhc=new RequestHistoryController();
    	  if(requestId!=null && requestId.trim().length()>=0)
    	    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,requestId);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
    
		
	}
	
	private boolean hasApprovalErrors()
	{
//	  if (this.getContractor() == null	|| this.getContractor().getPersonId() == null) 
//	    {
//			errorMessages.add(CommonUtil.getBundleMessage("errMsg.selectVender"));
//			return true;
//		}
//		if (this.getActualCost() == null || this.getActualCost().length() <= 0) 
//		{
//			errorMessages.add(CommonUtil.getBundleMessage("errMsg.actualCoseReq"));
//			return true;
//		}
		if(  
				cmbPaymentSource.getValue() == null || 
				cmbPaymentSource.getValue().toString().equals( "-1" ) 
		    )
		{
			errorMessages.add(CommonUtil.getBundleMessage("mems.payment.err.msg.paymentSrcReq"));
			tabPanel.setSelectedTab("deductionListId");
			return true;
		}
		if(
				cmbPaymentSource.getValue() != null && 
				cmbPaymentSource.getValue().toString().equals( String.valueOf( WebConstants.PaymentSrcType.AID_ID) ) && 

				( cmbDisbursementSource.getValue()==null || cmbDisbursementSource.getValue().equals("-1") )
		    )
		{
			errorMessages.add(CommonUtil.getBundleMessage("mems.normaldisb.err.msg.disbSrcReq"));
			tabPanel.setSelectedTab("deductionListId");
			return true;
		}
		if(
				//cmbPaymentSource.getValue() != null && 
				//cmbPaymentSource.getValue().toString().equals( String.valueOf( WebConstants.PaymentSrcType.SELF_ACCOUNT_ID ) ) && 
				getDeductFromList() != null && getDeductFromList().size() > 0
		  )
		{
			if( this.getActualCost() != null && 
				this.getActualCost().trim().length()> 0 && 
				getSumOfDistributedAmount( getDeductFromList() ).compareTo( new Double( this.getActualCost() ) ) != 0
	          )
			{
				errorMessages.add(CommonUtil.getBundleMessage("errMsg.distributedAmountnotEqualToActual"));
				tabPanel.setSelectedTab("deductionListId");
				return true;
			}
			else if(
					(
						this.getActualCost() == null || 
					    this.getActualCost().trim().length()<= 0 
					 )&& 
					  this.getEstimatedCost() != null  && 
					  this.getEstimatedCost().trim().length() > 0 && 
					 getSumOfDistributedAmount(getDeductFromList()).compareTo(new Double(this.getEstimatedCost())) != 0
				    )
			{
				errorMessages.add(CommonUtil.getBundleMessage("errMsg.distributedAmountnotEqualToActual"));
				tabPanel.setSelectedTab("deductionListId");
				return true;
			}
		}
	  return false;
	}
	private Double getSumOfDistributedAmount(List<DistributePopUpView> deductFromList) 
	{
		Double totalAmount = 0D;
		for(DistributePopUpView deductFrom :  deductFromList)
		{
			if(deductFrom.getSelected()!= null && deductFrom.getSelected() && deductFrom.getAmountToBeDeducted()!=null)
			{
				totalAmount += roundDecimals(new Double(deductFrom.getAmountToBeDeducted()));
				//System.out.println(deductFrom.getAmountToBeDeducted());
				//System.out.println(totalAmount);
			}
		}
		return roundDecimals(totalAmount);
	}



	@SuppressWarnings("unchecked")
	public void btnApprove_Click()
	{
		successMessages = new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			if(hasApprovalErrors())
			{return;}
				
				putRequestDetailInRequest();
				
				putMaintenaceDeductionInView();
				
				requestView.setDistributionCriteria(new Long(cmbDeductCrit.getValue().toString()));
				if( cmbDisbursementSource.getValue() != null )
				{
					requestView.setRequestPriorityId( new Long(cmbDisbursementSource.getValue().toString()));
				}
				requestView.setStatusId(WebConstants.REQUEST_STATUS_APPROVED_ID);
				requestView.setUpdatedBy( getLoggedInUserId() );
				
				requestView = new RequestService().persistMinorMaintenanceRequestInTransaction(requestView,getMaintenanceDeductionList());
//				if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
//					 getIncompleteRequestTasks();
				setTaskOutCome(TaskOutcome.APPROVE);
				viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
				saveCommentAndAttachements(MessageConstants.RequestEvents.REQUEST_APPROVED);
		    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.MAINTENANCE_REQUEST_APPROVED));
		    	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("btnApprove_Click|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
		
	}
	private void putRequestDetailInRequest()throws Exception 
	{
			Set<RequestDetailView> requestDetails = new HashSet<RequestDetailView>();
			RequestDetailView requestDetail = new RequestDetailView();
			setRequestView();
			
			if(
				requestView != null && requestView.getRequestId() != null && requestView.getRequestDetailView() != null &&
				requestView.getRequestDetailView().size() > 0
			   )
			{
				requestDetail = requestView.getRequestDetailView().iterator().next();
				
				if(this.getEstimatedCost() != null && this.getEstimatedCost().length() > 0)
					requestDetail.setEstimatedCost(new Double(this.getEstimatedCost()));
				
				if(this.getTxtRemarks() != null && this.getTxtRemarks().length() > 0)
					requestDetail.setEvaluationRemarks(this.getTxtRemarks());
				
				if(this.getActualCost() != null && this.getActualCost().length() > 0)
					requestDetail.setActualCost(new Double(this.getActualCost()));
				
				requestDetail.setRequestId(requestView.getRequestId());
			}
			else
			{
				requestDetail.setCreatedOn(new Date());
				requestDetail.setCreatedBy(getLoggedInUserId());
				requestDetail.setIsDeleted(new Long(0));
				requestDetail.setRecordStatus(new Long(1));
				requestDetail.setRequestDetailId(null);
				if(requestView != null && requestView.getRequestId() != null)
				{
					requestDetail.setRequestId(requestView.getRequestId());
				}
				if(this.getActualCost() != null && this.getActualCost().length() > 0)
					requestDetail.setActualCost(new Double(this.getActualCost()));
				
				if(this.getEstimatedCost() != null && this.getEstimatedCost().length() > 0)
					requestDetail.setEstimatedCost(new Double(this.getEstimatedCost()));
				
				if(this.getTxtRemarks() != null && this.getTxtRemarks().length() > 0)
					requestDetail.setEvaluationRemarks(this.getTxtRemarks());
				
			}
			requestDetail.setUpdatedOn(new Date());
			requestDetail.setUpdatedBy(getLoggedInUserId());
			requestDetails.add(requestDetail);
			requestView.setRequestDetailView(requestDetails);
	}



	public void btnReject_Click()
	{
		String methodName ="btnReject_Click";
		logger.logInfo(methodName+"|"+"Start..");
		errorMessages = new ArrayList<String>(0);
		try	
    	{
//	    	if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
//				  getIncompleteRequestTasks();
			setTaskOutCome(TaskOutcome.REJECT);
			viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			new RequestService().setRequestAsRejected(new Long(this.requestId), getLoggedInUserId());
			saveCommentAndAttachements(MessageConstants.RequestEvents.REQUEST_REJECTED);
	    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.MAINTENANCE_REQUEST_REJECTED));
	    	imgClearContract.setRendered(false);
	    	imgSearchContractor.setRendered(false);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
		
	}
	
	
	@SuppressWarnings("unchecked")
	public void btnComplete_Click()
	{
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			if(hasCompletionErrors())
			{return;}
			
				RequestView requestView = (RequestView) viewMap.get(REQUEST_VIEW);
				if(
						cmbPaymentSource.getValue() != null && 
						cmbPaymentSource.getValue().toString().equals( String.valueOf( WebConstants.PaymentSrcType.AID_ID ) )
				  )
				{
					requestView.setRequestPriorityId( new Long(cmbDisbursementSource.getValue().toString()));
				}
				new RequestService().completeMinorMaintenanceRequest(requestView ,requestView.getRequestId(), getLoggedInUserId());
				
		    	saveCommentAndAttachements(MessageConstants.RequestEvents.REQUEST_COMPLETED_AND_DISBURSEMENT_CREATED);
//		    	 if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
//					  getIncompleteRequestTasks();
	        	setTaskOutCome(TaskOutcome.OK);
				viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		    	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
		    	successMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.MAINTENANCE_REQUEST_COMPLETED));
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("btnComplete_Click|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
		
	}
	
		
	public org.richfaces.component.html.HtmlTab getTabPaymentTerms() {
		return tabPaymentTerms;
	}

	public void setTabPaymentTerms(
			org.richfaces.component.html.HtmlTab tabPaymentTerms) {
		this.tabPaymentTerms = tabPaymentTerms;
	}
	public org.richfaces.component.html.HtmlTab getTabAuditTrail() {
		return tabAuditTrail;
	}
	
	
	public void setTabAuditTrail(org.richfaces.component.html.HtmlTab tabAuditTrail) {
		this.tabAuditTrail = tabAuditTrail;
	}
	public boolean getIsViewModePopUp()
	{
		boolean isViewModePopup=true;
		if(
			!viewRootMap.containsKey(WebConstants.VIEW_MODE) || 
			!viewRootMap.get(WebConstants.VIEW_MODE).equals(WebConstants.LEASE_CONTRACT_VIEW_MODE_POPUP)
		  )
		{
			isViewModePopup=false;
		}
		
		return isViewModePopup;
	}

	public String getPageTitle() {
		if(viewRootMap.containsKey("pageTitle") && viewRootMap.get("pageTitle")!=null)
			pageTitle = viewRootMap.get("pageTitle").toString();
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
		if(this.pageTitle !=null)
			viewRootMap.put("pageTitle",this.pageTitle );
			
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}


	public String getPageMode() {
		if(viewRootMap.get(Page_Mode.PAGE_MODE)!=null)
			pageMode =viewRootMap.get(Page_Mode.PAGE_MODE).toString();
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public String getContractCreatedOn() {
		return createdOn;
	}

	public void setContractCreatedOn(String contractCreatedOn) {
		this.createdOn = contractCreatedOn;
	}

	public String getContractCreatedBy() {
		return createdBy;
	}

	public void setContractCreatedBy(String contractCreatedBy) {
		this.createdBy = contractCreatedBy;
	}

	public HtmlCommandButton getBtnApprove() {
		return btnApprove;
	}

	public void setBtnApprove(HtmlCommandButton btnApprove) {
		this.btnApprove = btnApprove;
	}

	public HtmlCommandButton getBtnReject() {
		return btnReject;
	}

	public void setBtnReject(HtmlCommandButton btnReject) {
		this.btnReject = btnReject;
	}

	public HtmlCommandButton getBtnComplete() {
		return btnComplete;
	}

	public void setBtnComplete(HtmlCommandButton btnComplete) {
		this.btnComplete = btnComplete;
	}



	public HtmlPanelGrid getTbl_Action() {
		return tbl_Action;
	}

	public void setTbl_Action(HtmlPanelGrid tbl_Action) {
		this.tbl_Action = tbl_Action;
	}

	public String getTxtRemarks() 
	{
		if(viewRootMap.containsKey("txtRemarks") && viewRootMap.get("txtRemarks")!=null)
			txtRemarks = viewRootMap.get("txtRemarks").toString();
		return txtRemarks;
	}

	@SuppressWarnings("unchecked")
	public void setTxtRemarks(String txtRemarks) 
	{
		this.txtRemarks = txtRemarks;
		if(this.txtRemarks !=null)
			viewRootMap.put("txtRemarks",this.txtRemarks);
	}

	
		public HtmlCommandButton getBtnSave() {
		return btnSave;
	}

	public void setBtnSave(HtmlCommandButton btnSave) {
		this.btnSave = btnSave;
	}

	public HtmlCommandButton getBtnSend_For_Approval() {
		return btnSend_For_Approval;
	}

	public void setBtnSend_For_Approval(HtmlCommandButton btnSend_For_Approval) {
		this.btnSend_For_Approval = btnSend_For_Approval;
	}

			public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public PersonView getContractor() {
	 if(viewRootMap.containsKey(CONTRACTOR_VIEW))
		 contractor = (PersonView)viewRootMap.get(CONTRACTOR_VIEW);
		return contractor;
	}
	@SuppressWarnings("unchecked")
	public void setContractor(PersonView contractor) {
		this.contractor = contractor;
		if(this.contractor!=null)
			viewRootMap.put(CONTRACTOR_VIEW,this.contractor);
	}

	public org.richfaces.component.html.HtmlTab getTabBasicInfo() {
		return tabBasicInfo;
	}

	public void setTabBasicInfo(
			org.richfaces.component.html.HtmlTab tabBasicInfo) {
		this.tabBasicInfo = tabBasicInfo;
	}

	public HtmlGraphicImage getImgUnitSearch() {
		return imgUnitSearch;
	}

	public void setImgUnitSearch(HtmlGraphicImage imgUnitSearch) {
		this.imgUnitSearch = imgUnitSearch;
	}

	public UnitView getUnitView() {
		if(viewRootMap.containsKey(WebConstants.UNIT_VIEW))
				unitView = (UnitView)viewRootMap.get(WebConstants.UNIT_VIEW);
		return unitView;
	}

	public void setUnitView(UnitView unitView) {
		this.unitView = unitView;
		if(this.unitView!=null)
			viewRootMap.put(WebConstants.UNIT_VIEW,this.unitView);
	}
	public UserView getUserView() {
		if(viewRootMap.containsKey(WebConstants.USER_VIEW))
				userView = (UserView)viewRootMap.get(WebConstants.USER_VIEW);
		return userView;
	}

	public void setUserView(UserView userView) {
		this.userView = userView;
		if(this.userView!=null)
			viewRootMap.put(WebConstants.USER_VIEW,this.userView);
	}

	public ContractView getContractView() {
		if(viewRootMap.containsKey(CONTRACT_VIEW))
				contractView = (ContractView)viewRootMap.get(CONTRACT_VIEW);
		return contractView;
	}

	public void setContractView(ContractView contractView) {
		this.contractView = contractView;
		if(this.contractView!=null)
			viewRootMap.put(CONTRACT_VIEW,this.contractView);
	}
	public String getHdnPropertyId() {
		return hdnPropertyId;
	}

	public void setHdnPropertyId(String hdnPropertyId) {
		this.hdnPropertyId = hdnPropertyId;
	}

	public HtmlGraphicImage getImgPropertySearch() {
		return imgPropertySearch;
	}

	public void setImgPropertySearch(HtmlGraphicImage imgPropertySearch) {
		this.imgPropertySearch = imgPropertySearch;
	}

	public HtmlSelectOneMenu getCmbMaintenanceType() {
		return cmbMaintenanceType;
	}

	public void setCmbMaintenanceType(HtmlSelectOneMenu cmbMaintenanceType) {
		this.cmbMaintenanceType = cmbMaintenanceType;
	}

	public String getSelectOneMaintenanceType() {
		if(viewRootMap.containsKey("selectOneMaintenanceType"))
				selectOneMaintenanceType = viewRootMap.get("selectOneMaintenanceType").toString();
		return selectOneMaintenanceType;
	}

	public void setSelectOneMaintenanceType(String selectOneMaintenanceType) {
		this.selectOneMaintenanceType = selectOneMaintenanceType;
		if(this.selectOneMaintenanceType !=null)
			viewRootMap.put("selectOneMaintenanceType", this.selectOneMaintenanceType);
	}

	@SuppressWarnings( "unchecked" )
	public List<String> getSelectManyWorkType() {
		if(viewRootMap.containsKey("selectOneWorkType"))
		{
			selectManyWorkType =  (List<String>)viewRootMap.get("selectOneWorkType");
		}
		return selectManyWorkType;
	}

	@SuppressWarnings( "unchecked" )
	public void setSelectManyWorkType( List<String> selectManyWorkType) {
		
		this.selectManyWorkType = selectManyWorkType;
		if( this.selectManyWorkType !=null )
		{
			viewRootMap.put("selectOneWorkType", this.selectManyWorkType);
		}
	}
	public HtmlSelectManyCheckbox getCmbWorkType() {
		return cmbWorkType;
	}

	public void setCmbWorkType(HtmlSelectManyCheckbox cmbWorkType) {
		this.cmbWorkType = cmbWorkType;
	}

	public String getHdnApplicantId() {
		return hdnApplicantId;
	}

	public void setHdnApplicantId(String hdnApplicantId) {
		this.hdnApplicantId = hdnApplicantId;
	}

	public org.richfaces.component.html.HtmlTab getTabApplicationDetails() {
		return tabApplicationDetails;
	}

	public void setTabApplicationDetails(
			org.richfaces.component.html.HtmlTab tabApplicationDetails) {
		this.tabApplicationDetails = tabApplicationDetails;
	}

	public HtmlCommandButton getBtnCompleteSiteVisit() {
		return btnCompleteSiteVisit;
	}

	public void setBtnCompleteSiteVisit(HtmlCommandButton btnCompleteSiteVisit) {
		this.btnCompleteSiteVisit = btnCompleteSiteVisit;
	}

	public HtmlCommandButton getBtnSaveSiteVisit() {
		return btnSaveSiteVisit;
	}

	public void setBtnSaveSiteVisit(HtmlCommandButton btnSaveSiteVisit) {
		this.btnSaveSiteVisit = btnSaveSiteVisit;
	}


	public HtmlDataTable getSiteVisitDataTable() {
		return siteVisitDataTable;
	}

	public void setSiteVisitDataTable(HtmlDataTable siteVisitDataTable) {
		this.siteVisitDataTable = siteVisitDataTable;
	}

	public List<FollowUpView> getFollowUpList() {
		if(viewRootMap.containsKey("followUpList"))
			{
				followUpList =(ArrayList<FollowUpView>)viewRootMap.get("followUpList");
				viewRootMap.put("RECORD_SIZE",followUpList.size());
			}
		return followUpList;
	}



	public void setFollowUpList(List<FollowUpView> followUpList) {
		this.followUpList = followUpList;
		if(this.followUpList  !=null && this.followUpList.size()>0)
			viewRootMap.put("followUpList",this.followUpList );
	}


	public HtmlDataTable getFollowUpDataTable() {
		return followUpDataTable;
	}



	public void setFollowUpDataTable(HtmlDataTable followUpDataTable) {
		this.followUpDataTable = followUpDataTable;
	}




	public HtmlCommandButton getBtnSaveFollowUp() {
		return btnSaveFollowUp;
	}



	public void setBtnSaveFollowUp(HtmlCommandButton btnSaveFollowUp) {
		this.btnSaveFollowUp = btnSaveFollowUp;
	}



	public org.richfaces.component.html.HtmlTab getTabFollowUp() {
		return tabFollowUp;
	}



	public void setTabFollowUp(org.richfaces.component.html.HtmlTab tabFollowUp) {
		this.tabFollowUp = tabFollowUp;
	}



	public HtmlGraphicImage getImgContractSearch() {
		return imgContractSearch;
	}



	public void setImgContractSearch(HtmlGraphicImage imgContractSearch) {
		this.imgContractSearch = imgContractSearch;
	}



	public String getHdnContractId() {
		return hdnContractId;
	}



	public void setHdnContractId(String hdnContractId) {
		this.hdnContractId = hdnContractId;
	}
	
	public String getContractorScreenQueryStringPopUpMode() {
		
		return ContractorSearch.Keys.MODE_SELECT_ONE_POPUP;
	}

	

	public String getContractorScreenQueryStringViewMode() {
		return ContractorSearch.Keys.PAGE_MODE;
	}



	public HtmlInputText getTxtContractNum() {
		return txtContractNum;
	}



	public void setTxtContractNum(HtmlInputText txtContractNum) {
		this.txtContractNum = txtContractNum;
	}



	public HtmlInputText getTxtContractorNum() {
		return txtContractorNum;
	}



	public void setTxtContractorNum(HtmlInputText txtContractorNum) {
		this.txtContractorNum = txtContractorNum;
	}



	public HtmlGraphicImage getImgSearchContractor() {
		return imgSearchContractor;
	}



	public void setImgSearchContractor(HtmlGraphicImage imgSearchContractor) {
		this.imgSearchContractor = imgSearchContractor;
	}



	public HtmlCommandButton getBtnOtherSave() {
		return btnOtherSave;
	}



	public void setBtnOtherSave(HtmlCommandButton btnOtherSave) {
		this.btnOtherSave = btnOtherSave;
	}



	public HtmlSelectOneMenu getCmbRequestPriority() {
		return cmbRequestPriority;
	}



	public void setCmbRequestPriority(HtmlSelectOneMenu cmbRequestPriority) {
		this.cmbRequestPriority = cmbRequestPriority;
	}



	public String getSelectOneRequestPriority() {
		return selectOneRequestPriority;
	}



	public void setSelectOneRequestPriority(String selectOneRequestPriority) {
		this.selectOneRequestPriority = selectOneRequestPriority;
	}



	public List<SelectItem> getRequestPriority() {
		if(viewRootMap.containsKey(REQUEST_PRIORITY_COMBO))
			requestPriority = (ArrayList<SelectItem>)viewRootMap.get(REQUEST_PRIORITY_COMBO);
		return requestPriority;
	}



	public void setRequestPriority(List<SelectItem> requestPriority) {
		this.requestPriority = requestPriority;
	}



	public HtmlGraphicImage getImgSearchEngineer() {
		return imgSearchEngineer;
	}



	public void setImgSearchEngineer(HtmlGraphicImage imgSearchEngineer) {
		this.imgSearchEngineer = imgSearchEngineer;
	}



	public HtmlInputTextarea getTxtboxRemarks() {
		return txtboxRemarks;
	}



	public void setTxtboxRemarks(HtmlInputTextarea txtboxRemarks) {
		this.txtboxRemarks = txtboxRemarks;
	}



	public HtmlOutputLabel getLblRemarks() {
		return lblRemarks;
	}



	public void setLblRemarks(HtmlOutputLabel lblRemarks) {
		this.lblRemarks = lblRemarks;
	}


	private boolean hasCompletionErrors()
	{
		boolean hasErrors=false;
//		if(this.getActualCost()!=null && this.getActualCost().trim().length()==0)
//		{
//			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.ACTUAL_COST_REQ));
//			hasSaveFollowUpErrors = true;		
//		}
//		else
		/******Recently Commented*****/
//		{
//			try {
//		          new Double(this.getActualCost());		
//			} catch (Exception e) {
//				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.INVALID_ACTUAL_COST ));
//				hasSaveFollowUpErrors = true;
//			}
//			
//		}
//		if(this.getActualCost()!=null && !StringHelper.isEmpty(this.getActualCost()))
//		{
//			Double cost;
//			cost=Double.parseDouble(this.getActualCost());
//			if(cost.compareTo(0D)<=0)
//			{
//				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.INVALID_ACTUAL_COST ));
//				hasErrors = true;
//			}
//				
//		}
/*		if(this.getActualCost()!=null && this.getActualCost().trim().length()!=0)
		{
			try {
		        new Double(this.getActualCost());		
			} catch (Exception e) {
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.MaintenanceRequest.INVALID_ACTUAL_COST ));
				hasErrors = true;
				return hasErrors;
			}
		}
		List<FollowUpView> fvl=this.getFollowUpList();
		for(FollowUpView fuv:fvl)
		{
			if(fuv.getActualCost()==null)
			{	errorMessages.add(CommonUtil.getBundleMessage("maintenanceRequest.followUp.acutalCostRequired"));
				hasErrors = true;
				return hasErrors;
			}
		}*/
		if(  
				cmbPaymentSource.getValue() == null || 
				cmbPaymentSource.getValue().toString().equals( "-1" ) 
		    )
		{
			errorMessages.add(CommonUtil.getBundleMessage("mems.payment.err.msg.paymentSrcReq"));
			hasErrors = true;
		}
		else if(
				cmbPaymentSource.getValue() != null && 
				cmbPaymentSource.getValue().toString().equals( String.valueOf( WebConstants.PaymentSrcType.AID_ID ) ) && 

				( cmbDisbursementSource.getValue()==null || cmbDisbursementSource.getValue().equals("-1") )
		    )
		{
			errorMessages.add(CommonUtil.getBundleMessage("mems.normaldisb.err.msg.disbSrcReq"));
			hasErrors = true;
		}
		return hasErrors;
	}

	public void addActualCostToFollowUp()
	{
		FollowUpView followUpView=(FollowUpView) followUpDataTable.getRowData();
		viewRootMap.put("SELECTED_FOLLOWUP", followUpView);
		viewRootMap.put("SELECTED_INDEX", followUpDataTable.getRowIndex());
		if(followUpView.getRemarks()!=null)
			this.setTxtRemarks(followUpView.getRemarks());
		if(followUpView.getActualCost()!=null)
			this.setActualCost(followUpView.getActualCost().toString());
		
	}



	public HtmlGraphicImage getImgClearContract() {
		return imgClearContract;
	}



	public void setImgClearContract(HtmlGraphicImage imgClearContract) {
		this.imgClearContract = imgClearContract;
	}



	public void receiveAsset()
	{
		try
		{
		if(sessionMap.get(WebConstants.AssetSearchOutcomes.ASSETS_SEARCH_SELECTED_ONE_ASSET) == null)
		{return;}
			AssetsGridView assetGrid = (AssetsGridView) sessionMap.remove(WebConstants.AssetSearchOutcomes.ASSETS_SEARCH_SELECTED_ONE_ASSET);
			List<DistributePopUpView> deductionList = null;
			deductionList = new CollectionProcedureService().getBeneficiariesByAssetId(assetGrid.getAssetId(), null);
			//if asset doesnt have beneficiary. Most probably in case of Underguardian and legal assistance files and founding
			if(deductionList  == null || deductionList .size() <= 0)
			{
					InheritanceFileView fileView = getInheritanceFileView();
					if( 
						!WebConstants.InheritanceFileType.FILES_TYPE_SAME_OWNER_BENEFICIARY.contains(  fileView.getFileTypeKey() )
					   )
					{
						errorMessages = new ArrayList<String>();
						errorMessages.add(CommonUtil.getBundleMessage("minorMaintenance.errMsg.noBeneficiary"));
						return;
					}
						List<InheritanceBeneficiary> ib =  InheritanceFileService.getInheritanceBeneficiaryByFileId( fileView.getInheritanceFileId() );
						DistributePopUpView view = new DistributePopUpView();
						for (InheritanceBeneficiary inheritanceBeneficiary : ib) 
						{
						   if( inheritanceBeneficiary.getBeneficiary().getPersonId().compareTo( new Long ( fileView.getFilePersonId() ) ) != 0 )
						   {continue;}
							view.setInheritanceFileId(fileView.getInheritanceFileId());
							view.setInheritanceFileNumber(fileView.getFileNumber());
							view.setInheritanceFileStatusEn( fileView.getStatusEn() );
							view.setInheritanceFileStatusAr( fileView.getStatusAr() );
							view.setInheritanceFileTypeEn( fileView.getFileTypeEn() );
							view.setInheritanceFileTypeAr( fileView.getFileTypeAr() );
							view.setInheritanceFileTypeDataValue( fileView.getFileTypeKey()  );
							view.setBeneficiaryId( inheritanceBeneficiary.getBeneficiary().getPersonId());
							view.setInheritedBeneficiaryId( inheritanceBeneficiary.getInheritanceBeneficiaryId() );
							if(inheritanceBeneficiary.getCostCenter()  != null && inheritanceBeneficiary.getCostCenter().trim().length()>0 )
						    {
								view.setCostCenter( inheritanceBeneficiary.getCostCenter() );
						    }
						    else if(inheritanceBeneficiary.getOldCostCenter()  != null && inheritanceBeneficiary.getOldCostCenter().trim().length()>0 )
						    {
						    	view.setCostCenter( inheritanceBeneficiary.getOldCostCenter() );
						    }
							view.setBeneficiaryName( inheritanceBeneficiary.getBeneficiary().getFullName() );
							view.setIsMinor( inheritanceBeneficiary.getBeneficiary().getIsMinor() );
							if (inheritanceBeneficiary.getSharePercentage() != null)
							{
								view.setShariaPercent(inheritanceBeneficiary.getSharePercentage());
							}
							if( fileView.getTotalInheritanceShare() != null)
							{
								view.setTotalFileShares( fileView.getTotalInheritanceShare() );
							}
							deductionList = new ArrayList<DistributePopUpView>();
							deductionList.add(view);
							
					  }
			}
			if(  deductionList != null && deductionList.size() > 0 )
			{
				setDeductFromList(deductionList);
				setAssetView(assetGrid);
			}
		}
		catch(Exception e)
		{
			logger.LogException("receiveAsset|Error Occured:", e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}



	public AssetsGridView getAssetView() 
	{
		if(viewMap.get(ASSET_VIEW) != null )
			assetView = (AssetsGridView) viewMap.get(ASSET_VIEW);
		return assetView;
	}



	@SuppressWarnings("unchecked")
	public void setAssetView(AssetsGridView assetView) 
	{
		if(assetView != null && assetView.getAssetId() != null)
			viewMap.put(ASSET_VIEW, assetView);
		this.assetView = assetView;
	}



	public String getHdnAssetId() {
		return hdnAssetId;
	}



	public void setHdnAssetId(String hdnAssetId) {
		this.hdnAssetId = hdnAssetId;
	}



	public HtmlCommandButton getBtnSendForEval() {
		return btnSendForEval;
	}



	public void setBtnSendForEval(HtmlCommandButton btnSendForEval) {
		this.btnSendForEval = btnSendForEval;
	}
	@SuppressWarnings("unchecked")
	public void btnSendForEvaluation_Click()
	{
		successMessages = new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			if( !hasTask() )
			{
				errorMessages.add( ResourceUtil.getInstance().getProperty("commons.msg.fectchtasktoperformaction") );
				return;
			}
			new RequestService().setRequestAsEvaluationRequired(new Long(this.requestId), getLoggedInUserId());
			setTaskOutCome(TaskOutcome.OK);
			viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			saveCommentAndAttachements(MessageConstants.RequestEvents.REQUEST_SENT_FOR_EVALUATION);
	    	successMessages.add(CommonUtil.getBundleMessage("successMsg.requestSentForEval"));
	    	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException( "btnSendForEvaluation_Click|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
	}
	public boolean isPageModeEvalRequired()
	{
		boolean isPageModeEvalRequired = false;
    	if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.EVALUATION_REQ))
    		isPageModeEvalRequired = true ;
    	return isPageModeEvalRequired;
	}
	@SuppressWarnings("unchecked")
	public void btnSendForFollowup_Click()
	{
		successMessages = new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			if(viewMap.get(REQUEST_VIEW) != null)
			{
				 
				RequestView requestView = (RequestView) viewMap.get(REQUEST_VIEW);
				new RequestServiceAgent().setRequestAsOnFollowUp(requestView.getRequestId(), getLoggedInUserId());
//				if(!viewRootMap.containsKey(WebConstants.TASK_LIST_SELECTED_USER_TASK))
//					  getIncompleteRequestTasks();
	        	setTaskOutCome(TaskOutcome.FORWARD);
				viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
//				btnSendForFollowup.setRendered(false);
//				btnComplete.setRendered(false);
//				tbl_FollowupRemarks.setRendered(true);
//				txtboxRemarks.setRendered(true);
				saveCommentAndAttachements(MessageConstants.RequestEvents.REQUEST_SENT_FOR_FOLLOWUP);
		    	successMessages.add(CommonUtil.getBundleMessage("maintenanceRequest.msg.sentForFollowup"));
		    	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
			}
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("btnSendForFollowup_Click|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
	}
	public void templateMethodSignature()
	{
		String methodName ="templateMethodSignature";
		logger.logInfo(methodName+"|"+"Start..");
		successMessages = new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
	}



	@SuppressWarnings("unchecked")
	public List<DistributePopUpView> getDeductFromList() 
	{
		if(viewMap.get(DEDUCT_FROM_LIST) != null)
		{
			deductFromList = (List<DistributePopUpView>) viewMap.get(DEDUCT_FROM_LIST);
		}
		return deductFromList;
	}



	@SuppressWarnings("unchecked")
	public void setDeductFromList(List<DistributePopUpView> deductFromList) 
	{
		if(deductFromList != null && deductFromList.size() > 0)
		{
			viewMap.put(DEDUCT_FROM_LIST, deductFromList);
		}
		this.deductFromList = deductFromList;
	}



	public HtmlDataTable getDeductFromDataTable() {
		return deductFromDataTable;
	}



	public void setDeductFromDataTable(HtmlDataTable deductFromDataTable) {
		this.deductFromDataTable = deductFromDataTable;
	}



	@SuppressWarnings("unchecked")
	public List<SelectItem> getDeductionCriteriaList() 
	{
		if(viewMap.get(DEDUCTION_CRIT_LIST) != null)
			deductionCriteriaList = (List<SelectItem>) viewMap.get(DEDUCTION_CRIT_LIST);
		return deductionCriteriaList;
	}



	public void setDeductionCriteriaList(List<SelectItem> deductionCriteriaList) {
		this.deductionCriteriaList = deductionCriteriaList;
	}
	double roundDecimals(double d) {
    	DecimalFormat oneDForm = new DecimalFormat("#.00");
	return Double.valueOf(oneDForm.format(d));
}
	public void btnApply_Click()
	{
		if(getDeductFromList() != null && getDeductFromList().size() > 0)
		{
			deductFromList = getDeductFromList();
			if(cmbDeductCrit.getValue() != null && cmbDeductCrit.getValue().toString().compareTo("-1") != 0)
			{
				double sumCaluclatedAmount = 0.00d;
				if(
					( this.getActualCost() != null && this.getActualCost().length() > 0 && isNumber( this.getActualCost() ) ) || 
					( this.getEstimatedCost() != null && this.getEstimatedCost().length() > 0 && isNumber( this.getEstimatedCost() ) ) 	
				   )
				{
					Long critId = new Long(cmbDeductCrit.getValue().toString());
					double cost = 0d;
					if(( this.getActualCost() != null && this.getActualCost().length() > 0 && isNumber( this.getActualCost() ) ) )
					{
						cost = new Double(this.getActualCost());
					}
					else
					{
						cost = new Double(this.getEstimatedCost() );
					}
					double amountForEach = 0;
					DistributePopUpView lasObj = null;
					if(critId.compareTo(getEquallyDD().getDomainDataId()) == 0) //if equally
					{
						byte selectCount = 0;
						
						for(DistributePopUpView deductionFrom  : deductFromList)
						{
							if(deductionFrom.getSelected()!= null && deductionFrom.getSelected())
							{
								selectCount++; 
							}
						}
						if(selectCount == 0)
						{
							errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.beneficiaryReq"));
							return ;
						}
						amountForEach = cost/selectCount;
						for(DistributePopUpView deductionFrom  : deductFromList)
						{
							if(deductionFrom.getSelected() != null && deductionFrom.getSelected())
							{
								amountForEach =roundDecimals(amountForEach );
								deductionFrom.setAmountToBeDeducted( String.valueOf(  amountForEach ) );
								sumCaluclatedAmount +=amountForEach ;
								lasObj = deductionFrom;
							}
							else
								deductionFrom.setAmountToBeDeducted(null);
							
							
						}
					}
					
					else
					if(critId.compareTo(getBasedOnShareDD().getDomainDataId()) == 0) //if based on share
					{
						byte selectCount = 0;
						for(DistributePopUpView deductionFrom  : deductFromList)
						{
							if(deductionFrom.getSelected() != null && deductionFrom.getSelected())
							{
								double sharedAmount = 0;
								selectCount++;
								sharedAmount = roundDecimals((deductionFrom.getAssetPercent()/deductionFrom.getTotalAssetShares())*cost);
								deductionFrom.setAmountToBeDeducted( String.valueOf(  sharedAmount  ) );
								sumCaluclatedAmount +=sharedAmount ;
								lasObj = deductionFrom;
							}
							else
								deductionFrom.setAmountToBeDeducted(null);
						}
						if(selectCount == 0)
						{
							errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.beneficiaryReq"));
							return ;
						}
					}
					
					else
					if(critId.compareTo(getBasedOnShariaDD().getDomainDataId()) == 0) //if based on sharia
					{
						byte selectCount = 0;
						for(DistributePopUpView deductionFrom  : deductFromList)
						{
							if(deductionFrom.getSelected() != null && deductionFrom.getSelected())
							{
								selectCount++;
								double shariaAmount = 0;
								shariaAmount = roundDecimals( (deductionFrom.getShariaPercent()/deductionFrom.getTotalFileShares())*cost);
								deductionFrom.setAmountToBeDeducted(String.valueOf( shariaAmount ) );
								sumCaluclatedAmount +=shariaAmount ;
								lasObj = deductionFrom;
							}
							else
								deductionFrom.setAmountToBeDeducted(null);
						}
						if(selectCount == 0)
						{
							errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.beneficiaryReq"));
							return ;
						}
					}
					
					if( sumCaluclatedAmount> cost)
					{
						Double newAmount = roundDecimals( Double.valueOf( lasObj.getAmountToBeDeducted()) - ( sumCaluclatedAmount-cost) );  
						lasObj.setAmountToBeDeducted(  newAmount.toString() );
							
					}
					else if( sumCaluclatedAmount< cost)
					{
						Double newAmount = roundDecimals( Double.valueOf( lasObj.getAmountToBeDeducted() ) + ( cost-sumCaluclatedAmount));  
						lasObj.setAmountToBeDeducted(   newAmount.toString() );
							
					}
				}
				else
				{
					requestView = ((RequestView)viewMap.get(REQUEST_VIEW));
					if( requestView != null && 
						requestView.getStatusId().compareTo(WebConstants.REQUEST_STATUS_NEW_ID) != 0 )
					{
						errorMessages.add(CommonUtil.getBundleMessage("errMsg.actualCoseReq"));
					}
				}
					
			}
			else
			{
				errorMessages.add(CommonUtil.getBundleMessage("errMsg.deductionCritReq"));
			}
		}
		else
		{
			errorMessages.add(CommonUtil.getBundleMessage("minorMaintenance.errMsg.noDeductionCrriteria"));
		}
		
		
	}



	public HtmlSelectOneMenu getCmbDeductCrit() {
		return cmbDeductCrit;
	}



	public void setCmbDeductCrit(HtmlSelectOneMenu cmbDeductCrit) {
		this.cmbDeductCrit = cmbDeductCrit;
	}



	public DomainDataView getEquallyDD() 
	{
		if(viewMap.get(EQUALLY_DD) != null)
			equallyDD = (DomainDataView) viewMap.get(EQUALLY_DD);
		return equallyDD;
	}



	public void setEquallyDD(DomainDataView equallyDD) {
		this.equallyDD = equallyDD;
	}



	public DomainDataView getBasedOnShariaDD() 
	{
		if(viewMap.get(BASED_ON_SHARIA_DD) != null)
			basedOnShariaDD = (DomainDataView) viewMap.get(BASED_ON_SHARIA_DD);
		return basedOnShariaDD;
	}



	public void setBasedOnShariaDD(DomainDataView basedOnShariaDD) {
		this.basedOnShariaDD = basedOnShariaDD;
	}



	public DomainDataView getBasedOnShareDD() 
	{
		if(viewMap.get(BASED_ON_SHARE_DD) != null)
			basedOnShareDD = (DomainDataView) viewMap.get(BASED_ON_SHARE_DD);
		return basedOnShareDD;
	}



	public void setBasedOnShareDD(DomainDataView basedOnShareDD) {
		this.basedOnShareDD = basedOnShareDD;
	}
	
	public boolean validateReview() throws Exception
	{
		if( null==txtReviewComments.getValue() || 0==txtReviewComments.getValue().toString().trim().length()) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_REVIEW_COMMT));
			
			return false;
		}
		if( null==cmbUserGroups.getValue() || cmbUserGroups.getValue().toString().equals( "-1")) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_REVIEW_GRP));
			return false;
		}
		return true;
	}

	public void onSentForReview() 
	{
		try 
		{
			if( validateReview() ) 
			{
			     	requestView = ((RequestView)viewMap.get(REQUEST_VIEW));
					putRequestDetailInRequest();
					putMaintenaceDeductionInView();
				    requestView.setMemsNolReviewGrpId( cmbUserGroups.getValue().toString());
				    RequestService reqWS = new RequestService();
					
					requestView.setDistributionCriteria(new Long(cmbDeductCrit.getValue().toString()));
					requestView.setRequestPriorityId( new Long(cmbDisbursementSource.getValue().toString()));
					requestView.setStatusId(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID );
					requestView.setUpdatedBy( getLoggedInUserId() );
					
					requestView = new RequestService().persistMinorMaintenanceRequestInTransaction(requestView,getMaintenanceDeductionList());
					reqWS.saveOrUpdate( getReviewRequestVO(requestView));
					setTaskOutCome( TaskOutcome.FORWARD);
					CommonUtil.saveSystemComments( WebConstants.REQUEST,
												   MessageConstants.MemsNOL.MSG_REQ_SENT_FOR_REVIEW,
												   requestView.getRequestId());
					viewMap.remove(WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW);
					viewMap.remove(WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW_LIST);
					successMessages.add( CommonUtil.getBundleMessage( MessageConstants.
																	  MemsCommonRequestMsgs.SUCC_REQ_REVIEW));
				   	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
			}
			
		} catch(Exception ex) {
			logger.LogException("[Exception occured in review()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	/**
	 * @throws Exception
	 * @throws RemoteException
	 */
	private void invokeBlockingBPEL(Request request) throws Exception, RemoteException 
	{
		 SystemParameters parameters = SystemParameters.getInstance();
		 String endPoint= parameters.getParameter( "MEMSBlockingBPEL" );
		 MEMSBlockingBPELPortClient port=new MEMSBlockingBPELPortClient();
		 port.setEndpoint(endPoint);
		 port.initiate(
				 		Integer.parseInt( request.getRequestId().toString()),
				 		CommonUtil.getLoggedInUser(),
				 		"1", 
				 		null, 
				 		null
				 	   );
	}
	
	public boolean hasSendForBlockingErrors () throws Exception
	{
		
		Request request= BlockingAmountService.getActiveBlockingRequestForMaintenace(requestView );
		if( request != null && request.getRequestId() != null )
		{
			String msg  = java.text.MessageFormat.format(
														 CommonUtil.getBundleMessage("minormaintenance.errMsg.blockingRequestAlreadyCreatedAgainsMaintenanceRequest"), 
														 request.getRequestNumber()
														);
			errorMessages.add( msg );
			return true;
			
		}
		if ( 
				(this.getActualCost() == null || this.getActualCost().length() <= 0 ) && 
				(this.getEstimatedCost() == null || this.getEstimatedCost().length() <= 0 )
		   ) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.actualorEstimatedCostReqBlocking"));
			return true;
		}
		
		if( 
				cmbPaymentSource.getValue() != null && 
				cmbPaymentSource.getValue().toString().equals( String.valueOf( WebConstants.PaymentSrcType.AID_ID) )
		   )
		{
			errorMessages.add(CommonUtil.getBundleMessage("minormaintenance.errMsg.blockingNotAllowedForAid"));
			tabPanel.setSelectedTab("deductionListId");
			return true;
		}
		if(  hasApprovalErrors() )
			return true;
		
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public void onSendForBlocking() 
	{
		try 
		{
	    	requestView =  (RequestView)viewMap.get(REQUEST_VIEW);
			if( hasSendForBlockingErrors() ) 
			{return;}
							
			

			BlockingAmountService service = new BlockingAmountService();
	 
		     try
		     {
		    	putRequestDetailInRequest();
				putMaintenaceDeductionInView();
				deductFromList = getDeductFromList();
		    	ApplicationContext.getContext().getTxnContext().beginTransaction();

		    	requestView.setDistributionCriteria(new Long(cmbDeductCrit.getValue().toString()));
				requestView.setRequestPriorityId( new Long(cmbDisbursementSource.getValue().toString()));
				requestView.setUpdatedBy( getLoggedInUserId() );
				
		     	Request blockingRequest = service.persistBlockingFromMaintenanceRequest(requestView , deductFromList, getLoggedInUserId(),getMaintenanceDeductionList() );
		     
		     	invokeBlockingBPEL( blockingRequest);
		     	String message = java.text.MessageFormat.format( 
		     													 CommonUtil.getBundleMessage( "mems.maintenance.message.activity.sentForBlocking" ),
		     													 blockingRequest.getRequestNumber()
		     													);
		     	
				CommonUtil.saveSystemComments( 
											   WebConstants.REQUEST,
											   "mems.maintenance.message.activity.sentForBlocking",
											   requestView.getRequestId(),
											   blockingRequest.getRequestNumber()
											  );
				ApplicationContext.getContext().getTxnContext().commit();
				successMessages.add( message );
				
		     }
		     catch(Exception e)
		     {
		    	 ApplicationContext.getContext().getTxnContext().rollback();
		    	 throw e;
		     }
		     finally
		     {
		    	 ApplicationContext.getContext().getTxnContext().release();
		     }
		} 
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in onSendForBlocking()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	private ReviewRequestView getReviewRequestVO(RequestView requestView) throws Exception 
	{
		ReviewRequestView reviewReqVO = new ReviewRequestView();
		reviewReqVO.setRequestId(requestView.getRequestId());
		reviewReqVO.setCreatedBy(CommonUtil.getLoggedInUser());
		reviewReqVO.setCreatedOn(DateUtil.getCurrentDate());
		reviewReqVO.setRfc(txtReviewComments.getValue().toString());
		reviewReqVO.setGroupId(cmbUserGroups.getValue().toString());
		return reviewReqVO;
	}
	@SuppressWarnings("unchecked")
	public void onReviewDone() 
	{
		try 
		{

			requestView = ((RequestView)viewMap.get(REQUEST_VIEW));
			ReviewRequestView reviewRequestView = (ReviewRequestView) viewMap.get(
																	WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW);
			new RequestService().setRequestStatus(requestView.getRequestId(), WebConstants.REQUEST_STATUS_REVIEW_DONE, getLoggedInUserId());	
			new UtilityService().persistReviewRequest(reviewRequestView);
			 setTaskOutCome(TaskOutcome.OK);
			CommonUtil.saveSystemComments( WebConstants.REQUEST, 
										   MessageConstants.MemsNOL.MSG_REQ_REVIWED, 
										   requestView.getRequestId());
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.
					  										  MemsCommonRequestMsgs.SUCC_REQ_REVIEW_DONE));
		   	viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
		} catch (Exception ex) {
			logger.LogException("[Exception occured in done()]", ex);
			errorMessages.add(CommonUtil
								.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));												
		}
	}

	@SuppressWarnings("unchecked")
	public void btnDoneEvaluation_Click()
	{
		successMessages = new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			requestView = ((RequestView)viewMap.get(REQUEST_VIEW));
			if(requestView == null && requestView.getRequestId() == null && hasEvaluationError()){return;}
			 
			 putRequestDetailInRequest();
	   		 Long id = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_EVALUATED);
	   		 requestView.setStatusId(id);
			 new RequestService().evaluateMinorMaintenanceRequest(requestView);
			 if(hasTask())
			 {
				 setTaskOutCome( TaskOutcome.OK );
				 viewRootMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
			 }
	   		 requestView = (RequestView)viewRootMap.get(REQUEST_VIEW);
		   	 sessionMap.put("REQUEST_DATE", requestView.getRequestDate());
		   	 saveCommentAndAttachements("commons.msg.evaluationDone") ;
	   		 viewRootMap.put(Page_Mode.PAGE_MODE, Page_Mode.VIEW);
   	         successMessages.add(ResourceUtil.getInstance().getProperty("commons.msg.evaluationDone"));
   	         imgSearchContractor.setRendered(true);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(  "btnDoneEvaluation_Click|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
	}



	private boolean hasEvaluationError() 
	{
		boolean hasError = false;
		if (this.getContractor() == null	|| this.getContractor().getPersonId() == null) 
	    {
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.selectVender"));
			return true;
		}
		if(this.getEstimatedCost() == null || this.getEstimatedCost().trim().length() <= 0 || !isNumber(this.getEstimatedCost().trim()))
		{
			hasError = true;
			errorMessages.add(CommonUtil.getBundleMessage("maintenanceRequest.messages.estimatedCostRequired"));
			return hasError;
		}
		if(this.getTxtRemarks() == null || this.getTxtRemarks().trim().length() <= 0)
		{
			hasError = true;
			errorMessages.add(CommonUtil.getBundleMessage("commons.RequiredRemarks"));
			return hasError;
		}
		return hasError;
	}



	@SuppressWarnings("unchecked")
	public List<MaintenanceDeductionView> getMaintenanceDeductionList() 
	{
		if(viewMap.get(MAINTENANCE_DEDUCTION_VIEW_LIST) != null )
			maintenanceDeductionList = (List<MaintenanceDeductionView>) viewMap.get(MAINTENANCE_DEDUCTION_VIEW_LIST);			
		return maintenanceDeductionList;
	}



	@SuppressWarnings("unchecked")
	public void setMaintenanceDeductionList(List<MaintenanceDeductionView> maintenanceDeductionList) 
	{
		this.maintenanceDeductionList = maintenanceDeductionList;
		if(maintenanceDeductionList != null && maintenanceDeductionList.size() > 0)
		{
			viewMap.put(MAINTENANCE_DEDUCTION_VIEW_LIST, maintenanceDeductionList);
		}
	}
	@SuppressWarnings( "unchecked" )
	public void putMaintenaceDeductionInView()
	{
		
		deductFromList = getDeductFromList();
	    List<MaintenanceDeductionView> deductionList = new ArrayList<MaintenanceDeductionView>();
	    if(deductFromList == null || deductFromList.size() <= 0) {return;}
		for(DistributePopUpView  deductionFrom : deductFromList)
		{
			MaintenanceDeductionView deductionView = new MaintenanceDeductionView();
			if( deductionFrom.getSelected() == null || !deductionFrom.getSelected()){continue;}
				
			
			deductionView.setMaintenanceDeductionId(null);
			deductionView.setCreatedBy(getLoggedInUserId());
			deductionView.setCreatedOn(new Date());
			deductionView.setAmount(new Double(deductionFrom.getAmountToBeDeducted()));
			InheritanceBeneficiaryView beneficiary = new InheritanceBeneficiaryView();
			beneficiary.setInheritanceBeneficiaryId(deductionFrom.getInheritedBeneficiaryId());
			deductionView.setInhBeneficiaryView(beneficiary);
			if(viewMap.get(REQUEST_VIEW) != null)
			{
			  deductionView.setRequestView((RequestView) viewMap.get(REQUEST_VIEW));
			}
			deductionList.add(deductionView);
		}
		if(deductionList.size() > 0)
		{
			setMaintenanceDeductionList(deductionList);
		}
		
	}

	public HtmlCommandButton getBtnDone() {
		return btnDone;
	}



	public void setBtnDone(HtmlCommandButton btnDone) {
		this.btnDone = btnDone;
	}



	public HtmlCommandButton getBtnSendForFollowup() {
		return btnSendForFollowup;
	}



	public void setBtnSendForFollowup(HtmlCommandButton btnSendForFollowup) {
		this.btnSendForFollowup = btnSendForFollowup;
	}



	public HtmlCommandButton getBtnApplyDeduction() {
		return btnApplyDeduction;
	}



	public void setBtnApplyDeduction(HtmlCommandButton btnApplyDeduction) {
		this.btnApplyDeduction = btnApplyDeduction;
	}



	public HtmlInputText getTxtVendor() {
		return txtVendor;
	}



	public void setTxtVendor(HtmlInputText txtVendor) {
		this.txtVendor = txtVendor;
	}



	public HtmlInputText getTxtEvaRemarks() {
		return txtEvaRemarks;
	}



	public void setTxtEvaRemarks(HtmlInputText txtEvaRemarks) {
		this.txtEvaRemarks = txtEvaRemarks;
	}



	public HtmlInputText getTxtActualCost() {
		return txtActualCost;
	}



	public void setTxtActualCost(HtmlInputText txtActualCost) {
		this.txtActualCost = txtActualCost;
	}



	public HtmlInputText getTxtEstimatedCost() {
		return txtEstimatedCost;
	}



	public void setTxtEstimatedCost(HtmlInputText txtEstimatedCost) {
		this.txtEstimatedCost = txtEstimatedCost;
	}



	public HtmlOutputLabel getLblVendor() {
		return lblVendor;
	}



	public void setLblVendor(HtmlOutputLabel lblVendor) {
		this.lblVendor = lblVendor;
	}



	public HtmlOutputLabel getLblEvaRemarks() {
		return lblEvaRemarks;
	}



	public void setLblEvaRemarks(HtmlOutputLabel lblEvaRemarks) {
		this.lblEvaRemarks = lblEvaRemarks;
	}



	public HtmlOutputLabel getLblActualCost() {
		return lblActualCost;
	}



	public void setLblActualCost(HtmlOutputLabel lblActualCost) {
		this.lblActualCost = lblActualCost;
	}



	public HtmlOutputLabel getLblEstimatedCost() {
		return lblEstimatedCost;
	}



	public void setLblEstimatedCost(HtmlOutputLabel lblEstimatedCost) {
		this.lblEstimatedCost = lblEstimatedCost;
	}



	public org.richfaces.component.html.HtmlTab getTabDeductionFrom() {
		return tabDeductionFrom;
	}



	public void setTabDeductionFrom(
			org.richfaces.component.html.HtmlTab tabDeductionFrom) {
		this.tabDeductionFrom = tabDeductionFrom;
	}



	public HtmlGraphicImage getImgSearchAsset() {
		return imgSearchAsset;
	}



	public void setImgSearchAsset(HtmlGraphicImage imgSearchAsset) {
		this.imgSearchAsset = imgSearchAsset;
	}
	public boolean isPageModeCompleted()
	{
		if(viewRootMap.get(Page_Mode.PAGE_MODE).toString().equals(Page_Mode.COMPLETE))
			return true;
		else
			return false;
	}



	public HtmlPanelGrid getTbl_FollowupRemarks() {
		return tbl_FollowupRemarks;
	}



	public void setTbl_FollowupRemarks(HtmlPanelGrid tbl_FollowupRemarks) {
		this.tbl_FollowupRemarks = tbl_FollowupRemarks;
	}
	public  boolean  isNumber(String s) 
	{
		try {
		Double.parseDouble(s);
		}
		catch (NumberFormatException nfe) {
		return false;
		}
		return true;
		}



	public HtmlSelectBooleanCheckbox getCboDeduction() {
		return cboDeduction;
	}



	public void setCboDeduction(HtmlSelectBooleanCheckbox cboDeduction) {
		this.cboDeduction = cboDeduction;
	}



	@SuppressWarnings("unchecked")
	public HashMap<Long, Double> getBeneAmountMap() 
	{
		if(viewMap.get(BENEFICIARY_AMOUNT_MAP) != null)
			beneAmountMap = (HashMap<Long, Double>) viewMap.get(BENEFICIARY_AMOUNT_MAP);
		return beneAmountMap;
	}



	@SuppressWarnings("unchecked")
	public void setBeneAmountMap(HashMap<Long, Double> beneAmountMap) 
	{
		if(beneAmountMap != null && beneAmountMap.size() > 0)
		{
			viewMap.put(BENEFICIARY_AMOUNT_MAP, beneAmountMap);
		}
		this.beneAmountMap = beneAmountMap;
	}



	public HtmlCommandButton getBtnDoneFollowup() {
		return btnDoneFollowup;
	}



	public void setBtnDoneFollowup(HtmlCommandButton btnDoneFollowup) {
		this.btnDoneFollowup = btnDoneFollowup;
	}
	private boolean hasDoneFollowupErrors()
	{
	  if (this.getContractor() == null	|| this.getContractor().getPersonId() == null) 
	    {
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.selectVender"));
			return true;
		}
		if (this.getActualCost() == null || this.getActualCost().length() <= 0) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.actualCoseReq"));
			return true;
		}
			
	  return false;
	}
	
	@SuppressWarnings("unchecked")
	public void btnDoneFollowup_Click()
	{
		successMessages = new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		try	
    	{
			
		 if(this.requestId ==null || this.requestId.trim().length() <= 0 || hasDoneFollowupErrors())
	   	 {return;}
			 RequestService service = new RequestService();
			 List<FollowUpView> fvl=this.getFollowUpList();
//			 FollowUpView FUP = new FollowUpView();
//			 FUP.setFollowUpId(-1l); 
//			 for (FollowUpView followUpView : fvl) {
//				if(
//					//FUP.getIsDeleted().compareTo(WebConstants.DEFAULT_IS_DELETED ) ==0 &&
//					FUP.getFollowUpId().compareTo( followUpView.getFollowUpId() ) < 0
//				  )
//				{
//					FUP = followUpView;
//				}
//			} 
//			 
//			 txtActualCost.setValue(  FUP.getActualCost().toString() ); 
			 
//			 String value = txtActualCost.getValue().toString();
			 List<MaintenanceDeductionView> list = null;
			 btnApply_Click();
			
			 putMaintenaceDeductionInView();
			 list  = getMaintenanceDeductionList();
			 putRequestDetailInRequest();
		//	 putControlValuesinViews();
			 //requestView.setDistributionCriteria(new Long(cmbDeductCrit.getValue().toString()));
			 service.minorMaintenanceFollowupDone(requestView,list);
			 setTaskOutCome(TaskOutcome.OK);
			 viewRootMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	   		 saveCommentAndAttachements(MessageConstants.RequestEvents.REQUEST_FOLLOW_UP_DONE);
	   		 successMessages.add(CommonUtil.getBundleMessage("extendApplication.details.messages.requestFollowUpSuccess"));
	   		 viewRootMap.put(Page_Mode.PAGE_MODE,Page_Mode.VIEW);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException( "btnDoneFollowup_Click|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
	}



	public HtmlInputTextarea getTxtMaintenaceDetails() {
		return txtMaintenaceDetails;
	}



	public void setTxtMaintenaceDetails(HtmlInputTextarea txtMaintenaceDetails) {
		this.txtMaintenaceDetails = txtMaintenaceDetails;
	}



	public HtmlGraphicImage getImgManageAsset() {
		return imgManageAsset;
	}



	public void setImgManageAsset(HtmlGraphicImage imgManageAsset) {
		this.imgManageAsset = imgManageAsset;
	}
	public void openManageAsset()
	{
		try	
		{	
			if (assetView.getAssetId()!=null)
			{
				String javaScriptText = " var screen_width = 970;"
						+ "var screen_height = 550;"
						+ "var popup_width = screen_width;"
						+ "var popup_height = screen_height;"
						+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
						+ "window.open('ManageAssets.jsf?pageMode=MODE_SELECT_ONE_POPUP&"
						+ WebConstants.ASSET_ID+"="+assetView.getAssetId()
						+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";
		
				executeJavascript(javaScriptText);
			}
		}
		catch (Exception exception) 
		{
			logger.LogException("openManageAsset--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private void executeJavascript(String javascript) throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
	}
	//Called by button of file details
	@SuppressWarnings("unchecked")
	  public void onOpenFile() 
	  {
		try	
		{	
			
			
			sessionMap.put( WebConstants.InheritanceFile.FILE_ID,getInheritanceFileView().getInheritanceFileId() );
			sessionMap.put (WebConstants.InheritanceFilePageMode.IS_POPUP,true);
			executeJavascript(   "javaScript:openFile();" );
		    
		}
		catch (Exception exception) 
		{
			logger.LogException("onOpenFile --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	  }
	//Called by link of distribution details grid
	  @SuppressWarnings("unchecked")
	  public void openFilePopUp() 
	  {
		try	
		{	
			
			DistributePopUpView obj = ( DistributePopUpView) deductFromDataTable.getRowData();
			sessionMap.put( WebConstants.InheritanceFile.FILE_ID,obj.getInheritanceFileId() );
			sessionMap.put (WebConstants.InheritanceFilePageMode.IS_POPUP,true);
			executeJavascript(   "javaScript:openFile();" );
		    
		}
		catch (Exception exception) 
		{
			logger.LogException("openFilePopUp --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	  }
	  
	@SuppressWarnings("unchecked")
	public InheritanceFileView getInheritanceFileView() 
	{
		if ( viewMap.get( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW ) != null )
		{
			inheritanceFileView  = (InheritanceFileView) viewMap.get( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW ) ;
			
		}
		return inheritanceFileView;
	}

	@SuppressWarnings("unchecked")
	public void setInheritanceFileView(InheritanceFileView inheritanceFileView) 
	{
		this.inheritanceFileView = inheritanceFileView;
		if(this.inheritanceFileView != null)
		{
			viewMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW,this.inheritanceFileView);
			setHdnFileNumber( inheritanceFileView.getFileNumber().toString() );
			setHdnFileId( inheritanceFileView.getInheritanceFileId().toString() );
		}
	}



	public String getHdnFileId() {
		return hdnFileId;
	}



	public void setHdnFileId(String hdnFileId) {
		this.hdnFileId = hdnFileId;
	}


	public String getHdnFileNumber() {
		return hdnFileNumber;
	}



	public void setHdnFileNumber(String hdnFileNumber) {
		this.hdnFileNumber = hdnFileNumber;
	}



	public HtmlCommandButton getBtnPrint() {
		return btnPrint;
	}



	public void setBtnPrint(HtmlCommandButton btnPrint) {
		this.btnPrint = btnPrint;
	}
	@SuppressWarnings("unchecked") 
	public List<SelectItem> getPaymentSource() {
		if( viewMap.containsKey(WebConstants.MemsPaymentCategories.PAYMENT_SOURCE) ) {
			return (List<SelectItem>) viewMap.get(WebConstants.MemsPaymentCategories.PAYMENT_SOURCE);
		}
		return new ArrayList<SelectItem>(0);
	}
	@SuppressWarnings("unchecked") 
	public List<SelectItem> getDisbursementSources() {
		if( viewMap.containsKey(DISBURSEMENT_SOURCES) ) {
			return (List<SelectItem>) viewMap.get(DISBURSEMENT_SOURCES);
		}
		return new ArrayList<SelectItem>(0);
	}




	public HtmlSelectOneMenu getCmbPaymentSource() {
		return cmbPaymentSource;
	}



	public void setCmbPaymentSource(HtmlSelectOneMenu cmbPaymentSource) {
		this.cmbPaymentSource = cmbPaymentSource;
	}



	public HtmlSelectOneMenu getCmbDisbursementSource() {
		return cmbDisbursementSource;
	}



	public void setCmbDisbursementSource(HtmlSelectOneMenu cmbDisbursementSource) {
		this.cmbDisbursementSource = cmbDisbursementSource;
	}

	@SuppressWarnings( "unchecked" )
	public MemsFollowupView getMemsFollowUp() 
	{
		if( viewMap.get(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW) != null )
		{
			memsFollowup = (MemsFollowupView)viewMap.get(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW);
		}
		return memsFollowup;
	}
	@SuppressWarnings( "unchecked" )
	public void setMemsFollowUp(MemsFollowupView followUp) {
		this.memsFollowup = followUp;
		if( this.memsFollowup != null)
		{
			viewMap.put(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW,this.memsFollowup);
		}
	}



	public String getOldActualCost() {
		return oldActualCost;
	}



	public void setOldActualCost(String oldActualCost) {
		this.oldActualCost = oldActualCost;
	}



	public HtmlSelectOneMenu getCmbUserGroups() {
		return cmbUserGroups;
	}



	public void setCmbUserGroups(HtmlSelectOneMenu cmbUserGroups) {
		this.cmbUserGroups = cmbUserGroups;
	}



	public HtmlCommandButton getBtnRevReq() {
		return btnRevReq;
	}



	public void setBtnRevReq(HtmlCommandButton btnRevReq) {
		this.btnRevReq = btnRevReq;
	}



	public HtmlCommandButton getBtnReviewed() {
		return btnReviewed;
	}



	public void setBtnReviewed(HtmlCommandButton btnReviewed) {
		this.btnReviewed = btnReviewed;
	}



	public HtmlInputText getTxtReviewComments() {
		return txtReviewComments;
	}



	public void setTxtReviewComments(HtmlInputText txtReviewComments) {
		this.txtReviewComments = txtReviewComments;
	}



	public HtmlOutputText getLblRevComments() {
		return lblRevComments;
	}



	public void setLblRevComments(HtmlOutputText lblRevComments) {
		this.lblRevComments = lblRevComments;
	}



	public HtmlOutputText getLblSendTo() {
		return lblSendTo;
	}



	public void setLblSendTo(HtmlOutputText lblSendTo) {
		this.lblSendTo = lblSendTo;
	}



	public org.richfaces.component.html.HtmlTab getTabReview() {
		return tabReview;
	}



	public void setTabReview(org.richfaces.component.html.HtmlTab tabReview) {
		this.tabReview = tabReview;
	}



	public HtmlCommandButton getBtnSendForBlocking() {
		return btnSendForBlocking;
	}



	public void setBtnSendForBlocking(HtmlCommandButton btnSendForBlocking) {
		this.btnSendForBlocking = btnSendForBlocking;
	}
}
