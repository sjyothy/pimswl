package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.entity.Zakat;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.ZakatCalculationService;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings("serial")
public class ZakatCalculationTestBean extends AbstractMemsBean 
{
    private HtmlDataTable dataTable = new HtmlDataTable();
    private List<Zakat> dataList = new ArrayList<Zakat>();
    
    
    @SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
			}
			else
			{
				updateValuesFromMap();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
	 
		updateValuesToMap();
	}
	
	
	
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
	
	}
	@SuppressWarnings( "unchecked")
	public void onCalculateZakat()  
	{
		try
		{
			updateValuesFromMap();
			ZakatCalculationService zakatCalculationService = new ZakatCalculationService();
			zakatCalculationService.processJob();
			updateValuesToMap();
		}
		catch ( Exception e )
		{
			logger.LogException( "onCalculateZakat|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}



	public HtmlDataTable getDataTable() {
		return dataTable;
	}



	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}



	public List<Zakat> getDataList() {
		return dataList;
	}



	public void setDataList(List<Zakat> dataList) {
		this.dataList = dataList;
	}
	
}
