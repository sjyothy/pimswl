package com.avanza.pims.web.mems.minors;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlPanelGrid;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.bpel.memsrecommendationdisbursementbpel.proxy.MEMSRecommendationDisbursementBPELPortClient;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.mems.SocialResearchService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.SocialResearchView;
import com.avanza.pims.ws.vo.mems.DisbursementSourceView;
import com.avanza.pims.ws.vo.mems.ResearchRecommendationView;
import com.avanza.ui.util.ResourceUtil;


public class SocialResearchBean extends AbstractMemsBean{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(SocialResearchBean.class);
	private static final String TASK_ATT_SOCIAL_RESEARCH_ID = "SOCIAL_RESEARCH_ID";
	private static final String RECOMMENDATION_DISBURSEMENT_ID = "RECOMMENDATION_ID";
	private static final String TASK_TYPE = "TASK_TYPE";
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_APPROVE = "PAGE_MODE_APPROVE";
	private static final String PAGE_MODE_RESUBMIT = "PAGE_MODE_RESUBMIT";
	private static final String PAGE_MODE_EXECUTE_RECOMMENDATIONS = "PAGE_MODE_EXECUTE_RECOMMENDATIONS";
	private static final String PAGE_MODE_DISBURSE_PAYMENTS = "PAGE_MODE_DISBURSE_PAYMENTS";
	private static final String RESEARCH_RECOMMENDATION_ROW = "RESEARCH_RECOMMENDATION_ROW";
	private final String SOCIAL_RECOMM_VIEW_LIST_UPDATED_RECORD = "SOCIAL_RECOMM_VIEW_LIST_UPDATED_RECORD";
	
	
	
	private String socialResearchId ;
	private SocialResearchService researchService = new SocialResearchService();
	private List<ResearchRecommendationView> recommendationsList = new ArrayList<ResearchRecommendationView>( 0 );
	private String disburseSrcId;
    private SocialResearchView researchView = new SocialResearchView();
	private HtmlDataTable dataTable;
    private String pageTitle;
    private String pageMode;
    protected List<String> errorMessages = new ArrayList<String>();
	protected List<String> successMessages = new ArrayList<String>();
	private HtmlCommandButton btnCloseRecommendation = new HtmlCommandButton();
	private HtmlCommandButton btnRejectRecommendation = new HtmlCommandButton();
	
	private HtmlCommandButton btnResubmit = new HtmlCommandButton();
	
	
	private HtmlCommandButton btnSendToFinance = new HtmlCommandButton();
	private HtmlCommandButton btnApprove = new HtmlCommandButton();
	private HtmlCommandButton btnReject = new HtmlCommandButton();
	private HtmlCommandButton btnAddDisburseSrc = new HtmlCommandButton();
	private HtmlCommandButton btnComplete = new HtmlCommandButton();
	private HtmlCommandButton btnDisburseDone = new HtmlCommandButton();
	protected HtmlPanelGrid tbl_Action = new HtmlPanelGrid();
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			canAddAttachmentsNotes(true);
			if( !isPostBack() )
			{
				getDataFromTaskList();
				getSocialResearch();
				getResearchRecommendation();
			}
		}
		catch ( Exception e )
		{
			logger.LogException("init|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));			
			
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void canAddAttachmentsNotes(boolean canAdd) {
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	@SuppressWarnings("unchecked")
	public void loadAttachments(Long objectId)
	{
		
		//common settings
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.SocialResearch.PROCEDURE_KEY);
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, WebConstants.Attachment.PIMS_REPOSITORY_ID);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.SocialResearch.EXTERNAL_ID);
		viewMap.put("noteowner", WebConstants.SocialResearch.EXTERNAL_ID);
    	
		if(objectId!= null)
		{
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, objectId.toString());
			viewMap.put("entityId", objectId.toString());
		}
	}
	@SuppressWarnings( "unchecked" )
	public void prerender()
	{
		

		try
		{
		   disableAllControls();
	       if( this.getPageMode().equals( PAGE_MODE_APPROVE ) )
	    	   setControlsForApproveMode();
	       else if( this.getPageMode().equals(PAGE_MODE_EXECUTE_RECOMMENDATIONS) )
	    	   setControlsForExecuteRecommendationMode();
	       else if ( this.getPageMode().equals( PAGE_MODE_DISBURSE_PAYMENTS ) )
	    	   setControlsForDisbursement();
	       else if ( this.getPageMode().equals( PAGE_MODE_RESUBMIT) )
	    	   setControlsForResubmit();
	    	   
		}
		catch ( Exception e )
		{
			errorMessages = new ArrayList<String>( 0 );
			logger.LogException("prerender|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));			
			
			
		}

		
	}
	private void setControlsForResubmit()
	{
		btnResubmit.setRendered( true );
	}
	private void setControlsForDisbursement()
	{
		btnDisburseDone.setRendered(true);
		
		
		
	}
	private void setControlsForApproveMode()
	{
		btnApprove.setRendered(true);
		btnReject.setRendered(true);
		
	}
	
	private void setControlsForExecuteRecommendationMode() throws Exception
	{
		btnComplete.setRendered(true);
		
		btnCloseRecommendation.setRendered(true);
		btnRejectRecommendation.setRendered(true);
		
		if( hasDisbursementRecommendations() )
		{
			btnSendToFinance.setRendered(true);
			btnAddDisburseSrc.setRendered(true);	
			tbl_Action.setRendered(true);	
		}
	}
	
	private void disableAllControls()
	{
		
		btnResubmit.setRendered( false );
		btnDisburseDone.setRendered(false);
		btnAddDisburseSrc.setRendered(false);
		btnApprove.setRendered(false);
		btnCloseRecommendation.setRendered(false);
		btnRejectRecommendation.setRendered(false);
		btnComplete.setRendered(false);
		btnReject.setRendered(false);
		btnSendToFinance.setRendered(false);
		tbl_Action.setRendered(false);
		
	}
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  UserTask userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	  logger.logInfo("getDataFromTaskList|TaskId..."+userTask.getTaskId());
      setUserTask(userTask);
      sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
      if(userTask.getTaskAttributes().get( TASK_ATT_SOCIAL_RESEARCH_ID )!=null)
      {
    	this.setSocialResearchId( userTask.getTaskAttributes().get( TASK_ATT_SOCIAL_RESEARCH_ID ).toString()  );  
    	  
      }
      setPageModeBasedOnTaskType();
     
    }
	
	@SuppressWarnings( "unchecked" )
	private void setPageModeBasedOnTaskType()
	{
		UserTask userTask = this.getUserTask();
		if( userTask.getTaskAttributes().get( TASK_TYPE )!=null)
	    {
	       String taskType =    	  userTask.getTaskAttributes().get( TASK_TYPE ).toString();
	    	
	       if( taskType.equals( WebConstants.SocialResearchTaskType.APPROVE_SOCIAL_REQ  ) )
	       {
	    	   this.setPageMode( PAGE_MODE_APPROVE );
	       }
	       else if( taskType.equals( WebConstants.SocialResearchTaskType.EDUCATION_CARING ) || 
	    		    taskType.equals( WebConstants.SocialResearchTaskType.FINANCIAL_CARING ) || 
	    		    taskType.equals( WebConstants.SocialResearchTaskType.HEALTH_CARING ) || 
	    		    taskType.equals( WebConstants.SocialResearchTaskType.SOCIAL_CARE_CARING ) ||
	    		    taskType.equals( WebConstants.SocialResearchTaskType.RESIDENTIAL_CARING) || 
	    		    taskType.equals( WebConstants.SocialResearchTaskType.SOCIAL_SOLIDARITY_CARING ) 
	              )
	       {
	    	   this.setPageMode( PAGE_MODE_EXECUTE_RECOMMENDATIONS );
	       }
	       else if( taskType.equals( WebConstants.SocialResearchTaskType.SOCIAL_RESEARCHER  )  )
	       {
	    	   this.setPageMode( PAGE_MODE_RESUBMIT );
	       }
	       else if ( taskType.equals( WebConstants.SocialResearchTaskType.REC_DIS_REQ ) )
	       {
	    	   this.setPageMode( PAGE_MODE_DISBURSE_PAYMENTS );
	       }
	    }
	}
	@SuppressWarnings( "unchecked" )
	public Long getCaringTypeFromTaskType()throws Exception
	{
		UserTask userTask = this.getUserTask();
		if( userTask.getTaskAttributes().get( TASK_TYPE )!=null)
	    {
	       String taskType =    	  userTask.getTaskAttributes().get( TASK_TYPE ).toString();
	    	return taskTypeCaringTypeMapping(taskType);
	    	
	    	
	    }
		return null;
	}

	/**
	 * @param taskType
	 * @return
	 */
	public static Long taskTypeCaringTypeMapping(String taskType)throws Exception 
	{
		
		if( taskType.equals( WebConstants.SocialResearchTaskType.EDUCATION_CARING ) )
			return new Long (WebConstants.CaringType.EDUCATION ) ;
		if( taskType.equals( WebConstants.SocialResearchTaskType.FINANCIAL_CARING ) )
			return new Long ( WebConstants.CaringType.FINANCIAL );
		if( taskType.equals( WebConstants.SocialResearchTaskType.HEALTH_CARING) )
			return new Long (WebConstants.CaringType.HEALTH );
		if( taskType.equals( WebConstants.SocialResearchTaskType.SOCIAL_CARE_CARING ) )
			return new Long (WebConstants.CaringType.SOCIAL_CARE );
		if ( taskType.equals( WebConstants.SocialResearchTaskType.SOCIAL_SOLIDARITY_CARING ) )
			return new Long (WebConstants.CaringType.SOCIAL_SOLIDARITY );
		if ( taskType.equals( WebConstants.SocialResearchTaskType.RESIDENTIAL_CARING) )
			return new Long (WebConstants.CaringType.RESIDENTIAL);
		return null;
	}
	@SuppressWarnings( "unchecked" )
	public void onTemplateMethod()
	{
		String methodName = "onSave|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			
			if( !hasAddDisSrcError() )
			{
				
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}
	@SuppressWarnings( "unchecked" )
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onOpenFile()
	{
		String methodName = "onOpenFile|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			sessionMap.put( WebConstants.InheritanceFile.FILE_ID, this.getResearchView().getInheritanceFileView().getInheritanceFileId() );
		    sessionMap.put (WebConstants.InheritanceFilePageMode.IS_POPUP,true);
			String javaScriptText ="var screen_width = 900;"+
            "var screen_height =600;"+
            "window.open('inheritanceFile.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes,resizable=true');";
		    executeJavascript(  javaScriptText );
		    
		    logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onAddRecommendationsFollowup()
	{
		String methodName = "onAddRecommendationsFollowup|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			ResearchRecommendationView  row = ( ResearchRecommendationView )dataTable.getRowData();
			sessionMap.put( WebConstants.RecommenationFollowup.RECOMMENDATION_ID, row.getResearchRecommendationId() );
		    String javaScriptText ="var screen_width = 800;"+
            "var screen_height = 550;"+
            "window.open('recommFollowup.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=100,scrollbars=yes,status=yes,resizeable=true');";
		    executeJavascript(  javaScriptText );
		    
		    logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}
	

	
	@SuppressWarnings( "unchecked" )
	public Boolean hasRowSelected()
	{
	    
		for (ResearchRecommendationView vo : this.getRecommendationsList()) {
			
        	if( vo.getSelected() != null && vo.getSelected() )
        	{
        	 	
             return true;               
        	 
        	}
		}
		errorMessages.add(ResourceUtil.getInstance().getProperty("socialResearch.SelectRow"));
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean hasSendToFinanceError()
	{
		Boolean hasError =false;
		String disbursementRequired="1";
		String isPeriodic="1";
		if( !hasRowSelected() )
			hasError = true;
		
		for (ResearchRecommendationView selectedRows : getSelectedRows()) {
			
			if( !selectedRows.getDisbursementRequired().trim().equals(disbursementRequired)|| 
				selectedRows.getIsPeriodic().trim().equals(isPeriodic)|| 
				//selectedRows.getPaymentDetailFormView()==null ||
				selectedRows.getAmount() == null || 
				selectedRows.getAmount()<= 0d 
			   )
			{
				if(!selectedRows.getDisbursementRequired().trim().equals(disbursementRequired))
				{
					hasError=true;
					errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SocialResearchRecommendations.SEND_TO_FINANCE_ERROR_DISB_REQ));
					
				}
				else
				{
					if(selectedRows.getIsPeriodic().trim().equals(isPeriodic))
					{
						hasError=true;
						errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SocialResearchRecommendations.SEND_TO_FINANCE_ERROR_URGENT));
						
					}
					else if(selectedRows.getAmount() == null || selectedRows.getAmount()<= 0d )
					{

					  String msg =  MessageFormat.format(
								                         CommonUtil.getBundleMessage("socialResearchRecommendation.amountRequiredForDisbursementReccomendation"),
								                         selectedRows.getRecommendationNumber()
								                        );
					 errorMessages.add(msg);
					  return true;
					 
					}
					
//					else if(selectedRows.getPaymentDetailFormView()==null)
//					{
//						hasError=true;
//						errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SocialResearchRecommendations.SEND_TO_FINANCE_ERROR_NO_P_DETAIL));
//						
//					}
				}
				break;
				
			}
			
		}
		
		return hasError;
	}
	
	private List<ResearchRecommendationView>  getSelectedRows() {
		List<ResearchRecommendationView> selectedList = new ArrayList<ResearchRecommendationView>();
		   for (ResearchRecommendationView vo : this.getRecommendationsList()) 
		   {
				
			   if( vo.getSelected() != null && vo.getSelected() )
           		{
           			selectedList.add( vo );
                   
                   
           		}
				}
		   return selectedList;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onSendToFinance()
	{
		final String methodName = "onSendToFinance|";
		try	
		{	
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			logger.logInfo(methodName + " --- STARTED --- ");
			SystemParameters parameters = SystemParameters.getInstance();
			MEMSRecommendationDisbursementBPELPortClient  port = new MEMSRecommendationDisbursementBPELPortClient();
	        String endPoint= parameters.getParameter( "MEMSRecommendationDisbursementBPELEndPoint" );
			if( !hasSendToFinanceError()  )
			{
				List<ResearchRecommendationView> selectedList = new ArrayList<ResearchRecommendationView>();
	            for (ResearchRecommendationView vo : this.getRecommendationsList()) {
					
	            	if( vo.getSelected() != null && vo.getSelected() )
	            	{
	            		setRowStatus(vo, WebConstants.ResearchRecommendationStatus.DISBURSEMENT_REQUIRED);
	                    selectedList.add( vo );
	                    
	                    
	            	}
				}
	            //Setting selected = false for already operated rows
 	             for (ResearchRecommendationView researchRecommendationView : selectedList) {
					researchRecommendationView.setSelected(false);
				}
	            
				Long researchRecommendationId = researchService.sendToFinance(selectedList, getLoggedInUserId() );
				port.setEndpoint(endPoint);
		       port.initiate( new Long (this.getSocialResearchId() ), researchRecommendationId, getLoggedInUserId(), null, null);
		       if(checkIfCompleteCanBePerformed(this.getRecommendationsList()))
	             {
	            	 onComplete();
	             }
				successMessages.add(ResourceUtil.getInstance().getProperty("socialResearch.RecommendationSendToFinance"));
			}
			
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public Boolean hasDisburseError()
	{
		Boolean hasError =false;
		if( !hasRowSelected() )
			hasError = true;

		for (ResearchRecommendationView selectedRows : getSelectedRows()) 
		{
		 if( selectedRows.getPaymentDetailFormView()==null )
		 {
			if(selectedRows.getPaymentDetailFormView()==null)
			{
				hasError=true;
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SocialResearchRecommendations.SEND_TO_FINANCE_ERROR_NO_P_DETAIL));
			}
		 }
		 break;
		}
		return hasError;
	}
	
	private Boolean hasAllRecommendationsDisbursed()
	{
		
		List<ResearchRecommendationView> selectedList = new ArrayList<ResearchRecommendationView>();
		DomainDataView closed = this.getRecommendationStatusFromType( WebConstants.ResearchRecommendationStatus.CLOSED );
        for (ResearchRecommendationView vo : this.getRecommendationsList()) {
			
        	//If there is any recommendation whose status is not closed i.e disbursed then return false;
        	if( vo.getStatus() != null  && vo.getStatus().compareTo( closed.getDomainDataId() ) != 0)
        	{
        		return false;
                
                
        	}
		}
        return true;
		
	}
	@SuppressWarnings( "unchecked" )
	public void onDisburse()
	{
		String methodName = "onDisburse|";
		try	
		{	
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			logger.logInfo(methodName + " --- STARTED --- ");
			if( !hasDisburseError()  )
			{
				List<ResearchRecommendationView> selectedList = new ArrayList<ResearchRecommendationView>();
	            for (ResearchRecommendationView vo : this.getRecommendationsList()) {
					
	            	if( vo.getSelected() != null && vo.getSelected() )
	            	{
	            		setRowStatus(vo, WebConstants.ResearchRecommendationStatus.CLOSED);
	                    selectedList.add( vo );
	                    
	                    
	            	}
				}
				researchService.disburseRecommendations(selectedList, getLoggedInUserId());
				if( hasAllRecommendationsDisbursed() )
				{
					this.setPageMode( PAGE_MODE_VIEW );
					setTaskOutCome(TaskOutcome.OK );
				}
				successMessages.add(ResourceUtil.getInstance().getProperty("socialResearch.RecommendationDisbursed"));
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	public Boolean hasRejectRecommendationsError()
	{
		Boolean hasError =false;
		if( !hasRowSelected() )
			return true;
		else
		{
		
		 	 for (ResearchRecommendationView selectedRows : getSelectedRows()) 
		 	 {
				
				if(	selectedRows.getRejectionReason() == null || selectedRows.getRejectionReason().trim().length() <= 0 )
					
				{
					{
					  String msg =  CommonUtil.getBundleMessage( "socialResearchRecommendation.msg.rejectionReasonRequired") ;
					 errorMessages.add(msg);
					 return true;
					}
				}
				
			}
		}
		
		return hasError;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean hasCloseRecommendationsError()
	{
		Boolean hasError =false;
		if( !hasRowSelected() )
			return true;
		else
		{
		 String isPeriodic="1"; // implies urgent is 0 
		
	 	 for (ResearchRecommendationView selectedRows : getSelectedRows()) {
			
			if(	selectedRows.getDisbursementRequired() != null && selectedRows.getDisbursementRequired().compareTo("1") == 0 )
				
			{
				if(!selectedRows.getIsPeriodic().trim().equals(isPeriodic) )
				{
				 errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SocialResearchRecommendations.CLOSE_RECOMMENDATION_ERROR));
				 return true;
				}
				
				if(selectedRows.getAmount() == null || selectedRows.getAmount()<= 0d )
				{
				  String msg =  MessageFormat.format(
							                         CommonUtil.getBundleMessage("socialResearchRecommendation.amountRequiredForDisbursementReccomendation"),
							                         selectedRows.getRecommendedAmount()
							                        );
				 errorMessages.add(msg);
				 return true;
				}
			}
			
		}
		}
		
		return hasError;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onRejectRecommendation()
	{
		try	
		{	
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			
			if( hasRejectRecommendationsError() )
			{
				return;
			}
			List<ResearchRecommendationView> selectedList = new ArrayList<ResearchRecommendationView>();
            for (ResearchRecommendationView vo : this.getRecommendationsList()) {
				
            	if( vo.getSelected() != null && vo.getSelected() )
            	{
            		setRowStatus(vo, WebConstants.ResearchRecommendationStatus.REJECTED);
                    selectedList.add( vo );
                   
            		
            	}
			}
            //ISSUE : the rows in selectedList have their selected set to true , which gives wrong results in getSelectedRows() in future 
             //DESC : the rows after being closed , have their selected still true , and the checkboxes disappear so we can't make the selected as false. getSelectedRow will 
             //       entertain the rows that have been closed , and will try to send them to finance , since their selected = true
             //RESOLUTION : below 
             for (ResearchRecommendationView researchRecommendationView : selectedList) {
				researchRecommendationView.setSelected(false);
			}
             //
             researchService.rejectRecommendation( selectedList,CommonUtil.getLoggedInUser() );
           
           
		}
		catch (Exception exception) 
		{
			logger.LogException( "onRejectRecommendation --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onCloseRecommendations()
	{
		final String methodName = "onCloseRecommendations|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			if( !hasCloseRecommendationsError()  )
			{
				List<ResearchRecommendationView> selectedList = new ArrayList<ResearchRecommendationView>();
	            for (ResearchRecommendationView vo : this.getRecommendationsList()) {
					
	            	if( vo.getSelected() != null && vo.getSelected() )
	            	{
	            		setRowStatus(vo, WebConstants.ResearchRecommendationStatus.CLOSED);
	                    selectedList.add( vo );
	                   
	            		
	            	}
				}
	            //ISSUE : the rows in selectedList have their selected set to true , which gives wrong results in getSelectedRows() in future 
	             //DESC : the rows after being closed , have their selected still true , and the checkboxes disappear so we can't make the selected as false. getSelectedRow will 
	             //       entertain the rows that have been closed , and will try to send them to finance , since their selected = true
	             //RESOLUTION : below 
	             for (ResearchRecommendationView researchRecommendationView : selectedList) {
					researchRecommendationView.setSelected(false);
				}
	             //
	             researchService.closeRecommendation( selectedList,CommonUtil.getLoggedInUser() );
	           
	             
	             if(checkIfCompleteCanBePerformed(this.getRecommendationsList()))
	             {
	            	 onComplete();
	             }
	             successMessages.add(ResourceUtil.getInstance().getProperty("socialResearch.RecommendationClosed"));
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	private Boolean checkIfCompleteCanBePerformed(
			List<ResearchRecommendationView> recommendationsList2) throws Exception 
	{
		// TODO Auto-generated method stub
		Boolean returnVal=true;
		DomainDataView closedStatus = new UtilityService().getDomainDataByValue(WebConstants.ResearchRecommendationStatus.RECOMMENDATION_STATUS,WebConstants.ResearchRecommendationStatus.CLOSED);
		DomainDataView disbReqStatus = new UtilityService().getDomainDataByValue(WebConstants.ResearchRecommendationStatus.RECOMMENDATION_STATUS,WebConstants.ResearchRecommendationStatus.DISBURSEMENT_REQUIRED);

		
		for (ResearchRecommendationView researchRecommendationView : recommendationsList2) 
		{
			if(researchRecommendationView.getStatus().compareTo(closedStatus.getDomainDataId())!=0
			&& researchRecommendationView.getStatus().compareTo(disbReqStatus.getDomainDataId())!=0 )
			{
				returnVal=false;
			}
			
		}
		return returnVal;
		
	}



	public Boolean hasApproveError()
	{
		Boolean hasError =false;
		
		return hasError;
	}
	@SuppressWarnings( "unchecked" )
	public void onApprove()
	{
		insertNewlyAddedPaymentDetails();
		String methodName = "onApprove|";
		try	
		{	
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			logger.logInfo(methodName + " --- STARTED --- ");
			
			
			if( !hasApproveError() )
			{			
				setTaskOutCome(TaskOutcome.APPROVE);
				successMessages.add(ResourceUtil.getInstance().getProperty("socialResearch.RecommendationApproved"));
				this.setPageMode( PAGE_MODE_VIEW );
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	private void insertNewlyAddedPaymentDetails()  {
		String methodName = "insertNewlyAddedPaymentDetails";
		
		try
		{
			logger.logInfo(methodName+ " Starts");
			researchService.addPaymentDetails(getRecommendationsList());
			logger.logInfo(methodName+ " Ends");
		}
		catch(Exception Ex)
		{
			logger.LogException(methodName + " crahsed ", Ex);
		}
		
		
	}



	public Boolean hasRejectError()
	{
		Boolean hasError =false;
		
		return hasError;
	}	
	@SuppressWarnings( "unchecked" )
	public void onReject()
	{
		String methodName = "onReject|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			errorMessages = new ArrayList<String>();
			if( !hasRejectError() )
			{
			successMessages = new ArrayList<String>();
			setTaskOutCome(TaskOutcome.REJECT);
			successMessages.add(ResourceUtil.getInstance().getProperty("socialResearch.RecommendationRejected"));
			this.setPageMode( PAGE_MODE_VIEW );
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	public Boolean hasCompleteError() throws Exception
	{
		Boolean hasError =false;
			for (ResearchRecommendationView recommendations : getRecommendationsList()) {
				
				if(!recommendations.getStatusKey().equals(WebConstants.ResearchRecommendationStatus.CLOSED)  && 
				   !recommendations.getStatusKey().equals(WebConstants.ResearchRecommendationStatus.DISBURSEMENT_REQUIRED) &&
				   !recommendations.getStatusKey().equals(WebConstants.ResearchRecommendationStatus.REJECTED)
				 )
				{
					hasError=true;
					errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SocialResearchRecommendations.COMPLETE_ERROR));
					break;
				}
			}
		return hasError;
	}
	@SuppressWarnings( "unchecked" )
	public void onComplete()
	{
		final String methodName = "onComplete|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();			
			if( !hasCompleteError() )
			{
				setTaskOutCome(TaskOutcome.OK);
				successMessages.add(ResourceUtil.getInstance().getProperty("socialResearch.RecommendationCompleted"));
				this.setPageMode( PAGE_MODE_VIEW );
			}
			
			
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onResubmit()
	{
		insertNewlyAddedPaymentDetails();
		String methodName = "onResubmit|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();			
			//if( !hasCompleteError() )
			{
				setTaskOutCome(TaskOutcome.OK);
				successMessages.add(ResourceUtil.getInstance().getProperty("socialResearch.RecommendationsResubmit"));
			}
			this.setPageMode( PAGE_MODE_VIEW );
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	public void setRowStatus(ResearchRecommendationView vo,String statusKey)
	{
		DomainDataView status = this.getRecommendationStatusFromType( statusKey );
		vo.setStatus( status.getDomainDataId() );
		vo.setStatusEn( status.getDataDescEn() );
		vo.setStatusAr( status.getDataDescAr() );
		vo.setStatusKey(  status.getDataValue() );
		vo.setUpdatedBy( getLoggedInUserId() );
		
	}
	public Boolean hasAddDisSrcError() throws Exception
	{
		Boolean hasError =false;
		if( !hasRowSelected() )
			hasError = true;
		
		String isPeriodic="1";
		for (ResearchRecommendationView recommendations : getSelectedRows()) {
			
			if(recommendations.getIsPeriodic().trim().equals(isPeriodic))
			{
				hasError=true;
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SocialResearchRecommendations.ADD_DISB_SRC_ERROR));
			}
		}
		return hasError;
	}
	@SuppressWarnings( "unchecked" )
	public void onAddDisbursementSource()
	{
		String methodName = "onAddDisbursementSource|";
		try	
		{
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			logger.logInfo(methodName + " --- STARTED --- ");
			HashMap disSrcMap = getDisburseSrcMap();
			if( !hasAddDisSrcError() )
			{
	            for (ResearchRecommendationView vo : this.getRecommendationsList()) {
					
	            	if( vo.getSelected() != null && vo.getSelected() )
	            	{
	            		String disSrcName="";
	            		DisbursementSourceView disSrc = new DisbursementSourceView();
	            		if( disSrcMap.containsKey( new Long (this.getDisburseSrcId()  ) ) )
	            			disSrcName = disSrcMap.get( new Long (this.getDisburseSrcId()  ) ).toString();
	            		disSrc.setSrcId( this.getDisburseSrcId() );
	            		if( isEnglishLocale() )
	            		{
	            		    disSrc.setSrcNameEn( disSrcName );
	            		    disSrc.setSrcNameAr( "" );
	            		}
	            		else
	            		{
	            			disSrc.setSrcNameAr( disSrcName );
	            			disSrc.setSrcNameEn( "" );
	            		}
	            		vo.setDisburseSrc( disSrc );
	            		
	            	}
				}
	            
	            successMessages.add(ResourceUtil.getInstance().getProperty("socialResearch.RecommendationDisSrcAdded"));
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
    	final String methodName="setTaskOutCome|";
    	logger.logInfo(methodName+"|"+" Start...");
    	try
    	{
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			String loggedInUser=getLoggedInUserId();
			logger.logInfo(methodName+" TaskId:%s| TaskOutCome:%s| loggedInUser:%s",userTask.getTaskId(),taskOutCome,loggedInUser);
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
			logger.logInfo(methodName+"|"+" Finish...");
    	}
    	catch(PIMSWorkListException ex)
    	{
    		
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    		throw ex;
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    		throw ex;
    	}
    }
	
	@SuppressWarnings( "unchecked" )
	public void getSocialResearch() throws Exception
	{
		HashMap<String, String> criteria = new HashMap<String, String>();
		criteria.put(WebConstants.ResearchRecommendationCriteria.SOCIAL_RESEARCH_ID, this.getSocialResearchId() );
		SocialResearchView view = researchService.getSocialResearchView( criteria ) ;
		this.setResearchView( view   );
		loadAttachments(view.getSocialResearchId() );
		
		
	}
	@SuppressWarnings( "unchecked" )
	public void getResearchRecommendation() throws Exception
	{
		HashMap<String, String> criteria = new HashMap<String, String>();
		criteria.put(WebConstants.ResearchRecommendationCriteria.SOCIAL_RESEARCH_ID, this.getSocialResearchId() );
		if( this.getCaringTypeFromTaskType()!= null )
		 criteria.put(WebConstants.ResearchRecommendationCriteria.CARING_TYPE, this.getCaringTypeFromTaskType().toString() );
		if( this.getUserTask()!= null && this.getUserTask().getTaskType()!=null && 
			this.getUserTask().getTaskType().equals( WebConstants.SocialResearchTaskType.REC_DIS_REQ ) )
		{
			
			 
			criteria.put(WebConstants.ResearchRecommendationCriteria.RES_REC_DIS_ID, this.getUserTask().getTaskAttributes().get( "RECOMMENDATION_ID" ).toString() );
			
		}
		
		this.setRecommendationsList( researchService.getResearchRecommendation( criteria ) );
		
		
	}

	@SuppressWarnings( "unchecked" )
	public Boolean hasDisbursementRecommendations() throws Exception
	{
		DomainDataView closed               = this.getRecommendationStatusFromType(WebConstants.ResearchRecommendationStatus.CLOSED);
		DomainDataView disbursementRequired = this.getRecommendationStatusFromType(WebConstants.ResearchRecommendationStatus.DISBURSEMENT_REQUIRED);
 		for (ResearchRecommendationView researchRecommendation : this.getRecommendationsList()) {
			
 			//if disbursement is required and its urgent disbursement and the status is not 
 			//closed or disbursement required
 			if( 
 					researchRecommendation.getDisbursementRequired().equals( "1" ) &&
 			    !researchRecommendation.getIsPeriodic().equals( "1" ) &&
 				   
 				researchRecommendation.getStatus().compareTo( closed.getDomainDataId() ) != 0 &&
 				researchRecommendation.getStatus().compareTo( disbursementRequired.getDomainDataId() ) != 0
 			  )
 			{
 				 return true;
 			}
		} 
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public List<ResearchRecommendationView> getRecommendationsList() {
		
		if( viewMap.containsKey("recommendationsList") )
			recommendationsList = (ArrayList<ResearchRecommendationView>) viewMap.get("recommendationsList");
		return recommendationsList;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setRecommendationsList(
			List<ResearchRecommendationView> recommendationsList) {
		this.recommendationsList = recommendationsList;
		if( this.recommendationsList!=null )
			viewMap.put("recommendationsList",this.recommendationsList); 
	}
	@SuppressWarnings( "unchecked" )
	public String getDisburseSrcId() {
		
		if( viewMap.containsKey("disburseSrcId") )
			disburseSrcId = viewMap.get("disburseSrcId").toString();
		
		return disburseSrcId;
	}
	@SuppressWarnings( "unchecked" )
	public void setDisburseSrcId(String disburseSrcId) {
		this.disburseSrcId = disburseSrcId;
		if( this.disburseSrcId !=null )
			viewMap.put("disburseSrcId ",this.disburseSrcId ); 

	}

	@SuppressWarnings( "unchecked" )
	 public DomainDataView getRecommendationStatusFromType( String statusType)
	 {
		String viewKey = "RecommendationStatus"+statusType;
		 DomainDataView ddv;
		 if ( viewMap.get( viewKey ) != null )
		 {
			 ddv = (DomainDataView)viewMap.get( viewKey );
		 }
		 else
		 {
			 
			 ddv = CommonUtil.getIdFromType( this.getRecommendationStatusList(), statusType);
			 viewMap.put( viewKey,ddv);
		 
		 } 
	 
	    return ddv;
	 }

	@SuppressWarnings( "unchecked" )
	 public List<DomainDataView> getRecommendationStatusList()
	 {
		 List<DomainDataView >ddvList;
		 if ( viewMap.get( "RecommendationStatusList" ) != null )
		 {
			 ddvList = (ArrayList<DomainDataView>)viewMap.get( "RecommendationStatusList" );
		 }
		 else
		 {
			 
			 ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.ResearchRecommendationStatus.RECOMMENDATION_STATUS);
			 viewMap.put( "RecommendationStatusList",ddvList );
		 
		 } 
	 
	    return ddvList;
	 }

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	
	@SuppressWarnings( "unchecked" )
	public SocialResearchView getResearchView() {
		if( viewMap.get( "researchView" ) != null )
			researchView = ( SocialResearchView )viewMap.get( "researchView" ) ;
		return researchView;
	}

	@SuppressWarnings( "unchecked" )
	public void setResearchView(SocialResearchView researchView) {
	
		this.researchView = researchView;
		if( this.researchView != null )
			viewMap.put("researchView", this.researchView);
	}
	
	@SuppressWarnings( "unchecked" )
	public HashMap<Long,String> getDisburseSrcMap() throws Exception
	{
		String methodName = "getDisburseSrcMap";
		logger.logInfo(methodName + "Start");
	    HashMap<Long, String> disbursementSourceMap ;
        if ( viewMap.get( "disbursementSourceMap"  ) == null )
        {
    	   DisbursementService service = new DisbursementService();
    	   disbursementSourceMap= service.getDisbursementSourceList( CommonUtil.getIsEnglishLocale() );
           viewMap.put("disbursementSourceMap", disbursementSourceMap )      ;  
        
        }
        else
        {
        	
        	disbursementSourceMap =  (HashMap<Long,String>)viewMap.get( "disbursementSourceMap"  ) ;
        	
        }
        logger.logInfo(methodName + "Finish");
		return disbursementSourceMap;
			
		}

	
	
	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask)
	{
		
	
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
		

	}
	@SuppressWarnings( "unchecked" )
	public String getSocialResearchId() {
		if( viewMap.get( "socialResearchId" ) != null )
			socialResearchId = viewMap.get( "socialResearchId" ).toString();
		return socialResearchId;
	}


@SuppressWarnings( "unchecked" )
	public void setSocialResearchId(String socialResearchId) {
		this.socialResearchId = socialResearchId;
		if( this.socialResearchId !=null )
			viewMap.put("socialResearchId", socialResearchId);
	}



public String getPageTitle() {
	
	this.setPageTitle(ResourceUtil.getInstance().getProperty("SocialResearch.Title"));
	return pageTitle;
}



public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
}
	
public String getErrorMessages()
{

	return CommonUtil.getErrorMessages(errorMessages);
}
public String getSuccessMessages()
{
	String messageList="";
	if ((successMessages== null) || (successMessages.size() == 0)) 
	{
		messageList = "";
	}
	else
	{
		
		for (String message : successMessages) 
			{
				messageList +=  "<LI>" +message+ "<br></br>" ;
		    }
		
	}
	return (messageList);
}


@SuppressWarnings( "unchecked" )
public String getPageMode() {
	if( viewMap.get("pageMode")!= null )
		pageMode = viewMap.get("pageMode").toString();
	return pageMode;
}


@SuppressWarnings( "unchecked" )
public void setPageMode(String pageMode) {
	
	this.pageMode = pageMode;
	if( this.pageMode != null )
		viewMap.put( "pageMode", this.pageMode );
}



public HtmlCommandButton getBtnCloseRecommendation() {
	return btnCloseRecommendation;
}



public void setBtnCloseRecommendation(HtmlCommandButton btnCloseRecommendation) {
	this.btnCloseRecommendation = btnCloseRecommendation;
}



public HtmlCommandButton getBtnSendToFinance() {
	return btnSendToFinance;
}



public void setBtnSendToFinance(HtmlCommandButton btnSendToFinance) {
	this.btnSendToFinance = btnSendToFinance;
}



public HtmlCommandButton getBtnApprove() {
	return btnApprove;
}



public void setBtnApprove(HtmlCommandButton btnApprove) {
	this.btnApprove = btnApprove;
}



public HtmlCommandButton getBtnReject() {
	return btnReject;
}



public void setBtnReject(HtmlCommandButton btnReject) {
	this.btnReject = btnReject;
}



public HtmlCommandButton getBtnAddDisburseSrc() {
	return btnAddDisburseSrc;
}



public void setBtnAddDisburseSrc(HtmlCommandButton btnAddDisburseSrc) {
	this.btnAddDisburseSrc = btnAddDisburseSrc;
}



public HtmlCommandButton getBtnComplete() {
	return btnComplete;
}



public void setBtnComplete(HtmlCommandButton btnComplete) {
	this.btnComplete = btnComplete;
}



public HtmlPanelGrid getTbl_Action() {
	return tbl_Action;
}



public void setTbl_Action(HtmlPanelGrid tbl_Action) {
	this.tbl_Action = tbl_Action;
}



public HtmlCommandButton getBtnDisburseDone() {
	return btnDisburseDone;
}



public void setBtnDisburseDone(HtmlCommandButton btnDisburseDone) {
	this.btnDisburseDone = btnDisburseDone;
}



public HtmlCommandButton getBtnResubmit() {
	return btnResubmit;
}



public void setBtnResubmit(HtmlCommandButton btnResubmit) {
	this.btnResubmit = btnResubmit;
}




public boolean isShowCheckBox()
{
	if( this.getPageMode().equals( PAGE_MODE_APPROVE ) || this.getPageMode().equals( PAGE_MODE_VIEW ) )
		return false;
	return true;
	
}



@SuppressWarnings( "unchecked" )
public void openPaymentsPopUp(javax.faces.event.ActionEvent event) {
	
	ResearchRecommendationView gridViewRow = (ResearchRecommendationView) dataTable.getRowData();
	String javaScriptText = " var screen_width = 800;"
			+ "var screen_height = 250;"
			+ "var popup_width = screen_width-200;"
			+ "var popup_height = screen_height;"
			+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
			+ "window.open('paymentDetailsPopUp.jsf?pageMode=MODE_SELECT_MANY_POPUP&"
			+ WebConstants.PERSON_ID
			+ "="
			+ gridViewRow.getPerson().getPersonId()			
			+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";

	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	sessionMap.put(RESEARCH_RECOMMENDATION_ROW,gridViewRow);
	sendToParent(javaScriptText);
 
}	
@SuppressWarnings("unused")
public String sendToParent(String javaScript) {
	
	FacesContext facesContext = FacesContext.getCurrentInstance();
	AddResource addResource = AddResourceFactory.getInstance(facesContext);
	addResource.addInlineScriptAtPosition(facesContext,AddResource.HEADER_BEGIN, javaScript);
	return "";
}
@SuppressWarnings("unchecked")
public void obtainChildParams() {

	if (sessionMap.containsKey(RESEARCH_RECOMMENDATION_ROW))
	{
		ResearchRecommendationView parentRow = new ResearchRecommendationView();
		parentRow = (ResearchRecommendationView) sessionMap.get(RESEARCH_RECOMMENDATION_ROW);
		sessionMap.remove(RESEARCH_RECOMMENDATION_ROW);
		viewMap.put(RESEARCH_RECOMMENDATION_ROW,parentRow);
		for (ResearchRecommendationView row : this.getRecommendationsList()) {

			if (parentRow.getResearchRecommendationId().longValue() == row
					.getResearchRecommendationId()) 
			{
				row.setPaymentDetailFormView(parentRow.getPaymentDetailFormView());
				successMessages.add(CommonUtil.getBundleMessage(WebConstants.SocialResearchRecommendations.PAYMENT_INFO_SUCCESS));

				break;
			}

		}
		this.getRecommendationsList();

	}

}
@SuppressWarnings( "unchecked" )
public void openManageBeneficiaryPopUp(javax.faces.event.ActionEvent event) {
	
	ResearchRecommendationView gridViewRow = (ResearchRecommendationView) dataTable.getRowData();
	String javaScriptText = " var screen_width = 970;"
			+ "var screen_height = 550;"
			+ "var popup_width = screen_width;"
			+ "var popup_height = screen_height;"
			+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
			+ "window.open('ManageBeneficiary.jsf?pageMode=MODE_SELECT_ONE_POPUP&"
			+ WebConstants.PERSON_ID
			+ "="
			+ gridViewRow.getPerson().getPersonId()
			+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";

	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	sessionMap.put(RESEARCH_RECOMMENDATION_ROW,gridViewRow);
	sendToParent(javaScriptText);

}
@SuppressWarnings("unchecked")
public void editRecommendation()
{
	ResearchRecommendationView selectedRecommendation = (ResearchRecommendationView)dataTable.getRowData();
	sessionMap.put(WebConstants.PAGE_MODE, WebConstants.InheritanceFilePageMode.PAGE_MODE_IS_EDIT_POPUP);
	sessionMap.put(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW,selectedRecommendation.getMemsFollowup()) ;
	sessionMap.put(WebConstants.MemsFollowupTab.FOLLOW_UP_SINGLE_RECOMMENDATION,selectedRecommendation) ;
	sessionMap.put(WebConstants.InheritanceFile.FILE_ID, this.getResearchView().getInheritanceFileView().getInheritanceFileId());
	//if(!selectedRecommendation.getMemsFollowup().isShowCheckBox())// memsfollowup is null
	if( selectedRecommendation.getStatusKey().equals(WebConstants.ResearchRecommendationStatus.CLOSED ) ||
		selectedRecommendation.getStatusKey().equals(WebConstants.ResearchRecommendationStatus.REJECTED )
	  )
	{
		sessionMap.put(WebConstants.VIEW_MODE, true);
	}
	else
	{
		sessionMap.put(WebConstants.VIEW_MODE, false);
	}
	executeJavascript("openFollowupPopup();");


}

public void updateRecommendation()
{
	if(sessionMap.get(SOCIAL_RECOMM_VIEW_LIST_UPDATED_RECORD)!=null) // means we have a updated record
	{
		ResearchRecommendationView updatedRow = (ResearchRecommendationView)sessionMap.get(SOCIAL_RECOMM_VIEW_LIST_UPDATED_RECORD);
		List<ResearchRecommendationView> view = getRecommendationsList();
		int index=0;
		for (ResearchRecommendationView recommendations : getRecommendationsList()) 
		{
			
			if(recommendations.getResearchRecommendationId().compareTo(updatedRow.getResearchRecommendationId())==0)
				getRecommendationsList().set(index,updatedRow);
			index++;
		}
	}
	
}



public HtmlCommandButton getBtnRejectRecommendation() {
	return btnRejectRecommendation;
}



public void setBtnRejectRecommendation(HtmlCommandButton btnRejectRecommendation) {
	this.btnRejectRecommendation = btnRejectRecommendation;
}

}