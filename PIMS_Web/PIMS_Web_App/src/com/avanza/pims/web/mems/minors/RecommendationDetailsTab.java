package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.SocialResearchService;
import com.avanza.pims.ws.vo.mems.ResearchRecommendationView;

public class RecommendationDetailsTab extends AbstractController


{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5160927008167503230L;

	

	
	private List<ResearchRecommendationView> dataList = new ArrayList<ResearchRecommendationView>();
	private HtmlDataTable dataTable;	
	private transient Logger logger = Logger.getLogger(SocialResearchBean.class);
	/////////////////////////////
	
	private List<String> errorMessages = new ArrayList<String>();
	
	
	private boolean isEnglishLocale = CommonUtil.getIsEnglishLocale();
	private boolean isArabicLocale = CommonUtil.getIsArabicLocale();
	
	
	
	FacesContext context = FacesContext.getCurrentInstance();
	@SuppressWarnings("unchecked")
	Map viewRootMap=context.getViewRoot().getAttributes();
	@SuppressWarnings("unchecked")
	Map session = getFacesContext().getExternalContext().getSessionMap();

	
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}
	@SuppressWarnings( "unchecked" )
	public void init(){
		
		super.init();
		try
		{
			Map viewMap = viewRootMap;
						
			if(!isPostBack())
			{
				
				
			
				if(viewMap.get(WebConstants.PERSON_ID)!=null)
				{
					loadDataList(Long.parseLong(viewMap.get(WebConstants.PERSON_ID).toString()));
					}
				
			
			
				
			}

			



		}
		catch(Exception e){
			logger.LogException("init|Error Occured", e );
		}
//		loadDataList();

	}
	@SuppressWarnings("unchecked")
	public void preprocess()
	{

		super.preprocess();
	

	}


		
	@SuppressWarnings("unchecked")
	public List<ResearchRecommendationView> getRecommendationList()
	{
		Map viewMap = getFacesContext().getViewRoot().getAttributes();
		dataList = (List<ResearchRecommendationView>)viewMap.get("researchRecommendation");
		if(dataList==null)
			dataList = new ArrayList<ResearchRecommendationView>();
		return dataList;
	}

	
	@SuppressWarnings("unchecked")
	private void loadDataList(Long personId) 
	{
		 String methodName="loadDataList";
		 if(dataList != null)
			 dataList.clear();
		 
		 logger.logInfo(methodName+" starts");
		 
		List<ResearchRecommendationView> list = new ArrayList<ResearchRecommendationView>();
		 
		try
		{
			SocialResearchService researchService = new SocialResearchService();
			HashMap<String, String> criteria = new HashMap<String, String>();
			criteria.put(WebConstants.ResearchRecommendationCriteria.PERSON_ID, personId.toString() );
			list=researchService.getResearchRecommendation( criteria );
			
//			
			viewRootMap.put("researchRecommendation", list);
	    }
		catch (Exception ex) 
		{
			logger.LogException(methodName+" crashed", ex);
	    } 
		logger.logInfo(methodName+" finished");		
		
		

	}
	
    
    
	
    @SuppressWarnings("unchecked")
	public void prerender(){
		super.prerender();
		
	}
	
    @SuppressWarnings("unchecked")
    private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
    @SuppressWarnings("unchecked")    
	public String getLocale()
    {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	

	//////////added for showing record size with the paginator
	public Integer getRecordSize(){
		if (dataList != null) {
			return dataList.size();
		}
		else{
			return 0;
		}
	}
	//////////added for showing record size with the paginator
	
	
	public List<ResearchRecommendationView> getDataList() {
		return dataList;
	}
	public void setDataList(List<ResearchRecommendationView> dataList) {
		this.dataList = dataList;
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	

	
    
}
