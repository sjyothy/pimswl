package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.FamilyVillageEducatonAspectService;
import com.avanza.pims.ws.vo.mems.EducationAspectsView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageEducationAspectPopupBean extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	EducationAspectsView pageView;	
	private Long personId;
	private Boolean isDropOut;
	protected HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
	public FamilyVillageEducationAspectPopupBean() 
	{
		pageView = new EducationAspectsView();
		pageView.setCreatedBy (getLoggedInUserId());
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			if(!isPostBack())
			{
				if (request.getParameter("personId") != null) 
				{
					personId = Long.valueOf( request.getParameter("personId").toString() );
					pageView.setPersonId(personId);
				}
			}
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		if(viewMap.get("pageView") != null)
		{
			pageView =  (EducationAspectsView)viewMap.get("pageView");
		}
		if(viewMap.get("isDropOut") != null)
		{
			isDropOut =  (Boolean)viewMap.get("isDropOut");
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
		if(pageView != null)
		{
			viewMap.put("pageView",pageView);
		}
		if(isDropOut!= null)
		{
			viewMap.put("isDropOut",isDropOut);
		}
		
	}	
	
	@SuppressWarnings("unchecked")
	private boolean hasErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		
//		if( pageView.getDiseaseTypeId() == null || pageView.getDiseaseTypeId().equals("-1")) {
//			errorMessages.add(CommonUtil.getBundleMessage("researchFamilyVillageBeneficiary.msg.EducationAspect.ChronicDiseaseRequired"));
//			return true;
//		}
		
		return hasSaveErrors;
		
	}

	
	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
		
			if(hasErrors())return;
			
			pageView.setIsDropOut( isDropOut?"1":"0" );
			pageView.setUpdatedBy (getLoggedInUserId());
			FamilyVillageEducatonAspectService.addEducationAspects( pageView );
			successMessages.add( ResourceUtil.getInstance().getProperty( "commons.savedSuccessfully" ) );
		}
		catch ( Exception e )
		{
			logger.LogException( "onSave--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public EducationAspectsView getPageView() {
		return pageView;
	}

	public void setPageView(EducationAspectsView pageView) {
		this.pageView = pageView;
	}

	public Boolean getIsDropOut() {
		return isDropOut;
	}

	public void setIsDropOut(Boolean isDropOut) {
		this.isDropOut = isDropOut;
	}


}
