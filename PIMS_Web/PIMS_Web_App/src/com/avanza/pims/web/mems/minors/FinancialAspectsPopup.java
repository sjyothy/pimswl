package com.avanza.pims.web.mems.minors;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.ResearchFormBeneficiaryService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.AssetWiseRevenueDetailsView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.ResearchFormBeneficiaryView;
import com.avanza.pims.ws.vo.mems.AnnualIncomeView;
import com.avanza.pims.ws.vo.mems.MonthlyExpenseView;
import com.avanza.pims.ws.vo.mems.MonthlyIncomeView;
import com.avanza.ui.util.ResourceUtil;


@SuppressWarnings( "unchecked" )
public class FinancialAspectsPopup extends AbstractMemsBean 
{
	private static final long serialVersionUID = 9174468029380277403L;
	HttpServletRequest request;
	private Long inheritanceFileId;
	private long personId;
	private ResearchFormBeneficiaryService service;
	private UtilityService us;
	private Boolean isViewModePopUp;
	ResearchFormBeneficiaryView view;
	MonthlyIncomeView mivu;
	List<MonthlyIncomeView> mivuList;
	MonthlyExpenseView mevu;
	List<MonthlyExpenseView> mevuList;
	AnnualIncomeView aivu;
	List<AnnualIncomeView> aivuList;
	private boolean openedForOwner;
	protected HtmlTabPanel tabPanel;
	private static final String IS_OPENED_FOR_OWNER = "IS_OPENED_FOR_OWNER";
	private static final String TAB_FINANCIAL_ASPECTS = "tabFinancialAspects";
	private static final String INCOME_CATEGORY_LIST = "incomeCategoryList";
	private static final String VIEW = "researchFormBeneficiaryView";
	private static final String SCREEN_VIEW_MODE_POPUP="pageMode";
	Double monthlyRevenue;
	Double annualRevenue ;
    List<AssetWiseRevenueDetailsView> dataList;
    private HtmlDataTable dataTable;
	
	private List<SelectItem>  incomeCategoryList; 
	public FinancialAspectsPopup() 
	{		
		request = ( HttpServletRequest ) this.getFacesContext().getExternalContext().getRequest();
		incomeCategoryList = new ArrayList<SelectItem>(0);
		view = new ResearchFormBeneficiaryView();
		
		monthlyRevenue = 0.00d;
		annualRevenue = 0.00d;
		dataList = new ArrayList<AssetWiseRevenueDetailsView>();
		dataTable = new org.apache.myfaces.component.html.ext.HtmlDataTable();
		
		mivu = new MonthlyIncomeView();
		mivuList = new ArrayList<MonthlyIncomeView>();
		mivuList.add(mivu);//adding empty element to list
		
		mevu = new MonthlyExpenseView();
		mevuList = new ArrayList<MonthlyExpenseView>();
		mevuList.add(mevu);//adding empty element to list
		
		aivu = new AnnualIncomeView();
		aivuList = new ArrayList<AnnualIncomeView>();
		aivuList.add(aivu);//adding empty element to list
		
		view.setLastMonthlyIncome(mivuList);
		view.setLastMonthlyExpense(mevuList);
		view.setLastAnnualIncome(aivuList);
		
		tabPanel = new HtmlTabPanel();
		service =  new ResearchFormBeneficiaryService();
		us = new UtilityService();
	}
	@Override
	public void init() 
	{
		errorMessages = new ArrayList<String>(0);
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException( " init()-- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception,PimsBusinessException
	{
		if( !isPostBack() )
		{
			if(sessionMap.get(WebConstants.PERSON_TYPE) != null)
			{
				if(sessionMap.get(WebConstants.PERSON_TYPE).toString().compareTo(WebConstants.PERSON_TYPE_OWNER) == 0)
				{
					sessionMap.remove(WebConstants.PERSON_TYPE);
					viewMap.put(IS_OPENED_FOR_OWNER, true);
				}
			}
			
			if (request.getParameter(SCREEN_VIEW_MODE_POPUP) != null) 
			{
				isViewModePopUp = true;
			}
			else
			{
				isViewModePopUp = false;
			}
			if (request.getParameter(WebConstants.PERSON_ID) != null) 
			{
				this.personId = Long.valueOf( request.getParameter(WebConstants.PERSON_ID).toString() );
			}
			else if (sessionMap.get(WebConstants.PERSON_ID) != null) //beneficiaryID 
			{
				personId = Long.valueOf( sessionMap.get(WebConstants.PERSON_ID).toString() );
				sessionMap.remove(WebConstants.PERSON_ID);
			}
			
			if (sessionMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null) 
			{
				inheritanceFileId  = Long.valueOf( sessionMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID ).toString());
				sessionMap.remove(WebConstants.InheritanceFile.INHERITANCE_FILE_ID);
				view.setInhFileId(inheritanceFileId);
			}
			view.setPersonId(personId);
            view.setLoggedInUser( CommonUtil.getLoggedInUser() );
			view = service.getResearchFormData(view, personId);
			getAssetWiseReveuesForPersonId(personId);
			loadLists();
		}
		if( viewMap.get( WebConstants.PERSON_ID  ) != null )
		{
		    personId = Long.valueOf( viewMap.get( WebConstants.PERSON_ID ).toString() );
		}
		if( viewMap.get( SCREEN_VIEW_MODE_POPUP )!= null )
		{
		    isViewModePopUp = (Boolean)viewMap.get( SCREEN_VIEW_MODE_POPUP  );
		}
		if( viewMap.get( VIEW )!= null )
		{
		    view = (ResearchFormBeneficiaryView)viewMap.get( VIEW );
		}
		
		if( viewMap.get( INCOME_CATEGORY_LIST ) != null )
		{
		    incomeCategoryList   = (List<SelectItem>)viewMap.get( INCOME_CATEGORY_LIST );
		}
		
		if( viewMap.get("dataList" ) != null )
	    {
	    	dataList = (  ArrayList<AssetWiseRevenueDetailsView> ) viewMap.get( "dataList" );
	    }

	    if(  viewMap.get( "annualRevenue" ) != null )
	    {
	    	annualRevenue =  (Double)viewMap.get( "annualRevenue" );
	    }
	    if( viewMap.get( "monthlyRevenue" ) != null )
	    {
	    	monthlyRevenue  = (Double)viewMap.get( "monthlyRevenue");
	    }
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		
		if( personId > 0 )
		{
		  viewMap.put(WebConstants.PERSON_ID, personId );
		}
		
	    if( view != null )
	    {
	    	viewMap.put(VIEW, view);
	    }
	    if( dataList != null )
	    {
	    	viewMap.put("dataList", dataList);
	    }
	    if( isViewModePopUp != null && isViewModePopUp )
	    {
	    	viewMap.put( SCREEN_VIEW_MODE_POPUP,true);
	    }
	    else
	    {
	    	viewMap.put( SCREEN_VIEW_MODE_POPUP,false);
	    }
	   
	    if(  annualRevenue != null )
	    {
	    	viewMap.put( "annualRevenue" , annualRevenue );
	    }
	    if( monthlyRevenue != null )
	    {
	    	viewMap.put( "monthlyRevenue", monthlyRevenue  );
	    }
	}
	
	
	@SuppressWarnings("unchecked")
	private void getAssetWiseReveuesForPersonId(Long personId)throws Exception
	{
		dataList = InheritanceFileService.getAssetWiseReveuesForPersonId( personId );
		for (AssetWiseRevenueDetailsView obj : dataList) 
		{
		  monthlyRevenue += obj.getMonthlyRevenue();	
		  annualRevenue  += obj.getAnnualRevenue();
		}
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		monthlyRevenue  = Double.valueOf(oneDForm.format(monthlyRevenue));
		annualRevenue  = Double.valueOf(oneDForm.format(annualRevenue));
	}
	
	private boolean hasError() throws Exception 
	{
		boolean hasError = false;
		
		if( hasFinancialAspectError())
		{
			tabPanel.setSelectedTab( TAB_FINANCIAL_ASPECTS );
			hasError = true;
		}
		return hasError;
	}
	 
	private boolean isEmptyMonthlyIncome()throws Exception
	{
		if (view.getLastMonthlyIncome() == null || view.getLastMonthlyIncome().size() <= 0 )
		{
			return true;
		}
		else if (view.getLastMonthlyIncome() != null && view.getLastMonthlyIncome().size()> 0 )
		{
			MonthlyIncomeView lmiv =  view.getLastMonthlyIncome().get(0);
			if(		
				StringHelper.isEmpty(lmiv.getIncomeSource()) 
				&& StringHelper.isEmpty(lmiv.getOthers())
				&&	StringHelper.isEmpty(lmiv.getPension())
				&&	StringHelper.isEmpty(lmiv.getRealEstate())
				&&	StringHelper.isEmpty(lmiv.getSocSecurity())
				&&	StringHelper.isEmpty(lmiv.getTotal())
				&&	StringHelper.isEmpty(lmiv.getTransport())
			)
				return true;

		}
		return false;
		
		
	}
	private boolean isEmptyAnnualIncome()throws Exception
	{
		if (view.getLastAnnualIncome() == null || view.getLastAnnualIncome().size() <= 0 ){
			
			return true;
		}
		else if (view.getLastAnnualIncome() != null && view.getLastAnnualIncome().size()> 0 ){
			AnnualIncomeView laiv =  view.getLastAnnualIncome().get(0);
			if(		
				StringHelper.isEmpty(laiv.getIncomeSource()) 
				&& StringHelper.isEmpty(laiv.getOthers())
				&&	StringHelper.isEmpty(laiv.getLands())
				&&	StringHelper.isEmpty(laiv.getRealEstate())
				&&	StringHelper.isEmpty(laiv.getShares())
				&&	StringHelper.isEmpty(laiv.getTotal())
				&&	StringHelper.isEmpty(laiv.getTradeLicenses())
			)
				return true;
		}
		return false;
		
		
	}

	private boolean isEmptyMonthlyExpenses()throws Exception 
	{
		
		//check third row 
		if (view.getLastMonthlyExpense() == null || view.getLastMonthlyExpense().size() <= 0 ){
			return true;
		}
		else if (view.getLastMonthlyExpense() != null && view.getLastMonthlyExpense().size()> 0 ){
			MonthlyExpenseView lmev =  view.getLastMonthlyExpense().get(0);
			if(		
				StringHelper.isEmpty(lmev.getClothes()) 
				&& StringHelper.isEmpty(lmev.getElectricity())
				&&	StringHelper.isEmpty(lmev.getEntertainment())
				&&	StringHelper.isEmpty(lmev.getFood())
				&&	StringHelper.isEmpty(lmev.getMobile())
				&&	StringHelper.isEmpty(lmev.getPersonalExp())
				&&	StringHelper.isEmpty(lmev.getPetrol())
				&&	StringHelper.isEmpty(lmev.getServant())
				&&	StringHelper.isEmpty(lmev.getStationary())
				&&	StringHelper.isEmpty(lmev.getTelephone())
				&&	StringHelper.isEmpty(lmev.getTotal())
			)
				return true;
		}
		return false; 
	}

	private boolean hasFinancialAspectError()throws Exception
	{
		boolean hasErrors=false;
		return hasErrors;
	}
	public void onOpenAssetWiseRevenueDetailsPopup()
	{
		errorMessages  = new ArrayList<String>(0);
		successMessages= new ArrayList<String>(0);
		try
		{
			updateValuesFromMaps();
			AssetWiseRevenueDetailsView  row = (AssetWiseRevenueDetailsView)dataTable.getRowData();
			sessionMap.put("assetTypeId", row.getAssetTypeId() );
			sessionMap.put(WebConstants.PERSON_ID, view.getPersonId() );
			sessionMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_ID , view.getInhFileId() );
			executeJavaScript("javaScript:openAssetWiseRevenueDetailsPopup();");
	
		}
		catch(Exception e)
		{
			logger.LogException("onOpenAssetWiseRevenueDetailsPopup|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public void onSave()
	{
		errorMessages  = new ArrayList<String>(0);
		successMessages= new ArrayList<String>(0);
		try
		{
			updateValuesFromMaps();
			if( !hasError() )
			{
				
				if( isEmptyMonthlyIncome()  )
				{
					view.setSaveMonthlyIncome(false);
				}
				if(isEmptyMonthlyExpenses() )
				{
					view.setSaveMonthlyExpense( false);
				}
				if(isEmptyAnnualIncome()  )
				{
					view.setSaveAnnualIncome( false);
				}
				
				service.saveResearchForm( view );
				view = service.getResearchFormData(view, personId);
				successMessages.add(ResourceUtil.getInstance().getProperty("common.messages.Save"));
				view.setLoggedInUser(CommonUtil.getLoggedInUser());
				view.setSaveMonthlyIncome(true);
				view.setSaveMonthlyExpense( true);
				view.setSaveAnnualIncome( true );
				
			}

				updateValuesToMaps();
		}
		catch(Exception e)
		{
			logger.LogException("onSave|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void loadLists() throws PimsBusinessException,Exception 
	{
		loadIncomeCategory(); 
	}


	private void loadIncomeCategory() throws PimsBusinessException
	{

		List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<DomainDataView> domainData = service.getIncomeCategoryList();
		for (DomainDataView domainDataView : domainData) 
		{
			SelectItem item = new SelectItem();
			item.setValue(domainDataView.getDomainDataId().toString());
			if (CommonUtil.getIsEnglishLocale())
				item.setLabel(domainDataView.getDataDescEn());
			else
				item.setLabel(domainDataView.getDataDescAr());
			selectItem.add(item);
		}
		Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		this.setIncomeCategoryList(selectItem);

	}
	public List<SelectItem> getIncomeCategoryList() {
		return incomeCategoryList;
	}

	public void setIncomeCategoryList(List<SelectItem> incomeCategoryList) {
		this.incomeCategoryList = incomeCategoryList;
		if ( this.incomeCategoryList != null)
		{
			viewMap.put(INCOME_CATEGORY_LIST,this.incomeCategoryList );
		}
		
	}
	public ResearchFormBeneficiaryView getView() {
		return view;
	}

	public void setView(ResearchFormBeneficiaryView view) {
		this.view = view;
	}
	public Boolean getIsViewModePopUp() {
		return isViewModePopUp;
	}
	public void setIsViewModePopUp(Boolean isViewModePopUp) {
		this.isViewModePopUp = isViewModePopUp;
	}
	public Long getInheritanceFileId() {
		return inheritanceFileId;
	}
	public void setInheritanceFileId(Long inheritanceFileId) {
		this.inheritanceFileId = inheritanceFileId;
	}
	
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}
	public boolean isOpenedForOwner() {
		if(viewMap.get(IS_OPENED_FOR_OWNER) != null){
			return (Boolean) viewMap.get(IS_OPENED_FOR_OWNER);
		}
		return false;
	}
	public void setOpenedForOwner(boolean openedForOwner) {
		this.openedForOwner = openedForOwner;
	}
	public Double getMonthlyRevenue() {
		return monthlyRevenue;
	}
	public void setMonthlyRevenue(Double monthlyRevenue) {
		this.monthlyRevenue = monthlyRevenue;
	}
	public Double getAnnualRevenue() {
		return annualRevenue;
	}
	public void setAnnualRevenue(Double annualRevenue) {
		this.annualRevenue = annualRevenue;
	}
	public List<AssetWiseRevenueDetailsView> getDataList() {
		return dataList;
	}
	public void setDataList(List<AssetWiseRevenueDetailsView> dataList) {
		this.dataList = dataList;
	}
	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

}
