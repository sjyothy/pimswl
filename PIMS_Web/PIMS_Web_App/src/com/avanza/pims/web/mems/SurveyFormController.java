package com.avanza.pims.web.mems;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.SurveyType;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.ws.mems.FamilyVillageFileService;
import com.avanza.pims.ws.services.SurveyService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.SurveyInstanceView;
import com.avanza.ui.util.ResourceUtil;

public class SurveyFormController extends AbstractMemsBean {
	private static final long serialVersionUID = -4983101685930099314L;
	private PersonView beneficiaryPerson = new PersonView();
	SurveyInstanceView pageView;
	private HtmlDataTable dataTable;
	private Long personId;
	private String title;
	private Long surveyTypeId;
	private Long surveyInstanceId;
	FamilyVillageFileService fvService = new FamilyVillageFileService();
	protected HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
	public SurveyFormController() {
		pageView = new SurveyInstanceView();

	}

	@Override
	@SuppressWarnings("unchecked")
	public void init() {
		try {
			if (!isPostBack()) {
				
				if (request.getParameter("personId") != null) {
					personId = Long.valueOf(request.getParameter("personId").toString());
					beneficiaryPerson = fvService.getBeneficiaryPersonById(personId);
				}
				if (request.getParameter("surveyTypeId") != null) {
					surveyTypeId = Long.valueOf(request.getParameter("surveyTypeId").toString());
					SurveyType surveyType = EntityManager.getBroker().findById(SurveyType.class, surveyTypeId );
					title = isEnglishLocale()? surveyType.getTypeEn():surveyType.getTypeAr();
				}
				if (request.getParameter("surveyInstanceId") != null) {
					surveyInstanceId = Long.valueOf(request.getParameter("surveyInstanceId").toString());
					pageView.setSurveyInstanceId(surveyInstanceId);
				}					
				
				pageView = SurveyService.getSurvey(surveyTypeId, personId, surveyInstanceId);
				if(pageView.getSurveyInstanceId()== null){
					pageView.setCreatedBy(getLoggedInUserId());
					pageView.setUpdatedBy(getLoggedInUserId());
					
				}
				pageView.setIsDeleted(0l);
				pageView.setPersonId(personId);
				pageView.setSurveyTypeId(surveyTypeId);

			}
			updateValuesFromMaps();
		} catch (Exception exception) {
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		} 
	}

	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception {
		
		if (viewMap.get(WebConstants.PERSON_ID) != null) {
			personId = Long.valueOf(viewMap.get(WebConstants.PERSON_ID).toString());
		}
		if (viewMap.get("title") != null) {
			title = viewMap.get("title").toString();
		}
		if (viewMap.get("surveyTypeId") != null) {
			surveyTypeId = Long.valueOf(viewMap.get("surveyTypeId").toString());
		}
		if (viewMap.get("surveyInstanceId") != null) {
			surveyInstanceId = Long.valueOf(viewMap.get("surveyInstanceId").toString());
		}
		if (viewMap.get("pageView") != null) {
			pageView = (SurveyInstanceView) viewMap.get("pageView");
		}

		updateValuesToMaps();
	}

	@SuppressWarnings("unchecked")
	private void updateValuesToMaps() {
		if (personId != null) {
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
		if (title  != null) {
			viewMap.put("title",title);
		}
		if (surveyTypeId != null) {
			viewMap.put("surveyTypeId", surveyTypeId);

		}
		if ("pageView" != null) {
			viewMap.put("pageView", pageView);
		}
	}

	@SuppressWarnings("unchecked")
	public void onSave() throws Exception {
		
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			updateValuesFromMaps();
			SurveyService.persistSurveyInstance(pageView);
			successMessages.add( ResourceUtil.getInstance().getProperty( "commons.savedSuccessfully" ) );
			updateValuesToMaps();
		}
		catch ( Exception e )
		{
			logger.LogException( "onSave--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
		
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getSurveyTypeId() {
		return surveyTypeId;
	}

	public void setSurveyTypeId(Long surveyTypeId) {
		this.surveyTypeId = surveyTypeId;
	}

	public SurveyInstanceView getPageView() {
		return pageView;
	}

	public void setPageView(SurveyInstanceView pageView) {
		this.pageView = pageView;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public Long getSurveyInstanceId() {
		return surveyInstanceId;
	}

	public void setSurveyInstanceId(Long surveyInstanceId) {
		this.surveyInstanceId = surveyInstanceId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public PersonView getBeneficiaryPerson() {
		return beneficiaryPerson;
	}

	public void setBeneficiaryPerson(PersonView beneficiaryPerson) {
		this.beneficiaryPerson = beneficiaryPerson;
	}


}
