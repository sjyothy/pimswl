package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.ProblemAspectsService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.ProblemAspectView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageProblemAspectTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private List< ProblemAspectView> dataListProblemAspect;
	private HtmlDataTable dataTableProblemAspect;
	
	private String tabMode;
	private Long personId;
	private Integer recordSizeProblemAspect;
	public FamilyVillageProblemAspectTabBacking() 
	{
		dataTableProblemAspect             =  new HtmlDataTable();
		dataListProblemAspect		        =  new ArrayList<ProblemAspectView>();
		recordSizeProblemAspect  		=  0;
		tabMode          = Constant.EndowmentFile.MODE_READ_ONLY;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.PROBLEM_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.PROBLEM_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			tabMode = (String) viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		if( viewMap.get( "dataListProblemAspect" ) != null )
		{
			dataListProblemAspect = (ArrayList<ProblemAspectView>)viewMap.get( "dataListProblemAspect" );
		}
				
		if( viewMap.get( "recordSizeProblemAspect" ) != null )
		{
			recordSizeProblemAspect = Integer.parseInt( viewMap.get( "recordSizeProblemAspect" ).toString() );
		}
		
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, tabMode );
		}
		if(recordSizeProblemAspect !=null)
		{
			viewMap.put("recordSizeProblemAspect",recordSizeProblemAspect);
		}
		if( dataListProblemAspect != null && dataListProblemAspect.size()  > 0  )
		{
			viewMap.put( "dataListProblemAspect", dataListProblemAspect );
		}
		
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
	}	
	
	

	public void populateTab( PersonView personView ) throws Exception
	{
		updateValuesFromMaps();
		personId = personView.getPersonId();
		setPageDataDefaults();
		loaddataListProblemAspect( personId );
		
		updateValuesToMaps();
	}
	
	private void setPageDataDefaults()throws Exception
	{
		PersonView person  = new PersonView();
		person.setPersonId(personId);
	}

	private void loaddataListProblemAspect(Long personId) throws Exception {
		
		dataListProblemAspect= ProblemAspectsService.getProblemAspects( personId );
		recordSizeProblemAspect=dataListProblemAspect.size();
	}

	
	@SuppressWarnings( "unchecked" )
	public void onDeleteProblemAspect()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteProblemAspect--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.PROBLEM_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.PROBLEM_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
	}
	
	
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}
	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public List<ProblemAspectView> getDataListProblemAspect() {
		return dataListProblemAspect;
	}

	public void setDataListProblemAspect(
			List<ProblemAspectView> dataListProblemAspect) {
		this.dataListProblemAspect = dataListProblemAspect;
	}

	public HtmlDataTable getDataTableProblemAspect() {
		return dataTableProblemAspect;
	}

	public void setDataTableProblemAspect(
			HtmlDataTable dataTableProblemAspect) {
		this.dataTableProblemAspect = dataTableProblemAspect;
	}

	public Integer getRecordSizeProblemAspect() {
		return recordSizeProblemAspect;
	}

	public void setRecordSizeProblemAspect(
			Integer recordSizeProblemAspect) {
		this.recordSizeProblemAspect = recordSizeProblemAspect;
	}

}
