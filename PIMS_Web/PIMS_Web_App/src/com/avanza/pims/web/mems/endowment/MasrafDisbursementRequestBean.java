package com.avanza.pims.web.mems.endowment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlInputText;
import javax.servlet.ServletContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.Utils.generatorViews.MasarifSearchView;
import com.avanza.pims.bpel.proxy.MEMSEndowmentDisbursementBPELPortClient;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.Masraf;
import com.avanza.pims.entity.PaymentMethod;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationDetails;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.MasarifService;
import com.avanza.pims.ws.mems.endowment.EndowmentDisbursementService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.PaymentDetailsView;
import com.avanza.ui.util.ResourceUtil;

public class MasrafDisbursementRequestBean extends AbstractMemsBean
{
	private static final long serialVersionUID = 1L;
	private transient Logger logger = Logger.getLogger(MasrafDisbursementRequestBean.class);
	
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_RESUBMITTED = "PAGE_MODE_RESUBMITTED";
	private static final String PAGE_MODE_APPROVAL = "PAGE_MODE_APPROVAL";
	private static final String PAGE_MODE_DISBURSEMENT_REQUIRED = "PAGE_MODE_DISBURSEMENT_REQUIRED";
	
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_DISBURSMENT_DETAILS   = "disbursementsTab";
	private static final String TAB_APPLICANTS   = "applicationTab";
    
	DecimalFormat oneDForm = new DecimalFormat("#.00");
    private String pageTitle;
    private String pageMode;
    private String hdnPersonId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;
    private String txtRemarks;
    private RequestService requestService = new RequestService();
    private EndowmentDisbursementService endDisService = new EndowmentDisbursementService(); 
	private Masraf masrafForDisplay;
	private RequestView requestView ;
	private List<DisbursementDetailsView> dataList=  new ArrayList<DisbursementDetailsView>();
	private DisbursementDetailsView disbursementDetail ;
    protected HtmlTabPanel tabPanel = new HtmlTabPanel();
    private HtmlInputText accountBalance = new HtmlInputText();
    private HtmlInputText requestedAmount= new HtmlInputText();
    private HtmlInputText availableBalance= new HtmlInputText();
    private String totalDisbursementsAdded="0.00";
    private HtmlDataTable dataTable;    
	public MasrafDisbursementRequestBean(){}
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.EndowmentDisbursement.PROCEDURE);
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.EndowmentDisbursement.PROCEDURE);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		viewMap.put("noteowner", WebConstants.REQUEST);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		loadAttachmentsAndComments( null );
		
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
			getDataFromTaskList();
		}
		else if ( getRequestMap().get( WebConstants.REQUEST_VIEW) != null )
		{
			getDataFromSearch();
		}
		else 
		{
			setDataForFirstTime();
		}
		getBalanceDetailsForMasraf( requestView.getMasrafId(),null );
		updateValuesFromMap();
		getPageModeFromRequestStatus();
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		requestView = new RequestView();
		requestView.setCreatedOn(new Date());
		requestView.setCreatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedOn(new Date());
		requestView.setStatusId( Constant.REQUEST_STATUS_NEW_ID );
		requestView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		requestView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
		requestView.setRequestDate( new Date() );
		requestView.setRequestTypeId( WebConstants.MemsRequestType.MASRAF_DISBURSEMENT);
		if( sessionMap.get(WebConstants.Masraf.MasrafObject) !=null )
		{
			MasarifSearchView masraf =  (MasarifSearchView )sessionMap.remove(WebConstants.Masraf.MasrafObject);
			requestView.setMasrafId( masraf.getMasrafId() );
			populateMasrafDetails(masraf.getMasrafId() );
		}
		
		getNewDisbursementDetailObject();
	}

	/**
	 * @param masraf
	 */
	@SuppressWarnings("unchecked")
	private void populateMasrafDetails(Long  masrafId) throws Exception
	{
		Masraf retrievedMasraf = EntityManager.getBroker().findById(Masraf.class, masrafId );
		masrafForDisplay= new Masraf();
		masrafForDisplay.setMasrafId(retrievedMasraf.getMasrafId());
		
		masrafForDisplay.setMasrafName(retrievedMasraf.getMasrafName());
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void getNewDisbursementDetailObject() throws Exception 
	{
		disbursementDetail = new DisbursementDetailsView();
		disbursementDetail.setCreatedOn(new Date());
		disbursementDetail.setCreatedBy(  getLoggedInUserId()  );
		disbursementDetail.setUpdatedBy(  getLoggedInUserId()  );
		disbursementDetail.setUpdatedOn(new Date());
		disbursementDetail.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		disbursementDetail.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
		disbursementDetail.setSrcType( WebConstants.PaymentSrcType.SELF_ACCOUNT_ID );
		disbursementDetail.setStatus( WebConstants.DisbursementStatus.NEW_ID );
		disbursementDetail.setHasInstallment(0l);
		disbursementDetail.setPaymentType( WebConstants.MemsPaymentTypes.DISBURSEMENTS_ID );
		disbursementDetail.setIsDisbursed(0l);
		disbursementDetail.setMasrafId(requestView.getMasrafId());
		
		PaymentDetailsView pd= new PaymentDetailsView();
		pd.setIsDeleted(Constant.DEFAULT_IS_DELETED);
		
		List<PaymentDetailsView> pdList= new ArrayList<PaymentDetailsView>();
		pdList.add( pd );
		disbursementDetail.setPaymentDetails(  pdList );
	}

	
	@SuppressWarnings("unchecked")
	public void onOpenRequestDisbursementDetailsForMasrafPopup()
	{
		try
		{
			updateValuesFromMap();
			if( requestView.getMasrafId() == null || requestView.getMasrafId()  == null ) return;
			List<Long> disDetIdNotIn = loopDataList();
			if( disDetIdNotIn.size() > 0 )
				sessionMap.put("disDetIdNotIn", disDetIdNotIn);
			String reqId = requestView.getRequestId()!= null ? requestView.getRequestId().toString():"-1";
			executeJavaScript( "javaScript:openRequestDisbursementDetailsForMasrafPopup("+requestView.getMasrafId()+","+ reqId +");");
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenStatementOfAccountCriteriaPopup|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	@SuppressWarnings("unchecked")
	public void onOpenEndowemntSearchPopup()
	{
		try
		{
			updateValuesFromMap();
			if( requestView.getMasrafId() == null || requestView.getMasrafId()  == null ) return;
			sessionMap.put( "MasrafId", requestView.getMasrafId() );
			executeJavaScript( "javaScript:openSearchEndowmentsPopup('masrafDisbursement');");
		}
		catch(Exception ex)
		{
			logger.LogException("onEndowemntSearchPopup|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<Long> loopDataList()throws Exception 
	{
		List<Long> disDetIdNotIn = new ArrayList<Long>();
		Double sumDisbursements = 0.0d;
		for (DisbursementDetailsView obj: dataList) 
		{
		  if( obj.getDisDetId() != null )
		  {
			  disDetIdNotIn.add(obj.getDisDetId());
		  }
		  sumDisbursements += obj.getAmount();
		}
		setTotalDisbursementsAdded( oneDForm.format( sumDisbursements ) );
		return disDetIdNotIn;
	}
	
	@SuppressWarnings("unchecked")
	public void onOpenStatementOfAccountCriteriaPopup()
	{
		try
		{
			updateValuesFromMap();
			if( requestView.getMasrafId() == null || requestView.getMasrafId()  == null ) return;
			MasarifSearchView masrafView = new MasarifSearchView();
			masrafView.setMasrafId( requestView.getMasrafId()  );
			sessionMap.put(WebConstants.Masraf.MasrafStatement, masrafView);
			executeJavaScript( "openStatementofAccountMasrafCriteriaPopup();");
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenStatementOfAccountCriteriaPopup|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	
	@SuppressWarnings("unchecked")
	private void getBalanceDetailsForMasraf( Long masrafId, Long endowmentId ) throws Exception
	{
		Double amount = 0.0d;
		if(  endowmentId == null )
		{
			amount =  MasarifService.getMasrafBalance(masrafId, "0");
		}
		else
		{
			amount =  EndowmentDisbursementService.getBalanceForMasrafAndEndowmentAmount(masrafId, endowmentId);
		}
		String formattedAmount = oneDForm.format( amount ) ;
		accountBalance.setValue( formattedAmount.equals(".00")?"0.00":formattedAmount );
		//	Long requestIdNot = requestView != null && requestView.getRequestId() != null ?requestView.getRequestId():-1;
		//	List<Long> disDetIdNotIn = loopDataList();
		Double reqAmount = EndowmentDisbursementService.getRequestDisbursementsForMasrafAmount(masrafId, null,null);//requestIdNot, disDetIdNotIn);
		String formattedReqAmount = oneDForm.format( reqAmount );  
		requestedAmount.setValue(formattedReqAmount.equals(".00")?"0.00":formattedReqAmount );
		
		calculateAvailableBalance(amount, reqAmount);
	}

	/**
	 * @param amount
	 * @param reqAmount
	 */
	private void calculateAvailableBalance(Double amount, Double reqAmount)throws Exception 
	{
		availableBalance.setValue(   oneDForm.format( amount - reqAmount ) );
	}
	
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromRequestStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		tabPanel.setSelectedTab(TAB_APPLICANTS);
		if( this.requestView == null  || 
			this.requestView.getRequestId() == null ||
			this.requestView.getStatusId() == null 
		  ) 
		{ return; }
		if( getStatusDisbursementRequired() )
		{
			setPageMode( PAGE_MODE_DISBURSEMENT_REQUIRED);
			tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS );
		}
		else if( getStatusApproval() )
		{
			setPageMode( PAGE_MODE_APPROVAL );
			tabPanel.setSelectedTab( TAB_DISBURSMENT_DETAILS);
		}
		else if(  getStatusRejectedResubmitted() )
		{
			setPageMode(PAGE_MODE_RESUBMITTED);
			tabPanel.setSelectedTab("commentsTab" );
			
		}
		else if (getStatusCompleted())
		{
			
			setPageMode(PAGE_MODE_VIEW);
			tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS );
		}	
			
	}
	public boolean getStatusNew() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_NEW ) == 0;
	}
	public boolean getStatusApproval() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED ) == 0;
	}
	public boolean getStatusDisbursementRequired() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ ) == 0;
	}
	public boolean getStatusRejectedResubmitted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT) == 0;
	}
	public boolean getStatusCompleted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_COMPLETE ) == 0;
	}

	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = ( RequestView )viewMap.get( WebConstants.REQUEST_VIEW ) ;
			requestView.setUpdatedBy(getLoggedInUserId());
		}
		if( viewMap.get( "dataList" ) != null )
		{
			dataList = ( ArrayList<DisbursementDetailsView>)viewMap.get( "dataList"  ) ;
		}
		if( viewMap.get("disbursementDetail") != null )
		{
			disbursementDetail  = (DisbursementDetailsView)viewMap.get("disbursementDetail");
		}
		if( viewMap.get("totalDisbursementsAdded") != null)
		{
			totalDisbursementsAdded = viewMap.get("totalDisbursementsAdded").toString();
		}
		if( viewMap.get("masrafForDisplay") != null )
		{
			masrafForDisplay = (Masraf)viewMap.get("masrafForDisplay");
		}
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( requestView != null )
		{
			viewMap.put(  WebConstants.REQUEST_VIEW , requestView);
		}
		if( dataList != null )
		{
			viewMap.put("dataList", dataList);
		}
		if(disbursementDetail != null )
		{
			viewMap.put("disbursementDetail",disbursementDetail);
		}
		
		if(totalDisbursementsAdded != null )
		{
			viewMap.put("totalDisbursementsAdded",totalDisbursementsAdded);
		}
		if( masrafForDisplay != null && masrafForDisplay.getMasrafId() != null )
		{
			viewMap.put("masrafForDisplay",masrafForDisplay);
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		requestView = ( RequestView)getRequestMap().get( WebConstants.REQUEST_VIEW) ;
		if( this.requestView !=null )
		{
		  getRequestDetails( requestView.getRequestId() );
		}
	}
	
	@SuppressWarnings(  "unchecked"  )
	private void populateApplicationDetailsTab()throws Exception
	{
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		String status = "";
		boolean isRequestNull =  null == requestView || null == requestView.getApplicantView() ||null == requestView.getRequestId();
		if ( isRequestNull && hdnPersonName!= null && hdnPersonName.trim().length() >0 ) 
		{
			bean.populateApplicationDetails(
					"", 
						                    "", 
						                    new Date(), 
						                    "", 
						                    hdnPersonName, 
						                    "", 
						                    hdnCellNo, 
						                    ""
						                  );
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, hdnPersonId);
			PersonView person = new PersonView();
			person.setPersonId( new Long( hdnPersonId ) );
			requestView.setApplicantView( person);
			
            return;
		}
		
		status = CommonUtil.getIsEnglishLocale()?requestView.getStatusEn():requestView.getStatusAr();
		bean.populateApplicationDetails(
				                         requestView.getRequestNumber(), 
					                     status, 
					                     requestView.getRequestDate(), 
					                     requestView.getDescription(), 
					                     requestView.getApplicantView().getPersonFullName(), 
					                     "", 
					                     requestView.getApplicantView().getCellNumber(), 
					                     requestView.getApplicantView().getEmail()
					                   );
		if( requestView.getApplicantView() != null && requestView.getApplicantView().getPersonId() != null  )
		{
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,requestView.getApplicantView().getPersonId() );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, requestView.getApplicantView().getPersonId() );
		}
		if ( requestView!= null && requestView.getStatus() != null && 
			!requestView.getStatusDataValue().equals( WebConstants.REQUEST_STATUS_NEW ) 
		    )
		{
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}
	}
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  UserTask userTask = ( UserTask )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  setUserTask(userTask);
	  if( userTask.getTaskAttributes().get(  WebConstants.REQUEST_ID ) == null ){ return; }
	  long  id = new Long  ( userTask.getTaskAttributes().get( WebConstants.REQUEST_ID ) );
	  getRequestDetails( id );
	  
	  
    }
	
	@SuppressWarnings( "unchecked" )
	private void getRequestDetails( long id ) throws Exception  
	{
		requestView =  requestService.getRequestById( id );
		populateMasrafDetails(requestView.getMasrafId() );
		dataList =  requestView.getDisubrsementDetailsView();
		loopDataList();
		requestView.setDisubrsementDetailsView( null );
		
		getNewDisbursementDetailObject();
		populateApplicationDetailsTab();
		updateValuesToMap();
		loadAttachmentsAndComments( id );
	}

	@SuppressWarnings( "unchecked" )
	public void onPrint()
    {
    	try
    	{
    		updateValuesFromMap();
    		CommonUtil.printMasrafDisbursementReport( requestView.getRequestId() );
    	}
		catch (Exception ex) 
		{
			logger.LogException( "onPrint|Error Occured:",ex);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
    	
    }
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
	     saveComments();
		 saveAttachments(requestView.getRequestId().toString());
		 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	  {
		  String notesOwner = WebConstants.REQUEST;
		  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, requestView.getRequestId());
	  }
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
		String notesOwner = WebConstants.REQUEST;
    	if(txtRemarks !=null && this.txtRemarks.length()>0)
    	{
    	  CommonUtil.saveRemarksAsComments(requestView.getRequestId() , txtRemarks, notesOwner) ;
    	}
    	NotesController.saveNotes(notesOwner, requestView.getRequestId() );
    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }

	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( requestView != null && requestView.getRequestId()!= null )
		 {
			 loadAttachmentsAndComments( requestView.getRequestId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchEndowments()
	{
		if( sessionMap.get(WebConstants.SELECTED_ROW)== null )
		{return;}
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			Endowment endowment = (Endowment)sessionMap.remove(WebConstants.SELECTED_ROW);
			disbursementDetail.setEndowmentId(endowment.getEndowmentId());
			disbursementDetail.setEndowmentName(endowment.getEndowmentName() );
			disbursementDetail.setEndowmentCostCenter( endowment.getCostCenter()  );
			getBalanceDetailsForMasraf( requestView.getMasrafId(), endowment.getEndowmentId() );

			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromSearchEndowments --- CRASHED ---", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings("unchecked")
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMap();
			if(hdnPersonType.equals( "APPLICANT" ) )
			{
				populateApplicationDetailsTab();
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{
			if(requestView == null || requestView.getRequestId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,requestView.getRequestId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
//		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) 
//		{
//			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
//			tabPanel.setSelectedTab(TAB_APPLICANTS);
//			return true;
//		}
//		else
//		if( accountBalance == null || accountBalance.equals("0.0") )
//		{
//			errorMessages.add(CommonUtil.getBundleMessage("endowmentDisbursement.msg.remainingBalaneRequired"));
//			tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS);
//			return true;
//		}
		if( dataList == null || dataList.size() <=0  )
		{
			errorMessages.add( CommonUtil.getBundleMessage("mems.normaldisb.err.msg.atleastOneDisDetailRequired") );
			tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS);
			return true;
		}
		//Sending -1 to flag that whole request amount needs to be checked
		hasSaveErrors = hasBalanceErrors(-1);
		if( hasSaveErrors )return true;
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}
		return hasSaveErrors;
	}

	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			saveRequestInTransaction();
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
			
			getRequestDetails( requestView.getRequestId() );
			successMessages.add( ResourceUtil.getInstance().getProperty("thirdPartRevenue.msg.saved"));
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private boolean hasBalanceErrors( double amountToValidate ) throws Exception
	{
		boolean hasBalanceErrors=false;
		double amount =  MasarifService.getMasrafBalance( requestView.getMasrafId(), "0" );
		
		Long requestIdNot = requestView != null && requestView.getRequestId() != null ? requestView.getRequestId():-1;
		List<Long> disDetIdNotIn = loopDataList();
		if( amountToValidate <=0 )
		{
			amountToValidate = Double.valueOf(  totalDisbursementsAdded );
		}
		double reqAmount = EndowmentDisbursementService.getRequestDisbursementsForMasrafAmount( requestView.getMasrafId(), requestIdNot, disDetIdNotIn );
        double remainingBalance = amount-reqAmount;
        
		if(  remainingBalance < amountToValidate )
		{
			String msg = java.text.MessageFormat.format(
													    CommonUtil.getBundleMessage( "common.error.masrafBalanceNotEnough"),
													    remainingBalance,
													    amountToValidate
													   );
			errorMessages.add( msg );
			hasBalanceErrors  = true;
		}
		return hasBalanceErrors;
	}
	private boolean hasAddDisbursementDetailErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
//		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) 
//		{
//			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
//			tabPanel.setSelectedTab(TAB_APPLICANTS);
//			return true;
//		}
//		else
		if(disbursementDetail.getAmount() == null || disbursementDetail.getAmount().doubleValue()<=0)
		{
			errorMessages.add(CommonUtil.getBundleMessage("common.error.validation.invalidAmount"));
			tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS);
			return true;
		}
		else
		{
			hasSaveErrors = hasBalanceErrors(disbursementDetail.getAmount().doubleValue());
			if( hasSaveErrors )return true;
		}
		if( disbursementDetail.getDescription() == null || disbursementDetail.getDescription().trim().length() <=0 )
		{
			errorMessages.add(CommonUtil.getBundleMessage("socialProgram.messages.descReq"));
			tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS);
			return true;
		}

		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}
		return hasSaveErrors;
	}

	
	@SuppressWarnings( "unchecked" )
	public void onEdit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
	        disbursementDetail = ( DisbursementDetailsView )dataTable.getRowData();
	        if( disbursementDetail.getEndowmentId() != null )
	        {
	        	getBalanceDetailsForMasraf( requestView.getMasrafId(), disbursementDetail.getEndowmentId() );
	        }
	        
	        dataList.remove(disbursementDetail);
	        
	        manipulateBalance (disbursementDetail,true);
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onEdit--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	private boolean hasDeleteErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
        if( requestView.getRequestId() != null && dataList.size()==1 )
        {
        	errorMessages.add(ResourceUtil.getInstance().getProperty("donationRequest.msg.deleteError"));
        	return true;
        }
		return hasSaveErrors;
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDelete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			DisbursementDetailsView  row = ( DisbursementDetailsView )dataTable.getRowData();
			updateValuesFromMap();
			
			if( hasDeleteErrors() ){ return; }
	        
	        if( row.getDisDetId()  != null)
	        {
	        	row.setUpdatedBy( getLoggedInUserId() );
	        	row.setIsDeleted(1l);
	        }
	        else
	        {
	        	dataList.remove( row );
	        }
	        manipulateBalance (row,true);
			successMessages.add( ResourceUtil.getInstance().getProperty("donationRequest.msg.deletedSuccessfully"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onDelete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onAdd()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			if( hasAddDisbursementDetailErrors() ){ return; }
			List<PaymentDetailsView> pdList = disbursementDetail.getPaymentDetails();
			DisbursementDetailsView clonedView = (DisbursementDetailsView)BeanUtils.cloneBean(disbursementDetail);
			clonedView.setPaymentDetails(pdList);
			PaymentMethod paymentMethodObj = EntityManager.getBroker().findById(PaymentMethod.class, Long.valueOf( clonedView.getPaymentDetails().get(0).getPaymentMethodString() ));
			clonedView.getPaymentDetails().get(0).setPaymentMethodEn( paymentMethodObj.getDescription());
			clonedView.getPaymentDetails().get(0).setPaymentMethodAr( paymentMethodObj.getDescriptionAr());
			dataList.add( 0, clonedView);
			manipulateBalance(clonedView, false);
			getNewDisbursementDetailObject();
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onAdd --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	private void manipulateBalance (DisbursementDetailsView obj, boolean shouldSubtract)throws Exception
	{
		Double requestBalance = 0.0d;
		Double accBalance = 0.0d;
		if( accountBalance.getValue() != null )
		{
			String accountBalanceString = accountBalance.getValue().toString().trim();
			if( accountBalanceString .length() > 0)
			{
				
				accBalance = Double.valueOf( accountBalanceString );
				
			}
		}
		if( requestedAmount.getValue() != null )
		{
			String requestAmtString = requestedAmount.getValue().toString().trim();
			if( requestAmtString .length() > 0)
			{
				
				requestBalance = Double.valueOf( requestAmtString );
				
			}
		}
		if(!shouldSubtract)
		{
			requestedAmount.setValue( oneDForm.format(  requestBalance + obj.getAmount() ) );
			if(totalDisbursementsAdded != null && totalDisbursementsAdded.trim().length() > 0 )
			{
				totalDisbursementsAdded =  oneDForm.format( Double.valueOf(  totalDisbursementsAdded )+ obj.getAmount() );
			}
			
		}
		else
		{
			requestedAmount.setValue( oneDForm.format(  requestBalance - obj.getAmount() ) );
			if(totalDisbursementsAdded != null && totalDisbursementsAdded.trim().length() > 0 )
			{
				totalDisbursementsAdded =  oneDForm.format( Double.valueOf(  totalDisbursementsAdded )- obj.getAmount() );
			}
		}
			
		requestBalance = Double.valueOf( requestedAmount.getValue().toString() );
		calculateAvailableBalance(accBalance, requestBalance);
	}
	private void saveRequestInTransaction() throws Exception 
	{
		try
		{
			Long status = WebConstants.REQUEST_STATUS_NEW_ID;
            if(requestView.getStatusId() != null)
            {
            	status = null;
            }
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistRequest( status, WebConstants.DisbursementStatus.NEW_ID );
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistRequest(Long requestStatus,Long disbursementStatus) throws Exception 
	{
		if(requestStatus!=null) requestView.setStatusId(requestStatus);
		populateView(disbursementStatus);
		requestView = endDisService.persistDisbursementRequest( requestView );
	}
	
	private void populateView( Long disbursementStatus )throws Exception
	{
		List<DisbursementDetailsView > ddList = new ArrayList<DisbursementDetailsView>();
		
		for (DisbursementDetailsView obj : dataList) 
		{
			if( obj.getIsDeleted().compareTo(1l)==0 ||
				obj.getStatus().compareTo(WebConstants.DisbursementStatus.DISBURSED_ID ) == 0 
			  )
			{continue;}
			if(disbursementStatus != null)
			{
				obj.setStatus(disbursementStatus);
				
				
			}	
			obj.setOriginalAmount(obj.getAmount());
			obj.setUpdatedBy( getLoggedInUserId());
			obj.setUpdatedOn( new Date() );
			ddList.add(obj);
		}
		requestView.setDisubrsementDetailsView(ddList);
	}
	
	@SuppressWarnings( "unchecked" )
	public void onSubmit()
	{
		
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		String event  =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		try	
		{	
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			if( hasSaveErrors() ){ return; }
			
			 updateValuesFromMap();
			 
			 SystemParameters parameters = SystemParameters.getInstance();
			 String endPoint= parameters.getParameter( "MEMSEndowmentDisbursementBPEL" );
			 MEMSEndowmentDisbursementBPELPortClient port=new MEMSEndowmentDisbursementBPELPortClient();
			 port.setEndpoint(endPoint);
			 
			 persistRequest( status, WebConstants.DisbursementStatus.NEW_ID );
			 getRequestDetails( requestView.getRequestId() );
			 saveCommentsAttachment( event );
			 
			 port.initiate(
					 		CommonUtil.getLoggedInUser(),
					 		Integer.parseInt( requestView.getRequestId().toString()),
					 		requestView.getRequestNumber(), 
					 		null, 
					 		null
					 	   );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSubmit --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onResubmitted()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() ){ return; }
		 
		 updateValuesFromMap();	
		 
		 persistRequest( status ,WebConstants.DisbursementStatus.NEW_ID);
		 getRequestDetails( requestView.getRequestId() );
		 
		 setTaskOutCome(TaskOutcome.OK);
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onResubmitted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	private boolean hasReasonProvided() throws Exception
	{
		if(txtRemarks ==null || txtRemarks.trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.reasonRequired")  );
			return false;
		}
		return true;
	}
	private boolean hasApproveErrors()throws Exception
	{
		boolean hasError =false;
		
		for (DisbursementDetailsView  row : dataList) 
		{
			String msg="";
			if( 
					row.getPaymentDetails() == null || 
					row.getPaymentDetails().size()<=0 
			   )
			{
				msg = java.text.MessageFormat.format(
														CommonUtil.getBundleMessage("masraf.payment.popup.err.msg.paymentMethodReq"),
														row.getRefNum()
													);
				errorMessages.add( msg );
				tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS);
				return true;
			}			
			else
			{
				PaymentDetailsView obj = row.getPaymentDetails().get(0);
				if( obj.getPaymentMethod()== null || obj.getPaymentMethod().compareTo(-1l)==0)
				{
					msg = java.text.MessageFormat.format(
															CommonUtil.getBundleMessage("masraf.payment.popup.err.msg.paymentMethodReq"),
															row.getRefNum()
														);
					errorMessages.add( msg );
					tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS);
					return true;
				}
				if( obj.getOthers()== null || obj.getOthers().trim().length()<=0 )
				{
					msg = java.text.MessageFormat.format(
															CommonUtil.getBundleMessage("masraf.payment.popup.err.msg.paidToReq"),
															row.getRefNum()
														);
					errorMessages.add( msg );
					tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS);
					return true;
				}
			}
			
		}
		
		return hasError;
	}
	@SuppressWarnings( "unchecked" )
	public void onApprove()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_DISBURSEMENT;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_DISBURSEMENT;
		try	
		{	
			 updateValuesFromMap();
			 if( hasApproveErrors() ) return;

			 ApplicationContext.getContext().getTxnContext().beginTransaction();	
			
			 persistRequest( status,WebConstants.DisbursementStatus.DIS_REQ_ID );
			 setTaskOutCome(TaskOutcome.APPROVE);
			 getRequestDetails( requestView.getRequestId() );
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onApprove--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onRejected()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( status ,WebConstants.DisbursementStatus.REJECTED_ID);
			 setTaskOutCome(TaskOutcome.FORWARD);
			 getRequestDetails( requestView.getRequestId() );
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onRejectedByFinance()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_REJECTED_FINANCE;
		String event  =  MessageConstants.RequestEvents.REQUEST_REJECTED_FINANCE;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( status,WebConstants.DisbursementStatus.DIS_REQ_ID );
			 setTaskOutCome(TaskOutcome.REJECT);
			 getRequestDetails( requestView.getRequestId() );
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	private boolean hasOnCompleteErrors()throws Exception
	{
		errorMessages = new ArrayList<String>();
		
        return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDisburse()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			List<DisbursementDetailsView>  listToDisburse = new ArrayList<DisbursementDetailsView>();
			if ( hasOnDisburseErrors(listToDisburse) ) return;
			
			executeDisbursementInTransaction(listToDisburse);
			
			getRequestDetails( requestView.getRequestId() );
			successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsFinanceMsgs.SUCC_DISB));
		}
		catch (Exception exception) 
		{
			logger.LogException( "onDisburse--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	/**
	 * @param listToDisburse
	 * @return
	 * @throws Exception
	 */
	private boolean hasOnDisburseErrors(List<DisbursementDetailsView> listToDisburse) throws Exception 
	{
		boolean hasErrors = false;
		for (DisbursementDetailsView  row : dataList) 
		{
			if( row.getSelected() == null || !row.getSelected() )
				continue;
			hasErrors = hasBalanceErrors( row.getAmount()  );
			if ( hasErrors )
				break;
			row.setOriginalAmount( row.getAmount() );
			row.setUpdatedBy( getLoggedInUserId());
			row.setUpdatedOn( new Date() );
			
			listToDisburse.add(row);
		}
		if( listToDisburse == null || listToDisburse.size() <= 0 )
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_NO_ROW));
			tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS);
			return true;
		}
		return hasErrors;
	}

	/**
	 * @param listToDisburse
	 * @throws Exception
	 */
	private void executeDisbursementInTransaction(List<DisbursementDetailsView> listToDisburse) throws Exception 
	{
		try	
		{	
		  ApplicationContext.getContext().getTxnContext().beginTransaction();
			endDisService.executeDisbursements(requestView.getRequestId(), getLoggedInUserId(),listToDisburse);
			String disRefNums ="";
			for (DisbursementDetailsView obj : listToDisburse) {
				disRefNums += obj.getRefNum() +",";
			}
			if( disRefNums.length() > 0 )
			{
				disRefNums = disRefNums.substring(0 ,disRefNums.length()-1);
			}
			
			NotesController.saveSystemNotesForRequest(WebConstants.REQUEST,"masraf.disbursementMsg", requestView.getRequestId(),disRefNums);
			saveCommentsAttachment( "" );
		  ApplicationContext.getContext().getTxnContext().commit();
		}
		catch (Exception exception) 
		{
		  ApplicationContext.getContext().getTxnContext().rollback();
		  throw exception;
		}
		finally
		{
	      ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onComplete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COMPLETE);
		String msg    =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		String event  =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 	 updateValuesFromMap();
				 if( hasOnCompleteErrors() ) return;
				 persistRequest( status,WebConstants.DisbursementStatus.DISBURSED_ID );
				 setTaskOutCome(TaskOutcome.APPROVE);
				 getRequestDetails( requestView.getRequestId() );
				 saveCommentsAttachment( event );

			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onComplete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = getUserTask();
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    }



	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask )
	{
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}


	public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("masraf.title.disbursement"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			(( getPageMode().equals( PAGE_MODE_NEW)  || getPageMode().equals( PAGE_MODE_RESUBMITTED ) )&&
			viewMap.get( "alreadyExistingRequest") == null	
		  )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowApproval()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_APPROVAL)   )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowComplete()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_DISBURSEMENT_REQUIRED)   )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowPrint()
	{
		if( !getStatusNew() ) 
		{
			return true;
		}
		return false;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  getPageMode().equals( PAGE_MODE_NEW ) &&
			viewMap.get( "alreadyExistingRequest") == null	
		  )  
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowResubmitButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_RESUBMITTED ) &&
				viewMap.get( "alreadyExistingRequest") == null	
		  )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}


	public HtmlInputText getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(HtmlInputText accountBalance) {
		this.accountBalance = accountBalance;
	}


	public DisbursementDetailsView getDisbursementDetail() {
		return disbursementDetail;
	}

	public void setDisbursementDetail(DisbursementDetailsView disbursementDetail) {
		this.disbursementDetail = disbursementDetail;
	}

	public List<DisbursementDetailsView> getDataList() {
		return dataList;
	}

	public void setDataList(List<DisbursementDetailsView> dataList) {
		this.dataList = dataList;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public HtmlInputText getRequestedAmount() {
		return requestedAmount;
	}

	public void setRequestedAmount(HtmlInputText requestedAmount) {
		this.requestedAmount = requestedAmount;
		
	}

	public String getTotalDisbursementsAdded() {
		return totalDisbursementsAdded;
	}

	public void setTotalDisbursementsAdded(String totalDisbursementsAdded) {
		this.totalDisbursementsAdded = totalDisbursementsAdded;
		if(this.totalDisbursementsAdded.equals(".00") || this.totalDisbursementsAdded == null || this.totalDisbursementsAdded.length() <=0 )
		{
			this.totalDisbursementsAdded = "0.00";
		}
	}

	public HtmlInputText getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(HtmlInputText availableBalance) {
		this.availableBalance = availableBalance;
	}

	public Masraf getMasrafForDisplay() {
		return masrafForDisplay;
	}

	public void setMasrafForDisplay(Masraf masrafForDisplay) {
		this.masrafForDisplay = masrafForDisplay;
	}



}