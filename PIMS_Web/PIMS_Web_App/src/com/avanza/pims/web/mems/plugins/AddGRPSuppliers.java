package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.model.SelectItem;

import oracle.tip.pc.services.functions.Xpath20Function.UpperCaseFunction;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.data.ApplicationContext;
import com.avanza.pims.entity.DisReqBenAddSupplierView;
import com.avanza.pims.entity.GrpSupplierCode;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.ui.util.ResourceUtil;


@SuppressWarnings( "unchecked" )

public class AddGRPSuppliers extends AbstractMemsBean
{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6416579601242526598L;
	private HtmlDataTable dataTable;
	private HtmlCommandButton btnAdd ;
	DisbursementService service  = new DisbursementService();
	private HtmlCommandButton btnDone ;
    private List<DisReqBenAddSupplierView> dataList = null;
    private String selectedSupplierName;
    private String selectedSupplierCode;
    List<SelectItem> codeList;
	public AddGRPSuppliers() 
	{	
		
		dataTable = new HtmlDataTable();
		btnAdd = new HtmlCommandButton();
		btnDone = new HtmlCommandButton();
		dataList = new ArrayList<DisReqBenAddSupplierView>( 0 );
		codeList = new ArrayList<SelectItem>(0);
	}
	
	@Override
	public void init() 
	{
		
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException( "init --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}
	
	
	private void updateValuesFromMaps() throws Exception
	{
		// Disbursement Details View
		if ( sessionMap.get( WebConstants.AssociateCostCenter.LIST_DIS_DET_IDS) != null )
		{
			viewMap.put(
						 WebConstants.AssociateCostCenter.LIST_DIS_DET_IDS,
						 sessionMap.remove( WebConstants.AssociateCostCenter.LIST_DIS_DET_IDS )
					   );
			getRequestBeneficiaries(); 
		}  
		if ( viewMap.get( WebConstants.AssociateCostCenter.DATA_LIST_VIEW) != null )
		{
			dataList = ( ArrayList<DisReqBenAddSupplierView> ) viewMap.get( WebConstants.AssociateCostCenter.DATA_LIST_VIEW);
		}	
		if ( viewMap.get( "codeList" ) != null )
		{
			codeList = ( ArrayList<SelectItem> ) viewMap.get( "codeList" );
		}	
		
		updateValuesToMaps();
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void getRequestBeneficiaries() throws Exception 
	{
		if ( viewMap.get( WebConstants.AssociateCostCenter.LIST_DIS_DET_IDS) ==null )return;
		List<Long>  idsList = (ArrayList<Long>)viewMap.get( WebConstants.AssociateCostCenter.LIST_DIS_DET_IDS) ;
		if( idsList.size() <= 1000 )
		{	
			dataList = service.getRequestBeneficiariesForAddSupplier( idsList );
		}
		else
		{
			double loopCount = Math.ceil( ( idsList.size()*1.00) / 1000.00 );
			int lastCount=  0 ;
			List<Long>  idsSubList = new  ArrayList<Long>();
			for ( int i=0; i<loopCount;i++ )
			{
				
				int upperBound = (lastCount+1000)> idsList.size() ? ( (1000*i) + (idsList.size() - lastCount  ) ):(lastCount+1000);
//				System.out.println("lastCount:"+lastCount+"|upperBound:"+upperBound);
				idsSubList = idsList.subList(lastCount, upperBound);
				
				dataList .addAll(
									service.getRequestBeneficiariesForAddSupplier( idsSubList ) 
								);
				lastCount =upperBound;
			}
		}
//		System.out.println(dataList.size());
		viewMap.put( WebConstants.AssociateCostCenter.DATA_LIST_VIEW,dataList );
	}
	
	private void updateValuesToMaps()
	{
		 
		 if( dataList != null )
		 {
			 viewMap.put( WebConstants.AssociateCostCenter.DATA_LIST_VIEW, dataList );
		 }	 
		 if ( codeList   != null )
		 {
				viewMap.put( "codeList",codeList  );
		 }
	}
	
	public void onSupplierChange()
	{
		
		try
		{
			updateValuesFromMaps();
			//if ( !hasErrors() ){return;}
				List<GrpSupplierCode> list = DisbursementService.getAllGrpSupplierCodesBySupplierId(selectedSupplierName);
				codeList = new ArrayList<SelectItem>();
				for (GrpSupplierCode obj : list) 
				{
					SelectItem item = new SelectItem (obj.getCodeId().toString(),obj.getSupplierCode());
					codeList.add(item);
				}
			    
				updateValuesToMaps();
		}
		catch ( Exception e )
		{
			logger.LogException(   "onSupplierChange--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
	}
	
	
	private boolean hasErrors()
	{
		boolean hasErrors = false;
		if( selectedSupplierName == null || selectedSupplierName.equals("-1") )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("grpSupplier.msg.supplierRequired"));
			hasErrors = true;
		}
		else if( selectedSupplierCode== null || selectedSupplierCode.equals("-1") )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("grpSupplier.msg.codeRequired"));
			hasErrors = true;
		}
		return hasErrors ;
	}
	public void onDone()
	{
		
		try
		{
			updateValuesFromMaps();
			if ( hasErrors() ){return;}
			
			StringBuilder ids= new StringBuilder();
			for (DisReqBenAddSupplierView  view : dataList) 
			{
				if( !view.getSelected() )continue; 
				
				ids.append( view.getDisReqBenId()).append( ",");
				
			}
			if( ids.length()> 0 )
			{
				service.modifySupplier(ids.toString().substring(0,ids.length()-1), selectedSupplierName , selectedSupplierCode);
			}
//			getRequestBeneficiaries();
			updateValuesToMaps();
		}
		catch ( Exception e )
		{
			logger.LogException(   "onDone --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
	}
	
	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<DisReqBenAddSupplierView> getDataList() {
		return dataList;
	}

	public void setDataList(List<DisReqBenAddSupplierView> dataList) {
		this.dataList = dataList;
	}
	public HtmlCommandButton getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(HtmlCommandButton btnAdd) {
		this.btnAdd = btnAdd;
	}

	public HtmlCommandButton getBtnDone() {
		return btnDone;
	}

	public void setBtnDone(HtmlCommandButton btnDone) {
		this.btnDone = btnDone;
	}

	public String getSelectedSupplierName() {
		return selectedSupplierName;
	}

	public void setSelectedSupplierName(String selectedSupplierName) {
		this.selectedSupplierName = selectedSupplierName;
	}

	public String getSelectedSupplierCode() {
		return selectedSupplierCode;
	}

	public void setSelectedSupplierCode(String selectedSupplierCode) {
		this.selectedSupplierCode = selectedSupplierCode;
	}

	public List<SelectItem> getCodeList() {
		return codeList;
	}

	public void setCodeList(List<SelectItem> codeList) {
		this.codeList = codeList;
	}

		


}
