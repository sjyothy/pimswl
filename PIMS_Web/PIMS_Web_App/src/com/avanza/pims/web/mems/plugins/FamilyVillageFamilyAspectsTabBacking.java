package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.FamilyVillageFamilyAspectService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.FamilyAspectFinancialView;
import com.avanza.pims.ws.vo.mems.FamilyAspectPsychologyView;
import com.avanza.pims.ws.vo.mems.FamilyAspectSocialView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageFamilyAspectsTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private List< FamilyAspectSocialView> dataListFamilyAspectSocial;
	private List< FamilyAspectFinancialView> dataListFamilyAspectFinancial;
	private List< FamilyAspectPsychologyView> dataListFamilyAspectPsychology;
	private HtmlDataTable dataTableFamilyAspectSocial;
	private HtmlDataTable dataTableFamilyAspectFinancial;
	private HtmlDataTable dataTableFamilyAspectPsychology;
	FamilyVillageFamilyAspectService service;
	
	private String tabMode;
	private Long personId;
	private Integer recordSizeFamilyAspectSocial;
	private Integer recordSizeFamilyAspectPsychology;
	private Integer recordSizeFamilyAspectFinancial;
	public FamilyVillageFamilyAspectsTabBacking() 
	{
		service = new FamilyVillageFamilyAspectService();
		dataTableFamilyAspectSocial             =  new HtmlDataTable();
		dataTableFamilyAspectFinancial          =  new HtmlDataTable();
		dataTableFamilyAspectPsychology         =  new HtmlDataTable();
		dataListFamilyAspectSocial		        =  new ArrayList<FamilyAspectSocialView>();
		dataListFamilyAspectFinancial    		=  new ArrayList<FamilyAspectFinancialView>();
		dataListFamilyAspectPsychology 			=  new ArrayList<FamilyAspectPsychologyView>();
		recordSizeFamilyAspectFinancial  		=  0;
		recordSizeFamilyAspectPsychology  		=  0;
		recordSizeFamilyAspectSocial  		=  0;
		tabMode          = Constant.EndowmentFile.MODE_READ_ONLY;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			tabMode = (String) viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		if( viewMap.get( "dataListFamilyAspectFinancial" ) != null )
		{
			dataListFamilyAspectFinancial = (ArrayList<FamilyAspectFinancialView>)viewMap.get( "dataListFamilyAspectFinancial" );
		}
		if( viewMap.get( "recordSizeFamilyAspectFinancial" ) != null )
		{
			recordSizeFamilyAspectFinancial = Integer.parseInt( viewMap.get( "recordSizeFamilyAspectFinancial" ).toString() );
		}
				
		if( viewMap.get( "dataListFamilyAspectSocial" ) != null )
		{
			dataListFamilyAspectSocial = (ArrayList<FamilyAspectSocialView>)viewMap.get( "dataListFamilyAspectSocial" );
		}
		if( viewMap.get( "recordSizeFamilyAspectSocial" ) != null )
		{
			recordSizeFamilyAspectSocial = Integer.parseInt( viewMap.get( "recordSizeFamilyAspectSocial" ).toString() );
		}
		
		if( viewMap.get( "dataListFamilyAspectPsychology" ) != null )
		{
			dataListFamilyAspectPsychology = (ArrayList<FamilyAspectPsychologyView>)viewMap.get( "dataListFamilyAspectPsychology" );
		}
		if( viewMap.get( "recordSizeFamilyAspectPsychology" ) != null )
		{
			recordSizeFamilyAspectPsychology = Integer.parseInt( viewMap.get( "recordSizeFamilyAspectPsychology" ).toString() );
		}
		
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, tabMode );
		}
		if(recordSizeFamilyAspectPsychology !=null)
		{
			viewMap.put("recordSizeFamilyAspectPsychology",recordSizeFamilyAspectPsychology);
		}
		if(recordSizeFamilyAspectSocial !=null)
		{
			viewMap.put("recordSizeFamilyAspectSocial",recordSizeFamilyAspectSocial);
		}
		if(recordSizeFamilyAspectFinancial !=null)
		{
			viewMap.put("recordSizeFamilyAspectFinancial",recordSizeFamilyAspectFinancial);
		}
		if( dataListFamilyAspectFinancial != null && dataListFamilyAspectFinancial.size()  > 0  )
		{
			viewMap.put( "dataListFamilyAspectFinancial", dataListFamilyAspectFinancial );
		}
		if( dataListFamilyAspectSocial != null && dataListFamilyAspectSocial.size()  > 0  )
		{
			viewMap.put( "dataListFamilyAspectSocial", dataListFamilyAspectSocial );
		}
		if( dataListFamilyAspectPsychology != null && dataListFamilyAspectPsychology.size()  > 0  )
		{
			viewMap.put( "dataListFamilyAspectPsychology", dataListFamilyAspectPsychology );
		}
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
	}	
	
	

	public void populateTab( PersonView personView ) throws Exception
	{
		updateValuesFromMaps();
		personId = personView.getPersonId();
		setPageDataDefaults();
		loadDataListFamilyAspectFinancial( personId );
		loadDataListFamilyAspectPsychology( personId );
		loadDataListFamilyAspectSocial( personId );
		setTotalRows( dataListFamilyAspectFinancial.size() );
		
		updateValuesToMaps();
	}
	
	private void setPageDataDefaults()throws Exception
	{
		PersonView person  = new PersonView();
		person.setPersonId(personId);
	}

	private void loadDataListFamilyAspectSocial(Long personId) throws Exception {
		
		dataListFamilyAspectSocial= FamilyVillageFamilyAspectService.getFamilyAspectSocial( personId );
		recordSizeFamilyAspectSocial=dataListFamilyAspectSocial.size();
	}
	
	private void loadDataListFamilyAspectPsychology(Long personId) throws Exception {
		
		dataListFamilyAspectPsychology  = FamilyVillageFamilyAspectService.getFamilyAspectPsychology( personId );
		recordSizeFamilyAspectPsychology=dataListFamilyAspectPsychology.size();
	}

	private void loadDataListFamilyAspectFinancial(Long personId) throws Exception {
		
		dataListFamilyAspectFinancial= FamilyVillageFamilyAspectService.getFamilyAspectFinancial( personId );
		recordSizeFamilyAspectFinancial=dataListFamilyAspectFinancial.size();
	}
		@SuppressWarnings( "unchecked" )
	public void onDeleteAspectSocial()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteAspectSocial--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
	}
  
	@SuppressWarnings( "unchecked" )
	public void onDeleteAspectFinancial()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteAspectFinancial--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDeleteAspectPsychology()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteAspectPsychology--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
	}
	
	
	
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}
	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public List<FamilyAspectPsychologyView> getDataListFamilyAspectPsychology() {
		return dataListFamilyAspectPsychology;
	}

	public void setDataListFamilyAspectPsychology(
			List<FamilyAspectPsychologyView> dataListFamilyAspectPsychology) {
		this.dataListFamilyAspectPsychology = dataListFamilyAspectPsychology;
	}

	public List<FamilyAspectSocialView> getDataListFamilyAspectSocial() {
		return dataListFamilyAspectSocial;
	}

	public void setDataListFamilyAspectSocial(
			List<FamilyAspectSocialView> dataListFamilyAspectSocial) {
		this.dataListFamilyAspectSocial = dataListFamilyAspectSocial;
	}

	public List<FamilyAspectFinancialView> getDataListFamilyAspectFinancial() {
		return dataListFamilyAspectFinancial;
	}

	public void setDataListFamilyAspectFinancial(
			List<FamilyAspectFinancialView> dataListFamilyAspectFinancial) {
		this.dataListFamilyAspectFinancial = dataListFamilyAspectFinancial;
	}

	public HtmlDataTable getDataTableFamilyAspectPsychology() {
		return dataTableFamilyAspectPsychology;
	}

	public void setDataTableFamilyAspectPsychology(
			HtmlDataTable dataTableFamilyAspectPsychology) {
		this.dataTableFamilyAspectPsychology = dataTableFamilyAspectPsychology;
	}

	public HtmlDataTable getDataTableFamilyAspectSocial() {
		return dataTableFamilyAspectSocial;
	}

	public void setDataTableFamilyAspectSocial(
			HtmlDataTable dataTableFamilyAspectSocial) {
		this.dataTableFamilyAspectSocial = dataTableFamilyAspectSocial;
	}

	public HtmlDataTable getDataTableFamilyAspectFinancial() {
		return dataTableFamilyAspectFinancial;
	}

	public void setDataTableFamilyAspectFinancial(
			HtmlDataTable dataTableFamilyAspectFinancial) {
		this.dataTableFamilyAspectFinancial = dataTableFamilyAspectFinancial;
	}

	public Integer getRecordSizeFamilyAspectSocial() {
		return recordSizeFamilyAspectSocial;
	}

	public void setRecordSizeFamilyAspectSocial(Integer recordSizeFamilyAspectSocial) {
		this.recordSizeFamilyAspectSocial = recordSizeFamilyAspectSocial;
	}

	public Integer getRecordSizeFamilyAspectPsychology() {
		return recordSizeFamilyAspectPsychology;
	}

	public void setRecordSizeFamilyAspectPsychology(
			Integer recordSizeFamilyAspectPsychology) {
		this.recordSizeFamilyAspectPsychology = recordSizeFamilyAspectPsychology;
	}

	public Integer getRecordSizeFamilyAspectFinancial() {
		return recordSizeFamilyAspectFinancial;
	}

	public void setRecordSizeFamilyAspectFinancial(
			Integer recordSizeFamilyAspectFinancial) {
		this.recordSizeFamilyAspectFinancial = recordSizeFamilyAspectFinancial;
	}

	
	


}
