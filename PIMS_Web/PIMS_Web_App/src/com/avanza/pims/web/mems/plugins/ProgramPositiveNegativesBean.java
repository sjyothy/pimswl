package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.minors.SocialProgramsBean;
import com.avanza.pims.ws.mems.SocialProgramService;
import com.avanza.pims.ws.vo.mems.ProgramPositiveNegativesView;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings( "unchecked" )
public class ProgramPositiveNegativesBean extends AbstractMemsBean
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8816174038795067243L;
	
	private List<ProgramPositiveNegativesView> list;
	private HtmlDataTable dataTable;
	private ProgramPositiveNegativesView programPositiveNegativesView;
	private String listLoadedFromDb;
	private SocialProgramsBean parentBean;	
	public ProgramPositiveNegativesBean() 
	{	
		programPositiveNegativesView = new ProgramPositiveNegativesView() ;
		list = new ArrayList<ProgramPositiveNegativesView>(0);
		dataTable = new HtmlDataTable();
		
	}
	
	@Override
	public void init() 
	{
		String METHOD_NAME = "init()";
		errorMessages = new ArrayList<String>(0);
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			parentBean.setErrorMessages( errorMessages );
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws PimsBusinessException
	{
		
		// SPONSOR VIEW
		if ( viewMap.get( WebConstants.SocialProgramPositiveNegatives.VO ) != null )
		{
			programPositiveNegativesView = (ProgramPositiveNegativesView) viewMap.get( WebConstants.SocialProgramPositiveNegatives.VO );
		}		
		
		// SPONSOR VIEW LIST
		if ( viewMap.get( WebConstants.SocialProgramPositiveNegatives.DATA_LIST ) != null )
		{
			list = ( List<ProgramPositiveNegativesView>) viewMap.get( WebConstants.SocialProgramPositiveNegatives.DATA_LIST );
		}		
		parentBean = (SocialProgramsBean)getBean( "pages$SocialProgram" );
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		
		// PURPOSE VIEW
		if ( programPositiveNegativesView != null  )
		{
			viewMap.put( WebConstants.SocialProgramPositiveNegatives.VO , programPositiveNegativesView);
		}
		
		// LIST
		if ( list != null )
		{
			viewMap.put( WebConstants.SocialProgramPositiveNegatives.DATA_LIST, list );
		}
	}	
	
	@SuppressWarnings("unchecked")
	public void onLoadList( Long programId )throws Exception
	{
		final String METHOD_NAME = "onLoadList()";
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( this.getListLoadedFromDb()== null || this.getListLoadedFromDb().equals("0") )
			{
				
				list =  new SocialProgramService().loadProgramPositiveNegatives( programId  );
				this.setListLoadedFromDb( "1" );
				updateValuesToMaps();
			}
	}

	
	public String getListLoadedFromDb() {
		if( viewMap.get( "posnegListLoadedFromDB" ) != null )
		{
			listLoadedFromDb = viewMap.get( "posnegListLoadedFromDB" ).toString(); 
		}
		return listLoadedFromDb;
	}
    @SuppressWarnings( "unchecked" )
	public void setListLoadedFromDb(String listLoadedFromDb) {
		this.listLoadedFromDb = listLoadedFromDb;
		if( this.listLoadedFromDb != null )
		{
			
			viewMap.put( "posnegListLoadedFromDB",this.listLoadedFromDb ); 
		}
	}
    @SuppressWarnings("unchecked")
	public void onDelete( )
	{
	    final String METHOD_NAME = "onDelete()";
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			updateValuesFromMaps();
			ProgramPositiveNegativesView item = ( ProgramPositiveNegativesView )dataTable.getRowData(); 
			
			if(  item.getProgramPositiveNegativesId() != null)
			{
			item.setIsDeleted( "1" );
			}
			else
			{
			 list.remove(item);
			}
			updateValuesToMaps();
			successMessages.add( ResourceUtil.getInstance().getProperty("socialProgram.messages.sponsorDeleted") );
			logger.logInfo( METHOD_NAME + " --- Finish --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		parentBean.setErrorMessages( errorMessages );
		}
		
	}
	@SuppressWarnings("unchecked")
	public void onSave()
	{
		final String METHOD_NAME = "onSave()";
		errorMessages = new ArrayList<String>();
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( isValid() )
			{
				ProgramPositiveNegativesView newVo = new ProgramPositiveNegativesView();
				setDefaultValues(newVo);
				newVo.setIsPositive( programPositiveNegativesView.getIsPositive()  );
				newVo.setComments( programPositiveNegativesView.getComments()  );
				list.add(0, newVo);
				programPositiveNegativesView = new ProgramPositiveNegativesView();
				updateValuesToMaps();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			parentBean.setErrorMessages( errorMessages );
		}
	}

	private void setDefaultValues(ProgramPositiveNegativesView newVo) {
		newVo.setCreatedBy(getLoggedInUserId() );
		newVo.setCreatedEn(getLoggedInUserObj().getFullName());
		newVo.setCreatedAr( getLoggedInUserObj().getSecondaryFullName() );
		newVo.setCreatedOn( new Date() );
		newVo.setIsDeleted( "0" );
	}
	
	public String onSessionDetailDelete()
	{
		updateValuesToMaps();
		return null;
	}
	
	private boolean isValid() throws Exception
	{
		boolean valid = true;
		
		// VALIDATION CHECKS STARTS HERE
		if( programPositiveNegativesView.getIsPositive() == null || programPositiveNegativesView.getIsPositive().trim().length() <=0  ||
			programPositiveNegativesView.getIsPositive().trim().equals("-1")	
		  )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.posNegTypeReq"));
			parentBean.setErrorMessages(errorMessages);
			valid = false;
		}
		if( programPositiveNegativesView.getComments() == null || programPositiveNegativesView.getComments().trim().length() <=0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.commentsReq"));
			parentBean.setErrorMessages(errorMessages);
			valid = false;
		}
		// VALIDATION CHECKS ENDS HERE
		
		return valid;
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<ProgramPositiveNegativesView> getList() {
		return list;
	}

	public void setList(List<ProgramPositiveNegativesView> list) {
		this.list = list;
	}

	public ProgramPositiveNegativesView getProgramPositiveNegativesView() {
		return programPositiveNegativesView;
	}

	public void setProgramPositiveNegativesView(ProgramPositiveNegativesView ProgramPositiveNegativesView) {
		this.programPositiveNegativesView = ProgramPositiveNegativesView;
	}
}
