package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.minors.TakharujDetailManageBacking;
import com.avanza.pims.web.mems.minors.TakharujManageBacking;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.SessionDetailView;

public class SessionDetailsTabBacking extends AbstractMemsBean
{
	private static final long serialVersionUID = 8517238816949483790L;
	
	private RequestView requestView;
	private SessionDetailView sessionDetailView;
	private List<SessionDetailView> sessionDetailViewList;
	private final String SESSION_DETAIL_VIEW_LIST = "SESSION_DETAIL_VIEW_LIST";
	private HtmlDataTable sessionDetailViewDataTable;
	private boolean parentTakharuj;
	private UtilityService utilityService;
	
	private String tabMode;
	
	public SessionDetailsTabBacking() 
	{	
		requestView = new RequestView();		
		sessionDetailView = new SessionDetailView();
		sessionDetailViewList = new ArrayList<SessionDetailView>(0);
		
		sessionDetailViewDataTable = new HtmlDataTable();
		
		utilityService = new UtilityService();
		
		tabMode = WebConstants.Session.SESSION_DETAIL_TAB_MODE_VIEW_ONLY;
	}
	
	@Override
	public void init() 
	{
		String METHOD_NAME = "init()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws PimsBusinessException
	{
		// TAB MODE
		if ( viewMap.get( WebConstants.Session.SESSION_DETAIL_TAB_MODE_KEY ) != null )
		{
			tabMode = (String) viewMap.get( WebConstants.Session.SESSION_DETAIL_TAB_MODE_KEY );
		}
		
		
		// REQUEST VIEW
		if ( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = (RequestView) viewMap.get( WebConstants.REQUEST_VIEW );
		}
		
		// SESSION DETAIL VIEW
		if ( viewMap.get( WebConstants.Session.SESSION_DETAIL_VIEW ) != null )
		{
			sessionDetailView = (SessionDetailView) viewMap.get( WebConstants.Session.SESSION_DETAIL_VIEW );
		}		
		
		// SESSION DETAIL VIEW LIST
		if ( viewMap.get( WebConstants.Session.SESSION_DETAIL_VIEW_LIST ) != null )
		{
			sessionDetailViewList = (List<SessionDetailView>) viewMap.get( WebConstants.Session.SESSION_DETAIL_VIEW_LIST );
		}
		else if ( requestView != null && requestView.getRequestId() != null )
		{
			SessionDetailView sessionDetailView = new SessionDetailView();
			sessionDetailView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
			sessionDetailView.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
			sessionDetailView.setRequestView( requestView );
			sessionDetailViewList = utilityService.getJudgeSession(sessionDetailView);			
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( WebConstants.Session.SESSION_DETAIL_TAB_MODE_KEY, tabMode );
		}
		
		// SESSION DETAIL VIEW
		if ( sessionDetailView != null  )
		{
			viewMap.put( WebConstants.Session.SESSION_DETAIL_VIEW, sessionDetailView );
		}
		
		// SESSION DETAIL VIEW LIST
		if ( sessionDetailViewList != null )
		{
			viewMap.put( WebConstants.Session.SESSION_DETAIL_VIEW_LIST, sessionDetailViewList );
		}
		
		
	}	
	
	@SuppressWarnings("unchecked")
	public String onSessionDetailSave()
	{
		final String METHOD_NAME = "onSessionDetailSave()";
		errorMessages = new ArrayList<String>();
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( isValid() )
			{
				SessionDetailView newSessionDetailView = new SessionDetailView();
								
				newSessionDetailView.setJudgeName( sessionDetailView.getJudgeName() );
				newSessionDetailView.setPlace( sessionDetailView.getPlace() );
				newSessionDetailView.setDate( sessionDetailView.getDate() );
				newSessionDetailView.setTimeHH( sessionDetailView.getTimeHH() );
				newSessionDetailView.setTimeMM( sessionDetailView.getTimeMM() );
				newSessionDetailView.setDecision( sessionDetailView.getDecision() );
				newSessionDetailView.setMaturityDate( sessionDetailView.getMaturityDate() );
				newSessionDetailView.setRequestView( requestView );
				
				newSessionDetailView.setCreatedBy( CommonUtil.getLoggedInUser() );
				newSessionDetailView.setCreatedOn( new Date() );
				newSessionDetailView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				newSessionDetailView.setUpdatedOn( new Date() );
				newSessionDetailView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
				newSessionDetailView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
				
				sessionDetailViewList.add(0, newSessionDetailView);
				//saving it in viewMap to retrieve it from the parent bean later , for maturity date
				
				
				updateValuesToMaps();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			if(errorMessages!= null && errorMessages.size() > 0 )
			{
				viewMap.put(WebConstants.SESION_DETAIL_TAB_ERROR_MESSAGES,errorMessages);
			}
		}
		return null;
	}
	
	public String onSessionDetailDelete()
	{
		SessionDetailView selectedSessionDetailView = (SessionDetailView) sessionDetailViewDataTable.getRowData();
		sessionDetailViewList.remove( selectedSessionDetailView );
		updateValuesToMaps();
		return null;
	}
	
	private boolean isValid() 
	{
		boolean valid = true;
		
		// VALIDATION CHECKS STARTS HERE
		
		if ( sessionDetailView.getJudgeName() == null || sessionDetailView.getJudgeName().equalsIgnoreCase("") )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.session.detail.tab.judge.name" ) );
			valid = false;
		}
		
		if ( sessionDetailView.getPlace() == null || sessionDetailView.getPlace().equalsIgnoreCase("") )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.session.detail.tab.place" ) );
			valid = false;
		}
		
		if ( sessionDetailView.getDate() == null )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.session.detail.tab.date" ) );
			valid = false;
		}
		
		if ( sessionDetailView.getTimeHH() == null || sessionDetailView.getTimeHH().equalsIgnoreCase("") )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.session.detail.tab.timeHH" ) );
			valid = false;
		}
		
		if ( sessionDetailView.getTimeMM() == null || sessionDetailView.getTimeMM().equalsIgnoreCase("") )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.session.detail.tab.timeMM" ) );
			valid = false;
		}
		
		if ( sessionDetailView.getDecision() == null || sessionDetailView.getDecision().equalsIgnoreCase("") )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.session.detail.tab.decision" ) );
			valid = false;
		}
		
		if ( !isParentTakharuj() && sessionDetailView.getMaturityDate() == null )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.session.detail.tab.maturityDate" ) );
			valid = false;
		}
		
		// VALIDATION CHECKS ENDS HERE
		
		//DECIDING THE PARENT PAGE OF THIS TAB AND BASED ON THAT SHOW ERROR MESSAGE ON THAT PARENT
		if  ( !valid )
		{
			if( isParentTakharuj() )
			{
				
				TakharujManageBacking bean =  (TakharujManageBacking)getBean( "pages$takharujManage" );
				bean.setErrorMessages( errorMessages );
				
			}
		}
		return valid;
	}
	
	public Integer getSessionDetailViewListRecordSize()
	{		
		return sessionDetailViewList.size();
	}
	
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( WebConstants.Session.SESSION_DETAIL_TAB_MODE_UPDATABLE ) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}

	public SessionDetailView getSessionDetailView() {
		return sessionDetailView;
	}

	public void setSessionDetailView(SessionDetailView sessionDetailView) {
		this.sessionDetailView = sessionDetailView;
	}

	public List<SessionDetailView> getSessionDetailViewList() {
		return sessionDetailViewList;
	}

	public void setSessionDetailViewList(
			List<SessionDetailView> sessionDetailViewList) {
		this.sessionDetailViewList = sessionDetailViewList;
	}

	public HtmlDataTable getSessionDetailViewDataTable() {
		return sessionDetailViewDataTable;
	}

	public void setSessionDetailViewDataTable(
			HtmlDataTable sessionDetailViewDataTable) {
		this.sessionDetailViewDataTable = sessionDetailViewDataTable;
	}

	public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public UtilityService getUtilityService() {
		return utilityService;
	}

	public void setUtilityService(UtilityService utilityService) {
		this.utilityService = utilityService;
	}

	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}

	public boolean isParentTakharuj() {
		if( viewMap.get( WebConstants.Session.SESSION_DETAIL_TAB_PARENT ) != null  && 
			viewMap.get( WebConstants.Session.SESSION_DETAIL_TAB_PARENT ).toString().equals(WebConstants.Session.PARENT_TAKHARUJ ) 	
		  )
		    return true;
		else
			return false;
	}

	public void setParentTakharuj(boolean parentTakharuj) {
		this.parentTakharuj = parentTakharuj;
	}	
}
