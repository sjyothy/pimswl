package com.avanza.pims.web.mems.endowment;
import static com.avanza.pims.web.util.validator.MasrafValidator.isMasrafNameValidated;

import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.Utils.generatorViews.MasarifSearchView;
import com.avanza.pims.entity.EndFileAsso;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.MasarifService;
import com.avanza.pims.ws.mems.endowment.EndowmentService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.BankView;
import com.avanza.ui.util.ResourceUtil;

public class ManageMasarifBean extends AbstractMemsBean {
	private static final long serialVersionUID = 1L;
	MasarifService masarifService = new MasarifService();

	List<EndFileAsso> dataListEndowemntFileAsso;
	private HtmlDataTable dataTableFileAsso;
	private boolean englishLocale;
	private String VIEW_MODE = "pageMode";
	private String MODE_SELECT_ONE_POPUP = "MODE_SELECT_ONE_POPUP";
	private String MODE_SELECT_MANY_POPUP = "MODE_SELECT_MANY_POPUP";
	private String READONLY = "READONLY";
	private String MASRAF_VIEW = "MASRAF_VIEW";
	private String ALL_PARENTS = "ALL_PARENTS";
	private String PARENT_MAP = "PARENT_MAP";
	private String BANK_JSON = "BANK_JSON" ;
	private String GRP_JSON = "GRP_JSON" ;
	private String READONLY_PARENT_COMBO = "READONLY_PARENT_COMBO" ;
	private String MASARIF_VIEW_CLONE = "MASARIF_VIEW_CLONE" ;
	private String ALL_CHILDS = "ALL_CHILDS" ;
	private String BANK_MAP = "BANK_MAP" ;
	private String parentBankJson;
	private String parentGRPJson;
	private String totalEndowments="0";
	private HashMap<Long, BankView> bankMap = null;
	private boolean enableParentMasraf;
	private String styleClass;
	private String parentStyleClass;
	private List<SelectItem> allParents = new ArrayList<SelectItem>();
	private boolean popup;
	private boolean readOnly;
	private HashMap<String, MasarifSearchView> parentMap = new HashMap<String, MasarifSearchView>();
	private MasarifSearchView masarifView = new MasarifSearchView();
	private MasarifSearchView masarifViewClone = new MasarifSearchView();
	private HttpServletRequest request = (HttpServletRequest) this
	.getFacesContext().getExternalContext().getRequest();
	private boolean isShowActivitylogTab;
	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		super.init();
		try {
			if (!isPostBack()) 
			{
				bankMap = new UtilityService().getBankViewMap();
				setBankMap(bankMap);
				checkDataInMapsandQueryString();
				populateLists();
				setFieldValue();
				requestHistoryTabClick();
			}
		} catch (Exception es) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init" + "|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	private void setFieldValue() 
	{
		
		if(viewMap.get(PARENT_MAP) != null && getMasarifView()!=null &&  getMasarifView().getParentId() != null && masarifView.getParentId().trim().length() > 0)
		{
			
			parentMap = (HashMap<String, MasarifSearchView>) viewMap.get(PARENT_MAP);
			
			if(parentMap.containsKey(masarifView.getMasrafId()))
			{
				parentMap.remove(masarifView.getMasrafId());
				setParentMap(parentMap);
			}
			if(parentMap.get(masarifView.getParentId()) != null)
			{
				MasarifSearchView parentMasraf = null;
				parentMasraf = parentMap.get(masarifView.getParentId());
				masarifView.setParentName(parentMasraf.getMasrafName());
				masarifView.setParentGrp(parentMasraf.getGrpAccNumber());
				if(isEnglishLocale())
					masarifView.setParentBankName(parentMasraf.getBankNameEn());
				else
					masarifView.setParentBankName(parentMasraf.getBankNameAr());
			}
		}
		
	}

	@SuppressWarnings("unchecked")
	private void populateLists() throws Exception 
	{
		List<MasarifSearchView> parentMasarif = null;
		masarifView = getMasarifView();
		String childIds = ",";
		if(masarifView != null && masarifView.getMasrafId() != null )
		{
			List<MasarifSearchView> childs =   masarifService.getAllChildMasarif( masarifView.getMasrafId());
			if(childs != null && childs.size() > 0)
			{
				//do not fill parent combo, and keep it readonly
				//viewMap.put(READONLY_PARENT_COMBO,true);
				viewMap.put(ALL_CHILDS,childs);
				for (MasarifSearchView masarifSearchView : childs) 
				{
				  childIds += masarifSearchView.getMasrafId().toString() +",";	
				}
				//return;
			}
		}
		parentMasarif = masarifService.getAllParentMasarif();
		List<SelectItem> parents = new ArrayList<SelectItem>();
		parentBankJson = "{";
		parentGRPJson = "{";
		if(parentMasarif != null && parentMasarif.size() > 0)
		{

			for(MasarifSearchView masarif : parentMasarif)
			{
				//if selected masraf is  in list or if selected masraf's children are in list then remove them		
				if(
						(
							masarifView != null && 
							masarifView.getMasrafId() != null && 
							masarifView.getMasrafId().compareTo(masarif.getMasrafId()) == 0
						)||
						childIds.contains( ","+masarif.getMasrafId().toString()+"," )
				  )
				{continue;}
				parentMap.put(masarif.getMasrafId().toString(), masarif);
				if(isEnglishLocale())
					parentBankJson+= masarif.getMasrafId().toString()+":\'"+(masarif.getBankNameEn()!=null?masarif.getBankNameEn():"")+"\',";
				else
					parentBankJson+= masarif.getMasrafId().toString()+":\'"+(masarif.getBankNameAr()!= null?masarif.getBankNameAr():"")+"\',";
				
				parentGRPJson+= masarif.getMasrafId().toString()+":\'"+(masarif.getGrpAccNumber()!= null ?masarif.getGrpAccNumber():"")+"\',";
				
				SelectItem item = new SelectItem(masarif.getMasrafId().toString(),masarif.getMasrafName());
				parents.add(item);
			}
			if(parents != null && parents.size() > 0)
			{
				viewMap.put(ALL_PARENTS, parents);
			}
			if(parentMap != null && parentMap.size() > 0)
			{
				viewMap.put(PARENT_MAP, parentMap);
			}
			if( parentBankJson.indexOf(",") >0)
			{
				parentBankJson = parentBankJson.substring(0, parentBankJson.lastIndexOf(','))+"};";
			}
			if( parentGRPJson.indexOf(",") >0)
			{
				parentGRPJson = parentGRPJson.substring(0, parentGRPJson.lastIndexOf(','))+"};";
			}
			setParentBankJson(parentBankJson);
			setParentGRPJson(parentGRPJson);
		}

		
	}

	@SuppressWarnings("unchecked")
	private void checkDataInMapsandQueryString() throws Exception, IllegalAccessException, InstantiationException, 
														InvocationTargetException, NoSuchMethodException 
	{
		checkPageMode();
		if(sessionMap.get(WebConstants.SELECTED_ROW) != null)
		{
			masarifView = (MasarifSearchView) sessionMap.remove(WebConstants.SELECTED_ROW);
			
		}
		else if(request.getParameter("masrafId")!=null)
		{
			MasarifSearchView searchCriteria = new MasarifSearchView();
			searchCriteria.setMasrafId(
										Long.valueOf( request.getParameter("masrafId").toString() )
									  );
			
			List<MasarifSearchView> list = masarifService.getAllMasraf(searchCriteria);
			if( list != null  )
			{
				masarifView  = list.get(0);
			}
		}
		if(masarifView != null)
		{
			setMasarifView(masarifView);
			setMasarifViewClone((MasarifSearchView) BeanUtils.cloneBean(masarifView));
			viewMap.put(WebConstants.SELECTED_ROW, masarifView);
		}

		
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() {

		
		if(
				sessionMap.remove(VIEW_MODE) != null || 
				request.getParameter(VIEW_MODE)!=null 	
		  )	
		{
			viewMap.put(VIEW_MODE, READONLY);
		}
		
	}
	
	public boolean isEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}

	public void setEnglishLocale(boolean englishLocale) {
		this.englishLocale = englishLocale;
	}
	
	@SuppressWarnings("unchecked")
	public void onEndowmentsTab() 
	{
		EndowmentService endowmentService = new EndowmentService();
		try 
		{
			masarifView = getMasarifView();
			if( masarifView == null || masarifView.getMasrafId() == null  )return;
			
	        this.setDataListEndowemntFileAsso(
	        										endowmentService.getEndowmentFileAssosByMasrafId( masarifView.getMasrafId()  )
	        								  );
	        if(	dataListEndowemntFileAsso != null )
	        {
	        	setTotalEndowments( String.valueOf( dataListEndowemntFileAsso.size()  ) );
	        }
	        
		}
		catch (Exception e) 
		{
			logger.LogException("onEndowmentsTab |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			endowmentService = null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void onOpenStatementOfAccountCriteriaPopup()
	{
		try
		{
			masarifView = getMasarifView();
			if(masarifView == null || masarifView.getMasrafId() ==null) return;
			sessionMap.put(WebConstants.Masraf.MasrafStatement, masarifView);
			executeJavaScript( "openStatementofAccountMasrafCriteriaPopup();");
			
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenStatementOfAccountCriteriaPopup|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void onSave() 
	{
		try 
		{
			masarifView = getMasarifView();
			boolean isFirstTime = true;
			if(masarifView.getMasrafId() != null)
			{
				isFirstTime = false;
			}
			if(isValidated(masarifView))
			{
				masarifView = getPopulatedMasraf(masarifView);
				masarifService.persistMasraf(masarifView);
				if(isFirstTime)
				{
					saveSystemComments(MessageConstants.MasrafEvent.ADDED);
					successMessages.add(CommonUtil.getBundleMessage("masraf.label.successMsg.masrafAdded"));
					viewMap.put(WebConstants.SELECTED_ROW, masarifView);
				}
				else
				{
					StringBuilder logEn = new StringBuilder("");
					StringBuilder logAr = new StringBuilder("");
					formatChangeLog(logEn, logAr);
					if(logEn.length() > 0 || logAr.length() > 0)
					{
						saveSystemComments(logEn.toString(),logAr.toString());
					}
					successMessages.add(CommonUtil.getBundleMessage("masraf.label.successMsg.masrafUpdate"));
				}
				if(masarifView.getParentId() == null || masarifView.getParentId().toString().length() <= 0)
				{
					masarifView.setParentBankName(null);
					masarifView.setParentGrp(null);
				}
				else
				{
					if(isEnglishLocale())
					{
							masarifView.setParentBankName(masarifView.getParentBankNameEn());
					}
					else
					{
							masarifView.setParentBankName(masarifView.getParentBankNameAr());
					}
				}
				setMasarifView(masarifView);
				setMasarifViewClone((MasarifSearchView) BeanUtils.cloneBean(masarifView));
				requestHistoryTabClick();
			}
		} catch (Exception e) {
			logger.LogException("onSave |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private MasarifSearchView getPopulatedMasraf(MasarifSearchView masarif) {
		Date today = new Date();
		if(masarif.getMasrafId() != null){
			masarif.setUpdatedBy(CommonUtil.getLoggedInUser());
			masarif.setUpdatedOn(today);
		}
		else{
			masarif.setCreatedBy(CommonUtil.getLoggedInUser());
			masarif.setUpdatedBy(CommonUtil.getLoggedInUser());
			masarif.setCreatedOn(today);
			masarif.setUpdatedOn(today);
			masarif.setDeleted(false);
			masarif.setIsSystem(0L);
		}
		return masarif;
	}

	private boolean isValidated(MasarifSearchView masrafView) throws Exception 
	{
		if (!(isMasrafNameValidated(masrafView.getMasrafName())
				//&& isBankValidated(masrafView.getBankId())
				//&& isBankAccValidated(masrafView.getBankAccountNumber()) 
				//&& isGRPAccValidated(masrafView.getGrpAccNumber())
			  )
			) 
	    {

			errorMessages.add(CommonUtil.getBundleMessage("payments.errMsg.provideMandatoryField"));
			return false;
		}
		
		return true;
	}

	public String onBack() {
			return "searchMasarif";
	}

	public boolean isPopup() {
		return isReadOnly();
	}

	public void setPopup(boolean popup) {
		this.popup = popup;
	}

	public MasarifSearchView getMasarifView() {
		if(viewMap.get(MASRAF_VIEW) != null){
			masarifView = (MasarifSearchView) viewMap.get(MASRAF_VIEW); 
		}
		return masarifView;
	}

	@SuppressWarnings("unchecked")
	public void setMasarifView(MasarifSearchView masarifView) {
		if(masarifView != null){
			viewMap.put(MASRAF_VIEW, masarifView);
		}
		this.masarifView = masarifView;
	}
	private void saveSystemComments(String sysNote) throws Exception 
	{
		if(getMasarifView() != null && getMasarifView().getMasrafId() != null)
		{
			Long masrafId = getMasarifView().getMasrafId();
			CommonUtil.saveSystemComments(WebConstants.Masraf.NOTES_OWNER, sysNote, masrafId);
		}
	}
	private void saveSystemComments(String sysNoteEn,String sysNoteAr) throws Exception 
	{
		if(getMasarifView() != null && getMasarifView().getMasrafId() != null)
		{
			Long masrafId = getMasarifView().getMasrafId();
			NotesController.saveSystemNotesForRequest(WebConstants.Masraf.NOTES_OWNER,sysNoteEn,sysNoteAr,masrafId);
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getAllParents() {
		if(viewMap.get(ALL_PARENTS) != null){
			allParents = (List<SelectItem>) viewMap.get(ALL_PARENTS);
		}
		return allParents;
	}

	public void setAllParents(List<SelectItem> allParents) {
		this.allParents = allParents;
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, MasarifSearchView> getParentMap() {
		if(viewMap.get(PARENT_MAP) != null)
		{
			parentMap = (HashMap<String, MasarifSearchView>) viewMap.get(PARENT_MAP); 
		}
		return parentMap;
	}

	public void setParentMap(HashMap<String, MasarifSearchView> parentMap) {
		this.parentMap = parentMap;
	}

	public String getParentBankJson() {
		if(viewMap.get(BANK_JSON) != null)
			parentBankJson = viewMap.get(BANK_JSON).toString();
		return parentBankJson;
	}

	@SuppressWarnings("unchecked")
	public void setParentBankJson(String parentBankJson) {
		if(parentBankJson != null && parentBankJson.length() > 0){
			viewMap.put(BANK_JSON, parentBankJson);
		}
		this.parentBankJson = parentBankJson;
	}

	public String getParentGRPJson() {
		if(viewMap.get(GRP_JSON) != null)
			parentGRPJson = viewMap.get(GRP_JSON).toString(); 
		return parentGRPJson;
	}

	@SuppressWarnings("unchecked")
	public void setParentGRPJson(String parentGRPJson) {
		if(parentGRPJson != null && parentGRPJson.length() > 0){
			viewMap.put(GRP_JSON, parentGRPJson);
		}
		this.parentGRPJson = parentGRPJson;
	}
	public void requestHistoryTabClick()
	{
		String methodName="requestHistoryTabClick";
    	logger.logInfo(methodName+"|"+"Start..");
    	if(getMasarifView() != null && getMasarifView().getMasrafId() != null)
    	try	
    	{
    	  RequestHistoryController rhc=new RequestHistoryController();
    	  String masrafId= getMasarifView().getMasrafId().toString();
    	  if(masrafId!=null )
    	    rhc.getAllRequestTasksForRequest(WebConstants.Masraf.NOTES_OWNER,masrafId);
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
	}

	public boolean isEnableParentMasraf() {
		if(viewMap.get(READONLY_PARENT_COMBO) == null || !(Boolean)viewMap.get(READONLY_PARENT_COMBO))
			return true;
		return false;
	}

	public void setEnableParentMasraf(boolean enableParentMasraf) {
		this.enableParentMasraf = enableParentMasraf;
	}

	public String getStyleClass() {
		if( !isReadOnly() )
			return "";
		return "READONLY";
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	public boolean isReadOnly() {
		if(viewMap.get(VIEW_MODE) != null && viewMap.get(VIEW_MODE).toString().compareTo(READONLY) == 0){
			return true;
		}
		return false;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public String getParentStyleClass() {
		if(isEnableParentMasraf() && !isReadOnly())
			return "";
		return "READONLY";
	}

	public void setParentStyleClass(String parentStyleClass) {
		this.parentStyleClass = parentStyleClass;
	}

	public MasarifSearchView getMasarifViewClone() {
		if(viewMap.get(MASARIF_VIEW_CLONE) != null){
			masarifViewClone = (MasarifSearchView) viewMap.get(MASARIF_VIEW_CLONE);
		}
		return masarifViewClone;
	}

	@SuppressWarnings("unchecked")
	public void setMasarifViewClone(MasarifSearchView masarifViewClone) {
		if(masarifViewClone != null && masarifViewClone.getMasrafId() != null){
			viewMap.put(MASARIF_VIEW_CLONE, masarifViewClone);
		}
		this.masarifViewClone = masarifViewClone;
	}	
	public void formatChangeLog(StringBuilder logEn,StringBuilder logAr){
		Locale localeAr = new Locale("ar");
		Locale localeEn = new Locale("en");
		ResourceBundle bundleAr = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(),localeAr);
		ResourceBundle bundleEn = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(), localeEn);
		
		masarifView = getMasarifView();
		masarifViewClone = getMasarifViewClone();
		if(masarifView != null && masarifView.getMasrafId() != null && masarifViewClone != null && masarifViewClone.getMasrafId() != null)
		{
			
			if(
				!masarifView.getMasrafName().equalsIgnoreCase(masarifViewClone.getMasrafName()) && 
				 masarifViewClone.getMasrafName()!= null
			  ) //name changed
			{
				logEn.append(MessageFormat.format(bundleEn.getString("masraf.changeLog.masrafNameChanged"), masarifViewClone.getMasrafName(),masarifView.getMasrafName())+",");
				logAr.append(MessageFormat.format(bundleAr.getString("masraf.changeLog.masrafNameChanged"), masarifViewClone.getMasrafName(),masarifView.getMasrafName())+",");
			}
			
			if (
					!masarifView.getBankId().equalsIgnoreCase(masarifViewClone.getBankId()) && 
					 masarifViewClone.getBankId() != null
			   ) // name bank
			{
				bankMap = getBankMap();
				if (bankMap != null && bankMap.containsKey(Long.parseLong(masarifView.getBankId()))) 
				{
					BankView bank = null;
					bank = bankMap.get(Long.parseLong(masarifView.getBankId()));
					
					logEn.append(MessageFormat.format(bundleEn.getString("masraf.changeLog.masrafBankChanged"),masarifViewClone.getBankNameEn(),bank.getBankEn())+",");
					logAr.append(MessageFormat.format(bundleAr.getString("masraf.changeLog.masrafBankChanged"),masarifViewClone.getBankNameAr(),bank.getBankAr())+",");
				}
			}
			
			if(
				! masarifView.getBankAccountNumber().equalsIgnoreCase(masarifViewClone.getBankAccountNumber()) && 
				masarifViewClone.getBankAccountNumber() != null 
			  ) //Bank Account changed
			{
				logEn.append(MessageFormat.format(bundleEn.getString("masraf.changeLog.masrafBankAccChanged"), masarifViewClone.getBankAccountNumber(),masarifView.getBankAccountNumber())+",");
				logAr.append(MessageFormat.format(bundleAr.getString("masraf.changeLog.masrafBankAccChanged"), masarifViewClone.getBankAccountNumber(),masarifView.getBankAccountNumber())+",");
			}
			
			if(
					! masarifView.getGrpAccNumber().equalsIgnoreCase(masarifViewClone.getGrpAccNumber()) && 
					masarifViewClone.getGrpAccNumber() != null
			  ) //GRP changed
			{
				logEn.append(MessageFormat.format(bundleEn.getString("masraf.changeLog.masrafGRPChanged"), masarifViewClone.getGrpAccNumber(),masarifView.getGrpAccNumber())+",");
				logAr.append(MessageFormat.format(bundleAr.getString("masraf.changeLog.masrafGRPChanged"), masarifViewClone.getGrpAccNumber(),masarifView.getGrpAccNumber())+",");
			}
			if( 
					masarifView.getParentId() != null && 
					masarifViewClone.getParentId() != null
			  )
			{
				if(! masarifView.getParentId().equalsIgnoreCase(masarifViewClone.getParentId())) //GRP changed
				{
					parentMap = getParentMap();
					if(parentMap.containsKey(masarifView.getParentId()))
					{
						MasarifSearchView parentMasraf = parentMap.get(masarifView.getParentId()); 
						logEn.append(MessageFormat.format(bundleEn.getString("masraf.changeLog.masrafParentChanged"), masarifViewClone.getParentName(),parentMasraf.getMasrafName()));
						logAr.append(MessageFormat.format(bundleAr.getString("masraf.changeLog.masrafParentChanged"), masarifViewClone.getParentName(),parentMasraf.getMasrafName()));
					}
				}
			}
			else
			{
				parentMap = getParentMap();
				if(StringHelper.isEmpty(masarifView.getParentId()) && StringHelper.isNotEmpty(masarifViewClone.getParentId()))
				{
					logEn.append(MessageFormat.format(bundleEn.getString("masraf.changeLog.masrafParentAnyToNone"), masarifViewClone.getParentName()));
					logAr.append(MessageFormat.format(bundleAr.getString("masraf.changeLog.masrafParentAnyToNone"), masarifViewClone.getParentName()));
				}
				else if (masarifView.getParentId() != null)
				{
					MasarifSearchView parentMasraf = parentMap.get(masarifView.getParentId()); 
					logEn.append(MessageFormat.format(bundleEn.getString("masraf.changeLog.masrafParentNoneToAny"),parentMasraf.getMasrafName()));
					logAr.append(MessageFormat.format(bundleAr.getString("masraf.changeLog.masrafParentNoneToAny"),parentMasraf.getMasrafName()));
				}
				
			}
		}
	}

	@SuppressWarnings("unchecked")
	public HashMap<Long, BankView> getBankMap() {
		if(viewMap.get(BANK_MAP) != null)
			bankMap = (HashMap<Long, BankView>) viewMap.get(BANK_MAP);
		return bankMap;
	}

	@SuppressWarnings("unchecked")
	public void setBankMap(HashMap<Long, BankView> bankMap) {
		if(bankMap != null && bankMap.size() > 0){
			viewMap.put(BANK_MAP,bankMap);
		}
		this.bankMap = bankMap;
	}

	public boolean isShowActivitylogTab() {
		if(viewMap.containsKey(WebConstants.SELECTED_ROW))
			return true;
		return false;
	}

	public void setShowActivitylogTab(boolean isShowActivitylogTab) {
		this.isShowActivitylogTab = isShowActivitylogTab;
	}

	@SuppressWarnings("unchecked")
	public List<EndFileAsso> getDataListEndowemntFileAsso() {
		if( viewMap.get("dataListEndowemntFileAsso") !=  null )
		{
			dataListEndowemntFileAsso = (ArrayList<EndFileAsso>)viewMap.get("dataListEndowemntFileAsso"); 
		}
		return dataListEndowemntFileAsso;
	}

	@SuppressWarnings("unchecked")
	public void setDataListEndowemntFileAsso(List<EndFileAsso> dataListEndowemntFileAsso) 
	{
		this.dataListEndowemntFileAsso = dataListEndowemntFileAsso;
		if( this.dataListEndowemntFileAsso != null)
		{
			viewMap.put("dataListEndowemntFileAsso", this.dataListEndowemntFileAsso);
		}
	}

	public HtmlDataTable getDataTableFileAsso() {
		return dataTableFileAsso;
	}

	public void setDataTableFileAsso(HtmlDataTable dataTableFileAsso) {
		this.dataTableFileAsso = dataTableFileAsso;
	}
	@SuppressWarnings("unchecked")
	public String getTotalEndowments() {
		if(  viewMap.get("totalEndowments") != null )
		{
			totalEndowments =  viewMap.get("totalEndowments").toString() ;
		}
		return totalEndowments;
	}
	@SuppressWarnings("unchecked")
	public void setTotalEndowments(String totalEndowments) {
		this.totalEndowments = totalEndowments;
		viewMap.put("totalEndowments", this.totalEndowments);
	}
}
