package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.MemsFollowupView;

public class DisbursementDetailsTabBean extends AbstractMemsBean
{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2526741634539592008L;
	private HtmlDataTable dataTable;
	private List<DisbursementDetailsView> list = new ArrayList<DisbursementDetailsView>();
	DisbursementService service ;
    public DisbursementDetailsTabBean() 
	{
		dataTable = new HtmlDataTable();
		service   = new DisbursementService();
	}
	
	@Override
	@SuppressWarnings( "unchecked" )
	public void init() 
	{
		try	
		{	
			 loadData();
		}
		catch (Exception exception) 
		{
			logger.LogException( "init --- CRASHED --- ", exception);
		}
	}
	@SuppressWarnings( "unchecked" )
	private void loadData() throws Exception
	{
		if( viewMap.get("memsFollowupView") != null )
		{
			MemsFollowupView followUpView = (  MemsFollowupView  )viewMap.get("memsFollowupView");
			if(followUpView.getDisDetailsIds()!=null && followUpView.getDisDetailsIds().size() >0  )
			{
				setList( service.getDisbursementDetails( followUpView.getDisDetailsIds() ) );
			}
		}
		
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	@SuppressWarnings("unchecked")
	public List<DisbursementDetailsView> getList() {
		if( ( list== null || list.size() <= 0 ) && 
		    viewMap.get("disbursementDetailsList") != null )
		{
			list = (ArrayList<DisbursementDetailsView>)viewMap.get("disbursementDetailsList") ;
		}
		return list;
	}
	@SuppressWarnings("unchecked")
	public void setList(List<DisbursementDetailsView> list) {
	
		this.list = list;
		if( this.list !=null )
		{
			viewMap.put("disbursementDetailsList",this.list); 
		}
	}

}
