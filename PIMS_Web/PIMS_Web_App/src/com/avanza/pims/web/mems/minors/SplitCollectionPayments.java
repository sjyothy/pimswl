package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.CollectionProcedureService;
import com.avanza.pims.ws.vo.OpenFileDistribute;
import com.avanza.ui.util.ResourceUtil;

public class SplitCollectionPayments extends AbstractMemsBean 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8948793371607545289L;
	private ArrayList<String> errorMessages;
	private ArrayList<String> successMessages;
	OpenFileDistribute openFileDistribute ;
	private String originalAmount;
	@SuppressWarnings("unchecked")
	@Override
	public void init()
	{
		super.init();
		try
		{
			if( isPostBack() ){return;}
			
			getValuesFromSession();
		}
		catch( Exception e)
		{
			logger.LogException(" init--- CRASHED --- ", e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void getValuesFromSession() throws Exception
	{
		if( sessionMap.get( WebConstants.OpenFileDistributePopUpKeys.OBJECT ) != null )
		{
			
		OpenFileDistribute obj = (OpenFileDistribute) sessionMap.remove( WebConstants.OpenFileDistributePopUpKeys.OBJECT );
		originalAmount = obj.getOriginalAmount().toString();
		obj.setAmount(null);
		this.setOpenFileDistribute( obj )	;
		}
	}
	@SuppressWarnings( "unchecked" )
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
	}
	
	@SuppressWarnings( "unchecked" )
	private boolean hasError() throws Exception
	{
		boolean hasError =false;
		hasError = this.getOpenFileDistribute().getAmount()==null || this.getOpenFileDistribute().getAmount().compareTo(0d)==0;
		return hasError;
	}
	
	
	@SuppressWarnings( "unchecked" )
    public void onSplit()
	{
	  errorMessages = new ArrayList<String>(0);
	  successMessages = new ArrayList<String>();
	  try
	  {
		  OpenFileDistribute row = this.getOpenFileDistribute();
		  if( hasError() )return;
		  
		  CollectionProcedureService.onSplit(row, getLoggedInUserId() );
		  executeJavascript("javaScript:closeWindowSubmit();");
	  }
	  catch( Exception e )
	  {
		  logger.LogException( "onSplit| --- CRASHED --- ", e );
  		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	  }  
	  finally
	  {
		  if( errorMessages != null && errorMessages.size() > 0 )
		  {
		   viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR, errorMessages);
		  }
		  else if( successMessages != null && successMessages .size() > 0 )
		  {
		   viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_SUCC, successMessages );
		  }
	  }
		
	}
	
	@SuppressWarnings( "unchecked" )
	public OpenFileDistribute getOpenFileDistribute() {
		if ( viewMap.get( WebConstants.OpenFileDistributePopUpKeys.OBJECT ) != null )
			
			openFileDistribute= ( OpenFileDistribute )viewMap.get(WebConstants.OpenFileDistributePopUpKeys.OBJECT );
		return openFileDistribute;
	}
	@SuppressWarnings( "unchecked" )
	public void setOpenFileDistribute(OpenFileDistribute openFileDistribute) {
		this.openFileDistribute = openFileDistribute;
	
		if( this.openFileDistribute!= null  )
			viewMap.put(WebConstants.OpenFileDistributePopUpKeys.OBJECT,this.openFileDistribute);
	}

	public String getErrorMessages()
	{

		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	public String getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(String originalAmount) {
		this.originalAmount = originalAmount;
	}




	
}
