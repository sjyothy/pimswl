package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.entity.CostCenterIdentifier;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.CostCenterIdentifierService;
import com.avanza.ui.util.ResourceUtil;
/**
 * @author Kamran Ahmed
 */
public class CostCenterManageBean extends AbstractMemsBean {
	private static final long serialVersionUID = 1L;
	
	private List<CostCenterIdentifier> identifiers;
	private HtmlSelectOneMenu cboOwnershipTypeId;
	private HtmlInputText txtIdentifierLetter;
	private HtmlDataTable tblcostCenter;
	private CostCenterIdentifier costCenterPojo;
	private CostCenterIdentifierService ccService;
	private boolean englishLocale;
	private static class Keys{
		public static String IDENTIFIERS="IDENTIFIERS";
		public static String SELECTED_COST_CENTER="SELECTED_COST_CENTER";
		public static String PAGE_MODE="PAGE_MODE";
		public static String PAGE_MODE_ADD="PAGE_MODE_ADD";
		public static String PAGE_MODE_UPDATE="PAGE_MODE_UPDATE";
	}
	public CostCenterManageBean() {
		identifiers = new ArrayList<CostCenterIdentifier>();
		cboOwnershipTypeId =  new HtmlSelectOneMenu();
		txtIdentifierLetter = new HtmlInputText();
		costCenterPojo = new CostCenterIdentifier();
		ccService = new CostCenterIdentifierService();
	}

	public void init()
	{
		super.init();
		try
		{
			if (!isPostBack()) 
			{
				
			}
		}
		catch (Exception e) {
		logger.LogException("init crashed", e);
		errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	public void onDelete(){
		final String  methodName="onDelete()";
		logger.logInfo("Starting "+methodName);
		try{
			costCenterPojo = (CostCenterIdentifier) tblcostCenter.getRowData();
			ccService.deleteCostCenterIdentifier(costCenterPojo);
			successMessages.add(CommonUtil.getBundleMessage("successMsg.costCenterDelete"));
			identifiers.remove(costCenterPojo);
		}
		catch (Exception e) {
			logger.logError(methodName+" crashed");
		}
		logger.logInfo("Ending "+methodName);
	}
	public void onSearch(){
		final String  methodName="onSearch()";
		logger.logInfo("Starting "+methodName);
		try{
			clearSearchResult();
			if(isCriteriaValidated()){
				fillCostCenter(costCenterPojo);
				identifiers = ccService.getAllCostIdentifiers(costCenterPojo);
				if(identifiers != null && identifiers.size() > 0){
					setIdentifiers(identifiers);
				}
				else{
					errorMessages.add(CommonUtil.getBundleMessage("commons.noRecordFound"));
				}
			}
		}
		catch (Exception e) {
			logger.logError(methodName+" crashed");
		}
		logger.logInfo("Ending "+methodName);
	}
	

	private void clearSearchResult() {
		if(identifiers != null && identifiers.size() > 0){
			identifiers.clear();
		}
	}

	private void fillCostCenter(CostCenterIdentifier costCenter) {
		final String  methodName="fillCostCenter()";
		logger.logInfo("Starting "+methodName);
		try{
			if(txtIdentifierLetter.getValue() != null && StringHelper.isNotEmpty(txtIdentifierLetter.getValue().toString())){
				costCenter.setIdentifierLetter(txtIdentifierLetter.getValue().toString());
			}
			
			if(cboOwnershipTypeId.getValue() != null && (cboOwnershipTypeId.getValue().toString().compareTo("-1") != 0)){
				costCenter.setOwnershiptTypeId(Long.valueOf(cboOwnershipTypeId.getValue().toString()));
			}
		}
		catch (Exception e) {
			logger.logError(methodName+" crashed");
		}
		logger.logInfo("Ending "+methodName);
	}

	private boolean isCriteriaValidated() {
		boolean isValidated = true;
		if(txtIdentifierLetter.getValue() == null && (cboOwnershipTypeId.getValue() == null || cboOwnershipTypeId.getValue().toString().compareTo("-1") == 0)){
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.criteriaSpecify"));
			isValidated = false;
		}
		return isValidated;
	}
	@SuppressWarnings("unchecked")
	public void openAddPopup()
	{
		final String  methodName ="openAddPopup";
		logger.logInfo(methodName+"|"+"Start..");
		try	
    	{
			sessionMap.put(Keys.PAGE_MODE, Keys.PAGE_MODE_ADD);
			executeJavascript("openAddUpdatePopup();");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
	}
	@SuppressWarnings("unchecked")
	public void openUpdatePopup()
	{
		final String  methodName ="openUpdatePopup";
		logger.logInfo(methodName+"|"+"Start..");
		CostCenterIdentifier selectedCostCenter =  (CostCenterIdentifier) tblcostCenter.getRowData();
		try	
    	{
			if(selectedCostCenter != null)
				sessionMap.put(Keys.SELECTED_COST_CENTER, selectedCostCenter);
			sessionMap.put(Keys.PAGE_MODE, Keys.PAGE_MODE_UPDATE);
			
			executeJavascript("openAddUpdatePopup();");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	public void prerender() {
		super.prerender();
	}

	@SuppressWarnings("unchecked")
	public List<CostCenterIdentifier> getIdentifiers() {
		if(viewMap.get(Keys.IDENTIFIERS) != null){
			identifiers = (List<CostCenterIdentifier>) viewMap.get(Keys.IDENTIFIERS);
		}
		return identifiers;
	}

	@SuppressWarnings("unchecked")
	public void setIdentifiers(List<CostCenterIdentifier> identifiers) {
		if(identifiers != null && identifiers.size() > 0){
			viewMap.put(Keys.IDENTIFIERS,identifiers);
		}
		this.identifiers = identifiers;
	}

	public HtmlSelectOneMenu getCboOwnershipTypeId() {
		return cboOwnershipTypeId;
	}

	public void setCboOwnershipTypeId(HtmlSelectOneMenu cboOwnershipTypeId) {
		this.cboOwnershipTypeId = cboOwnershipTypeId;
	}

	public HtmlInputText getTxtIdentifierLetter() {
		return txtIdentifierLetter;
	}

	public void setTxtIdentifierLetter(HtmlInputText txtIdentifierLetter) {
		this.txtIdentifierLetter = txtIdentifierLetter;
	}

	public HtmlDataTable getTblcostCenter() {
		return tblcostCenter;
	}

	public void setTblcostCenter(HtmlDataTable tblcostCenter) {
		this.tblcostCenter = tblcostCenter;
	}

	public CostCenterIdentifier getCostCenterPojo() {
		return costCenterPojo;
	}

	public void setCostCenterPojo(CostCenterIdentifier costCenterPojo) {
		this.costCenterPojo = costCenterPojo;
	}

	public boolean isEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}

	public void setEnglishLocale(boolean englishLocale) {
		this.englishLocale = englishLocale;
	}
}
