package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.minors.SocialProgramsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.SocialProgramService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.SessionDetailView;
import com.avanza.pims.ws.vo.mems.ProgramActionListView;
import com.avanza.pims.ws.vo.mems.ProgramPurposesView;
import com.avanza.ui.util.ResourceUtil;

public class ProgramActionList extends AbstractMemsBean
{
	private static final long serialVersionUID = 8517238816949483790L;
	
	private List<ProgramActionListView> list;
	private HtmlDataTable dataTable;
	private ProgramActionListView  programActionList;
	private String listLoadedFromDb;
	private SocialProgramsBean parentBean;	
	public ProgramActionList() 
	{	
		programActionList = new ProgramActionListView() ;
		list = new ArrayList<ProgramActionListView>(0);
		
		dataTable = new HtmlDataTable();
	}
	
	@Override
	public void init() 
	{
		String METHOD_NAME = "init()";
		errorMessages = new ArrayList<String>(0);
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			parentBean.setErrorMessages( errorMessages );
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws PimsBusinessException
	{
		
		
		
		// PURPOSE VIEW
		if ( viewMap.get( WebConstants.SocialProgramActionList.ACTION_VO ) != null )
		{
			programActionList = (ProgramActionListView) viewMap.get( WebConstants.SocialProgramActionList.ACTION_VO );
		}		
		
		// PURPOSE VIEW LIST
		if ( viewMap.get( WebConstants.SocialProgramActionList.ACTION_DATA_LIST ) != null )
		{
			list = ( List<ProgramActionListView>) viewMap.get( WebConstants.SocialProgramActionList.ACTION_DATA_LIST );
		}		
		parentBean = (SocialProgramsBean)getBean( "pages$SocialProgram" );
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		
		// PURPOSE VIEW
		if ( programActionList != null  )
		{
			viewMap.put( WebConstants.SocialProgramActionList.ACTION_VO , programActionList );
		}
		
		// LIST
		if ( list != null )
		{
			viewMap.put( WebConstants.SocialProgramActionList.ACTION_DATA_LIST, list );
		}
	}	
	
	@SuppressWarnings("unchecked")
	public void onLoadActionList( Long programId )throws Exception
	{
		final String METHOD_NAME = "onLoadPurposes()";
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( this.getListLoadedFromDb()== null || this.getListLoadedFromDb().equals("0") )
			{
				
				list =  new SocialProgramService().loadProgramActionList(  programId  );
				this.setListLoadedFromDb( "1" );
				updateValuesToMaps();
			}
	}
	@SuppressWarnings("unchecked")
	public String onSave()
	{
		final String METHOD_NAME = "onSave()";
		errorMessages = new ArrayList<String>(0);
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( isValid() )
			{
				ProgramActionListView newVo = new ProgramActionListView();
				newVo.setCreatedBy(getLoggedInUserId() );
				newVo.setCreatedEn(getLoggedInUserObj().getFullName());
				newVo.setCreatedAr( getLoggedInUserObj().getSecondaryFullName() );
				newVo.setCreatedOn( new Date() );
				newVo.setIsDeleted( "0" );
				newVo.setDescription(  programActionList.getDescription() );
				list.add(0, newVo);
				//saving it in viewMap to retrieve it from the parent bean later , for maturity date
				programActionList.setDescription(  "" ); 
				
				updateValuesToMaps();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			parentBean.setErrorMessages( errorMessages );
		}
		
		return null;
	}
	
	public String onSessionDetailDelete()
	{
		updateValuesToMaps();
		return null;
	}
	@SuppressWarnings("unchecked")
	public void onDelete( )
	{
	    final String METHOD_NAME = "onDelete()";
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			updateValuesFromMaps();
			ProgramActionListView item = ( ProgramActionListView )dataTable.getRowData(); 
			
			if(  item.getActionListId()!= null)
			{
			item.setIsDeleted( "1" );
			}
			else
			{
			 list.remove(item);
			}
			updateValuesToMaps();
			logger.logInfo( METHOD_NAME + " --- Finish --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		parentBean.setErrorMessages( errorMessages );
		}
		
	}

	private boolean isValid() throws Exception 
	{
		boolean valid = true;

		// VALIDATION CHECKS STARTS HERE
		if( programActionList.getDescription() == null || programActionList.getDescription().trim().length() <=0 )
		{
		 errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.actionDescReq"));
		 parentBean.setErrorMessages( errorMessages );
		 valid = false;
		}
		// VALIDATION CHECKS ENDS HERE
		return valid;
	}

	public List<ProgramActionListView> getList() {
		return list;
	}

	public void setList(List<ProgramActionListView> list) {
		this.list = list;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public ProgramActionListView getProgramActionList() {
		return programActionList;
	}

	public void setProgramActionList(ProgramActionListView programActionList) {
		this.programActionList = programActionList;
	}

	@SuppressWarnings( "unchecked" )
	public String getListLoadedFromDb() {
		if( viewMap.get( "actionlistLoadedFromDb" ) != null )
		{
			listLoadedFromDb = viewMap.get( "actionlistLoadedFromDb" ).toString(); 
		}
		return listLoadedFromDb;
	}
    @SuppressWarnings( "unchecked" )
	public void setListLoadedFromDb(String listLoadedFromDb) {
		this.listLoadedFromDb = listLoadedFromDb;
		if( this.listLoadedFromDb != null )
		{
			
			viewMap.put( "actionlistLoadedFromDb",this.listLoadedFromDb ); 
		}
	}
	


}
