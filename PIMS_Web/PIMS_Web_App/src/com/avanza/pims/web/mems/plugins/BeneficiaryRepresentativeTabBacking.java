package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.data.ApplicationContext;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.BeneficiaryRepresentative;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.InheritanceBeneficiary;
import com.avanza.pims.entity.Person;
import com.avanza.pims.entity.Relationship;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.BeneficiaryRepresentativeService;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.ui.util.ResourceUtil;

public class BeneficiaryRepresentativeTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private List< BeneficiaryRepresentative> dataList;
	
	private HtmlDataTable dataTable;
	BeneficiaryRepresentativeService service;
	private String selectOneInheritanceBeneficiaryId;
	private String relationshipIdString;
	private String relationshipDesc;
	private List<SelectItem> inheritanceBeneficiariesList = new ArrayList<SelectItem>();
	private String tabMode;
	BeneficiaryRepresentative pageView;
	private Long inheritanceFileId;
	private String hdnrepresentativePersonId;
	private String hdnrepresentativePersonName;
	public BeneficiaryRepresentativeTabBacking() 
	{
		service = new BeneficiaryRepresentativeService();
		dataTable        = new HtmlDataTable();
		dataList         = new ArrayList<BeneficiaryRepresentative>();
		tabMode          = Constant.EndowmentFile.MODE_READ_ONLY;
		pageView         = new BeneficiaryRepresentative();
		dataTable        =  new HtmlDataTable();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put("BENEF_REP_ERRORS", errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put("BENEF_REP_SUCCESS", successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			tabMode = (String) viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		if( viewMap.get( "BeneficiaryRepresentativeList" ) != null )
		{
			dataList = (ArrayList<BeneficiaryRepresentative>)viewMap.get( "BeneficiaryRepresentativeList" );
		}
		if( viewMap.get( "BeneficiaryRepresentative" ) != null )
		{
			pageView = (BeneficiaryRepresentative)viewMap.get( "BeneficiaryRepresentative" );
		}
		if( viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID)!= null )
		{
			inheritanceFileId = Long.valueOf( viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID ).toString() );
		}
		if( viewMap.get("inheritanceBeneficiariesList")!= null )
		{
			inheritanceBeneficiariesList= (ArrayList<SelectItem>)viewMap.get("inheritanceBeneficiariesList");
		}
		
		if( viewMap.get("relationshipDesc")!= null){
			relationshipDesc = viewMap.get("relationshipDesc").toString();
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, tabMode );
		}
		if( dataList != null  )
		{
			viewMap.put( "BeneficiaryRepresentativeList", dataList );
		}
		if( pageView != null )
		{
			viewMap.put( "BeneficiaryRepresentative", pageView );
		}
		if( inheritanceFileId != null )
		{
			viewMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_ID , inheritanceFileId);
		}
		if(inheritanceBeneficiariesList != null){
			viewMap.put("inheritanceBeneficiariesList", inheritanceBeneficiariesList);
			
		}
		if( relationshipDesc != null ){
			viewMap.put("relationshipDesc",relationshipDesc);
		}
	}	
	
	

	public void populateTab( Long inheritanceFileId ) throws Exception
	{
		updateValuesFromMaps();
		this.inheritanceFileId = inheritanceFileId;
	
		loadDataList();
		setTotalRows( dataList.size() );
		loadBeneficiaryList(); 
		updateValuesToMaps();
	}
	
	private void loadBeneficiaryList()throws Exception{
		inheritanceBeneficiariesList = new ArrayList<SelectItem>();
		List<InheritanceBeneficiary> list = InheritanceFileService.getInheritanceBeneficiaryByFileId(this.inheritanceFileId);
		
		for (InheritanceBeneficiary obj: list) {
			inheritanceBeneficiariesList.add( new SelectItem(
																	obj.getInheritanceBeneficiaryId().toString(),
																	obj.getBeneficiary().getFullName()
			
			
															)
											);
		}
	}
	private void setPageDataDefaults()throws Exception
	{
		pageView = new BeneficiaryRepresentative();
		pageView.setCreatedBy(getLoggedInUserId());
		pageView.setCreatedOn(new Date());
		pageView.setUpdatedBy(getLoggedInUserId());
		pageView.setUpdatedOn(new Date());
		pageView.setIsDeleted(0l);
		
	}

	private void loadDataList() throws Exception {

		Map<String,Object> searchCriteriaMap = new HashMap<String,Object>();
		searchCriteriaMap.put("inheritanceFileId", inheritanceFileId);
		dataList = new ArrayList<BeneficiaryRepresentative>();
		dataList = BeneficiaryRepresentativeService.listRepresentatives(searchCriteriaMap);
	}

	
	@SuppressWarnings("unchecked")
	private boolean hasErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		
		if( hdnrepresentativePersonId == null || hdnrepresentativePersonId.trim().length() <=0) {
			errorMessages.add(CommonUtil.getBundleMessage("beneficiaryRepresentative.msg.representativerequired"));
			return true;
		}

		if( selectOneInheritanceBeneficiaryId== null || selectOneInheritanceBeneficiaryId.equals("-1") ) {
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.beneficiaryReq"));
			return true;
		}
		if( relationshipIdString== null || relationshipIdString.equals("-1") ) {
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.benRelationShipRequired"));
			return true;
		}
		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onAdd()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			updateValuesFromMaps();
			setPageDataDefaults();
			if(hasErrors())return;

			Long personId = Long.valueOf(hdnrepresentativePersonId);
			Person representative = EntityManager.getBroker().findById(Person.class, personId);
			pageView.setRepresentative(representative);
			
			InheritanceBeneficiary beneficiary = new InheritanceBeneficiary();
			beneficiary.setInheritanceBeneficiaryId(Long.valueOf(selectOneInheritanceBeneficiaryId));
			pageView.setInheritanceBeneficiary(beneficiary);
			
			Relationship relationship = new Relationship();
			relationship.setRelationshipId(Long.valueOf( relationshipIdString));
			pageView.setRelationship(relationship);
			
			pageView.setRelationshipDesc(relationshipDesc.trim());
			
			BeneficiaryRepresentativeService.persistBeneficiaryRepresentative(pageView);
			
			selectOneInheritanceBeneficiaryId ="-1";
			relationshipIdString="-1";
			
			loadDataList();
			updateValuesToMaps();
		}
		catch ( Exception e )
		{
			logger.LogException( "onAdd--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put("BENEF_REP_ERRORS", errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put("BENEF_REP_SUCCESS", successMessages );
			}
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			updateValuesFromMaps();
			Long personId = Long.valueOf(hdnrepresentativePersonId);
			Person person = EntityManager.getBroker().findById(Person.class, personId);
			
			pageView.setRepresentative(person);
			updateValuesToMaps();
		}
		catch ( Exception e )
		{
			logger.LogException( "onMessageFromSearchPerson--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put("BENEF_REP_ERRORS", errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put("BENEF_REP_SUCCESS", successMessages );
			}
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void ononMessageFromSearchPersonForRepresentative()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
		}
		catch ( Exception e )
		{
			logger.LogException( "onMessageFromSearchPersonForRepresentative--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put("BENEF_REP_ERRORS", errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put("BENEF_REP_SUCCESS", successMessages );
			}
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onDelete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			BeneficiaryRepresentative row = ( BeneficiaryRepresentative  )dataTable.getRowData();
			updateValuesFromMaps();
			row.setIsDeleted(1l);
			row.setUpdatedBy(getLoggedInUserId());
			BeneficiaryRepresentativeService.persistBeneficiaryRepresentative(row);
			loadDataList();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDelete--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put("BENEF_REP_ERRORS", errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put("BENEF_REP_SUCCESS", successMessages );
			}
		}
		
	}
  
	
		
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}

	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<BeneficiaryRepresentative> getDataList() {
		return dataList;
	}

	public void setDataList(List<BeneficiaryRepresentative> dataList) {
		this.dataList = dataList;
	}

	public String getRepresentativePersonId() {
		return hdnrepresentativePersonId;
	}

	public void setRepresentativePersonId(String representativePersonId) {
		this.hdnrepresentativePersonId = representativePersonId;
	}

	public String getSelectOneInheritanceBeneficiaryId() {
		return selectOneInheritanceBeneficiaryId;
	}

	public void setSelectOneInheritanceBeneficiaryId(
			String selectOneInheritanceBeneficiaryId) {
		this.selectOneInheritanceBeneficiaryId = selectOneInheritanceBeneficiaryId;
	}

	public List<SelectItem> getInheritanceBeneficiariesList() {
		return inheritanceBeneficiariesList;
	}

	public void setInheritanceBeneficiariesList(
			List<SelectItem> inheritanceBeneficiariesList) {
		this.inheritanceBeneficiariesList = inheritanceBeneficiariesList;
	}

	public BeneficiaryRepresentative getPageView() {
		return pageView;
	}

	public void setPageView(BeneficiaryRepresentative pageView) {
		this.pageView = pageView;
	}

	public Long getInheritanceFileId() {
		return inheritanceFileId;
	}

	public void setInheritanceFileId(Long inheritanceFileId) {
		this.inheritanceFileId = inheritanceFileId;
	}

	public String getHdnrepresentativePersonId() {
		return hdnrepresentativePersonId;
	}

	public void setHdnrepresentativePersonId(String hdnrepresentativePersonId) {
		this.hdnrepresentativePersonId = hdnrepresentativePersonId;
	}

	public String getRelationshipIdString() {
		return relationshipIdString;
	}

	public void setRelationshipIdString(String relationshipIdString) {
		this.relationshipIdString = relationshipIdString;
	}

	public String getRelationshipDesc() {
		return relationshipDesc;
	}

	public void setRelationshipDesc(String relationshipDesc) {
		this.relationshipDesc = relationshipDesc;
	}

	public String getHdnrepresentativePersonName() {
		return hdnrepresentativePersonName;
	}

	public void setHdnrepresentativePersonName(String hdnrepresentativePersonName) {
		this.hdnrepresentativePersonName = hdnrepresentativePersonName;
	}
	


}
