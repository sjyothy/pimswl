package com.avanza.pims.web.mems;

import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.bpel.proxy.MEMSBlockingBPELPortClient;
import com.avanza.pims.entity.BlockingDetails;
import com.avanza.pims.entity.BlockingReason;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.InheritanceBeneficiary;
import com.avanza.pims.entity.InheritanceFile;
import com.avanza.pims.entity.Person;
import com.avanza.pims.entity.Request;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationDetails;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.BlockingAmountService;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.PersonalAccountTrxService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.ui.util.ResourceUtil;


public class BlockingRequestBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(BlockingRequestBean.class);
	
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_RESUBMITTED = "PAGE_MODE_RESUBMITTED";
	private static final String PAGE_MODE_APPROVAL_REQUIRED = "APPROVAL_REQUIRED";
	private static final String PAGE_MODE_APPROVED = "APPROVED";
	private static final String PAGE_MODE_REVIEW_REQUIRED= "REVIEW_REQUIRED";
	
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_DETAILS   = "paymentsTab";
	private static final String TAB_REVIEW  = "reviewTab";
	private static final String TAB_APPLICANTS   = "applicationTab";

	private static final String PERSON_BALANCE_MAP			= "personBalance";
	private static final String PERSON_BLOCKING_MAP			= "personBlocking";
	private String pageTitle;
    private String pageMode;
    private String hdnPersonId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;
    
    private String txtRemarks;
    
	private List<BlockingDetails> blockingList = new ArrayList<BlockingDetails>();
	private List<SelectItem> blockForList = new ArrayList<SelectItem>();
	private HtmlInputText accountBalance = new HtmlInputText();
	private HtmlInputText blockingBalance = new HtmlInputText();
	private HtmlInputText availableBalance = new HtmlInputText();
	private String blockingPersonId ;
	private BlockingAmountService blockingService = new BlockingAmountService();
	private Request request ;
	private BlockingDetails blocking = new BlockingDetails();
	private InheritanceFileView inheritanceFile;
	private String selectOneBlockFor;
	private String selectOneReason;
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();    
	private HtmlDataTable dataTable;
    private HtmlSelectOneMenu cmbReviewGroup = new HtmlSelectOneMenu();
    
	public BlockingRequestBean(){}
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.BlockingRequest.PROCEDURE_BLOCKING_REQUEST );
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.BlockingRequest.PROCEDURE_BLOCKING_REQUEST );
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		viewMap.put("noteowner", WebConstants.REQUEST);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		loadAttachmentsAndComments( null );
		
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
		  getDataFromTaskList();
		}
		else if ( getRequestMap().get( WebConstants.REQUEST_VIEW) != null )
		{
		  getDataFromRequest();
		  
		}
		else if (getRequestMap().get( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) != null) 
		{
			getDataFromFileSearch();
		}
		loadBlockForList();
		setBlockingDetailsForAddition();
		updateValuesFromMap();
		getPageModeFromRequestStatus();
	}
	@SuppressWarnings("unchecked")
	public void prerender()
	{
		try
		{
			if( !isPostBack() )
			{
				onBlockForChange();
			}
			if(viewMap.containsKey(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES))
			{
				errorMessages.addAll( (ArrayList<String>)viewMap.get(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES) );
				viewMap.remove(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES);
				
			}
			if (  getStatusReviewRequired() )
			{
				viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
			}
			
			if( 	!isTaskAvailable() && 
					( 
					  getStatusApprovalRequired() || getStatusReviewRequired() || getStatusApproved() || 
					  getStatusRejectedResubmitted() ||
					  getStatusReviewDone() || getStatusRejectedByFinance() || getStatusReviewRequired()  
					   
					)
			 )
			{
				if( viewMap.remove("fetchTaskNotified") == null  )
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty("commons.msg.fectchtasktoperformaction"));
					viewMap.put("fetchTaskNotified", true);
				}
			}
		}
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in prerender()]", ex);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	@SuppressWarnings("unchecked")
	private void setBlockingDetailsForAddition() throws Exception
	{
	  blocking = new BlockingDetails();
	  blocking.setPerson(new Person());
	  DomainDataView ddNew =  CommonUtil.getDomainDataView(WebConstants.BlockinDetailsStatus.KEY, WebConstants.BlockinDetailsStatus.NEW_ID);
	  DomainData status = new DomainData();
	  
	  status.setDomainDataId(ddNew.getDomainDataId());
	  status.setDataDescEn( ddNew.getDataDescEn() );
	  status.setDataDescAr( ddNew.getDataDescAr() );
	  blocking.setCreatedBy(   getLoggedInUserId() );
	  blocking.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
	  blocking.setStatus(  status );
	  viewMap.put(  WebConstants.BlockingRequest.Blocking , blocking );
	  accountBalance.setValue( "" );
	  blockingBalance.setValue( "" );
	  availableBalance.setValue("");
	}
	@SuppressWarnings("unchecked")
	private void loadBlockForList() throws Exception
	{
		
		
		PersonalAccountTrxService accountWS = new PersonalAccountTrxService();
		Map<Long, Double> personBalanceMap = new HashMap<Long, Double>();
		Map<Long, Double> personBlockingMap = new HashMap<Long, Double>();
		
		
		//Load Main Account
		blockForList.add( new SelectItem(inheritanceFile.getFilePersonId(), inheritanceFile.getFilePerson() ) );
		long filePersonId = Long.valueOf( inheritanceFile.getFilePersonId() );
		Double amount = 0.0d;
		if( inheritanceFile.getCostCenter() != null && inheritanceFile.getCostCenter().trim().length() > 0 )
		{
		  amount = PersonalAccountTrxService.getBalanceForCostCenter(inheritanceFile.getCostCenter());
		  if(  inheritanceFile.getOldCostCenter() != null && inheritanceFile.getOldCostCenter().trim().length() > 0 )
		  {
			  amount += PersonalAccountTrxService.getBalanceForCostCenter(inheritanceFile.getOldCostCenter());
		  }
		 //amount = PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount( filePersonId ,inheritanceFile.getInheritanceFileId() );
		 //			Double amount = accountWS.getAmountByPersonId( filePersonId );
		}
		Double blocking = BlockingAmountService.getBlockingAmount( filePersonId , null, inheritanceFile.getInheritanceFileId(), null, null );
		personBalanceMap.put( filePersonId , amount);
		viewMap.put( PERSON_BALANCE_MAP, personBalanceMap);
		personBlockingMap.put( filePersonId , blocking );
		viewMap.put( PERSON_BLOCKING_MAP , personBlockingMap);
		selectOneBlockFor = inheritanceFile.getFilePersonId();
		
		List<InheritanceBeneficiary> list=  InheritanceFileService.getInheritanceBeneficiaryByFileId(  inheritanceFile.getInheritanceFileId() );
		//load beneficiaries
		for (InheritanceBeneficiary obj : list) 
		{
			if( obj.getBeneficiary().getPersonId().compareTo( Long.valueOf(  inheritanceFile.getFilePersonId() ) ) ==0  ) continue;
			if( obj.getCostCenter() == null && obj.getCostCenter().trim().length() <= 0 )
			{
			  continue;
			}
		    blockForList.add( new SelectItem( obj.getBeneficiary().getPersonId().toString(), obj.getBeneficiary().getFullName() ) );
		    amount = PersonalAccountTrxService.getBalanceForPersonAndFileIdAmount( obj.getBeneficiary().getPersonId()  ,inheritanceFile.getInheritanceFileId() );
//			amount = accountWS.getAmountByPersonId( obj.getBeneficiary().getPersonId() );
			personBalanceMap.put( obj.getBeneficiary().getPersonId(), amount);
			viewMap.put( PERSON_BALANCE_MAP, personBalanceMap);

			
			blocking = BlockingAmountService.getBlockingAmount( obj.getBeneficiary().getPersonId(), null, null , null, null);
			personBlockingMap.put( obj.getBeneficiary().getPersonId(), blocking );
			viewMap.put( PERSON_BLOCKING_MAP , personBlockingMap);
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		request = new Request();
		request.setCreatedOn(new Date());
		request.setCreatedBy(  getLoggedInUserId()  );
		request.setUpdatedBy(  getLoggedInUserId()  );
		request.setUpdatedOn(new Date());
		request.setStatusId( 9001l );
		request.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		request.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
		request.setRequestDate( new Date() );
		if( inheritanceFile != null && inheritanceFile.getInheritanceFileId() != null  )
		{
		InheritanceFile file =  new InheritanceFile();
		file.setInheritanceFileId( inheritanceFile.getInheritanceFileId());
		request.setInheritanceFile(file);
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromRequestStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		tabPanel.setSelectedTab(TAB_DETAILS);
		if( this.request== null  || 
			this.request.getRequestId() == null ||
			this.request.getStatusId() == null 
		  ) 
		{ return; }
		else if( getStatusApprovalRequired()  || getStatusRejectedByFinance()  || getStatusReviewDone() )
		{
			setPageMode(PAGE_MODE_APPROVAL_REQUIRED);
			if( getStatusApprovalRequired() )
			{
				tabPanel.setSelectedTab( TAB_DETAILS );
			}
			else if( getStatusReviewDone()  )
			{
				tabPanel.setSelectedTab( TAB_REVIEW );
			}
			else if( getStatusRejectedByFinance()  )
			{
				tabPanel.setSelectedTab( TAB_DETAILS );
			}
		}
		else if( getStatusReviewRequired()   )
		{
			setPageMode( PAGE_MODE_REVIEW_REQUIRED );
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
			tabPanel.setSelectedTab( TAB_REVIEW );
		}
		else if( getStatusApproved() )
		{
			setPageMode(PAGE_MODE_APPROVED);
			tabPanel.setSelectedTab( TAB_DETAILS );
		}
		else if( getStatusRejectedResubmitted() )
		{
			setPageMode(PAGE_MODE_RESUBMITTED);
			tabPanel.setSelectedTab( TAB_DETAILS );
		}
		else if ( getStatusCompleted() )
		{
			setPageMode(PAGE_MODE_VIEW);
			tabPanel.setSelectedTab( TAB_DETAILS );
		}	
			
	}

	public boolean getStatusNew() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_NEW_ID ) == 0;
	}
	public boolean getStatusApprovalRequired() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID ) == 0;
	}
	public boolean getStatusReviewRequired() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID) == 0;
	}
	public boolean getStatusReviewDone() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_REVIEW_DONE_ID) == 0;
	}
	public boolean getStatusApproved() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_APPROVED_ID ) == 0;
	}
	public boolean getStatusRejected() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_ID ) == 0;
	}
	public boolean getStatusRejectedByFinance() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_FINANCE_REJECTED_ID ) == 0;
	}
	public boolean getStatusRejectedResubmitted() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT_ID) == 0;
	}
	public boolean getStatusCompleted() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_COMPLETE_ID ) == 0;
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.REQUEST ) != null )
		{
			request = ( Request )viewMap.get( WebConstants.REQUEST ) ;
			request.setUpdatedBy(getLoggedInUserId());
		}
		if( viewMap.get(  WebConstants.BlockingRequest.BlockingList ) != null)
		{
			blockingList = (	ArrayList<BlockingDetails> )viewMap.get(  WebConstants.BlockingRequest.BlockingList ); 
		}
		if( viewMap.get(  WebConstants.BlockingRequest.Blocking) != null)
		{
			blocking = (	BlockingDetails  )viewMap.get(  WebConstants.BlockingRequest.Blocking ); 
		}
		if( viewMap.get( WebConstants.BlockingRequest.InheritanceFile ) != null )
		{
			inheritanceFile = (InheritanceFileView)viewMap.get(  WebConstants.BlockingRequest.InheritanceFile );
		}
		if( viewMap.get( WebConstants.BlockingRequest.BlockFor ) != null )
		{
			blockForList = (ArrayList<SelectItem>)viewMap.get(  WebConstants.BlockingRequest.BlockFor);
		}
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( request != null )
		{
		  viewMap.put(  WebConstants.REQUEST , request);
		}
		if( blockingList != null )
		{
			viewMap.put(  WebConstants.BlockingRequest.BlockingList, blockingList);
		}
		if( blocking != null )
		{
			viewMap.put(  WebConstants.BlockingRequest.Blocking , blocking );
		}
		else
		{
			viewMap.put(  WebConstants.BlockingRequest.Blocking, new BlockingDetails() );
		}
		if( inheritanceFile != null )
		{
			viewMap.put(  WebConstants.BlockingRequest.InheritanceFile, inheritanceFile);
			
		}
		if( blockForList != null )
		{
			 viewMap.put(  WebConstants.BlockingRequest.BlockFor, blockForList );
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void getDataFromFileSearch()throws Exception
	{
		inheritanceFile = (InheritanceFileView) getRequestMap().get( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);
		setDataForFirstTime();
	}
	

	@SuppressWarnings( "unchecked" )
	private void getDataFromRequest()throws Exception
	{
		RequestView requestView = ( RequestView )getRequestMap().get( WebConstants.REQUEST_VIEW) ;
		if( requestView !=null )
		{
		  getBlockingRequest( requestView.getRequestId() );
		}
		if( requestView.getInheritanceFileView() != null )
		{
			inheritanceFile = requestView.getInheritanceFileView() ;
		}
	}
	
	@SuppressWarnings(  "unchecked"  )
	private void populateApplicationDetailsTab()throws Exception
	{
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		String status = "";
		boolean isRequestNull =  null == request || null == request.getApplicant() ||null == request.getRequestId();
		if ( isRequestNull && hdnPersonName!= null && hdnPersonName.trim().length() >0 ) 
		{
			bean.populateApplicationDetails(
						                    "", 
						                    "", 
						                    new Date(), 
						                    "", 
						                    hdnPersonName, 
						                    "", 
						                    hdnCellNo, 
						                    ""
						                  );
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, hdnPersonId);
			Person  person = new Person();
			person.setPersonId( new Long( hdnPersonId ) );
			request.setApplicant( person);
			
            return;
		}
		
		String applicantName  = request.getApplicant() != null ?request.getApplicant().getFullName():"";
		DomainDataView ddv = CommonUtil.getDomainDataView(WebConstants.REQUEST_STATUS,  request.getStatusId() );
		status = isEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr();
		bean.populateApplicationDetails(
				                         request.getRequestNumber(), 
					                     status, 
					                     request.getRequestDate(), 
					                     request.getDescription(), 
					                     applicantName, 
					                     "", 
					                     "", 
					                     ""
					                   );
		if( request.getApplicant() != null && request.getApplicant().getPersonId() != null  )
		{
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,request.getApplicant().getPersonId() );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, request.getApplicant().getPersonId() );
		}
		if ( request!= null && request.getStatusId() != null && 
			!request.getStatusId().equals( WebConstants.REQUEST_STATUS_NEW_ID ) 
		    )
		{
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}
	}
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  UserTask userTask = ( UserTask )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  setUserTask(userTask);
	  if( userTask.getTaskAttributes().get(  WebConstants.REQUEST_ID ) == null ){ return; }
	  long  id = new Long  ( userTask.getTaskAttributes().get( WebConstants.REQUEST_ID ) );
	  getBlockingRequest( id );
	  if( request.getInheritanceFile() != null )
	  {
		 inheritanceFile = new InheritanceFileView();
		 inheritanceFile.setInheritanceFileId( request.getInheritanceFile().getInheritanceFileId() );
		 inheritanceFile.setFileNumber( request.getInheritanceFile().getFileNumber()  );
		 inheritanceFile.setFilePersonId( request.getInheritanceFile().getFileOwner().getPersonId().toString() );
		 inheritanceFile.setFilePerson( request.getInheritanceFile().getFileOwner().getFullName() );
	  }
	  
    }
	
	@SuppressWarnings( "unchecked" )
	private void getBlockingRequest( long id ) throws Exception 
	{
		request = blockingService.getBlockingRequestByRequestId( id );
		inheritanceFile = new InheritanceFileView();
		inheritanceFile.setFileNumber( request.getInheritanceFile().getFileNumber() );
		inheritanceFile.setCreatedBy(  request.getInheritanceFile().getCreatedBy() );
		inheritanceFile.setCreatedOn(  request.getInheritanceFile().getCreatedOn() );
		inheritanceFile.setInheritanceFileId(  request.getInheritanceFile().getInheritanceFileId() );
		DomainDataView status = CommonUtil.getDomainDataView( WebConstants.InheritanceFileStatus.INH_FILE_STATUS, request.getInheritanceFile().getStatus() );
		inheritanceFile.setStatusAr( status.getDataDescAr() );
		inheritanceFile.setStatusEn( status.getDataDescEn() );
		
		DomainDataView type = CommonUtil.getDomainDataView( WebConstants.InheritanceFileType.INH_FILE_TYPE, request.getInheritanceFile().getFileType() );
		inheritanceFile.setFileTypeAr( type.getDataDescAr() );
		inheritanceFile.setFileTypeEn( type.getDataDescEn() );
		
		if(request.getBlockingDetails() != null )
		{
			blockingList = new ArrayList<BlockingDetails>();
			
			blockingList.addAll(  request.getBlockingDetails() );
			request.setBlockingDetails( null ); 
			
		}
		
		populateApplicationDetailsTab();
		updateValuesToMap();
		loadAttachmentsAndComments( id );
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(request.getRequestId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.REQUEST;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, request.getRequestId());
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.REQUEST;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(request.getRequestId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, request.getRequestId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public void onOpenInheritanceFile() 
	{
		
		try	
		{
			updateValuesFromMap();
			sessionMap.put( WebConstants.InheritanceFile.FILE_ID, getInheritanceFile().getInheritanceFileId());
		    sessionMap.put( WebConstants.InheritanceFilePageMode.IS_POPUP, true);
			executeJavaScript("javaScript:openFile();"); 
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenInheritanceFile|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}


	}
	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( request != null && request.getRequestId()!= null )
		 {
			 loadAttachmentsAndComments( request.getRequestId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMap();
			if(hdnPersonType.equals( "APPLICANT" ) )
			{
				populateApplicationDetailsTab();
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	@SuppressWarnings( "unchecked" )
	public void onPaymentMethodChanged()
	{
		try	
		{
			updateValuesFromMap();
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onPaymentMethodChanged|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onOpenBlockingDetailsPopup() 
	{
		try
		{
			updateValuesFromMap();
			if( selectOneBlockFor != null && !selectOneBlockFor.equals( "-1" ) )
			{
				executeJavaScript(
								  "javaScript:openBlockingDetailsPopup('"+selectOneBlockFor+"','"+inheritanceFile.getInheritanceFileId()+"');"
								 );
			} 
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onOpenBlockingDetailsPopup()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onBlockForChange() 
	{
		try
		{
			if( selectOneBlockFor != null && !selectOneBlockFor.equals( "-1" ) )
			{
				DecimalFormat oneDForm = new DecimalFormat("#.00");
				Map<Long, Double> balanceMap = getBalanceMap();
				Long beneficiaryId = new Long( selectOneBlockFor );
				Double amount = 0.00d  ;
				Double blockingMapAmount= 0.00d ;
				if( balanceMap.containsKey( beneficiaryId) ) 
				{
					amount = balanceMap.get( beneficiaryId);
					accountBalance.setValue( oneDForm.format( amount ) );
				}
				
				Map<Long, Double> blockingMap = getBlockingMap();
				if( blockingMap.containsKey( beneficiaryId) ) 
				{
					blockingMapAmount = blockingMap.get( beneficiaryId);
					blockingBalance.setValue( oneDForm.format(  blockingMapAmount ) );
				}
				
				availableBalance.setValue(   oneDForm.format( amount-blockingMapAmount) );
				blockingPersonId = selectOneBlockFor;
			} 
			else 
			{
				accountBalance.setValue( "" );
				blockingBalance.setValue( "" );
			}
		} catch(Exception ex) {
			logger.LogException("[Exception occured in onBlockForChange()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if(request == null || request.getRequestId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,request.getRequestId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if( blockingList==null || blockingList.size() <= 0) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("blocking.atleastOneBlockingRequired"));
			tabPanel.setSelectedTab(TAB_DETAILS);
			return true;
		}
		return hasSaveErrors;
		
	}
	private boolean hasNavigateToUnblockingErrors() throws Exception
	{
		boolean hasNavigateToUnblockingErrors=false;
		errorMessages = new ArrayList<String>();
		boolean anySelected=false;
		for ( BlockingDetails blocking : blockingList) 
		{
			if( blocking.getSelected() != null && blocking.getSelected() )
			{
				anySelected = true;
				break;
			}
		}
		if( !anySelected )
		{
			errorMessages.add(CommonUtil.getBundleMessage( "BlockingRequest.msg.selectAtleastOneBlockingLine" ));
			tabPanel.setSelectedTab(TAB_DETAILS);
			return true;
		}

		return hasNavigateToUnblockingErrors;
		
	}
	
	@SuppressWarnings( "unchecked" )
	public String onNavigateToUnblocking()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasNavigateToUnblockingErrors() ){ return""; }
			List<BlockingDetails> selectedList  = new ArrayList<BlockingDetails>();
			for ( BlockingDetails blocking : blockingList) 
			{
				if( blocking.getSelected() != null && blocking.getSelected() )
				{
					selectedList.add( blocking );
				}
			}
			sessionMap.put( WebConstants.BlockingRequest.BlockingList , selectedList );
			if ( inheritanceFile != null )
			{
				getRequestMap().put( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW, inheritanceFile ); 
			}
			updateValuesToMap();
			return "unblock";
		}
		catch (Exception exception) 
		{
			logger.LogException( "onNavigateToUnblocking --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}
	
	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			saveBlockingRequestInTransaction();
			getBlockingRequest( request.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
			successMessages.add( ResourceUtil.getInstance().getProperty("thirdPartRevenue.msg.saved"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	
	private void saveBlockingRequestInTransaction() throws Exception 
	{
		try
		{
			
			Long status = 9001l;
            if(request.getStatusId() != null)
            {
            	status = null;
            }
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistBlockingRequest( status );
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistBlockingRequest(Long status) throws Exception 
	{
	
		if(status!=null) request.setStatusId(status);
		blockingService.persist( request, blockingList );
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onSubmit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		String event  =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		try	
		{	
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			ApplicationContext.getContext().getTxnContext().beginTransaction();	 

			 persistBlockingRequest(  WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID );
			 getBlockingRequest( request.getRequestId() );
			 updateValuesToMap();
			 invokeBPEL();			 
			 saveCommentsAttachment( event );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSubmit --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	/**
	 * @throws Exception
	 * @throws RemoteException
	 */
	private void invokeBPEL() throws Exception, RemoteException 
	{
		 SystemParameters parameters = SystemParameters.getInstance();
		 String endPoint= parameters.getParameter( "MEMSBlockingBPEL" );
		 MEMSBlockingBPELPortClient port=new MEMSBlockingBPELPortClient();
		 port.setEndpoint(endPoint);
		 port.initiate(
				 		Integer.parseInt( request.getRequestId().toString()),
				 		CommonUtil.getLoggedInUser(),
				 		"1", 
				 		null, 
				 		null
				 	   );
	}
	@SuppressWarnings( "unchecked" )
	public void onResubmit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		try	
		{
			 updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 		 
			 persistBlockingRequest( WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID);
			 getBlockingRequest( request.getRequestId() );
			 
			 setTaskOutCome(TaskOutcome.APPROVE);
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onResubmitted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onReviewed()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_REVIEWED;
		String event  =  MessageConstants.RequestEvents.REQUEST_REVIEWED;
		try	
		{
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			 

			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 	
			 UtilityService utilityService = new UtilityService();
			 ReviewRequestView reviewRequestView = ( ReviewRequestView ) viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW );
			 utilityService.persistReviewRequest( reviewRequestView );
				
			 persistBlockingRequest( WebConstants.REQUEST_STATUS_REVIEW_DONE_ID );
			 getBlockingRequest( request.getRequestId() );
			 
			 setTaskOutCome(TaskOutcome.OK);
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onReviewed--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	
	public boolean hasReviewErrors() throws Exception
	{
		if( null==cmbReviewGroup.getValue() || cmbReviewGroup.getValue().toString().equals( "-1")) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_REVIEW_GRP));
			return true;
		}
		
		
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public void onReviewRequired()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW;
		try	
		{	updateValuesFromMap();	
			if( hasSaveErrors() || hasReviewErrors() || !hasReasonProvided()){ return; }
			 
			 
			 UtilityService utilityService = new UtilityService();
			 ReviewRequestView reviewRequestView = new ReviewRequestView();
			 reviewRequestView.setCreatedBy( CommonUtil.getLoggedInUser() );
			 reviewRequestView.setCreatedOn( new Date() );
			 reviewRequestView.setRfc( txtRemarks.trim() );
			 reviewRequestView.setGroupId( cmbReviewGroup.getValue().toString() );
			 reviewRequestView.setRequestId( request.getRequestId() );
		
			 try 
			 {
					 ApplicationContext.getContext().getTxnContext().beginTransaction();
					 utilityService.persistReviewRequest( reviewRequestView );				
					 persistBlockingRequest( WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID);
					 ApplicationContext.getContext().getTxnContext().commit();
			 }
			 catch (Exception exception) 
			 {
					ApplicationContext.getContext().getTxnContext().rollback();
					throw exception;
			 }
			finally
			{
				ApplicationContext.getContext().getTxnContext().release();
			}

			 getBlockingRequest( request.getRequestId() );
			 setTaskOutCome(TaskOutcome.FORWARD);
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			logger.LogException("onReviewRequired--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	
	@SuppressWarnings( "unchecked" )
	public void onApproved()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_APPROVED;
		String event  =  MessageConstants.RequestEvents.REQUEST_APPROVED;
		try	
		{
			 updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 persistBlockingRequest( WebConstants.REQUEST_STATUS_COMPLETE_ID );
			 //persistBlockingRequest( WebConstants.REQUEST_STATUS_APPROVED_ID );
			 getBlockingRequest( request.getRequestId() );
			 //As per the users there should be no task after approval.
			 setTaskOutCome(TaskOutcome.CLOSE);
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onApproved--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

		
	@SuppressWarnings( "unchecked" )
	public void onSendBack()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		try	
		{
			updateValuesFromMap();
			if( hasSaveErrors() || !hasReasonProvided() ){ return; }
			
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 persistBlockingRequest( WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT_ID );
			 getBlockingRequest( request.getRequestId() );
			 
			 setTaskOutCome(TaskOutcome.REJECT);
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSendBack--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onComplete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		String event  =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() ){ return; }
		 
		 updateValuesFromMap();	
		 
		 persistBlockingRequest( WebConstants.REQUEST_STATUS_COMPLETE_ID );
		 getBlockingRequest( request.getRequestId() );
		 
		 setTaskOutCome(TaskOutcome.APPROVE);

		 updateValuesToMap();
		 
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onComplete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onRejectedFromFinance()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_REJECTED_FINANCE;
		String event  =  MessageConstants.RequestEvents.REQUEST_REJECTED_FINANCE;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() || !hasReasonProvided()){ return; }
		 
		 updateValuesFromMap();	
		 
		 persistBlockingRequest( WebConstants.REQUEST_STATUS_FINANCE_REJECTED_ID );
		 getBlockingRequest( request.getRequestId() );
		 
		 setTaskOutCome(TaskOutcome.REJECT);

		 updateValuesToMap();
		 
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejectedFromFinance--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	private boolean hasReasonProvided() throws Exception
	{
		if(txtRemarks ==null || txtRemarks.trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.reasonRequired")  );
			return false;
		}
		return true;
	}
	@SuppressWarnings( "unchecked" )
	public void onRejected()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_REJECTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_REJECTED;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistBlockingRequest( WebConstants.REQUEST_STATUS_REJECTED_ID );
			 setTaskOutCome(TaskOutcome.CLOSE);
			 getBlockingRequest( request.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private boolean hasAddErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if( selectOneReason == null || selectOneReason.equals("-1") )
		{
			hasSaveErrors=true;
			errorMessages.add(ResourceUtil.getInstance().getProperty("blocking.reasonRequired"));
//			tabPanel.setSelectedTab(TAB_DETAILS);
		}
		if( blocking.getAmount() == null  )
		{
			hasSaveErrors=true;
			errorMessages.add(ResourceUtil.getInstance().getProperty("blocking.msg.amountRequired"));
			
		}
		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onAdd()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			if( hasAddErrors() ){ return; }
			
			BlockingDetails cloned = BlockingDetails.copyDetails( blocking );
			Long personId = new Long( selectOneBlockFor );	
			Person person = new Person();
			person.setPersonId( personId );
			cloned.setPerson(person);
			cloned.setUpdatedBy( getLoggedInUserId() );
			InheritanceBeneficiary inheritanceBeneficiary = InheritanceFileService.getInheritanceBeneficiariesByArgs( personId, inheritanceFile.getInheritanceFileId() );
			if( inheritanceBeneficiary != null )
			{
			 cloned.setInheritanceBeneficiary(inheritanceBeneficiary);
			}
		    BlockingReason reason = new BlockingReason();
		    reason.setReasonId( new Long( selectOneReason  ) );
		    cloned.setReason(reason);
			blockingList.add( 0, cloned);
			saveBlockingRequestInTransaction();
			
			getBlockingRequest( request.getRequestId() );
			setBlockingDetailsForAddition();
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty("BlockingRequest.msg.added"));
		}
		catch (Exception exception) 
		{
			logger.LogException( "onAdd --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	private boolean hasDeleteErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
        if( request.getRequestId() != null && blockingList.size()==1 )
        {
        	errorMessages.add(ResourceUtil.getInstance().getProperty("BlockingRequest.msg.deleteError"));
        	hasSaveErrors = true;
        }
		return hasSaveErrors;
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onEdit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			blocking = ( BlockingDetails )dataTable.getRowData();
			selectOneBlockFor = blocking.getPerson().getPersonId().toString();
			selectOneReason = blocking.getReason().getReasonId().toString();
			blockingList.remove(blocking);
			onBlockForChange();
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onEdit--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onDelete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			BlockingDetails row = ( BlockingDetails )dataTable.getRowData();
			updateValuesFromMap();
			
			if( hasDeleteErrors() ){ return; }
	        
	        if( row.getBlockingDetailId()  != null )
	        {
	        	row.setUpdatedBy( getLoggedInUserId() );
	        	row.setIsDeleted(1l);
	        	saveBlockingRequestInTransaction();
	        	
	        }
	        blockingList.remove( row );	
			successMessages.add( ResourceUtil.getInstance().getProperty("BlockingRequest.msg.deletedSuccessfully"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onDelete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = getUserTask();
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    }



	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask )
	{
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}


	public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("BlockingRequest.title.heading"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}
	@SuppressWarnings( "unchecked" )
	public Request getMaintenanceRequest() 
	{
		
		if( viewMap.get("maintenanceRequest") != null )
		{
			return (Request)viewMap.get("maintenanceRequest") ;
		}
		for (BlockingDetails blocking  : blockingList) 
		{
			
			if( blocking.getMaintenanceRequest() != null )
			{
				viewMap.put("maintenanceRequest", blocking.getMaintenanceRequest());
				return blocking.getMaintenanceRequest() ;
			}
		}
		return null;

	}
	@SuppressWarnings( "unchecked" )
	public boolean getIsFromMaintenanceRequest() 
	{
		
		if( viewMap.get("maintenanceRequest") != null )
		{
			return true;
		}
		if( blockingList == null )
		{
			return false;
		}
		for (BlockingDetails blocking  : blockingList) 
		{
			
			if( blocking.getMaintenanceRequest() != null )
			{
				viewMap.put("maintenanceRequest", blocking.getMaintenanceRequest());
				return true;
			}
		}
		 return false;


	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_NEW)  || getPageMode().equals( PAGE_MODE_RESUBMITTED ) )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowUnblockButton()
	{
		if( 
			getPageMode().equals( PAGE_MODE_VIEW ) && 
			request.getStatusId().compareTo( WebConstants.REQUEST_STATUS_COMPLETE_ID ) == 0  
	      ) 
		{
			return true;
		}
		return false;
	}
	

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  getPageMode().equals( PAGE_MODE_NEW ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowResubmitButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_RESUBMITTED ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowApproveButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_APPROVAL_REQUIRED )  )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowReviewDone()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_REVIEW_REQUIRED)  )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowComplete()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_APPROVED ) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	private Map<Long, Double> getBalanceMap() {
		if( viewMap.containsKey( PERSON_BALANCE_MAP)) {
			return (HashMap<Long, Double>) viewMap.get( PERSON_BALANCE_MAP);
		}
		return new HashMap<Long, Double>();
	}

	@SuppressWarnings("unchecked")
	private Map<Long, Double> getBlockingMap() {
		if( viewMap.containsKey( PERSON_BLOCKING_MAP )) {
			return (HashMap<Long, Double>) viewMap.get( PERSON_BLOCKING_MAP  );
		}
		return new HashMap<Long, Double>();
	}
	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


	public List<BlockingDetails> getBlockingList() {
		return blockingList;
	}

	public void setBlockingList(List<BlockingDetails> blockingList) {
		this.blockingList = blockingList;
	}

	public BlockingDetails getBlocking() {
		return blocking;
	}

	public void setBlocking(BlockingDetails blocking) {
		this.blocking = blocking;
	}

	public List<SelectItem> getBlockForList() {
		return blockForList;
	}

	public void setBlockForList(List<SelectItem> blockForList) {
		this.blockForList = blockForList;
	}

	public InheritanceFileView getInheritanceFile() {
		return inheritanceFile;
	}

	public void setInheritanceFile(InheritanceFileView inheritanceFile) {
		this.inheritanceFile = inheritanceFile;
	}

	public String getSelectOneBlockFor() {
		return selectOneBlockFor;
	}

	public void setSelectOneBlockFor(String selectOneBlockFor) {
		this.selectOneBlockFor = selectOneBlockFor;
	}

	public HtmlSelectOneMenu getCmbReviewGroup() {
		return cmbReviewGroup;
	}

	public void setCmbReviewGroup(HtmlSelectOneMenu cmbReviewGroup) {
		this.cmbReviewGroup = cmbReviewGroup;
	}

	public HtmlInputText getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(HtmlInputText accountBalance) {
		this.accountBalance = accountBalance;
	}

	public HtmlInputText getBlockingBalance() {
		return blockingBalance;
	}

	public void setBlockingBalance(HtmlInputText blockingBalance) {
		this.blockingBalance = blockingBalance;
	}

	public String getSelectOneReason() {
		return selectOneReason;
	}

	public void setSelectOneReason(String selectOneReason) {
		this.selectOneReason = selectOneReason;
	}

	public String getBlockingPersonId() {
		return blockingPersonId;
	}

	public void setBlockingPersonId(String blockingPersonId) {
		this.blockingPersonId = blockingPersonId;
	}

	public HtmlInputText getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(HtmlInputText availableBalance) {
		this.availableBalance = availableBalance;
	}
	

}