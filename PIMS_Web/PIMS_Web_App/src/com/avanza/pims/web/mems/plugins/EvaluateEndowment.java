package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.component.html.HtmlCommandButton;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.EndFileAsso;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.endowment.EndowmentFilesService;
import com.avanza.ui.util.ResourceUtil;

public class EvaluateEndowment extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	
	private EndowmentFilesService endowmentFileService;
	private String pageMode;
	HtmlCommandButton btnDone = new HtmlCommandButton();
	EndFileAsso endowmentFileAsso;
	
	public EvaluateEndowment() 
	{
		endowmentFileService        = new EndowmentFilesService();
		pageMode                    = WebConstants.PAGE_MODE_EDIT;
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
			if(!isPostBack() )
			{
			 endowmentFileAsso.setUpdatedBy(getLoggedInUserId());
			}
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@Override
	public void prerender() 
	{
		if(getIsPageModeUpdatable() )
		{
			btnDone.setRendered(true);
			
		}
		else
		{
			btnDone.setRendered(false);
			
		}
	}
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if ( sessionMap.get(WebConstants.PAGE_MODE)!= null )
		{
			viewMap.put(
						WebConstants.PAGE_MODE,sessionMap.remove(WebConstants.PAGE_MODE)
			           );
		}
		if ( viewMap.get( WebConstants.PAGE_MODE ) != null )
		{
			pageMode = viewMap.get(WebConstants.PAGE_MODE).toString();
		}
		
		if( viewMap.get(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO) != null)
		{
			endowmentFileAsso = ( EndFileAsso )viewMap.get(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO);
			 
		}
		else if( sessionMap.get( Constant.EndowmentFile.ENDOWMENT_FILE_ASSO ) != null )
		{
			endowmentFileAsso = ( EndFileAsso )sessionMap.remove(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO);
			endowmentFileAsso  = endowmentFileService.getEndowmentFileAsso( endowmentFileAsso );
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( pageMode != null )
		{
		  viewMap.put( WebConstants.PAGE_MODE, pageMode);
		}
		if( endowmentFileAsso != null  )
		{
		  viewMap.put(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO,endowmentFileAsso );
		}
	}	
	
	public void populateValuesInObject() throws Exception
	{
		endowmentFileAsso.setUpdatedBy( getLoggedInUserId() );
		endowmentFileAsso.setEvaluatedBy( getLoggedInUserId() );
		endowmentFileAsso.setEvaluatedOn( new Date() );
		endowmentFileAsso.setStatusId( Constant.EndowmentFileAssoStatus.EVALUATED_ID );
		
		endowmentFileAsso.getEndowment().setUpdatedBy( getLoggedInUserId() );
		if( endowmentFileAsso.getExpectedRevenue()!= null )
		{
			endowmentFileAsso.getEndowment().setRevenueVal(Double.valueOf( endowmentFileAsso.getExpectedRevenue() ));
		}
		endowmentFileAsso.getEndowment().setRevenueFreq(	endowmentFileAsso.getExpRevFreq() );
	}
	
	private boolean hasDoneErrors() throws Exception
	{
		boolean hasErrors = false;
		if( endowmentFileAsso.getExpectedRevenue() == null )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentFileAsso.msg.expRevenueRequired"));
			hasErrors = true;
		}
		if( endowmentFileAsso.getExpRevFreq()  == null || endowmentFileAsso.getExpRevFreq().compareTo(-1l)==0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentFileAsso.msg.expRevenueFrequencyRequired"));
			hasErrors = true;
		}
		
		return hasErrors;
	}
    @SuppressWarnings( "unchecked" )
	public void onDone()
	{
		
		try
		{
			updateValuesFromMaps();
			errorMessages = new ArrayList<String>();
			populateValuesInObject();
			if( hasDoneErrors() )
			{return;}

			endowmentFileService.persistEndFileAsso(endowmentFileAsso);
			
			sessionMap.put(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO, endowmentFileAsso);
			executeJavaScript("sendDataToParent();");
		}
		catch ( Exception e )
		{
			logger.LogException( "onDone--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
    
        
    private void clearFields()
    {
    }
    
    public boolean getIsPageModeUpdatable()
	{
		boolean isPageModeUpdatable = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.PAGE_MODE_EDIT ) )
			isPageModeUpdatable = true;
		
		return isPageModeUpdatable;
	}

	public EndFileAsso getEndowmentFileAsso() 
	{
		return endowmentFileAsso;
	}

	public void setEndowmentFileAsso(EndFileAsso endowmentFileAsso) 
	{
		this.endowmentFileAsso = endowmentFileAsso;
	}

	public String getPageMode() 
	{
		return pageMode;
	}

	public void setPageMode(String pageMode) 
	{
		this.pageMode = pageMode;
	}

	public HtmlCommandButton getBtnDone() {
		return btnDone;
	}

	public void setBtnDone(HtmlCommandButton btnDone) {
		this.btnDone = btnDone;
	}

}
