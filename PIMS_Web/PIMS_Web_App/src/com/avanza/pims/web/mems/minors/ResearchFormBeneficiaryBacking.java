//package com.avanza.pims.web.mems.minors;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import javax.faces.component.html.HtmlInputText;
//import javax.faces.component.html.HtmlSelectOneMenu;
//import javax.faces.context.FacesContext;
//import javax.faces.model.SelectItem;
//import javax.servlet.http.HttpServletRequest;
//
//import org.apache.myfaces.component.html.ext.HtmlDataTable;
//
//import com.avanza.core.util.Logger;
//import com.avanza.core.util.StringHelper;
//import com.avanza.pims.business.exceptions.PimsBusinessException;
//import com.avanza.pims.constant.Constant;
//import com.avanza.pims.entity.AccomodationAspects;
//import com.avanza.pims.entity.AccomodationOnwershipType;
//import com.avanza.pims.entity.AccomodationType;
//import com.avanza.pims.entity.DomainData;
//import com.avanza.pims.entity.HomeConditionType;
//import com.avanza.pims.entity.HomeType;
//import com.avanza.pims.entity.PropertyBeneficiaryType;
//import com.avanza.pims.entity.RentingReason;
//import com.avanza.pims.entity.ResidenceType;
//import com.avanza.pims.util.list.ListComparator;
//import com.avanza.pims.web.MessageConstants;
//import com.avanza.pims.web.WebConstants;
//import com.avanza.pims.web.backingbeans.ApplicationBean;
//import com.avanza.pims.web.mems.AbstractMemsBean;
//import com.avanza.pims.web.util.CommonUtil;
//import com.avanza.pims.ws.mems.ResearchFormBeneficiaryService;
//import com.avanza.pims.ws.vo.DomainDataView;
//import com.avanza.pims.ws.vo.PeriodicDisbursementGridView;
//import com.avanza.pims.ws.vo.ResearchFormBeneficiaryView;
//import com.avanza.pims.ws.vo.mems.AnnualIncomeView;
//import com.avanza.pims.ws.vo.mems.MonthlyExpenseView;
//import com.avanza.pims.ws.vo.mems.MonthlyIncomeView;
//import com.avanza.ui.util.ResourceUtil;
//
//@SuppressWarnings("unchecked")
//public class ResearchFormBeneficiaryBacking extends AbstractMemsBean {
//	
//	
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 6864660383251630460L;
//
//	private transient Logger logger = Logger
//			.getLogger(ResearchFormBeneficiaryBacking.class); 
// 
//	FacesContext context = FacesContext.getCurrentInstance();
//	Map sessionMap;
//	Map viewRootMap;
//	private Long personId =null ;
//	private Long InheritanceFileId = null;
//	private HtmlDataTable dataTableMonthlyIncome;
//	private HtmlDataTable dataTableAnnualIncome;
//	private HtmlDataTable dataTableMonthlyExpenses;
//	private HtmlDataTable dataTableResearchGrid;
//
//	private List<MonthlyIncomeView> dataListMonthlyIncome = new ArrayList<MonthlyIncomeView>();
//	private List<AnnualIncomeView> dataListAnnualIncome = new ArrayList<AnnualIncomeView>();
//	private List<MonthlyExpenseView> dataListMonthlyExpenses = new ArrayList<MonthlyExpenseView>();
//	
//	private List<ResearchFormBeneficiaryView> dataListResearchGridView = new ArrayList<ResearchFormBeneficiaryView>();
//
//	private javax.faces.component.html.HtmlSelectBooleanCheckbox htmlIsMinor = new javax.faces.component.html.HtmlSelectBooleanCheckbox();
//	private javax.faces.component.html.HtmlSelectBooleanCheckbox htmlIsStudent = new javax.faces.component.html.HtmlSelectBooleanCheckbox();
//	private javax.faces.component.html.HtmlSelectBooleanCheckbox htmlApplyToAllBeneficiaries = new javax.faces.component.html.HtmlSelectBooleanCheckbox();
//	private boolean sortAscending = false;
//	private HtmlInputText txtFamilyNumber = new HtmlInputText();
//	private HtmlInputText htmlAssetDesc = new HtmlInputText();
//
//	private HtmlInputText htmlAssetNameAr = new HtmlInputText();
//	private HtmlSelectOneMenu disbursementTypeSelectMenu = new HtmlSelectOneMenu();
//
//	private HtmlSelectOneMenu accTypeSelectMenu = new HtmlSelectOneMenu();
//	private List<SelectItem> accTypeList = new ArrayList<SelectItem>(0);
//
//	private HtmlSelectOneMenu accOwnershipSelectMenu = new HtmlSelectOneMenu();
//	private List<SelectItem> accOwnershipList = new ArrayList<SelectItem>(0);
//
//	private HtmlSelectOneMenu resTypeSelectMenu = new HtmlSelectOneMenu();
//	private List<SelectItem> resTypeList = new ArrayList<SelectItem>(0);
//
//	private HtmlSelectOneMenu beneficiaryPropSelectMenu = new HtmlSelectOneMenu();
//	private List<SelectItem> beneficiaryPropList = new ArrayList<SelectItem>(0);
//
//	private HtmlSelectOneMenu homeConditionSelectMenu = new HtmlSelectOneMenu();
//	private List<SelectItem> homeConditionList = new ArrayList<SelectItem>(0);
//
//	private HtmlSelectOneMenu reasonForRentSelectMenu = new HtmlSelectOneMenu();
//	private List<SelectItem> reasonForRentList = new ArrayList<SelectItem>(0);
//
//	private HtmlSelectOneMenu inComeCategorySelectMenu = new HtmlSelectOneMenu();
//	private List<SelectItem> incomeCategoryList = new ArrayList<SelectItem>(0);
//
//	private List<String> typeOfHome = new ArrayList<String>(0);
//	private List<SelectItem> typeOfHomeList;
//
//	private HtmlSelectOneMenu disbursementSourceSelectMenu = new HtmlSelectOneMenu();
//	private HtmlSelectOneMenu fileTypeSelectMenu = new HtmlSelectOneMenu();
//	private List<SelectItem> fileStatusList = new ArrayList<SelectItem>(0);
//	private List<SelectItem> fileTypeList = new ArrayList<SelectItem>(0);
//	
//	HashMap saveResearchFormBeneficiaryMap = new HashMap();
//	HashMap<Long,String> accomodationTypeMap = new HashMap<Long,String>();
//	HashMap<Long,String> accomodationOwnershipMap = new HashMap<Long,String>();
//	HashMap<Long,String> residenceTypeMap = new HashMap<Long,String>();
//	HashMap<Long,String> homeConditionMap = new HashMap<Long,String>();
//	HashMap<Long,String> rentingReasonMap = new HashMap<Long,String>();
//	HashMap<Long,String> typeOfHomeMap = new HashMap<Long,String>();
//	HashMap<Long,String> beneficiaryOfPropertyMap = new HashMap<Long,String>();
//
//	private List<SelectItem> fileStatus = new ArrayList<SelectItem>();
//	private String VIEW_MODE = "pageMode";
//	private Integer paginatorMaxPages = 0;
//	private Integer paginatorRows = 0;
//	private Integer recordSize = 0;
//
//
//	private String MODE_SELECT_ONE_POPUP = "MODE_SELECT_ONE_POPUP";
//	private String MODE_SELECT_MANY_POPUP = "MODE_SELECT_MANY_POPUP";
//
//	private String DEFAULT_SORT_FIELD = "assetNumber";
//
//	private String pageMode;
//	private boolean isPageModeSelectOnePopUp = false;
//	private boolean isPageModeSelectManyPopUp = false;
//
//	private String CONTEXT = "context";
//	private HtmlInputText accTypeDesc = new HtmlInputText();
//	private HtmlInputText resTypeDesc = new HtmlInputText();
//	private HtmlInputText accOwnershipDesc = new HtmlInputText();
//	private HtmlInputText homeConditionDesc = new HtmlInputText();
//	private HtmlInputText rentingReasonDesc = new HtmlInputText();
//	private HtmlInputText homeTypeDesc = new HtmlInputText();
//
//	private String sortField;
//
//	private boolean sortDescending;
//	
//	public Logger getLogger() {
//		return logger;
//	}
//
//	public void setLogger(Logger logger) {
//		this.logger = logger;
//	}
//
//	public FacesContext getContext() {
//		return context;
//	}
//
//	public void setContext(FacesContext context) {
//		this.context = context;
//	}
//
//	public HtmlDataTable getDataTable() {
//		return dataTableMonthlyIncome;
//	}
//
//	public void setDataTable(HtmlDataTable dataTable) {
//		this.dataTableMonthlyIncome = dataTable;
//	}
//
//	public HtmlInputText getHtmlAssetDesc() {
//		return htmlAssetDesc;
//	}
//
//	public void setHtmlAssetDesc(HtmlInputText htmlAssetDesc) {
//		this.htmlAssetDesc = htmlAssetDesc;
//	}
//
//	public HtmlInputText getHtmlAssetNameAr() {
//		return htmlAssetNameAr;
//	}
//
//	public void setHtmlAssetNameAr(HtmlInputText htmlAssetNameAr) {
//		this.htmlAssetNameAr = htmlAssetNameAr;
//	}
//
//	public void checkState() {
//
//		ApplicationBean appBena = new ApplicationBean();
//		appBena.getRelationList();
//	}
//
//	public HtmlSelectOneMenu getFileTypeSelectMenu() {
//		return fileTypeSelectMenu;
//	}
//
//	public void setFileTypeSelectMenu(HtmlSelectOneMenu fileTypeSelectMenu) {
//		this.fileTypeSelectMenu = fileTypeSelectMenu;
//	}
//
//	public void init() {
//		super.init();
//		viewRootMap = getFacesContext().getViewRoot().getAttributes();
//
//		try {
//
//			sessionMap = context.getExternalContext().getSessionMap();
//
//			Map requestMap = context.getExternalContext().getRequestMap();
//			HttpServletRequest request = (HttpServletRequest) this
//					.getFacesContext().getExternalContext().getRequest();
//
//			if (!isPostBack())
//
//			{
//
//				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
//				setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
//				setSortField(DEFAULT_SORT_FIELD);
//				setSortItemListAscending(true);
//
//				if (request.getParameter(VIEW_MODE) != null) {
//					if (request.getParameter(VIEW_MODE).equals(
//							MODE_SELECT_ONE_POPUP)) {
//						viewRootMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);
//					} else if (request.getParameter(VIEW_MODE).equals(
//							MODE_SELECT_MANY_POPUP)) {
//						viewRootMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);
//
//					}
//
//					
//
//					if (request.getParameter(WebConstants.PERSON_ID) != null) 
//					{
//						viewRootMap.put(WebConstants.PERSON_ID, request.getParameter(WebConstants.PERSON_ID).toString());
//
//						this.personId = Long.parseLong(request.getParameter(WebConstants.PERSON_ID).toString());
//
//						// loadAccomodationAspectsByPersonId();
//					}
//					if (sessionMap.get(WebConstants.PERSON_ID) != null) //beneficiaryID 
//					{
//						viewRootMap.put(WebConstants.PERSON_ID, sessionMap.get(WebConstants.PERSON_ID).toString());
//						
//						sessionMap.remove(WebConstants.PERSON_ID);
//
//						this.personId = Long.parseLong(viewRootMap.get(
//								WebConstants.PERSON_ID).toString());
//
//						// loadAccomodationAspectsByPersonId();
//					}
//					if (sessionMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null) {
//						viewRootMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_ID, sessionMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID)
//								.toString());
//						
//						sessionMap.remove(WebConstants.InheritanceFile.INHERITANCE_FILE_ID);
//
//						this.InheritanceFileId = Long.parseLong(viewRootMap.get(
//								WebConstants.InheritanceFile.INHERITANCE_FILE_ID).toString());
//
//						// loadAccomodationAspectsByPersonId();
//					}
//					loadDataTable(personId);
//
//				}
//
//				loadLists();
//				loadSingleDummyRowInDataTables();
//				if (personId != null) {
//					loadAccomodationAspectsByPersonId();
//					tabFinancialAspects_Click();// loads the rows of financial aspects
//				}
//				// loadFileStatusList();
//				// loadFileTypeList();
//			}
//
//		} catch (Exception e)
//		{
//           logger.LogException("init|Error Occured", e);
//			errorMessages = new ArrayList<String>(0);
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//    		
//
//		}
//
//	}
//
//	private void loadDataTable(Long personId) throws Exception{
//		if(personId!=null)
//		{
//			ResearchFormBeneficiaryService service = new ResearchFormBeneficiaryService();
//			List<ResearchFormBeneficiaryView> view =service.getAccomodationAspectsByPersonId(personId);
//			this.setDataListResearchGridViewInitial(view);
//		}
//		
//	}
//	
//	public void calculateTotal() 
//	{
//		// TODO Auto-generated method stub
//		this.personId = Long.parseLong(viewRootMap.get(WebConstants.PERSON_ID).toString());
//		try
//		{
//		if(personId!=null)
//		{
//			List<MonthlyIncomeView> monthlyIncome =getDataListMonthlyIncome();
//			
//			for (MonthlyIncomeView monthlyIncomeView : monthlyIncome) {
//			    Double total=(
//			    	Double.parseDouble(monthlyIncomeView.getPension()!=null && monthlyIncomeView.getPension().compareTo("")!=0 ?monthlyIncomeView.getPension():"0" )+
//			    	Double.parseDouble(monthlyIncomeView.getIncomeSource()!=null && monthlyIncomeView.getIncomeSource().compareTo("")!=0 ? monthlyIncomeView.getIncomeSource():"0")+
//			    	Double.parseDouble(monthlyIncomeView.getRealEstate() !=null && monthlyIncomeView.getRealEstate().compareTo("")!=0  ? monthlyIncomeView.getRealEstate():"0")+
//			    	Double.parseDouble(monthlyIncomeView.getSocSecurity() !=null && monthlyIncomeView.getSocSecurity().compareTo("")!=0   ? monthlyIncomeView.getSocSecurity():"0")+
//			    	Double.parseDouble(monthlyIncomeView.getTransport() !=null && monthlyIncomeView.getTransport().compareTo("")!=0   ? monthlyIncomeView.getTransport():"0" ) +
//			    	Double.parseDouble(monthlyIncomeView.getOthers() !=null && monthlyIncomeView.getOthers().compareTo("")!=0   ? monthlyIncomeView.getOthers():"0" )) ;
//			    
//			    	monthlyIncomeView.setTotal(total.toString());
//				
//			}
//			setDataListMonthlyIncome(monthlyIncome);
//		}
//		}
//		catch(Exception ex)
//		{
//			logger.LogException("method calculatetotal crashed", ex);
//		}
//		
//	}
//	
//	public void calculateTotalForMonthlyExpenses() 
//	{
//		// TODO Auto-generated method stub
//		this.personId = Long.parseLong(viewRootMap.get(WebConstants.PERSON_ID).toString());
//		try
//		{
//		if(personId!=null)
//		{
//			List<MonthlyExpenseView> monthlyExpense =getDataListMonthlyExpenses();
//			
//			for (MonthlyExpenseView monthlyExpenseView : monthlyExpense) {
//			    Double total=(
//			    	Double.parseDouble(monthlyExpenseView.getFood()!=null && monthlyExpenseView.getFood().compareTo("")!=0 ?monthlyExpenseView.getFood():"0" )+
//			    	Double.parseDouble(monthlyExpenseView.getClothes()!=null && monthlyExpenseView.getClothes().compareTo("")!=0 ? monthlyExpenseView.getClothes():"0")+
//			    	Double.parseDouble(monthlyExpenseView.getStationary()!=null && monthlyExpenseView.getStationary().compareTo("")!=0  ? monthlyExpenseView.getStationary():"0")+
//			    	Double.parseDouble(monthlyExpenseView.getPersonalExp()!=null && monthlyExpenseView.getPersonalExp().compareTo("")!=0  ? monthlyExpenseView.getPersonalExp():"0")+
//			    	Double.parseDouble(monthlyExpenseView.getTelephone()!=null && monthlyExpenseView.getTelephone().compareTo("")!=0   ? monthlyExpenseView.getTelephone():"0")+
//			    	Double.parseDouble(monthlyExpenseView.getMobile()!=null && monthlyExpenseView.getMobile().compareTo("")!=0   ? monthlyExpenseView.getMobile():"0" ))+
//			    	Double.parseDouble(monthlyExpenseView.getElectricity()!=null && monthlyExpenseView.getElectricity().compareTo("")!=0   ? monthlyExpenseView.getElectricity():"0" )+
//			    	Double.parseDouble(monthlyExpenseView.getPetrol()!=null && monthlyExpenseView.getPetrol().compareTo("")!=0   ? monthlyExpenseView.getPetrol():"0" )+
//			    	Double.parseDouble(monthlyExpenseView.getEmergency()!=null && monthlyExpenseView.getEmergency().compareTo("")!=0   ? monthlyExpenseView.getEmergency():"0" )+
//			    	Double.parseDouble(monthlyExpenseView.getEntertainment()!=null && monthlyExpenseView.getEntertainment().compareTo("")!=0   ? monthlyExpenseView.getEntertainment():"0" )+
//			    	Double.parseDouble(monthlyExpenseView.getServant()!=null && monthlyExpenseView.getServant().compareTo("")!=0   ? monthlyExpenseView.getServant():"0" );
//			    
//			    
//			    monthlyExpenseView.setTotal(total.toString());
//				
//			}
//			setDataListMonthlyExpenses(monthlyExpense);
//		}
//		}
//		catch(Exception ex)
//		{
//			logger.LogException("method calculateTotalForMonthlyExpenses crashed", ex);
//		}
//		
//	}
//	
//	
//	
//	public void calculateTotalForAnnualIncome() 
//	{
//		// TODO Auto-generated method stub
//		this.personId = Long.parseLong(viewRootMap.get(WebConstants.PERSON_ID).toString());
//		try
//		{
//		if(personId!=null)
//		{
//			List<AnnualIncomeView> annualIncome =getDataListAnnualIncome();
//			
//			for (AnnualIncomeView annualIncomeView : annualIncome) {
//			    Double total=(
//			    	Double.parseDouble(annualIncomeView.getRealEstate()!=null && annualIncomeView.getRealEstate().compareTo("")!=0 ?annualIncomeView.getRealEstate():"0" )+
//			    	Double.parseDouble(annualIncomeView.getShares()!=null && annualIncomeView.getShares().compareTo("")!=0 ? annualIncomeView.getShares():"0")+
//			    	Double.parseDouble(annualIncomeView.getLands()!=null && annualIncomeView.getLands().compareTo("")!=0  ? annualIncomeView.getLands():"0")+
//			    	Double.parseDouble(annualIncomeView.getTradeLicenses() !=null && annualIncomeView.getTradeLicenses().compareTo("")!=0   ? annualIncomeView.getTradeLicenses():"0")+
//			    	Double.parseDouble(annualIncomeView.getOthers()  !=null && annualIncomeView.getOthers().compareTo("")!=0   ? annualIncomeView.getOthers():"0" ));
//			    
//			    annualIncomeView.setTotal(total.toString());
//				
//			}
//			setDataListAnnualIncome(annualIncome);
//		}
//		}
//		catch(Exception ex)
//		{
//			logger.LogException("method calculateTotalForAnnualIncome crashed", ex);
//		}
//		
//	}
//	
//		
////Retrives the first record to display
//
//	public void tabFinancialAspects_Click() {
//
//		viewRootMap.put("financialAspectsActive", true);
//		ResearchFormBeneficiaryService service = new ResearchFormBeneficiaryService();
//		Long personId = null;
//		if(viewRootMap.get(WebConstants.PERSON_ID) != null)
//			personId = new Long(viewRootMap.get(WebConstants.PERSON_ID).toString());
//		if (service.personExists(personId)) {
//
//			List<AnnualIncomeView> viewAnnual = service.getDataForAnnualIncome(personId);
//			List<AnnualIncomeView> listToBindAnnualIncome = new ArrayList<AnnualIncomeView>();
//			
//			if (viewAnnual.size() > 0)
//			{
//				listToBindAnnualIncome.add(viewAnnual.get(viewAnnual.size()-1));
//				this.setDataListAnnualIncome(listToBindAnnualIncome); //a list might come up against a personid , we'l select the first row only for disp
//			}
//
//			List<MonthlyIncomeView> viewIncome = service.getDataForMonthlyIncome(personId);
//			List<MonthlyIncomeView> listToBindMonthlyIncome = new ArrayList<MonthlyIncomeView>();			
//				
//			if (viewIncome.size() > 0)
//			{
//				listToBindMonthlyIncome.add(viewIncome.get(viewIncome.size()-1));		// first record only
//				this.setDataListMonthlyIncome(listToBindMonthlyIncome);
//			}
//
//			List<MonthlyExpenseView> viewExpenses = service.getDataForMonthlyExpenses(personId);
//			List<MonthlyExpenseView> listToBindMonthlyExpense= new ArrayList<MonthlyExpenseView>();
//
//			if (viewExpenses.size() > 0)
//			{
//				listToBindMonthlyExpense.add(viewExpenses.get(viewExpenses.size()-1)); // first record only
//				this.setDataListMonthlyExpenses(listToBindMonthlyExpense);
//			}
//
//		}
//
//	}
//
//	private void loadSingleDummyRowInDataTables() {
//		List<MonthlyExpenseView> view = new ArrayList<MonthlyExpenseView>();
//		MonthlyExpenseView expView = new MonthlyExpenseView();
////		expView.setClothes("0");
//		view.add(expView);
//		// view.get(0).setClothes("0");
//		this.setDataListMonthlyExpenses(view);
//
//		List<MonthlyIncomeView> incView = new ArrayList<MonthlyIncomeView>();
//		MonthlyIncomeView incomeView = new MonthlyIncomeView();
////		incomeView.setIncomeSource("0");
//		incView.add(incomeView);
//		this.setDataListMonthlyIncome(incView);
//
//		List<AnnualIncomeView> anuView = new ArrayList<AnnualIncomeView>();
//		AnnualIncomeView annualView = new AnnualIncomeView();
////		annualView.setIncomeSource("0");
//		anuView.add(annualView);
//		this.setDataListAnnualIncome(anuView);
//		// TODO Auto-generated method stub
//
//	}
//
//	private void loadAccomodationAspectsByPersonId() {
//		// TODO Auto-generated method stub
//		String methodName="loadAccomodationAspectsByPersonId";
//		logger.logInfo("loadAccomodationAspectsByPersonId started...");
//		try
//		{
//		ResearchFormBeneficiaryService service = new ResearchFormBeneficiaryService();
//		List<AccomodationAspects> aspectsList = service
//				.getAccomdationAspectsByPersonId(this.personId);
//		
//		AccomodationAspects aspects =null; 
//		
//		
//		if(aspectsList.size()>0)
//		{
//			Collections.sort(aspectsList, new ListComparator("accAspId",sortAscending));
//			aspects=aspectsList.get(aspectsList.size()-1);
//		}
//
//		List<String> homeTypes=null;
//		if(aspects != null)
//		{
//			if(aspects.getPerson() != null)
//			{
//				if(aspects.getPerson().getIsMinor() != null && aspects.getPerson().getIsMinor().compareTo(1L) == 0)
//					htmlIsMinor.setValue(true);
//				else
//					htmlIsMinor.setValue(false);
//				
//				if(aspects.getPerson().getIsStudent() != null && aspects.getPerson().getIsStudent().compareTo(1L) == 0)
//					htmlIsStudent.setValue(true);
//				else
//					htmlIsStudent.setValue(false);
//				
//				if(aspects.getPerson().getIncomeCategory() != null && aspects.getPerson().getIncomeCategory().compareTo(-1L) != 0)
//					inComeCategorySelectMenu.setValue(aspects.getPerson().getIncomeCategory().toString());
//				
//			}
//			
//			if(aspects.getAccomodationOnwershipType() != null && aspects.getAccomodationOnwershipType().getTypeId()!=null)
//				this.accOwnershipSelectMenu.setValue(aspects.getAccomodationOnwershipType().getTypeId().toString());
//			if(aspects.getAccomodationType() != null && aspects.getAccomodationType().getTypeId()!=null)
//				this.accTypeSelectMenu.setValue(aspects.getAccomodationType().getTypeId().toString());
//			if(aspects.getResidenceType() != null && aspects.getResidenceType().getTypeId()!=null)
//				this.resTypeSelectMenu.setValue(aspects.getResidenceType().getTypeId().toString());
//			if(aspects.getPropertyBeneficiaryType() != null && aspects.getPropertyBeneficiaryType().getTypeId()!=null)
//				this.beneficiaryPropSelectMenu.setValue(aspects.getPropertyBeneficiaryType().getTypeId().toString());
//			if(aspects.getHomeType()!=null)
//			{
//				String homeTypeArray[]=aspects.getHomeType().split(",");
//				homeTypes=Arrays.asList(homeTypeArray);
//				for (String string : homeTypes)
//				{
//					this.typeOfHome.add(string);	
//				}
//				this.setTypeOfHome(typeOfHome);
//			}
//			 
//			if(aspects.getHomeConditionType() != null && aspects.getHomeConditionType().getTypeId()!=null)
//				this.homeConditionSelectMenu.setValue(aspects.getHomeConditionType().getTypeId().toString()	);
//			if(aspects.getRentingReason() != null && aspects.getRentingReason().getRentingReasonId()!=null)
//				this.reasonForRentSelectMenu.setValue(aspects.getRentingReason().getRentingReasonId().toString());
//			if(aspects.getPerson() != null && aspects.getPerson().getIncomeCategory()!=null)
//				this.inComeCategorySelectMenu.setValue(aspects.getPerson().getIncomeCategory().toString());
//			
//			//loading description Boxes
//			this.accTypeDesc.setValue(aspects.getAccTypeDesc());
//			this.accOwnershipDesc.setValue(aspects.getAccOwnershipDesc());
//			this.resTypeDesc.setValue(aspects.getResTypeDesc());	
//			this.homeConditionDesc.setValue(aspects.getHomeConditionDesc());
//			this.rentingReasonDesc.setValue(aspects.getRentingReasonDesc());
//			this.homeTypeDesc.setValue(aspects.getHomeTypeDesc());
//			
//		}
//		}
//		catch(Exception ex)
//		{
//			logger.LogException("loadAccomodationAspectsByPersonId() crashed", ex);
//		}
//
//	}
//
//	public void loadLists() throws PimsBusinessException {
//		loadAccTypeList();
//		loadAccOwnershipList();
//		loadResTypeList();
//		loadBeneficiaryOfProperty();
//		loadTypesOfHome();
//		loadConditionOfHome();
//		loadReasonsForRentingList();
//		loadIncomeCategory(); // throws PIMSBusinessException
//	}
//
//	private void loadIncomeCategory() throws PimsBusinessException {
//
//		List<SelectItem> selectItem = new ArrayList<SelectItem>();
//		ResearchFormBeneficiaryService service = new ResearchFormBeneficiaryService();
//		List<DomainData> domainData = service.getIncomeCategoryList();
//
//		// List<DomainDataView> domainData = CommonUtil
//		// .getDomainDataListForDomainType();
//
//		for (DomainData domainDataView : domainData) {
//			SelectItem item = new SelectItem();
//			item.setValue(domainDataView.getDomainDataId().toString());
//			if (CommonUtil.getIsEnglishLocale())
//				item.setLabel(domainDataView.getDataDescEn());
//			else
//				item.setLabel(domainDataView.getDataDescAr());
//
//			selectItem.add(item);
//		}
//
//		this.setIncomeCategoryList(selectItem);
//
//		// TODO Auto-generated method stub
//
//	}
//
//	private void loadReasonsForRentingList() {
//		List<SelectItem> selectItem = new ArrayList<SelectItem>();
//		ResearchFormBeneficiaryService service = new ResearchFormBeneficiaryService();
//		List<RentingReason> reasonsForRentingPojo = service
//				.getReasonsForRentingList();
//		rentingReasonMap=service.getRentingReasonMap();
//		this.setRentingReasonMap(rentingReasonMap);
//		if (reasonsForRentingPojo != null && reasonsForRentingPojo.size() > 0) {
//			for (RentingReason rentingReason : reasonsForRentingPojo) {
//				// IF LOCALE ENGLISH - SEND THAT BOOLEAN TO THE SERVICE THROUGH
//				// THE BACKINGBEAN
//				SelectItem item = new SelectItem();
//				if (CommonUtil.getIsEnglishLocale()) {
//					item = new SelectItem(rentingReason.getRentingReasonId()
//							.toString(), rentingReason.getReasonNameEn());
//				} else
//					item = new SelectItem(rentingReason.getRentingReasonId()
//							.toString(), rentingReason.getReasonNameAr());
//
//				selectItem.add(item);
//			}
//		}
//
//		this.setReasonForRentList(selectItem);
//
//	}
//
//	private void loadConditionOfHome() {
//		List<SelectItem> selectItem = new ArrayList<SelectItem>();
//		ResearchFormBeneficiaryService service = new ResearchFormBeneficiaryService();
//		List<HomeConditionType> homeConditionPojo = service.getConditonOfHomeList();
//		homeConditionMap=service.getHomeConditionMap();
//		this.setHomeConditionMap(homeConditionMap);
//		if (homeConditionPojo != null && homeConditionPojo.size() > 0) {
//			for (HomeConditionType homecondition : homeConditionPojo) {
//				// IF LOCALE ENGLISH - SEND THAT BOOLEAN TO THE SERVICE THROUGH
//				// THE BACKINGBEAN
//				SelectItem item = new SelectItem();
//				if (CommonUtil.getIsEnglishLocale()) {
//					item = new SelectItem(homecondition.getTypeId().toString(),
//							homecondition.getTypeNameEn());
//				} else
//					item = new SelectItem(homecondition.getTypeId().toString(),
//							homecondition.getTypeNameAr());
//
//				selectItem.add(item);
//			}
//		}
//
//		this.setHomeConditionList(selectItem);
//
//	}
//
//	private void loadTypesOfHome() {
//		List<SelectItem> selectItem = new ArrayList<SelectItem>();
//		ResearchFormBeneficiaryService service = new ResearchFormBeneficiaryService();
//		List<HomeType> typesOfHomePojoList = service.getTypesOfHomeList();
//		typeOfHomeMap=service.getTypesOfHomeMap();
//		this.setTypeOfHomeMap(typeOfHomeMap);
//		if (typesOfHomePojoList != null && typesOfHomePojoList.size() > 0) {
//			for (HomeType homeType : typesOfHomePojoList) {
//				// IF LOCALE ENGLISH - SEND THAT BOOLEAN TO THE SERVICE THROUGH
//				// THE BACKINGBEAN
//				SelectItem item = new SelectItem();
//				if (CommonUtil.getIsEnglishLocale()) {
//					item = new SelectItem(homeType.getTypeId().toString(),
//							homeType.getTypeNameEn());
//				} else
//					item = new SelectItem(homeType.getTypeId().toString(),
//							homeType.getTypeNameAr());
//
//				selectItem.add(item);
//			}
//		}
//
//		this.setTypeOfHomeList(selectItem);
//		// TODO Auto-generated method stub
//
//	}
//
//	private void loadBeneficiaryOfProperty() {
//		List<SelectItem> selectItem = new ArrayList<SelectItem>();
//		ResearchFormBeneficiaryService service = new ResearchFormBeneficiaryService();
//		List<PropertyBeneficiaryType> propertyBeneficiaryPojoList = service
//				.getBeneficiaryPropertyList();
//		beneficiaryOfPropertyMap=service.getBeneficiaryOfPropertyMap();
//		this.setBeneficiaryOfPropertyMap(beneficiaryOfPropertyMap);
//		if (propertyBeneficiaryPojoList != null
//				&& propertyBeneficiaryPojoList.size() > 0) {
//			for (PropertyBeneficiaryType propBenefType : propertyBeneficiaryPojoList) {
//				// IF LOCALE ENGLISH - SEND THAT BOOLEAN TO THE SERVICE THROUGH
//				// THE BACKINGBEAN
//				SelectItem item = new SelectItem();
//				if (CommonUtil.getIsEnglishLocale()) {
//					item = new SelectItem(propBenefType.getTypeId().toString(),
//							propBenefType.getTypeNameEn());
//				} else
//					item = new SelectItem(propBenefType.getTypeId().toString(),
//							propBenefType.getTypeNameAr());
//
//				selectItem.add(item);
//			}
//		}
//
//		this.setBeneficiaryPropList(selectItem);
//
//	}
//
//	private void loadResTypeList() {
//
//		List<SelectItem> selectItem = new ArrayList<SelectItem>();
//		ResearchFormBeneficiaryService service = new ResearchFormBeneficiaryService();
//		List<ResidenceType> residentTypePojoList = service
//				.getResidentTypeList();
//		residenceTypeMap=service.getResidenceTypeMap();
//		this.setResidenceTypeMap(residenceTypeMap);
//		if (residentTypePojoList != null && residentTypePojoList.size() > 0) {
//			for (ResidenceType residenceType : residentTypePojoList) {
//				// IF LOCALE ENGLISH - SEND THAT BOOLEAN TO THE SERVICE THROUGH
//				// THE BACKINGBEAN
//				SelectItem item = new SelectItem();
//				if (CommonUtil.getIsEnglishLocale()) {
//					item = new SelectItem(residenceType.getTypeId().toString(),
//							residenceType.getTypeNameEn());
//				} else
//					item = new SelectItem(residenceType.getTypeId().toString(),
//							residenceType.getTypeNameAr());
//
//				selectItem.add(item);
//			}
//		}
//
//		this.setResTypeList(selectItem);
//
//	}
//
//	private void loadAccOwnershipList() {
//		List<SelectItem> selectItem = new ArrayList<SelectItem>();
//		ResearchFormBeneficiaryService service = new ResearchFormBeneficiaryService();
//		List<AccomodationOnwershipType> accomodationOwnerPojoList = service
//				.getAccomodationOwnerList();
//		accomodationOwnershipMap=service.getAccomodationOwnershipMap();
//		this.setAccomodationOwnershipMap(accomodationOwnershipMap);
//		if (accomodationOwnerPojoList != null
//				&& accomodationOwnerPojoList.size() > 0) {
//			for (AccomodationOnwershipType accomodationType : accomodationOwnerPojoList) {
//				// IF LOCALE ENGLISH - SEND THAT BOOLEAN TO THE SERVICE THROUGH
//				// THE BACKINGBEAN
//				SelectItem item = new SelectItem();
//				if (CommonUtil.getIsEnglishLocale()) {
//					item = new SelectItem(accomodationType.getTypeId()
//							.toString(), accomodationType.getTypeNameEn());
//				} else
//					item = new SelectItem(accomodationType.getTypeId()
//							.toString(), accomodationType.getTypeNameAr());
//
//				selectItem.add(item);
//			}
//		}
//
//		this.setAccOwnershipList(selectItem);
//		// TODO Auto-generated method stub
//
//	}
//
//	private void loadAccTypeList() {
//
//		List<SelectItem> selectItem = new ArrayList<SelectItem>();
//		ResearchFormBeneficiaryService service = new ResearchFormBeneficiaryService();
//		List<AccomodationType> accomodationTypePojoList = service
//				.getAccomodationTypeList();
//		accomodationTypeMap= service
//		.getAccomodationTypeMap();
//		this.setAccomodationTypeMap(accomodationTypeMap);
//		// selectItem = service.getAccomodationTypeList(CommonUtil
//		// .getIsEnglishLocale());
//
//		if (accomodationTypePojoList != null
//				&& accomodationTypePojoList.size() > 0) {
//			for (AccomodationType accomodationType : accomodationTypePojoList) {
//				// IF LOCALE ENGLISH - SEND THAT BOOLEAN TO THE SERVICE THROUGH
//				// THE BACKINGBEAN
//				SelectItem item = new SelectItem();
//				if (CommonUtil.getIsEnglishLocale()) {
//					item = new SelectItem(accomodationType.getTypeId()
//							.toString(), accomodationType.getTypeNameEn());
//				} else
//					item = new SelectItem(accomodationType.getTypeId()
//							.toString(), accomodationType.getTypeNameAr());
//
//				selectItem.add(item);
//			}
//		}
//
//		this.setAccTypeList(selectItem);
//		// TODO Auto-generated method stub
//
//	}
//
//	private void loadDisbursementTypeList() throws Exception {
//
//	}
//
//	public Integer getRecordSize() {
//		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
//				.getAttributes();
//		// Map viewMap = getFacesContext().getExternalContext().getSessionMap();
//		recordSize = (Integer) viewMap.get("recordSize");
//		if (recordSize == null)
//			recordSize = 0;
//		return recordSize;
//	}
//
//	public boolean getIsViewModePopUp() {
//		boolean returnVal = false;
//
//		if (viewRootMap.containsKey(VIEW_MODE)
//				&& viewRootMap.get(VIEW_MODE) != null) {
//			if (getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())
//				returnVal = true;
//		}
//
//		else {
//			returnVal = false;
//		}
//
//		return returnVal;
//
//	}
//	public boolean getInheritanceFileIdExists() {
//		boolean returnVal = false;
//
//		if (viewRootMap.containsKey(WebConstants.InheritanceFile.INHERITANCE_FILE_ID)
//				&& viewRootMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null) {
//			
//				returnVal = true;
//		}
//
//		else {
//			returnVal = false;
//		}
//
//		return returnVal;
//
//	}
//
//	public boolean getIsPageModeSelectOnePopUp() {
//		isPageModeSelectOnePopUp = false;
//		if (viewRootMap.containsKey(VIEW_MODE)
//				&& viewRootMap.get(VIEW_MODE) != null
//				&& viewRootMap.get(VIEW_MODE).toString().trim()
//						.equalsIgnoreCase(MODE_SELECT_ONE_POPUP)) {
//			isPageModeSelectOnePopUp = true;
//		}
//		return isPageModeSelectOnePopUp;
//
//	}
//
//	public boolean getIsPageModeSelectManyPopUp() {
//		isPageModeSelectManyPopUp = false;
//		if (viewRootMap.containsKey(VIEW_MODE)
//				&& viewRootMap.get(VIEW_MODE) != null
//				&& viewRootMap.get(VIEW_MODE).toString().trim()
//						.equalsIgnoreCase(MODE_SELECT_MANY_POPUP)) {
//			isPageModeSelectManyPopUp = true;
//		}
//		return isPageModeSelectManyPopUp;
//	}
//
//	public List<SelectItem> getAssetTypesList() {
//		List<SelectItem> assetTypes = new ArrayList<SelectItem>();
//		try {
//
//			assetTypes = CommonUtil.getAssetTypesList();
//
//		} catch (PimsBusinessException e) {
//			logger.LogException("getAssetTypesList() crashed", e);
//		}
//		return assetTypes;
//	}
//
//	public void doSearch() {
//		try {
//
//			loadDataList();
//		} catch (Exception e) {
//			System.out.println("Exception doSearch" + e);
//		}
//	}
//
//	public List<SelectItem> getFileStatus() {
//		fileStatus.clear();
//		try {
//
//			loadFileStatusList();
//
//		} catch (Exception ex) {
//
//		}
//		//		
//
//		return null;
//	}
//
//	public void btnSave_Click() 
//	{
//		errorMessages = new ArrayList<String>();
//		try 
//		{
//			HashMap<String,Object> map  =  new HashMap<String, Object>();
//			ResearchFormBeneficiaryView view = getFormParams( map );
//			if(view != null)
//			{
//				ResearchFormBeneficiaryService service = new ResearchFormBeneficiaryService();
//				service.saveAccomodationAspects(view,CommonUtil.getLoggedInUser());
//				this.setDataListResearchGridView(view);
//				successMessages.add(CommonUtil.getBundleMessage("successMsg.accAspectSaved"));
//				successMessages.add(CommonUtil.getBundleMessage("successMsg.finAspectSaved"));
//			}
//		}
//		catch (Exception e) 
//		{
//			logger.LogException("btnSave_Click() crashed |", e);
//			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//		}
//		
//	}
//
//
//	@SuppressWarnings("unchecked")
//	private ResearchFormBeneficiaryView getFormParams(HashMap<String,Object> map   ) throws Exception 
//	{
//		final String methodName = "getFormParams";
//		logger.logInfo(methodName + " Started");
//		String minusOne = "-1";
//		Boolean hasError=false;
//		ResearchFormBeneficiaryView view = new ResearchFormBeneficiaryView();
//		Object isMinor = htmlIsMinor.getValue();
//		Object isStudent = htmlIsStudent.getValue();
//		
//		Object applyToAllBeneficiaries = htmlApplyToAllBeneficiaries.getValue();
//		
//		//New fields
//		Object homeTypeDesc = this.homeTypeDesc.getValue();
//		Object rentingReasonDesc = this.rentingReasonDesc.getValue();
//		Object homeConditionDesc = this.homeConditionDesc.getValue();
//		Object accOwnershipDesc = this.accOwnershipDesc.getValue();
//		Object accTypeDesc = this.accTypeDesc.getValue();
//		Object resTypeDesc = this.resTypeDesc.getValue();
//		List<String> homeType = getTypeOfHome();
//		if (accTypeSelectMenu.getValue() != null && !accTypeSelectMenu.getValue().toString().trim().equals("") && !accTypeSelectMenu.getValue().toString().trim().equals(minusOne) ) 
//		{
//				view.setAccType( Long.valueOf( accTypeSelectMenu.getValue().toString() ) );
//				if(checkForMandatoryDesc(Long.valueOf(  accTypeSelectMenu.getValue().toString() ),getAccomodationTypeMap() ) )
//				{
//					if(accTypeDesc != null && StringHelper.isNotEmpty(accTypeDesc.toString()))
//					{
//					    view.setAccTypeDesc(accTypeDesc.toString());
//					}
//					else
//					{
//						errorMessages.add(CommonUtil.getBundleMessage("errMsg.accTypeDesc"));
//						hasError=true;
//					}
//				}
//				
//		}
//			if (accOwnershipSelectMenu.getValue() != null && !accOwnershipSelectMenu.getValue().toString().trim().equals("")	
//					&& !accOwnershipSelectMenu.getValue().toString().trim().equals(minusOne)
//					) 
//			{
//				view.setAccOwnership(Long.parseLong((accOwnershipSelectMenu.getValue().toString())));
//				
//				if(checkForMandatoryDesc(Long.parseLong((accOwnershipSelectMenu.getValue().toString())),getAccomodationOwnershipMap()))
//				{
//					if(accOwnershipDesc != null && StringHelper.isNotEmpty(accOwnershipDesc.toString()))
//					{
//					view.setAccomodationOwnership(accOwnershipDesc.toString());
//					}
//					else
//					{
//						errorMessages.add(CommonUtil.getBundleMessage("errMsg.accOwnDesc"));
//						hasError=true;
//					}
//				}
//			}
//			
//			if (resTypeSelectMenu.getValue() != null && !resTypeSelectMenu.getValue().toString().trim().equals("") && 
//					!resTypeSelectMenu.getValue().toString().trim().equals(minusOne) 
//					) 
//			{
//				view.setResType(Long.parseLong((resTypeSelectMenu.getValue().toString())));
//				if(checkForMandatoryDesc(Long.parseLong((resTypeSelectMenu.getValue().toString())),getResidenceTypeMap()))
//				{
//					if(resTypeDesc != null && StringHelper.isNotEmpty(resTypeDesc.toString()))
//					{
//					view.setResTypeDesc(resTypeDesc.toString());
//					}
//					else
//					{
//						errorMessages.add(CommonUtil.getBundleMessage("errMsg.resTypeDesc"));
//						hasError=true;
//					}
//				}
//			}
//			
//			
//			if (beneficiaryPropSelectMenu.getValue() != null && beneficiaryPropSelectMenu.getValue().toString().trim().length() > 0  &&
//				!beneficiaryPropSelectMenu.getValue().equals(minusOne) )
//			{
//				view.setBeneficiaryOfProperty(Long.parseLong((beneficiaryPropSelectMenu.getValue().toString())));
//			}
//
//			if ( homeConditionSelectMenu.getValue() != null && !homeConditionSelectMenu.getValue().toString().trim().equals("") &&
//				 !homeConditionSelectMenu.getValue().equals(minusOne) ) 
//			{
//				view.setConditionOfHome(Long.parseLong(homeConditionSelectMenu.getValue().toString() ));
//				if(checkForMandatoryDesc( Long.parseLong( homeConditionSelectMenu.getValue().toString() ),getHomeConditionMap() ) )
//				{
//					if(homeConditionDesc != null && StringHelper.isNotEmpty(homeConditionDesc.toString()))
//					{
//					view.setConditionOfHomeDesc(homeConditionDesc.toString());
//					}
//					else
//					{
//						errorMessages.add(CommonUtil.getBundleMessage("errMsg.homecondDesc"));
//						hasError=true;
//					}
//				}
//			}
//			
//			if ( reasonForRentSelectMenu.getValue() != null && !reasonForRentSelectMenu.getValue().toString().trim().equals("")
//				&& !reasonForRentSelectMenu.getValue().equals(minusOne)
//			    ) 
//			{
//				view.setReasonForRent(Long.parseLong(reasonForRentSelectMenu.getValue().toString() ));
//				if(checkForMandatoryDesc(Long.parseLong((reasonForRentSelectMenu.getValue().toString())),getRentingReasonMap()))
//				{
//					if(rentingReasonDesc != null && StringHelper.isNotEmpty(rentingReasonDesc.toString()))
//					{
//					view.setReasonForRentDesc(rentingReasonDesc.toString());
//					}
//					else
//					{
//						errorMessages.add(CommonUtil.getBundleMessage("errMsg.rentingReasonDesc"));
//						hasError=true;
//					}
//				}
//			}
//			
//			if (inComeCategorySelectMenu.getValue() != null && !inComeCategorySelectMenu.getValue().toString().trim().equals("") ) 
//			{
//				view.setIncomeCategory( Long.parseLong( inComeCategorySelectMenu.getValue().toString() ) );
//			}
//
//			if (homeType != null &&  homeType.size()>0)
//			{
//				view.setTypeOfHome(homeType);
//				if(checkForMandatoryDesc(homeType,getTypeOfHomeMap()))
//				{
//					if(homeTypeDesc != null && StringHelper.isNotEmpty(homeTypeDesc.toString()))
//					{
//					view.setTypeOfHomeDesc(homeTypeDesc.toString());
//					}
//					else
//					{
//						errorMessages.add(CommonUtil.getBundleMessage("errMsg.homeTypeDesc"));
//						hasError=true;
//					}
//				}
//				
//			}
//			if (isMinor != null) 
//			{
//				view.setIsMinor(isMinor.toString() == "true" ? new Long(1): new Long(0));
//			}
//			if (isStudent != null) 
//			{
//				view.setIsStudent(isStudent.toString() == "true" ? new Long(1): new Long(0));
//			}
//			if (applyToAllBeneficiaries!= null && getInheritanceFileIdExists() ) 
//			{
//				view.setApplyToAllBeneficiaries(applyToAllBeneficiaries.toString() == "true" ? new Long(1): new Long(0));
//				view.setInhFileId(new Long(viewRootMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID).toString()));
//			}
//
//			if (viewRootMap.get(WebConstants.PERSON_ID) != null) 
//			{
//				view.setPersonId(Long.parseLong(  viewRootMap .get(WebConstants.PERSON_ID).toString() ) );
//			}
//			if ( getDataListAnnualIncome()!= null ) 
//			{
//				map.put( Constant.ResearchForm.ANNUAL_INCOME,(AnnualIncomeView)getDataListAnnualIncome().get(0) );
//			}
//			dataListMonthlyExpenses=getDataListMonthlyExpenses();
//			if (getDataListMonthlyExpenses() != null ) 
//			{
//				map.put( Constant.ResearchForm.MONTHLY_EXPENSE, (MonthlyExpenseView) getDataListMonthlyExpenses().get(0) );
//			}
//			if (getDataListMonthlyIncome() != null) 
//			{
//				map.put( Constant.ResearchForm.MONTHLY_INCOME, (MonthlyIncomeView) getDataListMonthlyIncome().get(0) );
//			}
//		logger.logInfo(methodName + "ended");
//		if(hasError==true)
//			return null;
//		else
//			return view;
//	}
//
//	private Boolean checkForMandatoryDesc(Long accTypeId,
//			HashMap<Long, String> accomodationTypeMap2) {
//		
//		if(accomodationTypeMap2.containsKey(accTypeId)) 
//			return true;
//		else
//			return false;			
//			
//	}
//	private Boolean checkForMandatoryDesc(List<String> homeTypeIds,
//			HashMap<Long, String> accomodationTypeMap2) {
//		Boolean boolToReturn = false;
//		for (String string : homeTypeIds) 
//		{
//			if(accomodationTypeMap2.containsKey(Long.parseLong(string))) 
//				boolToReturn=true;
//			else
//				boolToReturn= false;
//			
//		}
//		return boolToReturn;
//					
//			
//	}
//
//	private void loadFileStatusList() throws PimsBusinessException {
//
//		List<SelectItem> allowedStatuses = new ArrayList<SelectItem>();
//
//		List<DomainDataView> domainDataList = CommonUtil
//				.getDomainDataListForDomainType(WebConstants.InheritanceFileStatus.INH_FILE_STATUS);
//		for (DomainDataView dd : domainDataList) {
//			boolean isAdd = true;
//
//			// if(getIsContextAuctionUnits() && (
//			// !dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS)) )
//			// isAdd = false;
//			//
//			// else if(getIsContextNewLeaseContract() && (
//			// !dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS)) )
//			// isAdd = false;
//			//			 
//			// else if(getIsContextInspectionUnits() &&
//			// !(dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS) ||
//			// dd.getDataValue().equals(WebConstants.UNIT_RENTED_STATUS) ) )
//			// isAdd = false;
//			//			 
//			// else if(getIsContextMaintenanceRequest() &&
//			// !(dd.getDataValue().equals(WebConstants.UNIT_RENTED_STATUS) ) )
//			// isAdd = false;
//
//			if (isAdd) {
//				SelectItem item = null;
//
//				if (CommonUtil.getIsEnglishLocale())
//					item = new SelectItem(dd.getDomainDataId().toString(), dd
//							.getDataDescEn());
//				else
//					item = new SelectItem(dd.getDomainDataId().toString(), dd
//							.getDataDescAr());
//
//				allowedStatuses.add(item);
//			}
//
//		}
//
//		this.setFileStatusList(allowedStatuses);
//
//	}
//
//	private void loadFileTypeList() throws PimsBusinessException {
//
//		List<SelectItem> allowedTypes = new ArrayList<SelectItem>();
//
//		List<DomainDataView> domainDataList = CommonUtil
//				.getDomainDataListForDomainType(WebConstants.InheritanceFileType.INH_FILE_TYPE);
//		for (DomainDataView dd : domainDataList) {
//			boolean isAdd = true;
//
//			// if(getIsContextAuctionUnits() && (
//			// !dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS)) )
//			// isAdd = false;
//			//
//			// else if(getIsContextNewLeaseContract() && (
//			// !dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS)) )
//			// isAdd = false;
//			//				 
//			// else if(getIsContextInspectionUnits() &&
//			// !(dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS) ||
//			// dd.getDataValue().equals(WebConstants.UNIT_RENTED_STATUS) ) )
//			// isAdd = false;
//			//				 
//			// else if(getIsContextMaintenanceRequest() &&
//			// !(dd.getDataValue().equals(WebConstants.UNIT_RENTED_STATUS) ) )
//			// isAdd = false;
//
//			if (isAdd) {
//				SelectItem item = null;
//
//				if (CommonUtil.getIsEnglishLocale())
//					item = new SelectItem(dd.getDomainDataId().toString(), dd
//							.getDataDescEn());
//				else
//					item = new SelectItem(dd.getDomainDataId().toString(), dd
//							.getDataDescAr());
//
//				allowedTypes.add(item);
//			}
//
//		}
//
//		this.setFileTypeList(allowedTypes);
//
//	}
//
//	private List<PeriodicDisbursementGridView> loadDataList() {
//		// String methodName = "loadDataList";
//		// if (dataList != null)
//		// dataList.clear();
//		//
//		// List<PeriodicDisbursementGridView> list = new ArrayList();
//		//
//		// try {
//		// searchDisbursementMap = getSearchCriteria();
//		// DisbursementService searchService = new DisbursementService();
//		// //
//		// //
//		// // /////////////////////////////////////////////// For server side
//		// // paging paging/////////////////////////////////////////
//		// int totalRows = searchService
//		// .searchDisbursementsGetTotalNumberOfRecords(searchDisbursementMap);
//		// setTotalRows(totalRows);
//		// doPagingComputations();
//		// //
//		// //////////////////////////////////////////////////////////////////////////////////////////////
//		// //
//		// //
//		//
//		// list = searchService.getDisbursementsByCriteria(
//		// searchDisbursementMap, getRowsPerPage(), getCurrentPage(),
//		// getSortField(), isSortItemListAscending());
//		//
//		// if (list.isEmpty() || list == null) {
//		// errorMessages = new ArrayList<String>();
//		// errorMessages
//		// .add(CommonUtil
//		// .getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
//		// }
//		// Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
//		// .getAttributes();
//		// viewMap.put("periodicDisbursementList", list);
//		// recordSize = totalRows;
//		// viewMap.put("recordSize", totalRows);
//		// paginatorRows = getPaginatorRows();
//		// paginatorMaxPages = recordSize / paginatorRows;
//		// if ((recordSize % paginatorRows) > 0)
//		// paginatorMaxPages++;
//		// if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
//		// paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
//		// viewMap.put("paginatorMaxPages", paginatorMaxPages);
//		// } catch (Exception ex) {
//		// ex.printStackTrace();
//		// }
//		// return list;
//		// // TODO Auto-generated method stub
//		//
//		// }
//		//
//		// public Integer getPaginatorRows() {
//		// paginatorRows = WebConstants.RECORDS_PER_PAGE;
//		// return paginatorRows;
//		return null;
//	}
//
//	@SuppressWarnings("unchecked")
//	public ArrayList<SelectItem> getFileStatusList() {
//		if (viewRootMap.get("fileStatusList") != null)
//			return (ArrayList<SelectItem>) viewRootMap.get("fileStatusList");
//		return new ArrayList<SelectItem>();
//	}
//
//	@SuppressWarnings("unchecked")
//	public void setFileStatusList(List<SelectItem> fileStatusList) {
//
//		this.fileStatusList = fileStatusList;
//		if (this.fileStatusList != null)
//			viewRootMap.put("fileStatusList", this.fileStatusList);
//	}
//
//	public List<SelectItem> getFileTypeList() {
//		if (viewRootMap.get("fileTypeList") != null)
//			return (ArrayList<SelectItem>) viewRootMap.get("fileTypeList");
//		return new ArrayList<SelectItem>();
//	}
//
//	public void setFileTypeList(List<SelectItem> fileTypeList) {
//		this.fileTypeList = fileTypeList;
//		if (fileTypeList != null)
//			viewRootMap.put("fileTypeList", this.fileTypeList);
//	}
//
//	public List<MonthlyIncomeView> getDataListMonthlyIncome() {
//
//		dataListMonthlyIncome = (List<MonthlyIncomeView>) viewMap
//				.get("monthlyIncomeList");
//		if (dataListMonthlyIncome == null)
//			dataListMonthlyIncome = new ArrayList<MonthlyIncomeView>();
//		return dataListMonthlyIncome;
//	}
//
//	public void setDataListMonthlyIncome(List<MonthlyIncomeView> dataList) {
//		this.dataListMonthlyIncome = dataList;
//		if (dataListMonthlyIncome != null) {
//			viewMap.put("monthlyIncomeList", dataListMonthlyIncome);
//		}
//	}
//
//	
//	public String getErrorMessages() {
//		return CommonUtil.getErrorMessages(errorMessages);
//	}
//
//	public String getSuccessMessages() {
//		String methodName = "getSuccessMessages";
//		logger.logInfo(methodName + "|" + " Start");
//
//		String messageList = "";
//		if ((successMessages == null) || (successMessages.size() == 0)) {
//			messageList = "";
//		} else {
//			for (String message : successMessages) {
//				messageList += message + " <br></br>";
//			}
//
//		}
//
//		logger.logInfo(methodName + "|" + " Finish");
//		return messageList;
//
//	}
//
//	public HtmlSelectOneMenu getAccTypeSelectMenu() {
//		return accTypeSelectMenu;
//	}
//
//	public void setAccTypeSelectMenu(HtmlSelectOneMenu accTypeSelectMenu) {
//		this.accTypeSelectMenu = accTypeSelectMenu;
//	}
//
//	public List<SelectItem> getAccTypeList() {
//		List<SelectItem> items = new ArrayList<SelectItem>();
//		if (viewRootMap.get("accTypeList") != null) {
//			items = (ArrayList<SelectItem>) viewRootMap.get("accTypeList");
//
//		}
//
//		return items;
//	}
//
//	public void setAccTypeList(List<SelectItem> accTypeList) {
//		this.accTypeList = accTypeList;
//		if (this.accTypeList != null) {
//			viewRootMap.put("accTypeList", accTypeList);
//		}
//	}
//
//	public HtmlSelectOneMenu getAccOwnershipSelectMenu() {
//		return accOwnershipSelectMenu;
//	}
//
//	public void setAccOwnershipSelectMenu(
//			HtmlSelectOneMenu accOwnershipSelectMenu) {
//		this.accOwnershipSelectMenu = accOwnershipSelectMenu;
//	}
//
//	public List<SelectItem> getAccOwnershipList() {
//		List<SelectItem> items = new ArrayList<SelectItem>();
//		if (viewRootMap.get("accOwnershipList") != null) {
//			items = (ArrayList<SelectItem>) viewRootMap.get("accOwnershipList");
//
//		}
//
//		return items;
//	}
//
//	public void setAccOwnershipList(List<SelectItem> accOwnershipList) {
//		this.accOwnershipList = accOwnershipList;
//		if (this.accOwnershipList != null) {
//			viewRootMap.put("accOwnershipList", accOwnershipList);
//		}
//
//	}
//
//	public HtmlSelectOneMenu getResTypeSelectMenu() {
//		return resTypeSelectMenu;
//	}
//
//	public void setResTypeSelectMenu(HtmlSelectOneMenu resTypeSelectMenu) {
//		this.resTypeSelectMenu = resTypeSelectMenu;
//	}
//
//	public List<SelectItem> getResTypeList() {
//
//		List<SelectItem> items = new ArrayList<SelectItem>();
//		if (viewRootMap.get("resTypeList") != null) {
//			items = (ArrayList<SelectItem>) viewRootMap.get("resTypeList");
//
//		}
//
//		return items;
//	}
//
//	public void setResTypeList(List<SelectItem> resTypeList) {
//		this.resTypeList = resTypeList;
//
//		if (this.resTypeList != null) {
//			viewRootMap.put("resTypeList", resTypeList);
//		}
//	}
//
//	public HtmlSelectOneMenu getBeneficiaryPropSelectMenu() {
//		return beneficiaryPropSelectMenu;
//	}
//
//	public void setBeneficiaryPropSelectMenu(
//			HtmlSelectOneMenu beneficiaryPropSelectMenu) {
//		this.beneficiaryPropSelectMenu = beneficiaryPropSelectMenu;
//	}
//
//	public List<SelectItem> getBeneficiaryPropList() {
//
//		List<SelectItem> items = new ArrayList<SelectItem>();
//		if (viewRootMap.get("beneficiaryPropList") != null) {
//			items = (ArrayList<SelectItem>) viewRootMap
//					.get("beneficiaryPropList");
//
//		}
//
//		return items;
//	}
//
//	public void setBeneficiaryPropList(List<SelectItem> beneficiaryPropList) {
//		this.beneficiaryPropList = beneficiaryPropList;
//		if (this.beneficiaryPropList != null) {
//			viewRootMap.put("beneficiaryPropList", beneficiaryPropList);
//		}
//	}
//
//	public HtmlSelectOneMenu getHomeConditionSelectMenu() {
//		return homeConditionSelectMenu;
//	}
//
//	public void setHomeConditionSelectMenu(
//			HtmlSelectOneMenu homeConditionSelectMenu) {
//		this.homeConditionSelectMenu = homeConditionSelectMenu;
//	}
//
//	public List<SelectItem> getHomeConditionList() {
//		List<SelectItem> items = new ArrayList<SelectItem>();
//		if (viewRootMap.get("homeConditionList") != null) {
//			items = (ArrayList<SelectItem>) viewRootMap
//					.get("homeConditionList");
//
//		}
//
//		return items;
//	}
//
//	public void setHomeConditionList(List<SelectItem> homeConditionList) {
//		this.homeConditionList = homeConditionList;
//
//		if (this.homeConditionList != null) {
//			viewRootMap.put("homeConditionList", homeConditionList);
//		}
//	}
//
//	public HtmlSelectOneMenu getReasonForRentSelectMenu() {
//		return reasonForRentSelectMenu;
//	}
//
//	public void setReasonForRentSelectMenu(
//			HtmlSelectOneMenu reasonForRentSelectMenu) {
//		this.reasonForRentSelectMenu = reasonForRentSelectMenu;
//	}
//
//	public List<SelectItem> getReasonForRentList() {
//
//		List<SelectItem> items = new ArrayList<SelectItem>();
//		if (viewRootMap.get("reasonForRentList") != null) {
//			items = (ArrayList<SelectItem>) viewRootMap
//					.get("reasonForRentList");
//
//		}
//
//		return items;
//	}
//
//	public void setReasonForRentList(List<SelectItem> reasonForRentList) {
//		this.reasonForRentList = reasonForRentList;
//
//		if (this.reasonForRentList != null) {
//			viewRootMap.put("reasonForRentList", reasonForRentList);
//		}
//	}
//
//	@SuppressWarnings("unchecked")
//	public List<String> getTypeOfHome() 
//	{
//		if(viewMap.get("TYPE_OF_HOME_LIST") != null)
//		{
//			typeOfHome = (List<String>) viewMap.get("TYPE_OF_HOME_LIST");
//		}
//		return typeOfHome;
//	}
//
//	@SuppressWarnings("unchecked")
//	public void setTypeOfHome(List<String> typeOfHome) 
//	{
//		if(typeOfHome != null )
//		{
// 			viewMap.put("TYPE_OF_HOME_LIST", typeOfHome);
//		}
//		this.typeOfHome = typeOfHome;
//	}
//
//	public List<SelectItem> getTypeOfHomeList() {
//		List<SelectItem> items = new ArrayList<SelectItem>();
//		if (viewRootMap.get("typeOfHomeList") != null) {
//			items = (ArrayList<SelectItem>) viewRootMap.get("typeOfHomeList");
//
//		}
//
//		return items;
//	}
//
//	public void setTypeOfHomeList(List<SelectItem> typeOfHomeList) {
//		this.typeOfHomeList = typeOfHomeList;
//
//		if (this.typeOfHomeList != null) {
//			viewRootMap.put("typeOfHomeList", typeOfHomeList);
//		}
//
//	}
//
//	public List<SelectItem> getIncomeCategoryList() {
//		List<SelectItem> items = new ArrayList<SelectItem>();
//		if (viewRootMap.get("incomeCategoryList") != null) {
//			items = (ArrayList<SelectItem>) viewRootMap
//					.get("incomeCategoryList");
//
//		}
//
//		return items;
//	}
//
//	public void setIncomeCategoryList(List<SelectItem> incomeCategoryList) {
//		this.incomeCategoryList = incomeCategoryList;
//
//		if (this.incomeCategoryList != null) {
//			viewRootMap.put("incomeCategoryList", incomeCategoryList);
//		}
//	}
//
//	public HtmlSelectOneMenu getInComeCategorySelectMenu() {
//		return inComeCategorySelectMenu;
//	}
//
//	public void setInComeCategorySelectMenu(
//			HtmlSelectOneMenu inComeCategorySelectMenu) {
//		this.inComeCategorySelectMenu = inComeCategorySelectMenu;
//	}
//
//	public javax.faces.component.html.HtmlSelectBooleanCheckbox getHtmlIsMinor() {
//		return htmlIsMinor;
//	}
//
//	public void setHtmlIsMinor(
//			javax.faces.component.html.HtmlSelectBooleanCheckbox htmlIsMinor) {
//		this.htmlIsMinor = htmlIsMinor;
//	}
//
//	public javax.faces.component.html.HtmlSelectBooleanCheckbox getHtmlIsStudent() {
//		return htmlIsStudent;
//	}
//
//	public void setHtmlIsStudent(
//			javax.faces.component.html.HtmlSelectBooleanCheckbox htmlIsStudent) {
//		this.htmlIsStudent = htmlIsStudent;
//	}
//
//	public HtmlDataTable getDataTableAnnualIncome() {
//		return dataTableAnnualIncome;
//	}
//
//	public void setDataTableAnnualIncome(HtmlDataTable dataTableAnnualExpense) {
//		this.dataTableAnnualIncome = dataTableAnnualExpense;
//	}
//
//	public HtmlDataTable getDataTableMonthlyIncome() {
//		return dataTableMonthlyIncome;
//	}
//
//	public void setDataTableMonthlyIncome(HtmlDataTable dataTableMonthlyIncome) {
//		this.dataTableMonthlyIncome = dataTableMonthlyIncome;
//	}
//
//	public List<AnnualIncomeView> getDataListAnnualIncome() {
//		dataListAnnualIncome = (List<AnnualIncomeView>) viewMap
//				.get("annualIncomeList");
//		if (dataListAnnualIncome == null)
//			dataListAnnualIncome = new ArrayList<AnnualIncomeView>();
//		return dataListAnnualIncome;
//	}
//
//	public void setDataListAnnualIncome(
//			List<AnnualIncomeView> dataListAnnualIncome) {
//		this.dataListAnnualIncome = dataListAnnualIncome;
//		if (dataListAnnualIncome != null) {
//			viewMap.put("annualIncomeList", dataListAnnualIncome);
//		}
//	}
//
//	public HtmlDataTable getDataTableMonthlyExpenses() {
//		return dataTableMonthlyExpenses;
//	}
//
//	public void setDataTableMonthlyExpenses(
//			HtmlDataTable dataTableMonthlyExpenses) {
//		this.dataTableMonthlyExpenses = dataTableMonthlyExpenses;
//	}
//
//	public List<MonthlyExpenseView> getDataListMonthlyExpenses() 
//	{
//		dataListMonthlyExpenses = (List<MonthlyExpenseView>) viewMap.get("dataListMonthlyExpenses");
//		if (dataListMonthlyExpenses == null)
//			dataListMonthlyExpenses = new ArrayList<MonthlyExpenseView>();
//		return dataListMonthlyExpenses;
//	}
//
//	public void setDataListMonthlyExpenses(
//			List<MonthlyExpenseView> dataListMonthlyExpenses) {
//		this.dataListMonthlyExpenses = dataListMonthlyExpenses;
//		if (dataListMonthlyExpenses != null) {
//			viewRootMap.put("dataListMonthlyExpenses", dataListMonthlyExpenses);
//		}
//
//	}
//
//	public javax.faces.component.html.HtmlSelectBooleanCheckbox getHtmlApplyToAllBeneficiaries() {
//		return htmlApplyToAllBeneficiaries;
//	}
//
//	public void setHtmlApplyToAllBeneficiaries(
//			javax.faces.component.html.HtmlSelectBooleanCheckbox htmlApplyToAllBeneficiaries) {
//		this.htmlApplyToAllBeneficiaries = htmlApplyToAllBeneficiaries;
//	}
//
//	public HtmlInputText getTxtFamilyNumber() {
//		return txtFamilyNumber;
//	}
//
//	public void setTxtFamilyNumber(HtmlInputText txtFamilyNumber) {
//		this.txtFamilyNumber = txtFamilyNumber;
//	}
//
//	public HtmlInputText getAccTypeDesc() {
//		return accTypeDesc;
//	}
//
//	public void setAccTypeDesc(HtmlInputText accTypeDesc) {
//		this.accTypeDesc = accTypeDesc;
//	}
//
//	public HtmlInputText getResTypeDesc() {
//		return resTypeDesc;
//	}
//
//	public void setResTypeDesc(HtmlInputText resTypeDesc) {
//		this.resTypeDesc = resTypeDesc;
//	}
//
//	public HtmlInputText getAccOwnershipDesc() {
//		return accOwnershipDesc;
//	}
//
//	public void setAccOwnershipDesc(HtmlInputText accOwnershipDesc) {
//		this.accOwnershipDesc = accOwnershipDesc;
//	}
//
//	public HtmlInputText getHomeConditionDesc() {
//		return homeConditionDesc;
//	}
//
//	public void setHomeConditionDesc(HtmlInputText homeConditionDesc) {
//		this.homeConditionDesc = homeConditionDesc;
//	}
//
//	public HtmlInputText getRentingReasonDesc() {
//		return rentingReasonDesc;
//	}
//
//	public void setRentingReasonDesc(HtmlInputText rentingReasonDesc) {
//		this.rentingReasonDesc = rentingReasonDesc;
//	}
//
//	public HtmlInputText getHomeTypeDesc() {
//		return homeTypeDesc;
//	}
//
//	public void setHomeTypeDesc(HtmlInputText homeTypeDesc) {
//		this.homeTypeDesc = homeTypeDesc;
//	}
//
//	public List<ResearchFormBeneficiaryView> getDataListResearchGridView() 
//	{
//		dataListResearchGridView=(ArrayList<ResearchFormBeneficiaryView>) viewRootMap.get("dataListResearchGridView");
//		if(dataListResearchGridView==null)
//			dataListResearchGridView = new ArrayList<ResearchFormBeneficiaryView>();
//		return dataListResearchGridView;
//	}
//
//	@SuppressWarnings("unchecked")
//	public void setDataListResearchGridView(ResearchFormBeneficiaryView dataListResearchGridRow) 
//	{
//		this.dataListResearchGridView = getDataListResearchGridView();
//		this.dataListResearchGridView.add(dataListResearchGridRow);
//		
//			
//		if ( dataListResearchGridView !=null)
//		  viewRootMap.put("dataListResearchGridView",dataListResearchGridView );
//	}
//	public void setDataListResearchGridViewInitial(List<ResearchFormBeneficiaryView> dataListResearchGridView) 
//	{
//		
//		
//		this.dataListResearchGridView = dataListResearchGridView;	
//			
//		if ( dataListResearchGridView !=null)
//		  viewRootMap.put("dataListResearchGridView",dataListResearchGridView );
//	}
//
//	public HtmlDataTable getDataTableResearchGrid() {
//		return dataTableResearchGrid;
//	}
//
//	public void setDataTableResearchGrid(HtmlDataTable dataTableResearchGrid) {
//		this.dataTableResearchGrid = dataTableResearchGrid;
//	}
//
//	public HashMap<Long, String> getAccomodationTypeMap() {
//		accomodationTypeMap = (HashMap<Long, String>) viewRootMap.get("accomodationTypeMap");
//		if(accomodationTypeMap==null)
//			accomodationTypeMap= new HashMap<Long, String>();
//		return accomodationTypeMap;
//	}
//
//	public void setAccomodationTypeMap(HashMap<Long, String> accomodationTypeMap) {
//		this.accomodationTypeMap = accomodationTypeMap;
//		if(accomodationTypeMap!=null)
//			viewRootMap.put("accomodationTypeMap", accomodationTypeMap);
//	}
//
//	public HashMap<Long, String> getAccomodationOwnershipMap() {
//		accomodationOwnershipMap = (HashMap<Long, String>) viewRootMap.get("accomodationOwnershipMap");
//		if(accomodationOwnershipMap==null)
//			accomodationOwnershipMap = new HashMap<Long, String>();
//		return accomodationOwnershipMap;
//	}
//
//	public void setAccomodationOwnershipMap(
//			HashMap<Long, String> accomodationOwnershipMap) {
//		this.accomodationOwnershipMap = accomodationOwnershipMap;
//		if(accomodationOwnershipMap!=null)
//			viewRootMap.put("accomodationOwnershipMap", accomodationOwnershipMap);
//	}
//
//	public HashMap<Long, String> getResidenceTypeMap() {
//		residenceTypeMap = (HashMap<Long, String>) viewRootMap.get("residenceTypeMap");
//		if(residenceTypeMap==null)
//			residenceTypeMap= new HashMap<Long, String>();
//		
//		return residenceTypeMap;
//	}
//
//	public void setResidenceTypeMap(HashMap<Long, String> residenceTypeMap) {
//		
//		this.residenceTypeMap = residenceTypeMap;
//		if(residenceTypeMap!=null)
//			viewRootMap.put("residenceTypeMap", residenceTypeMap);
//	}
//
//	public HashMap<Long, String> getHomeConditionMap() {
//		homeConditionMap=(HashMap<Long, String>) viewRootMap.get("homeConditionMap");
//		if(homeConditionMap==null)
//			homeConditionMap= new HashMap<Long, String>();
//		return homeConditionMap;
//	}
//
//	public void setHomeConditionMap(HashMap<Long, String> homeConditionMap) {
//		this.homeConditionMap = homeConditionMap;
//		if(homeConditionMap!=null)
//			viewRootMap.put("homeConditionMap",homeConditionMap);
//	}
//
//	public HashMap<Long, String> getRentingReasonMap() {
//		rentingReasonMap = (HashMap<Long, String>) viewRootMap.get("rentingReasonMap");
//		if (rentingReasonMap== null)
//			rentingReasonMap=  new HashMap<Long, String>();
//		return rentingReasonMap;
//	}
//
//	public void setRentingReasonMap(HashMap<Long, String> rentingReasonMap) {
//		this.rentingReasonMap = rentingReasonMap;
//		if(rentingReasonMap!=null)
//			viewRootMap.put("rentingReasonMap", rentingReasonMap);
//	}
//
//	public HashMap<Long, String> getTypeOfHomeMap() {
//		typeOfHomeMap = (HashMap<Long, String>) viewRootMap.get("typeOfHomeMap");
//		if (typeOfHomeMap== null)
//			typeOfHomeMap=  new HashMap<Long, String>();
//		return typeOfHomeMap;
//	}
//
//	public void setTypeOfHomeMap(HashMap<Long, String> typeOfHomeMap) {
//		this.typeOfHomeMap = typeOfHomeMap;
//		if(typeOfHomeMap!=null)
//			viewRootMap.put("typeOfHomeMap", typeOfHomeMap);
//	}
//
//	public HashMap<Long, String> getBeneficiaryOfPropertyMap() {
//		
//		beneficiaryOfPropertyMap= (HashMap<Long, String>) viewRootMap.get("beneficiaryOfPropertyMap");
//		if (beneficiaryOfPropertyMap== null)
//			beneficiaryOfPropertyMap=  new HashMap<Long, String>();
//		return beneficiaryOfPropertyMap;
//	}
//
//	public void setBeneficiaryOfPropertyMap(
//			HashMap<Long, String> beneficiaryOfPropertyMap) {
//		this.beneficiaryOfPropertyMap = beneficiaryOfPropertyMap;
//		if(beneficiaryOfPropertyMap!=null)
//			viewRootMap.put("beneficiaryOfPropertyMap", beneficiaryOfPropertyMap);
//	}
//}
