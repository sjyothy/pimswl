package com.avanza.pims.web.mems.endowment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.data.ApplicationContext;
import com.avanza.pims.entity.BoxDistributionHistory;
import com.avanza.pims.entity.DonationBox;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.DonationBoxService;
import com.avanza.pims.ws.vo.DonationBoxView;
import com.avanza.ui.util.ResourceUtil;

public class SearchDonationBoxBean extends AbstractSearchBean {
	private static final long serialVersionUID = 1L;
	private String DEFAULT_SORT_FIELD = "boxId";
	private String DATA_LIST = "DATA_LIST";
	DonationBoxService service = new DonationBoxService();

	DonationBoxView criteria = new DonationBoxView();
 	 List<DonationBoxView> list = new ArrayList<DonationBoxView>();
	
	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		super.init();
		try {
			if (!isPostBack()) {
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
				setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
				setSortField(DEFAULT_SORT_FIELD);
				checkDataInQueryString();
			}
		} catch (Exception es) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init |Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void checkDataInQueryString() {
		checkPageMode();
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() {

		if (request.getParameter(VIEW_MODE) != null) {

			if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) {
				viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);

			} else if (request.getParameter(VIEW_MODE).equals(
					MODE_SELECT_MANY_POPUP)) {
				viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);

			}
		}
	}
	@SuppressWarnings("unchecked")
	public String onAdd() 
	{
		return "Add";
	}
	
	@SuppressWarnings("unchecked")
	public String onOpenBoxInGroup() 
    {
		try 
		{
			List<DonationBoxView> listDonationBoxView = new ArrayList<DonationBoxView>();
			for (DonationBoxView box: list) 
			{
				if( box.isSelected())
				{
					
					listDonationBoxView.add(box);
				}
				
			}
			
			sessionMap.put(WebConstants.OpenBox.DonationBoxView, listDonationBoxView);
			
			
		} catch (Exception e) 
		{
			logger.LogException("onOpenBoxInGroup|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "OpenInGroup";
	}
	@SuppressWarnings("unchecked")
	public String onOpenBox() 
    {
		try 
		{
			DonationBoxView row =  (DonationBoxView) dataTable.getRowData();
			sessionMap.put(WebConstants.OpenBox.DonationBoxView, row);
			
			
		} catch (Exception e) 
		{
			logger.LogException("onOpenBox |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "Open";
	}
    @SuppressWarnings("unchecked")
	public String onEdit() 
    {
		try 
		{
			DonationBoxView row =  (DonationBoxView) dataTable.getRowData();
			sessionMap.put(WebConstants.SELECTED_ROW, row);
			
		} catch (Exception e) 
		{
			logger.LogException("onEdit |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "Edit";
	}
    @SuppressWarnings("unchecked")
	public void onSearch() {
		try {
			pageFirst();//at last
		} catch (Exception e) {
			logger.LogException("onSearch |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
    private boolean hasDistributionErrors(List<Long> list ) throws Exception
	{
		boolean hasDistributionErrors = false;
		errorMessages = new ArrayList<String>();
		if( list == null || list.size() <= 0 )
		{ 
			errorMessages.add(ResourceUtil.getInstance().getProperty("donationBox.msg.distributionSelectionRequired")  );
			hasDistributionErrors = true;
		
		}
		return hasDistributionErrors;
		
	}
    @SuppressWarnings("unchecked")
    public void onOpenDistribute() 
	{
		try 
		{
			List<DonationBoxView> list = getList();

			List<Long> selectedList = new ArrayList<Long>();
			for (DonationBoxView box : list) 
			{
				if( !box.isSelected()  ){continue;}
				selectedList.add(box.getBoxId());
			}
			if( hasDistributionErrors(selectedList) )return;
			
            sessionMap.put(WebConstants.SELECTED_ROWS, selectedList);
            executeJavascript("javaScript:openBoxDistributePopup();");
		} 
		catch (Exception e) 
		{
			logger.LogException("onOpenDistribute|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
    @SuppressWarnings("unchecked")
	public void onDistribute() 
	{
    	java.text.DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try 
		{
			List<Long> ids = (ArrayList)sessionMap.remove(WebConstants.SELECTED_ROWS);
			BoxDistributionHistory distributionInfo = (BoxDistributionHistory )sessionMap.remove(WebConstants.DonationBox.BoxDistribution);
            String distributedOnStr= format.format(distributionInfo.getDistributedOn() );

            try
            {
	            ApplicationContext.getContext().getTxnContext().beginTransaction();
				for (long id : ids) 
				{
					DonationBox  box = EntityManager.getBroker().findById(DonationBox.class, id);
					BoxDistributionHistory bdh = ( BoxDistributionHistory )BeanUtils.cloneBean( distributionInfo );
					bdh.setDonationBox(box );
					
					service.distributeBox( bdh );
					CommonUtil.saveSystemComments( WebConstants.DonationBox.NOTES_OWNER, 
							                       "donationBox.event.distribution",
							                       id,
							                       distributionInfo.getDistributedBy(),
							                       distributedOnStr
							                      );
			
				}
				ApplicationContext.getContext().getTxnContext().commit();
				onSearch();
				successMessages.add( ResourceUtil.getInstance().getProperty("donationBox.msg.distributedSuccessfully") );
            }
    		catch(Exception e)
    		{
    			ApplicationContext.getContext().getTxnContext().rollback();
    			throw e;
    		}
    		finally
    		{
    			ApplicationContext.getContext().getTxnContext().release();
    		}
		} 
		catch (Exception e) 
		{
			logger.LogException("onDistribute|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

    @SuppressWarnings("unchecked")
	public void onCancelled() 
	{
		try 
		{

            try
            {
            	ApplicationContext.getContext().getTxnContext().beginTransaction();
            	DonationBoxView donationBox = ( DonationBoxView )dataTable.getRowData();
            	donationBox.setStatus(String.valueOf( WebConstants.DonationBoxStatus.Cancelled_Id ) );
            	donationBox.setUpdatedBy(getLoggedInUserId());
            	donationBox.setUpdatedOn(new Date());
            	service.persist(	donationBox	);
            	CommonUtil.saveSystemComments( WebConstants.DonationBox.NOTES_OWNER, 
						                       "donationBox.event.cancelled",
						                       donationBox.getBoxId()
						                      );


				ApplicationContext.getContext().getTxnContext().commit();
				loadDataList();
				successMessages.add( ResourceUtil.getInstance().getProperty("donationBox.msg.cancelledSuccessfully") );
            }
    		catch(Exception e)
    		{
    			ApplicationContext.getContext().getTxnContext().rollback();
    			throw e;
    		}
    		finally
    		{
    			ApplicationContext.getContext().getTxnContext().release();
    		}
		} 
		catch (Exception e) 
		{
			logger.LogException("onCancelled|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	//We have to look for the list population for facilities

	public void doSearch() {
		try {
			loadDataList();
		} catch (Exception e) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("doSearch|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void doSearchItemList() {
		try {
			loadDataList();
		} catch (Exception e) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("doSearchItemList|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public List<DonationBoxView> loadDataList() throws Exception {
		
		/////////////////////////////////////////////// For server side paging paging/////////////////////////////////////////
//		int totalRows = 0;
//		totalRows = service.getDonationBoxRecordSize(criteria);
//		setTotalRows(totalRows);
//		doPagingComputations();
		//////////////////////////////////////////////////////////////////////////////////////////////
		list =  service.getAllDonationBox(criteria);
		

		if (list.isEmpty() || list == null) {
			errorMessages = new ArrayList<String>();
			errorMessages
					.add(CommonUtil
							.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			forPaging(0);// searchedRows
			if(viewMap.get(DATA_LIST) != null){
				viewMap.remove(DATA_LIST);
			}
		}
		else{
			this.setList(list);
			forPaging(list.size());// searchedRows
		}
		return list;
	}

	
	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	/**
	 * @return the recordSize
	 */
	@SuppressWarnings("unchecked")
	public Integer getRecordSize() {
		if (viewMap.get("recordSize") != null) {
			recordSize = (Integer) viewMap.get("recordSize");
		}
		if (recordSize == null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	@SuppressWarnings("unchecked")
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
		if (this.recordSize != null)
			viewMap.put("recordSize", this.recordSize);
	}

	public DonationBoxView getCriteria() {
		return criteria;
	}

	public void setCriteria(DonationBoxView criteria) {
		this.criteria = criteria;
	}

	@SuppressWarnings("unchecked")
	public List<DonationBoxView> getList() {
		if(viewMap.get(DATA_LIST) != null){
			list = (List<DonationBoxView>) viewMap.get(DATA_LIST);
		} 
		return list;
	}

	@SuppressWarnings("unchecked")
	public void setList(List<DonationBoxView> list) {
		if(list != null && list.size() > 0){
			viewMap.put(DATA_LIST, list);
		}
		this.list = list;
	}
	public boolean isSelectOnePopup(){
		if(viewMap.get(VIEW_MODE) != null && viewMap.get(VIEW_MODE).toString().compareTo(MODE_SELECT_ONE_POPUP) == 0)
			return true;
		else
			return false;
	}
	public boolean isSelectManyPopup(){
		if(viewMap.get(VIEW_MODE) != null && viewMap.get(VIEW_MODE).toString().compareTo(MODE_SELECT_MANY_POPUP) == 0)
			return true;
		else
			return false;
	}
	
	public boolean isPopup(){
		return (isPageModeSelectManyPopUp() || isPageModeSelectOnePopUp());
	}
	@SuppressWarnings("unchecked")
	public void onSingleSelect()
	{
		DonationBoxView selectedDonationBox = null;
		selectedDonationBox = (DonationBoxView) dataTable.getRowData();
		sessionMap.put(WebConstants.SELECTED_ROW,selectedDonationBox);
		executeJavascript("closeWindow();");
	}
	@SuppressWarnings("unchecked")
	public void onMultiSelect()
	{
		try 
		{
			list = getList();
			List<DonationBoxView> selectedDonationBoxes = new ArrayList<DonationBoxView>();
			if(list != null && list.size() > 0)
			{
				for(DonationBoxView box: list)
				{
					if(box.isSelected())
					{
						selectedDonationBoxes.add(box);
					}
				}
			}
			else
			{
					errorMessages = new ArrayList<String>();
					errorMessages.add(CommonUtil.getBundleMessage("donationBox.label.errMsg.noSelectMade"));
			}
			if(selectedDonationBoxes.size() > 0)
			{
				sessionMap.put(WebConstants.SELECTED_ROWS, selectedDonationBoxes);
				executeJavascript("closeWindow();");
			}
			else
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage("donationBox.label.errMsg.noSelectMade"));
			}
			
		} 
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onMultiSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}
	@SuppressWarnings("unchecked")
	private void executeJavascript(String javascript) 
	{
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
		} 
		catch (Exception exception) 
		{			
			logger.LogException("executeJavascript|Error Occured", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}
	public boolean isEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
}
