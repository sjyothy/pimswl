package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.minors.SocialProgramsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.SocialProgramService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.SessionDetailView;
import com.avanza.pims.ws.vo.mems.ProgramActionListView;
import com.avanza.pims.ws.vo.mems.ProgramBudgetView;
import com.avanza.pims.ws.vo.mems.ProgramPurposesView;
import com.avanza.pims.ws.vo.mems.ProgramRequirementsView;
import com.avanza.ui.util.ResourceUtil;

public class ProgramBudgetDetailsBean extends AbstractMemsBean
{
	
	private List<ProgramBudgetView> list;
	private HtmlDataTable dataTable;
	private ProgramBudgetView programBudgetView;
	private String listLoadedFromDb;
	private SocialProgramsBean parentBean;	
	public ProgramBudgetDetailsBean() 
	{	
		programBudgetView = new ProgramBudgetView() ;
		list = new ArrayList<ProgramBudgetView>(0);
		
		dataTable = new HtmlDataTable();
	}
	
	@Override
	public void init() 
	{
		String METHOD_NAME = "init()";
        errorMessages = new ArrayList<String>(0);		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			parentBean.setErrorMessages( errorMessages );
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws PimsBusinessException
	{
		
		
		
		// PURPOSE VIEW
		if ( viewMap.get( WebConstants.SocialProgramBudget.BUDGET_VO ) != null )
		{
			programBudgetView = (ProgramBudgetView) viewMap.get( WebConstants.SocialProgramBudget.BUDGET_VO );
		}		
		
		// PURPOSE VIEW LIST
		if ( viewMap.get( WebConstants.SocialProgramBudget.BUDGET_DATA_LIST ) != null )
		{
			list = ( List<ProgramBudgetView>) viewMap.get( WebConstants.SocialProgramBudget.BUDGET_DATA_LIST );
		}		
		parentBean = (SocialProgramsBean)getBean( "pages$SocialProgram" );
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		
		// PURPOSE VIEW
		if ( programBudgetView != null  )
		{
			viewMap.put( WebConstants.SocialProgramBudget.BUDGET_VO , programBudgetView);
		}
		
		// LIST
		if ( list != null )
		{
			viewMap.put( WebConstants.SocialProgramBudget.BUDGET_DATA_LIST, list );
		}
	}	
	
	@SuppressWarnings("unchecked")
	public void onLoadList( Long programId )throws Exception
	{
		final String METHOD_NAME = "onLoadPurposes()";
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( this.getListLoadedFromDb()== null || this.getListLoadedFromDb().equals("0") )
			{
				
				list =  new SocialProgramService().loadProgramBudgetList( programId  );
				this.setListLoadedFromDb( "1" );
				updateValuesToMaps();
			}
	}

	@SuppressWarnings( "unchecked" )
	public String getListLoadedFromDb() {
		if( viewMap.get( "budgetListLoadedFrom" ) != null )
		{
			listLoadedFromDb = viewMap.get( "budgetListLoadedFrom" ).toString(); 
		}
		return listLoadedFromDb;
	}
    @SuppressWarnings( "unchecked" )
	public void setListLoadedFromDb(String listLoadedFromDb) {
		this.listLoadedFromDb = listLoadedFromDb;
		if( this.listLoadedFromDb != null )
		{
			
			viewMap.put( "budgetListLoadedFrom",this.listLoadedFromDb ); 
		}
	}
	

	@SuppressWarnings("unchecked")
	public String onSave()
	{
		final String METHOD_NAME = "onSave()";
		errorMessages = new ArrayList<String>(0);
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( isValid() )
			{
				ProgramBudgetView newVo = new ProgramBudgetView();
				newVo.setCreatedBy(getLoggedInUserId() );
				newVo.setCreatedEn(getLoggedInUserObj().getFullName());
				newVo.setCreatedAr( getLoggedInUserObj().getSecondaryFullName() );
				newVo.setCreatedOn( new Date() );
				newVo.setIsDeleted( "0" );
				newVo.setDescription(  programBudgetView.getDescription() );
				newVo.setAmount(programBudgetView.getAmount());
				list.add(0, newVo);
				//saving it in viewMap to retrieve it from the parent bean later , for maturity date
				programBudgetView.setDescription(  "" ); 
				programBudgetView.setAmount( "" );
				
				updateValuesToMaps();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			parentBean.setErrorMessages( errorMessages );
		}
		
		return null;
	}
	
	public String onSessionDetailDelete()
	{
		updateValuesToMaps();
		return null;
	}
	
	private boolean isValid() throws Exception
	{
		boolean valid = true;
		
		// VALIDATION CHECKS STARTS HERE
		if( programBudgetView.getAmount() == null || programBudgetView.getAmount().trim().length()<=0 )
		{
			errorMessages.add( ResourceUtil.getInstance().getProperty("socialProgram.messages.budgetAmountRequired" ) );
			parentBean.setErrorMessages( errorMessages );
			valid = false;
		}
		else
		{
			try{
				Double.valueOf( programBudgetView.getAmount() ); 
			}
			catch(Exception e)
			{
				errorMessages.add( ResourceUtil.getInstance().getProperty("socialProgram.messages.budgetAmountInvalid" ) );
				parentBean.setErrorMessages( errorMessages );
				valid = false;
				
			}
		}
		if( programBudgetView.getDescription() == null || programBudgetView.getDescription().trim().length() <=0 )
		{
			errorMessages.add( ResourceUtil.getInstance().getProperty("socialProgram.messages.descriptionRequired" ) );
			parentBean.setErrorMessages( errorMessages );
			valid = false;
		}
		
		// VALIDATION CHECKS ENDS HERE
		
		return valid;
	}
	@SuppressWarnings("unchecked")
	public void onDelete( )
	{
	    final String METHOD_NAME = "onDelete()";
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			updateValuesFromMaps();
			ProgramBudgetView item = ( ProgramBudgetView )dataTable.getRowData(); 
			
			if(  item.getProgramBudgetId() != null)
			{
			item.setIsDeleted( "1" );
			}
			else
			{
			 list.remove(item);
			}
			updateValuesToMaps();
			logger.logInfo( METHOD_NAME + " --- Finish --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		parentBean.setErrorMessages( errorMessages );
		}
		
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<ProgramBudgetView> getList() {
		return list;
	}

	public void setList(List<ProgramBudgetView> list) {
		this.list = list;
	}

	public ProgramBudgetView getProgramBudgetView() {
		return programBudgetView;
	}

	public void setProgramBudgetView(ProgramBudgetView programBudgetView) {
		this.programBudgetView = programBudgetView;
	}






	


}
