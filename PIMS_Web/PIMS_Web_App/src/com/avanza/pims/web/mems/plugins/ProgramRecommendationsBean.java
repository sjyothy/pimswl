package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.minors.SocialProgramsBean;
import com.avanza.pims.ws.mems.SocialProgramService;
import com.avanza.pims.ws.vo.mems.ProgramRecommendationsView;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings( "unchecked" )
public class ProgramRecommendationsBean extends AbstractMemsBean
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1430958625510142177L;
	
	private List<ProgramRecommendationsView> list;
	private HtmlDataTable dataTable;
	private ProgramRecommendationsView programRecommendationsView;
	private String listLoadedFromDb;
	private SocialProgramsBean parentBean;	
	public ProgramRecommendationsBean() 
	{	
		programRecommendationsView = new ProgramRecommendationsView() ;
		list = new ArrayList<ProgramRecommendationsView>(0);
		dataTable = new HtmlDataTable();
		
	}
	
	@Override
	public void init() 
	{
		String METHOD_NAME = "init()";
		errorMessages = new ArrayList<String>(0);
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			parentBean.setErrorMessages( errorMessages );
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws PimsBusinessException
	{
		
		// SPONSOR VIEW
		if ( viewMap.get( WebConstants.SocialProgramRecommendations.VO ) != null )
		{
			programRecommendationsView = (ProgramRecommendationsView) viewMap.get( WebConstants.SocialProgramRecommendations.VO );
		}		
		
		// SPONSOR VIEW LIST
		if ( viewMap.get( WebConstants.SocialProgramRecommendations.DATA_LIST ) != null )
		{
			list = ( List<ProgramRecommendationsView>) viewMap.get( WebConstants.SocialProgramRecommendations.DATA_LIST );
		}		
		parentBean = (SocialProgramsBean)getBean( "pages$SocialProgram" );
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		
		// PURPOSE VIEW
		if ( programRecommendationsView != null  )
		{
			viewMap.put( WebConstants.SocialProgramRecommendations.VO , programRecommendationsView);
		}
		
		// LIST
		if ( list != null )
		{
			viewMap.put( WebConstants.SocialProgramRecommendations.DATA_LIST, list );
		}
	}	
	
	@SuppressWarnings("unchecked")
	public void onLoadList( Long programId )throws Exception
	{
		final String METHOD_NAME = "onLoadList()";
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( this.getListLoadedFromDb()== null || this.getListLoadedFromDb().equals("0") )
			{
				
				list =  new SocialProgramService().loadProgramRecommendations( programId  );
				this.setListLoadedFromDb( "1" );
				updateValuesToMaps();
			}
	}

	
	public String getListLoadedFromDb() {
		if( viewMap.get( "recommendationsListLoadedFromDB" ) != null )
		{
			listLoadedFromDb = viewMap.get( "recommendationsListLoadedFromDB" ).toString(); 
		}
		return listLoadedFromDb;
	}
    @SuppressWarnings( "unchecked" )
	public void setListLoadedFromDb(String listLoadedFromDb) {
		this.listLoadedFromDb = listLoadedFromDb;
		if( this.listLoadedFromDb != null )
		{
			
			viewMap.put( "recommendationsListLoadedFromDB",this.listLoadedFromDb ); 
		}
	}
    @SuppressWarnings("unchecked")
	public void onDelete( )
	{
	    final String METHOD_NAME = "onDelete()";
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			updateValuesFromMaps();
			ProgramRecommendationsView item = ( ProgramRecommendationsView )dataTable.getRowData(); 
			
			if(  item.getProgramRecommendationId() != null)
			{
			item.setIsDeleted( "1" );
			}
			else
			{
			 list.remove(item);
			}
			updateValuesToMaps();
			successMessages.add( ResourceUtil.getInstance().getProperty("socialProgram.messages.sponsorDeleted") );
			logger.logInfo( METHOD_NAME + " --- Finish --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		parentBean.setErrorMessages( errorMessages );
		}
		
	}
	@SuppressWarnings("unchecked")
	public void onSave()
	{
		final String METHOD_NAME = "onSave()";
		errorMessages = new ArrayList<String>();
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( isValid() )
			{
				ProgramRecommendationsView newVo = new ProgramRecommendationsView();
				setDefaultValues(newVo);
				newVo.setRecommendations( programRecommendationsView.getRecommendations() );
				list.add(0, newVo);
				programRecommendationsView = new ProgramRecommendationsView();
				updateValuesToMaps();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			parentBean.setErrorMessages( errorMessages );
		}
	}

	private void setDefaultValues(ProgramRecommendationsView newVo) {
		newVo.setCreatedBy(getLoggedInUserId() );
		newVo.setCreatedEn(getLoggedInUserObj().getFullName());
		newVo.setCreatedAr( getLoggedInUserObj().getSecondaryFullName() );
		newVo.setCreatedOn( new Date() );
		newVo.setIsDeleted( "0" );
	}
	
	public String onSessionDetailDelete()
	{
		updateValuesToMaps();
		return null;
	}
	
	private boolean isValid() throws Exception
	{
		boolean valid = true;
		
		// VALIDATION CHECKS STARTS HERE
		if( programRecommendationsView.getRecommendations() == null || programRecommendationsView.getRecommendations().trim().length() <=0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.recommendationReq"));
			parentBean.setErrorMessages(errorMessages);
			valid = false;
		}
		// VALIDATION CHECKS ENDS HERE
		
		return valid;
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<ProgramRecommendationsView> getList() {
		return list;
	}

	public void setList(List<ProgramRecommendationsView> list) {
		this.list = list;
	}

	public ProgramRecommendationsView getProgramRecommendationsView() {
		return programRecommendationsView;
	}

	public void setProgramRecommendationsView(ProgramRecommendationsView ProgramRecommendationsView) {
		this.programRecommendationsView = ProgramRecommendationsView;
	}
}
