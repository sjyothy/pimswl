package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.ZakatService;
import com.avanza.pims.ws.vo.mems.ZakatPersonalAccountTransLinkView;
import com.avanza.ui.util.ResourceUtil;

public class ZakatPersonalAccountTransLinkPopup extends AbstractMemsBean
{	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	HttpServletRequest request;
	List<ZakatPersonalAccountTransLinkView> list = new ArrayList<ZakatPersonalAccountTransLinkView>();
	private HtmlDataTable dataTable;
	public ZakatPersonalAccountTransLinkPopup() 
	{
		request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		Long id	  = null;
		
		if( request.getParameter("zakatId") != null)
		{
			id =  new Long(request.getParameter("zakatId").toString());
		}
		list = ZakatService.getPersonalTransFromZakatId( id );
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( list != null )
		{
			viewMap.put( "list", list );
		}
	}

	public List<ZakatPersonalAccountTransLinkView> getList() {
		return list;
	}

	public void setList(List<ZakatPersonalAccountTransLinkView> list) {
		this.list = list;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}	
    

}
