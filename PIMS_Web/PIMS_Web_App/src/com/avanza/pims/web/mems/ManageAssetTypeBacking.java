package com.avanza.pims.web.mems;

import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.entity.AssetType;
import com.avanza.pims.entity.AssetTypeCncrndDept;
import com.avanza.pims.entity.Bank;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.ui.util.ResourceUtil;

public class ManageAssetTypeBacking extends AbstractMemsBean {
	private static final long serialVersionUID = 1L;
	InheritanceFileService service = new InheritanceFileService();
	private HtmlSelectOneMenu cboAssetType = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cboBank = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cboGovtDeptt = new HtmlSelectOneMenu();
	private HtmlInputText txtTypeNameEn = new HtmlInputText();
	private HtmlInputText txtTypeNameAr = new HtmlInputText();
	private HtmlInputText txtContactPerson = new HtmlInputText();
	private HtmlInputText txtOfficePhone = new HtmlInputText();
	private HtmlInputText txtCellPhone = new HtmlInputText();
	private HtmlInputText txtEmail = new HtmlInputText();
	private HtmlInputText txtFax = new HtmlInputText();
	private HtmlInputText txtDescription = new HtmlInputText();
	private HtmlSelectBooleanCheckbox chkMandatory = new HtmlSelectBooleanCheckbox();
	private HtmlSelectBooleanCheckbox chkEligible = new HtmlSelectBooleanCheckbox();

	public ManageAssetTypeBacking() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		super.init();
		try {
			if (!isPostBack()) {
			}
		} catch (Exception es) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init" + "|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void onUpdate() {
		if (isValidated()) {
			try {
				persistAssetTypeAndDeptt();
				successMessages.add(CommonUtil
						.getBundleMessage("successMsg.costCenterUpdated"));
				// resetAllFields();
			} catch (Exception e) {
				errorMessages.add(ResourceUtil.getInstance().getProperty(
						MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
				logger.LogException("onUpdate has been crahsed|", e);
			}
		}
	}

	private void persistAssetTypeAndDeptt() throws Exception {
		if (viewMap.get("SELECTED_ASSET_TYPE") != null) {
			AssetType assetTypeToPersist = (AssetType) viewMap
					.get("SELECTED_ASSET_TYPE");
			fillAssetTypeToPesist(assetTypeToPersist);
			service.persistAssetTypeAndCncrndDeptt(assetTypeToPersist);
		}
	}

	private void fillAssetTypeToPesist(AssetType assetTypeToPersist) {

		if (txtTypeNameEn.getValue() != null
				&& txtTypeNameEn.getValue().toString().trim().length() > 0) {
			assetTypeToPersist.setAssetTypeNameEn(txtTypeNameEn.getValue()
					.toString());
		}

		if (txtTypeNameAr.getValue() != null
				&& txtTypeNameAr.getValue().toString().trim().length() > 0) {
			assetTypeToPersist.setAssetTypeNameAr(txtTypeNameAr.getValue()
					.toString());
		}

		assetTypeToPersist.setIsMandatory(Boolean.parseBoolean(chkMandatory
				.getValue().toString()) ? 1L : 0L);

		assetTypeToPersist.setIsTakharuable(Boolean.parseBoolean(chkEligible
				.getValue().toString()) ? 1L : 0L);
		Date today = new Date();

		AssetTypeCncrndDept deptt = new AssetTypeCncrndDept();
		Bank bank = new Bank();
		deptt.setAssetTypeCncrndDeptId(null);

		if (txtContactPerson.getValue() != null
				&& txtContactPerson.getValue().toString().trim().length() > 0) {
			deptt.setContactPersonName(txtContactPerson.getValue().toString()
					.trim());
		}

		if (txtCellPhone.getValue() != null
				&& txtCellPhone.getValue().toString().trim().length() > 0) {
			deptt.setPhone1(txtCellPhone.getValue().toString().trim());
		}

		if (txtOfficePhone.getValue() != null
				&& txtOfficePhone.getValue().toString().trim().length() > 0) {
			deptt.setPhone2(txtOfficePhone.getValue().toString().trim());
		}

		if (txtEmail.getValue() != null
				&& txtEmail.getValue().toString().trim().length() > 0) {
			deptt.setEmail(txtEmail.getValue().toString().trim());
		}

		if (txtFax.getValue() != null
				&& txtFax.getValue().toString().trim().length() > 0) {
			deptt.setFax(txtFax.getValue().toString().trim());
		}

		if (!cboGovtDeptt.getValue().toString().equals("-1"))
			deptt.setGovtDepttId(Long.parseLong(cboGovtDeptt.getValue()
					.toString()));

		if (!cboBank.getValue().toString().equals("-1")) {
			bank.setBankId(Long.parseLong(cboBank.getValue().toString()));
			deptt.setBank(bank);
		}
		deptt.setCreatedBy(CommonUtil.getLoggedInUser());
		deptt.setCreatedOn(today);
		deptt.setUpdatedBy(CommonUtil.getLoggedInUser());
		deptt.setUpdatedOn(today);
		deptt.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		deptt.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);

		assetTypeToPersist.setUpdatedBy(CommonUtil.getLoggedInUser());
		assetTypeToPersist.setUpdatedOn(today);

		assetTypeToPersist.setAssetTypeCncrndDeptt(deptt);
	}

	private boolean isValidated() {
		boolean validate = true;
		if (cboAssetType.getValue().toString().compareTo("-1") == 0) {
			errorMessages
					.add(CommonUtil
							.getBundleMessage("manageAssetType.errMsg.selectAssetType"));
			return false;
		}

		if (txtTypeNameEn.getValue() == null
				|| txtTypeNameEn.getValue().toString().trim().length() < 0) {
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage(""));
		}

		if (txtContactPerson.getValue() == null
				|| StringHelper.isEmpty(txtContactPerson.getValue().toString())) {
			errorMessages.add(CommonUtil
					.getBundleMessage("mems.inheritanceFile.contactPersonReq"));
			validate = false;
		}

		if (txtCellPhone.getValue() == null
				|| StringHelper.isEmpty(txtCellPhone.getValue().toString())) {
			errorMessages.add(CommonUtil
					.getBundleMessage("mems.inheritanceFile.celphoneReq"));
			validate = false;
		}
		if (txtOfficePhone.getValue() == null
				|| StringHelper.isEmpty(txtOfficePhone.getValue().toString())) {
			errorMessages.add(CommonUtil
					.getBundleMessage("mems.inheritanceFile.officephoneReq"));
			validate = false;
		}
		if (txtEmail.getValue() == null
				|| StringHelper.isEmpty(txtEmail.getValue().toString())) {
			errorMessages.add(CommonUtil
					.getBundleMessage("mems.inheritanceFile.emailReq"));
			validate = false;
		} else {
			// Email validation
			String email = txtEmail.getValue().toString();
			Pattern pattern = Pattern
					.compile("[a-zA-Z]*[0-9]*@[a-zA-Z]*.[a-zA-Z]*");
			Matcher matcher = pattern.matcher(email);
			if (!matcher.matches()) {
				errorMessages
						.add(CommonUtil
								.getBundleMessage("common.error.validation.invalidEmail"));
				validate = false;
			}
		}
		// If both Deptt and Bank r selected
		if ((cboBank.getValue().toString().compareTo("-1") != 0 && cboGovtDeptt
				.getValue().toString().compareTo("-1") != 0)
				|| (cboBank.getValue().toString().compareTo("-1") == 0 && cboGovtDeptt
						.getValue().toString().compareTo("-1") == 0)) {
			errorMessages.add(CommonUtil
					.getBundleMessage("mems.inheritanceFile.oneOfBanknGovt"));
			validate = false;
		}
		return validate;
	}

	public HtmlSelectOneMenu getCboAssetType() {
		return cboAssetType;
	}

	public void setCboAssetType(HtmlSelectOneMenu cboAssetType) {
		this.cboAssetType = cboAssetType;
	}

	public HtmlSelectOneMenu getCboBank() {
		return cboBank;
	}

	public void setCboBank(HtmlSelectOneMenu cboBank) {
		this.cboBank = cboBank;
	}

	public HtmlSelectOneMenu getCboGovtDeptt() {
		return cboGovtDeptt;
	}

	public void setCboGovtDeptt(HtmlSelectOneMenu cboGovtDeptt) {
		this.cboGovtDeptt = cboGovtDeptt;
	}

	public HtmlInputText getTxtTypeNameEn() {
		return txtTypeNameEn;
	}

	public void setTxtTypeNameEn(HtmlInputText txtTypeNameEn) {
		this.txtTypeNameEn = txtTypeNameEn;
	}

	public HtmlInputText getTxtTypeNameAr() {
		return txtTypeNameAr;
	}

	public void setTxtTypeNameAr(HtmlInputText txtTypeNameAr) {
		this.txtTypeNameAr = txtTypeNameAr;
	}

	public HtmlInputText getTxtContactPerson() {
		return txtContactPerson;
	}

	public void setTxtContactPerson(HtmlInputText txtContactPerson) {
		this.txtContactPerson = txtContactPerson;
	}

	public HtmlInputText getTxtOfficePhone() {
		return txtOfficePhone;
	}

	public void setTxtOfficePhone(HtmlInputText txtOfficePhone) {
		this.txtOfficePhone = txtOfficePhone;
	}

	public HtmlInputText getTxtCellPhone() {
		return txtCellPhone;
	}

	public void setTxtCellPhone(HtmlInputText txtCellPhone) {
		this.txtCellPhone = txtCellPhone;
	}

	public HtmlInputText getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(HtmlInputText txtEmail) {
		this.txtEmail = txtEmail;
	}

	public HtmlInputText getTxtFax() {
		return txtFax;
	}

	public void setTxtFax(HtmlInputText txtFax) {
		this.txtFax = txtFax;
	}

	public HtmlInputText getTxtDescription() {
		return txtDescription;
	}

	public void setTxtDescription(HtmlInputText txtDescription) {
		this.txtDescription = txtDescription;
	}

	public HtmlSelectBooleanCheckbox getChkMandatory() {
		return chkMandatory;
	}

	public void setChkMandatory(HtmlSelectBooleanCheckbox chkMandatory) {
		this.chkMandatory = chkMandatory;
	}

	public HtmlSelectBooleanCheckbox getChkEligible() {
		return chkEligible;
	}

	public void setChkEligible(HtmlSelectBooleanCheckbox chkEligible) {
		this.chkEligible = chkEligible;
	}

	@SuppressWarnings("unchecked")
	public void assetTypeChanged() {
		resetAllFields();
		if (cboAssetType.getValue() != null
				&& cboAssetType.getValue().toString().compareTo("-1") != 0) {
			try {
				AssetType selectedAssetType = service.getAssetTypeDetail(Long
						.parseLong(cboAssetType.getValue().toString()));
				if (selectedAssetType != null) {
					viewMap.put("SELECTED_ASSET_TYPE", selectedAssetType);
					fillUpAllFields(selectedAssetType);
				}
			} catch (Exception e) {
				errorMessages = new ArrayList<String>(0);
				logger.LogException("init" + "|Error Occured", e);
				errorMessages.add(ResourceUtil.getInstance().getProperty(
						MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		} else {
			resetAllFields();
		}
	}

	private void fillUpAllFields(AssetType selectedAssetType) {
		if (selectedAssetType != null) {
			txtTypeNameEn.setValue(selectedAssetType.getAssetTypeNameEn());
			txtTypeNameAr.setValue(selectedAssetType.getAssetTypeNameAr());

			chkEligible.setValue(selectedAssetType.getIsTakharuable()
					.equals(1L));
			chkMandatory
					.setValue(selectedAssetType.getIsMandatory().equals(1L));
		}
		if (selectedAssetType.getAssetTypeCncrndDeptt() != null
				&& selectedAssetType.getAssetTypeCncrndDeptt()
						.getAssetTypeCncrndDeptId() != null) {

			AssetTypeCncrndDept deptt = selectedAssetType
					.getAssetTypeCncrndDeptt();

			if (deptt.getBank() != null && deptt.getBank().getBankId() != null)
				cboBank.setValue(deptt.getBank().getBankId().toString());

			if (deptt.getGovtDepttId() != null)
				cboGovtDeptt.setValue(deptt.getGovtDepttId().toString());

			txtCellPhone.setValue(deptt.getPhone1());
			txtOfficePhone.setValue(deptt.getPhone2());
			txtFax.setValue(deptt.getFax());
			txtContactPerson.setValue(deptt.getContactPersonName());
			txtDescription.setValue("");
			txtEmail.setValue(deptt.getEmail());
		}
	}

	private void resetAllFields() {
		cboBank.setValue("-1");
		cboGovtDeptt.setValue("-1");

		txtCellPhone.setValue("");
		txtOfficePhone.setValue("");
		txtFax.setValue("");
		txtContactPerson.setValue("");
		txtDescription.setValue("");
		txtEmail.setValue("");
		txtTypeNameEn.setValue("");
		txtTypeNameAr.setValue("");

		chkEligible.setValue(false);
		chkMandatory.setValue(false);
	}
}
