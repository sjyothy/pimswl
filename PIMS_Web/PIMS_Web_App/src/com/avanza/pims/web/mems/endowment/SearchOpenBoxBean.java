package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.data.ApplicationContext;
import com.avanza.pims.entity.OpenBox;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.OpenBoxService;
import com.avanza.pims.ws.vo.DonationBoxView;
import com.avanza.ui.util.ResourceUtil;

public class SearchOpenBoxBean extends AbstractSearchBean {
	private static final long serialVersionUID = 1L;
	private String DEFAULT_SORT_FIELD = "openBoxId";
	private String DATA_LIST = "DATA_LIST";
	OpenBoxService service = new OpenBoxService();

	OpenBox criteria = new OpenBox();
 	 List<OpenBox> list = new ArrayList<OpenBox>();
	
	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		super.init();
		try {
			if (!isPostBack()) {
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
				setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
				setSortField(DEFAULT_SORT_FIELD);
				checkDataInQueryString();
			}
		} catch (Exception es) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init" + "|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void checkDataInQueryString() {
		checkPageMode();
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() {

		if (request.getParameter(VIEW_MODE) != null) {

			if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) {
				viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);

			} else if (request.getParameter(VIEW_MODE).equals(
					MODE_SELECT_MANY_POPUP)) {
				viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);

			}
		}
	}
	
    @SuppressWarnings("unchecked")
	public String onEdit() 
    {
		try 
		{
			OpenBox row =  (OpenBox) dataTable.getRowData();
			sessionMap.put(WebConstants.SELECTED_ROW, row);
			
		} catch (Exception e) 
		{
			logger.LogException("onEdit |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "Edit";
	}
    @SuppressWarnings("unchecked")
	public void onSearch() 
    {
		try 
		{
			
			loadDataList();
			pageFirst();
		}
		catch (Exception e) 
		{
			logger.LogException("onSearch |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
    
    @SuppressWarnings("unchecked")
	public void onCancelled() 
	{
		try 
		{

            try
            {
            	ApplicationContext.getContext().getTxnContext().beginTransaction();
            	OpenBox openBox = ( OpenBox )dataTable.getRowData();
            	openBox.setStatusId(   WebConstants.OpenBoxStatus.Cancelled_Id  );
            	openBox.setUpdatedBy(getLoggedInUserId());
            	openBox.setUpdatedOn(new Date());
            	service.persist(	openBox	);
            	CommonUtil.saveSystemComments( WebConstants.DonationBox.NOTES_OWNER, 
						                       "openBox.msg.cancelled",
						                       openBox.getOpenBoxId()
						                      );


				ApplicationContext.getContext().getTxnContext().commit();
				loadDataList();
				successMessages.add( ResourceUtil.getInstance().getProperty("openBox.msg.cancelled") );
            }
    		catch(Exception e)
    		{
    			ApplicationContext.getContext().getTxnContext().rollback();
    			throw e;
    		}
    		finally
    		{
    			ApplicationContext.getContext().getTxnContext().release();
    		}
		} 
		catch (Exception e) 
		{
			logger.LogException("onDistribute|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	//We have to look for the list population for facilities

	public void doSearch() {
		try {
			loadDataList();
		} catch (Exception e) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("doSearch" + "|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void doSearchItemList() {
		try {
			loadDataList();
		} catch (Exception e) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("doSearchItemList" + "|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public List<OpenBox> loadDataList() throws Exception {
		
		int totalRows = 0;
		totalRows = service.getOpenBoxRecordSize( criteria );
		setTotalRows(totalRows);
		doPagingComputations();

		list = service.searchOpenBox( criteria, getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());

		

		if (list.isEmpty() || list == null) {
			errorMessages = new ArrayList<String>();
			errorMessages
					.add(CommonUtil
							.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			forPaging(0);// searchedRows
			if(viewMap.get(DATA_LIST) != null){
				viewMap.remove(DATA_LIST);
			}
		}
		else{
			this.setList(list);
			forPaging(getTotalRows());// searchedRows
		}
		return list;
	}

	
	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	/**
	 * @return the recordSize
	 */
	@SuppressWarnings("unchecked")
	public Integer getRecordSize() {
		if (viewMap.get("recordSize") != null) {
			recordSize = (Integer) viewMap.get("recordSize");
		}
		if (recordSize == null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	@SuppressWarnings("unchecked")
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
		if (this.recordSize != null)
			viewMap.put("recordSize", this.recordSize);
	}

	public OpenBox getCriteria() {
		return criteria;
	}

	public void setCriteria(OpenBox criteria) {
		this.criteria = criteria;
	}

	@SuppressWarnings("unchecked")
	public List<OpenBox> getList() {
		if(viewMap.get(DATA_LIST) != null){
			list = (List<OpenBox>) viewMap.get(DATA_LIST);
		} 
		return list;
	}

	@SuppressWarnings("unchecked")
	public void setList(List<OpenBox> list) {
		if(list != null && list.size() > 0){
			viewMap.put(DATA_LIST, list);
		}
		this.list = list;
	}
	public boolean isSelectOnePopup(){
		if(viewMap.get(VIEW_MODE) != null && viewMap.get(VIEW_MODE).toString().compareTo(MODE_SELECT_ONE_POPUP) == 0)
			return true;
		else
			return false;
	}
	public boolean isSelectManyPopup(){
		if(viewMap.get(VIEW_MODE) != null && viewMap.get(VIEW_MODE).toString().compareTo(MODE_SELECT_MANY_POPUP) == 0)
			return true;
		else
			return false;
	}
	
	public boolean isPopup(){
		return (isPageModeSelectManyPopUp() || isPageModeSelectOnePopUp());
	}
	@SuppressWarnings("unchecked")
	public void onSingleSelect()
	{
		DonationBoxView selectedDonationBox = null;
		selectedDonationBox = (DonationBoxView) dataTable.getRowData();
		sessionMap.put(WebConstants.SELECTED_ROW,selectedDonationBox);
		executeJavascript("closeWindow();");
	}
	@SuppressWarnings("unchecked")
	public void onMultiSelect()
	{
		try 
		{
			list = getList();
			List<OpenBox> selectedDonationBoxes = new ArrayList<OpenBox>();
			if(list != null && list.size() > 0)
			{
				for(OpenBox box: list)
				{
					if(box.isSelected())
					{
						selectedDonationBoxes.add(box);
					}
				}
			}
			else
			{
					errorMessages = new ArrayList<String>();
					errorMessages.add(CommonUtil.getBundleMessage("donationBox.label.errMsg.noSelectMade"));
			}
			if(selectedDonationBoxes.size() > 0)
			{
				sessionMap.put(WebConstants.SELECTED_ROWS, selectedDonationBoxes);
				executeJavascript("closeWindow();");
			}
			else
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage("donationBox.label.errMsg.noSelectMade"));
			}
			
		} 
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onMultiSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}
	@SuppressWarnings("unchecked")
	private void executeJavascript(String javascript) 
	{
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
		} 
		catch (Exception exception) 
		{			
			logger.LogException("executeJavascript|Error Occured", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}
	public boolean isEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}
}
