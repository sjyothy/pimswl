package com.avanza.pims.web.mems;

import java.io.Serializable;

import javax.faces.component.html.HtmlDataTable;
import javax.servlet.http.HttpServletRequest;

import com.avanza.pims.web.WebConstants;

@SuppressWarnings("serial")
/**
 * <T> Class of dataList
 */
public abstract class AbstractSearchBean extends AbstractMemsBean implements
		Serializable {
	protected HtmlDataTable dataTable;
	protected String pageMode;
	protected Integer recordSize = 0;
	protected String VIEW_MODE = "pageMode";
	protected String MODE_SELECT_ONE_POPUP = "MODE_SELECT_ONE_POPUP";
	protected String MODE_SELECT_MANY_POPUP = "MODE_SELECT_MANY_POPUP";
	protected HttpServletRequest request = (HttpServletRequest) this
			.getFacesContext().getExternalContext().getRequest();

	public abstract void onSearch();

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public boolean isPageModeSelectManyPopUp() {
		if (viewMap.get(VIEW_MODE) != null
				&& viewMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase(
						MODE_SELECT_MANY_POPUP)) {
			return true;
		}
		return false;
	}

	public boolean isPageModeSelectOnePopUp() {
		if (viewMap.get(VIEW_MODE) != null
				&& viewMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase(
						MODE_SELECT_ONE_POPUP)) {
			return true;
		}
		return false;
	}

	public boolean issViewModePopUp() {

		if (viewMap.containsKey(VIEW_MODE) && viewMap.get(VIEW_MODE) != null
				&& (isPageModeSelectOnePopUp() || isPageModeSelectManyPopUp())) {
			return true;
		}
		return false;

	}

	/*
	 * ////////////////////Sorting///////////////////////////////////// private
	 * String sortField = null; private boolean sortAscending = true; // Actions
	 * -----------------------------------------------------------------------------------
	 * private static String getAttribute(ActionEvent event, String name) {
	 * return (String) event.getComponent().getAttributes().get(name); } public
	 * void sortDataList(ActionEvent event) { String sortFieldAttribute =
	 * getAttribute(event, "sortField"); // Get and set sort field and sort
	 * order. if (sortField != null && sortField.equals(sortFieldAttribute)) {
	 * sortAscending = !sortAscending; } else { sortField = sortFieldAttribute;
	 * sortAscending = true; } // Sort results. if (sortField != null) {
	 * Collections.sort(this.getList(), new ListComparator(sortField,
	 * sortAscending)); } }
	 */

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		return WebConstants.RECORDS_PER_PAGE;
	}

	/**
	 * @return the recordSize
	 */
	@SuppressWarnings("unchecked")
	public Integer getRecordSize() {
		if (viewMap.get("recordSize") != null) {
			recordSize = (Integer) viewMap.get("recordSize");
		}
		if (recordSize == null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize
	 *            the recordSize to set
	 */
	@SuppressWarnings("unchecked")
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
		if (this.recordSize != null)
			viewMap.put("recordSize", this.recordSize);
	}

	@SuppressWarnings("unchecked")
	public void forPaging(int totalRows) {
		recordSize = totalRows;
		this.setRecordSize(totalRows);
		paginatorRows = getPaginatorRows();
		paginatorMaxPages = recordSize / paginatorRows;
		if ((recordSize % paginatorRows) > 0)
			paginatorMaxPages++;
		if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
			paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		viewMap.put("paginatorMaxPages", paginatorMaxPages);
	}

	@SuppressWarnings("unchecked")
	public void forPaging(int totalRows,int pageRows) {
		recordSize = totalRows;
		this.setRecordSize(totalRows);
		paginatorRows = pageRows;
		paginatorMaxPages = recordSize / paginatorRows;
		if ((recordSize % paginatorRows) > 0)
			paginatorMaxPages++;
		if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
			paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		viewMap.put("paginatorMaxPages", paginatorMaxPages);
	}
}
