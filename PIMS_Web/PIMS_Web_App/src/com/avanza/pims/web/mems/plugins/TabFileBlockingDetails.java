package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.entity.BlockingDetails;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.BlockingAmountService;

public class TabFileBlockingDetails extends AbstractMemsBean
{	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tabMode;
    private List<BlockingDetails> blockingList ;
    private HtmlDataTable dataTable;
	public TabFileBlockingDetails() 
	{
		tabMode = WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_VIEW_ONLY;
		blockingList = new ArrayList<BlockingDetails>();
	}
	
	@Override
	public void init() 
	{
		
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException("init --- CRASHED --- ", exception);
		}
	}
	public void loadTabData(Long fileId) throws Exception
	{
		blockingList = BlockingAmountService.getBlockingListForFileId(fileId);
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws PimsBusinessException
	{
		// TAB MODE
		if ( viewMap.get( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY ) != null )
		{
			tabMode = (String) viewMap.get( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY );
		}

		if ( viewMap.get( WebConstants.BlockingRequest.BlockingList ) != null )
		{
			blockingList = (ArrayList<BlockingDetails>) viewMap.get( WebConstants.BlockingRequest.BlockingList );
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, tabMode );
		}
		if ( blockingList != null )
		{
			viewMap.put( WebConstants.BlockingRequest.BlockingList , blockingList );
		}
	}	

	@SuppressWarnings("unchecked")
	private boolean isValid( List<String> errorMessages  ) throws Exception 
	{
		boolean valid = true;
		boolean anySelected=false;
		for ( BlockingDetails blocking : blockingList) 
		{
			if( blocking.getSelected() != null && blocking.getSelected() )
			{
				anySelected = true;
				break;
			}
		}
		if( !anySelected )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "BlockingRequest.msg.selectAtleastOneBlockingLine" ) );
			return false;
		}
		return valid;
	}
	
    @SuppressWarnings( "unchecked" )
	public String onNavigateToUnblocking()
	{
		errorMessages = new ArrayList<String>();
		try	
		{	
			if ( !isValid( errorMessages ) ){ return "";  }
			updateValuesToMaps();
			List<BlockingDetails> selectedList  = new ArrayList<BlockingDetails>();
			for ( BlockingDetails blocking : blockingList) 
			{
				if( blocking.getSelected() != null && blocking.getSelected() )
				{
					selectedList.add( blocking );
				}
			}
			sessionMap.put( WebConstants.BlockingRequest.BlockingList , selectedList );
			updateValuesToMaps();
			return "unblock";
		}
		catch ( Exception e )
		{
			logger.LogException( "onNavigateToUnblocking --- CRASHED --- ", e);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		
		finally
		{
			if( errorMessages != null && errorMessages.size() > 0 )
			{
				viewMap.put( WebConstants.BlockingRequest.BLOCKING_DETAIL_TAB_ERROR_MESSAGES,errorMessages);
			}
		}
		return "";
	}
	
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE ) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<BlockingDetails> getBlockingList() {
		return blockingList;
	}

	public void setBlockingList(List<BlockingDetails> blockingList) {
		this.blockingList = blockingList;
	}

	
}
