package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.dao.RequestManager;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicantDetails;
import com.avanza.pims.web.backingbeans.HousingRejectReasonBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.workflow.WFClient;
import com.avanza.pims.web.workflow.WFTaskAction;
import com.avanza.pims.web.workflow.WorkFlowVO;
import com.avanza.pims.ws.mems.FamilyVillageFileService;
import com.avanza.pims.ws.mems.HousingService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.BeneficiaryGridView;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.ContactReferenceView;
import com.avanza.pims.ws.vo.FamilyVillageFileView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.TaskListVO;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.ui.util.ResourceUtil;


public class TerminateHousingRequestBean extends AbstractMemsBean
{

	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(TerminateHousingRequestBean.class);
	
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_RESUBMITTED = "PAGE_MODE_RESUBMITTED";
	private static final String PAGE_MODE_COLLECTION_REQUIRED = "COLLECTION_REQUIRED";
	private static final String PAGE_MODE_APPROVAL_REQUIRED = "APPROVAL_REQUIRED";
	private static final String PAGE_MODE_APPROVED = "APPROVED";
	private static final String PAGE_MODE_REJECTED = "REJECTED";
	private static final String PAGE_MODE_COMPLETED = "COMPLETED";
	
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_PAYMENT_DETAILS   = "paymentsTab";
	private static final String TAB_FILE   = "fileTab";
	private static final String TAB_ACTION = "actionTab";
	private static final String TAB_APPLICANTS   = "applicationTab";
	private static final String BENEFICIARY_TAB = "inheritanceBeneficiaryTabId";
	private static final String TAB_REVIEW  = "reviewTab";
	private static final String PAGE_MODE_REVIEW_REQUIRED= "REVIEW_REQUIRED";
	protected TaskListVO userTask;
    private String pageTitle;
    private String pageMode;
    private String hdnPersonId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;
    private String selectOneCollectedByUser;
    private String txtRemarks;
    private RequestService requestService = new RequestService();
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	private RequestView requestView ;
    protected HtmlCommandButton btnSendBack =new HtmlCommandButton();
    private HtmlDataTable dataTable;
    private String academicLevel;
    
    
	public TerminateHousingRequestBean(){}
	
	private PersonView beneficiary = new PersonView();
	
	private List<SelectItem> countryList = new ArrayList<SelectItem>();
	
	RequestManager requestManager = new RequestManager();
	
	private InheritanceFileView inheritanceFile = new InheritanceFileView();
	
	
	private HtmlSelectOneMenu cmbReviewGroup = new HtmlSelectOneMenu();
	
	
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.TerminateHousingRequest.PROCEDURE_TERMINATE_HOUSING_REQUEST);
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.TerminateHousingRequest.PROCEDURE_TERMINATE_HOUSING_REQUEST);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		viewMap.put("noteowner", WebConstants.REQUEST);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		
		loadAttachmentsAndComments( null );
		
		
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
		  getDataFromTaskList();
		}
		else if ( getRequestMap().get( WebConstants.REQUEST_VIEW) != null )
		{
		  getDataFromSearch();
		  
		}
		else
		{
			setDataForFirstTime();
		}
		loadApplicantDetailsTab();
		loadCombos();
		updateValuesFromMap();
		getPageModeFromRequestStatus();
	}
	
	public void prerender(){
	

		if( !isTaskAvailable() && 
			( 
					( getStatusApprovalRequired() || getStatusReviewRequired() ) 
			 )
		  )
		{
			if( viewMap.remove("fetchTaskNotified") == null  && 
				(
						sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null || 
						getRequestMap().get( WebConstants.REQUEST_VIEW) != null) 
				)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("commons.msg.fectchtasktoperformaction"));
				viewMap.put("fetchTaskNotified", true);
			}
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		requestView = new RequestView();
		requestView.setCreatedOn(new Date());
		requestView.setCreatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedOn(new Date());
		requestView.setStatusId( WebConstants.REQUEST_STATUS_NEW_ID );
		requestView.setStatusDataValue( WebConstants.REQUEST_STATUS_NEW );
		requestView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		requestView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
		requestView.setRequestDate( new Date() );
		requestView.setRequestTypeId( WebConstants.MemsRequestType.TERMINATE_HOUSING_REQUEST_ID);
		
		beneficiary.setCreatedOn(new Date());
		beneficiary.setUpdatedOn(new Date());
		beneficiary.setCreatedBy(getLoggedInUserId());
		beneficiary.setIsCompany(0L);
		beneficiary.setRecordStatus(1L);
		beneficiary.setIsDeleted(0L);
		

	}
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromRequestStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		tabPanel.setSelectedTab(TAB_APPLICANTS);
		if( this.requestView == null  || 
			this.requestView.getRequestId() == null ||
			this.requestView.getStatusId() == null 
		  ) 
		{ return; }
		if(userTask == null || (userTask.getClaimBy() != null && userTask.getClaimBy().equals(getLoggedInUserId())) ){
			if( getStatusApprovalRequired() || this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT) == 0 || getStatusReviewDone())
			{
				setPageMode( PAGE_MODE_APPROVAL_REQUIRED);
	//			tabPanel.setSelectedTab(TAB_ACTION );
				if( getStatusReviewDone()  )
				{
					tabPanel.setSelectedTab( TAB_REVIEW );
				}
				
				
			}
			
			else if (getStatusApproved())
			{
				setPageMode(PAGE_MODE_APPROVED);
				tabPanel.setSelectedTab(TAB_FILE );
			}	
			
			else if (getStatusRejected())
			{
				setPageMode(PAGE_MODE_REJECTED);
				tabPanel.setSelectedTab(TAB_FILE );
			}	
			
			
			else if( getStatusReviewRequired()   )
			{
				setPageMode( PAGE_MODE_REVIEW_REQUIRED );
				viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
				tabPanel.setSelectedTab( TAB_REVIEW );
			}
			
		}
		else {
			setPageMode(PAGE_MODE_VIEW);
		}
			
	}
	public boolean getStatusNew() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_NEW ) == 0;
	}
	public boolean getStatusApprovalRequired() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED) == 0;
	}
	public boolean getStatusRejected() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REJECTED) == 0;
	}
	public boolean getStatusApproved() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_APPROVED ) == 0;
	}
	
	public boolean getStatusCompleted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_COMPLETE ) == 0;
	}
	
	public boolean getStatusReviewRequired() {
		return this.requestView.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID) == 0;
	}
	public boolean getStatusReviewDone() {
		return this.requestView.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_REVIEW_DONE_ID) == 0;
	}

	
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = ( RequestView )viewMap.get( WebConstants.REQUEST_VIEW ) ;
			requestView.setUpdatedBy(getLoggedInUserId());
		}
		
		if( viewMap.get(  WebConstants.HousingRequest.BENEFICIARY) != null)
		{
			beneficiary = (	PersonView )viewMap.get(  WebConstants.HousingRequest.BENEFICIARY ); 
		}
		
		if( viewMap.get("InheritanceFile") != null)
		{
			inheritanceFile = (	InheritanceFileView )viewMap.get(  "InheritanceFile" ); 
		}
		
		if( viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null)
		{
			userTask = (	TaskListVO )viewMap.get(  WebConstants.TASK_LIST_SELECTED_USER_TASK); 
		}
		
		
		
		
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( requestView != null )
		{
		  viewMap.put(  WebConstants.REQUEST_VIEW , requestView);
		}
		
		
		if( beneficiary != null )
		{
			viewMap.put(  WebConstants.HousingRequest.BENEFICIARY, beneficiary);
		}
		
		if( inheritanceFile != null )
		{
			viewMap.put(  "InheritanceFile", inheritanceFile);
		}
//		if(countryList != null)
//			
//			viewMap.put(  "countryList", countryList);
		
		if(userTask != null)
			viewMap.put(  WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
		
	}
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		requestView = ( RequestView)getRequestMap().get( WebConstants.REQUEST_VIEW) ;
		if( this.requestView !=null )
		{
		  getTerminateHousingRequestDetails( requestView.getRequestId() );
		}
	}
	
        
	
	@SuppressWarnings(  "unchecked"  )
	public void loadApplicantDetailsTab()throws Exception
	{
		ApplicantDetails bean = (ApplicantDetails)getBean("pages$ApplicantDetails");
		
		if(requestView.getApplicantView() != null){
			String jobTitle = null, firstName = null;
			String stateId = null;
			if( requestView.getApplicantView().getContactReferenceViewSet() != null && 
					requestView.getApplicantView().getContactReferenceViewSet().iterator().hasNext()){
				ContactReferenceView referenceView = requestView.getApplicantView().getContactReferenceViewSet().iterator().next();
				jobTitle = referenceView.getJobTitle();
				firstName = referenceView.getFirstName();
			}
			
			if(requestView.getApplicantView().getContactInfoViewSet() != null && 
					requestView.getApplicantView().getContactInfoViewSet().iterator().hasNext()
					){
				ContactInfoView contactInfoView = requestView.getApplicantView().getContactInfoViewSet().iterator().next();
				stateId = contactInfoView.getStateId().toString();
			}
				
			
			bean.setApplicantDetails(requestView.getApplicantView().getCompanyName(), stateId, firstName, jobTitle);
		}
	}
	public void saveApplicantDetailsTab()throws Exception
	{
		ApplicantDetails bean = (ApplicantDetails)getBean("pages$ApplicantDetails");
		PersonView applicant = new PersonView();
		
		applicant.setCompanyName(bean.getTxtOrganization());
		
		Set<ContactInfoView> contactInfoViewSet =  new HashSet<ContactInfoView>();
		Set<ContactReferenceView> contactReferenceViewSet = new HashSet<ContactReferenceView>();
		
		ContactInfoView contactInfoView = new ContactInfoView();
		
		if(bean.getStateId() != null){
			
			contactInfoView.setStateId(Long.valueOf(bean.getStateId()));
			
			contactInfoView.setCreatedBy(getLoggedInUserId());
			contactInfoView.setCreatedOn(new Date());
			contactInfoView.setUpdatedBy(getLoggedInUserId());
			contactInfoView.setUpdatedOn(new Date());
			contactInfoView.setRecordStatus(1L);
			contactInfoView.setIsDeleted(0L);
			contactInfoViewSet.add(contactInfoView);
			applicant.setContactInfoViewSet(contactInfoViewSet);
			
			
		}
		
		if(bean.getTxtJobTitle() != null && bean.getTxtRequester() != null){
			ContactReferenceView contactReferenceView = new ContactReferenceView();
			
			contactReferenceView.setJobTitle(bean.getTxtJobTitle());
			contactReferenceView.setFirstName(bean.getTxtRequester());
			
			contactReferenceView.setCreatedBy(getLoggedInUserId());
			contactReferenceView.setCreatedOn(new Date());
			contactReferenceView.setUpdatedBy(getLoggedInUserId());
			contactReferenceView.setUpdatedOn(new Date());
			contactReferenceView.setRecordStatus(1L);
			contactReferenceView.setIsDeleted(0L);
			contactReferenceView.setContactInfoView(contactInfoView);
			
			
			contactReferenceViewSet.add(contactReferenceView);
			applicant.setContactReferenceViewSet(contactReferenceViewSet);
		}
		
		
		applicant.setRecordStatus(1L);
		applicant.setCreatedBy(getLoggedInUserId());
		applicant.setUpdatedBy(getLoggedInUserId());
		applicant.setCreatedOn(new Date());
		applicant.setUpdatedOn(new Date());
		applicant.setIsDeleted(0L);
		applicant.setIsCompany(1L);
		
		requestView.setApplicantView(applicant);
		
		
	}
	
	
	
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  userTask = ( TaskListVO )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  if( userTask.getRequestId() == null ){ return; }
	  long  id = new Long  ( userTask.getRequestId() );
	  getTerminateHousingRequestDetails( id );
	  
	  
    }
	
	@SuppressWarnings( "unchecked" )
	private void getTerminateHousingRequestDetails( long id ) throws Exception 
	{
		requestView = requestService.getRequestById(id );
		loadAttachmentsAndComments( id );
		beneficiary = requestView.getTenantsView();
		inheritanceFile = requestView.getInheritanceFileView();
		updateValuesToMap();
	}
	

	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	
	
	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(requestView.getRequestId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.REQUEST;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, requestView.getRequestId());
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.REQUEST;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(requestView.getRequestId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, requestView.getRequestId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }

	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( requestView != null && requestView.getRequestId()!= null )
		 {
			 loadAttachmentsAndComments( requestView.getRequestId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMap();
			if(hdnPersonType.equals( "APPLICANT" ) )
			{
//				populateApplicationDetailsTab();
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	@SuppressWarnings( "unchecked" )
	public void onPaymentMethodChanged()
	{
		try	
		{
			updateValuesFromMap();
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onPaymentMethodChanged|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if(requestView == null || requestView.getRequestId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,requestView.getRequestId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		
		ApplicantDetails bean = (ApplicantDetails)getBean("pages$ApplicantDetails");
		
		if( bean.getTxtOrganization() == null || bean.getTxtOrganization().equals("")) {
			errorMessages.add(CommonUtil.getBundleMessage("error.terminateHousing.organization"));
			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		if( bean.getTxtRequester() == null || bean.getTxtRequester().equals("")) { 
			errorMessages.add(CommonUtil.getBundleMessage("error.terminateHousing.requester"));
			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		if( bean.getTxtJobTitle() == null  || bean.getTxtJobTitle().equals("")) {
			errorMessages.add(CommonUtil.getBundleMessage("error.terminateHousing.jobTitle"));
			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		
		
		if( beneficiary.getFirstName() == null ) {
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.beneficiaryReq"));
			tabPanel.setSelectedTab(BENEFICIARY_TAB);
			return true;
		}
		if(requestView  == null || requestView.getTerminateHousingReasonId() == null || requestView.getTerminateHousingReasonId().equals("-1"))
		{
			errorMessages.add(CommonUtil.getBundleMessage("terminatehousingRequest.msg.terminationRequestReasonRequired"));
			tabPanel.setSelectedTab(BENEFICIARY_TAB);
			return true;
		}
		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
//			Long status = WebConstants.REQUEST_STATUS_NEW_ID;
			if(requestView.getSocialResearcherId().equals("-1"))
			{
				requestView.setSocialResearcherId(null);
			}
			saveApplicantDetailsTab();
			
			saveHousingRequestInTransaction(null);

			
			
			getPageModeFromRequestStatus();
			
			if(requestView.getStatusId().compareTo(WebConstants.REQUEST_STATUS_NEW_ID) == 0)
				saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
			else
				saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_UPDATED );
			successMessages.add( ResourceUtil.getInstance().getProperty( "thirdPartRevenue.msg.saved" ) );
			getTerminateHousingRequestDetails( requestView.getRequestId() );
			
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onApprove()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			Long status = WebConstants.REQUEST_STATUS_APPROVED_ID;
			
			ApplicationContext.getContext().getTxnContext().beginTransaction();
//			if( hasCompleteErrors() ){ return; }
			
			 updateValuesFromMap();
			 
//			 SystemParameters parameters = SystemParameters.getInstance();
//			 String endPoint= parameters.getParameter( "MEMSDonationRequestBPEL" );
//			 MEMSDonationRequestBPELPortClient port=new MEMSDonationRequestBPELPortClient();
//			 port.setEndpoint(endPoint);
			 persistRequest(status);
			 
			 
			
			 
			 
			 getTerminateHousingRequestDetails( requestView.getRequestId() );
			 
			 
			 saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_APPROVED );
			 
//			 port.initiate(
//					 		CommonUtil.getLoggedInUser(),
//					 		Integer.parseInt( requestView.getRequestId().toString()),
//					 		requestView.getRequestNumber(), 
//					 		null, 
//					 		null
//					 	   );
			 
			 
			 
			 
			 WorkFlowVO workFlowVO = new WorkFlowVO();
			 workFlowVO.setTaskAction(WFTaskAction.APPROVED.toString());
			 
			 CommonUtil.completeTask(workFlowVO,userTask,getLoggedInUserId());
			 
			 
			 
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 this.setPageMode( PAGE_MODE_VIEW );
			successMessages.add( ResourceUtil.getInstance().getProperty( "mems.req.common.suc.msgs.approve" ) );
			updateValuesToMap();
			
		}
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException( "onApprove --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally{
			ApplicationContext.getContext().getTxnContext().release();
		}
		
	}
	
	
	
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromHousingRejectionReason()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 (WebConstants.REQUEST_STATUS_CANCELED_ID);
		String msg    =  MessageConstants.RequestEvents.REQUEST_CANCELED;
		String event  =  MessageConstants.RequestEvents.REQUEST_CANCELED;
		try	
		{	
			ApplicationContext.getContext().getTxnContext().beginTransaction();
//			if(!hasReasonProvided() ){ return; }
			
			updateValuesFromMap();
			
			String reason = (String) sessionMap.get(HousingRejectReasonBean.REJECT_DESC);
			String reasonID = (String)sessionMap.get(HousingRejectReasonBean.REJECT_REASON_ID);
			
			requestView.setHousingRequestRejectionReasonDescription(reason);
			
			if(reasonID != null)
				requestView.setHousingRequestRejectionReasonId(Long.parseLong(reasonID));
			
			 persistRequest(status);
			 getTerminateHousingRequestDetails(requestView.getRequestId());
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 
		   	
		   	 WorkFlowVO workFlowVO = new WorkFlowVO();
			 workFlowVO.setTaskAction(WFTaskAction.REJECT.toString());
			 
			 CommonUtil.completeTask(workFlowVO,userTask,getLoggedInUserId());
		   	 
		   	 updateValuesToMap();
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromHousingRejectionReason--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onSendBackFromApprover()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			Long status = WebConstants.REQUEST_STATUS_REJECTED_ID;
			
//			if(!hasReasonProvided())
//				return ;
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			 updateValuesFromMap();
			 
			 persistRequest(status);
			 
			 getTerminateHousingRequestDetails( requestView.getRequestId() );
			 
			 
			 saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SENTBACK );
			 
			 WFClient client = new WFClient();
			 WorkFlowVO workFlowVO = new WorkFlowVO();
			 
			 workFlowVO.setUserId(getLoggedInUserId());
			 workFlowVO.setTaskId(userTask.getTaskId());
			 workFlowVO.setTaskAction(WFTaskAction.RESUBMIT.toString());
			 workFlowVO.setProcedureTaskId(userTask.getProcedureTaskId());
			 String result = client.completeTask(workFlowVO);
			 
			 logger.logInfo("result:client.completeTask " +  result);
			 
			 
			 
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 this.setPageMode( PAGE_MODE_VIEW );
			successMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.RequestEvents.REQUEST_SENTBACK) );
			updateValuesToMap();
			
		}
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException( "onApprove --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally{
			ApplicationContext.getContext().getTxnContext().release();
		}
		
	}
	
	
	@SuppressWarnings( "unchecked" )
	private void saveHousingRequestInTransaction(Long status ) throws Exception 
	{
		try
		{
			
			
//            if(requestView.getStatusId() != null)
//            {
//            	status = null;
//            }
            ApplicationContext.getContext().getTxnContext().beginTransaction();
            persistRequest( status );
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistRequest(Long status) throws Exception 
	{
	
		if(status!=null) requestView.setStatusId(status);
		HousingService.saveTerminateHousingRequest(beneficiary, requestView);
		
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onComplete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 (WebConstants.REQUEST_STATUS_COMPLETE_ID);
		String msg    =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		String event  =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		try	
		{	
			
			if( hasCompleteErrors() ){ return; }
			
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 updateValuesFromMap();
			 FamilyVillageFileService.removeBeneficiaryFromFile(requestView.getTenantsView().getPersonId(), requestView.getInheritanceFileId() , requestView.getRequestId(), getLoggedInUserId());
			 persistRequest(status);
			 saveCommentsAttachment( event );
		 	 WFClient client = new WFClient();
			 WorkFlowVO workFlowVO = new WorkFlowVO();
			 workFlowVO.setUserId(getLoggedInUserId());
			 workFlowVO.setTaskId(userTask.getTaskId());
			 workFlowVO.setTaskAction(WFTaskAction.APPROVED.toString());
			 String result = client.completeTask(workFlowVO);
			 
			 logger.logInfo("result:client.completeTask " +  result);
			
			 
			 
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	updateValuesToMap();
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onComplete --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onPostBack()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
//		Long status = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COLLECTION_REQ);
		Long status   =	 (WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT_ID);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		try	
		{	
			
			if( hasCompleteErrors() || !hasReasonProvided() ){ return; }
			
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			 updateValuesFromMap();
			 
//			 SystemParameters parameters = SystemParameters.getInstance();
//			 String endPoint= parameters.getParameter( "MEMSDonationRequestBPEL" );
//			 MEMSDonationRequestBPELPortClient port=new MEMSDonationRequestBPELPortClient();
//			 port.setEndpoint(endPoint);
//			 requestView.setInheritanceFileId(inheritanceFile.getInheritanceFileId());
			 persistRequest(status);
			 
//			 getTerminateHousingRequestDetails( requestView.getRequestId() );
			 
//			 FamilyVillageFileService.addFileBeneficiaries(beneficiary, inheritanceFile);
			 
			 
			 
			 saveCommentsAttachment( event );
			 
//			 port.initiate(
//					 		CommonUtil.getLoggedInUser(),
//					 		Integer.parseInt( requestView.getRequestId().toString()),
//					 		requestView.getRequestNumber(), 
//					 		null, 
//					 		null
//					 	   );
			 
		 	WFClient client = new WFClient();
			 
			 WorkFlowVO workFlowVO = new WorkFlowVO();
			 
			 workFlowVO.setUserId(getLoggedInUserId());
			 workFlowVO.setTaskId(userTask.getTaskId());
			 workFlowVO.setTaskAction(WFTaskAction.POSTBACK.toString());
			 workFlowVO.setProcedureTaskId(userTask.getProcedureTaskId());
			 String result = client.completeTask(workFlowVO);
			 
			 logger.logInfo("result:client.completeTask " +  result);
			
			 
			 
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	updateValuesToMap();
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onComplete --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onReSubmitted()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 (WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL;
		try	
		{	
			
			if( hasSaveErrors() ){ return; }
			
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			 updateValuesFromMap();
			 
			 persistRequest(status);
			 
			 
			 saveCommentsAttachment( event );
			 
			 
		 	WFClient client = new WFClient();
			 
			 WorkFlowVO workFlowVO = new WorkFlowVO();
			 
			 workFlowVO.setUserId(getLoggedInUserId());
			 workFlowVO.setTaskId(userTask.getTaskId());
			 workFlowVO.setTaskAction(WFTaskAction.POSTBACKRESUBMIT.toString());
			 workFlowVO.setProcedureTaskId(userTask.getProcedureTaskId());
			 String result = client.completeTask(workFlowVO);
			 
			 logger.logInfo("result:client.completeTask " +  result);
			
			 
			 
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	updateValuesToMap();
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onComplete --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onSubmitted()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID;
		String msg    =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		String event  =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() ){ return; }
		 
		 updateValuesFromMap();	
		 
		 persistRequest( status );
		 getTerminateHousingRequestDetails( requestView.getRequestId() );
		 

		 WFClient client = new WFClient();
		 
		 WorkFlowVO workFlowVO = new WorkFlowVO();
		 
		 workFlowVO.setRequestId(requestView.getRequestId());
		 workFlowVO.setUserId(getLoggedInUserId());
		 workFlowVO.setProcedureTypeId(WebConstants.TerminateHousingRequest.PROCEDURE_TERMINATE_HOUSING_REQUEST);
		
		 client.initiateProcess(workFlowVO);
		 
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		 
		 updateValuesToMap();
		 
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSubmitted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onReviewed()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_REVIEWED;
		String event  =  MessageConstants.RequestEvents.REQUEST_REVIEWED;
		try	
		{
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			 

			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 	
			 UtilityService utilityService = new UtilityService();
			 ReviewRequestView reviewRequestView = ( ReviewRequestView ) viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW );
			 utilityService.persistReviewRequest( reviewRequestView );
				
			 persistRequest( WebConstants.REQUEST_STATUS_REVIEW_DONE_ID );
			 
			 //todo: add task outcome
			 WorkFlowVO workFlowVO = new WorkFlowVO();
			 workFlowVO.setTaskAction(WFTaskAction.POSTBACKRESUBMIT.toString());
			 
			 CommonUtil.completeTask(workFlowVO,userTask,getLoggedInUserId());
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onReviewed--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	
	public boolean hasReviewErrors() throws Exception
	{
		if( null==cmbReviewGroup.getValue() || cmbReviewGroup.getValue().toString().equals( "-1")) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_REVIEW_GRP));
			return true;
		}
		
		
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public void onReviewRequired()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW;
		try	
		{	updateValuesFromMap();	
			if( hasSaveErrors() || hasReviewErrors() || !hasReasonProvided()){ return; }
			 
			 
			 UtilityService utilityService = new UtilityService();
			 ReviewRequestView reviewRequestView = new ReviewRequestView();
			 reviewRequestView.setCreatedBy( CommonUtil.getLoggedInUser() );
			 reviewRequestView.setCreatedOn( new Date() );
			 reviewRequestView.setRfc( txtRemarks.trim() );
			 reviewRequestView.setGroupId( cmbReviewGroup.getValue().toString() );
			 reviewRequestView.setRequestId( requestView.getRequestId() );
		
			 try 
			 {
					 ApplicationContext.getContext().getTxnContext().beginTransaction();
					 utilityService.persistReviewRequest( reviewRequestView );				
					 persistRequest( WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID);
					 ApplicationContext.getContext().getTxnContext().commit();
			 }
			 catch (Exception exception) 
			 {
					ApplicationContext.getContext().getTxnContext().rollback();
					throw exception;
			 }
			finally
			{
				ApplicationContext.getContext().getTxnContext().release();
			}

			 getTerminateHousingRequestDetails( requestView.getRequestId() );
			 
			 
//			 setTaskOutCome(TaskOutcome.FORWARD);
			 
			 WorkFlowVO workFlowVO = new WorkFlowVO();
			 workFlowVO.setTaskAction(WFTaskAction.REVIEW.toString());
			 workFlowVO.setAssignedGroups(cmbReviewGroup.getValue().toString());
			 CommonUtil.completeTask(workFlowVO,userTask,getLoggedInUserId());
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			logger.LogException("onReviewRequired--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	
	
	
	private boolean hasReasonProvided() throws Exception
	{
		if(txtRemarks ==null || txtRemarks.trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.reasonRequired")  );
			return false;
		}
		return true;
	}
	
	
	@SuppressWarnings( "unchecked" )
	private boolean hasCompleteErrors() throws Exception
	{
		errorMessages = new ArrayList<String>();

		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
//		if( inheritanceFile.getInheritanceFileId() == null) {
//			errorMessages.add(CommonUtil.getBundleMessage("associateCostCenter.msg.fileRequired"));
//			tabPanel.setSelectedTab(TAB_FILE);
//			return true;
//		}
		
		
		return hasSaveErrors;
		
	}
	

	
	public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("housingRequest.title.heading"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{

		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  
			(
					getPageMode().equals( PAGE_MODE_NEW ) ||  
					getPageMode().equals( PAGE_MODE_APPROVAL_REQUIRED ) ||
					getPageMode().equals( PAGE_MODE_REJECTED)
				) 
		   )
		{
			return true;
		}
		return false;
	
		
	}

	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{

		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  getPageMode().equals( PAGE_MODE_NEW ))
		{
			return true;
		}
		return false;
	
		
	}
	
	

	@SuppressWarnings( "unchecked" )
	public Boolean getShowResubmitForApprovalButton()
	{

		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  getPageMode().equals( PAGE_MODE_REJECTED ) )
		{
			return true;
		}
		return false;
	
		
	}
	

	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowCompleteButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
				( getPageMode().equals( PAGE_MODE_APPROVED ) || getPageMode().equals( PAGE_MODE_COMPLETED ) )  
					
		      ) 
			{
				return true;
			}
			return false;
		
	}
	

	@SuppressWarnings( "unchecked" )
	public Boolean getShowPostBackButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
				( getPageMode().equals( PAGE_MODE_APPROVED ) || getPageMode().equals( PAGE_MODE_COMPLETED ) )  
					
		      ) 
			{
				return true;
			}
			return false;
		
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowApproveRejectButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_APPROVAL_REQUIRED ) )
		 )
		{
			return true;
		}
		return false;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowPrintButton()
	{
		if(  getStatusCompleted() )
		{
			return true;
		}
		return false;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	
	
	public void loadCombos(){
		loadCountryList();
	}
	
	 @SuppressWarnings("unchecked")
	  public void loadCountryList()  {
			
			try {
			PropertyServiceAgent psa = new PropertyServiceAgent();
			List <RegionView> regionViewList = psa.getCountry();
			
			
			for(int i=0;i<regionViewList.size();i++)
			  {
				  RegionView rV=(RegionView)regionViewList.get(i);
				  SelectItem item;
				  if (isEnglishLocale())
				  {
					 item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionEn());			  }
				  else 
					  item = new SelectItem(rV.getRegionId().toString(), rV.getDescriptionAr());	  
			      
				
				  countryList.add(item);
			     
			  }
			Collections.sort(countryList, ListComparator.LIST_COMPARE);
			 viewMap.put("countryList", countryList);
			}catch (Exception e){
				logger.LogException("loadCountry|Error Occured ",e);
				
			}
		}
	
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public String getSelectOneCollectedByUser() {
		return selectOneCollectedByUser;
	}

	public void setSelectOneCollectedByUser(String selectOneCollectedByUser) {
		this.selectOneCollectedByUser = selectOneCollectedByUser;
	}


	public HtmlCommandButton getBtnSendBack() {
		return btnSendBack;
	}

	public void setBtnSendBack(HtmlCommandButton btnSendBack) {
		this.btnSendBack = btnSendBack;
	}

	public PersonView getBeneficiary() {
		return beneficiary;
	}

	public void setBeneficiary(PersonView beneficiary) {
		this.beneficiary = beneficiary;
	}
	

	public List<SelectItem> getCountryList() {
		if(viewMap.containsKey("countryList"))
			countryList= (List<SelectItem>) (viewMap.get("countryList"));
		return countryList;
	}

	public void setCountryList(List<SelectItem> countryList) {
		this.countryList = countryList;
		if(this.countryList !=null)
			viewMap.put("countryList",this.countryList );
	}
	
	public Boolean getBeneficiaryDetailsReadonlyMode(){
		Boolean applicationDetailsReadonlyMode = (Boolean)viewMap.get("beneficiaryDetailsReadonlyMode");
		if(applicationDetailsReadonlyMode==null)
			applicationDetailsReadonlyMode = false;
		return applicationDetailsReadonlyMode;
	}
	
	public String getBeneficiaryDescriptionReadonlyModeClass(){

		String applicationDetailsReadonlyMode = (String)viewMap.get("beneficiaryDescriptionReadonlyMode");
		if(applicationDetailsReadonlyMode==null)
			applicationDetailsReadonlyMode = "";
		return applicationDetailsReadonlyMode;
	
		
	}

	public InheritanceFileView getInheritanceFile() {
		return inheritanceFile;
	}

	public void setInheritanceFile(InheritanceFileView inheritanceFile) {
		this.inheritanceFile = inheritanceFile;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchFile()
	{
		errorMessages = new ArrayList<String>();
		try	
		{
			updateValuesFromMap();
			InheritanceFileView fileView = new InheritanceFileView();
			
			FamilyVillageFileView view = 		(FamilyVillageFileView)		sessionMap.remove(WebConstants.SELECTED_ROW);
			fileView.setInheritanceFileId( view.getInheritanceFileId());
			fileView.setFileNumber(view.getFileNumber());
			fileView.setFamilyVillageVillaView(view.getFamilyVillageVillaView());
			this.inheritanceFile = fileView;
			updateValuesToMap();
//			if( sessionMap.get(WebConstants.InheritanceFileSearchOutcomes.INHERITANCE_FILE_SEARCH_SELECTED_ONE_FILE ) != null )
//			{
//				
//				InheritanceFileView fileView = (InheritanceFileView)sessionMap.remove(WebConstants.InheritanceFileSearchOutcomes.INHERITANCE_FILE_SEARCH_SELECTED_ONE_FILE );
//				
//				this.inheritanceFile = fileView;
//				
//				updateValuesToMap();
//			}
		}
		catch(Exception e)
		{
			logger.LogException("onMessageFromSearchFile|Error Occured..",e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

    public void setCmbReviewGroup(HtmlSelectOneMenu cmbReviewGroup) {
        this.cmbReviewGroup = cmbReviewGroup;
    }

    public HtmlSelectOneMenu getCmbReviewGroup() {
        return cmbReviewGroup;
    }

    public void setAcademicLevel(String academicLevel) {
        this.academicLevel = academicLevel;
    }

    public String getAcademicLevel() {
        return academicLevel;
    }
    
    
    public String onMessageFromSearchBeneficiary(){
		
		try	
		{	
			
			if( sessionMap.get(WebConstants.BeneficiarySearchOutcomes.BENEFICIARY_SEARCH_SELECTED_ONE_BENEFICIARY ) != null )
			{
				
				BeneficiaryGridView beneficiaryGridView = (BeneficiaryGridView)sessionMap.remove( WebConstants.BeneficiarySearchOutcomes.BENEFICIARY_SEARCH_SELECTED_ONE_BENEFICIARY  );
				logger.logInfo("Selected Beneficiary: "+beneficiaryGridView.getBeneficiaryName());
				beneficiary.setFirstName(beneficiaryGridView.getPersonName());
				beneficiary.setPersonId(beneficiaryGridView.getBeneficiaryPersonId());
				beneficiary.setDateOfBirth(beneficiaryGridView.getDateOfBirth());
				beneficiary.setGender(beneficiaryGridView.getGender());
				inheritanceFile =new InheritanceFileView();
				inheritanceFile.setInheritanceFileId(beneficiaryGridView.getInheritanceFileId());
				requestView.setInheritanceFileView(inheritanceFile);
				requestView.setInheritanceFileId(inheritanceFile.getInheritanceFileId());
			}
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromSearchBeneficiary --- CRASHED --- ", exception);
		}
		
		return null;
    }

}
