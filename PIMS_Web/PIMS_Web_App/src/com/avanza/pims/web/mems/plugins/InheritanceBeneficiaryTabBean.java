package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.minors.InheritanceFileBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.vo.BeneficiaryDetailsView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;
import com.avanza.pims.ws.vo.RelationshipView;
import com.avanza.ui.util.ResourceUtil;

public class InheritanceBeneficiaryTabBean extends AbstractMemsBean {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private HtmlInputText txtFileNumber;
	private HtmlInputText txtFilePerson;
	private HtmlInputText txtSponser;
	private HtmlSelectOneMenu cboFileType;
	private HtmlCalendar clndrMissingFrom;
	private HtmlCalendar clndrCreatedOn;
	private List<SelectItem> benenficiaryPassportCityList;
	private List<SelectItem> cityList;
	private List<SelectItem> cityListSponsor;
	private boolean isVisible;
	private PersonView filePerson;
	private PersonView sponsor;
	private PersonView beneficiary;
	private PersonView nurse;
	private PersonView guardian;
	private String testingId;
	
	private List<DomainDataView> fileTypeListDD;
	private List<DomainDataView> tenantTypeListDD;
	private DomainDataView companyDDV;
	private DomainDataView individualDDV;
	private DomainDataView deceasedDDV;
	private DomainDataView underGuardianshipDDV;
	private DomainDataView legalAssitanceDDV;
	private DomainDataView foundingDDV;
	private DomainDataView absentDDV;
//	private static final String DECEASED = "DECEASED";
//	private static final String UNDER_GUARDIANSHIP = "UNDER_GUARDIANSHIP";
//	private static final String LEGAL_ASSISTANCE = "LEGAL_ASSISTANCE";
//	private static final String FOUNDING = "FOUNDING";
//	private static final String ABSENT = "ABSENT";
	private static final String INDEX_TO_EDIT = "INDEX_TO_EDIT";
	private static final String EXISTING_BENEFICIARY_ID = "EXISTING_BENEFICIARY_ID";
	
	private boolean isDeceased;
	private boolean isFounding;
	private boolean isLegalAsstnc;
	private boolean isAbsent;
	private boolean isUnderGuradian;
	private HtmlDataTable beneficiaryDetailsDataTable ;
	private List<BeneficiaryDetailsView> beneficiaryDetailsList;
//	private List<BeneficiaryGuardianNurseView> beneficiaryGuardianNurseList = new ArrayList<BeneficiaryGuardianNurseView>();
	private HashMap<Long,Set<RegionView>> countryToCity; 
	private StatementOfAccountCriteria reportCriteria;
	private PropertyServiceAgent psa;
	
	public void init() {
		super.init();
		if (!isPostBack()) 
		{
			putDomainDataInViewMap();
		}
	}
	@SuppressWarnings("unchecked")
	public void afterUpdateModelValues()
    {
    }

	public void renderFilePersonFieldLabel() {
		if (viewMap.get(WebConstants.InheritanceFile.FILE_TYPE_ID) != null) {
			Long selectedFileTypeId = -1L;
			selectedFileTypeId = Long.parseLong(viewMap.get(
					WebConstants.InheritanceFile.FILE_TYPE_ID).toString());
			if (selectedFileTypeId.compareTo(-1L) != 0) {
				if (viewMap.get(WebConstants.InheritanceFileType.INH_FILE_TYPE) != null) {
					checkSelectedFileTypeId(selectedFileTypeId);
				}
			}
		}
	}

	private void putDomainDataInViewMap() {

	}

	public InheritanceBeneficiaryTabBean() {
		psa = new PropertyServiceAgent();
		countryToCity = new HashMap<Long, Set<RegionView>>();
		txtFileNumber = new HtmlInputText();
		txtFilePerson = new HtmlInputText();
		txtSponser = new HtmlInputText();
		cboFileType = new HtmlSelectOneMenu();
		clndrMissingFrom = new HtmlCalendar();
		clndrCreatedOn = new HtmlCalendar();
		cityList = new ArrayList<SelectItem>();
		benenficiaryPassportCityList = new ArrayList<SelectItem>();
		cityListSponsor = new ArrayList<SelectItem>();
		
		filePerson = new PersonView();
		beneficiary = new PersonView();
		nurse = new PersonView();
		guardian = new PersonView();
		sponsor = new PersonView();
		fileTypeListDD = new ArrayList<DomainDataView>();
		tenantTypeListDD = new ArrayList<DomainDataView>();
		companyDDV = new DomainDataView();
		individualDDV = new DomainDataView();
		deceasedDDV = new DomainDataView();
		underGuardianshipDDV = new DomainDataView();
		legalAssitanceDDV = new DomainDataView();
		foundingDDV = new DomainDataView();
		absentDDV = new DomainDataView();
		beneficiaryDetailsList = new ArrayList<BeneficiaryDetailsView>();
		reportCriteria= new StatementOfAccountCriteria(ReportConstant.Report.STATEMENT_OF_ACCOUNT,ReportConstant.Processor.STATEMENT_OF_ACCOUNT,CommonUtil	.getLoggedInUser());
	}

	public HtmlInputText getTxtFileNumber() {
		return txtFileNumber;
	}

	public void setTxtFileNumber(HtmlInputText txtFileNumber) {
		this.txtFileNumber = txtFileNumber;
	}

	public HtmlInputText getTxtFilePerson() {
		return txtFilePerson;
	}

	public void setTxtFilePerson(HtmlInputText txtFilePerson) {
		this.txtFilePerson = txtFilePerson;
	}

	public HtmlInputText getTxtSponser() {
		return txtSponser;
	}

	public void setTxtSponser(HtmlInputText txtSponser) {
		this.txtSponser = txtSponser;
	}

	public HtmlSelectOneMenu getCboFileType() {
		return cboFileType;
	}

	public void setCboFileType(HtmlSelectOneMenu cboFileType) {
		this.cboFileType = cboFileType;
	}

	public HtmlCalendar getClndrMissingFrom() {
		return clndrMissingFrom;
	}

	public void setClndrMissingFrom(HtmlCalendar clndrMissingFrom) {
		this.clndrMissingFrom = clndrMissingFrom;
	}

	public HtmlCalendar getClndrCreatedOn() {
		return clndrCreatedOn;
	}

	public void setClndrCreatedOn(HtmlCalendar clndrCreatedOn) {
		this.clndrCreatedOn = clndrCreatedOn;
	}

	@SuppressWarnings("unchecked")
	public void showControls() {
		viewMap.put("IS_VISIBLE", true);
	}

	public boolean isVisible() {
		if (viewMap.get("IS_VISIBLE") != null)
			isVisible = (Boolean) viewMap.get("IS_VISIBLE");
		else
			isVisible = false;
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

	@SuppressWarnings("unchecked")
	public void loadCity(Long selectedCoutryId, String personType) {
		final String methodName = "loadCity";
		
		Set<RegionView> regionViewList = null;
		
		List<SelectItem> cmbCityList = new ArrayList<SelectItem>(0);
		try {

			if (selectedCoutryId != null && selectedCoutryId.compareTo(new Long(-1)) != 0) 
			{
				// First check in map then go to db
				if (viewMap.get("COUNTRY_TO_CITY_MAP") != null
						&& ((HashMap<Long, Set<RegionView>>) viewMap.get("COUNTRY_TO_CITY_MAP")).containsKey(selectedCoutryId)) {

					regionViewList = ((HashMap<Long, Set<RegionView>>) viewMap.get("COUNTRY_TO_CITY_MAP")).get(selectedCoutryId);
					if(regionViewList != null && regionViewList.size() > 0){
						cmbCityList = getCityDescFromRegion(regionViewList);
					}
				}
				// DB access block
				else {
					logger.logInfo(methodName + "|" + "Start");
					regionViewList = psa.getCity(selectedCoutryId);

					if(regionViewList != null && regionViewList.size() > 0){
						// add to map
						if (viewMap.get("COUNTRY_TO_CITY_MAP") != null) {
							countryToCity = (HashMap<Long, Set<RegionView>>) viewMap.get("COUNTRY_TO_CITY_MAP");
							countryToCity.put(selectedCoutryId, regionViewList);
							viewMap.put("COUNTRY_TO_CITY_MAP", countryToCity);

						} else {
							countryToCity.put(selectedCoutryId, regionViewList);
							viewMap.put("COUNTRY_TO_CITY_MAP", countryToCity);
						}
						cmbCityList = getCityDescFromRegion(regionViewList);
					}
				}
			if(personType.compareToIgnoreCase(WebConstants.InheritanceFile.PERSON_TYPE_BENEFICIARY) == 0 && cmbCityList != null )
				viewMap.put(WebConstants.InheritanceFile.CITY_LIST_BENEFICIARY, cmbCityList);
			else if(personType.compareToIgnoreCase("PassportIssuePlace") == 0 && cmbCityList != null )
				setBenenficiaryPassportCityList(cmbCityList);
			
			//			else
//				if(personType.compareToIgnoreCase(WebConstants.InheritanceFile.PERSON_TYPE_INHER_FILE_SPONSOR) == 0)
//					viewMap.put(WebConstants.InheritanceFile.CITY_LIST_SPONSOR, cmbCityList);
			logger.logInfo(methodName + "|" + "Finish");
		}
		} catch (Exception ex) {
			logger.LogException(methodName + "|Error Occured ", ex);

		}

	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getCityList() {
		if (viewMap.get(WebConstants.InheritanceFile.CITY_LIST_BENEFICIARY) != null)
			cityList = (List<SelectItem>) viewMap
					.get(WebConstants.InheritanceFile.CITY_LIST_BENEFICIARY);
		return cityList;
	}

	public void setCityList(List<SelectItem> cityList) {
		this.cityList = cityList;
	}


	public PersonView getFilePerson() {
		if (viewMap
				.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW) != null)
			filePerson = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW);
		if (filePerson.getCountryId() != null)

		{
			loadCity(Long.parseLong(filePerson.getCountryId()),WebConstants.InheritanceFile.PERSON_TYPE_FILE_PERSON);
			loadCity(Long.parseLong(filePerson.getPassportIssuePlaceIdString()),"PassportIssuePlace");
			
		}
		return filePerson;
	}

	@SuppressWarnings("unchecked")
	public void setFilePerson(PersonView filePerson) {
		if (filePerson != null && filePerson.getPersonId() != null)
			viewMap.put(
					WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW,
					filePerson);
		this.filePerson = filePerson;
	}

	public String getTestingId() {
		return testingId;
	}

	public void setTestingId(String testingId) {
		this.testingId = testingId;
	}

	@SuppressWarnings("unchecked")
	public void prerender() {
		super.prerender();
		getCityList();
		getBenenficiaryPassportCityList();
		
	}

	@SuppressWarnings("unchecked")
	private void checkSelectedFileTypeId(Long selectedFileTypeId) {
		getEachFileTypeFromViewMap();
		if (selectedFileTypeId.compareTo(absentDDV.getDomainDataId()) == 0) {
			viewMap
					.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
							WebConstants.InheritanceFileType.ABSENT);
		} else if (selectedFileTypeId.compareTo(foundingDDV.getDomainDataId()) == 0) {
			viewMap.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
					WebConstants.InheritanceFileType.FOUNDING);
		} else if (selectedFileTypeId.compareTo(underGuardianshipDDV
				.getDomainDataId()) == 0) {
			viewMap.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
					WebConstants.InheritanceFileType.UNDER_GUARDIANSHIP);
		} else if (selectedFileTypeId.compareTo(legalAssitanceDDV
				.getDomainDataId()) == 0) {
			viewMap.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
					WebConstants.InheritanceFileType.LEGAL_ASSISTANCE);
		} else if (selectedFileTypeId.compareTo(deceasedDDV.getDomainDataId()) == 0) {
			viewMap.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
					WebConstants.InheritanceFileType.DECEASED);
		}

	}

	private void getEachFileTypeFromViewMap() {
		if (viewMap.get(WebConstants.InheritanceFileType.ABSENT) != null) {
			absentDDV = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileType.ABSENT);
		}

		if (viewMap.get(WebConstants.InheritanceFileType.FOUNDING) != null) {
			foundingDDV = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileType.FOUNDING);
		}

		if (viewMap.get(WebConstants.InheritanceFileType.LEGAL_ASSISTANCE) != null) {
			legalAssitanceDDV = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileType.LEGAL_ASSISTANCE);
		}

		if (viewMap.get(WebConstants.InheritanceFileType.DECEASED) != null) {
			deceasedDDV = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileType.DECEASED);
		}

		if (viewMap.get(WebConstants.InheritanceFileType.UNDER_GUARDIANSHIP) != null) {
			underGuardianshipDDV = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileType.UNDER_GUARDIANSHIP);
		}
	}

	public List<DomainDataView> getFileTypeListDD() {
		return fileTypeListDD;
	}

	public void setFileTypeListDD(List<DomainDataView> fileTypeListDD) {
		this.fileTypeListDD = fileTypeListDD;
	}

	public List<DomainDataView> getTenantTypeListDD() {
		return tenantTypeListDD;
	}

	public void setTenantTypeListDD(List<DomainDataView> tenantTypeListDD) {
		this.tenantTypeListDD = tenantTypeListDD;
	}

	public DomainDataView getCompanyDDV() {
		return companyDDV;
	}

	public void setCompanyDDV(DomainDataView companyDDV) {
		this.companyDDV = companyDDV;
	}

	public DomainDataView getIndividualDDV() {
		return individualDDV;
	}

	public void setIndividualDDV(DomainDataView individualDDV) {
		this.individualDDV = individualDDV;
	}

	public DomainDataView getDeceasedDDV() {
		return deceasedDDV;
	}

	public void setDeceasedDDV(DomainDataView deceasedDDV) {
		this.deceasedDDV = deceasedDDV;
	}

	public DomainDataView getUnderGuardianshipDDV() {
		return underGuardianshipDDV;
	}

	public void setUnderGuardianshipDDV(DomainDataView underGuardianshipDDV) {
		this.underGuardianshipDDV = underGuardianshipDDV;
	}

	public DomainDataView getLegalAssitanceDDV() {
		return legalAssitanceDDV;
	}

	public void setLegalAssitanceDDV(DomainDataView legalAssitanceDDV) {
		this.legalAssitanceDDV = legalAssitanceDDV;
	}

	public DomainDataView getFoundingDDV() {
		return foundingDDV;
	}

	public void setFoundingDDV(DomainDataView foundingDDV) {
		this.foundingDDV = foundingDDV;
	}

	public DomainDataView getAbsentDDV() {
		return absentDDV;
	}

	public void setAbsentDDV(DomainDataView absentDDV) {
		this.absentDDV = absentDDV;
	}

	public boolean isDeceased() {
		isDeceased = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) {
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null && StringHelper.isNotEmpty(fileType)
					&& fileType.compareTo(WebConstants.InheritanceFileType.DECEASED) == 0)
				isDeceased = true;
		}
		return isDeceased;
	}

	public void setDeceased(boolean isDeceased) {
		this.isDeceased = isDeceased;
	}

	public boolean isFounding() {
		isFounding = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) {
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null && StringHelper.isNotEmpty(fileType)
					&& fileType.compareTo(WebConstants.InheritanceFileType.FOUNDING) == 0)
				isFounding = true;
		}
		return isFounding;
	}

	public void setFounding(boolean isFounding) {
		this.isFounding = isFounding;
	}

	public boolean isLegalAsstnc() {
		isLegalAsstnc = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) {
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null && StringHelper.isNotEmpty(fileType)
					&& fileType.compareTo(WebConstants.InheritanceFileType.LEGAL_ASSISTANCE) == 0)
				isLegalAsstnc = true;
		}
		return isLegalAsstnc;
	}

	public void setLegalAsstnc(boolean isLegalAsstnc) {
		this.isLegalAsstnc = isLegalAsstnc;
	}

	public boolean isAbsent() {
		isAbsent = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) {
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null && StringHelper.isNotEmpty(fileType)
					&& fileType.compareTo(WebConstants.InheritanceFileType.ABSENT) == 0)
				isAbsent = true;
		}
		return isAbsent;
	}

	public void setAbsent(boolean isAbsent) {
		this.isAbsent = isAbsent;
	}

	public boolean isUnderGuradian() {
		isUnderGuradian = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) 
		{
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null && StringHelper.isNotEmpty(fileType)
					&& fileType.compareTo(WebConstants.InheritanceFileType.UNDER_GUARDIANSHIP) == 0)
				isUnderGuradian = true;
		}
		return isUnderGuradian;
	}

	public void setUnderGuradian(boolean isUnderGuradian) {
		this.isUnderGuradian = isUnderGuradian;
	}

	public PersonView getSponsor() 
	{
		sponsor = new PersonView();
		if (viewMap
				.get(WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW) != null)
			sponsor = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW);
//		if (sponsor != null && sponsor.getPersonId() != null && sponsor.getCountryId() != null)
//
//		{
//			loadCity(Long.parseLong(sponsor.getCountryId()),WebConstants.InheritanceFile.PERSON_TYPE_INHER_FILE_SPONSOR);
//		}
		return sponsor;
	}

	@SuppressWarnings("unchecked")
	public void setSponsor(PersonView sponsor) {
		if(sponsor != null && sponsor.getPersonId() != null)
			viewMap.put(WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW, sponsor);
		this.sponsor = sponsor;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getCityListSponsor() 
	{
		if (viewMap.get(WebConstants.InheritanceFile.CITY_LIST_SPONSOR) != null)
			cityListSponsor = (List<SelectItem>) viewMap
					.get(WebConstants.InheritanceFile.CITY_LIST_SPONSOR);
		return cityListSponsor;
	}

	public void setCityListSponsor(List<SelectItem> cityListSponsor) {
		this.cityListSponsor = cityListSponsor;
	}

	public PersonView getBeneficiary() 
	{
		if (viewMap	.get(WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW) != null)
		{
			beneficiary = (PersonView) viewMap.get(WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW);
		}
		if (beneficiary != null  )
		{
			
			if ( beneficiary.getPassportName()==null || beneficiary.getPassportName().trim().equals("") )
			{
				beneficiary.setPassportName(beneficiary.getPersonFullName());
			}
			setBeneficiary(beneficiary);
		}
		return beneficiary;
	}

	@SuppressWarnings("unchecked")
	public void setBeneficiary(PersonView beneficiary) 
	{
		if(beneficiary != null)
			viewMap.put(WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW, beneficiary);
		this.beneficiary = beneficiary;
	}

	public PersonView getNurse() {
		if (viewMap
				.get(WebConstants.InheritanceFile.INHERITANCE_NUSRSE_VIEW) != null)
			nurse = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_NUSRSE_VIEW);
		if(nurse != null )
		{
			setNurse(nurse);
		}
		return nurse;
	}

	@SuppressWarnings("unchecked")
	public void setNurse(PersonView nurse) 
	{
		if(nurse != null )
			viewMap.put(WebConstants.InheritanceFile.INHERITANCE_NUSRSE_VIEW, nurse);
		this.nurse = nurse;
	}

	public PersonView getGuardian() 
	{
		if (viewMap
				.get(WebConstants.InheritanceFile.INHERITANCE_GUARDIAN_VIEW) != null)
			guardian = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_GUARDIAN_VIEW);
		if(guardian != null )
		{
			setGuardian(guardian);
		}
		return guardian;
	}

	@SuppressWarnings("unchecked")
	public void setGuardian(PersonView guardian) 
	{
		if(guardian != null )
			viewMap.put(WebConstants.InheritanceFile.INHERITANCE_GUARDIAN_VIEW, guardian);
		this.guardian = guardian;
	}

	@SuppressWarnings("unchecked")
	public List<BeneficiaryDetailsView> getBeneficiaryDetailsList() 
	{
		if(viewMap.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST) != null)
			beneficiaryDetailsList = (List<BeneficiaryDetailsView>) viewMap.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST);
		return beneficiaryDetailsList;
	}

	@SuppressWarnings("unchecked")
	public void setBeneficiaryDetailsList(	List<BeneficiaryDetailsView> beneficiaryDetailsList) 
	{
		if(beneficiaryDetailsList != null){
			Collections.sort(beneficiaryDetailsList, new ListComparator("beneficiaryName",true));
//			Collections.sort(beneficiaryDetailsList,ListComparator.PERSON_NAME_COMPARATOR);
			viewMap.put(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST,beneficiaryDetailsList);
		}
		this.beneficiaryDetailsList = beneficiaryDetailsList;
	}

	public HtmlDataTable getBeneficiaryDetailsDataTable() {
		return beneficiaryDetailsDataTable;
	}

	public void setBeneficiaryDetailsDataTable(
			HtmlDataTable beneficiaryDetailsDataTable) {
		this.beneficiaryDetailsDataTable = beneficiaryDetailsDataTable;
	}
	@SuppressWarnings("unchecked")
	public void editBeneficiaryDetails()
	{

		errorMessages = new ArrayList<String>(0);
		try 
		{
				BeneficiaryDetailsView selectedDetail = null;
				selectedDetail = (BeneficiaryDetailsView) beneficiaryDetailsDataTable.getRowData();
				
				if(viewMap.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST) != null)
					beneficiaryDetailsList = (List<BeneficiaryDetailsView>) viewMap.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST);
				
				for(BeneficiaryDetailsView details : beneficiaryDetailsList)
				{
					details.setSelected(false);
				}
				selectedDetail.setSelected(true);
				
				int indexToEdit = beneficiaryDetailsDataTable.getRowIndex();
				if(selectedDetail.getBeneficiaryGuardianId() != null){
					viewMap.put("SELECTED_GUARDIAN", selectedDetail.getBeneficiaryGuardianId());
				}
				
				if( selectedDetail.getSharePercentage() != null && selectedDetail.getSharePercentage().length() > 0 )
				viewMap.put("SELECTED_BENFICIARY_SHARE", selectedDetail.getSharePercentage());
				viewMap.put(INDEX_TO_EDIT, indexToEdit);
				
				if(selectedDetail.getInheritanceBeneficiaryId() != null)
					viewMap.put(EXISTING_BENEFICIARY_ID, selectedDetail.getInheritanceBeneficiaryId());
				
				if(selectedDetail != null)
				{
					if(selectedDetail.getBeneficiary() != null && selectedDetail.getBeneficiary().getPersonId() != null)
					{
						
						PersonView beneficiary = null;
						beneficiary = selectedDetail.getBeneficiary();
						beneficiary.setCostCenter( selectedDetail.getCostCenter() );
						if(selectedDetail.getRelationshipId() != null )//Optional in case if file person is beneficiary himself/herself
						{
							beneficiary.setRelationshipString(selectedDetail.getRelationshipId().toString());
							beneficiary.setRelationshipDesc(selectedDetail.getRelationshipDesc());
						}
						if( selectedDetail.getSharePercentage() != null && selectedDetail.getSharePercentage().length() >0 )
						beneficiary.setSharePercentage(selectedDetail.getSharePercentage());
						if( selectedDetail.getIsApplicant() != null && selectedDetail.getIsApplicant().equals("1") )
						{
							beneficiary.setApplicant(true);
						}
						else
						{
							beneficiary.setApplicant(false);
						}
						viewMap.put(WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW, beneficiary );
						
					}
						
					
					if(selectedDetail.getGuardianId()!= null && selectedDetail.getGuardianName() != null)
					{
						PersonView guardian = new PersonView();
						guardian.setPersonId(selectedDetail.getGuardianId());
						guardian.setPersonFullName(selectedDetail.getGuardianName());
						guardian.setRelationshipString(selectedDetail.getRelationshipStringGuardian());
						guardian.setRelationshipDesc(selectedDetail.getRelationshipDescGuardian());
						viewMap.put(WebConstants.InheritanceFile.INHERITANCE_GUARDIAN_VIEW, guardian);
						
					}
					
					if(selectedDetail.getNurseId()!= null && selectedDetail.getNurseName() != null)
					{
						PersonView nurse = new PersonView();
						nurse.setPersonId(selectedDetail.getNurseId());
						nurse.setPersonFullName(selectedDetail.getNurseName());
						nurse.setRelationshipString(selectedDetail.getRelationshipStringNurse());
						nurse.setRelationshipDesc(selectedDetail.getRelationshipDescNurse());
						viewMap.put(WebConstants.InheritanceFile.INHERITANCE_NUSRSE_VIEW, nurse);
					}
					countryChanged();
					passportCountryChanged();
				}
		} 
		catch (Exception exception) 
		{			
			logger.LogException( " editBeneficiaryDetails--- CRASHED --- ", exception);
	        errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
			viewMap.put(WebConstants.InherFileErrMsg.BENEFICIARY_ERR, errorMessages);
			
		}	
		
	}

	@SuppressWarnings("unchecked")
	public void addBeneficiaryDetails()
	{
		errorMessages = new ArrayList<String>(0);
		try 
		{
			if(isDetailsValidated())
			{
				if( getBeneficiaryDetailsList() != null)
				{
					beneficiaryDetailsList = getBeneficiaryDetailsList();
					//this beneficiary id and index will be set while editing details.
					if(viewMap.get(EXISTING_BENEFICIARY_ID) != null && viewMap.get(INDEX_TO_EDIT) != null)
					{
						//index to remove from grid, so we can add update done on the place
						int index = Integer.parseInt(viewMap.remove(INDEX_TO_EDIT).toString()); 
						beneficiaryDetailsList.remove(index);//remove old beneficiary details from grid
						
						viewMap.remove("SELECTED_BENFICIARY_SHARE");
						Long id= (Long) viewMap.get(EXISTING_BENEFICIARY_ID);//get existed inherBeneficiaryId 
						BeneficiaryDetailsView details = getBeneficiaryDetails();//compose view to show the details in grid
						details.setInheritanceBeneficiaryId(id);
						viewMap.remove(EXISTING_BENEFICIARY_ID);
						details.setSelected(false);
						beneficiaryDetailsList.add(details);//add update details to grid
						setBeneficiaryDetailsList(beneficiaryDetailsList);
						clearBeneficiaryInputFields(); //clear all fields except Nurse and Guardian
						successMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.successMsg.benDtlupdated"));
					}
					
					//in case: When beneficiary has not saved in database yet, and user if user is playing with grid
					else if(viewMap.get(INDEX_TO_EDIT) != null)
					{
						int index = Integer.parseInt(viewMap.get(INDEX_TO_EDIT).toString()); 
						beneficiaryDetailsList.remove(index);
						viewMap.remove(INDEX_TO_EDIT);
						viewMap.remove("SELECTED_BENFICIARY_SHARE");
						BeneficiaryDetailsView details = getBeneficiaryDetails();
						details.setSelected(false);
						beneficiaryDetailsList.add(details);
						setBeneficiaryDetailsList(beneficiaryDetailsList);
						clearBeneficiaryInputFields();
					}
					//in case: New Beneficiary is going to be added in existing list
					else
					{
						BeneficiaryDetailsView details = getBeneficiaryDetails();
						details.setSelected(false);
						beneficiaryDetailsList.add(details);
						setBeneficiaryDetailsList(beneficiaryDetailsList);
						clearBeneficiaryInputFields();
					}
				}
				else //if record is being added first time
				{
					BeneficiaryDetailsView details = getBeneficiaryDetails();
					details.setSelected(false);
					beneficiaryDetailsList.add(details);
					setBeneficiaryDetailsList(beneficiaryDetailsList);
					clearBeneficiaryInputFields();
				}
			}
			if(errorMessages != null && errorMessages.size() > 0)
				viewMap.put(WebConstants.InherFileErrMsg.BENEFICIARY_ERR, errorMessages);
		} 
		catch (Exception exception) 
		{			
			logger.LogException( " addBeneficiaryDetails--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
	        errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
			viewMap.put(WebConstants.InherFileErrMsg.BENEFICIARY_ERR, errorMessages);
			
		}
		finally{
			executeJavascript("javaScript:onProcessComplete();");
		}

			
	}
//	private Double getTotalSharePercentage(List<BeneficiaryDetailsView> beneDtlList) 
//	{
//		Double totalShare = 0D;
//		for(BeneficiaryDetailsView details : beneDtlList)
//		{
//			if(!details.isDeleted())
//			totalShare += new Double(details.getSharePercentage());
//		}
//		return totalShare;
//	}
	private Double getTotalBeneficiariesShares(List<BeneficiaryDetailsView> beneDtlList) 
	{
		Double totalShare = 0D;
		for(BeneficiaryDetailsView details : beneDtlList)
		{
			if(!details.isDeleted() && details.getSharePercentage()!= null && details.getSharePercentage().length() > 0 )
			totalShare += new Double(details.getSharePercentage());
		}
		return totalShare;
	}

	@SuppressWarnings("unchecked")
	private void clearBeneficiaryInputFields()
	{
		if(getBeneficiary() != null)
		{
			viewMap.put(WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW, new PersonView());
			getBeneficiary();
		}
		
//TODO: uncomment this block of code if user wants to edit the details of nurse and guardian too from same screen		
	if(getNurse() != null)
		{
			viewMap.put(WebConstants.InheritanceFile.INHERITANCE_NUSRSE_VIEW,new PersonView());
			getNurse();
		}
	if(getGuardian() != null)
		{
			viewMap.put(WebConstants.InheritanceFile.INHERITANCE_GUARDIAN_VIEW,new PersonView());
			getGuardian();
		}
	}
	private BeneficiaryDetailsView getBeneficiaryDetails()  throws Exception
	{
			BeneficiaryDetailsView beneficiaryDetails = new BeneficiaryDetailsView();
			
			if(viewMap.get("REMOVED_BENEFICIARY_ID") != null)
			{
				beneficiaryDetails.setInheritanceBeneficiaryId(new Long(viewMap.get("REMOVED_BENEFICIARY_ID").toString()));
				viewMap.remove("REMOVED_BENEFICIARY_ID");
			}
			beneficiary = getBeneficiary();
			guardian = getGuardian();
			nurse = getNurse();
			beneficiary.setUpdatedBy(CommonUtil.getLoggedInUser());
			beneficiary.setUpdatedOn(new Date());
			beneficiary.setUpdatedByCI(CommonUtil.getLoggedInUser());
			beneficiary.setUpdatedOnCI(new Date());
			
			beneficiaryDetails.setBeneficiary(beneficiary);
			beneficiaryDetails.setBeneficiaryName(beneficiary.getPersonFullName());//To view in Grid

			beneficiaryDetails.setCostCenter( beneficiary.getCostCenter());
			beneficiaryDetails.setOldNewCostCenter(CommonUtil.CombineCostCenter(beneficiaryDetails.getOldCostCenter(),beneficiaryDetails.getCostCenter()));
			if(nurse != null && nurse.getPersonId() != null)
			{
				beneficiaryDetails.setNurseId(nurse.getPersonId());//we are setting here only id and relationship information
				beneficiaryDetails.setNurseName(nurse.getPersonFullName());//To view in Grid
				beneficiaryDetails.setRelationshipStringNurse(nurse.getRelationshipString());
				beneficiaryDetails.setRelationshipDescNurse(nurse.getRelationshipDesc());
			}
			
			if(guardian != null && guardian.getPersonId() != null)
			{
				beneficiaryDetails.setGuardianId(guardian.getPersonId());//we are setting here only id and relationship information
				beneficiaryDetails.setGuardianName(guardian.getPersonFullName());//To view in Grid
				//if there will be mulitiple guardian against any beneficiary in future, then prepare a list of guatdian against beneficiary
				//to post it to tabs controller bean
				beneficiaryDetails.setRelationshipStringGuardian(guardian.getRelationshipString());
				beneficiaryDetails.setRelationshipDescGuardian(guardian.getRelationshipDesc());
			}
			else{
				beneficiaryDetails.setGuardianId(null);
				if(viewMap.get("SELECTED_GUARDIAN") != null){
					beneficiaryDetails.setGuardianToBeDeleted(Long.parseLong(viewMap.get("SELECTED_GUARDIAN").toString()));
					viewMap.remove("SELECTED_GUARDIAN");
				}
				beneficiaryDetails.setBeneficiaryGuardianId(null);
			}
			beneficiaryDetails.setMinor(beneficiary.isMinor());
			if(  beneficiary.isApplicant() )
			{
				beneficiaryDetails.setIsApplicant("1");
			}
			if(beneficiary.isMinor())
				beneficiaryDetails.setMinorString(CommonUtil.getBundleMessage("commons.yes"));
			else
				beneficiaryDetails.setMinorString(CommonUtil.getBundleMessage("commons.no"));
			
			RelationshipView beneficiaryRelationship = null;
			if(beneficiary.getRelationshipString() != null && beneficiary.getRelationshipString().compareTo("-1") != 0)
			{
				beneficiaryRelationship = new InheritanceFileService().getRelationshipById(Long.parseLong(beneficiary.getRelationshipString()));
				beneficiaryDetails.setRelationshipString(CommonUtil.getIsEnglishLocale()?beneficiaryRelationship.getRelationshipDescEn():beneficiaryRelationship.getRelationshipDescAr());
				beneficiaryDetails.setRelationshipId(Long.parseLong(beneficiary.getRelationshipString()));
				beneficiaryDetails.setRelationshipDesc(beneficiary.getRelationshipDesc());
			}
			
			beneficiaryDetails.setSharePercentage(beneficiary.getSharePercentage());
		return beneficiaryDetails;
	}

	@SuppressWarnings("unchecked")
	private boolean isDetailsValidated() throws Exception
	{
		boolean validated = true;
		InheritanceFileBean fileBean = (InheritanceFileBean) getBean("pages$inheritanceFile");
		boolean isMigratedFile = fileBean.getIsMigratedFile();
		boolean relationShipNotSelected =true;
		//Check if beneficiary available
		if(getBeneficiary() == null || getBeneficiary().getPersonId() == null)
		{
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.beneReq"));
			validated = false;
		}
		//If Beneficiary is available
		else 
		{
			beneficiary = getBeneficiary();
			//Validate Beneficiary
			if( !fileBean.isPersonInfoValidated(beneficiary, WebConstants.InheritanceFilePerson.BENEFICIARY) )
			{
				//errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.beneDtlReq"));
				validated = false;
				return validated;
			}
			relationShipNotSelected = (StringHelper.isEmpty(beneficiary.getRelationshipString()) || 
            beneficiary.getRelationshipString().compareTo("-1") == 0 ); 
			//If file is not migrated && Relationship is required(in Case of Absent/Deceased) then check its availability else don't check
			if( !isMigratedFile && fileBean.isRelationShipRequired()  && relationShipNotSelected )
			  
			{
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.benRelationShipRequired"));
				validated = false;
			}
			//if relationship is required and it has been selected and its others the desc is required
			if( fileBean.isRelationShipRequired() && !relationShipNotSelected &&  
				beneficiary.getRelationshipString().compareTo(WebConstants.RelationshipTypeIds.Others.toString()) == 0)
			{
				if(StringHelper.isEmpty(beneficiary.getRelationshipDesc()))
				{
				 errorMessages.add(CommonUtil.getBundleMessage("errMsg.beneRelDescReq"));
				 validated = false;
				}
			}
			
			//share validation
			if(getBeneficiaryDetailsList() != null)
			{
				beneficiaryDetailsList = getBeneficiaryDetailsList();
				double totalBeneShares, currentBeneShare = 0;
				
				totalBeneShares = getTotalBeneficiariesShares(beneficiaryDetailsList);
				
				//Old share of Beneficiary
				if(viewMap.get("SELECTED_BENFICIARY_SHARE") != null)
				{
					double oldShare = Double.parseDouble(viewMap.get("SELECTED_BENFICIARY_SHARE").toString());
					totalBeneShares -= oldShare;
				}
				if (StringHelper.isNotEmpty(beneficiary.getSharePercentage()))
				{
				//Share of beneficiary to add/update
				currentBeneShare = Double.parseDouble(beneficiary.getSharePercentage());
				totalBeneShares += currentBeneShare;
				}
				if(totalBeneShares > fileBean.getFileShareDbl())
				{
					errorMessages.add(CommonUtil.getBundleMessage("inheritanceFile.errMsg.saheTotalMustBeLEToTotalShare"));
					validated = false;
				}
				
			}
			//check if account number 
			if(StringHelper.isNotEmpty(beneficiary.getAccountNumber()) && beneficiary.getBankId().compareTo("-1") == 0){
				errorMessages.add(CommonUtil.getBundleMessage("addMissingMigratedPayments.errMsg.selectBank"));
				validated = false;
			}
		}
		if(getNurse() != null && getNurse().getPersonId() != null)
		{
			if(! fileBean.isPersonInfoValidated(getNurse(), WebConstants.InheritanceFilePerson.NURSE))
			{
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.nurseDtlReq"));
				validated = false;
			}
			relationShipNotSelected = (StringHelper.isEmpty(getNurse().getRelationshipString()) || getNurse().getRelationshipString().compareTo("-1") == 0);
			if(  !isMigratedFile && relationShipNotSelected )
			{
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.nurseRelationShipRequired"));
				validated = false;
			}
			
			if( !relationShipNotSelected  && 
				getNurse().getRelationshipString().compareTo(WebConstants.RelationshipTypeIds.Others.toString()) == 0
			  )
			{
				if(StringHelper.isEmpty(getNurse().getRelationshipDesc()))
				{
				 errorMessages.add(CommonUtil.getBundleMessage("errMsg.nurseRelDescReq"));
				 validated = false;
				}
			}
			//check if account number 
			if(StringHelper.isNotEmpty(getNurse().getAccountNumber()) && getNurse().getBankId().compareTo("-1") == 0){
				errorMessages.add(CommonUtil.getBundleMessage("inheritanceFileBean.errMsg.nurseBank"));
				validated = false;
			}
		}
		if(getGuardian() != null && getGuardian().getPersonId() != null)
		{
			if(! fileBean.isPersonInfoValidated(getGuardian(), WebConstants.InheritanceFilePerson.GUARDIAN))
			{
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.guardDtlReq"));
				validated = false;
			}
			relationShipNotSelected =(StringHelper.isEmpty(getGuardian().getRelationshipString()) || getGuardian().getRelationshipString().compareTo("-1") == 0);
			if(!isMigratedFile && relationShipNotSelected)
			{
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.guardRelationShipRequired"));
				validated = false;
			}
			if ( !relationShipNotSelected && getGuardian().getRelationshipString().compareTo(WebConstants.RelationshipTypeIds.Others.toString()) == 0)
			{
				if(StringHelper.isEmpty(getGuardian().getRelationshipDesc()))
				{
				 errorMessages.add(CommonUtil.getBundleMessage("errMsg.guardianRelDescReq"));
				 validated = false;
				}
			}
			//check if account number 
			if(StringHelper.isNotEmpty(getGuardian().getAccountNumber()) && getGuardian().getBankId().compareTo("-1") == 0){
				errorMessages.add(CommonUtil.getBundleMessage("inheritanceFileBean.errMsg.guardianBank"));
				validated = false;
			}
		}
		return validated;
	}
	@SuppressWarnings("unchecked")
	public void deleteBeneficiaryDetails()
	{
		errorMessages = new ArrayList<String>(0);
		try 
		{
				
		    BeneficiaryDetailsView selectedDetail = null;
			if(viewMap.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST) != null)
				beneficiaryDetailsList = (List<BeneficiaryDetailsView>) viewMap.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST);

			selectedDetail = (BeneficiaryDetailsView) beneficiaryDetailsDataTable.getRowData();
			if(selectedDetail.getInheritanceBeneficiaryId() != null)
				markSelectedBeneficiaryDetailAsDeleted(selectedDetail,beneficiaryDetailsList);
			else
			{
				int indexToDeleted = beneficiaryDetailsDataTable.getRowIndex();
				beneficiaryDetailsList.remove(indexToDeleted);
				if(beneficiaryDetailsList != null && beneficiaryDetailsList.size() > 0)
					viewMap.put(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST,beneficiaryDetailsList);
				else
					viewMap.remove(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST);
			}
		}
		catch (Exception exception) 
		{			
			logger.LogException( " deleteBeneficiaryDetails--- CRASHED --- ", exception);
	        errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
			viewMap.put(WebConstants.InherFileErrMsg.BENEFICIARY_ERR, errorMessages);
		}	
		
	}
		@SuppressWarnings("unchecked")
	private void markSelectedBeneficiaryDetailAsDeleted(BeneficiaryDetailsView detailToBeDeleted, List<BeneficiaryDetailsView> beneficiaryDetailsList)
	{
		errorMessages = new ArrayList<String>(0);
		try 
		{
			if(beneficiaryDetailsList != null && beneficiaryDetailsList.size() > 0)
			{
				for(BeneficiaryDetailsView existingDetail : beneficiaryDetailsList)
				{
					if(existingDetail.getInheritanceBeneficiaryId() != null)
					{
						if(existingDetail.getInheritanceBeneficiaryId().compareTo(detailToBeDeleted.getInheritanceBeneficiaryId()) == 0)
						{
							existingDetail.setDeleted(true);
							viewMap.put(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST, beneficiaryDetailsList);
							break;
						}
					}
				}
			}
		} 
		catch (Exception exception) 
		{			
			logger.LogException( " markSelectedBeneficiaryDetailAsDeleted--- CRASHED --- ", exception);
	        errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
			viewMap.put(WebConstants.InherFileErrMsg.BENEFICIARY_ERR, errorMessages);
			
		}
	}
	public boolean isReadonlyBeneficiay()
	{
		boolean readOnly = false;
		//readOnly = (isPageModeView() || isLegalAsstnc() || isUnderGuradian() || isFounding() || viewMap.containsKey(WebConstants.InheritanceFilePageMode.IS_POPUP));
		readOnly = (isPageModeView() || isLegalAsstnc() || viewMap.containsKey(WebConstants.InheritanceFilePageMode.IS_POPUP));
		return readOnly ;
	}
	public boolean isPageModeView()
	{
		boolean pageModeView = false;
		if(viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE) != null)
		{
			String pageMode = viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE).toString();
			if(pageMode.equalsIgnoreCase(WebConstants.InheritanceFilePageMode.PAGE_MODE_READ_ONLY))
				pageModeView = true;
			
		}
		return pageModeView;
	}
	public void countryChanged() 
	{
		if (getBeneficiary().getCountryId() != null && !getBeneficiary().getCountryId().equals("-1"))
			loadCity(new Long(getBeneficiary().getCountryId()), WebConstants.InheritanceFile.PERSON_TYPE_BENEFICIARY);
	}
	public void passportCountryChanged() 
	{
		if (getBeneficiary().getPassportIssuePlaceIdString() != null && getBeneficiary().getPassportIssuePlaceIdString().trim().length() > 0 )
			loadCity(Long.parseLong(getBeneficiary().getPassportIssuePlaceIdString()), "PassportIssuePlace");
		 getBenenficiaryPassportCityList();
	}
	@SuppressWarnings("unchecked")
	public void showBeneficiaryPopup()
	{
		final String METHOD_NAME = "showBeneficiaryPopup()";
		errorMessages = new ArrayList<String>(0);
		try 
		{
	
			if(getBeneficiaryDetailsList() != null && getBeneficiaryDetailsList().size() > 0)
			{
				List<Long> ids = new ArrayList<Long>();
				for(BeneficiaryDetailsView dtl : getBeneficiaryDetailsList())
				{
					ids.add(dtl.getBeneficiary().getPersonId());
				}
				sessionMap.put(WebConstants.InheritanceFile.PERSON_IDS_TO_BE_EXCLUDED, ids);
			}
			executeJavascript("showBeneficiaryPopup();");
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
            errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
			viewMap.put(WebConstants.InherFileErrMsg.BENEFICIARY_ERR, errorMessages);
			
		}
	}
	private void executeJavascript(String javascript) 
	{
		final String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void openFinancialAspectsPopup()
	{
		try
		{
			BeneficiaryDetailsView details = (BeneficiaryDetailsView) beneficiaryDetailsDataTable.getRowData();
			if(details.getBeneficiary() != null && details.getBeneficiary().getPersonId() != null)
			{
				sessionMap.put(WebConstants.PERSON_ID, details.getBeneficiary().getPersonId());
				sessionMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_ID,viewMap.get(WebConstants.InheritanceFile.FILE_ID));
				executeJavascript("javaScript:openFinancialAspectsPopup();");
			}
		} catch (Exception ex) {
			logger.LogException( "openFinancialAspectsPopup|Error Occured ", ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
		}
	}
	@SuppressWarnings("unchecked")
	public void openResearcherForm()
	{
		try
		{
			BeneficiaryDetailsView details = (BeneficiaryDetailsView) beneficiaryDetailsDataTable.getRowData();
			if(details.getBeneficiary() != null && details.getBeneficiary().getPersonId() != null)
			{
				sessionMap.put(WebConstants.PERSON_ID, details.getBeneficiary().getPersonId());
				sessionMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_ID,viewMap.get(WebConstants.InheritanceFile.FILE_ID));
				executeJavascript("openResearchForm();");
			}
		} catch (Exception ex) {
			logger.LogException( "openResearcherForm|Error Occured ", ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
		}
		
	}
	public void onAdultFromMinor() 
	{
		errorMessages= new ArrayList<String>(0);
		successMessages= new ArrayList<String>(0);
		try 
		{
			
			BeneficiaryDetailsView details = (BeneficiaryDetailsView) beneficiaryDetailsDataTable.getRowData();
			int indexOf=  beneficiaryDetailsList.indexOf(details );
			beneficiaryDetailsList.remove( indexOf );
				
			CommonUtil.onAdultFromMinor( details.getBeneficiary().getPersonId() , getLoggedInUserId() );
			details.setMinor(false);
			details.getBeneficiary().setMinor(false);
			details.setMinorString(CommonUtil.getBundleMessage("commons.no"));
			beneficiaryDetailsList.add(indexOf, details);
			setBeneficiaryDetailsList( beneficiaryDetailsList );
			successMessages.add(ResourceUtil.getInstance().getProperty("common.person.AdultFromMinor"));
		} 
		catch (Exception exception) 
		{			
			logger.LogException( "onAdultFromMinor --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void onMinorFromAdult() 
	{
		errorMessages= new ArrayList<String>(0);
		successMessages= new ArrayList<String>(0);
		try 
		{
			BeneficiaryDetailsView details = (BeneficiaryDetailsView) beneficiaryDetailsDataTable.getRowData();
			int indexOf=  beneficiaryDetailsList.indexOf(details );
			beneficiaryDetailsList.remove( indexOf );
			CommonUtil.onMinorFromAdult( details.getBeneficiary().getPersonId() , getLoggedInUserId() );
			details.setMinor(true);
			details.getBeneficiary().setMinor(true);
			details.setMinorString(CommonUtil.getBundleMessage("commons.yes"));
			beneficiaryDetailsList.add(indexOf, details);
			setBeneficiaryDetailsList(beneficiaryDetailsList);
			successMessages.add(ResourceUtil.getInstance().getProperty("common.person.MinorFromAdult"));
		} 
		catch (Exception exception) 
		{			
			logger.LogException( "onMinorFromAdult --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void openStatmntOfAccount()
	{
		try
		{
			BeneficiaryDetailsView details = (BeneficiaryDetailsView) beneficiaryDetailsDataTable.getRowData();
			if(details.getBeneficiary() != null && details.getBeneficiary().getPersonId() != null)
			{
				reportCriteria.setPersonId(details.getBeneficiary().getPersonId());
				HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
		    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
				executeJavascript("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
			}
		} catch (Exception ex) {
			logger.LogException( "openStatmntOfAccount|Error Occured ", ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
		}
		
	}
	
	public void openStatmntOfAccountNew()
	{
		try
		{
			BeneficiaryDetailsView details = (BeneficiaryDetailsView) beneficiaryDetailsDataTable.getRowData();
			StatementOfAccountCriteria statmntReportCriteria = new StatementOfAccountCriteria(ReportConstant.Report.STATEMENT_OF_ACCOUNT_NEW,ReportConstant.Processor.STATEMENT_OF_ACCOUNT_NEW,CommonUtil	.getLoggedInUser());;
			if(details.getBeneficiary().getPersonId() != null )
			{
				statmntReportCriteria= new StatementOfAccountCriteria(ReportConstant.Report.STATEMENT_OF_ACCOUNT_NEW,ReportConstant.Processor.STATEMENT_OF_ACCOUNT_NEW,CommonUtil	.getLoggedInUser());
				statmntReportCriteria.setPersonId( new Long ( details.getBeneficiary().getPersonId() ) );
	//			statmntReportCriteria.setInheritancefileId( details. );
				if( details.getCostCenter() != null &&   details.getCostCenter().trim().length()>0)
				{
					statmntReportCriteria.getCostCenters().add( details.getCostCenter() );
				}
				if( details.getOldCostCenter() != null &&   details.getOldCostCenter().trim().length()>0  )
				{
					statmntReportCriteria.getCostCenters().add( details.getOldCostCenter() );
				}
				HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
		    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, statmntReportCriteria);
				executeJavascript("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
			}
		} catch (Exception ex) {
			logger.LogException( "openStatmntOfAccountNew|Error Occured ", ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void openManageBeneficiaryPopUp(javax.faces.event.ActionEvent event) {
    
		try
		{
			BeneficiaryDetailsView gridViewRow = (BeneficiaryDetailsView) beneficiaryDetailsDataTable.getRowData();
			String javaScriptText = " var screen_width = 970;"
					+ "var screen_height = 550;"
					+ "var popup_width = screen_width;"
					+ "var popup_height = screen_height;"
					+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
					+ "window.open('ManageBeneficiary.jsf?pageMode=MODE_SELECT_ONE_POPUP&"
					+ WebConstants.PERSON_ID
					+ "="
					+ gridViewRow.getBeneficiary().getPersonId()
					+ "&CC_NEW"
					+ "="
					+ gridViewRow.getCostCenter()
					+ "&CC_OLD"
					+ "="
					+ gridViewRow.getOldCostCenter()
					+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";
	
			executeJavascript(javaScriptText);
		} catch (Exception ex) {
			logger.LogException( "openManageBeneficiaryPopUp|Error Occured ", ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
		}
	}

	public HashMap<Long, Set<RegionView>> getCountryToCity() {
		return countryToCity;
	}

	public void setCountryToCity(HashMap<Long, Set<RegionView>> countryToCity) {
		this.countryToCity = countryToCity;
	}
	@SuppressWarnings("unchecked")
	private List<SelectItem> getCityDescFromRegion(Set<RegionView> regionViewList) {


		Iterator itrator = regionViewList.iterator();
		List<SelectItem> cmbCityList = new ArrayList<SelectItem>(0);

		for (int i = 0; i < regionViewList.size(); i++) {
			RegionView rV = (RegionView) itrator.next();
			SelectItem item;
			if (CommonUtil.getIsEnglishLocale()) {
				item = new SelectItem(rV.getRegionId().toString(), rV
						.getDescriptionEn());
			} else {
				item = new SelectItem(rV.getRegionId().toString(), rV
						.getDescriptionAr());
			}
			cmbCityList.add(item);
		}
		Collections.sort(cmbCityList, ListComparator.LIST_COMPARE);
		return cmbCityList;
	}
	public void deleteNurseFromBeneDetail(){
		if(viewMap.get(WebConstants.InheritanceFile.INHERITANCE_NUSRSE_VIEW) != null){
			viewMap.remove(WebConstants.InheritanceFile.INHERITANCE_NUSRSE_VIEW);
			nurse = new PersonView();
		}
	}
	@SuppressWarnings("unchecked")
	public void deleteGuardianFromBeneDetail(){
		if(viewMap.get(WebConstants.InheritanceFile.INHERITANCE_GUARDIAN_VIEW) != null){
			viewMap.remove(WebConstants.InheritanceFile.INHERITANCE_GUARDIAN_VIEW);
			guardian = new PersonView();
		}
	}
	public List<SelectItem> getBenenficiaryPassportCityList() {
		if ( viewMap.get("BenenficiaryPassportCityList") != null )
		{
			benenficiaryPassportCityList = (ArrayList<SelectItem>)viewMap.get("BenenficiaryPassportCityList");
		}
		return benenficiaryPassportCityList;
	}
	public void setBenenficiaryPassportCityList(List<SelectItem> benenficiaryPassportCityList) {
		this.benenficiaryPassportCityList = benenficiaryPassportCityList;
		if(this.benenficiaryPassportCityList != null )
		{
			viewMap.put("BenenficiaryPassportCityList",this.benenficiaryPassportCityList); 
		}
	}
}
