package com.avanza.pims.web.mems.endowment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.pims.entity.AmountDetails;
import com.avanza.pims.entity.BoxDistributionHistory;
import com.avanza.pims.entity.DonationBox;
import com.avanza.pims.entity.OpenBox;
import com.avanza.pims.entity.OpenBoxCommittee;
import com.avanza.pims.entity.Request;
import com.avanza.pims.entity.RequestType;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.DonationBoxService;
import com.avanza.pims.ws.mems.EndowmentTransformUtil;
import com.avanza.pims.ws.mems.OpenBoxService;
import com.avanza.pims.ws.vo.DonationBoxView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.ui.util.ResourceUtil;


public class OpenBoxBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(OpenBoxBean.class);
	HttpServletRequest request;
	
	private static final String PAGE_MODE_CANCELLED= "PAGE_MODE_CANCELLED";
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_COMPLETION_REQ = "PAGE_MODE_COMPLETION_REQ";
	private static final String PAGE_MODE_COMPLETED = "PAGE_MODE_COMPLETED";
	
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_COMMITTEE   = "tabCommittee";
    private String pageTitle;
    private String pageMode;

    private OpenBox openBox;
    private List<OpenBoxCommittee> listCommittee= new ArrayList<OpenBoxCommittee>(); 
    private HtmlDataTable dataTableCommittee = new HtmlDataTable();
    private OpenBoxService service = new OpenBoxService();
    private String txtRemarks;

    protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	
    public OpenBoxBean()
    {
    	request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
 			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
				
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.OpenBox.PROCEDURE);
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.OpenBox.PROCEDURE);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.REQUEST);
		viewMap.put("noteowner", WebConstants.REQUEST );
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		loadAttachmentsAndComments( null );
		
		if ( sessionMap.get(WebConstants.SELECTED_ROW)!= null )
		{
		  getDataFromSearch();
		}
		else if ( sessionMap.get(WebConstants.OpenBox.DonationBoxView)!= null )
		{
			
			DonationBoxView donationBoxView = ( DonationBoxView )sessionMap.remove(WebConstants.OpenBox.DonationBoxView);
			setDataForFirstTime();
			DonationBox donationBox = new DonationBox();
			EndowmentTransformUtil.transformToDonationBox(donationBox, donationBoxView);
			openBox.setDonationBox(donationBox);
			
			List<BoxDistributionHistory> listBDH = DonationBoxService.getBoxHistoryByBoxId(donationBox.getBoxId());
			openBox.setBoxDistributionHistory( listBDH.get( 0 ) );
			
			openBox.setEndowmentProgram( donationBox.getEndowmentProgram() );
			
			
		}
		else if( request.getParameter("id") != null)
		{
			getOpenBoxDetails( 
					            new Long(request.getParameter("id").toString())
							 );
		}
		updateValuesFromMap();
		getPageModeFromStatus();
	}
	
	public void prerender(){
		try
		{
		if(sessionMap.get(WebConstants.TEAM_DATA_LIST)!=null)
		{ 
			addMemberFromPopup();
		}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		Request request = new Request();
		
		RequestType requestType = new RequestType();
		requestType.setRequestTypeId( WebConstants.MemsRequestType.OPEN_BOX );
		request.setRequestType( requestType );
		request.setCreatedBy(  getLoggedInUserId()  );
		request.setUpdatedBy(  getLoggedInUserId()  );
		request.setUpdatedOn(new Date());
		request.setCreatedOn( new Date() );
		request.setUpdatedOn( new Date() );
		request.setRequestDate( new Date() );
		request.setIsDeleted(  WebConstants.DEFAULT_IS_DELETED );
		request.setRecordStatus(  WebConstants.DEFAULT_RECORD_STATUS );
		request.setStatusId( WebConstants.REQUEST_STATUS_NEW_ID );
		
		openBox = new OpenBox();
		openBox.setCreatedOn(new Date());
		openBox.setCreatedBy(  getLoggedInUserId()  );
		openBox.setUpdatedBy(  getLoggedInUserId()  );
		openBox.setUpdatedOn(new Date());
		openBox.setCollectedOn( new Date() );
		openBox.setStatusId( WebConstants.OpenBoxStatus.New_Id );
		listCommittee = new ArrayList<OpenBoxCommittee>();
		openBox.setOpenBoxCommittees( new HashSet<OpenBoxCommittee>() );
		openBox.setRequest( request );
	}
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		if( this.openBox== null  || 
			this.openBox.getOpenBoxId()  == null ||
			this.openBox.getStatusId()  == null 
		  ) 
		{ return; }
		if(getStatusNew() && this.openBox != null  && this.openBox.getOpenBoxId()  != null )
		{
			setPageMode( PAGE_MODE_COMPLETION_REQ );
		}
		else if (getStatusCompleted() )
		{
			setPageMode(PAGE_MODE_VIEW);
			
		}
		else if (getStatusCancelled())
		{
			setPageMode(PAGE_MODE_CANCELLED);
		}
	}
	
	public boolean getStatusNew() {
		return this.openBox.getStatusId().compareTo(  WebConstants.OpenBoxStatus.New_Id) == 0;
	}

	public boolean getStatusCompleted() {
		return this.openBox.getStatusId().compareTo(  WebConstants.OpenBoxStatus.Completed_Id) == 0;
	}
	public boolean getStatusCancelled() {
		return this.openBox.getStatusId().compareTo(  WebConstants.OpenBoxStatus.Cancelled_Id) == 0;
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.OpenBox.OpenBox) != null )
		{
			openBox = ( OpenBox )viewMap.get( WebConstants.OpenBox.OpenBox) ;
			openBox.setUpdatedBy(getLoggedInUserId());
		}
		if( viewMap.get( WebConstants.OpenBox.OpenBoxCommittee) != null )
		{
			listCommittee = ( ArrayList<OpenBoxCommittee> )viewMap.get( WebConstants.OpenBox.OpenBoxCommittee) ;
		}
		
		updateValuesToMap();
	}
	@SuppressWarnings("unchecked")
	private void addMemberFromPopup() throws Exception
	{
		   List<UserView> tempReceivingTeam = new ArrayList<UserView>();
		   tempReceivingTeam = (List<UserView>)sessionMap.remove( WebConstants.TEAM_DATA_LIST );
		   for(UserView obj:tempReceivingTeam)
		   {
			      OpenBoxCommittee obc = new OpenBoxCommittee();
				  
			      UserDbImpl udb = new UserDbImpl();
				  udb.setSecondaryFullName( obj.getFullNameSecondary() );
				  udb.setLoginId(obj.getUserName());
				  
				  obc.setMember(udb);
				  obc.setMemberGroup( obj.getGroupNameSecondary() );
				  listCommittee.add(obc);
			}
		   getListCommittee();
		   updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( openBox!= null )
		{
		  viewMap.put(  WebConstants.OpenBox.OpenBox, openBox);
		}
		if( listCommittee != null && listCommittee.size() > 0 )
		{
			viewMap.put( WebConstants.OpenBox.OpenBoxCommittee, listCommittee); 
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		openBox = ( OpenBox )sessionMap.remove(WebConstants.SELECTED_ROW);
		if( this.openBox !=null )
		{
			getOpenBoxDetails( openBox.getOpenBoxId() );
		  
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void getOpenBoxDetails( long id ) throws Exception 
	{
		openBox = service.getOpenBoxById(id);
		openBox.setHoursMinutesFromCollectionDate();
		openBox.setUpdatedOn( new Date() );
		openBox.setUpdatedBy(getLoggedInUserId());
		
		openBox.setAmountCollectedOld(openBox.getAmountCollected());
		listCommittee = new ArrayList<OpenBoxCommittee>();
		if( openBox.getOpenBoxCommittees()!= null )
		{
			listCommittee.addAll( openBox.getOpenBoxCommittees()) ;
			openBox.setOpenBoxCommittees( new HashSet<OpenBoxCommittee>() );
		}
		updateValuesToMap();
		loadAttachmentsAndComments( openBox.getRequest().getRequestId() );
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, WebConstants.Attachment.PIMS_REPOSITORY_ID );		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
     saveComments();
	 saveAttachments(openBox.getRequest().getRequestId().toString());
	 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	  {
		  String notesOwner = WebConstants.REQUEST;
		  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, openBox.getRequest().getRequestId() );
	  }
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
		String notesOwner = WebConstants.REQUEST;
    	if(txtRemarks !=null && this.txtRemarks.length()>0)
    	{
    	  CommonUtil.saveRemarksAsComments(openBox.getRequest().getRequestId(), txtRemarks, notesOwner) ;
    	}
    	NotesController.saveNotes(notesOwner, openBox.getRequest().getRequestId());
    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }

	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromAmountDetailsPopUp()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			sessionMap.remove(WebConstants.OpenBox.OpenBox); 
			if(sessionMap.get(WebConstants.OpenBox.AmountDetails) != null )
			{
				AmountDetails amountDetails = (AmountDetails)sessionMap.remove(WebConstants.OpenBox.AmountDetails);
				Set<AmountDetails> set = new HashSet<AmountDetails>();
				set.add(amountDetails);
				openBox.setAmountDetails( set );
				openBox.setAmountCollected(amountDetails.getNewAmount()); 
			}
			if( openBox.getStatusId().compareTo(WebConstants.OpenBoxStatus.Completed_Id)==0 )
			{
				service.changeAmountDetials(openBox);
				//saveInTransaction(WebConstants.OpenBoxStatus.Completed_Id);
			}
			
			updateValuesToMap();
			
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromAmountDetailsPopUp--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void openAmountDetailsPopup()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			sessionMap.put(WebConstants.OpenBox.OpenBox, openBox);
			if( openBox.getOpenBoxId() == null || 
				openBox.getStatusId().compareTo(WebConstants.OpenBoxStatus.New_Id)==0
			  )
			{
				sessionMap.put(WebConstants.PAGE_MODE, WebConstants.PAGE_MODE_ADD);
				if( openBox.getOpenBoxId() == null && openBox.getAmountDetails() != null && openBox.getAmountDetails().size() > 0 )
				{
					sessionMap.put(WebConstants.OpenBox.AmountDetails, openBox.getAmountDetails().iterator().next());
				}
			}
			else if(openBox.getStatusId() != null && 
					openBox.getStatusId().compareTo(WebConstants.OpenBoxStatus.Completed_Id)==0)
			{
				sessionMap.put(WebConstants.PAGE_MODE, WebConstants.PAGE_MODE_EDIT);
			}
			executeJavaScript("javaScropt:openAmountDetailsPopup();");
			updateValuesToMap();
			
		}
		catch (Exception exception) 
		{
			logger.LogException( "openAmountDetailsPopup --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( openBox!= null && openBox.getOpenBoxId()!= null )
		 {
			 loadAttachmentsAndComments( openBox.getRequest().getRequestId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{
			if( openBox!= null && openBox.getOpenBoxId()!= null )
			{
				RequestHistoryController rhc=new RequestHistoryController();
				rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,openBox.getRequest().getRequestId().toString());
			}
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if( openBox.getCollectedOn() == null )
		{
			errorMessages.add( ResourceUtil.getInstance().getProperty( "openBox.msg.dateRequired" ) );
			hasSaveErrors=true;
		}
		if( openBox.getAmountCollectedStr() == null || openBox.getAmountCollectedStr().trim().length() <= 0 )
		{
			errorMessages.add( ResourceUtil.getInstance().getProperty( "openBox.msg.amountRequired" ) );
			hasSaveErrors=true;
		}
		else 
		{
			try
			{
				new Double(openBox.getAmountCollectedStr());
			}
			catch(Exception e)
			{
				errorMessages.add( ResourceUtil.getInstance().getProperty( "openBox.msg.invalidAmount" ) );
				hasSaveErrors=true;
			}
		}
		if( listCommittee == null || listCommittee.size() <= 0 )
		{
			errorMessages.add( ResourceUtil.getInstance().getProperty( "openBox.msg.committeeRequired" ) );
			tabPanel.setSelectedTab( TAB_COMMITTEE );
			hasSaveErrors=true;
		}
		
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		hasSaveErrors = true;
    	}

		return hasSaveErrors;
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDeleteCommitteeMember()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			OpenBoxCommittee row =(OpenBoxCommittee)dataTableCommittee.getRowData();
			listCommittee.remove(row);
			
			successMessages.add( ResourceUtil.getInstance().getProperty("openBox.msg.committeeMemberRemoved"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onDeleteCommitteeMember--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			saveInTransaction(  WebConstants.OpenBoxStatus.New_Id );
			getOpenBoxDetails( openBox.getOpenBoxId() );
			saveCommentsAttachment( "openBox.event.saved" );
			successMessages.add( ResourceUtil.getInstance().getProperty("openBox.msg.saved"));
			updateValuesToMap();
			getPageModeFromStatus();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onCompleted()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			saveInTransaction(  WebConstants.OpenBoxStatus.Completed_Id);
			getOpenBoxDetails( openBox.getOpenBoxId() );
			saveCommentsAttachment( "openBox.event.completed" );
			successMessages.add( ResourceUtil.getInstance().getProperty("openBox.msg.completed"));
			updateValuesToMap();
			getPageModeFromStatus();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onCompleted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	private void saveInTransaction(Long status ) throws Exception 
	{
		try
		{
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistBox( status );
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistBox(Long status) throws Exception 
	{
		java.text.DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		java.text.DateFormat dfT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		if(status!=null) openBox.setStatusId( status);
		String date = df.format( openBox.getCollectedOn() );
		date += " "+openBox.getHour()+":"+openBox.getMinute() +":00";
		openBox.setCollectedOn( dfT.parse(date) );
		if(listCommittee != null && listCommittee.size() > 0)
		{
			openBox.getOpenBoxCommittees().addAll(listCommittee);
		}
		service.persist( openBox );
	}

	public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("openBox.heading.title"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW )&& 
			 ( getPageMode().equals( PAGE_MODE_NEW)  || getPageMode().equals( PAGE_MODE_COMPLETION_REQ)  )&&  
			openBox != null &&	
			openBox.getDonationBox()!= null && 
			openBox.getDonationBox().getBoxId() !=null 
	      ) 
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowChangeAmount()
	{
		if( getStatusCompleted() && getPageMode().equals( PAGE_MODE_VIEW ) )   
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowAddAmountDetails()
	{
		if( getStatusNew() && !getPageMode().equals( PAGE_MODE_VIEW ) )   
		{
			return true;
		}
		return false;
	}
	public Boolean getShowCompletionButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) &&  
			  getPageMode().equals( PAGE_MODE_COMPLETION_REQ) &&
			  openBox != null &&
			  openBox.getDonationBox()!= null && 
			  openBox.getDonationBox().getBoxId() !=null 
		  )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}



	public List<OpenBoxCommittee> getListCommittee() {
		return listCommittee;
	}

	public void setListCommittee(List<OpenBoxCommittee> listCommittee) {
		this.listCommittee = listCommittee;
	}

	public OpenBox getOpenBox() {
		return openBox;
	}

	public void setOpenBox(OpenBox openBox) {
		this.openBox = openBox;
	}

	public HtmlDataTable getDataTableCommittee() {
		return dataTableCommittee;
	}

	public void setDataTableCommittee(HtmlDataTable dataTableCommittee) {
		this.dataTableCommittee = dataTableCommittee;
	}


}