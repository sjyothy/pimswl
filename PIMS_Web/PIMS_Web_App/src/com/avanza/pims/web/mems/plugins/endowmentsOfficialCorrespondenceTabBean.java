package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.data.ApplicationContext;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.EndFileAsso;
import com.avanza.pims.entity.EndowmentFile;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.endowment.EndowmentFilesService;
import com.avanza.pims.ws.mems.endowment.EndowmentService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.pims.ws.vo.mems.EndowmentFileVO;
import com.avanza.pims.ws.vo.mems.OfficialCorrespondenceView;
import com.avanza.ui.util.ResourceUtil;

public class endowmentsOfficialCorrespondenceTabBean extends AbstractMemsBean 
{
	private static final long serialVersionUID = 1L;
	private EndowmentFilesService endowmentFileService = new EndowmentFilesService(); 
	private List<OfficialCorrespondenceView> officialCorresList;
	private HtmlDataTable officialCorresDataTable;
	private HtmlSelectOneMenu cboGovtDepttId;
	private HtmlSelectOneMenu cboBankId; 
	private HtmlInputTextarea txtArDescription;
	private HtmlInputText txtFilePath;
	private HtmlInputFileUpload fileUploadCtrl;
	private UploadedFile selectedFile;
	private final String  CORRESPONDENCE_LIST;
	private final String  TASK_TYPE;
    DomainDataView offCorresOpen;
    DomainDataView offCorresClose;
    private final String noteOwner ;
	private final String externalId;
	private final String procedureTypeKey ;
	private String seletedEndFileAssoId;
	private List<SelectItem> endowmentsList = new ArrayList<SelectItem>();
	
	public endowmentsOfficialCorrespondenceTabBean() 
	{
		officialCorresList 			= new ArrayList<OfficialCorrespondenceView>();
		cboGovtDepttId 				= new HtmlSelectOneMenu();
		cboBankId 					= new HtmlSelectOneMenu();
		txtArDescription 			= new HtmlInputTextarea();
		txtFilePath 				= new HtmlInputText();
	    CORRESPONDENCE_LIST 		= "CORRESPONDENCE_LIST";
	    offCorresOpen 				= new DomainDataView();
	    offCorresClose 				= new DomainDataView();
	    noteOwner 					= Constant.EndowmentFile.NOTES_OWNER;
		externalId 					= WebConstants.InheritanceFile.EXTERNAL_ID_OFFICIAL_CORRES;
		procedureTypeKey 			= Constant.EndowmentFile.PROCEDURE_KEY_OPEN_FILE;
		seletedEndFileAssoId		= "-1";
		TASK_TYPE 					= "TASK_TYPE";
	} 
	public void init() 
	{
	  super.init();
	}
	
	
	
	private void clearFields() 
	{
		cboBankId.setValue("-1");
		cboGovtDepttId.setValue("-1");
		txtArDescription.setValue(null);
		setSeletedEndFileAssoId(  "-1" );
	}
	private OfficialCorrespondenceView getFilledOffCorrspndncView() throws Exception 
	{
		OfficialCorrespondenceView view = new  OfficialCorrespondenceView();
		if(cboBankId.getValue().toString().compareTo("-1") != 0)
			view.getBank().setBankId(new Long(cboBankId.getValue().toString()));
		else
			view.getGovtDeptt().setGovtDepttId(new Long(cboGovtDepttId.getValue().toString()));
		view.setOffCorrId(null);
		view.setUpdatedBy(CommonUtil.getLoggedInUser());
		view.setUpdatedOn(new Date());
		view.setCreatedBy(CommonUtil.getLoggedInUser());
		view.setCreatedOn(new Date());
		view.setIsDeleted(Constant.DEFAULT_IS_DELETED);
		view.setDescription(txtArDescription.getValue().toString());
		view.setEndFileAssoId( getSeletedEndFileAssoId() );
		view.setStatus(170001l);
		UserView user = new UtilityService().getUserByLoginId(CommonUtil.getLoggedInUser());
		if(isEnglishLocale())
			view.setCreatedByName(user.getFullNamePrimary());
		else
			view.setCreatedByName(user.getFullNameSecondary());
		return view;
	}
	
	@SuppressWarnings("unchecked")
	private boolean isValidated() 
	{
		boolean validate = true;
		if(getSeletedEndFileAssoId() == null || getSeletedEndFileAssoId().equals("-1") )
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("endowmentOffCorr.msg.endowmentRequired"));
		
		}
		if(cboBankId.getValue() != null && cboGovtDepttId.getValue() != null)
		{
			String bankId = cboBankId.getValue().toString();
			String govtDepttId = cboGovtDepttId.getValue().toString();
			if(bankId.compareTo("-1") == 0 && govtDepttId.compareTo("-1") == 0)
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.oneOfBanknGovt"));
			}
			else
			if(bankId.compareTo("-1") != 0 && govtDepttId.compareTo("-1") != 0)
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.oneOfBanknGovt"));
			}
		}
		
		if(txtArDescription.getValue() == null || txtArDescription.getValue().toString().length() <= 0 )
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.descriptionReq"));
		}
		else
		if(txtArDescription.getValue().toString().length() > 250 )
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.descriptionTooLarge"));
		}	

		if(errorMessages != null && errorMessages.size() > 0)
		{
			viewMap.put(Constant.EndowmentFile.ERR_REG_TAB, errorMessages);
		}
		return validate;
	}
	
	@SuppressWarnings("unchecked")
	public void saveCorrespondence()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		if(	!isValidated()	){return;}
		try
		{
			
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			
			officialCorresList = getOfficialCorresList();
			OfficialCorrespondenceView offCorresView;
			offCorresView = getFilledOffCorrspndncView();
			offCorresView = new InheritanceFileService().saveCorrespondence(offCorresView);
			
			if(fileUploadCtrl.getUploadedFile() != null)
			{
				//For Document
				viewMap.put(WebConstants.Attachment.REPOSITORY_ID, WebConstants.Attachment.PIMS_REPOSITORY_ID);
				viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
				viewMap.put("noteowner",WebConstants.InheritanceFile.NOTES_OWNER);
				viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, offCorresView.getOffCorrId().toString());
				viewMap.put("entityId", offCorresView.getOffCorrId());
				//For Document				
				String documentTitle = CommonUtil.getBundleMessage("mems.inheritanceFile.officialCoresTab.docTitle");
				DocumentView documentView = CommonUtil.upload(fileUploadCtrl.getUploadedFile(), documentTitle.toString(), null);
				if(documentView == null)
				{
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.filenotUploaded"));
					viewMap.put(WebConstants.InherFileErrMsg.OFFICIAL_CORRES_ERR, errorMessages);
				}
			}
			
			ApplicationContext.getContext().getTxnContext().commit();
			populateTab(null);
			clearFields();
			successMessages.add( ResourceUtil.getInstance().getProperty( "officialCorrespondence.msg.saved" ) );
		}
		catch (Exception e) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("saveCorrespondence Carshed|",e);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
			if(	errorMessages != null && errorMessages.size() > 0	)
			{
				viewMap.put( Constant.EndowmentFile.ERR_REG_TAB, errorMessages	);
			}
			else if( successMessages != null && successMessages .size() > 0	)
			{
				viewMap.put( Constant.EndowmentFile.SUCCESS_REG_TAB, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void deleteOffCorrspndnc()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try 
		{
			OfficialCorrespondenceView correspondenceToBeDeleted = (OfficialCorrespondenceView) officialCorresDataTable.getRowData();
			int indexToDelete = officialCorresDataTable.getRowIndex();
			new InheritanceFileService().deleteOfficialCorrspndnc(correspondenceToBeDeleted.getOffCorrId());
			officialCorresList.remove(indexToDelete);
			setOfficialCorresList(officialCorresList);
			populateTab(null);
			successMessages.add( ResourceUtil.getInstance().getProperty( "officialCorrespondence.msg.deleted" ) );
		} 
		catch (Exception e) 
		{
			logger.LogException("deleteOffCorrspndnc Crahshed", e);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_REG_TAB, errorMessages);
			}
			else if(	successMessages != null && successMessages .size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.SUCCESS_REG_TAB, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void closeCorrespondence()
	{
		errorMessages= new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			officialCorresList = getOfficialCorresList();
			List<OfficialCorrespondenceView> correspndncToClose = new ArrayList<OfficialCorrespondenceView>();
			if(officialCorresList == null && officialCorresList.size() < 0)
			{return;}
			for(OfficialCorrespondenceView view : officialCorresList)
			{
				if( !view.isSelected() ){continue;}
				view.setStatus(	170002l );
				view.setOpened(false);
				correspndncToClose.add(view);
			}
			if(correspndncToClose == null || correspndncToClose.size() <= 0)
			{
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.selectCrspndncToClose"));
				return;
			}
			new InheritanceFileService().closeCorrespondences(correspndncToClose);
			populateTab(null);
			successMessages.add( ResourceUtil.getInstance().getProperty( "officialCorrespondence.msg.close" ) );
			
		}
		catch (Exception e) 
		{
			logger.LogException("closeCorrespondence crashed", e);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_REG_TAB, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.SUCCESS_REG_TAB , successMessages );
			}
		}
	}

	
	@SuppressWarnings("unchecked")
	public void populateTab(EndowmentFile file)throws Exception
	{
		if( file==null )
		{
			EndowmentFileVO endowmentFileVO  = ( EndowmentFileVO )viewMap.get( Constant.EndowmentFile.ENDOWMENT_FILE_VO ) ;
			file	 = endowmentFileVO.getEndowmentFile();
		}
		else
		{
			populateEndowmentsList(file);
		}
		setOfficialCorresList( 
				 				endowmentFileService.getAllOfficialCorrespondencesForEndowmentFile(file)
				 			 );
	
		
	}
	
	@SuppressWarnings("unchecked")
	private void populateEndowmentsList(EndowmentFile file) throws Exception
	{
		EndowmentService  endowmentService = new EndowmentService();
		List< EndFileAsso> dataList = endowmentService.getEndowmentsByEndowmentFileId( file );
		endowmentsList = new ArrayList<SelectItem>();
		for(EndFileAsso obj :dataList )
		{
			String dispValue = obj.getEndowment().getEndowmentNum()+"-"+obj.getEndowment().getEndowmentName();
			SelectItem item = new SelectItem( 	obj.getEndFileAssoId().toString(), dispValue );
			endowmentsList.add(item);
		}
		Collections.sort(endowmentsList,ListComparator.LIST_COMPARE);
		setEndowmentsList(endowmentsList);
	}
	@SuppressWarnings("unchecked")
	public String downloadFile()
	{
		errorMessages = new ArrayList<String>();
		try 
		{
				OfficialCorrespondenceView offView = (OfficialCorrespondenceView) officialCorresDataTable.getRowData();
				if(offView.getDocument() != null && offView.getDocument().getDocumentId() != null)
				{
					sessionMap.put("documentId",  offView.getDocument().getDocumentId().toString());
				}
							
	     } 
		catch (Exception e) 
		{
			logger.LogException("downloadFile Crahshed", e);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_REG_TAB, errorMessages);
			}
		}
		return "DownloadFile";
	}
	
	
	@SuppressWarnings("unchecked")
	public void openFollowupPopup()
	{
		errorMessages = new ArrayList<String>();
		try
		{
			OfficialCorrespondenceView offCorrView = (OfficialCorrespondenceView) officialCorresDataTable.getRowData();
			sessionMap.put(WebConstants.PAGE_MODE, WebConstants.InheritanceFilePageMode.IS_POPUP);
			sessionMap.put(WebConstants.MemsFollowupTab.OFFICIAL_CORRESPONDENCE_ID,offCorrView.getOffCorrId()) ;
			if(viewMap.get(TASK_TYPE) != null)
				sessionMap.put(TASK_TYPE,viewMap.get(TASK_TYPE)) ;
				
			executeJavascript("openFollowupPopup();");
		} 
		catch (Exception e) 
		{
			logger.LogException("openFollowupPopup Crahshed", e);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_REG_TAB, errorMessages);
			}
		}
	}
	private void executeJavascript(String javascript) throws Exception
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
	}

	public HtmlInputFileUpload getFileUploadCtrl() {
		return fileUploadCtrl;
	}
	
	public void setFileUploadCtrl(HtmlInputFileUpload fileUploadCtrl) {
		this.fileUploadCtrl = fileUploadCtrl;
	}
	
	public UploadedFile getSelectedFile() {
		return selectedFile;
	}
	
	@SuppressWarnings("unchecked")
	public List<OfficialCorrespondenceView> getOfficialCorresList() 
	{
		if(viewMap.get(CORRESPONDENCE_LIST) != null)
			officialCorresList = (List<OfficialCorrespondenceView>) viewMap.get(CORRESPONDENCE_LIST); 
		return officialCorresList;
	}
	@SuppressWarnings("unchecked")
	public void setOfficialCorresList(List<OfficialCorrespondenceView> officialCorresList) 
	{
		if(officialCorresList != null && officialCorresList.size() > 0)
			viewMap.put(CORRESPONDENCE_LIST, officialCorresList);
		this.officialCorresList = officialCorresList;
	}
	public HtmlDataTable getOfficialCorresDataTable() {
		return officialCorresDataTable;
	}
	public void setOfficialCorresDataTable(HtmlDataTable officialCorresDataTable) {
		this.officialCorresDataTable = officialCorresDataTable;
	}
	public HtmlSelectOneMenu getCboGovtDepttId() {
		return cboGovtDepttId;
	}
	public void setCboGovtDepttId(HtmlSelectOneMenu cboGovtDepttId) {
		this.cboGovtDepttId = cboGovtDepttId;
	}
	public HtmlSelectOneMenu getCboBankId() {
		return cboBankId;
	}
	public void setCboBankId(HtmlSelectOneMenu cboBankId) {
		this.cboBankId = cboBankId;
	}
	public HtmlInputTextarea getTxtArDescription() {
		return txtArDescription;
	}
	public void setTxtArDescription(HtmlInputTextarea txtArDescription) {
		this.txtArDescription = txtArDescription;
	}
	public HtmlInputText getTxtFilePath() {
		return txtFilePath;
	}
	public void setTxtFilePath(HtmlInputText txtFilePath) {
		this.txtFilePath = txtFilePath;
	}
	public void setSelectedFile(UploadedFile selectedFile) {
		this.selectedFile = selectedFile;
	}
	
	public String getSeletedEndFileAssoId() {
		if( viewMap.get("seletedEndFileAssoId") != null)
		{
			seletedEndFileAssoId = viewMap.get("seletedEndFileAssoId").toString(); 
		}
		return seletedEndFileAssoId;
	}
	@SuppressWarnings("unchecked")
	public void setSeletedEndFileAssoId(String seletedEndFileAssoId) {
		this.seletedEndFileAssoId = seletedEndFileAssoId;
		if( this.seletedEndFileAssoId != null  )
		{
			viewMap.put("seletedEndFileAssoId", this.seletedEndFileAssoId );
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SelectItem> getEndowmentsList() 
	{
		if( viewMap.get("endowmentsList") != null)
		{
			endowmentsList = (ArrayList<SelectItem>)viewMap.get("endowmentsList"); 
		}
		return endowmentsList;
	}
	
	@SuppressWarnings("unchecked")
	public void setEndowmentsList(List<SelectItem> endowmentsList) 
	{
		this.endowmentsList = endowmentsList;
		if( this.endowmentsList  != null  )
		{
			viewMap.put("endowmentsList", this.endowmentsList );
		}
	}
}
