package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.FamilyVillageFamilyAspectService;
import com.avanza.pims.ws.vo.mems.FamilyAspectFinancialView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageFamilyAspectsFinancialPopupBean extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private List<String> incomeSource;
	FamilyAspectFinancialView pageView;	
	private Long personId;
	protected HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
	public FamilyVillageFamilyAspectsFinancialPopupBean() 
	{
		pageView = new FamilyAspectFinancialView();
		pageView.setCreatedBy (getLoggedInUserId());
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			if(!isPostBack())
			{
				if (request.getParameter("personId") != null) 
				{
					personId = Long.valueOf( request.getParameter("personId").toString() );
					pageView.setPersonId(personId);
				}
			}
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		if(viewMap.get("pageView") != null)
		{
			pageView =  (FamilyAspectFinancialView)viewMap.get("pageView");
		}
		if(viewMap.get("incomeSource") != null)
		{
			incomeSource =  (ArrayList<String>)viewMap.get("incomeSource");
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
		if(pageView != null)
		{
			viewMap.put("pageView",pageView);
		}
		if( incomeSource != null && incomeSource.size()>0)
		{
			viewMap.put("incomeSource",incomeSource);
		}
	}	
	
	@SuppressWarnings("unchecked")
	private boolean hasErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		
		if( pageView.getMonthlyIncomeIdString() == null || pageView.getMonthlyIncomeIdString().equals("-1")) {
			errorMessages.add(CommonUtil.getBundleMessage("researchFamilyVillageBeneficiary.msg.FamilyAspectFinancial.monthlyIncomeRequired"));
			return true;
		}
		
		if( pageView.getMonthlyIncomeSourceId() == null || pageView.getMonthlyIncomeSourceId().equals("-1")) {
			errorMessages.add(CommonUtil.getBundleMessage("researchFamilyVillageBeneficiary.msg.FamilyAspectFinancial.incomeSourceRequired"));
			return true;
		}
		
		if( pageView.getDescription() == null || pageView.getDescription().trim().length() <=0) {
			errorMessages.add(CommonUtil.getBundleMessage("violationDetails.msg.descriptionRequired "));
			return true;
		}
		return hasSaveErrors;
		
	}

	
	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			String csv = "";
			
			for (String eachIncomeSource : incomeSource) {
				csv = csv + eachIncomeSource+",";
			}
			if(StringHelper.isNotEmpty(csv))
			{
			pageView.setMonthlyIncomeSourceId(csv.substring(0,csv.length()-1));
			}
//			if(csv.length()>0)
//			{
//				pageView.setMonthlyIncomeSourceId(csv.substring(0,csv.length()-1));
//			}
			if(hasErrors())return;
			pageView.setUpdatedBy (getLoggedInUserId());
			FamilyVillageFamilyAspectService.addFamilyAspectFinancial(pageView);
			successMessages.add( ResourceUtil.getInstance().getProperty( "commons.savedSuccessfully" ) );
		}
		catch ( Exception e )
		{
			logger.LogException( "onSave--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public FamilyAspectFinancialView getPageView() {
		return pageView;
	}

	public void setPageView(FamilyAspectFinancialView pageView) {
		this.pageView = pageView;
	}

	public List<String> getIncomeSource() {
		return incomeSource;
	}

	public void setIncomeSource(List<String> incomeSource) {
		this.incomeSource = incomeSource;
	}

}
