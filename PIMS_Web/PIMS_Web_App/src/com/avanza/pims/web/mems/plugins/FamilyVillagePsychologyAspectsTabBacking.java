package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.FamilyVillagePsychologicalAspectsService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.PsychologicalConditionView;
import com.avanza.pims.ws.vo.mems.PsychologicalTestView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillagePsychologyAspectsTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private List< PsychologicalConditionView> dataListPsychologicalCondition;
	private List< PsychologicalTestView> dataListPsychologicalTests;
	private HtmlDataTable dataTablePsychologicalCondition;
	private HtmlDataTable dataTablePsychologicalTests;
	FamilyVillagePsychologicalAspectsService service;
	
	private String tabMode;
	private Long personId;
	private Integer recordSizePsychologicalCondition;
	private Integer recordSizePsychologicalTests;
	public FamilyVillagePsychologyAspectsTabBacking() 
	{
		service = new FamilyVillagePsychologicalAspectsService();
		dataTablePsychologicalCondition             =  new HtmlDataTable();
		dataTablePsychologicalTests          =  new HtmlDataTable();
		dataListPsychologicalCondition		        =  new ArrayList<PsychologicalConditionView>();
		dataListPsychologicalTests    		=  new ArrayList<PsychologicalTestView>();
		recordSizePsychologicalTests  		=  0;
		recordSizePsychologicalCondition  		=  0;
		tabMode          = Constant.EndowmentFile.MODE_READ_ONLY;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.PSYCHOLOGICAL_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.PSYCHOLOGICAL_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			tabMode = (String) viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		if( viewMap.get( "dataListPsychologicalCondition" ) != null )
		{
			dataListPsychologicalCondition = (ArrayList<PsychologicalConditionView>)viewMap.get( "dataListPsychologicalCondition" );
		}
		if( viewMap.get( "recordSizePsychologicalTests" ) != null )
		{
			recordSizePsychologicalTests = Integer.parseInt( viewMap.get( "recordSizePsychologicalTests" ).toString() );
		}
				
		if( viewMap.get( "dataListPsychologicalTests" ) != null )
		{
			dataListPsychologicalTests = (ArrayList<PsychologicalTestView>)viewMap.get( "dataListPsychologicalTests" );
		}
		if( viewMap.get( "recordSizePsychologicalCondition" ) != null )
		{
			recordSizePsychologicalCondition = Integer.parseInt( viewMap.get( "recordSizePsychologicalCondition" ).toString() );
		}
		
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, tabMode );
		}
		if(recordSizePsychologicalCondition !=null)
		{
			viewMap.put("recordSizePsychologicalCondition",recordSizePsychologicalCondition);
		}
		if(recordSizePsychologicalTests !=null)
		{
			viewMap.put("recordSizePsychologicalTests",recordSizePsychologicalTests);
		}
		if( dataListPsychologicalTests != null && dataListPsychologicalTests.size()  > 0  )
		{
			viewMap.put( "dataListPsychologicalTests", dataListPsychologicalTests );
		}
		if( dataListPsychologicalCondition != null && dataListPsychologicalCondition.size()  > 0  )
		{
			viewMap.put( "dataListPsychologicalCondition", dataListPsychologicalCondition );
		}
		
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
	}	
	
	

	public void populateTab( PersonView personView ) throws Exception
	{
		updateValuesFromMaps();
		personId = personView.getPersonId();
		setPageDataDefaults();
		loadDataListPsychologicalTests( personId );
		loadDataListPsychologicalCondition( personId );
		
		updateValuesToMaps();
	}
	
	private void setPageDataDefaults()throws Exception
	{
		PersonView person  = new PersonView();
		person.setPersonId(personId);
	}

	private void loadDataListPsychologicalCondition(Long personId) throws Exception {
		
		dataListPsychologicalCondition= FamilyVillagePsychologicalAspectsService.getPsychologicalConditions( personId );
		recordSizePsychologicalCondition=dataListPsychologicalCondition.size();
	}

	private void loadDataListPsychologicalTests(Long personId) throws Exception {
		
		dataListPsychologicalTests= FamilyVillagePsychologicalAspectsService.getPsychologicalTests( personId );
		recordSizePsychologicalTests=dataListPsychologicalTests.size();
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDeletePsychologicalCondition()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeletePsychologicalCondition--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.PSYCHOLOGICAL_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.PSYCHOLOGICAL_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDeletePsychologicalTests()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeletePsychologicalTests--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.PSYCHOLOGICAL_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.PSYCHOLOGICAL_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
	}
	
	
	
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}
	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public List<PsychologicalConditionView> getDataListPsychologicalCondition() {
		return dataListPsychologicalCondition;
	}

	public void setDataListPsychologicalCondition(
			List<PsychologicalConditionView> dataListPsychologicalCondition) {
		this.dataListPsychologicalCondition = dataListPsychologicalCondition;
	}

	public List<PsychologicalTestView> getDataListPsychologicalTests() {
		return dataListPsychologicalTests;
	}

	public void setDataListPsychologicalTests(
			List<PsychologicalTestView> dataListPsychologicalTests) {
		this.dataListPsychologicalTests = dataListPsychologicalTests;
	}

	public HtmlDataTable getDataTablePsychologicalCondition() {
		return dataTablePsychologicalCondition;
	}

	public void setDataTablePsychologicalCondition(
			HtmlDataTable dataTablePsychologicalCondition) {
		this.dataTablePsychologicalCondition = dataTablePsychologicalCondition;
	}

	public HtmlDataTable getDataTablePsychologicalTests() {
		return dataTablePsychologicalTests;
	}

	public void setDataTablePsychologicalTests(
			HtmlDataTable dataTablePsychologicalTests) {
		this.dataTablePsychologicalTests = dataTablePsychologicalTests;
	}

	public Integer getRecordSizePsychologicalCondition() {
		return recordSizePsychologicalCondition;
	}

	public void setRecordSizePsychologicalCondition(
			Integer recordSizePsychologicalCondition) {
		this.recordSizePsychologicalCondition = recordSizePsychologicalCondition;
	}

	public Integer getRecordSizePsychologicalTests() {
		return recordSizePsychologicalTests;
	}

	public void setRecordSizePsychologicalTests(Integer recordSizePsychologicalTests) {
		this.recordSizePsychologicalTests = recordSizePsychologicalTests;
	}
	
	


}
