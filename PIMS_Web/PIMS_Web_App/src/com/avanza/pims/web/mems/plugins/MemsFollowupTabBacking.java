package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.MemsFollowupService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.mems.MemsFollowupView;

public class MemsFollowupTabBacking extends AbstractMemsBean
{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2526741634539592008L;
	private MemsFollowupView memsFollowupView;
	private List<MemsFollowupView> memsFollowupViewList;
	private final String ACTIVATED_FROM_CORRESPONDENCE = "ACTIVATED_FROM_CORRESPONDENCE";
	private final String ACTIVATED_FROM_FOLLOWUP = "ACTIVATED_FROM_FOLLOWUP";
	private final String ACTIVATED_FROM = "ACTIVATED_FROM";
	private static final String TASK_TYPE = "TASK_TYPE";
	private HtmlDataTable memsFollowUpDataTable;
    private Long fileId;
    private Long officialCorrespondenceId;
    private Long followupId;
    private Long recommendationId;
    MemsFollowupService service;
	public MemsFollowupTabBacking() 
	{
		memsFollowupView = new MemsFollowupView();
		memsFollowupViewList = new ArrayList<MemsFollowupView>(0);
		
		memsFollowUpDataTable = new HtmlDataTable();
		service = new MemsFollowupService();
	}
	
	@Override
	@SuppressWarnings( "unchecked" )
	public void init() 
	{
		String METHOD_NAME = "init()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			getParameters();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	@SuppressWarnings( "unchecked" )
	private void getParameters() throws Exception
	{
		Map  searchCriteria;
		if( sessionMap.containsKey(WebConstants.MemsFollowupTab.FILE_ID) )//this will be executed while open in popup
		{
			this.setFileId( new Long ( sessionMap.get(WebConstants.MemsFollowupTab.FILE_ID).toString() ) );
			sessionMap.remove(WebConstants.MemsFollowupTab.FILE_ID) ;
			getFollowups();
		}
		else if( sessionMap.containsKey(WebConstants.MemsFollowupTab.OFFICIAL_CORRESPONDENCE_ID) ) //this will be executed while open in popup
		{
			Long corrId= new Long ( sessionMap.get(WebConstants.MemsFollowupTab.OFFICIAL_CORRESPONDENCE_ID).toString() );
			this.setOfficialCorrespondenceId( corrId );
			sessionMap.remove(WebConstants.MemsFollowupTab.OFFICIAL_CORRESPONDENCE_ID ) ;
			getFollowups();
			if(viewMap.containsKey("IS_POPUP"))
			{
				loadAttachments(corrId);
				viewMap.put(ACTIVATED_FROM, ACTIVATED_FROM_CORRESPONDENCE);
			}
		}
		else if( sessionMap.containsKey(WebConstants.MemsFollowupTab.RECOMMENDATIONS_ID) )//this will be executed while open in popup
		{
			viewMap.put(WebConstants.MemsFollowupTab.RECOMMENDATIONS_ID, sessionMap.get(WebConstants.MemsFollowupTab.RECOMMENDATIONS_ID).toString() );
			sessionMap.remove(WebConstants.MemsFollowupTab.RECOMMENDATIONS_ID) ;
		}
		//TODO:
		else if( viewMap.containsKey(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW) )//this will be executed while open in popup
		{
			MemsFollowupView followup = (MemsFollowupView) viewMap.get(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW);
			loadAttachments(followup.getFollowupId());
			viewMap.put("memsFollowupView", followup);
			viewMap.put(ACTIVATED_FROM, ACTIVATED_FROM_FOLLOWUP);
		}
		else
		{
			getFollowups();
		}
	}
	@SuppressWarnings( "unchecked" )
	private void getFollowups()throws Exception 
	{
		if(this.getOfficialCorrespondenceId() != null && viewMap.containsKey("IS_POPUP"))
		{
			getFollowUpByCorrespondenceId();
		}
		else
		{
			getFollowUpByFileId();
		}
	}
	@SuppressWarnings( "unchecked" )
	private void getFollowUpByFileId() throws Exception 
	{
		Map searchCriteria;
		searchCriteria= new HashMap();
		if(this.getFileId() != null)
		{
			searchCriteria.put(WebConstants.MemsFollowupTab.FILE_ID, this.getFileId().toString() );
			loadDataList( searchCriteria );
		}
	}

	@SuppressWarnings( "unchecked" )
	private void getFollowUpByCorrespondenceId() throws Exception {
		Map searchCriteria;
		searchCriteria= new HashMap();
		searchCriteria.put(WebConstants.MemsFollowupSearchCriteria.OFF_CORR_ID, this.getOfficialCorrespondenceId().toString() );
		loadDataList( searchCriteria );
	}
	

	@SuppressWarnings( "unchecked" )
	private void loadDataList( Map  searchCriteria ) throws Exception
	{
	
		this.setMemsFollowupViewList( service.getFollowup(searchCriteria) );
	}
	@SuppressWarnings("unchecked")
	private boolean hasError()
	{
		boolean hasError = false;
		memsFollowupView = this.getMemsFollowupView();
		if(memsFollowupView.getCategory() == null || memsFollowupView.getCategory().compareTo("-1") == 0)
		{
			hasError = true;
			errorMessages.add(CommonUtil.getBundleMessage("contractFollowUp.msg.followUpCat"));
		}
		
		if(memsFollowupView.getTypeId() == null || memsFollowupView.getTypeId().compareTo("-1") == 0)
		{
			hasError = true;
			errorMessages.add(CommonUtil.getBundleMessage("contractFollowUp.msg.followUpType"));
		}
		
		if(memsFollowupView.getFollowupDate() == null )
		{
			hasError = true;
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.FollowupDateReq"));
		}
		
		if(memsFollowupView.getDescription() == null || memsFollowupView.getDescription().length()<= 0)
		{
			hasError = true;
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.FollowupDescReq"));
		}
		
		if(memsFollowupView.getDetails() == null || memsFollowupView.getDetails().length()<= 0)
		{
			hasError = true;
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.FollowupDetReq"));
		}
		if(errorMessages != null && errorMessages.size() > 0)
		{
			viewMap.put(WebConstants.InherFileErrMsg.FOLLOWUP_ERR, errorMessages);
		}
		return hasError;
	}
	public void onSave()
	{
		String methodName = "onSave|";
		try	
		{	
			
			logger.logInfo(methodName + " --- STARTED --- ");
			if(!hasError())
			{
				MemsFollowupView vo = this.getMemsFollowupView();
				boolean isOpen = false;
				//if saved against file
				if( this.getFileId() != null )
				{
					vo.setInheritanceFileId(this.getFileId());
					if(StringHelper.isNotEmpty(vo.getCategory()))
					{
						DomainDataView domainData = null;
						domainData = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUpCategory.FOLLOW_UP_CATEGORY_TYPE), WebConstants.FollowUpCategory.SOCIAL_RESEARCH);
						if(domainData.getDomainDataId().compareTo(new Long(vo.getCategory())) == 0)
						{
							domainData = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.MemsFollowupTab.MEMS_FOLLOW_UP_STATUS), WebConstants.MemsFollowupTab.MEMS_FOLLOW_UP_STATUS_OPEN);
							vo.setStatusId(domainData.getDomainDataId());
							isOpen =  true;
						}
						else
						{
							domainData = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.MemsFollowupTab.MEMS_FOLLOW_UP_STATUS), WebConstants.MemsFollowupTab.MEMS_FOLLOW_UP_STATUS_CLOSED);
							vo.setStatusId(domainData.getDomainDataId());
							isOpen =  false;
						}	
					}
				}
				//if saved against correspondence
				else if ( this.getOfficialCorrespondenceId() != null && viewMap.containsKey("IS_POPUP"))
					vo.setOfficialCorrespondenceId( this.getOfficialCorrespondenceId() );
				
				vo.setUpdatedBy( CommonUtil.getLoggedInUser() );
				vo.setCreatedBy( CommonUtil.getLoggedInUser() );
				vo.setIsDeleted(0L);
				service.persistFollowup( vo );
				List <MemsFollowupView> list = this.getMemsFollowupViewList();
				if(viewMap.get("FOLLOW_UP_INDEX_TO_UPDATE") != null)
				{
					int index = (Integer) viewMap.get("FOLLOW_UP_INDEX_TO_UPDATE");
					list.remove(index);
					viewMap.remove("FOLLOW_UP_INDEX_TO_UPDATE");
				}
				
				if(isOpen)
					vo.setShowCheckBox(true);
				else
					vo.setShowCheckBox(false);
				//list.add( vo );


				if(this.getOfficialCorrespondenceId() != null && viewMap.containsKey("IS_POPUP"))
				{
					saveAttachment();
				}
				getFollowups();   
				clearFields();
					
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void clearFields() 
	{
		MemsFollowupView followup = new MemsFollowupView();
	    viewMap.put("memsFollowupView" ,followup);
	}
	@SuppressWarnings( "unchecked" )
	public String onNavigateToDisbursement()
	{
		errorMessages  = new ArrayList<String>();
		try	
		{
			
			MemsFollowupView followUp = (MemsFollowupView) memsFollowUpDataTable.getRowData();
			sessionMap.put(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW,followUp) ;
			sessionMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW, 
					             (InheritanceFileView)viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) 
					           );
			viewMap.remove(WebConstants.ApplicationDetails.APPLICATION_NUMBER);  
			return WebConstants.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID.toString();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onNavigateToDisbursement --- CRASHED --- ", exception);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}
	
	@SuppressWarnings( "unchecked" )
	public String onNavigateToMaintenance()
	{
		errorMessages  = new ArrayList<String>();
		try	
		{
			
			MemsFollowupView followUp = (MemsFollowupView) memsFollowUpDataTable.getRowData();
			sessionMap.put(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW,followUp) ;
			sessionMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW, 
					             (InheritanceFileView)viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) 
					           );
			viewMap.remove(WebConstants.ApplicationDetails.APPLICATION_NUMBER);  
			return WebConstants.REQUEST_TYPE_MINOR_MAINTENANCE_REQUEST .toString();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onNavigateToMaintenance --- CRASHED --- ", exception);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}
	
	public void onTemplateMethod()
	{
		String methodName = "onSave|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
	
		
	}	

	@SuppressWarnings( "unchecked" )
	public MemsFollowupView getMemsFollowupView() {
		if( viewMap.get( "memsFollowupView" )!=null )
			memsFollowupView = (MemsFollowupView)viewMap.get( "memsFollowupView" ); 
		return memsFollowupView;
	}
	@SuppressWarnings( "unchecked" )
	public void setMemsFollowupView(MemsFollowupView memsFollowupView) {
		this.memsFollowupView = memsFollowupView;
		if ( this.memsFollowupView != null )
			viewMap.put("memsFollowupView", this.memsFollowupView);
	}
	@SuppressWarnings( "unchecked" )
	public List<MemsFollowupView> getMemsFollowupViewList() {
		if( viewMap.get( "memsFollowupViewList" )!=null )
			memsFollowupViewList = ( ArrayList<MemsFollowupView> )viewMap.get( "memsFollowupViewList" );
		return memsFollowupViewList;
	}
	@SuppressWarnings( "unchecked" )
	public void setMemsFollowupViewList(List<MemsFollowupView> memsFollowupViewList) {
		this.memsFollowupViewList = memsFollowupViewList;
		if ( this.memsFollowupViewList != null )
			viewMap.put("memsFollowupViewList", this.memsFollowupViewList);
	}

	public HtmlDataTable getMemsFollowUpDataTable() {
		return memsFollowUpDataTable;
	}

	public void setMemsFollowUpDataTable(HtmlDataTable memsFollowUpDataTable) {
		this.memsFollowUpDataTable = memsFollowUpDataTable;
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
		
		
	}

	public Long getFileId() {
		if( viewMap.containsKey( WebConstants.MemsFollowupTab.FILE_ID )  )
			fileId = new Long (viewMap.get(WebConstants.MemsFollowupTab.FILE_ID).toString() );
		return fileId;
	}

	@SuppressWarnings( "unchecked" )
	public void setFileId(Long fileId) {
		
		this.fileId = fileId;
		if( this.fileId != null )
		viewMap.put(WebConstants.MemsFollowupTab.FILE_ID,this.fileId.toString());
	}

	public Long getOfficialCorrespondenceId() {
		if( viewMap.containsKey( WebConstants.MemsFollowupTab.OFFICIAL_CORRESPONDENCE_ID)  )
			officialCorrespondenceId = new Long (viewMap.get(WebConstants.MemsFollowupTab.OFFICIAL_CORRESPONDENCE_ID).toString() );
		
		return officialCorrespondenceId;
	}

	@SuppressWarnings( "unchecked" )
	public void setOfficialCorrespondenceId(Long officialCorrespondenceId) {
		this.officialCorrespondenceId = officialCorrespondenceId;
		if( this.officialCorrespondenceId != null )
			viewMap.put(WebConstants.MemsFollowupTab.OFFICIAL_CORRESPONDENCE_ID ,this.officialCorrespondenceId.toString());
	}

	public Long getRecommendationId() {
		return recommendationId;
	}

	public void setRecommendationId(Long recommendationId) {
		this.recommendationId = recommendationId;
	}
	private void saveAttachment() throws Exception 
	{
		if(viewMap.get(WebConstants.MemsFollowupTab.OFFICIAL_CORRESPONDENCE_ID) != null)
		{
			Long offCorId = new Long(viewMap.get(WebConstants.MemsFollowupTab.OFFICIAL_CORRESPONDENCE_ID).toString());
			CommonUtil.saveAttachments(offCorId);
		}
		else
		if(viewMap.get(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW) != null)
		{
			MemsFollowupView followup= (MemsFollowupView) viewMap.get(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW);
			CommonUtil.saveAttachments(followup.getFollowupId());
		}	
	}

	@SuppressWarnings("unchecked")
	public void loadAttachments(Long objectId)
	{
		//common settings
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.InheritanceFile.PROCEDURE_KEY_OPEN_FILE);
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, WebConstants.Attachment.PIMS_REPOSITORY_ID);
    	//common settings
		if(this.getOfficialCorrespondenceId() != null)
		{
			viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.MemsFollowupTab.EXTERNAL_ID_OFF_CORR);
			viewMap.put("noteowner", WebConstants.MemsFollowupTab.NOTES_OWNER_OFF_CORR);	
		}
		else
		if(this.getFollowupId() != null)
		{
			viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.MemsFollowupTab.EXTERNAL_ID_FOLLOW_UP);
			viewMap.put("noteowner", WebConstants.MemsFollowupTab.NOTES_OWNER_FOLLOW_UP);	
		}	
		
		if(objectId!= null)
		{
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, objectId.toString());
			viewMap.put("entityId", objectId.toString());
		}
	}
	@SuppressWarnings("unchecked")
//	public void openFollowupPopup()
//	{
//		MemsFollowupView followUp = (MemsFollowupView) memsFollowUpDataTable.getRowData();
//		sessionMap.put(WebConstants.PAGE_MODE, WebConstants.InheritanceFilePageMode.IS_POPUP);
//		sessionMap.put(WebConstants.MemsFollowupTab.OFFICIAL_CORRESPONDENCE_ID,followUp.getOfficialCorrespondenceId()) ;
//		executeJavascript("openFollowupPopup();");
//	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	public Long getFollowupId() 
	{
		if( viewMap.containsKey( WebConstants.MemsFollowupTab.FOLLOW_UP_ID)  )
			followupId = new Long (viewMap.get(WebConstants.MemsFollowupTab.FOLLOW_UP_ID).toString() );
		return followupId;
	}

	@SuppressWarnings("unchecked")
	public void setFollowupId(Long followupId) 
	{
		if( this.followupId != null )
			viewMap.put(WebConstants.MemsFollowupTab.FOLLOW_UP_ID ,this.followupId.toString());
		this.followupId = followupId;
	}
	public boolean isActivatedFromFollowup()
	{
		boolean isFromFollowup = false;
		if(viewMap.get(ACTIVATED_FROM) != null)
		isFromFollowup = (viewMap.get(ACTIVATED_FROM).toString().compareToIgnoreCase(ACTIVATED_FROM_FOLLOWUP) == 0);
		return isFromFollowup;
	}
	public boolean isActivatedFromCorrespondence()
	{
		boolean isFromCorrespondence = false;
		if(viewMap.get(ACTIVATED_FROM) != null)
		isFromCorrespondence = (viewMap.get(ACTIVATED_FROM).toString().compareToIgnoreCase(ACTIVATED_FROM_CORRESPONDENCE) == 0);
		return isFromCorrespondence;
	}
	@SuppressWarnings("unchecked")
	public void openRecommendationPopup()
	{
		MemsFollowupView followUp = (MemsFollowupView) memsFollowUpDataTable.getRowData();
		sessionMap.put(WebConstants.PAGE_MODE, WebConstants.InheritanceFilePageMode.IS_POPUP);
		sessionMap.put(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW,followUp) ;
		sessionMap.put(WebConstants.InheritanceFile.FILE_ID, viewMap.get(WebConstants.InheritanceFile.FILE_ID));
		if(!followUp.isShowCheckBox())
			sessionMap.put(WebConstants.VIEW_MODE, true);
		executeJavascript("openFollowupPopup();");
	}
	public void deleteFollowup()
	{
		MemsFollowupView follwupToDelete  = (MemsFollowupView) memsFollowUpDataTable.getRowData();
		int indexToDelete = memsFollowUpDataTable.getRowIndex();
		memsFollowupViewList = this.getMemsFollowupViewList();
		try 
		{
			
			follwupToDelete.setUpdatedBy(CommonUtil.getLoggedInUser());
			follwupToDelete.setUpdatedOn(new Date());
			follwupToDelete.setIsDeleted(1L);
			new InheritanceFileService().deleteFollowup(follwupToDelete);
			//memsFollowupViewList.remove(indexToDelete);
			getFollowups();
		} 
		catch (Exception e) 
		{
			logger.LogException("deleteFollowup carshed |", e);
		}
	}
	@SuppressWarnings("unchecked")
	public void editFollowup()
	{
		MemsFollowupView followUp = (MemsFollowupView) memsFollowUpDataTable.getRowData();
		int indexToUpdate = memsFollowUpDataTable.getRowIndex();
		viewMap.put("FOLLOW_UP_INDEX_TO_UPDATE", indexToUpdate);
		viewMap.put("memsFollowupView",  followUp);
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getFollowupCategory()
	{
		List<DomainDataView> ddList  = null;
		List<SelectItem> catList = new ArrayList<SelectItem>();
		String METHOD_NAME = "getFollowupCategory()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			if(viewMap.get(WebConstants.FollowUpCategory.FOLLOW_UP_CATEGORY_TYPE) == null)
				{
					ddList = CommonUtil.getDomainDataListForDomainType(WebConstants.FollowUpCategory.FOLLOW_UP_CATEGORY_TYPE);
					viewMap.put(WebConstants.FollowUpCategory.FOLLOW_UP_CATEGORY_TYPE, ddList);
				}
			else
				{
					ddList = (List<DomainDataView>) viewMap.get(WebConstants.FollowUpCategory.FOLLOW_UP_CATEGORY_TYPE);
					
				} 
			DomainDataView limDD = null;
			DomainDataView researchDD = null;
			if(ddList != null && ddList.size() > 0 )
			{
				limDD  = CommonUtil.getIdFromType(ddList, WebConstants.FollowUpCategory.INHERITANCE_UNIT);
				researchDD = CommonUtil.getIdFromType(ddList, WebConstants.FollowUpCategory.SOCIAL_RESEARCH);
				viewMap.put(WebConstants.FollowUpCategory.INHERITANCE_UNIT, limDD);
				viewMap.put(WebConstants.FollowUpCategory.SOCIAL_RESEARCH, researchDD);
				
				if(viewMap.get(TASK_TYPE) != null)
				{
					String taskType = viewMap.get(TASK_TYPE).toString();
					  
					   if(taskType.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.TASK_FOR_LIMITATION) == 0 ||
						   taskType.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.TASK_FOR_APPROVING_LIMITATION) == 0)
						   {
					   			for(DomainDataView dd : ddList)
					   			{
					   				if(dd.getDomainDataId().compareTo(limDD.getDomainDataId()) == 0)
					   				{
					   					SelectItem item = new SelectItem(dd.getDomainDataId().toString(),(isEnglishLocale()?dd.getDataDescEn() : dd.getDataDescAr()));
										catList.add(item);
										break;
					   				}
					   					
					   			}	
						   }
					   else
					   if(taskType.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.TASK_FOR_ASSIGNING_RESEARCHER) == 0 ||
							   taskType.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.TASK_FOR_RESEARCHER) == 0)
							   {
						   			for(DomainDataView dd : ddList)
						   			{
						   				if(dd.getDomainDataId().compareTo(researchDD.getDomainDataId()) == 0)
						   				{
						   					SelectItem item = new SelectItem(dd.getDomainDataId().toString(),(isEnglishLocale()?dd.getDataDescEn() : dd.getDataDescAr()));
											catList.add(item);
											break;
						   				}
						   					
						   			}	
							   } 
				}
				
			   else if(isActivatedFromCorrespondence())
			   {
				   for(DomainDataView dd : ddList)
		   			{
					   if(dd.getDomainDataId().compareTo(limDD.getDomainDataId()) == 0)
		   				{
					   		SelectItem item = new SelectItem(dd.getDomainDataId().toString(),(isEnglishLocale()?dd.getDataDescEn() : dd.getDataDescAr()));
							catList.add(item);
							break;
		   				}
		   			}	
			   }
			   else
			   {
				   for(DomainDataView dd : ddList)
		   			{
				   		SelectItem item = new SelectItem(dd.getDomainDataId().toString(),(isEnglishLocale()?dd.getDataDescEn() : dd.getDataDescAr()));
						catList.add(item);
		   			}	
			   }
					   
			}
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
		return catList;
	}
	public String getSocialResearchCatId()
	{
		if(viewMap.get(WebConstants.FollowUpCategory.SOCIAL_RESEARCH) != null)
		{
			DomainDataView ddView  =(DomainDataView) viewMap.get(WebConstants.FollowUpCategory.SOCIAL_RESEARCH) ;
			return ddView.getDomainDataId().toString();
		}
		else
			return null;
	}
	public boolean isPageModeView()
	{
		boolean isView= false;
		if(viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE) != null)
		{
			String pagemode = viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE).toString();
			if(pagemode.equalsIgnoreCase(WebConstants.InheritanceFilePageMode.PAGE_MODE_READ_ONLY))
			{
				isView = true;
			}
			else
				isView = false;
			
		}
		
		return isView;
	}
	public boolean isViewModePopup()
	{
		boolean isView = false;
		isView = viewMap.containsKey(WebConstants.InheritanceFilePageMode.IS_POPUP);
		return isView;
	}
}
