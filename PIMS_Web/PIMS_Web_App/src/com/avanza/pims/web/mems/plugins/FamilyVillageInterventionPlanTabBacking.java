package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.InterventionPlanService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.InterventionPlanView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageInterventionPlanTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private List< InterventionPlanView> dataListInterventionPlan;
	private HtmlDataTable dataTableInterventionPlan;
	InterventionPlanService service;
	
	private String tabMode;
	private Long personId;
	private Integer recordSizeInterventionPlan;
	public FamilyVillageInterventionPlanTabBacking() 
	{
		service = new InterventionPlanService();
		dataTableInterventionPlan             =  new HtmlDataTable();
		dataListInterventionPlan		        =  new ArrayList<InterventionPlanView>();
		recordSizeInterventionPlan  		=  0;
		tabMode          = Constant.EndowmentFile.MODE_READ_ONLY;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.INTERVENTION_PLAN_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.INTERVENTION_PLAN_TAB_SUCCESS, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			tabMode = (String) viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		if( viewMap.get( "dataListInterventionPlan" ) != null )
		{
			dataListInterventionPlan = (ArrayList<InterventionPlanView>)viewMap.get( "dataListInterventionPlan" );
		}
				
		if( viewMap.get( "recordSizeInterventionPlan" ) != null )
		{
			recordSizeInterventionPlan = Integer.parseInt( viewMap.get( "recordSizeInterventionPlan" ).toString() );
		}
		
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, tabMode );
		}
		if(recordSizeInterventionPlan !=null)
		{
			viewMap.put("recordSizeInterventionPlan",recordSizeInterventionPlan);
		}
		if( dataListInterventionPlan != null && dataListInterventionPlan.size()  > 0  )
		{
			viewMap.put( "dataListInterventionPlan", dataListInterventionPlan );
		}
		
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
	}	
	
	

	public void populateTab( PersonView personView ) throws Exception
	{
		updateValuesFromMaps();
		personId = personView.getPersonId();
		setPageDataDefaults();
		loadDataListInterventionPlan( personId );
		
		updateValuesToMaps();
	}
	
	private void setPageDataDefaults()throws Exception
	{
		PersonView person  = new PersonView();
		person.setPersonId(personId);
	}

	private void loadDataListInterventionPlan(Long personId) throws Exception {
		
		dataListInterventionPlan= InterventionPlanService.getInterventionPlans( personId );
		recordSizeInterventionPlan=dataListInterventionPlan.size();
	}

	
	@SuppressWarnings( "unchecked" )
	public void onDeleteInterventionPlan()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteInterventionPlan--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.INTERVENTION_PLAN_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.INTERVENTION_PLAN_TAB_SUCCESS, successMessages );
			}
		}
		
	}
	
	
	
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}
	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public List<InterventionPlanView> getDataListInterventionPlan() {
		return dataListInterventionPlan;
	}

	public void setDataListInterventionPlan(
			List<InterventionPlanView> dataListInterventionPlan) {
		this.dataListInterventionPlan = dataListInterventionPlan;
	}

	public HtmlDataTable getDataTableInterventionPlan() {
		return dataTableInterventionPlan;
	}

	public void setDataTableInterventionPlan(
			HtmlDataTable dataTableInterventionPlan) {
		this.dataTableInterventionPlan = dataTableInterventionPlan;
	}

	public Integer getRecordSizeInterventionPlan() {
		return recordSizeInterventionPlan;
	}

	public void setRecordSizeInterventionPlan(
			Integer recordSizeInterventionPlan) {
		this.recordSizeInterventionPlan = recordSizeInterventionPlan;
	}

}
