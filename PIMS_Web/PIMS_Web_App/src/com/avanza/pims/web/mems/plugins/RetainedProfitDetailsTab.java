package com.avanza.pims.web.mems.plugins;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.RetainedProfitDetails;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.endowment.RetainedProfitService;

public class RetainedProfitDetailsTab extends AbstractMemsBean
{	

	private static final long serialVersionUID = 1L;
	private String tabMode;
    private List<RetainedProfitDetails> retainedProfitLists ;
    private String sumTotalRetainedProfits = "0.0";
    private String sumTotalRetainedActiveProfits = "0.0";
    private String sumTotalRetainedReleasedProfits = "0.0";
    private HtmlDataTable dataTable;
	public RetainedProfitDetailsTab() 
	{
		tabMode = WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_VIEW_ONLY;
		retainedProfitLists = new ArrayList<RetainedProfitDetails>();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException("init --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void loadTabData() throws Exception
	{
		Long inheritanceBeneficiaryId = null,
			 fileId 				  = null , 
			 personId 				  = null;
		Double totalProfit = 0.0d, activeProfits = 0.0d, releasedProfits = 0.0d;
		
		if( viewMap.get( WebConstants.RetainedProfits.RetainedProfitsInheritanceBenId ) != null)
		{
			inheritanceBeneficiaryId = new Long( viewMap.get( WebConstants.RetainedProfits.RetainedProfitsInheritanceBenId ).toString() );
		}
		if( viewMap.get( WebConstants.RetainedProfits.RetainedProfitsInheritanceFileId ) != null)
		{
			fileId = new Long( viewMap.get( WebConstants.RetainedProfits.RetainedProfitsInheritanceFileId ).toString() );
		}
		if( viewMap.get( WebConstants.RetainedProfits.RetainedProfitsPersonId ) != null)
		{
			personId = new Long( viewMap.get( WebConstants.RetainedProfits.RetainedProfitsPersonId ).toString() );
		}
		retainedProfitLists = RetainedProfitService.getRetainedProfitsList( 
																			personId, 
																			inheritanceBeneficiaryId, 
																			fileId
																		  );
		if ( retainedProfitLists == null  ) return;
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		for (RetainedProfitDetails item : retainedProfitLists) 
		{
		  if( item.getStatus().getDomainDataId().compareTo( Constant.RetainedProfitDetailsStatus.ACTIVATED_ID ) == 0 )
		  {
			  activeProfits = activeProfits + item.getAmount();
		  }
		  if( item.getStatus().getDomainDataId().compareTo( Constant.RetainedProfitDetailsStatus.RELEASED_ID ) == 0 )
		  {
			  releasedProfits = releasedProfits + item.getAmount();
		  }
		  
		  totalProfit = totalProfit + item.getAmount();
		  if( totalProfit.compareTo(0d)>0  )
		  {
				  setSumTotalRetainedProfits( oneDForm.format(totalProfit) );
		  }
		  if( releasedProfits.compareTo(0d)>0  )
		  {
			  setSumTotalRetainedReleasedProfits( oneDForm.format(releasedProfits) );
		  }
		  if( activeProfits.compareTo(0d)>0  )
		  {
			  setSumTotalRetainedActiveProfits( oneDForm.format(activeProfits) );
		  }
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	public void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY ) != null )
		{
			tabMode = (String) viewMap.get( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY );
		}
		boolean loadData = false;
		if( sessionMap.get( WebConstants.RetainedProfits.RetainedProfitsInheritanceBenId ) != null )
		{
			viewMap.put( WebConstants.RetainedProfits.RetainedProfitsInheritanceBenId,
						 sessionMap.remove( WebConstants.RetainedProfits.RetainedProfitsInheritanceBenId ) 
					   );
			loadData = true;
		}
		if( sessionMap.get( WebConstants.RetainedProfits.RetainedProfitsInheritanceFileId ) != null )
		{
			viewMap.put( WebConstants.RetainedProfits.RetainedProfitsInheritanceFileId,
						 sessionMap.remove( WebConstants.RetainedProfits.RetainedProfitsInheritanceFileId ) 
					   );
			loadData = true;
		}
		if( sessionMap.get( WebConstants.RetainedProfits.RetainedProfitsPersonId ) != null )
		{
			viewMap.put( WebConstants.RetainedProfits.RetainedProfitsPersonId,
						 sessionMap.remove( WebConstants.RetainedProfits.RetainedProfitsPersonId ) 
					   );
			loadData = true;
		}
		if( loadData  )
		{
			loadTabData();
		}
		if ( viewMap.get( WebConstants.RetainedProfits.DataList ) != null )
		{
			retainedProfitLists = (ArrayList<RetainedProfitDetails>) viewMap.get( WebConstants.RetainedProfits.DataList );
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, tabMode );
		}
		if ( retainedProfitLists != null )
		{
			viewMap.put( WebConstants.RetainedProfits.DataList , retainedProfitLists );
		}
	}	

	@SuppressWarnings("unchecked")
	private boolean isValid( List<String> errorMessages  ) throws Exception 
	{
		boolean valid = true;
//		boolean anySelected=false;
//		for ( RetainedProfitDetails blocking : retainedProfitLists) 
//		{
//			if( blocking.getSelected() != null && blocking.getSelected() )
//			{
//				anySelected = true;
//				break;
//			}
//		}
//		if( !anySelected )
//		{
//			errorMessages.add( CommonUtil.getBundleMessage( "BlockingRequest.msg.selectAtleastOneBlockingLine" ) );
//			return false;
//		}
		return valid;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onReleaseRetainedAmount()
	{
		try	
		{
			errorMessages = new ArrayList<String>();
			updateValuesFromMaps();
			
			if( !isValid( errorMessages) ){}
				
		}
		catch(Exception ex)
		{
			logger.LogException("onReleaseRetainedAmount|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE ) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}

	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<RetainedProfitDetails> getRetainedProfitLists() {
		return retainedProfitLists;
	}

	public void setRetainedProfitLists(List<RetainedProfitDetails> blockingList) {
		this.retainedProfitLists = blockingList;
	}

	public String getSumTotalRetainedProfits() {
		return sumTotalRetainedProfits;
	}

	public void setSumTotalRetainedProfits(String sumTotalRetainedProfits) {
		this.sumTotalRetainedProfits = sumTotalRetainedProfits;
	}

	public String getSumTotalRetainedActiveProfits() {
		return sumTotalRetainedActiveProfits;
	}

	public void setSumTotalRetainedActiveProfits(
			String sumTotalRetainedActiveProfits) {
		this.sumTotalRetainedActiveProfits = sumTotalRetainedActiveProfits;
	}

	public String getSumTotalRetainedReleasedProfits() {
		return sumTotalRetainedReleasedProfits;
	}

	public void setSumTotalRetainedReleasedProfits(
			String sumTotalRetainedReleasedProfits) {
		this.sumTotalRetainedReleasedProfits = sumTotalRetainedReleasedProfits;
	}

	
}
