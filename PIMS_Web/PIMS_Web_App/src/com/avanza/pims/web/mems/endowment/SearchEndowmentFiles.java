package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.Utils.EndowmentsFileSearchCriteria;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.EndowmentFile;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.endowment.EndowmentFilesService;
import com.avanza.ui.util.ResourceUtil;

public class SearchEndowmentFiles extends AbstractSearchBean 
{
	private static final long serialVersionUID = 1L;
	private final String DEFAULT_SORT_FIELD = "fileNumber";
	private final String DATA_LIST = "DATA_LIST";
	EndowmentFilesService service = new EndowmentFilesService();

	EndowmentsFileSearchCriteria criteria = new EndowmentsFileSearchCriteria();
	List<EndowmentFile> dataList = new ArrayList<EndowmentFile>();
	

	@SuppressWarnings("unchecked")
	@Override
	public void init() 
	{
		super.init();
		try 
		{
			
			if (!isPostBack()) 
			{
				initData();
			}
		} catch (Exception es) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void initData() throws Exception 
	{
		setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
		setSortField(DEFAULT_SORT_FIELD);
		setSortItemListAscending(false);
		checkDataInQueryString();
	}

	private void checkDataInQueryString() {
		checkPageMode();
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() 
	{

		if (request.getParameter(VIEW_MODE) != null) 
		{

			if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);

			}
			else if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);

			}
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onSingleSelect() 
	{
		try 
		{
			EndowmentFile obj = null;
			obj = (EndowmentFile) dataTable.getRowData();
			sessionMap.put(WebConstants.SELECTED_ROW,obj);
			executeJavascript("closeWindow();");
		}
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onSingleSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public void onMultiSelect() 
	{
		try 
		{
			dataList = getDataList();
			List<EndowmentFile> files = new ArrayList<EndowmentFile>();
			for(EndowmentFile obj: dataList)
			{
				if( obj.isSelected() )
				{
					files.add(obj);
				}
			}
			
			if(files.size() <= 0)
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add( CommonUtil.getBundleMessage("endowment.errMsg.noSelectMade") );
				return;
			}
			viewMap.put(WebConstants.SELECTED_ROWS, files);
			executeJavascript("closeWindow();");
		}
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onMultiSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public String onAdd() 
	{
		return "manage";
	}
	
	@SuppressWarnings("unchecked")
	public String onIssueNOL() 
	{
		try 
		{
			EndowmentFile row = (EndowmentFile) dataTable.getRowData();
			if (row != null) 
			{
				sessionMap.put(Constant.EndowmentFile.ENDOWMENT_FILE , row);
				return "issueEndowmentNOL";
			}
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onIssueNOL |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	public String onEdit() 
	{
		try 
		{
			EndowmentFile row = (EndowmentFile) dataTable.getRowData();
			if (row != null) 
			{
				sessionMap.put(Constant.EndowmentFile.ENDOWMENT_FILE , row);
				return "manage";
			}
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onEdit |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}

	public void onSearch() 
	{
		try 
		{
			pageFirst();
		} 
		catch (Exception e) 
		{
			logger.LogException("onSearch |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void doSearchItemList()
	{
		try {
				loadDataList();
			}
		    catch (Exception e){
		    	errorMessages = new ArrayList<String>(0);
		 		logger.LogException("doSearchItemList"+"|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		    }
	}

	@SuppressWarnings("unchecked")
	public void loadDataList() throws Exception 
	{

		int totalRows = 0;
		totalRows = service.getTotalNumberOfRecords( criteria );
		setTotalRows(totalRows);
		doPagingComputations();

		dataList = service.search(criteria, getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());
		if (	dataList== null || 
				dataList.isEmpty() 
		   ) 
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			forPaging(0);
		}
		
		this.setDataList( dataList );
		forPaging(getTotalRows());
	}



	@SuppressWarnings("unchecked")
	public List<EndowmentFile> getDataList() 
	{
		if(viewMap.get(DATA_LIST) != null)
		{
			dataList = (List<EndowmentFile>) viewMap.get(DATA_LIST);
		} 
		return dataList ;
	}

	@SuppressWarnings("unchecked")
	public void setDataList(List<EndowmentFile> list) 
	{
		this.dataList = list;
		if(this.dataList != null )
		{
			viewMap.put(DATA_LIST, list);
		}

	}

	
	@SuppressWarnings("unchecked")
	
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javascript);
	}

	public EndowmentsFileSearchCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(EndowmentsFileSearchCriteria criteria) {
		this.criteria = criteria;
	}

	
}
