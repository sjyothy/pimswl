package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlInputText;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.entity.Request;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.minors.TakharujManageBacking;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;

public class ReviewDetailsTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private RequestView requestView;
	private ReviewRequestView reviewRequestView;
	private List<ReviewRequestView> reviewRequestViewList;
	
	private HtmlDataTable reviewRequestViewDataTable;
	private HtmlInputText htmlOtherDescision ;
	
	private UtilityService utilityService;
   private boolean parentTakharuj;	
	private String tabMode;

	public ReviewDetailsTabBacking() 
	{
		requestView = new RequestView();
		reviewRequestView = new ReviewRequestView();
		reviewRequestViewList = new ArrayList<ReviewRequestView>(0);
		htmlOtherDescision = new HtmlInputText();
		reviewRequestViewDataTable = new HtmlDataTable();
		
		utilityService = new UtilityService();
		
		tabMode = WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_VIEW_ONLY;
	}
	
	@Override
	public void init() 
	{
		
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException("init --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws PimsBusinessException
	{
		// TAB MODE
		if ( viewMap.get( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY ) != null )
		{
			tabMode = (String) viewMap.get( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY );
		}
		
		// REQUEST VIEW
		if ( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = (RequestView) viewMap.get( WebConstants.REQUEST_VIEW );
		}
		
		// REVIEW REQUEST VIEW
		if ( viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW ) != null )
		{
			reviewRequestView = (ReviewRequestView) viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW );
		}		
		
		// REVIEW REQUEST VIEW LIST
		if ( viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW_LIST ) != null )
		{
			reviewRequestViewList = (List<ReviewRequestView>) viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW_LIST );
		}
		else
		{
			long requestId =0;
			if ( requestView != null && requestView.getRequestId() != null )
			{
				requestId = requestView.getRequestId();
			}
			else if( viewMap.get( WebConstants.REQUEST ) != null )
			{
				Request request = ( Request )viewMap.get( WebConstants.REQUEST ) ;
				if( request != null && request.getRequestId() != null )
				{
					requestId =  request.getRequestId()	;
				}
			}
			ReviewRequestView reviewRequestView = new ReviewRequestView();			
			reviewRequestView.setRequestId( requestId );
			reviewRequestViewList = utilityService.getReviewRequest(reviewRequestView);
			
			if ( reviewRequestViewList != null && reviewRequestViewList.size() > 0 )
			{
				this.reviewRequestView = reviewRequestViewList.get(0);
			}
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, tabMode );
		}
		
		// REVIEW REQUEST VIEW
		if ( reviewRequestView != null  )
		{
			viewMap.put( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW, reviewRequestView );
		}
		
		// REVIEW REQUEST VIEW LIST
		if ( reviewRequestViewList != null )
		{
			viewMap.put( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW_LIST, reviewRequestViewList );
		}
	}	
	

    @SuppressWarnings( "unchecked" )
	public String onSave()
	{
		
		try
		{
			errorMessages = new ArrayList<String>();
			if ( isValid() )
			{
				reviewRequestView.setReviewedOn( new Date() );
				reviewRequestView.setReviewedBy( CommonUtil.getLoggedInUser() );
				updateValuesToMaps();
			}
		}
		catch ( Exception e )
		{
			logger.LogException(  "onSave --- EXCEPTION --- ", e );
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			viewMap.put( WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES ,errorMessages);
		}
		
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private boolean isValid() 
	{
		boolean valid = true;
		Object otherDesc = htmlOtherDescision.getValue();
		List<String> errorMessages = new ArrayList<String>( 0 );
		// VALIDATION CHECKS STARTS HERE
		if(reviewRequestView.getDecision().compareTo(WebConstants.REVIEW_DECISION_OTHER_ID.toString())==0)
		{
			if(otherDesc!=null && otherDesc.toString().trim().compareTo("")!=0 )
			{
				 valid=true;
				 reviewRequestView.setDescisonDesc(otherDesc.toString());
			}
			else
			{
				
					
				valid = false;
				errorMessages.add( CommonUtil.getBundleMessage(WebConstants.REVIEW_DETAIL_TAB_ERROR_OTHER_MISSING) );
				 
			}
		}
		if ( reviewRequestView.getReviewDesc() == null ||  reviewRequestView.getReviewDesc().trim().length() <= 0  )
		{
			valid = false;
			errorMessages.add( CommonUtil.getBundleMessage("reviewDetailTab.errorReviewRequired" ) );
			
			
		}
		// VALIDATION CHECKS ENDS HERE
		if ( !valid )
		{
			if ( isParentTakharuj() )
			{
				TakharujManageBacking bean =  (TakharujManageBacking)getBean( "pages$takharujManage" );
				bean.setErrorMessages( errorMessages );
			}	
			else
			{
				viewMap.put(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES,errorMessages);
			}
		}
		return valid;
	}
	
	public Integer getReviewRequestViewListRecordSize()
	{
		return reviewRequestViewList.size();
	}
	
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE ) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}

	public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public ReviewRequestView getReviewRequestView() {
		return reviewRequestView;
	}

	public void setReviewRequestView(ReviewRequestView reviewRequestView) {
		this.reviewRequestView = reviewRequestView;
	}

	public List<ReviewRequestView> getReviewRequestViewList() {
		return reviewRequestViewList;
	}

	public void setReviewRequestViewList(
			List<ReviewRequestView> reviewRequestViewList) {
		this.reviewRequestViewList = reviewRequestViewList;
	}

	public HtmlDataTable getReviewRequestViewDataTable() {
		return reviewRequestViewDataTable;
	}

	public void setReviewRequestViewDataTable(
			HtmlDataTable reviewRequestViewDataTable) {
		this.reviewRequestViewDataTable = reviewRequestViewDataTable;
	}

	public UtilityService getUtilityService() {
		return utilityService;
	}

	public void setUtilityService(UtilityService utilityService) {
		this.utilityService = utilityService;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}

	public HtmlInputText getHtmlOtherDescision() {
		return htmlOtherDescision;
	}

	public void setHtmlOtherDescision(HtmlInputText htmlOtherDescision) {
		this.htmlOtherDescision = htmlOtherDescision;
	}

	public boolean isParentTakharuj() {

		if( viewMap.get( WebConstants.ReviewRequest.REVIEW_DETAILS_PARENT ) != null  && 
				viewMap.get( WebConstants.ReviewRequest.REVIEW_DETAILS_PARENT ).toString().equals(WebConstants.ReviewRequest.PARENT_TAKHRUJ ) 	
			  )
			    return true;
			else
				return false;
	}
	public boolean isParentReceiveShares() {

		if( viewMap.get( WebConstants.ReviewRequest.REVIEW_DETAILS_PARENT ) != null  && 
				viewMap.get( WebConstants.ReviewRequest.REVIEW_DETAILS_PARENT ).toString().equals(WebConstants.ReviewRequest.PARENT_RECEIVE_SHARES) 	
			  )
			    return true;
			else
				return false;
	}

	public void setParentTakharuj(boolean parentTakharuj) {
		this.parentTakharuj = parentTakharuj;
	}	
}
