package com.avanza.pims.web.mems;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.entity.EndowmentExpType;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.ui.util.ResourceUtil;

public class ManageEndowmentExpenseType extends AbstractMemsBean 
{
	private static final long serialVersionUID = 1L;

	EndowmentExpType pageObject = new EndowmentExpType();
	

	@SuppressWarnings("unchecked")
	@Override
	public void init() 
	{
		super.init();
		try 
		{
			if (!isPostBack()) 
			{
				initData();
			}
		} catch (Exception es) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void initData() throws Exception 
	{
		checkDataInQueryString();
		if( pageObject.getTypeId() != null )
		{
			pageObject.setCreatedOn(new Date());
			pageObject.setCreatedBy(getLoggedInUserId());
		}
		pageObject.setUpdatedOn(new Date());
		pageObject.setUpdatedBy(getLoggedInUserId());
	}

	private void checkDataInQueryString() {
		if(sessionMap.get(WebConstants.SELECTED_ROW) != null)
		{
			setPageObject(  (EndowmentExpType)sessionMap.remove(WebConstants.SELECTED_ROW)   ) ;  
		}
	}

	public boolean hasDoneErrors()
	{
		boolean hasErrors = false;
		
		if( 
			pageObject.getTypeNameAr() == null || pageObject.getTypeNameAr() .trim().length() <= 0 ||
			pageObject.getTypeNameEn() == null || pageObject.getTypeNameEn() .trim().length() <= 0 
		  )
        {
           	 errorMessages.add(
           			 		   ResourceUtil.getInstance().getProperty(
    																	   "endowmentExpenseType.msg.typeNameRequired"
    																  )
    							  );
           	 hasErrors = true;
        }
		
        if( pageObject.getDescription()  == null || pageObject.getDescription().trim().length() <= 0 )
        {
           	 errorMessages.add(
           			 		   ResourceUtil.getInstance().getProperty(
    																	   "endowmentExpenseType.msg.descRequired"
    																	 )
    							  );
           	 hasErrors = true;
        }
		
         
		return hasErrors;
	}

	@SuppressWarnings("unchecked")
	public void onDone()
	{
		errorMessages   = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try 
		{
			if ( hasDoneErrors() ) return;
			if( pageObject.getTypeId()  == null )
			{
				pageObject.setCreatedBy( getLoggedInUserId() );
				pageObject.setCreatedOn(  new Date() );
				pageObject.setIsDeleted( 0l );
				pageObject.setRecordStatus( 1l );
				pageObject.setIsSystem("0" );
				
			}
			pageObject.setUpdatedBy( getLoggedInUserId() );
			sessionMap.put( WebConstants.SELECTED_ROW,pageObject );
			executeJavascript("javaScript:closeWindow();");
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onDone|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javascript);
	}


	@SuppressWarnings("unchecked")
	public EndowmentExpType getPageObject() {
		if( viewMap.get( "pageObject" ) != null )
		{
			pageObject = ( EndowmentExpType )viewMap.get( "pageObject" ); 
		}
		return pageObject;
	}
	@SuppressWarnings("unchecked")
	public void setPageObject(EndowmentExpType pageObject) {
		this.pageObject = pageObject;
		if( this.pageObject  != null )
		{
			viewMap.put( "pageObject",pageObject ); 
		}
	}

	
}
