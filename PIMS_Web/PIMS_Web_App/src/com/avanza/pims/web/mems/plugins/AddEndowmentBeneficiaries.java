package com.avanza.pims.web.mems.plugins;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.Utils.generatorViews.MasarifSearchView;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.EndFileAsso;
import com.avanza.pims.entity.EndFileBen;
import com.avanza.pims.entity.Person;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.MasarifService;
import com.avanza.pims.ws.mems.endowment.EndowmentFilesService;
import com.avanza.ui.util.ResourceUtil;

public class AddEndowmentBeneficiaries extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	
	private EndowmentFilesService endowmentFileService;
	private String pageMode;
	
	EndFileAsso endowmentFileAsso;
	EndFileBen endowmentFileBen;
	private String hdnPersonId;
	private String hdnPersonName;
	private List<EndFileBen> dataList = new ArrayList<EndFileBen>();
	private HtmlDataTable dataTable;
	private String totalPercentage;
	public AddEndowmentBeneficiaries() 
	{
		endowmentFileService        = new EndowmentFilesService();
		endowmentFileBen        = new EndFileBen();
		pageMode                = Constant.EndowmentFile.MODE_READ_ONLY;
		dataTable               = new HtmlDataTable();
		totalPercentage         = "0.0";
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
			if( !isPostBack() )
			{
			 endowmentFileAsso.setUpdatedBy(getLoggedInUserId());
			}
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			pageMode = viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE).toString();
		}
		if( viewMap.get(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO) != null)
		{
			endowmentFileAsso = ( EndFileAsso )viewMap.get(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO);
			 
		}
		else if( sessionMap.get( Constant.EndowmentFile.ENDOWMENT_FILE_ASSO ) != null )
		{
			endowmentFileAsso = ( EndFileAsso )sessionMap.remove(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO);
			endowmentFileAsso  = endowmentFileService.getEndowmentFileAsso( endowmentFileAsso );
			dataList.addAll( endowmentFileAsso.getEndFileBens() );
			sumTotalPercentages();
			
		}
		if( viewMap.get( Constant.EndowmentFile.ENDOWMENT_FILE_BEN ) != null  )
		{
		  endowmentFileBen    = ( EndFileBen )viewMap.get( Constant.EndowmentFile.ENDOWMENT_FILE_BEN );
		}
		if ( viewMap.get( "dataList" ) != null )
		{
			dataList = (ArrayList<EndFileBen>)viewMap.get("dataList");
		}
		if( viewMap.get("totalPercentage" ) != null )
		{
			totalPercentage = viewMap.get("totalPercentage" ) .toString();
		}
		updateValuesToMaps();
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void sumTotalPercentages()throws Exception 
	{
		double totalPer= 0.0d;
		for (EndFileBen item : dataList) {
			if(item.getSharePercentageFile() != null )
			{
				totalPer += item.getSharePercentageFile();
			}
		}
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		totalPercentage = oneDForm.format(totalPer);
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( pageMode != null )
		{
		  viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, pageMode);
		}
		if( endowmentFileAsso != null  )
		{
		  viewMap.put(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO,endowmentFileAsso );
		}
		if( endowmentFileBen != null  )
		{
		  viewMap.put(	Constant.EndowmentFile.ENDOWMENT_FILE_BEN, endowmentFileBen );
		}
		if( dataList != null )
		{
			setTotalRows( dataList.size() );
			viewMap.put("dataList",dataList );
		}
		if(totalPercentage  != null )
		{
			viewMap.put("totalPercentage" ,totalPercentage );
		}
	}	
	
	public void populateValuesInObject() throws Exception
	{
		if( endowmentFileBen.getEndFileBenId()==null )
		{
			endowmentFileBen.setCreatedBy(	getLoggedInUserId()	);
		}
		endowmentFileBen.setUpdatedBy(	getLoggedInUserId()	);
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMasarafChange()
	{
		try	
		{
			updateValuesFromMaps();
			if ( !endowmentFileBen.getMasrafId().equals("-1") )
			{
				hdnPersonId = "" ;
				hdnPersonName = "";
				endowmentFileBen.setPerson( null );
			}
		}
		catch(Exception ex)
		{
			logger.LogException("onMasarafChange|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onPersonChanged()
	{
		try	
		{
			updateValuesFromMaps();
			if( hdnPersonId != null && hdnPersonId.trim().length() > 0  )
			{
				
				Person person = new Person();
				person.setPersonId(new Long( hdnPersonId  ));
				person.setFirstName(  hdnPersonName );
				endowmentFileBen.setPerson( person );
				
				endowmentFileBen.setMasraf(null);
				endowmentFileBen.setMasrafId( "-1" );
			}
			else
			{
				endowmentFileBen.setPerson( null );
			}
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	private boolean hasDoneErrors() throws Exception
	{
		boolean hasErrors = false;
		if( endowmentFileAsso.getShareToEndow() == null ||  endowmentFileAsso.getShareToEndow().compareTo( 0l ) <=0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.shareToEndowRequired"));
			hasErrors = true;
		}
		return hasErrors;
	}
    @SuppressWarnings( "unchecked" )
	public void onDone()
	{
		
		try
		{
			updateValuesFromMaps();
			errorMessages = new ArrayList<String>();
			populateValuesInObject();
			
			if( hasDoneErrors() )
			{return;}
			
			sessionMap.put(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO, endowmentFileAsso);
			executeJavaScript("sendDataToParent();");
		}
		catch ( Exception e )
		{
			logger.LogException( "onSave--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
    
    private boolean hasAddErrors() throws Exception
	{
		boolean hasErrors = false;
		boolean isGeneration = isGeneration();
		boolean isCharity = isCharity();
		boolean isShared     =  !isGeneration && !isCharity;
		
		boolean isPersonEmpty  			=  endowmentFileBen.getPerson() == null || endowmentFileBen.getPerson().getPersonId()== null  ;
		boolean isMasrafEmpty           =  endowmentFileBen.getMasraf() == null || endowmentFileBen.getMasraf().getMasrafId() == null ;
		
		boolean isPersonAndMasrafEmpty  =  isPersonEmpty && isMasrafEmpty;
		
		boolean isPersonAndMasrafFilled =    endowmentFileBen.getPerson() != null && endowmentFileBen.getPerson().getPersonId()!= null &&
											 endowmentFileBen.getMasraf() != null && endowmentFileBen.getMasraf().getMasrafId() != null ;
		if( isGeneration && isPersonEmpty )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentFileBen.msg.beneficiaryPersonRequired"));
			hasErrors = true;
		}
		else if( isCharity && isMasrafEmpty )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentFileBen.msg.masrafRequired"));
			hasErrors = true;
		}
		
		else if ( 	isShared && ( isPersonAndMasrafEmpty || isPersonAndMasrafFilled ) )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentFileBen.msg.personorMasrafRequired"));
			hasErrors = true;
		}
		boolean isAmountAndPercentageEmpty = 	( endowmentFileBen.getAmount() == null ||  endowmentFileBen.getAmount() <=0 ) && 
												( endowmentFileBen.getSharePercentageFile() == null || endowmentFileBen.getSharePercentageFile() <= 0 );
		boolean isAmountAndPercentageFilled = 	  endowmentFileBen.getAmount() != null &&  endowmentFileBen.getAmount() >0  && 
												  endowmentFileBen.getSharePercentageFile() != null && endowmentFileBen.getSharePercentageFile() > 0 ;
												  
		if( isAmountAndPercentageEmpty || isAmountAndPercentageFilled )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentFileBen.msg.percentageOrAmountRequired"));
			hasErrors = true;
		}
		else if(  endowmentFileBen.getSharePercentageFile() != null && endowmentFileBen.getSharePercentageFile() > 100  )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentFileBen.msg.percentageNotExceedHundred"));
			hasErrors = true;
		}
		
		if(  endowmentFileBen.getFromDate() != null && endowmentFileBen.getToDate() !=  null &&
				  endowmentFileBen.getFromDate().compareTo( endowmentFileBen.getToDate() ) >= 0
			    )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentFileBen.msg.fromExceedTo"));
			hasErrors = true;
		}
		return hasErrors;
	}

	public boolean isCharity() {
		boolean isCharity    =  endowmentFileAsso.getEndowmentPurpose() != null && endowmentFileAsso.getEndowmentPurpose().getPurposeId() != null &&
							    endowmentFileAsso.getEndowmentPurpose().getPurposeId().compareTo(Constant.EndowmentPurpose.CHARITY_ID)==0;
		return isCharity;
	}

	public boolean isGeneration() {
		boolean isGeneration =  endowmentFileAsso.getEndowmentPurpose() != null && endowmentFileAsso.getEndowmentPurpose().getPurposeId() != null &&
								endowmentFileAsso.getEndowmentPurpose().getPurposeId().compareTo(Constant.EndowmentPurpose.GENERATION_ID)==0;
		return isGeneration;
	}
	
    @SuppressWarnings( "unchecked" )
	public void onAdd()
	{
    	errorMessages = new ArrayList<String>();
		try
		{
			updateValuesFromMaps();
			if( hasAddErrors() ){return;}
			populateValuesInObject();
			endowmentFileBen.setIsEditing(	0	);
			endowmentFileBen.setEndFileAsso(endowmentFileAsso);
			endowmentFileService.persistEndFileBen( endowmentFileBen );
			dataList.add(0,  endowmentFileBen);
			sumTotalPercentages();
			sessionMap.put(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO, endowmentFileAsso);
			executeJavaScript("sendDataToParent();");
			clearFields();
			updateValuesToMaps();
		
		}
		catch ( Exception e )
		{
			logger.LogException( "onAdd--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
    
    private void clearFields()
    {
    	hdnPersonId  = "";
    	hdnPersonName ="";
    	endowmentFileBen = new EndFileBen();
    	endowmentFileBen.setMasrafId("-1");
    }
    
    @SuppressWarnings( "unchecked" )
	public void onEdit()
	{
    	errorMessages = new ArrayList<String>();
		try
		{
			updateValuesFromMaps();
			endowmentFileBen   = (	EndFileBen	)dataTable.getRowData();
			dataList.remove( endowmentFileBen );
			endowmentFileBen.setIsEditing( 1 );
			if( endowmentFileBen.getBeneficiaryName() != null && 
					 ( endowmentFileBen.getMasrafId()== null || endowmentFileBen.getMasrafId().equals("-1") )
			  )
			{
				hdnPersonName = endowmentFileBen.getBeneficiaryName();
				hdnPersonId   = endowmentFileBen.getPerson().getPersonId().toString();
			}
			populateValuesInObject();
			
			
			updateValuesToMaps();
		}
		catch ( Exception e )
		{
			logger.LogException( "onEdit--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
    @SuppressWarnings( "unchecked" )
	public void onDelete()
	{
    	errorMessages = new ArrayList<String>();
		try
		{
			updateValuesFromMaps();
			EndFileBen	 endowmentFileBen   = (	EndFileBen	)dataTable.getRowData();
			endowmentFileBen.setEndFileAsso(endowmentFileAsso);
			populateValuesInObject();
			endowmentFileBen.setIsDeleted("1");   
			endowmentFileService.persistEndFileBen( endowmentFileBen );	
			dataList.remove(endowmentFileBen);
			updateValuesToMaps();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDelete--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
    
    public boolean getIsPageModeUpdatable()
	{
		boolean isPageModeUpdatable = false;
		
		if ( pageMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isPageModeUpdatable = true;
		
		return isPageModeUpdatable;
	}
    @SuppressWarnings("unchecked")
	public List<SelectItem> getAllMasarif() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		try {
			if (viewMap.get(WebConstants.ALL_MASARIF) != null) {
				// get data from viewMap
				items = (List<SelectItem>) viewMap.get(WebConstants.ALL_MASARIF);
			} else {
				// get data from db and put prepared list in viewMap
				List<MasarifSearchView> masarif = null;
				masarif = new MasarifService().getAllMasraf();
				for (MasarifSearchView masraf : masarif) {
					SelectItem item = new SelectItem(masraf.getMasrafId()
							.toString(), masraf.getMasrafName());
					items.add(item);
				}
				Collections.sort(items, ListComparator.LIST_COMPARE);
				if (items != null && items.size() > 0) {
					viewMap.put(WebConstants.ALL_MASARIF, items);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return items;
	}
	public EndFileAsso getEndowmentFileAsso() 
	{
		return endowmentFileAsso;
	}

	public void setEndowmentFileAsso(EndFileAsso endowmentFileAsso) 
	{
		this.endowmentFileAsso = endowmentFileAsso;
	}

	public String getPageMode() 
	{
		return pageMode;
	}

	public void setPageMode(String pageMode) 
	{
		this.pageMode = pageMode;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public List<EndFileBen> getDataList() {
		return dataList;
	}

	public void setDataList(List<EndFileBen> dataList) {
		this.dataList = dataList;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public EndFileBen getEndowmentFileBen() {
		return endowmentFileBen;
	}

	public void setEndowmentFileBen(EndFileBen endowmentFileBen) {
		this.endowmentFileBen = endowmentFileBen;
	}

	public String getTotalPercentage() {
		return totalPercentage;
	}

	public void setTotalPercentage(String totalPercentage) {
		this.totalPercentage = totalPercentage;
	}
}
