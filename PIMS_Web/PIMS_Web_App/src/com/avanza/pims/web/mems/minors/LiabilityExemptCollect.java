package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.DisburseSrcService;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.mems.PersonalAccountTrxService;
import com.avanza.pims.ws.vo.mems.DisbursementSourceView;
import com.avanza.pims.ws.vo.mems.LiablitiesView;
import com.avanza.ui.util.ResourceUtil;
import com.google.gson.Gson;

public class LiabilityExemptCollect extends AbstractMemsBean 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<SelectItem> disSrcList;
	//Map<String,DisbursementSourceView> disSrcMap;
	LiablitiesView liablitiesVO;
	
	public LiabilityExemptCollect()
	{
		disSrcList = new ArrayList<SelectItem>();
		//disSrcMap = new HashMap<String,DisbursementSourceView>();
		liablitiesVO = new LiablitiesView();
	}
	@SuppressWarnings("unchecked")
	@Override
	public void init()
	{
		super.init();
		try
		{
			if( !isPostBack() )
			{
				initializeContext();
	
			}
		}
		catch( Exception e)
		{
			logger.LogException(" init--- CRASHED --- ", e);
		}
	}
	private void initializeContext() throws Exception 
	{
		getValuesFromSession();
		loadCombos();
		if( !isExemptMode() )
		{
			liablitiesVO.setBeneficiaryBalance( new PersonalAccountTrxService().getAmountByPersonId( liablitiesVO.getBeneficiaryId() ) );
		    setLiablitiesVO(liablitiesVO);
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void getValuesFromSession() throws Exception
	{
		if( sessionMap.get( WebConstants.LiabilityExemptCollect.MODE) != null &&  
			sessionMap.get( WebConstants.LiabilityExemptCollect.MODE).toString().length() > 0 
		  )
		{
			viewMap.put(WebConstants.LiabilityExemptCollect.MODE, sessionMap.get( WebConstants.LiabilityExemptCollect.MODE).toString() );
			sessionMap.remove( WebConstants.LiabilityExemptCollect.MODE);
		}
		if( sessionMap.get( WebConstants.LiabilityExemptCollect.LIABILITY_VO) != null )  
		{
				setLiablitiesVO( (LiablitiesView)sessionMap.get( WebConstants.LiabilityExemptCollect.LIABILITY_VO) );
				sessionMap.remove( WebConstants.LiabilityExemptCollect.LIABILITY_VO);
		}
	}
	public boolean isExemptMode() 
	{
		if( viewMap.get(WebConstants.LiabilityExemptCollect.MODE )!= null && 
			viewMap.get(WebConstants.LiabilityExemptCollect.MODE ).toString().equals(WebConstants.LiabilityExemptCollect.EXEMPT)  
		  )
		{
		  return true;
		}
		  return false;
	}
	public boolean isViewMode() 
	{
		if( viewMap.get(WebConstants.LiabilityExemptCollect.MODE )!= null && 
			viewMap.get(WebConstants.LiabilityExemptCollect.MODE ).toString().equals(WebConstants.LiabilityExemptCollect.VIEW)  
		  )
		{
		  return true;
		}
		  return false;
	}
	@SuppressWarnings( "unchecked" )
	private void loadCombos() throws Exception
	{
	  if( isExemptMode() )
	  {
	    loadDisbursementSrc();
	  }
	}
	@SuppressWarnings( "unchecked" )
	private void loadDisbursementSrc()throws Exception
	{
		DisburseSrcService disSrcSrvc = new DisburseSrcService();
		List<DisbursementSourceView> list = disSrcSrvc.getDisbursementSourceByView( new DisbursementSourceView(), null, null, null, null );
		for (DisbursementSourceView vo : list) 
		{
			//disSrcMap.put(vo.getSrcId(), vo);
			SelectItem item =  new SelectItem( vo.getSrcId(),
					                           isEnglishLocale() ? vo.getSrcNameEn() : vo.getSrcNameAr()
					                          );
			disSrcList.add(item);
		}
	    if( list != null  && list.size() > 0 ) 
	    {
	    	setDisSrcList( disSrcList );
	    	//setDisSrcMap( disSrcMap );
	    	Gson json = new Gson();
	    	liablitiesVO.setDisbursementSrcListJSON( json.toJson(list) );
		    setLiablitiesVO(liablitiesVO);
	    }
	}

	private boolean hasOnCollectionError()
	{
		boolean hasOnCollectionError = false;
		if( liablitiesVO.getBeneficiaryBalance() == null || 
			liablitiesVO.getBeneficiaryBalance().doubleValue() <= 0 ||	
			liablitiesVO.getBeneficiaryBalance().doubleValue() <= liablitiesVO.getBenefAmount().doubleValue() )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("liabilityExemptCollect.messages.balanceNotEnough"));
			hasOnCollectionError  = true;
		}
		return hasOnCollectionError ;
	}
	@SuppressWarnings( "unchecked" )
	public void onCollect()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		DisbursementService service;
		try	
		{	
			logger.logInfo("onCollect--- STARTED --- ");
			service = new DisbursementService();
			if( !hasOnCollectionError() )
			{
				liablitiesVO =  getLiablitiesVO();
				liablitiesVO.setUpdatedBy(CommonUtil.getLoggedInUser());
				liablitiesVO.setUpdatedOn(new Date() );
				service.collectLiability( liablitiesVO );
				viewMap.put(WebConstants.LiabilityExemptCollect.MODE, WebConstants.LiabilityExemptCollect.VIEW );
				successMessages.add(ResourceUtil.getInstance().getProperty("liabilityExemptCollect.messages.collectedSuccessfully"));
			}
			logger.logInfo("onCollect--- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException("onCollect--- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}	
	private boolean hasOnExemptionError()
	{
		boolean hasOnExemptionError = false;
		if( liablitiesVO.getExemptLiabilityFromDisburseSrcId() == null || liablitiesVO.getExemptLiabilityFromDisburseSrcId().equals("-1") )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("liabilityExemptCollect.messages.disSrcReq"));
			hasOnExemptionError  = true;
		}
		return hasOnExemptionError ;
	}
	@SuppressWarnings( "unchecked" )
	public void onExempt()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		DisbursementService service;
		try	
		{	
			logger.logInfo("onExempt --- STARTED --- ");
			service = new DisbursementService();
			if( !hasOnExemptionError() )
			{
				liablitiesVO =  getLiablitiesVO();
				liablitiesVO.setUpdatedBy(CommonUtil.getLoggedInUser());
				liablitiesVO.setUpdatedOn(new Date() );
				service.exemptLiability( liablitiesVO );
				viewMap.put(WebConstants.LiabilityExemptCollect.MODE, WebConstants.LiabilityExemptCollect.VIEW );
				successMessages.add(ResourceUtil.getInstance().getProperty("liabilityExemptCollect.messages.exemptedSuccessfully"));
			}
			logger.logInfo("onExempt --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException("onExempt --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked")
	public List<SelectItem> getDisSrcList() 
	{
		if( viewMap.get( "disSrcList" ) != null)
		{
			disSrcList =  (ArrayList<SelectItem>)viewMap.get( "disSrcList" );		
		}
		return disSrcList;
	}
    @SuppressWarnings( "unchecked")
	public void setDisSrcList(List<SelectItem> disSrcList) 
    {
		this.disSrcList = disSrcList;
		if( this.disSrcList != null  )
		{
			viewMap.put("disSrcList",this.disSrcList);
		}
	}
//    @SuppressWarnings( "unchecked")
//	public Map<String, DisbursementSourceView> getDisSrcMap() 
//	{
//		if( viewMap.get( "disSrcMap" ) != null)
//		{
//			disSrcMap =  (Map<String, DisbursementSourceView> )viewMap.get( "disSrcMap" );		
//		}
//		return disSrcMap;
//	}
//    @SuppressWarnings( "unchecked")
//	public void setDisSrcMap(Map<String, DisbursementSourceView> disSrcMap) 
//    {
//		this.disSrcMap = disSrcMap;
//		if( this.disSrcMap != null )
//		{
//			viewMap.put( "disSrcMap" , this.disSrcMap ); 
//		}
//	}
    @SuppressWarnings( "unchecked")
	public LiablitiesView getLiablitiesVO() 
	{
		if( viewMap.get( WebConstants.LiabilityExemptCollect.LIABILITY_VO ) != null )
		{
			liablitiesVO	= (LiablitiesView )viewMap.get( WebConstants.LiabilityExemptCollect.LIABILITY_VO ); 
		}
		return liablitiesVO;
	}
	@SuppressWarnings( "unchecked")
	public void setLiablitiesVO(LiablitiesView liablitiesVO) 
	{
		this.liablitiesVO = liablitiesVO;
		if( this.liablitiesVO != null )
		{
			viewMap.put( 	WebConstants.LiabilityExemptCollect.LIABILITY_VO , this.liablitiesVO ); 
		}
	}

	
	
}
