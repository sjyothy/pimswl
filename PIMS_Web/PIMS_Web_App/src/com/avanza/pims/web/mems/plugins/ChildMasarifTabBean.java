package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import javax.faces.component.html.HtmlDataTable;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.Utils.generatorViews.MasarifSearchView;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;

@SuppressWarnings("serial")
public class ChildMasarifTabBean extends AbstractMemsBean{

	private List<MasarifSearchView> childMasarifList = new ArrayList<MasarifSearchView>();
	private HtmlDataTable childMasarifTable;
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	private boolean englishLocale;
	private String ALL_CHILDS = "ALL_CHILDS" ;
	 @Override
		public void init() 
	   {
			super.init();
		}
	


	public TimeZone getTimeZone()
	{
		 return TimeZone.getDefault();
		
	}
	public String getDateFormat()
	{
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
    	LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
    	return localeInfo.getDateFormat();
		
	}
	@SuppressWarnings("unchecked")
	public void onOpenStatementOfAccountCriteriaPopup()
	{
		try
		{
			MasarifSearchView masarifView = ( MasarifSearchView )childMasarifTable.getRowData();
			
			if(masarifView == null || masarifView.getMasrafId() ==null) return;
			sessionMap.put(WebConstants.Masraf.MasrafStatement, masarifView);
			executeJavaScript( "openStatementofAccountMasrafCriteriaPopup();");
			
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenStatementOfAccountCriteriaPopup|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	public Integer getPaginatorMaxPages() {
		paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
		return paginatorMaxPages;
	}

	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	public Integer getRecordSize() {
		if(viewMap.containsKey("recordSize"))
		recordSize=Integer.parseInt(viewMap.get("recordSize").toString() );
		return recordSize;
	}

	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}

	public boolean isEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}

	public void setEnglishLocale(boolean englishLocale) {
		this.englishLocale = englishLocale;
	}
	@SuppressWarnings("unchecked")
	public List<MasarifSearchView> getChildMasarifList() {
		if(viewMap.get(ALL_CHILDS) != null )
			childMasarifList = (List<MasarifSearchView>) viewMap.get(ALL_CHILDS); 
		return childMasarifList;
	}
	public void setChildMasarifList(List<MasarifSearchView> childMasarifList) {
		this.childMasarifList = childMasarifList;
	}
	public HtmlDataTable getChildMasarifTable() {
		return childMasarifTable;
	}
	public void setChildMasarifTable(HtmlDataTable childMasarifTable) {
		this.childMasarifTable = childMasarifTable;
	}
	
	
}
