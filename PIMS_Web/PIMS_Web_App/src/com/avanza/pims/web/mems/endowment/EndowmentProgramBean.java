package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.bpel.proxy.MEMSEndowmentProgramsBPELPortClient;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.ProgramMilestoneManager;
import com.avanza.pims.entity.DonationBox;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.EndowmentProgram;
import com.avanza.pims.entity.ProgramMilestone;
import com.avanza.pims.entity.ProgramRelatedBoxes;
import com.avanza.pims.entity.ProgramRelatedMasarif;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.EndowmentTransformUtil;
import com.avanza.pims.ws.mems.endowment.EndowmentProgramService;
import com.avanza.pims.ws.vo.DonationBoxView;
import com.avanza.pims.ws.vo.ProgramCollectionsView;
import com.avanza.ui.util.ResourceUtil;


public class EndowmentProgramBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(EndowmentProgramBean.class);
	HttpServletRequest request;
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	
	
	private static final String PAGE_MODE_APPROVAL_REQUIRED = "APPROVAL_REQUIRED";
	private static final String PAGE_MODE_FINANCE_REJECTED = "FINANCE_REJECTED";
	private static final String PAGE_MODE_RESUBMITTED = "RESUBMITTED";
	private static final String PAGE_MODE_FINANCE = "FINANCE";
	private static final String PAGE_MODE_PUBLISH = "PUBLISH";
	private static final String PAGE_MODE_CLOSE = "CLOSE";
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_MASARIF = "masarifTab";
	private static final String TAB_MILESTONE = "mileStoneTab";
	private static final String TAB_BOXES   = "boxesTab";
	
    private String pageTitle;
    private String pageMode;
    private boolean viewModePopup;
    private String hdnPersonId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;
    
    private String hdnFilePersonId;
    private String hdnFilePersonName;
    private String txtRemarks;
    private EndowmentProgramService endowmentProgramService = new EndowmentProgramService();
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	
	private EndowmentProgram endowmentProgram;
	private List<ProgramRelatedMasarif> programRelatedMasarif;
	private ProgramMilestone programMilestone = new ProgramMilestone();
	private List<ProgramMilestone> listProgramMilestone= new ArrayList<ProgramMilestone>();
	private List<ProgramRelatedBoxes> programRelatedBoxes = new ArrayList<ProgramRelatedBoxes>();
	private List<DonationBox> donationBoxes = new ArrayList<DonationBox>();
	private List<ProgramCollectionsView> programCollections = new ArrayList<ProgramCollectionsView>();
	private String sumExpPercentagesStr = "0.0";
	private String sumPercentagesStr = "0.0";
	private HtmlDataTable dataTableBox;
	private HtmlDataTable dataTableProgramCollections;
    private HtmlDataTable dataTableProgramMilestone;
	public EndowmentProgramBean()
	{
		request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
				updateValuesFromMap();
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,Constant.EndowmentProgram.PROCEDURE_ENDOWMENT_PROGRAM);
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, Constant.EndowmentProgram.PROCEDURE_ENDOWMENT_PROGRAM);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_ENDOWMENT_PROGRAM);
		viewMap.put("noteowner", Constant.EndowmentProgram.NOTES_OWNER);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		loadAttachmentsAndComments( null );
		viewModePopup = false;
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
		  getDataFromTaskList();
		}
		else if ( sessionMap.get( Constant.EndowmentProgram.ENDOWMENT_PROGRAM) != null )
		{
		  getDataFromSearch();
		}
		else if(request.getParameter("programId")!=null)
		{
			long programId = Long.valueOf( request.getParameter("programId").toString());
			getEndowmentProgramDetails( programId );
			viewModePopup=true;
		}
		else
		{
			setDataForFirstTime();
		}
		updateValuesFromMap();
		getPageModeFromProgramStatus();
		onMasarifTab();
	}
	
	@SuppressWarnings( "unchecked" )
	public void prerender()
	{
		try
		{
			getErrorMessagesFromTab();
		}
		catch ( Exception e )
		{
			logger.LogException( "prerender|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		endowmentProgram = new EndowmentProgram();
		endowmentProgram.setCreatedBy(  getLoggedInUserId()  );
		endowmentProgram.setUpdatedBy(  getLoggedInUserId()  );
		endowmentProgram.setStatusId( Constant.EndowmentProgramStatus.NEW_ID );

	}
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromProgramStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		if( this.endowmentProgram == null  || 
			this.endowmentProgram.getEndowmentProgramId() == null ||
			this.endowmentProgram.getStatusId() == null 
			
		  ) 
		{ return; }
		
		boolean hasUserTask = getUserTask() != null;
		
		if( getStatusApprovalRequired() && hasUserTask  )
		{
			this.setPageMode( PAGE_MODE_APPROVAL_REQUIRED );
			
		}
		else if( getStatusFinanceRejected() && hasUserTask  )
		{
			this.setPageMode( PAGE_MODE_FINANCE_REJECTED);
		}
		else if( getStatusResubmitted() && hasUserTask  )
		{
			
			this.setPageMode( PAGE_MODE_RESUBMITTED );
		}
		else if( getStatusPublishRequired()  && hasUserTask  )
		{
			this.setPageMode( PAGE_MODE_PUBLISH );
		}
		else if( getStatusDisbursementRequired()   && hasUserTask  )
		{
			
			this.setPageMode( PAGE_MODE_FINANCE );
		}
		else if( getStatusPublished() && endowmentProgram.getEndDate() == null && !hasUserTask)
		{
			
				this.setPageMode( PAGE_MODE_CLOSE );
			
		}
		else if( getStatusPublished() && endowmentProgram.getEndDate() != null && !hasUserTask)
		{
			    this.setPageMode( PAGE_MODE_VIEW );
		} 
		else if( getStatusNotified() && hasUserTask && endowmentProgram.getHasCloseTaskCreated().equals("1") )
		{
				this.setPageMode( PAGE_MODE_CLOSE );
		}
		else if( getStatusCompleted() || ( ! getStatusNew() && !hasUserTask) ) 
		{
			this.setPageMode( PAGE_MODE_VIEW );
		}
	}
	public boolean getStatusNew() {
		return this.endowmentProgram.getStatusId().compareTo(  Constant.EndowmentProgramStatus.NEW_ID ) == 0;
	}

	public boolean getStatusCompleted() {
		return this.endowmentProgram.getStatusId().compareTo(  Constant.EndowmentProgramStatus.CLOSED_ID ) == 0;
	}

	public boolean getStatusApprovalRequired() {
		return this.endowmentProgram.getStatusId().compareTo(  Constant.EndowmentProgramStatus.APPROVAL_REQUIRED_ID ) == 0;
	}
	public boolean getStatusPublishRequired() {
		return this.endowmentProgram.getStatusId().compareTo(  Constant.EndowmentProgramStatus.PUBLISH_REQUIRED_ID ) == 0;
	}
	public boolean getStatusPublished() {
		return this.endowmentProgram.getStatusId().compareTo(  Constant.EndowmentProgramStatus.PUBLISHED_ID ) == 0;
	}
	public boolean getStatusFinanceRejected() {
		return this.endowmentProgram.getStatusId().compareTo(  Constant.EndowmentProgramStatus.FINANCE_REJECTED_ID) == 0;
	}
	public boolean getStatusDisbursementRequired() {
		return this.endowmentProgram.getStatusId().compareTo(  Constant.EndowmentProgramStatus.APPROVED_DISBURSEMENT_REQ_ID ) == 0;
	}
	public boolean getStatusResubmitted() {
		return this.endowmentProgram.getStatusId().compareTo(  Constant.EndowmentProgramStatus.RESUBMITTED_ID) == 0;
	}
	public boolean getStatusNotified() {
		return this.endowmentProgram.getStatusId().compareTo(  Constant.EndowmentProgramStatus.NOTIFIED_ID) == 0;
	}
	
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( Constant.EndowmentProgram.ENDOWMENT_PROGRAM ) != null )
		{
			endowmentProgram  = ( EndowmentProgram )viewMap.get( Constant.EndowmentProgram.ENDOWMENT_PROGRAM ) ;
			endowmentProgram.setUpdatedBy( getLoggedInUserId() );
		}
		if( viewMap.get( Constant.EndowmentProgram.ENDOWMENT_PROGRAM_MASARIF ) != null )
		{
			programRelatedMasarif = ( List<ProgramRelatedMasarif> )viewMap.get( Constant.EndowmentProgram.ENDOWMENT_PROGRAM_MASARIF ) ;
		}
		if( viewMap.get( Constant.EndowmentProgram.ENDOWMENT_PROGRAM_BOXES ) != null )
		{
//			programRelatedBoxes = ( List<ProgramRelatedBoxes> )viewMap.get( Constant.EndowmentProgram.ENDOWMENT_PROGRAM_BOXES ) ;
			donationBoxes = ( List<DonationBox> )viewMap.get( Constant.EndowmentProgram.ENDOWMENT_PROGRAM_BOXES ) ;
		}
		if( viewMap.get( Constant.EndowmentProgram.PROGRAM_MILESTONE ) != null )
		{
			listProgramMilestone = ( List<ProgramMilestone> )viewMap.get( Constant.EndowmentProgram.PROGRAM_MILESTONE ) ;
		}
		if( viewMap.get( "viewModePopup" ) != null )
		{
			viewModePopup = (Boolean)viewMap.get( "viewModePopup" );
		}
		if( viewMap.get( "programMilestone" ) != null )
		{
			
			programMilestone = ( ProgramMilestone )viewMap.get( "programMilestone" ) ;
			if( programMilestone.getCreatedBy() == null )
			{
				programMilestone.setCreatedBy( getLoggedInUserId() );
			}
			programMilestone.setUpdatedBy( getLoggedInUserId() );
		}
		if( viewMap.get( "programCollections" ) != null )
		{
			programCollections = (ArrayList<ProgramCollectionsView>)viewMap.get( "programCollections" );  
		}
		
		if( viewMap.get("sumExpPercentagesStr" ) != null )
		{
			sumExpPercentagesStr = viewMap.get("sumExpPercentagesStr" ).toString();
		}
		if( viewMap.get("sumPercentagesStr" ) != null )
		{
			sumPercentagesStr = viewMap.get("sumPercentagesStr" ).toString();
		}
		 updateValuesToMap();

	}
	
	@SuppressWarnings("unchecked")
	private void getErrorMessagesFromTab() throws Exception
	{
		if(viewMap.get(Constant.EndowmentProgram.ERR_MASARIF_TAB) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(Constant.EndowmentProgram.ERR_MASARIF_TAB);
			errorMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_MASARIF);
		}
		else if(viewMap.get(Constant.EndowmentProgram.SUCCESS_MASARIF_TAB) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(Constant.EndowmentProgram.SUCCESS_MASARIF_TAB);
			successMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_MASARIF);
		}
		else if(viewMap.get(Constant.EndowmentProgram.ERR_BOXES_TAB) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(Constant.EndowmentProgram.ERR_BOXES_TAB);
			errorMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_BOXES);
		}
		else if(viewMap.get(Constant.EndowmentProgram.SUCCESS_BOXES_TAB) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(Constant.EndowmentProgram.SUCCESS_BOXES_TAB);
			successMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_BOXES);
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( endowmentProgram != null )
		{
		  viewMap.put( Constant.EndowmentProgram.ENDOWMENT_PROGRAM, endowmentProgram );
		}
		if( programRelatedMasarif != null )
		{
		  viewMap.put( Constant.EndowmentProgram.ENDOWMENT_PROGRAM_MASARIF , programRelatedMasarif  );
		}
//		if( programRelatedBoxes != null )
//		{
//		  viewMap.put( Constant.EndowmentProgram.ENDOWMENT_PROGRAM_BOXES, programRelatedBoxes );
//		}
		if( donationBoxes != null )
		{
		  viewMap.put( Constant.EndowmentProgram.ENDOWMENT_PROGRAM_BOXES, donationBoxes );
		}
		if( listProgramMilestone != null )
		{
		  viewMap.put( Constant.EndowmentProgram.PROGRAM_MILESTONE , listProgramMilestone  );
		}
		if( programMilestone != null )
		{
			viewMap.put( "programMilestone",programMilestone  );
		}
		if( programCollections != null )
		{
			viewMap.put( "programCollections",programCollections);
		}
		if(viewModePopup)
		{
		  viewMap.put( "viewModePopup" ,viewModePopup);
		}
//		else
//		{
//			viewMap.put( "viewModePopup" ,false);
//		}
	}
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		endowmentProgram = ( EndowmentProgram )sessionMap.remove( Constant.EndowmentProgram.ENDOWMENT_PROGRAM);
		if( this.endowmentProgram !=null )
		{
		  getEndowmentProgramDetails( endowmentProgram.getEndowmentProgramId() );
		}
	}
	
	
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  UserTask userTask = ( UserTask )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  setUserTask(userTask);
	  if( userTask.getTaskAttributes().get( Constant.EndowmentProgram.ENDOWMENT_PROGRAM_ID) == null ){ return; }
	  long  id = new Long  ( userTask.getTaskAttributes().get( Constant.EndowmentProgram.ENDOWMENT_PROGRAM_ID ) );
	  getEndowmentProgramDetails( id );
	  
	  
    }
	
	@SuppressWarnings( "unchecked" )
	private void getEndowmentProgramDetails( long id ) throws Exception 
	{
		endowmentProgram = endowmentProgramService.getEndowmentProgramById(  id );
		getRelatedMasarif();
		getRelatedBoxes();
		updateValuesToMap();
		loadAttachmentsAndComments( id );
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(endowmentProgram.getEndowmentProgramId() .toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = Constant.EndowmentProgram.NOTES_OWNER;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, endowmentProgram.getEndowmentProgramId()  );
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = Constant.EndowmentProgram.NOTES_OWNER;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    		
	    	  CommonUtil.saveRemarksAsComments(endowmentProgram.getEndowmentProgramId() , txtRemarks, notesOwner) ;
	    	NotesController.saveNotes(notesOwner, endowmentProgram.getEndowmentProgramId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }

	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( endowmentProgram!= null &&  endowmentProgram.getEndowmentProgramId() != null)
		 {
			 loadAttachmentsAndComments( endowmentProgram.getEndowmentProgramId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	
	@SuppressWarnings( "unchecked" )
	public void onMasarifTab()
	{
		try	
		{

			updateValuesFromMap();
			getRelatedMasarif();
			updateValuesToMap();
            tabPanel.setSelectedTab( TAB_MASARIF);
		}
		catch(Exception ex)
		{
			logger.LogException("onMasarifTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	

	@SuppressWarnings( "unchecked" )
	public void onProgramCollectionsTab()
	{
		try	
		{
			updateValuesFromMap();
			programCollections =  endowmentProgramService.getProgramRealatedCollectionsByProgram( endowmentProgram ) ;
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onProgramCollectionsTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	
	/**
	 * @throws Exception
	 */
	private void getRelatedMasarif() throws Exception {
		programRelatedMasarif = endowmentProgramService.getProgramMasarif( endowmentProgram ) ;
	}

	
	
	@SuppressWarnings( "unchecked" )
	public boolean hasAddProgramMilestoneError() throws Exception
	{
		boolean hasError =false;
		double sumExpPercentage = 0.0d;
		double sumPercentage = 0.0d;
		for (ProgramMilestone obj: listProgramMilestone ) 
		{
		  sumExpPercentage += obj.getExpCompPercentage();
		  if( obj.getCompPercentage() != null )
		  {
			  sumPercentage += obj.getCompPercentage();
		  }
		}
		
		if( programMilestone.getExpCompletionDate() == null )
		{
			errorMessages.add(CommonUtil.getBundleMessage("endowmentProgram.msg.expCompDateRequired"));
			hasError = true;
		}
		if( programMilestone.getExpCompPercentageStr()  == null || programMilestone.getExpCompPercentageStr().trim().length() <= 0 )
		{
			errorMessages.add(CommonUtil.getBundleMessage("endowmentProgram.msg.expCompPercentRequired"));
			hasError = true;
		}
		else 
		{
			sumExpPercentage  = programMilestone.getExpCompPercentage() + sumExpPercentage ;
			if( sumExpPercentage > 100 )
			{
				errorMessages.add(CommonUtil.getBundleMessage("endowmentProgram.msg.expPercentageValidation"));
				hasError = true;
			}
		}
		if( programMilestone.getDescription()   == null || programMilestone.getDescription().trim().length() <= 0 )
		{
			errorMessages.add(CommonUtil.getBundleMessage("endowmentProgram.msg.activityRequired"));
			hasError = true;
		}
		
		if( programMilestone.getStatusId() == null ||  programMilestone.getStatusId().equals("-1") )
		{
			errorMessages.add(CommonUtil.getBundleMessage("endowmentProgram.msg.statusRequired"));
			hasError = true;
		}
		else if ( new Long( programMilestone.getStatusId() ).compareTo( Constant.ProgramMilestoneStatus.COMPLETED_ID )==0  )
		{
			if( programMilestone.getCompletionDate() == null )
			{
				errorMessages.add(CommonUtil.getBundleMessage("endowmentProgram.msg.CompDateRequired"));
				hasError = true;
			}
			if( programMilestone.getCompPercentageStr()  == null || programMilestone.getCompPercentageStr().trim().length() <= 0 )
			{
				errorMessages.add(CommonUtil.getBundleMessage("endowmentProgram.msg.PercentRequired"));
				hasError = true;
			}
			
			if( programMilestone.getCompletionRemarks()  == null || programMilestone.getCompletionRemarks().trim().length() <= 0 )
			{
				errorMessages.add(CommonUtil.getBundleMessage("endowmentProgram.msg.CompremarksRequired"));
				hasError = true;
			}
			
			
		}
		
		if( programMilestone.getCompPercentageStr()  != null && programMilestone.getCompPercentageStr().trim().length() > 0 )
		{
			sumPercentage  = programMilestone.getCompPercentage() + sumPercentage ;
			if( sumPercentage > 100 )
			{
				errorMessages.add(CommonUtil.getBundleMessage("endowmentProgram.msg.percentageValidation") );
				hasError = true;
			}
		}
		if( hasError )
		{
			tabPanel.setSelectedTab( TAB_MILESTONE );
		}
		return hasError;
	}
	@SuppressWarnings( "unchecked" )
	public void onAddProgramMilestone()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		
		try	
		{
			updateValuesFromMap();
			ProgramMilestoneManager mgr = new ProgramMilestoneManager();
			if( hasAddProgramMilestoneError()  ) return;
			programMilestone.setEndowmentProgram(endowmentProgram);
			programMilestone.setIsDeleted( Constant.DEFAULT_IS_DELETED );
			mgr.persist(programMilestone);
            programMilestone = new ProgramMilestone();
            getProgramMileStones() ;
			updateValuesToMap();
     
		}
		catch(Exception ex)
		{
			logger.LogException("onAddProgramMilestone|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	@SuppressWarnings("unchecked")
	private void getSumPercentages() throws Exception
	{
		double sumExpPercentage = 0.0d;
		double sumPercentage = 0.0d;
		for (ProgramMilestone obj: listProgramMilestone ) 
		{
		  sumExpPercentage += obj.getExpCompPercentage();
		  if( obj.getCompPercentage() != null )
		  {
			  sumPercentage += obj.getCompPercentage();
		  }
		}
		sumExpPercentagesStr = String.valueOf(sumExpPercentage);
		viewMap.put("sumExpPercentagesStr", sumExpPercentagesStr);
		
		sumPercentagesStr = String.valueOf(sumPercentage);
		viewMap.put("sumPercentagesStr", sumPercentagesStr);
	}
	
	@SuppressWarnings( "unchecked" )
	public boolean hasDeleteMilestoneError() throws Exception
	{
			
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public void onDeleteProgramMilestone()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		ProgramMilestoneManager mgr = new ProgramMilestoneManager();
		try	
		{
			updateValuesFromMap();
			
			if( hasDeleteMilestoneError()  ) return;
			
			ProgramMilestone obj = (ProgramMilestone)dataTableProgramMilestone.getRowData();

			if( obj.getMileStoneId() != null )
			{
				obj.setIsDeleted(1l);
				obj.setUpdatedBy( getLoggedInUserId() );
				mgr.persist(obj);
			}

			
			getProgramMileStones() ;
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onDeleteProgramMilestone|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}


	@SuppressWarnings( "unchecked" )
	public void onEditProgramMilestone()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		
		try	
		{
			updateValuesFromMap();
//			if( hasDeleteMilestoneError()  ) return;
			programMilestone = (ProgramMilestone)dataTableProgramMilestone.getRowData();
			listProgramMilestone.remove(programMilestone );
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onEdiProgramMilestone|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}


	@SuppressWarnings( "unchecked" )
	public void onProgramMilestoneTab()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		
		try	
		{

			updateValuesFromMap();
			getProgramMileStones() ;
			updateValuesToMap();
            tabPanel.setSelectedTab( TAB_MILESTONE );
		}
		catch(Exception ex)
		{
			logger.LogException("onProgramMilestoneTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	

	
	private void getProgramMileStones() throws Exception 
	{
		if(endowmentProgram.getEndowmentProgramId() != null )
		{
			listProgramMilestone = endowmentProgramService.getProgramMileStone( endowmentProgram ) ;
			getSumPercentages();
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onBoxesTab()
	{
		try	
		{

			updateValuesFromMap();
			if( programRelatedBoxes == null || programRelatedBoxes.size() <= 0  )
			{
				getRelatedBoxes();
			}
			if( donationBoxes == null || donationBoxes.size() <= 0  )
			{
				getRelatedBoxes();
			}
			updateValuesToMap();
            tabPanel.setSelectedTab( TAB_BOXES	);
		}
		catch(Exception ex)
		{
			logger.LogException("onBoxesTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	private void getRelatedBoxes() throws Exception {
//		programRelatedBoxes = endowmentProgramService.getProgramBoxes( endowmentProgram ) ;
		donationBoxes  = endowmentProgramService.getDonationBoxesByProgramId( endowmentProgram ) ;
	}
		
	@SuppressWarnings( "unchecked" )
	public void onDeleteBox()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			ProgramRelatedBoxes box =  (ProgramRelatedBoxes)dataTableBox.getRowData();
			if( box.getProgRelBoxId() == null)
			{
				programRelatedBoxes.remove(box);
			}
			else
			{
				endowmentProgramService.deleteBox(box);
			}
			getEndowmentProgramDetails( endowmentProgram.getEndowmentProgramId() );
			getProgramRelatedBoxes();
			successMessages.add( ResourceUtil.getInstance().getProperty("endowmentProgram.msg.boxDeleted"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onDeleteBox--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchEndowments()
	{
		errorMessages = new ArrayList<String>();
		try	
		{
			updateValuesFromMap();
			Endowment endowment = (Endowment)sessionMap.remove(WebConstants.SELECTED_ROW);
			if( endowment.getCostCenter() == null || endowment.getCostCenter().trim().length() <= 0  )
			{
				errorMessages.add(CommonUtil.getBundleMessage("generalCollection.msg.endowmentNotCostCenter"));
				return;
			}
			endowmentProgram.setEndowment(endowment);
			updateValuesToMap();
	
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchEndowments|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchDonationBox()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			if( sessionMap.get(WebConstants.SELECTED_ROWS)== null )
			{return;}
			List<DonationBoxView> list= (ArrayList<DonationBoxView>)sessionMap.remove(WebConstants.SELECTED_ROWS);
			for (DonationBoxView newBoxView: list) 
			{
				com.avanza.pims.entity.DonationBox newBox         = new com.avanza.pims.entity.DonationBox();
				EndowmentTransformUtil.transformToDonationBox(newBox, newBoxView);
				newBox.setEndowmentProgram(endowmentProgram);
//				ProgramRelatedBoxes boxes = new ProgramRelatedBoxes();
//				boxes.setEndowmentProgram(endowmentProgram);
//				boxes.setDonationBox(newBox);
//				programRelatedBoxes.add(  0,boxes);
				donationBoxes.add(0 ,newBox);
			}
			successMessages.add( ResourceUtil.getInstance().getProperty( "endowmentProgram.msg.boxAdded" ) );
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromDonationBox --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if(endowmentProgram== null || endowmentProgram.getEndowmentProgramId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(Constant.EndowmentProgram.NOTES_OWNER,endowmentProgram.getEndowmentProgramId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if(  hasBasicInfoError() )
		{
			hasSaveErrors = true;
			//tabPanel.setSelectedTab(TAB_BASIC_INFO);
		}

//		10)	If program manager is not selected then following message should be displayed to user and system should switch from the current tab to Basic Info Tab
//
//		“Program manager is required”
//
//		11)	If sum of all masraf percentage is less than 0 then following message should be displayed to user and system should switch from the current tab to Related Masarif Tab
//
//		“Sum of all masarif percentage must be equal to 100”
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}

		return hasSaveErrors;
		
	}

	private boolean hasBasicInfoError()throws Exception 
	{
		boolean hasSaveErrors=false;
		if( endowmentProgram.getProgName() == null || endowmentProgram.getProgName().trim().length() <= 0 )
    	{
    		errorMessages.add( ResourceUtil.getInstance().getProperty( "endowmentProgram.msg.nameRequired" ) );
    		hasSaveErrors =  true;
    	}

		if( endowmentProgram.getPublishDate() == null )
    	{
    		errorMessages.add( ResourceUtil.getInstance().getProperty( "endowmentProgram.msg.publishDateRequired" ) );
    		hasSaveErrors =  true;
    	}
		else if( endowmentProgram.getPublishDate().compareTo( new Date() )< 0 )
    	{
    		errorMessages.add( ResourceUtil.getInstance().getProperty( "endowmentProgram.msg.publishDateLessThanToday" ) );
    		hasSaveErrors =  true;
    	}

		else if( endowmentProgram.getStartDate() != null &&
			endowmentProgram.getPublishDate()!= null &&
			endowmentProgram.getStartDate().compareTo(endowmentProgram.getPublishDate() )> 0 
		   )
    	{
    		errorMessages.add( ResourceUtil.getInstance().getProperty( "endowmentProgram.msg.startdateLessthanPublishDate" ) );
    		hasSaveErrors =  true;
    	}
		
		else if( endowmentProgram.getEndDate()     != null && 
			endowmentProgram.getPublishDate() != null &&
			endowmentProgram.getEndDate().compareTo(endowmentProgram.getPublishDate() ) < 0 
		   )
    	{
    		errorMessages.add( ResourceUtil.getInstance().getProperty( "endowmentProgram.msg.enddateGreaterthanPublishDate" ) );
    		hasSaveErrors =  true;
    	}
		else if( endowmentProgram.getEndDate()     != null && 
				endowmentProgram.getStartDate() != null &&
				endowmentProgram.getEndDate().compareTo(endowmentProgram.getStartDate() ) < 0 
			   )
	    {
	    	errorMessages.add( ResourceUtil.getInstance().getProperty( "endowmentProgram.msg.enddateGreaterthanStartDate" ) );
	    	hasSaveErrors =  true;
	    }

		if( endowmentProgram.getProgramBudget() == null ||  endowmentProgram.getProgramBudget() < 0l )
		{
			errorMessages.add( ResourceUtil.getInstance().getProperty( "endowmentProgram.msg.budgetRequired" ) );
	    	hasSaveErrors =  true;
		}
		
		if( endowmentProgram.getEndProgDisSrc()== null ||  endowmentProgram.getEndProgDisSrc().getEndProgDisSrcId() == null )
		{
			errorMessages.add( ResourceUtil.getInstance().getProperty( "endowmentProgram.msg.budgetSrcRequired" ) );
	    	hasSaveErrors =  true;
		}
		if( endowmentProgram.getManager() == null || endowmentProgram.getManager().equals("-1") )
		{
			errorMessages.add( ResourceUtil.getInstance().getProperty( "endowmentProgram.msg.managerRequired" ) );
	    	hasSaveErrors =  true;
		}
		return hasSaveErrors;
	}

	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			saveProgramInTransaction();
			getEndowmentProgramDetails( endowmentProgram.getEndowmentProgramId() );
			successMessages.add( ResourceUtil.getInstance().getProperty("endowmentProgram.msg.savedSuccessfully"));
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	
	private void saveProgramInTransaction() throws Exception 
	{
		try
		{
			
			Long status = Constant.EndowmentProgramStatus.NEW_ID;
            if(endowmentProgram.getStatusId() != null)
            {
            	status = null;
            }
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistEndowmentProgram( status );
			updateValuesToMap();
			saveCommentsAttachment( "endowmentProgram.event.ProgramSaved");
			
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistEndowmentProgram(Long status) throws Exception 
	{
	
		if(status!=null) endowmentProgram.setStatusId(status);
		if( programRelatedMasarif != null )
		{
			Set<ProgramRelatedMasarif > set = new HashSet<ProgramRelatedMasarif >(  );
			for (ProgramRelatedMasarif related : programRelatedMasarif) 
			{
				if( related.getPercentage() != null && related.getPercentage().compareTo(0D)>0 )
				{
					
					set.add(related);
					endowmentProgram.setProgramRelatedMasarifs( set );
				}
			}
			
		}
//	    if( programRelatedBoxes != null )
//		{
//			Set<ProgramRelatedBoxes> set = new HashSet<ProgramRelatedBoxes>(  );
//			for (ProgramRelatedBoxes related : programRelatedBoxes ) 
//			{
//					related.setProgRelBoxId( null );	
//					set.add(related);
//					endowmentProgram.setProgramRelatedBoxes( set );
//			}
//			
//		}
		if( donationBoxes != null )
		{
			Set<DonationBox> set = new HashSet<DonationBox>(  );
			for (DonationBox related : donationBoxes ) 
			{
						
					set.add(related);
					endowmentProgram.setDonationBoxes( set );
			}
			
		}  
		endowmentProgramService.persist( endowmentProgram );
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onSubmit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status =  Constant.EndowmentProgramStatus.APPROVAL_REQUIRED_ID;
		String msg    =  "endowmentProgram.msg.submitted";
		String event  =  "endowmentProgram.event.submitted";
		try	
		{	
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			if( hasSaveErrors() ){ return; }
			 updateValuesFromMap();	
			 SystemParameters parameters = SystemParameters.getInstance();
			 String endPoint= parameters.getParameter( "MEMSEndowmentProgramsBPEL" );
			 MEMSEndowmentProgramsBPELPortClient port=new MEMSEndowmentProgramsBPELPortClient();
			 
			 port.setEndpoint(endPoint);
			 persistEndowmentProgram( status );
			 updateValuesToMap();
			 saveCommentsAttachment( event );
			 port.initiate(
					 		CommonUtil.getLoggedInUser(),
					 		endowmentProgram.getEndowmentProgramId().toString(),
					 		endowmentProgram.getRefNum(), 
					 		null, 
					 		null
					 	   );
			 ApplicationContext.getContext().getTxnContext().commit();
			 getEndowmentProgramDetails( endowmentProgram.getEndowmentProgramId() );

		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSubmit --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onResubmitted()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		long status =  Constant.EndowmentProgramStatus.APPROVAL_REQUIRED_ID;
		String msg    =  "endowmentProgram.msg.resubmitted";
		String event  =  "endowmentProgram.event.resubmitted";
		try	
		{	
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			if( hasSaveErrors() ){ return; }
			 updateValuesFromMap();	
			 
			 persistEndowmentProgram( status );
			 setTaskOutCome(TaskOutcome.OK);
			 updateValuesToMap();
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 getEndowmentProgramDetails( endowmentProgram.getEndowmentProgramId() );
			 //onMasarifTab();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onResubmitted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	private boolean hasReasonProvided() throws Exception
	{
		if(txtRemarks ==null || txtRemarks.trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.reasonRequired")  );
			return false;
		}
		return true;
	}
	@SuppressWarnings( "unchecked" )
	public void onRejected()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		long status =  Constant.EndowmentProgramStatus.REJECTED_ID;
		String msg    =  "endowmentProgram.msg.rejected";
		String event  =  "endowmentProgram.event.rejected";
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistEndowmentProgram( status );
			 setTaskOutCome(TaskOutcome.REJECT);
			 updateValuesToMap();
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 getEndowmentProgramDetails( endowmentProgram.getEndowmentProgramId() );
			 //onMasarifTab();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onSendBack()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		long status =  Constant.EndowmentProgramStatus.RESUBMITTED_ID;
		String msg    =  "endowmentProgram.msg.sentBack";
		String event  =  "endowmentProgram.event.sentBack";
		try	
		{	
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			 
			 persistEndowmentProgram( status );
			 setTaskOutCome(TaskOutcome.OK);
			 updateValuesToMap();
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 getEndowmentProgramDetails( endowmentProgram.getEndowmentProgramId() );
			 //onMasarifTab();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSendBack--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onApprove()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		long status =  Constant.EndowmentProgramStatus.PUBLISH_REQUIRED_ID;
		String msg    =  "endowmentProgram.msg.approvedSentForPublishing";
		String event  =  "endowmentProgram.event.sentForPublishing";
		TaskOutcome outcome = TaskOutcome.APPROVE;
		try	
		{	
			updateValuesFromMap();
			
			if( hasSaveErrors() ) return;
			 
			ApplicationContext.getContext().getTxnContext().beginTransaction(); 	
			 //If dis src is amaf
			 if( endowmentProgram.getEndProgDisSrc().getEndProgDisSrcId().compareTo(1L) == 0)
			 {
				status  =  Constant.EndowmentProgramStatus.APPROVED_DISBURSEMENT_REQ_ID;
				msg     =  "endowmentProgram.msg.approvedSentToFinance";
				event   =  "endowmentProgram.event.sentForDisbursement";
			    outcome =  TaskOutcome.FORWARD; 	
			 }
			 endowmentProgramService.setGenerateCostCenter(true);
			 
			 persistEndowmentProgram( status );
			 
			 setTaskOutCome(outcome);
			 updateValuesToMap();
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 getEndowmentProgramDetails( endowmentProgram.getEndowmentProgramId() );
			 //onMasarifTab();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onApprove--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onPublished()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		long status =  Constant.EndowmentProgramStatus.PUBLISHED_ID;
		String msg    =  "endowmentProgram.msg.published";
		String event  =  "endowmentProgram.event.published";
		try	
		{	
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			 updateValuesFromMap();	
			 
			 persistEndowmentProgram( status );
			 setTaskOutCome(TaskOutcome.OK);
			 updateValuesToMap();
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 getEndowmentProgramDetails( endowmentProgram.getEndowmentProgramId() );
			 //onMasarifTab();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onPublished--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}


	@SuppressWarnings( "unchecked" )
	public void onDisbursed()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		long status =  Constant.EndowmentProgramStatus.PUBLISH_REQUIRED_ID;
		String msg    =  "endowmentProgram.msg.disbursed";
		String event  =  "endowmentProgram.event.disbursed";
		try	
		{	
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			 updateValuesFromMap();	
			 
			 persistEndowmentProgram( status );
			 setTaskOutCome(TaskOutcome.APPROVE);
			 updateValuesToMap();
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 getEndowmentProgramDetails( endowmentProgram.getEndowmentProgramId() );
			 //onMasarifTab();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onDisbursed--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onFinanceRejected()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		long status =  Constant.EndowmentProgramStatus.FINANCE_REJECTED_ID;
		String msg    =  "endowmentProgram.msg.financeRejectedSentBack";
		String event  =  "endowmentProgram.event.financeRejectedSentBack";
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistEndowmentProgram( status );
			 setTaskOutCome(TaskOutcome.REJECT);
			 updateValuesToMap();
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 getEndowmentProgramDetails( endowmentProgram.getEndowmentProgramId() );
			 //onMasarifTab();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onFinanceRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	
	private boolean hasOnCloseErrors() throws Exception
	{
		boolean hasErrors=false;
		if( endowmentProgram.getEndProgEvalType() == null || endowmentProgram.getEndProgEvalType().getEndProgEvalTypeId() == null )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.evalType")); 
			return true;
		}
		if( endowmentProgram.getEvalRemarks() == null || endowmentProgram.getEvalRemarks().trim().length() <= 0 )
		{

			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.evalRemarksReq")); 
			return true;
		}

		return hasErrors;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onClose()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status =  Constant.EndowmentProgramStatus.CLOSED_ID;
		String msg    =  "endowmentProgram.msg.closed";
		String event  =  "endowmentProgram.event.closed";
		try	
		{	
			if( hasOnCloseErrors() ){ return; }
			 updateValuesFromMap();	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 persistEndowmentProgram(status );
			 if(getUserTask() != null)
			 {
				 setTaskOutCome(TaskOutcome.OK);
			 }
			 updateValuesToMap();
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();

			 getEndowmentProgramDetails( endowmentProgram.getEndowmentProgramId() );
			 onMasarifTab();
		   	 
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onClose--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}

		
	}
	
	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
    	try
    	{
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = getUserTask();
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    	}
    	catch(PIMSWorkListException ex)
    	{
    		
    		logger.LogException("setTaskOutCome| Error Occured...",ex);
    		throw ex;
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("setTaskOutCome| Error Occured...",ex);
    		throw ex;
    	}
    }



	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask )
	{
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}


	public String getPageTitle() {
	
	if(viewModePopup || pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  || pageMode.equals(  PAGE_MODE_RESUBMITTED )  )
	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("endowmentProgram.title.heading"));
	}
	else if( pageMode.equals(  PAGE_MODE_APPROVAL_REQUIRED ) || pageMode.equals(  PAGE_MODE_FINANCE_REJECTED )  )
	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("endowmentProgram.title.approveProgram"));
	}
	else if( pageMode.equals(  PAGE_MODE_FINANCE )  )
	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("endowmentProgram.title.finance"));
	}
	else if( pageMode.equals(  PAGE_MODE_PUBLISH )  )
	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("endowmentProgram.title.publish"));
	}	 	 
	else if( pageMode.equals(  PAGE_MODE_CLOSE)  )
	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("endowmentProgram.title.close"));
	}	 	 
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW )   && !getPageMode().equals( PAGE_MODE_PUBLISH ) && 
			!getPageMode().equals( PAGE_MODE_FINANCE) && !getPageMode().equals( PAGE_MODE_CLOSE )&&
			!viewModePopup
		   
		  )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowRemarks()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&
			 ( getPageMode().equals( PAGE_MODE_FINANCE) || 
			  getPageMode().equals( PAGE_MODE_APPROVAL_REQUIRED) ||
			  getPageMode().equals( PAGE_MODE_FINANCE_REJECTED) 
			 ) 
			 &&
				!viewModePopup
			   
		  )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_NEW ) &&
				!viewModePopup
		  )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowResubmitButton()
	{
		if( getPageMode().equals( PAGE_MODE_RESUBMITTED )  &&
				!viewModePopup
				   )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowDisburse()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
				getPageMode().equals( PAGE_MODE_FINANCE ) &&
				!viewModePopup
				    )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowPublished()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
				getPageMode().equals( PAGE_MODE_PUBLISH ) &&
				!viewModePopup
				   )
		{
			return true;
		}
		return false;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowApproveRejectSendBack()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( 
			  getPageMode().equals( PAGE_MODE_APPROVAL_REQUIRED ) ||
			  getPageMode().equals( PAGE_MODE_FINANCE_REJECTED ) 
			)&&
			!viewModePopup
			   
		  )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowClose()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
				getPageMode().equals( PAGE_MODE_CLOSE ) &&
				!viewModePopup
				   )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowClosingEvaluation()
	{
		if( getStatusCompleted() || getShowClose() &&!viewModePopup)
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}

	public String getHdnFilePersonId() {
		return hdnFilePersonId;
	}

	public void setHdnFilePersonId(String hdnFilePersonId) {
		this.hdnFilePersonId = hdnFilePersonId;
	}

	public String getHdnFilePersonName() {
		return hdnFilePersonName;
	}

	public void setHdnFilePersonName(String hdnFilePersonName) {
		this.hdnFilePersonName = hdnFilePersonName;
	}

	public EndowmentProgram getEndowmentProgram() {
		return endowmentProgram;
	}

	public void setEndowmentProgram(EndowmentProgram endowmentProgram) {
		this.endowmentProgram = endowmentProgram;
	}

	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public List<ProgramRelatedMasarif> getProgramRelatedMasarif() {
		return programRelatedMasarif;
	}

	public void setProgramRelatedMasarif(List<ProgramRelatedMasarif> programRelatedMasarif) {
		this.programRelatedMasarif = programRelatedMasarif;
	}

	public List<ProgramRelatedBoxes> getProgramRelatedBoxes() {
		return programRelatedBoxes;
	}

	public void setProgramRelatedBoxes(List<ProgramRelatedBoxes> programRelatedBoxes) {
		this.programRelatedBoxes = programRelatedBoxes;
	}

	public HtmlDataTable getDataTableBox() {
		return dataTableBox;
	}

	public void setDataTableBox(HtmlDataTable dataTableBox) {
		this.dataTableBox = dataTableBox;
	}

	public boolean isViewModePopup() {
		return viewModePopup;
	}

	public void setViewModePopup(boolean viewModePopup) {
		this.viewModePopup = viewModePopup;
	}

	public List<ProgramMilestone> getListProgramMilestone() {
		return listProgramMilestone;
	}

	public void setListProgramMilestone(List<ProgramMilestone> listProgramMilestone) {
		this.listProgramMilestone = listProgramMilestone;
	}

	public HtmlDataTable getDataTableProgramMilestone() {
		return dataTableProgramMilestone;
	}

	public void setDataTableProgramMilestone(HtmlDataTable dataTableProgramMilestone) {
		this.dataTableProgramMilestone = dataTableProgramMilestone;
	}

	public ProgramMilestone getProgramMilestone() {
		return programMilestone;
	}

	public void setProgramMilestone(ProgramMilestone programMilestone) {
		this.programMilestone = programMilestone;
	}

	public String getSumExpPercentagesStr() {
		return sumExpPercentagesStr;
	}

	public void setSumExpPercentagesStr(String sumExpPercentagesStr) {
		this.sumExpPercentagesStr = sumExpPercentagesStr;
	}

	public String getSumPercentagesStr() {
		return sumPercentagesStr;
	}

	public void setSumPercentagesStr(String sumPercentagesStr) {
		this.sumPercentagesStr = sumPercentagesStr;
	}

	public List<DonationBox> getDonationBoxes() {
		return donationBoxes;
	}

	public void setDonationBoxes(List<DonationBox> donationBoxes) {
		this.donationBoxes = donationBoxes;
	}

	public List<ProgramCollectionsView> getProgramCollections() {
		return programCollections;
	}

	public void setProgramCollections(
			List<ProgramCollectionsView> programCollections) {
		this.programCollections = programCollections;
	}

	public HtmlDataTable getDataTableProgramCollections() {
		return dataTableProgramCollections;
	}

	public void setDataTableProgramCollections(
			HtmlDataTable dataTableProgramCollections) {
		this.dataTableProgramCollections = dataTableProgramCollections;
	}
	
	

}