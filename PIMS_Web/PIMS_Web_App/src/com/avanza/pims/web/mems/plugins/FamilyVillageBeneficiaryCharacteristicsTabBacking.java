package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.data.ApplicationContext;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.FamilyVillageBeneficiaryService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.BeneficiaryCharacteristicsView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageBeneficiaryCharacteristicsTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private List< BeneficiaryCharacteristicsView> dataList;
	
	private HtmlDataTable dataTable;
	FamilyVillageBeneficiaryService service;
	
	private String tabMode;
	BeneficiaryCharacteristicsView pageView;
	private Long personId;
	public FamilyVillageBeneficiaryCharacteristicsTabBacking() 
	{
		service = new FamilyVillageBeneficiaryService();
		dataTable        = new HtmlDataTable();
		dataList         = new ArrayList<BeneficiaryCharacteristicsView>();
		tabMode          = Constant.EndowmentFile.MODE_READ_ONLY;
		pageView         = new BeneficiaryCharacteristicsView();
		dataTable        =  new HtmlDataTable();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.RECOMMENDATION_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.RECOMMENDATION_TAB_SUCCESS, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			tabMode = (String) viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		if( viewMap.get( "BeneficiaryCharacteristicsViewList" ) != null )
		{
			dataList = (ArrayList<BeneficiaryCharacteristicsView>)viewMap.get( "BeneficiaryCharacteristicsViewList" );
		}
		if( viewMap.get( "BeneficiaryCharacteristicsView" ) != null )
		{
			pageView = (BeneficiaryCharacteristicsView)viewMap.get( "BeneficiaryCharacteristicsView" );
		}
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, tabMode );
		}
		if( dataList != null && dataList.size()  > 0  )
		{
			viewMap.put( "BeneficiaryCharacteristicsViewList", dataList );
		}
		if( pageView != null )
		{
			viewMap.put( "BeneficiaryCharacteristicsView", pageView );
		}
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
	}	
	
	

	public void populateTab( PersonView personView ) throws Exception
	{
		updateValuesFromMaps();
		personId = personView.getPersonId();
		setPageDataDefaults();
		loadDataList(personId);
		setTotalRows( dataList.size() );
		
		updateValuesToMaps();
	}
	
	private void setPageDataDefaults()throws Exception
	{
		pageView = new BeneficiaryCharacteristicsView();
		PersonView person  = new PersonView();
		person.setPersonId(personId);
		pageView.setPerson( person);
		pageView.setCreatedBy(getLoggedInUserId());
		pageView.setCreatedOn(new Date());
		pageView.setUpdatedBy(getLoggedInUserId());
		pageView.setUpdatedOn(new Date());
		pageView.setIsDeleted(0l);
		
	}

	private void loadDataList(Long personId) throws Exception {
		
		dataList = FamilyVillageBeneficiaryService.getBeneficiaryCharacteristics( personId);
	}

	
	@SuppressWarnings("unchecked")
	private boolean hasErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		
//		if( pageView.getComments()== null || pageView.getComments().trim().length() <=0) {
//			errorMessages.add(CommonUtil.getBundleMessage("familyVillageManageBeneficiary.msg.recommendationRequired"));
//			return true;
//		}
		
		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onAdd()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			if(hasErrors())return;
			try
			{
				ApplicationContext.getContext().getTxnContext().beginTransaction();
				BeneficiaryCharacteristicsView object =(BeneficiaryCharacteristicsView)BeanUtils.cloneBean( pageView );
				FamilyVillageBeneficiaryService.addBeneficiaryCharacteristic( object);
				loadDataList(personId);
				setPageDataDefaults(); 
				ApplicationContext.getContext().getTxnContext().commit();
			}
			catch(Exception e){ApplicationContext.getContext().getTxnContext().rollback();throw e;}
			finally{ApplicationContext.getContext().getTxnContext().release();}
			loadDataList(personId);
		}
		catch ( Exception e )
		{
			logger.LogException( "onAdd--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.RECOMMENDATION_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.RECOMMENDATION_TAB_SUCCESS, successMessages );
			}
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDelete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
			
			
//			
//			if( deleted )
//			{
//			 dataList.remove(obj);
//			}
		}
		catch ( Exception e )
		{
			logger.LogException( "onDelete--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.RECOMMENDATION_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.RECOMMENDATION_TAB_SUCCESS, successMessages );
			}
		}
		
	}
  
	
		
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}

	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<BeneficiaryCharacteristicsView> getDataList() {
		return dataList;
	}

	public void setDataList(List<BeneficiaryCharacteristicsView> dataList) {
		this.dataList = dataList;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public BeneficiaryCharacteristicsView getPageView() {
		return pageView;
	}

	
	public void setPageView(BeneficiaryCharacteristicsView pageView) {
		this.pageView = pageView;
	}

	


}
