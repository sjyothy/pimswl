package com.avanza.pims.web.mems.minors;

import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Convert;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.bpel.memsopenfilebpel.proxy.MEMSOpenFileBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.entity.BeneficiaryRepresentative;
import com.avanza.pims.proxy.MEMSSocialReserchBPELPortClient;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.BeneficiarySharesTabController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.plugins.BeneficiaryRepresentativeTabBacking;
import com.avanza.pims.web.mems.plugins.DistributeShareTabBean;
import com.avanza.pims.web.mems.plugins.InheritanceBeneficiaryTabBean;
import com.avanza.pims.web.mems.plugins.InheritanceFileBasicInfoTabBean;
import com.avanza.pims.web.mems.plugins.InheritedAssetsTabBean;
import com.avanza.pims.web.mems.plugins.TabFileBlockingDetails;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.MemsFollowupService;
import com.avanza.pims.ws.mems.SocialResearchService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.BeneficiaryDetailsView;
import com.avanza.pims.ws.vo.BeneficiaryGuardianView;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InheritanceBeneficiaryView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.OpenFileDistribute;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.SocialResearchView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.pims.ws.vo.mems.AssetMemsView;
import com.avanza.pims.ws.vo.mems.InheritedAssetShareInfoView;
import com.avanza.pims.ws.vo.mems.InheritedAssetView;
import com.avanza.pims.ws.vo.mems.MemsFollowupView;
import com.avanza.pims.ws.vo.mems.OfficialCorrespondenceView;
import com.avanza.pims.ws.vo.mems.ResearchRecommendationView;
import com.avanza.ui.util.ResourceUtil;

/**
 * In order to use this class, please read this with care When navigating from
 * RequestSearch/InheritanceFileSearch : 1)InheritanceFileView will be available
 * in viewMap with key = WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW
 * 2)RequestView will be available in viewMap with key =
 * WebConstants.REQUEST_VIEW 3)filePersonView (PersonView) will be available in
 * viewMap with key = WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW
 * 4)SponserView (PersonView) will be available in viewMap with key =
 * WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW
 * 5)BeneficiaryDetailsView will be available in viewMap with key =
 * WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST
 * 
 * @author Kamran Ahmed
 * 
 */
public class InheritanceFileBean extends AbstractMemsBean {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private String hdnApplicantId;
	private String hdnInheritanceSponsorId;
	private String hdnGuardianId;
	private String hdnBeneficiaryId;
	private String hdnInheritancePersonId;
	private String hdnNurseId;
	private String hdnAssetId;
	private String hdnManagerId;
	private String rejecttionReason;
	private HtmlInputText txtFileNumber;
	private HtmlInputText txtFileStatus;
	private HtmlSelectOneMenu cboFileType;
	private HtmlSelectOneMenu cboResearcher;
	private HtmlCalendar clndrCreatedOn;
	private HtmlTabPanel fileTabPanel;
	private String txtTotalFileShare;
	private HtmlInputText txtNewCostCenter;
	private String txtOldCostCenter;
	private HtmlSelectOneMenu cboAssignedExpert;

	private HtmlCommandButton btnAcknowledge = new HtmlCommandButton();
	private HtmlCommandButton btnDistDone = new HtmlCommandButton();
	private HtmlCommandButton btnSaveRequest = new HtmlCommandButton();
	private HtmlCommandButton btnSubmitRequest = new HtmlCommandButton();
	private HtmlCommandButton btnInitLimDone = new HtmlCommandButton();
	private HtmlCommandButton btnFinalLimDone = new HtmlCommandButton();
	private HtmlCommandButton btnRejectResubmit = new HtmlCommandButton();
	private HtmlCommandButton btnInitLimRejected = new HtmlCommandButton();
	private HtmlCommandButton btnApproveRequest = new HtmlCommandButton();
	private HtmlCommandButton btnResearcher = new HtmlCommandButton();
	private HtmlCommandButton btnSendRecommForApproval = new HtmlCommandButton();
	private HtmlInputTextarea txtAreaRejectionReason = new HtmlInputTextarea();
	private HtmlOutputLabel lblRejectionReason = new HtmlOutputLabel();
	private HtmlCommandButton btnAssginedExpert = new HtmlCommandButton();

	private List<SelectItem> researcherList;
	private List<DomainDataView> fileTypeListDD;
	private List<DomainDataView> tenantTypeListDD;
	private List<DomainDataView> requestStatusListDD;
	private List<BeneficiaryDetailsView> beneficiaryDetailsList;
	private List<DomainDataView> inherFilestatusListDD;
	private List<DomainDataView> offCorrStatusDDList;
	private List<DomainDataView> followupCatDDList;
	private List<SelectItem> assignedExpertList;

	private DomainDataView offCorresOpen;
	private DomainDataView offCorresClose;
	private DomainDataView companyDDV;
	private DomainDataView individualDDV;
	private DomainDataView deceasedDDV;
	private DomainDataView underGuardianshipDDV;
	private DomainDataView legalAssitanceDDV;
	private DomainDataView foundingDDV;
	private DomainDataView absentDDV;
	private DomainDataView rqstStNewDD;
	private DomainDataView rqstStApprovalRequiredDD;
	private DomainDataView rqstStRejectedResubmitDD;
	private DomainDataView rqstStApprovedDD;
	private DomainDataView rqstStInitLimDoneDD;
	private DomainDataView rqstStFinalLimDoneDD;
	private DomainDataView rqstStLimRejectedDD;
	private DomainDataView fileStNewDD;
	private DomainDataView fileStApprovalRequiredDD;
	private DomainDataView fileStRejectedResubmitDD;
	private DomainDataView fileStApprovedDD;
	private DomainDataView fileStInitLimDoneDD;
	private DomainDataView fileStFinalLimDoneDD;
	private DomainDataView fileStLimRejectedDD;
	private DomainDataView fileStClosedDD;
	private DomainDataView fileStDistributionReqDD;
	private DomainDataView socialResearch;
	private boolean isDeceased;
	private boolean isFounding;
	private boolean isLegalAsstnc;
	private boolean isAbsent;
	private boolean isUnderGuradian;
	private String pageMode;
	private final String noteOwner;
	@SuppressWarnings("unused")
	private final String externalId;
	@SuppressWarnings("unused")
	private final String procedureTypeKey;
	@SuppressWarnings("unused")
	private final String INHERITED_ASSET_MEMS_VIEW_LIST = "INHERITED_ASSET_MEMS_VIEW_LIST";
	private final String REVENUE_TYPE_MAP = "REVENUE_TYPE_MAP";
	private final String ASSET_MEMS_VIEW = "ASSET_MEMS_VIEW";
	private final String IS_ASSET_SELECTED_FROM_SEARCH = "IS_ASSET_SELECTED_FROM_SEARCH";
	private final String INDEX_TO_EDIT = "INDEX_TO_EDIT";
	private final String CORRESPONDENCE_LIST = "CORRESPONDENCE_LIST";
	private final String MEMS_CNRCRND_DEPT_VIEW = "MEMS_CNRCRND_DEPT_VIEW";
	private final String TASK_LIST_USER_TASK = "TASK_LIST_USER_TASK";
	private final String SHOW_DIST_TAB = "SHOW_DIST_TAB";

	private static final String TAB_ID_APPLICANT = "appDetailsTab";
	private static final String TAB_ID_BASIC_INFO = "inheritanceFileBasicInfoTabId";
	private static final String TAB_ID_BENEFICIARY = "inheritanceBeneficiaryTabId";
	private static final String TAB_ID_INHERITED_ASSET = "inheritedAssetsId";
	private static final String TAB_ID_BENEFICIARY_SHARE = "beneficiarySharesTabId";
	private static final String TAB_ID_ATTACHMENT = "attachmentTab";
	@SuppressWarnings("unused")
	private static final String TAB_ID_COMMENT = "commentsTab";
	private static final String TAB_ID_FOLLOW_UP = "memsfollowUpTabId";
	private static final String TAB_ID_OFF_CORRESP = "officialCorrespondenceTabId";
	private static final String TAB_ID_DISTRIBUTE_SHARES = "distributionTab";
	private static final String RESEARCHER_LIST = "RESEARCHER_LIST";
	private static final String TASK_TYPE = "TASK_TYPE";
	private static final String FOLLOW_UP_VIEW_LIST = "memsFollowupViewList";
	private static final String IS_RESEARCHER_ASSIGNED = "IS_RESEARCHER_ASSIGNED";
	private static final String ASSET_SHARE_INFO = "ASSET_SHARE_INFO";
	private static final String FILE_DISTRIBUTION_REQ_STATUS = "FILE_DISTRIBUTION_REQ_STATUS";
	private static final String ASSIGNED_EXPERT_LIST = "ASSIGNED_EXPERT_LIST";
	private static final String IS_INHERITANCE_EXPERT_ASSIGNED = "IS_INHERITANCE_EXPERT_ASSIGNED";
	private SocialResearchService socialService;

	private HashMap<Long, DomainDataView> revenueTypeMap = new HashMap<Long, DomainDataView>();
	private List<DomainDataView> revenueTypeListDD = new ArrayList<DomainDataView>();

	public InheritanceFileBean() {
		txtFileNumber = new HtmlInputText();
		txtFileStatus = new HtmlInputText();
		cboFileType = new HtmlSelectOneMenu();
		cboResearcher = new HtmlSelectOneMenu();
		clndrCreatedOn = new HtmlCalendar();
		fileTabPanel = new HtmlTabPanel();
		txtTotalFileShare = "";
		txtNewCostCenter = new HtmlInputText();
		txtOldCostCenter = "";
		fileTypeListDD = new ArrayList<DomainDataView>();
		tenantTypeListDD = new ArrayList<DomainDataView>();
		requestStatusListDD = new ArrayList<DomainDataView>();
		companyDDV = new DomainDataView();
		individualDDV = new DomainDataView();
		deceasedDDV = new DomainDataView();
		underGuardianshipDDV = new DomainDataView();
		legalAssitanceDDV = new DomainDataView();
		foundingDDV = new DomainDataView();
		absentDDV = new DomainDataView();
		beneficiaryDetailsList = new ArrayList<BeneficiaryDetailsView>();
		inherFilestatusListDD = new ArrayList<DomainDataView>();
		rqstStNewDD = new DomainDataView();
		rqstStApprovalRequiredDD = new DomainDataView();
		rqstStRejectedResubmitDD = new DomainDataView();
		rqstStApprovedDD = new DomainDataView();
		rqstStInitLimDoneDD = new DomainDataView();
		rqstStFinalLimDoneDD = new DomainDataView();
		rqstStLimRejectedDD = new DomainDataView();
		fileStNewDD = new DomainDataView();
		fileStApprovalRequiredDD = new DomainDataView();
		fileStRejectedResubmitDD = new DomainDataView();
		fileStApprovedDD = new DomainDataView();
		fileStInitLimDoneDD = new DomainDataView();
		fileStFinalLimDoneDD = new DomainDataView();
		fileStLimRejectedDD = new DomainDataView();
		researcherList = new ArrayList<SelectItem>();
		noteOwner = WebConstants.InheritanceFile.NOTES_OWNER;
		procedureTypeKey = WebConstants.InheritanceFile.PROCEDURE_KEY_OPEN_FILE;
		externalId = WebConstants.InheritanceFile.EXTERNAL_ID_OPEN_FILE;
		offCorrStatusDDList = new ArrayList<DomainDataView>();
		followupCatDDList = new ArrayList<DomainDataView>();
		offCorresOpen = new DomainDataView();
		offCorresClose = new DomainDataView();
		socialResearch = new DomainDataView();
		fileStClosedDD = new DomainDataView();
		socialService = new SocialResearchService();
		cboAssignedExpert = new HtmlSelectOneMenu();
		assignedExpertList = new ArrayList<SelectItem>();
	}

	@SuppressWarnings("unchecked")
	public void init() {
		super.init();
		try {
			if (!isPostBack()) {
				// populating viewMap with required data
				putDomainDataInViewMap();
				checkValuesInSessionMap();
				checkForUndistributedTransactions();
				if (viewMap.get(WebConstants.InheritanceFile.FILE_ID) != null) {
					if (viewMap.get(WebConstants.InheritanceFile.FILE_TYPE_ID) != null) {
						viewMap
								.put(
										WebConstants.Attachment.PROCEDURE_SUB_KEY,
										viewMap
												.get(
														WebConstants.InheritanceFile.FILE_TYPE_ID)
												.toString());
					}
					loadAttachmentsAndComments(Long.parseLong(viewMap.get(
							WebConstants.InheritanceFile.FILE_ID).toString()));
				} else {
					loadAttachmentsAndComments(null);
				}
				// if(request is new)
				if (!viewMap
						.containsKey(WebConstants.ApplicationDetails.APPLICATION_DATE)) {
					viewMap.put(
							WebConstants.ApplicationDetails.APPLICATION_DATE,
							new Date());
					viewMap.put(WebConstants.InheritanceFile.CREATED_ON,
							new Date());
				}
				// viewMap.put(WebConstants.InheritanceFilePageMode.PAGE_MODE,
				// WebConstants.InheritanceFilePageMode.PAGE_MODE_READ_ONLY);

			} else {
				if (viewMap.get(WebConstants.InheritanceFile.FILE_ID) != null) {
					if (viewMap.get(WebConstants.InheritanceFile.FILE_TYPE_ID) != null) {
						viewMap
								.put(
										WebConstants.Attachment.PROCEDURE_SUB_KEY,
										viewMap
												.get(
														WebConstants.InheritanceFile.FILE_TYPE_ID)
												.toString());
					}
					loadAttachmentsAndComments(Long.parseLong(viewMap.get(
							WebConstants.InheritanceFile.FILE_ID).toString()));
				}
			}
		} catch (Exception e) {
			logger.LogException("init crashed", e);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}
	}

	@SuppressWarnings("unchecked")
	private void checkForUndistributedTransactions() throws Exception {
		if (viewMap.get(WebConstants.REQUEST_VIEW) != null) {
			RequestView requestView = (RequestView) viewMap
					.get(WebConstants.REQUEST_VIEW);
			boolean show = WebConstants.InheritanceFileType.FILES_TYPE_SAME_OWNER_BENEFICIARY
					.indexOf(requestView.getInheritanceFileView()
							.getFileTypeKey()) != 0
					&& new InheritanceFileService()
							.hasUndistributedPaymentsAgainstFileOwners(requestView
									.getInheritanceFileView()
									.getInheritanceFileId());
			viewMap.put(SHOW_DIST_TAB, show);

		}
	}

	@SuppressWarnings("unchecked")
	private void checkValuesInSessionMap() throws Exception {
		if (sessionMap.get(WebConstants.InheritanceFilePageMode.IS_POPUP) != null) {
			viewMap
					.put(
							WebConstants.InheritanceFilePageMode.IS_POPUP,
							sessionMap
									.remove(WebConstants.InheritanceFilePageMode.IS_POPUP));
			sessionMap.remove(WebConstants.InheritanceFilePageMode.IS_POPUP);
			canAddAttachmentsAndComments(false);
		}

		// From file Search
		if (sessionMap.get(WebConstants.InheritanceFile.FILE_ID) != null) {
			Long fileId = Long.parseLong(sessionMap.remove(
					WebConstants.InheritanceFile.FILE_ID).toString());
			RequestView requestView = new InheritanceFileService()
					.getInheritanceFileRequestByFileId(fileId, null);

			if (requestView != null && requestView.getRequestId() != null) {
				getFileDetails(fileId, requestView);
			}

		} else // From request Search
		if (sessionMap.get(WebConstants.REQUEST_VIEW) != null) {
			RequestView requestView = (RequestView) sessionMap
					.remove(WebConstants.REQUEST_VIEW);
			Long fileId = requestView.getInheritanceFileView()
					.getInheritanceFileId();
			// if(requestView != null && requestView.getRequestId() != null)
			// {
			// requestView = new
			// RequestService().getRequestById(requestView.getRequestId());
			// if(requestView != null && requestView.getRequestId() != null)
			// {
			// getFileDetails(fileId, requestView);
			// }
			// }
			getFileDetails(fileId, requestView);

		} else if (sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null) {
			viewMap.put("evaluationRequestDetailsReadonlyMode", true);
			checkTask();
		} else// New file
		{
			viewMap.put("OLD_FILE_TYPE", "-1");
			viewMap.put(WebConstants.InheritanceFilePageMode.PAGE_MODE,
					WebConstants.InheritanceFilePageMode.PAGE_MODE_NEW);
			if (getRqstStNewDD() != null
					&& getRqstStNewDD().getDomainDataId() != null)
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,
						CommonUtil.getIsEnglishLocale() ? getRqstStNewDD()
								.getDataDescEn() : getRqstStNewDD()
								.getDataDescAr());

			if (getFileStNewDD() != null
					&& getFileStNewDD().getDomainDataId() != null) {
				viewMap.put(WebConstants.InheritanceFile.FILE_STATUS_DESC,
						CommonUtil.getIsEnglishLocale() ? getFileStNewDD()
								.getDataDescEn() : getFileStNewDD()
								.getDataDescAr());
				viewMap.put(WebConstants.InheritanceFile.FILE_STATUS_ID,
						getFileStNewDD().getDomainDataId());
				setPageModeByFilesStatus();
			}
		}
		if (sessionMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE) != null) {
			pageMode = sessionMap.get(
					WebConstants.InheritanceFilePageMode.PAGE_MODE).toString();
			if (pageMode
					.equalsIgnoreCase(WebConstants.InheritanceFilePageMode.PAGE_MODE_READ_ONLY))
				viewMap
						.put(
								WebConstants.InheritanceFilePageMode.PAGE_MODE,
								WebConstants.InheritanceFilePageMode.PAGE_MODE_READ_ONLY);
			sessionMap.remove(WebConstants.InheritanceFilePageMode.PAGE_MODE);
		}

		// check User group and assinged Category
		// User user = SecurityManager.getUser(getLoggedInUserId());
		// HashSet<UserGroup> groups = null;
		// groups = user.getAddedGroups();
		// {
		// if(groups != null)
		// {
		// Iterator iter = groups.iterator();
		// PersonManager pm = new PersonManager();
		// while(iter.hasNext())
		// {
		// UserGroup group = (UserGroup) iter.next();
		// List<FollowupCatGroupMapping> mappingList =
		// pm.getFollowupCategoryMappingByGroupId(group.getUserGroupId());
		// }
		// }
		// }

	}

	@SuppressWarnings("unchecked")
	private void getFileDetails(Long fileId, RequestView requestView) {
		// to show related data in different tabs
		putRequestDataInViewMap(requestView);
		viewMap.put(WebConstants.REQUEST_VIEW, requestView);
		viewMap.put(WebConstants.InheritanceFile.FILE_TYPE_ID, requestView
				.getInheritanceFileView().getFileTypeId());
		viewMap.put(WebConstants.InheritanceFile.FILE_ID, fileId);
		// status to handle page mode
		viewMap.put(WebConstants.InheritanceFile.FILE_STATUS_ID, requestView
				.getInheritanceFileView().getStatusId());
		if (StringHelper.isNotEmpty(requestView.getInheritanceFileView()
				.getResearcher()))
			viewMap.put(IS_RESEARCHER_ASSIGNED, true);
		if (StringHelper.isNotEmpty(requestView.getInheritanceFileView()
				.getAssignedExpert()))
			viewMap.put(IS_INHERITANCE_EXPERT_ASSIGNED, true);
		setPageModeByFilesStatus();
	}

	@SuppressWarnings("unchecked")
	private void putRequestDataInViewMap(RequestView requestView) {
		checkSelectedFileTypeId(Long.valueOf(requestView
				.getInheritanceFileView().getFileTypeId()));
		fillApplicationDetails(requestView);
		fillFileInfo(requestView);

		if (isFounding() || isLegalAsstnc()) {
			fillFileBasicInfoTab(requestView);
		} else if (isUnderGuradian()) {
			fillFileBasicInfoTab(requestView);
			fillBeneficiaryDetailsForGrid(requestView);
		} else {
			fillFilePersonInfo(requestView);
			fillBeneficiaryDetailsForGrid(requestView);
		}
		if (isFounding())
			fillSponsorInfo(requestView);
	}

	@SuppressWarnings("unchecked")
	private void fillFileBasicInfoTab(RequestView requestView) {
		PersonView beneficiaryPerson = null;
		PersonView filePerson = null;
		InheritanceBeneficiaryView inhBeneficiary = null;

		if (requestView.getInheritanceFileView() != null
				&& requestView.getInheritanceFileView().getInheritanceFileId() != null) {

			filePerson = requestView.getInheritanceFileView()
					.getFilePersonView();
			// NEW Code
			for (InheritanceBeneficiaryView beneficiary : requestView
					.getInheritanceFileView().getInheritanceBeneficiaryList()) {
				if (beneficiary.isSelfBeneficiary()) {
					inhBeneficiary = beneficiary;
					break;
				}

			}
			// NEW Code
			// inhBeneficiary =
			// requestView.getInheritanceFileView().getInheritanceBeneficiaryList().get(0);
			beneficiaryPerson = inhBeneficiary.getBeneficiary();
			beneficiaryPerson.setSharePercentage(inhBeneficiary
					.getSharePercentage().toString());

			viewMap.put(
					WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW,
					filePerson);
			loadFilePersonCities();

			viewMap.put(WebConstants.InheritanceFile.FILE_PERSON_BEENFICIARY,
					beneficiaryPerson);

			// if Guardian is available
			if (inhBeneficiary.getBeneficiaryGuardianViewList() != null
					&& inhBeneficiary.getBeneficiaryGuardianViewList().size() > 0) {
				PersonView guardian = null;
				BeneficiaryGuardianView beneGuardian = null;
				beneGuardian = inhBeneficiary.getBeneficiaryGuardianViewList()
						.get(0);

				guardian = beneGuardian.getGuardian();
				guardian.setRelationshipString(beneGuardian
						.getRelationshipView().getRelationshipId().toString());
				guardian
						.setRelationshipDesc(beneGuardian.getRelationshipDesc());
				viewMap.put("SELECTED_GUARDIAN", beneGuardian
						.getBeneficiaryGuardianId());
				viewMap.put(WebConstants.InheritanceFile.FILE_PERSON_GUARDIAN,
						guardian);
			}

			// if Nurse is available
			if (inhBeneficiary.getNurse() != null
					&& inhBeneficiary.getNurse().getPersonId() != null) {
				PersonView nurse = null;

				nurse = inhBeneficiary.getNurse();
				nurse.setRelationshipString(inhBeneficiary
						.getNurseRelationshipIdString());
				nurse.setRelationshipDesc(inhBeneficiary
						.getNurseRelationshipDesc());

				viewMap.put(WebConstants.InheritanceFile.FILE_PERSON_NURSE,
						nurse);
			}
		}

	}

	private void loadFilePersonCities() {
		InheritanceFileBasicInfoTabBean bean = (InheritanceFileBasicInfoTabBean) getBean("pages$inheritanceFileBasicInfoTab");
		bean.passportCountryChanged();
		bean.countryChanged();
	}

	private void loadBeneficiaryCities() {
		InheritanceBeneficiaryTabBean bean = (InheritanceBeneficiaryTabBean) getBean("pages$inheritanceBeneficiaryTab");
		bean.passportCountryChanged();
		bean.countryChanged();
	}

	@SuppressWarnings("unchecked")
	private void fillFileInfo(RequestView requestView) {
		if (requestView.getInheritanceFileView() != null
				&& requestView.getInheritanceFileView().getInheritanceFileId() != null) {
			InheritanceFileView file = requestView.getInheritanceFileView();
			txtFileNumber.setValue(file.getFileNumber());
			txtFileStatus.setValue(CommonUtil.getIsEnglishLocale() ? file
					.getStatusEn() : file.getStatusAr());
			txtNewCostCenter.setValue(file.getCostCenter());
			txtOldCostCenter = file.getOldCostCenter() != null ? file
					.getOldCostCenter().trim() : "";
			// This code is written to remove the '.' if any from the value, coz
			// value is considered as integer
			// if(file.getTotalInheritanceShare() != null){
			//				
			// int index =
			// file.getTotalInheritanceShare().toString().indexOf(".");
			// if(index != -1){
			// String totalShareWitoutPrecision =
			// file.getTotalInheritanceShare().toString().substring(0, index);
			// txtTotalFileShare = totalShareWitoutPrecision;
			// }
			// }
			DecimalFormat oneDForm = new DecimalFormat("#");
			txtTotalFileShare = oneDForm.format( file.getTotalInheritanceShare());

			cboFileType.setValue(file.getFileTypeId().toString());
			clndrCreatedOn.setValue(file.getCreatedOn());
			if (StringHelper.isNotEmpty(file.getRejectionReason()))
				txtAreaRejectionReason.setValue(file.getRejectionReason());
			viewMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW,
					requestView.getInheritanceFileView());
			if (StringHelper.isNotEmpty(requestView.getInheritanceFileView()
					.getResearcher())) {
				cboResearcher.setValue(requestView.getInheritanceFileView()
						.getResearcher());
				if (!isTaskForAssigningResearcher()) {
					cboResearcher.setDisabled(true);
					btnResearcher.setRendered(false);
				}
			}

			if (StringHelper.isNotEmpty(requestView.getInheritanceFileView()
					.getAssignedExpert())) {
				cboAssignedExpert.setValue(requestView.getInheritanceFileView()
						.getAssignedExpert());
				if (!isTaskForAssigningResearcher()) {
					cboAssignedExpert.setDisabled(true);
					btnAssginedExpert.setRendered(false);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void fillBeneficiaryDetailsForGrid(RequestView requestView) {
		if (requestView.getInheritanceFileView() != null
				&& requestView.getInheritanceFileView().getInheritanceFileId() != null) {
			List<InheritanceBeneficiaryView> beneficiaryList = new ArrayList<InheritanceBeneficiaryView>();
			List<BeneficiaryDetailsView> beneficiaryDetailsList = new ArrayList<BeneficiaryDetailsView>();
			boolean isEnglishLocale = CommonUtil.getIsEnglishLocale();
			beneficiaryList = requestView.getInheritanceFileView()
					.getInheritanceBeneficiaryList();
			if (beneficiaryList != null && beneficiaryList.size() > 0) {
				for (InheritanceBeneficiaryView beneficiaryView : beneficiaryList) {
					if (beneficiaryView.getIsDeleted().compareTo(0L) == 0
							&& ((isUnderGuradian() && !beneficiaryView
									.isSelfBeneficiary()) || (!isUnderGuradian() && beneficiaryView
									.isSelfBeneficiary()))) {
						BeneficiaryDetailsView beneficiaryDetail = new BeneficiaryDetailsView();
						beneficiaryDetail
								.setInheritanceBeneficiaryId(beneficiaryView
										.getInheritanceBeneficiaryId());
						beneficiaryDetail.setDeleted(false);
						beneficiaryDetail.setIsApplicant(beneficiaryView
								.getIsApplicant());
						beneficiaryDetail.setBeneficiary(beneficiaryView
								.getBeneficiary());
						beneficiaryDetail.setBeneficiaryName(beneficiaryView
								.getBeneficiary().getPersonFullName());
						if (beneficiaryView.getSharePercentage() != null
								&& beneficiaryView.getSharePercentage()
										.toString().trim().length() > 0) {
							beneficiaryDetail
									.setSharePercentage(beneficiaryView
											.getSharePercentage().toString());
						}
						beneficiaryDetail.setMinor(beneficiaryView
								.getBeneficiary().isMinor());
						beneficiaryDetail.setCostCenter(beneficiaryView
								.getCostCenter());
						beneficiaryDetail.setOldCostCenter(beneficiaryView
								.getOldCostCenter());
						beneficiaryDetail.setOldNewCostCenter(CommonUtil
								.CombineCostCenter(beneficiaryView
										.getOldCostCenter(), beneficiaryView
										.getCostCenter()));

						// TODO: put yes and no into message resource
						beneficiaryDetail.setMinorString(beneficiaryDetail
								.isMinor() ? CommonUtil
								.getBundleMessage("commons.yes") : CommonUtil
								.getBundleMessage("commons.no"));
						if (beneficiaryView.getNurse() != null
								&& beneficiaryView.getNurse().getPersonId() != null) {
							PersonView nurseView = beneficiaryView.getNurse();
							beneficiaryDetail.setNurseId(nurseView
									.getPersonId());
							beneficiaryDetail
									.setNurseName(getNameByPersonView(nurseView));
							beneficiaryDetail
									.setRelationshipStringNurse(beneficiaryView
											.getNurseRelationshipIdString());
							beneficiaryDetail
									.setRelationshipDescNurse(beneficiaryView
											.getNurseRelationshipDesc());
						}
						if (beneficiaryView.getBeneficiaryGuardianViewList() != null
								&& beneficiaryView
										.getBeneficiaryGuardianViewList()
										.size() > 0) {

							BeneficiaryGuardianView beneficiaryGuardian = beneficiaryView
									.getBeneficiaryGuardianViewList().get(0);

							beneficiaryDetail
									.setBeneficiaryGuardianId(beneficiaryGuardian
											.getBeneficiaryGuardianId());
							PersonView guardianView = beneficiaryGuardian
									.getGuardian();// coz there is only one
													// guardian currenlty
													// associated with
													// beneficiary
							beneficiaryDetail
									.setGuardianName(getNameByPersonView(guardianView));
							beneficiaryDetail.setGuardianId(guardianView
									.getPersonId());
							beneficiaryDetail
									.setRelationshipDescGuardian(beneficiaryGuardian
											.getRelationshipDesc());
							if (beneficiaryGuardian.getRelationshipIdString() != null
									&& StringHelper
											.isNotEmpty(beneficiaryGuardian
													.getRelationshipIdString()))
								beneficiaryDetail
										.setRelationshipStringGuardian(beneficiaryGuardian
												.getRelationshipIdString());
							else
								beneficiaryDetail
										.setRelationshipStringGuardian(beneficiaryGuardian
												.getRelationshipView()
												.getRelationshipId().toString());
						}

						if (beneficiaryView.getRelationshipIdString() != null
								&& beneficiaryView.getRelationshipIdString()
										.compareTo("-1") != 0) {
							beneficiaryDetail.setRelationshipId(Long
									.parseLong(beneficiaryView
											.getRelationshipIdString()));
							beneficiaryDetail
									.setRelationshipString(isEnglishLocale ? beneficiaryView
											.getRelationshipEn()
											: beneficiaryView
													.getRelationshipAr());
							beneficiaryDetail
									.setRelationshipDesc(beneficiaryView
											.getRelationshipDesc());
						}

						beneficiaryDetailsList.add(beneficiaryDetail);
					} else
						continue;// skip deleted beneficiary

				}
				Collections.sort(beneficiaryDetailsList, new ListComparator(
						"beneficiaryName", true));
				// Collections.sort(beneficiaryDetailsList,ListComparator.PERSON_NAME_COMPARATOR);
				viewMap.put(
						WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST,
						beneficiaryDetailsList);
			}
		}

	}

	@SuppressWarnings("unchecked")
	private void fillSponsorInfo(RequestView requestView) {
		if (requestView.getInheritanceFileView() != null
				&& requestView.getInheritanceFileView().getSponsorView() != null
				&& requestView.getInheritanceFileView().getSponsorView()
						.getPersonId() != null)
			viewMap.put(WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW,
					requestView.getInheritanceFileView().getSponsorView());
	}

	@SuppressWarnings("unchecked")
	private void fillFilePersonInfo(RequestView requestView) {
		if (requestView.getInheritanceFileView() != null
				&& requestView.getInheritanceFileView().getFilePersonView() != null
				&& requestView.getInheritanceFileView().getFilePersonView()
						.getPersonId() != null) {
			viewMap.put(
					WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW,
					requestView.getInheritanceFileView().getFilePersonView());
			loadFilePersonCities();
		}
	}

	@SuppressWarnings("unchecked")
	/**
	 * Desc: This method will use in view_only and edit mode
	 */
	private void fillApplicationDetails(RequestView requestView) {
		if (requestView != null && requestView.getRequestNumber() != null) {
			if (requestView.getApplicantView() != null
					&& requestView.getApplicantView().getPersonId() != null) {
				PersonView applicant = requestView.getApplicantView();
				if (requestView.getApplicantView().getPersonFullName() != null
						&& requestView.getApplicantView().getPersonFullName()
								.trim().length() > 0)
					viewMap
							.put(
									WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
									requestView.getApplicantView()
											.getPersonFullName());
				else if (requestView.getApplicantView().getCompanyName() != null
						&& applicant.getCompanyName().trim().length() > 0)
					viewMap
							.put(
									WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
									requestView.getApplicantView()
											.getCompanyName());
				String personType = getTenantType(applicant);
				if (personType != null)
					viewMap
							.put(
									WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,
									personType);
				if (applicant.getCellNumber() != null)
					viewMap
							.put(
									WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,
									applicant.getCellNumber());
				if (applicant.getPersonId() != null) {
					viewMap.put(WebConstants.ApplicationDetails.APPLICATION_ID,
							applicant.getPersonId());
					hdnApplicantId = applicant.getPersonId().toString();

				}
			}
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_NUMBER,
					requestView.getRequestNumber());
			if (requestView.getStatusId() != null)
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_STATUS,
						CommonUtil.getIsEnglishLocale() ? requestView
								.getStatusEn() : requestView.getStatusAr());
			if (requestView.getDescription() != null
					&& StringHelper.isNotEmpty(requestView.getDescription()))
				viewMap
						.put(
								WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION,
								requestView.getDescription());
			if (requestView.getRequestDate() != null)
				viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE,
						requestView.getRequestDate());
		}
	}

	@SuppressWarnings("unchecked")
	private void putDomainDataInViewMap() {

		try {

			List<SelectItem> groupUserList = new CommonUtil()
					.getResearcherGroup();
			if (groupUserList != null && groupUserList.size() > 0)
				viewMap.put(RESEARCHER_LIST, groupUserList);

			List<SelectItem> inheritanceUserList = new CommonUtil()
					.getAssignedInheritanceExpertGroup();
			if (inheritanceUserList != null && inheritanceUserList.size() > 0)
				viewMap.put(ASSIGNED_EXPERT_LIST, inheritanceUserList);

			revenueTypeListDD = CommonUtil
					.getDomainDataListForDomainType(WebConstants.InheritanceFile.ASSET_REVENUE_TYPE);
			if (revenueTypeListDD != null && revenueTypeListDD.size() > 0) {
				for (DomainDataView revenueDD : revenueTypeListDD) {
					revenueTypeMap.put(revenueDD.getDomainDataId(), revenueDD);
				}
				if (revenueTypeMap != null && revenueTypeMap.size() > 0) {
					viewMap.put(REVENUE_TYPE_MAP, revenueTypeMap);
				}
			}

			tenantTypeListDD = CommonUtil
					.getDomainDataListForDomainType(WebConstants.TENANT_TYPE);
			if (tenantTypeListDD != null && tenantTypeListDD.size() > 0) {
				viewMap.put(WebConstants.SESSION_CONTRACT_TENANT_TYPE,
						individualDDV);
			}

			companyDDV = CommonUtil.getIdFromType(tenantTypeListDD,
					WebConstants.TENANT_TYPE_COMPANY);
			if (companyDDV != null && companyDDV.getDomainDataId() != null) {
				viewMap.put(WebConstants.TENANT_TYPE_COMPANY, companyDDV);
			}

			individualDDV = CommonUtil.getIdFromType(tenantTypeListDD,
					WebConstants.TENANT_TYPE_INDIVIDUAL);
			if (individualDDV != null
					&& individualDDV.getDomainDataId() != null) {
				viewMap.put(WebConstants.TENANT_TYPE_INDIVIDUAL, individualDDV);
			}

			fileTypeListDD = CommonUtil
					.getDomainDataListForDomainType(WebConstants.InheritanceFileType.INH_FILE_TYPE);
			if (fileTypeListDD != null && fileTypeListDD.size() > 0) {
				viewMap.put(WebConstants.InheritanceFileType.INH_FILE_TYPE,
						fileTypeListDD);
			}

			deceasedDDV = CommonUtil.getIdFromType(fileTypeListDD,
					WebConstants.InheritanceFileType.DECEASED);
			if (deceasedDDV != null && deceasedDDV.getDomainDataId() != null) {
				viewMap.put(WebConstants.InheritanceFileType.DECEASED,
						deceasedDDV);
			}

			foundingDDV = CommonUtil.getIdFromType(fileTypeListDD,
					WebConstants.InheritanceFileType.FOUNDING);
			if (foundingDDV != null && foundingDDV.getDomainDataId() != null) {
				viewMap.put(WebConstants.InheritanceFileType.FOUNDING,
						foundingDDV);
			}

			underGuardianshipDDV = CommonUtil.getIdFromType(fileTypeListDD,
					WebConstants.InheritanceFileType.UNDER_GUARDIANSHIP);
			if (underGuardianshipDDV != null
					&& underGuardianshipDDV.getDomainDataId() != null) {
				viewMap.put(
						WebConstants.InheritanceFileType.UNDER_GUARDIANSHIP,
						underGuardianshipDDV);
			}

			absentDDV = CommonUtil.getIdFromType(fileTypeListDD,
					WebConstants.InheritanceFileType.ABSENT);
			if (absentDDV != null && absentDDV.getDomainDataId() != null) {
				viewMap.put(WebConstants.InheritanceFileType.ABSENT, absentDDV);
			}

			legalAssitanceDDV = CommonUtil.getIdFromType(fileTypeListDD,
					WebConstants.InheritanceFileType.LEGAL_ASSISTANCE);
			if (legalAssitanceDDV != null
					&& legalAssitanceDDV.getDomainDataId() != null) {
				viewMap.put(WebConstants.InheritanceFileType.LEGAL_ASSISTANCE,
						legalAssitanceDDV);
			}

			requestStatusListDD = CommonUtil
					.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
			inherFilestatusListDD = CommonUtil
					.getDomainDataListForDomainType(WebConstants.InheritanceFileStatus.INH_FILE_STATUS);
			if (requestStatusListDD != null && requestStatusListDD.size() > 0) {
				rqstStNewDD = CommonUtil.getIdFromType(requestStatusListDD,
						WebConstants.REQUEST_STATUS_NEW);
				rqstStApprovalRequiredDD = CommonUtil.getIdFromType(
						requestStatusListDD,
						WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
				rqstStRejectedResubmitDD = CommonUtil.getIdFromType(
						requestStatusListDD,
						WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT);
				rqstStApprovedDD = CommonUtil.getIdFromType(
						requestStatusListDD,
						WebConstants.REQUEST_STATUS_APPROVED);
				rqstStInitLimDoneDD = CommonUtil.getIdFromType(
						requestStatusListDD,
						WebConstants.REQUEST_STATUS_INITIAL_LIMITATION_DONE);
				rqstStFinalLimDoneDD = CommonUtil.getIdFromType(
						requestStatusListDD,
						WebConstants.REQUEST_STATUS_FINAL_LIMITATION_DONE);
				rqstStLimRejectedDD = CommonUtil.getIdFromType(
						requestStatusListDD,
						WebConstants.REQUEST_STATUS_LIMITATION_REJECTED);

				if (rqstStNewDD != null
						&& rqstStNewDD.getDomainDataId() != null)
					viewMap.put(WebConstants.REQUEST_STATUS_NEW, rqstStNewDD);
				if (rqstStApprovalRequiredDD != null
						&& rqstStApprovalRequiredDD.getDomainDataId() != null)
					viewMap.put(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED,
							rqstStApprovalRequiredDD);
				if (rqstStRejectedResubmitDD != null
						&& rqstStRejectedResubmitDD.getDomainDataId() != null)
					viewMap.put(WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT,
							rqstStRejectedResubmitDD);
				if (rqstStApprovedDD != null
						&& rqstStApprovedDD.getDomainDataId() != null)
					viewMap.put(WebConstants.REQUEST_STATUS_APPROVED,
							rqstStApprovedDD);
				if (rqstStInitLimDoneDD != null
						&& rqstStInitLimDoneDD.getDomainDataId() != null)
					viewMap
							.put(
									WebConstants.REQUEST_STATUS_INITIAL_LIMITATION_DONE,
									rqstStInitLimDoneDD);
				if (rqstStFinalLimDoneDD != null
						&& rqstStFinalLimDoneDD.getDomainDataId() != null)
					viewMap.put(
							WebConstants.REQUEST_STATUS_FINAL_LIMITATION_DONE,
							rqstStFinalLimDoneDD);
				if (rqstStLimRejectedDD != null
						&& rqstStLimRejectedDD.getDomainDataId() != null)
					viewMap.put(
							WebConstants.REQUEST_STATUS_LIMITATION_REJECTED,
							rqstStLimRejectedDD);
			}
			if (inherFilestatusListDD != null
					&& inherFilestatusListDD.size() > 0) {
				fileStNewDD = CommonUtil.getIdFromType(inherFilestatusListDD,
						WebConstants.InheritanceFileStatus.NEW);
				fileStApprovalRequiredDD = CommonUtil.getIdFromType(
						inherFilestatusListDD,
						WebConstants.InheritanceFileStatus.APPROVAL_REQUIRED);
				fileStRejectedResubmitDD = CommonUtil.getIdFromType(
						inherFilestatusListDD,
						WebConstants.InheritanceFileStatus.REJECTED_RESUBMIT);
				fileStApprovedDD = CommonUtil.getIdFromType(
						inherFilestatusListDD,
						WebConstants.InheritanceFileStatus.APPROVED);
				fileStInitLimDoneDD = CommonUtil
						.getIdFromType(
								inherFilestatusListDD,
								WebConstants.InheritanceFileStatus.INITIAL_LIMITATION_DONE);
				fileStFinalLimDoneDD = CommonUtil
						.getIdFromType(
								inherFilestatusListDD,
								WebConstants.InheritanceFileStatus.FINAL_LIMITATION_DONE);
				fileStLimRejectedDD = CommonUtil.getIdFromType(
						inherFilestatusListDD,
						WebConstants.InheritanceFileStatus.LIMITATION_REJECTED);
				fileStClosedDD = CommonUtil.getIdFromType(
						inherFilestatusListDD,
						WebConstants.InheritanceFileStatus.CLOSED);
				fileStDistributionReqDD = CommonUtil
						.getIdFromType(
								inherFilestatusListDD,
								WebConstants.InheritanceFileStatus.DISTRIBUTION_REQUIRED);

				if (fileStNewDD != null
						&& fileStNewDD.getDomainDataId() != null)
					viewMap.put(WebConstants.InheritanceFileStatus.NEW,
							fileStNewDD);
				if (fileStApprovalRequiredDD != null
						&& fileStApprovalRequiredDD.getDomainDataId() != null)
					viewMap
							.put(
									WebConstants.InheritanceFileStatus.APPROVAL_REQUIRED,
									fileStApprovalRequiredDD);
				if (fileStRejectedResubmitDD != null
						&& fileStRejectedResubmitDD.getDomainDataId() != null)
					viewMap
							.put(
									WebConstants.InheritanceFileStatus.REJECTED_RESUBMIT,
									fileStRejectedResubmitDD);
				if (fileStApprovedDD != null
						&& fileStApprovedDD.getDomainDataId() != null)
					viewMap.put(WebConstants.InheritanceFileStatus.APPROVED,
							fileStApprovedDD);
				if (fileStInitLimDoneDD != null
						&& fileStInitLimDoneDD.getDomainDataId() != null)
					viewMap
							.put(
									WebConstants.InheritanceFileStatus.INITIAL_LIMITATION_DONE,
									fileStInitLimDoneDD);
				if (fileStFinalLimDoneDD != null
						&& fileStFinalLimDoneDD.getDomainDataId() != null)
					viewMap
							.put(
									WebConstants.InheritanceFileStatus.FINAL_LIMITATION_DONE,
									fileStFinalLimDoneDD);
				if (fileStLimRejectedDD != null
						&& fileStLimRejectedDD.getDomainDataId() != null)
					viewMap
							.put(
									WebConstants.InheritanceFileStatus.LIMITATION_REJECTED,
									fileStLimRejectedDD);
				if (fileStClosedDD != null
						&& fileStClosedDD.getDomainDataId() != null)
					viewMap.put("FILE_CLOSED_STATUS", fileStClosedDD);
				if (fileStDistributionReqDD != null
						&& fileStDistributionReqDD.getDomainDataId() != null)
					viewMap.put(FILE_DISTRIBUTION_REQ_STATUS,
							fileStDistributionReqDD);
			}

			offCorrStatusDDList = CommonUtil
					.getDomainDataListForDomainType(WebConstants.OFFICIAL_CORRESPONDENCE.DOMAIN_TYPE_OFF_CORR_STATUS);
			if (offCorrStatusDDList != null && offCorrStatusDDList.size() > 0) {
				viewMap
						.put(
								WebConstants.OFFICIAL_CORRESPONDENCE.DOMAIN_TYPE_OFF_CORR_STATUS,
								offCorrStatusDDList);

				offCorresOpen = CommonUtil
						.getIdFromType(
								offCorrStatusDDList,
								WebConstants.OFFICIAL_CORRESPONDENCE.DOMAIN_DATA_OFF_CORR_STATUS_OPEN);
				if (offCorresOpen != null
						&& offCorresOpen.getDomainDataId() != null) {
					viewMap
							.put(
									WebConstants.OFFICIAL_CORRESPONDENCE.DOMAIN_DATA_OFF_CORR_STATUS_OPEN,
									offCorresOpen);
				}

				offCorresClose = CommonUtil
						.getIdFromType(
								offCorrStatusDDList,
								WebConstants.OFFICIAL_CORRESPONDENCE.DOMAIN_DATA_OFF_CORR_STATUS_CLOSED);
				if (offCorresClose != null
						&& offCorresClose.getDomainDataId() != null) {
					viewMap
							.put(
									WebConstants.OFFICIAL_CORRESPONDENCE.DOMAIN_DATA_OFF_CORR_STATUS_CLOSED,
									offCorresClose);
				}

				followupCatDDList = CommonUtil
						.getDomainDataListForDomainType(WebConstants.FollowUpCategory.FOLLOW_UP_CATEGORY_TYPE);
				if (followupCatDDList != null && followupCatDDList.size() > 0) {
					viewMap
							.put(
									WebConstants.FollowUpCategory.FOLLOW_UP_CATEGORY_TYPE,
									followupCatDDList);

					socialResearch = CommonUtil.getIdFromType(
							offCorrStatusDDList,
							WebConstants.FollowUpCategory.SOCIAL_RESEARCH);
					if (socialResearch != null
							&& socialResearch.getDomainDataId() != null) {
						viewMap.put(
								WebConstants.FollowUpCategory.SOCIAL_RESEARCH,
								socialResearch);
					}
				}

			}
			viewMap.put("OPEN_FILE_PROCEDURE", true);
		} catch (Exception e) {
			logger.LogException("putDomainDataInViewMap() crashed", e);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}

	}

	public String getHdnApplicantId() {
		return hdnApplicantId;
	}

	public void setHdnApplicantId(String hdnApplicantId) {
		this.hdnApplicantId = hdnApplicantId;
	}

	@SuppressWarnings("unchecked")
	private void populateApplicantDetails(PersonView pv) {
		if (pv.getPersonId() != null)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,
					pv.getPersonId());
		if (pv.getPersonFullName() != null
				&& pv.getPersonFullName().trim().length() > 0)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
					pv.getPersonFullName());
		else if (pv.getCompanyName() != null
				&& pv.getCompanyName().trim().length() > 0)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
					pv.getCompanyName());
		String personType = getTenantType(pv);
		if (personType != null)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,
					personType);
		if (pv.getCellNumber() != null)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,
					pv.getCellNumber());
		if (pv.getPersonId() != null) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_ID, pv
					.getPersonId());
			hdnApplicantId = pv.getPersonId().toString();
		}
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE,
				new Date());

	}

	@SuppressWarnings("unchecked")
	@Override
	public void prerender() {
		super.prerender();
		getPersonInformationByType();
		if (viewMap.get(WebConstants.InherFileErrMsg.BENEFICIARY_ERR) != null) {
			List<String> errMsg = (List<String>) viewMap
					.get(WebConstants.InherFileErrMsg.BENEFICIARY_ERR);
			errorMessages.addAll(errMsg);
			viewMap.remove(WebConstants.InherFileErrMsg.BENEFICIARY_ERR);
		}
		if (viewMap.get(WebConstants.InherFileErrMsg.INHER_ASSET_ERR) != null) {
			List<String> errMsg = (List<String>) viewMap
					.get(WebConstants.InherFileErrMsg.INHER_ASSET_ERR);
			errorMessages.addAll(errMsg);
			viewMap.remove(WebConstants.InherFileErrMsg.INHER_ASSET_ERR);
		}
		if (viewMap.get(WebConstants.InherFileErrMsg.ASSET_SHARE_ERR) != null) {
			List<String> errMsg = (List<String>) viewMap
					.get(WebConstants.InherFileErrMsg.ASSET_SHARE_ERR);
			errorMessages.addAll(errMsg);
			viewMap.remove(WebConstants.InherFileErrMsg.ASSET_SHARE_ERR);
		}
		if (viewMap.get(WebConstants.InherFileErrMsg.OFFICIAL_CORRES_ERR) != null) {
			List<String> errMsg = (List<String>) viewMap
					.get(WebConstants.InherFileErrMsg.OFFICIAL_CORRES_ERR);
			errorMessages.addAll(errMsg);
			viewMap.remove(WebConstants.InherFileErrMsg.OFFICIAL_CORRES_ERR);
		}
		if (viewMap.get(WebConstants.InherFileErrMsg.FOLLOWUP_ERR) != null) {
			List<String> errMsg = (List<String>) viewMap
					.get(WebConstants.InherFileErrMsg.FOLLOWUP_ERR);
			errorMessages.addAll(errMsg);
			viewMap.remove(WebConstants.InherFileErrMsg.FOLLOWUP_ERR);
		}
		if (viewMap
				.get(WebConstants.InherFileErrMsg.FILE_PERSON_BASIC_INFO_ERR) != null) {
			List<String> errMsg = (List<String>) viewMap
					.get(WebConstants.InherFileErrMsg.FILE_PERSON_BASIC_INFO_ERR);
			errorMessages.addAll(errMsg);
			viewMap
					.remove(WebConstants.InherFileErrMsg.FILE_PERSON_BASIC_INFO_ERR);
		}

		if (viewMap.get(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR) != null) {
			List<String> errMsg = (List<String>) viewMap
					.get(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR);
			errorMessages.addAll(errMsg);
			viewMap.remove(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR);
		}
		if (viewMap
				.get(WebConstants.BlockingRequest.BLOCKING_DETAIL_TAB_ERROR_MESSAGES) != null) {
			List<String> errMsg = (List<String>) viewMap
					.remove(WebConstants.BlockingRequest.BLOCKING_DETAIL_TAB_ERROR_MESSAGES);
			errorMessages.addAll(errMsg);
		}
		if (viewMap.get("BENEF_REP_ERRORS") != null) {
			List<String> errMsg = (List<String>) viewMap.remove("BENEF_REP_ERRORS");
			errorMessages.addAll(errMsg);
		}
		
		// ////////////
		if (viewMap.get(WebConstants.InherFileSuccessMsg.BENEFICIARY_SUCC) != null) {
			List<String> succMsg = (List<String>) viewMap
					.get(WebConstants.InherFileSuccessMsg.BENEFICIARY_SUCC);
			successMessages.addAll(succMsg);
			viewMap.remove(WebConstants.InherFileSuccessMsg.BENEFICIARY_SUCC);
		}
		if (viewMap.get(WebConstants.InherFileSuccessMsg.INHER_ASSET_SUCC) != null) {
			List<String> succMsg = (List<String>) viewMap
					.get(WebConstants.InherFileSuccessMsg.INHER_ASSET_SUCC);
			successMessages.addAll(succMsg);
			viewMap.remove(WebConstants.InherFileSuccessMsg.INHER_ASSET_SUCC);
		}
		if (viewMap.get(WebConstants.InherFileSuccessMsg.ASSET_SHARE_SUCC) != null) {
			List<String> succMsg = (List<String>) viewMap
					.get(WebConstants.InherFileSuccessMsg.ASSET_SHARE_SUCC);
			successMessages.addAll(succMsg);
			viewMap.remove(WebConstants.InherFileSuccessMsg.ASSET_SHARE_SUCC);
		}
		if (viewMap.get(WebConstants.InherFileSuccessMsg.OFFICIAL_CORRES_SUCC) != null) {
			List<String> succMsg = (List<String>) viewMap
					.get(WebConstants.InherFileSuccessMsg.OFFICIAL_CORRES_SUCC);

			successMessages.addAll(succMsg);
			viewMap
					.remove(WebConstants.InherFileSuccessMsg.OFFICIAL_CORRES_SUCC);
		}

		if (viewMap.get(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_SUCC) != null) {
			List<String> errMsg = (List<String>) viewMap
					.remove(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_SUCC);
			successMessages.addAll(errMsg);
		}
		if (viewMap.get("BENEF_REP_SUCCESS") != null) {
			List<String> errMsg = (List<String>) viewMap.remove("BENEF_REP_SUCCESS");
			successMessages.addAll(errMsg);
		}
		if (sessionMap.remove("DISTRIBUTION_TAB") != null) {
			fileTabPanel.setSelectedTab(TAB_ID_DISTRIBUTE_SHARES);
			onDistributionTabClick();
		}

	}

	@SuppressWarnings("unchecked")
	private void getPersonInformationByType() {
		try {

			// *****************************************************************************/
			// Applicant
			if (hdnApplicantId != null
					&& StringHelper.isNotEmpty(hdnApplicantId)) {
				PropertyServiceAgent psa = new PropertyServiceAgent();
				PersonView pv = new PersonView();
				pv.setPersonId(Long.parseLong(hdnApplicantId));

				List<PersonView> personsList = psa.getPersonInformation(pv);

				if (personsList != null && personsList.size() > 0) {
					pv = personsList.get(0);
					populateApplicantDetails(pv);
					viewMap.put(WebConstants.ApplicationDetails.APPLICANT_VIEW,
							pv);
					hdnApplicantId = null;
				}
			}
			// *****************************************************************************/
			// Sponsor
			else if (hdnInheritanceSponsorId != null
					&& StringHelper.isNotEmpty(hdnInheritanceSponsorId)) {

				PersonView personDetail = new PersonView();
				personDetail = new PropertyService()
						.getPersonInformationById(Long
								.parseLong(hdnInheritanceSponsorId));
				viewMap.put(
						WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW,
						personDetail);

				if (isFounding())
					viewMap.put(
							WebConstants.InheritanceFile.FILE_PERSON_GUARDIAN,
							personDetail);
				hdnInheritanceSponsorId = null;

			}
			// *****************************************************************************/
			// File Person
			else if (hdnInheritancePersonId != null
					&& StringHelper.isNotEmpty(hdnInheritancePersonId)) {
				// Check if file owner is associated with any other file as an
				// owner
				try {
					String fileNumber = new InheritanceFileService()
							.isFilePersonPresentInOtherFileOfSameType(new Long(
									hdnInheritancePersonId), cboFileType
									.getValue().toString());
					if (fileNumber != null && fileNumber.length() > 0) {
						String msg = java.text.MessageFormat
								.format(
										CommonUtil
												.getBundleMessage("inheritanceFile.msg.filePersonAssociatedWithOtherFile"),
										fileNumber);
						errorMessages.add(msg);
						return;
					}
					// int fileCount = new
					// InheritanceFileService().getFileCountByOwnerId(new
					// Long(hdnInheritancePersonId));
					// if(fileCount >= 1)
					// {
					// errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.alreadyAssociated"));
					// hdnInheritancePersonId = null;
					// return;
					// }
				} catch (Exception e) {
					logger.logInfo(
							"getPersonInformationByType() crashed while receiving person with Id = "
									+ hdnInheritanceSponsorId, e);
					errorMessages
							.add(CommonUtil
									.getBundleMessage("mems.inheritanceFile.selectDifferentPerson"));
					return;
				}

				PersonView personDetail = new PersonView();
				personDetail = new PropertyService()
						.getPersonInformationById(Long
								.parseLong(hdnInheritancePersonId));

				viewMap
						.put(
								WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW,
								personDetail);
				loadFilePersonCities();

				/**
				 * in case is file type is one of these(Legal Asstnc/ Under
				 * Guardian/Founding) the File Person will be the Beneficiary
				 * and share% will 100% and no beneficiary can be added further
				 */
				if (isFilePersonBeneficiary()) {
					personDetail.setSharePercentage("100"); // in these case
															// share will be
															// read only and
															// 100%
					viewMap
							.put(
									WebConstants.InheritanceFile.FILE_PERSON_BEENFICIARY,
									personDetail);
				}
				hdnInheritancePersonId = null;
			}
			// *****************************************************************************/
			// Beneficiary
			else if (hdnBeneficiaryId != null
					&& StringHelper.isNotEmpty(hdnBeneficiaryId)) {
				PersonView personDetail = new PersonView();
				personDetail = new PropertyService()
						.getPersonInformationById(Long
								.parseLong(hdnBeneficiaryId));
				viewMap
						.put(
								WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW,
								personDetail);
				loadBeneficiaryCities();
				hdnBeneficiaryId = null;
			}
			// *****************************************************************************/
			// Nurse
			else if (hdnNurseId != null && StringHelper.isNotEmpty(hdnNurseId)) {
				PersonView personDetail = new PersonView();
				personDetail = new PropertyService()
						.getPersonInformationById(Long.parseLong(hdnNurseId));

				if (isFilePersonBeneficiary()) {
					viewMap.put(WebConstants.InheritanceFile.FILE_PERSON_NURSE,
							personDetail);
				} else {
					viewMap
							.put(
									WebConstants.InheritanceFile.INHERITANCE_NUSRSE_VIEW,
									personDetail);
				}
				hdnNurseId = null;
			}
			// *****************************************************************************/
			// Guardian
			else if (hdnGuardianId != null
					&& StringHelper.isNotEmpty(hdnGuardianId)) {
				PersonView personDetail = new PersonView();
				personDetail = new PropertyService()
						.getPersonInformationById(Long.parseLong(hdnGuardianId));

				if (isFilePersonBeneficiary()) {
					viewMap.put(
							WebConstants.InheritanceFile.FILE_PERSON_GUARDIAN,
							personDetail);
				} else {
					viewMap
							.put(
									WebConstants.InheritanceFile.INHERITANCE_GUARDIAN_VIEW,
									personDetail);
				}

				hdnGuardianId = null;
			}
			// *****************************************************************************/
			// Manager
			else if (StringHelper.isNotEmpty(hdnManagerId)) {

				PersonView personDetail = new PersonView();
				personDetail = new PropertyService()
						.getPersonInformationById(Long.parseLong(hdnManagerId));
				{
					viewMap.put(WebConstants.InheritanceFile.MANAGER_VIEW,
							personDetail);
					InheritedAssetsTabBean assetTabBean = (InheritedAssetsTabBean) getBean("pages$inheritedAssetsTab");
					assetTabBean.getTxtManagerName().setValue(
							personDetail.getPersonFullName());
				}
				hdnManagerId = null;
			}
		} catch (PimsBusinessException e) {
			logger.LogException("getPersonInformationByType crashed", e);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}

	}

	@SuppressWarnings("unused")
	private String getPersonName(PersonView pv) {
		String personName = "";
		if (pv.getIsCompany().compareTo(1L) == 0)
			personName = pv.getCompanyName();
		else
			personName = pv.getFirstName() + " " + pv.getMiddleName() + " "
					+ pv.getLastName();

		return personName;
	}

	@SuppressWarnings("unchecked")
	private String getTenantType(PersonView tenantView) {

		String method = "getTenantType";
		DomainDataView ddv;
		if (tenantView != null && tenantView.getPersonId() != null
				&& tenantView.getIsCompany() != null) {
			if (tenantView.getIsCompany().compareTo(new Long(1)) == 0)
				ddv = (DomainDataView) viewMap
						.get(WebConstants.TENANT_TYPE_COMPANY);
			else
				ddv = (DomainDataView) viewMap
						.get(WebConstants.TENANT_TYPE_INDIVIDUAL);
			if (CommonUtil.getIsArabicLocale())
				return ddv.getDataDescAr();
			else if (CommonUtil.getIsEnglishLocale())
				return ddv.getDataDescEn();
		}
		logger.logDebug(method + "|" + "Finish...");
		return "";

	}

	public HtmlInputText getTxtFileNumber() {
		return txtFileNumber;
	}

	public void setTxtFileNumber(HtmlInputText txtFileNumber) {
		this.txtFileNumber = txtFileNumber;
	}

	public HtmlInputText getTxtFileStatus() {
		if (viewMap.get(WebConstants.InheritanceFile.FILE_STATUS_DESC) != null)
			txtFileStatus.setValue(viewMap
					.get(WebConstants.InheritanceFile.FILE_STATUS_DESC));
		return txtFileStatus;
	}

	public void setTxtFileStatus(HtmlInputText txtFileStatus) {
		this.txtFileStatus = txtFileStatus;
	}

	public HtmlSelectOneMenu getCboFileType() {
		return cboFileType;
	}

	public void setCboFileType(HtmlSelectOneMenu cboFileType) {
		this.cboFileType = cboFileType;
	}

	public HtmlCalendar getClndrCreatedOn() {
		if (viewMap.get(WebConstants.InheritanceFile.CREATED_ON) != null)
			clndrCreatedOn.setValue(viewMap
					.get(WebConstants.InheritanceFile.CREATED_ON));
		return clndrCreatedOn;
	}

	public void setClndrCreatedOn(HtmlCalendar clndrCreatedOn) {
		this.clndrCreatedOn = clndrCreatedOn;
	}

	@SuppressWarnings("unchecked")
	public void fileTypeChanged() {
		String fileTypeId = cboFileType.getValue().toString();
		viewMap.put(WebConstants.InheritanceFile.FILE_TYPE_ID, fileTypeId);
		// Call tab bean method to render controls
		// InheritanceFileBasicInfoTabBean inherFileBasicTab =
		// (InheritanceFileBasicInfoTabBean)
		// getBean("pages$inheritanceFileBasicInfoTab");
		// inherFileBasicTab.renderFilePersonFieldLabel();
		checkSelectedFileTypeId(new Long(fileTypeId));
		if (viewMap.get("OLD_FILE_TYPE") != null) {
			if (viewMap.get("OLD_FILE_TYPE").toString().compareTo(fileTypeId) != 0) {
				viewMap.put("OLD_FILE_TYPE", fileTypeId);
				populateMandatoryDocs();
			}
		} else
			viewMap.put("OLD_FILE_TYPE", fileTypeId);
	}

	public List<DomainDataView> getFileTypeListDD() {
		return fileTypeListDD;
	}

	public void setFileTypeListDD(List<DomainDataView> fileTypeListDD) {
		this.fileTypeListDD = fileTypeListDD;
	}

	public List<DomainDataView> getTenantTypeListDD() {
		return tenantTypeListDD;
	}

	public void setTenantTypeListDD(List<DomainDataView> tenantTypeListDD) {
		this.tenantTypeListDD = tenantTypeListDD;
	}

	public DomainDataView getCompanyDDV() {
		return companyDDV;
	}

	public void setCompanyDDV(DomainDataView companyDDV) {
		this.companyDDV = companyDDV;
	}

	public DomainDataView getIndividualDDV() {
		return individualDDV;
	}

	public void setIndividualDDV(DomainDataView individualDDV) {
		this.individualDDV = individualDDV;
	}

	public String getHdnInheritanceSponsorId() {
		return hdnInheritanceSponsorId;
	}

	public void setHdnInheritanceSponsorId(String hdnInheritanceSponsorId) {
		this.hdnInheritanceSponsorId = hdnInheritanceSponsorId;
	}

	public String getHdnGuardianId() {
		return hdnGuardianId;
	}

	public void setHdnGuardianId(String hdnGuardianId) {
		this.hdnGuardianId = hdnGuardianId;
	}

	public String getHdnBeneficiaryId() {
		return hdnBeneficiaryId;
	}

	public void setHdnBeneficiaryId(String hdnBeneficiaryId) {
		this.hdnBeneficiaryId = hdnBeneficiaryId;
	}

	public String getHdnInheritancePersonId() {
		return hdnInheritancePersonId;
	}

	public void setHdnInheritancePersonId(String hdnInheritancePersonId) {
		this.hdnInheritancePersonId = hdnInheritancePersonId;
	}

	public String getHdnNurseId() {
		return hdnNurseId;
	}

	public void setHdnNurseId(String hdnNurseId) {
		this.hdnNurseId = hdnNurseId;
	}

	/**
	 * @author Kamran Ahmed Description: This method will save the inheritance
	 *         File information
	 */
	@SuppressWarnings("unchecked")
	public void saveFile() {
		HashMap<String, Object> outputMap = null;
		boolean isFirstTime = false;

		try {
			if (!isValidated()) {
				return;
			}

			isFirstTime = (viewMap.containsKey(WebConstants.REQUEST_VIEW) ? false
					: true);
			outputMap = new InheritanceFileService()
					.persistInheritanceFile(getRequiredValuesMap());
			putValuesInRelatedFileds(outputMap);// this will show request number
												// and file number on screen
			saveAttachmentComment();
			if (isFirstTime) {
				saveSystemComments(MessageConstants.RequestEvents.REQUEST_SAVED);
				successMessages
						.add(CommonUtil
								.getBundleMessage("mems.inheritanceFile.successMsg.filerequestSaved"));
			} else {
				saveSystemComments(MessageConstants.RequestEvents.REQUEST_UPDATED);
				successMessages
						.add(CommonUtil
								.getBundleMessage("mems.inheritanceFile.successMsg.filerequestUpdated"));
			}
			if (viewMap.get("REMOVED_BENEFICIARY_ID") != null) // Remove the
																// Beneficiary
																// Id from view
																// who is being
																// deleted
				viewMap.remove("REMOVED_BENEFICIARY_ID");
			viewMap.put("FILE_SAVED", true);
		} catch (Exception e) {
			logger.LogException("saveFile|Error Occured :", e);
			errorMessages
					.add(CommonUtil
							.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void saveSystemComments(String sysNote) throws Exception {

		if (viewMap.get(WebConstants.InheritanceFile.FILE_ID) != null) {
			Long fileId = new Long(viewMap.get(
					WebConstants.InheritanceFile.FILE_ID).toString());
			CommonUtil.saveSystemComments(noteOwner, sysNote, fileId);
		}
	}

	private void saveAttachmentComment() throws Exception {
		if (viewMap.get(WebConstants.InheritanceFile.FILE_ID) != null) {
			Long fileId = new Long(viewMap.get(
					WebConstants.InheritanceFile.FILE_ID).toString());
			CommonUtil.saveAttachments(fileId);
			CommonUtil.saveComments(fileId, noteOwner);
		}
	}

	private boolean isValidated() throws Exception {
		boolean validate = true;

		InheritanceFileView file = null;
		if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) != null) {
			file = (InheritanceFileView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);
		} else {
			file = new InheritanceFileView();
		}
		boolean isMigratedFile = (file.getMigrated() == 1L);
		if (errorMessages.size() > 0)
			errorMessages.clear();// clear the messages before adding new
									// errors
		// check FileType if not selected
		if (cboFileType.getValue() == null
				|| cboFileType.getValue().toString().compareTo("-1") == 0) {
			errorMessages
					.add(CommonUtil.getBundleMessage("errMsg.fileTypeReq"));
			return false;
		}
		if (!isMigratedFile) {
			// check if applicant is selected and page mode is New and file is
			// not migrated
			if (isPageModeNew()
					&& viewMap
							.get(WebConstants.ApplicationDetails.APPLICANT_VIEW) == null) {
				errorMessages
						.add(CommonUtil
								.getBundleMessage("mems.inheritanceFile.selectApplicant"));
				fileTabPanel.setSelectedTab(TAB_ID_APPLICANT);
				return false;
			}
			// Check if Total shares are not mentioned
			// if(( txtTotalFileShare == null ||
			// StringHelper.isEmpty(txtTotalFileShare.toString() ) ) )
			// {
			// errorMessages.add(CommonUtil.getBundleMessage("inheritanceFile.errMsg.plzProvideTotalShare"));
			// return false;
			// }
			if (isFounding() && !isSponsorValidated()) {
				fileTabPanel.setSelectedTab(TAB_ID_BASIC_INFO);
				return false;
			}
		}
		// check each mandatory field of file person
		if (!isFilePersonValidated()) {
			fileTabPanel.setSelectedTab(TAB_ID_BASIC_INFO);
			return false;
		}
		// check if fileType is among those in which filePerson is her/himself
		// is Beneficiary
		// then there is no need to validate data Beneficiary Tab.
		// We have to validate the Basic Info tab in above case
		// if( !isFilePersonBeneficiary() )
		// {
		// // if(!isBeneficiaryDetailsValidated())
		// // {
		// // fileTabPanel.setSelectedTab(TAB_ID_BENEFICIARY);
		// // return false;
		// // }
		// // //check beneficiaries share and file share equality
		// // if( !isBeneficiariesShareValidated() )
		// // {
		// //
		// errorMessages.add(CommonUtil.getBundleMessage("inheritanceFile.errMsg.saheTotalMustBeLEToTotalShare"));
		// // return false;
		// // }
		// }
		else {
			// nurse relation
			if (viewMap.get(WebConstants.InheritanceFile.FILE_PERSON_NURSE) != null) {
				InheritanceFileBean fileBean = (InheritanceFileBean) getBean("pages$inheritanceFile");
				PersonView nurse = (PersonView) viewMap
						.get(WebConstants.InheritanceFile.FILE_PERSON_NURSE);
				if (!fileBean.isPersonInfoValidated(nurse,
						WebConstants.InheritanceFilePerson.NURSE)) {
					errorMessages
							.add(CommonUtil
									.getBundleMessage("mems.inheritanceFile.nurseDtlReq"));
					return false;
				}
				if (StringHelper.isEmpty(nurse.getRelationshipString())
						|| nurse.getRelationshipString().compareTo("-1") == 0) {
					errorMessages
							.add(CommonUtil
									.getBundleMessage("mems.inheritanceFile.nurseRelationShipRequired"));
					return false;
				} else if (nurse.getRelationshipString().compareTo(
						WebConstants.RelationshipTypeIds.Others.toString()) == 0) {
					if (StringHelper.isEmpty(nurse.getRelationshipDesc())) {
						errorMessages.add(CommonUtil
								.getBundleMessage("errMsg.nurseRelDescReq"));
						return false;
					}
				}
			}

			// guardian relation
			if (viewMap.get(WebConstants.InheritanceFile.FILE_PERSON_GUARDIAN) != null) {
				InheritanceFileBean fileBean = (InheritanceFileBean) getBean("pages$inheritanceFile");
				PersonView guardian = (PersonView) viewMap
						.get(WebConstants.InheritanceFile.FILE_PERSON_GUARDIAN);
				if (!fileBean.isPersonInfoValidated(guardian,
						WebConstants.InheritanceFilePerson.GUARDIAN)) {
					errorMessages
							.add(CommonUtil
									.getBundleMessage("mems.inheritanceFile.guardDtlReq"));
					return false;
				}
				if (StringHelper.isEmpty(guardian.getRelationshipString())
						|| guardian.getRelationshipString().compareTo("-1") == 0) {
					errorMessages
							.add(CommonUtil
									.getBundleMessage("mems.inheritanceFile.guardRelationShipRequired"));
					return false;
				} else if (guardian.getRelationshipString().compareTo(
						WebConstants.RelationshipTypeIds.Others.toString()) == 0) {
					if (StringHelper.isEmpty(guardian.getRelationshipDesc())) {
						errorMessages.add(CommonUtil
								.getBundleMessage("errMsg.guardianRelDescReq"));
						return false;
					}
				}
			}

		}
		// Check Mandatory Doc attached or not
		if (!AttachmentBean.validateMandatoryAndLaterProvidedDocs()) {
			errorMessages.add(CommonUtil
					.getBundleMessage("errMsg.provideDocsOrMarkLater"));
			fileTabPanel.setSelectedTab(TAB_ID_ATTACHMENT);
			return false;
		}
		return validate;
	}

	@SuppressWarnings("unchecked")
	private boolean isBeneficiariesShareValidated() throws Exception {
		boolean validated = true;

		beneficiaryDetailsList = (List<BeneficiaryDetailsView>) viewMap
				.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST);
		double totalBeneShares = 0;

		totalBeneShares += getTotalBeneficiariesShares(beneficiaryDetailsList);
		if (totalBeneShares > getFileShareDbl()) {
			validated = false;
			String msg = MessageFormat
					.format(
							CommonUtil
									.getBundleMessage("mems.inheritanceFile.msg.sumOfBenefShareNotEqualToFileShare"),
							String.valueOf(totalBeneShares), String
									.valueOf(getFileShareDbl()));
			errorMessages.add(msg);
			fileTabPanel.setSelectedTab(TAB_ID_BENEFICIARY);

		}
		return validated;
	}

	@SuppressWarnings("unchecked")
	private boolean isBeneficiaryDetailsValidated() {
		boolean validated = true;
		if (viewMap.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST) == null
				|| ((List<BeneficiaryDetailsView>) viewMap
						.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST))
						.size() <= 0) {
			errorMessages.add(CommonUtil
					.getBundleMessage("mems.inheritanceFile.beneDetailsReq"));
			validated = false;
		}
		return validated;
	}

	@SuppressWarnings("unused")
	private boolean isNurseValidated() {
		boolean validated = false;
		PersonView nurseView = null;
		if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_NUSRSE_VIEW) != null) {
			nurseView = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_NUSRSE_VIEW);
			// if(isPersonInfoValidate(nurseView))
			// validated = true;
		} else
			errorMessages.add(CommonUtil
					.getBundleMessage("mems.inheritanceFile.beneDetailsReq"));
		return validated;
	}

	@SuppressWarnings("unused")
	private boolean isGuardianValidated() {
		boolean validated = false;
		PersonView guardian = null;
		if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_GUARDIAN_VIEW) != null) {
			guardian = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_GUARDIAN_VIEW);
			// if(isPersonInfoValidate(guardian))
			// validated = true;

		} else
			errorMessages.add(CommonUtil
					.getBundleMessage("mems.inheritanceFile.guardianReq"));
		return validated;
	}

	private boolean isFilePersonValidated() throws Exception {
		boolean validated = true;
		PersonView filePerson = null;
		if (viewMap
				.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW) == null) {
			errorMessages.add(CommonUtil
					.getBundleMessage("mems.inheritanceFile.filePersonReq"));
			return false;
		}
		filePerson = (PersonView) viewMap
				.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW);

		if (!isPersonInfoValidated(filePerson,
				WebConstants.InheritanceFilePerson.FILE_PERSON)) {
			validated = false;
		}

		return validated;
	}

	private boolean isSponsorValidated() {
		boolean validated = true;
		@SuppressWarnings("unused")
		PersonView sponsor = null;
		if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW) == null) {
			errorMessages.add(CommonUtil
					.getBundleMessage("mems.inheritanceFile.sponsorReq"));
			validated = false;
		}
		return validated;
	}

	@SuppressWarnings("unused")
	private boolean isBeneficiaryValidated() {
		boolean validated = true;
		PersonView beneficiaryView = null;
		if (viewMap
				.get(WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW) != null) {
			beneficiaryView = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW);
			// if(isPersonInfoValidate(beneficiaryView))
			// validated = true;

		} else
			errorMessages.add(CommonUtil
					.getBundleMessage("mems.inheritanceFile.beneficiaryReq"));

		return validated;
	}

	public boolean isPersonInfoValidated(PersonView personView,
			String personType) {
		boolean validated = true;
		if (personView != null) {
			InheritanceFileView file = null;
			if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) != null) {
				file = (InheritanceFileView) viewMap
						.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);
			} else {
				file = new InheritanceFileView();
			}
			boolean isMigrated = (file.getMigrated() == 1L);
			// If not (Sponsor or Nurse or Guardian )
			if (!(personType
					.compareTo(WebConstants.InheritanceFilePerson.SPONSOR) == 0
					|| personType
							.compareTo(WebConstants.InheritanceFilePerson.NURSE) == 0 || personType
					.compareTo(WebConstants.InheritanceFilePerson.GUARDIAN) == 0)) {
				if (StringHelper.isEmpty(personView.getPassportName())) {
					errorMessages
							.add(CommonUtil
									.getBundleMessage("mems.inheritanceFile.fullNameReq"));
					validated = false;
				}
				if (personView.getGender() == null) {
					errorMessages
							.add(CommonUtil
									.getBundleMessage("mems.inheritanceFile.genderRequired"));
					validated = false;
				}
				// If Not Migrated
				if (!isMigrated) {
					validated = isNotMigratedFilePersonInfoValidated(
							personView, personType, validated);
				}
				// Is Migrated File
				else {
					validated = isMigratedFilePersonInfoValidated(personView,
							personType, validated);
				}
			}
			if (WebConstants.InheritanceFilePerson.FILE_PERSON.equalsIgnoreCase(personType) &&isUnderGuradian()
					&& StringHelper.isEmpty(personView.getReason())) {
				errorMessages
						.add(CommonUtil
								.getBundleMessage("mems.inheritanceFile.reasonRequired"));
				validated = false;
			}
				

		}
		return validated;
	}

	private boolean isMigratedFilePersonInfoValidated(PersonView personView,
			String personType, boolean validated) {
		if (personView.getDateOfBirth() != null
				&& personView.getDateOfBirth().after(new Date())) {
			errorMessages
					.add(CommonUtil
							.getBundleMessage("mems.inheritanceFile.BDnotExceedingToday"));
			validated = false;
		}

		// In case of file type is deceased and person in file person
		if (personType
				.compareTo(WebConstants.InheritanceFilePerson.FILE_PERSON) == 0
				&& isDeceased()) {
			if (personView.getDateOfDeath() != null
					&& personView.getDateOfDeath().after(new Date())) {
				errorMessages.add(CommonUtil
						.getBundleMessage("mems.inheritanceFile.DDerr"));
				validated = false;
			} else if (personView.getDateOfBirth() != null
					&& personView.getDateOfDeath() != null) {
				if (personView.getDateOfBirth().after(
						personView.getDateOfDeath())) {
					errorMessages
							.add(CommonUtil
									.getBundleMessage("errMsg.deathDateNotExceedBirthDate"));
					validated = false;
				}

			}
			if (StringHelper.isNotEmpty(personView.getCurrentWives())) {
				try {
					@SuppressWarnings("unused")
					Long currentWives = new Long(personView.getCurrentWives());
				} catch (Exception e) {
					errorMessages
							.add(CommonUtil
									.getBundleMessage("mems.inheritanceFile.NOCWNumberic"));
					validated = false;
				}
			}
			if (StringHelper.isNotEmpty(personView.getPreviousWives())) {
				try {
					@SuppressWarnings("unused")
					Long prevWives = new Long(personView.getPreviousWives());
				} catch (Exception e) {
					errorMessages
							.add(CommonUtil
									.getBundleMessage("mems.inheritanceFile.NOPWNumberic"));
					validated = false;
				}
			}
		}

		// In case file type is absent and person is file person
		if (personType
				.compareTo(WebConstants.InheritanceFilePerson.FILE_PERSON) == 0
				&& isAbsent()) {
			if (personView.getMissingFrom() != null
					&& personView.getMissingFrom().after(new Date())) {
				errorMessages
						.add(CommonUtil
								.getBundleMessage("mems.inheritanceFile.missingErrtoday"));
				validated = false;
			} else if (personView.getDateOfBirth() != null
					&& personView.getMissingFrom().before(
							personView.getDateOfBirth())) {
				errorMessages
						.add(CommonUtil
								.getBundleMessage("mems.inheritanceFile.missingErrDOB"));
				validated = false;
			}
		}
		if (personType
				.compareTo(WebConstants.InheritanceFilePerson.FILE_PERSON) == 0) {
			// check if account number
			if (StringHelper.isNotEmpty(personView.getAccountNumber())
					&& (personView.getBankId() == null || personView
							.getBankId().compareTo("-1") == 0)) {
				errorMessages
						.add(CommonUtil
								.getBundleMessage("addMissingMigratedPayments.errMsg.selectBank"));
				validated = false;
			}
		}
		return validated;
	}

	private boolean isNotMigratedFilePersonInfoValidated(PersonView personView,
			String personType, boolean validated) {
		// if(
		// personType.compareTo(WebConstants.InheritanceFilePerson.BENEFICIARY)
		// == 0 &&
		// StringHelper.isEmpty(personView.getSharePercentage())
		// )
		// {
		// errorMessages.add(CommonUtil.getBundleMessage("inheritanceFile.errMsg.provideShareValue"));
		// validated = false;
		// }

		if (StringHelper.isEmpty(personView.getAddress1())) {
			errorMessages.add(CommonUtil
					.getBundleMessage("mems.inheritanceFile.addRequired"));
			validated = false;
		}
		if (StringHelper.isEmpty(personView.getNationalityIdString())
				|| personView.getNationalityIdString().compareTo("-1") == 0) {
			errorMessages.add(CommonUtil
					.getBundleMessage("mems.inheritanceFile.nationalityReq"));
			validated = false;
		}
		// if(personView.getDateOfBirth() == null)
		// {
		// errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.DOBReq"));
		// validated = false;
		// }
		// if(StringHelper.isEmpty(personView.getCountryId()) ||
		// personView.getCountryId().compareTo("-1") == 0)
		// {
		// errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.countryReq"));
		// validated = false;
		// }
		// if(StringHelper.isEmpty(personView.getCityId()) ||
		// personView.getCityId().compareTo("-1") == 0)
		// {
		// errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.cityReq"));
		// validated = false;
		// }
		// In case of file type is deceased and person in file person
		if (personType
				.compareTo(WebConstants.InheritanceFilePerson.FILE_PERSON) == 0
				&& isDeceased()) {
			if (personView.getDateOfDeath() == null) {
				errorMessages.add(CommonUtil
						.getBundleMessage("mems.inheritanceFile.DODReq"));
				validated = false;
			} else if (personView.getDateOfDeath().after(new Date())) {
				errorMessages.add(CommonUtil
						.getBundleMessage("mems.inheritanceFile.DDerr"));
				validated = false;
			} else if (personView.getDateOfBirth() != null
					&& personView.getDateOfDeath() != null) {
				if (personView.getDateOfBirth().after(
						personView.getDateOfDeath())) {
					errorMessages
							.add(CommonUtil
									.getBundleMessage("errMsg.deathDateNotExceedBirthDate"));
					validated = false;
				}

			}
			// If its male then only check for wives
			// if( personView.getGender()!= null &&
			// !personView.getGender().equals("F") )
			// {
			// if( StringHelper.isEmpty( personView.getCurrentWives() ) )
			// {
			// errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.NOCWreq"));
			// validated = false;
			// }
			// }
			if (StringHelper.isNotEmpty(personView.getCurrentWives())) {
				try {
					@SuppressWarnings("unused")
					Long currentWives = new Long(personView.getCurrentWives());
				} catch (Exception e) {
					errorMessages
							.add(CommonUtil
									.getBundleMessage("mems.inheritanceFile.NOCWNumberic"));
					validated = false;
				}
			}
			if (StringHelper.isNotEmpty(personView.getPreviousWives())) {
				try {
					@SuppressWarnings("unused")
					Long prevWives = new Long(personView.getPreviousWives());
				} catch (Exception e) {
					errorMessages
							.add(CommonUtil
									.getBundleMessage("mems.inheritanceFile.NOPWNumberic"));
					validated = false;
				}
			}
		}

		// In case file type is absent and person is file person
		if (personType
				.compareTo(WebConstants.InheritanceFilePerson.FILE_PERSON) == 0
				&& isAbsent()) {
			if (personView.getMissingFrom() == null) {
				errorMessages
						.add(CommonUtil
								.getBundleMessage("mems.inheritanceFile.missingDateReq"));
				validated = false;
			} else if (personView.getMissingFrom().after(new Date())) {
				errorMessages
						.add(CommonUtil
								.getBundleMessage("mems.inheritanceFile.missingErrtoday"));
				validated = false;
			} else if (personView.getDateOfBirth() != null
					&& personView.getMissingFrom().before(
							personView.getDateOfBirth())) {
				errorMessages
						.add(CommonUtil
								.getBundleMessage("mems.inheritanceFile.missingErrDOB"));
				validated = false;
			}
		}
		return validated;
	}

	/**
	 * @author Kamran Ahmed
	 * @param outputMap
	 *            Desc: This method will extract the information from outputMap
	 *            and put them into viewMap with relative keys
	 */
	@SuppressWarnings("unchecked")
	private void putValuesInRelatedFileds(HashMap<String, Object> outputMap) {
		if (outputMap != null && outputMap.size() > 0) {

			if (outputMap
					.get(WebConstants.InheritanceFileOutputKeys.FILE_NUMBER) != null)
				txtFileNumber.setValue(outputMap.get(
						WebConstants.InheritanceFileOutputKeys.FILE_NUMBER)
						.toString());

			if (outputMap
					.get(WebConstants.InheritanceFileOutputKeys.REQUEST_NUMBER) != null)
				viewMap
						.put(
								WebConstants.ApplicationDetails.APPLICATION_NUMBER,
								outputMap
										.get(
												WebConstants.InheritanceFileOutputKeys.REQUEST_NUMBER)
										.toString());

			if (outputMap.get(WebConstants.InheritanceFileOutputKeys.FILE_ID) != null) {
				Long fileId = (Long) outputMap
						.get(WebConstants.InheritanceFileOutputKeys.FILE_ID);
				viewMap.put(WebConstants.InheritanceFileOutputKeys.FILE_ID,
						fileId);
				try {
					RequestView requestView = new InheritanceFileService()
							.getInheritanceFileRequestByFileId(fileId, null);
					putRequestDataInViewMap(requestView);
					viewMap.put(WebConstants.REQUEST_VIEW, requestView);
				} catch (Exception e) {
					logger.LogException("putValuesInRelatedFileds () crashed ",
							e);
					errorMessages.add(CommonUtil
							.getBundleMessage("commons.ErrorMessage"));
				}
			}
		}
	}

	/**
	 * @author Kamran Ahmed Description: This method will put all the
	 *         information in map that are required to save a inheritance file.
	 */
	@SuppressWarnings("unchecked")
	private HashMap<String, Object> getRequiredValuesMap() {

		HashMap<String, Object> fileMap = new HashMap<String, Object>();// This
																		// will
																		// hold
																		// the
																		// complete
																		// data
																		// to be
																		// stored
																		// for
																		// inheritance
																		// file

		putInheritanceFileInMap(fileMap);// Put file related information
		putRequestInMap(fileMap); // Put Request related information
		putFilePersonInMap(fileMap); // Put FilePerson related information

		if (isFounding())
			putSponsorInMap(fileMap);// only sponsor id is being used
										// currently
			// if filePerson is him/herself beneficiary then add data in
			// InhritanceBeneficiary direct,
			// without putting it in Beneficiary_Details
		if (isFilePersonBeneficiary()) {
			putBeneficiaryDetailsInMap(fileMap);// This details will be
												// extracted from Basic Info Tab
			// For saving relatives
			if (isUnderGuradian()) {
				putBeneficiaryDetailsListInMap(fileMap);// This list contains
														// the complete
														// information of
														// Beneficiary/Guardian/Nurse
														// if provided by
														// inheritanceBeneficiaryTab.jsp
				beneficiaryDetailsList = (List<BeneficiaryDetailsView>) viewMap
						.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST);
				putBeneficiaryWithGuardianInMap(beneficiaryDetailsList, fileMap);
			}
		} else {
			// This list contains the complete information of
			// Beneficiary/Guardian/Nurse if provided by
			// nheritanceBeneficiaryTab.jsp
			putBeneficiaryDetailsListInMap(fileMap);
			beneficiaryDetailsList = (List<BeneficiaryDetailsView>) viewMap
					.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST);
			putBeneficiaryWithGuardianInMap(beneficiaryDetailsList, fileMap);
		}
		return fileMap;
	}

	private void putBeneficiaryDetailsInMap(HashMap<String, Object> fileMap) {
		InheritanceBeneficiaryView inheritanceBeneficiaryView = new InheritanceBeneficiaryView();
		RequestView savedRequest = null;

		// first check if guardian Available
		if (viewMap.get(WebConstants.InheritanceFile.FILE_PERSON_GUARDIAN) != null) {
			PersonView guardianPerson = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.FILE_PERSON_GUARDIAN);
			List<BeneficiaryGuardianView> beneficiaryGuardianList = new ArrayList<BeneficiaryGuardianView>();
			BeneficiaryGuardianView beneficiaryGuardianView = new BeneficiaryGuardianView();

			if (viewMap.get(WebConstants.REQUEST_VIEW) != null) // Saved Request
			{
				savedRequest = (RequestView) viewMap
						.get(WebConstants.REQUEST_VIEW);
				if (savedRequest.getInheritanceFileView() != null
						&& savedRequest.getInheritanceFileView()
								.getInheritanceBeneficiaryList() != null
						&& savedRequest.getInheritanceFileView()
								.getInheritanceBeneficiaryList().size() > 0
						&& savedRequest.getInheritanceFileView()
								.getInheritanceBeneficiaryList().get(0)
								.getBeneficiaryGuardianViewList() != null) {
					// Existing File existing New Guardian
					if (savedRequest.getInheritanceFileView()
							.getInheritanceBeneficiaryList().get(0)
							.getBeneficiaryGuardianViewList().size() > 0) {
						beneficiaryGuardianView = savedRequest
								.getInheritanceFileView()
								.getInheritanceBeneficiaryList().get(0)
								.getBeneficiaryGuardianViewList().get(0);
					} else {
						// Existing File with New Guardian
						beneficiaryGuardianView.setBeneficiaryGuardianId(null);
						beneficiaryGuardianView
								.setInheritanceBeneficiaryId(null);
						beneficiaryGuardianView.setCreatedBy(CommonUtil
								.getLoggedInUser());
					}
				}
			} else {
				// New File and New Guardian
				beneficiaryGuardianView.setBeneficiaryGuardianId(null);
				beneficiaryGuardianView.setInheritanceBeneficiaryId(null);
				beneficiaryGuardianView.setCreatedBy(CommonUtil
						.getLoggedInUser());
			}

			beneficiaryGuardianView
					.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
			beneficiaryGuardianView
					.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);

			if (guardianPerson.getRelationshipString() != null
					&& guardianPerson.getRelationshipString().compareTo("-1") != 0) {
				beneficiaryGuardianView.setRelationshipIdString(guardianPerson
						.getRelationshipString());
			}
			beneficiaryGuardianView.setRelationshipDesc(guardianPerson
					.getRelationshipDesc());
			beneficiaryGuardianView.setGuardianId(guardianPerson.getPersonId());
			beneficiaryGuardianView.setUpdatedBy(CommonUtil.getLoggedInUser());
			beneficiaryGuardianView.setUpdatedOn(new Date());
			beneficiaryGuardianList.add(beneficiaryGuardianView);
			inheritanceBeneficiaryView = getFilledInheritanceBeneficiaryView();// This
																				// method
																				// will
																				// fill
																				// inheritanceBeneficiaryView
			inheritanceBeneficiaryView
					.setBeneficiaryGuardianViewList(beneficiaryGuardianList);// Witg
																				// Guardian
		} else {
			inheritanceBeneficiaryView = getFilledInheritanceBeneficiaryView();// This
																				// method
																				// will
																				// fill
																				// inheritanceBeneficiaryView
			inheritanceBeneficiaryView.setBeneficiaryGuardianViewList(null);// No
																			// Guardian
			if (viewMap.get("SELECTED_GUARDIAN") != null) {
				inheritanceBeneficiaryView
						.setGuardianToBeDeleted((Long) viewMap
								.get("SELECTED_GUARDIAN"));
				viewMap.remove("SELECTED_GUARDIAN");
			}
		}
		fileMap.put(
				WebConstants.InheritanceFile.METHOD_ARG_INH_BENEFICIARY_VIEW,
				inheritanceBeneficiaryView);
	}

	/**
	 * @author Kamran Ahmed Description: This method will put the RequestView in
	 *         map
	 */
	private void putRequestInMap(HashMap<String, Object> fileMap) {
		RequestView requestToSave = new RequestView();
		RequestView savedRequest = null;
		if (viewMap.get(WebConstants.REQUEST_VIEW) != null) // Saved Request
		{
			savedRequest = (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
			if (savedRequest != null && savedRequest.getRequestId() != null) {
				requestToSave = savedRequest;
				requestToSave.setUpdatedBy(CommonUtil.getLoggedInUser());
				requestToSave.setUpdatedOn(new Date());
			}
		} else // New request
		{
			requestToSave.setRequestId(null);
			requestToSave.setCreatedBy(CommonUtil.getLoggedInUser());
			requestToSave.setCreatedOn(new Date());
			requestToSave.setDepositAmount(null);
			requestToSave.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
			requestToSave.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
			requestToSave.setRequestDate(new Date());
			requestToSave.setUpdatedBy(CommonUtil.getLoggedInUser());
			requestToSave.setUpdatedOn(new Date());
			requestToSave.setAuctionView(null);
			requestToSave.setBidderView(null);
			requestToSave.setTenantsView(null);
			requestToSave.setContractView(null);
			requestToSave
					.setRequestTypeId(WebConstants.MemsRequestType.INHERITANCE_FILE);
			if (viewMap
					.get(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) != null) {
				PersonView applicantView = new PersonView();
				applicantView
						.setPersonId(Long
								.parseLong(viewMap
										.get(
												WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID)
										.toString()));
				requestToSave.setApplicantView(applicantView);
			}
			// this inheritanceFileId will be available after saving file
			requestToSave.setInheritanceFileId(null);
			DomainDataView requestStaus = null;
			// NEW for NEW request
			requestStaus = getRqstStNewDD();
			if (requestStaus != null && requestStaus.getDomainDataId() != null)
				requestToSave.setStatusId(requestStaus.getDomainDataId());
		}
		if (viewMap
				.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION) != null)
			requestToSave.setDescription(viewMap.get(
					WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION)
					.toString());

		fileMap.put(WebConstants.InheritanceFile.METHOD_ARG_REQUEST_VIEW,
				requestToSave);
	}

	/**
	 * @author Kamran Ahmed Description: Prepare InheritanceileView and put in
	 *         Map
	 */
	private void putInheritanceFileInMap(HashMap<String, Object> fileMap) {

		InheritanceFileView savedFile = null;
		InheritanceFileView fileTosave = new InheritanceFileView();

		if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) != null)
			savedFile = (InheritanceFileView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);

		if (savedFile != null && savedFile.getInheritanceFileId() != null) {
			fileTosave = savedFile;
			fileTosave.setUpdatedOn(new Date());
			fileTosave.setUpdatedBy(CommonUtil.getLoggedInUser());
			if (txtTotalFileShare != null
					&& txtTotalFileShare.trim().length() > 0) {
				fileTosave.setTotalInheritanceShare(Double
						.parseDouble(txtTotalFileShare));
			}
		} else {
			// if New file
			fileTosave.setInheritanceFileId(null);
			fileTosave.setFileNumber(null);
			fileTosave.setCreatedBy(CommonUtil.getLoggedInUser());
			fileTosave.setCreatedOn(new Date());
			fileTosave.setUpdatedOn(new Date());
			fileTosave.setUpdatedBy(CommonUtil.getLoggedInUser());
			fileTosave.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
			fileTosave.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
			fileTosave.setFileTypeId(cboFileType.getValue().toString());
			if (txtTotalFileShare != null
					&& txtTotalFileShare.trim().length() > 0) {
				fileTosave.setTotalInheritanceShare(Double
						.parseDouble(txtTotalFileShare.toString()));
			} else {
				fileTosave.setTotalInheritanceShare(0d);
			}
			// for New Request status will be 'NEW'
			DomainDataView fileStatus = getFileStNewDD();
			if (fileStatus != null && fileStatus.getDomainDataId() != null) {
				fileTosave.setStatusId(fileStatus.getDomainDataId());
			}

			// researcher will be null in new file
			fileTosave.setResearcher(null);
		}
		// if New file
		if (txtFileNumber.getValue() != null)
			fileTosave.setFileNumber(txtFileNumber.getValue().toString());
		else
			fileTosave.setFileNumber(null);

		if (txtOldCostCenter != null
				&& StringHelper.isNotEmpty(txtOldCostCenter.trim())) {
			fileTosave.setOldCostCenter(txtOldCostCenter.trim());
		}
		if (txtNewCostCenter.getValue() != null
				&& StringHelper.isNotEmpty(txtNewCostCenter.getValue()
						.toString())) {
			fileTosave.setCostCenter(txtNewCostCenter.getValue().toString());
		}

		// if file type founding only then put the sponsor
		if (isFounding()) {
			// set file sponsor
			if (viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW) != null) {
				PersonView sponsor = (PersonView) viewMap
						.get(WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW);
				if (sponsor.getPersonId() != null)
					fileTosave.setSponsor(sponsor.getPersonId().toString());// this
																			// id
																			// will
																			// be
																			// used
																			// while
																			// creating
																			// POJOs
																			// of
																			// sponsor
																			// to
																			// save
			}
		}
		// set file person
		if (viewMap
				.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW) != null)

		{
			PersonView filePerson = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW);
			fileTosave.setFilePersonId(filePerson.getPersonId().toString());// this
																			// id
																			// will
																			// be
																			// used
																			// while
																			// creating
																			// POJOs
																			// of
																			// file
																			// person
																			// to
																			// save
			if (filePerson.getMissingFrom() != null)
				fileTosave.setMissingFrom(filePerson.getMissingFrom());
			else
				fileTosave.setMissingFrom(null);
		} else
			fileTosave.setSponsor(null);

		if (fileTosave.getStatusId() != null && getRqstStNewDD() != null
				&& getRqstStApprovalRequiredDD() != null) {

			// if(
			// fileTosave.getStatusId().compareTo(getRqstStNewDD().getDomainDataId())
			// != 0 &&
			// fileTosave.getStatusId().compareTo(getRqstStApprovalRequiredDD().getDomainDataId())
			// != 0)
			if (fileTosave.getStatusId().compareTo(
					getFileStNewDD().getDomainDataId()) != 0
					&& fileTosave.getStatusId().compareTo(
							getFileStApprovalRequiredDD().getDomainDataId()) != 0) {
				// if request/file status is not new and approval required, then
				// set a flag to generate costCenter for newly added beneficiary
				fileTosave.setGenerateBeneCostCenter(true);
			}
		} else
			fileTosave.setGenerateBeneCostCenter(false);

		fileMap.put(WebConstants.InheritanceFile.METHOD_ARG_FILE, fileTosave);

	}

	/**
	 * @author Kamran Ahmed Description: This method will put the
	 *         BeneficiaryDetail in map
	 */
	@SuppressWarnings("unchecked")
	private void putBeneficiaryDetailsListInMap(HashMap<String, Object> fileMap) {
		if (viewMap.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST) != null) {
			beneficiaryDetailsList = (List<BeneficiaryDetailsView>) viewMap
					.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST);
			fileMap
					.put(
							WebConstants.InheritanceFile.METHOD_ARG_BENEFICIARY_DETAILS_LIST,
							beneficiaryDetailsList);
		}
	}

	/**
	 * @author Kamran Ahmed Description: This method will put the FilePerson in
	 *         map
	 */
	private void putFilePersonInMap(HashMap<String, Object> fileMap) {
		if (viewMap
				.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW) != null) {
			PersonView filePerson = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW);
			filePerson.setUpdatedBy(CommonUtil.getLoggedInUser());
			fileMap.put(WebConstants.InheritanceFile.METHOD_ARG_FILE_PERSON,
					filePerson);
		}
	}

	@SuppressWarnings("unused")
	private void putSponsorInMap(HashMap<String, Object> fileMap) {
		if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW) != null) {
			PersonView sponsor = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW);
			sponsor.setUpdatedBy(CommonUtil.getLoggedInUser());
			fileMap.put(WebConstants.InheritanceFile.METHOD_ARG_SPONSOR,
					sponsor);
		} else
			errorMessages.add(CommonUtil
					.getBundleMessage("errMsg.fillSponsorInfo"));
	}

	/**
	 * @author Kamran Ahmed Description: This will put the beneficiary and
	 *         guardian in map
	 */
	private void putBeneficiaryWithGuardianInMap(
			List<BeneficiaryDetailsView> beneficiaryDetailsList,
			HashMap<String, Object> fileMap) {
		List<InheritanceBeneficiaryView> beneficiaryList = new ArrayList<InheritanceBeneficiaryView>();

		if (beneficiaryDetailsList != null && beneficiaryDetailsList.size() > 0) {

			for (BeneficiaryDetailsView beneficiaryDetail : beneficiaryDetailsList) {
				InheritanceBeneficiaryView inheritanceBeneficiaryView = new InheritanceBeneficiaryView();
				// first check if guardian Available
				if (beneficiaryDetail.getGuardianId() != null) {
					// here we are considering single guardian against
					// beneficiary
					// but it is possible that there will be more than one,
					// in that case iterate each beneficiaryDetail further to
					// collect the guardian list and set this list to
					// inheritanceBeneficiaryView

					List<BeneficiaryGuardianView> beneficiaryGuardianList = new ArrayList<BeneficiaryGuardianView>();
					BeneficiaryGuardianView beneficiaryGuardianView = new BeneficiaryGuardianView();

					// already saved
					if (beneficiaryDetail.getBeneficiaryGuardianId() != null) {
						beneficiaryGuardianView
								.setBeneficiaryGuardianId(beneficiaryDetail
										.getBeneficiaryGuardianId());
						beneficiaryGuardianView
								.setInheritanceBeneficiaryId(beneficiaryDetail
										.getInheritanceBeneficiaryId());
						// The person who has created the file will be same to
						// create beneficiaryGuardian
						beneficiaryGuardianView.setCreatedBy(beneficiaryDetail
								.getBeneficiary().getUpdatedBy());
					} else // New Guardian
					{
						beneficiaryGuardianView.setBeneficiaryGuardianId(null);
						beneficiaryGuardianView
								.setInheritanceBeneficiaryId(null);
						beneficiaryGuardianView.setCreatedBy(CommonUtil
								.getLoggedInUser());
					}
					beneficiaryGuardianView
							.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
					beneficiaryGuardianView
							.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
					beneficiaryGuardianView
							.setRelationshipIdString(beneficiaryDetail
									.getRelationshipStringGuardian());
					beneficiaryGuardianView
							.setRelationshipDesc(beneficiaryDetail
									.getRelationshipDescGuardian());
					// guardian person can be changed, so we will reload it in
					// both case: New/Edit
					beneficiaryGuardianView.setGuardianId(beneficiaryDetail
							.getGuardianId());// this is the person Id og
												// guardian
					beneficiaryGuardianView.setUpdatedBy(CommonUtil
							.getLoggedInUser());
					beneficiaryGuardianView.setUpdatedOn(new Date());

					beneficiaryGuardianList.add(beneficiaryGuardianView);

					inheritanceBeneficiaryView = getFilledInheritanceBeneficiaryView(beneficiaryDetail);// This
																										// method
																										// will
																										// fill
																										// inheritanceBeneficiaryView
					inheritanceBeneficiaryView
							.setBeneficiaryGuardianViewList(beneficiaryGuardianList);
				} else// if guardian not Available
				{

					inheritanceBeneficiaryView = getFilledInheritanceBeneficiaryView(beneficiaryDetail);// This
																										// method
																										// will
																										// fill
																										// inheritanceBeneficiaryView
					inheritanceBeneficiaryView
							.setBeneficiaryGuardianViewList(null);
				}
				inheritanceBeneficiaryView
						.setGuardianToBeDeleted(beneficiaryDetail
								.getGuardianToBeDeleted());
				inheritanceBeneficiaryView.setOldCostCenter(beneficiaryDetail
						.getOldCostCenter());
				inheritanceBeneficiaryView.setCostCenter(beneficiaryDetail
						.getCostCenter());

				beneficiaryList.add(inheritanceBeneficiaryView);
			}
		}
		fileMap.put(
				WebConstants.InheritanceFile.METHOD_ARG_BENEFICIARY_VIEW_LIST,
				beneficiaryList);
	}

	private InheritanceBeneficiaryView getFilledInheritanceBeneficiaryView(
			BeneficiaryDetailsView beneficiarydetail) {
		InheritanceBeneficiaryView inheritanceBeneficiaryView = new InheritanceBeneficiaryView();

		// saved inheritanceBeneficiary

		if (beneficiarydetail.getInheritanceBeneficiaryId() != null) {
			inheritanceBeneficiaryView
					.setInheritanceBeneficiaryId(beneficiarydetail
							.getInheritanceBeneficiaryId());
			InheritanceFileView savedFile = null;
			if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) != null)
				savedFile = (InheritanceFileView) viewMap
						.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);
			inheritanceBeneficiaryView.setInheritanceFileView(savedFile);
			inheritanceBeneficiaryView.setCreatedBy(savedFile.getCreatedBy());
			inheritanceBeneficiaryView.setIsDeleted(beneficiarydetail
					.isDeleted() ? WebConstants.IS_DELETED_TRUE
					: WebConstants.DEFAULT_IS_DELETED);
			inheritanceBeneficiaryView.setRecordStatus(beneficiarydetail
					.isDeleted() ? 0L : WebConstants.DEFAULT_RECORD_STATUS);
		}
		// if new beneficiary
		else {
			inheritanceBeneficiaryView.setInheritanceBeneficiaryId(null);
			inheritanceBeneficiaryView.setInheritanceFileView(null);
			inheritanceBeneficiaryView.setCreatedBy(CommonUtil
					.getLoggedInUser());
			inheritanceBeneficiaryView
					.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
			inheritanceBeneficiaryView
					.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);

		}
		// This information can be changed by the user, so reload it for both
		// cases : New /Edit
		inheritanceBeneficiaryView.setBeneficiaryId(beneficiarydetail
				.getBeneficiary().getPersonId());
		if (beneficiarydetail.getRelationshipId() != null) {
			inheritanceBeneficiaryView
					.setRelationshipIdString(beneficiarydetail
							.getRelationshipId().toString());
			inheritanceBeneficiaryView.setRelationshipDesc(beneficiarydetail
					.getBeneficiary().getRelationshipDesc());

		}

		if (beneficiarydetail.getNurseId() != null) // fetch Nurse details only
													// if one will be available
		{
			inheritanceBeneficiaryView.setNurseId(beneficiarydetail
					.getNurseId());
			inheritanceBeneficiaryView
					.setNurseRelationshipIdString(beneficiarydetail
							.getRelationshipStringNurse());
			inheritanceBeneficiaryView
					.setNurseRelationshipDesc(beneficiarydetail
							.getRelationshipDescNurse());
		}

		if (beneficiarydetail.getSharePercentage() != null
				&& StringHelper.isNotEmpty(beneficiarydetail
						.getSharePercentage()))
			inheritanceBeneficiaryView.setSharePercentage(Double
					.parseDouble(beneficiarydetail.getSharePercentage()));

		if (beneficiarydetail.getBeneficiary().getIncomeCategoryId() != null
				&& StringHelper.isNotEmpty(beneficiarydetail.getBeneficiary()
						.getIncomeCategoryId()))
			inheritanceBeneficiaryView.setIncomeCategory(beneficiarydetail
					.getBeneficiary().getIncomeCategoryId());

		inheritanceBeneficiaryView.setIsStudent(beneficiarydetail
				.getBeneficiary().isStudent() ? 1L : 0L);
		inheritanceBeneficiaryView.setIsMinor(beneficiarydetail
				.getBeneficiary().isMinor() ? 1L : 0L);
		inheritanceBeneficiaryView.setUpdatedOn(new Date());
		inheritanceBeneficiaryView.setUpdatedBy(CommonUtil.getLoggedInUser());
		if (isUnderGuradian())
			inheritanceBeneficiaryView.setSelfBeneficiary(false);
		else
			inheritanceBeneficiaryView.setSelfBeneficiary(true);

		inheritanceBeneficiaryView.setIsApplicant(beneficiarydetail
				.getIsApplicant());
		return inheritanceBeneficiaryView;
	}

	public List<DomainDataView> getRequestStatusListDD() {
		return requestStatusListDD;
	}

	public void setRequestStatusListDD(List<DomainDataView> requestStatusListDD) {
		this.requestStatusListDD = requestStatusListDD;
	}

	public DomainDataView getDeceasedDDV() {
		return deceasedDDV;
	}

	public void setDeceasedDDV(DomainDataView deceasedDDV) {
		this.deceasedDDV = deceasedDDV;
	}

	public DomainDataView getUnderGuardianshipDDV() {
		return underGuardianshipDDV;
	}

	public void setUnderGuardianshipDDV(DomainDataView underGuardianshipDDV) {
		this.underGuardianshipDDV = underGuardianshipDDV;
	}

	public DomainDataView getLegalAssitanceDDV() {
		return legalAssitanceDDV;
	}

	public void setLegalAssitanceDDV(DomainDataView legalAssitanceDDV) {
		this.legalAssitanceDDV = legalAssitanceDDV;
	}

	public DomainDataView getFoundingDDV() {
		if (viewMap.get(WebConstants.InheritanceFileType.FOUNDING) != null)
			foundingDDV = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileType.FOUNDING);
		return foundingDDV;
	}

	public void setFoundingDDV(DomainDataView foundingDDV) {
		this.foundingDDV = foundingDDV;
	}

	public DomainDataView getAbsentDDV() {
		return absentDDV;
	}

	public void setAbsentDDV(DomainDataView absentDDV) {
		this.absentDDV = absentDDV;
	}

	public DomainDataView getRqstStNewDD() {
		if (viewMap.get(WebConstants.REQUEST_STATUS_NEW) != null)
			rqstStNewDD = (DomainDataView) viewMap
					.get(WebConstants.REQUEST_STATUS_NEW);
		return rqstStNewDD;
	}

	public void setRqstStNewDD(DomainDataView rqstStNewDD) {
		this.rqstStNewDD = rqstStNewDD;
	}

	public DomainDataView getRqstStApprovalRequiredDD() {
		if (viewMap.get(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED) != null)
			rqstStApprovalRequiredDD = (DomainDataView) viewMap
					.get(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
		return rqstStApprovalRequiredDD;
	}

	public void setRqstStApprovalRequiredDD(
			DomainDataView rqstStApprovalRequiredDD) {
		this.rqstStApprovalRequiredDD = rqstStApprovalRequiredDD;
	}

	public DomainDataView getRqstStRejectedResubmitDD() {
		if (viewMap.get(WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT) != null)
			rqstStRejectedResubmitDD = (DomainDataView) viewMap
					.get(WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT);
		return rqstStRejectedResubmitDD;
	}

	public void setRqstStRejectedResubmitDD(
			DomainDataView rqstStRejectedResubmitDD) {
		this.rqstStRejectedResubmitDD = rqstStRejectedResubmitDD;
	}

	public DomainDataView getRqstStApprovedDD() {
		if (viewMap.get(WebConstants.REQUEST_STATUS_APPROVED) != null)
			rqstStApprovedDD = (DomainDataView) viewMap
					.get(WebConstants.REQUEST_STATUS_APPROVED);
		return rqstStApprovedDD;
	}

	public void setRqstStApprovedDD(DomainDataView rqstStApprovedDD) {
		this.rqstStApprovedDD = rqstStApprovedDD;
	}

	public DomainDataView getRqstStInitLimDoneDD() {
		if (viewMap.get(WebConstants.REQUEST_STATUS_INITIAL_LIMITATION_DONE) != null)
			rqstStInitLimDoneDD = (DomainDataView) viewMap
					.get(WebConstants.REQUEST_STATUS_INITIAL_LIMITATION_DONE);
		return rqstStInitLimDoneDD;
	}

	public void setRqstStInitLimDoneDD(DomainDataView rqstStInitLimDoneDD) {
		this.rqstStInitLimDoneDD = rqstStInitLimDoneDD;
	}

	public DomainDataView getRqstStFinalLimDoneDD() {
		if (viewMap.get(WebConstants.REQUEST_STATUS_FINAL_LIMITATION_DONE) != null)
			rqstStFinalLimDoneDD = (DomainDataView) viewMap
					.get(WebConstants.REQUEST_STATUS_FINAL_LIMITATION_DONE);
		return rqstStFinalLimDoneDD;
	}

	public void setRqstStFinalLimDoneDD(DomainDataView rqstStFinalLimDoneDD) {
		this.rqstStFinalLimDoneDD = rqstStFinalLimDoneDD;
	}

	public DomainDataView getRqstStLimRejectedDD() {
		if (viewMap.get(WebConstants.REQUEST_STATUS_LIMITATION_REJECTED) != null)
			rqstStLimRejectedDD = (DomainDataView) viewMap
					.get(WebConstants.REQUEST_STATUS_LIMITATION_REJECTED);
		return rqstStLimRejectedDD;
	}

	public void setRqstStLimRejectedDD(DomainDataView rqstStLimRejectedDD) {
		this.rqstStLimRejectedDD = rqstStLimRejectedDD;
	}

	public DomainDataView getFileStNewDD() {
		if (viewMap.get(WebConstants.InheritanceFileStatus.NEW) != null)
			fileStNewDD = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileStatus.NEW);
		return fileStNewDD;
	}

	public void setFileStNewDD(DomainDataView fileStNewDD) {
		this.fileStNewDD = fileStNewDD;
	}

	public DomainDataView getFileStApprovalRequiredDD() {
		if (viewMap.get(WebConstants.InheritanceFileStatus.APPROVAL_REQUIRED) != null)
			fileStApprovalRequiredDD = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileStatus.APPROVAL_REQUIRED);
		return fileStApprovalRequiredDD;
	}

	public void setFileStApprovalRequiredDD(
			DomainDataView fileStApprovalRequiredDD) {
		this.fileStApprovalRequiredDD = fileStApprovalRequiredDD;
	}

	public DomainDataView getFileStRejectedResubmitDD() {
		if (viewMap.get(WebConstants.InheritanceFileStatus.REJECTED_RESUBMIT) != null)
			fileStRejectedResubmitDD = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileStatus.REJECTED_RESUBMIT);
		return fileStRejectedResubmitDD;
	}

	public void setFileStRejectedResubmitDD(
			DomainDataView fileStRejectedResubmitDD) {
		this.fileStRejectedResubmitDD = fileStRejectedResubmitDD;
	}

	public DomainDataView getFileStApprovedDD() {
		if (viewMap.get(WebConstants.InheritanceFileStatus.APPROVED) != null)
			fileStApprovedDD = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileStatus.APPROVED);
		return fileStApprovedDD;
	}

	public void setFileStApprovedDD(DomainDataView fileStApprovedDD) {
		this.fileStApprovedDD = fileStApprovedDD;
	}

	public DomainDataView getFileStInitLimDoneDD() {
		if (viewMap
				.get(WebConstants.InheritanceFileStatus.INITIAL_LIMITATION_DONE) != null)
			fileStInitLimDoneDD = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileStatus.INITIAL_LIMITATION_DONE);
		return fileStInitLimDoneDD;
	}

	public void setFileStInitLimDoneDD(DomainDataView fileStInitLimDoneDD) {
		this.fileStInitLimDoneDD = fileStInitLimDoneDD;
	}

	public DomainDataView getFileStFinalLimDoneDD() {
		if (viewMap
				.get(WebConstants.InheritanceFileStatus.FINAL_LIMITATION_DONE) != null)
			fileStFinalLimDoneDD = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileStatus.FINAL_LIMITATION_DONE);
		return fileStFinalLimDoneDD;
	}

	public void setFileStFinalLimDoneDD(DomainDataView fileStFinalLimDoneDD) {
		this.fileStFinalLimDoneDD = fileStFinalLimDoneDD;
	}

	public DomainDataView getFileStLimRejectedDD() {
		if (viewMap.get(WebConstants.InheritanceFileStatus.LIMITATION_REJECTED) != null)
			fileStLimRejectedDD = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileStatus.LIMITATION_REJECTED);
		return fileStLimRejectedDD;
	}

	public void setFileStLimRejectedDD(DomainDataView fileStLimRejectedDD) {
		this.fileStLimRejectedDD = fileStLimRejectedDD;
	}

	public List<DomainDataView> getInherFilestatusListDD() {
		return inherFilestatusListDD;
	}

	public void setInherFilestatusListDD(
			List<DomainDataView> inherFilestatusListDD) {
		this.inherFilestatusListDD = inherFilestatusListDD;
	}

	public List<BeneficiaryDetailsView> getBeneficiaryDetailsList() {
		return beneficiaryDetailsList;
	}

	public void setBeneficiaryDetailsList(
			List<BeneficiaryDetailsView> beneficiaryDetailsList) {
		this.beneficiaryDetailsList = beneficiaryDetailsList;
	}

	private String getNameByPersonView(PersonView personView) {
		String completeName = "";
		if (personView.getIsCompany().compareTo(1L) == 0) {
			completeName = personView.getCompanyName();
		} else {
			if (StringHelper.isNotEmpty(personView.getFirstName())) {
				completeName = personView.getFirstName();
				if (StringHelper.isNotEmpty(personView.getMiddleName())) {
					completeName = completeName + " "
							+ personView.getMiddleName();
					if (StringHelper.isNotEmpty(personView.getLastName()))
						completeName = completeName + " "
								+ personView.getLastName();

				}
			}
		}
		return completeName;
	}

	@SuppressWarnings( { "unchecked", "unused" })
	private void setPageModeByFilesStatus() {
		Long fileStatusId = null;
		if (viewMap.get(WebConstants.InheritanceFile.FILE_STATUS_ID) != null)
			fileStatusId = (Long) viewMap
					.get(WebConstants.InheritanceFile.FILE_STATUS_ID);
		if (fileStatusId != null) {
			if (fileStatusId.compareTo(getFileStNewDD().getDomainDataId()) == 0) {
				pageMode = WebConstants.InheritanceFilePageMode.PAGE_MODE_NEW;
				canAddAttachmentsAndComments(true);
			} else if (fileStatusId.compareTo(getFileStApprovalRequiredDD()
					.getDomainDataId()) == 0) {
				pageMode = WebConstants.InheritanceFilePageMode.PAGE_MODE_APPROVAL_REQ;
				canAddAttachmentsAndComments(true);
			} else if (fileStatusId.compareTo(getFileStApprovedDD()
					.getDomainDataId()) == 0) {
				pageMode = WebConstants.InheritanceFilePageMode.PAGE_MODE_APPROVED;
				viewMap.put("applicationDetailsReadonlyMode", true);
				canAddAttachmentsAndComments(true);
			} else if (fileStatusId.compareTo(getFileStRejectedResubmitDD()
					.getDomainDataId()) == 0) {
				pageMode = WebConstants.InheritanceFilePageMode.PAGE_MODE_RJCTD_RESBMT;
				canAddAttachmentsAndComments(true);
			} else if (fileStatusId.compareTo(getFileStInitLimDoneDD()
					.getDomainDataId()) == 0) {
				pageMode = WebConstants.InheritanceFilePageMode.PAGE_MODE_INIT_LIM_DONE;
				viewMap.put("applicationDetailsReadonlyMode", true);
				canAddAttachmentsAndComments(true);
			} else if (fileStatusId.compareTo(getFileStFinalLimDoneDD()
					.getDomainDataId()) == 0) {
				pageMode = WebConstants.InheritanceFilePageMode.PAGE_MODE_FINAL_LIM_DONE;
				viewMap.put("applicationDetailsReadonlyMode", true);
				canAddAttachmentsAndComments(true);
			} else if (fileStatusId.compareTo(getFileStLimRejectedDD()
					.getDomainDataId()) == 0) {
				pageMode = WebConstants.InheritanceFilePageMode.PAGE_MODE_INIT_LIM_RJCTD;
				viewMap.put("applicationDetailsReadonlyMode", true);
				canAddAttachmentsAndComments(true);
			}

			else if (fileStatusId.compareTo(getFileStClosedDD()
					.getDomainDataId()) == 0) {
				pageMode = WebConstants.InheritanceFilePageMode.PAGE_MODE_READ_ONLY;
				viewMap.put("applicationDetailsReadonlyMode", true);
				canAddAttachmentsAndComments(false);
			} else if (fileStatusId.compareTo(getFileStDistributionReqDD()
					.getDomainDataId()) == 0) {
				pageMode = WebConstants.InheritanceFilePageMode.PAGE_MODE_DIST_REQ;
				viewMap.put("applicationDetailsReadonlyMode", true);
				canAddAttachmentsAndComments(true);
			} else {
				pageMode = WebConstants.InheritanceFilePageMode.PAGE_MODE_READ_ONLY;
				viewMap.put("applicationDetailsReadonlyMode", true);
				canAddAttachmentsAndComments(false);
			}
			if (viewMap.get(WebConstants.InheritanceFilePageMode.IS_POPUP) != null) {
				pageMode = WebConstants.InheritanceFilePageMode.PAGE_MODE_READ_ONLY;
				viewMap.put("applicationDetailsReadonlyMode", true);
				canAddAttachmentsAndComments(false);
			}

		}

		if (pageMode != null && StringHelper.isNotEmpty(pageMode))
			viewMap.put(WebConstants.InheritanceFilePageMode.PAGE_MODE,
					pageMode);
	}

	public String getPageMode() {
		if (viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE) != null)
			pageMode = viewMap.get(
					WebConstants.InheritanceFilePageMode.PAGE_MODE).toString();
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	/**
	 * This will sent the request for approval
	 */
	@SuppressWarnings("unchecked")
	public void submitFile() {
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);

		try {
			RequestView requestToSubmit = null;
			if (viewMap.get(WebConstants.REQUEST_VIEW) != null) // Saved Request
			{
				if (isBeneficiariesShareValidated())// isSharesSumValidated())
				{
					saveFile();
					if (viewMap.get("FILE_SAVED") != null
							&& (Boolean) viewMap.get("FILE_SAVED")) {
						requestToSubmit = (RequestView) viewMap
								.get(WebConstants.REQUEST_VIEW);
						if (requestToSubmit != null
								&& requestToSubmit.getRequestId() != null) {
							new InheritanceFileService()
									.setRequestAndFileAsApprovalReq(
											requestToSubmit.getRequestId(),
											requestToSubmit
													.getInheritanceFileView()
													.getInheritanceFileId(),
											CommonUtil.getLoggedInUser());
							DomainDataView fileStatusDD = getFileStApprovalRequiredDD();
							saveAttachmentComment();
							saveSystemComments(MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL);

							viewMap
									.put(
											WebConstants.InheritanceFile.FILE_STATUS_DESC,
											isEnglishLocale() ? fileStatusDD
													.getDataDescEn()
													: fileStatusDD
															.getDataDescAr());
							viewMap
									.put(
											WebConstants.ApplicationDetails.APPLICATION_STATUS,
											CommonUtil.getIsEnglishLocale() ? getRqstStApprovalRequiredDD()
													.getDataDescEn()
													: getRqstStNewDD()
															.getDataDescAr());
							getTxtFileStatus();
							btnSubmitRequest.setRendered(false);
							Long fileId = new Long(viewMap.get(
									WebConstants.InheritanceFile.FILE_ID)
									.toString());
							invokeOpenFileBpel(requestToSubmit.getRequestId(),
									fileId);
							requestToSubmit = new InheritanceFileService()
									.getInheritanceFileRequestByFileId(
											requestToSubmit
													.getInheritanceFileId(),
											null);
							getFileDetails(fileId, requestToSubmit);
							successMessages.add(CommonUtil
									.getBundleMessage("renew.msg.TaskSent"));
							viewMap
									.put(
											WebConstants.InheritanceFilePageMode.PAGE_MODE,
											WebConstants.InheritanceFilePageMode.PAGE_MODE_APPROVAL_REQ);
						}
					}
				}
			} else

			{
				errorMessages.add(CommonUtil
						.getBundleMessage("errMsg.saveFile"));
				btnSaveRequest.setDisabled(false);
				btnSubmitRequest.setDisabled(false);

			}
		} catch (Exception e) {
			logger.LogException("submitFile crashed", e);
			errorMessages
					.add(CommonUtil
							.getBundleMessage("mems.inheritanceFile.errMsg.requestNotSent"));
		}

	}

	@SuppressWarnings("unchecked")
	private boolean isSharesSumValidated() {
		boolean isValidated = true;
		if (viewMap.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST) != null) {
			List<BeneficiaryDetailsView> beneDetails = null;
			beneDetails = (List<BeneficiaryDetailsView>) viewMap
					.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST);
			if (getTotalSharePercentage(beneDetails).compareTo(100D) < 0) {
				errorMessages.add(CommonUtil
						.getBundleMessage("errMsg.ShareSumMBHundred"));
				isValidated = false;
			}
		}
		return isValidated;
	}

	public boolean hasApprovalError() throws Exception {
		boolean hasErrors = false;
		if (!isFilePersonBeneficiary()) {
			if (!isBeneficiaryDetailsValidated()) {
				fileTabPanel.setSelectedTab(TAB_ID_BENEFICIARY);
				return true;
			}
			// check beneficiaries share and file share equality
			if (!isBeneficiariesShareValidated()) {
				errorMessages
						.add(CommonUtil
								.getBundleMessage("inheritanceFile.errMsg.saheTotalMustBeLEToTotalShare"));
				return true;
			}
		}
		return hasErrors;

	}

	/**
	 * This will approved the request
	 */
	@SuppressWarnings("unchecked")
	public void approveRequest() {
		RequestView requestToApprove = null;
		try {
			if (viewMap.get(WebConstants.REQUEST_VIEW) == null) {
				return;
			}
			requestToApprove = (RequestView) viewMap
					.get(WebConstants.REQUEST_VIEW);
			if (requestToApprove == null
					|| requestToApprove.getRequestId() == null) {
				logger.logDebug("approveRequest|requestToApprove Is null");
				return;
			}
			if (hasApprovalError())
				return;
			new InheritanceFileService().setRequestAndFileAsApproved(
					requestToApprove.getRequestId(), requestToApprove
							.getInheritanceFileView().getInheritanceFileId(),
					CommonUtil.getLoggedInUser());

			DomainDataView fileStatusDD = getFileStApprovedDD();
			if (!completeTask(TaskOutcome.APPROVE)) {
				logger
						.logDebug("approveRequest|completeTask did not return value");
				return;
			}
			saveAttachmentComment();
			saveSystemComments(MessageConstants.RequestEvents.REQUEST_APPROVED);
			successMessages
					.add(CommonUtil
							.getBundleMessage("mems.inheritanceFile.successMsg.filerequestApproved"));
			viewMap.put(WebConstants.InheritanceFile.FILE_STATUS_ID,
					fileStatusDD.getDomainDataId());
			viewMap.put(WebConstants.InheritanceFile.FILE_STATUS_DESC,
					isEnglishLocale() ? fileStatusDD.getDataDescEn()
							: fileStatusDD.getDataDescAr());
			viewMap
					.put(
							WebConstants.ApplicationDetails.APPLICATION_STATUS,
							CommonUtil.getIsEnglishLocale() ? getRqstStApprovedDD()
									.getDataDescEn()
									: getRqstStNewDD().getDataDescAr());
			btnApproveRequest.setRendered(false);
			btnRejectResubmit.setRendered(false);
			cboFileType.setDisabled(true);
			getTxtFileStatus();
			canAddAttachmentsAndComments(false);
			requestToApprove = new InheritanceFileService()
					.getInheritanceFileRequestByFileId(requestToApprove
							.getInheritanceFileId(), null);
			getFileDetails(requestToApprove.getInheritanceFileId(),
					requestToApprove);

		} catch (Exception e) {
			logger.LogException("approveRequest|crashed", e);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}
	}

	/**
	 * This will reject and submit the request
	 */
	@SuppressWarnings("unchecked")
	public void rejectRequest() {
		RequestView requestToReject = null;
		String methodName = "rejectRequest()";
		logger.logInfo(methodName + " started...");
		try {
			if (viewMap.get(WebConstants.REQUEST_VIEW) != null) // Saved Request
			{
				requestToReject = (RequestView) viewMap
						.get(WebConstants.REQUEST_VIEW);
				if (requestToReject != null
						&& requestToReject.getRequestId() != null) {
					try {
						new InheritanceFileService()
								.setRequestAndFileAsRejected(requestToReject
										.getRequestId(), requestToReject
										.getInheritanceFileView()
										.getInheritanceFileId(), CommonUtil
										.getLoggedInUser());
					} catch (Exception e) {
						logger.LogException("rejectRequest() crashed", e);
						errorMessages.add(CommonUtil
								.getBundleMessage("commons.ErrorMessage"));
					}
					DomainDataView fileStatusDD = getFileStRejectedResubmitDD();
					if (completeTask(TaskOutcome.REJECT)) {
						saveAttachmentComment();
						saveSystemComments(MessageConstants.RequestEvents.REQUEST_REJECTED);
						successMessages
								.add(CommonUtil
										.getBundleMessage("mems.inheritanceFile.successMsg.filerequestRejected"));
						// viewMap.put(WebConstants.InheritanceFile.FILE_STATUS_ID,
						// fileStatusDD.getDomainDataId());
						viewMap.put(
								WebConstants.InheritanceFile.FILE_STATUS_DESC,
								isEnglishLocale() ? fileStatusDD
										.getDataDescEn() : fileStatusDD
										.getDataDescAr());
						viewMap
								.put(
										WebConstants.ApplicationDetails.APPLICATION_STATUS,
										CommonUtil.getIsEnglishLocale() ? getRqstStRejectedResubmitDD()
												.getDataDescEn()
												: getRqstStNewDD()
														.getDataDescAr());
						getTxtFileStatus();
						requestToReject = new InheritanceFileService()
								.getInheritanceFileRequestByFileId(
										requestToReject.getInheritanceFileId(),
										null);
						getFileDetails(requestToReject.getInheritanceFileId(),
								requestToReject);
					}
				}
			}
			logger.logInfo(methodName + " completed successfully...");
		} catch (Exception e) {
			logger.LogException(" rejectRequest() crashed", e);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}
	}

	/**
	 * This action will be called when the tab is being clicked
	 */
	@SuppressWarnings("unchecked")
	public void inheritedAssetTabClick() {
		loadInheritedAssetTabDetails();

	}

	@SuppressWarnings("unchecked")
	public List<InheritedAssetView> loadInheritedAssetTabDetails() {
		if (viewMap.get(INHERITED_ASSET_MEMS_VIEW_LIST) == null) {
			if (viewMap.get(WebConstants.InheritanceFile.FILE_ID) != null) {
				Long fileId = null;
				fileId = new Long(viewMap.get(
						WebConstants.InheritanceFile.FILE_ID).toString());
				List<InheritedAssetView> iAssetList = new ArrayList<InheritedAssetView>();
				List<InheritedAssetView> iAssetListToBeSentToAssetTab = new ArrayList<InheritedAssetView>();
				try {
					iAssetList = new InheritanceFileService()
							.getInheritedAssetsByFileId(fileId);
					if (iAssetList != null && iAssetList.size() > 0) {
						for (InheritedAssetView iAssetView : iAssetList) {

							if (iAssetView.getIsDeleted().compareTo(
									WebConstants.IS_DELETED_TRUE) == 0)
								continue;
							if (iAssetView.getAssetMemsView().getManagerId() == null) {
								iAssetView
										.getAssetMemsView()
										.setManagerName(
												CommonUtil
														.getBundleMessage("inheritanceFile.inheritedAssetTab.managerAmaf"));
							}
							iAssetListToBeSentToAssetTab.add(iAssetView);

						}
						viewMap.put(INHERITED_ASSET_MEMS_VIEW_LIST,
								iAssetListToBeSentToAssetTab);
						return iAssetListToBeSentToAssetTab;
					}
				} catch (Exception e) {
					logger.LogException("inheritedAssetTabClick() crashed", e);
					errorMessages.add(CommonUtil
							.getBundleMessage("commons.ErrorMessage"));
				}

			}
		} else {
			return (ArrayList<InheritedAssetView>) viewMap
					.get(INHERITED_ASSET_MEMS_VIEW_LIST);
		}
		return null;
	}

	public String getRevenueTypeDescById(Long id) {
		String desc = null;
		DomainDataView selectRevenueType = new DomainDataView();
		selectRevenueType = getRevenueDDById(id);
		if (selectRevenueType != null
				&& selectRevenueType.getDomainDataId() != null) {
			desc = CommonUtil.getIsEnglishLocale() ? selectRevenueType
					.getDataDescEn() : selectRevenueType.getDataDescAr();
		}
		return desc;
	}

	public DomainDataView getRevenueDDById(Long id) {
		revenueTypeMap = getRevenueTypeMap();
		if (revenueTypeMap != null && revenueTypeMap.size() > 0
				&& revenueTypeMap.containsKey(id)) {
			return revenueTypeMap.get(id);
		} else
			return null;
	}

	@SuppressWarnings("unchecked")
	public HashMap<Long, DomainDataView> getRevenueTypeMap() {
		if (viewMap.get(REVENUE_TYPE_MAP) != null)
			revenueTypeMap = (HashMap<Long, DomainDataView>) viewMap
					.get(REVENUE_TYPE_MAP);
		return revenueTypeMap;
	}

	@SuppressWarnings("unchecked")
	public void receiveAsset() {
		if (StringHelper.isNotEmpty(hdnAssetId)) {
			AssetMemsView assetMemsView = new AssetMemsView();
			try {
				assetMemsView = new InheritanceFileService()
						.getAssetMemsById(Long.parseLong(hdnAssetId));
				if (assetMemsView != null && assetMemsView.getAssetId() != null) {
					assetMemsView.setRevenueTypeString(CommonUtil
							.getIsEnglishLocale() ? assetMemsView
							.getAssetTypeView().getAssetTypeNameEn()
							: assetMemsView.getAssetTypeView()
									.getAssetTypeNameAr());
					// assetMemsView.setExtractedFromInheritedAsstId(assetMemsView.getInheritedAssetViewList().get(0).getInheritedAssetId());
					// assetMemsView.setAssociatedCncrndDepttId(assetMemsView.getConcernedDepartmentViewList().get(0).getMemsAssetCncrndDeptId());
					viewMap.put(ASSET_MEMS_VIEW, assetMemsView);
					if (assetMemsView.getConcernedDepartmentViewList() != null
							&& assetMemsView.getConcernedDepartmentViewList()
									.size() > 0)
						viewMap.put(MEMS_CNRCRND_DEPT_VIEW, assetMemsView
								.getConcernedDepartmentViewList().get(0));
					viewMap.put(IS_ASSET_SELECTED_FROM_SEARCH, true);
					InheritedAssetsTabBean iAssetBean = (InheritedAssetsTabBean) getBean("pages$inheritedAssetsTab");
					iAssetBean.assetTypeChanged();
				}
			} catch (Exception e) {
				logger.LogException("inheritedAssetTabClick() crashed", e);
				errorMessages.add(CommonUtil
						.getBundleMessage("commons.ErrorMessage"));
			}
		}
	}

	public String getHdnAssetId() {
		return hdnAssetId;
	}

	public void setHdnAssetId(String hdnAssetId) {
		this.hdnAssetId = hdnAssetId;
	}

	public void basicInfoTabClick() {
		fileTypeChanged();// This will reRender the fields, based on file
							// type.
	}

	public void inheritanceBeneficiaryTabClick() {
		fileTypeChanged();// This will reRender the fields, based on file
							// type.
	}
	public void onBeneficiaryRepresentativeTab() {
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			BeneficiaryRepresentativeTabBacking tab = (BeneficiaryRepresentativeTabBacking) getBean("pages$tabBeneficiaryRepresentative");
			if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) != null) {
				InheritanceFileView vo = (InheritanceFileView) viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);
				tab.populateTab(vo.getInheritanceFileId());
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "onBeneficiaryRepresentativeTab--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}
	@SuppressWarnings("unchecked")
	public void beneficiarySharesTabClick() {
		
		try {
			BeneficiarySharesTabController tab = (BeneficiarySharesTabController) getBean("pages$beneficiarySharesTabController");
			tab.loadTabDetails();
		}

		catch (Exception e) {
			logger.LogException("inheritedAssetTabClick() crashed", e);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}
	}

	@SuppressWarnings("unchecked")
	private void checkSelectedFileTypeId(Long selectedFileTypeId) {
		populateFileTypesDDFromViewMap();
		if (selectedFileTypeId.compareTo(absentDDV.getDomainDataId()) == 0) {
			viewMap.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
					WebConstants.InheritanceFileType.ABSENT);
			clearSponsor();
			clearBasicInfoTabData();
		} else if (selectedFileTypeId.compareTo(foundingDDV.getDomainDataId()) == 0) {
			viewMap.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
					WebConstants.InheritanceFileType.FOUNDING);
			clearBeneficiaryTabData();
		} else if (selectedFileTypeId.compareTo(underGuardianshipDDV
				.getDomainDataId()) == 0) {
			viewMap.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
					WebConstants.InheritanceFileType.UNDER_GUARDIANSHIP);
			clearSponsor();
			// clearBeneficiaryTabData();
		} else if (selectedFileTypeId.compareTo(legalAssitanceDDV
				.getDomainDataId()) == 0) {
			viewMap.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
					WebConstants.InheritanceFileType.LEGAL_ASSISTANCE);
			clearSponsor();
			clearBeneficiaryTabData();
		} else if (selectedFileTypeId.compareTo(deceasedDDV.getDomainDataId()) == 0) {
			viewMap.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
					WebConstants.InheritanceFileType.DECEASED);
			clearSponsor();
			clearBasicInfoTabData();
		}

	}

	private void clearBasicInfoTabData() {
		clearFilePersonNurse();
		clearFilePersonGuardian();
	}

	private void clearFilePersonGuardian() {
		if (viewMap.get(WebConstants.InheritanceFile.FILE_PERSON_GUARDIAN) != null) {
			viewMap.remove(WebConstants.InheritanceFile.FILE_PERSON_GUARDIAN);
		}
	}

	private void clearFilePersonNurse() {
		if (viewMap.get(WebConstants.InheritanceFile.FILE_PERSON_NURSE) != null) {
			viewMap.remove(WebConstants.InheritanceFile.FILE_PERSON_NURSE);
		}
	}

	private void clearBeneficiaryTabData() {
		clearBeneficiary();
		clearBeneficiaryNurse();
		clearBeneficiaryGuardian();
		clearBeneficiaryDetails();
	}

	private void clearBeneficiaryDetails() {
		if (viewMap.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST) != null) {
			viewMap
					.remove(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST);
		}
	}

	private void clearBeneficiaryGuardian() {
		if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_GUARDIAN_VIEW) != null) {
			viewMap
					.remove(WebConstants.InheritanceFile.INHERITANCE_GUARDIAN_VIEW);
		}
	}

	private void clearBeneficiaryNurse() {
		if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_NUSRSE_VIEW) != null) {
			viewMap
					.remove(WebConstants.InheritanceFile.INHERITANCE_NUSRSE_VIEW);
		}
	}

	private void clearBeneficiary() {
		if (viewMap
				.get(WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW) != null) {
			viewMap
					.remove(WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW);
		}
	}

	private void clearSponsor() {
		if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW) != null)
			viewMap
					.remove(WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW);
	}

	private void populateFileTypesDDFromViewMap() {
		if (viewMap.get(WebConstants.InheritanceFileType.ABSENT) != null) {
			absentDDV = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileType.ABSENT);
		}

		if (viewMap.get(WebConstants.InheritanceFileType.FOUNDING) != null) {
			foundingDDV = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileType.FOUNDING);
		}

		if (viewMap.get(WebConstants.InheritanceFileType.LEGAL_ASSISTANCE) != null) {
			legalAssitanceDDV = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileType.LEGAL_ASSISTANCE);
		}

		if (viewMap.get(WebConstants.InheritanceFileType.DECEASED) != null) {
			deceasedDDV = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileType.DECEASED);
		}

		if (viewMap.get(WebConstants.InheritanceFileType.UNDER_GUARDIANSHIP) != null) {
			underGuardianshipDDV = (DomainDataView) viewMap
					.get(WebConstants.InheritanceFileType.UNDER_GUARDIANSHIP);
		}
	}

	public boolean isPageModeView() {
		boolean view = false;
		if (viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE) != null) {
			String pageMode = viewMap.get(
					WebConstants.InheritanceFilePageMode.PAGE_MODE).toString();
			if (pageMode
					.equalsIgnoreCase(WebConstants.InheritanceFilePageMode.PAGE_MODE_READ_ONLY))
				view = true;

		}
		return view;
	}

	public boolean isPageModeInitLimDone() {
		boolean view = false;
		if (viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE) != null) {
			String pageMode = viewMap.get(
					WebConstants.InheritanceFilePageMode.PAGE_MODE).toString();
			if (pageMode
					.equalsIgnoreCase(WebConstants.InheritanceFilePageMode.PAGE_MODE_INIT_LIM_DONE))
				view = true;

		}
		return view;
	}

	public boolean isPageModeFinalLimDone() {
		boolean view = false;
		if (viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE) != null) {
			String pageMode = viewMap.get(
					WebConstants.InheritanceFilePageMode.PAGE_MODE).toString();
			if (pageMode
					.equalsIgnoreCase(WebConstants.InheritanceFilePageMode.PAGE_MODE_FINAL_LIM_DONE))
				view = true;

		}
		return view;
	}

	public boolean isPageModeInitLimRjctd() {
		boolean view = false;
		if (viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE) != null) {
			String pageMode = viewMap.get(
					WebConstants.InheritanceFilePageMode.PAGE_MODE).toString();
			if (pageMode
					.equalsIgnoreCase(WebConstants.InheritanceFilePageMode.PAGE_MODE_INIT_LIM_RJCTD))
				view = true;

		}
		return view;
	}

	public boolean isPageModeNew() {
		boolean view = false;
		if (viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE) != null) {
			String pageMode = viewMap.get(
					WebConstants.InheritanceFilePageMode.PAGE_MODE).toString();
			if (pageMode
					.equalsIgnoreCase(WebConstants.InheritanceFilePageMode.PAGE_MODE_NEW))
				view = true;

		}
		return view;
	}

	public boolean isPageModeAppReq() {
		boolean view = false;
		if (viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE) != null) {
			String pageMode = viewMap.get(
					WebConstants.InheritanceFilePageMode.PAGE_MODE).toString();
			if (pageMode
					.equalsIgnoreCase(WebConstants.InheritanceFilePageMode.PAGE_MODE_APPROVAL_REQ))
				view = true;

		}
		return view;
	}

	public boolean isPageModeApproved() {
		boolean view = false;
		if (viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE) != null) {
			String pageMode = viewMap.get(
					WebConstants.InheritanceFilePageMode.PAGE_MODE).toString();
			if (pageMode
					.equalsIgnoreCase(WebConstants.InheritanceFilePageMode.PAGE_MODE_APPROVED))
				view = true;

		}
		return view;
	}

	public boolean isPageModeDistReq() {
		boolean view = false;
		if (viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE) != null) {
			String pageMode = viewMap.get(
					WebConstants.InheritanceFilePageMode.PAGE_MODE).toString();
			if (pageMode
					.equalsIgnoreCase(WebConstants.InheritanceFilePageMode.PAGE_MODE_DIST_REQ))
				view = true;

		}
		return view;
	}

	public boolean isPageModeRejectedResubmit() {
		boolean view = false;
		if (viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE) != null) {
			String pageMode = viewMap.get(
					WebConstants.InheritanceFilePageMode.PAGE_MODE).toString();
			if (pageMode
					.equalsIgnoreCase(WebConstants.InheritanceFilePageMode.PAGE_MODE_RJCTD_RESBMT))
				view = true;

		}
		return view;
	}

	public boolean isShowGenerateCostCenterButtonButton() {
		boolean show = false;
		if (viewMap.get("isShowGenerateCostCenterButtonButton") == null) {
			if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) != null) {
				InheritanceFileView inheritanceFileView = (InheritanceFileView) viewMap
						.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);
				show = (!(isPageModeView() || isViewModePopUp())
						&& (isPageModeFinalLimDone() || isPageModeDistReq()) && (inheritanceFileView
						.getCostCenter() == null || inheritanceFileView
						.getCostCenter().trim().length() <= 0));
				if (!show) {
					for (InheritanceBeneficiaryView ib : inheritanceFileView
							.getInheritanceBeneficiaryList()) {
						if (ib.getIsDeleted().compareTo(
								WebConstants.DEFAULT_IS_DELETED) == 0
								&& (ib.getCostCenter() == null || ib
										.getCostCenter().trim().length() <= 0)) {
							show = true;
							break;
						}

					}
				}
			} else {
				show = false;
			}
			viewMap.put("isShowGenerateCostCenterButtonButton", show);
		} else {
			show = (Boolean) viewMap
					.get("isShowGenerateCostCenterButtonButton");
		}
		return show;
	}

	public boolean isShowSaveButton() {
		boolean show = false;
		show = (!(isPageModeView() || isViewModePopUp()) && (isPageModeNew()
				|| isPageModeAppReq() || isPageModeApproved()
				|| isPageModeInitLimDone() || isPageModeInitLimRjctd() || isPageModeRejectedResubmit()));
		return show;
	}

	public boolean isShowApproveButton() {
		boolean show = false;
		show = isTaskApproveOpeningNewFile();
		return show;
	}

	public boolean isShowSubmitButton() {
		boolean show = false;
		show = ((isPageModeNew() || isPageModeRejectedResubmit()) && !(isPageModeView() || isViewModePopUp()));
		return show;
	}

	public boolean isShowInitLimDoneButton() {
		boolean show = false;
		show = ((isPageModeApproved() || isPageModeInitLimRjctd()) && isTaskForLimitation());
		return show;
	}

	public boolean isShowCorrespondenceButton() {
		boolean show = false;
		show = ((isPageModeApproved() || isPageModeInitLimRjctd()
				|| isPageModeAppReq() || isPageModeDistReq()
				|| isPageModeFinalLimDone() || isPageModeInitLimDone()));
		return show;
	}

	public boolean isShowFinalLimDoneButton() {
		boolean show = false;
		show = (isPageModeInitLimDone() && isTaskForApprovingTheLimitatione());
		return show;
	}

	public boolean isShowRejectButton() {
		boolean show = false;
		show = isTaskApproveOpeningNewFile();
		return show;
	}

	public boolean isShowRejectInitLimButton() {
		boolean show = false;
		show = isTaskForApprovingTheLimitatione();
		return show;
	}

	public boolean isShowDistDoneBtn() {
		boolean show = false;

		if (isPageModeDistReq() && !isViewModePopUp()) {
			show = true;// (Boolean)viewMap.get(SHOW_DIST_TAB );
		}
		return show;
	}

	public boolean isShowDistDone() {
		boolean show = false;
		// show = isPageModeDistReq();
		if (viewMap.get(SHOW_DIST_TAB) != null && !isViewModePopUp()) {
			show = (Boolean) viewMap.get(SHOW_DIST_TAB);
		}
		return show;
	}

	@SuppressWarnings("unchecked")
	public void doneInitLimitation() {
		RequestView requestView = null;

		try {

			if ((isAssetAvailable() && isAllAssetsConfirmed())// &&
																// isAssetSharesAvailable())
					|| (!isAssetAvailable() && isAllCorrespondencesClosed())) {
				if (viewMap.get(WebConstants.REQUEST_VIEW) != null) // Saved
																	// Request
				{
					requestView = (RequestView) viewMap
							.get(WebConstants.REQUEST_VIEW);
					if (requestView != null
							&& requestView.getRequestId() != null) {
						new InheritanceFileService()
								.setRequestAndFileAsInitLimDone(requestView
										.getRequestId(), requestView
										.getInheritanceFileView()
										.getInheritanceFileId(), CommonUtil
										.getLoggedInUser());
						DomainDataView fileStatusDD = getFileStInitLimDoneDD();
						if (completeTask(TaskOutcome.OK)) {
							saveAttachmentComment();
							saveSystemComments(MessageConstants.RequestEvents.REQUEST_INITIAL_LIMITATION_DONE);

							viewMap
									.put(
											WebConstants.InheritanceFile.FILE_STATUS_DESC,
											isEnglishLocale() ? fileStatusDD
													.getDataDescEn()
													: fileStatusDD
															.getDataDescAr());
							viewMap
									.put(
											WebConstants.ApplicationDetails.APPLICATION_STATUS,
											CommonUtil.getIsEnglishLocale() ? getRqstStInitLimDoneDD()
													.getDataDescEn()
													: getRqstStNewDD()
															.getDataDescAr());
							getTxtFileStatus();
							requestView = new InheritanceFileService()
									.getInheritanceFileRequestByFileId(
											requestView.getInheritanceFileId(),
											null);
							getFileDetails(requestView.getInheritanceFileId(),
									requestView);
							successMessages
									.add(CommonUtil
											.getBundleMessage("request.event.initialLimDone"));
							btnInitLimDone.setRendered(false);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.LogException("doneInitLimitation() crashed", e);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}

	}

	@SuppressWarnings("unchecked")
	private boolean isAssetSharesAvailable() {
		boolean isAvailable = false;
		beneficiarySharesTabClick();
		if (viewMap.get(ASSET_SHARE_INFO) != null
				&& ((List<InheritedAssetShareInfoView>) viewMap
						.get(ASSET_SHARE_INFO)).size() > 0)
			isAvailable = true;
		else {
			errorMessages.add(CommonUtil
					.getBundleMessage("errMsg.assBeneShare"));
			fileTabPanel.setSelectedTab(TAB_ID_BENEFICIARY_SHARE);
		}
		return isAvailable;
	}

	@SuppressWarnings("unchecked")
	private boolean isAllCorrespondencesClosed() {
		boolean isAllCosed = true;
		List<OfficialCorrespondenceView> offCorresList = null;
		officialCorrespondenceTabClick();// to populate the list
		if (viewMap.get(CORRESPONDENCE_LIST) != null) {
			offCorresList = (List<OfficialCorrespondenceView>) viewMap
					.get(CORRESPONDENCE_LIST);
			if (offCorresList != null && offCorresList.size() > 0) {
				for (OfficialCorrespondenceView offCorrView : offCorresList) {
					if (offCorrView.isOpened()) {
						isAllCosed = false;
						errorMessages.add(CommonUtil
								.getBundleMessage("errMsg.pleaseCloseCorres"));
						fileTabPanel.setSelectedTab(TAB_ID_OFF_CORRESP);
						break;
					}
				}
			}
		}
		return isAllCosed;
	}

	@SuppressWarnings("unchecked")
	private boolean isAllAssetsConfirmed() {
		boolean isAllConfirmed = true;
		List<InheritedAssetView> iAssetList = null;
		if (viewMap.get(INHERITED_ASSET_MEMS_VIEW_LIST) != null) {
			iAssetList = (List<InheritedAssetView>) viewMap
					.get(INHERITED_ASSET_MEMS_VIEW_LIST);
			if (iAssetList != null && iAssetList.size() > 0) {
				for (InheritedAssetView iAsset : iAssetList) {
					if (!iAsset.getIsConfirmBool()) {
						isAllConfirmed = false;
						errorMessages
								.add(CommonUtil
										.getBundleMessage("errMsg.pleaseConfirmAssets"));
						fileTabPanel.setSelectedTab(TAB_ID_INHERITED_ASSET);
						break;
					}
				}
			}
		}
		return isAllConfirmed;
	}

	@SuppressWarnings("unchecked")
	private boolean isAssetAvailable() {
		boolean isAvailable = false;
		if (viewMap.get(INHERITED_ASSET_MEMS_VIEW_LIST) != null
				&& ((List<InheritedAssetView>) viewMap
						.get(INHERITED_ASSET_MEMS_VIEW_LIST)).size() > 0)
			isAvailable = true;
		// else
		// {
		// isAvailable = false;
		// errorMessages.add(CommonUtil.getBundleMessage("errMsg.pleaseAddAssets"));
		// fileTabPanel.setSelectedTab(TAB_ID_INHERITED_ASSET);
		// }
		return isAvailable;
	}

	@SuppressWarnings("unchecked")
	public void onOpenChangeResearcher() {
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try {
			sessionMap
					.put(
							WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW,
							viewMap
									.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW));
			executeJavascript("openChangeResearcherPopUp()");
		} catch (Exception exception) {

			logger.LogException("onOpenChangeResearcher --- CRASHED --- ",
					exception);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}

	}

	@SuppressWarnings("unchecked")
	public void onChangeResearcher() {
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try {
			InheritanceFileView file = null;
			String researcherId = sessionMap.remove(
					WebConstants.InheritanceFile.RESEARCHER_ID).toString();
			cboResearcher.setValue(researcherId);
			if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) != null) {
				file = (InheritanceFileView) viewMap
						.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);
				file.setResearcher(researcherId);
				// viewMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW,file);
				successMessages
						.add(CommonUtil
								.getBundleMessage("mems.inheritanceFile.msg.ResearcherChanged"));
			}

		} catch (Exception exception) {

			logger.LogException("onChangeResearcher --- CRASHED --- ",
					exception);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}

	}

	@SuppressWarnings("unchecked")
	public void onGenerateCostCenterForFileAfterFinalization() {
		try {

			RequestView request = (RequestView) viewMap
					.get(WebConstants.REQUEST_VIEW);

			new InheritanceFileService().generateCostCenterForFile(request
					.getInheritanceFileView().getInheritanceFileId());
			RequestView requestView = new InheritanceFileService()
					.getInheritanceFileRequestByFileId(request
							.getInheritanceFileView().getInheritanceFileId(),
							request.getRequestId());
			getFileDetails(request.getInheritanceFileView()
					.getInheritanceFileId(), requestView);
			viewMap.put("isShowGenerateCostCenterButtonButton", false);
			successMessages
					.add(CommonUtil
							.getBundleMessage("inheritanceFile.msg.CostCenterGeneratedAfterFinalization"));
		} catch (Exception exception) {

			logger
					.LogException(
							"onGenerateCostCenterForFileAfterFinalization --- CRASHED --- ",
							exception);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}

	}

	@SuppressWarnings("unchecked")
	public void onDistributionDone() {
		String methodName = "onDistributionDone|";
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try {
			logger.logInfo(methodName + " --- STARTED --- ");
			if (!validateDistribution()) {
				RequestView requestView = null;
				InheritanceFileService fileService = new InheritanceFileService();
				if (viewMap.get(WebConstants.REQUEST_VIEW) != null) // Saved
																	// Request
				{
					requestView = (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
					if (requestView != null&& requestView.getRequestId() != null) 
					{
						DomainDataView fileStatusDD = new DomainDataView();
						fileService.setRequestAndFileAsFinalLimDone(requestView
																		.getRequestId(), requestView
																		.getInheritanceFileView()
																		.getInheritanceFileId(), CommonUtil
																		.getLoggedInUser(), false, false);
						fileStatusDD = getFileStFinalLimDoneDD();
						completeTask(TaskOutcome.OK);
						saveAttachmentComment();
						saveSystemComments(MessageConstants.RequestEvents.REQUEST_DISTRIBUTION_DONE);
						successMessages
								.add(CommonUtil
										.getBundleMessage("successMsg.distributionDone"));
						viewMap.put(
								WebConstants.InheritanceFile.FILE_STATUS_DESC,
								isEnglishLocale() ? fileStatusDD
										.getDataDescEn() : fileStatusDD
										.getDataDescAr());
						viewMap
								.put(
										WebConstants.ApplicationDetails.APPLICATION_STATUS,
										CommonUtil.getIsEnglishLocale() ? getRqstStFinalLimDoneDD()
												.getDataDescEn()
												: getRqstStNewDD()
														.getDataDescAr());
						btnDistDone.setRendered(false);
						getTxtFileStatus();
						requestView = new InheritanceFileService()
								.getInheritanceFileRequestByFileId(requestView
										.getInheritanceFileId(), null);
						getFileDetails(requestView.getInheritanceFileId(),
								requestView);
					}
				}
			} else
				errorMessages.add(CommonUtil
						.getBundleMessage("errMessage.plzDisributeEachAmount"));

			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		} catch (Exception exception) {

			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}

	}

	@SuppressWarnings("unchecked")
	public void onOpenChangeAssignedExpert() {
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try {
			sessionMap
					.put(
							WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW,
							viewMap
									.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW));
			executeJavascript("openChangeAssignExpertPopUp()");
		} catch (Exception exception) {

			logger.LogException("onOpenChangeAssignedExpert --- CRASHED --- ",
					exception);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}

	}

	@SuppressWarnings("unchecked")
	public void onChangeAssignedExpert() {
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try {
			InheritanceFileView file = null;
			String researcherId = sessionMap.remove(
					WebConstants.InheritanceFile.ASSIGNED_EXPERT_ID).toString();
			cboAssignedExpert.setValue(researcherId);
			if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) != null) {
				file = (InheritanceFileView) viewMap
						.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);
				file.setResearcher(researcherId);
				// viewMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW,file);
				successMessages
						.add(CommonUtil
								.getBundleMessage("mems.inheritanceFile.msg.AssignedExpert"));
			}

		} catch (Exception exception) {

			logger.LogException("onChangeAssignedExpert --- CRASHED --- ",
					exception);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}

	}

	@SuppressWarnings("unchecked")
	private boolean validateDistribution() {
		boolean hasError = false;
		if (viewMap.get("distributionList") != null) {
			List<OpenFileDistribute> distributionList = null;
			distributionList = (List<OpenFileDistribute>) viewMap
					.get("distributionList");
			if (distributionList != null && distributionList.size() > 0) {
				for (OpenFileDistribute distributon : distributionList) {
					if (distributon.getShowDistributeIcon() == null
							|| distributon.getShowDistributeIcon().compareTo(
									"1") == 0) {
						hasError = true;
						return hasError;
					}
				}
			}
		}
		return hasError;
	}

	@SuppressWarnings("unchecked")
	public void doneFinalLimitation() {
		try {
			logger.logInfo("doneFinalLimitation()|started...");
			RequestView requestView = null;
			InheritanceFileService fileService = new InheritanceFileService();
			if (viewMap.get(WebConstants.REQUEST_VIEW) != null) // Saved Request
			{
				requestView = (RequestView) viewMap
						.get(WebConstants.REQUEST_VIEW);
				if (isFinalLimitationValidated()) {
					if (requestView != null
							&& requestView.getRequestId() != null) {
						DomainDataView fileStatusDD = new DomainDataView();

						boolean createDistributeTask = WebConstants.InheritanceFileType.FILES_TYPE_SAME_OWNER_BENEFICIARY
								.indexOf(requestView.getInheritanceFileView()
										.getFileTypeKey()) != 0
								&& fileService
										.hasUndistributedPaymentsAgainstFileOwners(requestView
												.getInheritanceFileView()
												.getInheritanceFileId());
						if (!createDistributeTask) {
							logger
									.logInfo("doneFinalLimitation()|No undistributed Trx...");
							fileService.setRequestAndFileAsFinalLimDone(
									requestView.getRequestId(), requestView
											.getInheritanceFileView()
											.getInheritanceFileId(), CommonUtil
											.getLoggedInUser(), false, true);
							fileStatusDD = getFileStFinalLimDoneDD();
							completeTask(TaskOutcome.APPROVE);
						} else {
							fileService.setRequestAndFileAsFinalLimDone(
									requestView.getRequestId(), requestView
											.getInheritanceFileView()
											.getInheritanceFileId(), CommonUtil
											.getLoggedInUser(), true, true);
							fileStatusDD = getFileStDistributionReqDD();
							completeTask(TaskOutcome.FORWARD);

						}
						saveAttachmentComment();
						saveSystemComments(MessageConstants.RequestEvents.REQUEST_FINAL_LIMITATION_DONE);
						successMessages
								.add(CommonUtil
										.getBundleMessage("request.event.finalLimDone"));
						viewMap.put(
								WebConstants.InheritanceFile.FILE_STATUS_DESC,
								isEnglishLocale() ? fileStatusDD
										.getDataDescEn() : fileStatusDD
										.getDataDescAr());
						btnFinalLimDone.setRendered(false);
						viewMap
								.put(
										WebConstants.ApplicationDetails.APPLICATION_STATUS,
										CommonUtil.getIsEnglishLocale() ? getRqstStFinalLimDoneDD()
												.getDataDescEn()
												: getRqstStNewDD()
														.getDataDescAr());
						btnFinalLimDone.setRendered(false);
						btnInitLimRejected.setRendered(false);
						txtAreaRejectionReason.setRendered(false);
						lblRejectionReason.setRendered(false);
						btnSaveRequest.setRendered(false);

						getTxtFileStatus();
						requestView = new InheritanceFileService()
								.getInheritanceFileRequestByFileId(requestView
										.getInheritanceFileId(), null);
						getFileDetails(requestView.getInheritanceFileId(),
								requestView);
					}
				}

			}
			logger.logInfo("doneFinalLimitation()|completed successfully...");
		} catch (Exception e) {
			logger.LogException("doneFinalLimitation()| crashed", e);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}
	}

	private boolean isFinalLimitationValidated() throws Exception {
		if (!AttachmentBean.mandatoryDocsValidated()) {
			errorMessages.add(CommonUtil
					.getBundleMessage("attachment.messages.mandatoryDocs"));
			fileTabPanel.setSelectedTab(TAB_ID_ATTACHMENT);
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public void rejectInitLimitation() {
		RequestView requestView = null;
		try {
			String methodName = "rejectInitLimitation()";
			logger.logInfo(methodName + " started...");
			if (txtAreaRejectionReason.getValue() != null
					&& txtAreaRejectionReason.getValue().toString().length() > 0) {
				if (viewMap.get(WebConstants.REQUEST_VIEW) != null) // Saved
																	// Request
				{
					requestView = (RequestView) viewMap
							.get(WebConstants.REQUEST_VIEW);
					if (requestView != null
							&& requestView.getRequestId() != null) {

						new InheritanceFileService()
								.setRequestAndFileAsLimRejected(requestView
										.getRequestId(), requestView
										.getInheritanceFileView()
										.getInheritanceFileId(), CommonUtil
										.getLoggedInUser(),
										txtAreaRejectionReason.getValue()
												.toString());

						DomainDataView fileStatusDD = getFileStLimRejectedDD();
						//					
						if (completeTask(TaskOutcome.REJECT)) {
							saveAttachmentComment();
							saveSystemComments(MessageConstants.RequestEvents.REQUEST_INITIAL_LIMITATION_REJECTED);
							successMessages
									.add(CommonUtil
											.getBundleMessage("request.event.initLimRejected"));

							// viewMap.put(WebConstants.InheritanceFile.FILE_STATUS_ID,
							// fileStatusDD.getDomainDataId());
							viewMap
									.put(
											WebConstants.InheritanceFile.FILE_STATUS_DESC,
											isEnglishLocale() ? fileStatusDD
													.getDataDescEn()
													: fileStatusDD
															.getDataDescAr());
							btnInitLimRejected.setRendered(false);
							btnFinalLimDone.setRendered(false);
							txtAreaRejectionReason.setDisabled(true);
							viewMap
									.put(
											WebConstants.ApplicationDetails.APPLICATION_STATUS,
											CommonUtil.getIsEnglishLocale() ? getRqstStLimRejectedDD()
													.getDataDescEn()
													: getRqstStNewDD()
															.getDataDescAr());
							getTxtFileStatus();
							requestView = new InheritanceFileService()
									.getInheritanceFileRequestByFileId(
											requestView.getInheritanceFileId(),
											null);
							getFileDetails(requestView.getInheritanceFileId(),
									requestView);
						}
						// viewMap.put(WebConstants.InheritanceFilePageMode.PAGE_MODE,
						// WebConstants.InheritanceFilePageMode.PAGE_MODE_INIT_LIM_RJCTD);
					}
				}
			} else
				errorMessages
						.add(CommonUtil
								.getBundleMessage("mems.inheritanceFile.rejectionReasonReq"));
			logger.logInfo(methodName + " completed successfully...");
		} catch (Exception e) {
			logger.LogException("rejectRequest() crashed", e);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}

	}

	public boolean isDeceased() {
		isDeceased = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) {
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null
					&& StringHelper.isNotEmpty(fileType)
					&& fileType
							.compareTo(WebConstants.InheritanceFileType.DECEASED) == 0)
				isDeceased = true;
		}
		return isDeceased;
	}

	public void setDeceased(boolean isDeceased) {
		this.isDeceased = isDeceased;
	}

	public boolean isFounding() {
		isFounding = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) {
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null
					&& StringHelper.isNotEmpty(fileType)
					&& fileType
							.compareTo(WebConstants.InheritanceFileType.FOUNDING) == 0)
				isFounding = true;
		}
		return isFounding;
	}

	public void setFounding(boolean isFounding) {
		this.isFounding = isFounding;
	}

	public boolean isLegalAsstnc() {
		isLegalAsstnc = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) {
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null
					&& StringHelper.isNotEmpty(fileType)
					&& fileType
							.compareTo(WebConstants.InheritanceFileType.LEGAL_ASSISTANCE) == 0)
				isLegalAsstnc = true;
		}
		return isLegalAsstnc;
	}

	public void setLegalAsstnc(boolean isLegalAsstnc) {
		this.isLegalAsstnc = isLegalAsstnc;
	}

	public boolean isAbsent() {
		isAbsent = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) {
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null
					&& StringHelper.isNotEmpty(fileType)
					&& fileType
							.compareTo(WebConstants.InheritanceFileType.ABSENT) == 0)
				isAbsent = true;
		}
		return isAbsent;
	}

	public void setAbsent(boolean isAbsent) {
		this.isAbsent = isAbsent;
	}

	public boolean isUnderGuradian() {
		isUnderGuradian = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) {
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null
					&& StringHelper.isNotEmpty(fileType)
					&& fileType
							.compareTo(WebConstants.InheritanceFileType.UNDER_GUARDIANSHIP) == 0)
				isUnderGuradian = true;
		}
		return isUnderGuradian;
	}

	public void setUnderGuradian(boolean isUnderGuradian) {
		this.isUnderGuradian = isUnderGuradian;
	}

	public HtmlTabPanel getFileTabPanel() {
		return fileTabPanel;
	}

	public void setFileTabPanel(HtmlTabPanel fileTabPanel) {
		this.fileTabPanel = fileTabPanel;
	}

	public HtmlCommandButton getBtnSaveRequest() {
		return btnSaveRequest;
	}

	public void setBtnSaveRequest(HtmlCommandButton btnSaveRequest) {
		this.btnSaveRequest = btnSaveRequest;
	}

	public HtmlCommandButton getBtnSubmitRequest() {
		return btnSubmitRequest;
	}

	public void setBtnSubmitRequest(HtmlCommandButton btnSubmitRequest) {
		this.btnSubmitRequest = btnSubmitRequest;
	}

	public HtmlCommandButton getBtnInitLimDone() {
		return btnInitLimDone;
	}

	public void setBtnInitLimDone(HtmlCommandButton btnInitLimDone) {
		this.btnInitLimDone = btnInitLimDone;
	}

	public HtmlCommandButton getBtnFinalLimDone() {
		return btnFinalLimDone;
	}

	public void setBtnFinalLimDone(HtmlCommandButton btnFinalLimDone) {
		this.btnFinalLimDone = btnFinalLimDone;
	}

	public HtmlCommandButton getBtnRejectResubmit() {
		return btnRejectResubmit;
	}

	public void setBtnRejectResubmit(HtmlCommandButton btnRejectResubmit) {
		this.btnRejectResubmit = btnRejectResubmit;
	}

	public HtmlCommandButton getBtnInitLimRejected() {
		return btnInitLimRejected;
	}

	public void setBtnInitLimRejected(HtmlCommandButton btnInitLimRejected) {
		this.btnInitLimRejected = btnInitLimRejected;
	}

	public HtmlCommandButton getBtnApproveRequest() {
		return btnApproveRequest;
	}

	public void setBtnApproveRequest(HtmlCommandButton btnApproveRequest) {
		this.btnApproveRequest = btnApproveRequest;
	}

	// will be used in included JSP
	public boolean isShowFilePersonSearch() {
		boolean show = false;
		show = (((isPageModeNew() || isPageModeAppReq() || isPageModeRejectedResubmit())) && !isViewModePopUp());
		return show;
	}

	// will be used in included JSP
	public boolean isShowSponsorSearch() {
		boolean show = false;
		show = ((isFounding() && (isPageModeNew() || isPageModeAppReq() || isPageModeRejectedResubmit())) && !isViewModePopUp());
		return show;
	}

	// will be used in included JSP
	public boolean isShowBeneficiarySearch() {
		boolean show = false;
		// show = (!(isLegalAsstnc() || isUnderGuradian() || isFounding() ||
		// isPageModeView() || isViewModePopUp()));
		show = (!(isLegalAsstnc() || isPageModeView() || isViewModePopUp()));
		return show;
	}

	public boolean isShowFileRequestOnFollowup() {
		boolean show = false;
		show = (!isPageModeView() && !isViewModePopUp() && !isPageModeNew() && !isPageModeAppReq());
		return show;
	}

	// will be used in included JSP
	public boolean isShowGuardianNurseSearch() {
		boolean show = false;
		show = (!(isPageModeView() || isViewModePopUp()));
		return show;
	}

	// will be used in included JSP
	public boolean isShowAddBeneficiaryButton() {
		boolean show = false;
		boolean isBeingEdited = viewMap.containsKey(INDEX_TO_EDIT);
		show = (( // Case 1: If legal Asst or under guardian then check if
					// isBeingEdited or there in no record in list
				(isLegalAsstnc() || isUnderGuradian()) && (isBeingEdited
						|| getBeneficiaryDetailsList() == null || getBeneficiaryDetailsList()
						.size() <= 0))
				|| (// Case 2:
				!(isLegalAsstnc() || isUnderGuradian()) && (isBeingEdited || !isPageModeView())) || !isViewModePopUp());
		return show;
	}

	// will be used in included JSP
	public boolean isShowAddAssetButton() {
		boolean show = false;
		show = ((isPageModeAppReq() || isPageModeApproved() || isPageModeInitLimRjctd()) && !(isViewModePopUp() || isPageModeView()));
		return show;
	}

	// will be used in included JSP
	public boolean isShowAddAssetButtonAfterFinalize() {
		boolean show = false;
		show = (isPageModeFinalLimDone() || isPageModeDistReq())
				&& !(isViewModePopUp() || isPageModeView());

		return show;
	}

	// will be used in included JSP
	public boolean isShowInheritedAssetTab() {
		boolean show = false;
		show = (isPageModeAppReq() || getIsMigratedFile()
				|| isPageModeApproved() || isPageModeInitLimDone()
				|| isPageModeFinalLimDone() || isPageModeInitLimRjctd()
				|| isPageModeView() || isPageModeDistReq());
		return show;
	}

	// will be used in included JSP

	public boolean isShowInheritedAssetTabGridConfirmActionItemsAfterFinalize() {
		boolean show = false;
		show = (isPageModeFinalLimDone() || isPageModeDistReq())
				&& !(isViewModePopUp() || isPageModeView());
		return show;
	}

	public boolean isShowInheritedAssetTabGridConfirmActionItems() {
		boolean show = false;
		show = (isPageModeApproved() || isPageModeInitLimDone() || isPageModeInitLimRjctd())
				&& !(isViewModePopUp() || isPageModeView());
		return show;
	}

	// will be used in included JSP
	public boolean isShowInheritedAssetTabGridEditItems() {
		boolean show = false;
		show = (isPageModeAppReq() || isPageModeApproved()
				|| isPageModeInitLimDone() || isPageModeInitLimRjctd() || isPageModeDistReq()

		)
				&& !(isViewModePopUp() || isPageModeView());
		return show;
	}

	// will be used in included JSP
	public boolean isShowInheritedAssetTabGridEditItemsAfterFinalize() {
		boolean show = false;
		show = (isPageModeFinalLimDone() || isPageModeDistReq())
				&& !(isViewModePopUp() || isPageModeView());
		return show;
	}

	// will be used in included JSP
	public boolean isShowAssetShareTab() {
		boolean show = false;
		show = (getIsMigratedFile() || isPageModeAppReq()
				|| isPageModeApproved() || isPageModeInitLimDone()
				|| isPageModeFinalLimDone() || isPageModeInitLimRjctd()
				|| isPageModeView() || isPageModeDistReq());
		return show;
	}

	// will be used in included JSP
	public boolean isShowSaveShareButton() {
		boolean show = false;
		show = ((isPageModeApproved() || isPageModeAppReq()
				|| isPageModeInitLimDone() || isPageModeInitLimRjctd()) && !(isViewModePopUp() || isPageModeView()));
		return show;
	}

	// will be used in included JSP
	public boolean isShowSaveShareButtonAfterFinalize() {
		boolean show = false;
		show = ((isPageModeFinalLimDone() || isPageModeDistReq()) && !(isViewModePopUp() || isPageModeView()));
		return show;
	}

	public void requestHistoryTabClick() {
		final String methodName = "requestHistoryTabClick|";
		if (viewMap.get(WebConstants.InheritanceFile.FILE_ID) != null)
			try {
				RequestHistoryController rhc = new RequestHistoryController();
				Long fileId = new Long(viewMap.get(
						WebConstants.InheritanceFile.FILE_ID).toString());
				if (fileId != null)
					rhc.getAllRequestTasksForRequest(noteOwner, fileId
							.toString());
			} catch (Exception ex) {
				logger.LogException(methodName + "Error Occured..", ex);
				errorMessages.add(CommonUtil
						.getBundleMessage("commons.ErrorMessage"));
			}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getResearcherList() {
		if (viewMap.get(RESEARCHER_LIST) != null) {
			return (List<SelectItem>) viewMap.get(RESEARCHER_LIST);
		}
		return researcherList;
	}

	public void sendToResearcher() {
		if (cboResearcher.getValue() == null
				|| cboResearcher.getValue().toString().compareTo("-1") == 0) {
			errorMessages.add(CommonUtil
					.getBundleMessage("mems.inheritanceFile.selectResearcher"));
			return;
		} else if (viewMap.get(WebConstants.InheritanceFile.FILE_ID) != null) {
			String researcher = cboResearcher.getValue().toString();
			Long fileId = new Long(viewMap.get(
					WebConstants.InheritanceFile.FILE_ID).toString());

			try {
				btnResearcher.setRendered(false);
				cboResearcher.setDisabled(true);
				new InheritanceFileService().assignResearcherToFile(fileId,
						researcher, CommonUtil.getLoggedInUser());
				if (completeTask(TaskOutcome.OK)) {
					saveSystemComments(MessageConstants.InheritanceFileMessage.SEND_TO_RESEARCHER);
					saveAttachmentComment();

					if (viewMap.get(WebConstants.REQUEST_VIEW) != null) // Saved
																		// Request
					{
						RequestView requestView = (RequestView) viewMap
								.get(WebConstants.REQUEST_VIEW);
						requestView = new InheritanceFileService()
								.getInheritanceFileRequestByFileId(requestView
										.getInheritanceFileId(), null);
						getFileDetails(requestView.getInheritanceFileId(),
								requestView);
					}
					successMessages
							.add(CommonUtil
									.getBundleMessage("mems.inheritanceFile.researcherAssigned"));
				}
			} catch (Exception e) {
				logger.LogException("sendToResearcher() crasehd |", e);
				errorMessages
						.add(CommonUtil
								.getBundleMessage("mems.inheritanceFile.researcherNotAssigned"));
				btnResearcher.setRendered(true);
			}

		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getAssignedExpertList() {
		if (viewMap.get(ASSIGNED_EXPERT_LIST) != null) {
			return (List<SelectItem>) viewMap.get(ASSIGNED_EXPERT_LIST);
		}
		return assignedExpertList;
	}

	public boolean isShowSendToResearcherButton() {
		boolean show = false;
		show = (isTaskForAssigningResearcher());
		return show;
	}

	public HtmlSelectOneMenu getCboResearcher() {
		return cboResearcher;
	}

	public void setCboResearcher(HtmlSelectOneMenu cboResearcher) {
		this.cboResearcher = cboResearcher;
	}

	public HtmlCommandButton getBtnResearcher() {
		return btnResearcher;
	}

	public void setBtnResearcher(HtmlCommandButton btnResearcher) {
		this.btnResearcher = btnResearcher;
	}

	public String getRejecttionReason() {
		return rejecttionReason;
	}

	public void setRejecttionReason(String rejecttionReason) {
		this.rejecttionReason = rejecttionReason;
	}

	public HtmlInputTextarea getTxtAreaRejectionReason() {
		return txtAreaRejectionReason;
	}

	public void setTxtAreaRejectionReason(
			HtmlInputTextarea txtAreaRejectionReason) {
		this.txtAreaRejectionReason = txtAreaRejectionReason;
	}

	public boolean isFileTypeDisabled() {
		boolean disabled = true;
		disabled = (isPageModeView() || !(isPageModeNew() || isPageModeAppReq()
				|| isPageModeRejectedResubmit() || isPageModeApproved()
				|| isPageModeFinalLimDone() || isPageModeInitLimDone()
				|| isPageModeInitLimRjctd() || isPageModeDistReq()));
		return disabled;
	}

	public boolean isResearcherDisabled() {
		boolean disabled = false;
		disabled = (isPageModeView());
		return disabled;
	}

	public boolean isShowRejectionReason() {
		boolean show = false;
		show = (isTaskForApprovingTheLimitatione() || isPageModeInitLimRjctd());
		return show;
	}

	public boolean isShowOfficialCorrespondence() {
		boolean show = true;
		show = (isPageModeApproved() || isPageModeInitLimDone()
				|| isPageModeFinalLimDone() || isPageModeDistReq()
				|| isPageModeInitLimRjctd() || isPageModeView());
		return show;
	}

	@SuppressWarnings("unchecked")
	public void officialCorrespondenceTabClick() {
		if (viewMap.get(WebConstants.InheritanceFile.FILE_ID) != null) {
			long fileId = new Long(viewMap.get(
					WebConstants.InheritanceFile.FILE_ID).toString());
			List<OfficialCorrespondenceView> officialCorresList = new ArrayList<OfficialCorrespondenceView>();

			try {
				officialCorresList = new InheritanceFileService()
						.getAllOffCorrByFileId(fileId);
				if (officialCorresList != null && officialCorresList.size() > 0) {
					for (OfficialCorrespondenceView offCorr : officialCorresList) {
						UserView user = new UtilityService()
								.getUserByLoginId(offCorr.getCreatedBy());
						if (isEnglishLocale())
							offCorr.setCreatedByName(user.getFullNamePrimary());
						else
							offCorr.setCreatedByName(user
									.getFullNameSecondary());

						if (offCorr.getStatus().compareTo(
								getOffCorresClose().getDomainDataId()) == 0) {
							offCorr.setOpened(false);
							offCorr.setStatusEn(getOffCorresClose()
									.getDataDescEn());
							offCorr.setStatusAr(getOffCorresClose()
									.getDataDescAr());
						} else {
							offCorr.setOpened(true);
							offCorr.setStatusEn(getOffCorresOpen()
									.getDataDescEn());
							offCorr.setStatusAr(getOffCorresOpen()
									.getDataDescAr());
						}
					}
					viewMap.put(CORRESPONDENCE_LIST, officialCorresList);
				}
			} catch (Exception e) {
				logger.LogException("officialCorrespondenceTabClick crashed| ",
						e);
				errorMessages.add(CommonUtil
						.getBundleMessage("commons.ErrorMessage"));
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void canAddAttachmentsAndComments(boolean canAdd) {
		viewMap.put("canAddAttachment", canAdd);
		viewMap.put("canAddNote", canAdd);
	}

	public List<DomainDataView> getOffCorrStatusDDList() {
		return offCorrStatusDDList;
	}

	public void setOffCorrStatusDDList(List<DomainDataView> offCorrStatusDDList) {
		this.offCorrStatusDDList = offCorrStatusDDList;
	}

	public DomainDataView getOffCorresOpen() {
		if (viewMap
				.get(WebConstants.OFFICIAL_CORRESPONDENCE.DOMAIN_DATA_OFF_CORR_STATUS_OPEN) != null)
			offCorresOpen = (DomainDataView) viewMap
					.get(WebConstants.OFFICIAL_CORRESPONDENCE.DOMAIN_DATA_OFF_CORR_STATUS_OPEN);
		return offCorresOpen;
	}

	public void setOffCorresOpen(DomainDataView offCorresOpen) {
		this.offCorresOpen = offCorresOpen;
	}

	public DomainDataView getOffCorresClose() {
		if (viewMap
				.get(WebConstants.OFFICIAL_CORRESPONDENCE.DOMAIN_DATA_OFF_CORR_STATUS_CLOSED) != null)
			offCorresClose = (DomainDataView) viewMap
					.get(WebConstants.OFFICIAL_CORRESPONDENCE.DOMAIN_DATA_OFF_CORR_STATUS_CLOSED);
		return offCorresClose;
	}

	public void setOffCorresClose(DomainDataView offCorresClose) {
		this.offCorresClose = offCorresClose;
	}

	public boolean isAssetMemsFieldReadonly() {
		boolean readonly = false;
		readonly = (isPageModeView() || viewMap
				.containsKey(IS_ASSET_SELECTED_FROM_SEARCH));
		return readonly;
	}

	@SuppressWarnings("unchecked")
	public boolean isViewModePopUp() {
		boolean isPopUp = false;
		isPopUp = (viewMap
				.containsKey(WebConstants.InheritanceFilePageMode.IS_POPUP));
		return isPopUp;
	}

	@SuppressWarnings("unchecked")
	public Boolean completeTask(TaskOutcome taskOutcome) throws Exception {
		logger.logInfo("completeTask() started...");
		UserTask userTask = null;
		Boolean success = true;
		String contextPath = ((ServletContext) getFacesContext()
				.getExternalContext().getContext())
				.getRealPath("\\WEB-INF\\config.properties");
		logger.logInfo("Contextpath is: " + contextPath);
		String loggedInUser = CommonUtil.getLoggedInUser();
		if (viewMap.containsKey(TASK_LIST_USER_TASK))
			userTask = (UserTask) viewMap.get(TASK_LIST_USER_TASK);
		BPMWorklistClient client = new BPMWorklistClient(contextPath);
		logger.logInfo("UserTask is: " + userTask.getTaskType());
		if (userTask != null) {
			client.completeTask(userTask, loggedInUser, taskOutcome);
		}
		logger.logInfo("completeTask() completed successfully!!!");
		return success;
	}

	public Boolean invokeOpenFileBpel(Long requestId, Long fileId)
			throws Exception {
		logger.logInfo("invokeOpenFileBpel() started...");
		Boolean success = true;
		MEMSOpenFileBPELPortClient bpelPortClient = new MEMSOpenFileBPELPortClient();
		SystemParameters parameters = SystemParameters.getInstance();
		String endPoint = parameters
				.getParameter(WebConstants.InheritanceFile.MEMS_OPEN_FILE_BPEL_END_POINT);
		String userId = CommonUtil.getLoggedInUser();
		bpelPortClient.setEndpoint(endPoint);
		bpelPortClient.initiate(userId, requestId.toString(),
				fileId.toString(), null, null);
		logger.logInfo("invokeOpenFileBpel() completed successfully!!!");
		return success;
	}

	public boolean isShowCloseButton() {
		boolean show = false;
		show = (isViewModePopUp());
		return show;
	}

	public boolean isShowFollowUpTab() {
		boolean show = false;
		show = (isPageModeApproved() || isPageModeInitLimDone()
				|| isPageModeFinalLimDone() || isPageModeDistReq()
				|| isPageModeInitLimRjctd() || isPageModeView());
		return show;
	}

	public void followUpTabClick() {

	}

	@SuppressWarnings("unchecked")
	public void checkTask() {
		String methodName = "checkTask ";
		logger.logInfo(methodName + "started.");
		UserTask userTask = null;
		Long requestId = 0L;
		try {
			userTask = (UserTask) sessionMap
					.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			viewMap.put(TASK_LIST_USER_TASK, userTask);
			viewMap.put(TASK_TYPE, userTask.getTaskType());
			if (userTask != null) {
				Map taskAttributes = userTask.getTaskAttributes();
				if (taskAttributes.get(WebConstants.UserTasks.REQUEST_ID) != null) {
					RequestView requestView = null;
					requestId = Convert.toLong(taskAttributes
							.get(WebConstants.UserTasks.REQUEST_ID));
					if (requestId != null) {
						requestView = new RequestService()
								.getRequestById(requestId);
						if (requestView != null
								&& requestView.getRequestId() != null) {
							// to show related data in different tabs
							putRequestDataInViewMap(requestView);
							viewMap.put(WebConstants.REQUEST_VIEW, requestView);
							Long fileId = requestView.getInheritanceFileView()
									.getInheritanceFileId();
							viewMap.put(
									WebConstants.InheritanceFile.FILE_TYPE_ID,
									requestView.getInheritanceFileView()
											.getFileTypeId());
							viewMap.put(WebConstants.InheritanceFile.FILE_ID,
									fileId);
							// status to handle page mode
							viewMap
									.put(
											WebConstants.InheritanceFile.FILE_STATUS_ID,
											requestView
													.getInheritanceFileView()
													.getStatusId());
							if (StringHelper.isNotEmpty(requestView
									.getInheritanceFileView().getResearcher()))
								viewMap.put(IS_RESEARCHER_ASSIGNED, true);

							if (StringHelper.isNotEmpty(requestView
									.getInheritanceFileView()
									.getAssignedExpert()))
								viewMap.put(IS_INHERITANCE_EXPERT_ASSIGNED,
										true);

							setPageModeByFilesStatus();
						}
					}

				}
				selectTabByTaskType();
			}
			logger.logInfo(methodName + " completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException(methodName + " crashed ", exception);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}
	}

	public boolean isTaskApproveOpeningNewFile() {
		boolean isTaskMatched = false;
		if (viewMap.get(TASK_TYPE) != null) {
			String taskType = viewMap.get(TASK_TYPE).toString();
			if (taskType
					.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.APPROVE_OPENING_NEW_FILE) == 0)
				isTaskMatched = true;
		}
		return isTaskMatched;
	}

	public boolean isTaskForLimitation() {
		boolean isTaskMatched = false;
		if (viewMap.get(TASK_TYPE) != null) {
			String taskType = viewMap.get(TASK_TYPE).toString();
			if (taskType
					.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.TASK_FOR_LIMITATION) == 0)
				isTaskMatched = true;
		}
		return isTaskMatched;
	}

	public boolean isTaskForApprovingTheLimitatione() {
		boolean isTaskMatched = false;
		if (viewMap.get(TASK_TYPE) != null) {
			String taskType = viewMap.get(TASK_TYPE).toString();
			if (taskType
					.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.TASK_FOR_APPROVING_LIMITATION) == 0)
				isTaskMatched = true;
		}
		return isTaskMatched;
	}

	public boolean isTaskForAssigningResearcher() {
		boolean isTaskMatched = false;
		if (viewMap.get(TASK_TYPE) != null) {
			String taskType = viewMap.get(TASK_TYPE).toString();
			if (taskType
					.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.TASK_FOR_ASSIGNING_RESEARCHER) == 0)
				isTaskMatched = true;
		}
		return isTaskMatched;
	}

	public boolean isTaskForResearcher() {
		boolean isTaskMatched = false;
		if (viewMap.get(TASK_TYPE) != null) {
			String taskType = viewMap.get(TASK_TYPE).toString();
			if (taskType
					.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.TASK_FOR_RESEARCHER) == 0)
				isTaskMatched = true;
		}
		return isTaskMatched;
	}

	@SuppressWarnings("unchecked")
	public void sendRecommendationsForApproval() {
		String methodName = "sendRecommendationsForApproval|";

		logger.logInfo(methodName + "Start|");
		try {

			if (viewMap.get(FOLLOW_UP_VIEW_LIST) != null) {
				List<MemsFollowupView> memsFollowupList = null;
				List<MemsFollowupView> memsFollowupListToSend = new ArrayList<MemsFollowupView>();
				DomainDataView domainData = null;
				boolean isAnySelected = false;
				domainData = CommonUtil
						.getIdFromType(
								CommonUtil
										.getDomainDataListForDomainType(WebConstants.MemsFollowupTab.MEMS_FOLLOW_UP_STATUS),
								WebConstants.MemsFollowupTab.MEMS_FOLLOW_UP_STATUS_SEND_FOR_APPROVAL);
				memsFollowupList = (List<MemsFollowupView>) viewMap
						.get(FOLLOW_UP_VIEW_LIST);
				if (memsFollowupList != null && memsFollowupList.size() > 0) {
					logger.logInfo(methodName + "MemsFollowUpList size=%s|",
							memsFollowupList.size());
					for (MemsFollowupView followup : memsFollowupList) {
						if (followup.isSelected()) {
							// if recommendations are attached then send it for
							// approval else show error message
							if (isFollowupRecommAvailable(followup
									.getFollowupId())) {
								logger.logInfo(methodName
										+ "Follow up selected|", followup
										.getFollowupId());
								followup.setStatusId(domainData
										.getDomainDataId());
								followup.setShowCheckBox(false);
								isAnySelected = true;
								followup.setSelected(false);
								memsFollowupListToSend.add(followup);
							} else {
								String formattedMsg = "";
								formattedMsg += MessageFormat
										.format(
												CommonUtil
														.getBundleMessage("errMsg.noRecommWithFollowUp"),
												followup.getDescription());
								errorMessages.add(formattedMsg);
								return;
							}
						}
					}
					try {
						if (isAnySelected) {
							SocialResearchView socialResearchView = new MemsFollowupService()
									.sendFollowupForApproval(memsFollowupListToSend);
							if (socialResearchView.getSocialResearchId() != null) {
								invokeSocialResearchBpel(socialResearchView
										.getSocialResearchId());
								saveSystemComments(MessageConstants.InheritanceFileMessage.FOLLOW_UP_RECOMM_SENT_FOR_APPROVAL);
								saveSystemComments(MessageConstants.InheritanceFileMessage.SENT_TO_SOCIAL_RESEARCH);
								// successMessages.add(CommonUtil.getBundleMessage("successMsg.FollowupSent"));
								successMessages
										.add(CommonUtil
												.getBundleMessage("successMsg.socResCreated"));
								btnSendRecommForApproval.setRendered(false);
							}
						} else {
							fileTabPanel.setSelectedTab(TAB_ID_FOLLOW_UP);
							errorMessages
									.add(CommonUtil
											.getBundleMessage("errMsg.selectFollowupForApproval"));
						}
					} catch (Exception e) {
						logger.LogException(
								"sendRecommendationsForApproval crashed", e);
						errorMessages.add(CommonUtil
								.getBundleMessage("commons.ErrorMessage"));
					}
					viewMap.put(FOLLOW_UP_VIEW_LIST, memsFollowupList);
				} else {

					fileTabPanel.setSelectedTab(TAB_ID_FOLLOW_UP);
					errorMessages.add(CommonUtil
							.getBundleMessage("errMsg.addFollowupForApproval"));
				}
			}
			logger.logInfo(methodName + "Finish|");
		} catch (Exception ex) {
			logger.LogException("sendRecommendationsForApproval crashed", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public boolean isFollowupRecommAvailable(Long followupId) throws Exception {
		boolean isAvailable = false;
		try {
			List<ResearchRecommendationView> recomm = socialService
					.getRecommendationsByFollowupId(followupId);
			if (recomm != null && recomm.size() > 0) {
				isAvailable = true;
			}
		} catch (Exception ex) {
			logger
					.LogException("isFollowupRecommAvailable|Error Occured..",
							ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return isAvailable;
	}

	public boolean isShowSendRecommForApproval() {
		boolean show = true;
		show = (isTaskForResearcher() || viewMap
				.containsKey(IS_RESEARCHER_ASSIGNED))
				&& !(isViewModePopUp() || isPageModeView());
		return show;
	}

	public HtmlCommandButton getBtnSendRecommForApproval() {
		return btnSendRecommForApproval;
	}

	public void setBtnSendRecommForApproval(
			HtmlCommandButton btnSendRecommForApproval) {
		this.btnSendRecommForApproval = btnSendRecommForApproval;
	}

	public List<DomainDataView> getRevenueTypeListDD() {
		return revenueTypeListDD;
	}

	public void setRevenueTypeListDD(List<DomainDataView> revenueTypeListDD) {
		this.revenueTypeListDD = revenueTypeListDD;
	}

	public void setResearcherList(List<SelectItem> researcherList) {
		this.researcherList = researcherList;
	}

	public void setRevenueTypeMap(HashMap<Long, DomainDataView> revenueTypeMap) {
		this.revenueTypeMap = revenueTypeMap;
	}

	public Boolean invokeSocialResearchBpel(Long soicalResearchId) {
		logger.logInfo("invokeSocialResearchBpel() started...");
		Boolean success = true;
		try {

			MEMSSocialReserchBPELPortClient bpelPortClient = new MEMSSocialReserchBPELPortClient();

			SystemParameters parameters = SystemParameters.getInstance();
			String endPoint = parameters
					.getParameter(WebConstants.SocialResearch.MEMS_SOCIAL_RESEARCH_BPEL_END_POINT);
			String userId = CommonUtil.getLoggedInUser();
			bpelPortClient.setEndpoint(endPoint);
			bpelPortClient.initiate(soicalResearchId, userId, viewMap.get(
					WebConstants.InheritanceFile.FILE_ID).toString(), null,
					null);
			logger
					.logInfo("invokeSocialResearchBpel() completed successfully!!!");
		} catch (PIMSWorkListException exception) {
			success = false;
			logger.LogException("invokeSocialResearchBpel() crashed ",
					exception);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		} catch (Exception exception) {
			success = false;
			logger.LogException("invokeSocialResearchBpel() crashed ",
					exception);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		} finally {
			if (!success) {
				errorMessages.clear();
				errorMessages
						.add(CommonUtil
								.getBundleMessage(MessageConstants.EvaluationApplication.MSG_REQUEST_SUBMIT_FAILURE));
			}
		}
		return success;
	}

	private void selectTabByTaskType() {
		String taskType = "";
		if (viewMap.get(TASK_TYPE) != null)
			taskType = viewMap.get(TASK_TYPE).toString();
		if (taskType
				.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.APPROVE_OPENING_NEW_FILE) == 0) {
			fileTabPanel.setSelectedTab(TAB_ID_APPLICANT);
		} else if (taskType
				.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.TASK_FOR_LIMITATION) == 0) {
			fileTabPanel.setSelectedTab(TAB_ID_INHERITED_ASSET);
			inheritedAssetTabClick();
		} else if (taskType
				.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.TASK_FOR_APPROVING_LIMITATION) == 0) {
			fileTabPanel.setSelectedTab(TAB_ID_OFF_CORRESP);
			officialCorrespondenceTabClick();
		} else if (taskType
				.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.TASK_FOR_ASSIGNING_RESEARCHER) == 0) {
			fileTabPanel.setSelectedTab(TAB_ID_APPLICANT);
		} else if (taskType
				.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.TASK_FOR_RESEARCHER) == 0) {
			fileTabPanel.setSelectedTab(TAB_ID_FOLLOW_UP);
			followUpTabClick();
		} else if (taskType
				.compareToIgnoreCase(WebConstants.InheritanceFileTaskType.TASK_FOR_FINANCE) == 0) {
			fileTabPanel.setSelectedTab(TAB_ID_DISTRIBUTE_SHARES);
			onDistributionTabClick();
		}

	}

	public HtmlOutputLabel getLblRejectionReason() {
		return lblRejectionReason;
	}

	public void setLblRejectionReason(HtmlOutputLabel lblRejectionReason) {
		this.lblRejectionReason = lblRejectionReason;
	}

	public boolean isResearcherAavailable() {
		boolean isAvailable = false;
		isAvailable = viewMap.containsKey(IS_RESEARCHER_ASSIGNED);
		if ((isTaskForAssigningResearcher() && !isAvailable)
				|| isPageModeFinalLimDone() || isPageModeDistReq())
			return true;

		return isAvailable;
	}

	public boolean isInheritanceExpertAavailable() {
		boolean isAvailable = false;
		isAvailable = viewMap.containsKey(IS_RESEARCHER_ASSIGNED);
		if (!isAvailable || isPageModeFinalLimDone() || isPageModeDistReq())
			return true;

		return isAvailable;
	}

	private Double getTotalSharePercentage(
			List<BeneficiaryDetailsView> beneDtlList) {
		Double totalShare = 0D;
		for (BeneficiaryDetailsView details : beneDtlList) {
			if (!details.isDeleted() && details.getSharePercentage() != null
					&& details.getSharePercentage().trim().length() > 0)
				totalShare += new Double(details.getSharePercentage());
		}
		return totalShare;
	}

	public boolean isShowBeneDetailsInputs() {
		boolean show = false;
		show = !(isViewModePopUp() || isPageModeView());
		return show;
	}

	public DomainDataView getFileStDistributionReqDD() {

		if (viewMap.get(FILE_DISTRIBUTION_REQ_STATUS) != null)
			fileStDistributionReqDD = (DomainDataView) viewMap
					.get(FILE_DISTRIBUTION_REQ_STATUS);
		return fileStDistributionReqDD;
	}

	public void setFileStDistributionReqDD(
			DomainDataView fileStDistributionReqDD) {
		this.fileStDistributionReqDD = fileStDistributionReqDD;
	}

	public DomainDataView getFileStClosedDD() {
		if (viewMap.get("FILE_CLOSED_STATUS") != null)
			fileStClosedDD = (DomainDataView) viewMap.get("FILE_CLOSED_STATUS");
		return fileStClosedDD;
	}

	public void setFileStClosedDD(DomainDataView fileStClosedDD) {
		this.fileStClosedDD = fileStClosedDD;
	}

	public boolean isShowPrintStatement() {
		boolean show = false;
		show = (!(isPageModeNew() || isPageModeAppReq() || isPageModeView() || isViewModePopUp()));
		return show;
	}

	@SuppressWarnings("unchecked")
	public void onBlockingTabClick() {
		try {
			TabFileBlockingDetails tab = (TabFileBlockingDetails) getBean("pages$tabFileBlockingDetails");
			Long fileId = null;
			if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) != null) {
				InheritanceFileView vo = (InheritanceFileView) viewMap
						.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);
				fileId = vo.getInheritanceFileId();
			}
			tab.loadTabData(fileId);
		} catch (Exception e) {

			logger.LogException("onBlockingTabClick|Error Occured", e);
			errorMessages = new ArrayList<String>(0);

			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}

	@SuppressWarnings("unchecked")
	public void onDistributionTabClick() {
		try {
			DistributeShareTabBean tab = (DistributeShareTabBean) getBean("pages$distributeShareTab");
			Long fileId = null;
			if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) != null) {
				InheritanceFileView vo = (InheritanceFileView) viewMap
						.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);
				fileId = vo.getInheritanceFileId();
			}
			tab.loadTabData(fileId);
		} catch (Exception e) {

			logger.LogException("onDistributionTabClick|Error Occured", e);
			errorMessages = new ArrayList<String>(0);

			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}

	public HtmlCommandButton getBtnDistDone() {
		return btnDistDone;
	}

	public void setBtnDistDone(HtmlCommandButton btnDistDone) {
		this.btnDistDone = btnDistDone;
	}

	public boolean isShowBasicInfoTab() {
		boolean show = false;
		if (cboFileType.getValue() != null
				&& cboFileType.getValue().toString().compareTo("-1") != 0)
			show = true;
		return show;
	}

	public boolean isShowBenenficiaryTab() {
		boolean show = false;
		// if(cboFileType.getValue() != null &&
		// cboFileType.getValue().toString().compareTo("-1") != 0 && (isAbsent()
		// || isDeceased()))
		if (cboFileType.getValue() != null
				&& cboFileType.getValue().toString().compareTo("-1") != 0
				&& (isAbsent() || isDeceased() || isUnderGuradian()))
			show = true;
		return show;
	}
	public boolean isShowBenenficiaryRepresentativeTab() {
		boolean show = true;
//		// if(cboFileType.getValue() != null &&
//		// cboFileType.getValue().toString().compareTo("-1") != 0 && (isAbsent()
//		// || isDeceased()))
//		if (cboFileType.getValue() != null
//				&& cboFileType.getValue().toString().compareTo("-1") != 0
//				&& (isAbsent() || isDeceased() || isUnderGuradian()))
//			show = true;
		return show;
	}


	public boolean isRelationShipRequired() {
		boolean required = true;
		required = (isDeceased() || isAbsent());
		return required;
	}

	public boolean isFilePersonBeneficiary() {
		boolean result = true;
		result = (isUnderGuradian() || isLegalAsstnc() || isFounding());
		return result;
	}

	// will be used in included JSP
	public boolean isShowGuardianSearch() {
		boolean show = false;
		show = (!(isPageModeView() || isViewModePopUp() || isFounding()));
		return show;
	}

	// will be used in included JSP
	public boolean isShowNurseSearch() {
		boolean show = false;
		show = (!(isPageModeView() || isViewModePopUp()));
		return show;
	}

	@SuppressWarnings("unchecked")
	public void removeDetailsAndKeepBeneficiary() {
		if (viewMap
				.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW) != null) {
			// remove Details
			if (viewMap
					.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST) != null
					&& ((List<BeneficiaryDetailsView>) viewMap
							.get(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST))
							.size() > 0) {

				viewMap
						.remove(WebConstants.InheritanceFile.BENEFICIARY_DETAILS_LIST);
			}
			PersonView filePerson = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW);
			filePerson.setSharePercentage("100");
			viewMap.put(
					WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW,
					filePerson);

		}
	}

	public boolean isShowAcknowledgeButton() {
		boolean show = true;
		show = isTaskForResearcher()
				&& !(isViewModePopUp() || isPageModeView());
		return show;
	}

	public HtmlCommandButton getBtnAcknowledge() {
		return btnAcknowledge;
	}

	public void setBtnAcknowledge(HtmlCommandButton btnAcknowledge) {
		this.btnAcknowledge = btnAcknowledge;
	}

	@SuppressWarnings("unchecked")
	public void acknowledgeResearcherTask() {
		final String methodName = "acknowledgeResearcherTask|";
		try {
			// here we will terminate one branch of open file bpel and invoke
			// another BPEL
			if (viewMap.get(TASK_TYPE) != null
					&& viewMap
							.get(TASK_TYPE)
							.toString()
							.compareTo(
									WebConstants.InheritanceFileTaskType.TASK_FOR_RESEARCHER) == 0) {
				// if task type is available for researcher then terminate old
				// BPEL
				if (completeTask(TaskOutcome.OK)) {

					btnAcknowledge.setRendered(false);
					saveSystemComments(MessageConstants.InheritanceFileMessage.FOLLOW_UP_TASK_ACKNOWLEGDE);
					saveAttachmentComment();
					if (viewMap.get(WebConstants.REQUEST_VIEW) != null) // Saved
																		// Request
					{
						RequestView requestView = (RequestView) viewMap
								.get(WebConstants.REQUEST_VIEW);
						requestView = new InheritanceFileService()
								.getInheritanceFileRequestByFileId(requestView
										.getInheritanceFileId(), null);
						getFileDetails(requestView.getInheritanceFileId(),
								requestView);
					}
					successMessages
							.add(CommonUtil
									.getBundleMessage("successMessage.followupTaskCompleted"));
				}
			}
		} catch (Exception ex) {
			logger.LogException(methodName + "|Error Occured..", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		logger.logInfo(methodName + "|" + "Finish..");
	}

	public boolean isShowPrintStatmentOfAccImage() {
		boolean show = true;
		show = (!(isPageModeNew() || isPageModeAppReq() || isPageModeView() || isViewModePopUp()));
		return show;
	}

	private InheritanceBeneficiaryView getFilledInheritanceBeneficiaryView() {
		InheritanceBeneficiaryView inheritanceBeneficiaryView = new InheritanceBeneficiaryView();

		PersonView beneficiaryPerson = null;
		PersonView filePerson = null;
		PersonView nursePerson = null;
		RequestView savedRequest = null;

		if (viewMap.get(WebConstants.REQUEST_VIEW) != null) // Saved Request
		{
			savedRequest = (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);
			if (savedRequest.getInheritanceFileView() != null
					&& savedRequest.getInheritanceFileView()
							.getInheritanceBeneficiaryList() != null
					&& savedRequest.getInheritanceFileView()
							.getInheritanceBeneficiaryList().size() > 0) {
				inheritanceBeneficiaryView = savedRequest
						.getInheritanceFileView()
						.getInheritanceBeneficiaryList().get(0);
			}
		} else {
			inheritanceBeneficiaryView.setInheritanceBeneficiaryId(null);
			inheritanceBeneficiaryView.setInheritanceFileView(null);
			inheritanceBeneficiaryView.setCreatedBy(CommonUtil
					.getLoggedInUser());
			inheritanceBeneficiaryView
					.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
			inheritanceBeneficiaryView
					.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
			inheritanceBeneficiaryView.setRelationshipIdString(null);
			inheritanceBeneficiaryView.setRelationshipDesc(null);
			inheritanceBeneficiaryView.setSelfBeneficiary(true);
		}
		if (viewMap
				.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW) != null) {
			beneficiaryPerson = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.FILE_PERSON_BEENFICIARY);
			filePerson = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW);
			beneficiaryPerson.setPassportName(filePerson.getPassportName());
		}

		if (viewMap.get(WebConstants.InheritanceFile.FILE_PERSON_NURSE) != null) {
			nursePerson = (PersonView) viewMap
					.get(WebConstants.InheritanceFile.FILE_PERSON_NURSE);
		}

		// This information can be changed by the user, so reload it for both
		// cases : New /Edit
		inheritanceBeneficiaryView.setBeneficiaryId(beneficiaryPerson
				.getPersonId());

		if (nursePerson != null && nursePerson.getPersonId() != null) // fetch
																		// Nurse
																		// details
																		// only
																		// if
																		// one
																		// will
																		// be
																		// available
		{
			inheritanceBeneficiaryView.setNurseId(nursePerson.getPersonId());
			if (nursePerson.getRelationshipString() != null
					&& nursePerson.getRelationshipString().compareTo("-1") != 0) {
				inheritanceBeneficiaryView
						.setNurseRelationshipIdString(nursePerson
								.getRelationshipString());
			}
			inheritanceBeneficiaryView.setNurseRelationshipDesc(nursePerson
					.getRelationshipDesc());
		}

		if (beneficiaryPerson.getSharePercentage() != null
				&& StringHelper.isNotEmpty(beneficiaryPerson
						.getSharePercentage()))
			inheritanceBeneficiaryView.setSharePercentage(Double
					.parseDouble(beneficiaryPerson.getSharePercentage()));
		inheritanceBeneficiaryView.setUpdatedOn(new Date());
		inheritanceBeneficiaryView.setUpdatedBy(CommonUtil.getLoggedInUser());

		return inheritanceBeneficiaryView;
	}

	@SuppressWarnings("unchecked")
	public void openFilePersonPopup() {
		List<Long> filePersonIds = null;
		try {
			// filePersonIds = new
			// InheritanceFileService().getAllFilePersonIds();
			// if(filePersonIds != null && filePersonIds.size() > 0)
			// {
			// sessionMap.put(WebConstants.InheritanceFile.PERSON_IDS_TO_BE_EXCLUDED,
			// filePersonIds);
			// }
			executeJavascript("showFilePersonPopup();");
		} catch (Exception ex) {
			logger.LogException("openFilePersonPopup|Error Occured..", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void executeJavascript(String javascript) throws Exception {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javascript);
	}

	/**
	 * Desc : This method will hide and show the image to search Nurse and
	 * guardian
	 * 
	 */
	public boolean isShowPersonNurseAndGuardianSearch() {
		boolean show = true;
		show = (isFilePersonBeneficiary() && (isPageModeNew()
				|| isPageModeAppReq() || isPageModeRejectedResubmit()));
		return show;
	}

	/**
	 * Desc : This method will hide and show the image to open social research
	 * form against file person
	 * 
	 */
	public boolean isShowSocialResearchImage() {
		return (isAnyFileSelected() && isFilePersonBeneficiary());
	}

	public boolean isAnyFileSelected() {
		if (cboFileType.getValue() != null
				&& cboFileType.getValue().toString().compareTo("-1") != 0)
			return true;
		else
			return false;
	}

	@SuppressWarnings("unchecked")
	public void populateMandatoryDocs() {
		viewMap.put(WebConstants.Attachment.PROCEDURE_DOCS,
				new ArrayList<DocumentView>());
		viewMap.put(WebConstants.Attachment.PROCEDURE_SUB_KEY, cboFileType
				.getValue().toString());
		((AttachmentBean) getBean("pages$attachment")).getProcedureDocs();
	}

	public boolean isShowAttachmentTab() {
		return isAnyFileSelected();
	}

	@SuppressWarnings("unchecked")
	public void onAttachmentTabClick() {
		String METHOD_NAME = "onAttachmentTabClick";
		try {
			logger.logInfo(METHOD_NAME + " --- Start--- ");
			// TODO:
			logger.logInfo(METHOD_NAME + " --- Finish--- ");
		} catch (Exception e) {

			logger.LogException(METHOD_NAME + "|Error Occured", e);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}

	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(Long fileId) {
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY,
				WebConstants.InheritanceFile.PROCEDURE_KEY_OPEN_FILE);
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID,
				WebConstants.Attachment.PIMS_REPOSITORY_ID);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,
				WebConstants.InheritanceFile.EXTERNAL_ID_OPEN_FILE);
		viewMap.put("noteowner", WebConstants.InheritanceFile.NOTES_OWNER);

		if (fileId != null) {
			String entityId = fileId.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}

	public String getTxtTotalFileShare() {
		return txtTotalFileShare;
	}

	public void setTxtTotalFileShare(String txtTotalFileShare) {
		this.txtTotalFileShare = txtTotalFileShare;
	}

	private Double getTotalBeneficiariesShares(
			List<BeneficiaryDetailsView> beneDtlList) {
		Double totalShare = 0D;
		if (beneDtlList != null && beneDtlList.size() > 0) {
			for (BeneficiaryDetailsView details : beneDtlList) {
				if (!details.isDeleted()
						&& details.getSharePercentage() != null
						&& details.getSharePercentage().trim().length() > 0) {
					totalShare += new Double(details.getSharePercentage());
				}
			}
		}
		return totalShare;
	}

	public double getFileShareDbl() {
		if (txtTotalFileShare != null
				&& StringHelper.isNotEmpty(txtTotalFileShare)) {
			return Double.parseDouble(txtTotalFileShare);
		} else
			return 0D;
	}

	public HtmlInputText getTxtNewCostCenter() {
		return txtNewCostCenter;
	}

	public void setTxtNewCostCenter(HtmlInputText txtNewCostCenter) {
		this.txtNewCostCenter = txtNewCostCenter;
	}

	public String getTxtOldCostCenter() {
		return txtOldCostCenter;
	}

	public void setTxtOldCostCenter(String txtOldCostCenter) {
		this.txtOldCostCenter = txtOldCostCenter;
	}

	public boolean isAfterApproval() {
		return !(isPageModeNew() || isPageModeAppReq() || isPageModeRejectedResubmit());
	}

	public boolean getIsMigratedFile() {

		if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) != null) {
			InheritanceFileView file = (InheritanceFileView) viewMap
					.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);
			return (file.getMigrated() == 1L);
		}

		return false;
	}

	public void receiveManager() {
		if (StringHelper.isNotEmpty(hdnAssetId)) {
			AssetMemsView assetMemsView = new AssetMemsView();
			try {
				assetMemsView = new InheritanceFileService()
						.getAssetMemsById(Long.parseLong(hdnAssetId));
				if (assetMemsView != null && assetMemsView.getAssetId() != null) {
					assetMemsView.setRevenueTypeString(CommonUtil
							.getIsEnglishLocale() ? assetMemsView
							.getAssetTypeView().getAssetTypeNameEn()
							: assetMemsView.getAssetTypeView()
									.getAssetTypeNameAr());
					// assetMemsView.setExtractedFromInheritedAsstId(assetMemsView.getInheritedAssetViewList().get(0).getInheritedAssetId());
					// assetMemsView.setAssociatedCncrndDepttId(assetMemsView.getConcernedDepartmentViewList().get(0).getMemsAssetCncrndDeptId());
					viewMap.put(ASSET_MEMS_VIEW, assetMemsView);
					if (assetMemsView.getConcernedDepartmentViewList() != null
							&& assetMemsView.getConcernedDepartmentViewList()
									.size() > 0)
						viewMap.put(MEMS_CNRCRND_DEPT_VIEW, assetMemsView
								.getConcernedDepartmentViewList().get(0));
					viewMap.put(IS_ASSET_SELECTED_FROM_SEARCH, true);
					InheritedAssetsTabBean iAssetBean = (InheritedAssetsTabBean) getBean("pages$inheritedAssetsTab");
					iAssetBean.assetTypeChanged();
				}
			} catch (Exception e) {
				logger.LogException("inheritedAssetTabClick() crashed", e);
				errorMessages.add(CommonUtil
						.getBundleMessage("commons.ErrorMessage"));
			}
		}
	}

	public String getHdnManagerId() {
		return hdnManagerId;
	}

	public void setHdnManagerId(String hdnManagerId) {
		this.hdnManagerId = hdnManagerId;
	}

	/**
	 * @return the cboAssignedExpert
	 */
	public HtmlSelectOneMenu getCboAssignedExpert() {
		return cboAssignedExpert;
	}

	/**
	 * @param cboAssignedExpert
	 *            the cboAssignedExpert to set
	 */
	public void setCboAssignedExpert(HtmlSelectOneMenu cboAssignedExpert) {
		this.cboAssignedExpert = cboAssignedExpert;
	}
	
	

}
