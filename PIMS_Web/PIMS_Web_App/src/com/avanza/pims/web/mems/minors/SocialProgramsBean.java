package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.pims.Utils.SocialProgramSearch;
import com.avanza.pims.bpel.memssocialsolidatorybpel.proxy.MEMSSocialSolidatoryBPELPortClient;
import com.avanza.pims.constant.Constant;
import com.avanza.core.util.Logger;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.plugins.ProgramActionList;
import com.avanza.pims.web.mems.plugins.ProgramBudgetDetailsBean;
import com.avanza.pims.web.mems.plugins.ProgramParticipants;
import com.avanza.pims.web.mems.plugins.ProgramPositiveNegativesBean;
import com.avanza.pims.web.mems.plugins.ProgramPurposes;
import com.avanza.pims.web.mems.plugins.ProgramRecommendationsBean;
import com.avanza.pims.web.mems.plugins.ProgramRequirementBean;
import com.avanza.pims.web.mems.plugins.ProgramSponsorsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.SocialProgramService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.UserView;
import com.avanza.pims.ws.vo.mems.SocialProgramView;
import com.avanza.ui.util.ResourceUtil;


public class SocialProgramsBean extends AbstractMemsBean{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(SocialProgramsBean.class);
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "PAGE_MODE_NEW";
	private static final String PAGE_MODE_APPROVAL_REQ = "PAGE_MODE_APPROVAL_REQ";
	private static final String PAGE_MODE_APPROVE = "PAGE_MODE_APPROVE";
	private static final String PAGE_MODE_COMPLETE = "PAGE_MODE_COMPLETE";
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_BASIC_INFO = "basicInfo";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_POS_NEG = "programPosNeg";
	private static final String TAB_RECOMMENDATIONS = "programRecommendationsTab";
	private static final String EXTERNAL_ID ="externalId";
    private String pageTitle;
    private String pageMode;
	private HtmlCommandButton btnSave = new HtmlCommandButton();
	private HtmlCommandButton btnSubmit = new HtmlCommandButton();
	private HtmlCommandButton btnApprove = new HtmlCommandButton();
	private HtmlCommandButton btnReject = new HtmlCommandButton();
	private HtmlCommandButton btnComplete = new HtmlCommandButton();
    private SocialProgramService socialProgramService = new SocialProgramService();	
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	private Map<String,String> gracePeriodMap = new HashMap<String, String>();
	
	private SocialProgramView socialProgramView ;
	public SocialProgramsBean()
	{
		socialProgramView = new SocialProgramView();
	}
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
		
				 viewMap.put(PROCEDURE_TYPE,WebConstants.PROCEDURE_SOCIAL_SOLIDATORY );
		    	 viewMap.put(EXTERNAL_ID ,WebConstants.Attachment.EXTERNAL_ID_SOCIAL_SOLIDARITY);
		    	 viewMap.put("canAddAttachment", true);
		 		 viewMap.put("canAddNote", true);
		 	
				
		 		loadAttachmentsAndComments(null);
				if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
				{
					
				  getDataFromTaskList();
				}
				
				else if ( sessionMap.get(WebConstants.SocialSolidarityProgram.SOCIAL_PROGRAM_VO_FROM_SEARCH ) != null )
				{
					
				  getDataFromSearch();
				}
				
				getPageModeFromProgramStatus();
			}
			
			updateValuesFromMap();
			
	         	
		}
		catch ( Exception e )
		{
			logger.LogException("init|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromProgramStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		if( this.getSocialProgramView() != null  && this.getSocialProgramView().getSocialProgramId() != null)
		{
			
//			if( this.getSocialProgramView().getStatusKey().equals( WebConstants.SocialProgramStatus.NEW) )
//			{
//				this.setPageMode( PAGE_MODE_NEW );
//			} else
			if( this.getSocialProgramView().getStatusKey().equals( WebConstants.SocialProgramStatus.APPROVAL_REQUIRED) )
			{
				this.setPageMode( PAGE_MODE_APPROVAL_REQ );
			}
			else if( this.getSocialProgramView().getStatusKey().equals( WebConstants.SocialProgramStatus.APPROVED ) )
			{
				this.setPageMode( PAGE_MODE_COMPLETE );
			}
			else if( this.getSocialProgramView().getStatusKey().equals( WebConstants.SocialProgramStatus.REJECT ) ||
					this.getSocialProgramView().getStatusKey().equals( WebConstants.SocialProgramStatus.COMPLETE )
			        ) 
			{
				this.setPageMode( PAGE_MODE_VIEW );
			}
		}
	}
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		
		this.setSocialProgramView(  ( SocialProgramView )sessionMap.get(WebConstants.SocialSolidarityProgram.SOCIAL_PROGRAM_VO_FROM_SEARCH ) );
		sessionMap.remove(WebConstants.SocialSolidarityProgram.SOCIAL_PROGRAM_VO_FROM_SEARCH );
		
		if(this.getSocialProgramView() !=null )
		{
			loadAttachmentsAndComments(this.getSocialProgramView().getSocialProgramId());
		}
	}
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap()
	{
		
		if( viewMap.get( "SocialProgramView" ) != null )
		{
			socialProgramView = ( SocialProgramView )viewMap.get( "SocialProgramView" ) ;
		}
		getGracePeriodListMap();
		 updateValuesToMap();

	}
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap()
	{
		if( socialProgramView != null )
		{
		  viewMap.put("SocialProgramView" , socialProgramView);
		}
	}
	@SuppressWarnings( "unchecked" )
	public void prerender()
	{
		try
		{
		   renderControls();
		 //FROM Search Member Popup
			if( sessionMap.get( WebConstants.USER_VIEW ) != null)
			{
				UserView assignedEmp = (UserView) sessionMap.get( WebConstants.USER_VIEW );
				sessionMap.remove( WebConstants.USER_VIEW );
				//socialProgramView = getSocialProgramView();
				socialProgramView.setAssignedEmpId( assignedEmp.getUserName() );
				socialProgramView.setAssignedEmpEn( assignedEmp.getFullNamePrimary() );
				socialProgramView.setAssignedEmpAr( assignedEmp.getFullNameSecondary() );
				updateValuesToMap();
				
				successMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.empSelected"));
			}
		}
		catch ( Exception e )
		{
			errorMessages = new ArrayList<String>( 0 );
			logger.LogException("prerender|Error Occured..",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));			
		}
	}
	private void renderControls() {
		disableAllControls();
		   if( this.getPageMode().equals( PAGE_MODE_NEW ) )
	       {
	    	   setControlsForNewMode();
	       }
		   else if( this.getPageMode().equals( PAGE_MODE_APPROVAL_REQ ) )
	       {
	    	   setControlsForApproveMode();
	       }
		   else if( this.getPageMode().equals( PAGE_MODE_COMPLETE ) )
	       {
	    	   setControlsForCompleteMode();
	       }
	}
	private void setControlsForNewMode()
	{
		if( this.getSocialProgramView() != null && this.getSocialProgramView().getSocialProgramId() != null)
		{
		 btnSubmit.setRendered( true );
		
		}
		btnSave.setRendered( true );
		
	}
	private void setControlsForApproveMode()
	{
		btnApprove.setRendered(true);
		btnReject.setRendered(true);
		
	}
	private void setControlsForCompleteMode()
	{
		btnComplete.setRendered(true);
	}
	private void disableAllControls()
	{
		
		btnApprove.setRendered( false );
		btnReject.setRendered( false );
		btnComplete.setRendered( false );
		btnSubmit.setRendered( false );
		btnSave.setRendered( false );
	}
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  final String methodName="getDataFromTaskList";
	  
	  UserTask userTask = (UserTask) sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	  logger.logInfo(methodName+"|"+" TaskId..."+userTask.getTaskId());
	  setUserTask(userTask);
      sessionMap.remove(WebConstants.TASK_LIST_SELECTED_USER_TASK);
	  if(userTask.getTaskAttributes().get( "SOCIAL_RESEARCH_ID" )!=null)
      {
		  SocialProgramSearch searchCriteria = new SocialProgramSearch();
		  searchCriteria.setSocialProgramId( new Long  ( userTask.getTaskAttributes().get( "SOCIAL_RESEARCH_ID" ) ) );
		  List<SocialProgramView> list =  socialProgramService.getAllSocialPrograms(searchCriteria ,null, null, null , null );
		  this.setSocialProgramView( list.get( 0 )  );
		  loadAttachmentsAndComments(new Long  ( userTask.getTaskAttributes().get( "SOCIAL_RESEARCH_ID" ) ) );
    	  
      }
      
     
    }
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments(Long id)
	{
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_SOCIAL_SOLIDATORY);
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
    	viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.Attachment.EXTERNAL_ID_SOCIAL_SOLIDARITY);
		viewMap.put("noteowner", WebConstants.SocialSolidarityProgram.NOTES_OWNER);
		if(id != null){
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception {
		     saveComments();
			 saveAttachments(getSocialProgramView().getSocialProgramId().toString());
			 saveSystemComments(eventDesc);
		 }
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	final String methodName="saveSystemComments|";
    	try
    	{
	    	  logger.logInfo(methodName + "started...");
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.SocialSolidarityProgram.NOTES_OWNER;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, getSocialProgramView().getSocialProgramId() );
	    	  }
	    	  logger.logInfo(methodName + "completed successfully!!!");
	    	
    	}
    	catch (Exception exception) {
			logger.LogException(methodName + "crashed ", exception);
			throw exception;
		}
    }
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  )
    {
		Boolean success = false;
    	final String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	String notesOwner = WebConstants.SocialSolidarityProgram.NOTES_OWNER;
	    	//if(this.txtRemarks!=null && this.txtRemarks.length()>0)
	    	  //CommonUtil.saveRemarksAsComments(referenceId, txtRemarks, notesOwner) ;
	    	NotesController.saveNotes(notesOwner, getSocialProgramView().getSocialProgramId());
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null)
	    	{
	    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
		    	success = CommonUtil.updateDocuments();
	    	}
	    	
	    	
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	@SuppressWarnings( "unchecked" )
	public void tabAttachmentsComments_Click()
    {
	 if( getSocialProgramView()!= null &&  getSocialProgramView().getSocialProgramId() != null)
	 {
		 loadAttachmentsAndComments( getSocialProgramView().getSocialProgramId() );
	 }
	
    }
	@SuppressWarnings( "unchecked" )
	private void getGracePeriodListMap()
	{
		if( gracePeriodMap.size() <=0  )
		{
			List<DomainDataView> list = CommonUtil.getDomainDataListForDomainType(WebConstants.GRACE_PERIOD_TYPE);
			for (DomainDataView obj : list) {
			
				gracePeriodMap.put( obj.getDomainDataId().toString(),obj.getDataValue() );
			}
			viewMap.put(WebConstants.GRACE_PERIOD_TYPE, gracePeriodMap);
		}
		else
		{
			gracePeriodMap = (Map<String,String> )viewMap.get( WebConstants.GRACE_PERIOD_TYPE );
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onCalculateEndDate()
	{
		final String methodName = "onCalculateEndDate|";
		errorMessages = new ArrayList<String>();
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			
			updateValuesFromMap();
			if( socialProgramView.getPeriodFreq()!=null && socialProgramView.getPeriodDuration()!=null && 
				socialProgramView.getStartDate()!=null )
			{
				if( gracePeriodMap.containsKey( socialProgramView.getPeriodDuration()  ) )
				{
				       String gracePeriodDataValue = gracePeriodMap.get( socialProgramView.getPeriodDuration() ).toString(); 
				       //For Dates
	                   Calendar cal = Calendar.getInstance();
	                   //Setting The date o parameter
	                   cal.setTime( socialProgramView.getStartDate() );
			           if( gracePeriodDataValue.equals( WebConstants.GracePeriodDataValues.DAYS) )
			           {
			           
			           cal.add(Calendar.DATE,  Integer.valueOf(  socialProgramView.getPeriodFreq().toString() )  );
			            
			           }
			           else if( gracePeriodDataValue.equals( WebConstants.GracePeriodDataValues.MONTHS) )
			           { 
			           cal.add(Calendar.MONTH, Integer.valueOf(  socialProgramView.getPeriodFreq().toString() )   );
			           } 
			           else if(gracePeriodDataValue.equals(WebConstants.GracePeriodDataValues.YEARS) )
			           {
			            cal.add(Calendar.YEAR, Integer.valueOf(  socialProgramView.getPeriodFreq().toString() )   );
			           }
			           socialProgramView.setEndDate( cal.getTime());
			           updateValuesToMap();
				}
			}
			else
			{
			       socialProgramView.setEndDate( null );
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch ( Exception e )
		{
				logger.LogException("init|Error Occured..",e);
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));			
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onTemplateMethod()
	{
		final String methodName = "onSave|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			
//			if( !hasAddDisSrcError() )
			{
				
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onProgramRecommendationsTabClick()
	{
		final String methodName = "onProgramRecommendationsTabClick|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			ProgramRecommendationsBean bean =  (ProgramRecommendationsBean)getBean( "pages$programRecommendations" );
			if ( this.getSocialProgramView()!= null && this.getSocialProgramView().getSocialProgramId()!= null )
			{
			  
				bean.onLoadList( this.getSocialProgramView().getSocialProgramId() );
			}
			else
			{
				bean.setListLoadedFromDb( "1" );
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onProgramPositiveNegativesTabClick()
	{
		final String methodName = "onProgramPositiveNegativesTabClick|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			ProgramPositiveNegativesBean bean =  (ProgramPositiveNegativesBean)getBean( "pages$programPositiveNegatives" );
			if ( this.getSocialProgramView()!= null && this.getSocialProgramView().getSocialProgramId()!= null )
			{
				bean.onLoadList( this.getSocialProgramView().getSocialProgramId() );
			}
			else
			{
				bean.setListLoadedFromDb( "1" );
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onProgramPurposeTabClick()
	{
		final String methodName = "onProgramPurposeTabClick|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			ProgramPurposes purposeBean =  (ProgramPurposes)getBean( "pages$programPurposes");
			if ( this.getSocialProgramView()!= null && this.getSocialProgramView().getSocialProgramId()!= null )
			{
			  
			   purposeBean.onLoadPurposes( this.getSocialProgramView().getSocialProgramId() );
			}
			else
			{
				purposeBean.setListLoadedFromDb( "1" );
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onProgramActionTabClick()
	{
		final String methodName = "onProgramActionTabClick|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			ProgramActionList bean =  (ProgramActionList)getBean( "pages$programActionList");
			if ( this.getSocialProgramView()!= null && this.getSocialProgramView().getSocialProgramId()!= null )
			{
			  
				bean.onLoadActionList( this.getSocialProgramView().getSocialProgramId() );
			}
			else
			{
				bean.setListLoadedFromDb( "1" );
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onProgramRequirementTabClick()
	{
		final String methodName = "onProgramRequirementTabClick|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			ProgramRequirementBean bean =  (ProgramRequirementBean)getBean( "pages$programRequirement" );
			if ( this.getSocialProgramView()!= null && this.getSocialProgramView().getSocialProgramId()!= null )
			{
			  
				bean.onLoadList( this.getSocialProgramView().getSocialProgramId() );
			}
			else
			{
				bean.setListLoadedFromDb( "1" );
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onProgramBudgetTabClick()
	{
		final String methodName = "onProgramBudgetTabClick|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			ProgramBudgetDetailsBean bean =  (ProgramBudgetDetailsBean)getBean( "pages$programBudget");
			if ( this.getSocialProgramView()!= null && this.getSocialProgramView().getSocialProgramId()!= null )
			{
			  
				bean.onLoadList( this.getSocialProgramView().getSocialProgramId() );
			}
			else
			{
				bean.setListLoadedFromDb( "1" );
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onProgramSponsorsTabClick()
	{
		final String methodName = "onProgramSponsorsTabClick|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			ProgramSponsorsBean bean =  (ProgramSponsorsBean)getBean( "pages$programSponsors");
			if ( this.getSocialProgramView()!= null && this.getSocialProgramView().getSocialProgramId()!= null )
			{
			  
				bean.onLoadList( this.getSocialProgramView().getSocialProgramId() );
			}
			else
			{
				bean.setListLoadedFromDb( "1" );
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onProgramParticipantTabClick()
	{
		String methodName = "onProgramParticipantTabClick|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			ProgramParticipants bean =  (ProgramParticipants)getBean( "pages$programParticipants");
			if ( this.getSocialProgramView()!= null && this.getSocialProgramView().getSocialProgramId()!= null )
			{
			  
				bean.onLoadList( this.getSocialProgramView().getSocialProgramId() );
			}
			else
			{
				bean.setListLoadedFromDb( "1" );
			}
			bean.showControlForDefaultView();
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		SocialProgramView vo = getSocialProgramView();
		if( hasBasicInfoError( vo) )
		{
			tabPanel.setSelectedTab(TAB_BASIC_INFO);
			return true;
		}
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}
		
		return hasSaveErrors;
		
	}
	private boolean hasBasicInfoError(SocialProgramView vo) throws Exception 
	{
		boolean hasSaveErrors= false;
		if(  vo.getProgramName() == null ||  vo.getProgramName().trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.nameReq"));
			hasSaveErrors = true;
		}
		if(  vo.getProgramType() == null ||  vo.getProgramType().trim().length() <= 0  || vo.getProgramType().trim().equals("-1"))
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.typeReq"));
			hasSaveErrors = true;
		}
		if(  vo.getAssignedEmpId() == null ||  vo.getAssignedEmpId().trim().length() <= 0  )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.empReqq"));
			hasSaveErrors = true;
		}
		if(  vo.getTargetGrp() == null ||  vo.getTargetGrp().trim().length() <= 0  || vo.getTargetGrp().trim().equals("-1"))
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.targerGrpReq"));
			hasSaveErrors = true;
		}
		if( vo.getPeriodFreq() == null  )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.freqReq"));
			hasSaveErrors = true;
		}
		if( vo.getPeriodDuration() == null  || vo.getPeriodDuration().trim().length() <= 0  || vo.getPeriodDuration().trim().equals("-1"))
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.durationReq"));
			hasSaveErrors = true;
		}
		if( vo.getExpectedBudget() == null  )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.expectedBudgetReq"));
			hasSaveErrors = true;
		}
		if( vo.getDescription() == null  )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.descReq"));
			hasSaveErrors = true;
		}
		return hasSaveErrors;
	}
	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		String methodName = "onSave|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			if( !hasSaveErrors() )
			{
				persistSocialProgram();
				saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
				successMessages.add( ResourceUtil.getInstance().getProperty("socialProgram.msg.Saved"));
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	private void persistSocialProgram() throws Exception 
	{
		HashMap<String,Object> map = new HashMap<String,Object>();
		SocialProgramView vo = getSocialProgramView();
		if ( vo.getSocialProgramId() ==  null  )
		{
		 vo.setCreatedBy( getLoggedInUserId() );
		 vo.setUpdatedBy( getLoggedInUserId() );
		 vo.setStatusId( getStatusFromType(WebConstants.SocialProgramStatus.NEW).getDomainDataId() );
		}
		map.put( Constant.SocialProgram.SOCIAL_PROGRAM_VO, vo);
		
		ProgramPurposes purposeBean =  (ProgramPurposes)getBean( "pages$programPurposes" );
		ProgramActionList actionListBean =  (ProgramActionList)getBean( "pages$programActionList" );
		ProgramRequirementBean requirementBean =  (ProgramRequirementBean)getBean( "pages$programRequirement" );
		ProgramBudgetDetailsBean budgetBean =  (ProgramBudgetDetailsBean)getBean( "pages$programBudget" );
		ProgramParticipants participantBean =  (ProgramParticipants)getBean( "pages$programParticipants" );
		ProgramSponsorsBean sponsorsBean =  (ProgramSponsorsBean)getBean( "pages$programSponsors" );
		ProgramRecommendationsBean recommendationBean =  (ProgramRecommendationsBean)getBean( "pages$programRecommendations" );
		ProgramPositiveNegativesBean positiveNegativesBean =  (ProgramPositiveNegativesBean)getBean( "pages$programPositiveNegatives" );
		
		 if( purposeBean.getList()!= null && purposeBean.getList().size() > 0  )
		 {
		     map.put( Constant.SocialProgram.PROGRAM_PURPOSES, purposeBean.getList() );
		 }
		 if( actionListBean.getList()!= null && actionListBean.getList().size() >0  )
		 {
		     map.put( Constant.SocialProgram.PROGRAM_ACTION_LIST, actionListBean.getList() );
		 }
		 if( requirementBean.getList()!= null && requirementBean.getList().size() >0  )
		 {
		     map.put( Constant.SocialProgram.PROGRAM_REQ_LIST, requirementBean.getList() );
		 }
		 if( budgetBean.getList()!= null && budgetBean.getList().size() >0  )
		 {
		     map.put( Constant.SocialProgram.PROGRAM_BUDGET_LIST, budgetBean.getList() );
		 }
		 if( participantBean.getList()!= null && participantBean.getList().size() >0  )
		 {
		     map.put( Constant.SocialProgram.PROGRAM_PARTICIPANTS, participantBean.getList() );
		 }
		 if( sponsorsBean.getList()!= null && sponsorsBean.getList().size() >0  )
		 {
		     map.put( Constant.SocialProgram.PROGRAM_SPONSORSS, sponsorsBean.getList() );
		 }
		 if( recommendationBean.getList()!= null && recommendationBean.getList().size() >0  )
		 {
		     map.put( Constant.SocialProgram.PROGRAM_RECOMMENDATIONS, recommendationBean.getList() );
		 }
		 if( positiveNegativesBean.getList()!= null && positiveNegativesBean.getList().size() >0  )
		 {
		     map.put( Constant.SocialProgram.PROGRAM_POSITIVE_NEGATIVES, positiveNegativesBean.getList() );
		 }
		 socialProgramService.persistSocialProgram( map );
	}
	@SuppressWarnings( "unchecked" )
	public void onSubmit()
	{
		String methodName = "onSubmit|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			if( getSocialProgramView() != null )
	   		{
				HashMap<String,Object> map = new HashMap<String,Object>();
				SocialProgramView vo = getSocialProgramView();
				if ( vo.getSocialProgramId() !=  null  )
				{
					 SystemParameters parameters = SystemParameters.getInstance();
					 String endPoint= parameters.getParameter( "MEMSSocialSolidarityBPEL" );
//			         MEMSSocialSolidatoryBPELPortClient  port=new MEMSSocialSolidatoryBPELPortClient();
//			   	     port.setEndpoint(endPoint);
//			   	     logger.logInfo(methodName+"|"+port.getEndpoint().toString()) ;
					 vo.setCreatedBy( getLoggedInUserId() );
					 vo.setUpdatedBy( getLoggedInUserId() );
					 DomainDataView   ddApprovalRequired =  getStatusFromType(WebConstants.SocialProgramStatus.APPROVAL_REQUIRED); 
					 vo.setStatusId(  ddApprovalRequired.getDomainDataId() );
					 vo.setStatusAr(  ddApprovalRequired.getDataDescAr() );
					 vo.setStatusEn(  ddApprovalRequired.getDataDescEn() );
					 vo.setStatusKey( ddApprovalRequired.getDataValue() );
					 map.put( Constant.SocialProgram.SOCIAL_PROGRAM_VO, vo);
					 socialProgramService.persistSocialProgram( map );
//				   	 port.initiate( socialProgramView.getSocialProgramId(), CommonUtil.getLoggedInUser(), null, null);
				   	successMessages.add( ResourceUtil.getInstance().getProperty("socialProgram.msg.Submitted"));
				   	
				   	saveCommentsAttachment( "" );
					saveSystemComments(MessageConstants.RequestEvents.REQUEST_SUBMIT);
				}
			
			 
		   	this.setPageMode( PAGE_MODE_APPROVE );
	   		}  	
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		 }
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onApprove()
	{
		String methodName = "onApprove|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			if( getSocialProgramView() != null )
	   		{
				HashMap<String,Object> map = new HashMap<String,Object>();
				SocialProgramView vo = getSocialProgramView();
				if ( vo.getSocialProgramId() !=  null  )
				{
					 vo.setCreatedBy( getLoggedInUserId() );
					 vo.setUpdatedBy( getLoggedInUserId() );
					 DomainDataView   ddApprovalRequired =  getStatusFromType(WebConstants.SocialProgramStatus.APPROVED); 
					 vo.setStatusId(  ddApprovalRequired.getDomainDataId() );
					 vo.setStatusAr(  ddApprovalRequired.getDataDescAr() );
					 vo.setStatusEn(  ddApprovalRequired.getDataDescEn() );
					 vo.setStatusKey( ddApprovalRequired.getDataValue() );
					 map.put( Constant.SocialProgram.SOCIAL_PROGRAM_VO, vo);
					 persistSocialProgram();
					 //socialProgramService.persistSocialProgram( map );
					 setTaskOutCome( TaskOutcome.APPROVE );
					 successMessages.add( ResourceUtil.getInstance().getProperty("socialProgram.msg.Approved"));
					 saveCommentsAttachment( "" );
					saveSystemComments(MessageConstants.RequestEvents.REQUEST_APPROVED);
					 
				}
			
			 
		   	this.setPageMode( PAGE_MODE_COMPLETE );
	   		}  	
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		 }
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onReject()
	{
		String methodName = "onReject|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			if( getSocialProgramView() != null )
	   		{
				HashMap<String,Object> map = new HashMap<String,Object>();
				SocialProgramView vo = getSocialProgramView();
				if ( vo.getSocialProgramId() !=  null  )
				{
					 vo.setCreatedBy( getLoggedInUserId() );
					 vo.setUpdatedBy( getLoggedInUserId() );
					 DomainDataView   ddApprovalRequired =  getStatusFromType(WebConstants.SocialProgramStatus.REJECT ); 
					 vo.setStatusId(  ddApprovalRequired.getDomainDataId() );
					 vo.setStatusAr(  ddApprovalRequired.getDataDescAr() );
					 vo.setStatusEn(  ddApprovalRequired.getDataDescEn() );
					 vo.setStatusKey( ddApprovalRequired.getDataValue() );
					 map.put( Constant.SocialProgram.SOCIAL_PROGRAM_VO, vo);
					 socialProgramService.persistSocialProgram( map );
					 setTaskOutCome( TaskOutcome.REJECT );
					 successMessages.add( ResourceUtil.getInstance().getProperty("socialProgram.msg.Reject"));
					 
					 saveCommentsAttachment( "" );
					saveSystemComments(MessageConstants.RequestEvents.REQUEST_REJECTED);
				}
			
			 
		   	this.setPageMode( PAGE_MODE_COMPLETE );
	   		}  	
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		 }
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	private boolean hasOnCompleteErrors() throws Exception
	{
		return true;
		
	}
	@SuppressWarnings( "unchecked" )
	public void onComplete()
	{
		String methodName = "onComplete|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			if( getSocialProgramView() != null )
	   		{
				HashMap<String,Object> map = new HashMap<String,Object>();
				SocialProgramView vo = getSocialProgramView();
				if ( vo.getSocialProgramId() !=  null  )
				{
					 vo.setCreatedBy( getLoggedInUserId() );
					 vo.setUpdatedBy( getLoggedInUserId() );
					 DomainDataView   ddStatus =  getStatusFromType(WebConstants.SocialProgramStatus.COMPLETE ); 
					 vo.setStatusId(  ddStatus.getDomainDataId() );
					 vo.setStatusAr(  ddStatus.getDataDescAr() );
					 vo.setStatusEn(  ddStatus.getDataDescEn() );
					 vo.setStatusKey( ddStatus.getDataValue() );
					 map.put( Constant.SocialProgram.SOCIAL_PROGRAM_VO, vo);
					 persistSocialProgram();
					 //socialProgramService.persistSocialProgram( map );
					 setTaskOutCome(TaskOutcome.OK);
					 successMessages.add( ResourceUtil.getInstance().getProperty("socialProgram.msg.Complete"));
					 
					 saveCommentsAttachment( "" );
					 saveSystemComments(MessageConstants.RequestEvents.REQUEST_COMPLETED);
				}
			
			 
		   	this.setPageMode( PAGE_MODE_VIEW );
	   		}  	
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		 }
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
    	String methodName="setTaskOutCome|";
    	logger.logInfo(methodName+"|"+" Start...");
    	try
    	{
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
//	    	UserTask userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
//			String loggedInUser=getLoggedInUserId();
//			logger.logInfo(methodName+" TaskId:%s| TaskOutCome:%s| loggedInUser:%s",userTask.getTaskId(),taskOutCome,loggedInUser);
//			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
//			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
//			logger.logInfo(methodName+"|"+" Finish...");
    	}
//    	catch(PIMSWorkListException ex)
//    	{
//    		
//    		logger.LogException(methodName+"|"+" Error Occured...",ex);
//    		throw ex;
//    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+" Error Occured...",ex);
    		throw ex;
    	}
    }
	
	
	@SuppressWarnings( "unchecked" )
	 public DomainDataView getStatusFromType( String statusType)
	 {
		String viewKey = "Status"+statusType;
		 DomainDataView ddv;
		 if ( viewMap.get( viewKey ) != null )
		 {
			 ddv = (DomainDataView)viewMap.get( viewKey );
		 }
		 else
		 {
			 
			 ddv = CommonUtil.getIdFromType( this.getStatusList(), statusType);
			 viewMap.put( viewKey,ddv);
		 
		 } 
	 
	    return ddv;
	 }

	@SuppressWarnings( "unchecked" )
	 public List<DomainDataView> getStatusList()
	 {
		 List<DomainDataView >ddvList;
		 if ( viewMap.get( "StatusList" ) != null )
		 {
			 ddvList = (ArrayList<DomainDataView>)viewMap.get( "StatusList" );
		 }
		 else
		 {
			 
			 ddvList = CommonUtil.getDomainDataListForDomainType( WebConstants.SocialProgramStatus.KEY );
			 viewMap.put( "StatusList",ddvList );
		 
		 } 
	 
	    return ddvList;
	 }

	


	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask)
	{
		
	
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
		

	}


public String getPageTitle() {
	
	this.setPageTitle(ResourceUtil.getInstance().getProperty("socialProgram.title"));
	return pageTitle;
}



public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
}
	
public String getErrorMessages()
{

	return CommonUtil.getErrorMessages(errorMessages);
}
public String getSuccessMessages()
{
	String messageList="";
	if ((successMessages== null) || (successMessages.size() == 0)) 
	{
		messageList = "";
	}
	else
	{
		
		for (String message : successMessages) 
			{
				messageList +=  "<LI>" +message+ "<br></br>" ;
		    }
		
	}
	return (messageList);
}


@SuppressWarnings( "unchecked" )
public String getPageMode() {
	if( viewMap.get("pageMode")!= null )
		pageMode = viewMap.get("pageMode").toString();
	return pageMode;
}


@SuppressWarnings( "unchecked" )
public void setPageMode(String pageMode) {
	
	this.pageMode = pageMode;
	if( this.pageMode != null )
		viewMap.put( "pageMode", this.pageMode );
}

public HtmlCommandButton getBtnApprove() {
	return btnApprove;
}



public void setBtnApprove(HtmlCommandButton btnApprove) {
	this.btnApprove = btnApprove;
}



public HtmlCommandButton getBtnReject() {
	return btnReject;
}



public void setBtnReject(HtmlCommandButton btnReject) {
	this.btnReject = btnReject;
}

public HtmlCommandButton getBtnComplete() {
	return btnComplete;
}

public void setBtnComplete(HtmlCommandButton btnComplete) {
	this.btnComplete = btnComplete;
}

public HtmlCommandButton getBtnSave() {
	return btnSave;
}



public void setBtnSave(HtmlCommandButton btnSave) {
	this.btnSave = btnSave;
}



public HtmlCommandButton getBtnSubmit() {
	return btnSubmit;
}



public void setBtnSubmit(HtmlCommandButton btnSubmit) {
	this.btnSubmit = btnSubmit;
}
public HtmlTabPanel getTabPanel() {
	return tabPanel;
}
public void setTabPanel(HtmlTabPanel tabPanel) {
	this.tabPanel = tabPanel;
}

@SuppressWarnings( "unchecked" )
public SocialProgramView getSocialProgramView() {
	
	if( viewMap.get( "SocialProgramView" ) != null )
	{
		socialProgramView = ( SocialProgramView )viewMap.get( "SocialProgramView" ) ;
	}
	return socialProgramView;
}
@SuppressWarnings( "unchecked" )
public void setSocialProgramView(SocialProgramView socialProgramView) {
	this.socialProgramView = socialProgramView;
	if( this.socialProgramView  != null )
	{
		viewMap.put( "SocialProgramView", this.socialProgramView ) ;
		
	}
}

public void requestHistoryTabClick()
{
	String methodName="requestHistoryTabClick";
	logger.logInfo(methodName+"|"+"Start..");
		
	if(getSocialProgramView().getSocialProgramId()!=null)
	try	
	{
	  RequestHistoryController rhc=new RequestHistoryController();
	  
	  Long socialProgramId= getSocialProgramView().getSocialProgramId();
	  if(socialProgramId!=null )
	    rhc.getAllRequestTasksForRequest(WebConstants.SocialSolidarityProgram.NOTES_OWNER,socialProgramId.toString());
	}
	catch(Exception ex)
	{
		logger.LogException(methodName+"|"+"Error Occured..",ex);
		errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
	}
	logger.logInfo(methodName+"|"+"Finish..");
}	








}