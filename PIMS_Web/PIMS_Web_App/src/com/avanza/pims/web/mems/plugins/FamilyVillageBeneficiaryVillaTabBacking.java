package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.FamilyVillageFileService;
import com.avanza.pims.ws.vo.InheritanceBeneficiaryView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageBeneficiaryVillaTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	
	private FamilyVillageFileService  service;
	private String tabMode;
	InheritanceBeneficiaryView pageView;
	private Long personId;
	public FamilyVillageBeneficiaryVillaTabBacking() 
	{
		service = new FamilyVillageFileService();
		tabMode          = Constant.EndowmentFile.MODE_READ_ONLY;
		pageView         = new InheritanceBeneficiaryView();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.VILLA_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.VILLA_TAB_SUCCESS, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			tabMode = (String) viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		
		if( viewMap.get( "InheritanceBeneficiaryView" ) != null )
		{
			pageView = (InheritanceBeneficiaryView)viewMap.get( "InheritanceBeneficiaryView" );
		}
		if( viewMap.get("recommendationPersonId")!= null )
		{
			personId = Long.valueOf( viewMap.get("recommendationPersonId" ).toString() );
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, tabMode );
		}
		if( pageView != null )
		{
			
			viewMap.put( "InheritanceBeneficiaryView", pageView );
		}
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
	}	
	
	

	public void populateTab( PersonView personView ) throws Exception
	{
		updateValuesFromMaps();
		personId = personView.getPersonId();
		List<InheritanceBeneficiaryView> list =  FamilyVillageFileService.getInheritanceBeneficiariesByPersonId(personId);
		pageView = list.get(0);
		updateValuesToMaps();
	}
	

	
	@SuppressWarnings( "unchecked" )
	public void onOpenFile()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			try
			{
				
			}
			catch(Exception e){throw e;}
		}
		catch ( Exception e )
		{
			logger.LogException( "onOpenFile--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.VILLA_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.VILLA_TAB_SUCCESS, successMessages );
			}
		}
	}
	
	
  
	
		
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}


	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public InheritanceBeneficiaryView getPageView() {
		return pageView;
	}

	public void setPageView(InheritanceBeneficiaryView pageView) {
		this.pageView = pageView;
	}

	


}
