package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.FamilyVillageFamilyAspectService;
import com.avanza.pims.ws.mems.FamilyVillageHealthAspectService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.HealthAspectsView;
import com.avanza.pims.ws.vo.mems.MedicalHistoryView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageHealthAspectTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	 
	private List<HealthAspectsView> dataListHealthAspectsChronic;
	private List<HealthAspectsView> dataListHealthAspectsAcute;
	private List<MedicalHistoryView> dataListMedicalHistory;
	private HtmlDataTable dataTableHealthAspectsChronic;
	private HtmlDataTable dataTableHealthAspectsAcute;
	private HtmlDataTable dataTableMedicalHistory;
	FamilyVillageFamilyAspectService service;
	
	private String tabMode;
	private Long personId;
	private Integer recordSizeHealthAspectsChronic;
	private Integer recordSizeHealthAspectsAcute;
	private Integer recordSizeMedicalHistory;
	public FamilyVillageHealthAspectTabBacking() 
	{
		service = new FamilyVillageFamilyAspectService();
		dataTableHealthAspectsChronic             =  new HtmlDataTable();
		dataTableHealthAspectsAcute          =  new HtmlDataTable();
		dataTableMedicalHistory         =  new HtmlDataTable();
		dataListHealthAspectsChronic		        =  new ArrayList<HealthAspectsView>();
		dataListHealthAspectsAcute    		=  new ArrayList<HealthAspectsView>();
		dataListMedicalHistory 			=  new ArrayList<MedicalHistoryView>();
		recordSizeMedicalHistory  		=  0;
		recordSizeHealthAspectsAcute  		=  0;
		recordSizeHealthAspectsChronic  		=  0;
		tabMode          = Constant.EndowmentFile.MODE_READ_ONLY;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			tabMode = (String) viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		if( viewMap.get( "dataListHealthAspectsAcute" ) != null )
		{
			dataListHealthAspectsAcute = (ArrayList<HealthAspectsView>)viewMap.get( "dataListHealthAspectsAcute" );
		}
		if( viewMap.get( "recordSizeHealthAspectsAcute" ) != null )
		{
			recordSizeHealthAspectsAcute = Integer.parseInt( viewMap.get( "recordSizeHealthAspectsAcute" ).toString() );
		}
				
		if( viewMap.get( "dataListHealthAspectsChronic" ) != null )
		{
			dataListHealthAspectsChronic = (ArrayList<HealthAspectsView>)viewMap.get( "dataListHealthAspectsChronic" );
		}
		if( viewMap.get( "recordSizeHealthAspectsChronic" ) != null )
		{
			recordSizeHealthAspectsChronic = Integer.parseInt( viewMap.get( "recordSizeHealthAspectsChronic" ).toString() );
		}
		
		if( viewMap.get( "dataListMedicalHistory" ) != null )
		{
			dataListMedicalHistory = (ArrayList<MedicalHistoryView>)viewMap.get( "dataListMedicalHistory" );
		}
		if( viewMap.get( "recordSizeMedicalHistory" ) != null )
		{
			recordSizeMedicalHistory = Integer.parseInt( viewMap.get( "recordSizeMedicalHistory" ).toString() );
		}
		
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, tabMode );
		}
		if(recordSizeMedicalHistory !=null)
		{
			viewMap.put("recordSizeMedicalHistory",recordSizeMedicalHistory);
		}
		if(recordSizeHealthAspectsAcute !=null)
		{
			viewMap.put("recordSizeHealthAspectsAcute",recordSizeHealthAspectsAcute);
		}
		if(recordSizeHealthAspectsChronic !=null)
		{
			viewMap.put("recordSizeHealthAspectsChronic",recordSizeHealthAspectsChronic);
		}
		if( dataListHealthAspectsAcute != null && dataListHealthAspectsAcute.size()  > 0  )
		{
			viewMap.put( "dataListHealthAspectsAcute", dataListHealthAspectsAcute );
		}
		if( dataListHealthAspectsChronic!= null && dataListHealthAspectsChronic.size()  > 0  )
		{
			viewMap.put( "dataListHealthAspectsChronic", dataListHealthAspectsChronic );
		}
		if( dataListMedicalHistory != null && dataListMedicalHistory.size()  > 0  )
		{
			viewMap.put( "dataListMedicalHistory", dataListMedicalHistory );
		}
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
	}	
	
	

	public void populateTab( PersonView personView ) throws Exception
	{
		updateValuesFromMaps();
		personId = personView.getPersonId();
		setPageDataDefaults();
		loadDataListHealthAspectsAcute(personId);
		loadDataListHealthAspectsChronic(personId);
		loadDataListMedicalHistory(personId);
		
		updateValuesToMaps();
	}
	
	private void setPageDataDefaults()throws Exception
	{
		PersonView person  = new PersonView();
		person.setPersonId(personId);
	}

	private void loadDataListHealthAspectsAcute(Long personId) throws Exception {
		
		dataListHealthAspectsAcute= FamilyVillageHealthAspectService.getDiseasesByCategory( personId, Constant.ACUTE_DISEASE );
		recordSizeHealthAspectsAcute= dataListHealthAspectsAcute.size();
	}
   private void loadDataListHealthAspectsChronic(Long personId) throws Exception {
		
		dataListHealthAspectsChronic= FamilyVillageHealthAspectService.getDiseasesByCategory( personId, Constant.CHRONIC_DISEASE );
		recordSizeHealthAspectsChronic= dataListHealthAspectsChronic.size();
	}
    private void loadDataListMedicalHistory(Long personId) throws Exception {
		
		dataListMedicalHistory = FamilyVillageHealthAspectService.getMedicalHistory( personId  );
		recordSizeMedicalHistory = dataListMedicalHistory.size();
	}
	@SuppressWarnings( "unchecked" )
	public void onDeleteHealthAspectChronic()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteHealthAspectChronic--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
	}
  
	@SuppressWarnings( "unchecked" )
	public void onDeleteHealthAspectAcute()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteHealthAspectAcute--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDeleteMedicalHistory()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteMedicalHistory--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
	}
	
	
	
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}

	public List<HealthAspectsView> getDataListHealthAspectsChronic() {
		return dataListHealthAspectsChronic;
	}

	public void setDataListHealthAspectsChronic(
			List<HealthAspectsView> dataListHealthAspectsChronic) {
		this.dataListHealthAspectsChronic = dataListHealthAspectsChronic;
	}

	public List<HealthAspectsView> getDataListHealthAspectsAcute() {
		return dataListHealthAspectsAcute;
	}

	public void setDataListHealthAspectsAcute(
			List<HealthAspectsView> dataListHealthAspectsAcute) {
		this.dataListHealthAspectsAcute = dataListHealthAspectsAcute;
	}

	public List<MedicalHistoryView> getDataListMedicalHistory() {
		return dataListMedicalHistory;
	}

	public void setDataListMedicalHistory(
			List<MedicalHistoryView> dataListMedicalHistory) {
		this.dataListMedicalHistory = dataListMedicalHistory;
	}

	public HtmlDataTable getDataTableHealthAspectsChronic() {
		return dataTableHealthAspectsChronic;
	}

	public void setDataTableHealthAspectsChronic(
			HtmlDataTable dataTableHealthAspectsChronic) {
		this.dataTableHealthAspectsChronic = dataTableHealthAspectsChronic;
	}

	public HtmlDataTable getDataTableHealthAspectsAcute() {
		return dataTableHealthAspectsAcute;
	}

	public void setDataTableHealthAspectsAcute(
			HtmlDataTable dataTableHealthAspectsAcute) {
		this.dataTableHealthAspectsAcute = dataTableHealthAspectsAcute;
	}

	public HtmlDataTable getDataTableMedicalHistory() {
		return dataTableMedicalHistory;
	}

	public void setDataTableMedicalHistory(HtmlDataTable dataTableMedicalHistory) {
		this.dataTableMedicalHistory = dataTableMedicalHistory;
	}

	public Integer getRecordSizeHealthAspectsChronic() {
		return recordSizeHealthAspectsChronic;
	}

	public void setRecordSizeHealthAspectsChronic(
			Integer recordSizeHealthAspectsChronic) {
		this.recordSizeHealthAspectsChronic = recordSizeHealthAspectsChronic;
	}

	public Integer getRecordSizeHealthAspectsAcute() {
		return recordSizeHealthAspectsAcute;
	}

	public void setRecordSizeHealthAspectsAcute(Integer recordSizeHealthAspectsAcute) {
		this.recordSizeHealthAspectsAcute = recordSizeHealthAspectsAcute;
	}

	public Integer getRecordSizeMedicalHistory() {
		return recordSizeMedicalHistory;
	}

	public void setRecordSizeMedicalHistory(Integer recordSizeMedicalHistory) {
		this.recordSizeMedicalHistory = recordSizeMedicalHistory;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}



}
