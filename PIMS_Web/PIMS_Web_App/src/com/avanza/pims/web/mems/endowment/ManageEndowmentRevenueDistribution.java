package com.avanza.pims.web.mems.endowment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.EndowmentExpense;
import com.avanza.pims.entity.PropertyRevenueDistributionSearchView;
import com.avanza.pims.entity.PropertyRevenueDistributionView;
import com.avanza.pims.entity.Request;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.RevenueDistributionDetailsService;
import com.avanza.pims.ws.mems.endowment.AmafDistributionPercentageService;
import com.avanza.pims.ws.mems.endowment.EndowmentExpenseService;
import com.avanza.pims.ws.mems.endowment.PropertyDistributionService;
import com.avanza.ui.util.ResourceUtil;

public class ManageEndowmentRevenueDistribution extends AbstractSearchBean 
{
	private static final long serialVersionUID = 1L;
	private final String DEFAULT_SORT_FIELD = "unitNumber";
	private final String DATA_LIST = "DATA_LIST";
	PropertyDistributionService service = new PropertyDistributionService();
	PropertyRevenueDistributionSearchView pageObject = new PropertyRevenueDistributionSearchView();
	List<PropertyRevenueDistributionView> dataList = new ArrayList<PropertyRevenueDistributionView>();
	List<EndowmentExpense> dataListExpenses = new ArrayList<EndowmentExpense>();
	HtmlDataTable dataTableExpenses;
	private Integer expensesRecordsSize;
	private Double totalExpenseAmount;
	private Double totalAvailableRevenueAfterExpenseDeduction;
	private Double totalRebuildRevenue;
	private Double totalRevenueToDistribute;
	private Double totalFeesToBeDeductedAfterExpenseDeduction;
	private Double totalAvailableRevenueAfterFeesDeduction;
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	
	@SuppressWarnings("unchecked")
	@Override
	public void init() 
	{
		super.init();
		try 
		{
			if (!isPostBack()) 
			{
				initData();
			}
			else
			{
				updateValuesFromMap();
			}
		} 
		catch (Exception es) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		setPagingVariables();
		updateValuesFromMap();
	}

	@SuppressWarnings( "unchecked" )
	private void setPagingVariables()  throws Exception
	{
		setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
		setSortField(DEFAULT_SORT_FIELD);
		setSortItemListAscending(false);
	}

	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if(sessionMap.get(WebConstants.SELECTED_ROW) != null)
		{
			pageObject =  (PropertyRevenueDistributionSearchView)sessionMap.remove(WebConstants.SELECTED_ROW);
			loadExpenseDataList();
			pageFirst();
		}
		else if( viewMap.get("PropertyRevenueDistributionView") != null )
		{
			pageObject =  (PropertyRevenueDistributionSearchView)( viewMap.get("PropertyRevenueDistributionView") ); 
		}
		if(viewMap.get(DATA_LIST) != null)
		{
			dataList = (List<PropertyRevenueDistributionView>) viewMap.get(DATA_LIST);
		}
		if(viewMap.get("dataListExpenses") != null)
		{
			dataListExpenses = (List<EndowmentExpense>) viewMap.get("dataListExpenses");
		}		
		if( viewMap.get("expensesRecordsSize") != null )
		{
			expensesRecordsSize = Integer.valueOf( viewMap.get("expensesRecordsSize").toString() );
		}
		if( viewMap.get("totalExpenseAmount") != null )
		{
			totalExpenseAmount = Double.valueOf( viewMap.get("totalExpenseAmount").toString() );
		}
		if( viewMap.get("totalAvailableRevenueAfterExpenseDeduction") != null )
		{
			totalAvailableRevenueAfterExpenseDeduction = Double.valueOf( viewMap.get("totalAvailableRevenueAfterExpenseDeduction").toString() );
		}
		if( viewMap.get("totalRevenueToDistribute") != null )
		{
			totalRevenueToDistribute = Double.valueOf( viewMap.get("totalRevenueToDistribute").toString() );
		}
		if( viewMap.get("totalRebuildRevenue") != null )
		{
			totalRebuildRevenue = Double.valueOf( viewMap.get("totalRebuildRevenue").toString() );
		}
		if( viewMap.get("totalFeesToBeDeductedAfterExpenseDeduction") != null )
		{
			totalFeesToBeDeductedAfterExpenseDeduction = Double.valueOf( viewMap.get("totalFeesToBeDeductedAfterExpenseDeduction").toString() );
		}
		if( viewMap.get("totalAvailableRevenueAfterFeesDeduction") != null )
		{
			totalAvailableRevenueAfterFeesDeduction = Double.valueOf( viewMap.get("totalAvailableRevenueAfterFeesDeduction").toString() );
		}
		
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if(this.pageObject != null )
		{
			viewMap.put("PropertyRevenueDistributionView", this.pageObject );
		}
		if(this.dataList != null )
		{
			viewMap.put(DATA_LIST, this.dataList);
		}
		if( this.dataListExpenses != null )
		{
			viewMap.put("dataListExpenses",dataListExpenses);
		}
		if( expensesRecordsSize != null )
		{
			viewMap.put("expensesRecordsSize", expensesRecordsSize );
		}
		if( totalExpenseAmount  != null )
		{
			viewMap.put("totalExpenseAmount" ,totalExpenseAmount );
		}
		doCalculations();
		viewMap.put("totalAvailableRevenue" ,totalAvailableRevenueAfterExpenseDeduction );
		viewMap.put("totalRebuildRevenue" ,totalRebuildRevenue );
		viewMap.put("totalRevenueToDistribute" ,totalRevenueToDistribute );
		viewMap.put("totalFeesToBeDeductedAfterExpenseDeduction" ,totalFeesToBeDeductedAfterExpenseDeduction );
		viewMap.put("totalAvailableRevenueAfterFeesDeduction" ,totalAvailableRevenueAfterFeesDeduction );
	}

	@SuppressWarnings("unchecked")
	private void doCalculations() throws Exception 
	{
		totalAvailableRevenueAfterExpenseDeduction  = 0.0d;
		totalRebuildRevenue   	   					= 0.0d;
		totalRevenueToDistribute   					= 0.0d;
		totalFeesToBeDeductedAfterExpenseDeduction  = 0.0d;
		totalAvailableRevenueAfterFeesDeduction     = 0.0d;
		
		if(
			pageObject.getAmountToDistribute().compareTo(0d)>0 
		  )
		{

			DecimalFormat oneDForm = new DecimalFormat( "#.00" );
			if ( pageObject.getAmountToDistribute().compareTo( totalExpenseAmount )> 0 )
			{
				totalAvailableRevenueAfterExpenseDeduction = Double.valueOf( oneDForm.format( pageObject.getAmountToDistribute() - totalExpenseAmount ) );
				totalAvailableRevenueAfterFeesDeduction  = totalAvailableRevenueAfterExpenseDeduction;  
				totalFeesToBeDeductedAfterExpenseDeduction = AmafDistributionPercentageService.getTotalFeesAmountToBeDeductedAfterExpenseDeduction(totalAvailableRevenueAfterExpenseDeduction);
				totalAvailableRevenueAfterFeesDeduction    = Double.valueOf( oneDForm.format( totalAvailableRevenueAfterExpenseDeduction - totalFeesToBeDeductedAfterExpenseDeduction ) ) ;
				totalRebuildRevenue   = Double.valueOf( oneDForm.format( totalAvailableRevenueAfterFeesDeduction * 0.40d ) );
				totalRevenueToDistribute   = Double.valueOf( oneDForm.format( totalAvailableRevenueAfterFeesDeduction * 0.60d ) );
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void onDistributeAll() 
	{
		try 
		{
			updateValuesFromMap();
			RevenueDistributionDetailsService rddService = new RevenueDistributionDetailsService();
			Request request  = null;
//			rddService.distributeFromPropertyRevenueDistributionSearchView(request, pageObject, CommonUtil.getLoggedInUser() );
			pageFirst();
			updateValuesToMap();
		} 
		catch (Exception e) 
		{
			logger.LogException("onDistribute |Error Occured", e);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public void onDistributeSingle() 
	{
		try 
		{
			updateValuesFromMap();
			PropertyRevenueDistributionView row = (PropertyRevenueDistributionView)getDataTable().getRowData();
			RevenueDistributionDetailsService rddService = new RevenueDistributionDetailsService();
			Request request  = null;
			distributeFromPropertyRevenueDistributionViewInTransaction(row,rddService, request);
			row.setDistributedAmount( row.getAmountToDistribute() );
			updateValuesToMap();
		} 
		catch (Exception e) 
		{
			logger.LogException("onDistributeSingle |Error Occured", e);
			errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
		}
		
	}

	/**
	 * @param row
	 * @param rddService
	 * @param request
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void distributeFromPropertyRevenueDistributionViewInTransaction(
																			PropertyRevenueDistributionView row,
																			RevenueDistributionDetailsService rddService, 
																			Request request
																		   )throws Exception 
	{
		try 
		{
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			rddService.distributeFromPropertyRevenueDistributionView(request, row , CommonUtil.getLoggedInUser() );
			ApplicationContext.getContext().getTxnContext().commit();
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}


	public void onExpensesTab()
	{
		try 
		{
			updateValuesFromMap();
			loadExpenseDataList();
			updateValuesToMap();
				
		}
		catch (Exception e)
		{
		    	errorMessages = new ArrayList<String>(0);
		 		logger.LogException("onExpensesTab|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void loadExpenseDataList() throws Exception 
	{
		EndowmentExpenseService expenseService = new EndowmentExpenseService();
		EndowmentExpense criteria = new EndowmentExpense();
		Endowment endowment = new Endowment();
		endowment.setEndowmentId( pageObject.getEndowmentId() );
		criteria.setEndowment(endowment);
		expensesRecordsSize =  expenseService.getTotalNumberOfRecords(criteria);
		totalExpenseAmount = expenseService.getTotalAmountOfExpense(criteria);
		dataListExpenses    =  expenseService.search(criteria, null, null, "expenseId", false);
	}
	public void onSearch() 
	{
		try 
		{
			pageFirst();
		} 
		catch (Exception e) 
		{
			logger.LogException("onSearch |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void doSearchItemList()
	{
		try {
				loadDataList();
			}
		    catch (Exception e){
		    	errorMessages = new ArrayList<String>(0);
		 		logger.LogException("doSearchItemList|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		    }
	}

	@SuppressWarnings("unchecked")
	public void loadDataList() throws Exception 
	{
		int totalRows = 0;
		updateValuesFromMap();
		PropertyRevenueDistributionView criteria = new PropertyRevenueDistributionView();
		criteria.setPropertyId( pageObject.getPropertyId() );
		totalRows = service.getTotalNumberOfRecordsForRevenueDetail(criteria );
		setTotalRows(totalRows);
		doPagingComputations();
		
		dataList = service.searchForRevenueDetail(
													criteria, 
													getRowsPerPage(),
													getCurrentPage(),
													getSortField(),
													isSortItemListAscending()
												 );

		if (dataList== null || dataList.isEmpty() ) 
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			forPaging(0);
		}
		this.setDataList( dataList );
		forPaging(getTotalRows());

		updateValuesToMap();
	}

	@SuppressWarnings("unchecked")
	public List<PropertyRevenueDistributionView> getDataList() 
	{
		 
		return dataList ;
	}

	@SuppressWarnings("unchecked")
	public void setDataList(List<PropertyRevenueDistributionView> list) 
	{
		this.dataList = list;
		

	}
	
	@SuppressWarnings("unchecked")
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javascript);
	}
	
	public PropertyRevenueDistributionSearchView getPageObject() {
		return pageObject;
	}

	public void setPageObject(PropertyRevenueDistributionSearchView pageObject) {
		this.pageObject = pageObject;
	}

	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}

	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public HtmlDataTable getDataTableExpenses() {
		return dataTableExpenses;
	}

	public void setDataTableExpenses(HtmlDataTable dataTableExpenses) {
		this.dataTableExpenses = dataTableExpenses;
	}

	public List<EndowmentExpense> getDataListExpenses() {
		return dataListExpenses;
	}

	public void setDataListExpenses(List<EndowmentExpense> dataListExpenses) {
		this.dataListExpenses = dataListExpenses;
	}

	public int getExpensesRecordsSize() {
		return expensesRecordsSize;
	}

	public void setExpensesRecordsSize(int expensesRecordsSize) {
		this.expensesRecordsSize = expensesRecordsSize;
	}

	public Double getTotalExpenseAmount() {
		return totalExpenseAmount;
	}

	public void setTotalExpenseAmount(Double totalExpenseAmount) {
		this.totalExpenseAmount = totalExpenseAmount;
	}

	public Double getTotalAvailableRevenueAfterExpenseDeduction() {
		return totalAvailableRevenueAfterExpenseDeduction;
	}

	public void setTotalAvailableRevenueAfterExpenseDeduction(Double totalAvailableRevenueAfterExpenseDeduction) {
		this.totalAvailableRevenueAfterExpenseDeduction = totalAvailableRevenueAfterExpenseDeduction;
	}

	public Double getTotalRebuildRevenue() {
		return totalRebuildRevenue;
	}

	public void setTotalRebuildRevenue(Double totalRebuildRevenue) {
		this.totalRebuildRevenue = totalRebuildRevenue;
	}

	public Double getTotalRevenueToDistribute() {
		return totalRevenueToDistribute;
	}

	public void setTotalRevenueToDistribute(Double totalRevenueToDistribute) {
		this.totalRevenueToDistribute = totalRevenueToDistribute;
	}

	public Double getTotalFeesToBeDeductedAfterExpenseDeduction() {
		return totalFeesToBeDeductedAfterExpenseDeduction;
	}

	public void setTotalFeesToBeDeductedAfterExpenseDeduction(
			Double totalFeesToBeDeductedAfterExpenseDeduction) {
		this.totalFeesToBeDeductedAfterExpenseDeduction = totalFeesToBeDeductedAfterExpenseDeduction;
	}

	public Double getTotalAvailableRevenueAfterFeesDeduction() {
		return totalAvailableRevenueAfterFeesDeduction;
	}

	public void setTotalAvailableRevenueAfterFeesDeduction(
			Double totalAvailableRevenueAfterFeesDeduction) {
		this.totalAvailableRevenueAfterFeesDeduction = totalAvailableRevenueAfterFeesDeduction;
	}
	
}

