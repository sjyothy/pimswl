package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationBean;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.InheritedAssetSharesView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.TakharujDetailView;
import com.avanza.pims.ws.vo.mems.TakharujEffectView;
import com.avanza.pims.ws.vo.mems.TakharujInheritedAssetView;

public class TakharujEffectBacking extends AbstractMemsBean {
	
	private static final long serialVersionUID = 6740905670990704192L;
	
	private TakharujInheritedAssetView takharujInheritedAssetView;
	private List<TakharujDetailView> takharujDetailViewList;
	private List<TakharujEffectView> takharujEffectViewList;
	private Map<String, InheritedAssetSharesView> inheritedAssetSharesViewMap;
	private Map<Long, TakharujEffectView> takharujDetailView;
	private String selectedFromInheritedAssetSharesId;
	private String selectedFromShare;
	private PersonView toPersonView;
	private String selectedToInheritedAssetSharesId;
	private String selectedToShare;
	private String takharujShare;
	private Boolean popModeView;
	private HtmlDataTable takharujDetailViewDataTable;
	private List<SelectItem> beneficiarySelectItems;
	
	private PropertyServiceAgent propertyServiceAgent;
	
	public TakharujEffectBacking() {		
		
		takharujInheritedAssetView = new TakharujInheritedAssetView();
		takharujDetailViewList = new ArrayList<TakharujDetailView>(0);
		selectedFromInheritedAssetSharesId = "";
		selectedFromShare = "";
		toPersonView = new PersonView();
		selectedToInheritedAssetSharesId = "";
		selectedToShare = "";
		takharujShare = "";
		
		takharujDetailViewDataTable = new HtmlDataTable();
		beneficiarySelectItems = new ArrayList<SelectItem>(0);
		
		propertyServiceAgent = new PropertyServiceAgent();
	}
	
	@Override
	public void init() 
	{
		String METHOD_NAME = "init()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAKHARUJ INHERITED ASSET VIEW
		if ( sessionMap.get( WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW ) != null )
		{
			takharujInheritedAssetView = ( TakharujInheritedAssetView ) sessionMap.get( WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW );
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAKHARUJ INHERITED ASSET VIEW
		if ( takharujInheritedAssetView != null )
		{
			viewMap.put( WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW, takharujInheritedAssetView );
		}
		
		loadDataTable();
		
	}
	
	private void loadDataTable() {
		// TODO Auto-generated method stub
		List<TakharujEffectView> viewToBind = new ArrayList<TakharujEffectView>();
		
		if(viewMap.containsKey(WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW ))
		{
			takharujInheritedAssetView=(TakharujInheritedAssetView)viewMap.get(WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW);
			takharujDetailView = new HashMap<Long, TakharujEffectView>();
			if(takharujInheritedAssetView!=null)
			{
				for (TakharujDetailView detailView : takharujInheritedAssetView.getTakharujDetailViewList())
				{
					
						//ExtractPersonId
					
					if(takharujDetailView.containsKey(detailView.getFromInheritedAssetSharesView().getInheritanceBeneficiaryView().getBeneficiary().getPersonId()))
					{
						//if a person is both in the From and To column of the grid , the code below will wrongly see a singleRowTo as a singleRowFor ,
						// singleRowFrom.getTakhurshare will result in null in the above case.
						
					
						TakharujEffectView singleRowFrom = takharujDetailView.get(detailView.getFromInheritedAssetSharesView().getInheritanceBeneficiaryView().getBeneficiary().getPersonId());
						
						if(singleRowFrom.getTakharujShare()!=null)
						{
							
							Double afterTakharujShare = detailView.getFromSharePercentage() - detailView.getTakharujShare()-singleRowFrom.getTakharujShare() ;
							if(singleRowFrom.getIsTo())
							{
								afterTakharujShare=(detailView.getFromSharePercentage()* (detailView.getTakharujShare()+singleRowFrom.getTakharujShare()) )/ 100;
								afterTakharujShare=Math.abs((singleRowFrom.getCurrentAfterTakharuj())-afterTakharujShare);
								singleRowFrom.setAfterTakharujShare(afterTakharujShare);
							}
							else
							{	
								singleRowFrom.setAfterTakharujShare(afterTakharujShare); // we may have to introduce getCurrentAftertakh here too incase we have a From such that it came to To later
							//possibly we'll have to subtract the modified current share value , by keeping a variable which has the currentshares in it ,and subtracting it from the above quantity
							}
							singleRowFrom.setTakharujShare((detailView.getTakharujShare()+singleRowFrom.getTakharujShare()));
						
						}
						else 
						{
							// have to subtract stuff in this section , subtract that amount that we obtain below , from the amount we obtained when personId was in To sections.
							
							Double afterTakharujShare = detailView.getFromSharePercentage() - detailView.getTakharujShare();
							afterTakharujShare=singleRowFrom.getAfterTakharujShare()-afterTakharujShare; // singleRowForm contains singleRowTo
							singleRowFrom.setTakharujShare(detailView.getTakharujShare());
							singleRowFrom.setAfterTakharujShare(afterTakharujShare	);
//							singleRowFrom.setIsTo(true);
//							singleRowFrom.setIsFrom(false);
							
							
						}
					}
					else
					{
						Long fromPersonId=detailView.getFromInheritedAssetSharesView().getInheritanceBeneficiaryView().getBeneficiary().getPersonId();
						
						TakharujEffectView singleRowFrom = new TakharujEffectView();
						
						singleRowFrom.setCurrentShare(detailView.getFromSharePercentage());
						Double afterTakharujShare = detailView.getFromSharePercentage() -  detailView.getTakharujShare() ;
						singleRowFrom.setTakharujShare(detailView.getTakharujShare());
						singleRowFrom.setAfterTakharujShare(afterTakharujShare);
						singleRowFrom.setBeneficiary(detailView.getFromInheritedAssetSharesView().getInheritanceBeneficiaryView().getBeneficiary().getPersonFullName());
						takharujDetailView.put(fromPersonId,singleRowFrom);
						singleRowFrom.setIsFrom(true);
						singleRowFrom.setIsTo(false);
						viewToBind.add(singleRowFrom);
					}
											
						
						
						
						
						if(takharujDetailView.containsKey(detailView.getToPersonView().getPersonId()))
						{
							TakharujEffectView singleRowTo  = takharujDetailView.get(detailView.getToPersonView().getPersonId());
							
							if(singleRowTo.getIsFrom())
							{
								Double afterTakharujShare=detailView.getTakharujShare();
								afterTakharujShare=singleRowTo.getAfterTakharujShare()+afterTakharujShare;
								singleRowTo.setAfterTakharujShare(afterTakharujShare);
							}
							else
							{
								Double afterTakharujShare= 0.0D; 
								if(singleRowTo.getAfterTakharujShare() != null)
									afterTakharujShare = singleRowTo.getAfterTakharujShare() +detailView.getTakharujShare();
								else
									afterTakharujShare = detailView.getToSharePercentage() +  detailView.getTakharujShare();
							singleRowTo.setAfterTakharujShare(afterTakharujShare);
							}
							
						}
						else
						{
							TakharujEffectView singleRowTo = new TakharujEffectView();
							Double afterTakharujShare = detailView.getToSharePercentage() + detailView.getTakharujShare();
							singleRowTo.setCurrentShare(detailView.getToSharePercentage());			
							singleRowTo.setBeneficiary(detailView.getToPersonView().getFirstName());
							singleRowTo.setAfterTakharujShare(afterTakharujShare); // currentshare plus , aftertakharuj quanitity of "FROM"
							singleRowTo.setCurrentAfterTakharuj(singleRowTo.getAfterTakharujShare());
							takharujDetailView.put(detailView.getToPersonView().getPersonId(),singleRowTo);
							singleRowTo.setIsTo(true);
							singleRowTo.setIsFrom(false);
							viewToBind.add(singleRowTo);
						}
//						viewToBind.add(singleRowFrom);
						
						
						
						
					
				}
				viewMap.put("effectViewList",viewToBind);
				
				
			}
			
		}
	}

	public int getTakharujDetailViewListRecordSize()
	{
		int size = 0;
		
		for ( TakharujDetailView takharujDetailView : takharujDetailViewList )
			if ( takharujDetailView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
				size++;
		
		return size;
	}
	
	public void onFromBeneficiaryChange()
	{
		if ( selectedFromInheritedAssetSharesId != null && ! selectedFromInheritedAssetSharesId.equalsIgnoreCase("") )
		{
			InheritedAssetSharesView selectedInheritedAssetSharesView = inheritedAssetSharesViewMap.get( selectedFromInheritedAssetSharesId );
			selectedFromShare = String.valueOf( selectedInheritedAssetSharesView.getSharePercentage() );
		}
		else
		{
			selectedFromShare = "";
		}
	}
	
	public void onToBeneficiaryChange() throws PimsBusinessException
	{
		if ( toPersonView != null && toPersonView.getPersonId() != null )
		{
			toPersonView = propertyServiceAgent.getPersonInformation( toPersonView.getPersonId() );
			selectedToInheritedAssetSharesId = "";
			selectedToShare = "0.00";
			
			if ( inheritedAssetSharesViewMap != null && inheritedAssetSharesViewMap.size() > 0 )
			{
				for ( InheritedAssetSharesView inheritedAssetSharesView : inheritedAssetSharesViewMap.values() )
				{
					// Checking if the selected to person is already a beneficiary in the selected file & selected asset
					if ( inheritedAssetSharesView != null 
							&& inheritedAssetSharesView.getInheritedAssetSharesId() != null
							&& inheritedAssetSharesView.getInheritanceBeneficiaryView() != null 
							&& inheritedAssetSharesView.getInheritanceBeneficiaryView().getBeneficiary() != null 
							&& inheritedAssetSharesView.getInheritanceBeneficiaryView().getBeneficiary().getPersonId() != null							
							&& toPersonView.getPersonId().equals( inheritedAssetSharesView.getInheritanceBeneficiaryView().getBeneficiary().getPersonId() ) )
					{
						selectedToInheritedAssetSharesId = String.valueOf( inheritedAssetSharesView.getInheritedAssetSharesId() );
						selectedToShare = String.valueOf( inheritedAssetSharesView.getSharePercentage() );
						break;
					}
				}
			}
		}
	}
	
	private boolean isValid() 
	{
		boolean isValid = true;
		
		if ( selectedFromInheritedAssetSharesId == null || selectedFromInheritedAssetSharesId.equalsIgnoreCase("") )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.takharuj.detail.from.beneficiary.empty" ) );
			isValid = false;
		}
		
		if ( toPersonView == null || toPersonView.getPersonId() == null )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.takharuj.detail.to.beneficiary.empty" ) );
			isValid = false;
		}
		
		if ( takharujShare == null || takharujShare.equalsIgnoreCase("") )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.takharuj.detail.takharuj.share.empty" ) );
			isValid = false;
		}
		
		return isValid;
	}
	
	public String onTakharujDetailAdd()
	{
		final String METHOD_NAME = "onTakharujDetailAdd()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			if ( isValid() )
			{
				TakharujDetailView takharujDetailView = new TakharujDetailView();
				
				
				// Takharuj Inherited Asset
				takharujDetailView.setTakharujInheritedAssetId( takharujInheritedAssetView.getTakharujInheritedAssetId() );
				
				
				// From Beneficiary
				if ( selectedFromInheritedAssetSharesId != null && ! selectedFromInheritedAssetSharesId.equalsIgnoreCase("") )
				{
					InheritedAssetSharesView fromInheritedAssetSharesView = inheritedAssetSharesViewMap.get( selectedFromInheritedAssetSharesId );
					
					if ( fromInheritedAssetSharesView != null && fromInheritedAssetSharesView.getInheritedAssetSharesId() != null )
					{
						takharujDetailView.setFromInheritedAssetSharesView( fromInheritedAssetSharesView );
					}
				}
				
				// From Share
				if ( selectedFromShare != null && ! selectedFromShare.equalsIgnoreCase("") )
				{
					takharujDetailView.setFromSharePercentage( Double.valueOf( selectedFromShare ) );
				}			
				
				
				// To Beneficiary			
				if ( selectedToInheritedAssetSharesId != null && ! selectedToInheritedAssetSharesId.equalsIgnoreCase("") )
				{
					InheritedAssetSharesView toInheritedAssetSharesView = inheritedAssetSharesViewMap.get( selectedToInheritedAssetSharesId );
					
					if ( toInheritedAssetSharesView != null && toInheritedAssetSharesView.getInheritedAssetSharesId() != null )
					{
						takharujDetailView.setToInheritedAssetSharesView( toInheritedAssetSharesView );
					}
				}
				
				// To Share			
				if ( selectedToShare != null && ! selectedToShare.equalsIgnoreCase("") )
				{
					takharujDetailView.setToSharePercentage( Double.valueOf( selectedToShare ) );
				}
				
				
				// To Person
				if ( toPersonView != null && toPersonView.getPersonId() != null )
				{
					toPersonView = propertyServiceAgent.getPersonInformation( toPersonView.getPersonId() );
					takharujDetailView.setToPersonView( toPersonView );
				}
				
				
				// Takharuj Share
				if ( takharujShare != null && ! takharujShare.equalsIgnoreCase("") )
				{
					takharujDetailView.setTakharujShare( Double.valueOf( takharujShare ) );				
				}
				
				takharujDetailView.setCreatedBy( CommonUtil.getLoggedInUser() );
				takharujDetailView.setCreatedOn( new Date() );
				takharujDetailView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				takharujDetailView.setUpdatedOn( new Date() );
				takharujDetailView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
				takharujDetailView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
				
				takharujDetailViewList.add( takharujDetailView );
				
				selectedFromInheritedAssetSharesId = "";
				selectedFromShare = "";
				toPersonView = new PersonView();
				selectedToInheritedAssetSharesId = "";
				selectedToShare = "";
				takharujShare = "";
				
				updateValuesToMaps();
			}
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return null;
	}
	
	public String onTakharujDetailDelete()
	{
		TakharujDetailView selectedTakharujDetailView = (TakharujDetailView) takharujDetailViewDataTable.getRowData();
		
		if ( selectedTakharujDetailView.getTakharujDetailId() == null )
			takharujDetailViewList.remove( selectedTakharujDetailView );
		else
			selectedTakharujDetailView.setIsDeleted( 1L );
		
		updateValuesToMaps();
		
		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	public String onTakharujDetailDone()
	{	
		sessionMap.put(WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW, takharujInheritedAssetView);
		executeJavascript("closeAndPassTakharujInheritedAssetView();");
		return null;
	}
	
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	public TakharujInheritedAssetView getTakharujInheritedAssetView() {
		return takharujInheritedAssetView;
	}

	public void setTakharujInheritedAssetView(
			TakharujInheritedAssetView takharujInheritedAssetView) {
		this.takharujInheritedAssetView = takharujInheritedAssetView;
	}

	public List<TakharujDetailView> getTakharujDetailViewList() {
		return takharujDetailViewList;
	}

	public void setTakharujDetailViewList(
			List<TakharujDetailView> takharujDetailViewList) {
		this.takharujDetailViewList = takharujDetailViewList;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public HtmlDataTable getTakharujDetailViewDataTable() {
		return takharujDetailViewDataTable;
	}

	public void setTakharujDetailViewDataTable(
			HtmlDataTable takharujDetailViewDataTable) {
		this.takharujDetailViewDataTable = takharujDetailViewDataTable;
	}
	
	public Map<String, InheritedAssetSharesView> getInheritedAssetSharesViewMap() {
		return inheritedAssetSharesViewMap;
	}

	public void setInheritedAssetSharesViewMap(
			Map<String, InheritedAssetSharesView> inheritedAssetSharesViewMap) {
		this.inheritedAssetSharesViewMap = inheritedAssetSharesViewMap;
	}

	public List<SelectItem> getBeneficiarySelectItems() {
		return beneficiarySelectItems;
	}

	public void setBeneficiarySelectItems(
			List<SelectItem> beneficiarySelectItems) {
		this.beneficiarySelectItems = beneficiarySelectItems;
	}

	public String getSelectedFromInheritedAssetSharesId() {
		return selectedFromInheritedAssetSharesId;
	}

	public void setSelectedFromInheritedAssetSharesId(
			String selectedFromInheritedAssetSharesId) {
		this.selectedFromInheritedAssetSharesId = selectedFromInheritedAssetSharesId;
	}

	public String getSelectedFromShare() {
		return selectedFromShare;
	}

	public void setSelectedFromShare(String selectedFromShare) {
		this.selectedFromShare = selectedFromShare;
	}

	public PersonView getToPersonView() {
		return toPersonView;
	}

	public void setToPersonView(PersonView toPersonView) {
		this.toPersonView = toPersonView;
	}

	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	public String getSelectedToInheritedAssetSharesId() {
		return selectedToInheritedAssetSharesId;
	}

	public void setSelectedToInheritedAssetSharesId(
			String selectedToInheritedAssetSharesId) {
		this.selectedToInheritedAssetSharesId = selectedToInheritedAssetSharesId;
	}

	public String getSelectedToShare() {
		return selectedToShare;
	}

	public void setSelectedToShare(String selectedToShare) {
		this.selectedToShare = selectedToShare;
	}

	public String getTakharujShare() {
		return takharujShare;
	}

	public void setTakharujShare(String takharujShare) {
		this.takharujShare = takharujShare;
	}

	public Boolean getPopModeView() {
		return popModeView;
	}

	public void setPopModeView(Boolean popModeView) {
		this.popModeView = popModeView;
	}

	public Map<Long, TakharujEffectView> getTakharujDetailView() {
		return takharujDetailView;
	}

	public void setTakharujDetailView(
			Map<Long, TakharujEffectView> takharujDetailView) {
		this.takharujDetailView = takharujDetailView;
	}

	public List<TakharujEffectView> getTakharujEffectViewList() {
		takharujEffectViewList=(ArrayList<TakharujEffectView>)viewMap.get("effectViewList");
		if(takharujEffectViewList==null)
			takharujEffectViewList=new ArrayList<TakharujEffectView>();		
		return takharujEffectViewList;
	}

	public void setTakharujEffectViewList(
			List<TakharujEffectView> takharujEffectViewList) {
		this.takharujEffectViewList = takharujEffectViewList;
	}
}
