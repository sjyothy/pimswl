package com.avanza.pims.web.mems.minors;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.WebContext;
import com.avanza.pims.bpel.memscollectionbpel.proxy.MEMSCollectionBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.AssetMems;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.FinancialTransaction;
import com.avanza.pims.entity.InheritanceFile;
import com.avanza.pims.entity.XxArReceiptAmaf;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationDetails;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.GRP.GRPService;
import com.avanza.pims.ws.mems.BlockingAmountService;
import com.avanza.pims.ws.mems.CollectionProcedureService;
import com.avanza.pims.ws.mems.PersonalAccountTrxService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.utility.RevenueFollowupService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.BankView;
import com.avanza.pims.ws.vo.BounceChequeView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.PaymentMethodView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.AssetMemsView;
import com.avanza.pims.ws.vo.mems.CollectionPaymentDetailsView;
import com.avanza.pims.ws.vo.mems.CollectionProcedureView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.ui.util.ResourceUtil;

public class CollectionProcedureBackingBean extends AbstractMemsBean 
{
	
	public interface Page_Mode {
		
		public static final String PAGE_MODE ="PAGE_MODE";
		public static final String ADD_EDIT  ="ADD_EDIT";
		public static final String APPROVE_REJECT     ="APPROVE_REJECT";
		public static final String COLLECT_PAYMENT     ="COLLECT_PAYMENT";
		public static final String MOVE_COLLECTION_TASK     ="MOVE_COLLECTION_TASK";
		public static final String MOVE_DISTRIBUTION_TASK     ="MOVE_DISTRIBUTION_TASK";
		public static final String REVIEW_REQUIRED    ="REVIEW_REQUIRED";
		public static final String CONFIRM   ="CONFIRM";
		public static final String VIEW      ="PAGE_MODE_VIEW";
		
		
	}

	
	private static final long serialVersionUID = 4747380054126421906L;
	
	private transient Logger logger = Logger.getLogger(CollectionProcedureBackingBean.class);
	private final String TASK_LIST_USER_TASK = "TASK_LIST_USER_TASK";
	private final String TASK_TYPE = "TASK_TYPE";
	private static final String TAB_REVIEW = "reviewTab"; 
	private static final String TAB_DETAILS = "basicInfoTab";
	private final String notesOwner = WebConstants.REQUEST;

	FacesContext context = FacesContext.getCurrentInstance();
	@SuppressWarnings("unchecked")
	private HtmlDataTable dtCollection;
	protected HtmlTabPanel richPanel = new HtmlTabPanel();
	private HtmlInputText htmlFileStatus = new HtmlInputText ();
	private HtmlInputText  htmlFileDate = new HtmlInputText ();
    private HtmlCommandButton btnSave = new HtmlCommandButton();
    private HtmlInputTextarea htmlRFC = new HtmlInputTextarea();
    private String rowId="";
    private HtmlCommandButton btnSubmit = new HtmlCommandButton();
    private HtmlCommandButton btnConfirm = new HtmlCommandButton();
	private HtmlInputText  htmlFilePerson = new HtmlInputText ();
	private HtmlInputText  htmlFileType = new HtmlInputText ();
	private HtmlInputText htmlFileNumber = new HtmlInputText();
	private HtmlSelectOneMenu collectedAsSelectMenu = new HtmlSelectOneMenu();
	private List<SelectItem> collectedAsList = new ArrayList<SelectItem>(0);
	private String VIEW_MODE = "pageMode";
	private Integer paginatorRows = 0;

	private List<String> successMessages = new ArrayList<String>();

	private String MODE_SELECT_ONE_POPUP = "MODE_SELECT_ONE_POPUP";
	private String MODE_SELECT_MANY_POPUP = "MODE_SELECT_MANY_POPUP";
	private HtmlInputText txtRefNum = new HtmlInputText ();
	private HtmlInputText txtAmount = new HtmlInputText ();
	private HtmlInputText txtAccNum = new HtmlInputText ();
	private HtmlSelectOneMenu cmbBank = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu cmbAssets = new HtmlSelectOneMenu();
	private HtmlCalendar refDate = new HtmlCalendar();
    
	private String pageMode;
	private boolean isPageModeSelectOnePopUp = false;
	private boolean isPageModeSelectManyPopUp = false;
	private RequestView requestView = new RequestView();
    private Long inheritanceFileId;
    private Long fileOwnerId;
    private String fileCostCenter;
    private String oldFileCostCenter;
    
    private String hdnPersonId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;
    
    private Long assetMemsId;
    private CollectionPaymentDetailsView collectionPaymentDetailsView = new CollectionPaymentDetailsView();
    private List<CollectionPaymentDetailsView> collectionList = new ArrayList<CollectionPaymentDetailsView>( 0 );
    private Double totalCollectionAmount;
    private Double totalCollectionCount;
    private String selectOneAssetId;
    private List<SelectItem> assetNameList = new ArrayList<SelectItem>();
    private HtmlGraphicImage imgDeleteCollection = new HtmlGraphicImage();
    private HtmlGraphicImage imgEditCollection = new HtmlGraphicImage();
    private HtmlGraphicImage imgPrintPaymentReceipt = new HtmlGraphicImage();
    
	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public FacesContext getContext() {
		return context;
	}

	public void setContext(FacesContext context) {
		this.context = context;
	}


	public HtmlInputText getHtmlFileNumber() {
		return htmlFileNumber;
	}

	public void setHtmlFileNumber(HtmlInputText htmlFileNumber) {
		this.htmlFileNumber = htmlFileNumber;
	}

	@SuppressWarnings("unchecked")
	public void init() 
	{
		super.init();
		try 
		{
			if ( !isPostBack() )
			{
				initializeContext();
			}
			else
			{
				rowId = "";
			}
		}
		catch (Exception es) 
		{
			logger.LogException("init|ErrorOccured|", es);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void initializeContext() throws Exception
	{
		viewMap.put( "canAddAttachment" , true );
		viewMap.put( "canAddNote", true );
		viewMap.put( WebConstants.Attachment.EXTERNAL_ID, WebConstants.Attachment.EXTERNAL_ID_REQUEST );
		loadCollectedAsList();
		setBankViewMap( new HashMap<Long,BankView>() );
		
		// From Task List
		if( sessionMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK )!= null  )
		{
		    openFromTaskList();
		}
		
		// From Search Asset
		else if (sessionMap.get(WebConstants.ASSET_ID) != null) 
		{
			openFromAssetSearch();
		}
		
		// From File Search
		else if (sessionMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null) 
		{
			openFromFileSearch();
		}
		
		// From request Search
		else if (FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW)!=null )
		{
			openFromRequestSearch();
		}
		//From Cheque List (Payment Search)
		else if (FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.ReplaceCheque.BOUNCE_CHEQUE_VIEW )!= null)
		{
			openFromChequeList();
			
		}
		
		//Loading file parameters
		if( this.getInheritanceFileId() != null )
		{
			loadInhFileParams();
			loadAssetNamesByInheritanceFileId();
			this.assetNameList.add(0,new SelectItem("-1",""));
		}
		
		//If request was made from Asset Search
		else
		{
			loadAssetNamesByAssetId();
		}
	}
	@SuppressWarnings( "unchecked" )
	public void openFromChequeList() throws Exception
	{
		BounceChequeView bounceChequeView = null;
		if( FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.ReplaceCheque.BOUNCE_CHEQUE_VIEW ) != null)
		{
		 bounceChequeView =(BounceChequeView)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().remove(WebConstants.ReplaceCheque.BOUNCE_CHEQUE_VIEW );
		} 
		else
		{
		 bounceChequeView = (BounceChequeView)sessionMap.remove(WebConstants.SELECTED_ROW);
		}
		getRelatedCollectionTrxsForPaymentScheduleId(bounceChequeView.getPaymentScheduleId() );
		if(this.getInheritanceFileId() == null)
		{
			setInheritanceFileId( bounceChequeView.getInheritanceFileId() );
			loadAttachmentsAndComments(null);
			this.setPageMode(Page_Mode.ADD_EDIT);
			
		}
		enableDisableForReplaceCheque(true);
	}
	
	@SuppressWarnings( "unchecked" )
	public void getRelatedCollectionTrxsForPaymentScheduleId(Long paymentScheduleId)throws Exception
	{
		CollectionProcedureView view = CollectionProcedureService.getCollectionsByPaymentScheduleId(paymentScheduleId);
		CollectionProcedureView newView = new CollectionProcedureView();
		newView.setInheritanceFileId( view.getInheritanceFileId());
		
		newView.setAmountString(view.getAmountString());
		newView.setAmount(view.getAmount());
		newView.setInheritedAssetId(   view.getInheritedAssetId() );
		newView.setOldCollectionTrxId( view.getCollectionTrxId() );
		newView.setOldCollectionTrxNumber(  view.getTrxNumber() );
		
		CollectionPaymentDetailsView obj =  new CollectionPaymentDetailsView();
		obj.setAccountNo( view.getPaymentCriteriaView().getAccountNumber() );
		obj.setAmount( view.getAmountString() );
		obj.setOldCollectionTrxId( view.getCollectionTrxId() );
		obj.setIsDeleted( 0L ) ;
		obj.setOldPaymentScheduleNumber(view.getTrxNumber());
		obj.setMyHashCode( obj.hashCode());
		obj.setIsBeingEdited( 0L );
		obj.setPaymentMethod( view.getPaymentMethod() );
		if(view.getPaymentCriteriaView() != null && view.getPaymentCriteriaView().getBankId() != null)
		{
		 obj.setBankId(view.getPaymentCriteriaView().getBankId().toString());
		}
		obj.setDescription(view.getDescription());
		obj.setRefDate( view.getPaymentCriteriaView().getDate() );
		List<CollectionProcedureView>  listCollectionProcedureView = new ArrayList<CollectionProcedureView>();
		listCollectionProcedureView.add(newView);
		obj.setCollectionTrxView( listCollectionProcedureView );
		
		this.setCollectionPaymentDetailsView(obj);
	}
	
	@SuppressWarnings( "unchecked" )
	private void openFromRequestSearch() throws Exception
	{
		RequestView rv = (RequestView)FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebConstants.REQUEST_VIEW);
		ApplicationContext.getContext().get(WebContext.class).removeAttribute(WebConstants.REQUEST_VIEW);
		this.setRequestView(rv);
		populateApplicationDetailsTab();
		loadCollectionTransactionsAgainstRequest(rv.getRequestId());
		viewMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_ID,rv.getInheritanceFileId());
		loadInhFileParams( );
		loadAttachmentsAndComments(rv.getRequestId());
		setPageModeBasedOnRequestStatus();
	}

	
	@SuppressWarnings( "unchecked" )
	private void openFromAssetSearch() throws Exception 
	{
		if ( sessionMap.get(WebConstants.ASSET_ID)!= null )
		{
	     this.setAssetMemsId( new Long ( sessionMap.remove(WebConstants.ASSET_ID).toString() ) );
		 this.setPageMode(Page_Mode.ADD_EDIT);
		}
		if( sessionMap.get(WebConstants.CollectionProcedure.COLLECT_BANK_TRANSFER) != null )
		{
		   viewMap.put( WebConstants.CollectionProcedure.COLLECT_BANK_TRANSFER,
				        sessionMap.remove( WebConstants.CollectionProcedure.COLLECT_BANK_TRANSFER  )  
				       );	
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void openFromFileSearch() throws Exception 
	{
		setInheritanceFileId( new Long ( sessionMap.remove(WebConstants.InheritanceFile.INHERITANCE_FILE_ID).toString() ) );
		loadAttachmentsAndComments(null);
		this.setPageMode(Page_Mode.ADD_EDIT);
	}
	
	@SuppressWarnings( "unchecked" )
	private void loadAssetNamesByInheritanceFileId() throws Exception 
	{
			List<AssetMemsView> assetMems;
			if ( this.getInheritanceFileId()  != null) 
			{
				HashMap<String,String> extraCriteria = new HashMap<String, String>();
				extraCriteria.put(WebConstants.ASSET_SEARCH_CRITERIA.PENDING_CHEQUES_CASH,WebConstants.ASSET_SEARCH_CRITERIA.PENDING_CHEQUES_CASH);
				assetMems = new UtilityService().getAssetViewByInheritanceFileId(  this.getInheritanceFileId(),extraCriteria );
				populateAssetMems( assetMems);
			}
	}
	@SuppressWarnings( "unchecked" )
	private void loadAssetNamesByAssetId() throws Exception 
	{
		AssetMemsView assetMems;
		if (this.getAssetMemsId() != null) 
		{
			List<AssetMemsView> assetMemsList = new ArrayList<AssetMemsView>( 0 );
			assetMems = new UtilityService().getAssetViewById( this.getAssetMemsId()  );
			assetMemsList.add( assetMems );
			populateAssetMems( assetMemsList );
			if( this.getPageMode().equals(Page_Mode.ADD_EDIT) )
			{
				 this.getCollectionPaymentDetailsView().getSingleCollectionTrxView().setSelectOneAssetId(this.getAssetMemsId().toString());
				 onAssetNameChange();
			}
	   }

	}

	@SuppressWarnings( "unchecked" )
	private void populateAssetMems(	List<AssetMemsView> assetMems) throws Exception 
	{
		List<SelectItem> selectList = new ArrayList<SelectItem>();
		viewMap.put("AssetMems", assetMems);
		for (AssetMemsView assetMems2 : assetMems)
		{
			SelectItem item = new SelectItem();

			item.setValue(assetMems2.getAssetId().toString());
			
			if (CommonUtil.getIsEnglishLocale())
				item.setLabel(assetMems2.getAssetNameEn()!=null?assetMems2.getAssetNameEn():assetMems2.getAssetNameAr());
			else if(assetMems2.getAssetNameAr() != null)
				item.setLabel(assetMems2.getAssetNameAr()!=null?assetMems2.getAssetNameAr():assetMems2.getAssetNameEn() );
            
			selectList.add(item);
			setAssetMemsMap( assetMems2 );

		}
		Collections.sort(selectList, ListComparator.LIST_COMPARE);
		this.setAssetNameList(selectList);
	}
	@SuppressWarnings( "unchecked" )
	public List<SelectItem> getAssetNameList()
	{

		assetNameList = (ArrayList<SelectItem>) viewMap.get("assetNameList");
		
		if (assetNameList == null)
		{
			assetNameList = new ArrayList<SelectItem>();
		}
		

		return assetNameList;
	}
	@SuppressWarnings( "unchecked" )
	public void setAssetNameList(List<SelectItem> assetNameList) {
		this.assetNameList = assetNameList;
		if (assetNameList != null)
			viewMap.put("assetNameList", assetNameList);
	}
	@SuppressWarnings( "unchecked" )
	private HashMap<Long,AssetMemsView>  getAssetMemsMap( )
	{
		if( viewMap.get("AssetMemsMap") != null )
		{
		  return ( HashMap<Long, AssetMemsView> )viewMap.get("AssetMemsMap"); 
		}
		return null;
	}
	@SuppressWarnings( "unchecked" )
	private void setAssetMemsMap( AssetMemsView assetMems )
	{
		HashMap<Long,AssetMemsView> map = new HashMap<Long, AssetMemsView>( 0 );
		if( viewMap.get("AssetMemsMap") != null )
		{
			map = ( HashMap<Long, AssetMemsView> )viewMap.get("AssetMemsMap"); 
		}
		if( !map.containsKey( assetMems.getAssetId() ) )
		{
			map.put(assetMems.getAssetId() , assetMems );
		}
		viewMap.put("AssetMemsMap", map);
	}
	@SuppressWarnings( "unchecked" )
	private AssetMemsView getAssetMemsFromMapByAssetId( Long assetId )
	{
		HashMap<Long,AssetMemsView> map = new HashMap<Long, AssetMemsView>( 0 );
		if( viewMap.get("AssetMemsMap") != null )
		{
			map = ( HashMap<Long, AssetMemsView> )viewMap.get("AssetMemsMap"); 
		}
		if( map.containsKey( assetId ) )
		{
			return (AssetMemsView )map.get(assetId );
		}
		
			return null;
		
	}
	
	

	
	@SuppressWarnings( "unchecked" )
	public void tabAttachmentsComments_Click() {

		String methodName = "tabAttachmentsComments_Click";
		logger.logInfo(methodName + "|" + "Start...");
		try {
			viewMap.put("canAddNote", true);
			
				loadAttachmentsAndComments(this.getRequestView().getRequestId());
		} catch (Exception ex) {
			logger.LogException(methodName + "|" + "Exception Occured...", ex);
		}
		logger.logInfo(methodName + "|" + "Finish...");

	}
	public boolean isPageModeAddEdit()
	{
		if( this.getPageMode()!=null && this.getPageMode().equals(Page_Mode.ADD_EDIT) )
		{
			
			return true;
		}
		return false;
		
	}
	public boolean isReqestPresent()
	{
		return  this.getRequestView() !=null && this.getRequestView().getRequestId() != null ;
		
	}
	public boolean isConfirmBankTransfer()
	{
		if( viewMap.get(WebConstants.CollectionProcedure.COLLECT_BANK_TRANSFER ) !=null  )
		{
			
			return true;
		}
		return false;
		
	}
	public boolean isPageModeConfirm()
	{
		if( this.getPageMode()!=null && this.getPageMode().equals( Page_Mode.CONFIRM ) )
		{
			
			return true;
		}
		return false;
		
	}
	public boolean isTaskPresent()
	{
		//return viewMap.get(TASK_LIST_USER_TASK)!= null ;
		return true;
		
	}
	
	public boolean isPageModeMoveDistributionTask()
	{
		if( this.getPageMode()!=null && this.getPageMode().equals( Page_Mode.MOVE_DISTRIBUTION_TASK) )
		{
			
			return true;
		}
		return false;
		
	}
	public boolean isPageModeMoveCollectionTask()
	{
		if( this.getPageMode()!=null && this.getPageMode().equals( Page_Mode.MOVE_COLLECTION_TASK) )
		{
			
			return true;
		}
		return false;
		
	}
	public boolean isPageModeApproveReject()
	{
		if( this.getPageMode()!=null && this.getPageMode().equals( Page_Mode.APPROVE_REJECT ) )
		{
			
			return true;
		}
		return false;
		
	}
	public boolean isPageModeReview()
	{
		if( this.getPageMode()!=null && this.getPageMode().equals( Page_Mode.REVIEW_REQUIRED) )
		{
			
			return true;
		}
		return false;
		
	}
	public boolean isPageModeCollect()
	{
		if( this.getPageMode()!=null && this.getPageMode().equals( Page_Mode.COLLECT_PAYMENT ) )
		{
			
			return true;
		}
		return false;
		
	}
	public boolean isPageModeView()
	{
		if( this.getPageMode()!=null && this.getPageMode().equals( Page_Mode.VIEW) )
		{
			
			return true;
		}
		return false;
		
	}
	public boolean getIsViewMode() {
		boolean returnVal = false;
//
//		if (viewMap.containsKey("requestView") || viewMap.containsKey("requestId"))
//		{
//		
//				returnVal = true;
//		}
//
//		else {
//			returnVal = false;
//		}

		return returnVal;

	}
	public boolean getIsRequestNew() {
		boolean returnVal = false;
		try
		{ 
			
		if (getRequestView() != null)
		{
			RequestView view=getRequestView();
			
			//domain data obtain status id for status new
			DomainDataView ddView = new UtilityService().getDomainDataByDomainDataId(view.getStatusId());
			if(ddView.getDataValue().compareTo("REQUEST STATUS NEW")==0)		
				returnVal = true;
			else
				returnVal = false;
		}

		else 
		{
			returnVal = false;
		}
		}
		catch(Exception ex)
		{
			logger.LogException("getIsRequestNew--Error Occured", ex);
		}

		return returnVal;
//		return true;

	}
	public boolean getIsRequestApprovalRequired() {
		boolean returnVal = false;
		String methodName = "getIsRequestApprovalRequired";
		try
		{

		if (getRequestView() != null || viewMap.containsKey("requestId"))
		{
			Long requestId = Long.valueOf(viewMap.get("requestId").toString());
			// GOT TO PICK REQUESTVIEW FROM REQUESTID , SINCE REQUESTVIEW IS NOT AVAILBALE WHEN COMING FORM TASKLIST
			CollectionProcedureService service = new CollectionProcedureService();
			Long statusId=service.getRequestById(requestId);
			//domain data obtain status id for status new
			DomainDataView ddView = new UtilityService().getDomainDataByDomainDataId(statusId);
			
			if(ddView.getDataValue().compareTo("REQUEST STATUS APPROVAL REQUIRED")==0)		
				returnVal = true;
			else
				returnVal = false;
		}

		else {
			returnVal = false;
		}
		}
		catch(Exception ex)
		{
			logger.LogException( methodName +"Error Occured:",ex );
		}

		return returnVal;
//		return true;

	}
	public boolean getIsFileIdPresentInRequest() {
		boolean returnVal = false;

		if (this.getInheritanceFileId()!=null)
		{
			returnVal= true;
				
		}

		else {
			returnVal = false;
		}

		return returnVal;

	}
	@SuppressWarnings( "unchecked" )
	private void loadCollectionTransactionsAgainstRequest( Long requestId )throws Exception 
	{
		CollectionProcedureService service = new CollectionProcedureService();
		HashMap map = service.getCollectionsPaymentDetailsByRequestId( requestId, CommonUtil.getIsEnglishLocale());
		if( map.containsKey(Constant.CollectionProcedureLoadingParams.COLLECTION_PROC_VIEW_LIST) && map.get(Constant.CollectionProcedureLoadingParams.COLLECTION_PROC_VIEW_LIST)!=null )
		{				
		 collectionList = (List<CollectionPaymentDetailsView>) map.get(Constant.CollectionProcedureLoadingParams.COLLECTION_PROC_VIEW_LIST);
		 this.setCollectionList(collectionList);
		}
		if( map.containsKey(Constant.CollectionProcedureLoadingParams.FILE_ID ) && map.get(Constant.CollectionProcedureLoadingParams.FILE_ID )!= null )
		{
		  this.setInheritanceFileId(new Long( map.get(Constant.CollectionProcedureLoadingParams.FILE_ID ).toString() ));
		}
		if( map.containsKey(Constant.CollectionProcedureLoadingParams.ASSET_MEMS_ID ) && map.get(Constant.CollectionProcedureLoadingParams.ASSET_MEMS_ID )!= null  )
		{
			this.setAssetMemsId(  new Long ( map.get(Constant.CollectionProcedureLoadingParams.ASSET_MEMS_ID ).toString() ));
			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void setPageModeBasedOnRequestStatus() throws Exception 
	{
		String taskType = "";
		
		if( this.getRequestView() == null || this.getRequestView().getStatusId() == null)
		{return;}
		
		if( viewMap.get(TASK_TYPE) != null )
		{
			taskType = viewMap.get(TASK_TYPE).toString();
		}
		if( this.getRequestView().getStatusId().compareTo( WebConstants.REQUEST_STATUS_NEW_ID ) == 0 )
		{
		  this.setPageMode(Page_Mode.ADD_EDIT);	
		}
		
		else if( this.getRequestView().getStatusId().compareTo( WebConstants.REQUEST_STATUS_COLLECTION_REQ_ID ) == 0 )
		{
			this.setPageMode(Page_Mode.COLLECT_PAYMENT);	
		}
		else if( this.getRequestView().getStatusId().compareTo( WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID ) == 0 )
		{
			this.setPageMode(Page_Mode.APPROVE_REJECT);	
		}
		else if( this.getRequestView().getStatusId().compareTo( WebConstants.REQUEST_STATUS_DISTRIBUTION_REQ_ID ) == 0  )
		{
			if(taskType.equals("CollectRequest"))
			{
				this.setPageMode(Page_Mode.MOVE_COLLECTION_TASK);
				richPanel.setSelectedTab(TAB_DETAILS);
			}
			else
			{
				this.setPageMode(Page_Mode.CONFIRM);
				richPanel.setSelectedTab(TAB_DETAILS);
			}
		}
		else if( this.getRequestView().getStatusId().compareTo( WebConstants.REQUEST_STATUS_REVIEW_DONE_ID ) == 0 )
		{
			this.setPageMode(Page_Mode.CONFIRM);
			richPanel.setSelectedTab(TAB_REVIEW);
		}
		else if( this.getRequestView().getStatusId().compareTo( WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID ) == 0 )
		{
			this.setPageMode(Page_Mode.REVIEW_REQUIRED);
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
			richPanel.setSelectedTab(TAB_REVIEW);
			
		}
		else if( this.getRequestView().getStatusId().compareTo( WebConstants.REQUEST_STATUS_COMPLETE_ID ) == 0 )
		{
			if(taskType.equals("CollectRequest"))
			{
				this.setPageMode(Page_Mode.MOVE_COLLECTION_TASK);
				richPanel.setSelectedTab(TAB_DETAILS);
			}
			else if(taskType.equals("DisburseRequest"))
			{
				this.setPageMode(Page_Mode.MOVE_DISTRIBUTION_TASK);
				richPanel.setSelectedTab(TAB_DETAILS);
			}
			
			else
			{
				this.setPageMode(Page_Mode.VIEW);
			}
		} 
	}
	
	@SuppressWarnings( "unchecked" )
	private void loadInhFileParams() throws Exception 
	{
			Long fileId = null;
			if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null)
				fileId = Long.valueOf(viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID).toString());
			if (fileId != null) 
			{
				InheritanceFileView inhFile = new UtilityService().getInheritanceFileById(fileId);
				htmlFileNumber.setValue(inhFile.getFileNumber());
				htmlFilePerson.setValue( inhFile.getFilePerson() );
				
				DomainDataView domainData = new UtilityService().getDomainDataByDomainDataId(inhFile.getStatusId());
				DomainDataView domainDataFileType = new UtilityService().getDomainDataByDomainDataId( new Long (inhFile.getFileTypeId()));
				
				PropertyService service = new PropertyService();
				this.setFileOwnerId( new Long( inhFile.getFilePersonId() ) );
				this.setFileCostCenter(inhFile.getCostCenter() );
				this.setOldFileCostCenter(inhFile.getOldCostCenter() );
				PersonView personView=service.getPersonById( fileOwnerId );
				if (CommonUtil.getIsEnglishLocale()) 
				{
					htmlFileStatus.setValue(domainData.getDataDescEn());
					htmlFileType.setValue(domainDataFileType.getDataDescEn());
					htmlFileDate.setValue(inhFile.getCreatedOn());
				 
					
				} 
				else
				{
					htmlFileStatus.setValue(domainData.getDataDescAr());
					htmlFileType.setValue(domainDataFileType.getDataDescAr());
					htmlFileDate.setValue(inhFile.getCreatedOn());
				 
				}

			}

	}
	@SuppressWarnings("unchecked")
	private void loadCollectedAsList() throws Exception 
	{

		List<SelectItem> collectedAsList = new ArrayList<SelectItem>();
		HashMap <Long ,PaymentMethodView> map = new UtilityService().getPaymentMethodViewMap();
		setPaymentMethodViewMap(map);
		for (Long key : map.keySet()) 
		{
			PaymentMethodView paymentMethod = map.get( key );
			SelectItem item = new SelectItem();
			item.setValue(paymentMethod.getPaymentMethodId().toString());
			if (CommonUtil.getIsEnglishLocale())
				item.setLabel(paymentMethod.getDescription());
			else
				item.setLabel(paymentMethod.getDescriptionAr());
			collectedAsList.add(item);
		}
		   Collections.sort(collectedAsList, ListComparator.LIST_COMPARE);
		this.setCollectedAsList(collectedAsList);
	}
	@SuppressWarnings( "unchecked" )
	private Map<Long,PaymentMethodView> getPaymentMethodViewMap()
	{
		return (Map<Long,PaymentMethodView> )viewMap.get( "PaymentMethodViewMap" );
	}
	@SuppressWarnings( "unchecked" )
	private void setPaymentMethodViewMap( Map<Long,PaymentMethodView> map)
	{
		 viewMap.put("PaymentMethodViewMap", map);
	}
	
	@SuppressWarnings( "unchecked" )
	private Map<Long,BankView> getBankViewMap()
	{
		return ( Map<Long,BankView> )viewMap.get( "BankViewMap" );
	}
	@SuppressWarnings( "unchecked" )
	private void setBankViewMap( Map<Long,BankView> map) throws Exception
	{
		
		 
		 if( viewMap.get( "BankViewMap" ) == null )
		 {
			 
			 viewMap.put("BankViewMap",new UtilityService().getBankViewMap() );
		 }
	}
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(Long requestId) throws Exception
	{
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		String externalId = WebConstants.Attachment.EXTERNAL_ID_REQUEST;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		
		viewMap.put("noteowner", WebConstants.REQUEST);

		if (requestId != null) {
			String entityId = requestId.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}
	private void saveCommensAttachment(String eventDesc) throws Exception 
	{
		RequestView rv = this.getRequestView();
		saveComments( rv.getRequestId() );
		saveAttachments( rv.getRequestId().toString() );
	    if( eventDesc != null && eventDesc.trim().length() > 0 )
	    {
		 CommonUtil.saveSystemComments( WebConstants.REQUEST,eventDesc, rv.getRequestId() );
	    }
	}
	public Boolean saveComments(Long referenceId)throws Exception
    {
		Boolean success = false;
    	
    	NotesController.saveNotes(notesOwner, referenceId);
    	success = true;
    	return success;
    }
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId) throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	private boolean hasOnDeleteError() throws Exception
	{
		boolean hasError = false;
		errorMessages = new ArrayList<String>(0);
		
		//If there is  only on payment detail then do no let user delete payment
		if( this.getCollectionList().size() <= 1L )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("collection.msg.onePaymentDetailsReq"));
			hasError = true;
		}
		else
		{
			int noOfActiveRecords = 0;
			//If payment details are present except for one all are deleted then do no let user delete payment
			for (CollectionPaymentDetailsView obj : this.getCollectionList() ) 
			{
				if( obj.getIsDeleted().longValue() != 1l)
				{
					noOfActiveRecords += 1;   	
				}
				
			}
			if(  noOfActiveRecords <= 1)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("collection.msg.onePaymentDetailsReq"));
				hasError = true;		
			}
			
		}
		
		return hasError;
	}
	public void onDelete()
	{
		try 
		{
            if(  hasOnDeleteError() )
            {
            	return;
            }
			CollectionPaymentDetailsView obj = ( CollectionPaymentDetailsView )dtCollection.getRowData();
			if( obj.getPaymentDetailsId() == null  )
			{
			 this.getCollectionList().remove( obj  );
			}
			else
			{
			 obj.setIsDeleted(1L);
			}
		} 
		catch (Exception ex) 
		{
			logger.LogException( "onDelete|Error Occured|",ex );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	
		
	}
	public void onEdit()
	{
		try 
		{
			rowId ="";
			CollectionPaymentDetailsView obj = ( CollectionPaymentDetailsView )dtCollection.getRowData();
			obj.setIsBeingEdited(1L);
			this.setCollectionPaymentDetailsView( obj );
			onCollectedAsChanged();
			enableDisableForReplaceCheque(collectionPaymentDetailsView.getOldCollectionTrxId() != null );
		} 
		catch (Exception ex) 
		{
			logger.LogException( "onEdit|Error Occured|",ex );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	
		
	}
	public void onRequestHistoryTabClick()
	{
		try	
		{
			if(getRequestView() != null && getRequestView().getRequestId() !=null)
			{
		     RequestHistoryController rhc=new RequestHistoryController();
		     rhc.getAllRequestTasksForRequest(notesOwner,getRequestView().getRequestId().toString());
			}
		}
		catch(Exception ex)
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onRequestHistoryTabClickError|Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	

	private void enableDisableForReplaceCheque(boolean disable )throws Exception
	{
		if(disable )
		{
			txtAmount.setStyleClass( "INPUT_TEXT READONLY");
		}
		else
		{
			txtAmount.setStyleClass( "INPUT_TEXT");
		}
		txtAmount.setReadonly(disable);
//		cmbAssets.setDisabled(disable);
		collectedAsSelectMenu.setDisabled(disable);
//		refDate.setDisabled(disable);
	}
	
	
	public void onCollectedAsChanged()
	{
		try 
		{
	        collectionPaymentDetailsView = this.getCollectionPaymentDetailsView();
			if( collectionPaymentDetailsView.getPaymentMethod().equals(WebConstants.PAYMENT_METHOD_CASH_ID.toString() ) )
			{
				collectionPaymentDetailsView.setAccountNo("");
				txtAccNum.setDisabled(true);
				txtAccNum.setStyleClass( "INPUT_TEXT READONLY");
				
				collectionPaymentDetailsView.setReferenceNo("");
				txtRefNum.setDisabled(true);
				txtRefNum.setStyleClass( "INPUT_TEXT READONLY");
				
				collectionPaymentDetailsView.setBankId("");
				cmbBank.setDisabled(true);
				
				collectionPaymentDetailsView.setRefDate( new Date() );
				refDate.setDisabled(true);
				
				
			}
			else
			{
				txtAccNum.setDisabled(false);
				txtAccNum.setStyleClass( "INPUT_TEXT");
				
				txtRefNum.setDisabled(false);
				txtRefNum.setStyleClass( "INPUT_TEXT");
				
				if(  collectionPaymentDetailsView.getIsBeingEdited() != null &&
					 collectionPaymentDetailsView.getIsBeingEdited().longValue() != 1  )
				{
				 collectionPaymentDetailsView.setBankId("-1");
				}
				cmbBank.setDisabled(false);
				refDate.setDisabled(false);
		   }
		} 
		catch (Exception ex) 
		{
			logger.LogException( "onCollectedAsChanged|Error Occured|",ex );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public void openManageAsset()
	{
		try
		{
			CollectionPaymentDetailsView selectedRow = (CollectionPaymentDetailsView) dtCollection.getRowData();
			String javaScriptText = " var screen_width = 970;"
					+ "var screen_height = 550;"
					+ "var popup_width = screen_width;"
					+ "var popup_height = screen_height;"
					+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
					+ "window.open('ManageAssets.jsf?pageMode=MODE_SELECT_ONE_POPUP&"
					+ WebConstants.ASSET_ID+"="+selectedRow.getSingleCollectionTrxView().getAssetId()
					+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";
	
			sendToParent( javaScriptText );
		} 
		catch (Exception ex) 
		{
			logger.LogException( "openManageAsset|Error Occured|",ex );
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public boolean hasGridAdditionErrors()throws Exception
	{
			boolean hasError = false;
			hasError = hasEmptyCostCenterError();
			if(this.getCollectionPaymentDetailsView().getPaymentMethod() == null || this.getCollectionPaymentDetailsView().getPaymentMethod().equals("-1") )
			{
					errorMessages.add(ResourceUtil.getInstance().getProperty("BankRemittanceReport.ErrorMsg.PaymentMethod"));
					hasError = true;
			}
			if( this.getCollectionPaymentDetailsView().getAmount() == null || this.getCollectionPaymentDetailsView().getAmount().trim().length() <=0  )
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("collection.msg.amountNull"));
				hasError = true;
			}
			else
			{
				Double amount = null;
				try
				{
				  amount = new Double( this.getCollectionPaymentDetailsView().getAmount());
				}
				catch(Exception e)
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty("collection.msg.invalidAmount"));
					hasError = true;
				}
				if( amount != null  )
				{
					if( amount <= 0 )
					{
						errorMessages.add(ResourceUtil.getInstance().getProperty("collection.msg.amountGreaterThanZero"));
						hasError = true;
						
					}
				}
			}
			
			if( this.getCollectionPaymentDetailsView().getDescription() == null || this.getCollectionPaymentDetailsView().getDescription().trim().length() <= 0 )
			{
				
				errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.descReq"));
				hasError = true;
				
			}
			if( !isPageModeCollect() )
			{
				return hasError;
			}
			
			hasError = hasChequeOrBankTransferDetailsError( collectionPaymentDetailsView ) ; 
			
			
			return hasError;
	} 

	public void onAdd()
	{
		try 
		{
			rowId = "";
			collectionPaymentDetailsView = this.getCollectionPaymentDetailsView();
			if( !hasGridAdditionErrors() )
			{
				
				Map<Long,PaymentMethodView> map = getPaymentMethodViewMap();
				PaymentMethodView paymentMethod = map.get( new Long(collectionPaymentDetailsView.getPaymentMethod() ));
				collectionPaymentDetailsView.setPaymentMethodEn( paymentMethod.getDescription() );
				collectionPaymentDetailsView.setPaymentMethodAr( paymentMethod.getDescriptionAr() );
				Map<Long,BankView> mapBank = getBankViewMap();
				if( collectionPaymentDetailsView.getBankId() != null && 
					collectionPaymentDetailsView.getBankId().trim().length()>0 && 
				   !collectionPaymentDetailsView.getBankId().equals("-1")  )
				{
					BankView bank = mapBank.get( new Long(collectionPaymentDetailsView.getBankId() ));
					collectionPaymentDetailsView.setBankEn( bank.getBankEn() );
					collectionPaymentDetailsView.setBankAr( bank.getBankAr() );
					collectionPaymentDetailsView.setBankId( collectionPaymentDetailsView.getBankId() );
				}
				collectionPaymentDetailsView.setIsDeleted( 0L );
				if( collectionPaymentDetailsView.getIsBeingEdited() == null  || 
					collectionPaymentDetailsView.getIsBeingEdited().longValue() == 0L )
				{
					collectionPaymentDetailsView.setMyHashCode(  collectionPaymentDetailsView.hashCode() );
					collectionList= getCollectionList();
					collectionPaymentDetailsView.setIsBeingEdited( 0L );
					CollectionProcedureView collectionProcedureView =  collectionPaymentDetailsView.getSingleCollectionTrxView();
					addCollectionProcedureView(collectionProcedureView);
				    //Setter is explicitly called so that collectionTrxView list can also have elements
					collectionPaymentDetailsView.setSingleCollectionTrxView( collectionProcedureView );
					collectionList.add( collectionPaymentDetailsView ); 
					this.setCollectionList(  collectionList );
				}
				else
				{
					collectionPaymentDetailsView.setIsBeingEdited( 0L );
					CollectionProcedureView collectionProcedureView = collectionPaymentDetailsView.getCollectionTrxView().get(0);
					addCollectionProcedureView( collectionProcedureView );
					//Setter is explicitly called so that collectionTrxView list can also have elements
					collectionPaymentDetailsView.setSingleCollectionTrxView( collectionProcedureView );
				}
			
				this.setCollectionPaymentDetailsView( new CollectionPaymentDetailsView() );
				enableDisableForReplaceCheque(collectionPaymentDetailsView.getOldCollectionTrxId() != null );
			}
		} 
		catch (Exception ex) 
		{
			logger.LogException( "onAdd|Error Occured|",ex );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private void addCollectionProcedureView( CollectionProcedureView collectionProcedureView)throws Exception
	{
		collectionProcedureView.setHashCode(  collectionProcedureView.hashCode() );
		
		if(   collectionProcedureView.getSelectOneAssetId() != null && 
			! collectionProcedureView.getSelectOneAssetId().equals("-1") )
		{
			collectionProcedureView.setAssetId( Long.valueOf( collectionProcedureView.getSelectOneAssetId() ) );
		}
		HashMap<Long,AssetMemsView> map = getAssetMemsMap();
		if( map!= null && map.containsKey( Long.valueOf( collectionProcedureView.getSelectOneAssetId() ) ) )
		{
			AssetMemsView assetView = map.get( Long.valueOf( collectionProcedureView.getSelectOneAssetId() )  );
			collectionProcedureView.setAssetName(assetView.getAssetNameEn());
			collectionProcedureView.setAssetNameAr(assetView.getAssetNameAr());
			collectionProcedureView.setAssetTypeEn( assetView.getAssetTypeView().getAssetTypeNameEn());
			collectionProcedureView.setAssetTypeAr( assetView.getAssetTypeView().getAssetTypeNameAr());
		}
		 
		collectionProcedureView.setAmount( new Double ( this.getCollectionPaymentDetailsView().getAmount() ) );
		collectionProcedureView.setIsDeleted( 0L );
		
		
	}
	@SuppressWarnings( "unchecked" )
    public void onAssetNameChange()
	{
	  try
	  {
		collectionPaymentDetailsView = this.getCollectionPaymentDetailsView();
	    CollectionProcedureView collectionProcedureView = collectionPaymentDetailsView.getSingleCollectionTrxView();
	    if( collectionProcedureView.getSelectOneAssetId() == null || collectionProcedureView.getSelectOneAssetId().equals("-1") )
	    {
//	    	String assetId =collectionProcedureView.getSelectOneAssetId();
//			collectionProcedureView = new CollectionProcedureView();
//			collectionProcedureView.setSelectOneAssetId( assetId );
	    	collectionProcedureView.setAssetTypeEn( null );
		    collectionProcedureView.setAssetTypeAr( null );
		      
			return;
	    }
	      populateAssetDetailsInFields(collectionProcedureView);
	      setCollectionPaymentDetailsView(collectionPaymentDetailsView);
	    
	  
	  }
	  catch( Exception e )
	  {
		  logger.LogException( "onAssetNameChange| --- CRASHED --- ", e );
          errorMessages = new ArrayList<String>(0);
  		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	  }  
	}

	private void populateAssetDetailsInFields( CollectionProcedureView collectionProcedureView) throws Exception 
	{
		  AssetMemsView  asset = getAssetMemsFromMapByAssetId(  new Long ( collectionProcedureView.getSelectOneAssetId() ) );
	      collectionProcedureView.setAssetTypeEn( asset.getAssetTypeView().getAssetTypeNameEn() );
	      collectionProcedureView.setAssetTypeAr( asset.getAssetTypeView().getAssetTypeNameAr() );
	      collectionProcedureView.setAssetNameAr( asset.getAssetNameAr() );
		  collectionProcedureView.setAssetNameEn( asset.getAssetNameEn() );
	      if( asset.getAssetTypeView().getAssetTypeId().equals( WebConstants.AssetType.CASH ) )
	      {
	       collectionProcedureView.setAmount( asset.getAmount() );
	       collectionProcedureView.setAmountString( asset.getAmountString() );
	       collectionPaymentDetailsView.setAmount(asset.getAmount().toString() );   
	       collectionPaymentDetailsView.setPaymentMethod(WebConstants.PAYMENT_METHOD_CASH_ID.toString() );
	       onCollectedAsChanged();
	      }
	      else if( asset.getAssetTypeView().getAssetTypeId().longValue() == WebConstants.AssetType.CHEQUE.longValue() ||
	    		   asset.getAssetTypeView().getAssetTypeId().longValue() == WebConstants.AssetType.BANK_TRANSFER.longValue() 
	    		  )
	      {
		       if( asset.getAmount() != null  )
		       {
		    	 collectionProcedureView.setAmount( asset.getAmount() );
		    	 collectionProcedureView.setAmountString( asset.getAmountString() );
		         collectionPaymentDetailsView.setAmount(asset.getAmount().toString() );
		       }
		       if(asset.getBankView()!= null && asset.getBankView().getBankId() != null)
		       {
		        collectionPaymentDetailsView.setBankId( asset.getBankView().getBankId().toString() );
		       }
		       collectionPaymentDetailsView.setReferenceNo( asset.getFieldNumber() );
		       collectionPaymentDetailsView.setRefDate( asset.getDueDate() );
		       if (asset.getAssetTypeView().getAssetTypeId().longValue() == WebConstants.AssetType.CHEQUE.longValue() )
		       {
		           collectionPaymentDetailsView.setPaymentMethod(WebConstants.PAYMENT_METHOD_CHEQUE_ID.toString() );
		       }
		       else
		       {
		    	   collectionPaymentDetailsView.setPaymentMethod(WebConstants.PAYMENT_METHOD_BANK_TRANSFER_ID.toString() );
		       }
		       onCollectedAsChanged();  
	      }
	}
	
	@SuppressWarnings(  "unchecked"  )
	private void populateApplicationDetailsTab()throws Exception
	{
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		String status = "";
		requestView  = getRequestView(); 
		boolean isRequestNull =  null == requestView || null == requestView.getApplicantView() ||null == requestView.getRequestId();
		boolean isApplicantDiffFromPerson = isRequestNull || requestView.getApplicantView() == null || 
											requestView.getApplicantView().getPersonId() == null ||
											!requestView.getApplicantView().getPersonId().toString().equals(hdnPersonId);
		
		if ( isApplicantDiffFromPerson && hdnPersonName!= null && hdnPersonName.trim().length() >0 ) 
		{
			bean.populateApplicationDetails(
						                    "", 
						                    "", 
						                    new Date(), 
						                    "", 
						                    hdnPersonName, 
						                    "", 
						                    hdnCellNo, 
						                    ""
						                  );
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, hdnPersonId);
			PersonView person = new PersonView();
			person.setPersonId( new Long( hdnPersonId ) );
			requestView.setApplicantView( person);
			setRequestView (requestView);
			
            return;
		}
		
		status = CommonUtil.getIsEnglishLocale()?requestView.getStatusEn():requestView.getStatusAr();
		bean.populateApplicationDetails(
				                         requestView.getRequestNumber(), 
					                     status, 
					                     requestView.getRequestDate(), 
					                     requestView.getDescription(), 
					                     requestView.getApplicantView().getPersonFullName(), 
					                     "", 
					                     requestView.getApplicantView().getCellNumber(), 
					                     requestView.getApplicantView().getEmail()
					                   );
		if( requestView.getApplicantView() != null && requestView.getApplicantView().getPersonId() != null  )
		{
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,requestView.getApplicantView().getPersonId() );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, requestView.getApplicantView().getPersonId() );
		}
		if ( requestView!= null && requestView.getStatus() != null && 
			!requestView.getStatusDataValue().equals( WebConstants.REQUEST_STATUS_NEW ) 
		    )
		{
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		try	
		{
			if(hdnPersonType.equals( "APPLICANT" ) )
			{
				populateApplicationDetailsTab();
			}
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage") );
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromDistributePopUp()
	{
		
		try
		{
			if (sessionMap.containsKey("CollectionTransactionBatch"))
			{
				//CollectionProcedureView collView = (CollectionProcedureView) sessionMap.get("CollectionTransactionBatch");
				sessionMap.remove("CollectionTransactionBatch");
				CollectionPaymentDetailsView fromPopup = (CollectionPaymentDetailsView)sessionMap.remove( "PaymentDetailsView" );
				
				for (CollectionPaymentDetailsView row : this.getCollectionList() ) 
				{
				  if (fromPopup.getMyHashCode()== row.getMyHashCode() ) 
				  {
					List<CollectionPaymentDetailsView> list = this.getCollectionList();
					list.remove(row );
					list.add(fromPopup);
					this.setCollectionList(list);
					break;
				  }
				}
				successMessages.add(CommonUtil.getBundleMessage(WebConstants.COLLECTION_SUCCESS_DIST_INFO));
			}
		}
		catch( Exception e )
		{
			
			logger.LogException( "onMessageFromDistributePopUp--- CRASHED --- ", e );
            errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	private boolean hasOpenDistributePopUpError( CollectionPaymentDetailsView row  )throws Exception
	{
		boolean hasError      =  false;
		if( this.getFileCostCenter() == null  )
		{
			errorMessages.add( CommonUtil.getBundleMessage(  "disburseFinance.msg.associateCostCenter" ) );
			return true;
		}
		
		Double fileBalance    = PersonalAccountTrxService.getBalanceForCostCenter(this.getFileCostCenter());
		if( this.getOldFileCostCenter() != null && this.getOldFileCostCenter().trim().length() > 0 )
		{
			fileBalance += PersonalAccountTrxService.getBalanceForCostCenter(this.getOldFileCostCenter());
		}
		if( !row.getSingleCollectionTrxView().isDistributed() )
		{
			Double  blockingAmnt =  BlockingAmountService.getBlockingAmount(this.getFileOwnerId(), null, this.getInheritanceFileId(), null, null);
			int errorCode = validateBalance(fileBalance, blockingAmnt, 0.00d, Double.valueOf( row.getAmount() ), this.getFileOwnerId(), null,this.getInheritanceFileId(),WebConstants.MemsRequestType.COLLECTION_DISBURSEMENT);
			hasError  = errorCode > 0 ;
		}
		boolean hasARPostedToGRP = hasPaymentARPostedToGRP(row.getSingleCollectionTrxView().getCollectionTrxId() );
		if(!hasARPostedToGRP)
		{
			hasError = true;
		}
		return hasError;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onOpenDistributePopUp()
	{
		errorMessages = new ArrayList<String>();
		try
		{
			rowId= "";
			CollectionPaymentDetailsView selectedRow = (CollectionPaymentDetailsView) dtCollection.getRowData();
			
			if( hasOpenDistributePopUpError( selectedRow ) ) return;
			
			String collectionTrxId="" ;
			Boolean pageModeConfirm=(isPageModeConfirm()!=true? (isPageModeView()!=true?false:true) : true);
			if ( this.getRequestView().getRequestId() != null && selectedRow.getSingleCollectionTrxView().getCollectionTrxId() != null)
			{
				collectionTrxId= "collectionTrxId="+selectedRow.getSingleCollectionTrxView().getCollectionTrxId().toString()+"&";
			}
			String args = collectionTrxId+ 
			  (
					this.getInheritanceFileId() != null && selectedRow.getSingleCollectionTrxView().getAssetId() != null ? 
				    WebConstants.InheritanceFile.INHERITANCE_FILE_ID + "=" + this.getInheritanceFileId().toString()+ "&" + 
				    WebConstants.ASSET_ID+ "="+ selectedRow.getSingleCollectionTrxView().getAssetId().toString()
					: (
						this.getInheritanceFileId()!= null ? ( WebConstants.InheritanceFile.INHERITANCE_FILE_ID + "=" + this.getInheritanceFileId().toString() )
								: WebConstants.ASSET_ID+ "="+ selectedRow.getSingleCollectionTrxView().getAssetId().toString()
					  )
								
			  ) + "&"+Page_Mode.CONFIRM+"="+pageModeConfirm.toString();
			
			String javaScriptText = "javaScript:openDistributePopup('"+args+"');"; 
					

			sessionMap.put("CollectionTransactionBatch", selectedRow.getSingleCollectionTrxView() );
			sessionMap.put("PaymentDetailsView", selectedRow );

			sendToParent( javaScriptText );
		}
		catch( Exception e )
		{
			
			logger.LogException( "onOpenDistributePopUp| --- CRASHED --- ", e );
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
		
	@Deprecated
	@SuppressWarnings( "unchecked" )
	public void onOpenCollectionDetails()
	{
		String methodName = "onOpenCollectionDetails|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			if( this.getInheritanceFileId() != null )
			{
			     sessionMap.put( WebConstants.InheritanceFile.FILE_ID, this.getInheritanceFileId());
			}
			if( this.getAssetMemsId() != null )
			{
			     sessionMap.put(WebConstants.ASSET_ID,this.getAssetMemsId());
			}
			if( this.getRequestView() != null )
			{
				 
				 sessionMap.put( WebConstants.REQUEST, this.getRequestView() );
			}
			sessionMap.put(WebConstants.PAGE_MODE, this.getPageMode() );
			
			CollectionPaymentDetailsView obj = ( CollectionPaymentDetailsView )dtCollection.getRowData();
			if( obj != null )
			{
				sessionMap.put(WebConstants.CollectionProcedure.COLLECTION_PAYMENT_DETAILS_VIEW, obj );
			}
			
			sendToParent(  "openCollectionDetails();" );
		    
		    logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}



	public boolean hasErrors()throws Exception
	{
		boolean hasErrors = false;
		errorMessages = new ArrayList<String>(0);
		if( this.getCollectionList() == null || this.getCollectionList().size() <=  0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("collection.msg.paymentDetailsReq"));
			richPanel.setSelectedTab(TAB_DETAILS);
			hasErrors = true;	
		}
		
		hasErrors = hasEmptyCostCenterError();
		
		return hasErrors;
	}

	/**
	 * 
	 */
	private boolean hasEmptyCostCenterError() {
		if( 
				this.getRequestView().getRequestId() != null &&
				this.getRequestView().getInheritanceFileView() != null &&
			    (
			      this.getRequestView().getInheritanceFileView().getCostCenter() == null || 
				  this.getRequestView().getInheritanceFileView().getCostCenter().trim().length() <= 0
				  
			    )
			    &&
			    (
					      this.getRequestView().getInheritanceFileView().getOldCostCenter() == null || 
						  this.getRequestView().getInheritanceFileView().getOldCostCenter().trim().length() <= 0
						  
			    )
			  )
		{
				errorMessages.add( ResourceUtil.getInstance().getProperty( "collectionProcedure.msg.fileCostCenterEmpty" ) );
				return true;
		}
		else if ( getInheritanceFileId() != null)
		{
		   InheritanceFile file = EntityManager.getBroker().findById(InheritanceFile.class, getInheritanceFileId() );
		    if (file != null &&
				    (
				    		file .getCostCenter() == null || 
				    		file .getCostCenter().trim().length() <= 0
					  
				    )
				    &&
				    (
				    		file.getOldCostCenter() == null || 
				    		file.getOldCostCenter().trim().length() <= 0
							  
				    )
				  )
			{
					errorMessages.add( ResourceUtil.getInstance().getProperty( "collectionProcedure.msg.fileCostCenterEmpty" ) );
					return true;
			}
			
		}
		
		return false;
	} 
    @SuppressWarnings( "unchecked" )
	public void btnSave_Click() 
    {
		Long requestId = null;
		successMessages = new ArrayList<String>(0);
		try 
		{
			
			if ( hasErrors() ) 		
			{
				return ;
			}
			String eventDesc = "collectionProcedure.event.RequestSaved";
			if ( this.getRequestView() != null && this.getRequestView().getRequestId() != null ) 
			{
				
				successMessages.add(CommonUtil.getBundleMessage(WebConstants.COLLECTION_SUCCESS_COLL_UPDATED));
				requestId =this.getRequestView().getRequestId();
				eventDesc = "collectionProcedure.event.RequestUpdted";
			}
			else
			{
			    successMessages.add(CommonUtil.getBundleMessage(WebConstants.COLLECTION_SUCCESS_COLL_ADDED));
			}
			requestId = saveTransaction( requestId ,"");
			if(requestId!=null)
			{ 
				RequestView rv = new RequestView();
				rv.setRequestId( requestId );
				
				List<RequestView>requestViewList= new PropertyServiceAgent().getAllRequests(rv, null, null, null);
	            rv= requestViewList.get(0);
	            this.setRequestView(rv);
			    loadCollectionTransactionsAgainstRequest( requestId );
			    saveCommensAttachment( eventDesc );
			}
			if( !getPageMode().equals( Page_Mode.COLLECT_PAYMENT ) && !getPageMode().equals( Page_Mode.CONFIRM) )
			{
			 this.setPageMode( Page_Mode.ADD_EDIT );
			}
		} 
		catch (Exception ex) 
		{
			successMessages = new ArrayList<String>(0);
			errorMessages = new ArrayList<String>(0);
			logger.LogException( "btnSave_Click()|Error Occured",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		
	}

	private Long saveTransaction(Long requestId,String status) throws Exception 
	{
		CollectionProcedureService service = new CollectionProcedureService();
		try
		{
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			requestId = service.persistCollection( 
												   this.getRequestView(),
												   this.getCollectionList(),
												   CommonUtil.getLoggedInUser(), 
												   this.getInheritanceFileId() , 
					                               this.getAssetMemsId(),requestId,status 
					                              );
			ApplicationContext.getContext().getTxnContext().commit() ;
		} 
		catch (Exception ex) 
		{
			logger.LogException( "saveTransaction|Error..",ex);
			ApplicationContext.getContext().getTxnContext().rollback();
			throw ex;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
		return requestId;
	}

	private boolean hasChequeOrBankTransferDetailsError(CollectionPaymentDetailsView obj )
	{
		boolean hasError = false;
		rowId = "";
		
			if(  obj.getPaymentMethod().equals( WebConstants.PAYMENT_METHOD_BANK_TRANSFER_ID.toString() )  || 
				 obj.getPaymentMethod().equals( WebConstants.PAYMENT_METHOD_CHEQUE_ID.toString() )
			  )
			{
				if(  obj.getPaymentMethod().equals(WebConstants.PAYMENT_METHOD_CHEQUE_ID.toString() )  && 
						(  obj.getReferenceNo() == null || 
						   obj.getReferenceNo().trim().length() <=0  )
				  )
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty("collection.msg.refNumReq"));
					hasError = true;
				}
				if(  obj.getBankId() == null || 
					 obj.getBankId().trim().length() <= 0 ||  
			    	 obj.getBankId().trim().equals("-1")
				  )
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty("collection.msg.bankReq"));
					hasError = true;
				}
			
				if(  obj.getRefDate() == null )
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty("collection.msg.dateReq"));
					hasError = true;
				}
				if(  obj.getPaymentMethod().equals(WebConstants.PAYMENT_METHOD_BANK_TRANSFER_ID.toString() )  &&
					(  obj.getAccountNo() == null || 
					   obj.getAccountNo().trim().length() <= 0  	)
				   )
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty("collection.msg.accountReq"));
					hasError = true;
				}
			
			}
			if (hasError )
			{
				richPanel.setSelectedTab(TAB_DETAILS);
				if(obj.getPaymentDetailsId() != null )
				{
					rowId +=obj.getPaymentDetailsId().toString();
				}
		
			}
			
		
		return hasError;
	}

	public boolean hasOnCollectionErrors()throws Exception
	{
			boolean hasError = false;
			if( 
				this.getRequestView().getInheritanceFileView() != null &&
			    (
			      this.getRequestView().getInheritanceFileView().getCostCenter() == null || 
				  this.getRequestView().getInheritanceFileView().getCostCenter().trim().length() <= 0
				  
			    )
			    &&
			    (
					      this.getRequestView().getInheritanceFileView().getOldCostCenter() == null || 
						  this.getRequestView().getInheritanceFileView().getOldCostCenter().trim().length() <= 0
						  
			    )
			  )
			{
				errorMessages.add( ResourceUtil.getInstance().getProperty( "collectionProcedure.msg.fileCostCenterEmpty" ) );
				return true;
			}
			
			for (CollectionPaymentDetailsView obj : this.getCollectionList() ) 
			{
				hasError =hasChequeOrBankTransferDetailsError(obj);
				if(hasError)
					return hasError;
				
				
			}
			
//			((AttachmentBean)getBean("pages$attachment")).getProcedureDocs();
//			for (CollectionPaymentDetailsView obj : this.getCollectionList() ) 
//			{
//			
//				if (obj.getPaymentMethod().compareTo(WebConstants.PAYMENT_METHOD_CHEQUE) == 0 && !AttachmentBean.mandatoryDocsValidated())
//				{
//					errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Attachment.MSG_MANDATORY_DOCS)));
//					richPanel.setSelectedTab("attachmentTab");
//					hasError =true;
//				}
//			}
			
			
			return hasError;
	} 
	@SuppressWarnings ( "unchecked" )
	public void onMoveCollect()
	{
		
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		try 
		{
			boolean distributionRequired = true;
			if ( this.getRequestView() != null  && this.getRequestView().getRequestId() != null)  
			{
				if( this.getRequestView().getInheritanceFileView() != null )
				{
					boolean isDecOrAbsentFile = this.getRequestView().getInheritanceFileView().getFileTypeId().compareTo("161001")==0 ||
												 this.getRequestView().getInheritanceFileView().getFileTypeId().compareTo("161005")==0
							                    ;
					boolean isFinalized = (this.getRequestView().getInheritanceFileView().getStatusId().compareTo(162005L)==0);
					
					distributionRequired=  isDecOrAbsentFile && isFinalized;
					
				}
				boolean hasNonChequePayments =false;
				for (CollectionPaymentDetailsView obj: this.getCollectionList()) 
				{
					if( obj.getIsDeleted().longValue() == Constant.DEFAULT_IS_DELETED &&
						! obj.getPaymentMethod().equals( WebConstants.PAYMENT_METHOD_CHEQUE_ID.toString()  ) 
					  )
					{
						hasNonChequePayments =true;
						break;
					}
				}
				distributionRequired = distributionRequired && hasNonChequePayments;
				
				
				
				if(distributionRequired)
				{
					completeTask(TaskOutcome.OK);
				}
				else
				{
					completeTask(TaskOutcome.COMPLETE);
					
					
				}
				
				this.setPageMode( Page_Mode.VIEW );
			}
		} 
		catch (Exception ex) 
		{
			logger.LogException( "onMoveCollect|Error Occured:",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings ( "unchecked" )
	public void onCollect() 
    {
	
		errorMessages = new ArrayList<String>(0);
		successMessages = new ArrayList<String>(0);
		try 
		{
			if( hasOnCollectionErrors() )
				return;
			boolean distributionRequired = true;
			if ( this.getRequestView() != null  && this.getRequestView().getRequestId() != null)  
			{
				if( this.getRequestView().getInheritanceFileView() != null )
				{
					boolean isDecOrAbsentFile = this.getRequestView().getInheritanceFileView().getFileTypeId().compareTo("161001")==0 ||
												 this.getRequestView().getInheritanceFileView().getFileTypeId().compareTo("161005")==0
							                    ;
					boolean isFinalized = (this.getRequestView().getInheritanceFileView().getStatusId().compareTo(162005L)==0);
					
					distributionRequired=  isDecOrAbsentFile && isFinalized;
					
				}
				boolean hasNonChequePayments =false;
				for (CollectionPaymentDetailsView obj: this.getCollectionList()) 
				{
					if( obj.getIsDeleted().longValue() == Constant.DEFAULT_IS_DELETED &&
						! obj.getPaymentMethod().equals( WebConstants.PAYMENT_METHOD_CHEQUE_ID.toString()  ) 
					  )
					{
						hasNonChequePayments =true;
						break;
					}
				}
				distributionRequired = distributionRequired && hasNonChequePayments;
				collectPayments(distributionRequired);
				
				
				if(distributionRequired)
				{
					completeTask(TaskOutcome.OK);
					saveCommensAttachment("collectionProcedure.event.CollectionDone");
					successMessages.add(CommonUtil.getBundleMessage("collectionProcedure.msg.PaymentsCollectedSuccessfully"));
				}
				else
				{
					completeTask(TaskOutcome.COMPLETE);
					saveCommensAttachment("collectionProcedure.event.CollectionDoneRequestCompleted");
					successMessages.add(CommonUtil.getBundleMessage("collectionProcedure.msg.PaymentsCollectedSuccessfullyRequestCompleted"));
					
					
				}
				
				this.setPageMode( Page_Mode.VIEW );
			}
		} 
		catch (Exception ex) 
		{
			logger.LogException( "onCollect|Error Occured:",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	
    }
    
	private void collectPayments(boolean distributionRequired) throws Exception 
	{
	    try
	    {
			CollectionProcedureService service = new CollectionProcedureService();
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			service.persistCollection( this.getRequestView(),this.getCollectionList(), CommonUtil.getLoggedInUser(), this.getInheritanceFileId() , 
                    				   this.getAssetMemsId(),this.getRequestView().getRequestId(),""
                                     );
			service.collectPayments(  this.getRequestView().getRequestId() ,CommonUtil.getLoggedInUser(),distributionRequired);
			
			ApplicationContext.getContext().getTxnContext().commit();
		}
		catch ( Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release() ;
		}
	
	}
	
	private boolean isApproveValidated()throws Exception
	{
		RevenueFollowupService fuService = new RevenueFollowupService();
		boolean isValid = true;
		for (CollectionPaymentDetailsView obj : this.getCollectionList()) 
		{
		  String openedFollowup = fuService.getOpenFollowupForParameters(obj.getSingleCollectionTrxView().getAssetId(), null, null);
		  if(  openedFollowup == null ) { continue;}
		     
		  String message = java.text.MessageFormat.format( 
														 	ResourceUtil.getInstance().getProperty( "collectionProcedure.msg.openFollowupPresent" ),
														 	openedFollowup,
														 	obj.getSingleCollectionTrxView().getAssetNameAr()
													   	 );
		  errorMessages.add( message );
		  isValid =  false;
		}
		return isValid;
	}


	@SuppressWarnings("unchecked")
	public void onApprove() 
	{
		try 
		{
			if( !isApproveValidated() )
				return ;

            approveRejectInTransaction(  
            		
            							WebConstants.REQUEST_STATUS_COLLECTION_REQ,	
            							TaskOutcome.APPROVE
                                      );
	        CommonUtil.saveSystemComments(
	        							  WebConstants.REQUEST, 
	        		                      MessageConstants.RequestEvents.REQUEST_APPROVED,getRequestView().getRequestId()
	        		                     );
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.RequestEvents.REQUEST_APPROVED )  );
			
			this.setPageMode(Page_Mode.VIEW);
		}
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in onApprove()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void approveRejectInTransaction(String status, TaskOutcome taskOutcome) throws Exception 
	{
		CollectionProcedureService service = new CollectionProcedureService();
		try
		{
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			service.persistCollection( 
										this.getRequestView(),
										this.getCollectionList(), 
										CommonUtil.getLoggedInUser(), 
										this.getInheritanceFileId() , 
					                    this.getAssetMemsId(),
					                    this.getRequestView().getRequestId(),
					                    status 
					                 );
			completeTask(taskOutcome);
			ApplicationContext.getContext().getTxnContext().commit() ;
		} 
		catch (Exception ex) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw ex;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	private boolean isRejectValidated()throws Exception
	{
		
		boolean isValid = true;
		if( htmlRFC.getValue() == null || StringHelper.isEmpty( htmlRFC.getValue().toString().trim()  )  )
		{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.plzProvidecomments"));
			isValid = false;
		}
		return isValid;
	}

	@SuppressWarnings("unchecked")
	public void onReject() 
	{
		try 
		{
			if( !isRejectValidated() )
				return ;

            approveRejectInTransaction(  
            							WebConstants.REQUEST_STATUS_REJECTED,	
            							TaskOutcome.REJECT
                                      );
	        CommonUtil.saveSystemComments(
	        							  WebConstants.REQUEST, 
	        		                      MessageConstants.RequestEvents.REQUEST_REJECTED,
	        		                      getRequestView().getRequestId()
	        		                     );
	        CommonUtil.saveRemarksAsComments(
	        								 this.getRequestView().getRequestId(), 
	        								 htmlRFC.getValue().toString().trim(), 
	        								 WebConstants.REQUEST
	        								);
			successMessages.add( 
								 CommonUtil.getBundleMessage( MessageConstants.RequestEvents.REQUEST_REJECTED )  
							   );
			
			this.setPageMode(Page_Mode.VIEW);
		}
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in onReject()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	
	public boolean hasDistributionErrors()throws Exception
	{
		boolean hasErrors = false;
		errorMessages = new ArrayList<String>(0);
		//In case of underguardian and other single benef file it will not come to this step hence the 
		//check if for  <=0. For deceased and absent file it must be >0
		for (CollectionPaymentDetailsView obj : this.getCollectionList() ) 
		{
			if( !obj.getSingleCollectionTrxView().getPaymentMethodId().toString().equals("1") &&
				(
						obj.getSingleCollectionTrxView().getDistributionInfo() == null  || 
						obj.getSingleCollectionTrxView().getDistributionInfo().size() <=0
				)
			   )
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("collection.msg.oneCollectionDetailsReq"));
				richPanel.setSelectedTab(TAB_DETAILS);
				if(obj.getPaymentDetailsId() != null )
				{
					rowId +=obj.getPaymentDetailsId().toString();
				}
				hasErrors = true;
				break;
				
				
			}
			
		}
		return hasErrors;
	}
	@SuppressWarnings ( "unchecked" )
	public void onMOveDistribute() 
    {
	
		errorMessages = new ArrayList<String>(0);
		rowId ="";
		try 
		{
//			if( hasDistributionErrors() )
//			{
//				return ;
//			}
			if ( this.getRequestView() != null  && this.getRequestView().getRequestId() != null)  
			{
				completeTask(TaskOutcome.OK);
				this.setPageMode( Page_Mode.VIEW );
			}
		} 
		catch (Exception ex) 
		{
			logger.LogException( "onMOveDistribute|Error Occured:",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	
    }
	
	private boolean hasOncChangeAssetError() throws Exception
	{
		if ( selectOneAssetId == null || selectOneAssetId.equals( "-1" ) )
		{
			 errorMessages.add(ResourceUtil.getInstance().getProperty("openFileDistribute.msg.selectAsset"));
	  		 return true;
		
		}
		boolean hasSelected = false;
		for (CollectionPaymentDetailsView item : collectionList) 
		{
		   if ( item.getSelected()  == null || !item.getSelected() ) continue;
		   hasSelected = true;
		   break;
		}
		if( !hasSelected )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("commons.atleastoneselected"));
	  		return true;
		}
		return false;
	}

	
	@SuppressWarnings( "unchecked" )
	public void onChangeAsset()
	{
		errorMessages = new ArrayList<String>(0);
		try
		{
			if( hasOncChangeAssetError() ) return;
			collectionList = getCollectionList();
			String updatedBy = getLoggedInUserId();
			Long assetId = new Long (selectOneAssetId );
			requestView = this.getRequestView();
			
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			for (CollectionPaymentDetailsView item : collectionList) 
			{
				   if ( item.getSelected()  == null || !item.getSelected() ) continue;
			   	   
				   CollectionProcedureView collectionTrx = item.getSingleCollectionTrxView();
				   CollectionProcedureService.changeCollectionAsset(collectionTrx.getCollectionTrxId(), assetId,updatedBy );
				   AssetMems assetMems = EntityManager.getBroker().findById(AssetMems.class, assetId);
				   if( item.getSingleCollectionTrxView().getAssetId() == null  )
				   {
					   CommonUtil.saveSystemComments(
							   							WebConstants.REQUEST, 
							   							"collectionProcedure.msg.assetAddedAfterCollection", 
							   							requestView.getRequestId() , 
							   							assetMems.getAssetNameAr(),
							   							collectionTrx.getTrxNumber()
							   						);   
				   }
				   else 
				   {
					   CommonUtil.saveSystemComments(
							   							WebConstants.REQUEST, 
							   							"collectionProcedure.msg.assetReplacedAfterCollection", 
							   							requestView.getRequestId() ,
							   							collectionTrx.getAssetNameAr(),
							   							collectionTrx.getTrxNumber(),
							   							assetMems.getAssetNameAr()
							   						);   
				   }
					   
				   
			   }
			ApplicationContext.getContext().getTxnContext().commit();
			loadCollectionTransactionsAgainstRequest( requestView.getRequestId() );
			successMessages.add( CommonUtil.getBundleMessage( "commons.msg.assetChangedSuccessfully" ) );
		}
		catch( Exception e )
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException(   "onChangeAsset --- CRASHED --- ", e );
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	
	@SuppressWarnings ( "unchecked" )
	public void onDistribute() 
    {
	
		errorMessages = new ArrayList<String>(0);
		rowId ="";
		try 
		{
			if( hasDistributionErrors() )
			{
				return ;
			}
			if ( this.getRequestView() != null  && this.getRequestView().getRequestId() != null)  
			{
				distribute();
				saveCommensAttachment("collectionProcedure.msg.DistributionSuccessfully");
				completeTask(TaskOutcome.OK);
				loadCollectionTransactionsAgainstRequest( this.getRequestView().getRequestId() );
				successMessages.add(CommonUtil.getBundleMessage("collectionProcedure.msg.DistributionSuccessfully"));
				this.setPageMode( Page_Mode.VIEW );
			}
		} 
		catch (Exception ex) 
		{
			logger.LogException( "onDistribute|Error Occured:",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	
    }
	private void distribute() throws Exception 
	{
	    try
	    {
			CollectionProcedureService service = new CollectionProcedureService();
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			List<CollectionPaymentDetailsView > nonChequeList = new ArrayList<CollectionPaymentDetailsView>();
			for (CollectionPaymentDetailsView obj : this.getCollectionList() ) 
			{
				if( obj.getSingleCollectionTrxView().getPaymentMethod() != "1")
				{
					nonChequeList.add(obj);
				}
				
			}
			service.doDistribution(
									nonChequeList, 
									CommonUtil.getLoggedInUser(), 
									this.getInheritanceFileId() , 
	                  				this.getAssetMemsId(),
	                  				this.getRequestView().getRequestId() 
	                  			  );
			
			ApplicationContext.getContext().getTxnContext().commit();
		}
		catch ( Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release() ;
		}
	
	}
	
	private boolean isReviewValidated()throws Exception
	{
		
		boolean isValid = true;
		if( htmlRFC.getValue() == null || StringHelper.isEmpty( htmlRFC.getValue().toString().trim()  )  )
		{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.plzProvidecomments"));
			isValid = false;
		}
		return isValid;
	}

	@SuppressWarnings("unchecked")
	public void onReview() 
	{
		try 
		{
			if( !isReviewValidated() )
				return ;

			Long requestId =this.getRequestView().getRequestId();
			saveReview(requestId,WebConstants.REQUEST_STATUS_REVIEW_REQUIRED,true);
			completeTask(TaskOutcome.FORWARD);
	        CommonUtil.saveSystemComments(WebConstants.REQUEST, 
	        		                      MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW,getRequestView().getRequestId()
	        		                     );
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsPaymentDisbursementMsgs.REVIEW_REQUIRED)  );
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_VIEW_ONLY);
			this.setPageMode(Page_Mode.VIEW);
		}
		catch(Exception ex) 
		{
			logger.LogException("[Exception occured in review()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private Long saveReview(Long requestId,String status,boolean reviewRequired) throws Exception 
	{
		CollectionProcedureService service = new CollectionProcedureService();
		UtilityService utilityService = new UtilityService();
		try
		{
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			requestId = service.persistCollection( this.getRequestView(),this.getCollectionList(), CommonUtil.getLoggedInUser(), this.getInheritanceFileId() , 
					                               this.getAssetMemsId(),requestId,status 
					                              );
			if( reviewRequired )
			{
				ReviewRequestView reviewRequestView = new ReviewRequestView();
				reviewRequestView.setCreatedBy( CommonUtil.getLoggedInUser() );
				reviewRequestView.setCreatedOn( new Date() );
				reviewRequestView.setRfc( htmlRFC.getValue().toString().trim() );
	//			reviewRequestView.setGroupId( cmbReviewGroup.getValue().toString() );
				reviewRequestView.setRequestId( getRequestView().getRequestId() );
				utilityService.persistReviewRequest( reviewRequestView );
				
			}
			ApplicationContext.getContext().getTxnContext().commit() ;
			
		} 
		catch (Exception ex) 
		{
			logger.LogException( "addTransaction|Error..",ex);
			ApplicationContext.getContext().getTxnContext().rollback();
			throw ex;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
		return requestId;
	}
	@SuppressWarnings("unchecked")
	public void reviewDone() {
		try {
			UtilityService utilityService = new UtilityService();
			ReviewRequestView reviewRequestView = ( ReviewRequestView ) viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW );
			utilityService.persistReviewRequest( reviewRequestView );
			
			Long requestId =this.getRequestView().getRequestId();
			saveReview(requestId,WebConstants.REQUEST_STATUS_REVIEW_DONE,false);
			completeTask(TaskOutcome.OK);
			CommonUtil.saveSystemComments(WebConstants.REQUEST, 
					                      MessageConstants.RequestEvents.REQUEST_REVIEWED,getRequestView().getRequestId()
					                      );
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.
											MemsPaymentDisbursementMsgs.REVIEW_DONE));
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_VIEW_ONLY);
			this.setPageMode(Page_Mode.VIEW);
		} catch (Exception ex) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("[Exception occured in reviewDone()]", ex);
		}
	}
//	
//    
//    @SuppressWarnings ( "unchecked" )
//	public void btnConfirm_Click() 
//    {
//	
//		try 
//		{
//			if ( this.getRequestView() != null  && this.getRequestView().getRequestId() != null)  
//			{
//				completeCollectionRequestTransaction();
//				saveCommensAttachment("collectionProcedure.event.RequestConfirmed");
//				completeTask(TaskOutcome.OK);
//				successMessages.add(CommonUtil.getBundleMessage(WebConstants.COLLECTION_SUCCESS_CONFIRMED));
//				this.setPageMode( Page_Mode.VIEW );
//			}
//		} 
//		catch (Exception ex) 
//		{
//			logger.LogException( "btnConfirm_Click|Error Occured:",ex);
//			errorMessages = new ArrayList<String>(0);
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//		}
//	}
//    
//    
//	private void completeCollectionRequestTransaction() throws Exception 
//	{
//	    try
//	    {
//			CollectionProcedureService service = new CollectionProcedureService();
//			ApplicationContext.getContext().getTxnContext().beginTransaction();
//			service.confirmCollectionRequest( this.getRequestView().getRequestId() ,CommonUtil.getLoggedInUser());
//			
//			ApplicationContext.getContext().getTxnContext().commit();
//		}
//		catch ( Exception e)
//		{
//			ApplicationContext.getContext().getTxnContext().rollback();
//			throw e;
//		}
//		finally
//		{
//			ApplicationContext.getContext().getTxnContext().release() ;
//		}
//	
//	}
////	
//    @SuppressWarnings ( "unchecked" )
//	public void onConfirmingBankTransfer() 
//    {
//    	Long requestId = null;
//		try 
//		{
//			if( hasErrors() )
//				return;
//			requestId = onConfirmBankTransferTransaction();
//			if(requestId!=null )
//			{ 
//				RequestView rv = new RequestView();
//				rv.setRequestId( requestId );
//				List<RequestView>requestViewList= new PropertyServiceAgent().getAllRequests(rv, null, null, null);
//	            rv= requestViewList.get(0);
//	            this.setRequestView(rv);
//			    loadCollectionTransactionsAgainstRequest( requestId );
//			    saveCommensAttachment("collectionProcedure.event.RequestConfirmed");
//			}
//			successMessages.add(CommonUtil.getBundleMessage(WebConstants.COLLECTION_SUCCESS_CONFIRMED));
//			this.setPageMode( Page_Mode.VIEW );
//		} 
//		catch (Exception ex) 
//		{
//			logger.LogException( "btnConfirm_Click|Error Occured:",ex);
//			errorMessages = new ArrayList<String>(0);
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
//		}
//	}

//	private Long onConfirmBankTransferTransaction()throws Exception 
//	{
//		Long requestId=null;
//		CollectionProcedureService service = new CollectionProcedureService();
//		try
//		{
//			ApplicationContext.getContext().getTxnContext().beginTransaction();
//			requestId = service.persistCollection( this.getRequestView(),this.getCollectionList(), CommonUtil.getLoggedInUser(), this.getInheritanceFileId() ,
//						                           this.getAssetMemsId(),requestId,"" 
//						                         );
//			service.submitRequest(requestId, CommonUtil.getLoggedInUser());
//			service.confirmCollectionRequest(requestId, CommonUtil.getLoggedInUser());
//			ApplicationContext.getContext().getTxnContext().commit();
//		}
//		catch( Exception e)
//		{
//			ApplicationContext.getContext().getTxnContext().rollback();
//			throw e;
//		}
//		finally
//		{
//			ApplicationContext.getContext().getTxnContext().release();
//		}
//		
//		return requestId;
//	}

	@SuppressWarnings ( "unchecked" )
	public void openPaymentReceipt()
	{
	    try
	    {
			CollectionPaymentDetailsView row = (CollectionPaymentDetailsView)dtCollection.getRowData();
			logger.logInfo("openPaymentReceipt|Payment Details Id:%s|RequestId:%s", row.getPaymentDetailsId(), this.getRequestView().getRequestId() );
			CommonUtil.printMEMSPaymentReceipt( this.getRequestView().getRequestId().toString(),row.getPaymentDetailsId().toString(), getFacesContext());
	    }
	    catch( Exception e )
	    {
	    	logger.LogException( "openPaymentReceipt|Error Occured:",e);
	    	errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	    }
	}
	@SuppressWarnings ( "unchecked" )
	public Boolean completeTask(TaskOutcome taskOutcome) throws Exception 
	{
		UserTask userTask = null;
		Boolean success = true;
		String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;   	        
        if(viewMap.containsKey(TASK_LIST_USER_TASK))
        {
			userTask = (UserTask) viewMap.get( TASK_LIST_USER_TASK );
			BPMWorklistClient client = new BPMWorklistClient(contextPath);
			if( userTask!=null )
			{
				client.completeTask(userTask, CommonUtil.getLoggedInUser(), taskOutcome);
			}
        }
		return success;
	}
	@SuppressWarnings ( "unchecked" )
	public void btnSubmit_Click() 
	{
		
		try 
		{
			if( hasErrors() || this.getRequestView() == null || this.getRequestView().getRequestId() == null )
			{
				return;
			}
			submitRequestTransaction();
			saveCommensAttachment("collectionProcedure.event.RequestSubmitted");
			MEMSCollectionBPELPortClient bpelPortClient= new MEMSCollectionBPELPortClient();
			bpelPortClient.setEndpoint( SystemParameters.getInstance().getParameter( WebConstants.CollectionProcedure.BPEL_END_POINT ) );
			bpelPortClient.initiate(this.getRequestView().getRequestId(), CommonUtil.getLoggedInUser(), null, null);
			CommonUtil.printMemsReport( this.getRequestView().getRequestId() );
			this.setPageMode( Page_Mode.VIEW);
			successMessages.add( CommonUtil.getBundleMessage( WebConstants.COLLECTION_SUCCESS_COLL_SUBMITTED ) );
			
		}
		catch (Exception ex) 
		{
			logger.LogException( "btnSubmit_Click|Error Occured:",ex);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
    public void onPrint()
    {
    	try
    	{
    		CommonUtil.printMemsReport( this.getRequestView().getRequestId() );
    	}
		catch (Exception ex) 
		{
			logger.LogException( "onPrint|Error Occured:",ex);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
    	
    }
	private void submitRequestTransaction() throws Exception 
	{
		CollectionProcedureService service = new CollectionProcedureService();
		try
		{
		  ApplicationContext.getContext().getTxnContext().beginTransaction();
		  //Save Request Details
		  service.persistCollection(
				  					 this.getRequestView(),					
				  					 this.getCollectionList(), 
				  					 CommonUtil.getLoggedInUser(), 
				  					 this.getInheritanceFileId() , 
                  					 this.getAssetMemsId(),
                  					 this.getRequestView().getRequestId(),
                  					 WebConstants.REQUEST_STATUS_COLLECTION_REQ
                  				   );
		  service.submitRequest( this.getRequestView().getRequestId(),CommonUtil.getLoggedInUser() );
		  ApplicationContext.getContext().getTxnContext().commit();
		}
		catch(Exception ex)
		{
		  ApplicationContext.getContext().getTxnContext().rollback();
		  throw ex;
		}
		finally
		{
		  ApplicationContext.getContext().getTxnContext().release();
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onMessageFromCollectionPaymentDetailsPopup()
	{
		
		if( sessionMap.get( WebConstants.CollectionProcedure.COLLECTION_PAYMENT_DETAILS_VIEW ) != null )
		{
			CollectionPaymentDetailsView message = ( CollectionPaymentDetailsView )sessionMap.get( WebConstants.CollectionProcedure.COLLECTION_PAYMENT_DETAILS_VIEW ); 
			sessionMap.remove( WebConstants.CollectionProcedure.COLLECTION_PAYMENT_DETAILS_VIEW );
			for (CollectionPaymentDetailsView element : collectionList ) {
				if( element.getMyHashCode() == message.getMyHashCode() )
				{
					successMessages = new ArrayList<String>();
					List<CollectionPaymentDetailsView> list = this.getCollectionList();
					list.remove(element);
					list.add(message);
					successMessages.add(CommonUtil.getBundleMessage( "collection.msg.collectionDetailsAdded" ) ) ;
					break;
				}
				
			}
		}
		
	}

	public boolean getIsViewModePopUp() {
		boolean returnVal = false;

		if (viewMap.containsKey(VIEW_MODE)
				&& viewMap.get(VIEW_MODE) != null) {
			if (getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())
				returnVal = true;
		}

		else {
			returnVal = false;
		}

		return returnVal;

	}

	public boolean getIsInheritanceFileAvailable() {
		boolean returnVal = false;

		if (this.getInheritanceFileId() != null)
		{

			returnVal = true;
		}

		else {
			returnVal = false;
		}

		return returnVal;
		// return true ;//just to check

	}

	public boolean getIsPageModeSelectOnePopUp() {
		isPageModeSelectOnePopUp = false;
		if (viewMap.containsKey(VIEW_MODE)
				&& viewMap.get(VIEW_MODE) != null
				&& viewMap.get(VIEW_MODE).toString().trim()
						.equalsIgnoreCase(MODE_SELECT_ONE_POPUP)) {
			isPageModeSelectOnePopUp = true;
		}
		return isPageModeSelectOnePopUp;

	}

	public boolean getIsPageModeSelectManyPopUp() {
		isPageModeSelectManyPopUp = false;
		if (viewMap.containsKey(VIEW_MODE)
				&& viewMap.get(VIEW_MODE) != null
				&& viewMap.get(VIEW_MODE).toString().trim()
						.equalsIgnoreCase(MODE_SELECT_MANY_POPUP)) {
			isPageModeSelectManyPopUp = true;
		}
		return isPageModeSelectManyPopUp;
	}

	public List<SelectItem> getAssetTypesList() {
		List<SelectItem> assetTypes = new ArrayList<SelectItem>();
		try {

			assetTypes = CommonUtil.getAssetTypesList();

		} catch (PimsBusinessException e) {
			logger.LogException("getAssetTypesList() crashed", e);
		}
		return assetTypes;
	}


	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

		
	@SuppressWarnings("unchecked")
	private void openFromTaskList()throws Exception
	{
		UserTask userTask = null;
		Long requestId = 0L;
		userTask = (UserTask) sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
		viewMap.put(TASK_LIST_USER_TASK,userTask);
		viewMap.put(TASK_TYPE, userTask.getTaskType());
		if(userTask!=null)
		{
			Map taskAttributes =  userTask.getTaskAttributes();				
			if(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID)!=null)
			{
				requestId = Convert.toLong(taskAttributes.get(WebConstants.UserTasks.REQUEST_ID));
				viewMap.put("requestId",requestId);
				viewMap.put(WebConstants.REQUEST_ID, requestId);
				if(requestId!=null)
				{
				    RequestView rv = new RequestView();
					rv.setRequestId( requestId );
					List<RequestView>requestViewList= new PropertyServiceAgent().getAllRequests(rv, null, null, null);
                    rv= requestViewList.get(0);
                    this.setRequestView(rv);
                    populateApplicationDetailsTab();
                    setPageModeBasedOnRequestStatus();
				    loadCollectionTransactionsAgainstRequest(requestId);
				    loadAttachmentsAndComments(requestId);
				}
						
			}
		}
	}
	
	public String sendToParent(String javaScript) throws Exception 
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText = javaScript;
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,AddResource.HEADER_BEGIN, javaScriptText);
		return "";
	}

	public String getSuccessMessages() {

		String messageList = "";
		if ((successMessages == null) || (successMessages.size() == 0)) {
			messageList = "";
		} else {
			for (String message : successMessages) {
				messageList += message + " <br></br>";
			}

		}

		return messageList;

	}


	public HtmlInputText  getHtmlFileStatus() {
		return htmlFileStatus;
	}

	public void setHtmlFileStatus(HtmlInputText  htmlFileStatus) {
		this.htmlFileStatus = htmlFileStatus;
	}

	public HtmlSelectOneMenu getCollectedAsSelectMenu() {
		return collectedAsSelectMenu;
	}

	public void setCollectedAsSelectMenu(HtmlSelectOneMenu collectedAsSelectMenu) {
		this.collectedAsSelectMenu = collectedAsSelectMenu;
	}
	@SuppressWarnings( "unchecked" )
	public List<SelectItem> getCollectedAsList() {

		collectedAsList = (ArrayList<SelectItem>) viewMap
				.get("collectedAsList");
		if (collectedAsList == null)
			collectedAsList = new ArrayList<SelectItem>();
		return collectedAsList;
	}
	@SuppressWarnings( "unchecked" )
	public void setCollectedAsList(List<SelectItem> collectedAsList) {
		this.collectedAsList = collectedAsList;
		if (collectedAsList != null) {
			viewMap.put("collectedAsList", collectedAsList);
		}
	}



	public HtmlInputText  getHtmlFileType() {
		return htmlFileType;
	}

	public void setHtmlFileType(HtmlInputText htmlFileType) {
		this.htmlFileType = htmlFileType;
	}

	@SuppressWarnings("unchecked")
	public RequestView getRequestView() {

		if (viewMap.containsKey(WebConstants.REQUEST_VIEW))
			requestView = (RequestView) viewMap.get(WebConstants.REQUEST_VIEW);

		return requestView;
	}

	@SuppressWarnings("unchecked")
	public void setRequestView(RequestView requestView) {

		this.requestView = requestView;
		if (this.requestView != null)
			viewMap.put(WebConstants.REQUEST_VIEW, this.requestView);
	}
	@SuppressWarnings( "unchecked" )
	public Long getAssetMemsId() {
		if ( viewMap.get( WebConstants.ASSET_ID ) != null )
			assetMemsId = new Long ( viewMap.get( WebConstants.ASSET_ID ).toString()   );
		return assetMemsId;
	}
	@SuppressWarnings( "unchecked" )
	public void setAssetMemsId(Long assetMemsId) 
	{
		this.assetMemsId = assetMemsId;
		if( this.assetMemsId!= null  )
			viewMap.put( WebConstants.ASSET_ID ,this.assetMemsId ); 
	}
	@SuppressWarnings( "unchecked" )
	public Long getInheritanceFileId() {
		if ( viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null )
			inheritanceFileId = new Long ( viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID).toString()   );
		return inheritanceFileId;
	}
	@SuppressWarnings( "unchecked" )
	public void setInheritanceFileId(Long inheritanceFileId) {
		this.inheritanceFileId = inheritanceFileId;
		if( this.inheritanceFileId!= null  )
			viewMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_ID,this.inheritanceFileId); 
	}

	@SuppressWarnings( "unchecked" )
	public String getPageMode() {

	if( viewMap.get(Page_Mode.PAGE_MODE )!=null )
	{
		pageMode = viewMap.get(Page_Mode.PAGE_MODE ).toString();
	}
	return pageMode;
	}
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
	this.pageMode = pageMode;
	if( this.pageMode!=null )
	{
		
		viewMap.put(Page_Mode.PAGE_MODE, this.pageMode);
	}
	}


	@SuppressWarnings( "unchecked" )
	public DomainDataView getRequestStatusFromType( String type)
{
	String viewKey = "RequestStatus"+type;
	 DomainDataView ddv;
	 if ( viewMap.get( viewKey ) != null )
	 {
		 ddv = (DomainDataView)viewMap.get( viewKey );
	 }
	 else
	 {
		 
		 ddv = CommonUtil.getIdFromType( this.getRequestStatusList(), type );
		 viewMap.put( viewKey,ddv);
	 
	 } 

   return ddv;
}

@SuppressWarnings( "unchecked" )
public List<DomainDataView> getRequestStatusList()
{
	 List<DomainDataView >ddvList;
	 String viewKey = "RequestStatusList";
	 if ( viewMap.get( viewKey ) != null )
	 {
		 ddvList = (ArrayList<DomainDataView>)viewMap.get( viewKey );
	 }
	 else
	 {
		 
		 ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.REQUEST_STATUS);
		 viewMap.put( viewKey,ddvList );
	 
	 } 

   return ddvList;
}

@SuppressWarnings( "unchecked" )
public void onOpenFile()
{
	String methodName = "onOpenFile|";
	try	
	{	
		logger.logInfo(methodName + " --- STARTED --- ");
		sessionMap.put( WebConstants.InheritanceFile.FILE_ID, this.getInheritanceFileId());
	    sessionMap.put (WebConstants.InheritanceFilePageMode.IS_POPUP,true);
		String javaScriptText ="var screen_width = 900;"+
        "var screen_height =600;"+
        "window.open('inheritanceFile.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes,resizable=true');";
		sendToParent(  javaScriptText );
	    
	    logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
	}
	catch (Exception exception) 
	{
		logger.LogException(methodName + " --- CRASHED --- ", exception);
		errorMessages = new ArrayList<String>(0);
		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	}
	
}

public HtmlInputText getHtmlFilePerson() {
	return htmlFilePerson;
}

public void setHtmlFilePerson(HtmlInputText htmlFilePerson) {
	this.htmlFilePerson = htmlFilePerson;
}

public HtmlInputText getHtmlFileDate() {
	return htmlFileDate;
}

public void setHtmlFileDate(HtmlInputText htmlFileDate) {
	this.htmlFileDate = htmlFileDate;
}

@SuppressWarnings( "unchecked" )
public CollectionPaymentDetailsView getCollectionPaymentDetailsView() {
	if(  viewMap.get( "collectionPaymentDetailsView" )!= null )
	{
		collectionPaymentDetailsView  = ( CollectionPaymentDetailsView  )viewMap.get( "collectionPaymentDetailsView" );
	}
	return collectionPaymentDetailsView;
}
@SuppressWarnings( "unchecked" )
public void setCollectionPaymentDetailsView(CollectionPaymentDetailsView collectionPaymentDetailsView) 
{
	this.collectionPaymentDetailsView = collectionPaymentDetailsView;
	if( this.collectionPaymentDetailsView != null )
	{
		
		viewMap.put("collectionPaymentDetailsView", this.collectionPaymentDetailsView );
	}
}
@SuppressWarnings( "unchecked" )
public List<CollectionPaymentDetailsView> getCollectionList() 
{
	
	
	if( viewMap.get("paymentcollectionList" ) != null )
	{
		collectionList = ( List<CollectionPaymentDetailsView> )viewMap.get("paymentcollectionList" );
	}
	return collectionList;
	
}

@SuppressWarnings( "unchecked" )
public void setCollectionList(List<CollectionPaymentDetailsView> collectionList) {
	this.collectionList = collectionList;
	if( this.collectionList != null )
	{
		Collections.sort(this.collectionList, new ListComparator("referenceNo",true));
		totalCollectionAmount = 0.0D;
		totalCollectionCount = 0d;
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		for (CollectionPaymentDetailsView obj : collectionList) 
		{
			try
			{
			  totalCollectionAmount = totalCollectionAmount + Double.parseDouble( oneDForm.format( oneDForm.parse( obj.getAmount() ) ) );
			}
			catch(Exception e)
			{
				logger.LogException("Error occured while counting total:", e );
			}
			
			totalCollectionCount++;
		}
		setTotalCollectionAmount(totalCollectionAmount);
		setTotalCollectionCount(totalCollectionCount);
		viewMap.put("paymentcollectionList", this.collectionList );
	}
}

public HtmlDataTable getDtCollection() {
	return dtCollection;
}

public void setDtCollection(HtmlDataTable dtCollection) {
	this.dtCollection = dtCollection;
}

public HtmlGraphicImage getImgDeleteCollection() {
	return imgDeleteCollection;
}

public void setImgDeleteCollection(HtmlGraphicImage imgDeleteCollection) {
	this.imgDeleteCollection = imgDeleteCollection;
}

public HtmlGraphicImage getImgEditCollection() {
	return imgEditCollection;
}

public void setImgEditCollection(HtmlGraphicImage imgEditCollection) {
	this.imgEditCollection = imgEditCollection;
}

public HtmlCommandButton getBtnSave() {
	return btnSave;
}

public void setBtnSave(HtmlCommandButton btnSave) {
	this.btnSave = btnSave;
}

public HtmlCommandButton getBtnSubmit() {
	return btnSubmit;
}

public void setBtnSubmit(HtmlCommandButton btnSubmit) {
	this.btnSubmit = btnSubmit;
}

public HtmlCommandButton getBtnConfirm() {
	return btnConfirm;
}

public void setBtnConfirm(HtmlCommandButton btnConfirm) {
	this.btnConfirm = btnConfirm;
}

public HtmlGraphicImage getImgPrintPaymentReceipt() {
	return imgPrintPaymentReceipt;
}

public void setImgPrintPaymentReceipt(HtmlGraphicImage imgPrintPaymentReceipt) {
	this.imgPrintPaymentReceipt = imgPrintPaymentReceipt;
}

public HtmlInputText getTxtRefNum() {
	return txtRefNum;
}

public void setTxtRefNum(HtmlInputText txtRefNum) {
	this.txtRefNum = txtRefNum;
}

public HtmlInputText getTxtAccNum() {
	return txtAccNum;
}

public void setTxtAccNum(HtmlInputText txtAccNum) {
	this.txtAccNum = txtAccNum;
}

public HtmlSelectOneMenu getCmbBank() {
	return cmbBank;
}

public void setCmbBank(HtmlSelectOneMenu cmbBank) {
	this.cmbBank = cmbBank;
}

public HtmlCalendar getRefDate() {
	return refDate;
}

public void setRefDate(HtmlCalendar refDate) {
	this.refDate = refDate;
}

public HtmlInputTextarea getHtmlRFC() {
	return htmlRFC;
}

public void setHtmlRFC(HtmlInputTextarea htmlRFC) {
	this.htmlRFC = htmlRFC;
}

public HtmlTabPanel getRichPanel() {
	return richPanel;
}

public void setRichPanel(HtmlTabPanel richPanel) {
	this.richPanel = richPanel;
}

public String getRowId() {
	return rowId;
}

public void setRowId(String rowId) {
	this.rowId = rowId;
}

@SuppressWarnings("unchecked")
public Long getFileOwnerId() 
{
	if ( viewMap.get( "fileOwnerId" ) != null )
	{
		fileOwnerId = new Long ( viewMap.get( "fileOwnerId" ).toString()   );
	}
	return fileOwnerId;
}

@SuppressWarnings("unchecked")
public void setFileOwnerId(Long fileOwnerId) 
{
	this.fileOwnerId = fileOwnerId;
	if( this.fileOwnerId != null )
	{
		viewMap.put( "fileOwnerId", this.fileOwnerId  );
	}
}

@SuppressWarnings("unchecked")
public String getFileCostCenter() 
{

	if ( viewMap.get( "fileCostCenter") != null )
	{
		fileCostCenter = viewMap.get( "fileCostCenter") .toString();
	}
	return fileCostCenter;
}
@SuppressWarnings( "unchecked" )
public void setFileCostCenter(String fileCostCenter) {
	this.fileCostCenter = fileCostCenter;
	if( this.fileCostCenter != null )
	{
		viewMap.put( "fileCostCenter", this.fileCostCenter );
	}
}

@SuppressWarnings("unchecked")
public String getOldFileCostCenter() 
{

	if ( viewMap.get( "oldfileCostCenter") != null )
	{
		oldFileCostCenter= viewMap.get( "oldfileCostCenter") .toString();
	}
	return oldFileCostCenter;
}
@SuppressWarnings( "unchecked" )
public void setOldFileCostCenter(String oldfileCostCenter) {
	this.oldFileCostCenter= oldfileCostCenter;
	if( this.oldFileCostCenter!= null )
	{
		viewMap.put( "oldfileCostCenter", this.oldFileCostCenter);
	}
}

public String getSelectOneAssetId() {
	return selectOneAssetId;
}

public void setSelectOneAssetId(String selectOneAssetId) {
	this.selectOneAssetId = selectOneAssetId;
}

public Double getTotalCollectionCount() {
	
	if( viewMap.get("totalCollectionCount") != null )
	{
		totalCollectionCount = Double.parseDouble(  viewMap.get("totalCollectionCount").toString() ) ; 
	}
	else
	{
		totalCollectionCount = 0.0d;
	}
	return totalCollectionCount;
}

@SuppressWarnings("unchecked")
public void setTotalCollectionCount(Double totalCollectionCount) {
	this.totalCollectionCount = totalCollectionCount;
	if(this.totalCollectionCount != null)
	{
		viewMap.put("totalCollectionCount", this.totalCollectionCount);
	}
}

public Double getTotalCollectionAmount() {
	if( viewMap.get("totalCollectionAmount") != null )
	{
		totalCollectionAmount = Double.parseDouble(  viewMap.get("totalCollectionAmount").toString() ) ; 
	}
	else
	{
		totalCollectionAmount = 0.0d;
	}
	return totalCollectionAmount;
}

@SuppressWarnings("unchecked")
public void setTotalCollectionAmount(Double totalCollectionAmount) {
	this.totalCollectionAmount = totalCollectionAmount;
	if(this.totalCollectionAmount != null)
	{
		viewMap.put("totalCollectionAmount", this.totalCollectionAmount);
	}
}

public HtmlSelectOneMenu getCmbAssets() {
	return cmbAssets;
}

public void setCmbAssets(HtmlSelectOneMenu cmbAssets) {
	this.cmbAssets = cmbAssets;
}

public HtmlInputText getTxtAmount() {
	return txtAmount;
}

public void setTxtAmount(HtmlInputText txtAmount) {
	this.txtAmount = txtAmount;
}

public String getHdnPersonId() {
	return hdnPersonId;
}

public void setHdnPersonId(String hdnPersonId) {
	this.hdnPersonId = hdnPersonId;
}

public String getHdnPersonType() {
	return hdnPersonType;
}

public void setHdnPersonType(String hdnPersonType) {
	this.hdnPersonType = hdnPersonType;
}

public String getHdnPersonName() {
	return hdnPersonName;
}

public void setHdnPersonName(String hdnPersonName) {
	this.hdnPersonName = hdnPersonName;
}

public String getHdnCellNo() {
	return hdnCellNo;
}

public void setHdnCellNo(String hdnCellNo) {
	this.hdnCellNo = hdnCellNo;
}

public String getHdnIsCompany() {
	return hdnIsCompany;
}

public void setHdnIsCompany(String hdnIsCompany) {
	this.hdnIsCompany = hdnIsCompany;
}

 

}
