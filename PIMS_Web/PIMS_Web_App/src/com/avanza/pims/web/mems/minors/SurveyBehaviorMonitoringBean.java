package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.util.Logger;
import com.avanza.pims.constant.Constant.SurveyForm;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.plugins.SurveyInstanceHistoryTabBacking;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.FamilyVillageFileService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.ui.util.ResourceUtil;


public class SurveyBehaviorMonitoringBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(SurveyBehaviorMonitoringBean.class);
	
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private String VIEW_MODE = "pageMode";
	private String PAGE_MODE_POPUP = "MODE_SELECT_ONE_POPUP";
	private static final String TAB_SURVEYS ="tabSurveys";
    private String pageTitle;
    private String pageMode;
    private String txtRemarks;
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	FamilyVillageFileService fvService = new FamilyVillageFileService();
	private PersonView beneficiaryPerson = new PersonView();

     public SurveyBehaviorMonitoringBean(){
    	 
     }
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.Attachment.EXTERNAL_ID_PERSON);
		viewMap.put("noteowner", WebConstants.NOTES_OWNER_PERSON);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
		if(request.getParameter(VIEW_MODE)!=null && request.getParameter(VIEW_MODE).equals(PAGE_MODE_POPUP)) 
	   	{ 	      	    		
	 	   viewMap.put(VIEW_MODE, PAGE_MODE_POPUP);
	 	}
	   	if(request.getParameter(WebConstants.PERSON_ID)!=null) 
	   	{ 	      	    		
	   		beneficiaryPerson.setPersonId( Long.valueOf(request.getParameter(WebConstants.PERSON_ID).toString()) );
	    }
	   	else if(sessionMap.get(WebConstants.PERSON_ID )!= null )
	   	{
	   		
	   		beneficiaryPerson.setPersonId( Long.valueOf(sessionMap.remove(WebConstants.PERSON_ID).toString() ) );
	   	}
	   	isViewModePopUp();
		getData();
		loadAttachmentsAndComments( beneficiaryPerson.getPersonId() );
		//First Tab Click Action will be invoked below.
		onSurveyHistoryTab(); 
		updateValuesFromMap();
	}
	@SuppressWarnings("unchecked")
	private void getErrorMessagesFromTab() throws Exception
	{
		if(viewMap.get(WebConstants.SurveyBehaviorMonitoringFormKeys.SURVEY_HISTORY_TAB_ERRORS) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.SurveyBehaviorMonitoringFormKeys.SURVEY_HISTORY_TAB_ERRORS);
			errorMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_SURVEYS);
		}
	
		else if(viewMap.get(WebConstants.SurveyBehaviorMonitoringFormKeys.SURVEY_HISTORY_TAB_SUCCESS) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(WebConstants.SurveyBehaviorMonitoringFormKeys.SURVEY_HISTORY_TAB_SUCCESS);
			successMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_SURVEYS);
		}
	}
	public void prerender(){
		try{
				getErrorMessagesFromTab();
				
		}
		catch ( Exception e )
		{
			logger.LogException( "prerender|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.ManageBeneficiaryFamilyVillageProcess.PERSON ) != null )
		{
			beneficiaryPerson= ( PersonView)viewMap.get( WebConstants.ManageBeneficiaryFamilyVillageProcess.PERSON) ;
		}
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( beneficiaryPerson != null )
		{
		  viewMap.put(  WebConstants.ManageBeneficiaryFamilyVillageProcess.PERSON , beneficiaryPerson);
		}
			

	}
	@SuppressWarnings( "unchecked" )
	private void getData()throws Exception
	{
		beneficiaryPerson = fvService.getBeneficiaryPersonById(beneficiaryPerson.getPersonId());
		
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	
	
	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(beneficiaryPerson.getPersonId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.NOTES_OWNER_PERSON;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, beneficiaryPerson.getPersonId());
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.NOTES_OWNER_PERSON;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(beneficiaryPerson.getPersonId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, beneficiaryPerson.getPersonId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
			 loadAttachmentsAndComments( beneficiaryPerson.getPersonId() );
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	
	public void onMessageFromAddSurvey()
	{
		try	
		{
            updateValuesFromMap();       
//            FamilyVillageFamilyAspectsTabBacking bean = (FamilyVillageFamilyAspectsTabBacking)getBean("pages$tabFamilyVillageFamilyAspects");
//            bean.populateTab(beneficiaryPerson);
            updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromAddSurvey|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings("unchecked")
	public String onBackToCallingPage()
	{
		try	
		{
            updateValuesFromMap();  
            Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
            sessionMap.put(WebConstants.PERSON_ID, beneficiaryPerson.getPersonId());
            sessionMap.put("CONTROL_TRANSFERRED_FROM", "SURVEY_BEHAVIOR_MONITORING");
            return "ResearchFamilyVillageBeneficiary";
		}
		catch(Exception ex)
		{
			logger.LogException("onBackToCallingPage|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		return "";
	}
	
	@SuppressWarnings("unchecked")
	public void onSurveyHistoryTab()
	{
		try	
		{
            updateValuesFromMap();       
            SurveyInstanceHistoryTabBacking bean = (SurveyInstanceHistoryTabBacking)getBean("pages$tabSurveyInstanceHistory");
            bean.populateTab(beneficiaryPerson,SurveyForm.SURVEY_FOR_BEHAVIOR_MONITORING);
            updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onFocusDifficultiesTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	public boolean isViewModePopUp() {
		boolean returnVal = false;

		if (viewMap.get(VIEW_MODE) != null && viewMap.get(VIEW_MODE).toString().equals( PAGE_MODE_POPUP) )
		{
				returnVal = true;
		}
		else {
			returnVal = false;
		}

		return returnVal;

	}

	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.NOTES_OWNER_PERSON,beneficiaryPerson.getPersonId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
		public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("donationRequest.title.heading"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}


	public PersonView getBeneficiaryPerson() {
		return beneficiaryPerson;
	}


	public void setBeneficiaryPerson(PersonView beneficiaryPerson) {
		this.beneficiaryPerson = beneficiaryPerson;
	}


	
	

}