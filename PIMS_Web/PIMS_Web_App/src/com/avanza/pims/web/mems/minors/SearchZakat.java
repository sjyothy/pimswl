package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.Utils.ZakatSearchCriteria;
import com.avanza.pims.entity.Request;
import com.avanza.pims.entity.Zakat;
import com.avanza.pims.scheduler.ContractUtilizedAmountJob;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.ZakatService;
import com.avanza.ui.util.ResourceUtil;

public class SearchZakat extends AbstractSearchBean 
{
	private static final long serialVersionUID = 1L;
	private final String DEFAULT_SORT_FIELD = "dueOn";
	private final String DATA_LIST = "DATA_LIST";
	ZakatService service = new ZakatService();

	ZakatSearchCriteria  criteria = new ZakatSearchCriteria();
	List<Zakat> dataList = new ArrayList<Zakat>();

	@SuppressWarnings("unchecked")
	@Override
	public void init() 
	{
		super.init();
		try 
		{
			
			if (!isPostBack()) 
			{
				initData();
			}
		} catch (Exception es) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	private void initData() throws Exception 
	{
		setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
		setSortField(DEFAULT_SORT_FIELD);
		setSortItemListAscending(false);
		checkDataInQueryString();
	}

	private void checkDataInQueryString() {
		checkPageMode();
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() 
	{
		if (request.getParameter(VIEW_MODE) != null) 
		{
			if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);
			}
			else if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void onCreateZakatDeductionRequest() 
	{
		try
		{
			criteria.setStatusId(String.valueOf(  WebConstants.ZakatStatus.Active_Id ) );
			
			List<Zakat> zakatList = service.search(criteria, null,null,null,null);
			if( zakatList != null && zakatList.size() > 0 )
			{
				Request request = service.persistZakatDeductionRequest(zakatList, getLoggedInUserId());
				String msg = 
							 java.text.MessageFormat.format( 
									 						 CommonUtil.getBundleMessage("zakat.msg.zakatRequestSaveed"),
									 						 request.getRequestNumber()
									 					    );
				loadDataList();					 
				successMessages.add(msg);
			}
			else
			{
				
				errorMessages.add( CommonUtil.getBundleMessage("zakat.msg.noActiveZakatPresent") );
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "onCreateZakatDeductionRequest|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked")
	public void onNameClicked()  
	{
		try
		{
			Zakat row = (Zakat) dataTable.getRowData();
			if( 
				row.getInheritanceFile() != null && row.getInheritanceFile().getInheritanceFileId() != null &&
				row.getInheritanceFile().getFileOwner().getPersonId().compareTo( row.getPerson().getPersonId() ) == 0
			  )
			{
				sessionMap.put( WebConstants.InheritanceFile.FILE_ID, row.getInheritanceFile().getInheritanceFileId() );
			    sessionMap.put( WebConstants.InheritanceFilePageMode.IS_POPUP, true);
				executeJavaScript( "javaScript:openFile();");
			}
			else
			{
				executeJavaScript( "javaScript:openBeneficiaryPopup("+ row.getPerson().getPersonId() +");");
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "onNameClicked|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}

	@SuppressWarnings( "unchecked")
	public void onCalculateZakat()  
	{
		try
		{

//			ZakatCalculationService zakatCalculationService = new ZakatCalculationService();
//			zakatCalculationService.processJob();

//			NotificationProvider notificationProvider = NotificationFactory.getInstance().createNotifier(NotifierType.JMSBased);
//			String to = "danish_farooq@emitac.ae";
//			notificationProvider.sendEmail("Test", "Test 123 123 testing", to, null, "SMTP-CHANNEL",PriorityType.Medium);
			
			ContractUtilizedAmountJob job = new ContractUtilizedAmountJob();
			job.execute();
		}
		catch ( Exception e )
		{
			logger.LogException( "onCalculateZakat|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}


	@SuppressWarnings("unchecked")
	public String onEdit() 
	{
		try 
		{
			Zakat row = (Zakat) dataTable.getRowData();
			if (row != null) 
			{
//				sessionMap.put(Constant.Zakat.ENDOWMENT_FILE , row);
				return "manage";
			}
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onEdit |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}

	public void onSearch() 
	{
		try 
		{
			pageFirst();// at last
		} 
		catch (Exception e) 
		{
			logger.LogException("onSearch |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void doSearchItemList()
	{
		try {
				loadDataList();
			}
		    catch (Exception e){
		    	errorMessages = new ArrayList<String>(0);
		 		logger.LogException("doSearchItemList|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		    }
	}

	@SuppressWarnings("unchecked")
	public void loadDataList() throws Exception 
	{

		int totalRows = 0;
		totalRows = service.getTotalNumberOfRecords( criteria );
		setTotalRows(totalRows);
		doPagingComputations();

		dataList = service.search(getCriteria(), getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());

		if (dataList== null || dataList.isEmpty() ) 
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			forPaging(0);
		}
		
		this.setCriteria(criteria);
		this.setDataList( dataList );
		forPaging(dataList.size());
	}



	@SuppressWarnings("unchecked")
	public List<Zakat> getDataList() 
	{
		if(viewMap.get(DATA_LIST) != null)
		{
			dataList = (List<Zakat>) viewMap.get(DATA_LIST);
		} 
		return dataList ;
	}

	@SuppressWarnings("unchecked")
	public void setDataList(List<Zakat> list) 
	{
		this.dataList = list;
		if(this.dataList != null )
		{
			viewMap.put(DATA_LIST, list);
		}

	}

	
	@SuppressWarnings("unchecked")
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javascript);
	}

	@SuppressWarnings("unchecked")
	public ZakatSearchCriteria getCriteria() 
	{
		if( viewMap.get("criteria") != null )
		{
			criteria = (ZakatSearchCriteria )viewMap.get("criteria");  
		}
		return criteria;
	}

	@SuppressWarnings("unchecked")
	public void setCriteria(ZakatSearchCriteria criteria) {
		this.criteria = criteria;
		if( this.criteria  != null )
		{
			 viewMap.put("criteria",criteria);  
		}

	}

	
}
