package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.EndFileAsso;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.EndowmentFile;
import com.avanza.pims.entity.PaymentSchedule;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.endowment.EndowmentFileBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.endowment.EndowmentService;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.mems.EndowmentView;
import com.avanza.ui.util.ResourceUtil;

public class EndowmentDetailsTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private List< EndFileAsso> dataList;
	
	private HtmlDataTable dataTable;
	private EndowmentService  endowmentService;
	private String tabMode;

	public EndowmentDetailsTabBacking() 
	{
		endowmentService = new EndowmentService();
		dataTable        = new HtmlDataTable();
		dataList         = new ArrayList<EndFileAsso>();
		tabMode          = Constant.EndowmentFile.MODE_READ_ONLY;
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_END_TAB, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.SUCCESS_END_TAB, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			tabMode = (String) viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		if( viewMap.get( "EndFileAssoList" ) != null )
		{
			dataList = (ArrayList<EndFileAsso>)viewMap.get( "EndFileAssoList" );
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, tabMode );
		}
		if( dataList != null && dataList.size()  > 0  )
		{
			viewMap.put( "EndFileAssoList", dataList );
		}
		
	}	
	

	public void populateTab( EndowmentFile file ,Long assetTypeId ) throws Exception
	{
		updateValuesFromMaps();
		//if( file.getEndFileAssos() == null || file.getEndFileAssos().size() <= 0 )
		{
			dataList = endowmentService.getEndowments( file,assetTypeId  );
			Set<EndFileAsso> set = new HashSet<EndFileAsso>(dataList);
			file.setEndFileAssos(set);
			//Collections.sort(dataList,new ListComparator("",true));
			setTotalRows( dataList.size() );
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings( "unchecked" )
	public void openEndowmentPopUp()
	{
		
		try
		{
			
		}
		catch ( Exception e )
		{
			logger.LogException( "openEndowmentPopUp--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_END_TAB, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.SUCCESS_END_TAB, successMessages );
			}
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onAddBeneficiary()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			
			EndFileAsso obj = ( EndFileAsso )dataTable.getRowData();
			sessionMap.put(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO, obj);
			executeJavaScript("javaScript:openEndowmentFileBenPopup();");
		}
		catch ( Exception e )
		{
			logger.LogException( "onAddBeneficiary--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_END_TAB, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.SUCCESS_END_TAB, successMessages );
			}
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onEvaluateEndowments()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			EndFileAsso obj = ( EndFileAsso )dataTable.getRowData();
			if( obj.getStatusId().compareTo(Constant.EndowmentFileAssoStatus.EVALUATED_ID) == 0 )
			{
				sessionMap.put(WebConstants.PAGE_MODE, WebConstants.PAGE_MODE_VIEW);
			}
			sessionMap.put(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO, obj);
			executeJavaScript("javaScript:openEvaluateEndowmentsPopup();");
		}
		catch ( Exception e )
		{
			logger.LogException( "onEvaluateEndowments--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_END_TAB, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.SUCCESS_END_TAB, successMessages );
			}
		}
	}
		
	@SuppressWarnings( "unchecked" )
	public void onAddThirdPartyUnits()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			EndFileAsso obj = ( EndFileAsso )dataTable.getRowData();
			sessionMap.put(Constant.Endowments.ENDOWMENT, obj.getEndowment().getEndowmentId() );
			sessionMap.put(WebConstants.VIEW_MODE, ThirdPartyPropUnitsTabBean.Keys.VIEW_MODE_POPUP);
			executeJavaScript("javaScript:openThirdPartyUnitPopup();");
		}
		catch ( Exception e )
		{
			logger.LogException( "onAddThirdPartyUnits--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_END_TAB, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.SUCCESS_END_TAB, successMessages );
			}
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public String onReceiveProperty()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			EndFileAsso obj = ( EndFileAsso )dataTable.getRowData();
			sessionMap.put(Constant.Endowments.ENDOWMENT, obj.getEndowment() );
			sessionMap.put(Constant.EndowmentFile.ENDOWMENT_FILE_ID, obj.getEndowmentFile().getFileId()  );
			if( viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
			{
				sessionMap.put(  "EF_"+WebConstants.TASK_LIST_SELECTED_USER_TASK ,
								viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) 
				               ) ;
			}
			return "receiveproperty";
		}
		catch ( Exception e )
		{
			logger.LogException( "onReceiveProperty--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_END_TAB, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.SUCCESS_END_TAB, successMessages );
			}
		}
		return "";
		
	}
	
	@SuppressWarnings( "unchecked" )
	public String onPortfolio()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		
		try
		{
			EndFileAsso obj = ( EndFileAsso )dataTable.getRowData();
			sessionMap.put(Constant.Endowments.ENDOWMENT, obj.getEndowment() );
			sessionMap.put(Constant.EndowmentFile.ENDOWMENT_FILE_ID, obj.getEndowmentFile().getFileId()  );
			return "portfolio";
		}
		catch ( Exception e )
		{
			logger.LogException( "onPortfolio--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_END_TAB, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.SUCCESS_END_TAB, successMessages );
			}
		}
		return "";
		
	}
	@SuppressWarnings( "unchecked" )
	public void onCollectPayment()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		
		try
		{
			EndFileAsso obj = ( EndFileAsso )dataTable.getRowData();
			PaymentScheduleView payment =EndowmentService.getPaymentScheduleForEndowmentTypeCashBankTransferCheck(obj.getEndowment(), getLoggedInUserId());
			List<PaymentScheduleView> paymentList = new ArrayList<PaymentScheduleView>();
			paymentList.add(payment);
			sessionMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION, paymentList);
	        PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
	        sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
	        executeJavaScript("javascript:openPopupReceivePayment();");
		}
		catch ( Exception e )
		{
			logger.LogException( "onCollect--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_END_TAB, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.SUCCESS_END_TAB, successMessages );
			}
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onEdit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			EndFileAsso obj = ( EndFileAsso )dataTable.getRowData();
			EndowmentFileBean bean = (EndowmentFileBean)getBean("pages$endowmentFile");
			if( bean.getStatusNew() || bean.getStatusRegistrationReq() )
			{
				sessionMap.put(WebConstants.PAGE_MODE, WebConstants.PAGE_MODE_EDIT);
			}
			else
			{
				sessionMap.put(WebConstants.PAGE_MODE, WebConstants.PAGE_MODE_VIEW);
			}
			sessionMap.put(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO, obj);
			executeJavaScript("javaScript:openEndowmentFileAssoPopup();");
		}
		catch ( Exception e )
		{
			logger.LogException( "onEdit--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_END_TAB, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.SUCCESS_END_TAB, successMessages );
			}
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDelete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
			EndFileAsso obj = ( EndFileAsso )dataTable.getRowData();
			EndowmentFileBean bean = (EndowmentFileBean)getBean("pages$endowmentFile");
			boolean deleted = bean.deleteEndowment(obj);
			if( deleted )
			{
			 dataList.remove(obj);
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "onDelete--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_END_TAB, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.SUCCESS_END_TAB, successMessages );
			}
		}
		
	}
    @SuppressWarnings( "unchecked" )
	public void onAdd()
	{
    	errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			sessionMap.put( WebConstants.PAGE_MODE,WebConstants.PAGE_MODE_EDIT);
			executeJavaScript("javaScript:openEndowmentFileAssoPopup();");
			
		}
		catch ( Exception e )
		{
			logger.LogException( "onAdd --- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.ERR_END_TAB, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(Constant.EndowmentFile.SUCCESS_END_TAB, successMessages );
			}
		}
	}
    public boolean hasNoBeneficiaries()
	{
		EndFileAsso obj = ( EndFileAsso )dataTable.getRowData();
        return (obj.getEndFileBens() == null || obj.getEndFileBens().size() <= 0  );
	}
	
	public Integer getReviewRequestViewListRecordSize()
	{
		return dataList.size();
	}
	
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}

	public List<EndFileAsso> getDataList() {
		return dataList;
	}

	public void setDataList(List<EndFileAsso> dataList) {
		this.dataList = dataList;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

}
