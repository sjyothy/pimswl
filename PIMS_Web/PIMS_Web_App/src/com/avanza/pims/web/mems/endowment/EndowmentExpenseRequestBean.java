package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.EndowmentExpType;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.ExpenseTypeUsrGrp;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationDetails;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.workflow.WFClient;
import com.avanza.pims.web.workflow.WFTaskAction;
import com.avanza.pims.web.workflow.WorkFlowVO;
import com.avanza.pims.ws.mems.EndowmentTransformUtil;
import com.avanza.pims.ws.mems.endowment.EndowmentExpenseService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.TaskListVO;
import com.avanza.pims.ws.vo.UnitView;
import com.avanza.pims.ws.vo.mems.EndowmentExpenseView;
import com.avanza.pims.ws.vo.mems.EndowmentView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.ui.util.ResourceUtil;


public class EndowmentExpenseRequestBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(EndowmentExpenseRequestBean.class);
	
	private static final String PAGE_MODE_POPUP_VIEW = "PAGE_MODE_POPUP_VIEW";
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_RESUBMITTED = "PAGE_MODE_RESUBMITTED";
	private static final String PAGE_MODE_COMPLETE = "PAGE_MODE_COMPLETE";
	private static final String PAGE_MODE_APPROVE = "PAGE_MODE_APPROVE";
	private static final String PAGE_MODE_FINANCE = "PAGE_MODE_FINANCE";
	private static final String PAGE_MODE_REVIEW = "PAGE_MODE_REVIEW";
	private static final String PAGE_MODE_AUDIT = "PAGE_MODE_AUDIT";
	
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_DETAILS   = "detailsTab";
	private static final String TAB_REVIEW   = "reviewTab";
	private static final String TAB_APPLICANTS   = "applicationTab";
    private String pageTitle;
    private String pageMode;
    private String hdnPersonId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;
    private String hdnPropertyId  ;
    private String txtRemarks;
    private EndowmentExpenseService service = new EndowmentExpenseService();
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	
	private RequestView requestView ;
    private EndowmentExpenseView pageView = new EndowmentExpenseView();
    private List<EndowmentExpenseView> dataList = new ArrayList<EndowmentExpenseView>();
    private HtmlDataTable dataTable;
    private HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
    
    private HtmlGraphicImage imgUnitSearch=new HtmlGraphicImage();
    private HtmlGraphicImage imgPropertySearch=new HtmlGraphicImage();
    private HtmlGraphicImage imgEndowmentSearch=new HtmlGraphicImage();
    private HtmlSelectOneMenu cmbReviewGroup = new HtmlSelectOneMenu();
    protected TaskListVO userTask;
	public EndowmentExpenseRequestBean(){}
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
				onMessageFromSearchUnit(); 
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.EndowmentExpense.PROCEDURE_TYPE);
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.EndowmentExpense.PROCEDURE_TYPE);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		viewMap.put("noteowner", WebConstants.REQUEST);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		loadAttachmentsAndComments( null );
		boolean isPopup = false;
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
		  getDataFromTaskList();
		}
		else if ( getRequestMap().get( WebConstants.REQUEST_VIEW) != null )
		{
		  getDataFromSearch();
		}
		else if( request.getParameter("requestId") != null ) 
		{
			
			getDataFromQueryString();
			this.setPageMode( PAGE_MODE_POPUP_VIEW );
			tabPanel.setSelectedTab( TAB_DETAILS );
			isPopup = true;
		}
		
		else
		{
			setDataForFirstTime();
		}
		
		updateValuesFromMap();
		if( !isPopup )
		{
			getPageModeFromRequestStatus();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void prerender()
	{
		if(viewMap.containsKey(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES))
		{
			if (errorMessages == null ) errorMessages = new ArrayList<String>();
			errorMessages.addAll( (ArrayList<String>)viewMap.get(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES) );
			viewMap.remove(WebConstants.REVIEW_DETAIL_TAB_ERROR_MESSAGES);
			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		requestView = new RequestView();
		requestView.setCreatedOn(new Date());
		requestView.setCreatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedOn(new Date());
		requestView.setStatusId( 9001l );
		requestView.setStatusDataValue( WebConstants.REQUEST_STATUS_NEW  );
		requestView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		requestView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
		requestView.setRequestDate( new Date() );
		requestView.setRequestTypeId( WebConstants.MemsRequestType.ENDOWMENT_EXPENSE_REQUEST);
		
		pageView  = new EndowmentExpenseView();
		pageView.setCreatedBy( getLoggedInUserId() );
		pageView.setCreatedOn( new Date() );
		pageView.setUpdatedBy(  getLoggedInUserId()  );
		pageView.setUpdatedOn(new Date());
		pageView.setStatus( WebConstants.EndowmentExpenseStatus.NEW_ID );
		DomainDataView ddView = CommonUtil.getDomainDataView(
															  WebConstants.EndowmentExpenseStatus.DOMAIN_TYPE_KEY,
															  WebConstants.EndowmentExpenseStatus.NEW_ID 
														     );
		pageView.setStatusEn( ddView.getDataDescEn() );
		pageView.setStatusAr( ddView.getDataDescAr() );
		pageView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED.toString() );
		EndowmentView view = ( EndowmentView )sessionMap.remove(WebConstants.SELECTED_ROW );
		pageView.setEndowment(view);

	}
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromRequestStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		
		tabPanel.setSelectedTab(TAB_DETAILS);
		if( this.requestView == null  || 
			this.requestView.getRequestId() == null ||
			this.requestView.getStatusId() == null 
		  ) 
		{ return; }

		if(  getStatusRejectedResubmitted() )
		{
			setPageMode(PAGE_MODE_RESUBMITTED);
			tabPanel.setSelectedTab("commentsTab" );
			
		}
		//Either request status is approval required or review done, Since review can be done at approver step and also at finance step, therefore,
		//in order to identify who sent for review old status is checked if old status value is approval required than it was sent by approver and if old
		//status is approved than it was sent by finance
		else if (
					this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED) == 0 ||
					(
					this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REVIEW_DONE) == 0 &&
					this.requestView.getOldStatusId().compareTo(WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID)==0
					)
				
				)
		{
			
			setPageMode(PAGE_MODE_APPROVE);
			if(this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REVIEW_DONE) == 0 ){
				tabPanel.setSelectedTab( TAB_REVIEW);
			}
			else
			tabPanel.setSelectedTab( TAB_DETAILS );
		}
		else if ( 
					this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_APPROVED) == 0||
					(
					this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REVIEW_DONE) == 0 &&
					this.requestView.getOldStatusId().compareTo(WebConstants.REQUEST_STATUS_APPROVED_ID)==0
					)||
					(
					this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REVIEW_DONE) == 0 &&
					this.requestView.getOldStatusId().compareTo(WebConstants.REQUEST_STATUS_AUDITED_ID)==0
					)||
					this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_AUDITED ) == 0
				)
		{
			
			setPageMode(PAGE_MODE_FINANCE);
			if(this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REVIEW_DONE) == 0 ){
				tabPanel.setSelectedTab( TAB_REVIEW);
			}
			else
			tabPanel.setSelectedTab( TAB_DETAILS );
		}
		else if ( 
				this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REVIEW_REQUIRED) == 0
			)
		{
			setPageMode(PAGE_MODE_REVIEW);
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
			tabPanel.setSelectedTab( TAB_REVIEW);
		}
		else if ( 
				this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_AUDIT_REQUIRED) == 0
			)
		{
			setPageMode(PAGE_MODE_AUDIT);
			tabPanel.setSelectedTab( TAB_DETAILS  );
		}
		else if ( 
				this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_COMPLETION_REQUIRED) == 0
			)
		{
			setPageMode(PAGE_MODE_COMPLETE);
			tabPanel.setSelectedTab( TAB_DETAILS  );
		}
		else if (getStatusCompleted())
		{
			setPageMode(PAGE_MODE_VIEW);
			tabPanel.setSelectedTab( TAB_DETAILS );
		}	
			
	}
	public boolean getStatusNew() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_NEW ) == 0;
	}

	public boolean getStatusRejectedResubmitted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT) == 0;
	}
	
	public boolean getStatusCompleted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_COMPLETE ) == 0;
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = ( RequestView )viewMap.get( WebConstants.REQUEST_VIEW ) ;
			requestView.setUpdatedBy(getLoggedInUserId());
		}
		if( viewMap.get(WebConstants.EndowmentExpense.EndExpView) != null)
		{
			this.pageView = (EndowmentExpenseView)viewMap.get(WebConstants.EndowmentExpense.EndExpView) ;
		}
		if( viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null)
		{
			userTask = (	TaskListVO )viewMap.get(  WebConstants.TASK_LIST_SELECTED_USER_TASK  ); 
		}
		if(request.getParameter("pageMode")!=null)
		{
			pageMode = request.getParameter("pageMode").toString() ;
		}
		if(viewMap.get("dataList")!= null)
		{
			this.dataList= (ArrayList<EndowmentExpenseView>)viewMap.get("dataList");
		}
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( requestView != null )
		{
		  viewMap.put(  WebConstants.REQUEST_VIEW , requestView);
		}
		if(this.dataList != null)
		{
			viewMap.put("dataList",this.dataList);
		}
		if( this.pageView != null)
		{
			viewMap.put(  WebConstants.EndowmentExpense.EndExpView, pageView  );
		}
		if(userTask != null)
			viewMap.put(  WebConstants.TASK_LIST_SELECTED_USER_TASK , userTask);

	}
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		requestView = ( RequestView)getRequestMap().get( WebConstants.REQUEST_VIEW) ;
		if( this.requestView !=null )
		{
		  getRequestDetails( requestView.getRequestId() );
		}
	}
	
	@SuppressWarnings(  "unchecked"  )
	private void populateApplicationDetailsTab()throws Exception
	{
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		String status = "";
		boolean isRequestNull =  null == requestView || null == requestView.getApplicantView() ||null == requestView.getRequestId();
		if ( isRequestNull && hdnPersonName!= null && hdnPersonName.trim().length() >0 ) 
		{
			bean.populateApplicationDetails(
						                    "", 
						                    "", 
						                    new Date(), 
						                    "", 
						                    hdnPersonName, 
						                    "", 
						                    hdnCellNo, 
						                    ""
						                  );
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, hdnPersonId);
			PersonView person = new PersonView();
			person.setPersonId( new Long( hdnPersonId ) );
			requestView.setApplicantView( person);
			
            return;
		}
		
		status = CommonUtil.getIsEnglishLocale()?requestView.getStatusEn():requestView.getStatusAr();
		bean.populateApplicationDetails(
				                         requestView.getRequestNumber(), 
					                     status, 
					                     requestView.getRequestDate(), 
					                     requestView.getDescription(), 
					                     requestView.getApplicantView().getPersonFullName(), 
					                     "", 
					                     requestView.getApplicantView().getCellNumber(), 
					                     requestView.getApplicantView().getEmail()
					                   );
		if( requestView.getApplicantView() != null && requestView.getApplicantView().getPersonId() != null  )
		{
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,requestView.getApplicantView().getPersonId() );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, requestView.getApplicantView().getPersonId() );
		}
		if ( requestView!= null && requestView.getStatus() != null && 
			!requestView.getStatusDataValue().equals( WebConstants.REQUEST_STATUS_NEW ) 
		    )
		{
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}
	}
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  userTask = ( TaskListVO )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  if( userTask.getRequestId() == null ){ return; }
	  long  id = new Long  ( userTask.getRequestId() );
	  getRequestDetails( id );
    }
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromQueryString()throws Exception
    {
	  long  id = new Long  ( request.getParameter("requestId").toString() );
	  getRequestDetails( id );
    }
	
	@SuppressWarnings( "unchecked" )
	private void getRequestDetails( long id ) throws Exception 
	{
		requestView = new RequestService().getRequestById( id );
		setDataList ( 
						EndowmentExpenseService.getEndowmentExpenseViewByRequestId(requestView.getRequestId()) 
					);
		populateApplicationDetailsTab();
		updateValuesToMap();
		loadAttachmentsAndComments( id );
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(requestView.getRequestId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.REQUEST;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, requestView.getRequestId());
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.REQUEST;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(requestView.getRequestId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, requestView.getRequestId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }

	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( requestView != null && requestView.getRequestId()!= null )
		 {
			 loadAttachmentsAndComments( requestView.getRequestId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMap();
			if(hdnPersonType.equals( "APPLICANT" ) )
			{
				populateApplicationDetailsTab();
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	
	public void onMessageFromSearchUnit()throws Exception
    {
		try {
				if(!sessionMap.containsKey(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT)){return;}
			    this.pageView.setEndowment(null);
			    this.pageView.setPropertyView(null);
			    
			    
				this.pageView.setUnitView(
												(UnitView)sessionMap.remove(WebConstants.UnitSearchOutcomes.UNIT_SEARCH_SELECTED_ONE_UNIT)
										 );
				hdnPropertyId = this.pageView.getUnitView().getPropertyId().toString();
				onMessageFromSearchProperty();
				updateValuesToMap();
	  }
	  catch(Exception e)
	  {
			logger.logInfo("onMessageFromSearchUnit|ErrorOccured::",e);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	  }
    }

	public void onMessageFromSearchProperty()
	{
		
		try {
			if(hdnPropertyId!=null && hdnPropertyId.trim().length()>0)
			{
			  this.pageView.setEndowment(null);
			  this.pageView.setUnitView(null);
			  this.pageView.setPropertyView( new PropertyService().getPropertyByID(new Long(hdnPropertyId)) );
			  if(this.pageView.getPropertyView().getEndowmentId() != null)
			  {
				  EndowmentView endowment = new EndowmentView();
				  endowment.setEndowmentId(this.pageView.getPropertyView().getEndowmentId());
				  endowment.setEndowmentNum( this.pageView.getPropertyView().getEndowmentReference());
				  this.pageView.setEndowment( endowment);
			  }
			}
		
			updateValuesToMap();
		} catch (Exception e) 
		{
			logger.logInfo("onMessageFromSearchProperty|ErrorOccured::",e);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchEndowments()
	{
		if( sessionMap.get(WebConstants.SELECTED_ROW)== null )
		{return;}
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	

			updateValuesFromMap();
			Endowment endowment = (Endowment)sessionMap.remove(WebConstants.SELECTED_ROW);
			EndowmentView endowmentView = new EndowmentView();
			EndowmentTransformUtil.transformToEndowmentView(endowmentView, endowment);
			pageView.setEndowment(endowmentView);
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromSearchEndowments --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if(requestView == null || requestView.getRequestId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,requestView.getRequestId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	private boolean hasAddErrors() throws Exception
	{
		boolean hasErrors=false;
		errorMessages = new ArrayList<String>();
		
		if( this.pageView.getEndowment() == null || this.getPageView().getEndowment().getEndowmentId()== null )
		{
			errorMessages.add(CommonUtil.getBundleMessage("endowmentExpenseType.msg.endowmentRequired"));
			tabPanel.setSelectedTab(TAB_DETAILS);
			hasErrors=true;
		}
		if( this.pageView.getEndowmentExpenseTypeId() == null || this.pageView.getEndowmentExpenseTypeId().equals("-1") )
		{
			errorMessages.add(CommonUtil.getBundleMessage("endowmentExpenseType.msg.typeRequired"));
			tabPanel.setSelectedTab(TAB_DETAILS);
			hasErrors=true;
		}
		else {
			EndowmentExpType  expType = EntityManager.getBroker().findById(EndowmentExpType.class, Long.valueOf(this.pageView.getEndowmentExpenseTypeId()));
			int count = 0;
			for (ExpenseTypeUsrGrp item : expType.getApproverUserGroups()) {
				if(item.getUserGroupId()!= null){
					count=1;
					break;
				}
			} 
			if(count==0)
			{
				errorMessages.add(CommonUtil.getBundleMessage("endowmentExpense.msg.noApproverGrp"));
				tabPanel.setSelectedTab(TAB_DETAILS);
				hasErrors=true;
			}
		}
		if( this.pageView.getDescription() == null || this.pageView.getDescription().trim().length() <= 0 )
		{
			errorMessages.add(CommonUtil.getBundleMessage("mems.payment.err.msgs.descReq"));
			tabPanel.setSelectedTab(TAB_DETAILS);
			hasErrors=true;
		}
		if( this.pageView.getAmount() == null )
		{
			errorMessages.add(CommonUtil.getBundleMessage("donationRequest.msg.amountRequired"));
			tabPanel.setSelectedTab(TAB_DETAILS);
			hasErrors=true;
		}
		

		return hasErrors;
		
	}

	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		
		if( this.dataList == null || this.dataList.size()<=0 )
		{
			errorMessages.add(CommonUtil.getBundleMessage("endowmentExpense.msg.endowmentExpenseRequired"));
			tabPanel.setSelectedTab(TAB_DETAILS);
			hasSaveErrors=true;
		}
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}

		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onAdd()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			
			if( hasAddErrors() ){ return; }
			EndowmentExpenseView clonedView = (EndowmentExpenseView)BeanUtils.cloneBean(pageView);
			
			dataList.add( 0, clonedView);
			saveRequestInTransaction();
			getRequestDetails(  requestView.getRequestId() );
			pageView = new EndowmentExpenseView();
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty("endowmentExpense.msg.expenseAdded"));
		}
		catch (Exception exception) 
		{
			logger.LogException( "onAdd--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	private boolean hasDeleteErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
        if( requestView.getRequestId() != null && dataList.size()==1 )
        {
        	errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentExpense.msg.deleteError"));
        }
		return hasSaveErrors;
		
	}
	@SuppressWarnings( "unchecked" )
	public void onDelete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			EndowmentExpenseView row = ( EndowmentExpenseView )dataTable.getRowData();
			updateValuesFromMap();
			
			if( hasDeleteErrors() ){ return; }
	        
	        if( row.getExpenseId() != null)
	        {
	        	row.setUpdatedBy( getLoggedInUserId() );
	        	EndowmentExpenseService.deleteEndowmentExpense(row);
	        }
	        dataList.remove( row );	
			
			successMessages.add( ResourceUtil.getInstance().getProperty("endowmentExpense.msg.deleteSuccess"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onDelete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	
	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			saveRequestInTransaction();
			getRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
			successMessages.add( ResourceUtil.getInstance().getProperty("thirdPartRevenue.msg.saved"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	private boolean hasCompletionErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}

		return hasSaveErrors;
		
	}
	private boolean hasApprovalErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
//		if(!AttachmentBean.mandatoryDocsValidated())
//    	{
//    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
//    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
//    		return true;
//    	}

		return hasSaveErrors;
		
	}
	@SuppressWarnings( "unchecked" )
	public void onApprove()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			Long status = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVED);
			updateValuesFromMap();
			if( hasApprovalErrors() ){ return; }
			try 
			 {
				 ApplicationContext.getContext().getTxnContext().beginTransaction();
				 
				 persistRequest( status, 
						 		WebConstants.EndowmentExpenseStatus.DISBURSEMENT_REQUIRED_ID,
						 		WFTaskAction.APPROVED.toString(),
						 		null
						 		);
				 ApplicationContext.getContext().getTxnContext().commit();
			 }
			 catch (Exception exception) 
			 {
					ApplicationContext.getContext().getTxnContext().rollback();
					throw exception;
			 }
			finally
			{
				ApplicationContext.getContext().getTxnContext().release();
			}
			getRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_APPROVED);
			this.setPageMode( PAGE_MODE_VIEW );
			successMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.RequestEvents.REQUEST_APPROVED));
			
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onApprove--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onSendForAudit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			if( hasApprovalErrors() ){ return; }
			try 
			 {
				 ApplicationContext.getContext().getTxnContext().beginTransaction();
				 
				 persistRequest( WebConstants.REQUEST_STATUS_AUDIT_REQUIRED_ID, 
						 		null,
						 		WFTaskAction.OK.toString(),
						 		null
						 		);
				 ApplicationContext.getContext().getTxnContext().commit();
			 }
			 catch (Exception exception) 
			 {
					ApplicationContext.getContext().getTxnContext().rollback();
					throw exception;
			 }
			finally
			{
				ApplicationContext.getContext().getTxnContext().release();
			}
			getRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( "request.event.AuditRequired");
			this.setPageMode( PAGE_MODE_VIEW );
			successMessages.add( ResourceUtil.getInstance().getProperty("request.event.AuditRequired"));
			
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSendForAudit--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onAudited()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			Long status = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_AUDITED);
			updateValuesFromMap();
			if( hasApprovalErrors() ){ return; }
			persistRequest(status, null, WFTaskAction.POSTBACKRESUBMIT.toString(), null);
			getRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( "request.event.Audited");
			this.setPageMode( PAGE_MODE_VIEW );
			successMessages.add( ResourceUtil.getInstance().getProperty("request.event.Audited"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSendForAudit--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onReviewed()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_REVIEWED;
		String event  =  MessageConstants.RequestEvents.REQUEST_REVIEWED;
		try	
		{
			updateValuesFromMap();
//			if( hasSaveErrors() ){ return; }
			 	
			 UtilityService utilityService = new UtilityService();
			 ReviewRequestView reviewRequestView = ( ReviewRequestView ) viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW );
			 try 
			 {
				 ApplicationContext.getContext().getTxnContext().beginTransaction();
				 utilityService.persistReviewRequest( reviewRequestView );
				 persistRequest( WebConstants.REQUEST_STATUS_REVIEW_DONE_ID, 
						 		null,
						 		WFTaskAction.POSTBACKRESUBMIT.toString(),
						 		null
						 		);
				 saveCommentsAttachment( event);
				 ApplicationContext.getContext().getTxnContext().commit();
			 }
			 catch (Exception exception) 
			 {
					ApplicationContext.getContext().getTxnContext().rollback();
					throw exception;
			 }
			finally
			{
				ApplicationContext.getContext().getTxnContext().release();
			}
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			logger.LogException("onReviewed--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	
	public boolean hasReviewErrors() throws Exception
	{
		if( null==cmbReviewGroup.getValue() || cmbReviewGroup.getValue().toString().equals( "-1")) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_REVIEW_GRP));
			return true;
		}
		
		
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public void onReviewRequired()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW;
		try	
		{	updateValuesFromMap();	
			if( hasSaveErrors() || hasReviewErrors() || !hasReasonProvided()){ return; }
			 
			 
			 UtilityService utilityService = new UtilityService();
			 ReviewRequestView reviewRequestView = new ReviewRequestView();
			 reviewRequestView.setCreatedBy( CommonUtil.getLoggedInUser() );
			 reviewRequestView.setCreatedOn( new Date() );
			 reviewRequestView.setRfc( txtRemarks.trim() );
			 reviewRequestView.setGroupId( cmbReviewGroup.getValue().toString() );
			 reviewRequestView.setRequestId( requestView.getRequestId() );
			 try 
			 {
					 ApplicationContext.getContext().getTxnContext().beginTransaction();
					 utilityService.persistReviewRequest( reviewRequestView );				
					 persistRequest(
							 		WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID,
							 		null,
							 		WFTaskAction.REVIEW.toString(),
							 		cmbReviewGroup.getValue().toString()
							 		);
					 saveCommentsAttachment( event );
					 
					 ApplicationContext.getContext().getTxnContext().commit();
			 }
			 catch (Exception exception) 
			 {
					ApplicationContext.getContext().getTxnContext().rollback();
					throw exception;
			 }
			finally
			{
				ApplicationContext.getContext().getTxnContext().release();
			}

			getRequestDetails( requestView.getRequestId() );
			
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			logger.LogException("onReviewRequired--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	
	@SuppressWarnings( "unchecked" )
	public void onComplete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			if( hasCompletionErrors() ){ return; }
			try 
			 {
				 ApplicationContext.getContext().getTxnContext().beginTransaction();
				 
				 persistRequest( WebConstants.REQUEST_STATUS_COMPLETE_ID , 
						 		null,
						 		WFTaskAction.OK.toString(),
						 		null
						 		);
				 ApplicationContext.getContext().getTxnContext().commit();
			 }
			 catch (Exception exception) 
			 {
					ApplicationContext.getContext().getTxnContext().rollback();
					throw exception;
			 }
			finally
			{
				ApplicationContext.getContext().getTxnContext().release();
			}
			getRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_COMPLETED );
			this.setPageMode( PAGE_MODE_VIEW );
			successMessages.add( ResourceUtil.getInstance().getProperty("request.event.RequestCompleted"));
			
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onComplete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onDisburse()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			List<EndowmentExpenseView> selectedRows = new ArrayList<EndowmentExpenseView>();
			
			for (EndowmentExpenseView vo : this.getDataList()) {
				if( 
						vo.getIsDeleted().equals("0") && 
						vo.getStatus().compareTo(WebConstants.EndowmentExpenseStatus.DISBURSEMENT_REQUIRED_ID)==0 &&
						vo.getSelected() != null && vo.getSelected()
				  )
				{
						vo.setUpdatedBy(getLoggedInUserId());
						selectedRows.add(vo);
				}
			}
			
			if(selectedRows.size()<=0)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentExpense.msg.AtleastOneRowSelect"));
			}
			else
			{
				EndowmentExpenseService.onDisburseExpenses(
															requestView,
															selectedRows, 
															getLoggedInUserId()
															
														  );
				this.setDataList( EndowmentExpenseService.getEndowmentExpenseViewByRequestId(requestView.getRequestId()) );
				successMessages.add( ResourceUtil.getInstance().getProperty("endowmentExpense.msg.disbursedSuccessfully"));
			}
			
			
			
			
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onDisburse--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	/**
	 * @param status
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void completeRequestInTransaction(Long status,String taskAction,String assignedGroups) throws Exception 
	{
		try
		{
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistRequest(status,WebConstants.EndowmentExpenseStatus.DISBURSEMENT_REQUIRED_ID,taskAction,assignedGroups );
			
		    
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	
	private void saveRequestInTransaction() throws Exception 
	{
		try
		{
			
			Long status = 9001l;
            if(requestView.getStatusId() != null)
            {
            	status = null;
            }
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistRequest( status,WebConstants.EndowmentExpenseStatus.NEW_ID,null,null );
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistRequest(Long status, Long endowmentExpenseStatus,String taskAction,String assignedGroups) throws Exception 
	{
	
		if(status!=null) {
			
			if(status.compareTo(WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID)==0){
				requestView.setOldStatusId(requestView.getStatusId());
			}
			//following should proceed the above line always
			requestView.setStatusId(status);
		}
				
		requestView = service.persist(
										requestView,
										dataList , 
										endowmentExpenseStatus,
										getLoggedInUserId() 
									  );
		if(taskAction != null && taskAction.trim().length()>0)
		{
			WorkFlowVO workFlowVO = new WorkFlowVO();
			workFlowVO.setTaskAction(taskAction);
			if(assignedGroups !=null && assignedGroups.length()>0)
			{
				workFlowVO.setAssignedGroups(assignedGroups);
			}
			CommonUtil.completeTask(workFlowVO,userTask,getLoggedInUserId());
		}
	}
	
	
	
	@SuppressWarnings( "unchecked" )
	public void onSubmit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		String event  =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		
		try	
		{
			String approverGroups = EndowmentExpenseService.getApproverUserGroupsForRequestId(requestView.getRequestId());
			if( hasSaveErrors() ){ return; }
			 updateValuesFromMap();
			 try{
					 ApplicationContext.getContext().getTxnContext().beginTransaction();
						 persistRequest( status,null,null,null );
						 WFClient client = new WFClient();
						 WorkFlowVO workFlowVO = new WorkFlowVO();
						 workFlowVO.setRequestId(requestView.getRequestId());
						 workFlowVO.setUserId(getLoggedInUserId());
						 workFlowVO.setProcedureTypeId(WebConstants.EndowmentExpense.PROCEDURE_TYPE);
						 workFlowVO.setAssignedGroups(approverGroups);
						 client.initiateProcess(workFlowVO);
						 saveCommentsAttachment( event );
					 ApplicationContext.getContext().getTxnContext().commit();
			 }
			 catch (Exception exception) 
			 {
					ApplicationContext.getContext().getTxnContext().rollback();
					throw exception;
			 }
			 
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			logger.LogException("onSubmit --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}
	
	@SuppressWarnings( "unchecked" )
	public void onResubmitted()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() ){ return; }
		 
		 updateValuesFromMap();	
		 
		 persistRequest( status,WebConstants.EndowmentExpenseStatus.NEW_ID,WFTaskAction.POSTBACK.toString(),null );
		 getRequestDetails( requestView.getRequestId() );

		 updateValuesToMap();
		 
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onResubmitted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	private boolean hasReasonProvided() throws Exception
	{
		if(txtRemarks ==null || txtRemarks.trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.reasonRequired")  );
			return false;
		}
		return true;
	}

	@SuppressWarnings( "unchecked" )
	public void onRejected()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_REJECTED);
		
		String msg    =  MessageConstants.RequestEvents.REQUEST_REJECTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_REJECTED;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( status,WebConstants.EndowmentExpenseStatus.REJECTED_ID,WFTaskAction.REJECT.toString(),null );
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onSendForComplete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_COMPLETION;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_COMPLETION;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
//			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			 persistRequest( WebConstants.REQUEST_STATUS_COMPLETION_REQUIRED_ID,
					 		null,
					 		WFTaskAction.APPROVED.toString(),
					 		null
					 	   );
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSendForComplete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onSendBackFromApprover()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( 
					 		status,
					 		WebConstants.EndowmentExpenseStatus.NEW_ID,
					 		WFTaskAction.RESUBMIT.toString(),
					 		null
					 	   );
			 
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSendBackFromApprover--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onSendBackFromFinance()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_FINANCE_REJECTED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_REJECTED_FINANCE;
		String event  =  MessageConstants.RequestEvents.REQUEST_REJECTED_FINANCE;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( 
					 			status,
					 			null,
					 			WFTaskAction.RESUBMIT.toString(),
					 			null
					 	   );
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSendBackFromFinance--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("endowmentExpense.heading.manage"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowDisburse() {
		if(  ! getPageMode().equals( PAGE_MODE_FINANCE ) )
		{return false;}
		
		for (EndowmentExpenseView vo : this.getDataList()) {
			if( 
					vo.getIsDeleted().equals("0") && 
					vo.getStatus().compareTo(WebConstants.EndowmentExpenseStatus.DISBURSEMENT_REQUIRED_ID)==0 
			  )
			{return true;}
			
		}
		return false;
		
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  !getPageMode().equals( PAGE_MODE_POPUP_VIEW ) &&
			( getPageMode().equals( PAGE_MODE_NEW)  || getPageMode().equals( PAGE_MODE_RESUBMITTED ) )  
				
	      ) 
		{
			return true;
		}
		return false;
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  !getPageMode().equals( PAGE_MODE_POPUP_VIEW ) &&  getPageMode().equals( PAGE_MODE_NEW ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowResubmitButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) &&  !getPageMode().equals( PAGE_MODE_POPUP_VIEW ) && getPageMode().equals( PAGE_MODE_RESUBMITTED ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowReview()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) &&  !getPageMode().equals( PAGE_MODE_POPUP_VIEW ) && getPageMode().equals( PAGE_MODE_REVIEW) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowApprove()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) &&  !getPageMode().equals( PAGE_MODE_POPUP_VIEW ) && getPageMode().equals( PAGE_MODE_APPROVE) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowFinance()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) &&  !getPageMode().equals( PAGE_MODE_POPUP_VIEW ) && getPageMode().equals( PAGE_MODE_FINANCE ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowAudit()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) &&  !getPageMode().equals( PAGE_MODE_POPUP_VIEW ) && getPageMode().equals( PAGE_MODE_AUDIT ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowComplete()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) &&  !getPageMode().equals( PAGE_MODE_POPUP_VIEW ) && getPageMode().equals( PAGE_MODE_COMPLETE ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getIsPopup()
	{
		return  getPageMode().equals( "POPUP" );
	}
	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public EndowmentExpenseView getPageView() {
		return pageView;
	}

	public void setPageView(EndowmentExpenseView pageView) {
		this.pageView = pageView;
	}

	public List<EndowmentExpenseView> getDataList() {
		return dataList;
	}

	public void setDataList(List<EndowmentExpenseView> dataList) {
		this.dataList = dataList;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public String getHdnPropertyId() {
		return hdnPropertyId;
	}

	public void setHdnPropertyId(String hdnPropertyId) {
		this.hdnPropertyId = hdnPropertyId;
	}

	public HtmlGraphicImage getImgUnitSearch() {
		return imgUnitSearch;
	}

	public void setImgUnitSearch(HtmlGraphicImage imgUnitSearch) {
		this.imgUnitSearch = imgUnitSearch;
	}

	public HtmlGraphicImage getImgPropertySearch() {
		return imgPropertySearch;
	}

	public void setImgPropertySearch(HtmlGraphicImage imgPropertySearch) {
		this.imgPropertySearch = imgPropertySearch;
	}

	public HtmlGraphicImage getImgEndowmentSearch() {
		return imgEndowmentSearch;
	}

	public void setImgEndowmentSearch(HtmlGraphicImage imgEndowmentSearch) {
		this.imgEndowmentSearch = imgEndowmentSearch;
	}

	public HtmlSelectOneMenu getCmbReviewGroup() {
		return cmbReviewGroup;
	}

	public void setCmbReviewGroup(HtmlSelectOneMenu cmbReviewGroup) {
		this.cmbReviewGroup = cmbReviewGroup;
	}

	public TaskListVO getUserTask() {
		return userTask;
	}

	public void setUserTask(TaskListVO userTask) {
		this.userTask = userTask;
	}

	

}