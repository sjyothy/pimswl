package com.avanza.pims.web.mems;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.servlet.ServletContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.bpel.proxy.MEMSZakatDeductionBPELPortClient;
import com.avanza.pims.entity.Request;
import com.avanza.pims.entity.Zakat;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationDetails;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.ZakatService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.ui.util.ResourceUtil;


public class ZakatDeductionManage extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(ZakatDeductionManage.class);
	
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_RESUBMITTED = "PAGE_MODE_RESUBMITTED";
	private static final String PAGE_MODE_APPROVAL_REQUIRED = "APPROVAL_REQUIRED";
	private static final String PAGE_MODE_APPROVED = "APPROVED";
	private static final String PAGE_MODE_REVIEW_REQUIRED= "REVIEW_REQUIRED";
	
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_DETAILS   = "detailsTab";
	private static final String TAB_REVIEW  = "reviewTab";

	private String pageTitle;
    private String pageMode;
    private String txtRemarks;
    
	private ZakatService zakatDeductionService = new ZakatService();
	private Request request ;
	
	private List<Zakat> dataList = new ArrayList<Zakat>();
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();    
	private HtmlDataTable dataTable;
    private HtmlSelectOneMenu cmbReviewGroup = new HtmlSelectOneMenu();
    private Integer zakatListRecordsSize = 0;
    private double totalZakatAmount = 0.0d; 
    
	public ZakatDeductionManage(){}
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.ZakatDeduction.PROCEDURE_TYPE );
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.ZakatDeduction.PROCEDURE_TYPE);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		viewMap.put("noteowner", WebConstants.REQUEST);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		loadAttachmentsAndComments( null );
		
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
		  getDataFromTaskList();
		}
		else if ( getRequestMap().get( WebConstants.REQUEST_VIEW) != null )
		{
		  getDataFromRequest();
		 
		}
		updateValuesFromMap();
		getPageModeFromRequestStatus();
	}
 
	
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromRequestStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		tabPanel.setSelectedTab(TAB_DETAILS);
		if( this.request== null  || 
			this.request.getRequestId() == null ||
			this.request.getStatusId() == null 
		  ) 
		{ return; }
		else if( getStatusApprovalRequired()  || getStatusRejectedByFinance()  || getStatusReviewDone() )
		{
			setPageMode(PAGE_MODE_APPROVAL_REQUIRED);
			if( getStatusApprovalRequired() )
			{
				tabPanel.setSelectedTab( TAB_DETAILS );
			}
			else if( getStatusReviewDone()  )
			{
				tabPanel.setSelectedTab( TAB_REVIEW );
			}
			else if( getStatusRejectedByFinance()  )
			{
				tabPanel.setSelectedTab( "commentsTab" );
				onAttachmentsCommentsClick();
			}
		}
		else if( getStatusReviewRequired()   )
		{
			setPageMode( PAGE_MODE_REVIEW_REQUIRED );
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
			tabPanel.setSelectedTab( TAB_REVIEW );
		}
		else if( getStatusApproved() )
		{
			setPageMode(PAGE_MODE_APPROVED);
			tabPanel.setSelectedTab( TAB_DETAILS );
		}
		else if( getStatusRejectedResubmitted() )
		{
			setPageMode(PAGE_MODE_RESUBMITTED);
			tabPanel.setSelectedTab( "commentsTab" );
			onAttachmentsCommentsClick();
			
		}
		else if ( getStatusCompleted()  || getStatusRejected()  )
		{
			setPageMode(PAGE_MODE_VIEW);
			tabPanel.setSelectedTab( TAB_DETAILS );
		}	
			
	}

	public boolean getStatusNew() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_NEW_ID ) == 0;
	}
	public boolean getStatusApprovalRequired() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID ) == 0;
	}
	public boolean getStatusReviewRequired() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID) == 0;
	}
	public boolean getStatusReviewDone() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_REVIEW_DONE_ID) == 0;
	}
	public boolean getStatusApproved() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_APPROVED_ID ) == 0;
	}
	public boolean getStatusRejected() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_ID ) == 0;
	}
	public boolean getStatusRejectedByFinance() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_FINANCE_REJECTED_ID ) == 0;
	}
	public boolean getStatusRejectedResubmitted() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT_ID) == 0;
	}
	public boolean getStatusCompleted() {
		return this.request.getStatusId().compareTo(  WebConstants.REQUEST_STATUS_COMPLETE_ID ) == 0;
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.REQUEST ) != null )
		{
			request = ( Request )viewMap.get( WebConstants.REQUEST ) ;
			request.setUpdatedBy(getLoggedInUserId());
		}
		if( viewMap.get( WebConstants.ZakatDeduction.ZakatList) != null )
		{
			dataList = ( ArrayList<Zakat> )viewMap.get( WebConstants.ZakatDeduction.ZakatList ) ;
		}
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( request != null )
		{
		  viewMap.put(  WebConstants.REQUEST , request);
		}
		
		if( dataList != null )
		{
			viewMap.put( WebConstants.ZakatDeduction.ZakatList,dataList);
		}
	}
	

	@SuppressWarnings( "unchecked" )
	private void getDataFromRequest()throws Exception
	{
		RequestView requestView = ( RequestView )getRequestMap().get( WebConstants.REQUEST_VIEW) ;
		if( requestView !=null )
		{
		  getZakatRequest( requestView.getRequestId() );
		  populateApplicationDetailsTab();
		}
	}
	
	@SuppressWarnings(  "unchecked"  )
	private void populateApplicationDetailsTab()throws Exception
	{
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		String status = "";
	
		
		String applicantName  = request.getApplicant() != null ?request.getApplicant().getFullName():"";
		DomainDataView ddv = CommonUtil.getDomainDataView(WebConstants.REQUEST_STATUS,  request.getStatusId() );
		status = isEnglishLocale()?ddv.getDataDescEn():ddv.getDataDescAr();
		bean.populateApplicationDetails(
				                         request.getRequestNumber(), 
					                     status, 
					                     request.getRequestDate(), 
					                     request.getDescription(), 
					                     applicantName, 
					                     "", 
					                     "", 
					                     ""
					                   );
		if( request.getApplicant() != null && request.getApplicant().getPersonId() != null  )
		{
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,request.getApplicant().getPersonId() );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, request.getApplicant().getPersonId() );
		}
		if ( request!= null && request.getStatusId() != null && 
			!request.getStatusId().equals( WebConstants.REQUEST_STATUS_NEW_ID ) 
		    )
		{
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}
	}
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  UserTask userTask = ( UserTask )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  setUserTask(userTask);
	  if( userTask.getTaskAttributes().get(  WebConstants.REQUEST_ID ) == null ){ return; }
	  long  id = new Long  ( userTask.getTaskAttributes().get( WebConstants.REQUEST_ID ) );
	  getZakatRequest( id );
	  populateApplicationDetailsTab();
	
	  
    }
	
	@SuppressWarnings( "unchecked" )
	private void getZakatRequest( long id ) throws Exception 
	{
		request = zakatDeductionService.getZakatDeductionRequestById( id );
		dataList = zakatDeductionService.getZakatListByRequestId( request.getRequestId());
		if( dataList != null )
		{
			setZakatListRecordsSize( dataList.size() );
		}
		else
		{
			setZakatListRecordsSize( 0 );
		}
		totalZakatAmount  = 0.0d;
		for (Zakat zakat : dataList) 
		{
			totalZakatAmount += zakat.getAmountToDeduct();
			
		} 
		setTotalZakatAmount(totalZakatAmount);
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(request.getRequestId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.REQUEST;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, request.getRequestId());
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.REQUEST;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(request.getRequestId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, request.getRequestId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( request != null && request.getRequestId()!= null )
		 {
			 loadAttachmentsAndComments( request.getRequestId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }

	
	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if(request == null || request.getRequestId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,request.getRequestId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings( "unchecked")
	public void onNameClicked()  
	{
		try
		{
			Zakat row = (Zakat) dataTable.getRowData();
			if( 
				row.getInheritanceFile() != null && row.getInheritanceFile().getInheritanceFileId() != null &&
				row.getInheritanceFile().getFileOwner().getPersonId().compareTo( row.getPerson().getPersonId() ) == 0
			  )
			{
				sessionMap.put( WebConstants.InheritanceFile.FILE_ID, row.getInheritanceFile().getInheritanceFileId() );
			    sessionMap.put( WebConstants.InheritanceFilePageMode.IS_POPUP, true);
				executeJavaScript( "javaScript:openFile();");
			}
			else
			{
				executeJavaScript( "javaScript:openBeneficiaryPopup("+ row.getPerson().getPersonId() +");");
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "onNameClicked|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
//		if( blockingList==null || blockingList.size() <= 0) 
//		{
//			errorMessages.add(CommonUtil.getBundleMessage("blocking.atleastOneBlockingRequired"));
//			tabPanel.setSelectedTab(TAB_DETAILS);
//			return true;
//		}
		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			saveRequestInTransaction();
			getZakatRequest( request.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
			successMessages.add( ResourceUtil.getInstance().getProperty("thirdPartRevenue.msg.saved"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	
	private void saveRequestInTransaction() throws Exception 
	{
		try
		{
			
			Long status = 9001l;
            if(request.getStatusId() != null)
            {
            	status = null;
            }
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistRequest( status );
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistRequest(Long status) throws Exception 
	{
	
		if(status!=null) request.setStatusId(status);
		zakatDeductionService.persistZakatDeductionRequest( request, dataList , getLoggedInUserId() );
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onSubmit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		String event  =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		try	
		{	
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			ApplicationContext.getContext().getTxnContext().beginTransaction();	 

			 persistRequest(  WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID );
			 getZakatRequest( request.getRequestId() );
			 updateValuesToMap();
			 invokeBPEL();			 
			 saveCommentsAttachment( event );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSubmit --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	/**
	 * @throws Exception
	 * @throws RemoteException
	 */
	private void invokeBPEL() throws Exception, RemoteException 
	{
		 SystemParameters parameters = SystemParameters.getInstance();
		 String endPoint= parameters.getParameter( "MEMSZakatDeductionManage" );
		 MEMSZakatDeductionBPELPortClient port=new MEMSZakatDeductionBPELPortClient();
		 port.setEndpoint(endPoint);
		 port.initiate(
				 		CommonUtil.getLoggedInUser(),		
				 		Integer.parseInt( request.getRequestId().toString()),
				 		request.getRequestNumber(), 
				 		null, 
				 		null
				 	   );
	}
	@SuppressWarnings( "unchecked" )
	public void onResubmit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		try	
		{
			 updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 		 
			 persistRequest( WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED_ID);
			 getZakatRequest( request.getRequestId() );
			 
			 setTaskOutCome(TaskOutcome.OK);
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onResubmitted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onReviewed()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_REVIEWED;
		String event  =  MessageConstants.RequestEvents.REQUEST_REVIEWED;
		try	
		{
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			 

			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 	
			 UtilityService utilityService = new UtilityService();
			 ReviewRequestView reviewRequestView = ( ReviewRequestView ) viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW );
			 utilityService.persistReviewRequest( reviewRequestView );
				
			 persistRequest( WebConstants.REQUEST_STATUS_REVIEW_DONE_ID );
			 getZakatRequest( request.getRequestId() );
			 
			 setTaskOutCome(TaskOutcome.OK);
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onReviewed--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	
	public boolean hasReviewErrors() throws Exception
	{
		if( null==cmbReviewGroup.getValue() || cmbReviewGroup.getValue().toString().equals( "-1")) {
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_REVIEW_GRP));
			return true;
		}
		
		
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public void onReviewRequired()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW;
		try	
		{	updateValuesFromMap();	
			if( hasSaveErrors() || hasReviewErrors() || !hasReasonProvided()){ return; }
			 
			 
			 UtilityService utilityService = new UtilityService();
			 ReviewRequestView reviewRequestView = new ReviewRequestView();
			 reviewRequestView.setCreatedBy( CommonUtil.getLoggedInUser() );
			 reviewRequestView.setCreatedOn( new Date() );
			 reviewRequestView.setRfc( txtRemarks.trim() );
			 reviewRequestView.setGroupId( cmbReviewGroup.getValue().toString() );
			 reviewRequestView.setRequestId( request.getRequestId() );
		
			 try 
			 {
					 ApplicationContext.getContext().getTxnContext().beginTransaction();
					 utilityService.persistReviewRequest( reviewRequestView );				
					 persistRequest( WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID);
					 ApplicationContext.getContext().getTxnContext().commit();
			 }
			 catch (Exception exception) 
			 {
					ApplicationContext.getContext().getTxnContext().rollback();
					throw exception;
			 }
			finally
			{
				ApplicationContext.getContext().getTxnContext().release();
			}

			 getZakatRequest( request.getRequestId() );
			 setTaskOutCome(TaskOutcome.FORWARD);
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			logger.LogException("onReviewRequired--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	
	@SuppressWarnings( "unchecked" )
	public void onApproved()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_APPROVED;
		String event  =  MessageConstants.RequestEvents.REQUEST_APPROVED;
		try	
		{
			 updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();

			 persistRequest( WebConstants.REQUEST_STATUS_APPROVED_ID );
			 getZakatRequest( request.getRequestId() );

			 setTaskOutCome(TaskOutcome.APPROVE);
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onApproved--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

		
	@SuppressWarnings( "unchecked" )
	public void onSendBack()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		try	
		{
			updateValuesFromMap();
			if( hasSaveErrors() || !hasReasonProvided() ){ return; }
			
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 persistRequest( WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT_ID );
			 getZakatRequest( request.getRequestId() );
			 
			 setTaskOutCome(TaskOutcome.REJECT);
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSendBack--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onComplete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		String event  =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		try	
		{	
			 
			 if( hasSaveErrors() ){ return; }
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 updateValuesFromMap();	
			 
			 persistRequest( WebConstants.REQUEST_STATUS_COMPLETE_ID );
			 getZakatRequest( request.getRequestId() );
			 
			 setTaskOutCome(TaskOutcome.APPROVE);
	
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
	
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onComplete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onRejectedFromFinance()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_REJECTED_FINANCE;
		String event  =  MessageConstants.RequestEvents.REQUEST_REJECTED_FINANCE;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() || !hasReasonProvided()){ return; }
		 
		 updateValuesFromMap();	
		 
		 persistRequest( WebConstants.REQUEST_STATUS_FINANCE_REJECTED_ID );
		 getZakatRequest( request.getRequestId() );
		 
		 setTaskOutCome(TaskOutcome.REJECT);

		 updateValuesToMap();
		 
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejectedFromFinance--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	private boolean hasReasonProvided() throws Exception
	{
		if(txtRemarks ==null || txtRemarks.trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.reasonRequired")  );
			return false;
		}
		return true;
	}
	@SuppressWarnings( "unchecked" )
	public void onRejected()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String msg    =  MessageConstants.RequestEvents.REQUEST_REJECTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_REJECTED;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( WebConstants.REQUEST_STATUS_REJECTED_ID );
			 setTaskOutCome(TaskOutcome.CLOSE);
			 getZakatRequest( request.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
  	private boolean hasDeleteErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if( request.getRequestId() != null )//&& blockingList.size()==1 )
        {
        	errorMessages.add(ResourceUtil.getInstance().getProperty("BlockingRequest.msg.deleteError"));
        	hasSaveErrors = true;
        }
		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onDelete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			Zakat row = ( Zakat )dataTable.getRowData();
			updateValuesFromMap();
			
			if( hasDeleteErrors() ){ return; }
	        
	        if( row.getZakatId()  != null )
	        {
	        	
	        	zakatDeductionService.deleteZakatFromZakatDeductionRequest(request, row, getLoggedInUserId() );
	        	
	        }
//	        else
//	        {
//	        	zakatListRecordsSize.remove( row );
//	        }
			successMessages.add( ResourceUtil.getInstance().getProperty("BlockingRequest.msg.deletedSuccessfully"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onDelete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = getUserTask();
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    }



	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask )
	{
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}


	public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("zakat.heading.zakatDeductManage"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}
		
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_NEW)  || getPageMode().equals( PAGE_MODE_RESUBMITTED ) )  
				
	      ) 
		{
			return true;
		}
		return false;
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  getPageMode().equals( PAGE_MODE_NEW ) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowResubmitButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_RESUBMITTED ) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowApproveButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_APPROVAL_REQUIRED )  )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowReviewDone()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_REVIEW_REQUIRED)  )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowComplete()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_APPROVED ) )
		{
			return true;
		}
		return false;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


	public HtmlSelectOneMenu getCmbReviewGroup() {
		return cmbReviewGroup;
	}

	public void setCmbReviewGroup(HtmlSelectOneMenu cmbReviewGroup) {
		this.cmbReviewGroup = cmbReviewGroup;
	}

	public List<Zakat> getDataList() {
		return dataList;
	}

	public void setDataList(List<Zakat> dataList) {
		this.dataList = dataList;
	}

	@SuppressWarnings( "unchecked" )
	public Integer getZakatListRecordsSize() 
	{
		if(viewMap.get("zakatListRecordsSize") != null )
		{
			zakatListRecordsSize=Integer.parseInt(viewMap.get("zakatListRecordsSize").toString() );
		}	
		
		return zakatListRecordsSize;
	}

	@SuppressWarnings( "unchecked" )
	public void setZakatListRecordsSize(Integer zakatListRecordsSize) 
	{
		this.zakatListRecordsSize = zakatListRecordsSize;
		if( this.zakatListRecordsSize != null )
		{
			viewMap.put("zakatListRecordsSize",this.zakatListRecordsSize );
		}
	}

	public double getTotalZakatAmount() {
		if(viewMap.get("totalZakatAmount") != null )
		{
			totalZakatAmount=Double.parseDouble( viewMap.get("totalZakatAmount").toString() );
		}	
		
		return totalZakatAmount;
	}

	@SuppressWarnings( "unchecked" )
	public void setTotalZakatAmount(double totalZakatAmount) {
		this.totalZakatAmount = totalZakatAmount;
		viewMap.put("totalZakatAmount",this.totalZakatAmount  );
	}

	

}