package com.avanza.pims.web.mems;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.entity.AmafDistributionPercentage;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.endowment.AmafDistributionPercentageService;
import com.avanza.ui.util.ResourceUtil;

public class SearchAmafDistributionPercent extends AbstractSearchBean 
{
	private static final long serialVersionUID = 1L;
	private final String DEFAULT_SORT_FIELD = "disPercentageId";
	private final String DATA_LIST = "DATA_LIST";
	AmafDistributionPercentageService service = new AmafDistributionPercentageService();

	AmafDistributionPercentage criteria = new AmafDistributionPercentage();
	AmafDistributionPercentage pageObject = new AmafDistributionPercentage();
	List<AmafDistributionPercentage> dataList = new ArrayList<AmafDistributionPercentage>();
	

	@SuppressWarnings("unchecked")
	@Override
	public void init() 
	{
		super.init();
		try 
		{
			if (!isPostBack()) 
			{
				initData();
			}
		} catch (Exception es) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void initData() throws Exception 
	{
		setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
		setSortField(DEFAULT_SORT_FIELD);
		setSortItemListAscending(false);
		if(sessionMap.get(WebConstants.SELECTED_ROW) == null)
		{
			onSearch();
		}
		checkDataInQueryString();
		
	}

	private void checkDataInQueryString() {
		checkPageMode();
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() 
	{

		if (request.getParameter(VIEW_MODE) != null) 
		{

			if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);

			}
			else if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);

			}
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onSingleSelect() 
	{
		try 
		{
			AmafDistributionPercentage obj = null;
			obj = (AmafDistributionPercentage) dataTable.getRowData();
			sessionMap.put(WebConstants.SELECTED_ROW,obj);
			executeJavascript("closeWindow();");
		}
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onSingleSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public void onMultiSelect() 
	{
		try 
		{
			dataList = getDataList();
			List<AmafDistributionPercentage> files = new ArrayList<AmafDistributionPercentage>();
			for(AmafDistributionPercentage obj: dataList)
			{
				if( obj.isSelected() )
				{
					files.add(obj);
				}
			}
			
			if(files.size() <= 0)
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add( CommonUtil.getBundleMessage("endowment.errMsg.noSelectMade") );
				return;
			}
			viewMap.put(WebConstants.SELECTED_ROWS, files);
			executeJavascript("closeWindow();");
		}
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onMultiSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public boolean hasSaveErrors()
	{
		boolean hasErrors = false;
		double  sumPercentage = 0d;
		
		for(AmafDistributionPercentage obj: dataList )
		{

         sumPercentage += obj.getPercentage();
		}
         if( sumPercentage != 100d )
         {
        	 errorMessages.add(
        			 		   ResourceUtil.getInstance().getProperty(
 																	   "amafDistPercentage.msg.percentageMustBeHundred"
 																	 )
 							  );
        	 hasErrors = true;
         }
		return hasErrors;
	}
	public void onSaveChanges()
	{
		errorMessages   = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try 
		{
			getDataList();
			if( hasSaveErrors()  ) return;
			service.persist(dataList,getLoggedInUserId());
			successMessages.add(ResourceUtil.getInstance().getProperty("amafDistPercentage.msg.changeSaved"));
		} 
		catch (Exception e) 
		{
			logger.LogException("onSaveChanges|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public boolean hasDoneErrors()
	{
		boolean hasErrors = false;
        if( pageObject.getDescription()  == null || pageObject.getDescription().trim().length() <= 0 )
        {
           	 errorMessages.add(
           			 		   ResourceUtil.getInstance().getProperty(
    																	   "amafDistPercentage.msg.descriptionReq"
    																	 )
    							  );
           	 hasErrors = true;
        }
		
         if( pageObject.getPercentage() == null || pageObject.getPercentage().compareTo(0d) <= 0 || 
        	 pageObject.getPercentage().compareTo(100d) > 0 
           )
         {
        	 errorMessages.add(
        			 		   ResourceUtil.getInstance().getProperty(
 																	   "amafDistPercentage.msg.percentageRequired"
 																	 )
 							  );
        	 hasErrors = true;
         }
		return hasErrors;
	}

	@SuppressWarnings("unchecked")
	public void onAddToList()
	{
		errorMessages   = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try 
		{
			getDataList();
			AmafDistributionPercentage obj=  (AmafDistributionPercentage )sessionMap.remove(WebConstants.SELECTED_ROW);
			dataList.add(0,obj);
			setDataList(dataList);
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onAddToList|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	public String onAdd() 
	{
		return "manage";
	}
	@SuppressWarnings("unchecked")
	public void onEdit() 
	{
		try 
		{
			getDataList();
			AmafDistributionPercentage row = (AmafDistributionPercentage) dataTable.getRowData();
			sessionMap.put(WebConstants.SELECTED_ROW, row);
			dataList.remove(row);
			setDataList(dataList);
			executeJavascript("javaScript:openPopupAdd();");
		} 
		catch (Exception e) 
		{
			logger.LogException("onEdit |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void onSearch() 
	{
		try 
		{
			loadDataList();
			pageFirst();// at last
		} 
		catch (Exception e) 
		{
			logger.LogException("onSearch |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	
	public void doSearchItemList()
	{
		try {
				loadDataList();
			}
		    catch (Exception e){
		    	errorMessages = new ArrayList<String>(0);
		 		logger.LogException("doSearchItemList|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		    }
	}

	@SuppressWarnings("unchecked")
	public void loadDataList() throws Exception 
	{
		int totalRows = 0;
		totalRows = service.getTotalNumberOfRecords( criteria );
		setTotalRows(totalRows);
		doPagingComputations();

		dataList = service.search(criteria, getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());

		if ( dataList== null || dataList.isEmpty() ) 
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			forPaging(0);
		}
		
		this.setDataList( dataList );
		forPaging(getTotalRows());
	}



	@SuppressWarnings("unchecked")
	public List<AmafDistributionPercentage> getDataList() 
	{
		if(viewMap.get(DATA_LIST) != null)
		{
			dataList = (List<AmafDistributionPercentage>) viewMap.get(DATA_LIST);
		} 
		return dataList ;
	}

	@SuppressWarnings("unchecked")
	public void setDataList(List<AmafDistributionPercentage> list) 
	{
		this.dataList = list;
		if(this.dataList != null )
		{
			viewMap.put(DATA_LIST, list);
		}

	}
	@SuppressWarnings("unchecked")
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javascript);
	}

	public AmafDistributionPercentage getCriteria() {
		return criteria;
	}

	public void setCriteria(AmafDistributionPercentage criteria) {
		this.criteria = criteria;
	}

	@SuppressWarnings("unchecked")
	public AmafDistributionPercentage getPageObject() {
		if( viewMap.get( "pageObject" ) != null )
		{
			pageObject = ( AmafDistributionPercentage )viewMap.get( "pageObject" ); 
		}
		return pageObject;
	}
	@SuppressWarnings("unchecked")
	public void setPageObject(AmafDistributionPercentage pageObject) {
		this.pageObject = pageObject;
		if( this.pageObject  != null )
		{
			viewMap.put( "pageObject",pageObject ); 
		}
	}

	
}
