package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.avanza.pims.entity.AmountDetails;
import com.avanza.pims.entity.OpenBox;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.OpenBoxService;
import com.avanza.ui.util.ResourceUtil;

public class OpenBoxAmountDetailsHistory extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	HttpServletRequest request;
	private OpenBoxService openBoxService;
	OpenBox openBox;
	List<AmountDetails> list = new ArrayList<AmountDetails>();
	public OpenBoxAmountDetailsHistory() 
	{
		openBoxService		        = new OpenBoxService();
		request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		Long openBoxId = null;
		if(sessionMap.get(WebConstants.OpenBox.OpenBox) != null)
		{
			openBox = (OpenBox)sessionMap.remove(WebConstants.OpenBox.OpenBox) ;
			openBoxId = openBox.getOpenBoxId();
			
		}
		else if( request.getParameter("id") != null)
		{
			openBoxId =  new Long(request.getParameter("id").toString());
		}
		list = openBoxService.getAmountDetailsHistoryForOpenBox( openBoxId );
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( list != null )
		{
			viewMap.put( "list", list );
		}
	}

	public List<AmountDetails> getList() {
		return list;
	}

	public void setList(List<AmountDetails> list) {
		this.list = list;
	}	
    

}
