package com.avanza.pims.web.mems.minors;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.CollectionProcedureService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.mems.CollectionPaymentDetailsView;
import com.avanza.pims.ws.vo.mems.CollectionProcedureView;
import com.avanza.pims.ws.vo.mems.DistributePopUpView;
import com.avanza.ui.util.ResourceUtil;

public class DistributePopUpBackingBean extends AbstractMemsBean 
{
	private static final long serialVersionUID = 1L;
	private transient Logger logger = Logger.getLogger(DistributePopUpBackingBean.class);
	private HtmlDataTable dataTable;
	private HtmlDataTable dataTableGeneralAccount;
	private HtmlCommandButton btnDistribute = new HtmlCommandButton();
	private HtmlSelectOneMenu distCritSelectMenu = new HtmlSelectOneMenu();
	private List<SelectItem> distCritList = new ArrayList<SelectItem>();
	private List<DistributePopUpView> assetBeneficiaryDataList = new ArrayList<DistributePopUpView>();
	private HtmlCommandButton btnDone = new HtmlCommandButton();
	private HtmlCommandButton btnDone2 = new HtmlCommandButton();
	private List<DistributePopUpView> generalAccountDataList = new ArrayList<DistributePopUpView>();
    private final String COLL_TRX_VIEW = "CollectionTransactionBatch";
	private final String  PAY_DET_VIEW="PaymentDetailsView";
    CollectionProcedureView collectionTrxView =  new CollectionProcedureView(); 
    CollectionPaymentDetailsView paymentDetailsView  = new CollectionPaymentDetailsView();
    static Map<String,DomainDataView> mapDistributionCriteria = new HashMap<String, DomainDataView>();
    private String rowId ="";
    
	@SuppressWarnings("unchecked")
	public void init() 
	{
		super.init();
		try 
		{
			rowId = "";
			if (!isPostBack()) 
			{
				HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
				getQueryString(request);
				if( sessionMap.get( COLL_TRX_VIEW )!= null )
				{
				  viewMap.put(COLL_TRX_VIEW, (CollectionProcedureView) sessionMap.remove(COLL_TRX_VIEW) );
				 
				}
				if( sessionMap.get(PAY_DET_VIEW )!= null )
				{
					viewMap.put(PAY_DET_VIEW, (CollectionPaymentDetailsView) sessionMap.remove(PAY_DET_VIEW) );
				}
				loadDistributionCriteria();
				loadDataTable();
			}

		} catch (Exception es)
		{
			logger.LogException( "init|Error Occured", es);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}

	@SuppressWarnings( "unchecked" )
	private void getQueryString(HttpServletRequest request) throws Exception
	{
		
		if (request.getParameter(WebConstants.ASSET_ID) != null) 
		{
			viewMap.put(WebConstants.ASSET_ID, request.getParameter(WebConstants.ASSET_ID).toString());

		}
		if (request.getParameter(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null) 
		{
			viewMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_ID,request.getParameter(WebConstants.InheritanceFile.INHERITANCE_FILE_ID).toString());

		}
		if (request.getParameter(WebConstants.PAYMENT_DETAILS_FORM_CRITERIA.AMOUNT) != null) 
		{

			viewMap.put( WebConstants.PAYMENT_DETAILS_FORM_CRITERIA.AMOUNT,request.getParameter(WebConstants.PAYMENT_DETAILS_FORM_CRITERIA.AMOUNT).toString());

		}
		if (request.getParameter("collectionTrxId") != null) {

			viewMap.put("collectionTrxId", request.getParameter("collectionTrxId").toString());

		}
		if (request.getParameter("CONFIRM") != null)// FOR PageModeConfirm belonging to CollectionProcedure
		{

			viewMap.put("PageModeConfirm", request.getParameter("CONFIRM").toString());

		}
	}
	public void prerender()
	{
		if( !isPostBack() )
		{
			 CollectionProcedureView view = (CollectionProcedureView) viewMap.get(COLL_TRX_VIEW);
			 if( view.getDistributionOption()!= null )
			 {
			  distCritSelectMenu.setValue( view.getDistributionOption().toString() );
//			  distributionCritChanged();
			 }
			 btnDone.setRendered( !view.isDistributed() );
			 btnDone2.setRendered( !view.isDistributed() );
		}
		
	}
	private void loadDataTable() throws Exception
	{
		Long assetId = null;
		Long inheritanceFileId = null;
		List<DistributePopUpView> view = new ArrayList<DistributePopUpView>();
		if (viewMap.get(WebConstants.ASSET_ID) != null)
		{
			assetId = Long.valueOf(viewMap.get(WebConstants.ASSET_ID).toString());
		}
		if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null)
		{
			inheritanceFileId = Long.valueOf(viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID).toString());
		}
		CollectionProcedureService service = new CollectionProcedureService();
		
		//If collection trx is against the asset 
		if (assetId != null) 
		{
//			Boolean generalAccountSelected=(Boolean)viewMap.get("generalAccountSelected");
//			if (generalAccountSelected==null || !generalAccountSelected)
//			{
				view = service.getBeneficiariesByAssetId(assetId,inheritanceFileId);
				getSelectedBeneficiaries(view);
				this.setAssetBeneficiaryDataList(view);
//			}
			//if beneficiary shares are not defined against asset than get all beneficiaries from file
			if ( (view ==null || view.size()<=0) && 
				 inheritanceFileId != null) 
			{
				view = service.getBeneficiariesByInheritanceFileId(inheritanceFileId);
				getSelectedBeneficiaries(view);
				this.setAssetBeneficiaryDataList(view);
			}		
		}
		//If collection trx is not against the asset 
		else if (inheritanceFileId != null) 
		{
			view = service.getBeneficiariesByInheritanceFileId(inheritanceFileId);
			getSelectedBeneficiaries(view);
			this.setAssetBeneficiaryDataList(view);
		}

	}
	public Boolean getPageModeConfirm()
	{
		Boolean returnVal = false;
		if (viewMap.get("PageModeConfirm")!=null)
			returnVal=Boolean.valueOf(viewMap.get("PageModeConfirm").toString());
		return returnVal;
			
	}
	private  void getSelectedOwners( List<DistributePopUpView> fromDb)
	{
		CollectionProcedureView fromParent = (CollectionProcedureView) viewMap.get(COLL_TRX_VIEW);
		DomainDataView ddGeneralAccount = this.getDistributionCriteriaFromType(WebConstants.DistributionCriteria.GENERAL_ACCOUNT);
		for (DistributePopUpView fromDbSingle : fromDb)
		{
			
			if( fromParent.getDistributionInfo()!= null && fromParent.getDistributionInfo().size()>0 )
			{
				for (DistributePopUpView fromParentSingle : fromParent.getDistributionInfo() )
				{
					if( fromParentSingle.getSelected()!= null && fromParentSingle.getSelected() )
					{
						if( fromParentSingle.getFileOwnerId() != null  )
						{
						if( fromDbSingle.getFileOwnerId().compareTo( fromParentSingle.getFileOwnerId() ) ==0 )
						{
							fromDbSingle.setAmount( fromParentSingle.getAmount() );
							fromDbSingle.setBasedOnCriteria( ddGeneralAccount.getDomainDataId() );
							fromDbSingle.setSelected(true);
							
						}
						}
						
					}
					
					
				}
				
				
			}
		}
		
	}
	private  void getSelectedBeneficiaries( List<DistributePopUpView> fromDb)
	{
		CollectionProcedureView fromParent = (CollectionProcedureView) viewMap.get(COLL_TRX_VIEW); 
		for (DistributePopUpView fromDbSingle : fromDb)
		{
				if( fromParent.getDistributionInfo() == null || fromParent.getDistributionInfo().size() <=0 )
				{
					continue;
				}
				for (DistributePopUpView fromParentSingle : fromParent.getDistributionInfo() )
				{
						if( fromParentSingle.getSelected() == null || !fromParentSingle.getSelected() )
						{continue;}
					
						if( fromParentSingle.getInheritedBeneficiaryId()!= null &&
							fromDbSingle.getInheritedBeneficiaryId().compareTo( fromParentSingle.getInheritedBeneficiaryId() ) ==0 )
						{
							System.out.println(fromParentSingle.getAmount() );
							fromDbSingle.setAmount( fromParentSingle.getAmount() );
							fromDbSingle.setSelected(true);
							if( 
									fromParentSingle!= null && 
									fromParentSingle.getIsDistributed() != null && 
									fromParentSingle.getIsDistributed().equals("Y")
							  )
							{
//								fromDbSingle.setTotalFileShares( 
//																fromParentSingle.getTotalFileShares() != null ?fromParentSingle.getTotalFileShares() :0D
//									 							);
								fromDbSingle.setShariaPercent( 
																fromParentSingle.getShariaPercent() != null ?fromParentSingle.getShariaPercent() :0D
															 );
								
//								fromDbSingle.setTotalAssetShares(
//																 fromParentSingle.getTotalAssetShares() != null ?fromParentSingle.getTotalAssetShares() :0D
//									 							);
								fromDbSingle.setAssetPercent( 
										 					 fromParentSingle.getAssetPercent()  != null ?fromParentSingle.getAssetPercent() :0D
			 												);
		
							}
							
						}
				}
		}
	}
    @SuppressWarnings( "unchecked" )
	private void loadDistributionCriteria() throws Exception 
	{
		List<SelectItem> distCritList = new ArrayList<SelectItem>();
		List<DomainDataView> distCriteriaList = new UtilityService().getDomainDataByDomainTypeName(Constant.DistributionCriteria.LIST_KEY);
		for (DomainDataView domainDataView : distCriteriaList) 
		{
			boolean addItem = true;

			if (  addItem )
			{
				SelectItem selectItem = new SelectItem();
				selectItem.setLabel( isEnglishLocale() ? domainDataView.getDataDescEn(): domainDataView.getDataDescAr() );
				selectItem.setValue( domainDataView.getDomainDataId().toString() );
				distCritList.add( selectItem );
				mapDistributionCriteria.put(  domainDataView.getDomainDataId().toString() , domainDataView );
			}
		}
		Collections.sort(distCritList, ListComparator.LIST_COMPARE);
		this.setDistCritList(distCritList);
	}
	public boolean getIsGeneralAccountSelected() 
	{
		boolean returnVal = false;
		if (viewMap.containsKey("generalAccountSelected")) 
		{
		
				returnVal = true;
		}

		else
		{
			returnVal = false;
		}

		return returnVal;

	}

	public boolean getIsCollectionIdAvailable() {
		boolean returnVal = false;

		if (viewMap.containsKey("collectionTrxId")) {

			returnVal = true;
		}

		else {
			returnVal = false;
		}

		return returnVal;

	}


	public List<SelectItem> getAssetTypesList() {
		List<SelectItem> assetTypes = new ArrayList<SelectItem>();
		try {

			assetTypes = CommonUtil.getAssetTypesList();

		} catch (PimsBusinessException e) {
			logger.LogException("getAssetTypesList() crashed", e);
		}
		return assetTypes;
	}

	public List<DistributePopUpView> getSelectedRows() throws Exception 
	{
		List<DistributePopUpView> selectedRowsList = new ArrayList<DistributePopUpView>(0);
		assetBeneficiaryDataList = getAssetBeneficiaryDataList();
		if (assetBeneficiaryDataList != null&& assetBeneficiaryDataList.size() > 0) 
		{
			for (DistributePopUpView selectedRows : assetBeneficiaryDataList)
			{
				// IF ANY SELECTED ROW HAS THE CONFLICBOOLEAN TO TRUE ,
				// RAISE ERROR here
				if ( selectedRows.getSelected()!= null && selectedRows.getSelected() ) 
				{
					selectedRowsList.add(selectedRows);

				}
			}

		}
		return selectedRowsList;
	}
	public List<DistributePopUpView> getSelectedRowsForOwner() throws Exception 
	{

		List<DistributePopUpView> selectedRowsList = new ArrayList<DistributePopUpView>(0);

		generalAccountDataList  = getGeneralAccountDataList();
		if (generalAccountDataList   != null&& generalAccountDataList  .size() > 0) 
		{
			for ( DistributePopUpView selectedRows : generalAccountDataList  )
			{
				// IF ANY SELECTED ROW HAS THE CONFLICBOOLEAN TO TRUE ,
				// RAISE ERROR here
				if ( selectedRows.getSelected()!= null && selectedRows.getSelected() ) 
				{
					selectedRowsList.add(selectedRows);

				}
			}

		}
		return selectedRowsList;
	}


	public boolean hasErrors(boolean fromDone)throws Exception  
	{
		boolean hasErrors = false;
		boolean hasSelected = false;
		boolean hasWrongCriteriaSelected = false;
		boolean hasNoAmount = false;
		boolean hasNoCostCenter=false;
		rowId= "";
			if(viewMap.get("generalAccountSelected")!=null)
			{
				Boolean generalAccountSelected=((Boolean)(viewMap.get("generalAccountSelected")));
				if(generalAccountSelected)
				{
					generalAccountDataList=getGeneralAccountDataList();
					if(generalAccountDataList!=null && generalAccountDataList.size()>0)
					{
						for(DistributePopUpView selectedRows : generalAccountDataList)
						{
							if(selectedRows.getSelected())
							{
								hasSelected=true;
								if(selectedRows.getAmount().trim().equals(""))
								{
									hasNoAmount=true;
									if(selectedRows.getCostCenter() == null ||selectedRows.getCostCenter().trim().length() <= 0 )
									{
										rowId +=selectedRows.getInheritedBeneficiaryId().toString();
										hasNoCostCenter=true;	
									}
									
								}
								
							}
							
							
						}
					}
					if( hasNoCostCenter )
					{
						hasErrors = true;
						errorMessages.add(CommonUtil.getBundleMessage( "collectionProcedure.msg.beneficiaryCostCenterEmpty" ) );
					}
					if (!hasSelected || generalAccountDataList == null || generalAccountDataList.size() <= 0  ) 
					{
						hasErrors = true;
						errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.NO_ROW_SELECTED));

					}
					if (hasNoAmount  ) 
					{
						hasErrors = true;
						errorMessages.add(CommonUtil.getBundleMessage(WebConstants.COLLECTION_ERROR_DIST_CRIT_NO_AMOUNT));

					}
				}
				
				
			}
			else
			{

				assetBeneficiaryDataList = getAssetBeneficiaryDataList();
				Double sumAssetShares = 0.00d;
				Double sumFileShares = 0.00d;
				Double sumDistributedAmount= 0.00d;
				DistributePopUpView lastObj = null;
				if (assetBeneficiaryDataList != null && assetBeneficiaryDataList.size() > 0) 
				{
					 
					for (DistributePopUpView selectedRows : assetBeneficiaryDataList) 
					{
					
						
						System.out.println("Selected Row Trx Det Id:"+selectedRows.getCollectionTrxDetailId() );
						if( selectedRows.getAmount() != null && selectedRows.getAmount().trim().length() > 0 )
						{
							logger.logDebug("Selected Row:%s",roundDecimals( Double.valueOf( selectedRows.getAmount() ) ));
							sumDistributedAmount = roundDecimals( sumDistributedAmount + Double.valueOf( selectedRows.getAmount() ) );
						}
						if( selectedRows.getShariaPercent() != null )
						{
							logger.logDebug("sumFileShares :%s|selectedRows.getShariaPercent() :%s",sumFileShares ,selectedRows.getShariaPercent() );
							sumFileShares =roundDecimals(   sumFileShares +selectedRows.getShariaPercent() );
						}
						if( selectedRows.getAssetPercent() != null )
						{
							logger.logDebug("SumAssetShares:%s|selectedRows.getAssetPercent():%s",sumAssetShares,selectedRows.getAssetPercent());
							sumAssetShares  = roundDecimals( sumAssetShares +selectedRows.getAssetPercent() );
						}
						// IF ANY SELECTED ROW HAS THE CONFLICBOOLEAN TO TRUE ,
						// RAISE ERROR here
						if (selectedRows != null && selectedRows.getSelected() != null && selectedRows.getSelected())
						{
							hasSelected = true;
							if(selectedRows.getCostCenter() == null ||selectedRows.getCostCenter().trim().length() <= 0 )
							{
								rowId +=selectedRows.getInheritedBeneficiaryId().toString()+",";
								hasNoCostCenter=true;
							}
						}
						lastObj = selectedRows;
						
						System.out.println("Sum:"+sumDistributedAmount );
					}
					if( fromDone && sumDistributedAmount.doubleValue() !=  getCollectionTrxView().getAmount().doubleValue() )
					{
						String msg = java.text.MessageFormat.format(
																	 CommonUtil.getBundleMessage( "collectionProcedure.msg.totalAmountToDistributeNotEqualSumOfDistribution" ),
																	 getCollectionTrxView().getAmount(),
																	 sumDistributedAmount.doubleValue()
	 
																	);
						hasErrors = true;
						errorMessages.add(msg);
						
					}
	
				}
				Object selectedCriteria = distCritSelectMenu.getValue();
				if(selectedCriteria==null || selectedCriteria.toString().compareToIgnoreCase("-1")==0)
				{
					 hasWrongCriteriaSelected=true;
				}
				 
				CollectionProcedureView view = (CollectionProcedureView) viewMap.get(COLL_TRX_VIEW);
				if( hasNoCostCenter )
				{
						hasErrors = true;
						errorMessages.add(CommonUtil.getBundleMessage( "collectionProcedure.msg.beneficiaryCostCenterEmpty" ) );
				}	
				 
				if(view.getAmount()==null)
					hasNoAmount=true;
				
				if (!hasSelected || assetBeneficiaryDataList == null || assetBeneficiaryDataList.size() <= 0 || hasWrongCriteriaSelected ) 
				{
					hasErrors = true;
					errorMessages.add(CommonUtil.getBundleMessage(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.NO_ROW_SELECTED));
	
				}
				if(hasNoAmount)
			    {
					hasErrors=true;
					errorMessages.add(CommonUtil.getBundleMessage(WebConstants.COLLECTION_ERROR_DIST_CRIT_NO_AMOUNT));
				}
				
				else if( 
						this.distCritSelectMenu.getValue().equals( String.valueOf( WebConstants.DistributionCriteria.BASED_ON_ASSET_SHARES_ID ) ) && 	
						lastObj.getTotalAssetShares() != null && lastObj.getTotalAssetShares() > 0 &&
						lastObj.getTotalAssetShares().compareTo( sumAssetShares ) != 0
					  )
				{
								String msg = java.text.MessageFormat.format(  
																			 CommonUtil.getBundleMessage("openFileDistribute.msg.totalAssetSharesNotEqualSumAssetShares"),
																			 lastObj.getTotalAssetShares(),
																			 sumAssetShares
								
																			);
								errorMessages.add(msg);
								hasErrors=true;

				}
				else if( 
						this.distCritSelectMenu.getValue().equals( String.valueOf( WebConstants.DistributionCriteria.BASED_ON_SHARIA_ID ) ) && 	
						lastObj.getTotalFileShares() != null && lastObj.getTotalFileShares()  > 0 &&
						lastObj.getTotalFileShares().compareTo( sumFileShares ) != 0
					  )
				{
								String msg = java.text.MessageFormat.format(  
																			 CommonUtil.getBundleMessage("openFileDistribute.msg.totalFileSharesNotEqualSumFileShares"),
																			 lastObj.getTotalFileShares(),
																			 sumAssetShares
								
																			);
								errorMessages.add(msg);
								hasErrors=true;

				}
//				else if( sumDistributedAmount.compareTo( totalAmount )!= 0 )
//				{
//					
//					String msg = java.text.MessageFormat.format(  
//																 CommonUtil.getBundleMessage("commons.msg.totalAmountToDistributeNotEqualToSumAmount"),
//																 totalAmount ,
//																 sumDistributedAmount
//																);
//					errorMessages.add(msg);
//					hasErrors=true;
//				}
			}

		return hasErrors;
	}

	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}

	public String getSuccessMessages() {
		String methodName = "getSuccessMessages";
		logger.logInfo(methodName + "|" + " Start");

		String messageList = "";
		if ((successMessages == null) || (successMessages.size() == 0)) {
			messageList = "";
		} else {
			for (String message : successMessages) {
				messageList += message + " <br></br>";
			}

		}

		logger.logInfo(methodName + "|" + " Finish");
		return messageList;

	}
	public void onChkChange()
	{
		try
		{
			DistributePopUpView changedRow =(DistributePopUpView) dataTable.getRowData();
			if( changedRow.getSelected() != null && !changedRow.getSelected() )
			{
				changedRow.setAmount("");
			}
			btnDistribute_Click();
		}
		catch (Exception e)
		{
			errorMessages = new ArrayList<String>(0);
    		logger.LogException("onChkChange|Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
	}
	public void btnDistribute_Click( ) 
	{
	 try
	 {
		List<DistributePopUpView> selectedRows = getSelectedRows();
		Double amount = null;
		errorMessages = new ArrayList<String>();
		btnDone.setRendered(true);
		btnDone2.setRendered(true);
		CollectionProcedureView view = (CollectionProcedureView) viewMap.get(COLL_TRX_VIEW);
		amount = view.getAmount();
		DomainDataView ddView = mapDistributionCriteria.get( this.distCritSelectMenu.getValue().toString() );
		
		if (this.distCritSelectMenu.getValue().toString().trim().compareTo("-1") == 0) 
		{
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.COLLECTION_ERROR_DIST_CRIT_NOT_SELECTED));
			return;
		}
		if (hasErrors(false)) 
		{ 
			btnDone.setRendered(false);
			btnDone2.setRendered(false);
			return;
		} 
		double sumCaluclatedAmount = 0.00d;
		DistributePopUpView lasObj = new DistributePopUpView();
		
		for (DistributePopUpView distributePopUpView : selectedRows) 
		{
			if (
					ddView.getDataValue().compareTo(WebConstants.DistributionCriteria.BASED_ON_SHARIA) == 0 ||
					ddView.getDataValue().compareTo( WebConstants.DistributionCriteria.BASED_ON_SHARIA_SELECTED) == 0
			   ) 
			{
			  if( distributePopUpView.getTotalFileShares() != null && distributePopUpView.getTotalFileShares() > 0 &&
				  distributePopUpView.getShariaPercent()   != null && distributePopUpView.getShariaPercent()   > 0 
				 )
			   {
				Double caluclatedAmount = ( distributePopUpView.getFileShareRatio() * amount ) ;
				
				caluclatedAmount=roundDecimals(caluclatedAmount);
				sumCaluclatedAmount +=caluclatedAmount; 
				distributePopUpView.setAmount(caluclatedAmount.toString());
				distributePopUpView.setBasedOnCriteria(Long.parseLong(this.distCritSelectMenu.getValue().toString()));
			   }
			  else
			  {
				  errorMessages.add(CommonUtil.getBundleMessage("collection.msg.FileShareorShariaShareRequired"));
				  btnDone.setRendered(false);
				  btnDone2.setRendered(false);
					return;
			  }

			}
			else if (
						ddView.getDataValue().compareTo( WebConstants.DistributionCriteria.BASED_ON_ASSET_SHARES ) == 0 ||
						ddView.getDataValue().compareTo( WebConstants.DistributionCriteria.BASED_ON_ASSET_SHARES_SELECTED ) == 0 
					) 
			{
				 if( 
					distributePopUpView.getTotalAssetShares() != null && distributePopUpView.getTotalAssetShares() > 0 &&
					distributePopUpView.getAssetPercent()     != null && distributePopUpView.getAssetPercent()     > 0 
				   )
			    {

					Double caluclatedAmount = ( distributePopUpView.getAssetShareRatio() * amount) ;
					caluclatedAmount=roundDecimals(caluclatedAmount);
					sumCaluclatedAmount +=caluclatedAmount;
					distributePopUpView.setAmount(caluclatedAmount.toString());
					distributePopUpView.setBasedOnCriteria(Long.parseLong(this.distCritSelectMenu.getValue().toString()));
				}
				else
				{
					  errorMessages.add(CommonUtil.getBundleMessage("collection.msg.AssetShareorShariaShareRequired"));
					  btnDone.setRendered(false);
					  btnDone2.setRendered(false);
						return;
				}
			}
			else if (ddView.getDataValue().compareTo(WebConstants.DistributionCriteria.EQUALLY) == 0) 
			{

				Double caluclatedAmount = (amount)/ selectedRows.size();
				caluclatedAmount=roundDecimals(caluclatedAmount);
				sumCaluclatedAmount +=caluclatedAmount;
				distributePopUpView.setAmount(caluclatedAmount.toString());
				distributePopUpView.setBasedOnCriteria(Long.parseLong(this.distCritSelectMenu.getValue().toString()));
			}
			else if (ddView.getDataValue().compareTo(WebConstants.DistributionCriteria.GENERAL_ACCOUNT) == 0) 
			{

				Double caluclatedAmount = (amount)/ selectedRows.size();
				caluclatedAmount=roundDecimals(caluclatedAmount);
				sumCaluclatedAmount +=caluclatedAmount;
				distributePopUpView.setAmount(caluclatedAmount.toString());
				distributePopUpView.setBasedOnCriteria(Long.parseLong(this.distCritSelectMenu.getValue().toString()));
			}
			lasObj = distributePopUpView;
			
		}
		double totalAmount = getCollectionTrxView().getAmount();
		if(	
			(
			ddView.getDataValue().compareTo( WebConstants.DistributionCriteria.BASED_ON_ASSET_SHARES_SELECTED ) == 0 ||
			ddView.getDataValue().compareTo( WebConstants.DistributionCriteria.BASED_ON_SHARIA_SELECTED) == 0
			) && sumCaluclatedAmount < totalAmount 
		  )
		{
			Double caluclatedAmount = roundDecimals( (totalAmount - sumCaluclatedAmount )/ selectedRows.size()) ;
			
			for (DistributePopUpView distributePopUpView : selectedRows) 
			{
				
				sumCaluclatedAmount +=caluclatedAmount;
				Double amt = roundDecimals(	Double.valueOf(  distributePopUpView.getAmount()) +caluclatedAmount );
				distributePopUpView.setAmount( 
												amt.toString() 
											 );
				lasObj = distributePopUpView;
			}
		}
		
		if( sumCaluclatedAmount> totalAmount )
		{
			Double newAmount = roundDecimals( Double.valueOf( lasObj.getAmount()) - roundDecimals( ( sumCaluclatedAmount-amount ) ) );  
			lasObj.setAmount(  newAmount.toString() );
				
		}
		else if( sumCaluclatedAmount < totalAmount  )
		{
			double lastObjAmount = 	Double.valueOf( lasObj.getAmount()!= null && lasObj.getAmount().trim().length()>0?lasObj.getAmount():"0");
			Double newAmount = roundDecimals( 
												lastObjAmount	+ ( amount-sumCaluclatedAmount)
											);  
//			if( newAmount.compareTo(anotherDouble) )
			lasObj.setAmount(  newAmount.toString() );
				
		}
		this.getAssetBeneficiaryDataList();
	 }
	 catch(Exception e)
	 {
			errorMessages = new ArrayList<String>(0);
    		logger.LogException("btnDistribute_Click|Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
			
	 }

	}
	
	double roundDecimals(double d) {
    	DecimalFormat oneDForm = new DecimalFormat("#.00");
	return Double.valueOf(oneDForm.format(d));
}
    @SuppressWarnings( "unchecked" )
	public void distributionCritChanged() 
    {
		errorMessages = new ArrayList<String>(0);
		try 
		{
			List<DistributePopUpView> list = this.getAssetBeneficiaryDataList();
			if( viewMap.containsKey("generalAccountSelected") )
			{
				CollectionProcedureView fromParent = (CollectionProcedureView) viewMap.get(COLL_TRX_VIEW);
				fromParent.setDistributionInfo(new ArrayList<DistributePopUpView>(0));
				viewMap.remove("generalAccountSelected");
				for (DistributePopUpView distributePopUpView : list) 
				{
					distributePopUpView.setCheckBoxRender(0L);
				}
				loadDataTable();
			}
			else
			{
						long distCriteria= 	Long.valueOf(  this.distCritSelectMenu.getValue().toString() );
						for (DistributePopUpView obj : list) 
						{
							if(
								distCriteria == WebConstants.DistributionCriteria.BASED_ON_SHARIA_ID.longValue() 
							  )
							{
								if	 ( obj.getShariaPercent() == null || obj.getShariaPercent().compareTo(0D) == 0 )
								{
									obj.setCheckBoxRender(1L);
									obj.setSelected(false);
									obj.setAmount("0");
								}
								else
								{
									obj.setCheckBoxRender(0L);
									obj.setSelected(true);
									
								}
							}
							else if(
										distCriteria == WebConstants.DistributionCriteria.BASED_ON_ASSET_SHARES_ID.longValue() 
								  	)
							{
									if	 ( obj.getAssetPercent()  == null || obj.getAssetPercent().compareTo(0D) == 0 )
									{
										obj.setCheckBoxRender(1L);
										obj.setSelected(false);
										obj.setAmount("0");
									}
									else
									{
										obj.setCheckBoxRender(0L);
										obj.setSelected(true);
									}
						   }
						   else
						   {
								obj.setCheckBoxRender(0L);
//								obj.setSelected(false);
//								obj.setAmount("0");
						   }
						}
			}
			btnDistribute_Click();

		}
		catch (Exception e) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("distributionCritChanged|Error Occured...", e);
		}
	}
	
	
    @SuppressWarnings( "unchecked" )
	public void btnDone_Click(ActionEvent event) 
	{
		try 
		{
		 if ( !hasErrors(true) ) 
		 {
			if (viewMap.get(COLL_TRX_VIEW) != null && viewMap.get( PAY_DET_VIEW )!= null ) 
			{
				CollectionProcedureView parentRow = null;
				CollectionPaymentDetailsView paymentDetailsView= ( CollectionPaymentDetailsView )viewMap.get( PAY_DET_VIEW );
				parentRow = (CollectionProcedureView) viewMap.get(COLL_TRX_VIEW);
				
				if( distCritSelectMenu.getValue() != null && !distCritSelectMenu.getValue().toString().equals("-1") )
				{
				 parentRow.setDistributionOption( new Long( distCritSelectMenu.getValue().toString() ) );
				}
				DomainDataView generalAccount = new UtilityService().getDomainDataByValue(Constant.DistributionCriteria.LIST_KEY,Constant.DistributionCriteria.LIST_GENERAL_ACCOUNT);
				if ( generalAccount.getDomainDataId().compareTo( Long.valueOf( distCritSelectMenu.getValue().toString() ) ) == 0 )
				{
					parentRow.setIsDistributedByGeneralAccount(true);
					if(getSelectedRowsForOwner() != null )
					{
						parentRow.setDistributionInfo(getSelectedRowsForOwner());
					}
				}
				else if (getSelectedRows() != null)
				{
					parentRow.setDistributionInfo(getSelectedRows());
				}
				sessionMap.put( COLL_TRX_VIEW,parentRow );
				paymentDetailsView.setSingleCollectionTrxView(parentRow);
				sessionMap.put( PAY_DET_VIEW, paymentDetailsView);
				String javaScriptText = "window.opener.populateDistributionDetails(); "+ 
				                        "window.close();";
				sendToParent(javaScriptText);
		    }
		}
		} 
		catch (Exception ex)
		{
			logger.LogException("btnDone_Click.distributepopupbacking crashed",ex);
		    errorMessages = new ArrayList<String>(0);
	    	errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	
	


	public String sendToParent(String javaScript) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText = javaScript;
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		return "";
	}

	
	public HtmlSelectOneMenu getDistCritSelectMenu() {
		return distCritSelectMenu;
	}

	public void setDistCritSelectMenu(HtmlSelectOneMenu distCritSelectMenu) {
		this.distCritSelectMenu = distCritSelectMenu;
	}
@SuppressWarnings( "unchecked")
	public List<SelectItem> getDistCritList() {
		distCritList = (ArrayList<SelectItem>) viewMap.get("distributionCriteriaList");
		if (distCritList == null)
			distCritList = new ArrayList<SelectItem>();
		return distCritList;
	}
@SuppressWarnings( "unchecked")
	public void setDistCritList(List<SelectItem> distCritList) {
		this.distCritList = distCritList;
		if (distCritList != null)
			viewMap.put("distributionCriteriaList", distCritList);
	}
@SuppressWarnings( "unchecked")
	public List<DistributePopUpView> getAssetBeneficiaryDataList() 
	{
		assetBeneficiaryDataList = (List<DistributePopUpView>) viewMap.get("assetBeneficiaryDataList");
		if (assetBeneficiaryDataList == null)
			assetBeneficiaryDataList = new ArrayList<DistributePopUpView>();
		Collections.sort(assetBeneficiaryDataList, new ListComparator("getBeneficiaryName"));
		return assetBeneficiaryDataList;
	}
@SuppressWarnings( "unchecked")
	public void setAssetBeneficiaryDataList(
			List<DistributePopUpView> assetBeneficiaryDataList) {
		this.assetBeneficiaryDataList = assetBeneficiaryDataList;
		if (assetBeneficiaryDataList != null) {
			viewMap.put("assetBeneficiaryDataList",
					assetBeneficiaryDataList);
		}
	}

	public HtmlCommandButton getBtnDistribute() {
		return btnDistribute;
	}

	public void setBtnDistribute(HtmlCommandButton btnDistribute) {
		this.btnDistribute = btnDistribute;
	}

	public HtmlDataTable getDataTableGeneralAccount() {
		return dataTableGeneralAccount;
	}

	public void setDataTableGeneralAccount(HtmlDataTable dataTableGeneralAccount) {
		this.dataTableGeneralAccount = dataTableGeneralAccount;
	}
	@SuppressWarnings( "unchecked" )
	public List<DistributePopUpView> getGeneralAccountDataList() 
	{
		generalAccountDataList = (List<DistributePopUpView>) viewMap.get("generalAccountDataList");
		if (generalAccountDataList == null)
			generalAccountDataList = new ArrayList<DistributePopUpView>();
		return generalAccountDataList;
	}
	@SuppressWarnings( "unchecked" )
	public void setGeneralAccountDataList(List<DistributePopUpView> generalAccountDataList) 
	{
		this.generalAccountDataList = generalAccountDataList;
		
		if (generalAccountDataList != null) {
			viewMap.put("generalAccountDataList",
					generalAccountDataList);
		}
	}
	@SuppressWarnings( "unchecked" )
	 public DomainDataView getDistributionCriteriaFromType( String disCriteria)
	 {
		String viewKey = "DistributionCriteria"+disCriteria;
		 DomainDataView ddv;
		 if ( viewMap.get( viewKey ) != null )
		 {
			 ddv = (DomainDataView)viewMap.get( viewKey );
		 }
		 else
		 {
			 
			 ddv = CommonUtil.getIdFromType( this.getDistributionCriteriaList(), disCriteria );
			 viewMap.put( viewKey,ddv);
		 
		 } 
	 
	    return ddv;
	 }

	@SuppressWarnings( "unchecked" )
	 public List<DomainDataView> getDistributionCriteriaList()
	 {
		 List<DomainDataView >ddvList;
		 if ( viewMap.get( "DistributionCriteriaList" ) != null )
		 {
			 ddvList = (ArrayList<DomainDataView>)viewMap.get( "DistributionCriteriaList" );
		 }
		 else
		 {
			 
			 ddvList = CommonUtil.getDomainDataListForDomainType(WebConstants.DistributionCriteria.DISTRIBUTION_CRITERIA);
			 viewMap.put( "DistributionCriteriaList",ddvList );
		 
		 } 
	 
	    return ddvList;
	 }
 
	
  @SuppressWarnings( "unchecked" )
  public void openFilePopUp() 
  {
			try	
			{	
				DistributePopUpView obj = ( DistributePopUpView ) dataTable.getRowData();
				sessionMap.put( WebConstants.InheritanceFile.FILE_ID, obj.getInheritanceFileId() );
			    sessionMap.put (WebConstants.InheritanceFilePageMode.IS_POPUP,true);
				sendToParent(  "javaScript:openFile();" );
			}
			catch (Exception exception) 
			{
				logger.LogException("openFilePopUp| --- CRASHED --- ", exception);
				errorMessages = new ArrayList<String>(0);
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
  }


	

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	

	@SuppressWarnings( "unchecked" )
	 public CollectionProcedureView getCollectionTrxView() 
	 {
		 if( viewMap.get(COLL_TRX_VIEW) != null )
		 {
			 collectionTrxView = (CollectionProcedureView) viewMap.get(COLL_TRX_VIEW);
		 }
		return collectionTrxView;
	 }
	@SuppressWarnings( "unchecked" )
	public void setCollectionTrxView(CollectionProcedureView collectionTrxView) {
		this.collectionTrxView = collectionTrxView;
		if( this.collectionTrxView != null )
		{
			
			viewMap.put(COLL_TRX_VIEW, this.collectionTrxView);
		}
	}
	@SuppressWarnings( "unchecked" )
	public CollectionPaymentDetailsView getPaymentDetailsView() {
		 if( viewMap.get(PAY_DET_VIEW) != null )
		 {
			 paymentDetailsView = (CollectionPaymentDetailsView) viewMap.get(PAY_DET_VIEW);
		 }

		return paymentDetailsView;
	}
	@SuppressWarnings( "unchecked" )
	public void setPaymentDetailsView(CollectionPaymentDetailsView paymentDetailsView) 
	{
		this.paymentDetailsView = paymentDetailsView;
		if( this.paymentDetailsView != null )
		{
		  viewMap.put( PAY_DET_VIEW, this.paymentDetailsView );	
		}
	}

	public String getRowId() {
		return rowId;
	}

	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	public HtmlCommandButton getBtnDone() {
		return btnDone;
	}

	public void setBtnDone(HtmlCommandButton btnDone) {
		this.btnDone = btnDone;
	}

	public HtmlCommandButton getBtnDone2() {
		return btnDone2;
	}

	public void setBtnDone2(HtmlCommandButton btnDone2) {
		this.btnDone2 = btnDone2;
	}


}
