package com.avanza.pims.web.mems.plugins;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlInputText;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.minors.SocialProgramsBean;
import com.avanza.pims.ws.mems.SocialProgramService;
import com.avanza.pims.ws.vo.mems.ProgramSponsorsView;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings( "unchecked" )
public class ProgramSponsorsBean extends AbstractMemsBean
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 587714131589891430L;
	private List<ProgramSponsorsView> list;
	private HtmlDataTable dataTable;
	private ProgramSponsorsView programSponsorsView;
	private String listLoadedFromDb;
	private HtmlInputText txtSponsorName;
	private SocialProgramsBean parentBean;	
	public ProgramSponsorsBean() 
	{	
		programSponsorsView = new ProgramSponsorsView() ;
		list = new ArrayList<ProgramSponsorsView>(0);
		txtSponsorName = new HtmlInputText();
		dataTable = new HtmlDataTable();
		
	}
	
	@Override
	public void init() 
	{
		String METHOD_NAME = "init()";
		errorMessages = new ArrayList<String>(0);
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			parentBean.setErrorMessages( errorMessages );
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws PimsBusinessException
	{
		
		// SPONSOR VIEW
		if ( viewMap.get( WebConstants.SocialProgramSponsors.VO ) != null )
		{
			programSponsorsView = (ProgramSponsorsView) viewMap.get( WebConstants.SocialProgramSponsors.VO );
		}		
		
		// SPONSOR VIEW LIST
		if ( viewMap.get( WebConstants.SocialProgramSponsors.DATA_LIST ) != null )
		{
			list = ( List<ProgramSponsorsView>) viewMap.get( WebConstants.SocialProgramSponsors.DATA_LIST );
		}		
		parentBean = (SocialProgramsBean)getBean( "pages$SocialProgram" );
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		
		// PURPOSE VIEW
		if ( programSponsorsView != null  )
		{
			viewMap.put( WebConstants.SocialProgramSponsors.VO , programSponsorsView);
		}
		
		// LIST
		if ( list != null )
		{
			viewMap.put( WebConstants.SocialProgramSponsors.DATA_LIST, list );
		}
	}	
	
	@SuppressWarnings("unchecked")
	public void onLoadList( Long programId )throws Exception
	{
		final String METHOD_NAME = "onLoadList()";
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( this.getListLoadedFromDb()== null || this.getListLoadedFromDb().equals("0") )
			{
				
				list =  new SocialProgramService().loadProgramSponsors( programId  );
				this.setListLoadedFromDb( "1" );
				updateValuesToMaps();
			}
	}

	
	public String getListLoadedFromDb() {
		if( viewMap.get( "sponsorListLoadedFromDB" ) != null )
		{
			listLoadedFromDb = viewMap.get( "sponsorListLoadedFromDB" ).toString(); 
		}
		return listLoadedFromDb;
	}
    @SuppressWarnings( "unchecked" )
	public void setListLoadedFromDb(String listLoadedFromDb) {
		this.listLoadedFromDb = listLoadedFromDb;
		if( this.listLoadedFromDb != null )
		{
			
			viewMap.put( "sponsorListLoadedFromDB",this.listLoadedFromDb ); 
		}
	}
    public void onAddPersonClick()
	{
		try
		{
			errorMessages = new ArrayList<String>();
			updateValuesFromMaps();
			String javaScript ="javaScript:showPersonPopupForSponsor();";
			List<Long> idToExclude = new ArrayList<Long>( 0 );
			for ( ProgramSponsorsView item : list) 
			{
				if( item.getSponsorId() != null && item.getIsDeleted() !=  null &&!item.getIsDeleted().equals("1") )
				{
					idToExclude.add( Long.valueOf( item.getSponsorId() ) );
				}
			}
			if( idToExclude.size() > 0 )
			{
				sessionMap.put(WebConstants.InheritanceFile.PERSON_IDS_TO_BE_EXCLUDED,idToExclude);
			}
			executeJavaScript(javaScript);
			
		}
		catch ( Exception e )
		{
			logger.LogException( "onAddInternalParticipants --- EXCEPTION --- ", e );
			if( sessionMap.get(WebConstants.InheritanceFile.PERSON_IDS_TO_BE_EXCLUDED)!=null )
			{
				sessionMap.remove(WebConstants.InheritanceFile.PERSON_IDS_TO_BE_EXCLUDED);
			}
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			parentBean.setErrorMessages( errorMessages );
		}
	}	
    
    @SuppressWarnings("unchecked")
	public void onDelete( )
	{
	    final String METHOD_NAME = "onDelete()";
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			updateValuesFromMaps();
			ProgramSponsorsView item = ( ProgramSponsorsView )dataTable.getRowData(); 
			
			if(  item.getProgramSponsorsId() != null)
			{
			item.setIsDeleted( "1" );
			}
			else
			{
			 list.remove(item);
			}
			updateValuesToMaps();
			successMessages.add( MessageFormat.format( 
					                                  ResourceUtil.getInstance().getProperty("socialProgram.messages.sponsorDeleted"),
					                                  item.getSponsorName()
					                                  )
					             );
			logger.logInfo( METHOD_NAME + " --- Finish --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		parentBean.setErrorMessages( errorMessages );
		}
		
	}
	@SuppressWarnings("unchecked")
	public void onSave()
	{
		final String METHOD_NAME = "onSave()";
		errorMessages = new ArrayList<String>();
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			
			if ( isValid() )
			{
				ProgramSponsorsView newVo = new ProgramSponsorsView();
				setDefaultValues(newVo);
				newVo.setSponsorId( programSponsorsView.getSponsorId() );
				newVo.setSponsorName( programSponsorsView.getSponsorName().trim() );
				newVo.setDescription(  programSponsorsView.getDescription() );
				newVo.setAmount( programSponsorsView.getAmount() );
				list.add(0, newVo);
				programSponsorsView = new ProgramSponsorsView();
				updateValuesToMaps();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			parentBean.setErrorMessages( errorMessages );
		}
	}

	private void setDefaultValues(ProgramSponsorsView newVo) {
		newVo.setCreatedBy(getLoggedInUserId() );
		newVo.setCreatedEn(getLoggedInUserObj().getFullName());
		newVo.setCreatedAr( getLoggedInUserObj().getSecondaryFullName() );
		newVo.setCreatedOn( new Date() );
		newVo.setIsDeleted( "0" );
	}
	
	public String onSessionDetailDelete()
	{
		updateValuesToMaps();
		return null;
	}
	
	private boolean isValid() throws Exception
	{
		boolean valid = true;
		
		// VALIDATION CHECKS STARTS HERE
		if( programSponsorsView.getAmount() == null ||  programSponsorsView.getAmount().trim().length() <=0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.sponsorAmountRequired"));
			parentBean.setErrorMessages(errorMessages);
			valid = false;
		}
		else
		{
		try
		{
			Double.valueOf(programSponsorsView.getAmount() );
		}
		catch( Exception e)
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.sponsorAmountInvalid"));
			parentBean.setErrorMessages(errorMessages);
			valid = false;
		}
		}
		if( programSponsorsView.getSponsorId() == null ||  programSponsorsView.getSponsorId().trim().length() <=0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.sponsorReq"));
			parentBean.setErrorMessages(errorMessages);
			valid = false;
		}
		// VALIDATION CHECKS ENDS HERE
		
		return valid;
	}


	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<ProgramSponsorsView> getList() {
		return list;
	}

	public void setList(List<ProgramSponsorsView> list) {
		this.list = list;
	}

	public ProgramSponsorsView getProgramSponsorsView() {
		return programSponsorsView;
	}

	public void setProgramSponsorsView(ProgramSponsorsView ProgramSponsorsView) {
		this.programSponsorsView = ProgramSponsorsView;
	}

	public HtmlInputText getTxtSponsorName() {
		return txtSponsorName;
	}

	public void setTxtSponsorName(HtmlInputText txtSponsorName) {
		this.txtSponsorName = txtSponsorName;
	}






	


}
