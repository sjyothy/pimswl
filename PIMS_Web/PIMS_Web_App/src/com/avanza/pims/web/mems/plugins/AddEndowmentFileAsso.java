package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.event.ValueChangeEvent;
import org.apache.myfaces.component.html.ext.HtmlPanelGrid;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.AssetType;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.EndFileAsso;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.EndowmentCategory;
import com.avanza.pims.entity.EndowmentPurpose;
import com.avanza.pims.entity.Person;
import com.avanza.pims.entity.Property;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.vo.PropertyView;
import com.avanza.ui.util.ResourceUtil;

public class AddEndowmentFileAsso extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	
	private String pageMode;
	
	private String endowmentTypeId;
	private String endowmentCategoryId;
	private String endowmentMainCategoryId;
	private String endowmentPurposeId;
	private String revenuePercentageString;
	private String reconstructionPercentageString;
	private HtmlSelectBooleanCheckbox chkIsManagerAmaf;
	EndFileAsso endowmentFileAsso;
	private String hdnPersonId;
	private String hdnPropertyId;
	private String hdnPersonName;
	private String endowmentTypeStatus;
	
	private HtmlPanelGrid gridTransportation = new HtmlPanelGrid();
	private HtmlPanelGrid gridVehicles = new HtmlPanelGrid();
	private HtmlPanelGrid gridAnimals = new HtmlPanelGrid();
	private HtmlPanelGrid gridLandProperties = new HtmlPanelGrid();
	private HtmlPanelGrid gridLicenses = new HtmlPanelGrid();
	private HtmlPanelGrid gridStockshares  =new HtmlPanelGrid();
	private HtmlPanelGrid gridCash = new HtmlPanelGrid();
	private HtmlPanelGrid gridCheque = new HtmlPanelGrid();
	private HtmlPanelGrid gridBankTransfer = new HtmlPanelGrid();
	private HtmlPanelGrid gridPension = new HtmlPanelGrid();
	private HtmlPanelGrid gridJewellery = new HtmlPanelGrid();
	
	public AddEndowmentFileAsso() 
	{
		endowmentFileAsso       = new EndFileAsso();
		endowmentFileAsso.setEndowment( new Endowment() );
		pageMode                = Constant.EndowmentFile.MODE_READ_ONLY;
		chkIsManagerAmaf        = new HtmlSelectBooleanCheckbox(); 
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			
			updateValuesFromMaps();
			if(!isPostBack() )
			{
				endowmentFileAsso.setUpdatedBy(getLoggedInUserId());
				endowmentFileAsso.getEndowment().setUpdatedBy(getLoggedInUserId());
				if(endowmentFileAsso.getEndFileAssoId() == null  )
				{
					endowmentFileAsso.setCreatedBy(getLoggedInUserId());
					endowmentFileAsso.getEndowment().setCreatedBy(getLoggedInUserId());
					
				}
			}
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@Override
	public void prerender() 
	{
		try
		{
			displayExtraFieldsGridsBasedOnType();
		}
		catch (Exception exception) 
		{
			logger.LogException(" prerender--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( WebConstants.PAGE_MODE) != null )
		{
			pageMode = (String) viewMap.get(WebConstants.PAGE_MODE);
		}
		else if ( sessionMap.get( WebConstants.PAGE_MODE) != null )
		{
			pageMode = (String) sessionMap.get(WebConstants.PAGE_MODE);
		}
		if( viewMap.get(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO) != null)
		{
			endowmentFileAsso     = ( EndFileAsso )viewMap.get(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO);
		}
		else if( sessionMap.get( Constant.EndowmentFile.ENDOWMENT_FILE_ASSO ) != null)
		{
			endowmentFileAsso = ( EndFileAsso )sessionMap.remove(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO);
			populatePage();
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.PAGE_MODE, pageMode);
		}
		if( endowmentFileAsso != null  )
		{
			viewMap.put(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO,endowmentFileAsso );
			
		}
	}	
	

	public void populatePage( ) throws Exception
	{
		endowmentTypeId       =  endowmentFileAsso.getEndowment().getAssetType().getAssetTypeId().toString();
		if( endowmentFileAsso.getEndowmentPurpose() != null && endowmentFileAsso.getEndowmentPurpose().getPurposeId() != null )
		{
			endowmentPurposeId    =  endowmentFileAsso.getEndowmentPurpose().getPurposeId().toString();
		}
		if( endowmentFileAsso.getEndowmentCategory() != null && endowmentFileAsso.getEndowmentCategory().getCategoryId()!= null )
		{
			endowmentCategoryId   =  endowmentFileAsso.getEndowmentCategory().getCategoryId().toString();
		}
        
		if( endowmentFileAsso.getEndowment() != null && endowmentFileAsso.getEndowment().getEndowmentMainCategory() != null && 
			endowmentFileAsso.getEndowment().getEndowmentMainCategory().getDomainDataId() != null 	
		  )
		{
			
			endowmentMainCategoryId = endowmentFileAsso.getEndowment().getEndowmentMainCategory().getDomainDataId().toString(); 
		}
        if(endowmentFileAsso.getEndowment().getRevenuePercentage() != null)
        {
        	revenuePercentageString=  endowmentFileAsso.getEndowment().getRevenuePercentage().toString();
        }
        if(endowmentFileAsso.getEndowment().getReconstructionPercentage() != null)
        {
        	reconstructionPercentageString  = endowmentFileAsso.getEndowment().getReconstructionPercentage().toString();
        }
		if( endowmentFileAsso.getEndowment().getIsAmafManager().equals("1") )
		{
			chkIsManagerAmaf.setValue(true);
			hdnPersonId           = "";
			hdnPersonName         = "";
		}
		else if( endowmentFileAsso.getEndowment().getManager()!= null && endowmentFileAsso.getEndowment().getManager().getPersonId() != null)
		{
			chkIsManagerAmaf.setValue(false);
			hdnPersonId           = endowmentFileAsso.getEndowment().getManager().getPersonId().toString();
			hdnPersonName         = endowmentFileAsso.getEndowment().getManager().getFullName() ;
		}
		if( isLandProperties() && endowmentFileAsso.getEndowment().getPropertyId()!= null )
		{
			hdnPropertyId =endowmentFileAsso.getEndowment().getPropertyId().toString();
		}
		
		if( endowmentFileAsso.getEndowment() != null && endowmentFileAsso.getEndowment().getEndowmentStatusType() != null && 
				endowmentFileAsso.getEndowment().getEndowmentStatusType().getDomainDataId() != null 	
			  )
			{
				
				endowmentTypeStatus = endowmentFileAsso.getEndowment().getEndowmentStatusType().getDomainDataId().toString(); 
			}
	}
	
	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasErrors = false;
		
		
		if( endowmentFileAsso.getEndowment().getEndowmentName() == null ||  endowmentFileAsso.getEndowment().getEndowmentName().trim().length()<=0  )
		{
	      	errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.nameRequired"));
	      	hasErrors = true;
		}
//		if( endowmentFileAsso.getEndowment().getCostCenter() == null ||  endowmentFileAsso.getEndowment().getCostCenter().trim().length()<=0  )
//		{
//			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentFile.msg.costcenterrequired"));
//			hasErrors = true;
//		}
		boolean isPercentagesEmpty=  
										( revenuePercentageString== null || revenuePercentageString.trim().length()<=0) 
										&&
										( reconstructionPercentageString== null || reconstructionPercentageString.trim().length()<=0);
		if( isPercentagesEmpty )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.RevenueReconstructionPercentageInvalid"));
			hasErrors = true;
		}
		else
		{
			boolean isPercentageInValid=Double.valueOf(  revenuePercentageString )+Double.valueOf(  reconstructionPercentageString )!=100d ;
			
			if( isPercentageInValid)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.RevenueReconstructionPercentageInvalid"));
				hasErrors = true;
			}
		}
		if( endowmentMainCategoryId == "-1" )
		{
				
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.maincategoryRequired"));
			hasErrors = true;
				 
		}
		if( endowmentCategoryId.equals("-1")  )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.categoryRequired"));
			hasErrors = true;
		}
		if( endowmentPurposeId.equals("-1")  )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.purposeRequired"));
			hasErrors = true;
		}
		
		
//		if( endowmentFileAsso.getOwnerShare() == null ||  endowmentFileAsso.getOwnerShare().compareTo( 0d ) <=0 )
//		{
//			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.ownerShareRequired"));
//			hasErrors = true;
//		}
//		if( endowmentFileAsso.getShareToEndow() == null ||  endowmentFileAsso.getShareToEndow().compareTo( 0l ) <=0 )
//		{
//			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.shareToEndowRequired"));
//			hasErrors = true;
//		}
		boolean isAmafMgr = Boolean.parseBoolean(  chkIsManagerAmaf.getValue().toString());
		if(     !isAmafMgr
				&&
			( hdnPersonId== null || hdnPersonId.trim().length() <= 0  )
		    )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.managerRequired"));
			hasErrors = true;
		}
//		7)	If sum of all the owner’s share including the provided share for this endowment exceeds 100 then following message should be displayed to user
//
//		“Sum of owner’s share for this endowment in all the files should not exceed 100�?
//

		if( endowmentTypeStatus.equals("-1")  )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.endowmentStatusRequired"));
			hasErrors = true;
		}
		
		return hasErrors;
		
	}
	public void populateValuesInObject() throws Exception
	{
		if( endowmentTypeId != null && !endowmentTypeId.trim().equals("-1") )
		{
			AssetType assetType = new AssetType();
			assetType.setAssetTypeId( new Long(endowmentTypeId.trim()) );
			endowmentFileAsso.getEndowment().setAssetType(assetType);
		}
		if( endowmentMainCategoryId != null && !endowmentMainCategoryId .trim().equals("-1") )
		{
			DomainData  dd = new DomainData();
			dd.setDomainDataId(  new Long( endowmentMainCategoryId ) );
			endowmentFileAsso.getEndowment().setEndowmentMainCategory( dd );
		}
		
		if(revenuePercentageString != null && revenuePercentageString.trim().length()> 0 )
        {
        	endowmentFileAsso.getEndowment().setRevenuePercentage( Double.valueOf(revenuePercentageString));
        }
		if(reconstructionPercentageString != null && reconstructionPercentageString.trim().length()> 0 )
        {
        	endowmentFileAsso.getEndowment().setReconstructionPercentage( Double.valueOf(reconstructionPercentageString));
        }
        if(endowmentFileAsso.getEndowment().getReconstructionPercentage() != null)
        {
        	reconstructionPercentageString  = endowmentFileAsso.getEndowment().getReconstructionPercentage().toString();
        }
		if( endowmentCategoryId != null && !endowmentCategoryId.trim().equals("-1") )
		{
			EndowmentCategory category = new EndowmentCategory ();
			category.setCategoryId(  new Long(endowmentCategoryId.trim()) );
			endowmentFileAsso.setEndowmentCategory( category );
		}
		if( endowmentPurposeId != null && !endowmentPurposeId.trim().equals("-1") )
		{
			EndowmentPurpose purpose = new EndowmentPurpose();
			purpose.setPurposeId(  new Long(endowmentPurposeId.trim()) );
			endowmentFileAsso.setEndowmentPurpose(  purpose );
		}
		boolean isAmafMgr = Boolean.parseBoolean(  chkIsManagerAmaf.getValue().toString());
		if( isAmafMgr )
		{
			endowmentFileAsso.getEndowment().setIsAmafManager("1");
			endowmentFileAsso.getEndowment().setManager( null );
		}
		else if( hdnPersonId != null && hdnPersonId.trim().length() > 0  &&
				!(
					endowmentFileAsso.getEndowment().getManager() != null && 
					endowmentFileAsso.getEndowment().getManager().getPersonId()!= null &&
					endowmentFileAsso.getEndowment().getManager().getPersonId().compareTo(new Long( hdnPersonId  )) == 0
				  )
		        )
		{
			endowmentFileAsso.getEndowment().setIsAmafManager("0");
			Person manager = new Person();
			manager.setPersonId(new Long( hdnPersonId  ));
			endowmentFileAsso.getEndowment().setManager(manager);
		}
		if( this.endowmentFileAsso.getOwnerShareString() == null || this.endowmentFileAsso.getOwnerShareString().trim().length() <=0  )
		{
			this.endowmentFileAsso.setOwnerShareString("100"); 
		}
		if( this.endowmentFileAsso.getShareToEndowString() == null || this.endowmentFileAsso.getShareToEndowString().trim().length() <=0  )
		{
			this.endowmentFileAsso.setShareToEndowString("100"); 
		}
		
		if( isLandProperties() )
		{
			//endowmentFileAsso.getEndowment().setFielDetials1( txtLandNumber.getValue().toString().trim() );
			if( hdnPropertyId != null && hdnPropertyId.trim().length() > 0  )
			{
				 Property property = new Property();
				 property.setPropertyId(new Long ( hdnPropertyId ));
				 Set<Property> set = new HashSet<Property>();
				 set.add(property);
				 endowmentFileAsso.getEndowment().setProperty(set);
			}
		}
		
		if( endowmentTypeStatus != null && !endowmentTypeStatus .trim().equals("-1") )
		{
			DomainData  dd = new DomainData();
			dd.setDomainDataId(  new Long( endowmentTypeStatus ) );
			endowmentFileAsso.getEndowment().setEndowmentStatusType( dd );
		}
		
		
		//endowmentFileAsso.getEndowment().clearAssetTypeRelatedFields();
	}
	private void clearAssetTypeRelatedFields()
	{
		endowmentFileAsso.getEndowment().clearAssetTypeRelatedFields();
		hdnPropertyId = "";
		endowmentFileAsso.getEndowment().setProperty( null );
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchEndowment()
	{
		try	
		{
			updateValuesFromMaps();
			endowmentFileAsso.setEndowment( (Endowment)sessionMap.remove(Constant.Endowments.ENDOWMENT) );
//			if(hdnPersonType.equals( "APPLICANT" ) )
//			{
//				populateApplicationDetailsTab();
//			}
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchEndowment|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchProperty()
	{
		try	
		{
			updateValuesFromMaps();
			if(hdnPropertyId!= null )
			{
				PropertyService service = new PropertyService();
				PropertyView property = service.getPropertyByID( new Long ( hdnPropertyId ) );
				String endowedName = property.getEndowedName();
				if( endowedName == null || endowedName.trim().length() <= 0 )
				{
					endowedName  = property.getCommercialName();
				}
				endowmentFileAsso.getEndowment().setEndowmentName(  endowedName );
				if( property.getCostCenter() != null && property.getCostCenter().trim().length() >0  )
				{
					endowmentFileAsso.getEndowment().setCostCenter( property.getCostCenter() );
				}
				if( property.getLandNumber()!= null && property.getLandNumber().trim().length() >0  )
				{
					endowmentFileAsso.getEndowment().setFielDetials1( property.getLandNumber() );
				}
				if( property.getLandArea()!= null )
				{
					endowmentFileAsso.getEndowment().setFielDetials4( property.getLandArea().toString() );
				}
				if( property.getAddressLineOne() !=  null && property.getAddressLineOne().trim().length() >0  )
				{
					endowmentFileAsso.getEndowment().setFielDetials3( property.getAddressLineOne().toString() );
				}
				endowmentFileAsso.getEndowment().setIsAmafManager("1");
				endowmentFileAsso.getEndowment().setManager( null );
				hdnPersonId = null;
				hdnPersonName = null;
				chkIsManagerAmaf.setValue(true);
			}
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchProperty|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMaps();
//			if(hdnPersonType.equals( "APPLICANT" ) )
//			{
//				populateApplicationDetailsTab();
//			}
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public void onManagerAmafClick()
	{
		try 
		{
			if( chkIsManagerAmaf.getValue() != null && (Boolean)chkIsManagerAmaf.getValue() )
			{
				 viewMap.put("SHOW_SEARCH_MANAGER_IMAGE",false);
                 hdnPersonId   = "";
                 hdnPersonName = "";
			}
		} 
		catch (Exception e) 
		{
			logger.logError("onManagerAmafClick|crashed | ",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
    @SuppressWarnings( "unchecked" )
	public void onSave()
	{
		
		try
		{
			updateValuesFromMaps();
			errorMessages = new ArrayList<String>();
			populateValuesInObject();
			
			if( hasSaveErrors() )
			{return;}
			
			sessionMap.put(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO, endowmentFileAsso);
			executeJavaScript("sendDataToParent();");
		}
		catch ( Exception e )
		{
			logger.LogException( "onSave--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
    @SuppressWarnings( "unchecked" )
	public void onAssetTypeChange(ValueChangeEvent evt)
	{
		try	
		{
			updateValuesFromMaps();
	
			
			endowmentTypeId =  evt.getNewValue().toString();
			if ( endowmentFileAsso.getEndowment() == null ) return;
			
			clearAssetTypeRelatedFields();
			displayExtraFieldsGridsBasedOnType();
		}
		catch(Exception ex)
		{
			logger.LogException("onAssetTypeChange|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	/**
	 * 
	 */
	private void displayExtraFieldsGridsBasedOnType()throws Exception
	{
		gridLandProperties.setRendered(false);
		gridStockshares.setRendered(false);
		gridTransportation.setRendered(false);
		gridVehicles.setRendered(false);
		gridAnimals.setRendered(false);
		gridLicenses.setRendered(false);
		gridCheque.setRendered(false);
		gridCash.setRendered(false);
		gridBankTransfer.setRendered(false);
		gridPension.setRendered(false);
		gridJewellery.setRendered(false);
		if(isLandProperties())
		{
			gridLandProperties.setRendered(true);
		}
		if(isLicenses())
		{
			gridLicenses.setRendered(true);
		}
		else if( isTransportation() )
		{
			gridTransportation.setRendered(true);
		}
		else if ( isVehicles() )
		{
			gridVehicles.setRendered(true);
		}
		else if (  isStockshares())
		{
			gridStockshares.setRendered(true);
		}
		else if( isAnimals()  )
		{
			gridAnimals.setRendered(true);
		}
		else if( isCash())
		{
			gridCash.setRendered(true);
		}
		else if( isCheque() )
		{
			gridCheque.setRendered(true);
		}
		else if( isBankTransfer() )
		{
			gridBankTransfer.setRendered(true);
		}
		else if( isPension() )
		{
			gridPension.setRendered(true);
		}
		else if( isJewellery()  )
		{
			gridJewellery.setRendered(true);
		}
	}
	
	public boolean isTransportation() 
	{
		return 	endowmentTypeId!= null &&
            	endowmentTypeId.equals(WebConstants.AssetType.TRANSPORTATIONS.toString());
	}
	public boolean isBankTransfer() 
	{
		return 	endowmentTypeId!= null &&
            	endowmentTypeId.equals(WebConstants.AssetType.BANK_TRANSFER.toString());
	}
	public boolean isJewellery() 
	{
		return 	endowmentTypeId!= null &&
            	endowmentTypeId.equals(WebConstants.AssetType.JEWELLERY.toString());
	}
	public boolean isPension() 
	{
		return 	endowmentTypeId!= null &&
            	endowmentTypeId.equals(WebConstants.AssetType.PENSION.toString());
	}
	public boolean isCheque() 
	{
		return 	endowmentTypeId!= null &&
            	endowmentTypeId.equals(WebConstants.AssetType.CHEQUE.toString());
	}
	public boolean isCash() 
	{
		return 	endowmentTypeId!= null &&
            	endowmentTypeId.equals(WebConstants.AssetType.CASH.toString());
	}
	public boolean isVehicles() 
	{
		return 	endowmentTypeId!= null &&
            	endowmentTypeId.equals(WebConstants.AssetType.VEHICLES.toString());
	}
	public boolean isAnimals() 
	{
		return 	endowmentTypeId!= null &&
            	endowmentTypeId.equals(WebConstants.AssetType.ANIMAL.toString());
	}
	public boolean isStockshares() 
	{
		return 	endowmentTypeId!= null &&
            	endowmentTypeId.equals(WebConstants.AssetType.STOCK_SHARES.toString());
	}
	public boolean isLicenses() 
	{
		return 	endowmentTypeId!= null &&
            	endowmentTypeId.equals(WebConstants.AssetType.LICENSES.toString());
	}


    public boolean isLandProperties() 
    {
		return	endowmentTypeId!= null &&
            	endowmentTypeId.equals(WebConstants.AssetType.LAND_PROPERTIES.toString());
	}

	public boolean getIsPageModeUpdatable()
	{
		boolean isPageModeUpdatable = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.PAGE_MODE_EDIT ) )
			isPageModeUpdatable = true;
		
		return isPageModeUpdatable;
	}

	
	public EndFileAsso getEndowmentFileAsso() 
	{
		return endowmentFileAsso;
	}

	public void setEndowmentFileAsso(EndFileAsso endowmentFileAsso) 
	{
		this.endowmentFileAsso = endowmentFileAsso;
	}

	public String getPageMode() 
	{
		return pageMode;
	}

	public void setPageMode(String pageMode) 
	{
		this.pageMode = pageMode;
	}

	public String getEndowmentTypeId() 
	{
		return endowmentTypeId;
	}

	public void setEndowmentTypeId(String endowmentTypeId) 
	{
		this.endowmentTypeId = endowmentTypeId;
	}

	public String getEndowmentCategoryId() 
	{
		return endowmentCategoryId;
	}

	public void setEndowmentCategoryId(String endowmentCategoryId) 
	{
		this.endowmentCategoryId = endowmentCategoryId;
	}

	public String getEndowmentPurposeId() 
	{
		return endowmentPurposeId;
	}

	public void setEndowmentPurposeId(String endowmentPurposeId) 
	{
		this.endowmentPurposeId = endowmentPurposeId;
	}

	public HtmlSelectBooleanCheckbox getChkIsManagerAmaf() {
		return chkIsManagerAmaf;
	}

	public void setChkIsManagerAmaf(HtmlSelectBooleanCheckbox chkIsManagerAmaf) {
		this.chkIsManagerAmaf = chkIsManagerAmaf;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnPropertyId() {
		return hdnPropertyId;
	}

	public void setHdnPropertyId(String hdnPropertyId) {
		this.hdnPropertyId = hdnPropertyId;
	}

	public HtmlPanelGrid getGridLandProperties() {
		return gridLandProperties;
	}

	public void setGridLandProperties(HtmlPanelGrid gridLandProperties) {
		this.gridLandProperties = gridLandProperties;
	}

	public HtmlPanelGrid getGridStockshares() {
		return gridStockshares;
	}

	public void setGridStockshares(HtmlPanelGrid gridStockshares) {
		this.gridStockshares = gridStockshares;
	}

	public HtmlPanelGrid getGridLicenses() {
		return gridLicenses;
	}

	public void setGridLicenses(HtmlPanelGrid gridLicenses) {
		this.gridLicenses = gridLicenses;
	}

	public HtmlPanelGrid getGridAnimals() {
		return gridAnimals;
	}

	public void setGridAnimals(HtmlPanelGrid gridAnimals) {
		this.gridAnimals = gridAnimals;
	}

	public HtmlPanelGrid getGridVehicles() {
		return gridVehicles;
	}

	public void setGridVehicles(HtmlPanelGrid gridVehicles) {
		this.gridVehicles = gridVehicles;
	}

	public HtmlPanelGrid getGridTransportation() {
		return gridTransportation;
	}

	public void setGridTransportation(HtmlPanelGrid gridTransportation) {
		this.gridTransportation = gridTransportation;
	}

	public HtmlPanelGrid getGridCash() {
		return gridCash;
	}

	public void setGridCash(HtmlPanelGrid gridCash) {
		this.gridCash = gridCash;
	}

	public HtmlPanelGrid getGridCheque() {
		return gridCheque;
	}

	public void setGridCheque(HtmlPanelGrid gridCheque) {
		this.gridCheque = gridCheque;
	}

	public HtmlPanelGrid getGridBankTransfer() {
		return gridBankTransfer;
	}

	public void setGridBankTransfer(HtmlPanelGrid gridBankTransfer) {
		this.gridBankTransfer = gridBankTransfer;
	}

	public HtmlPanelGrid getGridPension() {
		return gridPension;
	}

	public void setGridPension(HtmlPanelGrid gridPension) {
		this.gridPension = gridPension;
	}

	public HtmlPanelGrid getGridJewellery() {
		return gridJewellery;
	}

	public void setGridJewellery(HtmlPanelGrid gridJewellery) {
		this.gridJewellery = gridJewellery;
	}

	public String getEndowmentMainCategoryId() {
		return endowmentMainCategoryId;
	}

	public void setEndowmentMainCategoryId(String endowmentMainCategoryId) {
		this.endowmentMainCategoryId = endowmentMainCategoryId;
	}

	public String getRevenuePercentageString() {
		if(revenuePercentageString== null || revenuePercentageString.trim().length()<=0)
		{
			revenuePercentageString = "60";
		}
		return revenuePercentageString;
	}

	public void setRevenuePercentageString(String revenuePercentageString) {
		this.revenuePercentageString = revenuePercentageString;
	}

	public String getReconstructionPercentageString() {
		if(reconstructionPercentageString== null || reconstructionPercentageString.trim().length()<=0)
		{
			revenuePercentageString = "40";
		}
		return reconstructionPercentageString;
	}

	public void setReconstructionPercentageString(
			String reconstructionPercentageString) {
		this.reconstructionPercentageString = reconstructionPercentageString;
	}

	/**
	 * @return the endowmentTypeStatus
	 */
	public String getEndowmentTypeStatus() {
		return endowmentTypeStatus;
	}

	/**
	 * @param endowmentTypeStatus the endowmentTypeStatus to set
	 */
	public void setEndowmentTypeStatus(String endowmentTypeStatus) {
		this.endowmentTypeStatus = endowmentTypeStatus;
	}

	}
