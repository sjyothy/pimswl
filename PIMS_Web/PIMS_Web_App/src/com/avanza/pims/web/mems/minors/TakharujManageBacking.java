package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;


import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.bpel.memstakharoujbpel.proxy.MEMSTakharoujBPELPortClient;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationBean;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.InheritedAssetService;
import com.avanza.pims.ws.mems.TakharujService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InheritanceBeneficiaryView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.InheritedAssetSharesView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RelationshipView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.SessionDetailView;
import com.avanza.pims.ws.vo.mems.InheritedAssetView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.pims.ws.vo.mems.TakharujDetailView;
import com.avanza.pims.ws.vo.mems.TakharujInheritedAssetView;
import com.avanza.ui.util.ResourceUtil;

public class TakharujManageBacking extends AbstractMemsBean 
{	
	private static final long serialVersionUID = -6715134667764847993L;
	
	private InheritanceFileView inheritanceFileView;
	private List<SelectItem> inheritedAssetSelectItems;
	private Map<String, InheritedAssetView> inheritedAssetMap;
	private String selectedInheritedAssetId;
	private List<TakharujInheritedAssetView> takharujInheritedAssetViewList;
	private RequestView requestView;
	private ReviewRequestView reviewRequestView;
		
	private InheritedAssetService inheritedAssetService;
	private TakharujService takharujService;
	private UtilityService utilityService;
	
	private HtmlDataTable takharujInheritedAssetViewDataTable;
	private final String noteOwner;
	private final String externalId;
	private final String procedureTypeKey ;
	
	private static final String TAB_APPLICATION="applicationDetailsTab";
	private static final String TAB_DETAILS="takharujDetailsTab";
	private static final String TAB_SESSION="sessionTab";
	private static final String TAB_REVIEW="reviewTab";
	private static final String TAB_ATTACHMENT="attachmentTab";
	
	private SystemParameters systemParameters;
	private UserTask userTask;	
	private String soaConfigPath;
	private String pageMode;
	private HtmlTabPanel takharujTabPanel;
	public TakharujManageBacking() 
	{
		takharujTabPanel = new  HtmlTabPanel();
		inheritanceFileView = new InheritanceFileView();
		inheritedAssetSelectItems = new ArrayList<SelectItem>(0);
		inheritedAssetMap = new HashMap<String, InheritedAssetView>(0);
		selectedInheritedAssetId = "";
		takharujInheritedAssetViewList = new ArrayList<TakharujInheritedAssetView>(0);
		requestView = new RequestView();
		reviewRequestView = new ReviewRequestView();
		noteOwner = WebConstants.Takharuj.NOTES_OWNER;
		procedureTypeKey = WebConstants.Takharuj.PROCEDURE_KEY_TAKHARUJ;
		externalId = WebConstants.Takharuj.EXTERNAL_ID_TAKHARUJ;
		inheritedAssetService = new InheritedAssetService();
		takharujService = new TakharujService();
		utilityService = new UtilityService();
		
		takharujInheritedAssetViewDataTable = new HtmlDataTable();
		
		systemParameters = SystemParameters.getInstance();
		userTask = new UserTask();
		soaConfigPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties");
		pageMode = WebConstants.Takharuj.PAGE_MODE_REQUEST_NEW;
	}
	
	@Override
	public void init() 
	{
		String METHOD_NAME = "init()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			if ( ! isPostBack() )
			{
				loadAttachmentsAndComments(null);
				requestView.setRequestDate( new Date() );
				
				DomainDataView ddvRequestStatusNew = CommonUtil.getIdFromType( 
														CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
														WebConstants.REQUEST_STATUS_NEW );
				requestView.setStatusId( ddvRequestStatusNew.getDomainDataId() );
				requestView.setStatusEn( ddvRequestStatusNew.getDataDescEn() );
				requestView.setStatusAr( ddvRequestStatusNew.getDataDescAr() );
			}
			updateValuesFromMaps();

			if( getIsPageModeRequestRejectedResubmit() &&  requestView.getMemsNolReason() != null)
			{
				errorMessages.add(requestView.getMemsNolReason() );
				
			}
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// PAGE MODE
		if ( viewMap.get( WebConstants.Takharuj.TAKHARUJ_MANAGE_PAGE_MODE_KEY ) != null )
		{
			pageMode = (String) viewMap.get( WebConstants.Takharuj.TAKHARUJ_MANAGE_PAGE_MODE_KEY );
		}
		
		
		// USER TASK
		if ( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
		{
			userTask = (UserTask) viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK );
		}
		else if ( sessionMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
		{
			userTask = (UserTask) sessionMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK );
			sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
			
			if ( userTask.getTaskAttributes().get( WebConstants.REQUEST_ID ) != null )
			{				
				requestView = takharujService.getTakharujRequestByRequestId( Long.parseLong( userTask.getTaskAttributes().get( WebConstants.REQUEST_ID ) ) );
				
				if ( requestView != null && requestView.getRequestId() != null && requestView.getStatusId() != null )
				{
					setPageModeForTask( requestView );
				}
			}
		}
		
		
		// REQUEST VIEW
		if ( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = (RequestView) viewMap.get( WebConstants.REQUEST_VIEW );
			loadAttachmentsAndComments(requestView.getRequestId() );
		}
		else if ( sessionMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = (RequestView) sessionMap.get( WebConstants.REQUEST_VIEW );
			sessionMap.remove( WebConstants.REQUEST_VIEW );
			loadAttachmentsAndComments(requestView.getRequestId());
			if ( requestView.getRequestId() != null )
			{
				requestView = takharujService.getTakharujRequestByRequestId( requestView.getRequestId() );
				
				if ( requestView != null && requestView.getRequestId() != null && requestView.getStatusId() != null )
				{
					setPageModeForRequest( requestView );
				}
			}
		}			
		
		
		// INHERITANCE FILE VIEW
		if ( sessionMap.get( WebConstants.Takharuj.INHERITANCE_FILE_VIEW ) != null )
		{
			inheritanceFileView = (InheritanceFileView) sessionMap.get( WebConstants.Takharuj.INHERITANCE_FILE_VIEW );
//			loadAttachmentsAndComments(inheritanceFileView.getInheritanceFileId());
			sessionMap.remove( WebConstants.Takharuj.INHERITANCE_FILE_VIEW );
		}
		else if ( viewMap.get( WebConstants.Takharuj.INHERITANCE_FILE_VIEW ) != null )
		{
			inheritanceFileView = (InheritanceFileView) viewMap.get( WebConstants.Takharuj.INHERITANCE_FILE_VIEW );
//			loadAttachmentsAndComments(inheritanceFileView.getInheritanceFileId());
		}
		else if ( requestView != null && requestView.getRequestId() != null && 
				requestView.getInheritanceFileView() != null && requestView.getInheritanceFileView().getInheritanceFileId() != null )
		{
			inheritanceFileView = requestView.getInheritanceFileView();
		}
		
		
		// INHERITED ASSET MAP
		if ( viewMap.get( WebConstants.Takharuj.INHERITED_ASSET_MAP ) != null )
		{
			inheritedAssetMap = (Map<String, InheritedAssetView>) viewMap.get( WebConstants.Takharuj.INHERITED_ASSET_MAP );
		}
		
		
		// INHERITED ASSET SELECT ITEMS
		if ( viewMap.get( WebConstants.Takharuj.INHERITED_ASSET_SELECT_ITEMS ) != null )
		{
			inheritedAssetSelectItems = (List<SelectItem>) viewMap.get( WebConstants.Takharuj.INHERITED_ASSET_SELECT_ITEMS );
		}
		else if ( inheritanceFileView != null && inheritanceFileView.getInheritanceFileId() != null )
		{
			InheritedAssetView inheritedAssetView = new InheritedAssetView();			
			inheritedAssetView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			inheritedAssetView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			inheritedAssetView.setIsFinal( 1L );
			inheritedAssetView.setIsConfirm( 1L );
			inheritedAssetView.setIsConfirmBool( true );
			inheritedAssetView.setInheritanceFileView( inheritanceFileView );
			
			inheritedAssetSelectItems = ApplicationBean.getInheritedAssetList( inheritedAssetView, inheritedAssetMap );
		}
		
		
		// TAKHARUJ INHERITED ASSET VIEW LIST
		if ( viewMap.get( WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW_LIST ) != null )
		{
			takharujInheritedAssetViewList = (List<TakharujInheritedAssetView>) viewMap.get( WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW_LIST );
		}
		else if ( requestView != null && requestView.getRequestId() != null )
		{
			TakharujInheritedAssetView takharujInheritedAssetView = new TakharujInheritedAssetView();
			takharujInheritedAssetView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			takharujInheritedAssetView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			takharujInheritedAssetView.setRequestView( requestView );
			loadAttachmentsAndComments(requestView.getRequestId());
			takharujInheritedAssetViewList = takharujService.getTakharujInheritedAssetViewList( takharujInheritedAssetView );
		}
		
		updateValuesToMaps();
	}

	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// PAGE MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.Takharuj.TAKHARUJ_MANAGE_PAGE_MODE_KEY, pageMode );
			if( pageMode.equals(WebConstants.Takharuj.PAGE_MODE_REQUEST_NEW))
					canAddAttachmentsAndComments(true);
		}
		
		
		// USER TASK
		if ( userTask != null )
		{
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
		}
		
		
		// REQUEST VIEW
		if ( requestView != null )
		{
			viewMap.put( WebConstants.REQUEST_VIEW , requestView );
		}
		
		
		// INHERITANCE FILE VIEW
		if ( inheritanceFileView != null )
		{
			viewMap.put( WebConstants.Takharuj.INHERITANCE_FILE_VIEW, inheritanceFileView );			
		}
		
		
		// INHERITED ASSET MAP
		if ( inheritedAssetMap != null )
		{
			viewMap.put( WebConstants.Takharuj.INHERITED_ASSET_MAP, inheritedAssetMap );
		}
		
		
		// INHERITED ASSET SELECT ITEMS
		if ( inheritedAssetSelectItems != null )
		{
			viewMap.put( WebConstants.Takharuj.INHERITED_ASSET_SELECT_ITEMS, inheritedAssetSelectItems );
		}
		
		
		// TAKHARUJ INHERITED ASSET VIEW LIST
		if ( takharujInheritedAssetViewList != null )
		{
			viewMap.put( WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW_LIST, takharujInheritedAssetViewList );
		}
	}
	
	public String onAddTakharujInheritedAsset()
	{
		if ( selectedInheritedAssetId != null && ! selectedInheritedAssetId.equalsIgnoreCase("") )
		{
			// Check if the selected inherited asset doesn't already exits in the list
			for ( TakharujInheritedAssetView takharujInheritedAssetView :  takharujInheritedAssetViewList )
			{
				// Check if not deleted
				if ( takharujInheritedAssetView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
				{
					if ( takharujInheritedAssetView.getInheritedAssetView() != null 
							&& takharujInheritedAssetView.getInheritedAssetView().getInheritedAssetId() != null
							&& takharujInheritedAssetView.getInheritedAssetView().getInheritedAssetId().equals( Long.valueOf(selectedInheritedAssetId) ) 
						)
					{
							errorMessages.add( CommonUtil.getBundleMessage("takharujDetailsTab.errMsg.alreadyAsset") );
							return null;
					}
				}
			}
			
			InheritedAssetView selectedInheritedAssetView = inheritedAssetMap.get( selectedInheritedAssetId );
			
			TakharujInheritedAssetView takharujInheritedAssetView = new TakharujInheritedAssetView();
			takharujInheritedAssetView.setRequestView( requestView );
			takharujInheritedAssetView.setInheritedAssetView( selectedInheritedAssetView );
			takharujInheritedAssetView.setIsCompleteTakharuj( false );
			takharujInheritedAssetView.setCreatedBy( CommonUtil.getLoggedInUser() );
			takharujInheritedAssetView.setCreatedOn( new Date() );
			takharujInheritedAssetView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			takharujInheritedAssetView.setUpdatedOn( new Date() );
			takharujInheritedAssetView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			takharujInheritedAssetView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			
			takharujInheritedAssetViewList.add( takharujInheritedAssetView );
			
			updateValuesToMaps();
		}
		else
		{
			errorMessages.add( CommonUtil.getBundleMessage("takharujDetailsTab.errMsg.selectAsset") );
		}
		
		return null;
	}
	
	public String onDeleteTakharujInheritedAsset()
	{
		TakharujInheritedAssetView takharujInheritedAssetView = (TakharujInheritedAssetView) takharujInheritedAssetViewDataTable.getRowData();
		
		if ( takharujInheritedAssetView.getTakharujInheritedAssetId() == null )
			takharujInheritedAssetViewList.remove( takharujInheritedAssetView );
		else
			takharujInheritedAssetView.setIsDeleted( 1L );
		
		updateValuesToMaps();
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public String onDetailTakharujInheritedAsset()
	{
		TakharujInheritedAssetView takharujInheritedAssetView = (TakharujInheritedAssetView) takharujInheritedAssetViewDataTable.getRowData();
		
		sessionMap.put( WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW, takharujInheritedAssetView );
		if (  getIsPageModeViewOnly() || getIsPageModeReviewRequired() || getIsPageModeJudgeSessionReq()   )
		{
		  sessionMap.put( WebConstants.Takharuj.TAKHARUJ_DETAIL_POPUP_VIEW, "true" );
		}
		executeJavascript("openTakharujDetailManagePopup();");
		
		return null;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onReviewTabClick()
	{
		viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_PARENT, WebConstants.ReviewRequest.PARENT_TAKHRUJ );
	}
	
	@SuppressWarnings("unchecked")
	public void onSessionTabClick()
	{
		
		viewMap.put( WebConstants.Session.SESSION_DETAIL_TAB_PARENT, WebConstants.Session.PARENT_TAKHARUJ );
	}
	public int getTakharujInheritedAssetViewListRecordSize()
	{
		int size = 0;
		
		for ( TakharujInheritedAssetView takharujInheritedAssetView : takharujInheritedAssetViewList )
			if ( takharujInheritedAssetView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
				return 1;
		
		return size;
	}
	
	public String onReceiveTakharujInheritedAssetView()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{
			TakharujInheritedAssetView receivedTakharujInheritedAssetView = (TakharujInheritedAssetView) sessionMap.remove( WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW );;
			int index = 0;
			for ( TakharujInheritedAssetView takharujInheritedAssetView : takharujInheritedAssetViewList )
			{
				if ( takharujInheritedAssetView.getInheritedAssetView().getInheritedAssetId().equals( receivedTakharujInheritedAssetView.getInheritedAssetView().getInheritedAssetId() ) )
				{
					takharujInheritedAssetViewList.remove( index );
					takharujInheritedAssetViewList.add(index, receivedTakharujInheritedAssetView);
					break;
				}
				
				index++;
			}
			
			updateValuesToMaps();
			
			successMessages.add( CommonUtil.getBundleMessage( "msg.Takharuj.takharujDetailsReceived" ) );
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			logger.LogException( "onReceiveTakharujInheritedAssetView---CRASHED --- ", exception);			
		}
		
		return null;
	}
	
	public String onSave()
	{
		String METHOD_NAME = "onSave()";
		String path = null;
		boolean isSuccess = false;
				
		try	
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			if ( isValid() )
			{
				isSuccess = saveTakharujRequest( requestView );
				isSuccess = saveTakharujInheritedAsset( takharujInheritedAssetViewList );
				saveAttachmentComment();
				saveSystemComments(MessageConstants.RequestEvents.REQUEST_SAVED);
				if ( isSuccess )
				{
					updateValuesToMaps();
					successMessages.add( CommonUtil.getBundleMessage( "msg.Takharuj.takharujRequestSaveSuccessfully" ) );
				}
			}
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{			
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);			
		}
		
		return path;
	}
	private void saveSystemComments(String sysNote) throws Exception 
	{
		
		if(requestView.getRequestId() != null)
		{
			Long requestId = new Long(requestView.getRequestId());
			CommonUtil.saveSystemComments(noteOwner, sysNote, requestId);
			
		}
	}


	private boolean saveTakharujRequest(RequestView requestView) throws PimsBusinessException 
	{
		boolean isSuccess = false;
		
		if ( requestView.getRequestId() == null )
		{
			// Add Takharuj Request
			requestView.setCreatedBy( CommonUtil.getLoggedInUser() );
			requestView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			requestView.setInheritanceFileView( inheritanceFileView );
			isSuccess = takharujService.addTakharujRequestView( requestView );
		}
		else
		{
			// Update Takharuj Request
			requestView.setUpdatedBy( CommonUtil.getLoggedInUser() );
			isSuccess = takharujService.updateTakharujRequestView( requestView );
		}
		
		return isSuccess;
	}
	
	private boolean saveTakharujInheritedAsset(List<TakharujInheritedAssetView> takharujInheritedAssetViewList) throws PimsBusinessException
	{
		boolean isSuccess = false;
		
		for ( TakharujInheritedAssetView takharujInheritedAssetView : takharujInheritedAssetViewList )
		{
			if ( takharujInheritedAssetView.getTakharujInheritedAssetId() == null )
			{
				// Add Takharuj Inherited Asset View
				takharujInheritedAssetView.setCreatedBy( CommonUtil.getLoggedInUser() );
				takharujInheritedAssetView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				takharujInheritedAssetView.setRequestView( requestView );
				isSuccess = takharujService.addTakharujInheritedAssetView( takharujInheritedAssetView );
			}
			else
			{
				// Update Takharuj Inherited Asset View
				takharujInheritedAssetView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				isSuccess = takharujService.updateTakharujInheritedAssetView( takharujInheritedAssetView );
			}
		}
		
		return isSuccess;
	}

	private boolean isValid()  throws Exception
	{
		boolean isValid = true;
		
		if ( requestView.getApplicantView() == null || requestView.getApplicantView().getPersonId() == null )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.takharuj.manage.applicant.empty" ) );
			takharujTabPanel.setSelectedTab(TAB_APPLICATION);
			return false;
		}
		
		if ( getTakharujInheritedAssetViewListRecordSize() == 0 ) //|| ((TakharujDetailManageBacking) getBean("pages$takharujDetailManage")).getTakharujDetailViewListRecordSize() == 0 )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.takharuj.manage.takharujDetail.empty" ) );
			takharujTabPanel.setSelectedTab(TAB_DETAILS);
			return false;
		}
		// Checking if each asset has takharuj details present
//		else
//		{
//			for ( TakharujInheritedAssetView takharujInheritedAssetView : takharujInheritedAssetViewList )
//			{
//				if( takharujInheritedAssetView.getTakharujDetailViewList() == null || takharujInheritedAssetView.getTakharujDetailViewList().size() <= 0 )
//				{
//					errorMessages.add( CommonUtil.getBundleMessage( "err.takharuj.manage.takharujDetail.empty" ) );
//					isValid = false;
//					break;
//				}
//			}
//			
//		}
		 if(!AttachmentBean.mandatoryDocsValidated())
	    	{
	    		errorMessages.add(ResourceUtil.getInstance().getProperty((MessageConstants.Attachment.MSG_MANDATORY_DOCS)));
	    		takharujTabPanel.setSelectedTab(TAB_ATTACHMENT);
	    		return false;
	    	}
		
		return isValid;
	}
	public void onPrint()
    {
    	try
    	{
    		updateValuesFromMaps();
    		CommonUtil.printMemsReport( this.getRequestView().getRequestId() );
    	}
		catch (Exception ex) 
		{
			logger.LogException( "onPrint|Error Occured:",ex);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
    	
    }
	public String onResubmit()
	{
		String path = null;
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			
				DomainDataView ddvRequestStatus = CommonUtil.getIdFromType( 
						CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
						WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
				requestView.setStatusId( ddvRequestStatus.getDomainDataId() );
				requestView.setStatusEn( ddvRequestStatus.getDataDescEn() );
				requestView.setStatusAr( ddvRequestStatus.getDataDescAr() );
				if(saveTakharujRequest( requestView ))
				{
					updateTaskOutcome(userTask, TaskOutcome.OK, CommonUtil.getLoggedInUser(), soaConfigPath);
					pageMode = WebConstants.Takharuj.PAGE_MODE_VIEW_ONLY;
					saveAttachmentComment();
					saveSystemComments(MessageConstants.RequestEvents.REQUEST_RESUBMITTED);
				}
		}
		catch (Exception exception) 
		{				
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(  "onResubmit() --- CRASHED --- ", exception);
		}
		
		return path;
	}
	public String onSend()
	{
		final String METHOD_NAME = "onSend()";
		String path = null;
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			DomainDataView ddvRequestStatusApprovalRequired = CommonUtil.getIdFromType( 
					CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
					WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED );
			requestView.setStatusId( ddvRequestStatusApprovalRequired.getDomainDataId() );
			requestView.setStatusEn( ddvRequestStatusApprovalRequired.getDataDescEn() );
			requestView.setStatusAr( ddvRequestStatusApprovalRequired.getDataDescAr() );
			
			if ( saveTakharujRequest( requestView ) )
			{	
				MEMSTakharoujBPELPortClient portClient = new MEMSTakharoujBPELPortClient();
				portClient.setEndpoint( systemParameters.getParameter( WebConstants.Takharuj.MEMS_TAKHARUJ_BPEL_END_POINT ) );
				portClient.initiate( CommonUtil.getLoggedInUser(), String.valueOf(requestView.getRequestId()), null, null );
				
				pageMode = WebConstants.Takharuj.PAGE_MODE_VIEW_ONLY;
				
				updateValuesToMaps();	
				saveAttachmentComment();
				saveSystemComments(MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL);
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.RequestEvents.AUCTION_SENT_FOR_APPROVAL ) );
			}
			CommonUtil.printMemsReport( this.getRequestView().getRequestId() );
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{				
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	@SuppressWarnings("unchecked")
	private boolean hasApprovalError()
	{
		
		for ( TakharujInheritedAssetView takharujInheritedAssetView : takharujInheritedAssetViewList )
		{
			if( takharujInheritedAssetView.getTakharujDetailViewList() == null || takharujInheritedAssetView.getTakharujDetailViewList().size() <= 0 )
			{
				errorMessages.add( CommonUtil.getBundleMessage( "err.takharuj.manage.takharujDetail.empty" ) );
				takharujTabPanel.setSelectedTab(TAB_DETAILS);
                return true;
			}
		}
        return false;
	}
	
	public String onApprove()
	{
		final String METHOD_NAME = "onApprove()";
		String path = null;
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			if( !hasApprovalError() )
			{
				DomainDataView ddvRequestStatusApproved = CommonUtil.getIdFromType( 
						CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
						WebConstants.REQUEST_STATUS_APPROVED );
				requestView.setStatusId( ddvRequestStatusApproved.getDomainDataId() );
				requestView.setStatusEn( ddvRequestStatusApproved.getDataDescEn() );
				requestView.setStatusAr( ddvRequestStatusApproved.getDataDescAr() );
				saveTakharujRequest( requestView ); 
				saveAttachmentComment();
				saveSystemComments(MessageConstants.RequestEvents.REQUEST_APPROVED);
				
			
			}
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{				
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	@SuppressWarnings("unchecked")
	private boolean hasCompletionError()
	{
		
		for ( TakharujInheritedAssetView takharujInheritedAssetView : takharujInheritedAssetViewList )
		{
			if( takharujInheritedAssetView.getTakharujDetailViewList() == null || takharujInheritedAssetView.getTakharujDetailViewList().size() <= 0 )
			{
				errorMessages.add( CommonUtil.getBundleMessage( "err.takharuj.manage.takharujDetail.empty" ) );
				takharujTabPanel.setSelectedTab(TAB_DETAILS);
                return true;
			}
		}
        return false;
	}
	public String onComplete()
	{
		final String METHOD_NAME = "onComplete";
		String path = null;
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			if( !hasCompletionError() )
			{
				DomainDataView ddvRequestStatusCompleted = CommonUtil.getIdFromType( 
						CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
						WebConstants.REQUEST_STATUS_COMPLETE);
				requestView.setStatusId( ddvRequestStatusCompleted.getDomainDataId() );
				requestView.setStatusEn( ddvRequestStatusCompleted.getDataDescEn() );
				requestView.setStatusAr( ddvRequestStatusCompleted.getDataDescAr() );
				
				if ( saveTakharujRequest( requestView ) )
				{	
					// Author: Anil Verani 
					// My last function in Avanza Solutions :)
					// The function means Do Exit :)
					performTakharuj();
					
					updateTaskOutcome(userTask, TaskOutcome.APPROVE, CommonUtil.getLoggedInUser(), soaConfigPath);
					
					pageMode = WebConstants.Takharuj.PAGE_MODE_VIEW_ONLY;
					
					updateValuesToMaps();
					saveAttachmentComment();
					saveSystemComments(MessageConstants.RequestEvents.REQUEST_COMPLETED);
					successMessages.add( CommonUtil.getBundleMessage( MessageConstants.RequestEvents.REQUEST_COMPLETED) );
				}
			}
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{				
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	private void performTakharuj() throws Exception 
	{
		String METHOD_NAME = "performTakharuj()";
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			if ( takharujInheritedAssetViewList != null )
			{	
				for ( TakharujInheritedAssetView takharujInheritedAssetView : takharujInheritedAssetViewList )
				{
					if ( takharujInheritedAssetView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
					{
						List<TakharujDetailView> takharujDetailViewList = takharujInheritedAssetView.getTakharujDetailViewList();
						
						if ( takharujDetailViewList != null )
						{
							HashMap<Long, Double> fromCurrentShareMap = new HashMap<Long, Double>();//to keep the lastShareValue of from person
							HashMap<Long, Double> toCurrentShareMap = new HashMap<Long, Double>();//to keep the lastShareValue of to person
							for ( TakharujDetailView takharujDetailView : takharujDetailViewList )
							{
								if ( takharujDetailView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
								{
									InheritedAssetSharesView fromInheritedAssetSharesView = takharujDetailView.getFromInheritedAssetSharesView();
									InheritedAssetSharesView toInheritedAssetSharesView = takharujDetailView.getToInheritedAssetSharesView();
									PersonView toPersonView = takharujDetailView.getToPersonView();
									Long fromPersonId = takharujDetailView.getFromInheritedAssetSharesView().getInheritanceBeneficiaryView().getBeneficiary().getPersonId();
									Long toPersonId = toPersonView.getPersonId();
									
									Double fromShare = takharujDetailView.getFromSharePercentage();//total Share of from
									Double takharujShare = takharujDetailView.getTakharujShare(); //share to be takharujed
									Double toShare = takharujDetailView.getToSharePercentage();//total Share of to
									
									if (fromCurrentShareMap
											.containsKey(fromPersonId)) {
										fromShare = fromCurrentShareMap.get(fromPersonId);
									}
									if (toCurrentShareMap
											.containsKey(toPersonId)) {
										toShare = toCurrentShareMap.get(toPersonId);
									}
									
									/** Below logic has been commented bcoz percentage functionality has replaced by shareValue/Ratio
									// calculatedShare = takharujShare of fromShare
									// this calculatedShare will be deducted from fromShare and added to toShare 
//									Double calculatedShare = ( (takharujShare / 100.00D) * (fromShare / 100.00D) ) * 100.00D;
									*/
									// For creating inheritanceBeneficiaryView if not already included in the file
									if ( toInheritedAssetSharesView == null )
									{
										// need to create this toInheritedAssetSharesView object here
										// to create it first toPersonView should be added as inheritanceBeneficiaryView
										InheritanceBeneficiaryView inheritanceBeneficiaryView = new InheritanceBeneficiaryView();
										inheritanceBeneficiaryView.setBeneficiary( toPersonView );
										inheritanceBeneficiaryView.setInheritanceFileView( inheritanceFileView );
										inheritanceBeneficiaryView.setSharePercentage( 0.00D );
										inheritanceBeneficiaryView.setCreatedBy( CommonUtil.getLoggedInUser() );
										inheritanceBeneficiaryView.setUpdatedBy( CommonUtil.getLoggedInUser() );
										inheritanceBeneficiaryView.setFromTakharuj( 1L );										
										// JUGAR START - approved by BABA - E - PIMS / MEMS
										RelationshipView relationshipView = new RelationshipView();
										relationshipView.setRelationshipId( 112L );
										inheritanceBeneficiaryView.setRelationshipView( relationshipView );
										inheritanceBeneficiaryView.setRelationshipDesc( "From takharuj request#"+requestView.getRequestNumber() );
										// JUGAR END - approved by BABA - E - PIMS / MEMS
										
										takharujService.addInheritanceBeneficiaryView( inheritanceBeneficiaryView );
										
										toInheritedAssetSharesView = new InheritedAssetSharesView();
										toInheritedAssetSharesView.setInheritanceBeneficiaryView( inheritanceBeneficiaryView );
										toInheritedAssetSharesView.setInheritedAssetView( takharujInheritedAssetView.getInheritedAssetView() );
										toInheritedAssetSharesView.setIsWill( 0L );
										toInheritedAssetSharesView.setCreatedBy( CommonUtil.getLoggedInUser() );
										toInheritedAssetSharesView.setUpdatedBy( CommonUtil.getLoggedInUser() );
										
									}
									
									fromCurrentShareMap.put(fromPersonId, fromShare - takharujShare);//update from current share
									toCurrentShareMap.put(toPersonId,toShare + takharujShare);//update to current share
									
									fromInheritedAssetSharesView.setSharePercentage( fromShare - takharujShare );									
									toInheritedAssetSharesView.setSharePercentage( toShare + takharujShare );
									
									takharujService.updateInheritedAssetSharesView( fromInheritedAssetSharesView );
									
									if ( toInheritedAssetSharesView.getInheritedAssetSharesId() != null )
										takharujService.updateInheritedAssetSharesView( toInheritedAssetSharesView );
									else
										takharujService.addInheritedAssetSharesView( toInheritedAssetSharesView );
								}
							}
						}
					}
				}
			}
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}	
		catch (Exception exception) 
		{	
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
			throw exception;
		}			
	}
	private boolean hasRejectionError()
	{
		if( getReviewRequestView().getRfc() == null || getReviewRequestView().getRfc().trim().length()  <= 0 )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "socialResearchRecommendation.msg.rejectionReasonRequired" ));
			return true;
		}
		return false;
	}
	
	public String onReject()
	{
		String path = null;
		
		try
		{
			if( hasRejectionError() )
				return "";
			
			
			DomainDataView ddvRequestStatusRejected = CommonUtil.getIdFromType( CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
																				WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT 
																			   
			);
			requestView.setStatusId( ddvRequestStatusRejected.getDomainDataId() );
			requestView.setStatusEn( ddvRequestStatusRejected.getDataDescEn() );
			requestView.setStatusAr( ddvRequestStatusRejected.getDataDescAr() );
			requestView.setMemsNolReason(getReviewRequestView().getRfc().trim() );
			if ( saveTakharujRequest( requestView ) )
			{	
				updateTaskOutcome(userTask, TaskOutcome.REJECT, CommonUtil.getLoggedInUser(), soaConfigPath);
				
				pageMode = WebConstants.Takharuj.PAGE_MODE_VIEW_ONLY;
				
				updateValuesToMaps();
				saveAttachmentComment();
				saveSystemComments(MessageConstants.RequestEvents.REQUEST_REJECTED);
				CommonUtil.saveRemarksAsComments(requestView.getRequestId(), getReviewRequestView().getRfc(), WebConstants.Takharuj.NOTES_OWNER);
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.RequestEvents.REQUEST_REJECTED ) );
			}
			
		}
		catch (Exception exception) 
		{				
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException( "onReject() --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onCancelled()
	{
		String path = null;
		
		try
		{
			
			DomainDataView ddvRequestStatusCancelled = CommonUtil.getIdFromType( 
					CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
					WebConstants.REQUEST_STATUS_CANCELED );
			requestView.setStatusId( ddvRequestStatusCancelled.getDomainDataId() );
			requestView.setStatusEn( ddvRequestStatusCancelled.getDataDescEn() );
			requestView.setStatusAr( ddvRequestStatusCancelled.getDataDescAr() );
			
			if ( saveTakharujRequest( requestView ) )
			{	
				updateTaskOutcome(userTask, TaskOutcome.REJECT, CommonUtil.getLoggedInUser(), soaConfigPath);
				
				pageMode = WebConstants.Takharuj.PAGE_MODE_VIEW_ONLY;
				
				updateValuesToMaps();
				saveAttachmentComment();
				saveSystemComments(MessageConstants.RequestEvents.REQUEST_CANCELED);
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.RequestEvents.REQUEST_CANCELED ) );
			}
			
		}
		catch (Exception exception) 
		{				
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException( "onCancelled() --- CRASHED --- ", exception);
		}
		
		return path;
	}

	public String onJudge()
	{
		String METHOD_NAME = "onJudge()";
		String path = null;
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			DomainDataView ddvRequestStatusJudgeSessionReq = CommonUtil.getIdFromType( 
					CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
					WebConstants.REQUEST_STATUS_JUDGE_SESSION_REQ );
			requestView.setStatusId( ddvRequestStatusJudgeSessionReq.getDomainDataId() );
			requestView.setStatusEn( ddvRequestStatusJudgeSessionReq.getDataDescEn() );
			requestView.setStatusAr( ddvRequestStatusJudgeSessionReq.getDataDescAr() );
			
			if ( saveTakharujRequest( requestView ) )
			{	
				updateTaskOutcome(userTask, TaskOutcome.FORWARD, CommonUtil.getLoggedInUser(), soaConfigPath);
				
				pageMode = WebConstants.Takharuj.PAGE_MODE_VIEW_ONLY;
				
				updateValuesToMaps();
				saveAttachmentComment();
				saveSystemComments(MessageConstants.RequestEvents.REQUEST_SENT_TO_JUDGE);
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.RequestEvents.REQUEST_PAYMENT_SENT_FOR_JUDGE ) );
			}
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{				
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	@SuppressWarnings("unchecked")
	public String onJudgeDone()
	{
		String METHOD_NAME = "onJudgeDone()";
		String path = null;
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			DomainDataView ddvRequestStatusApprovalRequired = CommonUtil.getIdFromType( 
					CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
					WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED );
			requestView.setStatusId( ddvRequestStatusApprovalRequired.getDomainDataId() );
			requestView.setStatusEn( ddvRequestStatusApprovalRequired.getDataDescEn() );
			requestView.setStatusAr( ddvRequestStatusApprovalRequired.getDataDescAr() );
			
			if ( saveTakharujRequest( requestView ) )
			{	
				List<SessionDetailView> sessionDetailViewList = (List<SessionDetailView>) viewMap.get( WebConstants.Session.SESSION_DETAIL_VIEW_LIST );
				utilityService.persistJudgeSession(sessionDetailViewList.toArray(new SessionDetailView[sessionDetailViewList.size()]));
				
				updateTaskOutcome(userTask, TaskOutcome.OK, CommonUtil.getLoggedInUser(), soaConfigPath);
				
				pageMode = WebConstants.Takharuj.PAGE_MODE_VIEW_ONLY;
				
				updateValuesToMaps();
				saveAttachmentComment();
				saveSystemComments(MessageConstants.RequestEvents.REQUEST_JUDGE_DONE);
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.RequestEvents.REQUEST_PAYMENT_JUDGE_DONE ) );
			}
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{				
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onReview()
	{
		String METHOD_NAME = "onReview()";
		String path = null;
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			DomainDataView ddvRequestStatusReviewReq = CommonUtil.getIdFromType( 
					CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
					WebConstants.REQUEST_STATUS_REVIEW_REQUIRED );
			requestView.setStatusId( ddvRequestStatusReviewReq.getDomainDataId() );
			requestView.setStatusEn( ddvRequestStatusReviewReq.getDataDescEn() );
			requestView.setStatusAr( ddvRequestStatusReviewReq.getDataDescAr() );
			
			if ( isReviewValidated() && saveTakharujRequest( requestView ) )
			{
				
				ReviewRequestView reviewRequestView = new ReviewRequestView();
				reviewRequestView.setCreatedBy( CommonUtil.getLoggedInUser() );
				reviewRequestView.setCreatedOn( new Date() );
				reviewRequestView.setRfc( this.reviewRequestView.getRfc() );
				reviewRequestView.setGroupId( this.reviewRequestView.getGroupId() );
				reviewRequestView.setRequestId( requestView.getRequestId() );
				utilityService.persistReviewRequest( reviewRequestView );				
		
				updateTaskOutcome(userTask, TaskOutcome.LEGAL_DEPT_REVIEW, CommonUtil.getLoggedInUser(), soaConfigPath);
				
				pageMode = WebConstants.Takharuj.PAGE_MODE_VIEW_ONLY;
				
				updateValuesToMaps();
				saveAttachmentComment();
				saveSystemComments(MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW);
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_REVIEW ) );
			}
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{				
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	public String onReviewDone()
	{
		String METHOD_NAME = "onReviewDone()";
		String path = null;
		
		try
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			DomainDataView ddvRequestStatusApprovalRequired = CommonUtil.getIdFromType( 
					CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
					WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED );
			requestView.setStatusId( ddvRequestStatusApprovalRequired.getDomainDataId() );
			requestView.setStatusEn( ddvRequestStatusApprovalRequired.getDataDescEn() );
			requestView.setStatusAr( ddvRequestStatusApprovalRequired.getDataDescAr() );
			
			if ( saveTakharujRequest( requestView ) )
			{
				ReviewRequestView reviewRequestView = ( ReviewRequestView ) viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW );
				utilityService.persistReviewRequest( reviewRequestView );
				
				updateTaskOutcome(userTask, TaskOutcome.OK, CommonUtil.getLoggedInUser(), soaConfigPath);
				
				pageMode = WebConstants.Takharuj.PAGE_MODE_VIEW_ONLY;
				
				updateValuesToMaps();
				saveAttachmentComment();
				saveSystemComments(MessageConstants.RequestEvents.REQUEST_REVIEWED);
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsCommonRequestMsgs.SUCC_REQ_REVIEW_DONE ) );
			}
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{				
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.General.OPERATION_FAILURE ) );
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return path;
	}
	
	private void updateTaskOutcome(UserTask userTask, TaskOutcome taskOutCome, String loggedInUser, String soaConfigPath) throws Exception 
	{
		String METHOD_NAME = "updateTaskOutcome()";		
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");				
		BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(soaConfigPath);
		bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
		logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");				
	}
	
	private void setPageModeForRequest(RequestView requestView) {
		
		DomainDataView ddvRequestStatusNew = CommonUtil.getIdFromType( 
				CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
				WebConstants.REQUEST_STATUS_NEW );
		
		if ( requestView.getStatusId() != null && ! requestView.getStatusId().equals( ddvRequestStatusNew.getDomainDataId() ) )
		{
			pageMode = WebConstants.Takharuj.PAGE_MODE_VIEW_ONLY;
			canAddAttachmentsAndComments(true);	
		}
		setPageModeForTask(requestView);
	}
	
	@SuppressWarnings( "unchecked" )
	public void onOpenFile()
	{
		String methodName = "onOpenFile|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			sessionMap.put( WebConstants.InheritanceFile.FILE_ID, this.inheritanceFileView.getInheritanceFileId());
		    sessionMap.put (WebConstants.InheritanceFilePageMode.IS_POPUP,true);
			String javaScriptText ="var screen_width = 900;"+
	        "var screen_height =600;"+
	        "window.open('inheritanceFile.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes,resizable=true');";
			executeJavascript( javaScriptText);
		    
		    logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}
	@SuppressWarnings("unchecked")
	private void setPageModeForTask(RequestView requestView) {
		pageMode = WebConstants.Takharuj.PAGE_MODE_VIEW_ONLY;
		DomainDataView ddvRequestStatusApprovalRequired = CommonUtil.getIdFromType( 
				CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
				WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED );
		
		if ( requestView.getStatusId() != null && requestView.getStatusId().equals( ddvRequestStatusApprovalRequired.getDomainDataId() ) )
		{
			pageMode = WebConstants.Takharuj.PAGE_MODE_REQUEST_APPROVAL_REQUIRED;
			takharujTabPanel.setSelectedTab(TAB_DETAILS);
			canAddAttachmentsAndComments(true);	
			return;
		}
		
		DomainDataView ddvRequestStatusJudgeSessionReq = CommonUtil.getIdFromType( 
				CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
				WebConstants.REQUEST_STATUS_JUDGE_SESSION_REQ );
		
		if ( requestView.getStatusId() != null && requestView.getStatusId().equals( ddvRequestStatusJudgeSessionReq.getDomainDataId() ) )
		{
			pageMode = WebConstants.Takharuj.PAGE_MODE_REQUEST_JUDGE_SESSION_REQ;
			canAddAttachmentsAndComments(true);
			takharujTabPanel.setSelectedTab(TAB_SESSION);
			onSessionTabClick();
			viewMap.put( WebConstants.Session.SESSION_DETAIL_TAB_MODE_KEY, WebConstants.Session.SESSION_DETAIL_TAB_MODE_UPDATABLE );
			return;
		}
		
		DomainDataView ddvRequestStatusReviewReq = CommonUtil.getIdFromType( 
				CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
				WebConstants.REQUEST_STATUS_REVIEW_REQUIRED );
		
		if ( requestView.getStatusId() != null && requestView.getStatusId().equals( ddvRequestStatusReviewReq.getDomainDataId() ) )
		{
			pageMode = WebConstants.Takharuj.PAGE_MODE_REQUEST_REVIEW_REQUIRED;
			canAddAttachmentsAndComments(true);
			takharujTabPanel.setSelectedTab(TAB_REVIEW);
			onReviewTabClick();
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
			return;
		}
		
		DomainDataView ddvRequestStatusApproved = CommonUtil.getIdFromType( 
				CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
				WebConstants.REQUEST_STATUS_APPROVED );
		
		
		if ( requestView.getStatusId() != null && requestView.getStatusId().equals( ddvRequestStatusApproved.getDomainDataId() ) )
		{
			pageMode = WebConstants.Takharuj.PAGE_MODE_REQUEST_APPROVED;
			takharujTabPanel.setSelectedTab(TAB_DETAILS);
			canAddAttachmentsAndComments(true);	
			return;
		}
		
		DomainDataView ddvRequestStatusRejectedResubmitted = CommonUtil.getIdFromType( 
				CommonUtil.getDomainDataListForDomainType( WebConstants.REQUEST_STATUS ), 
				WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT);
		
		
		if ( requestView.getStatusId() != null && requestView.getStatusId().equals( ddvRequestStatusRejectedResubmitted.getDomainDataId() ) )
		{
			pageMode = WebConstants.Takharuj.PAGE_MODE_REQUEST_REJECTED_RESUBMIT;
			takharujTabPanel.setSelectedTab(TAB_DETAILS);
			canAddAttachmentsAndComments(true);	
			return;
		}
		
	}

	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	public boolean getIsPageModeViewOnly()
	{
		boolean isPageModeViewOnly = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Takharuj.PAGE_MODE_VIEW_ONLY ) )
			isPageModeViewOnly = true;
		
		return isPageModeViewOnly;
	}
	
	public boolean getIsPageModeRequestNew()
	{
		boolean isPageModeRequestNew = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Takharuj.PAGE_MODE_REQUEST_NEW ) )
			isPageModeRequestNew = true;
		
		return isPageModeRequestNew;
	}
	public boolean getIsPageModeRequestRejectedResubmit()
	{
		boolean isPageModeRequestNew = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Takharuj.PAGE_MODE_REQUEST_REJECTED_RESUBMIT ) )
			isPageModeRequestNew = true;
		
		return isPageModeRequestNew;
	}
	
	public boolean getIsPageModeApproved()
	{
		if ( pageMode.equalsIgnoreCase( WebConstants.Takharuj.PAGE_MODE_REQUEST_APPROVED) )
			return true;
		
		return false;
	}
	public boolean getIsPageModeApprovalRequired()
	{
		boolean isPageModeApprovalRequired = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Takharuj.PAGE_MODE_REQUEST_APPROVAL_REQUIRED ) )
			isPageModeApprovalRequired = true;
		
		return isPageModeApprovalRequired;
	}
	
	public boolean getIsPageModeReviewRequired()
	{
		boolean isPageModeReviewRequired = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Takharuj.PAGE_MODE_REQUEST_REVIEW_REQUIRED ) )
			isPageModeReviewRequired = true;
		
		return isPageModeReviewRequired;
	}
	
	public boolean getIsPageModeJudgeSessionReq()
	{
		boolean isPageModeJudgeSessionReq = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.Takharuj.PAGE_MODE_REQUEST_JUDGE_SESSION_REQ ) )
			isPageModeJudgeSessionReq = true;
		
		return isPageModeJudgeSessionReq;
	}

	public InheritedAssetService getInheritedAssetService() {
		return inheritedAssetService;
	}

	public void setInheritedAssetService(InheritedAssetService inheritedAssetService) {
		this.inheritedAssetService = inheritedAssetService;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public List<SelectItem> getInheritedAssetSelectItems() {
		return inheritedAssetSelectItems;
	}

	public void setInheritedAssetSelectItems(
			List<SelectItem> inheritedAssetSelectItems) {
		this.inheritedAssetSelectItems = inheritedAssetSelectItems;
	}

	public InheritanceFileView getInheritanceFileView() {
		return inheritanceFileView;
	}

	public void setInheritanceFileView(InheritanceFileView inheritanceFileView) {
		this.inheritanceFileView = inheritanceFileView;
	}

	public Map<String, InheritedAssetView> getInheritedAssetMap() {
		return inheritedAssetMap;
	}

	public void setInheritedAssetMap(
			Map<String, InheritedAssetView> inheritedAssetMap) {
		this.inheritedAssetMap = inheritedAssetMap;
	}

	public String getSelectedInheritedAssetId() {
		return selectedInheritedAssetId;
	}

	public void setSelectedInheritedAssetId(String selectedInheritedAssetId) {
		this.selectedInheritedAssetId = selectedInheritedAssetId;
	}

	public List<TakharujInheritedAssetView> getTakharujInheritedAssetViewList() {
		return takharujInheritedAssetViewList;
	}

	public void setTakharujInheritedAssetViewList(
			List<TakharujInheritedAssetView> takharujInheritedAssetViewList) {
		this.takharujInheritedAssetViewList = takharujInheritedAssetViewList;
	}

	public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public TakharujService getTakharujService() {
		return takharujService;
	}

	public void setTakharujService(TakharujService takharujService) {
		this.takharujService = takharujService;
	}

	public HtmlDataTable getTakharujInheritedAssetViewDataTable() {
		return takharujInheritedAssetViewDataTable;
	}

	public void setTakharujInheritedAssetViewDataTable(
			HtmlDataTable takharujInheritedAssetViewDataTable) {
		this.takharujInheritedAssetViewDataTable = takharujInheritedAssetViewDataTable;
	}

	public SystemParameters getSystemParameters() {
		return systemParameters;
	}

	public void setSystemParameters(SystemParameters systemParameters) {
		this.systemParameters = systemParameters;
	}

	public UserTask getUserTask() {
		return userTask;
	}

	public void setUserTask(UserTask userTask) {
		this.userTask = userTask;
	}

	public String getSoaConfigPath() {
		return soaConfigPath;
	}

	public void setSoaConfigPath(String soaConfigPath) {
		this.soaConfigPath = soaConfigPath;
	}

	public UtilityService getUtilityService() {
		return utilityService;
	}

	public void setUtilityService(UtilityService utilityService) {
		this.utilityService = utilityService;
	}

	public ReviewRequestView getReviewRequestView() {
		return reviewRequestView;
	}

	public void setReviewRequestView(ReviewRequestView reviewRequestView) {
		this.reviewRequestView = reviewRequestView;
	}

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}	
	@SuppressWarnings("unchecked")
	public void openTakharujEffect()
	{
		TakharujInheritedAssetView gridRow = (TakharujInheritedAssetView) takharujInheritedAssetViewDataTable.getRowData();
		String javaScriptText = " var screen_width = 1000;"
			+ "var screen_height = 380;"
			+ "var popup_width = screen_width;"
			+ "var popup_height = screen_height;"
			+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2;"
			+ "window.open('TakharujEffect.jsf?pageMode=MODE_SELECT_MANY_POPUP&"
			+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";

	Map sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	sessionMap.put(WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW,gridRow);
	executeJavascript(javaScriptText);
		
	}
	private void saveAttachmentComment() throws Exception 
	{
		if(requestView.getRequestId() != null)
		{
			Long requestId = new Long(requestView.getRequestId());
			CommonUtil.saveAttachments(requestId);
			CommonUtil.saveComments(requestId, noteOwner);
		}
		  
	}

	public String getNoteOwner() {
		return noteOwner;
	}

	public String getExternalId() {
		return externalId;
	}

	public String getProcedureTypeKey() {
		return procedureTypeKey;
	}
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(Long fileId)
	{
		
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();

		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.Takharuj.PROCEDURE_KEY_TAKHARUJ);
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, WebConstants.Attachment.PIMS_REPOSITORY_ID);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.Takharuj.EXTERNAL_ID_TAKHARUJ);
		viewMap.put("noteowner", WebConstants.Takharuj.NOTES_OWNER);
		
		if(fileId!= null)
		{
	    	String entityId = fileId.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}
	@SuppressWarnings("unchecked")
	private void canAddAttachmentsAndComments(boolean canAdd){
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	public void requestHistoryTabClick()
	{
		String methodName="requestHistoryTabClick";
    	logger.logInfo(methodName+"|"+"Start..");    	
    	
    	if(requestView.getRequestId()!= null)
    	try	
    	{
    	  RequestHistoryController rhc=new RequestHistoryController();
    	  Long requestId = new Long(requestView.getRequestId());
    	  if(requestId!=null )
    	    rhc.getAllRequestTasksForRequest(noteOwner,requestId.toString());
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
	}
	public void openManageAsset()
	{
		
		TakharujInheritedAssetView takharujRow = (TakharujInheritedAssetView) takharujInheritedAssetViewDataTable.getRowData();
		String javaScriptText = " var screen_width = 970;"
				+ "var screen_height = 550;"
				+ "var popup_width = screen_width;"
				+ "var popup_height = screen_height;"
				+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
				+ "window.open('ManageAssets.jsf?pageMode=MODE_SELECT_ONE_POPUP&"
				+ WebConstants.ASSET_ID+"="+takharujRow.getInheritedAssetView().getAssetMemsView().getAssetId()
				+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";

		executeJavascript(javaScriptText);
	}
	private boolean isReviewValidated() throws Exception
	{
		
		boolean isValid = true;
		if( this.reviewRequestView.getGroupId() == null || StringHelper.isEmpty( this.reviewRequestView.getGroupId()) || this.reviewRequestView.getGroupId().compareTo("-1") == 0){
			errorMessages.add(CommonUtil.getBundleMessage("takharuj.errMsg.plzSelectgGroup"));
			isValid = false;
		}
		if( this.reviewRequestView.getRfc() == null || StringHelper.isEmpty( this.reviewRequestView.getRfc())){
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.plzProvidecomments"));
			isValid = false;
		}
		return isValid;
	}

	public HtmlTabPanel getTakharujTabPanel() {
		return takharujTabPanel;
	}

	public void setTakharujTabPanel(HtmlTabPanel takharujTabPanel) {
		this.takharujTabPanel = takharujTabPanel;
	}
	@Override
	public void prerender() {
		super.prerender();
		if(!isPostBack() && pageMode != null && pageMode.compareTo(WebConstants.Takharuj.PAGE_MODE_REQUEST_REVIEW_REQUIRED) == 0)
			takharujTabPanel.setSelectedTab("reviewTab");
	}
}

