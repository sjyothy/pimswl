package com.avanza.pims.web.mems.plugins;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.mems.PropertyRevenueAndExpensesView;

public class TabRevenueAndExpensesList extends AbstractMemsBean
{	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tabMode;
	private List<PropertyRevenueAndExpensesView> dataList;
    private HtmlDataTable dataTable;
	public TabRevenueAndExpensesList() 
	{
		tabMode = WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_VIEW_ONLY;
		dataList=new ArrayList<PropertyRevenueAndExpensesView>();
		dataTable=new HtmlDataTable();
		
	}
	
	@Override
	public void init() 
	{
		
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException("init --- CRASHED --- ", exception);
		}
	}
	public void onDataFromParent(Long endowmentId) throws Exception
	{
		Endowment endowment = EntityManager.getBroker().findById(Endowment.class, endowmentId);
		DecimalFormat oneDForm = new DecimalFormat("#.00");
		dataList = new ArrayList<PropertyRevenueAndExpensesView>();
		PropertyRevenueAndExpensesView obj = new PropertyRevenueAndExpensesView();
		obj.setYear("2016");
		obj.setMasrafAlWaqfiPercentage(endowment.getRevenuePercentage().toString());
		obj.setReconstructionPercentage(endowment.getReconstructionPercentage().toString());
		if(endowment.getCostCenter().equals("246100"))
		{
		
		
		obj.setTotalRevenue("274166.69");
		obj.setTotalExpenses( "22158.34");
		
		
		
		
		}
		else if(endowment.getCostCenter().equals("246200"))
		{
			
		
			obj.setTotalRevenue("105000");
			obj.setTotalExpenses( "8745");
		}
		else if(endowment.getCostCenter().equals("200100"))
		{
			
		
			obj.setTotalRevenue("357972");
			obj.setTotalExpenses( "102096.3");
		}
		if(
				endowment.getCostCenter().equals("200100") || 
				endowment.getCostCenter().equals("246200") || 
				endowment.getCostCenter().equals("246100")		
			)
		{
			Double netProfit = Double.valueOf( obj.getTotalRevenue())-Double.valueOf( obj.getTotalExpenses());
			if(netProfit != null )
			{
			obj.setNetProfit(				
					 			oneDForm.format(  netProfit)
							);
			}
			Double revenueAmount = 				(
														Double.valueOf( obj.getNetProfit())*
														Double.valueOf(obj.getMasrafAlWaqfiPercentage())
													 )/100d;
			Double reconstructionAmount = 				(
					Double.valueOf( obj.getNetProfit())*
					Double.valueOf(obj.getReconstructionPercentage())
				 )/100d;
			if(reconstructionAmount != null )
			{
			obj.setReconstructionAmount( oneDForm.format( reconstructionAmount) );
			}
										
			if(revenueAmount != null )
			{
			obj.setMasrafAlWaqfiAmount( 
					oneDForm.format( revenueAmount));
			}
		}
		dataList.add(obj);
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws PimsBusinessException
	{
		// TAB MODE
		if ( viewMap.get( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY ) != null )
		{
			tabMode = (String) viewMap.get( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY );
		}

		if ( viewMap.get( "revenueAndExpensesList" ) != null )
		{
			dataList =  (ArrayList<PropertyRevenueAndExpensesView>) viewMap.get( "revenueAndExpensesList" );
		}
		updateValuesToMaps();
	}
		
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, tabMode );
		}
		if ( dataList != null )
		{
			viewMap.put( "revenueAndExpensesList" , dataList );
		}
	}	

	@SuppressWarnings("unchecked")
	private boolean hasError( List<String> errorMessages  ) throws Exception 
	{
		boolean hasErrors = true;
		
		
		return hasErrors;
	}
	
    @SuppressWarnings( "unchecked" )
	public void onNavigateToUnblocking()
	{
		errorMessages = new ArrayList<String>();
		try	
		{	
			if ( !hasError( errorMessages ) ){ return ;  }
			updateValuesToMaps();
			updateValuesToMaps();
	
		}
		catch ( Exception e )
		{
			logger.LogException( "onNavigateToUnblocking --- CRASHED --- ", e);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
		
		finally
		{
			if( errorMessages != null && errorMessages.size() > 0 )
			{
				viewMap.put( WebConstants.BlockingRequest.BLOCKING_DETAIL_TAB_ERROR_MESSAGES,errorMessages);
			}
		}
	}
	
	


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<PropertyRevenueAndExpensesView> getDataList() {
		return dataList;
	}

	public void setDataList(List<PropertyRevenueAndExpensesView> dataList) {
		this.dataList = dataList;
	}


	
}
