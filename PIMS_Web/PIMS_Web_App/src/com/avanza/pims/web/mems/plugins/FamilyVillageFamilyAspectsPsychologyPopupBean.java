package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.FamilyVillageFamilyAspectService;
import com.avanza.pims.ws.mems.FamilyVillageFileService;
import com.avanza.pims.ws.vo.mems.FamilyAspectPsychologyView;
import com.avanza.pims.ws.vo.mems.FamilyDetailsView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageFamilyAspectsPsychologyPopupBean extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	FamilyAspectPsychologyView pageView;	
	private Long personId;
	List<SelectItem>  familyDetailsList = new ArrayList<SelectItem>();
	protected HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
	public FamilyVillageFamilyAspectsPsychologyPopupBean() 
	{
		pageView = new FamilyAspectPsychologyView ();
		pageView.setCreatedBy (getLoggedInUserId());
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			if(!isPostBack())
			{
				if (request.getParameter("personId") != null) 
				{
					personId = Long.valueOf( request.getParameter("personId").toString() );
					loadFamilyDetailsByPersonId();
					
				}
			}
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		if(viewMap.get("pageView") != null)
		{
			pageView =  (FamilyAspectPsychologyView)viewMap.get("pageView");
		}
		if(viewMap.get("familyDetailsList") != null)
		{
			familyDetailsList =  (ArrayList<SelectItem>)viewMap.get("familyDetailsList");
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
		if(pageView != null)
		{
			viewMap.put("pageView",pageView);
		}
		if( familyDetailsList != null && familyDetailsList.size()>0)
		{
			viewMap.put("familyDetailsList",familyDetailsList);
		}
	}	
	
	@SuppressWarnings("unchecked")
	private boolean hasErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		
		if( pageView.getFamilyDetailsIdString() == null || pageView.getFamilyDetailsIdString().equals("-1")) {
			errorMessages.add(CommonUtil.getBundleMessage("researchFamilyVillageBeneficiary.msg.FamilyAspectPsychology.familyMemberRequired"));
			return true;
		}

		if( pageView.getDisorderTypeIdString()  == null || pageView.getDisorderTypeIdString().equals("-1")) {
			errorMessages.add(CommonUtil.getBundleMessage("researchFamilyVillageBeneficiary.msg.FamilyAspectPsychology.disorderTypeRequired"));
			return true;
		}
		
		if( pageView.getDescription() == null || pageView.getDescription().trim().length() <=0) {
			errorMessages.add(CommonUtil.getBundleMessage("violationDetails.msg.descriptionRequired"));
			return true;
		}
		return hasSaveErrors;
		
	}

	
	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			
			if(hasErrors())return;
			pageView.setUpdatedBy (getLoggedInUserId());
			FamilyVillageFamilyAspectService.addFamilyAspectPsychology(pageView);
			successMessages.add( ResourceUtil.getInstance().getProperty( "commons.savedSuccessfully" ) );
		}
		catch ( Exception e )
		{
			logger.LogException( "onSave--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	public void loadFamilyDetailsByPersonId() throws Exception
	{
	
		List<FamilyDetailsView> list = FamilyVillageFileService.getFamilyDetailsByPersonId(personId);
		
		SelectItem item = null;
		for (FamilyDetailsView fd : list) {
			 item = new SelectItem(
				                   fd.getFamilyDetailsId().toString(),fd.getFamilyMember().getPersonFullName()
								  );
		}
		familyDetailsList.add(item);
	}
	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	public FamilyAspectPsychologyView getPageView() {
		return pageView;
	}

	public void setPageView(FamilyAspectPsychologyView pageView) {
		this.pageView = pageView;
	}

	public List<SelectItem> getFamilyDetailsList() {
		return familyDetailsList;
	}

	public void setFamilyDetailsList(List<SelectItem> familyDetailsList) {
		this.familyDetailsList = familyDetailsList;
	}

}
