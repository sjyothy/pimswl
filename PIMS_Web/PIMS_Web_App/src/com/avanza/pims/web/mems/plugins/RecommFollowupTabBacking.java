package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.MemsFollowupService;
import com.avanza.pims.ws.vo.mems.MemsFollowupView;

public class RecommFollowupTabBacking extends AbstractMemsBean
{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2526741634539592008L;
	private MemsFollowupView memsFollowupView;
	private List<MemsFollowupView> memsFollowupViewList;
	private HtmlDataTable memsFollowUpDataTable;
    private Long recommendationId;
    MemsFollowupService service;
	public RecommFollowupTabBacking() 
	{
		memsFollowupView = new MemsFollowupView();
		memsFollowupViewList = new ArrayList<MemsFollowupView>(0);
		memsFollowUpDataTable = new HtmlDataTable();
		service = new MemsFollowupService();
	}
	
	@Override
	@SuppressWarnings( "unchecked" )
	public void init() 
	{
		String METHOD_NAME = "init()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			getParameters();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	@SuppressWarnings( "unchecked" )
	private void getParameters() throws Exception
	{
		Map  searchCriteria;
		if( viewMap.containsKey(WebConstants.RecommenationFollowup.RECOMMENDATION_ID) )//this will be executed while open in popup
		{
			long recommedationId =  new Long ( viewMap.get(WebConstants.RecommenationFollowup.RECOMMENDATION_ID).toString());
			searchCriteria= new HashMap();
			searchCriteria.put(WebConstants.RecommenationFollowup.RECOMMENDATION_ID,recommedationId );
			loadDataList( searchCriteria );
		}
	}
	@SuppressWarnings( "unchecked" )
	private void loadDataList( Map  searchCriteria ) throws Exception
	{
		this.setMemsFollowupViewList( service.getRecommFollowup(searchCriteria) );
	}
	private boolean hasError()
	{
		boolean hasError = false;
		
		return hasError;
	}
	public void onSave()
	{
		String methodName = "onSave|";
		try	
		{	
			
			logger.logInfo(methodName + " --- STARTED --- ");
			if(!hasError())
			{
				MemsFollowupView vo = this.getMemsFollowupView();
				
				if (viewMap.containsKey(WebConstants.RecommenationFollowup.RECOMMENDATION_ID))
					vo.setRecommedationId(new Long(viewMap.get(WebConstants.RecommenationFollowup.RECOMMENDATION_ID).toString()));
				
				vo.setUpdatedBy( CommonUtil.getLoggedInUser() );
				vo.setCreatedBy( CommonUtil.getLoggedInUser() );
				vo.setIsDeleted(0L);
				vo.setCreatedOn(new Date());
				vo.setUpdatedOn(new Date());
				service.persistRecommFollowup( vo );
				List <MemsFollowupView> list = this.getMemsFollowupViewList();
				list.add( vo );
				this.setMemsFollowupViewList(list);
				clearFields();
					
			}
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void clearFields() 
	{
		MemsFollowupView followup = new MemsFollowupView();
	    viewMap.put("memsFollowupView" ,followup);
	}

	public void onTemplateMethod()
	{
		String methodName = "onSave|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
			logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
		}
		
	}	

	@SuppressWarnings( "unchecked" )
	public MemsFollowupView getMemsFollowupView() {
		if( viewMap.get( "memsFollowupView" )!=null )
			memsFollowupView = (MemsFollowupView)viewMap.get( "memsFollowupView" ); 
		return memsFollowupView;
	}
	@SuppressWarnings( "unchecked" )
	public void setMemsFollowupView(MemsFollowupView memsFollowupView) {
		this.memsFollowupView = memsFollowupView;
		if ( this.memsFollowupView != null )
			viewMap.put("memsFollowupView", this.memsFollowupView);
	}
	@SuppressWarnings( "unchecked" )
	public List<MemsFollowupView> getMemsFollowupViewList() {
		if( viewMap.get( "memsFollowupViewList" )!=null )
			memsFollowupViewList = ( ArrayList<MemsFollowupView> )viewMap.get( "memsFollowupViewList" );
		return memsFollowupViewList;
	}
	@SuppressWarnings( "unchecked" )
	public void setMemsFollowupViewList(List<MemsFollowupView> memsFollowupViewList) {
		this.memsFollowupViewList = memsFollowupViewList;
		if ( this.memsFollowupViewList != null )
			viewMap.put("memsFollowupViewList", this.memsFollowupViewList);
	}

	public HtmlDataTable getMemsFollowUpDataTable() {
		return memsFollowUpDataTable;
	}

	public void setMemsFollowUpDataTable(HtmlDataTable memsFollowUpDataTable) {
		this.memsFollowUpDataTable = memsFollowUpDataTable;
	}
	
	public Boolean getIsEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
		
		
	}


	public Long getRecommendationId() {
		return recommendationId;
	}

	public void setRecommendationId(Long recommendationId) {
		this.recommendationId = recommendationId;
	}

	@SuppressWarnings("unchecked")
	public void loadAttachments(Long objectId)
	{
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.InheritanceFile.PROCEDURE_KEY_OPEN_FILE);
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, WebConstants.Attachment.PIMS_REPOSITORY_ID);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.RecommenationFollowup.RECOMMENDATION_ID);
		viewMap.put("noteowner", WebConstants.RecommenationFollowup.NOTES_OWNER);	
	
		if(objectId!= null)
		{
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, objectId.toString());
			viewMap.put("entityId", objectId.toString());
		}
	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	@SuppressWarnings("unchecked")
	public void openRecommendationPopup()
	{
		MemsFollowupView followUp = (MemsFollowupView) memsFollowUpDataTable.getRowData();
		sessionMap.put(WebConstants.PAGE_MODE, WebConstants.InheritanceFilePageMode.IS_POPUP);
		sessionMap.put(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW,followUp) ;
		sessionMap.put(WebConstants.InheritanceFile.FILE_ID, viewMap.get(WebConstants.InheritanceFile.FILE_ID));
		executeJavascript("openFollowupPopup();");
	}
	public void deleteFollowup()
	{
		MemsFollowupView follwupToDelete  = (MemsFollowupView) memsFollowUpDataTable.getRowData();
		int indexToDelete = memsFollowUpDataTable.getRowIndex();
		memsFollowupViewList = this.getMemsFollowupViewList();
		try 
		{
			
			follwupToDelete.setUpdatedBy(CommonUtil.getLoggedInUser());
			follwupToDelete.setUpdatedOn(new Date());
			follwupToDelete.setIsDeleted(1L);
			new InheritanceFileService().deleteRecommFollowup(follwupToDelete);
			memsFollowupViewList.remove(indexToDelete);
		} 
		catch (Exception e) 
		{
			logger.LogException("deleteFollowup carshed |", e);
		}
	}
	@SuppressWarnings("unchecked")
	public void editFollowup()
	{
		MemsFollowupView followUp = (MemsFollowupView) memsFollowUpDataTable.getRowData();
		int indexToUpdate = memsFollowUpDataTable.getRowIndex();
		viewMap.put("FOLLOW_UP_INDEX_TO_UPDATE", indexToUpdate);
		viewMap.put("memsFollowupView",  followUp);
	}
}
