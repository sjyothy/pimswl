package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.ui.util.ResourceUtil;

  public class ThirdPartyPaymentsTab extends AbstractSearchBean  
  {
		private static final long serialVersionUID = 1L;
	 	
		private final String DEFAULT_SORT_FIELD = "paymentScheduleId";
		RequestService service = new RequestService();
	 	List<PaymentScheduleView>  dataList ;
	 	PropertyService psa = new PropertyService();
	 	Long endowmentId; 
	 	Long endowmentFileId;
	 	@Override
		@SuppressWarnings("unchecked")
		public void init() 
		{
			try 
			{
				if( !isPostBack() )
				{
					setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
					setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
					setSortField(DEFAULT_SORT_FIELD);
					setSortItemListAscending(true);
				}
				
			} 
			catch (Exception exception) 
			{
				logger.LogException("init --- CRASHED --- ", exception);
				errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			}
		}


		@SuppressWarnings( "unchecked" )
		public void onDistribute()
		{
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			try	
			{	
				PaymentScheduleView ps = (PaymentScheduleView) dataTable.getRowData();
				sessionMap.put(WebConstants.DistributeEndowmentRevenue.PaymentView,ps );
				executeJavaScript("javaScript:openDistributeEndowmentRevenuePopup();");
			}
			catch (Exception exception) 
			{
				logger.LogException( "onDistribute--- CRASHED --- ", exception);
				errorMessages = new ArrayList<String>(0);
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
			
		}
		@SuppressWarnings("unchecked")
		public void onDataFromParent(Long endowmentId, Long endowmentFileId)throws Exception 
		{
			setEndowmentId(endowmentId);
			setEndowmentFileId(endowmentFileId);
			onSearch();
		}
	
		@SuppressWarnings("unchecked")
		public void onSearch() 
		{
			try 
			{
				pageFirst();// at last
			} 
			catch (Exception e) 
			{
				logger.LogException("onSearch |Error Occured", e);
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		}

		@SuppressWarnings("unchecked")
		public void doSearchItemList()
		{
			try {
					loadDataList();
				}
			    catch (Exception e){
			    	errorMessages = new ArrayList<String>(0);
			 		logger.LogException("doSearchItemList|Error Occured", e);
			 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			    }
		}

		@SuppressWarnings("unchecked")
		public void loadDataList() throws Exception 
		{
			HashMap searchMap = new HashMap();
			fillSearchCriteria(searchMap);
			int totalRows = 0;
			
			totalRows =  psa.searchChequeGetTotalNumberOfRecords(searchMap);
			setTotalRows(totalRows);
			doPagingComputations();

			dataList = service.getThirdPartyPayments(searchMap, getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());

			if (dataList == null || dataList.isEmpty() ) 
			{
				forPaging(0);
				if (dataList== null || dataList.isEmpty() ) 
				{
					errorMessages = new ArrayList<String>();
					errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
				}
			}
			
			this.setDataList( dataList );
			forPaging(getTotalRows());
		}

		@SuppressWarnings("unchecked")
		private void fillSearchCriteria(HashMap searchMap) 
		{
			if (getEndowmentId() != null )
			{
				searchMap.put("endowmentId", getEndowmentId() );
			}
			if (getEndowmentFileId() != null )
			{
				searchMap.put("endowmentFileId", getEndowmentFileId());
			}
		}

		private void executeJavascript(String javascript)throws Exception 
		{
				FacesContext facesContext = FacesContext.getCurrentInstance();			
				AddResource addResource = AddResourceFactory.getInstance(facesContext);
				addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
		}	
		
		@SuppressWarnings("unchecked")
		public List<PaymentScheduleView> getDataList() {
			if(viewMap.get( "dataListPayment" ) != null )
			{
				dataList = (ArrayList<PaymentScheduleView>)  viewMap.get( "dataListPayment")  ;
			}
			return dataList;
		}
		
		@SuppressWarnings("unchecked")
		public void setDataList(List<PaymentScheduleView> dataList) {
			this.dataList = dataList;
			if( this.dataTable  != null )
			{
				viewMap.put("dataListPayment",this.dataList );
			}
		}

		@SuppressWarnings("unchecked")
		public Long getEndowmentId() 
		{
			if(  viewMap.get( "searchEndowment") != null )
			{
				this.endowmentId = Long.valueOf( viewMap.get( "searchEndowment").toString() );
			}
			return endowmentId;
		}
		
		@SuppressWarnings("unchecked")
		public void setEndowmentId(Long endowmentId) 
		{
			this.endowmentId = endowmentId;
			if( this.endowmentId  != null )
			{
				viewMap.put("searchEndowment",this.endowmentId );
			}
		}

		@SuppressWarnings("unchecked")
		public Long getEndowmentFileId() 
		{
			if(  viewMap.get( "searchEndowmentFileId") != null )
			{
				this.endowmentFileId = Long.valueOf( viewMap.get( "searchEndowmentFileId").toString() );
			}
			return endowmentFileId;
		}
		
		@SuppressWarnings("unchecked")
		public void setEndowmentFileId(Long endowmentFileId) 
		{
			this.endowmentFileId = endowmentFileId;
			if( this.endowmentFileId  != null )
			{
				viewMap.put("searchEndowmentFileId",this.endowmentFileId );
			}
		}

  }
