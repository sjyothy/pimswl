package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.Utils.DistributedCollectionSearchCriteria;
import com.avanza.pims.entity.CollectionTrx;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.mems.minors.CollectionProcedureBackingBean.Page_Mode;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.CollectionProcedureService;
import com.avanza.pims.ws.mems.MinorTransformUtil;
import com.avanza.pims.ws.vo.OpenFileDistribute;
import com.avanza.pims.ws.vo.mems.CollectionPaymentDetailsView;
import com.avanza.pims.ws.vo.mems.CollectionProcedureView;
import com.avanza.ui.util.ResourceUtil;
import com.collaxa.cube.cluster.message.SetDefaultProcessRevision;

  public class DistributedCollectionsPopup extends AbstractSearchBean  
  {
		private static final long serialVersionUID = 1L;
	 	
		private final String DEFAULT_SORT_FIELD = "collectionTrxId";
		private final String DATA_LIST = "DATA_LIST";
		CollectionProcedureService service = new CollectionProcedureService();
		DistributedCollectionSearchCriteria criteria = new DistributedCollectionSearchCriteria();
	 	List<OpenFileDistribute>  dataList = new ArrayList<OpenFileDistribute>();
	    private Long inheritanceFileId;
		@Override
		@SuppressWarnings("unchecked")
		public void init() 
		{
			try 
			{
				if( !isPostBack() )
				{
					
					getDataFromParent();
//					getDistributedCollectionsList ();
					setCriteria(criteria);
					
					setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
					setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
					setSortField(DEFAULT_SORT_FIELD);
					setSortItemListAscending(true);
				}
			} 
			catch (Exception exception) 
			{
				logger.LogException("init --- CRASHED --- ", exception);
				errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			}
		}
		
		
		
//		@SuppressWarnings("unchecked")
//		private void getDistributedCollectionsList( )throws Exception
//		{
//			setDataList( InheritanceFileService.getDistributedPaymentsAgainstFile( this.getInheritanceFileId() ) ) ;
//	        
//		}
		
		@SuppressWarnings("unchecked")
		private void getDataFromParent()throws Exception 
		{
			if( sessionMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null )
			{
				setInheritanceFileId( new Long( sessionMap.remove( WebConstants.InheritanceFile.INHERITANCE_FILE_ID ).toString() ) );
				criteria.setFileId( getInheritanceFileId() );
			}
		}
	
		public void onSearch() 
		{
			try 
			{
				pageFirst();// at last
			} 
			catch (Exception e) 
			{
				logger.LogException("onSearch |Error Occured", e);
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		}

		public void doSearchItemList()
		{
			try {
					loadDataList();
				}
			    catch (Exception e){
			    	errorMessages = new ArrayList<String>(0);
			 		logger.LogException("doSearchItemList|Error Occured", e);
			 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			    }
		}

		@SuppressWarnings("unchecked")
		public void loadDataList() throws Exception 
		{

			int totalRows = 0;
			totalRows = service.getTotalNumberOfDistributedTrxs( criteria );
			setTotalRows(totalRows);
			doPagingComputations();

			dataList = service.searchDistributedTrx(criteria, getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());

			if (dataList== null || dataList.isEmpty() ) 
			{
				forPaging(0);
				if (dataList== null || dataList.isEmpty() ) 
				{
					errorMessages = new ArrayList<String>();
					errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
				}
			}
			
			this.setDataList( dataList );
			forPaging(getTotalRows());
		}


		
		@SuppressWarnings( "unchecked" )
		public void onOpenDistributePopUp()
		{
			try
			{
				errorMessages = new ArrayList<String>(0);
				OpenFileDistribute row = ( OpenFileDistribute )this.getDataTable().getRowData();
				String assetId = "";
				
				CollectionProcedureView view = new CollectionProcedureView();
				CollectionTrx trx= EntityManager.getBroker().findById(CollectionTrx.class, row.getCollectionTrxId() );
				if( trx.getAssetMems() != null && trx.getAssetMems().getAssetId() != null )
				{
					assetId  = WebConstants.ASSET_ID+ "="+ trx.getAssetMems().getAssetId().toString()+ "&";
					view.setAssetNameEn( trx.getAssetMems().getAssetNameEn()  );
					view.setAssetNameAr( trx.getAssetMems().getAssetNameAr()  );
					view.setAssetId( trx.getAssetMems().getAssetId()  );
				}
				
				view.setAmount(row.getAmount());
				view.setCollectionTrxId( row.getCollectionTrxId() );

				view.setTrxNumber( trx.getTrxNumber() );
				view.setDistributionOption( trx.getDistributionOption() );
				
				CollectionProcedureService.fillDistributionInfo(trx, view);
				CollectionPaymentDetailsView cpdView  = new CollectionPaymentDetailsView ();
				if( trx.getPaymentDetails() != null )
				{
					MinorTransformUtil.transformToCollectionPaymentDetailsView(cpdView,  trx.getPaymentDetails() );
				}
				else
				{
					CollectionTrx parenttrx= EntityManager.getBroker().findById(CollectionTrx.class, trx.getParentCollectionTrxId() );
					
					MinorTransformUtil.transformToCollectionPaymentDetailsView(cpdView,  parenttrx.getPaymentDetails() );
					
				}
				
				String args =  "collectionTrxId="+row.getCollectionTrxId().toString()+"&"+
								WebConstants.InheritanceFile.INHERITANCE_FILE_ID + "=" + this.getInheritanceFileId().toString()+ "&" + 
								assetId +
								Page_Mode.CONFIRM+"=false";

				String javaScriptText = "javaScript:openDistributePopup('"+args+"');";

				sessionMap.put("CollectionTransactionBatch", view );
				sessionMap.put("PaymentDetailsView", cpdView );
				executeJavascript( javaScriptText );
			}
			catch( Exception e )
			{
				
				logger.LogException(   "onOpenDistributePopUp --- CRASHED --- ", e );
	            errorMessages = new ArrayList<String>(0);
	    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}

		}

		private void executeJavascript(String javascript)throws Exception 
		{
				FacesContext facesContext = FacesContext.getCurrentInstance();			
				AddResource addResource = AddResourceFactory.getInstance(facesContext);
				addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
		}	
		
		@SuppressWarnings("unchecked")
		public List<OpenFileDistribute> getDataList() {
			if(viewMap.get( "dataList" ) != null )
			{
				dataList = (ArrayList<OpenFileDistribute>)  viewMap.get( "dataList")  ;
			}
			return dataList;
		}
		
		@SuppressWarnings("unchecked")
		public void setDataList(List<OpenFileDistribute> dataList) {
			this.dataList = dataList;
			if( this.dataTable  != null )
			{
				viewMap.put("dataList ",this.dataList );
			}
		}

		@SuppressWarnings("unchecked")
		public Long getInheritanceFileId() {
			if(viewMap.get( "inheritanceFileId" ) != null )
			{
				inheritanceFileId = Long.valueOf(  viewMap.get( "inheritanceFileId").toString() ) ;
			}
			return inheritanceFileId;
		}

		@SuppressWarnings("unchecked")
		public void setInheritanceFileId(Long inheritanceFileId) {
			this.inheritanceFileId = inheritanceFileId;
			if( this.inheritanceFileId != null )
			{
				viewMap.put("inheritanceFileId",this.inheritanceFileId);
			}
		}


		@SuppressWarnings("unchecked")
		public DistributedCollectionSearchCriteria getCriteria() {
			if( viewMap.get("criteria") != null )
			{
				criteria = (DistributedCollectionSearchCriteria )viewMap.get("criteria"); 
			}
			return criteria;
		}


		@SuppressWarnings("unchecked")
		public void setCriteria(DistributedCollectionSearchCriteria criteria) {
			this.criteria = criteria;
			if(this.criteria != null)
			{
				viewMap.put("criteria", criteria );
			}
		}



  }
