package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.NotesServiceAgent;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.EndowmentFile;
import com.avanza.pims.entity.InheritanceBeneficiary;
import com.avanza.pims.entity.Request;
import com.avanza.pims.exporter.ListBasedExporterMeta;
import com.avanza.pims.exporter.reportdetail.ReportDetail;
import com.avanza.pims.exporter.reportdetail.RequestsOnInheritanceFileTemplateReport;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.FileDetailsReportCriteria;
import com.avanza.pims.report.criteria.InheritanceDetailsReportCriteria;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.endowment.EndowmentFilesService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.ContactInfoView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.NotesVO;
import com.avanza.ui.util.ResourceUtil;



public class InheritenceFileSearch extends AbstractMemsBean
{
	private static final long serialVersionUID = 1L;

	private transient Logger logger = Logger.getLogger(InheritenceFileSearch.class);
	
	private HtmlDataTable dataTable;
	
	private HtmlSelectOneMenu migrated = new HtmlSelectOneMenu();
	private HtmlInputText textInheritenceFileNumber = new HtmlInputText();	
	private HtmlInputText textCreatedOn = new HtmlInputText();
	private HtmlInputText textCreatedBy = new HtmlInputText();
	private HtmlInputText textResearcher = new HtmlInputText();
	private HtmlSelectOneMenu listInheritenceFileType = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu listInheritenceFileStatus = new HtmlSelectOneMenu();
	private HtmlInputText textBenificiary = new HtmlInputText();
	private HtmlInputText textFilePerson = new HtmlInputText();
	private HtmlInputText textAssetNo = new HtmlInputText();
	private HtmlSelectOneMenu listAssetType = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu listCreatedBy = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu listResearchedBy = new HtmlSelectOneMenu();
	private HtmlCalendar clndrCreatedOnTo = new HtmlCalendar();
	private HtmlCalendar clndrCreatedOnFrom = new HtmlCalendar();
	private HtmlInputText textAssetNameEn = new HtmlInputText();
	private HtmlInputText textAssetNameAr = new HtmlInputText();
	private HtmlInputText textAssetDesc = new HtmlInputText();
	private HtmlInputText textSponsor = new HtmlInputText();
	private HtmlInputText textGuardian = new HtmlInputText();
	private HtmlInputText textNurse = new HtmlInputText();
	private HtmlInputText textNewCC =  new HtmlInputText();
	private HtmlInputText textBenCC =  new HtmlInputText();
	
	private HtmlInputText textOldCC =  new HtmlInputText();
	private List<SelectItem> createdByList = new ArrayList<SelectItem>();
	private FileDetailsReportCriteria reportCriteria;
	private InheritanceDetailsReportCriteria inheritanceDetailsCriteria;
	private List<SelectItem> inheritanceFileStatusList = new ArrayList<SelectItem>(0);
	
	private boolean isArabicLocale;	
    private boolean isEnglishLocale;
	private String pageMode;
	private boolean isPageModeSelectOnePopUp = false;
	private boolean isPageModeSelectManyPopUp = false;
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	
	private String  VIEW_MODE="pageMode";
	private String  CONTEXT="context";
	private String  MODE_SELECT_ONE_POPUP="MODE_SELECT_ONE_POPUP";
	private String  MODE_SELECT_MANY_POPUP="MODE_SELECT_MANY_POPUP";
	private static final String NOTE_TYPE = "NOTE_TYPE";
	private static final String USER_NOTE_TYPE = "User";
	private String  DEFAULT_SORT_FIELD = "unitNumber";	
	
	private static final String ISSUE_NOL = "IssueNOL";
	private static final String ISSUE_PAYMENT_REQUEST = "PaymentRequest";
	private static final String ISSUE_INVESTMENT_REQUEST = "InvestmentRequest";
	private static final String ISSUE_NORMAL_PAYMENT = "NormalPaymentRequest";
	private static final String BLOCKING_FILE_COMMENTS = "BLOCKING_FILE_COMMENTS";
	private static final String UNBLOCKING_FILE_COMMENTS = "UNBLOCKING_FILE_COMMENTS";
		
	List<SelectItem> unitType= new ArrayList<SelectItem>();
	List<SelectItem> unitUsage= new ArrayList<SelectItem>();
	List<SelectItem> unitInvestment= new ArrayList<SelectItem>();
	List<SelectItem> unitSide= new ArrayList<SelectItem>();
	List<SelectItem> unitStatus= new ArrayList<SelectItem>();	
	
	
	HashMap<String, Object> inheritenceFileSearchMap = new HashMap<String, Object> ();
	
	InheritanceFileView inheritenceFileView = new InheritanceFileView();
	String floorNumber1 = null;
	ContactInfoView contactInfoView  = new ContactInfoView();
	
		
	FacesContext context=FacesContext.getCurrentInstance();
    Map sessionMap;
    Map viewRootMap;
    private StatementOfAccountCriteria statmntReportCriteria;
    private final String noteOwner = WebConstants.InheritanceFile.NOTES_OWNER;
    private final String procedureTypeKey = WebConstants.InheritanceFile.PROCEDURE_KEY_OPEN_FILE;
    private final String externalId = WebConstants.InheritanceFile.EXTERNAL_ID_OPEN_FILE;

    private ListBasedExporterMeta exporterMeta = new ListBasedExporterMeta();
    
    public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
    public String getNumberFormat()
    {
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
    }
    
     @SuppressWarnings( "unchecked" )
     @Override 
	 public void init() 
     {    	
    	 super.init();
    	 viewRootMap = getFacesContext().getViewRoot().getAttributes();
    	 
    	 try
    	 {
             sessionMap=context.getExternalContext().getSessionMap();
       
              HttpServletRequest request=(HttpServletRequest)this.getFacesContext().getExternalContext().getRequest();	
		     if(!isPostBack())
		     {
		    	 	setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
			        setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);     
			        setSortField(DEFAULT_SORT_FIELD);
			        setSortItemListAscending(true);
		    	 
		    	 if (request.getParameter(VIEW_MODE)!=null )
		    	 {
		    		if(request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
		    		 { 	      	    		
	      	    		viewRootMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);
	      	    	}
	      	    	else if(request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP))
	      	    	{	      	    		
	      	    		viewRootMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);                  
	      	    
	      	    	}
		    		
		    		
		          
		    	 }
		    	 if (request.getParameter(CONTEXT)!=null )
		    	 {
	    			viewRootMap.put(CONTEXT, request.getParameter(CONTEXT).toString());
		    	 }
		    	 loadCreatedByList();
		     }
		    		 
    	 }
    	 catch(Exception es)
    	 {
    		logger.LogException( "init|Error Occured..",es );
 			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
    		 
    	 }
	        
	 }
     



	private void loadCreatedByList() throws Exception 
	{
		List<SelectItem> createdBy = new ArrayList<SelectItem>();
		createdBy =CommonUtil.getCreatedByList();
		setCreatedByList(createdBy);
	}
	@SuppressWarnings("unchecked")
     public ArrayList<SelectItem> getInheritanceFileStatusList() {
 		if (viewRootMap.get("inheritanceFileStatusList") != null)
 			return (ArrayList<SelectItem>) viewRootMap.get("inheritanceFileStatusList");
 		return new ArrayList<SelectItem>();
 	}
     
	 
	private List<InheritanceFileView> dataList = new ArrayList<InheritanceFileView>();
  
	@SuppressWarnings("unchecked")
	public List<InheritanceFileView> getInheritenceFileDataList()
	{
		Map<String, Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		dataList = (List<InheritanceFileView>)viewMap.get("inheritenceFileList");
		if(dataList==null)
			dataList = new ArrayList<InheritanceFileView>();
		return dataList;
	}	
		
	public List<InheritanceFileView> getSelectedInheritanceFile(){
		logger.logInfo("getSelectedInheritenceFile started...");
		List<InheritanceFileView> selectedInheritenceFileList = new ArrayList<InheritanceFileView>(0);
    	try{
    		dataList = getInheritenceFileDataList();
    		if(dataList != null && dataList.size() > 0)
    		{
    			for(InheritanceFileView inheritenceFileView:dataList )
    				if(inheritenceFileView.getSelected())
    				{
    					selectedInheritenceFileList.add(inheritenceFileView);
    				}    		
    		}
    		 		
    		logger.logInfo("getSelectedInheritenceFile completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("getSelectedInheritanceFile crashed ",exception);
    	}    	
    	return selectedInheritenceFileList;
    }
	
	public void doSearch (){
		try {
				pageFirst();
		     
		}
	    catch (Exception e){
	    	logger.LogException("doSearch  crashed ",e);
	    }
	}
	
/*	public InheritanceFileView getFilterCriteria()
	{
		InheritanceFileView inheritanceFileView = new InheritanceFileView();	
		
		String emptyValue = "-1";		
		
		inheritanceFileView.setFileNumber(String.valueOf(textInheritenceFileNumber.getValue()));
		//inheritanceFileView.setCreatedOn(textCreatedOn.getValue());		
		inheritanceFileView.setCreatedBy(String.valueOf(textCreatedBy.getValue()));		
		inheritanceFileView.setResearcher(String.valueOf(textResearcher.getValue()));		
		inheritanceFileView.setFileTypeEn(String.valueOf(listInheritenceFileType.getValue()));
		inheritanceFileView.setStatusId(Long.valueOf(listInheritenceFileStatus.getValue().toString()));
		inheritanceFileView.setBeneficiary(String.valueOf(textBenificiary.getValue()));
		inheritanceFileView.setFilePerson(String.valueOf(textFilePerson.getValue()));
		inheritanceFileView.setAssetNumber(String.valueOf(textAssetNo.getValue()));
		inheritanceFileView.setAssetTypeNameEn(String.valueOf(listAssetType.getValue()));
		inheritanceFileView.setAssetNameEn(String.valueOf(textAssetNameEn.getValue()));
		inheritanceFileView.setAssetNameAr(String.valueOf(textAssetNameAr.getValue()));
		inheritanceFileView.setAssetTypeDesc(String.valueOf(textAssetDesc.getValue()));
		inheritanceFileView.setSponsor(String.valueOf(textSponsor.getValue()));
		inheritanceFileView.setGuardian(String.valueOf(textGuardian.getValue()));
		return inheritanceFileView;
	}*/
	
	
    @SuppressWarnings("unchecked")
	public HashMap<String,Object> getSearchCriteria()
	{
		inheritenceFileSearchMap = new HashMap<String, Object>();		
		
		String emptyValue = "-1";		
		
		Object fileNumber = textInheritenceFileNumber.getValue();
		Date createdOnTo = (Date) clndrCreatedOnTo.getValue();
		Date createdOnFrom = (Date) clndrCreatedOnFrom.getValue();
//		Object createdBy = textCreatedBy.getValue();
		Object createdBy = listCreatedBy.getValue();
		Object researcher=listResearchedBy.getValue();
//		Object researcher = textResearcher.getValue();
		Object fileType = listInheritenceFileType.getValue();
		Object fileStatus = listInheritenceFileStatus.getValue();
		Object benificiary = textBenificiary.getValue();
		Object filePerson = textFilePerson.getValue();
		Object assetNo = textAssetNo.getValue();
		Object assetType = listAssetType.getValue();
		Object assetNameEn = textAssetNameEn.getValue();
		Object assetNameAr = textAssetNameAr.getValue();
		Object assetDesc = textAssetDesc.getValue();
		Object sponsor = textSponsor.getValue();
		Object guardian = textGuardian.getValue();
		Object nurse = textNurse.getValue();
		Object newCC =  textNewCC.getValue();
		Object oldCC =  textOldCC.getValue();
		Object benCC =  textBenCC.getValue();
		
		
		if(migrated.getValue() != null && migrated.getValue().toString().compareTo("1") == 0 ){
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.IS_MIGRATED, true);
		}
		else
			if(migrated.getValue() != null && migrated.getValue().toString().compareTo("2") == 0 ){
				inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.IS_MIGRATED, false);
			}
		
		if (newCC != null && newCC.toString().length() > 0) {
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.NEW_CC, newCC.toString());
		}
		if (benCC != null && benCC.toString().length() > 0) {
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.BEN_CC, benCC.toString());
		}
		 
		if (oldCC != null && oldCC.toString().length() > 0) {
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.OLD_CC, oldCC.toString());
		}	
		
		if(fileNumber !=null && !fileNumber.toString().equals(""))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.FILE_NUMBER, fileNumber);
		
		if(createdOnTo !=null)
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.CREATED_ON_TO, createdOnTo);
		
		if(createdOnFrom !=null)
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.CREATED_ON_FROM, createdOnFrom);
		
		if(createdBy !=null && !createdBy.toString().equals("") && !createdBy.toString().trim().equals(emptyValue))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.CREATED_BY, createdBy);
		
		if(researcher !=null && !researcher.toString().equals("") && !researcher.toString().trim().equals(emptyValue))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.RESEARCHER, researcher);
		
		if (assetType != null && !assetType.toString().equals("")&& !assetType.toString().trim().equals(emptyValue))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.ASSET_TYPE,assetType.toString());
		
		if (assetNo != null && !assetNo.toString().equals(""))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.ASSET_NUMBER,assetNo.toString());
		
		if (fileType != null && !fileType.toString().equals("") && !fileType.toString().trim().equals(emptyValue))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.FILE_TYPE,fileType.toString());
		
		if (filePerson != null && !filePerson.toString().equals(""))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.FILE_PERSON_NAME,filePerson.toString());
		
		if (fileStatus != null && !fileStatus.toString().equals("") && !fileStatus.toString().trim().equals(emptyValue))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.FILE_STATUS,fileStatus.toString());
		
		if (benificiary != null && !benificiary.toString().equals(""))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.BENEFICIARY,benificiary.toString());
		
		if (assetNameEn!= null && !assetNameEn.toString().equals(""))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.ASSET_NAME_EN,assetNameEn.toString());
		
		if (assetNameAr != null && !assetNameAr.toString().equals(""))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.ASSET_NAME_AR,assetNameAr.toString());
		

		if (assetDesc != null && !assetDesc.toString().equals(""))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.ASSET_DESC,assetDesc.toString());
		
		if (sponsor != null && !sponsor.toString().equals(""))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.SPONSOR,sponsor.toString());
		
		if (guardian != null && !guardian.toString().equals(""))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.GUARDIAN,guardian.toString());
		
		if (nurse != null && !nurse.toString().equals(""))
			inheritenceFileSearchMap.put(WebConstants.InheritanceSearchCriteria.NURSE,nurse.toString());
		
		
		
		return inheritenceFileSearchMap;
	}
	public void doSearchItemList()
	{
		loadDataList();
	}
	@SuppressWarnings("unchecked") 
	public List<SelectItem> getFileTypeList() throws Exception {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		String context = null;
		try {
			if(viewRootMap.get(CONTEXT) != null )
			{
				context = viewRootMap.get(CONTEXT).toString();
			}
			if (viewMap.get("FILE_TYPE_LIST") != null) {

				return (List<SelectItem>) viewMap.get("FILE_TYPE_LIST");
			}

			List<DomainDataView> list = new UtilityService().getDomainDataByDomainTypeName("INH_FILE_TYPE");
			if(context ==null || !context.equals("familyVillage") )
			{
				ResourceBundle bundleAr = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(),new Locale("ar"));
				String msgAr = bundleAr.getString("commons.All");
				ResourceBundle bundleEn = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(), new Locale("ar"));
				String msgEn = bundleEn.getString("commons.All");
				selectItems.add(new SelectItem("-1", isEnglishLocale() ?msgEn: msgAr));
			}
			for (DomainDataView type : list) 
			{
				if(context !=null && context.equals("familyVillage") && type.getDomainDataId().compareTo(Constant.InheritanceFileType.FAMILY_VILLAGE_ID)!= 0)
					continue;
				
				selectItems.add(new SelectItem(type.getDomainDataId().toString(), isEnglishLocale() ? 
						type.getDataDescEn()
						: type.getDataDescAr()));
			}
//			Collections.sort(selectItems, ListComparator.LIST_COMPARE);
			viewMap.put("FILE_TYPE_LIST", selectItems);

		} catch (Exception e) {
			logger.LogException("getFileTypeList() crashed", e);
		}
		return selectItems;

	}
	@SuppressWarnings( "unchecked")
	public List<InheritanceFileView> loadDataList() 
	{
		 String methodName="loadDataList";
		 if(dataList != null)
			 dataList.clear();
		 
		 List<InheritanceFileView> list = new ArrayList<InheritanceFileView>();		    
		 
		try
		{
			inheritenceFileSearchMap = getSearchCriteria();
			
			InheritanceFileService fileService = new InheritanceFileService();
			
			
			/////////////////////////////////////////////// For server side paging paging/////////////////////////////////////////
			int totalRows =  fileService.searchInheritenceFile(inheritenceFileSearchMap);
			setTotalRows(totalRows);
			doPagingComputations();
	        //////////////////////////////////////////////////////////////////////////////////////////////
			
			
			list =  fileService.searchInheritenceFileByCriteria(inheritenceFileSearchMap,getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending(),getIsEnglishLocale());
			
			if(list.isEmpty() || list==null)
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));	
			}
			Map<String, Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			viewMap.put("inheritenceFileList", list);
			recordSize = totalRows;
			viewMap.put("recordSize", totalRows);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
	    }
		catch (Exception ex) 
		{
			
			errorMessages = new ArrayList<String>(0);
    		logger.LogException(methodName+"|Error Occured", ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	    } 
		return list;
		
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	

	@SuppressWarnings("unchecked")
	public void setInheritanceFileStatusList(List<SelectItem> inhfileStatusList) {

		this.inheritanceFileStatusList = inhfileStatusList;
		if (this.inheritanceFileStatusList!= null)
			viewRootMap.put("inheritanceFileStatusList", this.inheritanceFileStatusList);
	}
	

	@SuppressWarnings("unchecked")
	public void sendManyFilesInfoToParent(javax.faces.event.ActionEvent event)//many assets selected
	{ 
	       
		try
		{
	      List<InheritanceFileView> selectedFiles= getSelectedFiles();
	      if(!selectedFiles.isEmpty())
	      {
	    	  	sessionMap.put(WebConstants.InheritanceFileSearchOutcomes.INHERITANCE_FILE_SEARCH_SELECTED_MANY_FILES, selectedFiles);
		        String javaScriptText = "javascript:closeWindowSubmit();";		
		        sendToParent(javaScriptText);
	      }
	      else
	      {
	        	errorMessages = new ArrayList<String>();
				
	      }
		}
		catch(Exception e)
		{
			errorMessages = new ArrayList<String>(0);
    		logger.LogException("|Error Occured", e);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			
		}
	        
	}
	private List<InheritanceFileView> getSelectedFiles() {
		logger.logInfo("getSelectedFiles started...");
		List<InheritanceFileView> selectedFilesList = new ArrayList<InheritanceFileView>(0);
    	try{
    		dataList = getInheritenceFileDataList();
    		if(dataList != null && dataList.size() > 0)
    		{
    			for(InheritanceFileView filesView:dataList )
    			
    				if(filesView.getSelected())
    				{
    					selectedFilesList.add(filesView);
    				}
    			
    		}
    		 		
    		logger.logInfo("getSelectedFiles completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("getSelectedFiles() crashed ",exception);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}    	
    	return selectedFilesList;
		// TODO Auto-generated method stub
	
	}
	public void sendToParent(String javaScript) throws Exception
	{
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String javaScriptText =javaScript;
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
	}

	@SuppressWarnings("unchecked")
	public void sendFileInfoToParent(javax.faces.event.ActionEvent event)
	{
		try
		{
		    InheritanceFileView inheritenceFileViewRow=(InheritanceFileView)dataTable.getRowData();
	        
	        String javaScriptText ="window.opener.populateFile('" +inheritenceFileViewRow.getInheritanceFileId().toString()+"'); window.close();"; 
	        	
	        Map sessionMap=FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	        sessionMap.put(WebConstants.InheritanceFileSearchOutcomes.INHERITANCE_FILE_SEARCH_SELECTED_ONE_FILE, inheritenceFileViewRow);
	        sendToParent(javaScriptText);
		}
		catch(Exception e)
		{
			logger.LogException("|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	        
	 }
	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
	
	
	public boolean isContextFamilyVillage()
	{
		
		if( 
				viewRootMap.get(CONTEXT)!=null && 
				viewRootMap.get(CONTEXT).toString().trim().equalsIgnoreCase("familyVillage"))
		{
			return  true;
		}
		else
			return false;
		
	}
	public boolean getIsContextPropertyInquiryInheritenceFile()
	{
		
		if(viewRootMap.containsKey(CONTEXT) && viewRootMap.get(CONTEXT)!=null && viewRootMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.UnitSearchContext.PROPERTY_INQUIRY_UNITS))
		{
			return  true;
		}
		else
			return false;
		
	}
	public boolean getIsContextAuctionInheritenceFile()
	{
		
		if(viewRootMap.containsKey(CONTEXT) && viewRootMap.get(CONTEXT)!=null && viewRootMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.UnitSearchContext.AUCTION_UNITS))
		{
			return  true;
		}
		else
			return false;
		
	}
	public boolean getIsContextNewLeaseContract()
	{
		
		if(viewRootMap.containsKey(CONTEXT) && viewRootMap.get(CONTEXT)!=null && viewRootMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.UnitSearchContext.NEW_LEASE_CONTRACT))
		{
			return  true;
		}
		else
			return false;
		
	}
	public boolean getIsContextInspectionInheritenceFile()
	{
		
		if(viewRootMap.containsKey(CONTEXT) && viewRootMap.get(CONTEXT)!=null && viewRootMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.UnitSearchContext.INSPECTION_UNITS))
		{
			return  true;
		}
		else
			return false;
		
	}
	public boolean getIsContextMaintenanceRequest()
	{
		
		if(viewRootMap.containsKey(CONTEXT) && viewRootMap.get(CONTEXT)!=null && viewRootMap.get(CONTEXT).toString().trim().equalsIgnoreCase(WebConstants.UnitSearchContext.MAINTENANCE_REQUEST))
		{
			return  true;
		}
		else
			return false;
		
	}	

	public boolean getIsPageModeSelectOnePopUp()
	{
		isPageModeSelectOnePopUp= false;
		if(viewRootMap.containsKey(VIEW_MODE) && viewRootMap.get(VIEW_MODE)!=null && viewRootMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase(MODE_SELECT_ONE_POPUP))
		{
			isPageModeSelectOnePopUp= true;
		}
		return isPageModeSelectOnePopUp;
		
	}
	public boolean getIsViewModePopUp()
	{
		boolean returnVal = false;
		
		if(viewRootMap.containsKey(VIEW_MODE) && viewRootMap.get(VIEW_MODE)!=null)
		{
			if(getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())		
				returnVal =  true;
		}
		 
		else 
		{
			returnVal =  false;
		}
		
		return returnVal;
		
	}
	public boolean getIsPageModeSelectManyPopUp()
	{
		isPageModeSelectManyPopUp= false;
		if(viewRootMap.containsKey(VIEW_MODE) && viewRootMap.get(VIEW_MODE)!=null && viewRootMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase(MODE_SELECT_MANY_POPUP))
		{
			isPageModeSelectManyPopUp= true;
		}
		return isPageModeSelectManyPopUp;
	}

	   public boolean getIsArabicLocale()
		{
			isArabicLocale = !getIsEnglishLocale();
			return isArabicLocale;
		}
	
	   public boolean getIsEnglishLocale()
		{
			WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
			return localeInfo.getLanguageCode().equalsIgnoreCase("en");
		}
	   
	   @SuppressWarnings( "unchecked" )
	   public String openCollectionProc()
		{
		   InheritanceFileView inheritanceFileView=(InheritanceFileView)dataTable.getRowData();
			
			sessionMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_ID, inheritanceFileView.getInheritanceFileId().toString());
			return "collectionProc";
		}
	   
	////////////////////Sorting/////////////////////////////////////
	private String sortField = null;
	private boolean sortAscending = true;

	private String  FILE_TO_BLOCKED = "FILE_TO_BLOCKED";
	private String  FILE_TO_UNBLOCKED = "FILE_TO_UNBLOCKED";
	// Actions -----------------------------------------------------------------------------------
	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}
	public void sortDataList(ActionEvent event) {
		String sortFieldAttribute = getAttribute(event, "sortField");
		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}
		// Sort results.
		if (sortField != null) {
			Collections.sort(dataList, new ListComparator(sortField,
					sortAscending));
		}
	}
	
	@SuppressWarnings("unchecked")
	public String issuePaymentRequest() {
		String navigateTo = "";
		InheritanceFileView selectedFile = (InheritanceFileView) getDataTable().getRowData();
		if( null!=selectedFile ) {
			getRequestMap().put(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW, selectedFile);
			navigateTo = ISSUE_PAYMENT_REQUEST;
		}
		return navigateTo;
	}
	
	@SuppressWarnings("unchecked")
	public String onBlockingRequest() 
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	
			InheritanceFileView selectedFile = (InheritanceFileView) getDataTable().getRowData();
			if( null!=selectedFile ) {
				getRequestMap().put( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW, selectedFile);
			}
			
		}
		
		catch (Exception exception) 
		{
			logger.LogException( "onBlockingRequest --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		return "";
		}
	
		return "BlockingRequest";
	}
	
	@SuppressWarnings("unchecked")
	public void onRequestsOnInheritanceFile() 
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			InheritanceFileView selectedFile = (InheritanceFileView) getDataTable().getRowData();
			ReportDetail reportDetail = new RequestsOnInheritanceFileTemplateReport(selectedFile);

			exporterMeta.setExportFileName("Requests");
			exporterMeta.setReportDetail(reportDetail);
			
			HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
			request.getSession().setAttribute(ReportConstant.Keys.REPORT_EXPORTER_META, exporterMeta);
			executeJavascript( "openLoadReportPopup('" + ReportConstant.EXPORT_LIST_BASED_EXCEL_REPORT + "');");
		}
		catch (Exception exception) 
		{
			logger.LogException( "onRequestsOnInheritanceFile--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public String onModifyEstateLimitationRequest() 
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			InheritanceFileView selectedFile = (InheritanceFileView) getDataTable().getRowData();
			if( null!=selectedFile ) 
			{
//				sessionMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW, selectedFile);
				sessionMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_ID, selectedFile.getInheritanceFileId() );
			}
		}
		catch (Exception exception) 
		{
			logger.LogException( "onModifyEstateLimitationRequest --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		return "";
		}
		return "ModifyEstateLimitationRequest";
	}
	@SuppressWarnings("unchecked")
	public String onMinorStatusChangeRequest() 
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			InheritanceFileView selectedFile = (InheritanceFileView) getDataTable().getRowData();
			if( null!=selectedFile ) 
			{
				sessionMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_ID, selectedFile.getInheritanceFileId() );
			}
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMinorStatusChangeRequest --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		return "";
		}
		return "MinorStatusChange";
	}
	
	private boolean hasTransferFileError( InheritanceFileView file ) throws Exception
	{
		boolean hasError = false;
		InheritanceFileService fileService = new InheritanceFileService();
		List<Request> unfinishedRequestsList  = fileService.getUnfinishedRequestsAgainstFile(file);		
		
		if( unfinishedRequestsList != null && unfinishedRequestsList.size() > 0  )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty( "inheritanceFile.msg.unfinishedRequestsPresent" ) );
			hasError= true;
			unfinishedRequestsList = null;
			
		}
		else if ( !fileService.areAssetsAvailableInFile(file) )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty( "inheritanceFile.msg.assetsNotPresent" ) );
			hasError= true;
			
		}
		else
		{
			Map<String,List<InheritanceBeneficiary> > map =  fileService.getBeneficiariesWithIssuesForTrasnfer( file );
			if( map != null )
			{
				if( map.containsKey( Constant.InheritanceFile.MINORS_LIST ) )
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty( "inheritanceFile.msg.minorsPresent" ) );
					hasError= true;
				}
				if( map.containsKey( Constant.InheritanceFile.UNDISBURSED_BALANCE_LIST ) )
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty( "inheritanceFile.msg.beneficiaryBalancePresent" ) );
					hasError= true;
				}
			}
			map = null;
		}
		return hasError;
	} 
	@SuppressWarnings("unchecked")
	public String onTransferToEndowmentFile() 
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			
			InheritanceFileView file = (InheritanceFileView) getDataTable().getRowData();
			if( hasTransferFileError( file ) ) return "";
			EndowmentFile endowmentFile = convertFileInTransaction(file);
			if (endowmentFile  != null) 
			{
			 sessionMap.put(Constant.EndowmentFile.ENDOWMENT_FILE , endowmentFile );
			}
			
		}
		
		catch (Exception exception) 
		{
			logger.LogException( "onTransferToEndowmentFile --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		return "";
		}
		return "endowmentFile";
	}
	
	private EndowmentFile convertFileInTransaction( InheritanceFileView file  ) throws Exception 
	{
		EndowmentFile endowmentFile = null;
		EndowmentFilesService endFileService = new EndowmentFilesService();
		try
		{
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			endowmentFile = endFileService.persistEndowmentFileFromInheritanceFile(file.getInheritanceFileId(),getLoggedInUserId() );
			CommonUtil.saveSystemComments(
											WebConstants.InheritanceFile.NOTES_OWNER, 
											"inheritanceFile.msg.fileConvertedToEndowmentFile", 
											file.getInheritanceFileId(), 
											endowmentFile.getFileNumber()
									 	 );
			CommonUtil.saveSystemComments(
											Constant.EndowmentFile.NOTES_OWNER, 
											"endowment.msg.fileCreatedFromInheritanceFile", 
											endowmentFile.getFileId(), 
											file.getFileNumber()
										 ) ;
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
		return endowmentFile;
	}
	@SuppressWarnings("unchecked")
	public String issueNormalPaymentRequest() {
		InheritanceFileView selectedFile = (InheritanceFileView) getDataTable().getRowData();
		String navigateTo = "";
		if( null!=selectedFile ) {
			getRequestMap().put( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW, selectedFile);
			navigateTo = ISSUE_NORMAL_PAYMENT;			
		}		
		return navigateTo;
	}
	
	@SuppressWarnings("unchecked")
	public String issueInvestmentRequest() {
		InheritanceFileView selectedFile = (InheritanceFileView) getDataTable().getRowData();
		String navigateTo = "";
		if( null!=selectedFile ) {
			getRequestMap().put(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW, selectedFile);
			navigateTo = ISSUE_INVESTMENT_REQUEST;
		}
		return navigateTo;
	}
	
	/**
	 * 	The function is registered as an action to the Issue NOL button. The function puts the File Id in the 
	 *  request and forwards the request to the IssueMemsNOL.jsp. Appropriate navigation rules are defined 
	 *  in the faces-config.xml
	 *  @author Farhan
	 *  @return IssueNOL upon success an empty string is returned on failure. 
	 */		
	@SuppressWarnings("unchecked")
	public String issueNOL() {
		InheritanceFileView selectedFile = (InheritanceFileView) getDataTable().getRowData();		
		if( null!=selectedFile) {
			Long fileId = selectedFile.getInheritanceFileId();
			getRequestMap().put(WebConstants.InheritanceFile.INHERITANCE_FILE_ID, fileId.toString());
			return ISSUE_NOL;
		}		
		return "";
	}

	public List<SelectItem> getInheritenceFileStatus() {
		unitStatus.clear();
		 
		try
		 {
//		 if(sessionMap.containsKey(WebConstants.UNIT_STATUS))
//			 loadInheritenceFileStatusList();
		 
		 /* List<DomainDataView> ddl=(ArrayList<DomainDataView>)sessionMap.get(WebConstants.UNIT_STATUS);	 
		  for(int i=0;i<ddl.size();i++)
		  {
			  DomainDataView ddv=ddl.get(i);
		      SelectItem item = new SelectItem(ddv.getDomainDataId().toString(), ddv.getDataDescEn());
		      unitStatus.add(item);
		  }*/
		  
		 }
		 catch(Exception ex)
		 {
			 
		 }
		
		return unitStatus;
		
		
	}

	public void setUnitStatus(List<SelectItem> unitStatus) {
		this.unitStatus = unitStatus;
	}	

	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public String getMODE_SELECT_ONE_POPUP() {
		return MODE_SELECT_ONE_POPUP;
	}

	public void setMODE_SELECT_ONE_POPUP(String mode_select_one_popup) {
		MODE_SELECT_ONE_POPUP = mode_select_one_popup;
	}

	public String getMODE_SELECT_MANY_POPUP() {
		return MODE_SELECT_MANY_POPUP;
	}

	public void setMODE_SELECT_MANY_POPUP(String mode_select_many_popup) {
		MODE_SELECT_MANY_POPUP = mode_select_many_popup;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
	
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	/**
	 * @return the recordSize
	 */
	@SuppressWarnings("unchecked")
	public Integer getRecordSize() {
		Map<String, Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}
	
	
	public HtmlInputText getTextInheritenceFileNumber() {
		return textInheritenceFileNumber;
	}
	public void setTextInheritenceFileNumber(HtmlInputText textInheritenceFileNumber) {
		this.textInheritenceFileNumber = textInheritenceFileNumber;
	}
	public HtmlInputText getTextCreatedOn() {
		return textCreatedOn;
	}
	public void setTextCreatedOn(HtmlInputText textCreatedOn) {
		this.textCreatedOn = textCreatedOn;
	}
	public HtmlInputText getTextCreatedBy() {
		return textCreatedBy;
	}
	
	public HtmlInputText getTextResearcher() {
		return textResearcher;
	}
	public void setTextResearcher(HtmlInputText listResearcher) {
		this.textResearcher = listResearcher;
	}
	public HtmlSelectOneMenu getListInheritenceFileType() {
		return listInheritenceFileType;
	}
	public void setListInheritenceFileType(HtmlSelectOneMenu listInheritenceFileType) {
		this.listInheritenceFileType = listInheritenceFileType;
	}
	public HtmlInputText getTextBenificiary() {
		return textBenificiary;
	}
	public void setTextBenificiary(HtmlInputText textBenificiary) {
		this.textBenificiary = textBenificiary;
	}
	public HtmlInputText getTextFilePerson() {
		return textFilePerson;
	}
	public void setTextFilePerson(HtmlInputText textFilePerson) {
		this.textFilePerson = textFilePerson;
	}
	public HtmlInputText getTextAssetNo() {
		return textAssetNo;
	}
	public void setTextAssetNo(HtmlInputText textAssetNo) {
		this.textAssetNo = textAssetNo;
	}
	public HtmlSelectOneMenu getListAssetType() {
		return listAssetType;
	}
	public void setListAssetType(HtmlSelectOneMenu listAssetType) {
		this.listAssetType = listAssetType;
	}
	public HtmlInputText getTextAssetNameEn() {
		return textAssetNameEn;
	}
	public void setTextAssetNameEn(HtmlInputText textAssetNameEn) {
		this.textAssetNameEn = textAssetNameEn;
	}
	public HtmlInputText getTextAssetNameAr() {
		return textAssetNameAr;
	}
	public void setTextAssetNameAr(HtmlInputText textAssetNameAr) {
		this.textAssetNameAr = textAssetNameAr;
	}
	public HtmlInputText getTextAssetDesc() {
		return textAssetDesc;
	}
	public void setTextAssetDesc(HtmlInputText textAssetDesc) {
		this.textAssetDesc = textAssetDesc;
	}
	public HtmlInputText getTextSponsor() {
		return textSponsor;
	}
	public void setTextSponsor(HtmlInputText textSponsor) {
		this.textSponsor = textSponsor;
	}
	public HtmlInputText getTextGuardian() {
		return textGuardian;
	}
	public void setTextGuardian(HtmlInputText textGuardian) {
		this.textGuardian = textGuardian;
	}
	
	public boolean isSortAscending() {
		return sortAscending;
	}
	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}
	public HtmlSelectOneMenu getListInheritenceFileStatus() {
		return listInheritenceFileStatus;
	}
	public void setListInheritenceFileStatus(
			HtmlSelectOneMenu listInheritenceFileStatus) {
		this.listInheritenceFileStatus = listInheritenceFileStatus;
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	@SuppressWarnings( "unchecked" )
	public List<SelectItem> getCreatedByList() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
		.getAttributes();
		createdByList = (List<SelectItem>) viewMap.get("createdByList");
		if (createdByList == null)
			createdByList = new ArrayList<SelectItem>();
		return createdByList;
	}
	
	@SuppressWarnings("unchecked")
	public void setCreatedByList(List<SelectItem> createdByList) {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot() 
		.getAttributes();
		
		this.createdByList = createdByList;
		if (createdByList!= null)
			viewMap.put("createdByList", this.createdByList);
	}
	public HtmlSelectOneMenu getListCreatedBy() {
		return listCreatedBy;
	}
	public void setListCreatedBy(HtmlSelectOneMenu listCreatedBy) {
		this.listCreatedBy = listCreatedBy;
	}
	public void setTextCreatedBy(HtmlInputText textCreatedBy) {
		this.textCreatedBy = textCreatedBy;
	}
	public HtmlSelectOneMenu getListResearchedBy() {
		return listResearchedBy;
	}
	public void setListResearchedBy(HtmlSelectOneMenu listResearchedBy) {
		this.listResearchedBy = listResearchedBy;
	}
	/**
	 * @author Kamran Ahmed
	 * @return String
	 * Desc:This method will enable open file screen, so user can create new inheritanc efile.
	 */
	public String createFile()
	{
		return "OPEN_INHERITANCE_FILE";
	}
	
	
	public String createFamilVillageFile()
	{
		return "OPEN_FAMILY_VILLAGE_FILE";
	}
	@SuppressWarnings("unchecked")
	public String onEditFamilyVillageFileType()
	{
		InheritanceFileView selectedFile = null;
		selectedFile = (InheritanceFileView) dataTable.getRowData();
		
		sessionMap.put(WebConstants.InheritanceFile.FILE_ID, selectedFile.getInheritanceFileId());
		return "EDIT_FAMILY_VILLAGE_FILE";
	}
	/**
	 * @author Kamran Ahmed
	 * @return String
	 * Desc:This method will enable open file screen, so user can Edit selected inheritanc efile.
	 */
	@SuppressWarnings("unchecked")
	public String editFile()
	{
		InheritanceFileView selectedFile = null;
		selectedFile = (InheritanceFileView) dataTable.getRowData();
		
		
		if(
		   selectedFile.getFileTypeId() != null && 
		   selectedFile.getFileTypeId().equals(
				   								String.valueOf( Constant.InheritanceFileType.FAMILY_VILLAGE_ID) 
				   							  ) 
		  )
		{
			sessionMap.put(WebConstants.InheritanceFile.FILE_ID, selectedFile.getInheritanceFileId());
			return "EDIT_FAMILY_VILLAGE_FILE";	
		}
		else
		{
			sessionMap.put(WebConstants.PAGE_MODE, WebConstants.PAGE_MODE_EDIT);
			if(selectedFile != null && selectedFile.getInheritanceFileId() != null)
			sessionMap.put(WebConstants.InheritanceFile.FILE_ID, selectedFile.getInheritanceFileId());
			return "EDIT_INHERITANCE_FILE";
		}
	}
	public HtmlCalendar getClndrCreatedOnTo() {
		return clndrCreatedOnTo;
	}
	public void setClndrCreatedOnTo(HtmlCalendar clndrCreatedOnTo) {
		this.clndrCreatedOnTo = clndrCreatedOnTo;
	}
	public HtmlCalendar getClndrCreatedOnFrom() {
		return clndrCreatedOnFrom;
	}
	public void setClndrCreatedOnFrom(HtmlCalendar clndrCreatedOnFrom) {
		this.clndrCreatedOnFrom = clndrCreatedOnFrom;
	}
	public HtmlInputText getTextNurse() {
		return textNurse;
	}
	public void setTextNurse(HtmlInputText textNurse) {
		this.textNurse = textNurse;
	}
	
	@SuppressWarnings("unchecked")
	public String onMinorMaintenanceRequest()
	{
		InheritanceFileView selectedFile = (InheritanceFileView) dataTable.getRowData();
		sessionMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW , selectedFile );
		return "MINOR_MAINTENANCE";
	}
	@SuppressWarnings("unchecked")
	public String onCreateTakharujRequest()
	{
		InheritanceFileView selectedFile = (InheritanceFileView) dataTable.getRowData();
		sessionMap.put( WebConstants.Takharuj.INHERITANCE_FILE_VIEW , selectedFile );
		return "TAKHARUJ_MANAGE";
	}
	@SuppressWarnings("unchecked")
	public void openFileBlockingComments()
	{
		sessionMap.put(FILE_TO_BLOCKED ,(InheritanceFileView) dataTable.getRowData());
		
		String javaScriptText = " var screen_width = 800;"
				+ "var screen_height = 250;"
				+ "var popup_width = screen_width-200;"
				+ "var popup_height = screen_height;"
				+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
				+ "window.open('FileBlockingComments.jsf?pageMode=MODE_SELECT_MANY_POPUP"
				+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";

		executeJavascript(javaScriptText);
	}
	public void blockFile()
	{
		if (sessionMap.get(FILE_TO_BLOCKED)!=null)
		{
			DomainDataView blockedDD = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.InheritanceFileStatus.INH_FILE_STATUS), WebConstants.InheritanceFileStatus.BLOCKED);
			InheritanceFileView fileToBlock = (InheritanceFileView) sessionMap.get(FILE_TO_BLOCKED);
			
			sessionMap.remove(FILE_TO_BLOCKED);
			long oldStatus = fileToBlock.getStatusId(); 
			fileToBlock.setOldStatus(oldStatus);
			fileToBlock.setStatusId(blockedDD.getDomainDataId());
			fileToBlock.setStatusEn(blockedDD.getDataDescEn());
			fileToBlock.setStatusAr(blockedDD.getDataDescAr());
		
			try 
			{
				new InheritanceFileService().blockFile(fileToBlock);
				loadDataList();//to refresh the list
			
				saveSystemCommentsAgainstFile(MessageConstants.RequestEvents.FILE_BLOCKED,fileToBlock.getInheritanceFileId());
				saveNotes(WebConstants.InheritanceFile.NOTES_OWNER, fileToBlock.getInheritanceFileId());
				successMessages.add(CommonUtil.getBundleMessage("systemNotes.fileBlocked"));
			} 
			catch (Exception e) 
			{
				errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		}
		else if(sessionMap.get(FILE_TO_UNBLOCKED)!=null)
		{
			unBlockFile();
		}
	}
	public void printFileDetailsReport()
	{
		InheritanceFileView file = (InheritanceFileView) dataTable.getRowData();
		if(file.getInheritanceFileId()!= null)
		{
//			if(isEnglishLocale())
//    	  		reportCriteria= new FileDetailsReportCriteria(ReportConstant.Report.FILE_DETAILS_AR,ReportConstant.Processor.FILE_DEETAILS,CommonUtil.getLoggedInUser());
//    	  	else
    	  		reportCriteria= new FileDetailsReportCriteria(ReportConstant.Report.FILE_DETAILS_AR,ReportConstant.Processor.FILE_DEETAILS,CommonUtil.getLoggedInUser());
			
			reportCriteria.setFileId(file.getInheritanceFileId());
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
			executeJavascript("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	public void printInheritanceDetailsReport()
	{
		InheritanceFileView file = (InheritanceFileView) dataTable.getRowData();
		if(file.getInheritanceFileId()!= null)
		{
			inheritanceDetailsCriteria= new InheritanceDetailsReportCriteria(ReportConstant.Report.INHERITANCE_DETAILS_AR,ReportConstant.Processor.INHERITANCE_DETAILS,CommonUtil.getLoggedInUser());
			inheritanceDetailsCriteria.setFileId(file.getInheritanceFileId());
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, inheritanceDetailsCriteria);
			executeJavascript("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
	}
	public void openStatmntOfAccount()
	{
		InheritanceFileView file = (InheritanceFileView) dataTable.getRowData();
		if(file.getFilePersonId() != null && file.getFilePersonId().trim().length() > 0)
		{
			statmntReportCriteria= new StatementOfAccountCriteria(ReportConstant.Report.STATEMENT_OF_ACCOUNT,ReportConstant.Processor.STATEMENT_OF_ACCOUNT,CommonUtil	.getLoggedInUser());
			statmntReportCriteria.setPersonId(new Long(file.getFilePersonId()));
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, statmntReportCriteria);
			executeJavascript("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
	}
	
	public void openStatmntOfAccountNew()
	{
		InheritanceFileView file = (InheritanceFileView) dataTable.getRowData();
		if(file.getFilePersonId() != null && file.getFilePersonId().trim().length() > 0)
		{
			statmntReportCriteria= new StatementOfAccountCriteria(ReportConstant.Report.STATEMENT_OF_ACCOUNT_NEW,ReportConstant.Processor.STATEMENT_OF_ACCOUNT_NEW,CommonUtil	.getLoggedInUser());
			statmntReportCriteria.setPersonId( new Long (file.getFilePersonId() ) );
			statmntReportCriteria.setInheritancefileId( file.getInheritanceFileId() );
			if( file.getCostCenter() != null )
			{
				statmntReportCriteria.getCostCenters().add( file.getCostCenter() );
			}
			if( file.getOldCostCenter() != null )
			{
				statmntReportCriteria.getCostCenters().add( file.getOldCostCenter() );
			}
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, statmntReportCriteria);
			executeJavascript("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
	}
	private void saveSystemCommentsAgainstFile(String sysNote, long fileId) throws Exception 
	{
		CommonUtil.saveSystemComments(noteOwner, sysNote,fileId);
	}
	
	public void unBlockFile()
	{
		
		if(sessionMap.get(FILE_TO_UNBLOCKED)!=null)
		{
//		InheritanceFileView fileToUnBlock = (InheritanceFileView) dataTable.getRowData();
		InheritanceFileView fileToUnBlock= (InheritanceFileView) sessionMap.get(FILE_TO_UNBLOCKED);
		sessionMap.remove(FILE_TO_UNBLOCKED);
		
		try 
		{
			new InheritanceFileService().unBlockFile(fileToUnBlock);
			loadDataList();//to refresh the list
			saveSystemCommentsAgainstFile(MessageConstants.RequestEvents.FILE_UNBLOCKED,fileToUnBlock.getInheritanceFileId());
			saveNotes(WebConstants.InheritanceFile.NOTES_OWNER, fileToUnBlock.getInheritanceFileId());
			successMessages.add(CommonUtil.getBundleMessage("systemNotes.fileUnBlocked"));
		} 
		catch (Exception e) 
		{
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	}
	
	public  void saveNotes(String notesOwner, Long entityId) throws PimsBusinessException {
		
		if (sessionMap.get(BLOCKING_FILE_COMMENTS)!=null ||  sessionMap.get(UNBLOCKING_FILE_COMMENTS)!=null)
		{
		
		try
		{	
				String blockingComments = sessionMap.get(BLOCKING_FILE_COMMENTS)!=null ? sessionMap.get(BLOCKING_FILE_COMMENTS).toString():null;
				String unblockingComments = sessionMap.get(UNBLOCKING_FILE_COMMENTS)!=null ? sessionMap.get(UNBLOCKING_FILE_COMMENTS).toString():null;
				String comments = blockingComments!=null?blockingComments:unblockingComments;;
				
//				String comments = sessionMap.get(BLOCKING_FILE_COMMENTS).toString();
				PropertyServiceAgent propertyServiceAgent = new PropertyServiceAgent();
				NotesServiceAgent notesServiceAgent = new NotesServiceAgent();
				NotesVO newnote = new NotesVO();
				newnote.setNoteowner(notesOwner);
				newnote.setNoteTypeId(propertyServiceAgent.getDomainDataByValue(NOTE_TYPE,USER_NOTE_TYPE ).getDomainDataId());
				newnote.setEntityId(entityId);
				newnote.setNote(comments);
				newnote.setDeleted(false);
				newnote.setRecordStatus(1);
				newnote.setCreatedBy( CommonUtil.getLoggedInUser());
				newnote.setUpdatedBy( CommonUtil.getLoggedInUser() );
				newnote.setCreatedOn(new Date());
				newnote.setUpdatedOn(new Date());
//				newnote.setCreatedByFullName( note.getCreatedByFullName() );
//				newnote.setCreatedByFullNameAr(   note.getCreatedByFullNameAr()  );
//				newnote.setUpdatedByFullName(  user.getFullName());
//				newnote.setUpdatedByFullNameAr(  user.getSecondaryFullName() );
//				note.setNote(newnote);
				notesServiceAgent.addNote(newnote);
				sessionMap.remove(BLOCKING_FILE_COMMENTS);
				sessionMap.remove(UNBLOCKING_FILE_COMMENTS);
		
	}
	catch (Exception e) {
            logger.LogException("saveNotes|Error Occured", e);
//            throw new PimsBusinessException(e);
	}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public void openFileUnblockingComments()
	{
		sessionMap.put(FILE_TO_UNBLOCKED ,(InheritanceFileView) dataTable.getRowData());
		
		String javaScriptText = " var screen_width = 800;"
				+ "var screen_height = 250;"
				+ "var popup_width = screen_width-200;"
				+ "var popup_height = screen_height;"
				+ "var leftPos = (screen_width-popup_width)/2, topPos = (screen_height-popup_height)/2 - 20;"
				+ "window.open('FileBlockingComments.jsf?pageMode=MODE_SELECT_MANY_POPUP"
				+ "','_blank','width='+(popup_width )+',height='+(popup_height)+',left='+leftPos+',top='+topPos+',scrollbars=yes,status=yes');";

		executeJavascript(javaScriptText);
	}
	public HtmlInputText getTextNewCC() {
		return textNewCC;
	}
	public void setTextNewCC(HtmlInputText textNewCC) {
		this.textNewCC = textNewCC;
	}
	public HtmlInputText getTextOldCC() {
		return textOldCC;
	}
	public void setTextOldCC(HtmlInputText textOldCC) {
		this.textOldCC = textOldCC;
	}
	public HtmlSelectOneMenu getMigrated() {
		return migrated;
	}
	public void setMigrated(HtmlSelectOneMenu migrated) {
		this.migrated = migrated;
	}
	public HtmlInputText getTextBenCC() {
		return textBenCC;
	}
	public void setTextBenCC(HtmlInputText textBenCC) {
		this.textBenCC = textBenCC;
	}
}
