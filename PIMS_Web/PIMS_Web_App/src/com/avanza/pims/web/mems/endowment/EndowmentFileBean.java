	package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.bpel.proxy.MEMSEndowmentEvaluationBPELPortClient;
import com.avanza.pims.bpel.proxy.MEMSEndowmentRegistrationPortClient;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.AssetType;
import com.avanza.pims.entity.EndFileAsso;
import com.avanza.pims.entity.EndowmentFile;
import com.avanza.pims.entity.Person;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationDetails;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.plugins.EndowmentDetailsTabBacking;
import com.avanza.pims.web.mems.plugins.endowmentsOfficialCorrespondenceTabBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.endowment.EndowmentFilesService;
import com.avanza.pims.ws.mems.endowment.EndowmentService;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.EndowmentFileVO;
import com.avanza.pims.ws.vo.mems.OfficialCorrespondenceView;
import com.avanza.ui.util.ResourceUtil;


public class EndowmentFileBean extends AbstractMemsBean{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(EndowmentFileBean.class);
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_REG_REQ = "REG_REQ";
	private static final String PAGE_MODE_EVALUATION_REQ = "EVALUATION_REQ";
	private static final String PAGE_MODE_COMPLETE = "PAGE_MODE_COMPLETE";
	private static final String PAGE_MODE_APPROVAL_REQ = "APPROVAL_REQ";
	private static final String PAGE_MODE_APPROVED = "APPROVED";
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_ENDOWMENT= "endowmentTab";
	private static final String TAB_REGISTRATION= "registrationTab";
	
	private static final String EXTERNAL_ID ="externalId";
	
    private String pageTitle;
    private String pageMode;
    private String hdnPersonId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;
    
    private String hdnFilePersonId;
    private String hdnFilePersonName;
    
    private EndowmentFilesService endowmentFileService = new EndowmentFilesService();	
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	HtmlCommandButton btnAddEndowment = new HtmlCommandButton();
	HtmlCommandButton btnAddEndowmentAfterCompletion = new HtmlCommandButton();
	private EndowmentFileVO endowmentFileVO; 
	private EndowmentFile endowmentFile;
	private RequestView requestView;
	org.richfaces.component.html.HtmlTab tabEndowments =new org.richfaces.component.html.HtmlTab();
	public EndowmentFileBean(){}
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
				if (sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY) != null )
				{
					boolean isPaymentCollected = (Boolean) (sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY));
					if(isPaymentCollected)
				    {
					  onMessageFromReceivePayments();
				    }
				}
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.PROCEDURE_ENDOWMENT_FILE );
		viewMap.put(EXTERNAL_ID ,WebConstants.Attachment.EXTERNAL_ID_ENDOWMENT_FILE);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		loadAttachmentsAndComments( null );
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
		  getDataFromTaskList();
		}
		else if ( sessionMap.get( Constant.EndowmentFile.ENDOWMENT_FILE ) != null )
		{
		  getDataFromSearch();
		}
		else if ( getRequestMap().get( WebConstants.REQUEST_VIEW) != null )
		{
			requestView = (RequestView) getRequestMap().remove( WebConstants.REQUEST_VIEW);
			getDataFromRequestSearch();
		}
		//On back button of portfolio and property screen or through javascript when opened as popup
		else if ( sessionMap.get( Constant.EndowmentFile.ENDOWMENT_FILE_ID ) != null )
		{
			  getEndowmentFileDetails(
					  					new Long ( sessionMap.remove( Constant.EndowmentFile.ENDOWMENT_FILE_ID ).toString() )
			  						  );
		}
		else
		{
			setDataForFirstTime();
		}
		updateValuesFromMap();
		getPageModeFromFileStatus();
	}
	
	@SuppressWarnings( "unchecked" )
	public void prerender()
	{
		try
		{
			getErrorMessagesFromTab();
			tabEndowments.setRendered(   hdnFilePersonId != null && hdnFilePersonId.trim().length()> 0 );
			btnAddEndowment.setRendered( getShowAddEndowmentButton() );
			btnAddEndowmentAfterCompletion.setRendered(getShowAddEndowmentButtonAfterCompletion());
		}
		catch ( Exception e )
		{
			logger.LogException( "prerender|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		endowmentFileVO = new EndowmentFileVO();
		endowmentFileVO.getEndowmentFile().setCreatedBy(  getLoggedInUserId()  );
		endowmentFileVO.getEndowmentFile().setUpdatedBy(  getLoggedInUserId()  );
		
		endowmentFileVO.getRequestView().setCreatedBy( getLoggedInUserId() ) ;
		endowmentFileVO.getRequestView().setUpdatedBy( getLoggedInUserId() );
		endowmentFileVO.getRequestView().setIsDeleted(Constant.DEFAULT_IS_DELETED);
		endowmentFileVO.getRequestView().setRecordStatus(Constant.DEFAULT_RECORD_STATUS);
		endowmentFileVO.getRequestView().setStatusId( 9001l );  
		endowmentFileVO.getRequestView().setRequestTypeId(WebConstants.MemsRequestType.ENDOWMENT_FILE);
		endowmentFileVO.getRequestView().setRequestDate(new Date());
		tabEndowments.setRendered(true);
	}
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromFileStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		if( this.endowmentFile == null  || 
			this.endowmentFile.getFileId() == null ||
			this.endowmentFile.getStatusId() == null
		  ) 
		{ return; }
		
		if( getStatusRegistrationReq() )
		{
			//onOfficialCorrespondenceTab();
			onEndowmentTab();
			this.setPageMode( PAGE_MODE_REG_REQ );
			
		}
		else if( getStatusApprovalReq() )
		{
			onEndowmentTab();
			this.setPageMode( PAGE_MODE_APPROVAL_REQ );
		}
		else if( getStatusEvaluationRequired() || getStatusApproved() )
		{
			onEndowmentTab();
			this.setPageMode( PAGE_MODE_EVALUATION_REQ);
		}
		else if( 
				//this.endowmentFile.getStatusId().compareTo(  WebConstants.SocialProgramStatus.REJECT ) == 0 ||
				getStatusCompleted()
		       ) 
		{
			onEndowmentTab();
			this.setPageMode( PAGE_MODE_VIEW );
		}
	}
	public boolean getStatusNew() {
		return this.endowmentFile.getStatusId() != null && this.endowmentFile.getStatusId().compareTo(  Constant.EndowmentFileStatus.NEW_ID ) == 0;
	}

	public boolean getStatusCompleted() {
		return this.endowmentFile.getStatusId() != null &&  this.endowmentFile.getStatusId().compareTo(  Constant.EndowmentFileStatus.COMPLETED_ID ) == 0;
	}

	public boolean getStatusEvaluationRequired() {
		return this.endowmentFile.getStatusId() != null && this.endowmentFile.getStatusId().compareTo( Constant.EndowmentFileStatus.EVALUATION_REQUIRED_ID ) == 0;
	}

	public boolean getStatusRegistrationReq() {
		return this.endowmentFile.getStatusId() != null && this.endowmentFile.getStatusId().compareTo(  Constant.EndowmentFileStatus.REGISTRATION_REQUIRED_ID ) == 0;
	}
	
	public boolean getStatusApprovalReq() {
		return this.endowmentFile.getStatusId() != null && this.endowmentFile.getStatusId().compareTo(  Constant.EndowmentFileStatus.APPROVAL_REQUIRED_ID ) == 0;
	}
	
	public boolean getStatusApproved() {
		return this.endowmentFile.getStatusId() != null && this.endowmentFile.getStatusId().compareTo(  Constant.EndowmentFileStatus.APPROVED_ID ) == 0;
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( Constant.EndowmentFile.ENDOWMENT_FILE_VO ) != null )
		{
			endowmentFileVO  = ( EndowmentFileVO )viewMap.get( Constant.EndowmentFile.ENDOWMENT_FILE_VO ) ;
			endowmentFile	 = endowmentFileVO.getEndowmentFile();
			requestView      = endowmentFileVO.getRequestView();
			if( viewMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION) != null )
			{
			
				requestView.setDescription(
						                       viewMap.get(WebConstants.ApplicationDetails.APPLICATION_DESCRIPTION).toString().trim() 
						                   );
			}
		}
		
		 
		 updateValuesToMap();

	}
	
	@SuppressWarnings("unchecked")
	private void getErrorMessagesFromTab() throws Exception
	{
		if(viewMap.get(Constant.EndowmentFile.ERR_REG_TAB) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(Constant.EndowmentFile.ERR_REG_TAB);
			errorMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_REGISTRATION);
		}
		else if(viewMap.get(Constant.EndowmentFile.SUCCESS_REG_TAB) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(Constant.EndowmentFile.SUCCESS_REG_TAB);
			successMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_REGISTRATION);
		}
		else if(viewMap.get(Constant.EndowmentFile.ERR_END_TAB) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(Constant.EndowmentFile.ERR_END_TAB);
			errorMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_ENDOWMENT);
		}
		else if(viewMap.get(Constant.EndowmentFile.SUCCESS_END_TAB) != null)
		{
			List<String> errMsg =(List<String>) viewMap.remove(Constant.EndowmentFile.SUCCESS_END_TAB);
			successMessages.addAll(errMsg);
			tabPanel.setSelectedTab(TAB_REGISTRATION);
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( endowmentFileVO != null )
		{
		  viewMap.put( Constant.EndowmentFile.ENDOWMENT_FILE_VO , endowmentFileVO );
		  requestView = endowmentFileVO.getRequestView();
		  endowmentFile  = endowmentFileVO.getEndowmentFile();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void getDataFromRequestSearch()throws Exception
	{
		
		if( requestView !=null  && requestView.getEndowmentFileId() != null)
		{
		  getEndowmentFileDetails( requestView.getEndowmentFileId() );
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		endowmentFile = ( EndowmentFile )sessionMap.remove( Constant.EndowmentFile.ENDOWMENT_FILE );
		if( this.endowmentFile !=null )
		{
		  getEndowmentFileDetails( endowmentFile.getFileId() );
		}
	}
	
	
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  UserTask userTask = ( UserTask )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  setUserTask(userTask);
	  if( userTask.getTaskAttributes().get( Constant.EndowmentFile.ENDOWMENT_FILE_ID ) == null ){ return; }
	  long  id = new Long  ( userTask.getTaskAttributes().get( Constant.EndowmentFile.ENDOWMENT_FILE_ID ) );
	  getEndowmentFileDetails( id );
	  //In case task is for evaluation then we will get the asset type id from user task to only show endowments corresponding
	  //to that type in endowmet tab
	  
	  if( userTask.getTaskAttributes().get( Constant.EndowmentFile.EVALUATE_ENDOWMENT_FOR_ASSET_TYPE_ID ) != null )
	  { 
	   endowmentFileVO.setEvaluateEndowmentForAssetTypeId(  
			  											   new Long (
			  													     userTask.getTaskAttributes().get( Constant.EndowmentFile.EVALUATE_ENDOWMENT_FOR_ASSET_TYPE_ID ) 
			  													     )
	  													  );
	  }
    }
	
	@SuppressWarnings( "unchecked" )
	private void getEndowmentFileDetails( long id ) throws Exception 
	{
		endowmentFileVO = endowmentFileService.getEndowmentFileById( id );
		updateValuesToMap();
		loadAttachmentsAndComments( id );
		populateApplicationDetailsTab();
		populateFilePersonInfo();
	}

	@SuppressWarnings(  "unchecked"  )
	private void populateApplicationDetailsTab()throws Exception
	{
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		String status = "";
		boolean isRequestNull =  null == requestView || null == requestView.getApplicantView() ||null == requestView.getRequestId();
		boolean isApplicantDiffFromPerson = isRequestNull || 
											!requestView.getApplicantView().getPersonId().toString().equals(hdnPersonId);
		if ( isApplicantDiffFromPerson && hdnPersonName!= null && hdnPersonName.trim().length() >0 ) 
		{
			bean.populateApplicationDetails(
						                    "", 
						                    "", 
						                    new Date(), 
						                    "", 
						                    hdnPersonName, 
						                    "", 
						                    hdnCellNo, 
						                    ""
						                  );
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, hdnPersonId);
			PersonView person = new PersonView();
			person.setPersonId( new Long( hdnPersonId ) );
			requestView.setApplicantView( person);
			
            return;
		}
		if( isRequestNull )return;
		
		status = CommonUtil.getIsEnglishLocale()?requestView.getStatusEn():requestView.getStatusAr();
		bean.populateApplicationDetails(
				                         requestView.getRequestNumber(), 
					                     status, 
					                     requestView.getRequestDate(), 
					                     requestView.getDescription(), 
					                     requestView.getApplicantView().getPersonFullName(), 
					                     "", 
					                     requestView.getApplicantView().getCellNumber(), 
					                     requestView.getApplicantView().getEmail()
					                   );
		if( requestView.getApplicantView() != null && requestView.getApplicantView().getPersonId() != null  )
		{
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,requestView.getApplicantView().getPersonId() );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, requestView.getApplicantView().getPersonId() );
		}
		if ( endowmentFile != null && endowmentFile.getStatus() != null && 
			!endowmentFile.getStatus().getDataValue().equals(Constant.EndowmentFileStatus.NEW ) 
		    )
		{
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_ENDOWMENT_FILE);
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
    	viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.Attachment.EXTERNAL_ID_ENDOWMENT_FILE);
		viewMap.put("noteowner", Constant.EndowmentFile.NOTES_OWNER);
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(endowmentFile.getFileId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = Constant.EndowmentFile.NOTES_OWNER;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, endowmentFile.getFileId() );
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
	    	String notesOwner = Constant.EndowmentFile.NOTES_OWNER;
	    	//if(this.txtRemarks!=null && this.txtRemarks.length()>0)
	    	  //CommonUtil.saveRemarksAsComments(referenceId, txtRemarks, notesOwner) ;
	    	NotesController.saveNotes(notesOwner, endowmentFile.getFileId());
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }
	
	

	@SuppressWarnings("unchecked")
	public void onShowInheritanceFilePopup() {
		try	 
		{	
			updateValuesFromMap();		
			sessionMap.put( WebConstants.InheritanceFile.FILE_ID, endowmentFile.getInheritanceFile().getInheritanceFileId());
		    sessionMap.put( WebConstants.InheritanceFilePageMode.IS_POPUP, true);
			String javaScriptText ="var screen_width = 900;"+
            	"var screen_height =600;"+
            	"window.open('inheritanceFile.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=40,scrollbars=yes,status=yes,resizable=true');";
		    executeJavaScript( javaScriptText);		    		    
		}
		catch (Exception ex)  {
			logger.LogException("[Exception occured in showInheritanceFilePopup()]", ex);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onMessageFromEvaluateEndowmentPopup()
	{
		try	
		{	
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			updateValuesFromMap();	
			
			if(	sessionMap.get(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO ) == null){return;}
			
			EndFileAsso obj = (EndFileAsso)sessionMap.remove( Constant.EndowmentFile.ENDOWMENT_FILE_ASSO );
			obj.setEndowmentFile(endowmentFile);
			String msg = java.text.MessageFormat.format( 
					 									ResourceUtil.getInstance().getProperty("endowmentFile.msg.endowmentEvaluated"),
					 									obj.getEndowment().getEndowmentNum()
					 									);
			successMessages.add( msg );
			updateValuesToMap();
			onEndowmentTab();
			tabPanel.setSelectedTab( TAB_ENDOWMENT );
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromEvaluateEndowmentPopup --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onMessageFromAddEndowmentPopup()
	{
		try	
		{	
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			updateValuesFromMap();	
			
			if(	sessionMap.get(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO ) == null){return;}
			
			EndFileAsso obj = (EndFileAsso)sessionMap.remove( Constant.EndowmentFile.ENDOWMENT_FILE_ASSO );
			obj.setEndowmentFile(endowmentFile);
			//if( endowmentFile.getFileId() == null && hasSaveErrors() )return; 
			
			int flag = saveEndFileAsso(obj);
			if( flag == 1 )
			{
				successMessages.add( ResourceUtil.getInstance().getProperty("endowmentFile.msg.savedSuccessfully"));
			}
			else
			{
				successMessages.add( ResourceUtil.getInstance().getProperty("endowmentFile.msg.endowmentAdded"));
			}
			updateValuesToMap();
			onEndowmentTab();
			tabPanel.setSelectedTab( TAB_ENDOWMENT );
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromAddEndowmentPopup --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	private boolean hasEndowmentDeletionError() throws Exception
	{
		boolean hasEndowmentDeletionError=false;
		errorMessages = new ArrayList<String>();
		if( endowmentFile.getEndFileAssos() .size()== 1)
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.endowmentDeleteError")  );
    		return true;
    	}
		
		return hasEndowmentDeletionError;
		
	}
	@SuppressWarnings( "unchecked" )
	public boolean deleteEndowment( EndFileAsso obj ) throws Exception
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		if( hasEndowmentDeletionError()) return false;
		updateValuesFromMap();	
		obj.setIsDeleted("1");
		saveEndFileAsso(obj);
		successMessages.add( ResourceUtil.getInstance().getProperty("endowment.msg.endowmentDeleteSucces"));
		
		updateValuesToMap();
		onEndowmentTab();
		tabPanel.setSelectedTab( TAB_ENDOWMENT );
		return true;
	}

	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromEndowmentFileBen()
	{
		try	
		{	
			errorMessages = new ArrayList<String>();
			successMessages = new ArrayList<String>();
			updateValuesFromMap();	
			
			if(	sessionMap.get(Constant.EndowmentFile.ENDOWMENT_FILE_ASSO ) == null){return;}
			
			EndFileAsso obj = (EndFileAsso)sessionMap.remove( Constant.EndowmentFile.ENDOWMENT_FILE_ASSO );
			obj.setEndowmentFile(endowmentFile);
			int flag = saveEndFileAsso(obj);
			String message = java.text.MessageFormat.format(
							                                 ResourceUtil.getInstance().getProperty("endowmentFile.msg.beneficiaryDetailsSaved"),
							                                 obj.getEndowment().getEndowmentNum()
							                                );
			successMessages.add( message );
			updateValuesToMap();
			onEndowmentTab();
			tabPanel.setSelectedTab( TAB_ENDOWMENT );
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromEndowmentFileBen--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}


	private int saveEndFileAsso(EndFileAsso obj) throws Exception 
	{
		int flag = 0;
		try
		{
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			if( endowmentFile.getFileId() == null )
			{
				Set<EndFileAsso> endFileAssos = new HashSet<EndFileAsso>();
				obj.setUpdatedBy( getLoggedInUserId() );
				endFileAssos.add( obj );
				endowmentFile.setEndFileAssos( endFileAssos );
				persistEndowmentFile( Constant.EndowmentFileStatus.NEW );
				saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
				flag =1;
			}
			else
			{
				endowmentFileService.persistEndFileAsso(obj);
			}
			ApplicationContext.getContext().getTxnContext().commit();
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
		return flag;
	}

	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMap();
			if(hdnPersonType.equals( "APPLICANT" ) )
			{
				populateApplicationDetailsTab();
				if( hdnFilePersonId == null || hdnFilePersonId.trim().length()<= 0 )
				{
					hdnFilePersonId   = hdnPersonId ;
					hdnFilePersonName = hdnPersonName ;
					populateFilePersonInfo();
				}
			}
			if(hdnPersonType.equals( "FILE_PERSON" ) )
			{
				populateFilePersonInfo();
				if( hdnPersonId == null || hdnPersonId.trim().length()<= 0 )
				{
					hdnPersonId = hdnFilePersonId;
					hdnPersonName = hdnFilePersonName;
					populateApplicationDetailsTab();
				}
				tabEndowments.setRendered(true);
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

		
	}
	@SuppressWarnings( "unchecked" )
	private void populateFilePersonInfo()
	{
		if( hdnPersonId != null  )
		{
			Person person = new Person();
			person.setPersonId( new Long( hdnFilePersonId ) );
			endowmentFile.setPerson(person);
		}
		else if ( endowmentFile.getPerson() != null && endowmentFile.getPerson().getPersonId() != null )
		{
			hdnFilePersonId   = endowmentFile.getPerson().getPersonId().toString();
			hdnFilePersonName = endowmentFile.getPerson().getFullName();
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( endowmentFile!= null &&  endowmentFile.getFileId() != null)
		 {
			 loadAttachmentsAndComments( endowmentFile.getFileId());
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	
	@SuppressWarnings( "unchecked" )
	public void onEndowmentTab()
	{
		try	
		{

			updateValuesFromMap();
			
			if( endowmentFile == null || endowmentFile.getFileId()== null ){return;}

			
			EndowmentDetailsTabBacking bean = (EndowmentDetailsTabBacking)getBean("pages$tabEndowmentDetails");
            bean.populateTab( endowmentFileVO.getEndowmentFile() ,endowmentFileVO.getEvaluateEndowmentForAssetTypeId() );
            tabPanel.setSelectedTab( TAB_ENDOWMENT );
		}
		catch(Exception ex)
		{
			logger.LogException("onEndowmentTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	
	@SuppressWarnings( "unchecked" )
	public void onOfficialCorrespondenceTab()
	{
		try	
		{

			updateValuesFromMap();
			if( endowmentFile == null || endowmentFile.getFileId()== null ){return;}
			endowmentsOfficialCorrespondenceTabBean bean = (endowmentsOfficialCorrespondenceTabBean)getBean("pages$endowmentsOfficialCorrespondenceTab");
            bean.populateTab( endowmentFileVO.getEndowmentFile() );
            tabPanel.setSelectedTab(TAB_REGISTRATION);
		}
		catch(Exception ex)
		{
			logger.LogException("onOfficialCorrespondenceTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	

	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if(endowmentFile == null || endowmentFile.getFileId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(Constant.EndowmentFile.NOTES_OWNER, endowmentFile.getFileId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	
	@SuppressWarnings( "unchecked" )
	public void onApplicationTab()
	{
		try	
		{	
			//ProgramSponsorsBean bean =  (ProgramSponsorsBean)getBean( "pages$programSponsors");
//			if ( this.endowmentFile!= null && this.endowmentFile.getSocialProgramId()!= null )
//			{
//			  
//				bean.onLoadList( this.endowmentFile.getSocialProgramId() );
//			}
//			else
//			{
//				bean.setListLoadedFromDb( "1" );
//			}
		}
		catch (Exception exception) 
		{
			logger.LogException( " onApplicationTab--- CRASHED --- ", exception);
		}
		
	}
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if( hasBasicInfoError( ) )
		{
			tabPanel.setSelectedTab(TAB_ENDOWMENT);
			return true;
		}
		if( endowmentFile.getInheritanceFile() != null )
		{
			for (EndFileAsso endowmentFileAsso  : endowmentFile.getEndFileAssos() )
			{
				if( endowmentFileAsso.getEndowment().getCostCenter() == null ||  endowmentFileAsso.getEndowment().getCostCenter().trim().length()<=0  )
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentFile.msg.costcenterrequired"));
					return true;
				}
				if( endowmentFileAsso.getEndowmentCategory() == null || endowmentFileAsso.getEndowmentCategory().getCategoryId().compareTo( -1l ) == 0  )
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.categoryRequired"));
					return true;
				}
				if( endowmentFileAsso.getEndowmentPurpose() != null && endowmentFileAsso.getEndowmentPurpose().getPurposeId().compareTo( -1l ) == 0   )
				{
					errorMessages.add(ResourceUtil.getInstance().getProperty("endowment.msg.purposeRequired"));
					return true;
				}
			}
		
		}
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}
		
		return hasSaveErrors;
		
	}
	private boolean hasBasicInfoError( ) throws Exception 
	{
		boolean hasSaveErrors= false;
		if( endowmentFile.getFileName() == null || endowmentFile.getFileName().trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentFile.msg.fileNameRequired"));
			hasSaveErrors = true;
		}
		if( endowmentFile.getPerson() == null || endowmentFile.getPerson().getPersonId() == null )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentFile.msg.fileOwnerRequired"));
			hasSaveErrors = true;
		}
		
		if( endowmentFile.getEndFileAssos() == null || endowmentFile.getEndFileAssos().isEmpty())
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentFile.msg.EndowmentsRequired"));
			hasSaveErrors = true;
		}
		
		return hasSaveErrors;
	}
	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }

			saveFileInTransaction();
			getEndowmentFileDetails( endowmentFile.getFileId() );
			onEndowmentTab();
			successMessages.add( ResourceUtil.getInstance().getProperty("endowmentFile.msg.savedSuccessfully"));
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	
	private void saveFileInTransaction() throws Exception 
	{
		try
		{
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			String status = Constant.EndowmentFileStatus.NEW;
            if(endowmentFile.getStatusId() != null)
            {
            	status = null;
            }
			persistEndowmentFile( status );
			updateValuesToMap();
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
			
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistEndowmentFile(String status) throws Exception 
	{
	
		setStatusIdForFileAndRequest(status);
		endowmentFileService.persist( endowmentFileVO );
	}

	/**
	 * @param status
	 */
	private void setStatusIdForFileAndRequest(String status)throws Exception 
	{
		if( status == null )return;
		if( status.equals( Constant.EndowmentFileStatus.NEW ) )
		{
			 endowmentFile.setStatusId( Constant.EndowmentFileStatus.NEW_ID );
			 endowmentFileVO.getRequestView().setStatusId(9001l);
			 endowmentFileVO.getRequestView().setStatus( "9001" );
		}
		else if( status.equals( Constant.EndowmentFileStatus.REGISTRATION_REQUIRED) )
		{
			 endowmentFile.setStatusId( Constant.EndowmentFileStatus.REGISTRATION_REQUIRED_ID);
			 endowmentFileVO.getRequestView().setStatusId(90057l);
			 endowmentFileVO.getRequestView().setStatus( "90057" );
		}
		else if( status.equals( Constant.EndowmentFileStatus.APPROVAL_REQUIRED) )
		{
			 endowmentFile.setStatusId( Constant.EndowmentFileStatus.APPROVAL_REQUIRED_ID);
			 endowmentFileVO.getRequestView().setStatusId(  Constant.REQUEST_STATUS_APPROVAL_REQUIRED_ID );
			 endowmentFileVO.getRequestView().setStatus( String.valueOf( Constant.REQUEST_STATUS_APPROVAL_REQUIRED_ID  ) );
		}
		else if( status.equals( Constant.EndowmentFileStatus.APPROVED ) )
		{
			 endowmentFile.setStatusId( Constant.EndowmentFileStatus.APPROVED_ID );
			 endowmentFileVO.getRequestView().setStatusId(  Constant.REQUEST_STATUS_APPROVED_ID  );
			 endowmentFileVO.getRequestView().setStatus( String.valueOf( Constant.REQUEST_STATUS_APPROVED_ID  ) );
		}
		else if( status.equals( Constant.EndowmentFileStatus.EVALUATION_REQUIRED ) )
		{
			 endowmentFile.setStatusId( Constant.EndowmentFileStatus.EVALUATION_REQUIRED_ID );
			 endowmentFileVO.getRequestView().setStatusId(90058l);
			 endowmentFileVO.getRequestView().setStatus( "90058" );
		}
		else if( status.equals( Constant.EndowmentFileStatus.COMPLETE ) )
		{
			 endowmentFile.setStatusId( Constant.EndowmentFileStatus.COMPLETED_ID );
			 endowmentFileVO.getRequestView().setStatusId(9005l);
			 endowmentFileVO.getRequestView().setStatus( "9005" );
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onSubmit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String status =  Constant.EndowmentFileStatus.REGISTRATION_REQUIRED;
		String msg    =  "endowmentFile.msg.submittedForRegistration";
		String event = MessageConstants.RequestEvents.REQUEST_SUBMIT;
		try	
		{	
			if( hasSaveErrors() ){ return; }
			 updateValuesFromMap();	
			 SystemParameters parameters = SystemParameters.getInstance();
			 String endPoint= parameters.getParameter( "MEMSEndowmentRegistrationBPEL" );
			 MEMSEndowmentRegistrationPortClient port=new MEMSEndowmentRegistrationPortClient();
			 port.setEndpoint(endPoint);
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 if ( !endowmentFile.hasAmafManagedAsset() )
			 {
				 status =  Constant.EndowmentFileStatus.COMPLETE;
				 msg    =  "endowmentFile.msg.completed";
				 event = MessageConstants.RequestEvents.REQUEST_COMPLETED;
			 }
			 else
			 {
				 port.initiate(
						 		CommonUtil.getLoggedInUser(),
						 		endowmentFileVO.getEndowmentFile().getFileId().toString(),
						 		endowmentFileVO.getEndowmentFile().getFileNumber(),
						 		Integer.valueOf( endowmentFileVO.getRequestView().getRequestId().toString() ),
						 		endowmentFileVO.getRequestView().getRequestNumber(),
						 		null,
						 		null
				              );
			 }
			 persistEndowmentFile( status );
			 updateValuesToMap();
			 saveCommentsAttachment( event );

			 ApplicationContext.getContext().getTxnContext().commit();
			 getEndowmentFileDetails( endowmentFile.getFileId() );
			 onEndowmentTab();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSubmit --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	@SuppressWarnings( "unchecked" )	
	private boolean hasRegisterErrors()throws Exception
	{
		boolean hasErrors=false;
			for (EndFileAsso item : endowmentFile.getEndFileAssos()) 
			{
				if(  item.getEndFileBens() == null || item.getEndFileBens().size() <= 0  )
				{
					String message= java.text.MessageFormat.format(
						    ResourceUtil.getInstance().getProperty("endowmentFile.msg.beneficiariesRequired"), 
                            item.getEndowment().getEndowmentNum()
                           ); 
					errorMessages.add( message );
	        		tabPanel.setSelectedTab(TAB_ENDOWMENT);
	    			onEndowmentTab();
	        		return true;
				}
				
			}
		endowmentsOfficialCorrespondenceTabBean bean = (endowmentsOfficialCorrespondenceTabBean)getBean("pages$endowmentsOfficialCorrespondenceTab");
        for(OfficialCorrespondenceView view :bean.getOfficialCorresList())
        {
        	if( view.getStatus().compareTo(	170001l ) == 0) 
        	{
        		errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentFile.msg.closeCorrespondences"));
        		tabPanel.setSelectedTab(TAB_REGISTRATION);
    			onOfficialCorrespondenceTab();
        		return true;
        	}
        }
		
		return hasErrors;
		
	}
	@SuppressWarnings( "unchecked" )
	public void onRegister()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String status =  Constant.EndowmentFileStatus.APPROVAL_REQUIRED;
		String msg    =  "endowmentFile.msg.registered";
		String event = MessageConstants.RequestEvents.REQUEST_SENT_FOR_APPROVAL;
		
		try	
		{	
			if( hasRegisterErrors() ){ return; }
			 updateValuesFromMap();	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
          			 
			 persistEndowmentFile( status );
			 setTaskOutCome(TaskOutcome.OK);
			  
			 updateValuesToMap();
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();

			 getEndowmentFileDetails( endowmentFile.getFileId() );
			 onEndowmentTab();
		   	 
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRegister--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	private boolean hasApprovalErrors()throws Exception
	{
		boolean hasErrors=false;
		String typesWithNoEvalGroups = endowmentFileService.getEndowmentTypesWithNoEvalGroups(endowmentFile);
		
		if( typesWithNoEvalGroups.length() > 0 )
		{
			String message= java.text.MessageFormat.format(
														    ResourceUtil.getInstance().getProperty("endowmentFile.msg.evaluationGrpsUndefined"), 
					                                        typesWithNoEvalGroups
					                                       ); 
			errorMessages.add(message);
			tabPanel.setSelectedTab(TAB_ENDOWMENT);
			onEndowmentTab();
			return true;
		}
		
		return hasErrors;
	}
	@SuppressWarnings( "unchecked" )
	public void onMessageFromReceivePayments()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
            
			PaymentReceiptView prv=(PaymentReceiptView)sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
			sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			prv.setPaidById(requestView.getApplicantView().getPersonId());
			Long status =  WebConstants.REQUEST_STATUS_COMPLETE_ID;
			
			boolean shallDistribute=false;
			
			prv = EndowmentService.collectEndowmentCashCheckBankTransferPayments( prv );
			
			CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());

			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_PAYMENT_COLLECTED_ONLY);

			viewMap.put("collectionCompleted", true);
			 updateValuesToMap();
			 ApplicationContext.getContext().getTxnContext().commit();

			 getEndowmentFileDetails( endowmentFile.getFileId() );
			 onEndowmentTab();
			successMessages.add( ResourceUtil.getInstance().getProperty("thirdPartRevenue.msg.collection"));
			
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromReceivePayments--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onApprove()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String status =  Constant.EndowmentFileStatus.APPROVED;
		String msg    =  "endowmentFile.msg.approved";
		String event = MessageConstants.RequestEvents.REQUEST_APPROVED;
		try	
		{	
			if( hasApprovalErrors() ){ return; }
			 updateValuesFromMap();	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
          			 
			 persistEndowmentFile( status );
			 setTaskOutCome(TaskOutcome.APPROVE);
			 generateEvaluationTasks(); 
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();

			 getEndowmentFileDetails( endowmentFile.getFileId() );
			 onEndowmentTab();
		   	 
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onApprove--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private boolean hasRejectionErrors()throws Exception
	{
		boolean hasErrors=false;
		return hasErrors;
	}
	@SuppressWarnings( "unchecked" )
	public void onReject()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String status =  Constant.EndowmentFileStatus.REGISTRATION_REQUIRED;
		String msg    =  "endowmentFile.msg.sendBack";
		String event = MessageConstants.RequestEvents.REQUEST_REJECTED;
		try	
		{	
			if( hasRejectionErrors() ){ return; }
			 updateValuesFromMap();	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
          			 
			 persistEndowmentFile( status );
			 setTaskOutCome(TaskOutcome.REJECT);
			 updateValuesToMap();
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();

			 getEndowmentFileDetails( endowmentFile.getFileId() );
			 onEndowmentTab();
		   	 
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onReject--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	
	@SuppressWarnings( "unchecked" )	
	private boolean hasEvaluateDoneErrors()throws Exception
	{
		boolean hasErrors=false;
		for (EndFileAsso item : endowmentFile.getEndFileAssos()) 
		{
			if( item.getEndowment().getAssetType().getAssetTypeId().compareTo(endowmentFileVO.getEvaluateEndowmentForAssetTypeId() ) != 0 ) continue; 
			if(  item.getStatusId().compareTo( Constant.EndowmentFileAssoStatus.EVALUATED_ID  ) != 0 )
			{
				errorMessages.add( ResourceUtil.getInstance().getProperty("endowmentFile.msg.evaluateAllEndowments") );
        		tabPanel.setSelectedTab(TAB_ENDOWMENT);
    			onEndowmentTab();
        		return true;
			}
			
		}
		return hasErrors;
		
	}
	@SuppressWarnings( "unchecked" )
	public void onEvaluateDone()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String status =  Constant.EndowmentFileStatus.EVALUATION_REQUIRED;
		String msg    =  "endowmentFile.msg.evaluated";
		
		try	
		{	
			if( hasEvaluateDoneErrors() ){ return; }
			
			 Long assetTypeId = endowmentFileVO.getEvaluateEndowmentForAssetTypeId();
			 updateValuesFromMap();	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
          			 
			 persistEndowmentFile( status );
			 setTaskOutCome(TaskOutcome.OK);
			 endowmentFileVO.setEvaluateEndowmentForAssetTypeId( null );

			 AssetType assetType = endowmentFileService.getAssetTypeById(assetTypeId);
			 msg  = java.text.MessageFormat.format( 
				 									ResourceUtil.getInstance().getProperty(msg),
				 									assetType.getAssetTypeNameAr() 
				 								  );
			 updateValuesToMap();
			 saveCommentsAttachment( null );
			 NotesController.saveSystemNotesForRequest(
					 								   WebConstants.SocialSolidarityProgram.NOTES_OWNER,
					 								   "endowmentFile.msg.evaluated",
					 								   endowmentFile.getFileId(),assetType.getAssetTypeNameAr()
					 								  );			 
			 ApplicationContext.getContext().getTxnContext().commit();
             
			 getEndowmentFileDetails( endowmentFile.getFileId() );
			 onEndowmentTab();
		   	 
			 successMessages.add( msg);
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onEvaluateDone--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	
	private void generateEvaluationTasks() throws Exception
	{
		 SystemParameters parameters = SystemParameters.getInstance();
		 String endPoint= parameters.getParameter( "MEMSEndowmentEvaluationBPEL" );
		 MEMSEndowmentEvaluationBPELPortClient port=new MEMSEndowmentEvaluationBPELPortClient();
		 port.setEndpoint(endPoint);
		 
		 Map<Long,Long> mapTaskGeneratedForAssetTypes = new HashMap<Long, Long>();
		 for (EndFileAsso endFileAsso : endowmentFileVO.getEndowmentFile().getEndFileAssos() )
		 {
			
		   Long assetTypeId = endFileAsso.getEndowment().getAssetType().getAssetTypeId();
		   if( mapTaskGeneratedForAssetTypes.containsKey(  assetTypeId ) )
			  continue;
		   
		   mapTaskGeneratedForAssetTypes.put(assetTypeId,assetTypeId);
		   String evalGroup = endowmentFileService.getEvalGroupsForAssetTypeId(assetTypeId);
		   port.initiate(
					 		CommonUtil.getLoggedInUser(),
					 		endowmentFileVO.getEndowmentFile().getFileId().toString(),
					 		assetTypeId.toString(), 
					 		endowmentFileVO.getEndowmentFile().getFileNumber(),
					 		Integer.valueOf( endowmentFileVO.getRequestView().getRequestId().toString() ),
					 		endowmentFileVO.getRequestView().getRequestNumber(),
					 		evalGroup,
					 		null,
					 		null
			              );
		 }
	}
	private boolean hasOnCompleteErrors() throws Exception
	{
		boolean hasErrors=false;
		for (EndFileAsso item : endowmentFile.getEndFileAssos()) 
		{
			if(  item.getStatusId().compareTo( Constant.EndowmentFileAssoStatus.EVALUATED_ID  ) != 0 )
			{
				errorMessages.add( ResourceUtil.getInstance().getProperty("endowmentFile.msg.evaluateAllEndowments") );
        		tabPanel.setSelectedTab(TAB_ENDOWMENT);
    			onEndowmentTab();
        		return true;
			}
			
		}
		return hasErrors;		
	}
	@SuppressWarnings( "unchecked" )
	public void onComplete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		String status =  Constant.EndowmentFileStatus.COMPLETE;
		String msg    =  "endowmentFile.msg.completed";
		String event = MessageConstants.RequestEvents.REQUEST_COMPLETED;
		try	
		{	
			if( hasOnCompleteErrors() ){ return; }
			 updateValuesFromMap();	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 persistEndowmentFile( status );
			 updateValuesToMap();
			 saveCommentsAttachment( event );
			 if( 
				 endowmentFile.getInheritanceFile() != null && 
				 endowmentFile.getInheritanceFile().getInheritanceFileId() != null
			   )
			 {
				 CommonUtil.saveSystemComments(
												WebConstants.InheritanceFile.NOTES_OWNER, 
												"inheritanceFile.msg.fileConvertedToEndowmentFileComplete", 
												endowmentFile.getInheritanceFile().getInheritanceFileId(), 
												endowmentFile.getFileNumber()
										 	 );
			 }
			 ApplicationContext.getContext().getTxnContext().commit();

			 getEndowmentFileDetails( endowmentFile.getFileId() );
			 onEndowmentTab();
		   	 
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onComplete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}

		
	}
	
	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws Exception
    {
    	try
    	{
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    	}
    	
    	catch(Exception ex)
    	{
    		logger.LogException("setTaskOutCome| Error Occured...",ex);
    		throw ex;
    	}
    }



	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask )
	{
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}


	public String getPageTitle() {
	
	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW) || pageMode.equals(  PAGE_MODE_APPROVED ) )
	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("endowmentFile.title.heading"));
	}
	else if( pageMode.equals(  PAGE_MODE_REG_REQ ) )
	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("endowmentFile.title.registerEndowments"));
	}
	else if( pageMode.equals(  PAGE_MODE_EVALUATION_REQ )  )
	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("endowmentFile.title.evaluateEndowments"));
	}
	else if( pageMode.equals(  PAGE_MODE_APPROVAL_REQ )  )
	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("endowmentFile.title.approveEndowments"));
	}	 	 
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowAddEndowmentButton()
	{
		if( 
				hdnFilePersonId != null && hdnFilePersonId.trim().length() > 1  &&
			(
				getPageMode().equals( PAGE_MODE_NEW )  || 
				getPageMode().equals( PAGE_MODE_REG_REQ ) 
			)
		  )
		{
			return true;
		}
		return false;
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowAddEndowmentButtonAfterCompletion()
	{
		if( 
				hdnFilePersonId != null && hdnFilePersonId.trim().length() > 1  &&
				getStatusCompleted() ||
				getStatusApproved()
		  )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowDeleteEndowmentLink()
	{
		if( getPageMode().equals( PAGE_MODE_NEW )  || 
				getPageMode().equals( PAGE_MODE_REG_REQ )  
		  )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowEditEndowmentLink()
	{
		if( getPageMode().equals( PAGE_MODE_NEW )  || 
				getPageMode().equals( PAGE_MODE_REG_REQ )  
		  )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowAddBeneficiariesLink()
	{
		if( getPageMode().equals( PAGE_MODE_NEW )  || 
			getPageMode().equals( PAGE_MODE_REG_REQ ) ||
			getPageMode().equals( PAGE_MODE_APPROVAL_REQ )|| 
			getPageMode().equals( PAGE_MODE_APPROVED)||
			getPageMode().equals( PAGE_MODE_EVALUATION_REQ)||
			getPageMode().equals( PAGE_MODE_COMPLETE)
			
		  )
		{
			return true;
		}
		//if file has been approved and still there is no beneficiary in endowment then show add beneficiary icon.
		else
		{
			EndowmentDetailsTabBacking bean = (EndowmentDetailsTabBacking )getBean("pages$tabEndowmentDetails");
			return bean.hasNoBeneficiaries();
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowThirdPartyLink()
	{
		if( getPageMode().equals( PAGE_MODE_NEW ) || 
			getPageMode().equals( PAGE_MODE_REG_REQ ) ||
			getPageMode().equals( PAGE_MODE_APPROVAL_REQ ) ||
			getPageMode().equals( PAGE_MODE_EVALUATION_REQ )
		  )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowReceivePropertyLink()
	{
		if( 
			!getPageMode().equals(PAGE_MODE_NEW)  &&
//			!getPageMode().equals(PAGE_MODE_COMPLETE ) &&
//			!getPageMode().equals(PAGE_MODE_EVALUATION_REQ) &&
			!getPageMode().equals(PAGE_MODE_VIEW)		
		
		 )
		{
			return true;
		}
		return false;
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowCollectPaymentLink()
	{
		if( 
			!getPageMode().equals(PAGE_MODE_NEW)  &&
//			!getPageMode().equals(PAGE_MODE_COMPLETE ) &&
//			!getPageMode().equals(PAGE_MODE_EVALUATION_REQ) &&
			!getPageMode().equals(PAGE_MODE_VIEW)		
		
		 )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowPortfolioLink()
	{
		if(	!getPageMode().equals(PAGE_MODE_NEW) && 
//			!getPageMode().equals(PAGE_MODE_COMPLETE ) &&
//			!getPageMode().equals(PAGE_MODE_EVALUATION_REQ) &&
			!getPageMode().equals(PAGE_MODE_VIEW) 
		)
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
				getPageMode().equals( PAGE_MODE_NEW ) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowRegistrationTab()
	{
		if( !getPageMode().equals( PAGE_MODE_NEW) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowEvaluateEndowmentLink()
	{
		if( 
			!getPageMode().equals( PAGE_MODE_NEW ) &&   
			!getPageMode().equals( PAGE_MODE_REG_REQ ) 
			&&
		    endowmentFileVO.getEvaluateEndowmentForAssetTypeId() != null
		  )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowEvaluation()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
		    ( getPageMode().equals( PAGE_MODE_APPROVED ) || getPageMode().equals( PAGE_MODE_EVALUATION_REQ ) )&&
		    endowmentFileVO.getEvaluateEndowmentForAssetTypeId() != null 
		    
		  )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowRegistration()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
				getPageMode().equals( PAGE_MODE_REG_REQ) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowApprove()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
				getPageMode().equals( PAGE_MODE_APPROVAL_REQ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowComplete()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
				getPageMode().equals( PAGE_MODE_EVALUATION_REQ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}

	public EndowmentFile getEndowmentFile() {
		return endowmentFile;
	}

	public void setEndowmentFile(EndowmentFile endowmentFile) {
		this.endowmentFile = endowmentFile;
	}

	public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public String getHdnFilePersonId() {
		return hdnFilePersonId;
	}

	public void setHdnFilePersonId(String hdnFilePersonId) {
		this.hdnFilePersonId = hdnFilePersonId;
	}

	public String getHdnFilePersonName() {
		return hdnFilePersonName;
	}

	public void setHdnFilePersonName(String hdnFilePersonName) {
		this.hdnFilePersonName = hdnFilePersonName;
	}

	public org.richfaces.component.html.HtmlTab getTabEndowments() {
		return tabEndowments;
	}

	public void setTabEndowments(org.richfaces.component.html.HtmlTab tabEndowments) {
		this.tabEndowments = tabEndowments;
	}

	public HtmlCommandButton getBtnAddEndowment() {
		return btnAddEndowment;
	}

	public void setBtnAddEndowment(HtmlCommandButton btnAddEndowment) {
		this.btnAddEndowment = btnAddEndowment;
	}

	public HtmlCommandButton getBtnAddEndowmentAfterCompletion() {
		return btnAddEndowmentAfterCompletion;
	}

	public void setBtnAddEndowmentAfterCompletion(
			HtmlCommandButton btnAddEndowmentAfterCompletion) {
		this.btnAddEndowmentAfterCompletion = btnAddEndowmentAfterCompletion;
	}
	
	

}