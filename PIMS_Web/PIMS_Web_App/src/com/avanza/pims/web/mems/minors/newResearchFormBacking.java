package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.entity.AccomodationOnwershipType;
import com.avanza.pims.entity.AccomodationType;
import com.avanza.pims.entity.DiseaseType;
import com.avanza.pims.entity.EducationLevel;
import com.avanza.pims.entity.Grade;
import com.avanza.pims.entity.HealthStatus;
import com.avanza.pims.entity.HobbyType;
import com.avanza.pims.entity.HomeConditionType;
import com.avanza.pims.entity.HomeType;
import com.avanza.pims.entity.OrphanCategory;
import com.avanza.pims.entity.PropertyBeneficiaryType;
import com.avanza.pims.entity.RentingReason;
import com.avanza.pims.entity.ResidenceType;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritedAssetService;
import com.avanza.pims.ws.mems.ResearchFormBeneficiaryService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.ResearchFormBeneficiaryView;
import com.avanza.pims.ws.vo.mems.AccomodationAspectsView;
import com.avanza.pims.ws.vo.mems.AnnualIncomeView;
import com.avanza.pims.ws.vo.mems.BeneficiariesPickListView;
import com.avanza.pims.ws.vo.mems.EducationAspectsView;
import com.avanza.pims.ws.vo.mems.HealthAspectsView;
import com.avanza.pims.ws.vo.mems.MonthlyExpenseView;
import com.avanza.pims.ws.vo.mems.MonthlyIncomeView;
import com.avanza.ui.util.ResourceUtil;


@SuppressWarnings( "unchecked" )
public class newResearchFormBacking extends AbstractMemsBean {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9174468029380277403L;
	HttpServletRequest request ;
	private Long inheritanceFileId;
	private long personId;
	private ResearchFormBeneficiaryService service;
	private UtilityService us;
	private Boolean isViewModePopUp;
	private Integer copyBeneficiaryListSize;
	private List<SelectItem> copyBeneficiaryList, incomeCategoryList ,rentingReasonList,homeConditionList,homeTypeList,beneficiaryPropList,resTypeList
	,accOwnershipList,accTypeList,hobbiesList,orphanCatlist,diseaseTypeList,educationLevelList,gradeList,healthStatusList ; 
	private Map<String,String> othersMap;
	ResearchFormBeneficiaryView view;
	MonthlyIncomeView mivu;
	List<MonthlyIncomeView> mivuList;
	MonthlyExpenseView mevu;
	List<MonthlyExpenseView> mevuList;
	AnnualIncomeView aivu;
	List<AnnualIncomeView> aivuList;
	private boolean openedForOwner;
	protected HtmlTabPanel tabPanel;
	private static final String IS_OPENED_FOR_OWNER = "IS_OPENED_FOR_OWNER";
	private static final String TAB_ACCOMODATION_ASPECTS = "tabAccomodationAspects";
	private static final String TAB_FINANCIAL_ASPECTS = "tabFinancialAspects";
	private static final String TAB_EDUCATIONAL_ASPECTS = "tabEducationalAspects";
	private static final String TAB_HEALTH_ASPECTS = "tabHealthAspects";
	private static final String INCOME_CATEGORY_LIST = "incomeCategoryList";
	private static final String VIEW = "researchFormBeneficiaryView";
	private static final String RENTING_REASON_LIST = "rentingReasonList";
	private static final String HOME_CONDITION_LIST = "homeConditionList";
	private static final String HOME_TYPE_LIST = "homeTypeList";
	private static final String BENEFICIARY_PROP_LIST = "beneficiaryPropList";
	private static final String RES_TYPE_LIST = "resTypeList";
	private static final String ACC_OWNERSHIP_LIST = "accOwnerShipList";
	private static final String ACC_TYPE_LIST = "accTypeList";
	private static final String HOBBIES_LIST = "HOBBIES_LIST";
	private static final String DISEASE_TYPE_LIST = "DISEASE_TYPE_LIST";
	private static final String EDUCATION_LEVEL_LIST = "EDUCATION_LEVEL_LIST";
	private static final String GRADE_LIST = "GRADE_LIST";
	private static final String HEALTH_STATUS_LIST = "HEALTH_STATUS_LIST";
	private static final String ORPHAN_CAT_LIST = "ORPHAN_CAT_LIST";
	private static final String COPY_BEN_LIST = "COPY_BEN_LIST";
	private static final String COPY_BEN_LIST_SIZE = "COPY_BEN_LIST_SIZE";
	
	private static final String SCREEN_VIEW_MODE_POPUP="pageMode";
	private static final String OTHERS_ID_MAP= "othersIdMap";
	private HtmlDataTable accomodationAspectsDT ;
	private HtmlDataTable educationAspectDT ;
	private HtmlDataTable healthAspectsDT;

	public newResearchFormBacking() 
	{		
		request = ( HttpServletRequest ) this.getFacesContext().getExternalContext().getRequest();
		copyBeneficiaryList = new ArrayList<SelectItem>(0);
		incomeCategoryList = new ArrayList<SelectItem>(0);
		rentingReasonList  = new ArrayList<SelectItem>(0);
		homeConditionList  = new ArrayList<SelectItem>(0);
		beneficiaryPropList  = new ArrayList<SelectItem>(0);
		homeTypeList = new ArrayList<SelectItem>(0);
		resTypeList  = new ArrayList<SelectItem>(0);
		accOwnershipList = new ArrayList<SelectItem>(0);
		accTypeList = new ArrayList<SelectItem>(0);
		hobbiesList = new ArrayList<SelectItem>(0);
		orphanCatlist = new ArrayList<SelectItem>(0);
		diseaseTypeList = new ArrayList<SelectItem>(0);
		educationLevelList = new ArrayList<SelectItem>();
		gradeList = new ArrayList<SelectItem>(0);
		healthStatusList = new ArrayList<SelectItem>(0);
		othersMap = new HashMap<String, String>();
		view = new ResearchFormBeneficiaryView();
		
		mivu = new MonthlyIncomeView();
		mivuList = new ArrayList<MonthlyIncomeView>();
		mivuList.add(mivu);//adding empty element to list
		
		mevu = new MonthlyExpenseView();
		mevuList = new ArrayList<MonthlyExpenseView>();
		mevuList.add(mevu);//adding empty element to list
		
		aivu = new AnnualIncomeView();
		aivuList = new ArrayList<AnnualIncomeView>();
		aivuList.add(aivu);//adding empty element to list
		
		view.setLastMonthlyIncome(mivuList);
		view.setLastMonthlyExpense(mevuList);
		view.setLastAnnualIncome(aivuList);
		
		tabPanel = new HtmlTabPanel();
		service =  new ResearchFormBeneficiaryService();
		us = new UtilityService();
	}
	@Override
	public void init() 
	{
		errorMessages = new ArrayList<String>(0);
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException( " init()-- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception,PimsBusinessException
	{
		if( !isPostBack() )
		{
			if(sessionMap.get(WebConstants.PERSON_TYPE) != null)
			{
				if(sessionMap.get(WebConstants.PERSON_TYPE).toString().compareTo(WebConstants.PERSON_TYPE_OWNER) == 0)
				{
					sessionMap.remove(WebConstants.PERSON_TYPE);
					viewMap.put(IS_OPENED_FOR_OWNER, true);
				}
			}
			
			if (request.getParameter(SCREEN_VIEW_MODE_POPUP) != null) 
			{
				isViewModePopUp = true;
			}
			else
			{
				isViewModePopUp = false;
			}
			if (request.getParameter("context") != null)
			{
				viewMap.put("context", request.getParameter("context") );
			}
			if (request.getParameter(WebConstants.PERSON_ID) != null) 
			{
				this.personId = Long.valueOf( request.getParameter(WebConstants.PERSON_ID).toString() );
			}
			else if (sessionMap.get(WebConstants.PERSON_ID) != null) //beneficiaryID 
			{
				personId = Long.valueOf( sessionMap.get(WebConstants.PERSON_ID).toString() );
				sessionMap.remove(WebConstants.PERSON_ID);
			}
			
			if (sessionMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null) 
			{
				inheritanceFileId  = Long.valueOf( sessionMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID ).toString());
				sessionMap.remove(WebConstants.InheritanceFile.INHERITANCE_FILE_ID);
				view.setInhFileId(inheritanceFileId);
			}
			view.setPersonId(personId);
            view.setLoggedInUser( CommonUtil.getLoggedInUser() );
            if(personId != 0)
            	view = service.getResearchFormData(view, personId);
			loadLists();
		}
		if( viewMap.get( WebConstants.PERSON_ID  ) != null )
		{
		    personId = Long.valueOf( viewMap.get( WebConstants.PERSON_ID ).toString() );
		}
		if( viewMap.get( SCREEN_VIEW_MODE_POPUP )!= null )
		{
		    isViewModePopUp = (Boolean)viewMap.get( SCREEN_VIEW_MODE_POPUP  );
		}
		if( viewMap.get( VIEW )!= null )
		{
		    view = (ResearchFormBeneficiaryView)viewMap.get( VIEW );
		}
		if ( viewMap.get( COPY_BEN_LIST ) != null )
		{
			copyBeneficiaryList = (List<SelectItem>)viewMap.get( COPY_BEN_LIST );
			
		    setCopyBeneficiaryListSize( copyBeneficiaryList!= null ? copyBeneficiaryList.size(): 0 );
		}
		if( viewMap.get( INCOME_CATEGORY_LIST ) != null )
		{
		    incomeCategoryList   = (List<SelectItem>)viewMap.get( INCOME_CATEGORY_LIST );
		}
		if( viewMap.get( RENTING_REASON_LIST  ) != null )
		{
		    rentingReasonList    = (List<SelectItem>)viewMap.get( RENTING_REASON_LIST );
		}
		if( viewMap.get( HOME_CONDITION_LIST ) != null )
		{
		    homeConditionList    = (List<SelectItem>)viewMap.get( HOME_CONDITION_LIST );
		}
		if( viewMap.get( BENEFICIARY_PROP_LIST ) != null )
		{
		    beneficiaryPropList  = (List<SelectItem>)viewMap.get( BENEFICIARY_PROP_LIST );
		}
		if( viewMap.get( HOME_TYPE_LIST ) != null )
		{
		    homeTypeList         = (List<SelectItem>)viewMap.get( HOME_TYPE_LIST );
		}
		if( viewMap.get( RES_TYPE_LIST ) != null )
		{
		    resTypeList          = (List<SelectItem>)viewMap.get( RES_TYPE_LIST );
		}
		if( viewMap.get( ACC_OWNERSHIP_LIST ) != null )
		{
		    accOwnershipList     = (List<SelectItem>)viewMap.get( ACC_OWNERSHIP_LIST );
		}
		if( viewMap.get( ACC_TYPE_LIST  ) != null )
		{
		    accTypeList          = (List<SelectItem>)viewMap.get( ACC_TYPE_LIST  );
		}
		if( viewMap.get( HOBBIES_LIST  ) != null )
		{
		    hobbiesList          = (List<SelectItem>)viewMap.get( HOBBIES_LIST  );
		}
		if( viewMap.get( ORPHAN_CAT_LIST  ) != null )
		{
		    orphanCatlist          = (List<SelectItem>)viewMap.get( ORPHAN_CAT_LIST  );
		}
		if( viewMap.get( GRADE_LIST  ) != null )
		{
		    gradeList          = (List<SelectItem>)viewMap.get( GRADE_LIST  );
		}
		if( viewMap.get( EDUCATION_LEVEL_LIST  ) != null )
		{
		    educationLevelList          = (List<SelectItem>)viewMap.get( EDUCATION_LEVEL_LIST  );
		}
		if( viewMap.get( HEALTH_STATUS_LIST  ) != null )
		{
		    healthStatusList 			= (List<SelectItem>)viewMap.get( HEALTH_STATUS_LIST  );
		}
		if( viewMap.get( DISEASE_TYPE_LIST  ) != null )
		{
		    diseaseTypeList          = (List<SelectItem>)viewMap.get( DISEASE_TYPE_LIST  );
		}
		if( viewMap.get( OTHERS_ID_MAP )!=null )
		{
		    othersMap = (Map<String,String>) viewMap.get( OTHERS_ID_MAP );
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		
		if( personId > 0 )
		{
		  viewMap.put(WebConstants.PERSON_ID, personId );
		}
		
		if( othersMap != null && othersMap.size() > 0 )
		{
			viewMap.put( OTHERS_ID_MAP,othersMap );
		}
	    if( view != null )
	    {
	    	viewMap.put(VIEW, view);
	    }
	    if( isViewModePopUp != null && isViewModePopUp )
	    {
	    	viewMap.put( SCREEN_VIEW_MODE_POPUP,true);
	    }
	    else
	    {
	    	viewMap.put( SCREEN_VIEW_MODE_POPUP,false);
	    }
	   
	}
	
	private boolean hasError() throws Exception 
	{
		boolean hasError = false;
		
		if(hasFormError()){
			hasError = true;
		}
		
		if( hasAccomdationAspectError())
		{
			tabPanel.setSelectedTab(TAB_ACCOMODATION_ASPECTS);
			hasError = true;
		}
		else if( hasFinancialAspectError())
		{
			tabPanel.setSelectedTab( TAB_FINANCIAL_ASPECTS );
			hasError = true;
		}
		else if( hasEducationalAspectError())
		{
				tabPanel.setSelectedTab(TAB_EDUCATIONAL_ASPECTS );
				hasError = true;
		}
		else if ( hasHealthAspectError())
		{
			tabPanel.setSelectedTab(TAB_HEALTH_ASPECTS );
			hasError = true;
	     }
		return hasError;
	}
	@SuppressWarnings("unchecked")
	public boolean isContextFamilyVillage()throws Exception
	{
		if ( viewMap.get("context") != null && viewMap.get("context").toString().equals("familyVillage"))
			return true;
		return false;
				
			
	}
	private boolean isEmptyHealthAspect() {
		if (view.getHealthAspects() == null
				|| (	(view.getHealthAspects().getDiseaseTypeId() == null || view.getHealthAspects().getDiseaseTypeId().compareTo("-1") == 0)
						&& (StringHelper.isEmpty(view.getHealthAspects().getHealthCardNumber()))
						&& (view.getHealthAspects().getHealthStatusId() == null || view.getHealthAspects().getHealthStatusId().compareTo("-1") == 0)
					)
			)
			return true;
		else
			return false;
	}
	private boolean isEmptyEduAspect() {
		if(view.getEducationAspects() == null ||
			(
					(view.getEducationAspects().getEducationLevelId() == null || view.getEducationAspects().getEducationLevelId().compareTo("-1") == 0)&&
					(view.getEducationAspects().getGradeId() == null || view.getEducationAspects().getGradeId().compareTo("-1") == 0) &&
					(StringHelper.isEmpty(view.getEducationAspects().getInstituteName()) )
			)
		)
		return true;
		return false;
	}
	private boolean isEmptyMonthlyIncome()throws Exception
	{
		if (view.getLastMonthlyIncome() == null || view.getLastMonthlyIncome().size() <= 0 )
		{
			return true;
		}
		else if (view.getLastMonthlyIncome() != null && view.getLastMonthlyIncome().size()> 0 )
		{
			MonthlyIncomeView lmiv =  view.getLastMonthlyIncome().get(0);
			if(		
				StringHelper.isEmpty(lmiv.getIncomeSource()) 
				&& StringHelper.isEmpty(lmiv.getOthers())
				&&	StringHelper.isEmpty(lmiv.getPension())
				&&	StringHelper.isEmpty(lmiv.getRealEstate())
				&&	StringHelper.isEmpty(lmiv.getSocSecurity())
				&&	StringHelper.isEmpty(lmiv.getTotal())
				&&	StringHelper.isEmpty(lmiv.getTransport())
			)
				return true;

		}
		return false;
		
		
	}
	private boolean isEmptyAnnualIncome()throws Exception
	{
		if (view.getLastAnnualIncome() == null || view.getLastAnnualIncome().size() <= 0 ){
			
			return true;
		}
		else if (view.getLastAnnualIncome() != null && view.getLastAnnualIncome().size()> 0 ){
			AnnualIncomeView laiv =  view.getLastAnnualIncome().get(0);
			if(		
				StringHelper.isEmpty(laiv.getIncomeSource()) 
				&& StringHelper.isEmpty(laiv.getOthers())
				&&	StringHelper.isEmpty(laiv.getLands())
				&&	StringHelper.isEmpty(laiv.getRealEstate())
				&&	StringHelper.isEmpty(laiv.getShares())
				&&	StringHelper.isEmpty(laiv.getTotal())
				&&	StringHelper.isEmpty(laiv.getTradeLicenses())
			)
				return true;
		}
		return false;
		
		
	}

	private boolean isEmptyMonthlyExpenses()throws Exception 
	{
		
		//check third row 
		if (view.getLastMonthlyExpense() == null || view.getLastMonthlyExpense().size() <= 0 ){
			return true;
		}
		else if (view.getLastMonthlyExpense() != null && view.getLastMonthlyExpense().size()> 0 ){
			MonthlyExpenseView lmev =  view.getLastMonthlyExpense().get(0);
			if(		
				StringHelper.isEmpty(lmev.getClothes()) 
				&& StringHelper.isEmpty(lmev.getElectricity())
				&&	StringHelper.isEmpty(lmev.getEntertainment())
				&&	StringHelper.isEmpty(lmev.getFood())
				&&	StringHelper.isEmpty(lmev.getMobile())
				&&	StringHelper.isEmpty(lmev.getPersonalExp())
				&&	StringHelper.isEmpty(lmev.getPetrol())
				&&	StringHelper.isEmpty(lmev.getServant())
				&&	StringHelper.isEmpty(lmev.getStationary())
				&&	StringHelper.isEmpty(lmev.getTelephone())
				&&	StringHelper.isEmpty(lmev.getTotal())
			)
				return true;
		}
		return false; 
	}
	private boolean isEmptyAccAspect() {
		if (view.getAccomdationAspects() == null
				|| (	(view.getAccomdationAspects().getAccomodationOwnershipId() == null || view.getAccomdationAspects().getAccomodationOwnershipId().compareTo("-1") == 0)
						&& (view.getAccomdationAspects().getAccomodationTypeId() == null || view.getAccomdationAspects().getAccomodationTypeId().compareTo("-1") == 0)
						&& (view.getAccomdationAspects().getBeneficiaryOfPropertyId() == null || view.getAccomdationAspects().getBeneficiaryOfPropertyId().compareTo("-1") == 0)
						&& (view.getAccomdationAspects().getConditionOfHomeId() == null || view.getAccomdationAspects().getConditionOfHomeId().compareTo("-1") == 0) 
						&& (view.getAccomdationAspects().getReasonForRentId() == null || view.getAccomdationAspects().getReasonForRentId().compareTo("-1") == 0 ) 
						&& (view.getAccomdationAspects().getResTypeId() == null || view.getAccomdationAspects().getResTypeId().compareTo("-1") == 0)
						&& (view.getAccomdationAspects().getTypeOfHome() == null || view.getAccomdationAspects().getTypeOfHome().size() <= 0)
					)
			)
			return true;
		else
			return false;
	}
	private boolean hasAccomdationAspectError()throws Exception
	{
		boolean hasErrors=false;
		if( view.getAccomdationAspects().getBeneficiaryOfProperty()!= null && view.getAccomdationAspects().getBeneficiaryOfProperty().trim().length() > 0  )
		{
			if( othersMap.containsKey(BENEFICIARY_PROP_LIST) &&  
				othersMap.get(BENEFICIARY_PROP_LIST).toString().equals(view.getAccomdationAspects().getBeneficiaryOfProperty() ) )
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("key to be provided"));
				hasErrors = true;
			}
		}
		if (othersMap.containsKey(ACC_TYPE_LIST)
				&& othersMap.get(ACC_TYPE_LIST).toString().equals(
						view.getAccomdationAspects().getAccomodationTypeId()) && StringHelper.isEmpty(view.getAccomdationAspects().getAccomodationTypeDesc())) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					"researchFrom.errMsg.descForAccType"));
			hasErrors = true;
		}
		if (othersMap.containsKey(ACC_OWNERSHIP_LIST)
				&& othersMap.get(ACC_OWNERSHIP_LIST).toString().equals(
						view.getAccomdationAspects().getAccomodationOwnershipId()) && StringHelper.isEmpty(view.getAccomdationAspects().getAccomodationOwnershipDesc())) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					"researchFrom.errMsg.descForAccOwnership"));
			hasErrors = true;
		}
		if (othersMap.containsKey(RES_TYPE_LIST)
				&& othersMap.get(RES_TYPE_LIST).toString().equals(
						view.getAccomdationAspects().getResTypeId()) && StringHelper.isEmpty(view.getAccomdationAspects().getResTypeDesc())) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					"researchFrom.errMsg.descForResidenceType"));
			hasErrors = true;
		}
		if (othersMap.containsKey(HOME_CONDITION_LIST)
				&& othersMap.get(HOME_CONDITION_LIST).toString().equals(
						view.getAccomdationAspects().getConditionOfHomeId()) && StringHelper.isEmpty(view.getAccomdationAspects().getConditionOfHomeDesc())) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					"researchFrom.errMsg.descForConditionOfHome"));
			hasErrors = true;
		}
		if (othersMap.containsKey(RENTING_REASON_LIST)
				&& othersMap.get(RENTING_REASON_LIST).toString().equals(
						view.getAccomdationAspects().getReasonForRentId()) && StringHelper.isEmpty(view.getAccomdationAspects().getReasonForRentDesc())) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					"researchFrom.errMsg.descForRentingReason"));
			hasErrors = true;
		}
		if ( othersMap.containsKey(HOME_TYPE_LIST) && view.getAccomdationAspects().getTypeOfHome() !=null && 
			     view.getAccomdationAspects().getTypeOfHome().contains(  othersMap.get(HOME_TYPE_LIST)  ) && 
			     StringHelper.isEmpty( view.getAccomdationAspects().getTypeOfHomeDesc()  )
			    )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(
			"researchFrom.errMsg.descForHomeType"));
			hasErrors = true;
		}
		
		return hasErrors;
	}
	private boolean hasFinancialAspectError()throws Exception
	{
		boolean hasErrors=false;
		return hasErrors;
	}
	private boolean hasEducationalAspectError()throws Exception
	{
		boolean hasErrors=false;
		String others="";
		if(view.getEducationAspects() != null){
			others = othersMap.get(EDUCATION_LEVEL_LIST);
			if(StringHelper.isNotEmpty(others)){
				if(view.getEducationAspects() != null && view.getEducationAspects().getEducationLevelId() != null && view.getEducationAspects().getEducationLevelId().compareTo(others) == 0 && StringHelper.isEmpty(view.getEducationAspects().getEducationLevelDesc())){
					errorMessages.add(CommonUtil.getBundleMessage("researchFrom.errMsg.descforEducationLevel"));
					hasErrors = true;
				}
			}
			
			others = othersMap.get(GRADE_LIST);
			if(StringHelper.isNotEmpty(others)){
				if(view.getEducationAspects() != null && view.getEducationAspects().getGradeId() != null && view.getEducationAspects().getGradeId().compareTo(others) == 0 && StringHelper.isEmpty(view.getEducationAspects().getGradeDesc())){
					errorMessages.add(CommonUtil.getBundleMessage("researchFrom.errMsg.descforGrade"));
					hasErrors = true;
				}
			}
		}
		return hasErrors;
	}
	private boolean hasHealthAspectError()throws Exception
	{
		boolean hasErrors=false;
		String others="";
		if(view.getHealthAspects() != null){
			others = othersMap.get(HEALTH_STATUS_LIST);
			if(StringHelper.isNotEmpty(others)){
				if(view.getHealthAspects() != null && view.getHealthAspects().getHealthStatusId() != null&& view.getHealthAspects().getHealthStatusId().compareTo(others) == 0 && StringHelper.isEmpty(view.getHealthAspects().getHealthStatusDesc())){
					errorMessages.add(CommonUtil.getBundleMessage("researchFrom.errMsg.descforHealthStatus"));
					hasErrors = true;
				}
			}
			
			others = othersMap.get(DISEASE_TYPE_LIST);
			if(StringHelper.isNotEmpty(others)){
				if(view.getHealthAspects() != null && view.getHealthAspects().getDiseaseTypeId() != null &&view.getHealthAspects().getDiseaseTypeId().compareTo(others) == 0 && StringHelper.isEmpty(view.getHealthAspects().getDiseaseTypeDesc())){
					errorMessages.add(CommonUtil.getBundleMessage("researchFrom.errMsg.descforDiseaseType"));
					hasErrors = true;
				}
			}
		}
		return hasErrors;
	}
	
	private boolean hasFormError()throws Exception
	{
		boolean hasErrors=false;
		
		if(!isContextFamilyVillage() &&
			(StringUtils.isBlank(view.getIncomeCategory()) || view.getIncomeCategory().equals("-1") )
		  )
		{
			errorMessages.add(CommonUtil.getBundleMessage("researchFrom.errMsg.incomeCategory"));
			hasErrors = true;
		}
		return hasErrors;
	}
	public void onAdultFromMinor() 
	{
		errorMessages= new ArrayList<String>(0);
		successMessages= new ArrayList<String>(0);
		try 
		{
			
			updateValuesFromMaps();
			
			CommonUtil.onAdultFromMinor( view.getPersonId() , getLoggedInUserId() );
			view.setMinor(false);
			successMessages.add(ResourceUtil.getInstance().getProperty("common.person.AdultFromMinor"));
			updateValuesToMaps();
		} 
		catch (Exception exception) 
		{			
			logger.LogException( "onAdultFromMinor --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void onMinorFromAdult() 
	{
		errorMessages= new ArrayList<String>(0);
		successMessages= new ArrayList<String>(0);
		try 
		{
			updateValuesFromMaps();
			
			CommonUtil.onMinorFromAdult( view.getPersonId() , getLoggedInUserId() );
			view.setMinor(true);
			successMessages.add(ResourceUtil.getInstance().getProperty("common.person.MinorFromAdult"));
			updateValuesToMaps();
		} 
		catch (Exception exception) 
		{			
			logger.LogException( "onMinorFromAdult --- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void onOpenAssetWiseRevenueDetailsPopup()
	{
		errorMessages  = new ArrayList<String>(0);
		
		try
		{
			updateValuesFromMaps();
			sessionMap.put(WebConstants.PERSON_ID, view.getPersonId() );
			sessionMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_ID , view.getInhFileId() );
			executeJavaScript("javaScript:openAssetWiseRevenueDetailsPopup();");
	
		}
		catch(Exception e)
		{
			logger.LogException("onOpenAssetWiseRevenueDetailsPopup|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public void onSave()
	{
		errorMessages  = new ArrayList<String>(0);
		successMessages= new ArrayList<String>(0);
		try
		{
			updateValuesFromMaps();
			if( !hasError() )
			{
				if(isEmptyAccAspect())
				{
					view.setSaveAccAspect(false);
				}
				if( isEmptyMonthlyIncome()  )
				{
					view.setSaveMonthlyIncome(false);
				}
				if(isEmptyMonthlyExpenses() )
				{
					view.setSaveMonthlyExpense( false);
				}
				if(isEmptyAnnualIncome()  )
				{
					view.setSaveAnnualIncome( false);
				}
				if(isEmptyEduAspect())
				{
					view.setSaveEduAspect(false);
				}
				if(isEmptyHealthAspect())
				{
					view.setSaveHealthAspect(false);
				}
				service.saveResearchForm( view );
				if(personId != 0)
					view = service.getResearchFormData(view, personId);
				successMessages.add(ResourceUtil.getInstance().getProperty("common.messages.Save"));
				view.setLoggedInUser(CommonUtil.getLoggedInUser());
				view.setSaveMonthlyIncome(true);
				view.setSaveMonthlyExpense( true);
				view.setSaveAnnualIncome( true );
				view.setSaveAccAspect(true);
				view.setSaveEduAspect(true);
				view.setSaveHealthAspect(true);
			}

				updateValuesToMaps();
		}
		catch(Exception e)
		{
			logger.LogException("onSave|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void loadLists() throws PimsBusinessException,Exception 
	{
		if(  view.getInhFileId() != null  && view.getInhFileId().longValue() > 0 )
		{
		 loadCopyBeneficiaryList();
		}
		loadAccTypeList();
		loadAccOwnershipList();
		loadResTypeList();
		loadBeneficiaryOfProperty();
		loadTypesOfHome();
		loadConditionOfHome();
		loadReasonsForRentingList();
		loadIncomeCategory(); 
		loadHobbies();
		loadGrade();
		loadHealthStatus();
		loadEducationLevel();
		loadOrphanCategories();
		loadDiseaseType();
	}

	private void loadDiseaseType() throws Exception {
		List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<DiseaseType> diseases = us.getAllDiseaseType();
		for (DiseaseType disease : diseases) {
			SelectItem item = new SelectItem();
			item.setValue(disease.getDiseaseTypeId().toString());
			if (CommonUtil.getIsEnglishLocale())
				item.setLabel(disease.getDescriptionEn());
			else
				item.setLabel(disease.getDescriptionAr());
			selectItem.add(item);
			if( disease.getKey()!= null && disease.getKey().equals(WebConstants.OTHERS))
			{
				othersMap.put(DISEASE_TYPE_LIST, disease.getDiseaseTypeId().toString() );
			}
		}
		Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		this.setDiseaseTypeList(selectItem);
	}
	private void loadOrphanCategories() throws Exception {
		List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<OrphanCategory> orphanCats = us.getAllOrphanCategories();
		for (OrphanCategory cat : orphanCats) {
			SelectItem item = new SelectItem();
			item.setValue(cat.getOrphanCategoryId().toString());
			if (CommonUtil.getIsEnglishLocale())
				item.setLabel(cat.getDescriptionEn());
			else
				item.setLabel(cat.getDescriptionAr());
			selectItem.add(item);
			if( cat.getKey()!= null && cat.getKey().equals(WebConstants.OTHERS))
			{
				othersMap.put(ORPHAN_CAT_LIST, cat.getOrphanCategoryId().toString() );
			}
		}
		Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		this.setOrphanCatlist(selectItem);
	}
	private void loadEducationLevel() throws Exception {
		List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<EducationLevel> educationLevels = us.getAllEducationLevel();
		for (EducationLevel level : educationLevels) {
			SelectItem item = new SelectItem();
			item.setValue(level.getEducationLevelId().toString());
			if (CommonUtil.getIsEnglishLocale())
				item.setLabel(level.getDescriptionEn());
			else
				item.setLabel(level.getDescriptionAr());
			selectItem.add(item);
			if( level.getKey()!= null && level.getKey().equals(WebConstants.OTHERS))
			{
				othersMap.put(EDUCATION_LEVEL_LIST, level.getEducationLevelId().toString() );
			}
		}
		Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		this.setEducationLevelList(selectItem);
		
	}
	private void loadHealthStatus() throws Exception {
		 List<SelectItem> selectItem = new ArrayList<SelectItem>();
			List<HealthStatus> healthStatuses = us.getAllHealthStatus();
			for (HealthStatus healthStatus : healthStatuses) {
				SelectItem item = new SelectItem();
				item.setValue(healthStatus.getHealthStatusId().toString());
				if (CommonUtil.getIsEnglishLocale())
					item.setLabel(healthStatus.getDescriptionEn());
				else
					item.setLabel(healthStatus.getDescriptionAr());
				selectItem.add(item);
				if( healthStatus.getKey()!= null && healthStatus.getKey().equals(WebConstants.OTHERS))
				{
					othersMap.put(HEALTH_STATUS_LIST, healthStatus.getHealthStatusId().toString() );
				}
			}
			Collections.sort(selectItem, ListComparator.LIST_COMPARE);
			this.setHealthStatusList(selectItem);
	}
	private void loadGrade() throws Exception {
		 List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<Grade> grades = us.getAllGrades();
		for (Grade grade : grades) {
			SelectItem item = new SelectItem();
			item.setValue(grade.getGradeId().toString());
			if (CommonUtil.getIsEnglishLocale())
				item.setLabel(grade.getDescriptionEn());
			else
				item.setLabel(grade.getDescriptionAr());
			selectItem.add(item);
			if( grade.getKey()!= null && grade.getKey().equals(WebConstants.OTHERS))
			{
				othersMap.put(GRADE_LIST, grade.getGradeId().toString() );
			}
		}
		Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		this.setGradeList(selectItem);
		
	}
	private void loadHobbies() throws Exception {
		List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<HobbyType> hobbies = us.getAllHobbies();
		for (HobbyType hobby : hobbies) 
		{
			SelectItem item = new SelectItem();
			item.setValue(hobby.getHobbyTypeId().toString());
			if (CommonUtil.getIsEnglishLocale())
				item.setLabel(hobby.getDescriptionEn());
			else
				item.setLabel(hobby.getDescriptionAr());
			selectItem.add(item);
			if( hobby.getKey()!= null && hobby.getKey().equals(WebConstants.OTHERS))
			{
				othersMap.put(HOBBIES_LIST, hobby.getHobbyTypeId().toString() );
			}
		}
		Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		this.setHobbiesList(selectItem);
		
	}
	private void loadIncomeCategory() throws PimsBusinessException
	{

		List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<DomainDataView> domainData = service.getIncomeCategoryList();
		for (DomainDataView domainDataView : domainData) 
		{
			SelectItem item = new SelectItem();
			item.setValue(domainDataView.getDomainDataId().toString());
			if (CommonUtil.getIsEnglishLocale())
				item.setLabel(domainDataView.getDataDescEn());
			else
				item.setLabel(domainDataView.getDataDescAr());
			selectItem.add(item);
		}
		Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		this.setIncomeCategoryList(selectItem);

	}
	public List<SelectItem> getIncomeCategoryList() {
		return incomeCategoryList;
	}

	public void setIncomeCategoryList(List<SelectItem> incomeCategoryList) {
		this.incomeCategoryList = incomeCategoryList;
		if ( this.incomeCategoryList != null)
		{
			viewMap.put(INCOME_CATEGORY_LIST,this.incomeCategoryList );
		}
		
	}

	private void loadReasonsForRentingList() throws Exception
	{
		List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<RentingReason> reasonsForRentingPojo = service.getReasonsForRentingList();
		if (reasonsForRentingPojo != null && reasonsForRentingPojo.size() > 0) 
		{
			for (RentingReason rentingReason : reasonsForRentingPojo) 
			{
				SelectItem item = new SelectItem();
				if (CommonUtil.getIsEnglishLocale()) 
				{
					
					item = new SelectItem(rentingReason.getRentingReasonId().toString(), rentingReason.getReasonNameEn());
				}
				else
				{
					item = new SelectItem(rentingReason.getRentingReasonId().toString(), rentingReason.getReasonNameAr());
				}
			
				if( rentingReason.getKey()!= null && rentingReason.getKey().equals(WebConstants.OTHERS))
				{
					othersMap.put(RENTING_REASON_LIST, rentingReason.getRentingReasonId().toString() );
				}
				selectItem.add(item);
			}
			Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		}

		setRentingReasonList( selectItem );

	}
	public List<SelectItem> getRentingReasonList() {
		return rentingReasonList;
	}

	public void setRentingReasonList(List<SelectItem> rentingReasonList) {
		this.rentingReasonList = rentingReasonList;
		if(this.rentingReasonList != null)
		{
			viewMap.put(RENTING_REASON_LIST, this.rentingReasonList);
		}
	}


	private void loadConditionOfHome() throws Exception
	{
		List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<HomeConditionType> homeConditionPojo = service.getConditonOfHomeList();
		if (homeConditionPojo != null && homeConditionPojo.size() > 0) 
		{
			for (HomeConditionType homecondition : homeConditionPojo) 
			{
				SelectItem item = new SelectItem();
				if (CommonUtil.getIsEnglishLocale()) 
				{
					item = new SelectItem(homecondition.getTypeId().toString(),homecondition.getTypeNameEn());
				}
				else
				{
					item = new SelectItem(homecondition.getTypeId().toString(),homecondition.getTypeNameAr());
				}
				if( homecondition.getKey()!= null && homecondition.getKey().equals(WebConstants.OTHERS) )
				{
					othersMap.put(HOME_CONDITION_LIST, homecondition.getTypeId().toString() );
				}
				
				selectItem.add(item);
			}
			Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		}

		this.setHomeConditionList(selectItem);

	}
	public List<SelectItem> getHomeConditionList() {
		return homeConditionList;
	}

	public void setHomeConditionList(List<SelectItem> homeConditionList) {
		this.homeConditionList = homeConditionList;
		if(  this.homeConditionList != null)
			viewMap.put(HOME_CONDITION_LIST ,this.homeConditionList);
	}

	private void loadTypesOfHome() throws Exception
	{
		List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<HomeType> typesOfHomePojoList = service.getTypesOfHomeList();
		if (typesOfHomePojoList != null && typesOfHomePojoList.size() > 0) 
		{
			for (HomeType pojo : typesOfHomePojoList) 
			{
				SelectItem item = new SelectItem();
				if (CommonUtil.getIsEnglishLocale()) 
				{
					item = new SelectItem(pojo.getTypeId().toString(),pojo.getTypeNameEn());
				}
				else
				{
					item = new SelectItem(pojo.getTypeId().toString(),pojo.getTypeNameAr());
				}
				if( pojo.getKey()!= null && pojo.getKey().equals(WebConstants.OTHERS) )
				{
					othersMap.put(HOME_TYPE_LIST , pojo.getTypeId().toString() );
				}
				
				selectItem.add(item);
			}
			Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		}

		this.setHomeTypeList(selectItem);
	}	
	private void loadCopyBeneficiaryList() throws Exception
	{
		List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<BeneficiariesPickListView> beneficariesList = new InheritedAssetService().getBeneficiariesView( view.getInhFileId() );
		if ( beneficariesList != null && beneficariesList.size() > 0) 
		{
			for (BeneficiariesPickListView vo : beneficariesList) 
			{
				if( vo.getPersonId().longValue() !=  view.getPersonId().longValue() )
				{	
				 SelectItem item = new SelectItem();
				 item = new SelectItem( vo.getPersonId().toString(), vo.getFullName());
				 selectItem.add(item);
				}
			}
			Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		}

		this.setCopyBeneficiaryList( selectItem );
		
	}	
	public List<SelectItem> getHomeTypeList() {
		return homeTypeList;
	}

	public void setHomeTypeList(List<SelectItem> homeTypeList) {
		this.homeTypeList = homeTypeList;
		if( this.homeTypeList != null)
		{
			viewMap.put(HOME_TYPE_LIST, this.homeTypeList );
		}
	}


	private void loadBeneficiaryOfProperty()throws Exception
	{
		List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<PropertyBeneficiaryType> propertyBeneficiaryPojoList = service.getBeneficiaryPropertyList();
		if (propertyBeneficiaryPojoList != null && propertyBeneficiaryPojoList.size() > 0) 
		{
			for (PropertyBeneficiaryType pojo : propertyBeneficiaryPojoList) 
			{
				SelectItem item = new SelectItem();
				if (CommonUtil.getIsEnglishLocale()) 
				{
					item = new SelectItem(pojo.getTypeId().toString(),pojo.getTypeNameEn());
				} 
				else
				{
					item = new SelectItem(pojo.getTypeId().toString(),pojo.getTypeNameAr());
				}
				if( pojo.getKey()!= null && pojo.getKey().equals(WebConstants.OTHERS) )
				{
					othersMap.put(BENEFICIARY_PROP_LIST , pojo.getTypeId().toString() );
				}
				selectItem.add(item);
			}
			Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		}

		this.setBeneficiaryPropList( selectItem);

	}
	public List<SelectItem> getBeneficiaryPropList() {
		return beneficiaryPropList;
	}

	public void setBeneficiaryPropList(List<SelectItem> beneficiaryPropList) {
		this.beneficiaryPropList = beneficiaryPropList;
		if( this.beneficiaryPropList != null)
		{
			viewMap.put(BENEFICIARY_PROP_LIST,this.beneficiaryPropList );
		}
	}



	private void loadResTypeList() throws Exception
	{
		List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<ResidenceType> residentTypePojoList = service.getResidentTypeList();
		if (residentTypePojoList != null && residentTypePojoList.size() > 0) 
		{
			for (ResidenceType pojo : residentTypePojoList) 
			{
				SelectItem item = new SelectItem();
				if (CommonUtil.getIsEnglishLocale()) 
				{
					item = new SelectItem(pojo.getTypeId().toString(),
					pojo.getTypeNameEn());
				} 
				else
				{
					item = new SelectItem(pojo.getTypeId().toString(),
					pojo.getTypeNameAr());
				}
				if( pojo.getKey()!= null && pojo.getKey().equals(WebConstants.OTHERS) )
				{
					othersMap.put(RES_TYPE_LIST, pojo.getTypeId().toString() );
				}
				selectItem.add(item);
			}
			Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		}

		this.setResTypeList(selectItem);

	}
	public List<SelectItem> getResTypeList() {
		return resTypeList;
	}

	public void setResTypeList(List<SelectItem> resTypeList) {
		this.resTypeList = resTypeList;
		if( this.resTypeList != null)
		{
			viewMap.put(RES_TYPE_LIST, resTypeList);
		}
	}


	private void loadAccOwnershipList() throws Exception
	{
		List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<AccomodationOnwershipType> accomodationOwnerPojoList = service.getAccomodationOwnerList();
		if (accomodationOwnerPojoList != null && accomodationOwnerPojoList.size() > 0) 
		{
			for (AccomodationOnwershipType pojo : accomodationOwnerPojoList) 
			{
				SelectItem item = new SelectItem();
				if (CommonUtil.getIsEnglishLocale()) 
				{
					item = new SelectItem(pojo.getTypeId().toString(), pojo.getTypeNameEn());
				} 
				else
				{
					item = new SelectItem(pojo.getTypeId().toString(), pojo.getTypeNameAr());
				}
				if( pojo.getKey()!= null && pojo.getKey().equals(WebConstants.OTHERS) )
				{
					othersMap.put(ACC_OWNERSHIP_LIST, pojo.getTypeId().toString() );
				}
				selectItem.add(item);
			}
			Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		}

		this.setAccOwnershipList(selectItem);

	}
	public List<SelectItem> getAccOwnershipList() {
		return accOwnershipList;
	}

	public void setAccOwnershipList(List<SelectItem> accOwnershipList) {
		this.accOwnershipList = accOwnershipList;
		if( this.accOwnershipList != null )
		{
			viewMap.put(ACC_OWNERSHIP_LIST, this.accOwnershipList);
		}
	}

	private void loadAccTypeList()throws Exception 
	{

		List<SelectItem> selectItem = new ArrayList<SelectItem>();
		List<AccomodationType> accomodationTypePojoList = service.getAccomodationTypeList();
		if (accomodationTypePojoList != null && accomodationTypePojoList.size() > 0) 
		{
			
			for (AccomodationType pojo : accomodationTypePojoList) {
				SelectItem item = new SelectItem();
				if (CommonUtil.getIsEnglishLocale()) 
				{
					item = new SelectItem(pojo.getTypeId().toString(), pojo.getTypeNameEn());
				} 
				else
				{
					item = new SelectItem(pojo.getTypeId().toString(), pojo.getTypeNameAr());
				}
				if( pojo.getKey()!= null && pojo.getKey().equals(WebConstants.OTHERS) )
				{
					othersMap.put(ACC_TYPE_LIST, pojo.getTypeId().toString() );
				}
				selectItem.add(item);
			}
			Collections.sort(selectItem, ListComparator.LIST_COMPARE);
		}

		this.setAccTypeList(selectItem);

	}

	public List<SelectItem> getAccTypeList() {
		return accTypeList;
	}

	public void setAccTypeList(List<SelectItem> accTypeList) {
		this.accTypeList = accTypeList;
		if(this.accTypeList!= null)
		{
			viewMap.put(ACC_TYPE_LIST,this.accTypeList);
		}
	}

	public ResearchFormBeneficiaryView getView() {
		return view;
	}

	public void setView(ResearchFormBeneficiaryView view) {
		this.view = view;
	}
	public Boolean getIsViewModePopUp() {
		return isViewModePopUp;
	}
	public void setIsViewModePopUp(Boolean isViewModePopUp) {
		this.isViewModePopUp = isViewModePopUp;
	}
	public Long getInheritanceFileId() {
		return inheritanceFileId;
	}
	public void setInheritanceFileId(Long inheritanceFileId) {
		this.inheritanceFileId = inheritanceFileId;
	}
	public List<SelectItem> getHobbiesList() {
		return hobbiesList;
	}
	public void setHobbiesList(List<SelectItem> hobbiesList) {
		this.hobbiesList = hobbiesList;
		if(this.hobbiesList!= null)
		{
			viewMap.put(HOBBIES_LIST,this.hobbiesList);
		}
	}
	public List<SelectItem> getOrphanCatlist() {
		return orphanCatlist;
	}
	public void setOrphanCatlist(List<SelectItem> orphanCatlist) {
		this.orphanCatlist = orphanCatlist;
		if(this.orphanCatlist!= null)
		{
			viewMap.put(ORPHAN_CAT_LIST,this.orphanCatlist);
		}
	}
	public List<SelectItem> getDiseaseTypeList() {
		return diseaseTypeList;
	}
	public void setDiseaseTypeList(List<SelectItem> diseaseTypeList) {
		this.diseaseTypeList = diseaseTypeList;
		if(this.diseaseTypeList!= null)
		{
			viewMap.put(DISEASE_TYPE_LIST,this.diseaseTypeList);
		}
	}
	public List<SelectItem> getEducationLevelList() {
		return educationLevelList;
	}
	public void setEducationLevelList(List<SelectItem> educationLevelList) {
		this.educationLevelList = educationLevelList;
		if(this.educationLevelList!= null)
		{
			viewMap.put(EDUCATION_LEVEL_LIST,this.educationLevelList);
		}
	}
	public List<SelectItem> getGradeList() {
		return gradeList;
	}
	public void setGradeList(List<SelectItem> gradeList) {
		this.gradeList = gradeList;
		if(this.gradeList!= null)
		{
			viewMap.put(GRADE_LIST,this.gradeList);
		}
	}
	public List<SelectItem> getHealthStatusList() {
		return healthStatusList;
	}
	public void setHealthStatusList(List<SelectItem> healthStatusList) {
		this.healthStatusList = healthStatusList;
		if(this.healthStatusList!= null)
		{
			viewMap.put(HEALTH_STATUS_LIST,this.healthStatusList);
		}
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}
	public boolean isOpenedForOwner() {
		if(viewMap.get(IS_OPENED_FOR_OWNER) != null){
			return (Boolean) viewMap.get(IS_OPENED_FOR_OWNER);
		}
		return false;
	}
	public void setOpenedForOwner(boolean openedForOwner) {
		this.openedForOwner = openedForOwner;
	}
	public List<SelectItem> getCopyBeneficiaryList() {
		return copyBeneficiaryList;
	}
	public void setCopyBeneficiaryList(List<SelectItem> copyBeneficiaryList) {
		this.copyBeneficiaryList = copyBeneficiaryList;
		if( this.copyBeneficiaryList != null && this.copyBeneficiaryList.size() > 0 )
		{
			viewMap.put( COPY_BEN_LIST, this.copyBeneficiaryList);
			
		}
		setCopyBeneficiaryListSize(this.copyBeneficiaryList != null ?copyBeneficiaryList.size() : 0);
	}
	
	public Integer getCopyBeneficiaryListSize() {
		return copyBeneficiaryListSize;
	}
	public void setCopyBeneficiaryListSize(Integer copyBeneficiaryListSize) {
		this.copyBeneficiaryListSize = copyBeneficiaryListSize;
		if( this.copyBeneficiaryListSize != null )
		{
			viewMap.put( COPY_BEN_LIST_SIZE, this.copyBeneficiaryListSize);
		}
	}
public void deleteAccomodationAspects(){
		
		
		try {
			AccomodationAspectsView recordToDeleted = (AccomodationAspectsView) accomodationAspectsDT.getRowData();
			long idToBeDeleted = Long.parseLong(recordToDeleted.getAccomodationAspectsId());
			service.deleteAccomodationAspects(idToBeDeleted);
			view.getAccomdationAspectsList().remove(recordToDeleted);
			if(view.getAccomdationAspectsList() != null && view.getAccomdationAspectsList().size() > 0 && view.getAccomdationAspectsList().get(0) != null){
				view.setAccomdationAspects((AccomodationAspectsView) BeanUtils.cloneBean(view.getAccomdationAspectsList().get(0)));
			}
		} 
		catch (Exception e) {
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			logger.LogException("deleteAccomodationAspects|Error Occured", e);
		}
	}
	public void deleteEducationAspects(){
		
		
		try {
			EducationAspectsView recordToDeleted = (EducationAspectsView) educationAspectDT.getRowData();
			long idToBeDeleted = recordToDeleted.getEducationAspectId();
			service.deleteEducationnAspects(idToBeDeleted);
			view.getEducationAspectsList().remove(recordToDeleted);
			if(view.getEducationAspectsList() != null && view.getEducationAspectsList().size() > 0 && view.getEducationAspectsList().get(0) != null){
				view.setEducationAspects((EducationAspectsView) BeanUtils.cloneBean(view.getEducationAspectsList().get(0)));
			}
		} 
		catch (Exception e) {
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			logger.LogException("deleteEducationAspects|Error Occured", e);
		}
	}
public void deleteHealthAspects(){
		
		
		try {
			HealthAspectsView recordToDeleted = (HealthAspectsView) healthAspectsDT.getRowData();
			long idToBeDeleted = recordToDeleted.getHealthAspectId();
			service.deleteHealthAspects(idToBeDeleted);
			view.getHealthAspectsList().remove(recordToDeleted);
			if(view.getHealthAspectsList() != null && view.getHealthAspectsList().size() > 0 && view.getHealthAspectsList().get(0) != null){
				view.setHealthAspects((HealthAspectsView) BeanUtils.cloneBean(view.getHealthAspectsList().get(0)));
			}
		} 
		catch (Exception e) {
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			logger.LogException("deleteHealthAspects|Error Occured", e);
		}
	}
public HtmlDataTable getAccomodationAspectsDT() {
	return accomodationAspectsDT;
}
public void setAccomodationAspectsDT(HtmlDataTable accomodationAspectsDT) {
	this.accomodationAspectsDT = accomodationAspectsDT;
}
public HtmlDataTable getEducationAspectDT() {
	return educationAspectDT;
}
public void setEducationAspectDT(HtmlDataTable educationAspectDT) {
	this.educationAspectDT = educationAspectDT;
}
public HtmlDataTable getHealthAspectsDT() {
	return healthAspectsDT;
}
public void setHealthAspectsDT(HtmlDataTable healthAspectsDT) {
	this.healthAspectsDT = healthAspectsDT;
}
}
