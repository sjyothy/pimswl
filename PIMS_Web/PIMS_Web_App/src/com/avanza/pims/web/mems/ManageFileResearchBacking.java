package com.avanza.pims.web.mems;

import java.util.ArrayList;

import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;

import com.avanza.pims.entity.FileTypeResearchReq;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.ui.util.ResourceUtil;

public class ManageFileResearchBacking extends AbstractMemsBean {
	private static final long serialVersionUID = 1L;
	InheritanceFileService service = new InheritanceFileService();
	private HtmlSelectOneMenu cboFileType = new HtmlSelectOneMenu();
	private HtmlSelectBooleanCheckbox chkResearchRequired = new HtmlSelectBooleanCheckbox();

	public ManageFileResearchBacking() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		super.init();
		try {
			if (!isPostBack()) {
			}
		} catch (Exception es) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init" + "|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void onUpdate() {
		if (isValidated()) {
			try {
				if(viewMap.get("SELECT_FILE_TYPE_RESEARCH") != null){
					FileTypeResearchReq fileTypeResearchReq = (FileTypeResearchReq) viewMap.get("SELECT_FILE_TYPE_RESEARCH");
					fileTypeResearchReq.setIsResearchRequired(Boolean.parseBoolean(chkResearchRequired.getValue().toString()) ? 1L:0L);
					fileTypeResearchReq.setUpdatedBy(CommonUtil.getLoggedInUser());
					service.updateFileResearchReq(fileTypeResearchReq);
					successMessages.add(CommonUtil
							.getBundleMessage("successMsg.costCenterUpdated"));	
				}
			} catch (Exception e) {
				logger.LogException("onUpdate has been crahsed|", e);
			}
		}
		else{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.fileTypeReq"));
		}
	}

	private boolean isValidated() {

		if(cboFileType.getValue() == null || cboFileType.getValue().toString().equals("-1")){
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public void fileTypeChanged() {
		if (cboFileType.getValue() != null	&& cboFileType.getValue().toString().compareTo("-1") != 0) {
			try {
				FileTypeResearchReq fileTypeResearch = service.getFileTypeResearchReq(Long.parseLong(cboFileType.getValue().toString()));
				if (fileTypeResearch != null) {
					chkResearchRequired.setValue(fileTypeResearch.getIsResearchRequired().equals(1L) ? true : false);
					viewMap.put("SELECT_FILE_TYPE_RESEARCH", fileTypeResearch);
				}
			} 
			catch (Exception e) {
				errorMessages = new ArrayList<String>(0);
				logger.LogException("init" + "|Error Occured", e);
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		} else {
			chkResearchRequired.setValue(false);
		}
	}

	public HtmlSelectOneMenu getCboFileType() {
		return cboFileType;
	}

	public void setCboFileType(HtmlSelectOneMenu cboFileType) {
		this.cboFileType = cboFileType;
	}

	public HtmlSelectBooleanCheckbox getChkResearchRequired() {
		return chkResearchRequired;
	}

	public void setChkResearchRequired(
			HtmlSelectBooleanCheckbox chkResearchRequired) {
		this.chkResearchRequired = chkResearchRequired;
	}

}
