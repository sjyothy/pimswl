package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.vo.mems.InheritedAssetShareInfoView;
import com.avanza.ui.util.ResourceUtil;

  public class AssetWiseRevenueDetailsPopup extends AbstractMemsBean 
  {
		private static final long serialVersionUID = 1L;
	    Long personId;
	    Long assetTypeId;
	    Long inheritanceFileId;
		private HtmlDataTable dataTable;
		Double monthlyRevenue = 0.00d;
		Double annualRevenue = 0.00d;
	    List<InheritedAssetShareInfoView> dataList = new ArrayList<InheritedAssetShareInfoView>();
	    
		@Override
		@SuppressWarnings("unchecked")
		public void init() 
		{
			try 
			{
				if( !isPostBack() )
				{
					getDataFromParent();
					getAssetWiseReveuesForPersonId ();
				}
			} 
			catch (Exception exception) 
			{
				logger.LogException("init --- CRASHED --- ", exception);
				errorMessages = new ArrayList<String>();
				errorMessages.add(ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			}
		}
		
		@SuppressWarnings("unchecked")
		private void getAssetWiseReveuesForPersonId( )throws Exception
		{
			dataList = InheritanceFileService.getDetailedAssetWiseReveuesForPersonId(  personId, assetTypeId );
			for (InheritedAssetShareInfoView obj : dataList) 
			{
			  monthlyRevenue += obj.getMonthlyRevenue();	
			  annualRevenue  += obj.getAnnualRevenue();
			}
		}
		
		@SuppressWarnings("unchecked")
		private void getDataFromParent()throws Exception 
		{
			if( sessionMap.get(WebConstants.PERSON_ID) != null )
			{
				personId = new Long( sessionMap.remove( WebConstants.PERSON_ID ).toString() );
			}
			if( sessionMap.get("assetTypeId") != null )
			{
				assetTypeId = new Long( sessionMap.remove( "assetTypeId" ).toString() );
			}
			if( sessionMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null )
			{
				inheritanceFileId = new Long( sessionMap.remove( WebConstants.InheritanceFile.INHERITANCE_FILE_ID ).toString() );
			}
		}
		
		public List<InheritedAssetShareInfoView> getDataList() {
			return dataList;
		}
		
		public void setDataList(List<InheritedAssetShareInfoView> dataList) {
			this.dataList = dataList;
		}
		
		public HtmlDataTable getDataTable() {
			return dataTable;
		}
		
		public void setDataTable(HtmlDataTable dataTable) {
			this.dataTable = dataTable;
		}

		public Double getMonthlyRevenue() {
			return monthlyRevenue;
		}

		public void setMonthlyRevenue(Double monthlyRevenue) {
			this.monthlyRevenue = monthlyRevenue;
		}

		public Double getAnnualRevenue() {
			return annualRevenue;
		}

		public void setAnnualRevenue(Double annualRevenue) {
			this.annualRevenue = annualRevenue;
		}

		public Long getAssetTypeId() {
			return assetTypeId;
		}

		public void setAssetTypeId(Long assetTypeId) {
			this.assetTypeId = assetTypeId;
		}

  }
