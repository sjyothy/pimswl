package com.avanza.pims.web.mems.plugins;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;

import org.apache.myfaces.component.html.ext.HtmlPanelGrid;
import org.apache.myfaces.custom.collapsiblepanel.HtmlCollapsiblePanelTag;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.BeneficiarySharesTabController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.mems.minors.InheritanceFileBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.SearchAssetsService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.SearchAssetsCriteriaView;
import com.avanza.pims.ws.vo.mems.AssetMemsView;
import com.avanza.pims.ws.vo.mems.AssetTypeView;
import com.avanza.pims.ws.vo.mems.InheritedAssetShareInfoView;
import com.avanza.pims.ws.vo.mems.InheritedAssetView;
import com.avanza.pims.ws.vo.mems.MemsAssetCncrndDeptView;
import com.avanza.ui.util.ResourceUtil;

public class InheritedAssetsTabBean extends AbstractMemsBean {
	private static final long serialVersionUID = 1L;
	private HtmlDataTable inheritedAssetsDataTable;
	private HtmlSelectOneMenu cboAssetType = new HtmlSelectOneMenu();
	private HtmlCollapsiblePanelTag collapsiblePanel = new HtmlCollapsiblePanelTag();
	private final String  ASSET_TYPE_ID = "ASSET_TYPE_ID";
	private boolean isPension;
	private boolean isTransportation;
	private boolean isJewellery;
	private boolean isCash;
	private boolean isCheque;
	private boolean isVehciles;
	private boolean isAnimals;
	private boolean isLandProperties;
	private boolean isLicenses;
	private boolean isStockshares;
	private boolean isLoanLiabilities;
	private boolean isBankTransfer;
	private boolean isShowSearchManagerImage;
	private HtmlInputText txtManagerName = new HtmlInputText();
	private HtmlSelectBooleanCheckbox revChk = new HtmlSelectBooleanCheckbox();
	private InheritedAssetView inheritedAssetView =  new  InheritedAssetView();
	private AssetMemsView assetMemsView  = new AssetMemsView();
	private MemsAssetCncrndDeptView memsAssetCncrndDeptView = new  MemsAssetCncrndDeptView();
	private List<InheritedAssetView> inheritedAssetList = new ArrayList<InheritedAssetView>();
	private final String ASSET_MEMS_VIEW = "ASSET_MEMS_VIEW";
	private final String INHERITED_ASSET_MEMS_VIEW_LIST = "INHERITED_ASSET_MEMS_VIEW_LIST";
	private final String MEMS_CNRCRND_DEPT_VIEW = "MEMS_CNRCRND_DEPT_VIEW";
	private final String INHERITED_ASSET_VIEW = "INHERITED_ASSET_VIEW";
//	private final String REVENUE_TYPE_MONTHLY = "REVENUE_TYPE_MONTHLY";
//	private final String REVENUE_TYPE_YEARLY = "REVENUE_TYPE_YEARLY";
	private final String REVENUE_TYPE_MAP = "REVENUE_TYPE_MAP";
	private final String ASSET_TYPE_MAP = "ASSET_TYPE_MAP";
	private final String PAYMENT_STATUS_PENDING = "PAYMENT_STATUS_PENDING";
	private final String SELECTED_INDEX = "SELECTED_INDEX";
	private final String IS_ASSET_SELECTED_FROM_SEARCH= "IS_ASSET_SELECTED_FROM_SEARCH";
	private final String IS_GENERATES_REVENUE= "IS_GENERATES_REVENUE";
//	private String INH_ASSET_MEMS_ROW = "INH_ASSET_MEMS_ROW";
	private List<DomainDataView> revenueTypeListDD = new ArrayList<DomainDataView>();
	private HashMap<Long, DomainDataView> revenueTypeMap = new HashMap<Long, DomainDataView>();
	private HashMap<Long, String> assetTypeMap = new HashMap<Long, String>();
	private Map<String,MemsAssetCncrndDeptView> assetTypeCncrndDepttMap = new HashMap<String, MemsAssetCncrndDeptView>();
	private HtmlInputText txtExpRev = new HtmlInputText();
	private HtmlSelectOneMenu cboRevType = new HtmlSelectOneMenu();
	private HtmlCalendar clndrFromDate = new HtmlCalendar();
	private HtmlInputText txtAssetTotalShare = new HtmlInputText();
	private HtmlSelectBooleanCheckbox chkIsManagerAmaf = new HtmlSelectBooleanCheckbox();
	DomainDataView revenueTypeMonthlyDD = new DomainDataView();
	DomainDataView revenueTypeYearlyDD = new DomainDataView();
	DomainDataView paymentStatusPendingDD = new DomainDataView();
	private HtmlPanelGrid panelAssetTypeDetails = new HtmlPanelGrid();
	InheritanceFileBean parentBean;
	boolean isMigratedFile = false ;
	NumberFormat formatter = new DecimalFormat("#.##");
	@SuppressWarnings("unchecked")
	public void init() 
	{
		super.init();
		try
		{
			
			txtExpRev.setDisabled(true);
			clndrFromDate.setDisabled(true);
			cboRevType.setDisabled(true);
			if (!isPostBack()) 
			{
				loadCombos();
			}
			parentBean = (InheritanceFileBean) getBean("pages$inheritanceFile");
			isMigratedFile = parentBean.getIsMigratedFile();
	 }
		catch (Exception e)
		{
			logger.LogException("init() crashed | ",e);
		}
		
	}

	private void loadCombos() throws PimsBusinessException {
		loadRevenueTypeList();
		
		loadPendingPaymentStatus();
			
		loadAssetTypes();
	}

	private void loadPendingPaymentStatus() {
		paymentStatusPendingDD = CommonUtil.getIdFromType(CommonUtil.getDomainDataListForDomainType(WebConstants.PAYMENT_SCHEDULE_STATUS), WebConstants.PAYMENT_SCHEDULE_STATUS_PENDING);
		if(paymentStatusPendingDD != null)
		{
			viewMap.put(PAYMENT_STATUS_PENDING, paymentStatusPendingDD);
		}
	}

	private void loadAssetTypes() throws PimsBusinessException {
		List<SearchAssetsCriteriaView> assetTypes = new SearchAssetsService().getAssetTypesList();
		if(assetTypes != null && assetTypes.size() > 0)
		{
			for(SearchAssetsCriteriaView assetType : assetTypes)
			{
				assetTypeMap.put(assetType.getAssetTypeid(),CommonUtil.getIsEnglishLocale()? assetType.getAssetNameEn():assetType.getAssetNameAr());
				
				assetTypeCncrndDepttMap.put(assetType.getAssetTypeid().toString(),assetType.getConcernedDepartment());
			}
			
			setAssetTypeCncrndDepttMap(assetTypeCncrndDepttMap);
			if(assetTypeMap != null && assetTypeMap.size() > 0)
			{
				viewMap.put(ASSET_TYPE_MAP, assetTypeMap);
			}
		}
	}

	private void loadRevenueTypeList() {
		revenueTypeListDD = CommonUtil.getDomainDataListForDomainType(WebConstants.InheritanceFile.ASSET_REVENUE_TYPE);
		if(revenueTypeListDD != null && revenueTypeListDD.size() > 0)
		{
			for(DomainDataView revenueDD : revenueTypeListDD)
			{
				revenueTypeMap.put(revenueDD.getDomainDataId(), revenueDD);
			}
			if(revenueTypeMap != null && revenueTypeMap.size() > 0)
			{
				viewMap.put(REVENUE_TYPE_MAP, revenueTypeMap);
			}
		}
	}
	
	@Override
	public void prerender() 
	{
		super.prerender();
	}
	@SuppressWarnings("unchecked")
	public void editAsset()
	{
		try
		{
			if(getInheritedAssetList()!= null)
			{
				List<InheritedAssetView> iAssetList = getInheritedAssetList();
				InheritedAssetView iAssetToBeUpdated = (InheritedAssetView) inheritedAssetsDataTable.getRowData();
				int selectedRecordIndex = inheritedAssetsDataTable.getRowIndex();
				viewMap.put(SELECTED_INDEX, selectedRecordIndex);
				
				//This will deselect other asset
				for(InheritedAssetView iAsset : iAssetList)
				{
					iAsset.setIsSelected(false);//show edit icon for whole list
				}
				iAssetToBeUpdated.setIsSelected(true); //remove edit icons
				
				//This will bring the complete info from DB
				iAssetToBeUpdated = new InheritanceFileService().getInheritedAssetById(iAssetToBeUpdated.getInheritedAssetId());
				iAssetToBeUpdated.setUpdatedBy(getLoggedInUserId());
				if(iAssetToBeUpdated.getAssetMemsView().getAmount() != null){
					iAssetToBeUpdated.getAssetMemsView().setAmountString(formatter.format(iAssetToBeUpdated.getAssetMemsView().getAmount()));
				}
				//This code is written to remove the '.' if any from the value, coz value is considered as integer
				if(iAssetToBeUpdated.getTotalShareValue() != null)
				{
					
					int index = iAssetToBeUpdated.getTotalShareValue().toString().indexOf(".");
					if(index != -1)
					{
						String totalShareWitoutPrecision = iAssetToBeUpdated.getTotalShareValue().toString().substring(0, index);
						txtAssetTotalShare.setValue(totalShareWitoutPrecision);
					}
				}
				
				setInheritedAssetView(iAssetToBeUpdated);
				if(iAssetToBeUpdated.getAssetMemsView() != null && iAssetToBeUpdated.getAssetMemsView().getFromDate() != null)
					clndrFromDate.setValue(iAssetToBeUpdated.getAssetMemsView().getFromDate());
				
				setAssetMemsView(iAssetToBeUpdated.getAssetMemsView());
				if(iAssetToBeUpdated.getAssetMemsView().getManagerId() != null){
					txtManagerName.setValue(iAssetToBeUpdated.getAssetMemsView().getManagerName());
					chkIsManagerAmaf.setValue(false);
					viewMap.put("SHOW_SEARCH_MANAGER_IMAGE", true);
				}
				else{
					chkIsManagerAmaf.setValue(true);
					viewMap.put("SHOW_SEARCH_MANAGER_IMAGE", false);
				}
				setMemsAssetCncrndDeptView(new MemsAssetCncrndDeptView());
				
				if(iAssetToBeUpdated.getAssetMemsView().getConcernedDepartmentViewList() != null && iAssetToBeUpdated.getAssetMemsView().getConcernedDepartmentViewList().size() > 0)
				{
					setMemsAssetCncrndDeptView(iAssetToBeUpdated.getAssetMemsView().getConcernedDepartmentViewList().get(0));
				}
				assetTypeChanged();//to rerender related fields
				setInheritedAssetList(iAssetList);
			}
			//collapsiblePanel.
		}
		catch (Exception e) 
		{
			logger.LogException("EditFromGridAndDB() crashed", e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			viewMap.put(WebConstants.InherFileErrMsg.INHER_ASSET_ERR, errorMessages);
		}
	}

	public HtmlDataTable getInheritedAssetsDataTable() {
		return inheritedAssetsDataTable;
	}

	public void setInheritedAssetsDataTable(HtmlDataTable inheritedAssetsDataTable) {
		this.inheritedAssetsDataTable = inheritedAssetsDataTable;
	}
	@SuppressWarnings("unchecked")
	public void assetTypeChanged()
	{
		try
		{
			assetMemsView = getAssetMemsView();
			if(assetMemsView != null && assetMemsView.getAssetTypeView().getAssetTypeIdString().compareTo("-1") != 0)
			{
				Long assetTypeId =Long.parseLong(assetMemsView.getAssetTypeView().getAssetTypeIdString());
				viewMap.put(ASSET_TYPE_ID , assetTypeId);
				if(viewMap.get(SELECTED_INDEX) == null && !viewMap.containsKey(IS_ASSET_SELECTED_FROM_SEARCH))
				{
					clearAssetTypeRelatedFields(assetMemsView );
					MemsAssetCncrndDeptView cncrndDeptt =  new MemsAssetCncrndDeptView();
					if( getAssetTypeCncrndDepttMap() != null && getAssetTypeCncrndDepttMap().get(assetTypeId.toString()) != null )
					{
					   cncrndDeptt =  getAssetTypeCncrndDepttMap().get(assetTypeId.toString());
					}
					setMemsAssetCncrndDeptView(cncrndDeptt);
				}
			}
	    }
		catch ( Exception e )
		{
			logger.LogException(  "Error Occured::",e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			viewMap.put(WebConstants.InherFileErrMsg.INHER_ASSET_ERR, errorMessages);
		}
	}

	@SuppressWarnings("unchecked")
	private void clearAssetTypeRelatedFields(AssetMemsView assetMemsView) 
	{
		assetMemsView.setAssetId(null);
		assetMemsView.setFieldNumber(null);
		assetMemsView.setFieldStatus(null);
		assetMemsView.setAmountString(null);
		assetMemsView.setAmount(null);
		assetMemsView.setFieldParty(null);
		assetMemsView.setFieldType(null);
		assetMemsView.getBankView().setBankId(null);
		assetMemsView.getBankView().setBankIdString("-1");
		assetMemsView.getGovtDepttView().setGovtDepttId(null);
		assetMemsView.getGovtDepttView().setGovtDepttIdString("-1");
		assetMemsView.setAccountName(null);
		assetMemsView.setWeightString(null);
		assetMemsView.setWeight(null);
		assetMemsView.setVehicleCategory(null);
		assetMemsView.setRegionName(null);
		assetMemsView.setLicenseName(null);
		assetMemsView.setIsAmafAccountBool(false);
		assetMemsView.setCompany(null);
		assetMemsView.setDueDate(null);
		assetMemsView.setDevolution(null);
		assetMemsView.setAssetNumber(null);
		assetMemsView.setAssetTypeView(new AssetTypeView());
		assetMemsView.setAssetNameEn(null);
		assetMemsView.setAssetNameAr(null);
		assetMemsView.setDescription(null);
		assetMemsView.setIncomeExpbool(false);
		assetMemsView.setIsIncomeExp(0L);
		assetMemsView.setRevenueTypeIdString("-1");
		assetMemsView.setRevenueType(null);
		assetMemsView.setFromDate(null);
		assetMemsView.setExpectedRevenue(null);
		assetMemsView.setExpectedRevenueString(null);

		if(assetMemsView != null)
			setAssetMemsView(assetMemsView);
		
		revChk.setValue(false);
		cboRevType.setValue("-1");
		cboRevType.setDisabled(true);
		clndrFromDate.setValue(null);
		clndrFromDate.setDisabled(true);
		txtAssetTotalShare.setValue(null);
		txtManagerName.setValue(null);
		chkIsManagerAmaf.setValue(false);
		viewMap.put("SHOW_SEARCH_MANAGER_IMAGE", true);
		if(viewMap.get(INHERITED_ASSET_VIEW) != null)
			viewMap.remove(INHERITED_ASSET_VIEW);
		if(viewMap.get(WebConstants.InheritanceFile.MANAGER_VIEW) != null){
			viewMap.remove(WebConstants.InheritanceFile.MANAGER_VIEW);
		}
	}

	public HtmlSelectOneMenu getCboAssetType() {
		return cboAssetType;
	}

	public void setCboAssetType(HtmlSelectOneMenu cboAssetType) {
		this.cboAssetType = cboAssetType;
	}

	public boolean isPension() 
	{
		isPension = false;
		if(viewMap.get(ASSET_TYPE_ID) != null)
		{
			Long assetTypeId =Long.parseLong(viewMap.get(ASSET_TYPE_ID).toString());
			if(assetTypeId.compareTo(WebConstants.AssetType.PENSION) == 0)
				isPension = true;
		}
		return isPension;
	}

	public void setPension(boolean isPension) {
		this.isPension = isPension;
	}

	public boolean isTransportation() 
	{
		isTransportation = false;
		if(viewMap.get(ASSET_TYPE_ID) != null)
		{
			Long assetTypeId =Long.parseLong(viewMap.get(ASSET_TYPE_ID).toString());
			if(assetTypeId.compareTo(WebConstants.AssetType.TRANSPORTATIONS) == 0)
				isTransportation = true;
		}
		return isTransportation;
	}

	public void setTransportation(boolean isTransportation) {
		this.isTransportation = isTransportation;
	}

	public boolean isJewellery()
	{
		isJewellery = false;
		if(viewMap.get(ASSET_TYPE_ID) != null)
		{
			Long assetTypeId =Long.parseLong(viewMap.get(ASSET_TYPE_ID).toString());
			if(assetTypeId.compareTo(WebConstants.AssetType.JEWELLERY) == 0)
				isJewellery = true;
		}
		return isJewellery;
	}

	public void setJewellery(boolean isJewellery) {
		this.isJewellery = isJewellery;
	}

	public boolean isCash() 
	{
		isCash = false;
		if(viewMap.get(ASSET_TYPE_ID) != null)
		{
			Long assetTypeId =Long.parseLong(viewMap.get(ASSET_TYPE_ID).toString());
			if(assetTypeId.compareTo(WebConstants.AssetType.CASH) == 0)
				isCash = true;
		}
		return isCash;
	}

	public void setCash(boolean isCash) {
		this.isCash = isCash;
	}

	public boolean isCheque() 
	{
		isCheque = false;
		if(viewMap.get(ASSET_TYPE_ID) != null)
		{
			Long assetTypeId =Long.parseLong(viewMap.get(ASSET_TYPE_ID).toString());
			if(assetTypeId.compareTo(WebConstants.AssetType.CHEQUE) == 0)
				isCheque = true;
		}
		return isCheque;
	}

	public void setCheque(boolean isCheque) {
		this.isCheque = isCheque;
	}

	public boolean isVehciles() 
	{
		isVehciles = false;
		if(viewMap.get(ASSET_TYPE_ID) != null)
		{
			Long assetTypeId =Long.parseLong(viewMap.get(ASSET_TYPE_ID).toString());
			if(assetTypeId.compareTo(WebConstants.AssetType.VEHICLES) == 0)
				isVehciles = true;
		}
		return isVehciles;
	}

	public void setVehciles(boolean isVehciles) {
		this.isVehciles = isVehciles;
	}

	public boolean isAnimals() 
	{
		isAnimals = false;
		if(viewMap.get(ASSET_TYPE_ID) != null)
		{
			Long assetTypeId =Long.parseLong(viewMap.get(ASSET_TYPE_ID).toString());
			if(assetTypeId.compareTo(WebConstants.AssetType.ANIMAL) == 0)
				isAnimals = true;
		}
		return isAnimals;
	}

	public void setAnimals(boolean isAnimals) {
		this.isAnimals = isAnimals;
	}

	public boolean isLandProperties() 
	{
		isLandProperties = false;
		if(viewMap.get(ASSET_TYPE_ID) != null)
		{
			Long assetTypeId =Long.parseLong(viewMap.get(ASSET_TYPE_ID).toString());
			if(assetTypeId.compareTo(WebConstants.AssetType.LAND_PROPERTIES) == 0)
				isLandProperties = true;
		}
		return isLandProperties;
	}

	public void setLandProperties(boolean isLandProperties) {
		this.isLandProperties = isLandProperties;
	}

	public boolean isLicenses() 
	{
		isLicenses = false;
		if(viewMap.get(ASSET_TYPE_ID) != null)
		{
			Long assetTypeId =Long.parseLong(viewMap.get(ASSET_TYPE_ID).toString());
			if(assetTypeId.compareTo(WebConstants.AssetType.LICENSES) == 0)
				isLicenses = true;
		}
		return isLicenses;
	}

	public void setLicenses(boolean isLicenses) {
		this.isLicenses = isLicenses;
	}

	public boolean isStockshares() 
	{
		isStockshares = false;
		if(viewMap.get(ASSET_TYPE_ID) != null)
		{
			Long assetTypeId =Long.parseLong(viewMap.get(ASSET_TYPE_ID).toString());
			if(assetTypeId.compareTo(WebConstants.AssetType.STOCK_SHARES) == 0)
				isStockshares = true;
		}
		return isStockshares;
	}

	public void setStockshares(boolean isStockshares) {
		this.isStockshares = isStockshares;
	}

	public String getASSET_TYPE_ID() {
		return ASSET_TYPE_ID;
	}

	public boolean isLoanLiabilities() 
	{
		isLoanLiabilities = false;
		if(viewMap.get(ASSET_TYPE_ID) != null)
		{
			Long assetTypeId =Long.parseLong(viewMap.get(ASSET_TYPE_ID).toString());
			if(assetTypeId.compareTo(WebConstants.AssetType.LOAN_LIABILITIES) == 0)
				isLoanLiabilities = true;
		}
		return isLoanLiabilities;
	}

	public void setLoanLiabilities(boolean isLoanLiabilities) {
		this.isLoanLiabilities = isLoanLiabilities;
	}

	public AssetMemsView getAssetMemsView() 
	{
		if(	viewMap.get(ASSET_MEMS_VIEW) != null)
		 assetMemsView = (AssetMemsView) viewMap.get(ASSET_MEMS_VIEW);

		else if(assetMemsView != null)
			setAssetMemsView(assetMemsView);
		
		return assetMemsView;
		
	}

	@SuppressWarnings("unchecked")
	public void setAssetMemsView(AssetMemsView assetMemsView) 
	{
		if(assetMemsView != null)
		{
			viewMap.put(ASSET_MEMS_VIEW, assetMemsView);
		}
		this.assetMemsView = assetMemsView;
	}

	@SuppressWarnings("unchecked")
	public List<InheritedAssetView> getInheritedAssetList() 
	{
		if(	viewMap.get(INHERITED_ASSET_MEMS_VIEW_LIST) != null)
			inheritedAssetList = (List<InheritedAssetView>) viewMap.get(INHERITED_ASSET_MEMS_VIEW_LIST);
		return inheritedAssetList;
	}

	@SuppressWarnings("unchecked")
	public void setInheritedAssetList(List<InheritedAssetView> inheritedAssetViewList)
	{
		if(inheritedAssetViewList != null)
		{
			viewMap.put(INHERITED_ASSET_MEMS_VIEW_LIST, inheritedAssetViewList);
		}
		this.inheritedAssetList = inheritedAssetViewList;
	}
	@SuppressWarnings("unchecked")
	public void addAsset()
	{
		try
		{
			if( !isValidated() )
			{return;}
				
					//if asset is selected from search, it means it was already associated with other files
					//so change the logic to save
					inheritedAssetView = getInheritedAssetView();//variables to be used below
					assetMemsView = getAssetMemsView();
					
					if(  clndrFromDate != null && 
						 clndrFromDate.getValue() != null && 
						 StringHelper.isNotEmpty( clndrFromDate.getValue().toString() )
					)
					{
						assetMemsView.setFromDate((Date) clndrFromDate.getValue());
					}
					List<MemsAssetCncrndDeptView> depttList = new ArrayList<MemsAssetCncrndDeptView>();
					if(assetMemsView != null)
					{
						if(getInheritedAssetList() != null)
						{
							inheritedAssetList = getInheritedAssetList();
						}
						
						if(viewMap.get(IS_ASSET_SELECTED_FROM_SEARCH) != null && (Boolean)viewMap.get(IS_ASSET_SELECTED_FROM_SEARCH))
						{
							inheritedAssetView = new InheritedAssetView();//if asset is selected from asset search, then create new inherited asset for current file.
							viewMap.remove(IS_ASSET_SELECTED_FROM_SEARCH);
						}
						
						assetMemsView 			= getFilledAssetMemsView();
						memsAssetCncrndDeptView = getFilledMemsAssetConcernedDepttView();
						inheritedAssetView 		= getFilledInheritedAssetView();
						inheritedAssetView.setAssetMemsView(assetMemsView);
						
						
						if(memsAssetCncrndDeptView != null)
							depttList.add(memsAssetCncrndDeptView);//creating list of deptt
						else
							depttList = null;
						
						inheritedAssetView.getAssetMemsView().setConcernedDepartmentViewList(depttList);
//						setAssetStatusBasedOnAssetType(assetMemsView);
						InheritedAssetView persistedAssetView=  new InheritedAssetView();
						
						persistedAssetView = new InheritanceFileService().persistInherAssetConcernedDeptt(inheritedAssetView);
						if(persistedAssetView.getAssetMemsView().getManagerId() == null)
						{
							persistedAssetView.getAssetMemsView().setManagerName(
																				 CommonUtil.getBundleMessage("inheritanceFile.inheritedAssetTab.managerAmaf")
																				);
						}
						persistedAssetView.setIsSelected(false);

						
						if(persistedAssetView.getAssetMemsView().getRevenueType()!= null)
						{
							persistedAssetView.getAssetMemsView().setRevenueTypeString(getRevenueTypeDescById(persistedAssetView.getAssetMemsView().getRevenueType()));
						}
						if(persistedAssetView.getAssetMemsView().getAssetTypeView() != null )
						{
							if(StringHelper.isNotEmpty(persistedAssetView.getAssetMemsView().getAssetTypeView().getAssetTypeIdString()) && assetMemsView.getAssetTypeView().getAssetTypeIdString().compareTo("-1") != 0)
							{
								Long id = Long.parseLong(persistedAssetView.getAssetMemsView().getAssetTypeView().getAssetTypeIdString());
								persistedAssetView.getAssetMemsView().getAssetTypeView().setAssetTypeNameEn(getAssetTypeDescById(id));
								persistedAssetView.getAssetMemsView().getAssetTypeView().setAssetTypeNameAr(getAssetTypeDescById(id));
							}
							else
								if(persistedAssetView.getAssetMemsView().getAssetTypeView().getAssetTypeId() != null)
								{
									Long id = persistedAssetView.getAssetMemsView().getAssetTypeView().getAssetTypeId();
									persistedAssetView.getAssetMemsView().getAssetTypeView().setAssetTypeNameEn(getAssetTypeDescById(id));
									persistedAssetView.getAssetMemsView().getAssetTypeView().setAssetTypeNameAr(getAssetTypeDescById(id));
								}
						}
						//editing asset
						if(viewMap.get(SELECTED_INDEX) != null)
						{
							 viewMap.remove(SELECTED_INDEX);
							for (InheritedAssetView asset : inheritedAssetList)
							{ 
								if( asset != null && asset.getInheritedAssetId() != null && 
									asset.getInheritedAssetId().compareTo(persistedAssetView.getInheritedAssetId())==0
								  )
								{
									inheritedAssetList.remove(asset);
									break;
								}
							}
							successMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.successMsg.assetUpdated"));
						}
						//Adding new asset
						else
						{
							successMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.successMsg.assetSaved"));
						}
						viewMap.put(WebConstants.InherFileSuccessMsg.INHER_ASSET_SUCC, successMessages);
						inheritedAssetList.add(persistedAssetView);
						setInheritedAssetList(inheritedAssetList);
						clearAssetTypeRelatedFields(assetMemsView);
						setMemsAssetCncrndDeptView(new MemsAssetCncrndDeptView());
					}
					if(viewMap.get(WebConstants.InheritanceFile.MANAGER_VIEW) != null)
					{
						viewMap.remove(WebConstants.InheritanceFile.MANAGER_VIEW);
					}
				
		}
		catch (Exception e) 
		{
			logger.LogException("addToGridAndSave crashed", e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			viewMap.put(WebConstants.InherFileErrMsg.INHER_ASSET_ERR, errorMessages);
		}
		finally{
			executeJavascript("javaScript:onProcessComplete();");
		}
	}
	@SuppressWarnings("unused")
	private void setAssetStatusBasedOnAssetType(AssetMemsView assetMemsView) throws Exception 
	{
		if(assetMemsView.getAssetTypeView() != null && StringHelper.isNotEmpty(assetMemsView.getAssetTypeView().getAssetTypeIdString()))
		{
			Long assetTypeId = Long.parseLong(assetMemsView.getAssetTypeView().getAssetTypeIdString());
			if(WebConstants.AssetType.CASH.compareTo(assetTypeId) == 0 ||
			   WebConstants.AssetType.CHEQUE.compareTo(assetTypeId) == 0 ||
			   WebConstants.AssetType.BANK_TRANSFER.compareTo(assetTypeId) == 0)
				assetMemsView.setAssetStatus(getPaymentStatusPendingDD().getDomainDataId());
			else
				assetMemsView.setAssetStatus(null);
		}
	}
	@SuppressWarnings("unchecked")
	private boolean hasDisableError(InheritedAssetView assetToBeDeleted) throws Exception
	{
		boolean hasErrors =false;
		BeneficiarySharesTabController shareTab = (BeneficiarySharesTabController) getBean("pages$beneficiarySharesTabController"); 
		List<InheritedAssetShareInfoView> sharesList = shareTab.getAssetShareDataList();
		//In case asset share tab has not been clicked
		if(sharesList == null || sharesList.size() <= 0 )
		{
			shareTab.loadTabDetails();
			sharesList = shareTab.getAssetShareDataList();
		}
		if( sharesList != null && sharesList.size() > 0 )
		{
			for (InheritedAssetShareInfoView item : sharesList) 
			{ 
				 if ( item.getInheritedAssetId().equals(assetToBeDeleted.getInheritedAssetId().toString() ) )
				 {
					 errorMessages.add(CommonUtil.getBundleMessage("inheritanceFile.inheritedAsset.deletionError"));
					 hasErrors = true;
					 break;
									 
				 }
			}
			
		}
		if (!hasErrors)
		{
			List<Long> closeRequestStatus = new ArrayList<Long>();
			closeRequestStatus.add( 9003L  );
			closeRequestStatus.add( 9005L  );
			closeRequestStatus.add( 90018L );
			closeRequestStatus.add( 90035L );
			closeRequestStatus.add( 90039L );
			closeRequestStatus.add( 90050L );
		
			String requestNumber= InheritanceFileService.hasIncompleteCollectionRequestForInheritedAssetsExists( assetToBeDeleted,closeRequestStatus );
			if(requestNumber != null && requestNumber.length() > 0 )
			 {
				 errorMessages.add(java.text.MessageFormat.format( 
						 											CommonUtil.getBundleMessage("inheritanceFile.inheritedAsset.msg.collectionRequestPresent"),
						 											requestNumber
						 			                             )								
				  			      );
				 hasErrors = true;
				 
								 
			 }
			requestNumber= InheritanceFileService.hasIncompleteTakharujRequestForInheritedAssetsExists( assetToBeDeleted,closeRequestStatus );
			if(requestNumber != null && requestNumber.length() > 0 )
			 {
				 errorMessages.add(java.text.MessageFormat.format( 
						 											CommonUtil.getBundleMessage("inheritanceFile.inheritedAsset.msg.takharujRequestPresent"),
						 											requestNumber
						 			                             )								
				  			      );
				 hasErrors = true;
				 
								 
			 }
		}
		if( hasErrors )
		{
			viewMap.put(WebConstants.InherFileErrMsg.INHER_ASSET_ERR, errorMessages);
		}
		return hasErrors;
	}	
	
	@SuppressWarnings("unchecked")
	public void disableAsset()
	{
		errorMessages = new ArrayList<String>();
		try
		{
			InheritedAssetView asset = (InheritedAssetView) inheritedAssetsDataTable.getRowData();
			if(getInheritedAssetList()== null || hasDisableError(asset))
			{return;}
				DomainData dd = EntityManager.getBroker().findById(DomainData.class, WebConstants.InheritedAssetStatus.INACTIVE_ID);
				asset.setUpdatedBy(getLoggedInUserId());
				asset.setUpdatedOn( new Date() );
				asset.setStatus(WebConstants.InheritedAssetStatus.INACTIVE_ID);
				asset.setStatusEn(dd.getDataDescEn());
				asset.setStatusEn(dd.getDataDescAr());
				new InheritanceFileService().disableInheritedAsset(asset);

				CommonUtil.saveSystemComments( WebConstants.InheritanceFile.NOTES_OWNER, 
						                       "mems.inheritanceFile.event.assetDisabled",
						                       new Long ( viewMap.get(WebConstants.InheritanceFile.FILE_ID).toString() ),
						                       asset.getAssetMemsView().getAssetNumber()
						                      ); 
				inheritedAssetList = getInheritedAssetList();
				
				if(inheritedAssetList != null && inheritedAssetList.size() > 0)
				{
					setInheritedAssetList(inheritedAssetList);
				}
				successMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.successMsg.assetDisabled"));
				viewMap.put(WebConstants.InherFileSuccessMsg.INHER_ASSET_SUCC, successMessages);
		}
		catch (Exception e) 
		{
			logger.LogException("disableAsset() crashed", e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			viewMap.put(WebConstants.InherFileErrMsg.INHER_ASSET_ERR, errorMessages);
		}
	
	}
	@SuppressWarnings("unchecked")
	public void enableAsset()
	{
		errorMessages = new ArrayList<String>();
		try
		{
			InheritedAssetView asset = (InheritedAssetView) inheritedAssetsDataTable.getRowData();
			if(getInheritedAssetList()== null )// && !hasDeletionError(assetToBeDeleted))
			{return;}
				DomainData dd = EntityManager.getBroker().findById(DomainData.class, WebConstants.InheritedAssetStatus.ACTIVE_ID);
				asset.setUpdatedBy(getLoggedInUserId());
				asset.setUpdatedOn( new Date() );
				asset.setStatus(WebConstants.InheritedAssetStatus.ACTIVE_ID);
				asset.setStatusEn(dd.getDataDescEn());
				asset.setStatusEn(dd.getDataDescAr());
				new InheritanceFileService().disableInheritedAsset(asset);
				
				asset.setStatus(WebConstants.InheritedAssetStatus.ACTIVE_ID);
				CommonUtil.saveSystemComments( WebConstants.InheritanceFile.NOTES_OWNER, 
						                       "mems.inheritanceFile.event.assetEnabled",
						                       new Long ( viewMap.get(WebConstants.InheritanceFile.FILE_ID).toString() ),
						                       asset.getAssetMemsView().getAssetNumber()
						                      ); 
				inheritedAssetList = getInheritedAssetList();
				
				if(inheritedAssetList != null && inheritedAssetList.size() > 0)
				{
					setInheritedAssetList(inheritedAssetList);
				}
				successMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.successMsg.assetEnabled"));
				viewMap.put(WebConstants.InherFileSuccessMsg.INHER_ASSET_SUCC, successMessages);
		}
		catch (Exception e) 
		{
			logger.LogException("deleteAssetAfterFinalize() crashed", e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			viewMap.put(WebConstants.InherFileErrMsg.INHER_ASSET_ERR, errorMessages);
		}
	
	}

	@SuppressWarnings("unchecked")
	public void deleteAsset()
	{
		errorMessages = new ArrayList<String>();
		try
		{
			InheritedAssetView assetToBeDeleted = (InheritedAssetView) inheritedAssetsDataTable.getRowData();
			if(getInheritedAssetList()!= null )// && !hasDeletionError(assetToBeDeleted))
			{
				inheritedAssetList = getInheritedAssetList();
				
				assetToBeDeleted.setUpdatedBy(getLoggedInUserId());
				new InheritanceFileService().deleteInheritedAsset(assetToBeDeleted);
				inheritedAssetList.remove(assetToBeDeleted);
				successMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.successMsg.assetRemoved"));
				viewMap.put(WebConstants.InherFileSuccessMsg.INHER_ASSET_SUCC, successMessages);
				if(inheritedAssetList != null && inheritedAssetList.size() > 0)
				{
					setInheritedAssetList(inheritedAssetList);
				}
				
			} 
		}
		catch (Exception e) 
		{
			logger.LogException("deleteAsset() crashed", e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			viewMap.put(WebConstants.InherFileErrMsg.INHER_ASSET_ERR, errorMessages);
		}
	
	}
	private InheritedAssetView getFilledInheritedAssetView() throws Exception
	{
		
		InheritedAssetView inheritedAssetToSave = new InheritedAssetView();
		inheritedAssetToSave = getInheritedAssetView();
		//update mode
		if(inheritedAssetToSave.getInheritedAssetId() != null)
		{
			inheritedAssetToSave.setUpdatedBy(getLoggedInUserId());
			inheritedAssetToSave.setUpdatedOn(new Date());
			if(txtAssetTotalShare.getValue() != null && StringHelper.isNotEmpty(txtAssetTotalShare.getValue().toString())){
				inheritedAssetToSave.setTotalShareValue(Double.valueOf(txtAssetTotalShare.getValue().toString()));
				inheritedAssetToSave.setOldTotalShareValue( txtAssetTotalShare.getValue().toString() );
			}
		}
		//update mode
		
		//add mode
		else
		{
			inheritedAssetToSave.setInheritedAssetId(null);
			inheritedAssetToSave.setUpdatedBy(getLoggedInUserId());
			inheritedAssetToSave.setUpdatedOn(new Date());
			inheritedAssetToSave.setCreatedBy(getLoggedInUserId());
			inheritedAssetToSave.setCreatedOn(new Date());
			inheritedAssetToSave.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
			inheritedAssetToSave.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
			if(txtAssetTotalShare.getValue() != null && StringHelper.isNotEmpty(txtAssetTotalShare.getValue().toString())){
				inheritedAssetToSave.setTotalShareValue(Double.valueOf(txtAssetTotalShare.getValue().toString()));
				inheritedAssetToSave.setOldTotalShareValue( txtAssetTotalShare.getValue().toString() );
			}
		}
		if(viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW) != null)
		{
			InheritanceFileView fileView = null;
			fileView = (InheritanceFileView) viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW);
			inheritedAssetView.setInheritanceFileView(fileView);
		}
		else
			inheritedAssetView.setInheritanceFileView(null);
		inheritedAssetToSave.setIsFinal(0L);//not finalized yet
		//add mode
		return inheritedAssetToSave;
	}

	private MemsAssetCncrndDeptView getFilledMemsAssetConcernedDepttView() throws Exception
	{
		MemsAssetCncrndDeptView memsAssetCncrndDepttToSave = null;
		//if( isDepttMandatory( new Long( getAssetMemsView().getAssetTypeView().getAssetTypeIdString() ) ) )
		{
			memsAssetCncrndDepttToSave = new MemsAssetCncrndDeptView();
			memsAssetCncrndDepttToSave = getMemsAssetCncrndDeptView();
			//update mode
			if(memsAssetCncrndDepttToSave.getMemsAssetCncrndDeptId() != null)
			{
				memsAssetCncrndDepttToSave.setUpdatedBy(getLoggedInUserId());
				memsAssetCncrndDepttToSave.setUpdatedOn(new Date());
			}
			//update mode
			
			//add mode
			else
			{
				memsAssetCncrndDepttToSave.setMemsAssetCncrndDeptId(null);
				memsAssetCncrndDepttToSave.setUpdatedBy(getLoggedInUserId());
				memsAssetCncrndDepttToSave.setUpdatedOn(new Date());
				memsAssetCncrndDepttToSave.setCreatedBy(getLoggedInUserId());
				memsAssetCncrndDepttToSave.setCreatedOn(new Date());
				memsAssetCncrndDepttToSave.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
				memsAssetCncrndDepttToSave.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
			}
			//add mode
		}
		return memsAssetCncrndDepttToSave;
	}

	private AssetMemsView getFilledAssetMemsView()  throws Exception
	{
		AssetMemsView assetMemsToSave = null;
		assetMemsToSave = getAssetMemsView();
		
		if( chkIsManagerAmaf.getValue() != null && (Boolean) chkIsManagerAmaf.getValue()){
			assetMemsToSave.setIsManagerAmaf(1L);
			assetMemsToSave.setManagerId(null);
		}
		else if (viewMap.get(WebConstants.InheritanceFile.MANAGER_VIEW)!= null ){
			PersonView manager = (PersonView) viewMap.get(WebConstants.InheritanceFile.MANAGER_VIEW);
			assetMemsToSave.setManagerId(manager.getPersonId());
			assetMemsToSave.setIsManagerAmaf(0L);
		}
			
			
		if(!assetMemsToSave.isIncomeExpbool())
		{
			assetMemsToSave.setExpectedRevenueString(null);
			assetMemsToSave.setFromDate(null);
			assetMemsToSave.setRevenueTypeString(null);
			assetMemsToSave.setRevenueTypeIdString(null);
			assetMemsToSave.setExpectedRevenue(null);
		}
		assetMemsToSave.setUpdatedBy(getLoggedInUserId());
		assetMemsToSave.setUpdatedOn(new Date());
		//add  mode
		if(assetMemsToSave.getAssetId() == null)
		{
			assetMemsToSave.setCreatedBy(getLoggedInUserId());
			assetMemsToSave.setCreatedOn(new Date());
			assetMemsToSave.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
			assetMemsToSave.setRecordStatus(WebConstants.DEFAULT_RECORD_STATUS);
		}
		//add mode
		
		if(assetMemsToSave.getAssetTypeView() != null && StringHelper.isNotEmpty(assetMemsToSave.getAssetTypeView().getAssetTypeIdString()))
		{
			Long assetTypeId = Long.parseLong(assetMemsToSave.getAssetTypeView().getAssetTypeIdString());
			if(WebConstants.AssetType.CASH.compareTo(assetTypeId) == 0 ||
			   WebConstants.AssetType.CHEQUE.compareTo(assetTypeId) == 0 ||
			   WebConstants.AssetType.BANK_TRANSFER.compareTo(assetTypeId) == 0)
				assetMemsToSave.setAssetStatus(getPaymentStatusPendingDD().getDomainDataId());
			else
				assetMemsToSave.setAssetStatus(null);
		}
		//To show share in grid
		if(txtAssetTotalShare.getValue() != null && StringHelper.isNotEmpty(txtAssetTotalShare.getValue().toString())){
			assetMemsToSave.setTotalAssetShareInFile(txtAssetTotalShare.getValue().toString());
		}
		return assetMemsToSave;
	}

	@SuppressWarnings("unchecked")
	private boolean isValidated() throws Exception
	{
		boolean validate = true;
		AssetMemsView asset = getAssetMemsView();
		
		if( isAssetMemsValidated( asset,false  ) )//&& isMemsCnrndDepttValidated() && isManagerValidated())
			return validate;
		else
		{
			if(errorMessages != null && errorMessages.size() > 0)
				viewMap.put(WebConstants.InherFileErrMsg.INHER_ASSET_ERR, errorMessages);
			validate = false;
		}
		return validate;
	}

	private boolean isManagerValidated() {
		boolean validate =  true;
		if(!(chkIsManagerAmaf.getValue() != null && (Boolean)chkIsManagerAmaf.getValue())){
			
			//first check in view, if its not available in viewMap then check in assetMemsView
			
			if (!(viewMap.get(WebConstants.InheritanceFile.MANAGER_VIEW) != null
					&& txtManagerName.getValue() != null && StringHelper
					.isNotEmpty(txtManagerName.getValue().toString()))) {
				
				if(getAssetMemsView().getManagerView() == null || getAssetMemsView().getManagerView().getPersonId() == null){
					errorMessages
					.add(CommonUtil
							.getBundleMessage("inheritanceFile.inheritedAssetTab.errMsg.selectManager"));
					validate = false;
				}
			} 
		}
		return validate;
	}

	private boolean isMemsCnrndDepttValidated() throws Exception 
	{
		boolean validate = true;
		AssetMemsView asset = getAssetMemsView();
		//For migrated File the concerned department check for Bank Transfer will be bypassed
		if ( isMigratedFile && isBankTransfer() )
			return true;		
		else if(isDepttMandatory(new Long(asset.getAssetTypeView().getAssetTypeIdString())))
		{

			if(getMemsAssetCncrndDeptView() != null )
			{
				MemsAssetCncrndDeptView deptt = getMemsAssetCncrndDeptView();
				if ( !isMigratedFile  )
				{
					if(   StringHelper.isEmpty(deptt.getContactPersonName()))
					{
						errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.contactPersonReq"));
						validate = false;	
					}
					
					if(StringHelper.isEmpty(deptt.getPhone1()))// cellphone
					{
						errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.celphoneReq"));
						validate = false;
					}
					if(StringHelper.isEmpty(deptt.getPhone2()))// Office Phone
					{
						errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.officephoneReq"));
						validate = false;
					}
					if(StringHelper.isEmpty(deptt.getEmail()))
					{
						errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.emailReq"));
						validate = false;
					}
					else
					{
						//Email validation
						String email = deptt.getEmail();
						Pattern pattern=Pattern.compile("[a-zA-Z]*[0-9]*@[a-zA-Z]*.[a-zA-Z]*");
						Matcher matcher = pattern.matcher(email);
						if(!matcher.matches())
						{
							errorMessages.add(CommonUtil.getBundleMessage("common.error.validation.invalidEmail"));
							validate = false;
						}
	
					}
				}
				//If both Deptt and Bank r selected or not a single is selected
				if( StringHelper.isNotEmpty(deptt.getGovtDepttView().getGovtDepttIdString()) && 
				    StringHelper.isNotEmpty(deptt.getBankView().getBankIdString()) && 
				    ( deptt.getGovtDepttView().getGovtDepttIdString().compareTo("-1") != 0 &&
				      deptt.getBankView().getBankIdString().compareTo("-1") != 0) || 
				    ( deptt.getGovtDepttView().getGovtDepttIdString().compareTo("-1") == 0 &&
					  deptt.getBankView().getBankIdString().compareTo("-1") == 0 )
				   )
				{
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.oneOfBanknGovt"));
					validate = false;
				}
				 
			}
			else
			{
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.cncrndDepttInfoReq"));
				validate = false;	
			}
		}
		return validate;
	}

	private boolean isAssetMemsValidated( AssetMemsView asset,Boolean validateCompleteAssetDetails ) throws Exception
	{
			boolean validate = true;
			if(asset == null)
			{
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.assetInfoReq"));
					return false;
			}
			if(asset.getAssetTypeView() == null ||asset.getAssetTypeView().getAssetTypeIdString().compareTo("-1") == 0)
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.assettypeReq"));
			}	
			if(StringHelper.isEmpty(asset.getAssetNameAr()))
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.aasetNameReq"));
			}
			//Rest of the details will
			if( !validateCompleteAssetDetails   )
			{
				return validate;
			}
			if(asset.isIncomeExpbool())
			{
				if(StringHelper.isEmpty(asset.getExpectedRevenueString()))
				{
					validate = false;
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.revReq"));
				}
				else
				{
					try {
						Double revenue = new Double(asset.getExpectedRevenueString());
						if(revenue.compareTo(0D) <= 0)
						{
							validate = false;
							errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.revGTZero"));
						}
					} catch (Exception e) 
					{
						validate = false;
						errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.revNumeric"));
					}
				}
				
				if(StringHelper.isEmpty(asset.getRevenueTypeIdString()) || asset.getRevenueTypeIdString().compareTo("-1") == 0)
				{
					validate = false;
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.revTypeReq"));	
				}
			
			}
			
			if(isCash() && !isCashValidated(asset))//Check cash  validations
			{
				validate = false;
			}
			else if(isCheque() && !isChequeValidated(asset))
			{
				validate = false;
			}
			else if(isBankTransfer() && !isBankTransferValidated(asset))
			{
				validate = false;
			}
			else if(isLoanLiabilities() && !isLoanLiabilityValidated(asset))
			{
				validate = false;
			}
			else if(isPension() && !isPensionValidated( asset ) )
			{
				validate = false;
			}
			
			if(asset.getExpectedRevenueString() != null && StringHelper.isNotEmpty(asset.getExpectedRevenueString()))
			{
				try 
				{
					 new Double(asset.getExpectedRevenueString());
				} 
				catch (Exception e) 
				{
					validate = false;
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.revNumeric"));
				}
			}
			if( !isMigratedFile &&  ( asset.getTotalAssetShareInFile()   == null || 
					                  StringHelper.isEmpty(asset.getTotalAssetShareInFile().toString()) ||
					                  asset.getTotalAssetShareInFile().equals("0") 
					                 )
					                  
			 )
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("inheritanceFile.errMsg.totalAssetShareRequired"));
			}
			//provide other validations below
		
		return validate;
	}

	private boolean isLoanLiabilityValidated(	AssetMemsView asset	) 
	{
		boolean validate = true;
		if(StringHelper.isEmpty(asset.getAmountString()))
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountReq"));
		}
		else
		{
			try 
			{	
				Double amount = new Double(asset.getAmountString());
				if(amount.compareTo(0D) == -1)
				{
					validate = false;
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountGTZero"));
				}
			}
			catch (Exception e) 
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountNumeric"));
				return validate;
			}
		}
		if( !isMigratedFile && StringHelper.isEmpty( asset.getFieldParty() ) )
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.partyNameReq"));
		}
		if( !isMigratedFile && asset.getDueDate() == null)
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.dueDateReq"));
		}
		return validate;
	}
	private boolean isPensionValidated(AssetMemsView asset) 
	{
		boolean validate = true;
		if(StringHelper.isEmpty(asset.getAmountString()))
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountReq"));
		}
		else
		{
			try 
			{	
				Double amount = new Double(asset.getAmountString());
				if(amount.compareTo(0D) == -1)
				{
					validate = false;
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountGTZero"));
				}
			}
			catch (Exception e) 
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountNumeric"));
				return validate;
			}
		}
		
		return validate;
	}

	private boolean isBankTransferValidated(AssetMemsView asset) 
	{
		boolean validate = true;
		if(StringHelper.isEmpty(asset.getAmountString()))
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.bankTransAmountReq"));
		}
		else
		{
			try 
			{	
				Double amount = new Double(asset.getAmountString());
				if(amount.compareTo(0D) == -1)
				{
					validate = false;
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountGTZero"));
				}
			}
			catch (Exception e) 
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountNumeric"));
				return validate;
			}
		}
		if( !isMigratedFile && asset.getDueDate() == null)
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.bankTransDateReq"));
		}
		
		if(asset.getBankView() == null || StringHelper.isEmpty(asset.getBankView().getBankIdString()) || asset.getBankView().getBankIdString().compareTo("-1") == 0)
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.bankReq"));
		}
		return validate;
	}

	private boolean isChequeValidated(AssetMemsView asset)
	{
		boolean validate = true;
		if(StringHelper.isEmpty(asset.getAmountString()))
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.chequeAmountReq"));
		}
		else
		{
			try 
			{	
				Double amount = new Double(asset.getAmountString());
				if(amount.compareTo(0D) == -1)
				{
					validate = false;
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountGTZero"));
				}
			}
			catch (Exception e) 
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountNumeric"));
				return validate;
			}
		}
		
		if( !isMigratedFile && StringHelper.isEmpty( asset.getFieldNumber() ) )
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.cheqNumberReq"));
		}
		
		if(asset.getDueDate() == null)
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.cheqDateReq"));
		}
		
		if(asset.getBankView() == null || StringHelper.isEmpty(asset.getBankView().getBankIdString()) || asset.getBankView().getBankIdString().compareTo("-1") == 0)
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.bankReq"));
		}
		return validate;
	}

	private boolean isCashValidated(AssetMemsView asset)  throws Exception
	{
		boolean validate= true;
		if(StringHelper.isEmpty(asset.getAmountString()))
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.cashAmountReq"));
		}
		else
		{
			try 
			{	
				Double amount = new Double(asset.getAmountString());
				if(amount.compareTo(0D) == -1)
				{
					validate = false;
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountGTZero"));
				}
			}
			catch (Exception e) 
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountNumeric"));
				return validate;
			}
		}
		return validate;
	}

	@SuppressWarnings("unchecked")
	public MemsAssetCncrndDeptView getMemsAssetCncrndDeptView() 
	{
		if(	viewMap.get(MEMS_CNRCRND_DEPT_VIEW) != null)
			memsAssetCncrndDeptView = (MemsAssetCncrndDeptView) viewMap.get(MEMS_CNRCRND_DEPT_VIEW);
		else if(memsAssetCncrndDeptView != null)
			setMemsAssetCncrndDeptView(memsAssetCncrndDeptView);
		return memsAssetCncrndDeptView;
	}

	@SuppressWarnings("unchecked")
	public void setMemsAssetCncrndDeptView(	MemsAssetCncrndDeptView memsAssetCncrndDeptView) 
	{
		if(memsAssetCncrndDeptView != null)
		{
			viewMap.put(MEMS_CNRCRND_DEPT_VIEW, memsAssetCncrndDeptView);
		}
		this.memsAssetCncrndDeptView = memsAssetCncrndDeptView;
	}

	public InheritedAssetView getInheritedAssetView() 
	{
		if(	viewMap.get(INHERITED_ASSET_VIEW) != null)
			inheritedAssetView = (InheritedAssetView) viewMap.get(INHERITED_ASSET_VIEW);
		else if(inheritedAssetView != null)
			setInheritedAssetView(inheritedAssetView);
		
		return inheritedAssetView;
	}

	@SuppressWarnings("unchecked")
	public void setInheritedAssetView(InheritedAssetView inheritedAssetView)
	{
		if(inheritedAssetView != null)
		{
			viewMap.put(INHERITED_ASSET_VIEW, inheritedAssetView);
		}
		this.inheritedAssetView = inheritedAssetView;
	}

	public List<DomainDataView> getRevenueTypeListDD() {
		return revenueTypeListDD;
	}

	public void setRevenueTypeListDD(List<DomainDataView> revenueTypeListDD) {
		this.revenueTypeListDD = revenueTypeListDD;
	}

	public DomainDataView getRevenueTypeMonthlyDD() {
		return revenueTypeMonthlyDD;
	}

	public void setRevenueTypeMonthlyDD(DomainDataView revenueTypeMonthlyDD) {
		this.revenueTypeMonthlyDD = revenueTypeMonthlyDD;
	}

	public DomainDataView getRevenueTypeYearlyDD() {
		return revenueTypeYearlyDD;
	}

	public void setRevenueTypeYearlyDD(DomainDataView revenueTypeYearlyDD) {
		this.revenueTypeYearlyDD = revenueTypeYearlyDD;
	}

	@SuppressWarnings("unchecked")
	public HashMap<Long, DomainDataView> getRevenueTypeMap() 
	{
		if(viewMap.get(REVENUE_TYPE_MAP) != null)
			revenueTypeMap = (HashMap<Long, DomainDataView>) viewMap.get(REVENUE_TYPE_MAP); 
		return revenueTypeMap;
	}

	public void setRevenueTypeMap(HashMap<Long, DomainDataView> revenueTypeMap) {
		this.revenueTypeMap = revenueTypeMap;
	}
	public DomainDataView getRevenueDDById(Long id)
	{
		revenueTypeMap = getRevenueTypeMap();
		if(revenueTypeMap != null && revenueTypeMap.size() > 0 && revenueTypeMap.containsKey(id))
		{
			return revenueTypeMap.get(id);
		}
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	public HashMap<Long, String> getAssetTypeMap() 
	{
		if(viewMap.get(ASSET_TYPE_MAP) != null)
			assetTypeMap = (HashMap<Long, String>) viewMap.get(ASSET_TYPE_MAP);
		return assetTypeMap;
	}

	public void setAssetTypeMap(HashMap<Long, String> assetTypeMap) {
		this.assetTypeMap = assetTypeMap;
	}
	
	public String getAssetTypeDescById(Long id)
	{
		assetTypeMap = getAssetTypeMap();
		if(assetTypeMap != null && assetTypeMap.size() > 0 && assetTypeMap.containsKey(id))
		{
			return assetTypeMap.get(id);
		}
		else
			return null;
	}
	public String getRevenueTypeDescById(Long id)
	{
		String desc = null;
		DomainDataView selectRevenueType = new DomainDataView();
		selectRevenueType = getRevenueDDById(id);
		if(selectRevenueType != null && selectRevenueType.getDomainDataId() != null)
		{
			desc = CommonUtil.getIsEnglishLocale()?selectRevenueType.getDataDescEn():selectRevenueType.getDataDescAr();
		}
		return desc;
	}

	public DomainDataView getPaymentStatusPendingDD() 
	{
		if(viewMap.get(PAYMENT_STATUS_PENDING) != null)
			paymentStatusPendingDD = (DomainDataView) viewMap.get(PAYMENT_STATUS_PENDING);
		return paymentStatusPendingDD;
	}

	public void setPaymentStatusPendingDD(DomainDataView paymentStatusPendingDD) {
		this.paymentStatusPendingDD = paymentStatusPendingDD;
	}

	public boolean isBankTransfer() 
	{
		isBankTransfer = false;
		if(viewMap.get(ASSET_TYPE_ID) != null)
		{
			Long assetTypeId =Long.parseLong(viewMap.get(ASSET_TYPE_ID).toString());
			if(assetTypeId.compareTo(WebConstants.AssetType.BANK_TRANSFER) == 0)
				isBankTransfer = true;
		}
		return isBankTransfer;
	}

	public void setBankTransfer(boolean isBankTransfer) {
		this.isBankTransfer = isBankTransfer;
	}
	public boolean  isShowRevControls()
	{
		boolean show= false;
		if(viewMap.get(IS_GENERATES_REVENUE) != null && (Boolean) viewMap.get(IS_GENERATES_REVENUE))
			show = true;
		return show;
	}
	@SuppressWarnings("unchecked")
	public void revenueCheckChanged()
	{
		if((Boolean)revChk.getValue())
			viewMap.put(IS_GENERATES_REVENUE, true);
		else
			viewMap.put(IS_GENERATES_REVENUE, false);
	}

	public HtmlSelectBooleanCheckbox getRevChk() {
		return revChk;
	}

	public void setRevChk(HtmlSelectBooleanCheckbox revChk) {
		this.revChk = revChk;
	}
	@SuppressWarnings("unchecked")
	public void confirmAsset()
	{
		try
		{
			errorMessages = new ArrayList<String>();
			InheritedAssetView assetToBeConfirmed = (InheritedAssetView) inheritedAssetsDataTable.getRowData();
			viewMap.put(ASSET_TYPE_ID,assetToBeConfirmed.getAssetMemsView().getAssetTypeView().getAssetTypeIdString());
			if( ! isAssetMemsValidated( assetToBeConfirmed.getAssetMemsView(),true  ) )
			{
				viewMap.put(WebConstants.InherFileErrMsg.INHER_ASSET_ERR, errorMessages);
				return;
			}
			if(viewMap.get(INHERITED_ASSET_MEMS_VIEW_LIST) != null)
			{
				List<InheritedAssetView> aasetList = (List<InheritedAssetView>) viewMap.get(INHERITED_ASSET_MEMS_VIEW_LIST);
				
				//This will deselect other asset
				for(InheritedAssetView asset : aasetList)
				{
				
					if(!asset.getIsConfirmBool())
						asset.setIsSelected(false);
				}
				assetToBeConfirmed.setIsConfirmBool(true);
				assetToBeConfirmed.setUpdatedBy(getLoggedInUserId());
				new InheritanceFileService().confirmInheritedAsset(assetToBeConfirmed.getInheritedAssetId(), getLoggedInUserId());
				successMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.successMsg.assetConfirmed"));
				viewMap.put(WebConstants.InherFileSuccessMsg.INHER_ASSET_SUCC, successMessages);
			} 
		}
		catch (Exception e) 
		{
			logger.LogException("confirmAsset() crashed", e);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			viewMap.put(WebConstants.InherFileErrMsg.INHER_ASSET_ERR, errorMessages);
		}
	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	@SuppressWarnings("unchecked")
	public void showAssetSearchPopup()
	{
		if(getInheritedAssetList() != null && getInheritedAssetList().size() > 0)
		{
			List<Long> idList = new ArrayList<Long>();
			for(InheritedAssetView iAssetView : getInheritedAssetList())
			{
				idList.add(iAssetView.getAssetMemsView().getAssetId());
			}
			sessionMap.put(WebConstants.InheritanceFile.ASSET_IDS_LIST, idList);
		}
		executeJavascript("showAssetSearchPopup();");
	}

	public HtmlInputText getTxtExpRev() {
		return txtExpRev;
	}

	public void setTxtExpRev(HtmlInputText txtExpRev) {
		this.txtExpRev = txtExpRev;
	}

	public HtmlSelectOneMenu getCboRevType() {
		return cboRevType;
	}

	public void setCboRevType(HtmlSelectOneMenu cboRevType) {
		this.cboRevType = cboRevType;
	}

	public HtmlCalendar getClndrFromDate() {
		return clndrFromDate;
	}

	public void setClndrFromDate(HtmlCalendar clndrFromDate) {
		this.clndrFromDate = clndrFromDate;
	}

	public void genRevClicked()
	{
		if((Boolean)revChk.getValue())
		{
			txtExpRev.setDisabled(false);
			clndrFromDate.setDisabled(false);
			cboRevType.setDisabled(false);
		}
		else
		{
			txtExpRev.setDisabled(true);
			clndrFromDate.setDisabled(true);
			cboRevType.setDisabled(true);
		}
		txtExpRev.setValue("");
		cboRevType.setValue("");
		clndrFromDate.setValue("");
	}
	@SuppressWarnings("unchecked")
	public void clearAllFields()throws Exception
	{
        try
        {
		    clearAssetTypeRelatedFields(getAssetMemsView());
			viewMap.put(MEMS_CNRCRND_DEPT_VIEW, new MemsAssetCncrndDeptView());
			if(viewMap.get(SELECTED_INDEX) != null)
			{
				getInheritedAssetList().get(new Integer(viewMap.get(SELECTED_INDEX).toString())).setIsSelected(false);
				viewMap.remove(SELECTED_INDEX);
			}
		}
		catch ( Exception e )
		{
			logger.LogException(  "Error Occured::",e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			viewMap.put(WebConstants.InherFileErrMsg.INHER_ASSET_ERR, errorMessages);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Long> getAssetTypeIdsWhereDepttMandatory()
	{
		successMessages = new ArrayList<String>(0);
		errorMessages = new ArrayList<String>(0);
		List<Long> assetIdList = null; 
		try	
    	{
			if( viewMap.get("ASSET_TYPE_IDS_WHERE_DEPT_MANDATORY")  != null)
			{
				assetIdList = (List<Long>) viewMap.get("ASSET_TYPE_IDS_WHERE_DEPT_MANDATORY"); 
			}
			else
			{
				assetIdList = CommonUtil.getAssetTypeIdWhereDepttMandatory();
				if(assetIdList != null && assetIdList.size() > 0)
				{
					viewMap.put("ASSET_TYPE_IDS_WHERE_DEPT_MANDATORY",assetIdList);
				}
			}
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("getAssetTypeIdsWhereDepttMandatory|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	return assetIdList;
	}
	private boolean isDepttMandatory(Long assetTypeId)
	{
		List<Long> assetTypeIds = null;
		boolean isMandatory = false;
		assetTypeIds = getAssetTypeIdsWhereDepttMandatory();
		if(assetTypeIds != null && assetTypeIds.size() > 0 && assetTypeIds.contains(assetTypeId))
		{
			isMandatory = true;
		}
		return isMandatory;
	}

	public HtmlPanelGrid getPanelAssetTypeDetails() {
		return panelAssetTypeDetails;
	}

	public void setPanelAssetTypeDetails(HtmlPanelGrid panelAssetTypeDetails) {
		this.panelAssetTypeDetails = panelAssetTypeDetails;
	}
	public void openManageAsset()
	{
		
		InheritedAssetView assetViewRow = (InheritedAssetView) inheritedAssetsDataTable.getRowData();
		String javaScriptText = "openManageAssetPopup("+ assetViewRow.getAssetMemsView().getAssetId()+");";
		executeJavascript(javaScriptText);
	}

	public HtmlInputText getTxtAssetTotalShare() {
		return txtAssetTotalShare;
	}

	public void setTxtAssetTotalShare(HtmlInputText txtAssetTotalShare) {
		this.txtAssetTotalShare = txtAssetTotalShare;
	}

	public HtmlSelectBooleanCheckbox getChkIsManagerAmaf() {
		return chkIsManagerAmaf;
	}

	public void setChkIsManagerAmaf(HtmlSelectBooleanCheckbox chkIsManagerAmaf) {
		this.chkIsManagerAmaf = chkIsManagerAmaf;
	}
	@SuppressWarnings("unchecked")
	public void managerAmafClick(){
		try {
		InheritanceFileBean fileBean = (InheritanceFileBean) getBean("pages$inheritanceFile") ;
		if(chkIsManagerAmaf.getValue() != null && (Boolean)chkIsManagerAmaf.getValue()){
			viewMap.put("SHOW_SEARCH_MANAGER_IMAGE",false);
			txtManagerName.setValue(null);
		}
		else{
			if(!(fileBean.isPageModeFinalLimDone() || fileBean.isViewModePopUp() || fileBean.isPageModeView()))
				viewMap.put("SHOW_SEARCH_MANAGER_IMAGE",true);
			if(viewMap.get(WebConstants.InheritanceFile.MANAGER_VIEW) != null){
				PersonView manager = (PersonView) viewMap.get(WebConstants.InheritanceFile.MANAGER_VIEW);
				txtManagerName.setValue(manager.getPersonFullName());
			}
			else if(getAssetMemsView() != null && StringHelper.isNotEmpty(getAssetMemsView().getManagerName()) && viewMap.get(SELECTED_INDEX) != null){
					txtManagerName.setValue(getAssetMemsView().getManagerName());
			}
		}
		} catch (Exception e) {
			logger.logError("managerAmafClick()  crashed | ",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	public boolean isShowSearchManagerImage() {
		if(viewMap.get("SHOW_SEARCH_MANAGER_IMAGE") != null && (Boolean) viewMap.get("SHOW_SEARCH_MANAGER_IMAGE")){
			return true;
		}
		else if(viewMap.get("SHOW_SEARCH_MANAGER_IMAGE") != null && !(Boolean) viewMap.get("SHOW_SEARCH_MANAGER_IMAGE")){
			return false;
		}
		return true;
	}

	public void setShowSearchManagerImage(boolean isShowSearchManagerImage) {
		this.isShowSearchManagerImage = isShowSearchManagerImage;
	}
	@SuppressWarnings("unchecked")
	public void openManagerSearchPopup()
	{
		String methodName ="openManagerSearchPopup";
		logger.logInfo(methodName+"|"+"Start..");
		try	
    	{
			executeJavascript("showManagerSearchPopup();");
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
	}

	public HtmlInputText getTxtManagerName() {
		return txtManagerName;
	}

	public void setTxtManagerName(HtmlInputText txtManagerName) {
		this.txtManagerName = txtManagerName;
	}

	public Map<String, MemsAssetCncrndDeptView> getAssetTypeCncrndDepttMap() {
		if( sessionMap.get("assetTypeCncrndDepttMap") != null)
		{
			assetTypeCncrndDepttMap = (Map<String, MemsAssetCncrndDeptView>)sessionMap.get("assetTypeCncrndDepttMap"); 
		}
		return assetTypeCncrndDepttMap;
	}

	public void setAssetTypeCncrndDepttMap(
			Map<String, MemsAssetCncrndDeptView> assetTypeCncrndDepttMap) {
		this.assetTypeCncrndDepttMap= assetTypeCncrndDepttMap;
		if(this.assetTypeCncrndDepttMap!= null)
		{
			sessionMap.put("assetTypeCncrndDepttMap",this.assetTypeCncrndDepttMap);
		}
	}

	public HtmlCollapsiblePanelTag getCollapsiblePanel() {
		return collapsiblePanel;
	}

	public void setCollapsiblePanel(HtmlCollapsiblePanelTag collapsiblePanel) {
		this.collapsiblePanel = collapsiblePanel;
	}
}
