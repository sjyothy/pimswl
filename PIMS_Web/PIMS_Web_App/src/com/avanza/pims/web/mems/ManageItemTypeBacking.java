package com.avanza.pims.web.mems;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.Utils.generatorViews.ItemTypeCriteriaView;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.ui.util.ResourceUtil;

public class ManageItemTypeBacking extends AbstractMemsBean {
	private static final long serialVersionUID = 1L;
	InheritanceFileService service = new InheritanceFileService();
	ItemTypeCriteriaView selectedRow = new ItemTypeCriteriaView();
	private HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
	String VIEW_MODE = "pageMode";
	String ADD_MODE = "ADD_MODE";
	String UPDATE_MODE = "UPDATE_MODE";
	private boolean addMode;
	
	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		super.init();
		try {
			if (!isPostBack()) {
				checkValuesInSession();
				checkPageMode();
			}
		} catch (Exception es) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init" + "|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void checkValuesInSession() {

		if (sessionMap.get(WebConstants.SELECTED_ROW) != null) {
			selectedRow = (ItemTypeCriteriaView) sessionMap
					.get(WebConstants.SELECTED_ROW);
			sessionMap.remove(WebConstants.SELECTED_ROW);
			viewMap.put(WebConstants.SELECTED_ROW, selectedRow);
		}
	}

	public void onUpdate() {
		if(isValidated()){
			try {
				service.persisItemType(getSelectedRow(),CommonUtil.getLoggedInUser());
				successMessages.add(CommonUtil.getBundleMessage("successMsg.costCenterUpdated"));
			} catch (Exception e) {
				logger.LogException("onUpdate has been crahsed|", e);
			}
		}
	}

	private boolean isValidated() {

		if(StringHelper.isEmpty(selectedRow.getTypeNameEn())){
			errorMessages.add(CommonUtil.getBundleMessage("common.messages.Save"));
			return false;
		}
		return true;
	}

	public ItemTypeCriteriaView getSelectedRow() {
		if(viewMap.get(WebConstants.SELECTED_ROW) != null)
			selectedRow = (ItemTypeCriteriaView) viewMap.get(WebConstants.SELECTED_ROW);
		return selectedRow;
	}

	public void setSelectedRow(ItemTypeCriteriaView selectedRow) {
		if(selectedRow != null ){
			viewMap.put(WebConstants.SELECTED_ROW, selectedRow);
		}
		this.selectedRow = selectedRow;
	}
	private void checkPageMode() {

		if (request.getParameter(VIEW_MODE) != null) {

			if (request.getParameter(VIEW_MODE).equals(ADD_MODE)) {
				viewMap.put(VIEW_MODE, ADD_MODE);

			} else if (request.getParameter(VIEW_MODE).equals(
					UPDATE_MODE)) {
				viewMap.put(VIEW_MODE, UPDATE_MODE);

			}
		}
	}
	public boolean isAddMode(){
		if(viewMap.get(VIEW_MODE) != null && viewMap.get(VIEW_MODE).toString().equals(ADD_MODE)){
			return true;
		}
		return false;
	}
	public void onSave() {
		if(isValidated()){
			try {
				service.persisItemType(getSelectedRow(),CommonUtil.getLoggedInUser());
				successMessages.add(CommonUtil.getBundleMessage("successMsg.costCenterAdded"));
			} catch (Exception e) {
				logger.LogException("onUpdate has been crahsed|", e);
			}
		}
	}
}
