package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlSelectBooleanCheckbox;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.SearchBeneficiaryService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.BeneficiaryGridView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.ui.util.ResourceUtil;

public class SearchBeneficiary extends AbstractMemsBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private transient Logger logger = Logger.getLogger(SearchBeneficiary.class);

	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	Map viewRootMap;
	private HtmlDataTable dataTable;
	 
	private static final String ISSUE_PAYMENT_REQUEST = "PaymentRequest";
	private static final String ISSUE_INVESTMENT_REQUEST = "InvestmentRequest";
	private static final String ISSUE_NORMAL_PAYMENT = "NormalPaymentRequest";
	
	private HtmlInputText textCC= new HtmlInputText();
	private HtmlInputText textBenCC= new HtmlInputText();
	private HtmlInputText htmlBeneficiaryName = new HtmlInputText();
	private HtmlSelectBooleanCheckbox htmlisMinorBool = new HtmlSelectBooleanCheckbox();
	private HtmlSelectBooleanCheckbox htmlIsStudent = new HtmlSelectBooleanCheckbox();
	private HtmlInputText htmlNationalIdNumber = new HtmlInputText();
	private HtmlInputText htmlCellNumber = new HtmlInputText();
	private HtmlInputText htmlFilePerson = new HtmlInputText();
	private HtmlInputText htmlpassportNumber = new HtmlInputText();
	private HtmlInputText htmlGuardian = new HtmlInputText();
	
	private HtmlCalendar birthDateTo = new HtmlCalendar();
	private HtmlCalendar birthDateFrom = new HtmlCalendar();
	
	private HtmlCalendar matDateTo = new HtmlCalendar();
	private HtmlCalendar matDateFrom = new HtmlCalendar();
	
	private HtmlCalendar expMatDateTo = new HtmlCalendar();
	private HtmlCalendar expMatDateFrom = new HtmlCalendar();
	
	private HtmlInputText htmlNurse = new HtmlInputText(); 
	private HtmlSelectOneMenu genderSelectMenu = new HtmlSelectOneMenu();
	
	
	private List<SelectItem> genderSelectList = new ArrayList<SelectItem>(0);
	private HtmlInputText htmlFileNumber = new HtmlInputText();
	
	private HtmlSelectOneMenu healthSelectMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu diseaseTypeSelectMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu incCategorySelectMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu fileStatusSelectMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu fileTypeSelectMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu researcherSelectMenu = new HtmlSelectOneMenu();
	
	private List<SelectItem> incCategoryList = new ArrayList<SelectItem>(0);
	private List<SelectItem> fileStatusList = new ArrayList<SelectItem>(0);
	private List<SelectItem> fileTypeList = new ArrayList<SelectItem>(0);
	private List<SelectItem> researcherList = new ArrayList<SelectItem>(0);
	@SuppressWarnings("unchecked")
	HashMap searchBeneficiaryMap = new HashMap();
	private List<BeneficiaryGridView> dataList = new ArrayList<BeneficiaryGridView>();
	private List<SelectItem> fileStatus = new ArrayList<SelectItem>();
	private String VIEW_MODE = "pageMode";
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;

	private String MODE_SELECT_ONE_POPUP = "MODE_SELECT_ONE_POPUP";
	private String MODE_SELECT_MANY_POPUP = "MODE_SELECT_MANY_POPUP";
	
	private String  DEFAULT_SORT_FIELD = "firstName";

	private String pageMode;
	private boolean isPageModeSelectOnePopUp = false;
	private boolean isPageModeSelectManyPopUp = false;
	
	private String  CONTEXT="context";
	private List<String> errorMessages;
	
	
	private StatementOfAccountCriteria reportCriteria=new StatementOfAccountCriteria(ReportConstant.Report.STATEMENT_OF_ACCOUNT,ReportConstant.Processor.STATEMENT_OF_ACCOUNT,CommonUtil	.getLoggedInUser());
		 

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public FacesContext getContext() {
		return context;
	}

	public void setContext(FacesContext context) {
		this.context = context;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	

	public HtmlInputText getHtmlBeneficiaryName() {
		return htmlBeneficiaryName;
	}

	public void setHtmlBeneficiaryName(HtmlInputText htmlBeneficiaryName) {
		this.htmlBeneficiaryName = htmlBeneficiaryName;
	}

	public HtmlSelectBooleanCheckbox getHtmlisMinorBool() {
		return htmlisMinorBool;
	}

	public void setHtmlisMinorBool(HtmlSelectBooleanCheckbox htmlisMinorBool) {
		this.htmlisMinorBool = htmlisMinorBool;
	}

	public HtmlSelectBooleanCheckbox getHtmlIsStudent() {
		return htmlIsStudent;
	}

	public void setHtmlIsStudent(HtmlSelectBooleanCheckbox htmlIsStudent) {
		this.htmlIsStudent = htmlIsStudent;
	}

	public HtmlInputText getHtmlNationalIdNumber() {
		return htmlNationalIdNumber;
	}

	public void setHtmlNationalIdNumber(HtmlInputText htmlNationalIdNumber) {
		this.htmlNationalIdNumber = htmlNationalIdNumber;
	}

	public HtmlInputText getHtmlCellNumber() {
		return htmlCellNumber;
	}

	public void setHtmlCellNumber(HtmlInputText htmlCellNumber) {
		this.htmlCellNumber = htmlCellNumber;
	}

	public HtmlInputText getHtmlFilePerson() {
		return htmlFilePerson;
	}

	public void setHtmlFilePerson(HtmlInputText htmlFilePerson) {
		this.htmlFilePerson = htmlFilePerson;
	}

	public HtmlInputText getHtmlNurse() {
		return htmlNurse;
	}

	public void setHtmlNurse(HtmlInputText htmlNurse) {
		this.htmlNurse = htmlNurse;
	}

	public HtmlSelectOneMenu getIncCategorySelectMenu() {
		return incCategorySelectMenu;
	}

	public void setIncCategorySelectMenu(HtmlSelectOneMenu incCategorySelectMenu) {
		this.incCategorySelectMenu = incCategorySelectMenu;
	}

	public List<SelectItem> getIncCategoryList() {
		if (viewRootMap.get("incCategoryList") != null)
			return (ArrayList<SelectItem>) viewRootMap.get("incCategoryList");
		return new ArrayList<SelectItem>();
		
	}

	public void setIncCategoryList(List<SelectItem> incCategoryList) {
		this.incCategoryList = incCategoryList;
		if (this.incCategoryList != null)
			viewRootMap.put("incCategoryList", this.incCategoryList);
		
	}

	public HtmlInputText getHtmlFileNumber() {
		return htmlFileNumber;
	}

	public void setHtmlFileNumber(HtmlInputText htmlFileNumber) {
		this.htmlFileNumber = htmlFileNumber;
	}

	

	public HtmlSelectOneMenu getFileStatusSelectMenu() {
		return fileStatusSelectMenu;
	}

	public void setFileStatusSelectMenu(HtmlSelectOneMenu fileStatusSelectMenu) {
		this.fileStatusSelectMenu = fileStatusSelectMenu;
	}

	public HtmlSelectOneMenu getFileTypeSelectMenu() {
		return fileTypeSelectMenu;
	}

	public void setFileTypeSelectMenu(HtmlSelectOneMenu fileTypeSelectMenu) {
		this.fileTypeSelectMenu = fileTypeSelectMenu;
	}

	@SuppressWarnings("unchecked")
	public void init() {
		super.init();
		viewRootMap = getFacesContext().getViewRoot().getAttributes();

		try 
		{

			sessionMap = context.getExternalContext().getSessionMap();

			HttpServletRequest request = (HttpServletRequest) this
					.getFacesContext().getExternalContext().getRequest();

			if (!isPostBack()) {
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
				setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
				 setSortField(DEFAULT_SORT_FIELD);
				setSortItemListAscending(true);
				
				 if (request.getParameter(VIEW_MODE)!=null )
		    	 {
		    		if(request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
		    		 { 	      	    		
	      	    		viewRootMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);
	      	    	}
	      	    	else if(request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP))
	      	    	{	      	    		
	      	    		viewRootMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);                  
	      	    
	      	    	}
		    		
		          
		    	 }

		    		if (request.getParameter(CONTEXT)!=null )
			    	 {
		    			viewRootMap.put(CONTEXT, request.getParameter(CONTEXT).toString());
			    	 }
				loadFileStatusList();
				loadFileTypeList();
				loadResearcherList();
				
				
			}

		} catch (Exception es) {
			logger.LogException( "init|Error Occured..",es );
 			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );

		}

	}
	private void loadResearcherList() throws PimsBusinessException 
	{
		
		List<SelectItem> groupUserList= new CommonUtil().getResearcherGroup();
		this.setResearcherList(groupUserList);
	}



	public Integer getRecordSize() {
		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}
	public boolean isContextFamilyVillage()
	{
		
		if( 
				viewRootMap.get(CONTEXT)!=null && 
				viewRootMap.get(CONTEXT).toString().trim().equalsIgnoreCase("familyVillage"))
		{
			return  true;
		}
		else
			return false;
		
	}
	public boolean getIsViewModePopUp() {
		boolean returnVal = false;

		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null) {
			if (getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())
				returnVal = true;
		}

		else {
			returnVal = false;
		}

		return returnVal;

	}

	public boolean getIsPageModeSelectOnePopUp() {
		isPageModeSelectOnePopUp = false;
		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null
				&& viewRootMap.get(VIEW_MODE).toString().trim()
						.equalsIgnoreCase(MODE_SELECT_ONE_POPUP)) {
			isPageModeSelectOnePopUp = true;
		}
		return isPageModeSelectOnePopUp;

	}

	public boolean getIsPageModeSelectManyPopUp() {
		isPageModeSelectManyPopUp = false;
		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null
				&& viewRootMap.get(VIEW_MODE).toString().trim()
						.equalsIgnoreCase(MODE_SELECT_MANY_POPUP)) {
			isPageModeSelectManyPopUp = true;
		}
		return isPageModeSelectManyPopUp;
	}

	public List<SelectItem> getAssetTypesList() {
		List<SelectItem> assetTypes = new ArrayList<SelectItem>();
		try {

			assetTypes = CommonUtil.getAssetTypesList();

		} catch (PimsBusinessException e) {
			logger.LogException("getAssetTypesList() crashed", e);
		}
		return assetTypes;
	}

	public void doSearch() 
	{
		try 
		{
			errorMessages=new ArrayList<String>();
			pageFirst();
//			loadDataList();
		} 
		catch (Exception e) 
		{
			logger.LogException("doSearch|crashed", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public List<SelectItem> getFileStatus() {
		fileStatus.clear();
		try {

			loadFileStatusList();

		} catch (Exception ex) {

		}
		//		

		return null;
	}

	private void loadFileStatusList() throws PimsBusinessException {

		List<SelectItem> allowedStatuses = new ArrayList<SelectItem>();

		List<DomainDataView> domainDataList = CommonUtil
				.getDomainDataListForDomainType(WebConstants.InheritanceFileStatus.INH_FILE_STATUS);
		for (DomainDataView dd : domainDataList) {
			boolean isAdd = true;

			// if(getIsContextAuctionUnits() && (
			// !dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS)) )
			// isAdd = false;
			//
			// else if(getIsContextNewLeaseContract() && (
			// !dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS)) )
			// isAdd = false;
			//			 
			// else if(getIsContextInspectionUnits() &&
			// !(dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS) ||
			// dd.getDataValue().equals(WebConstants.UNIT_RENTED_STATUS) ) )
			// isAdd = false;
			//			 
			// else if(getIsContextMaintenanceRequest() &&
			// !(dd.getDataValue().equals(WebConstants.UNIT_RENTED_STATUS) ) )
			// isAdd = false;

			if (isAdd) {
				SelectItem item = null;

				if (CommonUtil.getIsEnglishLocale())
					item = new SelectItem(dd.getDomainDataId().toString(), dd
							.getDataDescEn());
				else
					item = new SelectItem(dd.getDomainDataId().toString(), dd
							.getDataDescAr());

				allowedStatuses.add(item);
			}

		}

		this.setFileStatusList(allowedStatuses);

	}

	private void loadFileTypeList() throws PimsBusinessException {

		List<SelectItem> allowedTypes = new ArrayList<SelectItem>();

		List<DomainDataView> domainDataList = CommonUtil
				.getDomainDataListForDomainType(WebConstants.InheritanceFileType.INH_FILE_TYPE);
		for (DomainDataView dd : domainDataList) {
			boolean isAdd = true;

			// if(getIsContextAuctionUnits() && (
			// !dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS)) )
			// isAdd = false;
			//
			// else if(getIsContextNewLeaseContract() && (
			// !dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS)) )
			// isAdd = false;
			//				 
			// else if(getIsContextInspectionUnits() &&
			// !(dd.getDataValue().equals(WebConstants.UNIT_VACANT_STATUS) ||
			// dd.getDataValue().equals(WebConstants.UNIT_RENTED_STATUS) ) )
			// isAdd = false;
			//				 
			// else if(getIsContextMaintenanceRequest() &&
			// !(dd.getDataValue().equals(WebConstants.UNIT_RENTED_STATUS) ) )
			// isAdd = false;

			if (isAdd) {
				SelectItem item = null;

				if (CommonUtil.getIsEnglishLocale())
					item = new SelectItem(dd.getDomainDataId().toString(), dd
							.getDataDescEn());
				else
					item = new SelectItem(dd.getDomainDataId().toString(), dd
							.getDataDescAr());

				allowedTypes.add(item);
			}

		}

		this.setFileTypeList(allowedTypes);

	}

	@SuppressWarnings("unchecked")
	private List<BeneficiaryGridView> loadDataList() 
	{
		if (dataList != null)
			dataList.clear();

		List<BeneficiaryGridView> list = new ArrayList();
		errorMessages=new ArrayList<String>();
		try 
		{
			searchBeneficiaryMap = getSearchCriteria();
			SearchBeneficiaryService searchService = new SearchBeneficiaryService();
			int totalRows = searchService.searchBeneficiariesGetTotalNumberOfRecords(searchBeneficiaryMap);
			setTotalRows(totalRows);
			doPagingComputations();
			list = searchService.getBeneficiariesByCriteria(
															searchBeneficiaryMap,getRowsPerPage(), getCurrentPage(), getSortField(),
															isSortItemListAscending()
														    );
			if (list.isEmpty() || list == null) 
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			}
			Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			viewMap.put("beneficiariestList", list);
			recordSize = totalRows;
			viewMap.put("recordSize", totalRows);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize / paginatorRows;
			if ((recordSize % paginatorRows) > 0)
				paginatorMaxPages++;
			if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
		}
		catch (Exception ex) 
		{
			logger.LogException("loadDataList|crashed", ex);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return list;
	}

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}
	public void doSearchItemList()
	{
		loadDataList();
	}

	@SuppressWarnings("unchecked")
	private HashMap<String, Object> getSearchCriteria() {
		searchBeneficiaryMap = new HashMap<String, Object>();

		String emptyValue = "-1";
		Object fileCC =  textCC.getValue();
		Object benCC = textBenCC.getValue();
		Object beneficiaryName= htmlBeneficiaryName.getValue();
		Object isMinor = htmlisMinorBool.getValue();
		Object isStudent = htmlIsStudent.getValue();
		Object gender =  genderSelectMenu.getValue();
		Object passportNumber=  htmlpassportNumber.getValue();
		Object nationalID=  htmlNationalIdNumber.getValue();
		Object cellNumber=  htmlCellNumber.getValue();
		Object incCategory=  incCategorySelectMenu.getValue();
		
		Object nurse=  htmlNurse.getValue();
		Object guardian=  htmlGuardian.getValue();
		Object fileNumber=  htmlFileNumber.getValue();
		Object fileStatus=  fileStatusSelectMenu.getValue();
		Object fileType=  fileTypeSelectMenu.getValue();
		Object researcher=  researcherSelectMenu.getValue();
		Object filePerson=  htmlFilePerson.getValue();
		Date dateOfBirthTo = (Date) birthDateTo.getValue();
		Date dateOfBirthFrom = (Date) birthDateFrom.getValue();
		Date dateOfMatTo = (Date) matDateTo.getValue();
		Date dateOfMatFrom = (Date) matDateFrom.getValue();
		Date dateOfExpMatTo = (Date) expMatDateTo.getValue();
		Date dateOfExpMatFrom = (Date) expMatDateFrom.getValue();
		Object disType=  diseaseTypeSelectMenu.getValue();
		Object healthStatus =  healthSelectMenu.getValue(); 
		 
		if (healthStatus != null && !healthStatus.toString().trim().equals("")&& !healthStatus.toString().trim().equals(emptyValue)) 
		{
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.HEALTH_STATUS,healthStatus.toString());
		}
		if (disType != null && !disType.toString().trim().equals("")&& !disType.toString().trim().equals(emptyValue)) 
		{
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.DISEASE_TYPE,disType.toString());
		}
		if(dateOfExpMatFrom !=null)
		{
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.EXP_MATURITY_DATE_FROM, dateOfExpMatFrom);
		}
		if(dateOfExpMatTo !=null)
		{
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.EXP_MATURITY_DATE_To, dateOfExpMatTo);
		}
		
		if(dateOfBirthFrom !=null)
		{
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.BIRTH_DATE_FROM, dateOfBirthFrom);
		}
		if(dateOfBirthTo !=null)
		{
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.BIRTH_DATE_To, dateOfBirthTo);
		}
		
		if(dateOfMatFrom !=null)
		{
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.MATURITY_DATE_FROM, dateOfMatFrom);
		}
		if(dateOfBirthTo !=null)
		{
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.MATURITY_DATE_To, dateOfMatTo);
		}
		
		if (
				benCC != null && !benCC .toString().trim().equals("")&& 
			   !benCC .toString().trim().equals(emptyValue)
		   ) 
		{
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.BEN_CC,benCC .toString());
		}
		if (
				fileCC != null && !fileCC.toString().trim().equals("")&& 
			    !fileCC.toString().trim().equals(emptyValue)
		   ) 
		{
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.FILE_CC,fileCC.toString());
		}
		
		if (
			beneficiaryName != null && !beneficiaryName.toString().trim().equals("")&& 
			!beneficiaryName.toString().trim().equals(emptyValue)
		   ) 
		{
			searchBeneficiaryMap.put(
					WebConstants.SEARCH_BENEFICIARY_CRITERIA.BENEFICIARY_NAME,
					beneficiaryName.toString());
		}

		if (isMinor != null && !isMinor.toString().trim().equals("")
				&& !isMinor.toString().trim().equals(emptyValue) && (Boolean)isMinor!=false) {
			searchBeneficiaryMap.put(
					WebConstants.SEARCH_BENEFICIARY_CRITERIA.IS_MINOR,
					isMinor.toString());
		}
		if (isStudent != null && !isStudent.toString().trim().equals("")
				&& !isStudent.toString().trim().equals(emptyValue) && (Boolean)isStudent!=false) {
			searchBeneficiaryMap.put(
					WebConstants.SEARCH_BENEFICIARY_CRITERIA.IS_STUDENT,
					isStudent.toString());
		}
		if (gender != null && !gender.toString().trim().equals("")
				&& !gender.toString().trim().equals(emptyValue)) {
			searchBeneficiaryMap.put(
					WebConstants.SEARCH_BENEFICIARY_CRITERIA.GENDER,
					gender.toString());
		}
		if (passportNumber != null && !passportNumber.toString().trim().equals("")
				&& !passportNumber.toString().trim().equals(emptyValue)) {
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.PASSPORT_NUMBER,
					passportNumber.toString());
		}
		if (nationalID != null && !nationalID.toString().trim().equals("")
				&& !nationalID.toString().trim().equals(emptyValue)) {
			searchBeneficiaryMap.put(
					WebConstants.SEARCH_BENEFICIARY_CRITERIA.NATIONAL_ID_NUMBER, nationalID
							.toString());
		}
		if (cellNumber != null && !cellNumber.toString().trim().equals("")
				&& !cellNumber.toString().trim().equals(emptyValue)) {
			searchBeneficiaryMap.put(
					WebConstants.SEARCH_BENEFICIARY_CRITERIA.CELL_NUMBER,
					cellNumber.toString());
		}
		if (incCategory != null && !incCategory.toString().trim().equals("")
				&& !incCategory.toString().trim().equals(emptyValue)) {
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.INC_CATEGORY,
					incCategory.toString());
		}
		if (nurse != null
				&& !nurse.toString().trim().equals("")
				&& !nurse.toString().trim().equals(emptyValue)) {
			searchBeneficiaryMap.put(
					WebConstants.SEARCH_BENEFICIARY_CRITERIA.NURSE,
					nurse.toString());
		}
		if (guardian != null && !guardian.toString().trim().equals("")
				&& !guardian.toString().trim().equals(emptyValue)) {
			searchBeneficiaryMap.put(
					WebConstants.SEARCH_BENEFICIARY_CRITERIA.GUARDIAN, guardian
							.toString());
		}
		if (fileNumber != null && !fileNumber.toString().trim().equals("")
				&& !fileNumber.toString().trim().equals(emptyValue)) {
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.FILE_NUMBER,
					fileNumber.toString());
		}
		if (fileStatus != null && !fileStatus.toString().trim().equals("")
				&& !fileStatus.toString().trim().equals(emptyValue)) {
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.FILE_STATUS,
					fileStatus.toString());
		}
		if (fileType != null && !fileType.toString().trim().equals("")
				&& !fileType.toString().trim().equals(emptyValue)) {
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.FILE_TYPE,
					fileType.toString());
		}
		if (researcher != null && !researcher.toString().trim().equals("")
				&& !researcher.toString().trim().equals(emptyValue)) {
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.RESEARCHER,
					researcher.toString());
		}
		if (filePerson != null && !filePerson.toString().trim().equals("")
				&& !filePerson.toString().trim().equals(emptyValue)) {
			searchBeneficiaryMap.put(WebConstants.SEARCH_BENEFICIARY_CRITERIA.FILE_PERSON,
					filePerson.toString());
		}

		return searchBeneficiaryMap;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<SelectItem> getFileStatusList() {
		if (viewRootMap.get("fileStatusList") != null)
			return (ArrayList<SelectItem>) viewRootMap.get("fileStatusList");
		return new ArrayList<SelectItem>();
	}

	@SuppressWarnings("unchecked")
	public void setFileStatusList(List<SelectItem> fileStatusList) {

		this.fileStatusList = fileStatusList;
		if (this.fileStatusList != null)
			viewRootMap.put("fileStatusList", this.fileStatusList);
	}

	@SuppressWarnings("unchecked") 
	public List<SelectItem> getFileTypeList() throws Exception {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		String context = null;
		try {
			if(viewRootMap.get(CONTEXT) != null )
			{
				context = viewRootMap.get(CONTEXT).toString();
			}
			if (viewMap.get("FILE_TYPE_LIST") != null) {

				return (List<SelectItem>) viewMap.get("FILE_TYPE_LIST");
			}

			List<DomainDataView> list = new UtilityService().getDomainDataByDomainTypeName("INH_FILE_TYPE");
			if(context ==null || !context.equals("familyVillage") )
			{
				ResourceBundle bundleAr = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(),new Locale("ar"));
				String msgAr = bundleAr.getString("commons.All");
				ResourceBundle bundleEn = ResourceBundle.getBundle(FacesContext.getCurrentInstance().getApplication().getMessageBundle(), new Locale("ar"));
				String msgEn = bundleEn.getString("commons.All");
				selectItems.add(new SelectItem("-1", isEnglishLocale() ?msgEn: msgAr));
			}
			for (DomainDataView type : list) 
			{
				if(context !=null && context.equals("familyVillage") && type.getDomainDataId().compareTo(Constant.InheritanceFileType.FAMILY_VILLAGE_ID)!= 0)
					continue;
				
				selectItems.add(new SelectItem(type.getDomainDataId().toString(), isEnglishLocale() ? 
						type.getDataDescEn()
						: type.getDataDescAr()));
			}
			Collections.sort(selectItems, ListComparator.LIST_COMPARE);
			viewMap.put("FILE_TYPE_LIST", selectItems);

		} catch (Exception e) {
			logger.LogException("getFileTypeList() crashed", e);
		}
		return selectItems;

	}


	public void setFileTypeList(List<SelectItem> fileTypeList) {
		this.fileTypeList = fileTypeList;
		if (fileTypeList != null)
			viewRootMap.put("fileTypeList", this.fileTypeList);
	}

	@SuppressWarnings("unchecked")
	public List<BeneficiaryGridView> getDataList() {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		dataList = (List<BeneficiaryGridView>) viewMap.get("beneficiariestList");
		if (dataList == null)
			dataList = new ArrayList<BeneficiaryGridView>();
		return dataList;
	}

	public void setDataList(List<BeneficiaryGridView> dataList) {
		this.dataList = dataList;
	}
//	public List<BeneficiaryGridView> getSelectedAssets(){
//		logger.logInfo("getSelectedAssets started...");
//		List<AssetsGridView> selectedAssetsList = new ArrayList<AssetsGridView>(0);
//    	try{
//    		dataList = getDataList();
//    		if(dataList != null && dataList.size() > 0)
//    		{
//    			for(AssetsGridView assetView:dataList )
//    			
//    				if(assetView.getSelected())
//    				{
//    					selectedAssetsList.add(assetView);
//    				}
//    			
//    		}
//    		 		
//    		logger.logInfo("getSelectedAssets completed successfully!!!");
//    	}
//    	catch(Exception exception){
//    		logger.LogException("getSelectedAssets() crashed ",exception);
//    	}    	
//    	return selectedAssetsList;
//    }
	@SuppressWarnings("unchecked")
//	public void sendManyAssetsInfoToParent(javax.faces.event.ActionEvent event)//many assets selected
//	{ 
//	       
//		  final String viewId = "/SearchAssets.jsp"; 
//	      List<AssetsGridView> selectedAssets = getSelectedAssets();
//	      if(!selectedAssets.isEmpty())
//	      {
////	    	  	if(getIsContextAuctionUnits())
////	    	  	{
////	    	  		setResultsForAuctionUnits();
////	    	  	}
////	    	  	else if(getIsContextInspectionUnits())
////	    	  	{
////	    	  		setResultsForInspectionUnits();
////	    	  	}
////	    	  	else
////	    	  	{	    	  		 
//	    	  	sessionMap.put(WebConstants.AssetSearchOutcomes.ASSETS_SEARCH_SELECTED_MANY_ASSET, selectedAssets);
//	    		     
////	    	  	}	    	    
//		        String javaScriptText = "javascript:closeWindowSubmit();";		
//		        sendToParent(javaScriptText);
//	      }
//	      else
//	      {
//	        	errorMessages = new ArrayList<String>();
//				
//	      }	      
//	        
//	    }
	
	
	public void sendBeneficiaryInfoToParent(javax.faces.event.ActionEvent event)
	{
		try
		{
	        BeneficiaryGridView beneficiaryGridViewRow=(BeneficiaryGridView)dataTable.getRowData();
	        String javaScriptText ="window.opener.populateBeneficiary('" +beneficiaryGridViewRow.getInheritanceBeneficiaryId()+"');"+
	        		               "window.close();"; 
	        Map sessionMap=FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	        sessionMap.put(WebConstants.BeneficiarySearchOutcomes.BENEFICIARY_SEARCH_SELECTED_ONE_BENEFICIARY, beneficiaryGridViewRow);
	        sendToParent(javaScriptText);
		}
		catch(Exception e)
		{
			logger.LogException( "sendBeneficiaryInfoToParent|Error Occured..",e );
 			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
	 }
	
	public void sendManyBeneficiaryInfoToParent(javax.faces.event.ActionEvent event)//many assets selected
	{ 
	       
		  final String viewId = "/SearchBeneficiary.jsp"; 
	      List<BeneficiaryGridView> selectedBeneficiaries = getSelectedBeneficiaries();
	      if(!selectedBeneficiaries.isEmpty())
	      {
//	    	  	if(getIsContextAuctionUnits())
//	    	  	{
//	    	  		setResultsForAuctionUnits();
//	    	  	}
//	    	  	else if(getIsContextInspectionUnits())
//	    	  	{
//	    	  		setResultsForInspectionUnits();
//	    	  	}
//	    	  	else
//	    	  	{	    	  		 
	    	  	sessionMap.put(WebConstants.BeneficiarySearchOutcomes.BENEFICIARY_SEARCH_SELECTED_MANY_BENEFICIARIES, selectedBeneficiaries);
	    		     
//	    	  	}	    	    
		        String javaScriptText = "javascript:closeWindowSubmit();";		
		        sendToParent(javaScriptText);
	      }
	      else
	      {
	        	errorMessages = new ArrayList<String>();
				
	      }	      
	        
	    }
	private List<BeneficiaryGridView> getSelectedBeneficiaries() {
		logger.logInfo("getSelectedBeneficiaries started...");
		List<BeneficiaryGridView> selectedBeneficiaryList = new ArrayList<BeneficiaryGridView>(0);
    	try{
    		dataList = getDataList();
    		if(dataList != null && dataList.size() > 0)
    		{
    			for(BeneficiaryGridView beneficiaryView:dataList )
    			
    				if(beneficiaryView.getSelected())
    				{
    					selectedBeneficiaryList.add(beneficiaryView);
    				}
    			
    		}
    		 		
    		logger.logInfo("getSelectedBeneficiaries completed successfully!!!");
    	}
    	catch(Exception exception){
    		logger.LogException("getSelectedBeneficiaries() crashed ",exception);
    	}    	
    	return selectedBeneficiaryList;
		// TODO Auto-generated method stub
	}

	public String onEditFamilyVillageBeneficiary()
	{
	BeneficiaryGridView beneficiaryGridRow=(BeneficiaryGridView)dataTable.getRowData();
        
         
        	
        
	sessionMap.put(WebConstants.PERSON_ID, beneficiaryGridRow.getBeneficiaryPersonId());
	return "ManageFamilyVillageBeneficiary";
	}
	@SuppressWarnings("unchecked")
	public String onResearchFamilyVillageBeneficiary()
	{
		BeneficiaryGridView beneficiaryGridRow=(BeneficiaryGridView)dataTable.getRowData();
		sessionMap.put(WebConstants.PERSON_ID, beneficiaryGridRow.getBeneficiaryPersonId());
		return "ResearchFamilyVillageBeneficiary";
	}
	public String edit()
	{
		BeneficiaryGridView beneficiaryGridRow=(BeneficiaryGridView)dataTable.getRowData();
        sessionMap.put("beneficiaryGridRow", beneficiaryGridRow);
		return "ManageBeneficiary";
	}
	
	public void sendToParent(String javaScript)
	{
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String javaScriptText =javaScript;
        AddResource addResource = AddResourceFactory.getInstance(facesContext);
        addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);      
	}

	public HtmlInputText getHtmlpassportNumber() {
		return htmlpassportNumber;
	}

	public void setHtmlpassportNumber(HtmlInputText htmlpassportNumber) {
		this.htmlpassportNumber = htmlpassportNumber;
	}

	public HtmlInputText getHtmlGuardian() {
		return htmlGuardian;
	}

	public void setHtmlGuardian(HtmlInputText htmlGuardian) {
		this.htmlGuardian = htmlGuardian;
	}

	public HtmlSelectOneMenu getGenderSelectMenu() {
		return genderSelectMenu;
	}

	public void setGenderSelectMenu(HtmlSelectOneMenu genderSelectMenu) {
		this.genderSelectMenu = genderSelectMenu;
	}

	public List<SelectItem> getGenderSelectList() {
		return genderSelectList;
	}

	public void setGenderSelectList(List<SelectItem> genderSelectList) {
		this.genderSelectList = genderSelectList;
	}

	public HtmlSelectOneMenu getResearcherSelectMenu() {
		return researcherSelectMenu;
	}

	public void setResearcherSelectMenu(HtmlSelectOneMenu researcherSelectMenu) {
		this.researcherSelectMenu = researcherSelectMenu;
	}

	public List<SelectItem> getResearcherList() {
		if(viewRootMap.containsKey("researcherList") )
		{
			if(viewRootMap.get("researcherList")!=null)
				return (List<SelectItem>) viewRootMap.get("researcherList");						
		}
		return researcherList;
	}

	public void setResearcherList(List<SelectItem> researcherList) {
		if (researcherList!=null)
		{
			viewRootMap.put("researcherList", researcherList);
			this.researcherList = researcherList;
		}
	}

	public void setFileStatus(List<SelectItem> fileStatus) {
		this.fileStatus = fileStatus;
	}

	@SuppressWarnings("unchecked")
	public String issueNormalPaymentRequest() {
		logger.logInfo("[issueNormalPaymentRequest() .. Starts]");
		BeneficiaryGridView selectedBeneficiary = (BeneficiaryGridView) getDataTable().getRowData();
		String navigateTo = "";
		if( null!=selectedBeneficiary ) {
			getRequestMap().put( WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW, selectedBeneficiary);
			navigateTo = ISSUE_NORMAL_PAYMENT;			
		}		
		logger.logInfo("[issueNormalPaymentRequest() .. Ends]");
		return navigateTo;
	}

	
	@SuppressWarnings("unchecked")
	public String issuePaymentRequest() {
		logger.logInfo("[issuePaymentRequest .. Starts]");
		BeneficiaryGridView selectedBeneficiary = (BeneficiaryGridView) getDataTable().getRowData();
		if( null!=selectedBeneficiary ) {
			getRequestMap().put(WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW, selectedBeneficiary);
			return ISSUE_PAYMENT_REQUEST;
		}
		logger.logInfo("[issuePaymentRequest .. Ends]");
		return "";
	}
	@SuppressWarnings("unchecked")
	public String issueInvestmentRequest() {
		logger.logInfo("[issuePaymentRequest .. Starts]");
		BeneficiaryGridView selectedBeneficiary = (BeneficiaryGridView) getDataTable().getRowData();
		String navigateTo = "";
		if( null!=selectedBeneficiary ) {
			getRequestMap().put(WebConstants.InheritanceFile.INHERITANCE_BENEFICIARY_VIEW, selectedBeneficiary);
			navigateTo = ISSUE_INVESTMENT_REQUEST;
		}
		logger.logInfo("[issuePaymentRequest .. Ends]");
		return navigateTo;
		
	}
	
	public void openStatmntOfAccount()
	{
		BeneficiaryGridView details = (BeneficiaryGridView) dataTable.getRowData();
		if(details.getBeneficiaryPersonId() != null)
		{
			reportCriteria.setPersonId(details.getBeneficiaryPersonId());
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, reportCriteria);
			executeJavascript("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
	}
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	@SuppressWarnings("unchecked")
	public void openResearcherForm()
	{
		BeneficiaryGridView details = (BeneficiaryGridView) dataTable.getRowData();
		if(details.getBeneficiaryPersonId() != null)
		{
			sessionMap.put(WebConstants.PERSON_ID, details.getBeneficiaryPersonId());
//			sessionMap.put(WebConstants.InheritanceFile.INHERITANCE_FILE_ID,viewMap.get(WebConstants.InheritanceFile.FILE_ID));
			executeJavascript("openResearchForm();");
		}
		
	}

	public HtmlInputText getTextCC() {
		return textCC;
	}

	public void setTextCC(HtmlInputText textCC) {
		this.textCC = textCC;
	}

	public HtmlInputText getTextBenCC() {
		return textBenCC;
	}

	public void setTextBenCC(HtmlInputText textBenCC) {
		this.textBenCC = textBenCC;
	}

	public HtmlCalendar getBirthDateTo() {
		return birthDateTo;
	}

	public void setBirthDateTo(HtmlCalendar birthDateTo) {
		this.birthDateTo = birthDateTo;
	}

	public HtmlCalendar getBirthDateFrom() {
		return birthDateFrom;
	}

	public void setBirthDateFrom(HtmlCalendar birthDateFrom) {
		this.birthDateFrom = birthDateFrom;
	}

	public HtmlCalendar getMatDateTo() {
		return matDateTo;
	}

	public void setMatDateTo(HtmlCalendar matDateTo) {
		this.matDateTo = matDateTo;
	}

	public HtmlCalendar getMatDateFrom() {
		return matDateFrom;
	}

	public void setMatDateFrom(HtmlCalendar matDateFrom) {
		this.matDateFrom = matDateFrom;
	}

	public HtmlCalendar getExpMatDateTo() {
		return expMatDateTo;
	}

	public void setExpMatDateTo(HtmlCalendar expMatDateTo) {
		this.expMatDateTo = expMatDateTo;
	}

	public HtmlCalendar getExpMatDateFrom() {
		return expMatDateFrom;
	}

	public void setExpMatDateFrom(HtmlCalendar expMatDateFrom) {
		this.expMatDateFrom = expMatDateFrom;
	}

	public HtmlSelectOneMenu getDiseaseTypeSelectMenu() {
		return diseaseTypeSelectMenu;
	}

	public void setDiseaseTypeSelectMenu(HtmlSelectOneMenu diseaseTypeSelectMenu) {
		this.diseaseTypeSelectMenu = diseaseTypeSelectMenu;
	}

	public HtmlSelectOneMenu getHealthSelectMenu() {
		return healthSelectMenu;
	}

	public void setHealthSelectMenu(HtmlSelectOneMenu healthSelectMenu) {
		this.healthSelectMenu = healthSelectMenu;
	}

}
