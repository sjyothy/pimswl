package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.vo.mems.DisBenInhBenAssociationView;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.DisbursementRequestBeneficiaryView;
import com.avanza.ui.util.ResourceUtil;


@SuppressWarnings( "unchecked" )

public class AssociateCostCenterPopup extends AbstractMemsBean
{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6416579601242526598L;
	private HtmlDataTable dataTable;
	private HtmlCommandButton btnAdd ;
	private HtmlCommandButton btnDone ;
	private InheritanceFileService fileService;
    private List<DisBenInhBenAssociationView> dataList = null;
    private DisBenInhBenAssociationView data = null;
    private DisbursementDetailsView disbursementDetails = null;
    private DisbursementRequestBeneficiaryView disbursementRequestBeneficiary = null;
    List<SelectItem> itemList = null;
    Map<String,DisBenInhBenAssociationView> mapData = null;
    
	public AssociateCostCenterPopup() 
	{	
		
		dataTable = new HtmlDataTable();
		btnAdd = new HtmlCommandButton();
		btnDone = new HtmlCommandButton();
		dataList = new ArrayList<DisBenInhBenAssociationView>( 0 );
		data = new DisBenInhBenAssociationView();
		disbursementDetails = new DisbursementDetailsView();
		disbursementRequestBeneficiary = new DisbursementRequestBeneficiaryView();
		itemList = new ArrayList<SelectItem>();
		mapData = new HashMap<String, DisBenInhBenAssociationView>( 0 );
		fileService = new InheritanceFileService();
	}
	
	@Override
	public void init() 
	{
		String METHOD_NAME = "init()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");

			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}
	}
	
	
	private void updateValuesFromMaps() throws Exception
	{
		// Disbursement Details View
		if ( sessionMap.get( WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW ) != null )
		{
			disbursementDetails = ( DisbursementDetailsView ) sessionMap.get( WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW );
			sessionMap.remove( WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW  );
		}  
		else if ( viewMap.get( WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW ) != null )
		{
			disbursementDetails = ( DisbursementDetailsView ) viewMap.get( WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW );
		}
		
		// Disbursement Request Beneficiary View
		if ( sessionMap.get( WebConstants.AssociateCostCenter.DIS_REQ_BEN_VIEW  ) != null )
		{
			disbursementRequestBeneficiary = ( DisbursementRequestBeneficiaryView ) sessionMap.get( WebConstants.AssociateCostCenter.DIS_REQ_BEN_VIEW  );
			sessionMap.remove( WebConstants.AssociateCostCenter.DIS_REQ_BEN_VIEW   );
			if( disbursementRequestBeneficiary.getFileAssociations() != null && disbursementRequestBeneficiary.getFileAssociations().size() > 0  )
			{
				dataList = disbursementRequestBeneficiary.getFileAssociations() ;
			}
		}  
		else if ( viewMap.get( WebConstants.AssociateCostCenter.DIS_REQ_BEN_VIEW  ) != null )
		{
			disbursementRequestBeneficiary = ( DisbursementRequestBeneficiaryView ) viewMap.get( WebConstants.AssociateCostCenter.DIS_REQ_BEN_VIEW  );
		}	
		if( viewMap.get(  WebConstants.AssociateCostCenter.DATA_VIEW ) != null )
		{
			data = ( DisBenInhBenAssociationView )viewMap.get( WebConstants.AssociateCostCenter.DATA_VIEW ); 
		}
		
		if( viewMap.get(  WebConstants.AssociateCostCenter.DATA_LIST_VIEW  ) != null )
		{
			dataList = ( ArrayList<DisBenInhBenAssociationView> )viewMap.get( WebConstants.AssociateCostCenter.DATA_LIST_VIEW ); 
		}
		if( !isPostBack() )
		{
			
			mapData=  fileService.getDisBenInhBenAssociationViewForPersonId( disbursementRequestBeneficiary.getPersonId() );
		    loadCombo();
		}
		else
		{
			if( viewMap.get( WebConstants.AssociateCostCenter.MAP_INH_BEN_ASSO_VIEW ) != null)
		    {
			    mapData = (Map<String,DisBenInhBenAssociationView>) viewMap.get( WebConstants.AssociateCostCenter.MAP_INH_BEN_ASSO_VIEW );
		    }
			if( viewMap.get( WebConstants.AssociateCostCenter.ITEM_LIST_FILE  ) != null  )
			{
				itemList = (ArrayList<SelectItem>)viewMap.get( WebConstants.AssociateCostCenter.ITEM_LIST_FILE  );
			}
		}
		updateValuesToMaps();
	}
	
	private void updateValuesToMaps()
	{
		 if( disbursementDetails != null )
		 {
			 viewMap.put( WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW, disbursementDetails );
		 }
		 if( disbursementRequestBeneficiary != null )
		 {
			 viewMap.put( WebConstants.AssociateCostCenter.DIS_REQ_BEN_VIEW, disbursementRequestBeneficiary );
		 }
		 if( data != null )
		 {
			 viewMap.put( WebConstants.AssociateCostCenter.DATA_VIEW, data );
		 }
		 if( dataList != null )
		 {
			 viewMap.put( WebConstants.AssociateCostCenter.DATA_LIST_VIEW, dataList );
		 }	 
		 if( mapData != null && mapData.size() > 0 )
		 {
			 viewMap.put( WebConstants.AssociateCostCenter.MAP_INH_BEN_ASSO_VIEW, mapData);
		 }
		 if( itemList != null && itemList.size() > 0 )
		 {
			 viewMap.put( WebConstants.AssociateCostCenter.ITEM_LIST_FILE , itemList );
		 }
		
	}	
	private void loadCombo()
	{
		
		for (DisBenInhBenAssociationView obj: mapData.values())
		{
			SelectItem item = new SelectItem( obj.getInheritanceFileId().toString(), obj.getFileNumber() );
			itemList.add( item );
		}
		Collections.sort(itemList, ListComparator.LIST_COMPARE);
	}
	public void onChangeFileCombo( )
	{
		final String METHOD_NAME = "onChangeFileCombo()";
		
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			updateValuesFromMaps();
		    if( mapData.containsKey( data.getInheritanceFileId()) )
		    {
		    	DisBenInhBenAssociationView mapDataView = ( DisBenInhBenAssociationView )mapData.get( data.getInheritanceFileId()).clone(); 
		    	mapDataView.setAmount( mapDataView.getAmount() );
		    	mapDataView.setEditing( false );
		    	mapDataView.setDeleted( false );
		    	data = mapDataView;
		    }
			
			
			updateValuesToMaps();
			logger.logInfo( METHOD_NAME + " --- FINISHED --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		}

	}
	public void onDone()
	{
		final String METHOD_NAME = "onDone()";
		
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			updateValuesFromMaps();
			if ( isValid() )
			{
				
				disbursementRequestBeneficiary.setFileAssociations( dataList );
				for (DisbursementRequestBeneficiaryView  view : disbursementDetails.getRequestBeneficiaries()) 
				{
					if( view.getDesReqBeneficiary().longValue() ==  disbursementRequestBeneficiary.getDesReqBeneficiary().longValue() )
					{
						view =  disbursementRequestBeneficiary;
					    break;
					}
					
				}
				disbursementDetails.setHasCostCentersAssociated(true);
				sessionMap.put(WebConstants.AssociateCostCenter.DISBURSEMENT_DETAILS_VIEW, disbursementDetails );
				executeJavaScript( "window.opener.onMessageFromAssociateCostCenter();window.close();" );
				updateValuesToMaps();
			}
			logger.logInfo( METHOD_NAME + " --- FINISHED --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
	}
	
	public void onAdd()
	{
		final String METHOD_NAME = "onSave()";
		
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			updateValuesFromMaps();
			if ( isValid() )
			{
				data.setPersonId( disbursementRequestBeneficiary.getPersonId() );
				data.setPersonName(disbursementRequestBeneficiary.getName() );
				data.setDeleted(false);
				data.setEditing(false);
				data.setMyHashCode( data.hashCode() );
                dataList.add( data );		
                data = new DisBenInhBenAssociationView();
				updateValuesToMaps();
				
			}
			logger.logInfo( METHOD_NAME + " --- FINISHED --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
	}
	public void onEdit()
	{
		final String METHOD_NAME = "onEdit()";
		
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			updateValuesFromMaps();
			if ( isValid() )
			{
				updateValuesToMaps();
			}
			logger.logInfo( METHOD_NAME + " --- FINISHED --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
	}
	public void onDelete()
	{
		final String METHOD_NAME = "onDelete()";
		
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED --- " );
			updateValuesFromMaps();
			if ( isValid() )
			{
				updateValuesToMaps();
			}
			logger.logInfo( METHOD_NAME + " --- FINISHED --- " );
		}
		catch ( Exception e )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
	}
	public void onOpenFileFromDataTable()
	{
		final String methodName = "onOpenFileFromDataTable|";
		try	
		{	
			logger.logInfo(methodName + " --- STARTED --- ");
            openFile( ( ( DisBenInhBenAssociationView )dataTable.getRowData() ).getInheritanceFileId() );		    
		    logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	private void openFile( String fileId ) throws Exception
	{
	    final String methodName = "openFile|";
		logger.logInfo(methodName + " --- STARTED --- ");
		sessionMap.put( WebConstants.InheritanceFile.FILE_ID, new Long ( fileId  ) );
	    sessionMap.put (WebConstants.InheritanceFilePageMode.IS_POPUP,true);
		executeJavaScript(  "javaScript:openFile();"  );
	    logger.logInfo(methodName + " --- COMPLETED SUCCESSFULLY --- ");
		
	}
	
		public String onSessionDetailDelete()
	{
		updateValuesToMaps();
		return null;
	}
	
	private boolean isValid() 
	{
		boolean valid = true;
		
		// VALIDATION CHECKS STARTS HERE
		
		
		// VALIDATION CHECKS ENDS HERE
		
		return valid;
	}


	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<DisBenInhBenAssociationView> getDataList() {
		return dataList;
	}

	public void setDataList(List<DisBenInhBenAssociationView> dataList) {
		this.dataList = dataList;
	}

	public DisbursementDetailsView getDisbursementDetails() {
		return disbursementDetails;
	}

	public void setDisbursementDetails(DisbursementDetailsView disbursementDetails) {
		this.disbursementDetails = disbursementDetails;
	}

	public DisbursementRequestBeneficiaryView getDisbursementRequestBeneficiary() {
		return disbursementRequestBeneficiary;
	}

	public void setDisbursementRequestBeneficiary(
			DisbursementRequestBeneficiaryView disbursementRequestBeneficiary) {
		this.disbursementRequestBeneficiary = disbursementRequestBeneficiary;
	}

	public DisBenInhBenAssociationView getData() {
		return data;
	}

	public void setData(DisBenInhBenAssociationView data) {
		this.data = data;
	}

	public Map<String, DisBenInhBenAssociationView> getMapData() {
		return mapData;
	}

	public void setMapData(Map<String, DisBenInhBenAssociationView> mapData) {
		this.mapData = mapData;
	}

	public List<SelectItem> getItemList() {
		return itemList;
	}

	public void setItemList(List<SelectItem> itemList) {
		this.itemList = itemList;
	}

	public HtmlCommandButton getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(HtmlCommandButton btnAdd) {
		this.btnAdd = btnAdd;
	}

	public HtmlCommandButton getBtnDone() {
		return btnDone;
	}

	public void setBtnDone(HtmlCommandButton btnDone) {
		this.btnDone = btnDone;
	}

		


}
