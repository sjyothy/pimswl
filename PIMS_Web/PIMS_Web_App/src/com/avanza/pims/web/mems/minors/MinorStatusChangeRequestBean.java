package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.bpel.proxy.MEMSDonationRequestBPELPortClient;
import com.avanza.pims.bpel.proxy.MEMSMinorStatusChangeBPELPortClient;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.BeneficiaryDetailsForMobile;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationDetails;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.ReviewRequestView;
import com.avanza.ui.util.ResourceUtil;


public class MinorStatusChangeRequestBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(MinorStatusChangeRequestBean.class);
	
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_RESUBMITTED = "PAGE_MODE_RESUBMITTED";
	private static final String PAGE_MODE_REV_REQ = "PAGE_MODE_REV_REQ";
	private static final String PAGE_MODE_COMPLETE_REQUIRED = "PAGE_MODE_COMPLETE_REQUIRED";
	private static final String PAGE_MODE_COMPLETED = "PAGE_MODE_COMPLETED";

	
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_APPLICANTS   = "applicationTab";
    private String pageTitle;
    private String pageMode;
    private String hdnPersonId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;

    private String txtRemarks;
    private RequestService requestService = new RequestService();
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	private RequestView requestView ;
    
    protected HtmlCommandButton btnSendBack =new HtmlCommandButton();
    private HtmlSelectOneMenu cmbReviewGroup = new HtmlSelectOneMenu();
    private String selectOneBeneficiary ="-1";
    List<SelectItem> beneficiariesList = new ArrayList<SelectItem>();
    private BeneficiaryDetailsForMobile beneficiary = new BeneficiaryDetailsForMobile();
	public MinorStatusChangeRequestBean(){}
	
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.PROCEDURE_MINOR_STATUS_CHANGE );
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.PROCEDURE_MINOR_STATUS_CHANGE );
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		viewMap.put("noteowner", WebConstants.REQUEST);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		
		loadAttachmentsAndComments( null );
		
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
		  getDataFromTaskList();
		}
		else if ( getRequestMap().get( WebConstants.REQUEST_VIEW) != null )
		{
		  getDataFromSearch();
		  
		}
		else
		{
			setDataForFirstTime();
		}
		
		updateValuesFromMap();
		getPageModeFromRequestStatus();
	}
	
	public void prerender(){
	
		if ( getPageMode().equals(PAGE_MODE_COMPLETED) )
			{
				setPageMode(PAGE_MODE_VIEW);
			}
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		long fileId = Long.valueOf( sessionMap.remove( WebConstants.InheritanceFile.INHERITANCE_FILE_ID ).toString() );
		InheritanceFileView file = new InheritanceFileService().getInheritanceFileByFileId(fileId);
	
		requestView = new RequestView();
		requestView.setInheritanceFileId(fileId );
		requestView.setInheritanceFileView( file );
		requestView.setCreatedOn(new Date());
		requestView.setCreatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedOn(new Date());
		requestView.setStatusId( WebConstants.REQUEST_STATUS_NEW_ID );
		requestView.setStatusDataValue( WebConstants.REQUEST_STATUS_NEW );
		requestView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		requestView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
		requestView.setRequestDate( new Date() );
		requestView.setRequestTypeId( WebConstants.MemsRequestType.MINOR_STATUS_CHANGE );
		loadNonAdultBeneficiaries(null);

	}
	
	@SuppressWarnings( "unchecked" )
	private void loadNonAdultBeneficiaries(Long personId)throws Exception
	{
		beneficiariesList = new ArrayList<SelectItem>();
		List<BeneficiaryDetailsForMobile> list = InheritanceFileService.getBeneficiaryDetails(requestView.getInheritanceFileId(),personId,true);
		for (BeneficiaryDetailsForMobile beneficiaryDetailsForMobile : list) {
			beneficiariesList.add(
											new SelectItem(
																	beneficiaryDetailsForMobile.getPersonId().toString(),
																	beneficiaryDetailsForMobile.getBeneficiaryNameAr()
														  )
								);
		}
		Collections.sort(beneficiariesList, ListComparator.LIST_COMPARE);
	}
	
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromRequestStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		tabPanel.setSelectedTab(TAB_APPLICANTS);
		if( this.requestView == null  || 
			this.requestView.getRequestId() == null ||
			this.requestView.getStatusId() == null 
		  ) 
		{ return; }
		else if(  getStatusApprovalRequired() || getStatusReviewDone() )
		{
			setPageMode(PAGE_MODE_COMPLETE_REQUIRED);
			tabPanel.setSelectedTab("detailsTab" );
			
		}
		else if(  getStatusReviewRequired() )
		{
			setPageMode(PAGE_MODE_REV_REQ );
			viewMap.put( WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_KEY, WebConstants.ReviewRequest.REVIEW_DETAILS_TAB_MODE_UPDATABLE );
			tabPanel.setSelectedTab("tabReview" );
			
		}
		else if(  getStatusRejectedResubmitted() )
		{
			setPageMode(PAGE_MODE_RESUBMITTED);
			tabPanel.setSelectedTab("detailsTab" );
			
		}
		else if (getStatusCompleted())
		{
			setPageMode(PAGE_MODE_VIEW);
			tabPanel.setSelectedTab("detailsTab");
		}	
			
	}
	public boolean getStatusNew() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_NEW ) == 0;
	}
	
	public boolean getStatusRejectedResubmitted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT) == 0;
	}
	public boolean getStatusApprovalRequired() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED ) == 0;
	}
	public boolean getStatusReviewRequired() {
		return requestView.getStatusId()  != null && requestView.getStatusId() .compareTo( WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID ) == 0;
	}
	
	public boolean getStatusReviewDone() {
		return requestView.getStatusId()  != null && requestView.getStatusId() .compareTo( WebConstants.REQUEST_STATUS_REVIEW_DONE_ID) == 0;
	}
	public boolean getStatusCompleted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_COMPLETE ) == 0;
	}

	
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = ( RequestView )viewMap.get( WebConstants.REQUEST_VIEW ) ;
			requestView.setUpdatedBy(getLoggedInUserId());
			if(requestView.getTenantsView() != null && requestView.getTenantsView().getPersonId() != null)
			{
				selectOneBeneficiary = requestView.getTenantsView().getPersonId().toString();
				viewMap.put("selectOneBeneficiary",selectOneBeneficiary ); 
			}
		}
		if( viewMap.get("beneficiariesList") != null )
		{
			beneficiariesList =  (ArrayList<SelectItem>)viewMap.get("beneficiariesList");
		}
		if( viewMap.get("selectOneBeneficiary") != null )
		{
			selectOneBeneficiary =  viewMap.get("selectOneBeneficiary").toString();
		}
		if( viewMap.get("beneficiary") != null )
		{
			beneficiary =  (BeneficiaryDetailsForMobile)viewMap.get("beneficiary");
		}
		
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( requestView != null )
		{
		  viewMap.put(  WebConstants.REQUEST_VIEW , requestView);
		}
		if( selectOneBeneficiary != null && !selectOneBeneficiary.equals("-1"))
		{
		  viewMap.put(  "selectOneBeneficiary" , selectOneBeneficiary);
		  viewMap.put("beneficiary", beneficiary);
		}
		if( beneficiariesList != null   && beneficiariesList.size()>0)
		{
			viewMap.put(  "beneficiariesList" , beneficiariesList);
		}
	
		
		
	}
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		requestView = ( RequestView)getRequestMap().get( WebConstants.REQUEST_VIEW) ;
		if( this.requestView !=null )
		{
		  getRequestDetails( requestView.getRequestId() );
		}
	}
	
	@SuppressWarnings(  "unchecked"  )
	private void populateApplicationDetailsTab()throws Exception
	{
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		String status = "";
		boolean isRequestNull =  null == requestView || null == requestView.getApplicantView() ||null == requestView.getRequestId();
		boolean isApplicantDiffFromPerson = isRequestNull || 
											!requestView.getApplicantView().getPersonId().toString().equals(hdnPersonId);
		if ( isApplicantDiffFromPerson && hdnPersonName!= null && hdnPersonName.trim().length() >0 ) 
		{
			bean.populateApplicationDetails(
						                    "", 
						                    "", 
						                    new Date(), 
						                    "", 
						                    hdnPersonName, 
						                    "", 
						                    hdnCellNo, 
						                    ""
						                  );
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, hdnPersonId);
			PersonView person = new PersonView();
			person.setPersonId( new Long( hdnPersonId ) );
			requestView.setApplicantView( person);
			
            return;
		}
		
		status = CommonUtil.getIsEnglishLocale()?requestView.getStatusEn():requestView.getStatusAr();
		bean.populateApplicationDetails(
				                         requestView.getRequestNumber(), 
					                     status, 
					                     requestView.getRequestDate(), 
					                     requestView.getDescription(), 
					                     requestView.getApplicantView().getPersonFullName(), 
					                     "", 
					                     requestView.getApplicantView().getCellNumber(), 
					                     requestView.getApplicantView().getEmail()
					                   );
		if( requestView.getApplicantView() != null && requestView.getApplicantView().getPersonId() != null  )
		{
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,requestView.getApplicantView().getPersonId() );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, requestView.getApplicantView().getPersonId() );
		}
		if ( requestView!= null && requestView.getStatus() != null && 
			!requestView.getStatusDataValue().equals( WebConstants.REQUEST_STATUS_NEW ) 
		    )
		{
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}
	}
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  UserTask userTask = ( UserTask )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  setUserTask(userTask);
	  if( userTask.getTaskAttributes().get(  WebConstants.REQUEST_ID ) == null ){ return; }
	  long  id = new Long  ( userTask.getTaskAttributes().get( WebConstants.REQUEST_ID ) );
	  getRequestDetails( id );
	  
	  
    }
	
	@SuppressWarnings( "unchecked" )
	private void getRequestDetails( long id ) throws Exception 
	{
		requestView = requestService.getRequestById( id );
		
		populateApplicationDetailsTab();
		updateValuesToMap();
		loadAttachmentsAndComments( id );
		loadNonAdultBeneficiaries(null);
		selectOneBeneficiary = requestView.getTenantsView().getPersonId().toString();
		onBeneficiaryChanged();
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	
	
	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(requestView.getRequestId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.REQUEST;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, requestView.getRequestId());
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.REQUEST;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(requestView.getRequestId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, requestView.getRequestId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }

	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( requestView != null && requestView.getRequestId()!= null )
		 {
			 loadAttachmentsAndComments( requestView.getRequestId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMap();
			if(hdnPersonType.equals( "APPLICANT" ) )
			{
				populateApplicationDetailsTab();
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}

	@SuppressWarnings( "unchecked" )
	public void onBeneficiaryChanged()
	{
		try	
		{
			updateValuesFromMap();
			if(this.selectOneBeneficiary == null || this.selectOneBeneficiary.equals("-1"))return;
			List<BeneficiaryDetailsForMobile>list = InheritanceFileService.getBeneficiaryDetails(requestView.getInheritanceFileId(), Long.valueOf(selectOneBeneficiary), false);
			if(list!= null)
			{
				beneficiary = list.get(0);
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onBeneficiaryChanged|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	
	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if(requestView == null || requestView.getRequestId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,requestView.getRequestId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}

		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			
			saveRequestInTransaction();
			
			getRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
			successMessages.add( ResourceUtil.getInstance().getProperty( "thirdPartRevenue.msg.saved" ) );
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void saveRequestInTransaction() throws Exception 
	{
		try
		{
			
			Long status = Constant.REQUEST_STATUS_NEW_ID;
            if(requestView.getStatusId() != null){status = null;}
            PersonView personView = new PersonView();
            personView.setPersonId(new Long( selectOneBeneficiary ));
            requestView.setTenantsView(personView);
            		
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistRequest( status );
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistRequest( Long status ) throws Exception 
	{
	
		if(status!=null) requestView.setStatusId(status);
		
		InheritanceFileService.persistMinorStatusChange(requestView);
		
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onSubmit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		String event  =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		try	
		{	
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			if( hasSaveErrors() ){ return; }
			
			 updateValuesFromMap();
			 
			 SystemParameters parameters = SystemParameters.getInstance();
			 String endPoint= parameters.getParameter( "PIMSMinorStatusChangeBPELEndPoint" );
			 
			 MEMSMinorStatusChangeBPELPortClient port=new MEMSMinorStatusChangeBPELPortClient();
			 port.setEndpoint(endPoint);
			 
			 persistRequest( status );
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 port.initiate(
					 		Integer.parseInt( requestView.getRequestId().toString()),
					 		CommonUtil.getLoggedInUser(),
			//		 		requestView.getRequestNumber(), 
					 		null, 
					 		null
					 	   );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSubmit --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onResubmitted()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() ){ return; }
		 
		 updateValuesFromMap();	
		 
		 persistRequest( status );
		 getRequestDetails( requestView.getRequestId() );
		 
		 setTaskOutCome(TaskOutcome.APPROVE);

		 updateValuesToMap();
		 
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onResubmitted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onSentBack()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() ){ return; }
		 
		 updateValuesFromMap();	
		 
		 persistRequest( status );
		 getRequestDetails( requestView.getRequestId() );
		 
		 setTaskOutCome(TaskOutcome.REJECT);

		 updateValuesToMap();
		 
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onResubmitted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	
	private boolean isReviewValidated()throws Exception
	{
		
		boolean isValid = true;
		if( cmbReviewGroup.getValue() == null || StringHelper.isEmpty( cmbReviewGroup.getValue().toString().trim() ) || 
			this.cmbReviewGroup.getValue().toString().compareTo("-1") == 0)
		{
			errorMessages.add(CommonUtil.getBundleMessage("takharuj.errMsg.plzSelectgGroup"));
			isValid = false;
		}
		if( txtRemarks == null || StringHelper.isEmpty( txtRemarks.trim()  )  )
		{
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.plzProvidecomments"));
			isValid = false;
		}
		return isValid;
	}

	public void onReview() 
	{
		try 
		{
				updateValuesFromMap();
				if( !isReviewValidated() )
				return ;
				try
				{
					
					
					ApplicationContext.getContext().getTxnContext().beginTransaction();
					
						persistRequest( WebConstants.REQUEST_STATUS_REVIEW_REQUIRED_ID );
						
						ReviewRequestView reviewRequestView = new ReviewRequestView();
						reviewRequestView.setCreatedBy( CommonUtil.getLoggedInUser() );
						reviewRequestView.setCreatedOn( new Date() );
						reviewRequestView.setRfc( txtRemarks.toString().trim() );
						reviewRequestView.setGroupId( cmbReviewGroup.getValue().toString() );
						reviewRequestView.setRequestId( getRequestView().getRequestId() );
						
						UtilityService utilityService = new UtilityService();
						utilityService.persistReviewRequest( reviewRequestView );
						
						saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SENT_FOR_REVIEW );
						setTaskOutCome(  TaskOutcome.FORWARD ) ;
						
					ApplicationContext.getContext().getTxnContext().commit();
				}
				catch ( Exception e )
				{
					 ApplicationContext.getContext().getTxnContext().rollback();
					 throw e;
				}
				finally
				{
					 ApplicationContext.getContext().getTxnContext().release();
				}
				txtRemarks = "";
				cmbReviewGroup.setValue("-1");
				updateValuesToMap();
				successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsPaymentDisbursementMsgs.REVIEW_REQUIRED));
				
				
		} catch(Exception ex) {
			logger.LogException("[Exception occured in review()]", ex);
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	public void onReviewDone() 
	{
		try 
		{
			updateValuesFromMap();
			
			try
			{
				ApplicationContext.getContext().getTxnContext().beginTransaction();
				persistRequest( WebConstants.REQUEST_STATUS_REVIEW_DONE_ID );
				ReviewRequestView reviewRequestView = ( ReviewRequestView ) viewMap.get( WebConstants.ReviewRequest.REVIEW_REQUEST_VIEW );
				UtilityService utilityService = new UtilityService();
				utilityService.persistReviewRequest( reviewRequestView );
				saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_REVIEWED );
				
				setTaskOutCome( TaskOutcome.OK );
				ApplicationContext.getContext().getTxnContext().commit();
			}
			catch ( Exception e )
			{
				 ApplicationContext.getContext().getTxnContext().rollback();
				 throw e;
			}
			finally
			{
				 ApplicationContext.getContext().getTxnContext().release();
			}

			
			updateValuesToMap();
			successMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsPaymentDisbursementMsgs.REVIEW_DONE));
		} catch (Exception ex) {
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("[Exception occured in reviewDone()]", ex);
		}
	}


	private boolean hasReasonProvided() throws Exception
	{
		if(txtRemarks ==null || txtRemarks.trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.reasonRequired")  );
			return false;
		}
		return true;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onRejected()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_REJECTED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_REJECTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_REJECTED;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( status );
			 setTaskOutCome(TaskOutcome.CLOSE);
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onComplete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COMPLETE);
		String msg    =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		String event  =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
//			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( status );
			 setTaskOutCome(TaskOutcome.CLOSE);
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private boolean hasCollectionErrors() throws Exception
	{
		boolean hasCollectionErrors=false;
		errorMessages = new ArrayList<String>();

		return hasCollectionErrors;
		
	}
	

	@SuppressWarnings( "unchecked" )
	public void onPrintReceipt()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			CommonUtil.printMiscellaneousPaymentReceipt(requestView.getRequestId().toString(), null, FacesContext.getCurrentInstance());
		}
		catch (Exception exception) 
		{
			logger.LogException( "onPrintReceipt--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = getUserTask();
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    }



	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask )
	{
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}


	public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("donationRequest.title.heading"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_NEW)  || getPageMode().equals( PAGE_MODE_RESUBMITTED ) )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowReviewDoneButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_REV_REQ ) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  getPageMode().equals( PAGE_MODE_NEW ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowResubmitButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_RESUBMITTED ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowCompleteButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_COMPLETE_REQUIRED ) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowPrintButton()
	{
		if(  getStatusCompleted() )
		{
			return true;
		}
		return false;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

		public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public HtmlCommandButton getBtnSendBack() {
		return btnSendBack;
	}

	public void setBtnSendBack(HtmlCommandButton btnSendBack) {
		this.btnSendBack = btnSendBack;
	}

	public HtmlSelectOneMenu getCmbReviewGroup() {
		return cmbReviewGroup;
	}

	public void setCmbReviewGroup(HtmlSelectOneMenu cmbReviewGroup) {
		this.cmbReviewGroup = cmbReviewGroup;
	}

	public String getSelectOneBeneficiary() {
		return selectOneBeneficiary;
	}

	public void setSelectOneBeneficiary(String selectOneBeneficiary) {
		this.selectOneBeneficiary = selectOneBeneficiary;
	}


	public BeneficiaryDetailsForMobile getBeneficiary() {
		return beneficiary;
	}


	public void setBeneficiary(BeneficiaryDetailsForMobile beneficiary) {
		this.beneficiary = beneficiary;
	}


	public List<SelectItem> getBeneficiariesList() {
		return beneficiariesList;
	}


	public void setBeneficiariesList(List<SelectItem> beneficiariesList) {
		this.beneficiariesList = beneficiariesList;
	}
	
	

}