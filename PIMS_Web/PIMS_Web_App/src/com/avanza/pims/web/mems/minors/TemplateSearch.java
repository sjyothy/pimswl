package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.vo.TemplateView;
import com.avanza.ui.util.ResourceUtil;



public class TemplateSearch extends AbstractMemsBean
{
	private static final long serialVersionUID = 1L;

	private transient Logger logger = Logger.getLogger(InheritenceFileSearch.class);
	
	private HtmlDataTable dataTable;
	
	private HtmlInputText textMyName = new HtmlInputText();
	private HtmlSelectOneMenu listMyAge = new HtmlSelectOneMenu();
	
	private boolean isArabicLocale;	
    private boolean isEnglishLocale;
	private String pageMode;
	private boolean isPageModeSelectOnePopUp = false;
	private boolean isPageModeSelectManyPopUp = false;
	
	private Integer paginatorMaxPages = 0;
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;
	
	private String  VIEW_MODE="pageMode";
	private String  CONTEXT="context";
	private String  MODE_SELECT_ONE_POPUP="MODE_SELECT_ONE_POPUP";
	private String  MODE_SELECT_MANY_POPUP="MODE_SELECT_MANY_POPUP";
	private String  DEFAULT_SORT_FIELD = "unitNumber";
	
	
	HashMap<String, Object> templateSearchMap = new HashMap<String, Object> ();
	
	TemplateView templateView = new TemplateView();
		
	FacesContext context=FacesContext.getCurrentInstance();
    Map sessionMap;
    Map viewRootMap;
 
    
    public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
    public String getNumberFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getNumberFormat();
    }
     @Override 
	 public void init() 
     {    	
    	 super.init();
    	 viewRootMap = getFacesContext().getViewRoot().getAttributes();
    	 
    	 try
    	 {
             sessionMap=context.getExternalContext().getSessionMap();
       
              Map requestMap= context.getExternalContext().getRequestMap();
              HttpServletRequest request=(HttpServletRequest)this.getFacesContext().getExternalContext().getRequest();	
      	    
		     if(!isPostBack())
		     {
					setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
			        setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);     
			        setSortField(DEFAULT_SORT_FIELD);
			        setSortItemListAscending(true);
		    	 
		    	 if (request.getParameter(VIEW_MODE)!=null )
		    	 {
		    		if(request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
		    		 { 	      	    		
	      	    		viewRootMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);
	      	    	}
	      	    	else if(request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP))
	      	    	{	      	    		
	      	    		viewRootMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);                  
	      	    
	      	    	}
		    		
		    		if (request.getParameter(CONTEXT)!=null )
			    	 {
		    			viewRootMap.put(CONTEXT, request.getParameter(CONTEXT).toString());
			    	 }
		    	 }
		     }
		    		 
    	 }
    	 catch(Exception es)
    	 {
    		 logger.logInfo("TemplateSearch.init() crashed" + es);
    		 
    	 }  
	 }
     
   
     
     @Override
 	public void preprocess() {
 		// TODO Auto-generated method stub
 		super.preprocess();
 	}

 	@Override
 	public void prerender() {
 		// TODO Auto-generated method stub
 		super.prerender();
 	}	 
	
	private List<String> errorMessages;
	//private PropertyInquiryData dataItem = new PropertyInquiryData();
	private TemplateView dataItem = new TemplateView();
	//private List<PropertyInquiryData> dataList = new ArrayList<PropertyInquiryData>();
	private List<TemplateView> dataList = new ArrayList<TemplateView>();
  
	//	public InheritenceFile dataItem = new InheritenceFile();
	
	//public List<PropertyInquiryData> getPropertyInquiryDataList()
	public List<TemplateView> getInheritenceFileDataList()
	{
		Map<String, Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		dataList = (List<TemplateView>)viewMap.get("templateDataList");
		if(dataList==null)
			dataList = new ArrayList<TemplateView>();
		return dataList;
	}	
	
	public void doSearch (){
		try {
  	       
		     loadDataList();
		}
	    catch (Exception e){
		    System.out.println("Exception doSearch"+e);       
	    }
	}
	
    @SuppressWarnings("unchecked")
	public HashMap<String,Object> getSearchCriteria()
	{
		templateSearchMap = new HashMap<String, Object>();		
		
		String emptyValue = "-1";		
		
		Object myName = textMyName.getValue();
		Object myAge = listMyAge.getValue();
		
		if(myName !=null && !myName.toString().equals(""))
			templateSearchMap.put(WebConstants.InheritanceSearchCriteria.FILE_NUMBER, myName);
		
		if(myAge !=null && !myAge.toString().equals(""))
			templateSearchMap.put(WebConstants.InheritanceSearchCriteria.CREATED_ON, myAge);
		
		return templateSearchMap;
	}
    
	public void doSearchItemList()
	{
		loadDataList();
	}
    
	public List<TemplateView> loadDataList() 
	{
		 String methodName="loadDataList";
		 if(dataList != null)
			 dataList.clear();
		 
		 List<TemplateView> list = new ArrayList<TemplateView>();		    
		 
		try
		{
			templateSearchMap = getSearchCriteria();
			
			InheritanceFileService fileService = new InheritanceFileService();
			
			
			/////////////////////////////////////////////// For server side paging paging/////////////////////////////////////////
			int totalRows =  fileService.searchInheritenceFile(templateSearchMap);
			setTotalRows(totalRows);
			doPagingComputations();
	        //////////////////////////////////////////////////////////////////////////////////////////////
			
			// Copy the code of following method
			//list =  fileService.searchInheritenceFileByCriteria(templateSearchMap,getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());
			
			if(list.isEmpty() || list==null)
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));	
			}
			Map<String, Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
			viewMap.put("inheritenceFileList", list);
			recordSize = totalRows;
			viewMap.put("recordSize", totalRows);
			paginatorRows = getPaginatorRows();
			paginatorMaxPages = recordSize/paginatorRows;
			if((recordSize%paginatorRows)>0)
				paginatorMaxPages++;
			if(paginatorMaxPages>=WebConstants.SEARCH_RESULTS_MAX_PAGES)
				paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
			viewMap.put("paginatorMaxPages", paginatorMaxPages);
	    }
		catch (Exception ex) 
		{
	        ex.printStackTrace();
	    } 
		return list;
		
	}
	
	public String getErrorMessages() {
		return CommonUtil.getErrorMessages(errorMessages);
	}
	
	public String getBundleMessage(String key){
    	logger.logInfo("getBundleMessage(String) started...");
    	String message = "";
    	try
		{
    		message = ResourceUtil.getInstance().getProperty(key);

		logger.logInfo("getBundleMessage(String) completed successfully!!!");
		}
		catch (Exception exception) {
			logger.LogException("getBundleMessage(String) crashed ", exception);
		}
    	return message;
    }
	
	
public boolean getIsPageModeSelectOnePopUp()
	{
		isPageModeSelectOnePopUp= false;
		if(viewRootMap.containsKey(VIEW_MODE) && viewRootMap.get(VIEW_MODE)!=null && viewRootMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase(MODE_SELECT_ONE_POPUP))
		{
			isPageModeSelectOnePopUp= true;
		}
		return isPageModeSelectOnePopUp;
		
	}
	public boolean getIsViewModePopUp()
	{
		boolean returnVal = false;
		
		if(viewRootMap.containsKey(VIEW_MODE) && viewRootMap.get(VIEW_MODE)!=null)
		{
			if(getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())		
				returnVal =  true;
		}
		 
		else 
		{
			returnVal =  false;
		}
		
		return returnVal;
		
	}
	public boolean getIsPageModeSelectManyPopUp()
	{
		isPageModeSelectManyPopUp= false;
		if(viewRootMap.containsKey(VIEW_MODE) && viewRootMap.get(VIEW_MODE)!=null && viewRootMap.get(VIEW_MODE).toString().trim().equalsIgnoreCase(MODE_SELECT_MANY_POPUP))
		{
			isPageModeSelectManyPopUp= true;
		}
		return isPageModeSelectManyPopUp;
	}

	   public boolean getIsArabicLocale()
		{
			isArabicLocale = !getIsEnglishLocale();
			return isArabicLocale;
		}
	
	   public boolean getIsEnglishLocale()
		{
			WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
			return localeInfo.getLanguageCode().equalsIgnoreCase("en");
		}
	   
	////////////////////Sorting/////////////////////////////////////
	private String sortField = null;
	private boolean sortAscending = true;
	// Actions -----------------------------------------------------------------------------------
	private static String getAttribute(ActionEvent event, String name) {
		return (String) event.getComponent().getAttributes().get(name);
	}
	public void sortDataList(ActionEvent event) {
		String sortFieldAttribute = getAttribute(event, "sortField");
		// Get and set sort field and sort order.
		if (sortField != null && sortField.equals(sortFieldAttribute)) {
			sortAscending = !sortAscending;
		} else {
			sortField = sortFieldAttribute;
			sortAscending = true;
		}
		// Sort results.
		if (sortField != null) {
			Collections.sort(dataList, new ListComparator(sortField,
					sortAscending));
		}
	}
	
	public String getPageMode() {
		return pageMode;
	}

	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}

	public String getMODE_SELECT_ONE_POPUP() {
		return MODE_SELECT_ONE_POPUP;
	}

	public void setMODE_SELECT_ONE_POPUP(String mode_select_one_popup) {
		MODE_SELECT_ONE_POPUP = mode_select_one_popup;
	}

	public String getMODE_SELECT_MANY_POPUP() {
		return MODE_SELECT_MANY_POPUP;
	}

	public void setMODE_SELECT_MANY_POPUP(String mode_select_many_popup) {
		MODE_SELECT_MANY_POPUP = mode_select_many_popup;
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {
	
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	/**
	 * @return the recordSize
	 */
	public Integer getRecordSize() {
		Map<String, Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
//		Map viewMap = getFacesContext().getExternalContext().getSessionMap();
		recordSize = (Integer) viewMap.get("recordSize");
		if(recordSize==null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize the recordSize to set
	 */
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}
	
	
	private void openPopup(String javaScriptText) {
		logger.logInfo("openPopup() started...");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);			
			logger.logInfo("openPopup() completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("openPopup() crashed ", exception);
		}
	}

	
	public boolean isSortAscending() {
		return sortAscending;
	}
	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}
	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public HtmlInputText getTextMyName() {
		return textMyName;
	}
	public void setTextMyName(HtmlInputText textMyName) {
		this.textMyName = textMyName;
	}
	public HtmlSelectOneMenu getListMyAge() {
		return listMyAge;
	}
	public void setListMyAge(HtmlSelectOneMenu listMyAge) {
		this.listMyAge = listMyAge;
	}
}
