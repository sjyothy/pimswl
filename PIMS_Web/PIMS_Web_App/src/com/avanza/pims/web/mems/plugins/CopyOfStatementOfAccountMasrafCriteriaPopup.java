package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.mems.EndowmentView;
import com.avanza.ui.util.ResourceUtil;

public class CopyOfStatementOfAccountMasrafCriteriaPopup extends AbstractMemsBean
{	
	
	
	private static final long serialVersionUID = 1L;
	HttpServletRequest request;
	EndowmentView endowment;
	private Date fromDate;
	private Date toDate;
	public CopyOfStatementOfAccountMasrafCriteriaPopup() 
	{
		endowment = new EndowmentView();
		request   = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if( sessionMap.get( WebConstants.EndowmentManage.EndowmentStatement ) != null)
		{
			endowment = ( EndowmentView )sessionMap.remove( WebConstants.EndowmentManage.EndowmentStatement )  ;
		}
		else if(  viewMap.get( WebConstants.EndowmentManage.EndowmentStatement ) != null )
		{
			endowment = ( EndowmentView )viewMap.get( WebConstants.EndowmentManage.EndowmentStatement );
		}
		if(this.toDate == null)
		{
			this.toDate= new Date();
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( endowment != null )
		{
			viewMap.put( WebConstants.EndowmentManage.EndowmentStatement, endowment );
		}
	}

	@SuppressWarnings("unchecked")
	public void onOpenEndowmentStatementOfAccount()
	{
		try
		{
			updateValuesFromMaps();
			if(endowment == null || endowment.getEndowmentId() ==null) return;
			StatementOfAccountCriteria statmntReportCriteria;
			statmntReportCriteria= new StatementOfAccountCriteria(
																	ReportConstant.Report.STATEMENT_OF_ACCOUNT_ENDOWMENT,
																	ReportConstant.Processor.STATEMENT_OF_ACCOUNT_ENDOWMENT,
																	CommonUtil.getLoggedInUser()
																 );
//			DateFormat formatterWithOutTime = new SimpleDateFormat("dd/MM/yyyy");
			if( fromDate != null )
			{
				statmntReportCriteria.setFromDate( fromDate );//formatterWithOutTime.parse( "1/12/2014"));
			}
			if(toDate != null )
			{
				statmntReportCriteria.setToDate( toDate ); //formatterWithOutTime.parse("3/12/2014"));
			}
			else
			{
			    statmntReportCriteria.setToDate( new Date() ); //formatterWithOutTime.parse("3/12/2014"));
			}
			statmntReportCriteria.setEndowmentId( endowment.getEndowmentId());
//			if( endowment.getCostCenter() != null )
//			{
//				statmntReportCriteria.getCostCenters().add( endowment.getCostCenter() );
//			}
			
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, statmntReportCriteria);
			executeJavaScript( "openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenEndowmentStatmntOfAccount|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	public EndowmentView getEndowment() {
		return endowment;
	}

	public void setEndowment(EndowmentView endowment) {
		this.endowment = endowment;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}    

}
