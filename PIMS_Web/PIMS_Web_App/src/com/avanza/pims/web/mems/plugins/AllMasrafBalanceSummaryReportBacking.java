package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import com.avanza.pims.Utils.generatorViews.MasarifSearchView;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.ui.util.ResourceUtil;

public class AllMasrafBalanceSummaryReportBacking extends AbstractMemsBean
{	
	
	
	private static final long serialVersionUID = 1L;
	HttpServletRequest request;
	MasarifSearchView  masraf;
	private String forYear;
	private Date fromDate;
	private Date toDate;
	private Boolean showSubMasarif;
	private List<SelectItem> yearsList  = new ArrayList<SelectItem>();
	
	public AllMasrafBalanceSummaryReportBacking() 
	{
		masraf = new MasarifSearchView ();
		request   = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if( sessionMap.get( WebConstants.Masraf.MasrafStatement ) != null)
		{
			masraf = ( MasarifSearchView )sessionMap.remove( WebConstants.Masraf.MasrafStatement)  ;
		}
		else if(  viewMap.get( WebConstants.Masraf.MasrafStatement ) != null )
		{
			masraf = ( MasarifSearchView )viewMap.remove( WebConstants.Masraf.MasrafStatement)  ;
		}
		if(this.toDate == null)
		{
			this.toDate= new Date();
		}
		if( viewMap.get("yearsList") != null )
		{
			this.yearsList  = (ArrayList<SelectItem>)viewMap.get("yearsList"); 
		}
		else
		{
			this.yearsList   = new ArrayList<SelectItem>();
			for (int i=2015;i<=2030;i++)
			{
				String year = String.valueOf( i ) ;
				this.yearsList.add( new SelectItem ( year,year ) );
			}
		}
//		if( viewMap.get("forYear") != null )
//		{
//			this.forYear  = viewMap.get("forYear").toString(); 
//		}
//		if( viewMap.get("showSubMasarif")!=null )
//		{
//			showSubMasarif = (Boolean)viewMap.get("showSubMasarif");
//		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( masraf != null )
		{
			viewMap.put( WebConstants.Masraf.MasrafStatement, masraf);
		}
//		if(this.forYear != null)
//		{
//			viewMap.put("forYear",this.forYear);
//		}
		if(this.yearsList != null)
		{
			viewMap.put("yearsList",this.yearsList);
		}
//		if( showSubMasarif != null )
//		{
//			viewMap.put("showSubMasarif",showSubMasarif);
//		}
	}
	

	@SuppressWarnings("unchecked")
	public void onOpenStatementOfAccount()
	{
		try
		{
			updateValuesFromMaps();
			if( forYear == null || 	forYear.equals("-1"))return;
			
			StatementOfAccountCriteria statmntReportCriteria;
			statmntReportCriteria= new StatementOfAccountCriteria(
																	ReportConstant.Report.ALL_MASRAF_BALANCE_SUMMARY,
																	ReportConstant.Processor.ALL_MASRAF_BALANCE_SUMMARY,
																	CommonUtil.getLoggedInUser()
																  );
			if( forYear != null )
			{
				String nextYear = String.valueOf( Integer.valueOf(forYear) + 1 );
				statmntReportCriteria.setForYear(forYear);
				statmntReportCriteria.setFromDate(   statmntReportCriteria.getFormatterWithOutTime().parse("31/03/"+forYear) );
				statmntReportCriteria.setToDate(     statmntReportCriteria.getFormatterWithOutTime().parse("30/03/"	 +nextYear) );
				
				String previousYear = String.valueOf( Integer.valueOf(forYear) - 1 );
				statmntReportCriteria.setPreviousYear(previousYear);
				statmntReportCriteria.setFromDatePreviousYear( statmntReportCriteria.getFormatterWithOutTime().parse("31/03/"+previousYear) );
				statmntReportCriteria.setToDatePreviousYear(   statmntReportCriteria.getFormatterWithOutTime().parse("30/03/"+forYear) );
				
			}
			if( showSubMasarif != null && showSubMasarif )
			{
				statmntReportCriteria.setShowSubMasrafTrx("0");
			}
			else
			{
				statmntReportCriteria.setShowSubMasrafTrx("1");
			}
			statmntReportCriteria.setMasrafId( masraf.getMasrafId() );

			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, statmntReportCriteria);
			executeJavaScript( "openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenStatementOfAccount|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public MasarifSearchView getMasraf() {
		return masraf;
	}

	public void setMasraf(MasarifSearchView masraf) {
		this.masraf = masraf;
	}

	public Boolean getShowSubMasarif() {
		return showSubMasarif;
	}

	public void setShowSubMasarif(Boolean showSubMasarif) {
		this.showSubMasarif = showSubMasarif;
	}


	public List<SelectItem> getYearsList() {
		return yearsList;
	}

	public void setYearsList(List<SelectItem> yearsList) {
		this.yearsList = yearsList;
	}

	public String getForYear() 
	{
		return forYear;
	}

	public void setForYear(String forYear) {
		this.forYear = forYear;
	}


}
