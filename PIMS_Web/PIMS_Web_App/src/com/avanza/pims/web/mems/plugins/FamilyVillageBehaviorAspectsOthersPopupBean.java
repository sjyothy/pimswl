package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.FamilyVillageBehavioralAspectService;
import com.avanza.pims.ws.vo.mems.BehaviorAspectsOthersView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageBehaviorAspectsOthersPopupBean extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private List<String> hobbiesTypes;
	private List<String> goodInDealingWith;
	private List<String> notGoodInDealingWith;
	BehaviorAspectsOthersView pageView;	
	
	private Long personId;
	protected HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
	public FamilyVillageBehaviorAspectsOthersPopupBean() 
	{
		pageView = new BehaviorAspectsOthersView();
		pageView.setCreatedBy (getLoggedInUserId());
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			if(!isPostBack())
			{
				if (request.getParameter("personId") != null) 
				{
					personId = Long.valueOf( request.getParameter("personId").toString() );
					pageView.setPersonId(personId);
				}
			}
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		if(viewMap.get("pageView") != null)
		{
			pageView =  (BehaviorAspectsOthersView)viewMap.get("pageView");
		}
		if(viewMap.get("hobbiesTypes") != null)
		{
			hobbiesTypes =  (ArrayList<String>)viewMap.get("hobbiesTypes");
		}
		
		if(viewMap.get("goodInDealingWith") != null)
		{
			goodInDealingWith =  (ArrayList<String>)viewMap.get("goodInDealingWith");
		}
		if(viewMap.get("notGoodInDealingWith") != null)
		{
			notGoodInDealingWith =  (ArrayList<String>)viewMap.get("notGoodInDealingWith");
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
		if(pageView != null)
		{
			viewMap.put("pageView",pageView);
		}
		if( hobbiesTypes != null && hobbiesTypes.size()>0)
		{
			viewMap.put("hobbiesTypes",hobbiesTypes);
		}
		if( goodInDealingWith!= null && goodInDealingWith.size()>0)
		{
			viewMap.put("goodInDealingWith",goodInDealingWith);
		}
		if( notGoodInDealingWith!= null && notGoodInDealingWith.size()>0)
		{
			viewMap.put("notGoodInDealingWith",notGoodInDealingWith);
		}
		
	}	
	
	@SuppressWarnings("unchecked")
	private boolean hasErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
//		
//		if( pageView.getMonthlyIncomeIdString() == null || pageView.getMonthlyIncomeIdString().equals("-1")) {
//			errorMessages.add(CommonUtil.getBundleMessage("researchFamilyVillageBeneficiary.msg.FamilyAspectFinancial.monthlyIncomeRequired"));
//			return true;
//		}
//		
//		if( pageView.getMonthlyIncomeSourceId() == null || pageView.getMonthlyIncomeSourceId().equals("-1")) {
//			errorMessages.add(CommonUtil.getBundleMessage("researchFamilyVillageBeneficiary.msg.FamilyAspectFinancial.incomeSourceRequired"));
//			return true;
//		}
//		
//		if( pageView.getDescription() == null || pageView.getDescription().trim().length() <=0) {
//			errorMessages.add(CommonUtil.getBundleMessage("violationDetails.msg.descriptionRequired "));
//			return true;
//		}
		return hasSaveErrors;
		
	}

	
	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			String csv = "";
			
			for (String eachangerManifestations : hobbiesTypes) {
				csv = csv + eachangerManifestations+",";
			}
			if(StringHelper.isNotEmpty(csv))
			{
				pageView.setHobbiesType( csv.substring(0,csv.length()-1));
			}
			for (String eachangerReasons: goodInDealingWith) {
				csv = csv + eachangerReasons+",";
			}
			if(StringHelper.isNotEmpty(csv))
			{
				pageView.setGoodInDealingWith( csv.substring(0,csv.length()-1));
			}
			for (String eachbehaviorInPain: notGoodInDealingWith) {
				csv = csv + eachbehaviorInPain+",";
			}
			if(StringHelper.isNotEmpty(csv))
			{
				pageView.setNotGoodInDealingWith( csv.substring(0,csv.length()-1));
			}
			
			if(hasErrors())return;
			pageView.setUpdatedBy (getLoggedInUserId());
			FamilyVillageBehavioralAspectService.addBehaviorAspectsOthers( pageView);
			successMessages.add( ResourceUtil.getInstance().getProperty( "commons.savedSuccessfully" ) );
		}
		catch ( Exception e )
		{
			logger.LogException( "onSave--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public BehaviorAspectsOthersView getPageView() {
		return pageView;
	}

	public void setPageView(BehaviorAspectsOthersView pageView) {
		this.pageView = pageView;
	}

	public List<String> getHobbiesTypes() {
		return hobbiesTypes;
	}

	public void setHobbiesTypes(List<String> hobbiesTypes) {
		this.hobbiesTypes = hobbiesTypes;
	}

	public List<String> getGoodInDealingWith() {
		return goodInDealingWith;
	}

	public void setGoodInDealingWith(List<String> goodInDealingWith) {
		this.goodInDealingWith = goodInDealingWith;
	}

	public List<String> getNotGoodInDealingWith() {
		return notGoodInDealingWith;
	}

	public void setNotGoodInDealingWith(List<String> notGoodInDealingWith) {
		this.notGoodInDealingWith = notGoodInDealingWith;
	}


}
