package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.entity.BlockingDetails;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.BlockingAmountService;
import com.avanza.ui.util.ResourceUtil;

public class BlockingDetailsPopup extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	HttpServletRequest request;
	List<BlockingDetails> list = new ArrayList<BlockingDetails>();
	private HtmlDataTable dataTable;
	public BlockingDetailsPopup() 
	{
		request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		Long fileId	  = null;
		List<Long> longList = new ArrayList<Long>();
		if( request.getParameter("personId") != null)
		{
			
			String[] split =  request.getParameter("personId").toString().split(",");
			for (String string : split)
			{
				if( string.length() > 0 )
				{
					longList.add( new Long( string ) );
				}
			}
		}
		if( request.getParameter("fileId") != null)
		{
			fileId =  new Long(request.getParameter("fileId").toString());
		}
		list = BlockingAmountService.getBlockingListForArg(longList, null, fileId);
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( list != null )
		{
			viewMap.put( "list", list );
		}
	}

	public List<BlockingDetails> getList() {
		return list;
	}

	public void setList(List<BlockingDetails> list) {
		this.list = list;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}	
    

}
