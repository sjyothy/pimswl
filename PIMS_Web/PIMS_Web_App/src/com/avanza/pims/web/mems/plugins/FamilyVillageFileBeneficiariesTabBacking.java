package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.EndFileAsso;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.InheritanceBeneficiary;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.FamilyVillageFileService;
import com.avanza.pims.ws.vo.InheritanceBeneficiaryView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageFileBeneficiariesTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private List< InheritanceBeneficiaryView> dataList;
	
	private HtmlDataTable dataTable;
	private FamilyVillageFileService  familyVillageFileService;
	private PersonView person = new PersonView();
	private String tabMode;
    private String beneficairyDOB;
    
	public FamilyVillageFileBeneficiariesTabBacking() 
	{
		familyVillageFileService = new FamilyVillageFileService();
		dataTable        = new HtmlDataTable();
		dataList         = new ArrayList<InheritanceBeneficiaryView>();
		tabMode          = Constant.EndowmentFile.MODE_READ_ONLY;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.FamilVillagFileProcess.FAMILY_VILLAG_FILE_ERRORS_BENEFICIARY, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.FamilVillagFileProcess.FAMILY_VILLAG_FILE_SUCCESS_BENEFICIARY, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			tabMode = (String) viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		if( viewMap.get( "beneficiariesDataList" ) != null )
		{
			dataList = (ArrayList<InheritanceBeneficiaryView>)viewMap.get( "beneficiariesDataList" );
		}
		if(viewMap.get("personBeneficiary")!= null)
		{
			person= (PersonView)viewMap.get("personBeneficiary");
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, tabMode );
		}
		if( dataList != null && dataList.size()  > 0  )
		{
			viewMap.put( "beneficiariesDataList", dataList );
		}
		if(person != null )
		{
			viewMap.put("personBeneficiary",person);
		}
	}	
	
	public void setBeneficiaryFields()
	{
		person = new PersonView();
		person.setIsDeleted(0l);
		person.setRecordStatus(1l);
		person.setCreatedOn(new Date());
		person.setCreatedBy(getLoggedInUserId());
		person.setUpdatedOn(new Date());
		person.setUpdatedBy(getLoggedInUserId());
		person.setIsCompany(0l);
		
	}
	public void populateTab( RequestView requestView ) throws Exception
	{
		updateValuesFromMaps();
		loadDataList(requestView.getInheritanceFileId());
		setTotalRows( dataList.size() );
		setBeneficiaryFields();
		updateValuesToMaps();
	}

	private void loadDataList(Long inheritanceFileId) throws Exception {
		dataList = FamilyVillageFileService.getInheritanceBeneficiariesByFileId(inheritanceFileId);
	}
	
	
	@SuppressWarnings("unchecked")
	private boolean hasAddBeneficiaryErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if( person.getFirstName()== null || person.getFirstName().trim().length()<=0 ) {
			errorMessages.add(CommonUtil.getBundleMessage("familyVillagefile.msg.beneficiaryNameRequired"));
			return true;
		}
		if( person.getDateOfBirth() == null ) {
			errorMessages.add(CommonUtil.getBundleMessage("person.Msg.DateOfBirth"));
			return true;
		}
		if( person.getGender()== null || person.getGender().trim().equals("-1")) {
			errorMessages.add(CommonUtil.getBundleMessage("person.message.gender"));
			return true;
		}
		
		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onAddBeneficiary()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			if(hasAddBeneficiaryErrors())return;
			RequestView requestView = (RequestView)viewMap.get( WebConstants.REQUEST_VIEW ) ;
			
			FamilyVillageFileService.addFileBeneficiaries(person, requestView.getInheritanceFileView());
			loadDataList(requestView.getInheritanceFileId());
		}
		catch ( Exception e )
		{
			logger.LogException( "onAddBeneficiary--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.FamilVillagFileProcess.FAMILY_VILLAG_FILE_ERRORS_BENEFICIARY, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.FamilVillagFileProcess.FAMILY_VILLAG_FILE_SUCCESS_BENEFICIARY, successMessages );
			}
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDelete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			updateValuesFromMaps();
			successMessages = new ArrayList<String>();
			InheritanceBeneficiaryView obj = (InheritanceBeneficiaryView)dataTable.getRowData();
			InheritanceBeneficiary ib =  EntityManager.getBroker().findById(InheritanceBeneficiary.class, obj.getInheritanceBeneficiaryId());
			ib.setUpdatedOn(new Date());
			ib.setUpdatedBy(getLoggedInUserId() );
			ib.setIsDeleted(1l);
			EntityManager.getBroker().update(ib);
			dataList.remove(obj);
			updateValuesToMaps();
				
			successMessages.add( ResourceUtil.getInstance().getProperty( "revenueAndExpensesManage.msg.deleteRecord" ) );
		}
		catch ( Exception e )
		{
			logger.LogException( "onDelete--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.FamilVillagFileProcess.FAMILY_VILLAG_FILE_ERRORS_BENEFICIARY, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.FamilVillagFileProcess.FAMILY_VILLAG_FILE_SUCCESS_BENEFICIARY, successMessages );
			}
		}
		
	}
    
    public boolean hasNoBeneficiaries()
	{
		EndFileAsso obj = ( EndFileAsso )dataTable.getRowData();
        return (obj.getEndFileBens() == null || obj.getEndFileBens().size() <= 0  );
	}
	
		
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}

	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<InheritanceBeneficiaryView> getDataList() {
		return dataList;
	}

	public void setDataList(List<InheritanceBeneficiaryView> dataList) {
		this.dataList = dataList;
	}

	public String getBeneficairyDOB() {
		return beneficairyDOB;
	}

	public void setBeneficairyDOB(String beneficairyDOB) {
		this.beneficairyDOB = beneficairyDOB;
	}

	public PersonView getPerson() {
		return person;
	}

	public void setPerson(PersonView person) {
		this.person = person;
	}

}
