package com.avanza.pims.web.mems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.AssetMems;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.NextRevFollowup;
import com.avanza.pims.entity.ThirdPartyPropUnits;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.AssetMemsService;
import com.avanza.pims.ws.mems.ThirdPartyPropUnitsService;
import com.avanza.pims.ws.utility.RevenueFollowupService;
import com.avanza.pims.ws.vo.AssetsGridView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.ui.util.ResourceUtil;


public class RevenueFollowupBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(RevenueFollowupBean.class);
	HttpServletRequest request;
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_OPENED = "OPENED";
	private static final String PAGE_MODE_CLOSE = "CLOSE";
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	
	public boolean viewModePopup;
    private String pageTitle;
    private String pageMode;
    
    private String txtRemarks;
    
    private RevenueFollowupService service = new RevenueFollowupService ();
    protected HtmlCommandButton btnNextFollowup = new HtmlCommandButton();
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	private NextRevFollowup  pageObject;
	private List<SelectItem> thirdPartyUnits = new ArrayList<SelectItem>();
	private HtmlDataTable dataTableBox;
	public RevenueFollowupBean()
	{
		request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
//				 MEMSRevenueFollowupReminderJob job = new MEMSRevenueFollowupReminderJob();
//				 job.execute();
			}
			else
			{
				updateValuesFromMap();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.RevenueFollowup.ProcedureKey);
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.RevenueFollowup.ProcedureKey);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.RevenueFollowup.EXTERNAL_ID);
		viewMap.put("noteowner", WebConstants.RevenueFollowup.NOTES_OWNER);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		loadAttachmentsAndComments( null );
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
		  getDataFromTaskList();
		}
		else if ( sessionMap.get( WebConstants.RevenueFollowup.RevenueFollowup ) != null )
		{
		  getDataFromSearch();
		}
		else if(request.getParameter("id")!=null)
		{
			long programId = Long.valueOf( request.getParameter("id").toString());
			getPageObjectDetails( programId );
		}
		else if ( sessionMap.get(WebConstants.RevenueFollowup.OLD_FOLLOWUP ) !=null )
		{
			setDataForFirstTime();
			NextRevFollowup  oldFollowup = (NextRevFollowup)sessionMap.remove(WebConstants.RevenueFollowup.OLD_FOLLOWUP );
			pageObject.setAssetMems( oldFollowup.getAssetMems() );
			pageObject.setEndowment(oldFollowup.getEndowment());
			pageObject.setContactAddress( oldFollowup.getContactAddress());
			pageObject.setContactName( oldFollowup.getContactName() );
			pageObject.setContactNumber( oldFollowup.getContactNumber() );
			if( oldFollowup.getAssetMems().getExpectedRevenue() != null )
			{
				pageObject.setExpRevenue(  oldFollowup.getAssetMems().getExpectedRevenue()  );
			}
			else
			{
			pageObject.setExpRevenue( oldFollowup.getExpRevenue() );
			}
		}
			
		else
		{
			setDataForFirstTime();
		}
		updateValuesFromMap();
		getPageModeFromStatus();
	}
	
	@SuppressWarnings( "unchecked" )
	public void prerender()
	{
		try
		{
			renderNextFollowupButton();
		}
		catch ( Exception e )
		{
			logger.LogException( "prerender|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}

	@SuppressWarnings("unchecked")
	private void renderNextFollowupButton() throws Exception 
	{
			btnNextFollowup.setRendered(false);
			if( pageObject.getStatus().getDomainDataId().compareTo(WebConstants.RevenueFollowupStatus.Closed_Id ) != 0  )
			{return;}
			
			String openFollowup = null;
			if( pageObject.getAssetMems().getAssetId() != null )
			{
				openFollowup = service.getOpenFollowupForParameters(pageObject.getAssetMems().getAssetId(), null, null);
			}
			else if( pageObject.getEndowment().getEndowmentId() != null )
			{
				openFollowup = service.getOpenFollowupForParameters(null, pageObject.getEndowment().getEndowmentId(), null);
			}
			
			if( openFollowup == null )
			{
				btnNextFollowup.setRendered(true);
			}
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		pageObject = new NextRevFollowup();
		pageObject.setCreatedBy(  getLoggedInUserId()  );
		pageObject.setUpdatedBy(  getLoggedInUserId()  );
		
		DomainData status =  new DomainData();
		DomainDataView ddView = CommonUtil.getDomainDataView(WebConstants.RevenueFollowupStatus.Key, WebConstants.RevenueFollowupStatus.Opened_Id);
		status.setDataDescAr(ddView.getDataDescAr());
		status.setDataDescEn(ddView.getDataDescEn());
		status.setDomainDataId(ddView.getDomainDataId());
		pageObject.setStatus( status );

	}
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromStatus()throws Exception
	{
		setPageMode( PAGE_MODE_OPENED );
		if( this.pageObject == null  || 
			this.pageObject.getPrimaryId() == null ||
			this.pageObject.getStatus() == null 
			
		  ) 
		{ return; }
		
		boolean hasUserTask = getUserTask() != null;
		boolean closeTaskCreatedAndFetched = getStatusNotified() && hasUserTask;
		
		
		if( ( getStatusOpened() || closeTaskCreatedAndFetched)&& 
			this.pageObject.getPrimaryId() != null)
		{
			
		  this.setPageMode( PAGE_MODE_CLOSE );
			
		}
		else if( getStatusNotified()   && !closeTaskCreatedAndFetched  ) 
		{
			this.setPageMode( PAGE_MODE_VIEW );
		}
		else if( getStatusClosed()  || !closeTaskCreatedAndFetched  ) 
		{
			this.setPageMode( PAGE_MODE_VIEW );
		}
	}
	public boolean getStatusOpened() {
		return this.pageObject.getStatus().getDomainDataId().compareTo(  Constant.RevenueFollowupStatus.Opened_Id  ) == 0;
	}
	public boolean getStatusClosed() {
		return this.pageObject.getStatus().getDomainDataId().compareTo(  Constant.RevenueFollowupStatus.Closed_Id  ) == 0;
	}
	public boolean getStatusNotified() {
		return this.pageObject.getStatus().getDomainDataId().compareTo(  Constant.RevenueFollowupStatus.Notified_Id   ) == 0;
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.RevenueFollowup.RevenueFollowup ) != null )
		{
			pageObject  = ( NextRevFollowup )viewMap.get( WebConstants.RevenueFollowup.RevenueFollowup  ) ;
			pageObject.setUpdatedBy( getLoggedInUserId() );
		}
		if( viewMap.get( WebConstants.RevenueFollowup.ThirdPartyUnits ) != null )
		{
			thirdPartyUnits  = ( ArrayList<SelectItem> )viewMap.get( WebConstants.RevenueFollowup.ThirdPartyUnits  ) ;
		}  
		 updateValuesToMap();

	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( pageObject != null )
		{
		  viewMap.put( WebConstants.RevenueFollowup.RevenueFollowup, pageObject );
		}
		if( thirdPartyUnits != null )
		{
			viewMap.put( WebConstants.RevenueFollowup.ThirdPartyUnits, thirdPartyUnits ); 
		}
	}
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		pageObject = ( NextRevFollowup )sessionMap.remove( WebConstants.RevenueFollowup.RevenueFollowup);
		if( this.pageObject !=null )
		{
		  getPageObjectDetails( pageObject.getPrimaryId() );
		}
	}
	
	
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  UserTask userTask = ( UserTask )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  setUserTask(userTask);
	  if( userTask.getTaskAttributes().get( WebConstants.RevenueFollowup.FOLLOWUP_ID) == null ){ return; }
	  long  id = new Long  ( userTask.getTaskAttributes().get( WebConstants.RevenueFollowup.FOLLOWUP_ID ) );
	  getPageObjectDetails( id );
	  
	  
    }
	
	@SuppressWarnings( "unchecked" )
	private void getPageObjectDetails( long id ) throws Exception 
	{
		pageObject = service.getFollowupById( id );
		loadAttachmentsAndComments( id );
		loadThirdPartyUnitById();
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments( pageObject.getPrimaryId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.RevenueFollowup.NOTES_OWNER;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, pageObject.getPrimaryId()  );
	    	  }
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.RevenueFollowup.NOTES_OWNER;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    		
	    	  CommonUtil.saveRemarksAsComments(pageObject.getPrimaryId() , txtRemarks, notesOwner) ;
	    	NotesController.saveNotes(notesOwner, pageObject.getPrimaryId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }

	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( pageObject!= null &&  pageObject.getPrimaryId() != null)
		 {
			 loadAttachmentsAndComments( pageObject.getPrimaryId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }

	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchEndowments()
	{
		if( sessionMap.get(WebConstants.SELECTED_ROW)== null )
		{return;}
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	

			updateValuesFromMap();
			Endowment endowment = (Endowment)sessionMap.remove(WebConstants.SELECTED_ROW);
			this.pageObject.setEndowment(endowment);
			this.pageObject.setAssetMems(null);
			this.pageObject.setThirdPartyPropUnits(null);
			loadThirdPartyUnitById( );
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromSearchEndowments --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchAsset()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			AssetsGridView assetGridViewRow = (AssetsGridView)sessionMap.remove(WebConstants.AssetSearchOutcomes.ASSETS_SEARCH_SELECTED_ONE_ASSET);
			AssetMems asset  = AssetMemsService.getAssetMemsById(assetGridViewRow.getAssetId());
			this.pageObject.setEndowment(null);
			this.pageObject.setAssetMems(asset);
			this.pageObject.setThirdPartyPropUnits(null);
			loadThirdPartyUnitById();
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromSearchAsset --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings( "unchecked" )
	private void loadThirdPartyUnitById( ) throws Exception
	{
		Long endowmentId = null;
		Long assetId     =null;
		thirdPartyUnits = new ArrayList<SelectItem>();
		if( this.pageObject.getAssetMems() !=null && this.pageObject.getAssetMems().getAssetId() != null )
		{
			assetId = this.pageObject.getAssetMems().getAssetId();
		}
		else if( this.pageObject.getEndowment() !=null && this.pageObject.getEndowment().getEndowmentId() != null )
		{
			endowmentId = this.pageObject.getEndowment().getEndowmentId();
		}
		List<ThirdPartyPropUnits> list = ThirdPartyPropUnitsService.getAllThirdPartyUnits(endowmentId, assetId);
		for (ThirdPartyPropUnits unit : list) 
		{
		  SelectItem item = new SelectItem(
				  							unit.getThirdPartyPropUnitId().toString(),
				  							unit.getFloorNumber()+"-"+unit.getUnitNumber()
				           
		                                   );
		  thirdPartyUnits.add(item);
		}
		Collections.sort(thirdPartyUnits, ListComparator.LIST_COMPARE);
	}
	
	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if(pageObject== null || pageObject.getPrimaryId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.RevenueFollowup.NOTES_OWNER,pageObject.getPrimaryId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if(  hasBasicInfoError() )
		{
			hasSaveErrors = true;

		}

		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}

		return hasSaveErrors;
		
	}

	public boolean isThirdPartyEndowmentProperty()throws Exception
	{
		if( pageObject.getEndowment() != null && 
			pageObject.getEndowment().getEndowmentId() != null &&
			
			pageObject.getEndowment().getAssetType().getAssetTypeId().compareTo(WebConstants.AssetType.LAND_PROPERTIES)==0
		  )
		{
			return true;
		}
		
		return false;
	}
	
	public boolean isThirdPartyAssetProperty()throws Exception
	{
		if( pageObject.getAssetMems() != null && 
			pageObject.getAssetMems().getAssetId() != null &&
			pageObject.getAssetMems().getIsManagerAmaf()!= null &&
		
			pageObject.getAssetMems().getAssetType().getAssetTypeId().compareTo(WebConstants.AssetType.LAND_PROPERTIES)==0
			)
		{
			return true;
		}
		
		return false;
	}
	private boolean hasBasicInfoError()throws Exception 
	{
		boolean hasSaveErrors=false;
		boolean isProperty =  isThirdPartyAssetProperty() || isThirdPartyEndowmentProperty(); 
		if( 
			( pageObject.getEndowment() == null || pageObject.getEndowment().getEndowmentId() == null ) &&
			( pageObject.getAssetMems() == null || pageObject.getAssetMems().getAssetId()     == null )
	      )
    	{
    		errorMessages.add( ResourceUtil.getInstance().getProperty( "revenueFollowup.msg.assetRequired" ) );
    		hasSaveErrors =  true;
    	}
		
		else if(  isProperty &&
		         ( pageObject.getThirdPartyPropUnits() == null || pageObject.getThirdPartyPropUnits().getThirdPartyPropUnitId() == null)
		     
		  )
		{
    		errorMessages.add( ResourceUtil.getInstance().getProperty( "revenueFollowup.msg.unitRequired" ) );
    		hasSaveErrors =  true;
    	}
		else
		{
		  Long assetId      =  pageObject.getAssetMems() != null && 
				               pageObject.getAssetMems().getAssetId()!= null?
				            		   										pageObject.getAssetMems().getAssetId(): 
				            		   										null;
		  Long endowmentId  = pageObject.getEndowment() != null && 
	       					  pageObject.getEndowment().getEndowmentId() != null?
						    		   											pageObject.getEndowment().getEndowmentId(): 
						    		   											null;
						    		   										
		  Long unitId       = pageObject.getThirdPartyPropUnits() != null &&
		   					  pageObject.getThirdPartyPropUnits().getThirdPartyPropUnitId() != null ?
		   							  																pageObject.getThirdPartyPropUnits().getThirdPartyPropUnitId():
		   							  																null;
		  String currentFollowUpNumber = pageObject.getRefNum() != null ?pageObject.getRefNum() :""; 
		  String openedFollowup = service.getOpenFollowupForParameters(assetId, endowmentId, unitId);
		  
		  if(  
			   openedFollowup != null  && 
			   openedFollowup.compareTo( currentFollowUpNumber )!= 0 
			)
		   {
				String message = java.text.MessageFormat.format( 
																 ResourceUtil.getInstance().getProperty( "revenueFollowup.lbl.followupPresent" ),
																 openedFollowup 
															   );
				errorMessages.add( message );
	    		hasSaveErrors =  true;
	    		openedFollowup = null;
		   }
		}
		return hasSaveErrors;
	}

	@SuppressWarnings( "unchecked" )
	public String onInitiateNextRevFollowup()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			sessionMap.put(WebConstants.RevenueFollowup.OLD_FOLLOWUP , pageObject );

		}
		catch (Exception exception) 
		{
			logger.LogException( "onInitiateNextRevFollowup --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "InitiateNextRevFollowup";
		
	}

	
	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			saveInTransaction();
			getPageObjectDetails( pageObject.getPrimaryId() );
			saveCommentsAttachment( "revenueFollowup.event.saved");
			successMessages.add( ResourceUtil.getInstance().getProperty("revenueFollowup.msg.saved"));
			this.setPageMode( PAGE_MODE_CLOSE );
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	
	private void saveInTransaction() throws Exception 
	{
		try
		{
			Long status = Constant.RevenueFollowupStatus.Opened_Id;
//            if(pageObject.getStatus() != null)
//            {
//            	status = null;
//            }
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistpageObject( status );
			ApplicationContext.getContext().getTxnContext().commit();
			updateValuesToMap();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistpageObject(Long statusId) throws Exception 
	{
		DomainData status = new DomainData();
		if( statusId != null )
		{
			status.setDomainDataId(statusId);
			if(status!=null) pageObject.setStatus(status);
		}
		
		service.persist( pageObject );
	}
	
	private boolean hasReasonProvided() throws Exception
	{
		if(pageObject.getClosingRemarks()==null || pageObject.getClosingRemarks().trim().length() <= 0 )
		{
			//errorMessages.add(ResourceUtil.getInstance().getProperty("pageObject.msg.reasonRequired")  );
			return false;
		}
		return true;
	}
	
	private boolean hasOnCloseErrors() throws Exception
	{
		boolean hasErrors=false;
		if( !hasReasonProvided() )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("revenueFollowup.msg.closingremarksRequired")); 
			return true;
		}
		
		return hasErrors;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onClose()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status =  Constant.RevenueFollowupStatus.Closed_Id;
		String msg    =  "revenueFollowup.msg.closed";
		String event  =  "revenueFollowup.event.closed";
		try	
		{	
			if( hasOnCloseErrors() ){ return; }
			 updateValuesFromMap();
			 boolean hasUserTask = getUserTask() != null;
			 boolean closeTaskCreatedAndFetched = getStatusNotified() && hasUserTask;
				
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 pageObject.setClosedBy(getLoggedInUserId());
			 persistpageObject(status );
			 getPageObjectDetails( pageObject.getPrimaryId() );
			 updateValuesToMap();
			 if( closeTaskCreatedAndFetched )
			 {
			  setTaskOutCome(TaskOutcome.OK);
			 }
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onClose--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}

		
	}
	
	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
    	try
    	{
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = getUserTask();
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    	}
    	catch(PIMSWorkListException ex)
    	{
    		
    		logger.LogException("setTaskOutCome| Error Occured...",ex);
    		throw ex;
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("setTaskOutCome| Error Occured...",ex);
    		throw ex;
    	}
    }



	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask )
	{
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}


	public String getPageTitle() 
	{
	 this.setPageTitle( ResourceUtil.getInstance().getProperty( "revenueFollowup.lbl.heading" ) );
	 return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW )   )
		{
			return true;
		}
		return false;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowClose()
	{
		if(  
			getPageMode().equals( PAGE_MODE_CLOSE ) 
		  )
		{
			return true;
		}
		return false;
	}
	public boolean getPageModeView()
	{
		return getPageMode().equals( PAGE_MODE_VIEW ) ;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowClosingRemarks()
	{
		if( ( 
				getPageMode().equals( PAGE_MODE_VIEW ) &&
				this.pageObject.getStatus() != null && 
				this.pageObject.getStatus().getDomainDataId().compareTo(WebConstants.RevenueFollowupStatus.Closed_Id)==0
			) ||
			getPageMode().equals( PAGE_MODE_CLOSE ) 
			
		  )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public HtmlDataTable getDataTableBox() {
		return dataTableBox;
	}

	public void setDataTableBox(HtmlDataTable dataTableBox) {
		this.dataTableBox = dataTableBox;
	}

	public List<SelectItem> getThirdPartyUnits() {
		return thirdPartyUnits;
	}

	public void setThirdPartyUnits(List<SelectItem> thirdPartyUnits) {
		this.thirdPartyUnits = thirdPartyUnits;
	}

	public boolean isViewModePopup() {
		return viewModePopup;
	}

	public void setViewModePopup(boolean viewModePopup) {
		this.viewModePopup = viewModePopup;
	}

	public NextRevFollowup getPageObject() {
		return pageObject;
	}

	public void setPageObject(NextRevFollowup pageObject) {
		this.pageObject = pageObject;
	}

	public HtmlCommandButton getBtnNextFollowup() {
		return btnNextFollowup;
	}

	public void setBtnNextFollowup(HtmlCommandButton btnNextFollowup) {
		this.btnNextFollowup = btnNextFollowup;
	}
}