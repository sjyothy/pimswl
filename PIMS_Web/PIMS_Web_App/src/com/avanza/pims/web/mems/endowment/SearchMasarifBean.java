package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.Utils.generatorViews.MasarifSearchView;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.MasarifService;
import com.avanza.ui.util.ResourceUtil;

public class SearchMasarifBean extends AbstractSearchBean {
	private static final long serialVersionUID = 1L;
	private String DEFAULT_SORT_FIELD = "";
	private String DATA_LIST = "DATA_LIST";
	MasarifService service = new MasarifService();

	MasarifSearchView criteria = new MasarifSearchView();
	List<MasarifSearchView> list = new ArrayList<MasarifSearchView>();
	private boolean englishLocale;

	@SuppressWarnings("unchecked")
	@Override
	public void init() {
		super.init();
		try {
			if (!isPostBack()) {
				setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
				setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
				setSortField(DEFAULT_SORT_FIELD);
				setSortItemListAscending(true);
				checkDataInQueryString();
			}
		} catch (Exception es) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void checkDataInQueryString() {
		checkPageMode();
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() {

		if (request.getParameter(VIEW_MODE) != null) {

			if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) {
				viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);

			} else if (request.getParameter(VIEW_MODE).equals(
					MODE_SELECT_MANY_POPUP)) {
				viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);

			}
		}
	}
	@SuppressWarnings("unchecked")
	public String onDisburse() 
	{
		try 
		{
			MasarifSearchView row = (MasarifSearchView) dataTable.getRowData();
			sessionMap.put(WebConstants.Masraf.MasrafObject, row);
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onDisburse |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "disburse";
	}

	@SuppressWarnings("unchecked")
	public void onOpenStatementOfAccountCriteriaPopup()
	{
		try
		{
			MasarifSearchView masarifView = ( MasarifSearchView )dataTable.getRowData();
			
			if(masarifView == null || masarifView.getMasrafId() ==null) return;
			sessionMap.put(WebConstants.Masraf.MasrafStatement, masarifView);
			executeJavaScript( "openStatementofAccountMasrafCriteriaPopup();");
			
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenStatementOfAccountCriteriaPopup|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	@SuppressWarnings("unchecked")
	public String onEdit() {
		try {
			// GET_SELECTED_ROW
			MasarifSearchView row = (MasarifSearchView) dataTable.getRowData();

			try {
				// PASS_SELECTED_ROW_TO_POPUP
				if (row != null) {
					sessionMap.put(WebConstants.SELECTED_ROW, row);
				}
				// OPEN_UPDATE_POPUP
				executeJavascript("openEditPopup();");
			} catch (Exception ex) {
				errorMessages.add(ResourceUtil.getInstance().getProperty(
						MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		} catch (Exception e) {
			logger.LogException("onEdit |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "editMasraf";
	}

	public void onSearch() {
		try {
			loadDataList();
			pageFirst();// at last
		} catch (Exception e) {
			logger.LogException("onSearch |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public String onDelete() {
		try {
			// TODO:
		} catch (Exception e) {
			logger.LogException("onDelete |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}

	// We have to look for the list population for facilities

	public void doSearch() {
		try {
			loadDataList();
		} catch (Exception e) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("doSearch" + "|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void doSearchItemList() {
		try {
			loadDataList();
		} catch (Exception e) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("doSearchItemList" + "|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public List<MasarifSearchView> loadDataList() throws Exception {

		// ///////////////////////////////////////////// For server side paging
		// paging/////////////////////////////////////////
//		int totalRows = 0;
//		totalRows = service.getMasrafRecordSize(criteria);
//		setTotalRows(totalRows);
//		doPagingComputations();
		// ////////////////////////////////////////////////////////////////////////////////////////////
		list = service.getAllMasraf(criteria);

		if (list.isEmpty() || list == null) 
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			forPaging(0);// searchedRows
			if(viewMap.get(DATA_LIST) != null)
			{
				viewMap.remove(DATA_LIST);
			}
		}
		else
		{
			this.setList(list);
			forPaging(list.size());// searchedRows
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public void onOpenAllMasrafBalanceSummary()
	{
		try
		{
			StatementOfAccountCriteria statmntReportCriteria;
			statmntReportCriteria= new StatementOfAccountCriteria(
																	ReportConstant.Report.ALL_MASRAF_BALANCE_SUMMARY,
																	ReportConstant.Processor.ALL_MASRAF_BALANCE_SUMMARY,
																	CommonUtil.getLoggedInUser()
																 );
			if( criteria.getMasrafName() != null && criteria.getMasrafName().trim().length()>0 )
			{
				statmntReportCriteria.setMasrafName(criteria.getMasrafName());
			}

			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, statmntReportCriteria);
			executeJavaScript( "openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenAllMasrafBalanceSummary|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	/**
	 * @return the paginatorMaxPages
	 */
	public Integer getPaginatorMaxPages() {

		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}

	/**
	 * @param paginatorMaxPages
	 *            the paginatorMaxPages to set
	 */
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}

	/**
	 * @return the paginatorRows
	 */
	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	/**
	 * @param paginatorRows
	 *            the paginatorRows to set
	 */
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}

	/**
	 * @return the recordSize
	 */
	@SuppressWarnings("unchecked")
	public Integer getRecordSize() {
		if (viewMap.get("recordSize") != null) {
			recordSize = (Integer) viewMap.get("recordSize");
		}
		if (recordSize == null)
			recordSize = 0;
		return recordSize;
	}

	/**
	 * @param recordSize
	 *            the recordSize to set
	 */
	@SuppressWarnings("unchecked")
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
		if (this.recordSize != null)
			viewMap.put("recordSize", this.recordSize);
	}

	public MasarifSearchView getCriteria() {
		return criteria;
	}

	public void setCriteria(MasarifSearchView criteria) {
		this.criteria = criteria;
	}

	@SuppressWarnings("unchecked")
	public List<MasarifSearchView> getList() {
		if(viewMap.get(DATA_LIST) != null){
			list = (List<MasarifSearchView>) viewMap.get(DATA_LIST);
		} 
		return list;
	}

	@SuppressWarnings("unchecked")
	public void setList(List<MasarifSearchView> list) {
		if(list != null && list.size() > 0){
			viewMap.put(DATA_LIST, list);
		}
		this.list = list;
	}

	public boolean isSelectOnePopup() {
		if (viewMap.get(VIEW_MODE) != null
				&& viewMap.get(VIEW_MODE).toString().compareTo(
						MODE_SELECT_ONE_POPUP) == 0)
			return true;
		else
			return false;
	}

	public boolean isSelectManyPopup() {
		if (viewMap.get(VIEW_MODE) != null
				&& viewMap.get(VIEW_MODE).toString().compareTo(
						MODE_SELECT_MANY_POPUP) == 0)
			return true;
		else
			return false;
	}

	public boolean isPopup() {
		return (isPageModeSelectManyPopUp() || isPageModeSelectOnePopUp());
	}

	@SuppressWarnings("unchecked")
	public void onSingleSelect() {
		MasarifSearchView selectedMasraf = null;
		selectedMasraf = (MasarifSearchView) dataTable.getRowData();
		sessionMap.put(WebConstants.SELECTED_ROW,selectedMasraf);
		executeJavascript("closeWindow();");
	}

	@SuppressWarnings("unchecked")
	public void onMultiSelect() {
		try {
			list = getList();
			List<MasarifSearchView> selectedMasarif = new ArrayList<MasarifSearchView>();
			if(list != null && list.size() > 0){
				for(MasarifSearchView masraf: list){
					if(masraf.isSelected()){
						selectedMasarif.add(masraf);
					}
				}
			}
			else{
					errorMessages = new ArrayList<String>();
					errorMessages.add(CommonUtil.getBundleMessage("masraf.errMsg.noSelectMade"));
			}
			if(selectedMasarif.size() > 0){
				viewMap.put(WebConstants.SELECTED_ROWS, selectedMasarif);
				executeJavascript("closeWindow();");
			}
			else{
				errorMessages = new ArrayList<String>();
				errorMessages.add(CommonUtil.getBundleMessage("masraf.errMsg.noSelectMade"));
			}
		} catch (Exception e) {
			errorMessages = new ArrayList<String>(0);
			logger.LogException("doSearch" + "|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void executeJavascript(String javascript) {
		String METHOD_NAME = "executeJavascript()";
		try {
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		} catch (Exception exception) {
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	public boolean isEnglishLocale() {
		return CommonUtil.getIsEnglishLocale();
	}

	public void setEnglishLocale(boolean englishLocale) {
		this.englishLocale = englishLocale;
	}
	public String onAdd() {
		return "addMasraf";
	}

}
