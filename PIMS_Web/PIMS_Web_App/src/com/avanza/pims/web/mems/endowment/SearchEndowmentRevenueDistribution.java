package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.entity.EndowmentFile;
import com.avanza.pims.entity.PropertyRevenueDistributionSearchView;
import com.avanza.pims.entity.PropertyRevenueDistributionView;
import com.avanza.pims.entity.Request;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.RevenueDistributionDetailsService;
import com.avanza.pims.ws.mems.endowment.PropertyDistributionService;
import com.avanza.ui.util.ResourceUtil;

public class SearchEndowmentRevenueDistribution extends AbstractSearchBean 
{
	private static final long serialVersionUID = 1L;
	private final String DEFAULT_SORT_FIELD = "endowmentId";
	private final String DATA_LIST = "DATA_LIST";
	PropertyDistributionService service = new PropertyDistributionService();

	PropertyRevenueDistributionView criteria = new PropertyRevenueDistributionView();
	List<PropertyRevenueDistributionSearchView> dataList = new ArrayList<PropertyRevenueDistributionSearchView>();
	

	@SuppressWarnings("unchecked")
	@Override
	public void init() 
	{
		super.init();
		try 
		{
			
			if (!isPostBack()) 
			{
				initData();
			}
		} catch (Exception es) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void initData() throws Exception 
	{
		setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
		setSortField(DEFAULT_SORT_FIELD);
		setSortItemListAscending(false);
		checkDataInQueryString();
	}

	private void checkDataInQueryString() {
		checkPageMode();
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() 
	{

		if (request.getParameter(VIEW_MODE) != null) 
		{

			if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);

			}
			else if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);

			}
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onSingleSelect() 
	{
		try 
		{
			EndowmentFile obj = null;
			obj = (EndowmentFile) dataTable.getRowData();
			sessionMap.put(WebConstants.SELECTED_ROW,obj);
			executeJavascript("closeWindow();");
		}
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onSingleSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	
	@SuppressWarnings("unchecked")
	public String onManage() 
	{
		try 
		{
			PropertyRevenueDistributionSearchView row = (PropertyRevenueDistributionSearchView) dataTable.getRowData();
			if (row != null) 
			{
				sessionMap.put(WebConstants.SELECTED_ROW, row);
				return "manage";
			}
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onManage |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}
	
	@SuppressWarnings("unchecked")
	public void onDistribute() 
	{
		try 
		{
			PropertyRevenueDistributionSearchView row = (PropertyRevenueDistributionSearchView) dataTable.getRowData();
			
			RevenueDistributionDetailsService rddService = new RevenueDistributionDetailsService();
			Request request  = null;
			rddService.distributeFromPropertyRevenueDistributionSearchView(request, row, CommonUtil.getLoggedInUser() );
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onDistribute |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	public void onSearch() 
	{
		try 
		{
			pageFirst();
		} 
		catch (Exception e) 
		{
			logger.LogException("onSearch |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void doSearchItemList()
	{
		try {
				loadDataList();
			}
		    catch (Exception e){
		    	errorMessages = new ArrayList<String>(0);
		 		logger.LogException("doSearchItemList"+"|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		    }
	}

	@SuppressWarnings("unchecked")
	public void loadDataList() throws Exception 
	{

		int totalRows = 0;
		totalRows = service.getTotalNumberOfRecords( criteria );
		setTotalRows(totalRows);
		doPagingComputations();

		dataList = service.search(criteria, getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());

		if (dataList== null || dataList.isEmpty() ) 
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			forPaging(0);
		}
		
		this.setDataList( dataList );
		forPaging(getTotalRows());
	}



	@SuppressWarnings("unchecked")
	public List<PropertyRevenueDistributionSearchView> getDataList() 
	{
		if(viewMap.get(DATA_LIST) != null)
		{
			dataList = (List<PropertyRevenueDistributionSearchView>) viewMap.get(DATA_LIST);
		} 
		return dataList ;
	}

	@SuppressWarnings("unchecked")
	public void setDataList(List<PropertyRevenueDistributionSearchView> list) 
	{
		this.dataList = list;
		if(this.dataList != null )
		{
			viewMap.put(DATA_LIST, list);
		}

	}

	
	@SuppressWarnings("unchecked")
	
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javascript);
	}

	public PropertyRevenueDistributionView getCriteria() {
		return criteria;
	}

	public void setCriteria(PropertyRevenueDistributionView criteria) {
		this.criteria = criteria;
	}


	
}
