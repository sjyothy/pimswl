package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.ui.util.ResourceUtil;

public class WaqifBalanceDetailsReportBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = 1L;
	HttpServletRequest request;
	private Date fromDate;
	private Date toDate;
	private StatementOfAccountCriteria criteria;
	
	public StatementOfAccountCriteria getCriteria() 
	{
		return criteria;
	}

	public void setCriteria(StatementOfAccountCriteria criteria) {
		this.criteria = criteria;
	}

	@SuppressWarnings( "unchecked" )
	public WaqifBalanceDetailsReportBacking() 
	{
		criteria = new StatementOfAccountCriteria();
		request   = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException("init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if(this.toDate == null)
		{
			this.toDate= new Date();
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{	}

	@SuppressWarnings("unchecked")
	public void onOpenStatementOfAccount()
	{
		try
		{
			updateValuesFromMaps();
			
			StatementOfAccountCriteria statmntReportCriteria;
			statmntReportCriteria= new StatementOfAccountCriteria(
																	ReportConstant.Report.WAQIF_BALANCE_DETAILS,
																	ReportConstant.Processor.WAQIF_BALANCE_DETAILS,
																	getLoggedInUserObj().getFullName()
																  );
			if( fromDate != null )
			{
				statmntReportCriteria.setFromDate( fromDate );
			}
			else
			{
				statmntReportCriteria.setFromDate( criteria.getFormatterWithOutTime().parse("1/9/2013") );
			}
			if( toDate != null )
			{
				statmntReportCriteria.setToDate(toDate);
			}
			
			if( criteria.getWaqifName() != null && criteria.getWaqifName().trim().length() > 0)
			{
				statmntReportCriteria.setWaqifName(criteria.getWaqifName().toLowerCase().trim() );
			}
			if( criteria.getEndowmentFileName() != null &&  criteria.getEndowmentFileName().trim().length() > 0)
			{
				statmntReportCriteria.setEndowmentFileName(criteria.getEndowmentFileName().toLowerCase().trim() );
			}
			if( criteria.getEndowmentFileNumber() != null &&  criteria.getEndowmentFileNumber().trim().length() > 0 )
			{
				statmntReportCriteria.setEndowmentFileNumber(criteria.getEndowmentFileNumber().toLowerCase().trim() );
			}
			
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, statmntReportCriteria);
			executeJavaScript( "openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
		catch(Exception ex)
		{
			logger.LogException("onOpenStatementOfAccount|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
}
