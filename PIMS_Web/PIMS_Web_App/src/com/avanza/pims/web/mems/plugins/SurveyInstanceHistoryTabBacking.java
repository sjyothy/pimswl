package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.SurveyType;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.services.SurveyService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.SurveyInstanceView;
import com.avanza.ui.util.ResourceUtil;

public class SurveyInstanceHistoryTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private List< SurveyInstanceView> dataListSurveyInstances;
	private HtmlDataTable dataTableSurveyInstances;
	private String selectOneSurveyType;
	private String tabMode;
	private Long personId;
	private Long surveyTypeId;
	private Long surveyForm;
	private Integer recordSizeSurveyInstances;
	public SurveyInstanceHistoryTabBacking() 
	{
		dataTableSurveyInstances             =  new HtmlDataTable();
		dataListSurveyInstances		        =  new ArrayList<SurveyInstanceView>();
		recordSizeSurveyInstances  		=  0;
		tabMode          = Constant.EndowmentFile.MODE_READ_ONLY;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.SurveyBehaviorMonitoringFormKeys.SURVEY_HISTORY_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.SurveyBehaviorMonitoringFormKeys.SURVEY_HISTORY_TAB_SUCCESS, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			tabMode = (String) viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		if( viewMap.get( "dataListSurveyInstances" ) != null )
		{
			dataListSurveyInstances = (ArrayList<SurveyInstanceView>)viewMap.get( "dataListSurveyInstances" );
		}
				
		if( viewMap.get( "recordSizeSurveyInstances" ) != null )
		{
			recordSizeSurveyInstances = Integer.parseInt( viewMap.get( "recordSizeSurveyInstances" ).toString() );
		}
		if( viewMap.get("surveyTypeId" )!= null )
		{
			surveyTypeId = Long.valueOf( viewMap.get("surveyTypeId").toString() );
		}
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		if(viewMap.get("selectOneSurveyType")!=null)
		{
			selectOneSurveyType = viewMap.get("selectOneSurveyType").toString();
		}
		if(viewMap.get("surveyFor")!=null)
		{
			surveyForm = Long.valueOf( viewMap.get("surveyFor").toString() );
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, tabMode );
		}
		if(recordSizeSurveyInstances !=null)
		{
			viewMap.put("recordSizeSurveyInstances",recordSizeSurveyInstances);
		}
		if( dataListSurveyInstances != null && dataListSurveyInstances.size()  > 0  )
		{
			viewMap.put( "dataListSurveyInstances", dataListSurveyInstances );
		}
		if(surveyTypeId != null)
		{
			viewMap.put("surveyTypeId", surveyTypeId);
		}
		if( surveyForm != null )
		{
		  viewMap.put("surveyFor",surveyForm);	
		}
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
		if(selectOneSurveyType!=null && !selectOneSurveyType.equals("-1"))
		{
			viewMap.put("selectOneSurveyType",selectOneSurveyType);
		}
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getSurveyTypes() throws Exception {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		try {

			if (viewMap.get("SurveyTypesList") != null) {

				return (List<SelectItem>) viewMap.get("SurveyTypesList");
			}

			List<SurveyType> list = SurveyService.getSurveyTypes(surveyForm);
			for (SurveyType type : list) 
			{
				selectItems.add(
								new SelectItem(
												type.getTypeId().toString(), isEnglishLocale() ? 
																										type.getTypeEn()
																										: type.getTypeAr()
											  )
							   );
			}
			Collections.sort(selectItems, ListComparator.LIST_COMPARE);
			viewMap.put("SurveyTypesList", selectItems);

		} catch (Exception e) {
			logger.LogException("getSurveyTypes() crashed", e);
		}
		return selectItems;

	}
	public void populateTab( PersonView personView, Long surveyFor) throws Exception
	{
		updateValuesFromMaps();
		this.surveyForm=surveyFor;
		personId = personView.getPersonId();
		updateValuesToMaps();
	}
	@SuppressWarnings( "unchecked" )
	public void onSurveyTypeChanged(ValueChangeEvent event)
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			
			updateValuesFromMaps();
			this.surveyTypeId = Long.valueOf( event.getNewValue().toString());
			logger.logDebug("surveyTypeId :"+surveyTypeId );
			
			successMessages = new ArrayList<String>();
			if(surveyTypeId != null && surveyTypeId.compareTo(-1l)!= 0 )
			{
				dataListSurveyInstances= SurveyService.getSurveyInstancesForPersonAndSurveyType(
																								this.surveyTypeId , 
																								personId 
																								);
				recordSizeSurveyInstances=dataListSurveyInstances.size();
			}
			else
			{
				dataListSurveyInstances = new ArrayList<SurveyInstanceView>();
				recordSizeSurveyInstances=0;
			}
			getDataListSurveyInstances();
			updateValuesToMaps();
		}
		catch ( Exception e )
		{
			logger.LogException( "onSurveyTypeChanged--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.SurveyBehaviorMonitoringFormKeys.SURVEY_HISTORY_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.SurveyBehaviorMonitoringFormKeys.SURVEY_HISTORY_TAB_SUCCESS, successMessages );
			}
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onDeleteSurveyInstance()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteSurveyInstance--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.SurveyBehaviorMonitoringFormKeys.SURVEY_HISTORY_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.SurveyBehaviorMonitoringFormKeys.SURVEY_HISTORY_TAB_SUCCESS , successMessages );
			}
		}
		
	}
	
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}
	public Long getPersonId() {
		
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public List<SurveyInstanceView> getDataListSurveyInstances() {
		return dataListSurveyInstances;
	}

	public void setDataListSurveyInstances(
			List<SurveyInstanceView> dataListSurveyInstances) {
		this.dataListSurveyInstances = dataListSurveyInstances;
	}

	public HtmlDataTable getDataTableSurveyInstances() {
		return dataTableSurveyInstances;
	}

	public void setDataTableSurveyInstances(HtmlDataTable dataTableSurveyInstances) {
		this.dataTableSurveyInstances = dataTableSurveyInstances;
	}

	public Integer getRecordSizeSurveyInstances() {
		return recordSizeSurveyInstances;
	}

	public void setRecordSizeSurveyInstances(Integer recordSizeSurveyInstances) {
		this.recordSizeSurveyInstances = recordSizeSurveyInstances;
	}

	public Long getSurveyTypeId() {
		if( viewMap.get("surveyTypeId" )!= null )
		{
			surveyTypeId = Long.valueOf( viewMap.get("surveyTypeId").toString() );
		}
		return surveyTypeId;
	}

	public void setSurveyTypeId(Long surveyTypeId) {
		this.surveyTypeId = surveyTypeId;
	}

	public String getSelectOneSurveyType() {
		return selectOneSurveyType;
	}

	public void setSelectOneSurveyType(String selectOneSurveyType) {
		this.selectOneSurveyType = selectOneSurveyType;
	}

	public Long getSurveyForm() {
		return surveyForm;
	}

	public void setSurveyForm(Long surveyForm) {
		this.surveyForm = surveyForm;
	}

}
