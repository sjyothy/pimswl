package com.avanza.pims.web.mems.minors;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.util.Logger;
import com.avanza.pims.Utils.DateUtil;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritedAssetService;
import com.avanza.pims.ws.vo.mems.BeneficiariesPickListView;
import com.avanza.pims.ws.vo.mems.InheritedAssetShareInfoView;
import com.avanza.pims.ws.vo.mems.InheritedAssetView;
import com.avanza.ui.util.ResourceUtil;

public class BeneficiarySharesPopupController extends AbstractMemsBean 
{
	private static final long serialVersionUID = 1L;
	private transient Logger logger = Logger.getLogger(BeneficiarySharesPopupController.class);
	private HtmlDataTable dataTable = new HtmlDataTable();
	private HtmlCommandButton btnDone = new HtmlCommandButton();
	private String fileId;
	private Map<Long,Double> assignedSharesMap = new HashMap<Long,Double>();
	private Map<String,String> beneficiaryDetailsMap = new HashMap<String, String>();
	private Map<String,String> beneficiaryAssetShareMap = new HashMap<String, String>();
	Boolean markUnMarkAll = false;
	HtmlSelectOneMenu cmbBnfList = new HtmlSelectOneMenu();
	HtmlInputText txtShariaPercent = new HtmlInputText();
	private static final String ASSET_SHARE_INFO = "ASSET_SHARE_INFO";
	List<InheritedAssetShareInfoView> list = new ArrayList<InheritedAssetShareInfoView>();
	protected InheritedAssetService inheritedAssetService = new InheritedAssetService();
	DecimalFormat oneDForm = new DecimalFormat("#.00");
	@SuppressWarnings("unchecked")
	public void init() 
	{
		super.init();
		try 
		{
			
			if (!isPostBack()) 
			{
				
				assignedSharesMap = (HashMap<Long,Double>)sessionMap.remove( WebConstants.BeneficiarySharesPopup.ASSIGNED_SHARES_MAP ); 
				setListFromInheritedAssetView( (ArrayList<InheritedAssetView>)sessionMap.remove( WebConstants.BeneficiarySharesPopup.INHERITED_ASSET_MEMS_VIEW_LIST ) );
				setFileId(sessionMap.remove( WebConstants.InheritanceFile.FILE_ID).toString() );
				
				loadBeneficiaryList();
			}

		} catch (Exception es)
		{
			logger.LogException( "init|Error Occured", es);
			errorMessages = new ArrayList<String>(0);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}
	@SuppressWarnings("unchecked")
	private void loadBeneficiaryList() throws Exception 
	{
		long fileId = Long.parseLong( getFileId() );
		List<BeneficiariesPickListView> benefPickListView = inheritedAssetService.getBeneficiariesView( fileId );
		ArrayList<SelectItem> bnfList = new ArrayList<SelectItem>(0);
		beneficiaryDetailsMap  = getBeneficiaryDetailsMap();
		
		for(BeneficiariesPickListView singleItem : benefPickListView) 
		{
			String inheritanceBeneficiaryId = singleItem.getInheritedBeneficiaryId().toString();
			SelectItem selItem = new SelectItem(inheritanceBeneficiaryId ,singleItem.getFullName() );
			bnfList.add(selItem);
			beneficiaryDetailsMap.put(inheritanceBeneficiaryId , singleItem.getFullName()+"^"+ singleItem.getSharePercentage() );
		}
		
		Collections.sort( bnfList, ListComparator.LIST_COMPARE );
		viewMap.put(WebConstants.BeneficiarySharesPopup.BENENEFICIARY_LIST,bnfList);
		setBeneficiaryDetailsMap(beneficiaryDetailsMap);
		setBeneficiaryAssetShareMap(  inheritedAssetService.getAssetBeneficiaryShareMap(fileId) );
		if( sessionMap.get( WebConstants.BeneficiarySharesPopup.INHERITED_BENEF_ID) != null)
		{
			String benefId = sessionMap.remove( WebConstants.BeneficiarySharesPopup.INHERITED_BENEF_ID).toString();
			cmbBnfList.setValue(benefId);
			onBeneficiaryChanged();
		}
	}

	
	@SuppressWarnings("unchecked")
	public List<InheritedAssetShareInfoView> getList() 
	{
		if(	viewMap.get(ASSET_SHARE_INFO) != null){
			
			list = (ArrayList<InheritedAssetShareInfoView>) viewMap.get(ASSET_SHARE_INFO);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public void setList(List<InheritedAssetShareInfoView> list)
	{
		if(list != null)
		{
			viewMap.put(ASSET_SHARE_INFO, list);
		}
		this.list = list;
	}
	
	@SuppressWarnings("unchecked")
	private void setListFromInheritedAssetView(List<InheritedAssetView> inheritedAssetViewList)
	{
		beneficiaryDetailsMap  = getBeneficiaryDetailsMap();
		beneficiaryAssetShareMap = getBeneficiaryAssetShareMap();
		List<InheritedAssetShareInfoView> sharesList = new ArrayList<InheritedAssetShareInfoView>();
		if(inheritedAssetViewList != null)
		{
			for (InheritedAssetView inheritedAssetView : inheritedAssetViewList) 
			{
				InheritedAssetShareInfoView shareView = new InheritedAssetShareInfoView();
				shareView.setInheritedAssetId(inheritedAssetView.getInheritedAssetId().toString());
				shareView.setCreatedBy(CommonUtil.getLoggedInUser());
				shareView.setCreatedOn(DateUtil.getCurrentDate());
				shareView.setUpdatedBy(CommonUtil.getLoggedInUser());
				shareView.setUpdatedOn(DateUtil.getCurrentDate());
				shareView.setInheritedAssetNumber(inheritedAssetView.getAssetMemsView().getAssetNumber() );
				shareView.setInheritedAssetNameEn(inheritedAssetView.getAssetMemsView().getAssetNameEn());
				shareView.setAssetMemsId( inheritedAssetView.getAssetMemsView().getAssetId());
				shareView.setInheritedAssetNameAr(inheritedAssetView.getAssetMemsView().getAssetNameAr());
				shareView.setInheritedAssetTypeEn( inheritedAssetView.getAssetMemsView().getAssetTypeView().getAssetTypeNameEn());
				shareView.setInheritedAssetTypeAr( inheritedAssetView.getAssetMemsView().getAssetTypeView().getAssetTypeNameAr());
				shareView.setTotalShare(inheritedAssetView.getTotalShareValueString());
				Double assignedShares = assignedSharesMap.get(inheritedAssetView.getInheritedAssetId());
				assignedShares  = assignedShares  != null ? assignedShares:0;
				Double remaining = inheritedAssetView.getTotalShareValue() - assignedShares ;
				if( remaining.compareTo(0.0)<0 )
				{
					System.out.println("Remaining Negative");
				}
				shareView.setRemainingShare( remaining.compareTo( 0.0 )==0?"0.00":oneDForm.format( remaining ) );
				populateBeneficiarySharesDetails(shareView);
			    sharesList.add(shareView);
			
			}
			
		}
		setList(sharesList);
		
		this.list = sharesList;
	}

	
	@SuppressWarnings( "unchecked" )
	public void onDone() 
    {
		errorMessages = new ArrayList<String>(0);
		try 
		{
			sessionMap.put( WebConstants.BeneficiarySharesPopup.INHERITED_ASSET_MEMS_VIEW_LIST ,getList());
			executeJavascript("javaScript:closeWindowSubmit();");
		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("onBeneficiaryChanged| Error Occured...", e);

		}
	}


	@SuppressWarnings( "unchecked" )
	public void onCheckChanged() 
    {
		errorMessages = new ArrayList<String>(0);
		try 
		{
			
			for (InheritedAssetShareInfoView shareView : getList()) {
				
				
				shareView.setSelectWill( markUnMarkAll );
			}
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));

		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("onBeneficiaryChanged| Error Occured...", e);

		}
	}

	@SuppressWarnings( "unchecked" )
	public void onBeneficiaryChanged() 
    {
		errorMessages = new ArrayList<String>(0);
		try 
		{
			
			beneficiaryDetailsMap =  getBeneficiaryDetailsMap();
			beneficiaryAssetShareMap = getBeneficiaryAssetShareMap();
			for (InheritedAssetShareInfoView shareView : getList()) 
			{
			  populateBeneficiarySharesDetails(shareView);
			}

		} 
		catch (Exception e) 
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException("onBeneficiaryChanged| Error Occured...", e);

		}
	}

	
	@SuppressWarnings("unchecked")
	private void populateBeneficiarySharesDetails(InheritedAssetShareInfoView shareView) 
	{
		
		if( cmbBnfList.getValue() == null  || cmbBnfList.getValue().toString().equals("-1"))
		{
			shareView.setInheritedBeneficiaryId(null);
			shareView.setPctWeight("0");
			shareView.setOldPctWeight( "0");
			shareView.setIsWill(0L );
		    txtShariaPercent.setValue("");
		    return;
		}
		
		String id = cmbBnfList.getValue().toString();
		shareView.setInheritedBeneficiaryId(id);
		String detail = beneficiaryDetailsMap.get(id);
		String[] detailArr = detail.split("\\^");
		shareView.setInheritedBeneficiaryFullName( detailArr[0] );
		shareView.setInheritedBeneficiaryShariaShare( detailArr[1] );
		String key = id+"_"+shareView.getInheritedAssetId().toString();
		String value = beneficiaryAssetShareMap.get( key );
		if( value != null )
		{
            String[] willShare = value.split("_") ;
            shareView.setIsWill( Long.parseLong(  willShare[0] )  );
          	shareView.setPctWeight( willShare[1]  );
          	shareView.setOldPctWeight( willShare[1]  );
            if( !willShare[2].equals( "-1" ) )
            {
            	shareView.setInheritedAssetShareId(   willShare[2] );
            }
		
		}
		else
		{
			  Double totalShare   = Double.parseDouble( shareView.getTotalShare() );
			  Double shariaShare  = Double.parseDouble( detailArr[1] );
			  Double remainingShare = shareView.getRemainingShare() != null ?Double.parseDouble(shareView.getRemainingShare()):null;
              if( 
            	  remainingShare != null && 
            	  remainingShare.compareTo(0D)!= 0 &&
            	  totalShare.compareTo( shariaShare ) >= 0 &&
            	  remainingShare.compareTo(shariaShare) >= 0 
                )
              {
            	  shareView.setPctWeight(    detailArr[1] );
            	  shareView.setOldPctWeight( detailArr[1] );
            	  remainingShare = remainingShare - shariaShare;
            	  shareView.setRemainingShare(  
            			  						remainingShare.compareTo( 0.0 )==0?"0.00":oneDForm.format( remainingShare ) 
            	  							 );
            	  
        
					String mapValue = "0"+"_" +shariaShare+"_-1";
					beneficiaryAssetShareMap.put( key, mapValue );
					setBeneficiaryAssetShareMap(beneficiaryAssetShareMap);

              }
              else
              {
            	  shareView.setPctWeight( "0" );
            	  shareView.setOldPctWeight( "0");
              }
		  shareView.setIsWill( 0l  );
		  shareView.setInheritedAssetShareId(null );
		  
		}
		txtShariaPercent.setValue(detailArr[1]);
	}
	
	public String getFileId() {
		
		if(  viewMap.get("fileId")!= null){
			fileId= viewMap.get("fileId").toString();
	     }
		return fileId;
	}
	@SuppressWarnings("unchecked")
	public void setFileId(String fileId) {
		this.fileId = fileId;
		viewMap.put("fileId", fileId);
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SelectItem> getBeneficiaryList() {
		if (viewMap.containsKey(WebConstants.BeneficiarySharesPopup.BENENEFICIARY_LIST)) { 
			return (ArrayList<SelectItem>)viewMap.get(WebConstants.BeneficiarySharesPopup.BENENEFICIARY_LIST);
		}
		return new ArrayList<SelectItem>();		
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, String> getBeneficiaryDetailsMap() {
		if (viewMap.get("beneficiaryDetailsMap")!= null) {
			beneficiaryDetailsMap  = (HashMap<String,String>)viewMap.get("beneficiaryDetailsMap");
		}
		else
		{
			beneficiaryDetailsMap = new HashMap<String, String>();
		}
		
		return beneficiaryDetailsMap;
	}
	
	@SuppressWarnings("unchecked")
	public void setBeneficiaryDetailsMap(Map<String, String> beneficiaryDetailsMap) {
		this.beneficiaryDetailsMap = beneficiaryDetailsMap;
		viewMap.put("beneficiaryDetailsMap",this.beneficiaryDetailsMap );
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public HtmlSelectOneMenu getCmbBnfList() {
		return cmbBnfList;
	}
	public void setCmbBnfList(HtmlSelectOneMenu cmbBnfList) {
		this.cmbBnfList = cmbBnfList;
	}
	@SuppressWarnings("unchecked")
	public Map<String, String> getBeneficiaryAssetShareMap() {
		
		if (viewMap.get("beneficiaryAssetShareMap")!= null) {
			beneficiaryAssetShareMap  = (HashMap<String,String>)viewMap.get("beneficiaryAssetShareMap");
		}
		else
		{
			beneficiaryAssetShareMap = new HashMap<String, String>();
		}
		
		return beneficiaryAssetShareMap;
	}
	@SuppressWarnings("unchecked")
	public void setBeneficiaryAssetShareMap(
			Map<String, String> beneficiaryAssetShareMap) {
		this.beneficiaryAssetShareMap = beneficiaryAssetShareMap;
		viewMap.put("beneficiaryAssetShareMap",this.beneficiaryAssetShareMap );
	}
	private void executeJavascript(String javascript) 
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();			
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
		
	}
	public HtmlInputText getTxtShariaPercent() {
		return txtShariaPercent;
	}
	public void setTxtShariaPercent(HtmlInputText txtShariaPercent) {
		this.txtShariaPercent = txtShariaPercent;
	}
	public Boolean getMarkUnMarkAll() {
		return markUnMarkAll;
	}
	public void setMarkUnMarkAll(Boolean markUnMarkAll) {
		this.markUnMarkAll = markUnMarkAll;
	}
	public HtmlCommandButton getBtnDone() {
		return btnDone;
	}
	public void setBtnDone(HtmlCommandButton btnDone) {
		this.btnDone = btnDone;
	}
}
