package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.bpel.proxy.MEMSDonationRequestBPELPortClient;
import com.avanza.pims.entity.EndProgPaymentTypes;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.PaymentType;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationDetails;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.endowment.EndowmentProgramService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.EndowmentProgramView;
import com.avanza.ui.util.ResourceUtil;


public class DonationRequestBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(DonationRequestBean.class);
	
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_RESUBMITTED = "PAGE_MODE_RESUBMITTED";
	private static final String PAGE_MODE_COLLECTION_REQUIRED = "COLLECTION_REQUIRED";
	
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_PAYMENT_DETAILS   = "paymentsTab";
	private static final String TAB_APPLICANTS   = "applicationTab";
    private String pageTitle;
    private String pageMode;
    private String hdnPersonId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;
    private String selectOneCollectedByUser;
    private String txtRemarks;
    private RequestService requestService = new RequestService();
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	private RequestView requestView ;
    private List<PaymentScheduleView> paymentList = new ArrayList<PaymentScheduleView>();	
    private PaymentScheduleView paymentView = new PaymentScheduleView();
    protected HtmlCommandButton btnSendBack =new HtmlCommandButton();
    private HtmlDataTable dataTable;
	public DonationRequestBean(){}
	private  List<SelectItem> programPaymentTypes = new ArrayList<SelectItem>();
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
				if (sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY) != null )
				{
					boolean isPaymentCollected = (Boolean) (sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY));
					if(isPaymentCollected)
				    {
							onMessageFromReceivePayments();
							
				    }
				}
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.DonationRequest.PROCEDURE_DONATION_REQUEST );
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.DonationRequest.PROCEDURE_DONATION_REQUEST );
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		viewMap.put("noteowner", WebConstants.REQUEST);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		
		loadAttachmentsAndComments( null );
		
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
		  getDataFromTaskList();
		}
		else if ( getRequestMap().get( WebConstants.REQUEST_VIEW) != null )
		{
		  getDataFromSearch();
		  
		}
		else
		{
			setDataForFirstTime();
		}
		
		updateValuesFromMap();
		getPageModeFromRequestStatus();
	}
	
	public void prerender(){
	
		if ( getPageMode().equals(PAGE_MODE_COLLECTION_REQUIRED) && 
				 viewMap.get("collectionCompleted" ) != null 
			   )
			{
				setPageMode(PAGE_MODE_VIEW);
			}
		if( !isTaskAvailable() && 
			( 
				  getPageMode().equals(PAGE_MODE_COLLECTION_REQUIRED)  || 
				  getPageMode().equals(PAGE_MODE_RESUBMITTED) 
			 )
		  )
		{
			if( viewMap.remove("fetchTaskNotified") == null  )
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("commons.msg.fectchtasktoperformaction"));
				viewMap.put("fetchTaskNotified", true);
			}
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		requestView = new RequestView();
		requestView.setCreatedOn(new Date());
		requestView.setCreatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedOn(new Date());
		requestView.setStatusId( WebConstants.REQUEST_STATUS_NEW_ID );
		requestView.setStatusDataValue( WebConstants.REQUEST_STATUS_NEW );
		requestView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		requestView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
		requestView.setRequestDate( new Date() );
		requestView.setRequestTypeId( WebConstants.MemsRequestType.DONATION_REQUEST );
		

	}
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromRequestStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		tabPanel.setSelectedTab(TAB_APPLICANTS);
		if( this.requestView == null  || 
			this.requestView.getRequestId() == null ||
			this.requestView.getStatusId() == null 
		  ) 
		{ return; }
		if( getStatusCollectionRequired() )
		{
			setPageMode( PAGE_MODE_COLLECTION_REQUIRED);
			tabPanel.setSelectedTab(TAB_PAYMENT_DETAILS );
		}
		else if(  getStatusRejectedResubmitted() )
		{
			setPageMode(PAGE_MODE_RESUBMITTED);
			tabPanel.setSelectedTab("commentsTab" );
			
		}
		else if (getStatusCompleted())
		{
			setPageMode(PAGE_MODE_VIEW);
			tabPanel.setSelectedTab(TAB_PAYMENT_DETAILS );
		}	
			
	}
	public boolean getStatusNew() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_NEW ) == 0;
	}
	public boolean getStatusCollectionRequired() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_COLLECTION_REQ ) == 0;
	}
	public boolean getStatusRejectedResubmitted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT) == 0;
	}
	public boolean getStatusCompleted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_COMPLETE ) == 0;
	}

	
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = ( RequestView )viewMap.get( WebConstants.REQUEST_VIEW ) ;
			requestView.setUpdatedBy(getLoggedInUserId());
		}
		if( viewMap.get(  WebConstants.DonationRequest.PaymentList) != null)
		{
			paymentList = (	ArrayList<PaymentScheduleView> )viewMap.get(  WebConstants.DonationRequest.PaymentList ); 
		}
		if( viewMap.get(  WebConstants.DonationRequest.PaymentView) != null)
		{
			paymentView = (	PaymentScheduleView )viewMap.get(  WebConstants.DonationRequest.PaymentView );
//			if (paymentView.getPaymentDueOn() == null)
//			{
//				paymentView.setPaymentDueOn(new Date());
//			}
		}
		if( viewMap.get(  "programPaymentTypes") != null)
		{
			programPaymentTypes =(ArrayList<SelectItem>)viewMap.get(  "programPaymentTypes") ;
		}
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( requestView != null )
		{
		  viewMap.put(  WebConstants.REQUEST_VIEW , requestView);
		}
		
		
		if( paymentList != null )
		{
			viewMap.put(  WebConstants.DonationRequest.PaymentList, paymentList);
		}
		if( paymentView != null )
		{
			viewMap.put(  WebConstants.DonationRequest.PaymentView,paymentView );
		}
		else
		{
			viewMap.put(  WebConstants.DonationRequest.PaymentView, new PaymentScheduleView() );
		}
		if( programPaymentTypes != null )
		{
			viewMap.put(  "programPaymentTypes", programPaymentTypes);
		}
	}
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		requestView = ( RequestView)getRequestMap().get( WebConstants.REQUEST_VIEW) ;
		if( this.requestView !=null )
		{
		  getDonationRequestDetails( requestView.getRequestId() );
		}
	}
	
	@SuppressWarnings(  "unchecked"  )
	private void populateApplicationDetailsTab()throws Exception
	{
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		String status = "";
		boolean isRequestNull =  null == requestView || null == requestView.getApplicantView() ||null == requestView.getRequestId();
		boolean isApplicantDiffFromPerson = isRequestNull || 
											!requestView.getApplicantView().getPersonId().toString().equals(hdnPersonId);
		if ( isApplicantDiffFromPerson && hdnPersonName!= null && hdnPersonName.trim().length() >0 ) 
		{
			bean.populateApplicationDetails(
						                    "", 
						                    "", 
						                    new Date(), 
						                    "", 
						                    hdnPersonName, 
						                    "", 
						                    hdnCellNo, 
						                    ""
						                  );
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, hdnPersonId);
			PersonView person = new PersonView();
			person.setPersonId( new Long( hdnPersonId ) );
			requestView.setApplicantView( person);
			
            return;
		}
		
		status = CommonUtil.getIsEnglishLocale()?requestView.getStatusEn():requestView.getStatusAr();
		bean.populateApplicationDetails(
				                         requestView.getRequestNumber(), 
					                     status, 
					                     requestView.getRequestDate(), 
					                     requestView.getDescription(), 
					                     requestView.getApplicantView().getPersonFullName(), 
					                     "", 
					                     requestView.getApplicantView().getCellNumber(), 
					                     requestView.getApplicantView().getEmail()
					                   );
		if( requestView.getApplicantView() != null && requestView.getApplicantView().getPersonId() != null  )
		{
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,requestView.getApplicantView().getPersonId() );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, requestView.getApplicantView().getPersonId() );
		}
		if ( requestView!= null && requestView.getStatus() != null && 
			!requestView.getStatusDataValue().equals( WebConstants.REQUEST_STATUS_NEW ) 
		    )
		{
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}
	}
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  UserTask userTask = ( UserTask )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  setUserTask(userTask);
	  if( userTask.getTaskAttributes().get(  WebConstants.REQUEST_ID ) == null ){ return; }
	  long  id = new Long  ( userTask.getTaskAttributes().get( WebConstants.REQUEST_ID ) );
	  getDonationRequestDetails( id );
	  
	  
    }
	
	@SuppressWarnings( "unchecked" )
	private void getDonationRequestDetails( long id ) throws Exception 
	{
		requestView = requestService.getEndowmentRelatedPaymentRequestById(id );
		if(requestView.getPaymentScheduleView() != null )
		{
			paymentList = requestView.getPaymentScheduleView();
		}
		populateApplicationDetailsTab();
		updateValuesToMap();
		loadAttachmentsAndComments( id );
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	
	
	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(requestView.getRequestId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.REQUEST;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, requestView.getRequestId());
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.REQUEST;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(requestView.getRequestId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, requestView.getRequestId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }

	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( requestView != null && requestView.getRequestId()!= null )
		 {
			 loadAttachmentsAndComments( requestView.getRequestId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMap();
			if(hdnPersonType.equals( "APPLICANT" ) )
			{
				populateApplicationDetailsTab();
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	@SuppressWarnings( "unchecked" )
	public void onPaymentMethodChanged()
	{
		try	
		{
			updateValuesFromMap();
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onPaymentMethodChanged|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromEndowmentProgramSearch()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			EndowmentProgramView epView = (EndowmentProgramView)sessionMap.remove(WebConstants.SELECTED_ROW);
			paymentView.setEndowmentProgramView(epView);
			populateProgramPaymentTypes(epView.getEndowmentProgramId());
			updateValuesToMap();
			
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromEndowmentProgramSearch--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void populateProgramPaymentTypes(Long endowmentProgramId)throws Exception
	{
		programPaymentTypes= new ArrayList<SelectItem>();
		 List<EndProgPaymentTypes> list = EndowmentProgramService.getEndProgPaymentTypesByProgramId(endowmentProgramId,null);
		 for (EndProgPaymentTypes obj : list) {
			SelectItem item = new SelectItem(
												obj.getPaymentType().getPaymentTypeId().toString(),
												isEnglishLocale()?obj.getPaymentType().getDescriptionEn():obj.getPaymentType().getDescriptionAr()
					
											);
			programPaymentTypes.add(item);
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if(requestView == null || requestView.getRequestId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,requestView.getRequestId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		if( requestView.getDonationCollectedBy() ==null || requestView.getDonationCollectedBy().equals( "-1" )  ) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("donationRequest.msg.collectedByRequired"));
			tabPanel.setSelectedTab(TAB_PAYMENT_DETAILS);
			return true;
		}
		
		if( paymentList==null || paymentList.size() <= 0) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("thirdPartRevenue.msg.revenueRequired"));
			tabPanel.setSelectedTab(TAB_PAYMENT_DETAILS);
			return true;
		}
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}

		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			
			saveDonationRequestInTransaction();
			
			getDonationRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
			successMessages.add( ResourceUtil.getInstance().getProperty( "thirdPartRevenue.msg.saved" ) );
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void saveDonationRequestInTransaction() throws Exception 
	{
		try
		{
			
			Long status = 9001l;
            if(requestView.getStatusId() != null)
            {
            	status = null;
            }
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistDonationRequest( status );
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistDonationRequest(Long status) throws Exception 
	{
	
		if(status!=null) requestView.setStatusId(status);
		requestView.setPaymentScheduleView(paymentList);
		requestView = requestService.persistDonationRequest(requestView);
		
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void onSubmit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COLLECTION_REQ);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		String event  =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		try	
		{	
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			if( hasSaveErrors() ){ return; }
			
			 updateValuesFromMap();
			 
			 SystemParameters parameters = SystemParameters.getInstance();
			 String endPoint= parameters.getParameter( "MEMSDonationRequestBPEL" );
			 MEMSDonationRequestBPELPortClient port=new MEMSDonationRequestBPELPortClient();
			 port.setEndpoint(endPoint);
			 
			 persistDonationRequest( status );
			 getDonationRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 port.initiate(
					 		CommonUtil.getLoggedInUser(),
					 		Integer.parseInt( requestView.getRequestId().toString()),
					 		requestView.getRequestNumber(), 
					 		null, 
					 		null
					 	   );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSubmit --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onResubmitted()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COLLECTION_REQ);
		String msg    =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() ){ return; }
		 
		 updateValuesFromMap();	
		 
		 persistDonationRequest( status );
		 getDonationRequestDetails( requestView.getRequestId() );
		 
		 setTaskOutCome(TaskOutcome.OK);

		 updateValuesToMap();
		 
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onResubmitted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	private boolean hasReasonProvided() throws Exception
	{
		if(txtRemarks ==null || txtRemarks.trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.reasonRequired")  );
			return false;
		}
		return true;
	}
	@SuppressWarnings( "unchecked" )
	public void onRejected()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistDonationRequest( status );
			 setTaskOutCome(TaskOutcome.REJECT);
			 getDonationRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	private boolean hasCollectionErrors() throws Exception
	{
		boolean hasCollectionErrors=false;
		errorMessages = new ArrayList<String>();

		return hasCollectionErrors;
		
	}
	

	@SuppressWarnings( "unchecked" )
	public void onPrintReceipt()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			CommonUtil.printMiscellaneousPaymentReceipt(requestView.getRequestId().toString(), null, FacesContext.getCurrentInstance());
		}
		catch (Exception exception) 
		{
			logger.LogException( "onPrintReceipt--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onCollect()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasCollectionErrors() ){ return; }
			
			sessionMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION, paymentList);
	        PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
	        sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
	        executeJavaScript("javascript:openPopupReceivePayment();");
	        btnCollectPayment.setRendered(false);
	        btnSendBack.setRendered(false);
//			saveDonationRequestInTransaction();
//			getDonationRequestDetails( requestView.getRequestId() );
//			setTaskOutCome(TaskOutcome.OK);
			updateValuesToMap();
			//successMessages.add( ResourceUtil.getInstance().getProperty("endowmentProgram.msg.savedSuccessfully"));
		}
		catch (Exception exception) 
		{
			logger.LogException( "onCollect--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromReceivePayments()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			PaymentReceiptView prv=(PaymentReceiptView)sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
			sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			if( hasCollectionErrors() ){ return; }

			Long status = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COMPLETE);
			requestView.setStatusId(status);
			prv.setPaidById(requestView.getApplicantView().getPersonId());
			prv = requestService.collectRequestsPayments(requestView, prv);
//			Map<String,String> personIdMap = new HashMap<String, String>();
//			for (PaymentScheduleView payView : paymentList) 
//			{
//				
//				
//				if( 
//					!personIdMap.containsKey(  
//												requestView.getApplicantView().getPersonId().toString()+"_"+
//												payView.getEndowmentProgramView().getEndowment().getCostCenter()
//											)
//				  )
//				{
//					new GRPCustomersService().sendPersonLocationToGRP(
//																	   requestView.getRequestId(), 
//																	   requestView.getApplicantView().getPersonId(), 
//																	   "AMAF",
//																	   payView.getEndowmentProgramView().getEndowment().getCostCenter()
//								  									  );
//				
//					personIdMap.put( 
//							 		requestView.getApplicantView().getPersonId().toString()+"_"+payView.getEndowmentProgramView().getEndowment().getCostCenter(),
//							 		requestView.getApplicantView().getPersonId().toString()+"_"+payView.getEndowmentProgramView().getEndowment().getCostCenter()
//						   		   );
//				}
//	
//			}
			
			setTaskOutCome(TaskOutcome.OK);
			CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
			getDonationRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_COLLECT);
			btnCollectPayment.setRendered(false);
			viewMap.put("collectionCompleted", true);
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty("thirdPartRevenue.msg.collection"));
			this.setPageMode( PAGE_MODE_VIEW );
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromReceivePayments--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}


	private boolean hasAddErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
	
		if( paymentView.getEndowmentProgramView()==null || paymentView.getEndowmentProgramView().getEndowmentProgramId()==null )
		{
			hasSaveErrors=true;
			errorMessages.add(ResourceUtil.getInstance().getProperty("donationRequest.msg.endowmentProgramRequired"));
		}
		if( paymentView.getPaymentDueOn() == null )
		{
				hasSaveErrors=true;
				errorMessages.add(ResourceUtil.getInstance().getProperty("replaceCheque.errMsg.dueDateRequired"));
		}
		if( paymentView.getTypeIdStr() == null || paymentView.getTypeIdStr().equals("-1"))
		{
				hasSaveErrors=true;
				errorMessages.add(ResourceUtil.getInstance().getProperty("donationRequest.msg.programPaymentTypeNotPresent"));
		}
		if( paymentView.getDonationSourceIdString() ==null || paymentView.getDonationSourceIdString().equals("-1"))
		{
			hasSaveErrors=true;
			errorMessages.add(ResourceUtil.getInstance().getProperty("donationRequest.msg.donationSrcRequired"));
		}
		if( paymentView.getAmount()==null || paymentView.getAmount().compareTo(0d)<=0)
		{
			hasSaveErrors=true;
			errorMessages.add(ResourceUtil.getInstance().getProperty("mems.payment.err.msgs.amntReq"));
		}
		if( paymentView.getDescription() != null && paymentView.getDescription().trim().length() <= 0 )
		{
			hasSaveErrors=true;
			errorMessages.add(ResourceUtil.getInstance().getProperty("socialProgram.messages.descReq"));
		}
		if( hasSaveErrors )return true;
		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		
		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onAdd()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			if( hasAddErrors() ){ return; }
			PaymentType pt  = EntityManager.getBroker().findById(PaymentType.class, paymentView.getTypeId());
			paymentView.setTypeId(pt.getPaymentTypeId());
			paymentView.setTypeEn( pt.getDescriptionEn());
			paymentView.setTypeAr(pt.getDescriptionAr());
			PaymentScheduleView clonedView = (PaymentScheduleView)BeanUtils.cloneBean(paymentView);
			
			paymentList.add( 0, clonedView);
			saveDonationRequestInTransaction();
			getDonationRequestDetails( requestView.getRequestId() );
			paymentView = new PaymentScheduleView();
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty("donationRequest.msg.added"));
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	private boolean hasDeleteErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
        if( requestView.getRequestId() != null && paymentList.size()==1 )
        {
        	errorMessages.add(ResourceUtil.getInstance().getProperty("donationRequest.msg.deleteError"));
        }
		return hasSaveErrors;
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onEdit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
	        paymentView = ( PaymentScheduleView )dataTable.getRowData();
			
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onEdit--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onDelete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			PaymentScheduleView row = ( PaymentScheduleView )dataTable.getRowData();
			updateValuesFromMap();
			
			if( hasDeleteErrors() ){ return; }
	        
	        if( row.getPaymentScheduleId() != null)
	        {
	        	row.setUpdatedBy( getLoggedInUserId() );
	        	requestService.deleteEndowmentRelatedPayment(row);
	        }
	        paymentList.remove( row );	
			
			successMessages.add( ResourceUtil.getInstance().getProperty("donationRequest.msg.deletedSuccessfully"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onDelete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = getUserTask();
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    }



	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask )
	{
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}


	public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("donationRequest.title.heading"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_NEW)  || getPageMode().equals( PAGE_MODE_RESUBMITTED ) )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  getPageMode().equals( PAGE_MODE_NEW ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowResubmitButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_RESUBMITTED )  && isTaskAvailable()
		)
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowCollectButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && 
			 getPageMode().equals( PAGE_MODE_COLLECTION_REQUIRED ) &&
			 isTaskAvailable()
		 )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowPrintButton()
	{
		if(  getStatusCompleted() )
		{
			return true;
		}
		return false;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<PaymentScheduleView> getPaymentList() {
		return paymentList;
	}

	public void setPaymentList(List<PaymentScheduleView> paymentList) {
		this.paymentList = paymentList;
	}

	public PaymentScheduleView getPaymentView() {
		return paymentView;
	}

	public void setPaymentView(PaymentScheduleView paymentView) {
		this.paymentView = paymentView;
	}

	public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public String getSelectOneCollectedByUser() {
		return selectOneCollectedByUser;
	}

	public void setSelectOneCollectedByUser(String selectOneCollectedByUser) {
		this.selectOneCollectedByUser = selectOneCollectedByUser;
	}

	public List<SelectItem> getProgramPaymentTypes() {
		return programPaymentTypes;
	}

	public void setProgramPaymentTypes(List<SelectItem> programPaymentTypes) {
		this.programPaymentTypes = programPaymentTypes;
	}

	public HtmlCommandButton getBtnSendBack() {
		return btnSendBack;
	}

	public void setBtnSendBack(HtmlCommandButton btnSendBack) {
		this.btnSendBack = btnSendBack;
	}
	
	

}