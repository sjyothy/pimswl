package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.bpel.proxy.MEMSCollectThirdPartyEndowmentRevenuePortClient;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.ThirdPartyPaymentForTypes;
import com.avanza.pims.entity.ThirdPartyPropUnits;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationDetails;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.EndowmentTransformUtil;
import com.avanza.pims.ws.mems.ThirdPartyPropUnitsService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.EndowmentProgramView;
import com.avanza.pims.ws.vo.mems.EndowmentView;
import com.avanza.ui.util.ResourceUtil;


public class ThirdPartyRevenueRequestBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(ThirdPartyRevenueRequestBean.class);
	
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_RESUBMITTED = "PAGE_MODE_RESUBMITTED";
	private static final String PAGE_MODE_COMPLETE = "PAGE_MODE_COMPLETE";
	private static final String PAGE_MODE_COLLECTION_REQUIRED = "COLLECTION_REQUIRED";
	
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_PAYMENT_DETAILS   = "paymentsTab";
	private static final String TAB_APPLICANTS   = "applicationTab";
    private String pageTitle;
    private String pageMode;
    private String hdnPersonId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;
    
    private String txtRemarks;
    private RequestService requestService = new RequestService();
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	protected javax.faces.component.html.HtmlCommandButton btnCollect = new HtmlCommandButton();
	protected javax.faces.component.html.HtmlCommandButton btnCollectionreject = new HtmlCommandButton();
	List<SelectItem> selectThirdPartyPaymentForTypes = new ArrayList<SelectItem>();
	private RequestView requestView ;
    private List<PaymentScheduleView> paymentList = new ArrayList<PaymentScheduleView>();	
    private PaymentScheduleView paymentView = new PaymentScheduleView();
    private String forRebuild;
    private HtmlDataTable dataTable;
    private List<SelectItem> thirdPartyUnits = new ArrayList<SelectItem>();
    
	public ThirdPartyRevenueRequestBean(){}
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			sessionMap.put(WebConstants.IS_EMAIL_MANDATORY,"1");
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
				if (sessionMap.get(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY) != null )
				{
					boolean isPaymentCollected = (Boolean) (sessionMap.remove(WebConstants.PAYMENT_COLLECTED_SUCESSFULLY));
					if(isPaymentCollected)
				    {
					  onMessageFromReceivePayments();
				    }
				}
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.ThirdPartyRevenue.PROCEDURE_THIRD_PARTY_REVENUE );
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.ThirdPartyRevenue.PROCEDURE_THIRD_PARTY_REVENUE);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		viewMap.put("noteowner", WebConstants.REQUEST);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		loadAttachmentsAndComments( null );
		getThirdPartyPaymentForTypes();
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
		  getDataFromTaskList();
		}
		else if ( getRequestMap().get( WebConstants.REQUEST_VIEW) != null )
		{
		  getDataFromSearch();
		  
		}
		else
		{
			setDataForFirstTime();
		}
		
		updateValuesFromMap();
		getPageModeFromRequestStatus();
	}
	
	public void prerender()
	{
		if ( getPageMode().equals(PAGE_MODE_COLLECTION_REQUIRED) && 
			 viewMap.get("collectionCompleted" ) != null 
		   )
		{
			setPageMode(PAGE_MODE_VIEW);
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		requestView = new RequestView();
		requestView.setCreatedOn(new Date());
		requestView.setCreatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedOn(new Date());
		requestView.setStatusId( 9001l );
		requestView.setStatusDataValue( WebConstants.REQUEST_STATUS_NEW  );
		requestView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		requestView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
		requestView.setRequestDate( new Date() );
		requestView.setRequestTypeId( WebConstants.MemsRequestType.THIRD_PARTY_REVENUE_REQUEST );
		
		paymentView.setThirdPartyPropUnitId("-1");
		paymentView.setPaymentDueOn( new Date() );
	}
	
	@SuppressWarnings( "unchecked" )
	public List<SelectItem> getThirdPartyPaymentForTypes() throws Exception
    {

      List<ThirdPartyPaymentForTypes> objList = UtilityService.getThirdPartyPaymentForTypes();
      
      for(ThirdPartyPaymentForTypes obj : objList)
      {
    	  selectThirdPartyPaymentForTypes.add(
					  		  					new SelectItem(
					  		  							obj.getTypeId().toString(),isEnglishLocale()? obj.getTypeEn():obj.getTypeAr()
					  		  								)
						);
      }
	  Collections.sort(selectThirdPartyPaymentForTypes, ListComparator.LIST_COMPARE);
	  return selectThirdPartyPaymentForTypes;
	}
	
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromRequestStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		tabPanel.setSelectedTab(TAB_APPLICANTS);
		if( this.requestView == null  || 
			this.requestView.getRequestId() == null ||
			this.requestView.getStatusId() == null 
		  ) 
		{ return; }
		if( getStatusCollectionRequired() )
		{
			setPageMode( PAGE_MODE_COLLECTION_REQUIRED);
			tabPanel.setSelectedTab(TAB_PAYMENT_DETAILS );
		}
		else if(  getStatusRejectedResubmitted() )
		{
			setPageMode(PAGE_MODE_RESUBMITTED);
			tabPanel.setSelectedTab("commentsTab" );
			
		}
		else if(  getStatusDistributionRequired() )
		{
			setPageMode( PAGE_MODE_COMPLETE );
			tabPanel.setSelectedTab( TAB_PAYMENT_DETAILS );
			
		}
		else if (getStatusCompleted())
		{
			setPageMode(PAGE_MODE_VIEW);
			tabPanel.setSelectedTab(TAB_PAYMENT_DETAILS );
		}	
			
	}
	public boolean getStatusNew() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_NEW ) == 0;
	}
	public boolean getStatusCollectionRequired() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_COLLECTION_REQ ) == 0;
	}
	public boolean getStatusDistributionRequired() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_DISTRIBUTION_REQ) == 0;
	}
	public boolean getStatusRejectedResubmitted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT) == 0;
	}
	public boolean getStatusCompleted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_COMPLETE ) == 0;
	}

	
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = ( RequestView )viewMap.get( WebConstants.REQUEST_VIEW ) ;
			requestView.setUpdatedBy(getLoggedInUserId());
		}
		if( viewMap.get(  WebConstants.ThirdPartyRevenue.PaymentList) != null)
		{
			paymentList = (	ArrayList<PaymentScheduleView> )viewMap.get(  WebConstants.ThirdPartyRevenue.PaymentList ); 
		}
		if( viewMap.get(  WebConstants.ThirdPartyRevenue.PaymentView) != null)
		{
			paymentView = (	PaymentScheduleView )viewMap.get(  WebConstants.ThirdPartyRevenue.PaymentView ); 
		}
		if( viewMap.get(WebConstants.ThirdPartyRevenue.UnitList) != null)
		{
			thirdPartyUnits = (ArrayList<SelectItem>)viewMap.get(WebConstants.ThirdPartyRevenue.UnitList) ;
		}
		if( viewMap.get(  "ThirdPartyPaymentForTypes" ) != null )
        {
      	  selectThirdPartyPaymentForTypes = (ArrayList<SelectItem>)viewMap.get(  "ThirdPartyPaymentForTypes" ); 
        }
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( requestView != null )
		{
		  viewMap.put(  WebConstants.REQUEST_VIEW , requestView);
		}
		if( paymentList != null )
		{
			viewMap.put(  WebConstants.ThirdPartyRevenue.PaymentList, paymentList);
		}
		
		if( paymentView != null )
		{
			viewMap.put(  WebConstants.ThirdPartyRevenue.PaymentView,paymentView );
		}
		else
		{
			viewMap.put(  WebConstants.ThirdPartyRevenue.PaymentView, new PaymentScheduleView() );
		}
		if( thirdPartyUnits != null)
		{
			viewMap.put(  WebConstants.ThirdPartyRevenue.UnitList, thirdPartyUnits );
		}
		if( selectThirdPartyPaymentForTypes != null && selectThirdPartyPaymentForTypes.size() > 0)
		{
			viewMap.put(  "ThirdPartyPaymentForTypes", selectThirdPartyPaymentForTypes);
		}

		
	}
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		requestView = ( RequestView)getRequestMap().get( WebConstants.REQUEST_VIEW) ;
		if( this.requestView !=null )
		{
		  getRequestDetails( requestView.getRequestId() );
		}
	}
	
	@SuppressWarnings(  "unchecked"  )
	private void populateApplicationDetailsTab()throws Exception
	{
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		String status = "";
		boolean isRequestNull =  null == requestView || null == requestView.getApplicantView() ||null == requestView.getRequestId();
		boolean isApplicantDiffFromPerson = isRequestNull || 
										    !requestView.getApplicantView().getPersonId().toString().equals(hdnPersonId);
		if ( isApplicantDiffFromPerson && hdnPersonName!= null && hdnPersonName.trim().length() >0 ) 
		{
			bean.populateApplicationDetails(
						                    "", 
						                    "", 
						                    new Date(), 
						                    "", 
						                    hdnPersonName, 
						                    "", 
						                    hdnCellNo, 
						                    ""
						                  );
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, hdnPersonId);
			PersonView person = new PersonView();
			person.setPersonId( new Long( hdnPersonId ) );
			requestView.setApplicantView( person);
			
            return;
		}
		
		status = CommonUtil.getIsEnglishLocale()?requestView.getStatusEn():requestView.getStatusAr();
		bean.populateApplicationDetails(
				                         requestView.getRequestNumber(), 
					                     status, 
					                     requestView.getRequestDate(), 
					                     requestView.getDescription(), 
					                     requestView.getApplicantView().getPersonFullName(), 
					                     "", 
					                     requestView.getApplicantView().getCellNumber(), 
					                     requestView.getApplicantView().getEmail()
					                   );
		if( requestView.getApplicantView() != null && requestView.getApplicantView().getPersonId() != null  )
		{
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,requestView.getApplicantView().getPersonId() );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, requestView.getApplicantView().getPersonId() );
		}
		if ( requestView!= null && requestView.getStatus() != null && 
			!requestView.getStatusDataValue().equals( WebConstants.REQUEST_STATUS_NEW ) 
		    )
		{
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}
	}
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  UserTask userTask = ( UserTask )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  setUserTask(userTask);
	  if( userTask.getTaskAttributes().get(  WebConstants.REQUEST_ID ) == null ){ return; }
	  long  id = new Long  ( userTask.getTaskAttributes().get( WebConstants.REQUEST_ID ) );
	  getRequestDetails( id );
	  
	  
    }
	
	@SuppressWarnings( "unchecked" )
	private void getRequestDetails( long id ) throws Exception 
	{
		requestView = requestService.getEndowmentRelatedPaymentRequestById(id );
		if(requestView.getPaymentScheduleView() != null )
		{
			paymentList = requestView.getPaymentScheduleView();
		}
		populateApplicationDetailsTab();
		updateValuesToMap();
		loadAttachmentsAndComments( id );
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(requestView.getRequestId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.REQUEST;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, requestView.getRequestId());
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.REQUEST;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(requestView.getRequestId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, requestView.getRequestId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }

	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( requestView != null && requestView.getRequestId()!= null )
		 {
			 loadAttachmentsAndComments( requestView.getRequestId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMap();
			if(hdnPersonType.equals( "APPLICANT" ) )
			{
				populateApplicationDetailsTab();
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	@SuppressWarnings( "unchecked" )
	public void onPaymentMethodChanged()
	{
		try	
		{
			updateValuesFromMap();
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onPaymentMethodChanged|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchEndowments()
	{
		if( sessionMap.get(WebConstants.SELECTED_ROW)== null )
		{return;}
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	

			updateValuesFromMap();
			Endowment endowment = (Endowment)sessionMap.remove(WebConstants.SELECTED_ROW);
			EndowmentView endowmentView = new EndowmentView();
			EndowmentTransformUtil.transformToEndowmentView(endowmentView, endowment);
			paymentView.setEndowmentView(endowmentView);
			if( isThirdPartyEndowmentProperty() )
			{
				loadThirdPartyUnitById( endowmentView.getEndowmentId()  );
			}
			
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromSearchEndowments --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	private void loadThirdPartyUnitById( Long endowmentId ) throws Exception
	{
		thirdPartyUnits = new ArrayList<SelectItem>();
		List<ThirdPartyPropUnits> list = ThirdPartyPropUnitsService.getAllThirdPartyUnits(endowmentId, null);
		for (ThirdPartyPropUnits unit : list) 
		{
		  SelectItem item = new SelectItem(
				  							unit.getThirdPartyPropUnitId().toString(),
				  							unit.getFloorNumber()+"-"+unit.getUnitNumber()
				           
		                                   );
		  thirdPartyUnits.add(item);
		}
		Collections.sort(thirdPartyUnits, ListComparator.LIST_COMPARE);
	}
	
	public boolean isThirdPartyEndowmentProperty()throws Exception
	{
		if( paymentView.getEndowmentView().getAssetTypeId().compareTo(WebConstants.AssetType.LAND_PROPERTIES)==0
		  )
		{
			return true;
		}
		
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromEndowmentProgramSearch()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			EndowmentProgramView epView = (EndowmentProgramView)sessionMap.remove(WebConstants.SELECTED_ROW);
			paymentView.setEndowmentProgramView(epView);
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromEndowmentProgramSearch--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if(requestView == null || requestView.getRequestId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,requestView.getRequestId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		
		if( paymentList==null || paymentList.size() <= 0) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("thirdPartRevenue.msg.revenueDetailRequired "));
			tabPanel.setSelectedTab(TAB_PAYMENT_DETAILS);
			return true;
		}
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}

		return hasSaveErrors;
		
	}


	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			saveRequestInTransaction();
			getRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
			successMessages.add( ResourceUtil.getInstance().getProperty( "thirdPartRevenue.msg.saved" ) );
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private boolean hasCompletionErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		for(PaymentScheduleView  ps : paymentList)
		{
		   if( 
				   ps.getForRebuild() != null && ps.getForRebuild().equals("0") &&
					( 
							ps.getPaymentModeId().compareTo( WebConstants.PAYMENT_SCHEDULE_MODE_CASH_ID  ) == 0 ||
							ps.getPaymentModeId().compareTo( WebConstants.PAYMENT_SCHEDULE_MODE_BANK_TRANSFER_ID ) == 0
					) &&
				   ( ps.getIsDistributed()== null || ps.getIsDistributed().equals("0") ) 
		   	 )
		   {
			errorMessages.add(CommonUtil.getBundleMessage("thirdPartRevenue.msg.allPaymentsMustBeDistributed"));
			tabPanel.setSelectedTab(TAB_PAYMENT_DETAILS);
			return true;
		   }
		}
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}

		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onComplete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			Long status = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COMPLETE );
			updateValuesFromMap();
			if( hasCompletionErrors() ){ return; }
			completeRequestInTransaction(status);
			getRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_COMPLETED );
			this.setPageMode( PAGE_MODE_VIEW );
			successMessages.add( ResourceUtil.getInstance().getProperty("request.event.RequestCompleted"));
			
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onComplete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDistribute()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			PaymentScheduleView ps = (PaymentScheduleView) dataTable.getRowData();
			sessionMap.put(WebConstants.DistributeEndowmentRevenue.PaymentView,ps );
			executeJavaScript("javaScript:openDistributeEndowmentRevenuePopup();");
		}
		catch (Exception exception) 
		{
			logger.LogException( "onDistribute--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	/**
	 * @param status
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void completeRequestInTransaction(Long status) throws Exception 
	{
		try
		{
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistRequest(status);
			setTaskOutCome(TaskOutcome.APPROVE);
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	
	private void saveRequestInTransaction() throws Exception 
	{
		try
		{
			Long status = 9001l;
            if(	requestView.getStatusId() != null	){ status = null; }
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistRequest( status );
			ApplicationContext.getContext().getTxnContext().commit();
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistRequest(Long status) throws Exception 
	{
		if(status!=null) requestView.setStatusId(status);
		requestView.setPaymentScheduleView(paymentList);
		requestView = requestService.persistThirdPartyRevenueRequest( requestView );
	}
	
	@SuppressWarnings( "unchecked" )
	public void onSubmit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COLLECTION_REQ);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		String event  =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 if( hasSaveErrors() ){ return; }
			
			 updateValuesFromMap();
			 
			 SystemParameters parameters = SystemParameters.getInstance();
			 String endPoint= parameters.getParameter( "MEMSCollectThirdPartyRevenueBPEL" );
			 MEMSCollectThirdPartyEndowmentRevenuePortClient port=new MEMSCollectThirdPartyEndowmentRevenuePortClient();
			 port.setEndpoint(endPoint);
			 
			 persistRequest( status );
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 port.initiate(
					 		CommonUtil.getLoggedInUser(),
					 		Integer.valueOf( requestView.getRequestId().toString() ),
					 		requestView.getRequestNumber(), 
					 		null, 
					 		null
					 	  );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			 ApplicationContext.getContext().getTxnContext().rollback();
			 logger.LogException("onSubmit --- CRASHED --- ", exception);
    		 errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onResubmitted()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COLLECTION_REQ);
		String msg    =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() ){ return; }
		 
		 updateValuesFromMap();	
		 
		 persistRequest( status );
		 getRequestDetails( requestView.getRequestId() );
		 
		 setTaskOutCome(TaskOutcome.APPROVE);

		 updateValuesToMap();
		 
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onResubmitted--- CRASHED --- ", exception);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	private boolean hasReasonProvided() throws Exception
	{
		if(txtRemarks ==null || txtRemarks.trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.reasonRequired")  );
			return false;
		}
		return true;
	}
	@SuppressWarnings( "unchecked" )
	public void onRejected()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( status );
			 setTaskOutCome(TaskOutcome.REJECT);
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	private boolean hasCollectionErrors() throws Exception
	{
		boolean hasCollectionErrors=false;
		errorMessages = new ArrayList<String>();
		return hasCollectionErrors;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onCollect()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			if( hasCollectionErrors() ){ return; }
			sessionMap.put( WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION, paymentList);
	        PaymentReceiptView paymentReceiptView =new PaymentReceiptView();
	        sessionMap.put(WebConstants.PAYMENT_RECEIPT_VIEW, paymentReceiptView);
	        
	        executeJavaScript("javascript:openPopupReceivePayment();");
	        hideCollectionRelatedButton();
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onCollect--- CRASHED --- ", exception);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings( "unchecked" )
	private void hideCollectionRelatedButton() {
		btnCollect.setRendered(false);
		btnCollectionreject.setRendered(false);
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromReceivePayments()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			PaymentReceiptView prv=(PaymentReceiptView)sessionMap.remove(WebConstants.PAYMENT_RECEIPT_VIEW);
			sessionMap.remove(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE);
			prv.setPaidById(requestView.getApplicantView().getPersonId());
			if( hasCollectionErrors() ){ return; }

			Long status =  WebConstants.REQUEST_STATUS_COMPLETE_ID;
			
			boolean shallDistribute=false;
//			for (PaymentScheduleView payView : paymentList) 
//			{ 
//				if( 
//						payView.getForRebuild() != null && 
//						payView.getForRebuild().equals("0") &&
//						( 
//						  payView.getPaymentModeId().compareTo( WebConstants.PAYMENT_SCHEDULE_MODE_CASH_ID  ) == 0 ||
//						  payView.getPaymentModeId().compareTo( WebConstants.PAYMENT_SCHEDULE_MODE_BANK_TRANSFER_ID ) == 0
//						)
//				  )
//				{
//						status = WebConstants.REQUEST_STATUS_DISTRIBUTION_REQ_ID ;
//						shallDistribute = true;
//						break;
//				}
//			}

			requestView.setStatusId(status);
			prv = requestService.collectThirdPartyRevenue(requestView, prv);
			
//			if( shallDistribute )
//			{
//				setTaskOutCome(TaskOutcome.OK);
//			}
//			else
			{
				setTaskOutCome(TaskOutcome.CLOSE);
			}
			
			CommonUtil.updateChequeDocuments(prv.getPaymentReceiptId().toString());
			getRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_PAYMENT_COLLECTED_ONLY);
			this.setPageMode( PAGE_MODE_VIEW );
			viewMap.put("collectionCompleted", true);
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty("thirdPartRevenue.msg.collection"));
			
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromReceivePayments--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onPrintReceipt()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			CommonUtil.printMiscellaneousPaymentReceipt(requestView.getRequestId().toString(), null, FacesContext.getCurrentInstance());
		}
		catch (Exception exception) 
		{
			logger.LogException( "onPrintReceipt--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromDistributeEndowmentRevenue()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        boolean doComplete = true;
			updateValuesFromMap();
			PaymentScheduleView fromPopup=(PaymentScheduleView)sessionMap.remove(WebConstants.DistributeEndowmentRevenue.PaymentView);
			PaymentScheduleView toRemove = null;
			for ( PaymentScheduleView view : paymentList) 
			{
				 if (view.getPaymentScheduleId().compareTo( fromPopup.getPaymentScheduleId())==0)
				 {
					toRemove = view;
				 }
				 else if( view.getIsDistributed() == null || view.getIsDistributed().equals("0") )
				 {
					 doComplete =false;
				 }
			}
			paymentList.remove(toRemove);
			paymentList.add( fromPopup );
			updateValuesToMap();
			
			if( !doComplete )
			{
				String msg = java.text.MessageFormat.format( 
															 ResourceUtil.getInstance().getProperty("thirdPartRevenue.msg.paymentDistributed"),
															 fromPopup.getPaymentNumber()
														   );
				successMessages.add( msg );
			}
			else
			{
				onComplete();
			}
			
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromDistributeEndowmentRevenue--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings( "unchecked" )
	private boolean hasAddErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
	
		if( paymentView.getEndowmentView()==null || paymentView.getEndowmentView().getEndowmentId()==null )
		{
			hasSaveErrors=true;
			errorMessages.add(ResourceUtil.getInstance().getProperty("thirdPartRevenue.msg.endowmentRequired"));
		}
//		else if( isThirdPartyEndowmentProperty() && 
//				 ( paymentView.getThirdPartyPropUnitId() ==null || paymentView.getThirdPartyPropUnitId().equals("-1") )  
//			   )
//		{
//			hasSaveErrors=true;
//			errorMessages.add(ResourceUtil.getInstance().getProperty("thirdPartRevenue.msg.thirdPartyUnitRequired"));
//		}
		
		if( paymentView.getPaymentDueOnString() != null )
		{
			hasSaveErrors=true;
			errorMessages.add(ResourceUtil.getInstance().getProperty("replaceCheque.errMsg.dueDateRequired"));
		}	
		if( paymentView.getAmount()==null || paymentView.getAmount().compareTo(0d)<=0)
		{
			hasSaveErrors=true;
			errorMessages.add(ResourceUtil.getInstance().getProperty("mems.payment.err.msgs.amntReq"));
		}
		if( hasSaveErrors )return true;
		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		
		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onAdd()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			if( hasAddErrors() ){ return; }
			paymentView.setPaymentDueOn (paymentView.getPaymentDueOn() );
			Double amount= Double.valueOf(  paymentView.getAmountStr() );
			PaymentScheduleView clonedView = (PaymentScheduleView)BeanUtils.cloneBean(paymentView);
			clonedView.setIsDistributed("0");
			if(
					paymentView.getForRebuild()!= null && 
					paymentView.getForRebuild().equals(Constant.ThirdPartyPaymentFor.REBUILD_AND_REVENUE_Id.toString())
			  )
			{
				
				PaymentScheduleView anotherClonedView = (PaymentScheduleView)BeanUtils.cloneBean(paymentView);
				anotherClonedView.setForRebuild(Constant.ThirdPartyPaymentFor.REVENUE_ONLY_Id.toString() );
				anotherClonedView.setAmount(amount*.60);
				
				
				clonedView.setForRebuild(Constant.ThirdPartyPaymentFor.REBUILD_ONLY_Id.toString() );
				clonedView.setAmount(amount*.40);
				
				Double diffInAmount = amount - ( anotherClonedView.getAmount()+clonedView.getAmount());
				if( diffInAmount > 0 )
				{
					anotherClonedView.setAmount(anotherClonedView.getAmount()+diffInAmount);
				}
				if( diffInAmount < 0 )
				{
					anotherClonedView.setAmount(anotherClonedView.getAmount()-diffInAmount);
				}
				paymentList.add( 0, anotherClonedView);
			}
			paymentList.add( 0, clonedView);
			saveRequestInTransaction();
			getRequestDetails( requestView.getRequestId() );
			paymentView = new PaymentScheduleView();
			updateValuesToMap();
			successMessages.add( ResourceUtil.getInstance().getProperty("thirdPartRevenue.msg.added"));
		}
		catch (Exception exception) 
		{
			logger.LogException( "onAdd --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private boolean hasDeleteErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
        if( requestView.getRequestId() != null && paymentList.size()==1 )
        {
        	errorMessages.add(ResourceUtil.getInstance().getProperty("thirdPartRevenue.msg.deleteError"));
        	hasSaveErrors=true;
        }
		return hasSaveErrors;
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onEdit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
	        paymentView = ( PaymentScheduleView )dataTable.getRowData();
	        if( paymentView.getForRebuild() != null  )
	        {
	        	forRebuild = paymentView.getForRebuild().toString();
	        }

			if( isThirdPartyEndowmentProperty() )
			{
				loadThirdPartyUnitById(paymentView.getEndowmentView().getEndowmentId());
			}
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onEdit--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onDelete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			PaymentScheduleView row = ( PaymentScheduleView )dataTable.getRowData();
			updateValuesFromMap();
			
			if( hasDeleteErrors() ){ return; }
	        
	        if( row.getPaymentScheduleId() != null)
	        {
	        	row.setUpdatedBy( getLoggedInUserId() );
	        	requestService.deleteEndowmentRelatedPayment(row);
	        }
	        paymentList.remove( row );	
			
			successMessages.add( ResourceUtil.getInstance().getProperty("thirdPartRevenue.msg.deletedSuccessfully"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onDelete--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = getUserTask();
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    }



	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask )
	{
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}


	public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("thirdPartRevenue.title.heading"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_NEW)  || getPageMode().equals( PAGE_MODE_RESUBMITTED ) )  
				
	      ) 
		{
			return true;
		}
		return false;
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowPrintReceiptButton()
	{
		if( ! (getStatusNew() || getStatusCollectionRequired() ) )  
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  getPageMode().equals( PAGE_MODE_NEW ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowResubmitButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_RESUBMITTED ) )
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowCollectButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_COLLECTION_REQUIRED ) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowComplete()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_COMPLETE ) )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<PaymentScheduleView> getPaymentList() {
		return paymentList;
	}

	public void setPaymentList(List<PaymentScheduleView> paymentList) {
		this.paymentList = paymentList;
	}

	public PaymentScheduleView getPaymentView() {
		return paymentView;
	}

	public void setPaymentView(PaymentScheduleView paymentView) {
		this.paymentView = paymentView;
	}

	public javax.faces.component.html.HtmlCommandButton getBtnCollectionreject() {
		return btnCollectionreject;
	}

	public void setBtnCollectionreject(
			javax.faces.component.html.HtmlCommandButton btnCollectionreject) {
		this.btnCollectionreject = btnCollectionreject;
	}

	public javax.faces.component.html.HtmlCommandButton getBtnCollect() {
		return btnCollect;
	}

	public void setBtnCollect(
			javax.faces.component.html.HtmlCommandButton btnCollect) {
		this.btnCollect = btnCollect;
	}

	public List<SelectItem> getThirdPartyUnits() {
		return thirdPartyUnits;
	}

	public void setThirdPartyUnits(List<SelectItem> thirdPartyUnits) {
		this.thirdPartyUnits = thirdPartyUnits;
	}

	@SuppressWarnings( "unchecked" )
	public String getForRebuild() {
		if ( viewMap.get(WebConstants.ThirdPartyRevenue.ForRebuild) != null )
		{
			forRebuild = viewMap.get(WebConstants.ThirdPartyRevenue.ForRebuild).toString() ;
		}
		return forRebuild;
	}

	@SuppressWarnings( "unchecked" ) 
	public void setForRebuild(String forRebuild) {
		this.forRebuild = forRebuild;
		if( this.forRebuild != null )
		{
			viewMap.put(  WebConstants.ThirdPartyRevenue.ForRebuild, this.forRebuild);
		}
	}

	public List<SelectItem> getSelectThirdPartyPaymentForTypes() {
		return selectThirdPartyPaymentForTypes;
	}

	public void setSelectThirdPartyPaymentForTypes(
			List<SelectItem> selectThirdPartyPaymentForTypes) {
		this.selectThirdPartyPaymentForTypes = selectThirdPartyPaymentForTypes;
	}
	
	

}