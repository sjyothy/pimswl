package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.FamilyVillageEducatonAspectService;
import com.avanza.pims.ws.mems.FamilyVillageFamilyAspectService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.EducationAspectsView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageEducationAspectTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	 
	private List<EducationAspectsView> dataListEducationAspects;
	private HtmlDataTable dataTableEducationAspects;
	FamilyVillageFamilyAspectService service;
	
	private String tabMode;
	private Long personId;
	private Integer recordSizeEducationAspects;
	public FamilyVillageEducationAspectTabBacking() 
	{
		service = new FamilyVillageFamilyAspectService();
		dataTableEducationAspects             =  new HtmlDataTable();
		dataListEducationAspects		        =  new ArrayList<EducationAspectsView>();
		recordSizeEducationAspects 		=  0;
		tabMode          = Constant.EndowmentFile.MODE_READ_ONLY;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			tabMode = (String) viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		
		if( viewMap.get( "dataListEducationAspects" ) != null )
		{
			dataListEducationAspects = (ArrayList<EducationAspectsView>)viewMap.get( "dataListEducationAspects" );
		}
		if( viewMap.get( "recordSizeEducationAspects" ) != null )
		{
			recordSizeEducationAspects = Integer.parseInt( viewMap.get( "recordSizeEducationAspects" ).toString() );
		}
		
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, tabMode );
		}
		if(recordSizeEducationAspects !=null)
		{
			viewMap.put("recordSizeEducationAspects",recordSizeEducationAspects);
		}
		
		if( dataListEducationAspects != null && dataListEducationAspects.size()  > 0  )
		{
			viewMap.put( "dataListEducationAspects", dataListEducationAspects );
		}
		
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
	}	
	
	

	public void populateTab( PersonView personView ) throws Exception
	{
		updateValuesFromMaps();
		personId = personView.getPersonId();
		setPageDataDefaults();
		loadDataListEducationAspects(personId);
		
		updateValuesToMaps();
	}
	
	private void setPageDataDefaults()throws Exception
	{
		PersonView person  = new PersonView();
		person.setPersonId(personId);
	}

	private void loadDataListEducationAspects(Long personId) throws Exception {
		
		dataListEducationAspects= FamilyVillageEducatonAspectService.getEducationAspects( personId);
		recordSizeEducationAspects = dataListEducationAspects.size();
	}

	@SuppressWarnings( "unchecked" )
	public void onDeleteHealthAspectChronic()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteHealthAspectChronic--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
	}
  
	@SuppressWarnings( "unchecked" )
	public void onDeleteHealthAspectAcute()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteHealthAspectAcute--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDeleteMedicalHistory()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteMedicalHistory--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.FAMILY_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
	}
	
	
	
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}


	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public List<EducationAspectsView> getDataListEducationAspects() {
		return dataListEducationAspects;
	}

	public void setDataListEducationAspects(
			List<EducationAspectsView> dataListEducationAspects) {
		this.dataListEducationAspects = dataListEducationAspects;
	}

	public HtmlDataTable getDataTableEducationAspects() {
		return dataTableEducationAspects;
	}

	public void setDataTableEducationAspects(HtmlDataTable dataTableEducationAspects) {
		this.dataTableEducationAspects = dataTableEducationAspects;
	}

	public Integer getRecordSizeEducationAspects() {
		return recordSizeEducationAspects;
	}

	public void setRecordSizeEducationAspects(Integer recordSizeEducationAspects) {
		this.recordSizeEducationAspects = recordSizeEducationAspects;
	}



}
