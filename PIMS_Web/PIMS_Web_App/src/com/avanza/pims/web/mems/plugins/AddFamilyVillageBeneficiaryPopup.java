package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.FamilyVillageFileService;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.ui.util.ResourceUtil;

public class AddFamilyVillageBeneficiaryPopup extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	HttpServletRequest request;
	PersonView pageView;
	InheritanceFileView inheritanceFileView;
	RequestView requestView;
	private Date dateOfJoining;
	private HtmlSelectOneMenu cmbPassportType = new HtmlSelectOneMenu();
	private List<String> housingRequestReasons = new ArrayList<String>(); 
	public AddFamilyVillageBeneficiaryPopup() 
	{
		request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		pageView = new PersonView();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
	
			
			updateValuesFromMaps();
			
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if(!isPostBack())
		{
			
			pageView= new PersonView();
			pageView.setIsDeleted(0l);
			pageView.setRecordStatus(1l);
			pageView.setCreatedOn(new Date());
			pageView.setCreatedBy(getLoggedInUserId());
			pageView.setUpdatedOn(new Date());
			pageView.setUpdatedBy(getLoggedInUserId());
			pageView.setIsCompany(0l);
			
			if (
					request.getParameter("inheritanceFileId") != null &&
					!request.getParameter("inheritanceFileId").toString().equals("-1")
					
			    ) 
			{
				inheritanceFileView = new InheritanceFileView();
				inheritanceFileView.setInheritanceFileId(
														 Long.parseLong( 
																 		 request.getParameter("inheritanceFileId").toString()
																 	    )
														);
			}
			else if( sessionMap.get("InheritanceFileView")!= null)
			{
				 inheritanceFileView = (InheritanceFileView)sessionMap.remove("InheritanceFileView");
			}
			
			if (
					request.getParameter("requestId") != null &&
					!request.getParameter("requestId").toString().equals("-1")
					
			    ) 
			{
				requestView = new RequestView();
				requestView.setRequestId(
										 Long.parseLong( 
												 		 request.getParameter("requestId").toString()
												 	    )
										);
			}
			else if( sessionMap.get("RequestView")!= null)
			{
				 requestView = (RequestView)sessionMap.remove("RequestView");
			}
			
		}
		if( viewMap.get( "PersonView" ) != null )
		{
			pageView = (PersonView)viewMap.get( "PersonView" );
		}
		if( viewMap.get("InheritanceFileView")!= null)
		{
			 inheritanceFileView = (InheritanceFileView)viewMap.get("InheritanceFileView");
		}	
		if( viewMap.get("RequestView")!= null)
		{
			 requestView = (RequestView)viewMap.get("RequestView");
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( pageView != null )
		{
			viewMap.put( "PersonView", pageView );
		}
		if(inheritanceFileView != null )
		{
			viewMap.put("InheritanceFileView",inheritanceFileView);
		}
		if(requestView != null )
		{
			viewMap.put("RequestView",requestView);
		}
	}
	
	
	@SuppressWarnings("unchecked")
	private boolean hasAddBeneficiaryErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		if( 
				pageView.getFirstName()== null || 
				pageView.getFirstName().trim().length()<=0 
		  ) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("familyVillagefile.msg.beneficiaryNameRequired"));
			return true;
		}
		if(
				housingRequestReasons == null || housingRequestReasons.size() <=0  
		  )
		{
			errorMessages.add(CommonUtil.getBundleMessage("familyVillageManageBeneficiary.msg.hosingRequestReason"));
			return true;
		}	
		if( 
				pageView.getDateOfBirth() == null 
		  ) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("person.Msg.DateOfBirth"));
			return true;
		}
		if( 
				pageView.getGender()== null || 
				pageView.getGender().trim().equals("-1")
		  ) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("person.message.gender"));
			return true;
		}
		return hasSaveErrors;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onPassportTypeChanged() {
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			updateValuesToMaps();
			if (cmbPassportType.getValue() != null ){
				if(cmbPassportType.getValue().toString().equals("Passport"))
					pageView.setMasroom(false);
				else
					pageView.setMasroom(true);
				
			}
			
			
		}
		catch ( Exception e )
		{
			logger.LogException( "---EXCEPTION--- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onDateOfBirthChange()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			updateValuesToMaps();
			pageView.getDateOfBirth();
			pageView.getAge();
			
		}
		catch ( Exception e )
		{
			logger.LogException( "---EXCEPTION--- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}
	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			updateValuesFromMaps();
			if(hasAddBeneficiaryErrors())return;
			String csvHousingReasons = "";
			for(String eachReason : housingRequestReasons )
			{
				csvHousingReasons =eachReason +",";
			}
			if( csvHousingReasons.indexOf(",") > 0 )
			{
				csvHousingReasons = csvHousingReasons.substring(0,csvHousingReasons.length()-1);
			}
			pageView.setHousingReasonId(csvHousingReasons);
			
			FamilyVillageFileService.saveBeneficiaryDetails(
															pageView, 
															inheritanceFileView != null ? inheritanceFileView.getInheritanceFileId():null,
															requestView != null ? requestView.getRequestId():null
														    );
			updateValuesToMaps();
			successMessages.add( ResourceUtil.getInstance().getProperty( "commons.save" ) );
		}
		catch ( Exception e )
		{
			logger.LogException( "---EXCEPTION--- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		
	}

	public PersonView getPageView() {
		return pageView;
	}

	public void setPageView(PersonView pageView) {
		this.pageView = pageView;
	}

	public InheritanceFileView getInheritanceFileView() {
		return inheritanceFileView;
	}

	public void setInheritanceFileView(InheritanceFileView inheritanceFileView) {
		this.inheritanceFileView = inheritanceFileView;
	}

	public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public List<String> getHousingRequestReasons() {
		return housingRequestReasons;
	}

	public void setHousingRequestReasons(List<String> housingRequestReasons) {
		this.housingRequestReasons = housingRequestReasons;
	}

	public Date getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public HtmlSelectOneMenu getCmbPassportType() {
		return cmbPassportType;
	}

	public void setCmbPassportType(HtmlSelectOneMenu cmbPassportType) {
		this.cmbPassportType = cmbPassportType;
	}


}
