package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.data.ApplicationContext;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.FamilyVillageFileService;
import com.avanza.pims.ws.vo.BeneficiaryDetailsView;
import com.avanza.pims.ws.vo.InheritanceBeneficiaryView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.FamilyDetailsView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageFamilyDataTabBacking extends AbstractMemsBean {
	private static final long serialVersionUID = -4983101685930099314L;

	private List<FamilyDetailsView> dataList;

	private HtmlDataTable dataTable;
	private FamilyVillageFileService service;
	private String tabMode;
	FamilyDetailsView pageView;
	private Long personId;

	public FamilyVillageFamilyDataTabBacking() {
		service = new FamilyVillageFileService();
		dataTable = new HtmlDataTable();
		dataList = new ArrayList<FamilyDetailsView>();
		tabMode = Constant.EndowmentFile.MODE_READ_ONLY;
		pageView = new FamilyDetailsView();
		dataTable = new HtmlDataTable();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void init() {
		try {
			updateValuesFromMaps();
		} catch (Exception exception) {
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		} finally {
			if (errorMessages != null && errorMessages.size() > 0) {
				viewMap
						.put(
								WebConstants.ManageBeneficiaryFamilyVillageProcess.FAMILY_DATA_TAB_ERRORS,
								errorMessages);
			} else if (successMessages != null && successMessages.size() > 0) {
				viewMap
						.put(
								WebConstants.ManageBeneficiaryFamilyVillageProcess.FAMILY_DATA_TAB_SUCCESS,
								successMessages);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception {
		// TAB MODE
		if (viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE) != null) {
			tabMode = (String) viewMap
					.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		if (viewMap.get("FamilyDetailsViewList") != null) {
			dataList = (ArrayList<FamilyDetailsView>) viewMap
					.get("FamilyDetailsViewList");
		}
		if (viewMap.get("FamilyDetailsView") != null) {
			pageView = (FamilyDetailsView) viewMap.get("FamilyDetailsView");
		}
		if (viewMap.get(WebConstants.PERSON_ID) != null) {
			personId = Long.valueOf(viewMap.get(WebConstants.PERSON_ID)
					.toString());
		}
		updateValuesToMaps();
	}

	@SuppressWarnings("unchecked")
	private void updateValuesToMaps() {
		// TAB MODE
		if (tabMode != null) {
			viewMap.put(Constant.EndowmentFile.MODE_UPDATABLE, tabMode);
		}
		if (dataList != null && dataList.size() > 0) {
			viewMap.put("FamilyDetailsViewList", dataList);
		}
		if (pageView != null) {
			viewMap.put("FamilyDetailsView", pageView);
		}
		if (personId != null) {
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
	}

	public void populateTab(PersonView personView) throws Exception {
		updateValuesFromMaps();
		personId = personView.getPersonId();
		loadDataList(personId);
		setTotalRows(dataList.size());
		setPageDataDefaults();
		updateValuesToMaps();
	}

	private void setPageDataDefaults() throws Exception {
		pageView = new FamilyDetailsView();
		PersonView beneficiary = new PersonView();
		beneficiary.setPersonId(personId);
		pageView.setBeneficiary(beneficiary);
		PersonView familyMember= new PersonView();
		familyMember.setIsDeleted(0l);
		familyMember.setRecordStatus(1l);
		familyMember.setCreatedOn(new Date());
		familyMember.setCreatedBy(getLoggedInUserId());
		familyMember.setUpdatedOn(new Date());
		familyMember.setUpdatedBy(getLoggedInUserId());
		familyMember.setIsCompany(0l);
		pageView.setFamilyMember(familyMember);
		pageView.setCreatedBy(getLoggedInUserId());
		pageView.setCreatedOn(new Date());
		pageView.setUpdatedBy(getLoggedInUserId());
		pageView.setUpdatedOn(new Date());
		pageView.setIsDeleted(0l);

	}

	private void loadDataList(Long personId) throws Exception {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Constant.ResearchRecommendationCriteria.PERSON_ID, personId
				.toString());
		dataList = FamilyVillageFileService.getFamilyDetailsByPersonId(personId);
	}

	@SuppressWarnings("unchecked")
	private boolean hasErrors() throws Exception {
		boolean hasSaveErrors = false;
		errorMessages = new ArrayList<String>();

//		if (pageView.getComments() == null
//				|| pageView.getComments().trim().length() <= 0) {
//			errorMessages
//					.add(CommonUtil
//							.getBundleMessage("familyVillageManageBeneficiary.msg.recommendationRequired"));
//			return true;
//		}

		return hasSaveErrors;

	}
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
	}
	@SuppressWarnings("unchecked")
	public void onOpenResearcherForm()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try {
			
			FamilyDetailsView row= (FamilyDetailsView) dataTable.getRowData();
			sessionMap.put(WebConstants.PERSON_ID, row.getFamilyMember().getPersonId());
			executeJavascript("openResearchFormFamilyVillage();");
			
		} catch (Exception e) {
			logger.LogException("onOpenResearcherForm--- EXCEPTION --- ", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		} finally {
			if (errorMessages != null && errorMessages.size() > 0) {
				viewMap
						.put(
								WebConstants.ManageBeneficiaryFamilyVillageProcess.FAMILY_DATA_TAB_ERRORS,
								errorMessages);
			} else if (successMessages != null && successMessages.size() > 0) {
				viewMap
						.put(
								WebConstants.ManageBeneficiaryFamilyVillageProcess.FAMILY_DATA_TAB_SUCCESS,
								successMessages);
			}
		}
	}
	@SuppressWarnings("unchecked")
	public void onAdd() {
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try {
			if (hasErrors())
				return;
			try {
				ApplicationContext.getContext().getTxnContext().beginTransaction();
					FamilyDetailsView object = (FamilyDetailsView) BeanUtils.cloneBean(pageView);
					FamilyVillageFileService.persistFamilyDetails(object);
				ApplicationContext.getContext().getTxnContext().commit();
				setPageDataDefaults();
				loadDataList(personId);
				
				successMessages.add(ResourceUtil.getInstance().getProperty("successMsg.costCenterAdded"));
				
			} catch (Exception e) {
				ApplicationContext.getContext().getTxnContext().rollback();
				throw e;
			} finally {
				ApplicationContext.getContext().getTxnContext().release();
			}
			getDataList();
			
		} catch (Exception e) {
			logger.LogException("onAdd--- EXCEPTION --- ", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		} finally {
			if (errorMessages != null && errorMessages.size() > 0) {
				viewMap
						.put(
								WebConstants.ManageBeneficiaryFamilyVillageProcess.FAMILY_DATA_TAB_ERRORS,
								errorMessages);
			} else if (successMessages != null && successMessages.size() > 0) {
				viewMap
						.put(
								WebConstants.ManageBeneficiaryFamilyVillageProcess.FAMILY_DATA_TAB_SUCCESS,
								successMessages);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void onDelete() {
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try {
			updateValuesFromMaps();
			successMessages = new ArrayList<String>();
			FamilyDetailsView row= (FamilyDetailsView) dataTable.getRowData();
			row.setIsDeleted(1l);
			row.setUpdatedBy(getLoggedInUserId());
			row.setUpdatedOn(new Date() );
			FamilyVillageFileService.persistFamilyDetails(row);
			
			loadDataList(personId);
			
			successMessages.add(ResourceUtil.getInstance().getProperty("revenueAndExpensesManage.msg.deleteRecord"));
			updateValuesToMaps();
		} catch (Exception e) {
			logger.LogException("onDelete--- EXCEPTION --- ", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		} finally {
			if (errorMessages != null && errorMessages.size() > 0) {
				viewMap
						.put(
								WebConstants.ManageBeneficiaryFamilyVillageProcess.FAMILY_DATA_TAB_ERRORS,
								errorMessages);
			} else if (successMessages != null && successMessages.size() > 0) {
				viewMap
						.put(
								WebConstants.ManageBeneficiaryFamilyVillageProcess.FAMILY_DATA_TAB_SUCCESS,
								successMessages);
			}
		}

	}

	public boolean getIsTabModeUpdatable() {
		boolean isTabModeUpdatable = false;

		if (tabMode.equalsIgnoreCase(Constant.EndowmentFile.MODE_UPDATABLE))
			isTabModeUpdatable = true;

		return isTabModeUpdatable;
	}

	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<FamilyDetailsView> getDataList() {
		return dataList;
	}

	public void setDataList(List<FamilyDetailsView> dataList) {
		this.dataList = dataList;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public FamilyDetailsView getPageView() {
		return pageView;
	}

	public void setPageView(FamilyDetailsView pageView) {
		this.pageView = pageView;
	}

}
