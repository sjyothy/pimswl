package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.ui.util.ResourceUtil;

public class BeneficiaryDisbursementDetailsPopup extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	HttpServletRequest request;
	List<DisbursementDetailsView > list = new ArrayList<DisbursementDetailsView>();
	private HtmlDataTable dataTable;
	public BeneficiaryDisbursementDetailsPopup() 
	{
		request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if ( viewMap.get( "list" ) != null )
		{
			list =  (ArrayList<DisbursementDetailsView>)viewMap.get( "list" );
		}
		else
		{
			Long personId = null;
			Long fileId	  = null;
			Long requestId = null;
			List<Long> longList = new ArrayList<Long>();
			if( request.getParameter("personId") != null)
			{
				String[] split =  request.getParameter("personId").toString().split(",");
				for (String string : split)
				{
					if( string.length() > 0 )
					{
						longList.add( new Long( string ) );
					}
				}
			}
			if( request.getParameter("fileId") != null)
			{
				fileId =  new Long(request.getParameter("fileId").toString());
				viewMap.put("fileId",fileId );
			}
			if( request.getParameter("requestId") != null)
			{
				
				requestId =  new Long(request.getParameter("requestId").toString());
				if( requestId.compareTo(0l) <=0  )
				{
					requestId = null;
				}
			}
			List<Long> disDetIdNotIn = null;
			if( sessionMap .get("disDetIdNotIn") != null)
			{
				
				List<Long> split =  (ArrayList<Long>) sessionMap.remove( "disDetIdNotIn");
				disDetIdNotIn = new ArrayList<Long>();
				for (Long string : split)
				{
						disDetIdNotIn.add( string );
				}
			}
			list = DisbursementService.getRequestedDisbursements(longList, fileId, requestId,disDetIdNotIn);
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( list != null )
		{
			viewMap.put( "list", list );
		}
	}

	   @SuppressWarnings( "unchecked" )
		public void openManageBeneficiaryPopUp(javax.faces.event.ActionEvent event)
	   {
	    	try
	    	{
	    		DisbursementDetailsView gridViewRow = (DisbursementDetailsView) dataTable.getRowData();
	    		if(	gridViewRow.getPersonId()>0 && 
	    			(
						gridViewRow.getIsMultiple()== null ||	
						gridViewRow.getIsMultiple().compareTo(0L)==0
	    			)
	    		  )
	    		{

	    			executeJavaScript( "javaScript:openBeneficiaryPopup("+ gridViewRow.getPersonId()+");");
	    		}
	    		else if( gridViewRow.getBeneficiaryCount() > 1 )
	    		{
	    		  openMultipleBeneficiaryPopup(gridViewRow, true);
	    		}
	    		else
	    		{
	    			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsNormalDisbursementMsgs.ERR_NO_BENEFICIARY));
	    		}
		   } catch (Exception ex) {
				logger.LogException("[Exception occured in openManageBeneficiaryPopUp()]", ex);
				errorMessages = new ArrayList<String>(0);
				errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			}
		}
	   
	   @SuppressWarnings("unchecked")
		private void openMultipleBeneficiaryPopup( DisbursementDetailsView disbursement,boolean readOnly )throws Exception
		{
			sessionMap.put(WebConstants.LOAD_PAYMENT_DETAILS, disbursement);
			sessionMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_ID, Long.valueOf( viewMap.get("fileId").toString() ));
			if(readOnly)
			{
				sessionMap.put( WebConstants.PAGE_MODE, WebConstants.PAGE_MODE_VIEW);
			}
			executeJavaScript( "javaScript:openAddMultipleBenefificiaryForDisbursementPopup();");
		}


	public List<DisbursementDetailsView> getList() {
		return list;
	}

	public void setList(List<DisbursementDetailsView> list) {
		this.list = list;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}	
    

}
