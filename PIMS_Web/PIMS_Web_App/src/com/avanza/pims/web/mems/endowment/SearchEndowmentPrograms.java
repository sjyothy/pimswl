package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.Utils.EndProgSearchCriteria;
import com.avanza.pims.Utils.generatorViews.MasarifSearchView;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.EndowmentProgram;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractSearchBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.EndowmentTransformUtil;
import com.avanza.pims.ws.mems.MasarifService;
import com.avanza.pims.ws.mems.endowment.EndowmentProgramService;
import com.avanza.pims.ws.vo.mems.EndowmentProgramView;
import com.avanza.ui.util.ResourceUtil;

public class SearchEndowmentPrograms extends AbstractSearchBean 
{
	private static final long serialVersionUID = 1L;
	private final String DEFAULT_SORT_FIELD = "refNum";
	private final String DATA_LIST = "DATA_LIST";
	EndowmentProgramService service = new EndowmentProgramService();

	EndProgSearchCriteria criteria = new EndProgSearchCriteria();
	List<EndowmentProgram> dataList = new ArrayList<EndowmentProgram>();
	

	@SuppressWarnings("unchecked")
	@Override
	public void init() 
	{
		super.init();
		try 
		{
			if (!isPostBack()) 
			{
				initData();
			}
		} catch (Exception es) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void initData() throws Exception 
	{
		setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
		setSortField(DEFAULT_SORT_FIELD);
		setSortItemListAscending(false);
		checkDataInQueryString();
	}

	private void checkDataInQueryString() {
		checkPageMode();
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() 
	{

		if (request.getParameter(VIEW_MODE) != null) 
		{

			if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);

			}
			else if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);

			}
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onSingleSelect() 
	{
		try 
		{
			EndowmentProgram obj = null;
			obj = (EndowmentProgram) dataTable.getRowData();
			EndowmentProgramView view = new EndowmentProgramView();
			EndowmentTransformUtil.transformToEndowmentProgramView(view, obj);
			sessionMap.put(WebConstants.SELECTED_ROW,view);
			executeJavascript("closeWindow();");
		}
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onSingleSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public void onMultiSelect() 
	{
		try 
		{
			dataList = getDataList();
			List<EndowmentProgram> files = new ArrayList<EndowmentProgram>();
			for(EndowmentProgram obj: dataList)
			{
				if( obj.isSelected() )
				{
					files.add(obj);
				}
			}
			
			if(files.size() <= 0)
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add( CommonUtil.getBundleMessage("endowment.errMsg.noSelectMade") );
				return;
			}
			viewMap.put(WebConstants.SELECTED_ROWS, files);
			executeJavascript("closeWindow();");
		}
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onMultiSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public String onAdd() 
	{
		return "manage";
	}
	@SuppressWarnings("unchecked")
	public String onEdit() 
	{
		try 
		{
			EndowmentProgram row = (EndowmentProgram) dataTable.getRowData();
			if (row != null) 
			{
				sessionMap.put(Constant.EndowmentProgram.ENDOWMENT_PROGRAM, row);
				return "manage";
			}
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onEdit |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}

	public void onSearch() 
	{
		try 
		{
			pageFirst();// at last
		} 
		catch (Exception e) 
		{
			logger.LogException("onSearch |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void doSearchItemList()
	{
		try {
				loadDataList();
			}
		    catch (Exception e){
		    	errorMessages = new ArrayList<String>(0);
		 		logger.LogException("doSearchItemList"+"|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		    }
	}

	@SuppressWarnings("unchecked")
	public void loadDataList() throws Exception 
	{

		int totalRows = 0;
		totalRows = service.getTotalNumberOfRecords( criteria );
		setTotalRows(totalRows);
		doPagingComputations();

		dataList = service.search(criteria, getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());

		if (dataList== null || dataList.isEmpty() ) 
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			forPaging(0);
		}
		
		this.setDataList( dataList );
		forPaging(getTotalRows());
	}


	@SuppressWarnings("unchecked")
	public List<SelectItem> getAllMasarif() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		try {
			if (viewMap.get(WebConstants.ALL_MASARIF) != null) {
				// get data from viewMap
				items = (List<SelectItem>) viewMap.get(WebConstants.ALL_MASARIF);
			} else {
				// get data from db and put prepared list in viewMap
				List<MasarifSearchView> masarif = null;
				masarif = new MasarifService().getAllMasraf();
				for (MasarifSearchView masraf : masarif) {
					SelectItem item = new SelectItem(masraf.getMasrafId()
							.toString(), masraf.getMasrafName());
					items.add(item);
				}
				Collections.sort(items, ListComparator.LIST_COMPARE);
				if (items != null && items.size() > 0) {
					viewMap.put(WebConstants.ALL_MASARIF, items);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return items;
	}

	@SuppressWarnings("unchecked")
	public List<EndowmentProgram> getDataList() 
	{
		if(viewMap.get(DATA_LIST) != null)
		{
			dataList = (List<EndowmentProgram>) viewMap.get(DATA_LIST);
		} 
		return dataList ;
	}

	@SuppressWarnings("unchecked")
	public void setDataList(List<EndowmentProgram> list) 
	{
		this.dataList = list;
		if(this.dataList != null )
		{
			viewMap.put(DATA_LIST, list);
		}

	}

	
	@SuppressWarnings("unchecked")
	
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javascript);
	}

	public EndProgSearchCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(EndProgSearchCriteria criteria) {
		this.criteria = criteria;
	}

	
}
