/**
 * @author Kamran Ahmed
 */
package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.custom.fileupload.HtmlInputFileUpload;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.mems.InheritedAssetService;
import com.avanza.pims.ws.mems.SocialResearchService;
import com.avanza.pims.ws.vo.CaringTypeView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.BeneficiariesPickListView;
import com.avanza.pims.ws.vo.mems.MemsFollowupView;
import com.avanza.pims.ws.vo.mems.ResearchRecommendationView;

public class SocialReseacrhRecommTabBean extends AbstractMemsBean {

	private static final long serialVersionUID = 1L;

	private  final String SINGLE_RECOMMENDATION = "SINGLE_RECOMMENDATION";
	
	private final String DISBURSEMENT_LIST_MAP = "DISBURSEMENT_LIST_MAP";
	private final String CARING_TYPE_MAP = "CARING_TYPE_MAP";
	private final String RECOMMENDATION_STATUS_DD_MAP = "RECOMMENDATION_STATUS_DD_MAP";
	
	private final String DISBURSEMENT_TYPE = "DISBURSEMENT_TYPE";
	private final String BENENEFICIARY_LIST = "BENENEFICIARY_LIST";
	private final String SOCIAL_RECOMM_VIEW = "SOCIAL_RECOMM_VIEW";
	private final String SOCIAL_RECOMM_VIEW_LIST = "SOCIAL_RECOMM_VIEW_LIST";
	private final String SOCIAL_RECOMM_VIEW_LIST_UPDATED_RECORD = "SOCIAL_RECOMM_VIEW_LIST_UPDATED_RECORD";
	private final String BENEFICIARY_MAP = "BENEFICIARY_MAP";
	private HtmlDataTable researchRecommDataTable;
	private List<ResearchRecommendationView> researchRecommList = new ArrayList<ResearchRecommendationView>();
	private ResearchRecommendationView researchRecommView = new ResearchRecommendationView();
	private DomainDataView monthsDD = new DomainDataView();
	private DomainDataView yearsDD = new DomainDataView();
	private DomainDataView daysDD = new DomainDataView();
	private HashMap<String, String> beneficiaryMap= new HashMap<String, String>();
	private HtmlSelectOneMenu cboDisList = new HtmlSelectOneMenu();
	private HtmlSelectBooleanCheckbox chkDisReq = new HtmlSelectBooleanCheckbox();
	private HtmlSelectOneRadio rdoPeriodic = new  HtmlSelectOneRadio();
	private HtmlSelectOneMenu cboPeriod = new HtmlSelectOneMenu(); 
	private HtmlInputText txtAmount = new HtmlInputFileUpload();
	private HtmlInputText txtDurationValue = new HtmlInputText();
	private HtmlSelectOneMenu cboDurationFrequency = new HtmlSelectOneMenu();
	private HtmlCalendar clndrStartDate = new HtmlCalendar();
	private HtmlCalendar clndrEndDate = new HtmlCalendar();
	private HtmlSelectOneMenu cboCaringList = new HtmlSelectOneMenu();
	private HtmlInputTextarea txtAreaComments = new HtmlInputTextarea();
	private HtmlInputTextarea txtRejectionReason = new HtmlInputTextarea();
	
	private HtmlSelectOneMenu cboBeneficiaryPersonId = new HtmlSelectOneMenu();
	HashMap<Long, String> caringTypeMap =  new HashMap<Long, String>();
	HashMap<Long, String> disListMap =  new HashMap<Long, String>();
	HashMap<String,DomainDataView> recommendationStatusMap = new HashMap<String, DomainDataView>();
	
	private Boolean initializationDone=false;
	public SocialReseacrhRecommTabBean() {
		
	}

	private void setValuesInViewMap()throws Exception {
		if(viewMap.get(WebConstants.InheritanceFile.FILE_ID) != null)
		{
			Long fileId = new Long( viewMap.get(WebConstants.InheritanceFile.FILE_ID).toString());
			createBeneficiaryPickList(fileId);
			createDisbursementListType();
			createBeneficiaryPickList(fileId);
			createDisbursementListType();
		}
	}

	public HtmlDataTable getResearchRecommDataTable() {
		return researchRecommDataTable;
	}

	public void setResearchRecommDataTable(HtmlDataTable researchRecommDataTable) {
		this.researchRecommDataTable = researchRecommDataTable;
	}

	@SuppressWarnings("unchecked")
	private void createBeneficiaryPickList(Long fileId)throws Exception {
		List<BeneficiariesPickListView> benefPickListView = new InheritedAssetService().getBeneficiariesView(fileId);
		HashMap<String, String> beneficiaryMap = new HashMap<String, String>();
		ArrayList<SelectItem> bnfList = new ArrayList<SelectItem>(0);
		String minor = CommonUtil.getBundleMessage("commons.minor");
		for(BeneficiariesPickListView singleItem : benefPickListView) 
		{
			
			String benefDisplayValue =singleItem.getFullName();
			if(singleItem.getIsMinor()!=null && singleItem.getIsMinor().equals(1L))
			{
			   benefDisplayValue = singleItem.getFullName()+ "( "+minor +" )";
			}
			beneficiaryMap.put(singleItem.getPersonId().toString(),benefDisplayValue );
			SelectItem selItem = new SelectItem(singleItem.getPersonId().toString(),benefDisplayValue );
			bnfList.add(selItem);
		}
		viewMap.put(BENENEFICIARY_LIST,bnfList);
		viewMap.put(BENEFICIARY_MAP , beneficiaryMap);
	}

	@SuppressWarnings("unchecked")
	public void init() 
	{
		super.init();
		String METHOD_NAME = "init";
		if (!isPostBack()) 
		{
		}
		try	
		{	
			setValuesInViewMap();
			setValuesToMap();
			setControlsInitialValue();
			loadDataList();
			
		}
		catch (Exception exception) 
		{
		
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	@SuppressWarnings("unchecked")
	private void loadDataList()throws Exception {
		
		
		 if (isEditMode())
		 {
			 if(viewMap.get(SINGLE_RECOMMENDATION)!=null)
			 {
				 	ResearchRecommendationView selectedRecommendation = (ResearchRecommendationView)viewMap.get(SINGLE_RECOMMENDATION);
					List<ResearchRecommendationView> researchRecommendations = new ArrayList<ResearchRecommendationView>();
					researchRecommendations.add(selectedRecommendation);
					
					if(researchRecommendations != null && researchRecommendations.size()>0 )
					{
						viewMap.put(SOCIAL_RECOMM_VIEW_LIST, researchRecommendations);
					}
			 }
		 }
		 else
		if(viewMap.get("memsFollowupView") != null)
		{
			MemsFollowupView followup =(MemsFollowupView) viewMap.get("memsFollowupView"); 
			List<ResearchRecommendationView> researchRecommendations = new ArrayList<ResearchRecommendationView>();
			researchRecommendations=   new SocialResearchService().getRecommendationsByFollowupId(followup.getFollowupId());
			if(researchRecommendations != null && researchRecommendations.size() > 0)
			{
				viewMap.put(SOCIAL_RECOMM_VIEW_LIST, researchRecommendations);
			}
		}
		
	}

	private void setControlsInitialValue() 
	{
		if(isEditMode() && !initializationDone) // first time load in edit mode , value setting from parent
		{
			//set controls values as obtained from the sessionMap.put(WebConstants.MemsFollowupTab.FOLLOW_UP_SINGLE_RECOMMENDATION,selectedRecommendation
			ResearchRecommendationView selectedRecommendation = (ResearchRecommendationView)viewMap.get(SINGLE_RECOMMENDATION);
			if(selectedRecommendation!=null )
			{
				cboBeneficiaryPersonId.setValue(selectedRecommendation.getPerson().getPersonId().toString());
				cboCaringList.setValue(selectedRecommendation.getCaringType().getCaringTypeId().toString());
				cboDisList.setValue(selectedRecommendation.getDisbursementListTypeId()!=null ? selectedRecommendation.getDisbursementListTypeId().toString():null);
				if(selectedRecommendation.getDisbursementRequired()!=null)
					chkDisReq.setValue(selectedRecommendation.getDisbursementRequired().trim().equals("1")?true:false);
				txtAmount.setValue(selectedRecommendation.getAmount());
				cboPeriod.setValue(selectedRecommendation.getPeriodId()!=null ? selectedRecommendation.getPeriodId().toString():null);
				txtDurationValue.setValue(selectedRecommendation.getDurationValue()!=null ? selectedRecommendation.getDurationValue():null );
				cboDurationFrequency.setValue(selectedRecommendation.getDurationFreq()!=null ?selectedRecommendation.getDurationFreq().toString():"-1" );
				clndrStartDate.setValue(selectedRecommendation.getStartDate());
				clndrEndDate.setValue(selectedRecommendation.getEndDate());
				txtAreaComments.setValue(selectedRecommendation.getComments());
				txtRejectionReason.setValue(selectedRecommendation.getRejectionReason());
//				rdoPeriodic.setValue(selectedRecommendation.getIsPeriodic()!=null?(selectedRecommendation.getIsPeriodic().equals("1")?"1":"0"):null);
				// the above initialization should work only once when the form is loading , when one of the control's ajax onchange is called
				// the else part of the code should work , to reinitialize the controls with default values.
				
				if(cboCaringList.getValue() != null && cboCaringList.getValue().toString().compareTo("-1") != 0)
				{
					
					if(new Long(cboCaringList.getValue().toString()).compareTo(WebConstants.CaringType.FINANCIAL) == 0)
					{
						cboDisList.setDisabled(false);
//						chkDisReq.setDisabled(true);
						if(cboDisList.getValue() != null && cboDisList.getValue().toString().compareTo("-1") != 0)
						{
							chkDisReq.setValue(true);
							chkDisReq.setDisabled(true);
							setControlsValueForDisRequiredForEditMode();
//							rdoPeriodic.setDisabled(true);
						}
					}
					else
					{
						cboDisList.setDisabled(true);
						
					}
				}
				
			}
			
		}
		else if (isEditMode() && initializationDone) // after caring type changed 
		{
			controlResetForEditMode();
		}
		else 										//original insertion mode
		{
		//Controls handling
		cboDisList.setValue("-1");
		cboDisList.setDisabled(true);
		chkDisReq.setValue(false);
		chkDisReq.setDisabled(false);
//		rdoPeriodic.setValue("-1");
//		rdoPeriodic.setDisabled(true);
		cboPeriod.setValue("-1");
		cboPeriod.setDisabled(true) ;
		txtAmount.setValue(null);
		txtAmount.setDisabled(true);
		txtDurationValue.setValue(null);
		txtDurationValue.setDisabled(true);
		cboDurationFrequency.setValue("-1");
		cboDurationFrequency.setDisabled(true);
		clndrStartDate.setValue(null);
		clndrStartDate.setDisabled(true);
		clndrEndDate.setValue(null);
		//Controls handling
		}
	}
	private void setControlsValueForDisRequiredForEditMode() {
		// TODO Auto-generated method stub
		//Controls handling
//		rdoPeriodic.setDisabled(false);
		cboPeriod.setDisabled(false) ;
		txtAmount.setDisabled(false);
		txtDurationValue.setDisabled(false);
		cboDurationFrequency.setDisabled(false);
		//Controls handling
		
	}

	private void controlResetForEditMode()
	{
		//Controls handling
		cboDisList.setValue("-1");
		cboDisList.setDisabled(true);
		chkDisReq.setValue(false);
		chkDisReq.setDisabled(false);
//		rdoPeriodic.setValue("-1");
//		rdoPeriodic.setDisabled(true);
		cboPeriod.setValue("-1");
		cboPeriod.setDisabled(true) ;
		txtAmount.setValue(null);
		txtAmount.setDisabled(true);
		txtDurationValue.setValue(null);
		txtDurationValue.setDisabled(true);
		cboDurationFrequency.setValue("-1");
		cboDurationFrequency.setDisabled(true);
		clndrStartDate.setValue(null);
		clndrStartDate.setDisabled(true);
		clndrEndDate.setValue(null);
		txtAreaComments.setValue("");
		txtRejectionReason.setValue("");
		//Controls handling
	}
	private void setControlsValueForDisRequired() 
	{
		//Controls handling
//		rdoPeriodic.setValue("1");
//		rdoPeriodic.setDisabled(false);
		cboPeriod.setValue("-1");
		cboPeriod.setDisabled(false) ;
		txtAmount.setValue(null);
		txtAmount.setDisabled(false);
		txtDurationValue.setValue(null);
		txtDurationValue.setDisabled(false);
		cboDurationFrequency.setValue("-1");
		cboDurationFrequency.setDisabled(false);
		clndrStartDate.setValue(null);
		clndrStartDate.setDisabled(false);
		clndrEndDate.setValue(null);
		//Controls handling
	}
	private void setControlsValueForDisNotRequired() 
	{
		//Controls handling
//		rdoPeriodic.setValue("-1");
//		rdoPeriodic.setDisabled(true);
		cboPeriod.setValue("-1");
		cboPeriod.setDisabled(true) ;
		txtAmount.setValue(null);
		txtAmount.setDisabled(true);
		txtDurationValue.setValue(null);
		txtDurationValue.setDisabled(true);
		cboDurationFrequency.setValue("-1");
		cboDurationFrequency.setDisabled(true);
		clndrStartDate.setValue(null);
		clndrStartDate.setDisabled(true);
		clndrEndDate.setValue(null);
		//Controls handling
	}
	@SuppressWarnings("unchecked")
	public ArrayList<SelectItem> getBeneficiaryList() {
		if (viewMap.containsKey(BENENEFICIARY_LIST)) {
			return (ArrayList<SelectItem>) viewMap.get(BENENEFICIARY_LIST);
		}
		return new ArrayList<SelectItem>();
	}
	@SuppressWarnings("unchecked")
	public void createDisbursementListType() {

		HashMap<Long, String> disbursementTypeList = null;
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		DisbursementService service = new DisbursementService();

		try {
			disbursementTypeList = service.getDisbursementTypeList(CommonUtil.getIsEnglishLocale());
			viewMap.put(DISBURSEMENT_LIST_MAP, disbursementTypeList);
			Set s = disbursementTypeList.entrySet();

			Iterator it = s.iterator();

			while (it.hasNext()) {
				// key=value separator this by Map.Entry to get key and value
				Map.Entry m = (Map.Entry) it.next();

				// getKey is used to get key of HashMap
				Long key = (Long) m.getKey();

				// getValue is used to get value of key in HashMap
				String value = (String) m.getValue();

				selectItems.add(new SelectItem(key.toString(), value));

			}
			Collections.sort(selectItems, ListComparator.LIST_COMPARE);
			viewMap.put(DISBURSEMENT_TYPE, selectItems);
		} catch (PimsBusinessException e) {
			logger.LogException("createDisbursementTypesList() crashed", e);
		} catch (Exception e) {
			logger.LogException("createDisbursementTypesList() crashed", e);
		}
	}
	@SuppressWarnings("unchecked")
	public List<SelectItem> getDisbursementType()
	{
		List<SelectItem> disList = null;
		if(viewMap.get(DISBURSEMENT_TYPE) != null)
			disList = (List<SelectItem>) viewMap.get(DISBURSEMENT_TYPE);
		return disList;	
	}

	@SuppressWarnings( "unchecked" )
	public void onAddRecommendationsFollowup()
	{
		try	
		{	
			ResearchRecommendationView  row = ( ResearchRecommendationView )researchRecommDataTable.getRowData();
			sessionMap.put( WebConstants.RecommenationFollowup.RECOMMENDATION_ID, row.getResearchRecommendationId() );
		    String javaScriptText ="var screen_width = 800;"+
            "var screen_height = 550;"+
            "window.open('recommFollowup.jsf','_blank','width='+(screen_width-10)+',height='+(screen_height-100)+',left=0,top=100,scrollbars=yes,status=yes,resizeable=true');";
		    executeJavascript(  javaScriptText );
		    
		}
		catch (Exception exception) 
		{
			logger.LogException( "onAddRecommendationsFollowup| --- CRASHED --- ", exception);
		}
		
	}
	

	
	@SuppressWarnings("unchecked")
	public void addRecommendation()
	{
		if(isValidated())
		{
			ResearchRecommendationView researchRecomm = getFilledResearchRecommendation();
			if(researchRecomm.getResearchRecommendationId() == null)
			{
				DomainDataView draftStatus  = getRecommendationStatusMap().get(WebConstants.ResearchRecommendationStatus.DRAFT);
				researchRecomm.setStatus(draftStatus.getDomainDataId());
				researchRecomm.setCreatedBy(getLoggedInUserId());
				researchRecomm.setCreatedOn(new Date() );
				
			}
			else
			{
				researchRecomm.setUpdatedBy(getLoggedInUserId());
			}
			//recommendation against followup
			if(viewMap.get("memsFollowupView") != null)
			{
				MemsFollowupView folllowup = (MemsFollowupView) viewMap.get("memsFollowupView");
				MemsFollowupView fView = new MemsFollowupView();
				fView.setFollowupId(folllowup.getFollowupId());
				researchRecomm.setMemsFollowup(fView);
			}
			try 
			{
				if(isEditMode())
				{
					ResearchRecommendationView selectedRecommendation = (ResearchRecommendationView)viewMap.get(SINGLE_RECOMMENDATION);
					researchRecomm.setResearchRecommendationId(selectedRecommendation.getResearchRecommendationId());
					researchRecomm.setRecommendationNumber(selectedRecommendation.getRecommendationNumber());
				
					new SocialResearchService().persistResearchRecomm(researchRecomm);
					researchRecommList.clear();
					researchRecommList.add(researchRecomm);
					viewMap.put(SOCIAL_RECOMM_VIEW_LIST, researchRecommList);
					sessionMap.put(SOCIAL_RECOMM_VIEW_LIST_UPDATED_RECORD, researchRecomm); // the parent will read from this key
					
					setControlsInitialValue();
					txtAreaComments.setValue(null);
					txtRejectionReason.setValue(null);
					executeJavascript("sendUpdatedRow()");
				}
				else
				{
					new SocialResearchService().persistResearchRecomm(researchRecomm);
					researchRecommList = getResearchRecommList();
					researchRecommList.add(researchRecomm);
					viewMap.put(SOCIAL_RECOMM_VIEW_LIST, researchRecommList);
					setControlsInitialValue();
					txtAreaComments.setValue(null);
					txtRejectionReason.setValue(null);
				}
				
			} catch (Exception e) {
				logger.LogException("addRecommendation() crashed |", e);
			}
		}
	}
/**
 * Desc : This method will fill up the view to display in grid
 * @return ResearchRecommendationView
 */
	private ResearchRecommendationView getFilledResearchRecommendation() 
	{
		ResearchRecommendationView filledView = new ResearchRecommendationView();
		//column1
		filledView.setCaringType(getFilledCaringTypeView());
		
		//column2
		Long personId = new Long(cboBeneficiaryPersonId.getValue().toString());
		String beneName = getBeneficiaryMap().get(personId.toString());
		PersonView personView = new PersonView();
		personView.setPersonId(personId);
		personView.setFirstName(beneName);
		filledView.setPerson(personView);
		
		//column3
		if(chkDisReq.getValue() != null && StringHelper.isNotEmpty(chkDisReq.getValue().toString()))
		{
			if((Boolean) chkDisReq.getValue())//disbursement required
			{
				filledView.setDisbursementRequired("1");
//				//column5,7,8 if periodic
//				if(rdoPeriodic.getValue() != null && rdoPeriodic.getValue().toString().compareTo("-1") != 0 && 
//				   rdoPeriodic.getValue().toString().compareTo("1") == 0
//				   )
//				{
					Long periodId = new Long(cboPeriod.getValue().toString());
					filledView.setPeriodId(periodId);
					filledView.setStartDate((Date) clndrStartDate.getValue());
					filledView.setEndDate((Date) clndrEndDate.getValue());
					filledView.setDurationFreq(new Long(cboDurationFrequency.getValue().toString()));
					filledView.setDurationValue(new Long(txtDurationValue.getValue().toString()));
					filledView.setIsPeriodic("1");
//				}
//				
//				//column6 
//				else if(rdoPeriodic.getValue() != null && rdoPeriodic.getValue().toString().compareTo("-1") != 0 && 
//						rdoPeriodic.getValue().toString().compareTo("0") == 0
//						)
//				{
//					filledView.setIsPeriodic("0");
//				}
//				else
//					filledView.setIsPeriodic("-1");
				
			}
			else //if not required
			{
				filledView.setDisbursementRequired("0");
			}
		}
		else //if not required
		{
			filledView.setDisbursementRequired("0");
		}
		
		//column4
		if(cboDisList.getValue()!=null)
		{
			if(cboDisList.getValue().toString().compareTo("-1") != 0)
			{
				Long disListId = new Long(cboDisList.getValue().toString());
				filledView.setDisbursementListTypeId(disListId);
			}
		}
//		filledView.setDisbursementListTypeEn(getDisListMap().get(disListId));
//		filledView.setDisbursementListTypeAr(getDisListMap().get(disListId));
		
	
		//column9
		filledView.setComments(txtAreaComments.getValue().toString().trim() );
		filledView.setRejectionReason(txtRejectionReason.getValue().toString().trim() );
		//column10
		if(txtAmount.getValue() != null && StringHelper.isNotEmpty(txtAmount.getValue().toString()))
		{
			filledView.setAmount(new Double(txtAmount.getValue().toString()));
			filledView.setRecommendedAmount(new Double(txtAmount.getValue().toString()));
		}
		filledView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		return filledView;
	}


	private CaringTypeView getFilledCaringTypeView() 
	{
		CaringTypeView caringTypeView = new CaringTypeView();  
		String caringDesc = null;
		Long caringTypeId = new Long(cboCaringList.getValue().toString());
		caringDesc = getCaringTypeMap().get(caringTypeId);
		caringTypeView.setCaringTypeId(caringTypeId);
//		caringTypeView.setCaringTypeNameEn(caringDesc);
//		caringTypeView.setCaringTypeNameAr(caringDesc);
		return caringTypeView;
	}

	@SuppressWarnings("unchecked")
	private boolean isValidated() 
	{
		boolean isValidated = true;
		//if not beneficiary selected
		if(StringHelper.isEmpty(cboBeneficiaryPersonId.getValue().toString()) || cboBeneficiaryPersonId.getValue().toString().compareTo("-1") == 0)
		{
			isValidated = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.beneReq"));
		}
		
		//if no caring type selected
		if(StringHelper.isEmpty(cboCaringList.getValue().toString()) || cboCaringList.getValue().toString().compareTo("-1") == 0)
		{
			isValidated = false;
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.caringTypeRequired"));
		}
		//if caring type selected
//		else if(new Long(cboCaringList.getValue().toString()).compareTo(WebConstants.CaringType.FINANCIAL) == 0) // if caring type is financial
//		{
//			//if disbursement List not selected
//			if(StringHelper.isEmpty(cboDisList.getValue().toString()) || cboDisList.getValue().toString().compareTo("-1") == 0)
//			{
//				isValidated = false;
//				errorMessages.add(CommonUtil.getBundleMessage("errMsg.disburementListRequired"));
//			}
//		}
		//if disbursement required then check for required data
		if(StringHelper.isNotEmpty(chkDisReq.getValue().toString()) && (Boolean)chkDisReq.getValue())
		{
//			if(StringHelper.isNotEmpty(rdoPeriodic.getValue().toString()) && rdoPeriodic.getValue().toString().compareTo("-1") != 0)
//			{
				//periodic
//				if(rdoPeriodic.getValue().toString().compareTo("1") == 0)
//				{
					if(clndrStartDate.getValue() == null )
					{
						isValidated = false;
						errorMessages.add(CommonUtil.getBundleMessage("errMsg.startDateRequired"));
					}
					if(clndrEndDate.getValue() == null )
					{
						isValidated = false;
						errorMessages.add(CommonUtil.getBundleMessage("errMsg.endDateRequired"));
					}
					if(StringHelper.isEmpty(cboPeriod.getValue().toString()) || cboPeriod.getValue().toString().compareTo("-1") == 0)
					{
						isValidated = false;
						errorMessages.add(CommonUtil.getBundleMessage("errMsg.periodRequired"));
					}
//				}
				//urgent
//				else if(rdoPeriodic.getValue().toString().compareTo("0") == 0)
//				{
//					//no checking for this
//				}
//			}
			
			//common for both periodic and urgent
			try
			{
				//if amount less than zero
				//Old Code
//				if(StringHelper.isEmpty(txtAmount.getValue().toString()) || new Double(txtAmount.getValue().toString()).compareTo(0D) <= 0)
//				{
//					isValidated = false;
//					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountGTZero"));
//				}	
				//New Code
				if(!StringHelper.isEmpty(txtAmount.getValue().toString()) )
				{
					new Double(txtAmount.getValue().toString());
				}
			}
			catch (Exception e) //if casting exception occurs
			{
				{
					isValidated = false;
					errorMessages.add(CommonUtil.getBundleMessage("mems.payment.err.msgs.amntMustNumeric"));
					if(errorMessages != null && errorMessages.size() > 0)
					{
						viewMap.put(WebConstants.MemsFollowupTab.ERR_MSG_RECOMM_TAB, errorMessages);
					}
					return isValidated;
				}
			}
			
		}
//		else
//		{
//			rdoPeriodic.setValue("-1");
//		}
		// if comments not available
		if(txtAreaComments.getValue() != null && StringHelper.isEmpty(txtAreaComments.getValue().toString()))
		{
			isValidated = false;
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.commentsRequired"));
		}
		// if comments too large
		else if(txtAreaComments.getValue().toString().length() > 250)
		{
			isValidated = false;
			errorMessages.add(CommonUtil.getBundleMessage("errMsg.commentsTooLarge"));
		}
		if(errorMessages != null && errorMessages.size() > 0)
		{
			//these error messages will be displayed using parent backing prerender
			viewMap.put(WebConstants.MemsFollowupTab.ERR_MSG_RECOMM_TAB, errorMessages);
		}
		return isValidated;
	}

	@SuppressWarnings("unchecked")
	public List<ResearchRecommendationView> getResearchRecommList() 
	{
		if(viewMap.get(SOCIAL_RECOMM_VIEW_LIST) != null)
			researchRecommList = (List<ResearchRecommendationView>) viewMap.get(SOCIAL_RECOMM_VIEW_LIST);
		return researchRecommList;
	}

	@SuppressWarnings("unchecked")
	public void setResearchRecommList(
			List<ResearchRecommendationView> researchRecommList) {
		if(researchRecommList != null && researchRecommList.size() > 0)
		{
			viewMap.put(SOCIAL_RECOMM_VIEW_LIST, researchRecommList);
		}
		this.researchRecommList = researchRecommList;
	}
	public void editRecomm()
	{
	}
	@SuppressWarnings("unchecked")
	public void deleteRecomm()
	{
		ResearchRecommendationView researchRecomm =  (ResearchRecommendationView) researchRecommDataTable.getRowData();
		List<ResearchRecommendationView> researchRecommList = new ArrayList<ResearchRecommendationView>();
		researchRecomm.setIsDeleted(1L);
		try 
		{
			new SocialResearchService().deleteResearchRecommendation(researchRecomm);
		} 
		catch (Exception e) 
		{
			logger.LogException("deleteRecomm() crashed", e);
		}
		researchRecommList = getResearchRecommList();
		researchRecommList.remove(researchRecomm);
		viewMap.put(SOCIAL_RECOMM_VIEW_LIST, researchRecommList);
	}

	public DomainDataView getMonthsDD() {
		if(viewMap.get(WebConstants.GracePeriodDataValues.MONTHS) != null)
			monthsDD = (DomainDataView) viewMap.get(WebConstants.GracePeriodDataValues.MONTHS);
		return monthsDD;
	}

	public void setMonthsDD(DomainDataView monthsDD) {
		this.monthsDD = monthsDD;
	}

	public DomainDataView getYearsDD() {
		if(viewMap.get(WebConstants.GracePeriodDataValues.YEARS) != null)
			yearsDD = (DomainDataView) viewMap.get(WebConstants.GracePeriodDataValues.YEARS);
		return yearsDD;
	}

	public void setYearsDD(DomainDataView yearsDD) {
		this.yearsDD = yearsDD;
	}

	public DomainDataView getDaysDD() {
		if(viewMap.get(WebConstants.GracePeriodDataValues.DAYS) != null)
			daysDD = (DomainDataView) viewMap.get(WebConstants.GracePeriodDataValues.DAYS);
		return daysDD;
	}

	public void setDaysDD(DomainDataView daysDD) {
		this.daysDD = daysDD;
	}

	 public void calculateEndDate()
	 	{
	 		if(clndrStartDate.getValue() != null &&
	 		   txtDurationValue.getValue()!= null && StringHelper.isNotEmpty(txtDurationValue.getValue().toString())&& (new Double(txtDurationValue.getValue().toString())).compareTo(0D) > 0 &&
	 		   cboDurationFrequency.getValue() != null && cboDurationFrequency.getValue().toString().compareTo("-1") != 0)
	 		{
	 		 try{
					 int durationValue = new Integer(txtDurationValue.getValue().toString());
					   //For Dates
	                   Calendar cal = Calendar.getInstance();
	                   //Setting The date o parameter
	                   cal.setTime((Date) clndrStartDate.getValue());
			
			           if(cboDurationFrequency.getValue().toString().compareTo(getDaysDD().getDomainDataId().toString()) == 0)
			           {
			           
			           cal.add(Calendar.DATE,durationValue);
			           clndrEndDate.setValue(cal.getTime());
			           }
			           
			           if(cboDurationFrequency.getValue().toString().compareTo(getMonthsDD().getDomainDataId().toString()) == 0)
			           { 
			           cal.add(Calendar.MONTH, durationValue);
			           clndrEndDate.setValue(cal.getTime());
			           } 
			           if(cboDurationFrequency.getValue().toString().compareTo(getYearsDD().getDomainDataId().toString()) == 0)
			           {
			            cal.add(Calendar.YEAR,durationValue);
			            clndrEndDate.setValue(cal.getTime());
			           }
				} 
	 		 catch (Exception e) 
	 		 {
					logger.LogException("calculateEndDate() crashed", e);
			 }
	 		}
	 	}

	public ResearchRecommendationView getResearchRecommView() 
	{
		if(viewMap.get(SOCIAL_RECOMM_VIEW) != null)
			researchRecommView = (ResearchRecommendationView) viewMap.get(SOCIAL_RECOMM_VIEW);
		return researchRecommView;
	}

	@SuppressWarnings("unchecked")
	public void setResearchRecommView(ResearchRecommendationView researchRecommView) {
		if(researchRecommView != null)
			viewMap.put(SOCIAL_RECOMM_VIEW,researchRecommView);
		this.researchRecommView = researchRecommView;
	}

	public void setBeneficiaryMap(HashMap<String, String> beneficiaryMap) {
		this.beneficiaryMap = beneficiaryMap;
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, String> getBeneficiaryMap() 
	{
		if(viewMap.get(BENEFICIARY_MAP) != null )
			beneficiaryMap = (HashMap<String, String>) viewMap.get(BENEFICIARY_MAP);
		return beneficiaryMap;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void prerender() 
	{
		super.prerender();
		
	}
	@SuppressWarnings("unchecked")
	private void setValuesToMap() throws Exception 
	{
		List<DomainDataView>  durationFreqDDList = null;
		if(viewMap.get(WebConstants.GRACE_PERIOD_TYPE) == null)
			{
			durationFreqDDList= CommonUtil.getDomainDataListForDomainType(WebConstants.GRACE_PERIOD_TYPE);
				viewMap.put(WebConstants.GRACE_PERIOD_TYPE, durationFreqDDList);
			}
		else
			{
			durationFreqDDList =(List<DomainDataView>) viewMap.get(WebConstants.GRACE_PERIOD_TYPE); 
			}
		
		if(durationFreqDDList != null && durationFreqDDList.size() > 0)
		{
			
			monthsDD = CommonUtil.getIdFromType(durationFreqDDList,WebConstants.GracePeriodDataValues.MONTHS);
			if(monthsDD != null && monthsDD.getDomainDataId() != null)
			{
				viewMap.put(WebConstants.GracePeriodDataValues.MONTHS, monthsDD);
			}
			
			yearsDD = CommonUtil.getIdFromType(durationFreqDDList,WebConstants.GracePeriodDataValues.YEARS);
			if(yearsDD != null && yearsDD.getDomainDataId() != null)
			{
				viewMap.put(WebConstants.GracePeriodDataValues.YEARS, yearsDD);
			}
			
			daysDD = CommonUtil.getIdFromType(durationFreqDDList,WebConstants.GracePeriodDataValues.DAYS);
			if(daysDD != null && daysDD.getDomainDataId() != null)
			{
				viewMap.put(WebConstants.GracePeriodDataValues.DAYS, daysDD);
			}
		}
		
		
		if(viewMap.get(CARING_TYPE_MAP) == null)
		{
			HashMap<Long, String> caringTypeMap = null;
			caringTypeMap = CommonUtil.getCaringTypeMap();
			viewMap.put(CARING_TYPE_MAP, caringTypeMap);
		}
		
		if(viewMap.get(RECOMMENDATION_STATUS_DD_MAP) == null)
		{
			List<DomainDataView> recommStatusList = new ArrayList<DomainDataView>();
			HashMap<String,DomainDataView> recommStatusDD = new HashMap<String, DomainDataView>();
			recommStatusList =  CommonUtil.getDomainDataListForDomainType(WebConstants.ResearchRecommendationStatus.RECOMMENDATION_STATUS);
			if(recommStatusList != null && recommStatusList.size() > 0)
			{
				for(DomainDataView status : recommStatusList)
				{
					recommStatusDD.put(status.getDataValue(), status);
				}
			}
			if(recommStatusDD != null && recommStatusDD.size() > 0)
			{
				viewMap.put(RECOMMENDATION_STATUS_DD_MAP, recommStatusDD);
			}
			
		}
		
	}
	public void caringTypeChanged()
	{
			initializationDone=true;
			if(cboCaringList.getValue() != null && cboCaringList.getValue().toString().compareTo("-1") != 0)
			{
				
				if(new Long(cboCaringList.getValue().toString()).compareTo(WebConstants.CaringType.FINANCIAL) == 0)
				{
					cboDisList.setDisabled(false);
				}
				else
					setControlsInitialValue();
			}
			else
				setControlsInitialValue();
	}
	public void disburementListChanged()
	{
		if(cboDisList.getValue() != null && cboDisList.getValue().toString().compareTo("-1") != 0)
		{
			chkDisReq.setValue(true);
			chkDisReq.setDisabled(true);
			setControlsValueForDisRequired();
			//rdoPeriodic.setDisabled(true);
		}
		else
		{
			setControlsInitialValue();
			cboDisList.setDisabled(false);
			setControlsValueForDisNotRequired();
		}
	}
	public void disburemtentRequiredClicked()
	{
			if(chkDisReq.getValue() != null && (Boolean)chkDisReq.getValue())
			{
				setControlsValueForDisRequired();
			}
			else
				setControlsValueForDisNotRequired();
	}
	

	public HtmlSelectOneMenu getCboDisList() {
		return cboDisList;
	}

	public void setCboDisList(HtmlSelectOneMenu cboDisList) {
		this.cboDisList = cboDisList;
	}

	public HtmlSelectBooleanCheckbox getChkDisReq() {
		return chkDisReq;
	}

	public void setChkDisReq(HtmlSelectBooleanCheckbox chkDisReq) {
		this.chkDisReq = chkDisReq;
	}

	public HtmlSelectOneRadio getRdoPeriodic() {
		return rdoPeriodic;
	}

	public void setRdoPeriodic(HtmlSelectOneRadio rdoPeriodic) {
		this.rdoPeriodic = rdoPeriodic;
	}

	public HtmlSelectOneMenu getCboPeriod() {
		return cboPeriod;
	}

	public void setCboPeriod(HtmlSelectOneMenu cboPeriod) {
		this.cboPeriod = cboPeriod;
	}

	public HtmlInputText getTxtAmount() {
		return txtAmount;
	}

	public void setTxtAmount(HtmlInputText txtAmount) {
		this.txtAmount = txtAmount;
	}

	public HtmlInputText getTxtDurationValue() {
		return txtDurationValue;
	}

	public void setTxtDurationValue(HtmlInputText txtDurationValue) {
		this.txtDurationValue = txtDurationValue;
	}

	public HtmlSelectOneMenu getCboDurationFrequency() {
		return cboDurationFrequency;
	}

	public void setCboDurationFrequency(HtmlSelectOneMenu cboDurationFrequency) {
		this.cboDurationFrequency = cboDurationFrequency;
	}
	public void durationValueChanged()
	{
		calculateEndDate();
	}
	public void durationFrequencyChanged()
	{
		calculateEndDate();
	}
	public void startDateChanged()
	{
		calculateEndDate();
	}

	public HtmlCalendar getClndrStartDate() {
		return clndrStartDate;
	}

	public void setClndrStartDate(HtmlCalendar clndrStartDate) {
		this.clndrStartDate = clndrStartDate;
	}

	public HtmlCalendar getClndrEndDate() {
		return clndrEndDate;
	}

	public void setClndrEndDate(HtmlCalendar clndrEndDate) {
		this.clndrEndDate = clndrEndDate;
	}
	public void disburementTypeChanged()
	{
//		if(rdoPeriodic.getValue() != null && StringHelper.isNotEmpty(rdoPeriodic.getValue().toString())&& rdoPeriodic.getValue().toString().compareTo("-1") != 0)
//		{
//			if(rdoPeriodic.getValue().toString().compareTo("1") == 0)//periodic
//			{
				setControlsValuesForPeriodic();
//			}
//			else //urgent
//			{
//				setControlsValuesForUrgent();
//			}
//		}
	}
	public void setControlsValuesForPeriodic()
	{
		//Controls handling
		cboPeriod.setValue("-1");
		cboPeriod.setDisabled(false) ;
		txtAmount.setValue(null);
		txtAmount.setDisabled(false);
		txtDurationValue.setValue(null);
		txtDurationValue.setDisabled(false);
		cboDurationFrequency.setValue("-1");
		cboDurationFrequency.setDisabled(false);
		clndrStartDate.setValue(null);
		clndrStartDate.setDisabled(false);
		clndrEndDate.setValue(null);
		//Controls handling
	}
	public void setControlsValuesForUrgent()
	{
		//Controls handling
		cboPeriod.setValue("-1");
		cboPeriod.setDisabled(true) ;
		txtAmount.setValue(null);
		txtAmount.setDisabled(false);
		txtDurationValue.setValue(null);
		txtDurationValue.setDisabled(true);
		cboDurationFrequency.setValue("-1");
		cboDurationFrequency.setDisabled(true);
		clndrStartDate.setValue(null);
		clndrStartDate.setDisabled(true);
		clndrEndDate.setValue(null);
		//Controls handling	
	}

	public HtmlSelectOneMenu getCboCaringList() {
		return cboCaringList;
	}

	public void setCboCaringList(HtmlSelectOneMenu cboCaringList) {
		this.cboCaringList = cboCaringList;
	}

	public HtmlInputTextarea getTxtAreaComments() {
		return txtAreaComments;
	}

	public void setTxtAreaComments(HtmlInputTextarea txtAreaComments) {
		this.txtAreaComments = txtAreaComments;
	}

	public HtmlSelectOneMenu getCboBeneficiaryPersonId() {
		return cboBeneficiaryPersonId;
	}

	public void setCboBeneficiaryPersonId(HtmlSelectOneMenu cboBeneficiaryPersonId) {
		this.cboBeneficiaryPersonId = cboBeneficiaryPersonId;
	}

	@SuppressWarnings("unchecked")
	public HashMap<Long, String> getCaringTypeMap() 
	{
		if(viewMap.get(CARING_TYPE_MAP) != null)
			caringTypeMap = (HashMap<Long, String>) viewMap.get(CARING_TYPE_MAP);
		return caringTypeMap;
	}

	public void setCaringTypeMap(HashMap<Long, String> caringTypeMap) {
		this.caringTypeMap = caringTypeMap;
	}

	@SuppressWarnings("unchecked")
	public HashMap<Long, String> getDisListMap() 
	{
		if(viewMap.get(DISBURSEMENT_LIST_MAP) != null)
			disListMap = (HashMap<Long, String>) viewMap.get(DISBURSEMENT_LIST_MAP);
		return disListMap;
	}

	public void setDisListMap(HashMap<Long, String> disListMap) {
		this.disListMap = disListMap;
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, DomainDataView> getRecommendationStatusMap() 
	{
		if(viewMap.get(RECOMMENDATION_STATUS_DD_MAP) != null)
			recommendationStatusMap = (HashMap<String, DomainDataView>) viewMap.get(RECOMMENDATION_STATUS_DD_MAP);
		return recommendationStatusMap;
	}

	public void setRecommendationStatusMap(
			HashMap<String, DomainDataView> recommendationStatusMap) {
		this.recommendationStatusMap = recommendationStatusMap;
	}

	public boolean isViewMode()
	{
		if(viewMap.get(WebConstants.VIEW_MODE) != null)
			return (Boolean) viewMap.get(WebConstants.VIEW_MODE);
		return false;
	}
	
	public boolean isEditMode()
	{
		if(viewMap.get(WebConstants.PAGE_MODE_EDIT) != null)
			return (Boolean) viewMap.get(WebConstants.PAGE_MODE_EDIT);
		return false;
	}
	@SuppressWarnings( "unchecked" )
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	public HtmlInputTextarea getTxtRejectionReason() {
		return txtRejectionReason;
	}

	public void setTxtRejectionReason(HtmlInputTextarea txtRejectionReason) {
		this.txtRejectionReason = txtRejectionReason;
	}

} 
