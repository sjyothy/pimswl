package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.core.data.ApplicationContext;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.SocialResearchService;
import com.avanza.pims.ws.vo.CaringTypeView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.ResearchRecommendationView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageBeneficiaryRecommendationTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private List< ResearchRecommendationView> dataList;
	
	private HtmlDataTable dataTable;
	private SocialResearchService  service;
	private String tabMode;
	ResearchRecommendationView pageView;
	private Long personId;
	public FamilyVillageBeneficiaryRecommendationTabBacking() 
	{
		service = new SocialResearchService();
		dataTable        = new HtmlDataTable();
		dataList         = new ArrayList<ResearchRecommendationView>();
		tabMode          = Constant.EndowmentFile.MODE_READ_ONLY;
		pageView         = new ResearchRecommendationView();
		dataTable        =  new HtmlDataTable();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.RECOMMENDATION_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.RECOMMENDATION_TAB_SUCCESS, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			tabMode = (String) viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		if( viewMap.get( "ResearchRecommendationViewList" ) != null )
		{
			dataList = (ArrayList<ResearchRecommendationView>)viewMap.get( "ResearchRecommendationViewList" );
		}
		if( viewMap.get( "ResearchRecommendationView" ) != null )
		{
			pageView = (ResearchRecommendationView)viewMap.get( "ResearchRecommendationView" );
		}
		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, tabMode );
		}
		if( dataList != null && dataList.size()  > 0  )
		{
			viewMap.put( "ResearchRecommendationViewList", dataList );
		}
		if( pageView != null )
		{
			viewMap.put( "ResearchRecommendationView", pageView );
		}
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
	}	
	
	

	public void populateTab( PersonView personView ) throws Exception
	{
		updateValuesFromMaps();
		personId = personView.getPersonId();
		loadDataList(personId);
		setTotalRows( dataList.size() );
		setPageDataDefaults();
		updateValuesToMaps();
	}
	
	private void setPageDataDefaults()throws Exception
	{
		pageView = new ResearchRecommendationView();
		PersonView person  = new PersonView();
		person.setPersonId(personId);
		pageView.setPerson( person);
		CaringTypeView caringType = new CaringTypeView();
		caringType.setCaringTypeId(Constant.CaringType.SOCIAL_CARE);
		pageView.setCaringType(caringType);
		pageView.setCreatedBy(getLoggedInUserId());
		pageView.setCreatedOn(new Date());
		pageView.setUpdatedBy(getLoggedInUserId());
		pageView.setUpdatedOn(new Date());
		pageView.setIsDeleted(0l);
		pageView.setStatus(Constant.RecommendationStatus.CLOSED_ID);
		pageView.setDisbursementRequired("0");
		
	}

	private void loadDataList(Long personId) throws Exception {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Constant.ResearchRecommendationCriteria.PERSON_ID , personId.toString());
		dataList = service.getResearchRecommendation(map);
	}

	
	@SuppressWarnings("unchecked")
	private boolean hasErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		
		if( pageView.getComments()== null || pageView.getComments().trim().length() <=0) {
			errorMessages.add(CommonUtil.getBundleMessage("familyVillageManageBeneficiary.msg.recommendationRequired"));
			return true;
		}
		
		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onAdd()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			if(hasErrors())return;
			try
			{
				ApplicationContext.getContext().getTxnContext().beginTransaction();
				ResearchRecommendationView object =(ResearchRecommendationView)BeanUtils.cloneBean( pageView );
				service.persistResearchRecomm(object);
				loadDataList(personId);
				setPageDataDefaults(); 
				ApplicationContext.getContext().getTxnContext().commit();
			}
			catch(Exception e){ApplicationContext.getContext().getTxnContext().rollback();throw e;}
			finally{ApplicationContext.getContext().getTxnContext().release();}
			loadDataList(personId);
		}
		catch ( Exception e )
		{
			logger.LogException( "onAdd--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.RECOMMENDATION_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.RECOMMENDATION_TAB_SUCCESS, successMessages );
			}
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDelete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
			
			
//			
//			if( deleted )
//			{
//			 dataList.remove(obj);
//			}
		}
		catch ( Exception e )
		{
			logger.LogException( "onDelete--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.RECOMMENDATION_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ManageBeneficiaryFamilyVillageProcess.RECOMMENDATION_TAB_SUCCESS, successMessages );
			}
		}
		
	}
  
	
		
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}

	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public List<ResearchRecommendationView> getDataList() {
		return dataList;
	}

	public void setDataList(List<ResearchRecommendationView> dataList) {
		this.dataList = dataList;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public ResearchRecommendationView getPageView() {
		return pageView;
	}

	public void setPageView(ResearchRecommendationView pageView) {
		this.pageView = pageView;
	}

	


}
