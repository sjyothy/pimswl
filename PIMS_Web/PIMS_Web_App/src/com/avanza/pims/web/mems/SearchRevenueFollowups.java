package com.avanza.pims.web.mems;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.entity.AssetMems;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.entity.NextRevFollowup;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.utility.RevenueFollowupService;
import com.avanza.ui.util.ResourceUtil;

public class SearchRevenueFollowups extends AbstractSearchBean 
{
	private static final long serialVersionUID = 1L;
	private final String DEFAULT_SORT_FIELD = "refNum";
	private final String DATA_LIST = "DATA_LIST";
	RevenueFollowupService service = new RevenueFollowupService();

	NextRevFollowup criteria = new NextRevFollowup();
	List<NextRevFollowup> dataList = new ArrayList<NextRevFollowup>();
   public SearchRevenueFollowups()
   {
	   	Endowment end = new Endowment();
		criteria.setEndowment(end);
		AssetMems assetMems = new AssetMems ();
		criteria.setAssetMems(assetMems);
   }

	@SuppressWarnings("unchecked")
	@Override
	public void init() 
	{
		super.init();
		try 
		{
			if (!isPostBack()) 
			{
				initData();
			}
		} catch (Exception es) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void initData() throws Exception 
	{
		setRowsPerPage(WebConstants.RECORDS_PER_PAGE);
		setPageRange(WebConstants.SEARCH_RESULTS_MAX_PAGES);
		setSortField(DEFAULT_SORT_FIELD);
		setSortItemListAscending(false);
		checkDataInQueryString();
		
		
	}

	private void checkDataInQueryString() {
		checkPageMode();
	}

	@SuppressWarnings("unchecked")
	private void checkPageMode() 
	{

		if (request.getParameter(VIEW_MODE) != null) 
		{

			if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);

			}
			else if (request.getParameter(VIEW_MODE).equals(MODE_SELECT_MANY_POPUP)) 
			{
				viewMap.put(VIEW_MODE, MODE_SELECT_MANY_POPUP);

			}
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onSingleSelect() 
	{
		try 
		{
			NextRevFollowup obj = null;
			obj = (NextRevFollowup) dataTable.getRowData();
			sessionMap.put(WebConstants.SELECTED_ROW,obj);
			executeJavascript("closeWindow();");
		}
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onSingleSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public void onMultiSelect() 
	{
		try 
		{
			dataList = getDataList();
			List<NextRevFollowup> files = new ArrayList<NextRevFollowup>();
			for(NextRevFollowup obj: dataList)
			{
					files.add(obj);
			}
			
			if(files.size() <= 0)
			{
				errorMessages = new ArrayList<String>();
				errorMessages.add( CommonUtil.getBundleMessage("endowment.errMsg.noSelectMade") );
				return;
			}
			viewMap.put(WebConstants.SELECTED_ROWS, files);
			executeJavascript("closeWindow();");
		}
		catch (Exception e) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("onMultiSelect|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	public String onAdd() 
	{
		return "add";
	}
	@SuppressWarnings("unchecked")
	public String onEdit() 
	{
		try 
		{
			NextRevFollowup row = (NextRevFollowup) dataTable.getRowData();
			if (row != null) 
			{
				sessionMap.put(WebConstants.RevenueFollowup.RevenueFollowup, row);
				return "manage";
			}
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onEdit |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		return "";
	}

	public void onSearch() 
	{
		try 
		{
			loadDataList();
			pageFirst();// at last
		} 
		catch (Exception e) 
		{
			logger.LogException("onSearch |Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	public void doSearchItemList()
	{
		try {
				loadDataList();
			}
		    catch (Exception e){
		    	errorMessages = new ArrayList<String>(0);
		 		logger.LogException("doSearchItemList|Error Occured", e);
		 		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		    }
	}

	@SuppressWarnings("unchecked")
	public void loadDataList() throws Exception 
	{

		int totalRows = 0;
		totalRows = service.getTotalNumberOfRecords( criteria );
		setTotalRows(totalRows);
		doPagingComputations();

		dataList = service.search(criteria, getRowsPerPage(),getCurrentPage(),getSortField(),isSortItemListAscending());

		if (dataList== null || dataList.isEmpty() ) 
		{
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
			forPaging(0);
		}
		
		this.setDataList( dataList );
		forPaging( getTotalRows() );
	}



	@SuppressWarnings("unchecked")
	public List<NextRevFollowup> getDataList() 
	{
		if(viewMap.get(DATA_LIST) != null)
		{
			dataList = (List<NextRevFollowup>) viewMap.get(DATA_LIST);
		} 
		return dataList ;
	}

	@SuppressWarnings("unchecked")
	public void setDataList(List<NextRevFollowup> list) 
	{
		this.dataList = list;
		if(this.dataList != null )
		{
			viewMap.put(DATA_LIST, list);
		}

	}

	
	@SuppressWarnings("unchecked")
	
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javascript);
	}

	public NextRevFollowup getCriteria() {
		return criteria;
	}

	public void setCriteria(NextRevFollowup criteria) {
		this.criteria = criteria;
	}

	
}
