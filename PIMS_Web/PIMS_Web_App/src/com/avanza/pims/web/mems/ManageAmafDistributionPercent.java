package com.avanza.pims.web.mems;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.entity.AmafDistributionPercentage;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.ui.util.ResourceUtil;

public class ManageAmafDistributionPercent extends AbstractMemsBean 
{
	private static final long serialVersionUID = 1L;

	AmafDistributionPercentage pageObject = new AmafDistributionPercentage();
	

	@SuppressWarnings("unchecked")
	@Override
	public void init() 
	{
		super.init();
		try 
		{
			if (!isPostBack()) 
			{
				initData();
			}
		} catch (Exception es) 
		{
			errorMessages = new ArrayList<String>(0);
			logger.LogException("init|Error Occured", es);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	private void initData() throws Exception 
	{
		checkDataInQueryString();
		if( pageObject.getDisPercentageId() != null )
		{
			pageObject.setCreatedOn(new Date());
			pageObject.setCreatedBy(getLoggedInUserId());
		}
		pageObject.setUpdatedOn(new Date());
		pageObject.setUpdatedBy(getLoggedInUserId());
	}

	private void checkDataInQueryString() {
		if(sessionMap.get(WebConstants.SELECTED_ROW) != null)
		{
			setPageObject(  (AmafDistributionPercentage)sessionMap.remove(WebConstants.SELECTED_ROW)   ) ;  
		}
	}

	public boolean hasDoneErrors()
	{
		boolean hasErrors = false;
        if( pageObject.getDescription()  == null || pageObject.getDescription().trim().length() <= 0 )
        {
           	 errorMessages.add(
           			 		   ResourceUtil.getInstance().getProperty(
    																	   "amafDistPercentage.msg.descriptionReq"
    																	 )
    							  );
           	 hasErrors = true;
        }
		
         if( pageObject.getPercentage() == null || pageObject.getPercentage().compareTo(0d) <= 0 || 
        	 pageObject.getPercentage().compareTo(100d) > 0 
           )
         {
        	 errorMessages.add(
        			 		   ResourceUtil.getInstance().getProperty(
 																	   "amafDistPercentage.msg.percentageRequired"
 																	 )
 							  );
        	 hasErrors = true;
         }
		return hasErrors;
	}

	@SuppressWarnings("unchecked")
	public void onDone()
	{
		errorMessages   = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try 
		{
			if ( hasDoneErrors() ) return;
			if( pageObject.getDisPercentageId() == null )
			{
				pageObject.setCreatedBy( getLoggedInUserId() );
				pageObject.setCreatedOn(  new Date() );
				pageObject.setIsDelted( "0" );
				pageObject.setIsSystem( "0" );
			}
			pageObject.setUpdatedBy( getLoggedInUserId() );
			sessionMap.put( WebConstants.SELECTED_ROW,pageObject );
			executeJavascript("javaScript:closeWindow();");
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onDone|Error Occured", e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}

	@SuppressWarnings("unchecked")
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			AddResource addResource = AddResourceFactory
					.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext,
					AddResource.HEADER_BEGIN, javascript);
	}


	@SuppressWarnings("unchecked")
	public AmafDistributionPercentage getPageObject() {
		if( viewMap.get( "pageObject" ) != null )
		{
			pageObject = ( AmafDistributionPercentage )viewMap.get( "pageObject" ); 
		}
		return pageObject;
	}
	@SuppressWarnings("unchecked")
	public void setPageObject(AmafDistributionPercentage pageObject) {
		this.pageObject = pageObject;
		if( this.pageObject  != null )
		{
			viewMap.put( "pageObject",pageObject ); 
		}
	}

	
}
