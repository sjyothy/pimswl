package com.avanza.pims.web.mems.plugins;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputTextarea;

import org.apache.myfaces.custom.inputHtml.InputHtml;
import org.apache.myfaces.custom.inputHtml.InputHtmlTag;

import com.avanza.core.util.FreeMarkerConfigurator;
import com.avanza.notificationservice.event.Event;
import com.avanza.notificationservice.event.EventCatalog;
import com.avanza.notificationservice.event.MetaEvent;
import com.avanza.notificationservice.event.SubscriberEvent;
import com.avanza.notificationservice.notification.NotificationSender;
import com.avanza.notificationservice.notification.PriorityType;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.Person;
import com.avanza.pims.entity.PersonContactInfo;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.ui.util.ResourceUtil;


@SuppressWarnings( "unchecked" )

public class PayVendorNotificationPopup extends AbstractMemsBean
{
	private static final long serialVersionUID = 1L;
	
	private RequestView requestView;
	private List<DisbursementDetailsView> disbursementList; 
	private HtmlCommandButton btnSend ;
	private String txtEmailBody;
	private HtmlInputTextarea txtEmailSubject;
	org.apache.myfaces.custom.inputHtml.InputHtml  htmlTxt;
    private String vendorEmailAddress;
	
	public PayVendorNotificationPopup() 
	{	
		btnSend = new HtmlCommandButton();
		txtEmailSubject = new HtmlInputTextarea();
		txtEmailBody = "";//new InputHtmlTag();
		htmlTxt = new InputHtml();
		htmlTxt.setShowAllToolBoxes(false);
		disbursementList = new ArrayList<DisbursementDetailsView>();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException( "init --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@Override
	public void prerender()
	{
		if( vendorEmailAddress == null || vendorEmailAddress.length() <= 0  )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("payVendorNotification.msg.vendorEmailEmpty" ));
	        btnSend.setRendered(false);
		}
		else
		{
		       btnSend.setRendered(true);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if ( sessionMap.get( WebConstants.REQUEST) != null )
		{
			requestView  = (RequestView)sessionMap.remove( WebConstants.REQUEST );
		}  
		else if( viewMap.get( WebConstants.REQUEST) != null  )
		{
			requestView  = (RequestView)viewMap.get( WebConstants.REQUEST );
		}
		if( sessionMap.get("Disbursements" ) != null)
		{
			disbursementList = (ArrayList<DisbursementDetailsView>)sessionMap.remove("Disbursements");
		}
		if( !isPostBack())
		{
			loadEmailContent();
		}
		else if( viewMap.get( "vendorEmailAddress" ) != null   )
		{
			vendorEmailAddress =  viewMap.get( "vendorEmailAddress" ).toString(); 
		}
		
		if( viewMap.get("txtEmailBody" ) != null  )
		{
			txtEmailBody = viewMap.get("txtEmailBody" ).toString();
		}
		updateValuesToMaps();
	}

	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		if( requestView != null )
		{
			viewMap.put( WebConstants.REQUEST, requestView );
		}
		else if( disbursementList != null )
		{
			viewMap.put("Disbursements", disbursementList );
		}
		if( vendorEmailAddress != null && vendorEmailAddress.trim().length() >= 0 )
		{
			 viewMap.put( "vendorEmailAddress" , vendorEmailAddress);
		}
		if( txtEmailBody != null  )
		{
			viewMap.put("txtEmailBody", txtEmailBody.trim());
		}
	}	
	
	@SuppressWarnings("unchecked")
	private boolean hasErrors()
	{
		boolean hasErrors = false;
		if( vendorEmailAddress == null || vendorEmailAddress.length() <= 0  )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("payVendorNotification.msg.vendorEmailEmpty" ));
			hasErrors = true;
		}
		return hasErrors ;
	}
	
	@SuppressWarnings("unchecked")
	public void onSend()
	{
		
		errorMessages = new ArrayList<String>(0);
		try
		{
			updateValuesFromMaps();
			if ( hasErrors() ){return;}
			String value = htmlTxt.getValue().toString();
			NotificationSender.getInstance().sendEmail(
														txtEmailSubject.getValue().toString().trim(), 
														value, 
														vendorEmailAddress, 
														null, 
														"SMTP-CHANNEL",
														PriorityType.Medium
													  );
			updateValuesToMaps();
			successMessages.add(ResourceUtil.getInstance().getProperty("payVendorNotification.msg.vendorEmailSent"));
			executeJavaScript("javaScript:closeWindow();");
		}
		catch ( Exception e )
		{
			logger.LogException(   "onSend --- EXCEPTION --- ", e );
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
		
	}

	@SuppressWarnings("unchecked")
	private void loadEmailContent( )throws Exception 
	{
		
		
		Map<String, Object> dataMap = new HashMap<String, Object>(0);
		Person person = null;
		if( requestView.getRequestTypeId().compareTo( WebConstants.MemsRequestType.PAY_VENDOR_REQUEST_ID ) == 0 )
		{
		 person = EntityManager.getBroker().findById(Person.class, requestView.getContactorView().getPersonId() );
		}
		else if( requestView.getRequestTypeId().compareTo( WebConstants.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID ) == 0 )
		{
			DisbursementDetailsView ddView =  disbursementList.get(0);
			
			person = EntityManager.getBroker().findById(Person.class, ddView.getVendorId() );
		}
		vendorEmailAddress =  getEmailContactInfos(person);
		if(vendorEmailAddress == null || vendorEmailAddress.trim().length() <=  0)
		{
			throw new Exception("Email Address is not present for person id:"+person.getPersonId() );
		}
		
		setEventAttributes(dataMap, person);
 
		
		Event event = EventCatalog.getInstance().getMetaEvent( "Event.PayVendorNotification" ).createEvent();
		
		MetaEvent metaEvent = event.getMetaEvent();
		event.setValue(dataMap);
		dataMap = event.getAttributeMap();
		
		List<SubscriberEvent> subscriptions = EventCatalog.getInstance().getSubscriberEvent( metaEvent.getMetaEventId() );
		SubscriberEvent subscriberEvent = subscriptions.get(0);
		
		
		
		String templateId = subscriberEvent.getParamsCfg().getTextValue(SubscriberEvent.PARAM_TEMPLATE_ID);
		txtEmailSubject.setValue( 
									FreeMarkerConfigurator.getInstance().processTemplate(templateId + "_subject", ( Map ) dataMap)
								);
//		txtEmailBody=				FreeMarkerConfigurator.getInstance().processTemplate(templateId + "_content", ( Map ) dataMap);
		htmlTxt.setValue( 		FreeMarkerConfigurator.getInstance().processTemplate(templateId + "_content", ( Map ) dataMap) ) ; 
					
	}
	
	@SuppressWarnings("unchecked")
	private void setEventAttributes(  Map<String, Object>  dataMap,Person person ) throws Exception
	{
		dataMap.put("REQUEST_NUMBER", requestView.getRequestNumber() );
		double totalAmount = 0.0d;
		DecimalFormat dForm = new DecimalFormat("#.00");
		for ( DisbursementDetailsView obj : disbursementList )
		{
			totalAmount+= obj.getAmount();
		}
		dataMap.put("TOTAL_AMOUNT",   dForm.format( totalAmount ) );

		dataMap.put("VENDOR_NAME",   person.getFullName() );
	}
	
	@SuppressWarnings("unchecked")
	private String getEmailContactInfos(Person person)
    {
		String email="";
    	for (PersonContactInfo pci : person.getPersonContactInfos() )
		{
    		
    		if( 	
    				pci.getContactInfo() != null &&
    				pci.getContactInfo().getEmail()!=null && 
    				pci.getContactInfo().getEmail().length()>0 &&
    				email.indexOf(pci.getContactInfo().getEmail().toLowerCase() )< 0
    			
    		)
            {
    			email += pci.getContactInfo().getEmail().toLowerCase()+",";
            }

		}
    	if( email.length() >0  )
    	{
    		return email.substring(0 , email.length()-1).trim();
    	}
    	return null;
    }
	
	public HtmlCommandButton getBtnSend() {
		return btnSend;
	}

	public void setBtnSend(HtmlCommandButton btnSend) {
		this.btnSend = btnSend;
	}

	public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public String getTxtEmailBody() {
		return txtEmailBody;
	}

	public void setTxtEmailBody(String txtEmailBody) {
		this.txtEmailBody = txtEmailBody;
	}

	public HtmlInputTextarea getTxtEmailSubject() {
		return txtEmailSubject;
	}

	public void setTxtEmailSubject(HtmlInputTextarea txtEmailSubject) {
		this.txtEmailSubject = txtEmailSubject;
	}

	public List<DisbursementDetailsView> getDisbursementList() {
		return disbursementList;
	}

	public void setDisbursementList(List<DisbursementDetailsView> disbursementList) {
		this.disbursementList = disbursementList;
	}

	public String getVendorEmailAddress() {
		return vendorEmailAddress;
	}

	public void setVendorEmailAddress(String vendorEmailAddress) {
		this.vendorEmailAddress = vendorEmailAddress;
	}

	public org.apache.myfaces.custom.inputHtml.InputHtml getHtmlTxt() {
		return htmlTxt;
	}

	public void setHtmlTxt(org.apache.myfaces.custom.inputHtml.InputHtml htmlTxt) {
		this.htmlTxt = htmlTxt;
	}




}
