package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.avanza.pims.entity.AmountDetails;
import com.avanza.pims.entity.DenominationQuantityDetails;
import com.avanza.pims.entity.OpenBox;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.OpenBoxService;
import com.avanza.ui.util.ResourceUtil;

public class AddOpenBoxAmountDetails extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	
	private OpenBoxService openBoxService;
	private String pageMode;
	HttpServletRequest request ;
	AmountDetails amountDetails;
	OpenBox openBox;
	List<DenominationQuantityDetails> list = new ArrayList<DenominationQuantityDetails>();
	public AddOpenBoxAmountDetails() 
	{
		openBoxService		        = new OpenBoxService();
		pageMode                    = WebConstants.PAGE_MODE_VIEW;
		request  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
	@Override
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
			if(!isPostBack() )
			{
				amountDetails.setCreatedBy(getLoggedInUserId());
			}
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		if ( sessionMap.get(WebConstants.PAGE_MODE)!= null )
		{
			pageMode = sessionMap.remove(WebConstants.PAGE_MODE).toString();
		}
		else if ( viewMap.get( WebConstants.PAGE_MODE ) != null )
		{
			pageMode = viewMap.get(WebConstants.PAGE_MODE).toString();
		}
			viewMap.put(
						WebConstants.PAGE_MODE,WebConstants.PAGE_MODE_VIEW
			           );
		
		if(sessionMap.get(WebConstants.OpenBox.OpenBox) != null ||  request.getParameter("id") != null )
		{
			if ( sessionMap.get(WebConstants.OpenBox.OpenBox) != null  )
			{
				openBox = (OpenBox)sessionMap.remove(WebConstants.OpenBox.OpenBox) ;
			}
			else if (request.getParameter("id") != null )
			{
				openBox= openBoxService.getOpenBoxById(new Long(request.getParameter("id").toString() ) );
			}
			
			viewMap.put( WebConstants.OpenBox.OpenBox, openBox );
			if( sessionMap.get(WebConstants.OpenBox.AmountDetails)!= null )
			{
				amountDetails = (AmountDetails)sessionMap.remove(WebConstants.OpenBox.AmountDetails);
			}
			else 
			{
				if( request.getParameter("amountDetailsId") != null)
				{
					amountDetails = openBoxService.getAmountDetailForAmountDetailId(new Long(request.getParameter("amountDetailsId").toString() ) );
				}
				else
				{
					amountDetails = openBoxService.getLastAmountDetailForOpenBox(openBox);
				}
				if(getPageModeEdit())
				{
					amountDetails.setChangeReason(null);
					amountDetails.setCurrentAmount( amountDetails.getNewAmount() );
					amountDetails.setNewAmount(0d);
				}
			}
			
			 
			list.addAll( amountDetails.getDenominationQuantityDetailses() );
			Collections.sort(list,
							 new ListComparator("denomination",false)
							 );
		}
		else
		{
			if( viewMap.get(WebConstants.OpenBox.AmountDetails ) != null )
			{
				amountDetails = (AmountDetails)viewMap.get(WebConstants.OpenBox.AmountDetails ) ;
			}
			if( viewMap.get(WebConstants.OpenBox.OpenBox) != null )
			{
				openBox = (OpenBox)viewMap.get(WebConstants.OpenBox.OpenBox) ;
			}
			if( viewMap.get("list") != null)
			{
				list = (ArrayList<DenominationQuantityDetails>)viewMap.get("list") ;
			}
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( pageMode != null )
		{
		  viewMap.put( WebConstants.PAGE_MODE, pageMode);
		}
		if( amountDetails  != null )
		{
			viewMap.put(WebConstants.OpenBox.AmountDetails ,amountDetails); 
		}
		if( openBox != null )
		{
			viewMap.put(WebConstants.OpenBox.OpenBox, openBox );
		}
		if( list != null )
		{
			viewMap.put( "list", list );
		}
	}	
	
	public void populateValuesInObject() throws Exception
	{
	  amountDetails.setCreatedBy(getLoggedInUserId());
	  double amount = 0.0d;
	  for(DenominationQuantityDetails obj : list)
	  {
		  amount+=obj.getAmount();
	  }
	  amountDetails.setNewAmount( amount );
	  Set<DenominationQuantityDetails> set = new HashSet<DenominationQuantityDetails>();
	  set.addAll(list);
	  amountDetails.setDenominationQuantityDetailses(set);
	  
	}
	
	private boolean hasDoneErrors() throws Exception
	{
		boolean hasErrors = false;
		if( !getPageModeEdit() )
		{
			return false;
		}
		if( amountDetails.getChangeReason() == null || amountDetails.getChangeReason().trim().length() <= 0 )
		{
			errorMessages.add( ResourceUtil.getInstance().getProperty( "openBox.msg.reasonRequired" ) );
			hasErrors = true;
		}
		return hasErrors;
	}
    @SuppressWarnings( "unchecked" )
	public void onDone()
	{
    	errorMessages = new ArrayList<String>();
		try
		{
			updateValuesFromMaps();
			populateValuesInObject();
			if( hasDoneErrors() )
			{return;}
            sessionMap.put(WebConstants.OpenBox.AmountDetails, amountDetails);
            sessionMap.put(WebConstants.OpenBox.OpenBox, openBox);
			executeJavaScript("javaScript:closeWindowSubmit();");
		}
		catch ( Exception e )
		{
			
			logger.LogException( "onDone--- EXCEPTION --- ", e );
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
    
    public boolean getPageModeEdit()
	{
		if ( pageMode.equalsIgnoreCase( WebConstants.PAGE_MODE_EDIT ) )
			return true;
		return false;
	}
    
    public boolean getPageModeView()
	{
		if ( pageMode.equalsIgnoreCase( WebConstants.PAGE_MODE_VIEW ) )
			return true;
		return false;
	}

    public boolean getPageModeAdd()
	{
		if ( pageMode.equalsIgnoreCase( WebConstants.PAGE_MODE_ADD ) )
			return true;
		return false;
	}

	public String getPageMode() 
	{
		return pageMode;
	}

	public void setPageMode(String pageMode) 
	{
		this.pageMode = pageMode;
	}

	public AmountDetails getAmountDetails() {
		return amountDetails;
	}

	public void setAmountDetails(AmountDetails amountDetails) {
		this.amountDetails = amountDetails;
	}

	public OpenBox getOpenBox() {
		return openBox;
	}

	public void setOpenBox(OpenBox openBox) {
		this.openBox = openBox;
	}

	public List<DenominationQuantityDetails> getList() {
		return list;
	}

	public void setList(List<DenominationQuantityDetails> list) {
		this.list = list;
	}

}
