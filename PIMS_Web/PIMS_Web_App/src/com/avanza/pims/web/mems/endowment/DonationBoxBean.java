package com.avanza.pims.web.mems.endowment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlInputText;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.BoxDistributionHistory;
import com.avanza.pims.entity.BoxProgramHistory;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.DonationBoxService;
import com.avanza.pims.ws.vo.DonationBoxView;
import com.avanza.pims.ws.vo.mems.EndowmentProgramView;
import com.avanza.ui.util.ResourceUtil;


public class DonationBoxBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(DonationBoxBean.class);
	
	HtmlInputText txtOtherDesc = new HtmlInputText();
	HtmlInputText txtBoxNumber = new HtmlInputText();
	private static final String PAGE_MODE_CANCELLED= "PAGE_MODE_CANCELLED";
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_REGISTRATION_REQ = "PAGE_MODE_REGISTRATION_REQ";
	private static final String PAGE_MODE_REGISTERED = "PAGE_MODE_REGISTERED";
	
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_PROGRAM_CHANGE_HISTORY   = "programHistory";
	private static final String TAB_OPEN_BOX_HISTORY   = "opoenBoxHistory";
    private String pageTitle;
    private String pageMode;
    private HtmlDataTable dataTableDistribution = new HtmlDataTable();
    private List<BoxDistributionHistory> listDistribution = new ArrayList<BoxDistributionHistory>();
    private DonationBoxView donationBox;
    private List<BoxProgramHistory> listProgramHistory = new ArrayList<BoxProgramHistory>(); 
    private DonationBoxService boxService = new DonationBoxService();
    private String txtRemarks;
	protected HtmlTabPanel tabPanel = new HtmlTabPanel();
	public DonationBoxBean(){}
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.DonationBox.PROCEDURE_DONATION_BOX );
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.DonationBox.PROCEDURE_DONATION_BOX );
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_DONATION_BOX);
		viewMap.put("noteowner", WebConstants.DonationBox.NOTES_OWNER);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		loadAttachmentsAndComments( null );
		if ( sessionMap.get(WebConstants.SELECTED_ROW)!= null )
		{
		  getDataFromSearch();
		  
		}
		else
		{
			setDataForFirstTime();
		}
		
		updateValuesFromMap();
		getPageModeFromBoxStatus();
	}
	
	public void prerender(){
		
		if( ( getPageMode().equals(PAGE_MODE_NEW) ||getPageMode().equals(PAGE_MODE_REGISTRATION_REQ) ) 
			)
		{
			if (!donationBox.isGenerateBySystem() )
			{
				txtBoxNumber.setStyleClass( "" );
			}
		}
		else
		{
			txtBoxNumber.setReadonly(true);
			txtBoxNumber.setStyleClass( "READONLY" );
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		donationBox = new DonationBoxView();
		donationBox.setCreatedOn(new Date());
		donationBox.setCreatedBy(  getLoggedInUserId()  );
		donationBox.setUpdatedBy(  getLoggedInUserId()  );
		donationBox.setIsDeleted( Constant.DEFAULT_IS_DELETED );
		donationBox.setUpdatedOn(new Date());
		donationBox.setGenerateBySystem(true);
		txtBoxNumber.setStyleClass( "READONLY" );
		//donationBox.setStatus( String.valueOf(WebConstants.DonationBoxStatus.New_Id) );
		//donationBox.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		//requestView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
		//requestView.setRequestDate( new Date() );
		

	}
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromBoxStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		if( this.donationBox== null  || 
			this.donationBox.getBoxId()  == null ||
			this.donationBox.getStatus() == null 
		  ) 
		{ return; }
		if(getStatusNew() && this.donationBox != null  && this.donationBox.getBoxId()  != null )
		{
			setPageMode( PAGE_MODE_REGISTRATION_REQ );
		}
		else if (getStatusRegistered())
		{
			setPageMode(PAGE_MODE_VIEW);
			tabPanel.setSelectedTab(TAB_PROGRAM_CHANGE_HISTORY );
			
		}
		else if (getStatusCancelled())
		{
			setPageMode(PAGE_MODE_CANCELLED);
		}
		
			
	}
	
	public boolean getStatusNew() {
		return this.donationBox.getStatusKey() .compareTo(  WebConstants.DonationBoxStatus.New) == 0;
	}


	public boolean getStatusRegistered() {
		return this.donationBox.getStatusKey() .compareTo(  WebConstants.DonationBoxStatus.Registered) == 0;
	}
	public boolean getStatusCancelled() {
		return this.donationBox.getStatusKey() .compareTo(  WebConstants.DonationBoxStatus.Cancelled) == 0;
	}

	
	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.DonationBox.DonationBox) != null )
		{
			donationBox = ( DonationBoxView )viewMap.get( WebConstants.DonationBox.DonationBox ) ;
			donationBox.setUpdatedBy(getLoggedInUserId());
			if( viewMap.get( WebConstants.DonationBox.BoxDistribution) == null   )
			{
			 listDistribution =  DonationBoxService.getBoxHistoryByBoxId( donationBox.getBoxId() );
			 viewMap.put( WebConstants.DonationBox.BoxDistribution,listDistribution ); 
			}
		}
		if( viewMap.get( WebConstants.DonationBox.BoxDistribution) == null   )
		{
			listDistribution =  ( ArrayList<BoxDistributionHistory> )viewMap.get( WebConstants.DonationBox.BoxDistribution) ;
		}
		if( viewMap.get( WebConstants.DonationBox.BoxProgramHistory) != null )
		{
			listProgramHistory = ( ArrayList<BoxProgramHistory> )viewMap.get( WebConstants.DonationBox.BoxProgramHistory) ;
		}
		if( this.donationBox.isGenerateBySystem() )
		{
			txtBoxNumber.setStyleClass( "READONLY" );
		}
		else
		{
			txtBoxNumber.setStyleClass( "" );
		}
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( donationBox!= null )
		{
		  viewMap.put(  WebConstants.DonationBox.DonationBox, donationBox);
		}
		if( listProgramHistory  != null )
		{
			viewMap.put( WebConstants.DonationBox.BoxProgramHistory, listProgramHistory); 
		}
	}
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		donationBox = ( DonationBoxView )sessionMap.remove(WebConstants.SELECTED_ROW);
		if( this.donationBox !=null )
		{
			listProgramHistory =  boxService.getProgramHistoryByBoxId(donationBox.getBoxId());
			getDonationBoxDetails( donationBox.getBoxId() );
		  
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	private void getDonationBoxDetails( long id ) throws Exception 
	{
		
		donationBox = boxService.getDonationBoxById(id);
		updateValuesToMap();
		loadAttachmentsAndComments( id );
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(donationBox.getBoxId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
    	try
    	{
	    	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	    	  {
    		  String notesOwner = WebConstants.DonationBox.NOTES_OWNER;
	    	  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, donationBox.getBoxId());
	    	  }
	    	
    	}
    	catch (Exception exception) {
			logger.LogException("saveSystemComments|crashed ", exception);
			throw exception;
		}
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.DonationBox.NOTES_OWNER;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(donationBox.getBoxId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, donationBox.getBoxId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }

	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( donationBox != null && donationBox.getBoxId()!= null )
		 {
			 loadAttachmentsAndComments( donationBox.getBoxId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if( donationBox == null || donationBox.getBoxId()== null )return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.DonationBox.NOTES_OWNER,donationBox.getBoxId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
        if( !donationBox.isGenerateBySystem() && (donationBox.getBoxNumber()==null || donationBox.getBoxNumber().length() <= 0) )
        {
        	errorMessages.add(ResourceUtil.getInstance().getProperty("donationBox.msg.numberRequired")  );
        	hasSaveErrors = true;
        }
//        if( donationBox.getPlace() ==null || donationBox.getPlace().equals("-1"))
//        {
//        	errorMessages.add(ResourceUtil.getInstance().getProperty("donationBox.msg.placeRequired")  );
//        	hasSaveErrors = true;
//        }
//        
//        if( donationBox.getCommunity()  ==null || donationBox.getCommunity().equals("-1") )
//        {
//        	errorMessages.add(ResourceUtil.getInstance().getProperty("donationBox.msg.communityRequired")  );
//        	hasSaveErrors = true;
//        }
//        else if(  donationBox.getCommunity().equals("-2")  && 
//        		( donationBox.getOtherDesc() ==null || donationBox.getOtherDesc().length()<=0 ) 
//              )
//        {
//        	errorMessages.add(ResourceUtil.getInstance().getProperty("donationBox.msg.otherCommunityDescRequired")  );
//        	hasSaveErrors = true;
//        }
        if( donationBox.getEndowmentProgramView()==null || donationBox.getEndowmentProgramView().getEndowmentProgramId() == null )
        {
        	errorMessages.add(ResourceUtil.getInstance().getProperty("donationBox.msg.programRequired")  );
        	hasSaveErrors = true;
        }
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		hasSaveErrors = true;
    	}

		return hasSaveErrors;
		
	}

	@SuppressWarnings( "unchecked" )
	public void onBoxProgramChanged() throws Exception
	{
			boxService.changeBoxProgram( donationBox );
			ApplicationContext.getContext().getTxnContext().commit();
			getDonationBoxDetails( donationBox.getBoxId() );
			saveCommentsAttachment( "donationRequest.event.saved" );
            onProgramHistoryTab();
			successMessages.add( ResourceUtil.getInstance().getProperty("donationRequest.msg.programChanged"));
	}
	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			saveDonationRequestInTransaction(  String.valueOf(WebConstants.DonationBoxStatus.New_Id) );
			getDonationBoxDetails( donationBox.getBoxId() );
			saveCommentsAttachment( "donationRequest.event.saved" );
			successMessages.add( ResourceUtil.getInstance().getProperty("donationRequest.msg.saved"));
			updateValuesToMap();
			getPageModeFromBoxStatus();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onRegister()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			donationBox.setRegistrationDate( new Date() );
			donationBox.setRegisteredBy( getLoggedInUserId() );
			saveDonationRequestInTransaction(  String.valueOf(WebConstants.DonationBoxStatus.Registered_Id) );
			getDonationBoxDetails( donationBox.getBoxId() );
			saveCommentsAttachment( "donationRequest.event.registered" );
			successMessages.add( ResourceUtil.getInstance().getProperty("donationRequest.msg.registered"));
			
			updateValuesToMap();
			getPageModeFromBoxStatus();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	
	private void saveDonationRequestInTransaction(String status ) throws Exception 
	{
		try
		{
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistDonationBox( status );
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistDonationBox(String status) throws Exception 
	{
		if(status!=null) donationBox.setStatus(status);
		boxService.persist(	donationBox	);
	}
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromEndowmentProgramSearch()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			
			EndowmentProgramView epView = (EndowmentProgramView)sessionMap.remove(WebConstants.SELECTED_ROW);
			donationBox.setEndowmentProgramView(epView);
			if ( donationBox.getBoxId() != null && 
				 (
				   donationBox.getStatusKey().compareTo(WebConstants.DonationBoxStatus.New) != 0 ||
				   donationBox.getStatusKey().compareTo(WebConstants.DonationBoxStatus.Cancelled) != 0
				  )
				  
				)
			{
				onBoxProgramChanged();
			}
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onMessageFromEndowmentProgramSearch--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onCommunityChanged()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			if(donationBox.getCommunity().equals("-2"))
			{
				txtOtherDesc.setDisabled(false);
				txtOtherDesc.setStyleClass("");
			}
			else
			{
				donationBox.setOtherDesc("");
				txtOtherDesc.setDisabled(true);
				txtOtherDesc.setStyleClass("READONLT");
			}
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onCommunityChanged--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	@SuppressWarnings( "unchecked" )
	public void onOpenBoxHistoryTab()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onOpenBoxHistoryTab--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	@SuppressWarnings( "unchecked" )
	public void onProgramHistoryTab()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			listProgramHistory =  boxService.getProgramHistoryByBoxId(donationBox.getBoxId());
			updateValuesToMap();
			tabPanel.setSelectedTab(TAB_PROGRAM_CHANGE_HISTORY);
		}
		catch (Exception exception) 
		{
			logger.LogException( "onProgramHistoryTab--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("donationBox.heading.title"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowProgramHistory()
	{
		if( listProgramHistory != null && listProgramHistory.size() > 0 ) 
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_NEW)  || getPageMode().equals( PAGE_MODE_REGISTRATION_REQ ) )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	

	@SuppressWarnings( "unchecked" )
	public Boolean getShowRegistrationButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && 
			  getPageMode().equals( PAGE_MODE_REGISTRATION_REQ) 
		  )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}

	public DonationBoxView getDonationBox() {
		return donationBox;
	}

	public void setDonationBox(DonationBoxView donationBox) {
		this.donationBox = donationBox;
	}

	public HtmlInputText getTxtOtherDesc() {
		return txtOtherDesc;
	}

	public void setTxtOtherDesc(HtmlInputText txtOtherDesc) {
		this.txtOtherDesc = txtOtherDesc;
	}

	public List<BoxProgramHistory> getListProgramHistory() {
		return listProgramHistory;
	}

	public void setListProgramHistory(List<BoxProgramHistory> listProgramHistory) {
		this.listProgramHistory = listProgramHistory;
	}

	public HtmlInputText getTxtBoxNumber() {
		return txtBoxNumber;
	}

	public void setTxtBoxNumber(HtmlInputText txtBoxNumber) {
		this.txtBoxNumber = txtBoxNumber;
	}

	public HtmlDataTable getDataTableDistribution() {
		return dataTableDistribution;
	}

	public void setDataTableDistribution(HtmlDataTable dataTableDistribution) {
		this.dataTableDistribution = dataTableDistribution;
	}

	public List<BoxDistributionHistory> getListDistribution() {
		return listDistribution;
	}

	public void setListDistribution(List<BoxDistributionHistory> listDistribution) {
		this.listDistribution = listDistribution;
	}


}