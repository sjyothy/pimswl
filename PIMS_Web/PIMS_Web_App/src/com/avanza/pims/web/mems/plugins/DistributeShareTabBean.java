package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.entity.AssetMems;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.BlockingAmountService;
import com.avanza.pims.ws.mems.CollectionProcedureService;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.PersonalAccountTrxService;
import com.avanza.pims.ws.vo.OpenFileDistribute;
import com.avanza.pims.ws.vo.mems.InheritedAssetView;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings("serial")
public class DistributeShareTabBean extends AbstractMemsBean 
{
	
	List<OpenFileDistribute> distributionList = new ArrayList<OpenFileDistribute>( 0 );
	List<SelectItem> iaList = new ArrayList<SelectItem>();
	InheritanceFileService fileService = new InheritanceFileService();
	private HtmlDataTable dataTable;
	private final String IAS_LIST= "iasList";
	private String selectOneAssetId;
	private Long fileId;
	public void init() 
	{
		try
		{
			super.init();
		
		}
		catch( Exception e )
		{
			
			logger.LogException(  "init --- CRASHED --- ", e );
		}
	}
	
	
	public void loadTabData( Long fileId ) throws Exception
	{
		if( !hasLoaded() )
		{
		  this.setFileId(  fileId );
		  loadDistributedList();
		  loadInheritedAssets();
		  this.sethasLoaded();
		}
		
		
	}
	private void loadDistributedList() throws Exception
	{
		this.setDistributionList( fileService.getUndistributedPaymentsAgainstFile(  this.getFileId() ) );
	}
	private boolean hasOpenDistributePopUpError( OpenFileDistribute row  )throws Exception
	{
		boolean hasError      =  false;
		if( row.getCostCenter() == null  )
		{
			
			errorMessages.add( CommonUtil.getBundleMessage(  "disburseFinance.msg.associateCostCenter" ) );
			return true;
		}
		
		Double fileBalance    = PersonalAccountTrxService.getBalanceForCostCenter(row.getCostCenter());
		if( row.getOldCostCenter() != null && row.getOldCostCenter().trim().length() > 0 )
		{
			fileBalance += PersonalAccountTrxService.getBalanceForCostCenter(row.getOldCostCenter());
		}
		if( row.getShowDistributeIcon() != null && row.getShowDistributeIcon().equals("1") )
		{
			Double  blockingAmnt =  BlockingAmountService.getBlockingAmount(row.getFileOwnerId(), null, row.getInheritanceFileId(),null,null );
			int errorCode = validateBalance(fileBalance, blockingAmnt, 0.00d, row.getAmount(), row.getFileOwnerId(), null,row.getInheritanceFileId(),WebConstants.MemsRequestType.COLLECTION_DISBURSEMENT );
			hasError  = errorCode > 0 ;
		}
		boolean hasARPostedToGRP = hasPaymentARPostedToGRP(row.getCollectionTrxId() );
		if(!hasARPostedToGRP)
		{
			hasError = true;
		}
		return hasError;
	}
	@SuppressWarnings( "unchecked" )
	public void onOpenDistributePopUp()
	{
		try
		{
			errorMessages = new ArrayList<String>(0);
			OpenFileDistribute row = ( OpenFileDistribute )this.getDataTable().getRowData();
			if( hasOpenDistributePopUpError( row ) )return;
			sessionMap.put( WebConstants.OpenFileDistributePopUpKeys.FILE_ID, this.getFileId() ) ; 
			sessionMap.put( WebConstants.OpenFileDistributePopUpKeys.OBJECT, row ) ;
			executeJavascript( "openDistributePopUp();");
		}
		catch( Exception e )
		{
			
			logger.LogException(   "onOpenDistributePopUp --- CRASHED --- ", e );
            errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR, errorMessages);
		}
		finally
		{
		  if( errorMessages != null && errorMessages.size() > 0 )
		  {
		   viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR, errorMessages);
		  }

		}
	}
	@SuppressWarnings( "unchecked" )
	private void loadInheritedAssets() throws Exception
	{
	  	List< InheritedAssetView> list = fileService.getInheritedAssetsByFileId(  this.getFileId() );
		List<SelectItem> iaList = new ArrayList<SelectItem>();
	  	for (InheritedAssetView inheritedAssetView : list) 
	  	{
	  		if
	  		  ( 
	  				inheritedAssetView.getIsDeleted().compareTo( 1L )  == 0 ||
	  				(
		  				inheritedAssetView.getStatus() != null && 
		  				inheritedAssetView.getStatus().compareTo(WebConstants.InheritedAssetStatus.INACTIVE_ID )==0
	  				)
	  		  )
	  		{
	  			continue;
	  		}
	  		iaList.add(  
	  					new SelectItem (
	  									inheritedAssetView.getAssetMemsView().getAssetId().toString(),
	  									inheritedAssetView.getAssetMemsView().getAssetNameAr() 
	  								    )
	  				  );
	  		
		}
	  	Collections.sort(iaList, ListComparator.LIST_COMPARE);
	  	this.setIaList(iaList);
	}
	private boolean hasOncChangeAssetError() throws Exception
	{
		if ( selectOneAssetId == null || selectOneAssetId.equals( "-1" ) )
		{
			 errorMessages.add(ResourceUtil.getInstance().getProperty("openFileDistribute.msg.selectAsset"));
	  		 return true;
		
		}
		boolean hasSelected = false;
		for (OpenFileDistribute item : distributionList) 
		{
		   if ( item.getSelected()  == null || !item.getSelected() ) continue;
		   hasSelected = true;
		   break;
		}
		if( !hasSelected )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("commons.atleastoneselected"));
	  		return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public void onChangeAsset()
	{
		errorMessages = new ArrayList<String>(0);
		try
		{
			
			if( hasOncChangeAssetError() ) return;
			distributionList = getDistributionList();
			String updatedBy = getLoggedInUserId();
			Long assetId = new Long (selectOneAssetId );
			for (OpenFileDistribute item : distributionList) 
			{
				
			   if ( item.getSelected()  == null || !item.getSelected() ) continue;
			   boolean isChangeAssetAllowed =  item.getRequestId() == null ||
											   //Related to Colletion request
											   (
												   item.getRequestId() != null && 
												   item.getRequestTypeId().compareTo(WebConstants.MemsRequestType.COLLECTION_DISBURSEMENT ) == 0
											   )||
											   //Related to Lease contract and its sub distribution, in that case the asset id will be null
											   (
													item.getRequestTypeId().compareTo(WebConstants.MemsRequestType.COLLECTION_DISBURSEMENT ) != 0 &&
													item.getAssetId()== null
													
												);
			   if( isChangeAssetAllowed )
			   {
				   CollectionProcedureService.changeCollectionAsset(item, assetId,updatedBy );
				   AssetMems assetMems = EntityManager.getBroker().findById(AssetMems.class, assetId);
				   if(item.getRequestId()!= null &&  item.getAssetId() == null  )
				   {
					   CommonUtil.saveSystemComments(
							   							WebConstants.REQUEST, 
							   							"collectionProcedure.msg.assetAddedAfterCollection", 
							   							item.getRequestId() , 
							   							assetMems.getAssetNameAr(),
							   							item.getTrxNum()
							   						);   
				   }
				   else if( item.getRequestId() != null )  
				   {
					   CommonUtil.saveSystemComments(
							   							WebConstants.REQUEST, 
							   							"collectionProcedure.msg.assetReplacedAfterCollection", 
							   							item.getRequestId() ,
							   							item.getAssetNameAr(),
							   							item.getTrxNum(),
							   							assetMems.getAssetNameAr()
							   						);   
				   }
					   
				   
			   }
			}
			loadDistributedList();	
		}
		catch( Exception e )
		{
			
			logger.LogException(   "onChangeAsset --- CRASHED --- ", e );
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
		  if( errorMessages != null && errorMessages.size() > 0 )
		  {
		   viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR, errorMessages);
		  }

		}
	}

	@SuppressWarnings( "unchecked" )
	public void openDistributedCollectionsPopUp()
	{
		try
		{
			sessionMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_ID, this.getFileId() ) ;
			executeJavascript( "openDistributedCollectionsPopUp();");
		}
		catch( Exception e )
		{
			
			logger.LogException(   "openDistributedCollectionsPopUp--- CRASHED --- ", e );
            errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR, errorMessages);
		}
		finally
		  {
			  if( errorMessages != null && errorMessages.size() > 0 )
			  {
			   viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR, errorMessages);
			  }
			  else if( successMessages != null && successMessages .size() > 0 )
			  {
			   viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_SUCC, successMessages );
			  }
		  }
		
	}
	@SuppressWarnings( "unchecked" )
	public void openSplitPopUp()
	{
		try
		{
			OpenFileDistribute row = ( OpenFileDistribute )this.getDataTable().getRowData();
			sessionMap.put( WebConstants.OpenFileDistributePopUpKeys.OBJECT, row ) ;
			executeJavascript( "openSplitPopUp();");
		}
		catch( Exception e )
		{
			
			logger.LogException(   "openSplitPopUp--- CRASHED --- ", e );
            errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR, errorMessages);
		}
		finally
		  {
			  if( errorMessages != null && errorMessages.size() > 0 )
			  {
			   viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR, errorMessages);
			  }
			  else if( successMessages != null && successMessages .size() > 0 )
			  {
			   viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_SUCC, successMessages );
			  }
		  }
		
	}
	@SuppressWarnings( "unchecked" )
	public void onMessageFromDistributePopUp()
	{
		try
		{
			successMessages = new ArrayList<String>();
			OpenFileDistribute fromPopup = ( OpenFileDistribute ) sessionMap.get( WebConstants.OpenFileDistributePopUpKeys.OBJECT );
			sessionMap.remove( WebConstants.OpenFileDistributePopUpKeys.OBJECT );
			List<OpenFileDistribute > list =  this.getDistributionList();
            int index = 0 ;
			for (OpenFileDistribute obj: list ) {
				
				if ( obj.getMyHashCode() == fromPopup.getMyHashCode() )
				{
					list.remove( index );
					list.add( index, fromPopup );
					break;
				}
				index++;
			}
			this.setDistributionList( list );
			String msg = java.text.MessageFormat.format(  
														 ResourceUtil.getInstance().getProperty( "file.distribute.distributionInfoAdd" ),
														 fromPopup.getRefNum()
														);
			
			successMessages.add( msg );
		}
		catch( Exception e )
		{
			
			logger.LogException(  "onMessageFromDistributePopUp --- CRASHED --- ", e );
            errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    		viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR, errorMessages);
		}
		finally
		  {
			  if( errorMessages != null && errorMessages.size() > 0 )
			  {
			   viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR, errorMessages);
			  }
			  else if( successMessages != null && successMessages .size() > 0 )
			  {
			   viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_SUCC, successMessages );
			  }
		  }

	}
	
	@SuppressWarnings( "unchecked" )
	private boolean hasError(OpenFileDistribute row)throws Exception
	{
		if ( row.getDistributePopUpView() == null ||  row.getDistributePopUpView().size() <= 0 )
		{
	  		 errorMessages.add(ResourceUtil.getInstance().getProperty("errMessage.plzDisributeEachAmount"));
	  		 return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
    public void onDistribute()
	{
	  CollectionProcedureService cps = new CollectionProcedureService();
	  errorMessages = new ArrayList<String>(0);
	  try
	  {
		  OpenFileDistribute row = ( OpenFileDistribute )this.getDataTable().getRowData();
		  if( hasError( row ) )return; 
		  
		  List<OpenFileDistribute> list = this.getDistributionList();
		  row.setInheritanceFileId( Long.valueOf( viewMap.get(WebConstants.InheritanceFile.FILE_ID).toString() ) );
		  
		  cps.distribute( row, CommonUtil.getLoggedInUser() );
		  
		  
		  for (OpenFileDistribute openFileDistribute : list) 
		  {
			  if( openFileDistribute.getMyHashCode() == row.getMyHashCode() )
			  {
//				  openFileDistribute.setShowAddBenefIcon(   "0" );
				  openFileDistribute.setShowDistributeIcon( "0" );
			  }
			  //TODO:Check If the beneficiary is Adult If so then add in the Adult list
		  }
		  //TODO:If Adult List is not zero then initiate the receive mature request.
		  successMessages.add(ResourceUtil.getInstance().getProperty("successMsg.distributionDone"));
	  }
	  catch( Exception e )
	  {
		  logger.LogException( "onDistribute| --- CRASHED --- ", e );
          
  		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	  
	  }  
	  finally
	  {
		  cps = null;
		  if( errorMessages != null && errorMessages.size() > 0 )
		  {
		   viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR, errorMessages);
		  }
		  else if( successMessages != null && successMessages .size() > 0 )
		  {
		   viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_SUCC, successMessages );
		  }
	  }
		
	}
	
	@SuppressWarnings( "unchecked" )
    public void onMessageFromSplitPopUp()
	{
	  errorMessages = new ArrayList<String>(0);
	  successMessages = new ArrayList<String>();
	  try
	  {
		  loadDistributedList();
		  successMessages.add(ResourceUtil.getInstance().getProperty("collectionProcedure.msg.splitDone"));
	  }
	  catch( Exception e )
	  {
		  logger.LogException( "onMessageFromSplitPopUp| --- CRASHED --- ", e );
  		  errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
	  }  
	  finally
	  {
		  if( errorMessages != null && errorMessages.size() > 0 )
		  {
		   viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_ERR, errorMessages);
		  }
		  else if( successMessages != null && successMessages .size() > 0 )
		  {
		   viewMap.put(WebConstants.InherFileErrMsg.DISTRIBUTE_SAHRE_SUCC, successMessages );
		  }
	  }
		
	}
	
	private void executeJavascript(String javascript)throws Exception 
	{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
	}
	@SuppressWarnings( "unchecked" )
	public Long getFileId() {
		if( viewMap.get( "DistributeFileId" )!= null )
			fileId = new Long( viewMap.get( "DistributeFileId" ).toString() ) ;
		return fileId;
	}

	@SuppressWarnings( "unchecked" )
	public void setFileId(Long fileId) {
		this.fileId = fileId;
		if( this.fileId != null )
			viewMap.put("DistributeFileId",fileId  );
	}
	@SuppressWarnings( "unchecked" )
	public List<OpenFileDistribute> getDistributionList() {
		if ( viewMap.get( "distributionList" )!= null  )
			distributionList =  (ArrayList<OpenFileDistribute>) viewMap.get( "distributionList" );
		return distributionList;
	}
	@SuppressWarnings( "unchecked" )
	public void setDistributionList(List<OpenFileDistribute> distributionList) {
		this.distributionList = distributionList;
		if ( this.distributionList !=null )
			viewMap.put( "distributionList",this.distributionList );
	
	
	}

	@SuppressWarnings( "unchecked" )
	private boolean hasLoaded()
	{
		 if( viewMap.get( "hasLoaded") != null && viewMap.get( "hasLoaded" ).toString().equals("1") )
		  return true;
		 else 
			 return false;
	}
	@SuppressWarnings( "unchecked" )
	private void sethasLoaded()
	{
		 viewMap.put( "hasLoaded", "1") ;
		 
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}


	public String getSelectOneAssetId() {
		return selectOneAssetId;
	}


	public void setSelectOneAssetId(String selectOneAssetId) {
		this.selectOneAssetId = selectOneAssetId;
	}


	@SuppressWarnings( "unchecked" )
	public List<SelectItem> getIaList() {
		if( viewMap.get( IAS_LIST ) != null )
		{
			iaList =  (ArrayList<SelectItem>)viewMap.get( IAS_LIST ) ;
		}
		return iaList;
	}

	@SuppressWarnings( "unchecked" )
	public void setIaList(List<SelectItem> iaList) {
		this.iaList = iaList;
		viewMap.put( IAS_LIST,this.iaList );
	}
}
