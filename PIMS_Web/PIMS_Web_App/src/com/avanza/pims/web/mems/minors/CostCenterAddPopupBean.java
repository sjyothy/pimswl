package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.entity.CostCenterIdentifier;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.CostCenterIdentifierService;
/**
 * @author Kamran Ahmed
 */
public class CostCenterAddPopupBean extends AbstractMemsBean {
	private static final long serialVersionUID = 1L;
	
	private boolean isAddMode;
	private List<CostCenterIdentifier> identifiers;
	private HtmlSelectOneMenu cboOwnershipTypeId;
	private HtmlInputText txtIdentifierLetter;
	private HtmlDataTable tblcostCenter;
	private CostCenterIdentifier costCenterPojo;
	private CostCenterIdentifierService ccService;
	
	private static class Keys{
		public static String IDENTIFIERS="IDENTIFIERS";
		public static String SELECTED_COST_CENTER="SELECTED_COST_CENTER";
		public static String PAGE_MODE="PAGE_MODE";
		public static String PAGE_MODE_ADD="PAGE_MODE_ADD";
		public static String PAGE_MODE_UPDATE="PAGE_MODE_UPDATE";
	}
	public CostCenterAddPopupBean() {
		identifiers = new ArrayList<CostCenterIdentifier>();
		cboOwnershipTypeId =  new HtmlSelectOneMenu();
		txtIdentifierLetter = new HtmlInputText();
		costCenterPojo = new CostCenterIdentifier();
		ccService = new CostCenterIdentifierService();
	}

	public void init()
	{
		super.init();
		try
		{
			if (!isPostBack()) 
			{
				updateValuesFromMap();
			}
		}
		catch (Exception e) {
		logger.LogException("init crashed", e);
		errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	@SuppressWarnings("unchecked")
	private void updateValuesFromMap() {
		if(sessionMap.get(Keys.SELECTED_COST_CENTER) != null){
			costCenterPojo = (CostCenterIdentifier) sessionMap.get(Keys.SELECTED_COST_CENTER);
			sessionMap.remove(Keys.SELECTED_COST_CENTER);
			viewMap.put(Keys.SELECTED_COST_CENTER, costCenterPojo);
			txtIdentifierLetter.setValue(costCenterPojo.getIdentifierLetter().toString());
			cboOwnershipTypeId.setValue(costCenterPojo.getOwnershipTypeDD().getDomainDataId().toString());
			cboOwnershipTypeId.setDisabled(true);
		}
		
		if(sessionMap.get(Keys.PAGE_MODE) != null){
			String pageMode = sessionMap.get(Keys.PAGE_MODE).toString();
			sessionMap.remove(Keys.PAGE_MODE);
			if(pageMode.compareTo(Keys.PAGE_MODE_ADD) == 0)
				viewMap.put(Keys.PAGE_MODE, Keys.PAGE_MODE_ADD);
			else
				viewMap.put(Keys.PAGE_MODE, Keys.PAGE_MODE_UPDATE);
		}
		
	}

	public void onAdd(){
		final String  methodName="onAdd()";
		logger.logInfo("Starting "+methodName);
		try{
			if(isValidated()){
				
				identifiers = ccService.getAllCostIdentifiers();
				//if there are records already present in table
				if(identifiers != null && identifiers.size() > 0){
					
					if(isAddMode()){
						if(isIdentifierAlreadyExist() ||  isOwnershiptTypeAlreadyExist()){
							errorMessages.add(CommonUtil.getBundleMessage("errMsg.pleaseCorrrectErr"));
						}
						else{
							persistCostCenterId();
						}
					}
					else{
						if(isIdentifierAlreadyExist()){
							errorMessages.add(CommonUtil.getBundleMessage("errMsg.pleaseCorrrectErr"));
						}
						else{
							persistCostCenterId();
						}
					}
				}
				//if there is no record, it is first record to add
				else{
					costCenterPojo = getFilledCostCenter();
					ccService.persistCostCenterIdentifier(costCenterPojo);
					successMessages.add(CommonUtil.getBundleMessage("successMsg.costCenterAdded"));
				}
			}
			else{
				errorMessages.add(CommonUtil.getBundleMessage("payments.errMsg.provideMandatoryField"));
			}
		}
		catch (Exception e) {
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			logger.logError(methodName+" crashed");
			logger.LogException(methodName+" crashed...", e);
		}
		logger.logInfo("Ending "+methodName);
	}
	private void persistCostCenterId() throws Exception{
		costCenterPojo = getFilledCostCenter();
		ccService.persistCostCenterIdentifier(costCenterPojo);
		if(isAddMode())
			successMessages.add(CommonUtil.getBundleMessage("successMsg.costCenterAdded"));
		else
			successMessages.add(CommonUtil.getBundleMessage("successMsg.costCenterUpdated"));
	}

	private CostCenterIdentifier getFilledCostCenter() {
		CostCenterIdentifier costCenter = new CostCenterIdentifier();
		final String  methodName="getFilledCostCenter()";
		logger.logInfo("Starting "+methodName);
		try{
			if(StringHelper.isNotEmpty(txtIdentifierLetter.getValue().toString())){
				costCenter.setIdentifierLetter(txtIdentifierLetter.getValue().toString());
			}
			
			if(cboOwnershipTypeId.getValue().toString().compareTo("-1") != 0){
				costCenter.setOwnershiptTypeId(Long.valueOf(cboOwnershipTypeId.getValue().toString()));
				DomainData ownershiptTypeDD = new DomainData();
				ownershiptTypeDD.setDomainDataId(Long.valueOf(cboOwnershipTypeId.getValue().toString()));
				costCenter.setOwnershipTypeDD(ownershiptTypeDD);
			}
			if(!isAddMode() ){
				costCenter.setCostCenterIdentifierId(getCostCenterPojo().getCostCenterIdentifierId());
			}
			else
				costCenter.setCostCenterIdentifierId(null);
		}
		catch (Exception e) {
			logger.logError(methodName+" crashed");
		}
		logger.logInfo("Ending "+methodName);
		return costCenter;
	}

	private boolean isOwnershiptTypeAlreadyExist() {
		boolean isExist = false;
		for(CostCenterIdentifier costCenter :  identifiers){
			if(costCenter.getOwnershipTypeDD().getDomainDataId().toString().compareTo(cboOwnershipTypeId.getValue().toString()) == 0){
				isExist = true;
				errorMessages.add(CommonUtil.getBundleMessage("errMsg.ownershipTypeAlreadyPresent"));
				break;
			}
		}
		return isExist;
	}
	private boolean isIdentifierAlreadyExist() {
		boolean isExist = false;
		for(CostCenterIdentifier costCenter :  identifiers){
			if(costCenter.getIdentifierLetter().toString().compareTo(txtIdentifierLetter.getValue().toString()) == 0){
				isExist = true;
				errorMessages.add(CommonUtil.getBundleMessage("errMsg.identifierAlreadyPresent"));
				break;
			}
		}
		return isExist;
	}


//	private boolean isCriteriaValidated() {
//		boolean isValidated = true;
//		if(txtIdentifierLetter.getValue() == null && (cboOwnershipTypeId.getValue() == null || cboOwnershipTypeId.getValue().toString().compareTo("-1") == 0)){
//			errorMessages.add(CommonUtil.getBundleMessage("errMsg.criteriaSpecify"));
//			isValidated = false;
//		}
//		return isValidated;
//	}

	public void prerender() {
		super.prerender();
	}

	public HtmlSelectOneMenu getCboOwnershipTypeId() {
		return cboOwnershipTypeId;
	}

	public void setCboOwnershipTypeId(HtmlSelectOneMenu cboOwnershipTypeId) {
		this.cboOwnershipTypeId = cboOwnershipTypeId;
	}

	public HtmlInputText getTxtIdentifierLetter() {
		return txtIdentifierLetter;
	}

	public void setTxtIdentifierLetter(HtmlInputText txtIdentifierLetter) {
		this.txtIdentifierLetter = txtIdentifierLetter;
	}

	public HtmlDataTable getTblcostCenter() {
		return tblcostCenter;
	}

	public void setTblcostCenter(HtmlDataTable tblcostCenter) {
		this.tblcostCenter = tblcostCenter;
	}

	public CostCenterIdentifier getCostCenterPojo() {
		if(viewMap.get(Keys.SELECTED_COST_CENTER) != null){
			costCenterPojo = (CostCenterIdentifier) viewMap.get(Keys.SELECTED_COST_CENTER); 
		}
		return costCenterPojo;
	}

	public void setCostCenterPojo(CostCenterIdentifier costCenterPojo) {
		this.costCenterPojo = costCenterPojo;
	}

	public boolean isAddMode() {
		if(viewMap.get(Keys.PAGE_MODE) != null && viewMap.get(Keys.PAGE_MODE).toString().compareTo(Keys.PAGE_MODE_ADD) == 0)
			isAddMode = true;
		else
			isAddMode = false;
		return isAddMode;
	}

	public void setAddMode(boolean isAddMode) {
		this.isAddMode = isAddMode;
	}
	private boolean isValidated() {
		boolean isValidated = true;
		if((txtIdentifierLetter.getValue() == null || StringHelper.isEmpty(txtIdentifierLetter.getValue().toString()))|| (cboOwnershipTypeId.getValue() == null || cboOwnershipTypeId.getValue().toString().compareTo("-1") == 0)){
			isValidated = false;
		}
		return isValidated;
	}
	private boolean isUpdateMode(){
		return getCostCenterPojo() != null && getCostCenterPojo().getCostCenterIdentifierId() != null ?true: false;
	}
}
