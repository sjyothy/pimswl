package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlGraphicImage;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.entity.AssetMems;
import com.avanza.pims.entity.NextRevFollowup;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.AssetMemsService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.utility.RevenueFollowupService;
import com.avanza.pims.ws.vo.AssetsGridView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;

public class ManageAssetBean extends AbstractMemsBean {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private transient Logger logger=Logger.getLogger(ManageAssetBean.class);
	private String hdnApplicantId;
	private HtmlInputText txtFileNumber;
	private HtmlInputText txtFileStatus;
	private HtmlSelectOneMenu cboFileType;
	private HtmlCalendar clndrCreatedOn;
	private String VIEW_MODE = "pageMode";
	private String MODE_SELECT_ONE_POPUP = "MODE_SELECT_ONE_POPUP";
	private String INH_ASSET_MEMS_ROW = "INH_ASSET_MEMS_ROW";
	private String hdnManagerId;
	private HtmlInputText txtManagerName = new HtmlInputText();
	private boolean isShowSearchManagerImage;
	private HtmlSelectBooleanCheckbox chkIsManagerAmaf = new HtmlSelectBooleanCheckbox();
	private HtmlGraphicImage searchImage = new HtmlGraphicImage();
	List<NextRevFollowup> dataListNextRevFollowup = new ArrayList<NextRevFollowup>();
	public ManageAssetBean() {
		txtFileNumber = new HtmlInputText();
		txtFileStatus = new HtmlInputText();
		cboFileType = new HtmlSelectOneMenu();
		clndrCreatedOn = new HtmlCalendar();
	}

	@SuppressWarnings("unchecked")
	public void init() {
		super.init();
		if (!isPostBack()) {
			
			viewMap.put("canAddAttachment", true); 
		       viewMap.put("canAddNote", true);
			viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.Attachment.EXTERNAL_ID_REQUEST);
			
			
			
			HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
			
			 if (request.getParameter(VIEW_MODE)!=null )
	    	 {
	    		if(request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
	    		 { 	      	    		
      	    		viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);
      	    	}
	    		
	    		if(request.getParameter(WebConstants.ASSET_ID)!=null) 
	    		 { 	      	    		
	    			Long assetId =Long.valueOf(request.getParameter(WebConstants.ASSET_ID).toString());
					viewMap.put(WebConstants.ASSET_ID,assetId);
     	    	}
	    		
	          
	    	 }
			 
			 if(sessionMap.containsKey("assetGridRow"))
			 {
				 AssetsGridView assetRow = (AssetsGridView)sessionMap.get("assetGridRow");
				 sessionMap.remove("assetGridRow");
				 viewMap.put(WebConstants.ASSET_ID, assetRow.getAssetId());
			 }
			
			
		}
	}


	public String getHdnApplicantId() {
		return hdnApplicantId;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onRevenueFollowupTab()
	{
		try	
		{
			RevenueFollowupService service = new RevenueFollowupService();
			Long assetId = Long.valueOf( viewMap.get(WebConstants.ASSET_ID ).toString() );
			NextRevFollowup criteria = new NextRevFollowup();
			AssetMems assetMems = new AssetMems ();
			assetMems.setAssetId(assetId);
			criteria.setAssetMems(assetMems);
			dataListNextRevFollowup = service.search(criteria, null,null,null,null);
		}
		catch(Exception ex)
		{
			logger.LogException("onRevenueFollowupTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments(Long requestId) {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		//viewMap.put(WebConstants.Attachment.PROCEDURE_KEY,WebConstants.PROCEDURE_TYPE_NEW_LEASE_CONTRACT);
		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		String externalId = WebConstants.Attachment.EXTERNAL_ID_REQUEST;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.REQUEST);

		if (requestId != null) {
			String entityId = requestId.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}

	public void setHdnApplicantId(String hdnApplicantId) {
		this.hdnApplicantId = hdnApplicantId;
	}
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	String methodName="saveComments";
    	try{
	    	logger.logInfo(methodName + "started...");
	    	String notesOwner = WebConstants.REQUEST;
	    	NotesController.saveNotes(notesOwner, referenceId);
	    	success = true;
	    	logger.logInfo(methodName + "completed successfully!!!");
    	}
    	catch (Throwable throwable) {
			logger.LogException(methodName + " crashed ", throwable);
		}
    	return success;
    }
	public Boolean saveAttachments(String referenceId)
    {
		Boolean success = false;
    	try{
	    	logger.logInfo("saveAtttachments started...");
	    	if(referenceId!=null)
	    	{
		    	success = CommonUtil.updateDocuments();
	    	}
	    	logger.logInfo("saveAtttachments completed successfully!!!");
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	
	public boolean getIsViewModePopUp() {
		boolean returnVal = false;

		if (viewMap.containsKey(VIEW_MODE)
				&& viewMap.get(VIEW_MODE) != null) {
			
				returnVal = true;
		}

		else {
			returnVal = false;
		}

		return returnVal;

	}

	@SuppressWarnings("unchecked")
	private void populateApplicantDetails(PersonView pv) {
		if (pv.getPersonId() != null)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,
					pv.getPersonId());
		if (pv.getPersonFullName() != null
				&& pv.getPersonFullName().trim().length() > 0)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
					pv.getPersonFullName());
		else if (pv.getCompanyName() != null
				&& pv.getCompanyName().trim().length() > 0)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
					pv.getCompanyName());
		String personType = getTenantType(pv);
		if (personType != null)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,
					personType);
		if (pv.getCellNumber() != null)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,
					pv.getCellNumber());
		if (pv.getPersonId() != null) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_ID, pv
					.getPersonId());
			hdnApplicantId = pv.getPersonId().toString();
		}
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE,
				new Date());

	}

	@SuppressWarnings("unchecked")
	@Override
	public void prerender() {
		super.prerender();
		getPersonInformation();
		if(viewMap.get("ERROR_MESSAGES") != null){
			List<String> errMsg =(List<String>) viewMap.get("ERROR_MESSAGES");
			errorMessages.addAll(errMsg);
			viewMap.remove("ERROR_MESSAGES");
		}
		if(viewMap.get("SUCCESS_MESSAGES") != null){
			List<String> errMsg =(List<String>) viewMap.get("SUCCESS_MESSAGES");
			successMessages.addAll(errMsg);
			viewMap.remove("SUCCESS_MESSAGES");
		}
	}
	
	@SuppressWarnings("unchecked")
	private String getTenantType(PersonView tenantView) {

		String method = "getTenantType";
		logger.logDebug(method + "|" + "Start...");
		DomainDataView ddv;
		if (tenantView != null && tenantView.getPersonId() != null
				&& tenantView.getIsCompany() != null) {
			if (tenantView.getIsCompany().compareTo(new Long(1)) == 0)
				ddv = (DomainDataView) viewMap
						.get(WebConstants.TENANT_TYPE_COMPANY);
			else
				ddv = (DomainDataView) viewMap
						.get(WebConstants.TENANT_TYPE_INDIVIDUAL);
			if (CommonUtil.getIsArabicLocale())
				return ddv.getDataDescAr();
			else if (CommonUtil.getIsEnglishLocale())
				return ddv.getDataDescEn();
		}
		logger.logDebug(method + "|" + "Finish...");
		return "";

	}

	public HtmlInputText getTxtFileNumber() {
		return txtFileNumber;
	}

	public void setTxtFileNumber(HtmlInputText txtFileNumber) {
		this.txtFileNumber = txtFileNumber;
	}

	public HtmlInputText getTxtFileStatus() {
		return txtFileStatus;
	}

	public void setTxtFileStatus(HtmlInputText txtFileStatus) {
		this.txtFileStatus = txtFileStatus;
	}

	public HtmlSelectOneMenu getCboFileType() {
		return cboFileType;
	}

	public void setCboFileType(HtmlSelectOneMenu cboFileType) {
		this.cboFileType = cboFileType;
	}

	public HtmlCalendar getClndrCreatedOn() {
		return clndrCreatedOn;
	}

	public void setClndrCreatedOn(HtmlCalendar clndrCreatedOn) {
		this.clndrCreatedOn = clndrCreatedOn;
	}

	public void countryChanged() {
		System.out.println("hi");
		System.out.println("hi");
		System.out.println("hi");
		System.out.println("hi");
	}
	@SuppressWarnings("unchecked")
	private void getPersonInformation() {
		try {
			if (hdnManagerId != null && StringHelper.isNotEmpty(hdnManagerId)) {

				PersonView personDetail = new PersonView();
				personDetail = new PropertyService().getPersonInformationById(Long.parseLong(hdnManagerId));
				
				if(personDetail.getPersonId() != null){
					txtManagerName.setValue(personDetail.getPersonFullName());
					viewMap.put("MANAGER_ID",hdnManagerId);
				}
				hdnManagerId = null;
			}
		} catch (PimsBusinessException e) {
			logger.LogException("getPersonInformationByType crashed", e);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));
		}

	}

	public String getHdnManagerId() {
		return hdnManagerId;
	}

	public void setHdnManagerId(String hdnManagerId) {
		this.hdnManagerId = hdnManagerId;
	}

	public HtmlInputText getTxtManagerName() {
		return txtManagerName;
	}

	public void setTxtManagerName(HtmlInputText txtManagerName) {
		this.txtManagerName = txtManagerName;
	}

	public HtmlSelectBooleanCheckbox getChkIsManagerAmaf() {
		return chkIsManagerAmaf;
	}

	public void setChkIsManagerAmaf(HtmlSelectBooleanCheckbox chkIsManagerAmaf) {
		this.chkIsManagerAmaf = chkIsManagerAmaf;
	}
	public boolean showSearchManagerImage(){
		return true;
	}

	public boolean isShowSearchManagerImage() {
		return true;
	}

	public void setShowSearchManagerImage(boolean isShowSearchManagerImage) {
		this.isShowSearchManagerImage = isShowSearchManagerImage;
	}
	
	public void saveManager() {
		try {
			if (isValidated()) {
				HashMap<String, String> dataMap = new HashMap<String, String>();
				
				dataMap.put(WebConstants.ManageAssets.UPADTED_BY,getLoggedInUserId());
				if(viewMap.get("MANAGER_ID") != null){
					dataMap.put(WebConstants.ManageAssets.MANAGER_ID, viewMap.get("MANAGER_ID").toString());
					viewMap.remove("MANAGER_ID");
				}
				if(chkIsManagerAmaf.getValue() != null && (Boolean) chkIsManagerAmaf.getValue()) {
					txtManagerName.setValue(null);
				}
				dataMap.put(WebConstants.ManageAssets.ASSET_ID, viewMap.get(WebConstants.ASSET_ID).toString());
				new AssetMemsService().updateAssetMems(dataMap);
				saveSystemComments(MessageConstants.ManageAssetEvent.MANAGER_UPDATED);
				successMessages.add(CommonUtil.getBundleMessage("manageAsset.successMsg.managerSave"));
			}
		} catch (Exception e) {
			logger.LogException("saveManager crashed", e);
			errorMessages.add(CommonUtil
					.getBundleMessage("commons.ErrorMessage"));

		}
	}

	private boolean isValidated() {
		boolean validated =  true;
		if( !(chkIsManagerAmaf.getValue() != null && (Boolean) chkIsManagerAmaf.getValue()) && (txtManagerName.getValue() == null || StringHelper.isEmpty(txtManagerName.getValue().toString()))){
			validated = false;
			errorMessages.add(CommonUtil.getBundleMessage("inheritanceFile.inheritedAssetTab.errMsg.selectManager"));
		}
		return validated;
	}

	public HtmlGraphicImage getSearchImage() {
		return searchImage;
	}

	public void setSearchImage(HtmlGraphicImage searchImage) {
		this.searchImage = searchImage;
	}
	public void requestHistoryTabClick()
	{
		String methodName="requestHistoryTabClick";
    	logger.logInfo(methodName+"|"+"Start..");
    	if(viewMap.get(WebConstants.ASSET_ID) != null)
    	try	
    	{
    	  RequestHistoryController rhc=new RequestHistoryController();
    	  Long assetId= new Long(viewMap.get(WebConstants.ASSET_ID).toString());
    	  if(assetId!=null )
    	    rhc.getAllRequestTasksForRequest(WebConstants.ManageAssets.NOTES_OWNER,assetId.toString());
    	}
    	catch(Exception ex)
    	{
    		logger.LogException(methodName+"|"+"Error Occured..",ex);
    		errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
    	}
    	logger.logInfo(methodName+"|"+"Finish..");
	}
	private void saveSystemComments(String sysNote) throws Exception 
	{
		
		if(viewMap.get(WebConstants.ASSET_ID)!= null)
		{
			Long assetId = new Long(viewMap.get(WebConstants.ASSET_ID).toString());
			CommonUtil.saveSystemComments(WebConstants.ManageAssets.NOTES_OWNER, sysNote, assetId);
		}
	}

	public List<NextRevFollowup> getDataListNextRevFollowup() {
		return dataListNextRevFollowup;
	}

	public void setDataListNextRevFollowup(List<NextRevFollowup> dataList) {
		this.dataListNextRevFollowup = dataList;
	}
}
