package com.avanza.pims.web.mems.endowment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlInputText;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.pims.bpel.proxy.MEMSEndowmentDisbursementBPELPortClient;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.EndFileBen;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.soa.bpm.worklist.BPMWorklistClient;
import com.avanza.pims.soa.bpm.worklist.PIMSWorkListException;
import com.avanza.pims.soa.bpm.worklist.TaskOutcome;
import com.avanza.pims.soa.bpm.worklist.UserTask;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationDetails;
import com.avanza.pims.web.backingbeans.AttachmentBean;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.MasarifService;
import com.avanza.pims.ws.mems.PersonalAccountTrxService;
import com.avanza.pims.ws.mems.endowment.EndowmentDisbursementService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.mems.DisBenInhBenAssociationView;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.DisbursementRequestBeneficiaryView;
import com.avanza.pims.ws.vo.mems.EndFileBenView;
import com.avanza.pims.ws.vo.mems.EndowmentView;
import com.avanza.pims.ws.vo.mems.PaymentDetailsView;
import com.avanza.ui.util.ResourceUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


public class EndowmentDisbursementRequestBean extends AbstractMemsBean
{
	private static final long serialVersionUID = -5140067622599828719L;
	private transient Logger logger = Logger.getLogger(EndowmentDisbursementRequestBean.class);
	
	private static final String PAGE_MODE_VIEW = "PAGE_MODE_VIEW";
	private static final String PAGE_MODE_NEW = "NEW";
	private static final String PAGE_MODE_RESUBMITTED = "PAGE_MODE_RESUBMITTED";
	private static final String PAGE_MODE_APPROVAL = "PAGE_MODE_APPROVAL";
	private static final String PAGE_MODE_DISBURSEMENT_REQUIRED = "PAGE_MODE_DISBURSEMENT_REQUIRED";
	
	private static final String PROCEDURE_TYPE ="procedureType";
	private static final String TAB_ATTACHEMENT = "attachmentTab";
	private static final String TAB_DISBURSMENT_DETAILS   = "disbursementsTab";
	private static final String TAB_APPLICANTS   = "applicationTab";
    
	
    private String pageTitle;
    private String pageMode;
    private String hdnPersonId;
    private String hdnPersonType;
    private String hdnPersonName;
    private String hdnCellNo;
    private String hdnIsCompany;
    private String selectedBeneficiaryId;
    private String selectedPaymentMethodId;
    private String txtRemarks;
    private String totalDisbursements;
    private String totalAmount;
    private RequestService requestService = new RequestService();
    private EndowmentDisbursementService endDisService = new EndowmentDisbursementService(); 
	
	private RequestView requestView ;
//	private DisbursementDetailsView disbursementDetail =  new DisbursementDetailsView();
	private List<DisbursementDetailsView> disbursementList =  new ArrayList<DisbursementDetailsView>();
    private List<SelectItem> beneficiaryList  = new ArrayList<SelectItem>();
    
    
    protected HtmlTabPanel tabPanel = new HtmlTabPanel();
    private HtmlInputText accountBalance = new HtmlInputText();
    private HtmlDataTable disbursementTable = new HtmlDataTable(); 
    
	public EndowmentDisbursementRequestBean(){}
	
	@SuppressWarnings( "unchecked" )
	public void init()
	{
		try
		{
			if( !isPostBack() )
			{
				 initData();
			}
			else
			{
				updateValuesFromMap();
			}
		}
		catch ( Exception e )
		{
			logger.LogException( "init|Error Occured..",e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );			
		}
	}
	
	@SuppressWarnings( "unchecked" )
	private void initData() throws Exception 
	{
		viewMap.put(PROCEDURE_TYPE,WebConstants.EndowmentDisbursement.PROCEDURE);
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.EndowmentDisbursement.PROCEDURE);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID,WebConstants.Attachment.EXTERNAL_ID_REQUEST);
		viewMap.put("noteowner", WebConstants.REQUEST);
		viewMap.put("canAddAttachment", true);
		viewMap.put("canAddNote", true);
		loadAttachmentsAndComments( null );
		
		if ( sessionMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK) != null )
		{
			getDataFromTaskList();
		}
		else if ( getRequestMap().get( WebConstants.REQUEST_VIEW) != null )
		{
			getDataFromSearch();
		}
		else 
		{
			setDataForFirstTime();
		}
		
		updateValuesFromMap();
		getPageModeFromRequestStatus();
	}
	@SuppressWarnings( "unchecked" )
	public void prerender()
	{
		if( viewMap.get("alreadyExistingRequest" ) == null ) return;
		
		errorMessages = new ArrayList<String>();
		errorMessages.add( viewMap.get("alreadyExistingRequest" ).toString()  );
	}
	
	@SuppressWarnings( "unchecked" )
	private void setDataForFirstTime() throws Exception
	{
		requestView = new RequestView();
		requestView.setCreatedOn(new Date());
		requestView.setCreatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedBy(  getLoggedInUserId()  );
		requestView.setUpdatedOn(new Date());
		requestView.setStatusId( Constant.REQUEST_STATUS_NEW_ID );
		requestView.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
		requestView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
		requestView.setRequestDate( new Date() );
		requestView.setRequestTypeId( WebConstants.MemsRequestType.ENDOWMENT_DISBURSEMENT_REQUEST);
		
		
		if( sessionMap.get(WebConstants.SELECTED_ROW) != null )
		{
			EndowmentView view =  (EndowmentView )sessionMap.remove(WebConstants.SELECTED_ROW);
			requestView.setEndowment(view);
			loadBeneficiaryList(view);
		}

	}

	/**
	 * @param view
	 * @throws Exception
	 */
	private void loadBeneficiaryList(EndowmentView view) throws Exception 
	{
		List<EndFileBenView > endFileBenList = new ArrayList<EndFileBenView>();
		endFileBenList = endDisService.getEndowmentBeneficiariesByEndowmentId(view.getEndowmentId());
		if( endFileBenList != null &&  endFileBenList.size() > 0 )
		{
			populateBeneficiaryCombo(endFileBenList);
		}
	}
	
	private void populateBeneficiaryCombo(List<EndFileBenView > endFileBenList ) throws Exception
	{
		beneficiaryList = new ArrayList<SelectItem>();
		for (EndFileBenView endFileBenView : endFileBenList) 
		{
		  beneficiaryList.add(
				  				new SelectItem(
				  						        endFileBenView.getPerson().getPersonId().toString(),//+"_"+endFileBenView.getEndFileBenId(),
				  						        endFileBenView.getPersonName()
				  				              )
		  					 );
		}
	}
	public void openStatmntOfAccount()
	{

		try	
		{
			updateValuesFromMap();
			getSelectedBeneficiaryId();
			StatementOfAccountCriteria statmntReportCriteria;
			statmntReportCriteria= new StatementOfAccountCriteria(ReportConstant.Report.STATEMENT_OF_ACCOUNT,ReportConstant.Processor.STATEMENT_OF_ACCOUNT,CommonUtil	.getLoggedInUser());
			statmntReportCriteria.setPersonId(new Long(selectedBeneficiaryId));
			HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
	    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, statmntReportCriteria);
	    	executeJavaScript("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
		}
		catch(Exception ex)
		{
			logger.LogException("openStatmntOfAccount|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		
	}
	private EndFileBen getEndFileBenFromSelectedBeneficiary()throws Exception
	{
		if( selectedBeneficiaryId == null || selectedBeneficiaryId.equals("-1"))
			return null;
		else
			return EntityManager.getBroker().findById(EndFileBen.class, Long.valueOf(this.selectedBeneficiaryId.split("_")[1]) );
	}
	private String getPersonIdFromSelectedBeneficiary()throws Exception
	{
		if( selectedBeneficiaryId == null || selectedBeneficiaryId.equals("-1"))
			return null;
		else
			return this.selectedBeneficiaryId.split("_")[0];
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDeleteDisbursement()
	{
		try	
		{
			updateValuesFromMap();
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onDeleteDisbursement|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	
	@SuppressWarnings("unchecked")
	public void onBeneficiaryChange() throws Exception
	{
		try	
		{
			updateValuesFromMap();
			processBeneficiaryChange();
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onBeneficiaryChange|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		
	}

	/**
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void processBeneficiaryChange() throws Exception
	{
		if( selectedBeneficiaryId == null || selectedBeneficiaryId.equals("-1"))
		{
			accountBalance.setValue("0.00");
		}
		else
		{
		   RequestView alreadyExistingRequest = endDisService.getIncompleteEndowmentDisbRequestForPersonId(new Long( getPersonIdFromSelectedBeneficiary() ) );
		   if( 
				alreadyExistingRequest != null &&
				( 
				  requestView.getRequestId()==null || 	
				  requestView.getRequestId().compareTo(alreadyExistingRequest.getRequestId())!=0 
				) 	
			 )
		   {
				    String msg = java.text.MessageFormat.format(
				    											CommonUtil.getBundleMessage("endowmentDisbursement.msg.anotherRequestPresent"), 
				    											alreadyExistingRequest.getRequestNumber()
				    										    );
					viewMap.put("alreadyExistingRequest", msg);
					accountBalance.setValue("");
		   }
		   else 
		   {
			   if (disbursementList == null || disbursementList.size() <= 0)
			   {
			    long personId =Long.valueOf( getPersonIdFromSelectedBeneficiary() ); 
				getAccountBalanceByBeneficiaryId( personId );
				getDisbursementDetailsFromSelectedBeneficiary(personId);
			   }
				setTotalRows(disbursementList.size());
				double sumAmount=0.0d;
				for (DisbursementDetailsView view : disbursementList) {
					if(view.getDisDetId() == null )
					{
						view.setCreatedOn(new Date());
						view.setCreatedBy(  getLoggedInUserId()  );
						view.setUpdatedBy(  getLoggedInUserId()  );
						view.setUpdatedOn(new Date());
						view.setOriginalAmount(view.getAmount());
						view.setIsDeleted(WebConstants.DEFAULT_IS_DELETED);
						view.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS);
						view.setSrcType( WebConstants.PaymentSrcType.SELF_ACCOUNT_ID );
						view.setStatus( WebConstants.DisbursementStatus.NEW_ID );
						view.setHasInstallment(0l);
						view.setPaymentType( WebConstants.MemsPaymentTypes.DISBURSEMENTS_ID );
						view.setIsDisbursed(0l);
					}
					sumAmount+=view.getAmount();
				}
				DecimalFormat oneDForm = new DecimalFormat("#.00");
				totalAmount=oneDForm.format(sumAmount);
				accountBalance.setValue( totalAmount );
				viewMap.remove("alreadyExistingRequest");
		   }
		}
	}
	
	private void getDisbursementDetailsFromSelectedBeneficiary(Long personId)throws Exception
	{
		
		String json = EndowmentDisbursementService.GetUndistributedBeneficiaryEndowmentRevenueAmount(personId);
		Gson jsonObj = new GsonBuilder().create();
		disbursementList = jsonObj.fromJson(json,new TypeToken<ArrayList<DisbursementDetailsView>>(){}.getType());
	}
	private void getAccountBalanceByBeneficiaryId( Long beneficiaryId ) throws Exception
	{
		    PersonalAccountTrxService accountWS = new PersonalAccountTrxService();
			Double amount =  accountWS.getAmountByPersonId( beneficiaryId  );
			accountBalance.setValue(amount.toString());
	}
	
	@SuppressWarnings( "unchecked" )
	private void getPageModeFromRequestStatus()throws Exception
	{
		setPageMode( PAGE_MODE_NEW );
		tabPanel.setSelectedTab(TAB_APPLICANTS);
		if( this.requestView == null  || 
			this.requestView.getRequestId() == null ||
			this.requestView.getStatusId() == null 
		  ) 
		{ return; }
		if( getStatusDisbursementRequired() )
		{
			setPageMode( PAGE_MODE_DISBURSEMENT_REQUIRED);
			tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS );
		}
		else if( getStatusApproval() )
		{
			setPageMode( PAGE_MODE_APPROVAL );
			tabPanel.setSelectedTab( TAB_DISBURSMENT_DETAILS);
		}
		else if(  getStatusRejectedResubmitted() )
		{
			setPageMode(PAGE_MODE_RESUBMITTED);
			tabPanel.setSelectedTab("commentsTab" );
			
		}
		else if (getStatusCompleted())
		{
			
			setPageMode(PAGE_MODE_VIEW);
			tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS );
		}	
			
	}
	public boolean getStatusNew() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_NEW ) == 0;
	}
	public boolean getStatusApproval() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED ) == 0;
	}
	public boolean getStatusDisbursementRequired() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ ) == 0;
	}
	public boolean getStatusRejectedResubmitted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT) == 0;
	}
	public boolean getStatusCompleted() {
		return this.requestView.getStatusDataValue().compareTo(  WebConstants.REQUEST_STATUS_COMPLETE ) == 0;
	}

	@SuppressWarnings( "unchecked" )
	private void updateValuesFromMap() throws Exception
	{
		if( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = ( RequestView )viewMap.get( WebConstants.REQUEST_VIEW ) ;
			requestView.setUpdatedBy(getLoggedInUserId());
			disbursementList =  requestView.getDisubrsementDetailsView();//getDisbursementViewFromRequestView(); 
		}
		if( viewMap.get(WebConstants.EndowmentDisbursement.BenefList) != null)
		{
			beneficiaryList = (ArrayList<SelectItem>)viewMap.get(WebConstants.EndowmentDisbursement.BenefList);
		}
		if( viewMap.get("disbursementList") != null)
		{
			disbursementList = (ArrayList<DisbursementDetailsView>)viewMap.get("disbursementList");
		}
		if( viewMap.get("totalAmount") != null)
		{
			totalAmount = viewMap.get("totalAmount").toString();
		}
		if( viewMap.get("totalDisbursements") != null)
		{
			totalDisbursements = viewMap.get("totalDisbursements").toString();
		}
		updateValuesToMap();
	}
	
	@SuppressWarnings( "unchecked" )
	private void updateValuesToMap() throws Exception
	{
		if( requestView != null )
		{
//			if( disbursementDetail != null )
//			{
//				List<DisbursementDetailsView> list  = new ArrayList<DisbursementDetailsView>();
//				list.add(disbursementDetail);
//				requestView.setDisubrsementDetailsView(list);
//			}
			if (disbursementList != null && disbursementList.size()>0)
			{
				viewMap.put("disbursementList",disbursementList ); 
			}
			if( totalAmount  != null && totalAmount.length()> 0)
			{
				viewMap.put("totalAmount",totalAmount );
				accountBalance.setValue(totalAmount);
			}
			if( totalDisbursements != null && totalDisbursements.length()> 0)
			{
				viewMap.put("totalDisbursements",totalDisbursements );
			}
			
			viewMap.put(  WebConstants.REQUEST_VIEW , requestView);
		}
		
		
		if( beneficiaryList != null )
		{
			viewMap.put(WebConstants.EndowmentDisbursement.BenefList,beneficiaryList);
		}
	}
	
	private DisbursementDetailsView getDisbursementViewFromRequestView () throws Exception
	{
		if( requestView.getDisubrsementDetailsView() == null || requestView.getDisubrsementDetailsView().size() <= 0 )
		{
			return new DisbursementDetailsView();
		}
		return (DisbursementDetailsView)requestView.getDisubrsementDetailsView().get(0); 
		
	}
	
	@SuppressWarnings( "unchecked" )
	private void getDataFromSearch()throws Exception
	{
		requestView = ( RequestView)getRequestMap().get( WebConstants.REQUEST_VIEW) ;
		if( this.requestView !=null )
		{
		  getRequestDetails( requestView.getRequestId() );
		}
	}
	
	@SuppressWarnings(  "unchecked"  )
	private void populateApplicationDetailsTab()throws Exception
	{
		ApplicationDetails bean = (ApplicationDetails)getBean("pages$ApplicationDetails");
		String status = "";
		boolean isRequestNull =  null == requestView || null == requestView.getApplicantView() ||null == requestView.getRequestId();
		if ( isRequestNull && hdnPersonName!= null && hdnPersonName.trim().length() >0 ) 
		{
			bean.populateApplicationDetails(
					"", 
						                    "", 
						                    new Date(), 
						                    "", 
						                    hdnPersonName, 
						                    "", 
						                    hdnCellNo, 
						                    ""
						                  );
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,hdnPersonId );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, hdnPersonId);
			PersonView person = new PersonView();
			person.setPersonId( new Long( hdnPersonId ) );
			requestView.setApplicantView( person);
			
            return;
		}
		
		status = CommonUtil.getIsEnglishLocale()?requestView.getStatusEn():requestView.getStatusAr();
		bean.populateApplicationDetails(
				                         requestView.getRequestNumber(), 
					                     status, 
					                     requestView.getRequestDate(), 
					                     requestView.getDescription(), 
					                     requestView.getApplicantView().getPersonFullName(), 
					                     "", 
					                     requestView.getApplicantView().getCellNumber(), 
					                     requestView.getApplicantView().getEmail()
					                   );
		if( requestView.getApplicantView() != null && requestView.getApplicantView().getPersonId() != null  )
		{
			viewMap.put( WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,requestView.getApplicantView().getPersonId() );
			viewMap.put( WebConstants.LOCAL_PERSON_ID, requestView.getApplicantView().getPersonId() );
		}
		if ( requestView!= null && requestView.getStatus() != null && 
			!requestView.getStatusDataValue().equals( WebConstants.REQUEST_STATUS_NEW ) 
		    )
		{
			viewMap.put("applicationDetailsReadonlyMode", true);
			viewMap.put("applicationDescriptionReadonlyMode", "READONLY");
		}
	}
	
	@SuppressWarnings( "unchecked" )
	protected void getDataFromTaskList()throws Exception
    {
	  UserTask userTask = ( UserTask )sessionMap.remove( WebConstants.TASK_LIST_SELECTED_USER_TASK );
	  setUserTask(userTask);
	  if( userTask.getTaskAttributes().get(  WebConstants.REQUEST_ID ) == null ){ return; }
	  long  id = new Long  ( userTask.getTaskAttributes().get( WebConstants.REQUEST_ID ) );
	  getRequestDetails( id );
	  
	  
    }
	
	@SuppressWarnings( "unchecked" )
	private void getRequestDetails( long id ) throws Exception  
	{
		requestView = requestService.getRequestById( id );
		disbursementList =  requestView.getDisubrsementDetailsView();//getDisbursementViewFromRequestView();
		DisbursementDetailsView disbursementDetail=disbursementList.get(0);
		DisbursementRequestBeneficiaryView benef = (DisbursementRequestBeneficiaryView)disbursementDetail.getRequestBeneficiaries().get(0);
//		DisBenInhBenAssociationView disBenInhBen = benef.getFileAssociations().get(0);
		
		setSelectedBeneficiaryId( benef.getPersonId().toString());//+"_"+disBenInhBen.getEndFileBeneficiaryId().toString() );
		loadBeneficiaryList(requestView.getEndowment());
		processBeneficiaryChange();
		if( disbursementDetail.getPaymentDetails() != null )
		{
			for(PaymentDetailsView pd : disbursementDetail.getPaymentDetails())
			{
				selectedPaymentMethodId = pd.getPaymentMethod().toString();
				break;
			}
		}
		populateApplicationDetailsTab();
		updateValuesToMap();
		
		loadAttachmentsAndComments( id );
	}
	
	@SuppressWarnings( "unchecked" )
	public void loadAttachmentsAndComments( Long id )
	{

		String repositoryId = WebConstants.Attachment.PIMS_REPOSITORY_ID;
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, repositoryId);		
		
		if(id != null)
		{
	    	String entityId = id.toString();
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
			viewMap.put("entityId", entityId);
		}
	}	

	@SuppressWarnings( "unchecked" )
	private void saveCommentsAttachment(String eventDesc) throws Exception 
	{
		     saveComments();
			 saveAttachments(requestView.getRequestId().toString());
			 saveSystemComments(eventDesc);
	}
	
	@SuppressWarnings( "unchecked" )
	public void saveSystemComments(String sysNoteType) throws Exception
    {
	  if ( sysNoteType != null && sysNoteType.trim().length()>0)
	  {
		  String notesOwner = WebConstants.REQUEST;
		  NotesController.saveSystemNotesForRequest(notesOwner,sysNoteType, requestView.getRequestId());
	  }
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveComments(  ) throws Exception
    {
		Boolean success = false;
			String notesOwner = WebConstants.REQUEST;
	    	if(txtRemarks !=null && this.txtRemarks.length()>0)
	    	{
	    	  CommonUtil.saveRemarksAsComments(requestView.getRequestId() , txtRemarks, notesOwner) ;
	    	}
	    	NotesController.saveNotes(notesOwner, requestView.getRequestId() );
	    	success = true;
    	return success;
    }
	
	@SuppressWarnings( "unchecked" )
	public Boolean saveAttachments(String referenceId)throws Exception
    {
		Boolean success = false;
    	if(referenceId!=null)
    	{
    		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, referenceId);
	    	success = CommonUtil.updateDocuments();
    	}
    	return success;
    }

	@SuppressWarnings( "unchecked" )
	public void onAttachmentsCommentsClick()
    {
		try	
		{
		 if( requestView != null && requestView.getRequestId()!= null )
		 {
			 loadAttachmentsAndComments( requestView.getRequestId() );
		 }
		}
		catch(Exception ex)
		{
			logger.LogException("onAttachmentsCommentsClick|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

    }
	
	@SuppressWarnings( "unchecked" )
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMap();
			if(hdnPersonType.equals( "APPLICANT" ) )
			{
				populateApplicationDetailsTab();
			}
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}
	
	@SuppressWarnings( "unchecked" )
	public void onPaymentMethodChanged()
	{
		try	
		{
			updateValuesFromMap();
			updateValuesToMap();
		}
		catch(Exception ex)
		{
			logger.LogException("onPaymentMethodChanged|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}

	}


	@SuppressWarnings( "unchecked" )
	public void onActivityLogTab()
	{
		try	
		{

			if(requestView == null || requestView.getRequestId() ==null) return;
            RequestHistoryController rhc=new RequestHistoryController();
		    rhc.getAllRequestTasksForRequest(WebConstants.REQUEST,requestView.getRequestId().toString());
		}
		catch(Exception ex)
		{
			logger.LogException("onActivityLogTab|Error Occured..",ex);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}	
	private boolean hasSaveErrors() throws Exception
	{
		boolean hasSaveErrors=false;
		errorMessages = new ArrayList<String>();
		getSelectedBeneficiaryId();
		if( !viewMap.containsKey(WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID) ) {
			errorMessages.add(CommonUtil.getBundleMessage(WebConstants.CLREQUEST_NOAPPLICANTSELECTED));
			tabPanel.setSelectedTab(TAB_APPLICANTS);
			return true;
		}
		else if ( selectedBeneficiaryId == null || selectedBeneficiaryId.equals("-1") )
		{
			
			errorMessages.add(CommonUtil.getBundleMessage("endowmentDisbursement.msg.beneficiaryRequired"));
			tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS);
			return true;
		}
		else if( accountBalance == null || accountBalance.equals("0.0") )
		{
			errorMessages.add(CommonUtil.getBundleMessage("endowmentDisbursement.msg.remainingBalaneRequired"));
			tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS);
			return true;
		}
		if(!AttachmentBean.mandatoryDocsValidated())
    	{
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.Attachment.MSG_MANDATORY_DOCS)  );
    		tabPanel.setSelectedTab(TAB_ATTACHEMENT);
    		return true;
    	}

		return hasSaveErrors;
		
	}


	@SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
	        
			updateValuesFromMap();
			if( hasSaveErrors() ){ return; }
			saveRequestInTransaction();
			getRequestDetails( requestView.getRequestId() );
			saveCommentsAttachment( MessageConstants.RequestEvents.REQUEST_SAVED );
			successMessages.add( ResourceUtil.getInstance().getProperty("thirdPartRevenue.msg.saved"));
			updateValuesToMap();
		}
		catch (Exception exception) 
		{
			logger.LogException( "onSave --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	
	private void saveRequestInTransaction() throws Exception 
	{
		try
		{
			Long status = 9001l;
            if(requestView.getStatusId() != null)
            {
            	status = null;
            }
            ApplicationContext.getContext().getTxnContext().beginTransaction();
			persistRequest( status, WebConstants.DisbursementStatus.NEW_ID );
			ApplicationContext.getContext().getTxnContext().commit();
			
		}
		catch(Exception e)
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			throw e;
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	private void persistRequest(Long requestStatus,Long disbursementStatus) throws Exception 
	{
		if(requestStatus!=null) requestView.setStatusId(requestStatus);
		populateView(disbursementStatus);
		
		requestView = endDisService.persistDisbursementRequest( requestView );
	}
	
	private void populateView(Long disbursementStatus)throws Exception
	{
		PersonView person  = new PersonView();
		getSelectedBeneficiaryId();
		if( selectedBeneficiaryId  == null || selectedBeneficiaryId.equals("-1") ) return;
		Long  personId = Long.valueOf( getPersonIdFromSelectedBeneficiary() );
		person.setPersonId( personId  );
		requestView.setTenantsView(person);
		for (DisbursementDetailsView eachDisbursementDetails: disbursementList)
		{
			
			
			if(disbursementStatus != null)
			{
				eachDisbursementDetails.setStatus(disbursementStatus);	
			}
//			if( accountBalance != null && accountBalance.getValue() != null )
//			{
//				disbursementDetail.setAmount( new Double ( accountBalance.getValue().toString() ) );
//				disbursementDetail.setOriginalAmount( disbursementDetail.getAmount() );
//			}
			if( 
					eachDisbursementDetails.getRequestBeneficiaries() != null && 
					eachDisbursementDetails.getRequestBeneficiaries().size() > 0 
			  )
			{
				DisbursementRequestBeneficiaryView disReqBenView  = eachDisbursementDetails.getRequestBeneficiaries().get(0);
				disReqBenView.setPersonId( personId );
				disReqBenView.setAmount( eachDisbursementDetails.getAmount() );
				
			}
			else
			{
				List<DisbursementRequestBeneficiaryView > list = new ArrayList<DisbursementRequestBeneficiaryView>();
				List<DisBenInhBenAssociationView > setAsso = new ArrayList<DisBenInhBenAssociationView>();
				
				DisbursementRequestBeneficiaryView disReqBenView  = new DisbursementRequestBeneficiaryView();
				disReqBenView.setPersonId( personId );
				disReqBenView.setIsDeleted( 0l );
				disReqBenView.setAmount( eachDisbursementDetails.getAmount() );
	
				DisBenInhBenAssociationView asso = new DisBenInhBenAssociationView();
				asso.setAmount(eachDisbursementDetails.getAmount());
				setAsso.add(asso);
//				EndFileBen endFileBen = getEndFileBenFromSelectedBeneficiary() ;
				asso.setEndFileBeneficiaryId( eachDisbursementDetails.getEndFileBenId() );//endFileBen.getEndFileBenId() );
				disReqBenView.setFileAssociations(setAsso);
	
				list.add(disReqBenView);
				eachDisbursementDetails.setRequestBeneficiaries(list);
			}
	//		if( pageMode.equals(PAGE_MODE_DISBURSEMENT_REQUIRED) )
			if( pageMode.equals(PAGE_MODE_APPROVAL))
			{
				PaymentDetailsView pd = new PaymentDetailsView();
				pd.setPaymentMethod(new Long( selectedPaymentMethodId ) );
				pd.setPaidTo(personId);
				pd.setIsDeleted(0l);
				pd.setRefDate( new Date() );
				pd.setAmount(eachDisbursementDetails.getAmount()) ;
				List<PaymentDetailsView> list = new ArrayList<PaymentDetailsView>();
				list.add(pd);
				eachDisbursementDetails.setPaymentDetails(list );
			}
		
		}
		requestView.setDisubrsementDetailsView(disbursementList);
	}
	
	@SuppressWarnings( "unchecked" )
	public void onSubmit()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status = CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		String event  =  MessageConstants.RequestEvents.REQUEST_SUBMIT;
		try	
		{	
			ApplicationContext.getContext().getTxnContext().beginTransaction();
			if( hasSaveErrors() ){ return; }
			
			 updateValuesFromMap();
			 
			 SystemParameters parameters = SystemParameters.getInstance();
			 String endPoint= parameters.getParameter( "MEMSEndowmentDisbursementBPEL" );
			 MEMSEndowmentDisbursementBPELPortClient port=new MEMSEndowmentDisbursementBPELPortClient();
			 port.setEndpoint(endPoint);
			 
			 persistRequest( status, WebConstants.DisbursementStatus.NEW_ID );
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 
			 port.initiate(
					 		CommonUtil.getLoggedInUser(),
					 		Integer.parseInt( requestView.getRequestId().toString()),
					 		requestView.getRequestNumber(), 
					 		null, 
					 		null
					 	   );
			 
			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onSubmit --- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onResubmitted()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		String event  =  MessageConstants.RequestEvents.REQUEST_RESUBMITTED;
		try	
		{	
		 ApplicationContext.getContext().getTxnContext().beginTransaction();
		 if( hasSaveErrors() ){ return; }
		 
		 updateValuesFromMap();	
		 
		 persistRequest( status ,WebConstants.DisbursementStatus.NEW_ID);
		 getRequestDetails( requestView.getRequestId() );
		 
		 setTaskOutCome(TaskOutcome.OK);

		 updateValuesToMap();
		 
		 saveCommentsAttachment( event );
		 
		 ApplicationContext.getContext().getTxnContext().commit();

		 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
	   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onResubmitted--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}

	private boolean hasReasonProvided() throws Exception
	{
		if(txtRemarks ==null || txtRemarks.trim().length() <= 0 )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("endowmentProgram.msg.reasonRequired")  );
			return false;
		}
		return true;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onApprove()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_DISBURSEMENT_REQ);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_DISBURSEMENT;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENT_FOR_DISBURSEMENT;
		try	
		{	
			getSelectedPaymentMethodId();
			if( selectedPaymentMethodId == null || selectedPaymentMethodId.equals("-1")) 
			{
				errorMessages.add(CommonUtil.getBundleMessage("mems.payment.popup.err.msg.paymentMethodReq"));
				tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS);
			
			}
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 //if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( status,WebConstants.DisbursementStatus.DIS_REQ_ID );
			 setTaskOutCome(TaskOutcome.APPROVE);
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onApprove--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onRejected()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_REJECTED_RESUBMIT);
		String msg    =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		String event  =  MessageConstants.RequestEvents.REQUEST_SENTBACK;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( status ,WebConstants.DisbursementStatus.REJECTED_ID);
			 setTaskOutCome(TaskOutcome.FORWARD);
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	@SuppressWarnings( "unchecked" )
	public void onRejectedByFinance()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_APPROVAL_REQUIRED);
		String msg    =  MessageConstants.RequestEvents.REQUEST_REJECTED_FINANCE;
		String event  =  MessageConstants.RequestEvents.REQUEST_REJECTED_FINANCE;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 if( !hasReasonProvided() ) return;
			 updateValuesFromMap();	
			
			 persistRequest( status,WebConstants.DisbursementStatus.DIS_REQ_ID );
			 setTaskOutCome(TaskOutcome.REJECT);
			 getRequestDetails( requestView.getRequestId() );
			 updateValuesToMap();
			 
			 saveCommentsAttachment( event );
			 ApplicationContext.getContext().getTxnContext().commit();
			 
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		   	
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	private boolean hasOnCompleteErrors()throws Exception
	{
		errorMessages = new ArrayList<String>();
		getSelectedPaymentMethodId();
		if( selectedPaymentMethodId == null || selectedPaymentMethodId.equals("-1")) 
		{
			errorMessages.add(CommonUtil.getBundleMessage("mems.payment.popup.err.msg.paymentMethodReq"));
			tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS);
			return true;
		}
		
        return false;
	}

	@SuppressWarnings( "unchecked" )
	public void onDisburse()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try	
		{	
			updateValuesFromMap();
			List<DisbursementDetailsView>  listToDisburse = new ArrayList<DisbursementDetailsView>();
			if ( hasOnDisburseErrors(listToDisburse) ) return;
			
			executeDisbursementInTransaction(listToDisburse);
			
			getRequestDetails( requestView.getRequestId() );
			successMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsFinanceMsgs.SUCC_DISB));
		}
		catch (Exception exception) 
		{
			logger.LogException( "onDisburse--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	private boolean hasBalanceErrors( double amountToValidate,Long personId ) throws Exception
	{
		boolean hasBalanceErrors=false;
		PersonalAccountTrxService accountWS = new PersonalAccountTrxService();
		double amount =  accountWS.getAmountByPersonId( personId  );
        
		if(  amount < amountToValidate )
		{
			String msg = java.text.MessageFormat.format(
													    CommonUtil.getBundleMessage( "common.error.masrafBalanceNotEnough"),
													    amount,
													    amountToValidate
													   );
			errorMessages.add( msg );
			hasBalanceErrors  = true;
		}
		return hasBalanceErrors;
	}
	
	/**
	 * @param listToDisburse
	 * @return
	 * @throws Exception
	 */
	private boolean hasOnDisburseErrors(List<DisbursementDetailsView> listToDisburse) throws Exception 
	{
		boolean hasErrors = false;
		for (DisbursementDetailsView  row : getDisbursementList()) 
		{
			if( row.getSelected() == null || !row.getSelected() )
				continue;
			hasErrors = hasBalanceErrors( row.getAmount(),row.getRequestBeneficiaries().iterator().next().getPersonId()  );
			if ( hasErrors )
				break;
			row.setOriginalAmount( row.getAmount() );
			row.setUpdatedBy( getLoggedInUserId());
			row.setUpdatedOn( new Date() );
			
			listToDisburse.add(row);
		}
		if( listToDisburse == null || listToDisburse.size() <= 0 )
		{
			errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsNormalDisbursementMsgs.ERR_NO_ROW));
			tabPanel.setSelectedTab(TAB_DISBURSMENT_DETAILS);
			return true;
		}
		return hasErrors;
	}

	/**
	 * @param listToDisburse
	 * @throws Exception
	 */
	private void executeDisbursementInTransaction(List<DisbursementDetailsView> listToDisburse) throws Exception 
	{
		try	
		{	
		  ApplicationContext.getContext().getTxnContext().beginTransaction();
			endDisService.executeDisbursements(requestView.getRequestId(), getLoggedInUserId(),listToDisburse);
			String disRefNums ="";
			for (DisbursementDetailsView obj : listToDisburse) {
				disRefNums += obj.getRefNum() +",";
			}
			if( disRefNums.length() > 0 )
			{
				disRefNums = disRefNums.substring(0 ,disRefNums.length()-1);
			}
			
			NotesController.saveSystemNotesForRequest(WebConstants.REQUEST,"masraf.disbursementMsg", requestView.getRequestId(),disRefNums);
			saveCommentsAttachment( "" );
		  ApplicationContext.getContext().getTxnContext().commit();
		}
		catch (Exception exception) 
		{
		  ApplicationContext.getContext().getTxnContext().rollback();
		  throw exception;
		}
		finally
		{
	      ApplicationContext.getContext().getTxnContext().release();
		}
	}

	@SuppressWarnings( "unchecked" )
	public void onComplete()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		Long status   =	 CommonUtil.getDomainDataId(WebConstants.REQUEST_STATUS, WebConstants.REQUEST_STATUS_COMPLETE);
		String msg    =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		String event  =  MessageConstants.RequestEvents.REQUEST_COMPLETED;
		try	
		{	
			 ApplicationContext.getContext().getTxnContext().beginTransaction();
			 
			 	 updateValuesFromMap();
				 if( hasOnCompleteErrors() ) return;
				 persistRequest( status,WebConstants.DisbursementStatus.DISBURSED_ID );
				 setTaskOutCome(TaskOutcome.APPROVE);
				 getRequestDetails( requestView.getRequestId() );
				 updateValuesToMap();
				 saveCommentsAttachment( event );

			 ApplicationContext.getContext().getTxnContext().commit();
		   	 successMessages.add( ResourceUtil.getInstance().getProperty(msg));
		   	 txtRemarks = "";
		   	 this.setPageMode( PAGE_MODE_VIEW );
		 }
		catch (Exception exception) 
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException("onRejected--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>(0);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
	}
	
	
	@SuppressWarnings( "unchecked" )
	protected void setTaskOutCome(TaskOutcome taskOutCome)throws PIMSWorkListException,Exception
    {
	    	String contextPath = ((ServletContext) getFacesContext().getExternalContext().getContext()).getRealPath("\\WEB-INF\\config.properties") ;
	    	UserTask userTask = getUserTask();
			String loggedInUser=getLoggedInUserId();
			BPMWorklistClient bpmWorkListClient = new BPMWorklistClient(contextPath);
			bpmWorkListClient.completeTask(userTask, loggedInUser, taskOutCome);
    }



	@SuppressWarnings( "unchecked" )
	public UserTask getUserTask( )
	{
		if( viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK ) != null )
			return (UserTask) viewMap.get(WebConstants.TASK_LIST_SELECTED_USER_TASK);
		else 
			return null;
		
	}
	@SuppressWarnings( "unchecked" )
	public void setUserTask( UserTask userTask )
	{
		if( userTask != null )
			viewMap.put(WebConstants.TASK_LIST_SELECTED_USER_TASK, userTask);
	}


	public String getPageTitle() {
	
//	if( pageMode.equals(  PAGE_MODE_NEW )  || pageMode.equals(  PAGE_MODE_VIEW)  )
//	{
		this.setPageTitle(ResourceUtil.getInstance().getProperty("endowmentDisbursement.title.header"));
//	}
	
	return pageTitle;
	}



	public void setPageTitle(String pageTitle) {
	this.pageTitle = pageTitle;
	}
	
	public String getErrorMessages()
	{
	
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSaveButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			(( getPageMode().equals( PAGE_MODE_NEW)  || getPageMode().equals( PAGE_MODE_RESUBMITTED ) )&&
			viewMap.get( "alreadyExistingRequest") == null	
		  )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowApproval()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_APPROVAL)   )  
				
	      ) 
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public Boolean getShowComplete()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) && 
			( getPageMode().equals( PAGE_MODE_DISBURSEMENT_REQUIRED)   )  
				
	      ) 
		{
			return true;
		}
		return false;
	}

	@SuppressWarnings( "unchecked" )
	public Boolean getShowSubmitButton()
	{
		if( !getPageMode().equals( PAGE_MODE_VIEW ) &&  getPageMode().equals( PAGE_MODE_NEW ) &&
			viewMap.get( "alreadyExistingRequest") == null	
		  )  
		{
			return true;
		}
		return false;
	}
	@SuppressWarnings( "unchecked" )
	public Boolean getShowResubmitButton()
	{
		if(  !getPageMode().equals( PAGE_MODE_VIEW ) && getPageMode().equals( PAGE_MODE_RESUBMITTED ) &&
				viewMap.get( "alreadyExistingRequest") == null	
		  )
		{
			return true;
		}
		return false;
	}
	
	@SuppressWarnings( "unchecked" )
	public String getPageMode() {
		if( viewMap.get("pageMode")!= null )
			pageMode = viewMap.get("pageMode").toString();
		return pageMode;
	}
	
	
	@SuppressWarnings( "unchecked" )
	public void setPageMode(String pageMode) {
		
		this.pageMode = pageMode;
		if( this.pageMode != null )
			viewMap.put( "pageMode", this.pageMode );
	}
	public HtmlTabPanel getTabPanel() {
		return tabPanel;
	}
	public void setTabPanel(HtmlTabPanel tabPanel) {
		this.tabPanel = tabPanel;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonType() {
		return hdnPersonType;
	}

	public void setHdnPersonType(String hdnPersonType) {
		this.hdnPersonType = hdnPersonType;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnCellNo() {
		return hdnCellNo;
	}

	public void setHdnCellNo(String hdnCellNo) {
		this.hdnCellNo = hdnCellNo;
	}

	public String getHdnIsCompany() {
		return hdnIsCompany;
	}

	public void setHdnIsCompany(String hdnIsCompany) {
		this.hdnIsCompany = hdnIsCompany;
	}


	public String getTxtRemarks() {
		return txtRemarks;
	}

	public void setTxtRemarks(String txtRemarks) {
		this.txtRemarks = txtRemarks;
	}


	public List<SelectItem> getBeneficiaryList() {
		return beneficiaryList;
	}

	public void setBeneficiaryList(List<SelectItem> beneficiaryList) {
		this.beneficiaryList = beneficiaryList;
	}

	public HtmlInputText getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(HtmlInputText accountBalance) {
		this.accountBalance = accountBalance;
	}

	@SuppressWarnings("unchecked")
	public String getSelectedBeneficiaryId() {
		if( viewMap.get("selectedBeneficiaryId") != null )
		{
			selectedBeneficiaryId = viewMap.get("selectedBeneficiaryId").toString();  
		}
		else
		{
			selectedBeneficiaryId="-1";
		}
		return selectedBeneficiaryId;
	}

	@SuppressWarnings("unchecked") 
	public void setSelectedBeneficiaryId(String selectedBeneficiaryId) {
		
		this.selectedBeneficiaryId = selectedBeneficiaryId;
		if(this.selectedBeneficiaryId != null && !this.selectedBeneficiaryId.equals("-1"))
		{
			viewMap.put("selectedBeneficiaryId",selectedBeneficiaryId); 
		}
	}


	public String getSelectedPaymentMethodId() {
		if( viewMap.get("selectedPaymentMethodId") != null )
		{
			selectedPaymentMethodId = viewMap.get("selectedPaymentMethodId").toString();  
		}
		return selectedPaymentMethodId;
	}

	@SuppressWarnings("unchecked")
	public void setSelectedPaymentMethodId(String selectedPaymentMethodId) {
		this.selectedPaymentMethodId = selectedPaymentMethodId;
		if(this.selectedPaymentMethodId != null && !this.selectedPaymentMethodId.equals("-1"))
		{
			viewMap.put("selectedPaymentMethodId",selectedPaymentMethodId); 
		}
	}

	public List<DisbursementDetailsView> getDisbursementList() {
		return disbursementList;
	}

	public void setDisbursementList(List<DisbursementDetailsView> disbursementList) {
		this.disbursementList = disbursementList;
	}

	public HtmlDataTable getDisbursementTable() {
		return disbursementTable;
	}

	public void setDisbursementTable(HtmlDataTable disbursementTable) {
		this.disbursementTable = disbursementTable;
	}

	public String getTotalDisbursements() {
		return totalDisbursements;
	}

	public void setTotalDisbursements(String totalDisbursements) {
		this.totalDisbursements = totalDisbursements;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

}