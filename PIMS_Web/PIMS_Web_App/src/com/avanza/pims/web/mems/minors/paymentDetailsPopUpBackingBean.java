package com.avanza.pims.web.mems.minors;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.util.Logger;
import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.Person;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationBean;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.AssetsGridView;
import com.avanza.pims.ws.vo.PaymentCriteriaView;
import com.avanza.pims.ws.vo.PaymentMethodView;
import com.avanza.pims.ws.vo.PeriodicDisbursementGridView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.CollectionProcedureView;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.PaymentDetailsView;
import com.avanza.pims.ws.vo.mems.PeriodicDisbursementDetailsView;
import com.avanza.pims.ws.vo.mems.ResearchRecommendationView;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings("unchecked")
public class paymentDetailsPopUpBackingBean extends AbstractMemsBean {
	private transient Logger logger = Logger
			.getLogger(paymentDetailsPopUpBackingBean.class);

	FacesContext context = FacesContext.getCurrentInstance();
	Map sessionMap;
	Map viewRootMap;
	private HtmlDataTable dataTable;

	private HtmlInputText htmlReferenceNumber = new HtmlInputText();
	private HtmlInputText htmlReadOnlyBankName = new HtmlInputText();
	
	private HtmlInputText htmlOtherBeneficiary = new HtmlInputText();
	private HtmlInputText htmlAssetNameEn = new HtmlInputText();
	private HtmlInputText htmlDate = new HtmlInputText();
	private HtmlInputText htmlAccountNumber = new HtmlInputText();
	private HtmlOutputLabel htmlOtherBeneficiaryLbl = new HtmlOutputLabel();
	
	private HtmlInputText htmlBeneficiary = new HtmlInputText();
	private HtmlInputText htmlFilePersonName = new HtmlInputText();
	private HtmlCalendar clndrCreatedOn = new HtmlCalendar();
	private HtmlInputText htmlAssetNameAr = new HtmlInputText();
	private HtmlSelectOneMenu paidToSelectMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu paymentModeSelectMenu = new HtmlSelectOneMenu();

	private HtmlSelectOneMenu bankSelectMenu = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu fileTypeSelectMenu = new HtmlSelectOneMenu();
	private List<SelectItem> paidToList = new ArrayList<SelectItem>(0);
	private List<SelectItem> bankList = new ArrayList<SelectItem>(0);
	private List<SelectItem> paymentModeList = new ArrayList<SelectItem>(0);
	HashMap formParamMap = new HashMap();
	private List<AssetsGridView> dataList = new ArrayList<AssetsGridView>();
	private List<SelectItem> fileStatus = new ArrayList<SelectItem>();
	private String VIEW_MODE = "pageMode";
	private Integer paginatorRows = 0;
	private Integer recordSize = 0;

	private String MODE_SELECT_ONE_POPUP = "MODE_SELECT_ONE_POPUP";
	private String MODE_SELECT_MANY_POPUP = "MODE_SELECT_MANY_POPUP";
	private static final String RESEARCH_RECOMMENDATION_ROW = "RESEARCH_RECOMMENDATION_ROW";


	private boolean isPageModeSelectOnePopUp = false;
	private boolean isPageModeSelectManyPopUp = false;

	private String CONTEXT = "context";
	private String personId = "";

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public FacesContext getContext() {
		return context;
	}

	public void setContext(FacesContext context) {
		this.context = context;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public HtmlInputText getHtmlAssetNameNameEn() {
		return htmlAssetNameEn;
	}

	public void setHtmlAssetNameNameEn(HtmlInputText htmlAssetNameNameEn) {
		this.htmlAssetNameEn = htmlAssetNameNameEn;
	}

	public HtmlInputText getHtmlBeneficiary() {
		return htmlBeneficiary;
	}

	public void setHtmlBeneficiary(HtmlInputText htmlBeneficiary) {
		this.htmlBeneficiary = htmlBeneficiary;
	}

	public HtmlInputText getHtmlFilePersonName() {
		return htmlFilePersonName;
	}

	public void setHtmlFilePersonName(HtmlInputText htmlFilePersonName) {
		this.htmlFilePersonName = htmlFilePersonName;
	}

	public HtmlInputText getHtmlAssetNameAr() {
		return htmlAssetNameAr;
	}

	public void setHtmlAssetNameAr(HtmlInputText htmlAssetNameAr) {
		this.htmlAssetNameAr = htmlAssetNameAr;
	}

	public HtmlSelectOneMenu getFileTypeSelectMenu() {
		return fileTypeSelectMenu;
	}

	public void setFileTypeSelectMenu(HtmlSelectOneMenu fileTypeSelectMenu) {
		this.fileTypeSelectMenu = fileTypeSelectMenu;
	}

	@SuppressWarnings("unchecked")
	public void init() 
	{
		super.init();
		viewRootMap = getFacesContext().getViewRoot().getAttributes();
		try 
		{
			sessionMap = context.getExternalContext().getSessionMap();
			HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
			if (!isPostBack()) 
			{
				loadPaymentMethod();
				loadBanksMethod();
				if (request.getParameter(WebConstants.PERSON_ID) != null || 
					request.getParameter( WebConstants.InheritanceFile.INHERITANCE_FILE_ID ) != null 
					) 
				{
					if (request.getParameter(WebConstants.PERSON_ID) != null) 
					{
						viewRootMap.put(WebConstants.PERSON_ID, request.getParameter(WebConstants.PERSON_ID).toString());
					}
					else if(  request.getParameter( WebConstants.InheritanceFile.INHERITANCE_FILE_ID ) != null ) 
					{
						viewRootMap.put( WebConstants.InheritanceFile.INHERITANCE_FILE_ID,
								         request.getParameter( WebConstants.InheritanceFile.INHERITANCE_FILE_ID ).toString() 
								        );
					}
					if (request.getParameter(WebConstants.PAYMENT_DETAILS_FORM_CRITERIA.AMOUNT) != null) 
					{
						viewRootMap.put( WebConstants.PAYMENT_DETAILS_FORM_CRITERIA.AMOUNT,
								         request.getParameter(WebConstants.PAYMENT_DETAILS_FORM_CRITERIA.AMOUNT).toString());
					}
					if (request.getParameter("context") != null) 
					{
						viewRootMap.put("context",request.getParameter("context").toString());
					}
					if( sessionMap.containsKey(WebConstants.DISBURSEMENT_DETAIL_ID )  )
					{
						long ddId = Long.valueOf(sessionMap.remove( WebConstants.DISBURSEMENT_DETAIL_ID ).toString() );
						viewMap.put( WebConstants.DISBURSEMENT_DETAIL_ID, ddId );
					}
					if( sessionMap.containsKey(WebConstants.MULTIPLE_BENEFICIARIES ) )
					{
						
						sessionMap.remove(WebConstants.MULTIPLE_BENEFICIARIES );
						loadPaidToList( 1 );
					}
					else
					{
					    loadPaidToList( 0 );
					}
				}
				
				if (request.getParameter("context") != null) 
				{
					viewRootMap.put("context",request.getParameter("context").toString());
				}
				if (request.getParameter("CONFIRM") != null)// FOR PageModeConfirm belonging to CollectionProcedure
				{
					viewRootMap.put("PageModeConfirm", request.getParameter("CONFIRM").toString());
				}
				if (sessionMap.containsKey(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.SELECTED_ROW)) 
				{
					PeriodicDisbursementGridView parentRow = new PeriodicDisbursementGridView();
					parentRow = (PeriodicDisbursementGridView) sessionMap.get(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.SELECTED_ROW);
					sessionMap.remove(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.SELECTED_ROW);
					viewMap.put( WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.SELECTED_ROW,parentRow);
					if (parentRow.getPaymentDetailFormView() != null) 
					{
						putPeriodicDisViewValuesInControl(parentRow);
					}
				} 
				else if (sessionMap.containsKey(WebConstants.REVIEW_CONFIRM_DISB.REVIEW_CONFIRM_DISB_SELECTED_ROW))
				{
					PeriodicDisbursementDetailsView parentRow = new PeriodicDisbursementDetailsView();
					parentRow = (PeriodicDisbursementDetailsView) sessionMap.remove(WebConstants.REVIEW_CONFIRM_DISB.REVIEW_CONFIRM_DISB_SELECTED_ROW);
					viewMap.put(WebConstants.REVIEW_CONFIRM_DISB.REVIEW_CONFIRM_DISB_SELECTED_ROW,parentRow);

					if (parentRow.getPaymentDetailFormView() != null) 
					{
						if( parentRow.getPaymentDetailFormView().getPaymentMethodId() != null && parentRow.getPaymentDetailFormView().getPaymentMethodId().longValue()!=-1L  )
						{
						 paymentModeSelectMenu.setValue( parentRow.getPaymentDetailFormView().getPaymentMethodId().toString()  );
						}
						htmlReferenceNumber.setValue(parentRow.getPaymentDetailFormView().getReferenceNumber());
						bankSelectMenu.setValue(parentRow.getPaymentDetailFormView().getBankId() != null ? 
								                parentRow.getPaymentDetailFormView().getBankId().toString() : "-1");
						paidToSelectMenu.setValue(parentRow.getPaymentDetailFormView().getPaidTo() != null ? 
								                  parentRow.getPaymentDetailFormView().getPaidTo(): "-1");
						htmlReferenceNumber.setValue(parentRow.getPaymentDetailFormView().getReferenceNumber());
						htmlAccountNumber.setValue(parentRow.getPaymentDetailFormView().getAccountNumber() );
						clndrCreatedOn.setValue(parentRow.getPaymentDetailFormView().getDate());
						 if( null!=parentRow.getPaymentDetailFormView().getOther() ) 
						 {
								htmlOtherBeneficiary.setValue(parentRow.getPaymentDetailFormView().getOther());
								paidToSelectMenu.setValue("-1");
							}
						 if( parentRow.getPaymentDetailFormView().getBankId() == null  )
						 {
								htmlReadOnlyBankName.setValue(parentRow.getPaidToBank());
						 }
					}
					else
					{
						htmlAccountNumber.setValue(parentRow.getAccountNumber() );
						htmlOtherBeneficiary.setValue(parentRow.getPaidToName());
						paidToSelectMenu.setValue("-1");
						htmlReadOnlyBankName.setValue(parentRow.getPaidToBank());
					}
				} 
				else if (sessionMap.containsKey(WebConstants.LOAD_PAYMENT_DETAILS))
				{
					DisbursementDetailsView dtView = (DisbursementDetailsView) sessionMap.get(WebConstants.LOAD_PAYMENT_DETAILS);
					sessionMap.remove( WebConstants.LOAD_PAYMENT_DETAILS);
					if( sessionMap.containsKey(WebConstants.SHOW_READONLY_POPUP)) 
					{
						sessionMap.remove(WebConstants.SHOW_READONLY_POPUP);
						disableControls();
					}
					if( null!=dtView ) 
					{
						viewMap.put( WebConstants.LOAD_PAYMENT_DETAILS, dtView);
						List<PaymentDetailsView> listPaymentDetails = dtView.getPaymentDetails();
						if( null!=listPaymentDetails && 0<listPaymentDetails.size() ) 
						{
							PaymentDetailsView paymentView = listPaymentDetails.get( 0);
							if( null!=paymentView.getPaymentMethod() ) {
								paymentModeSelectMenu.setValue(paymentView.getPaymentMethod().toString());
							}
							if( null!=paymentView.getPaidTo() && paymentView.getPaidTo().longValue() != -1L) {
								paidToSelectMenu.setValue( paymentView.getPaidTo().toString() );
							}
							else
							{
								htmlOtherBeneficiary.setValue( paymentView.getOthers() );
								paidToSelectMenu.setValue("-1");
							}
							if( null!=paymentView.getReferenceNo() ) {
								htmlReferenceNumber.setValue(paymentView.getReferenceNo());
							}
							if( null!=paymentView.getAccountNo() ) {
								htmlAccountNumber.setValue(paymentView.getAccountNo());
							}
							
							if( null!=paymentView.getBankId() ) {
								bankSelectMenu.setValue(paymentView.getBankId().toString());
							}
							
							if( null!=paymentView.getRefDate()) {
								clndrCreatedOn.setValue(paymentView.getRefDate());
							}													
						}
					}
					if( null!=request.getParameter( WebConstants.FinanceTabPublishKeys.IS_FINANCE) ) {
						viewMap.put( WebConstants.FinanceTabPublishKeys.IS_FINANCE, true);						
					}
				}				
				else if(sessionMap.containsKey("CollectionTransactionBatch"))
				{
					CollectionProcedureView parentRow = new CollectionProcedureView();
					parentRow = (CollectionProcedureView) sessionMap.get("CollectionTransactionBatch");
					
					if (parentRow.getPaymentCriteriaView()!=null)
					{
						if (parentRow.getPaymentCriteriaView().getPaymentMethodId()!=null)
							paymentModeSelectMenu.setValue( parentRow.getPaymentCriteriaView().getPaymentMethodId().toString() );
						if (parentRow.getPaymentCriteriaView().getReferenceNumber()!=null)
							htmlReferenceNumber.setValue(parentRow.getPaymentCriteriaView().getReferenceNumber());
					     bankSelectMenu.setValue(parentRow.getPaymentCriteriaView().getBankId() != null ? parentRow.getPaymentCriteriaView().getBankId().toString() : "-1");
						 htmlAccountNumber.setValue(parentRow.getPaymentCriteriaView().getAccountNumber());
						 clndrCreatedOn.setValue(parentRow.getPaymentCriteriaView().getDate());
						 if( null!=parentRow.getPaymentCriteriaView().getOther() ) {
								htmlOtherBeneficiary.setValue(parentRow.getPaymentCriteriaView().getOther());
								paidToSelectMenu.setValue("-1");
							}
						 
						 
					}								
				}
				else if ( sessionMap.containsKey( WebConstants.MemsNormalDisbursements.PYMT_DTLS_FRM_POPUP)) {
					PaymentDetailsView paymentView = (PaymentDetailsView) sessionMap.get( WebConstants.MemsNormalDisbursements.PYMT_DTLS_FRM_POPUP);
					sessionMap.remove( WebConstants.MemsNormalDisbursements.PYMT_DTLS_FRM_POPUP);
					viewMap.put( WebConstants.MemsNormalDisbursements.PYMT_DTLS_FRM_POPUP, paymentView);					
					
					if( null!=paymentView.getPaymentMethod() ) {
						paymentModeSelectMenu.setValue(paymentView.getPaymentMethod().toString());
					}
					if( null!=paymentView.getPaidTo() && paymentView.getPaidTo().longValue() != -1L) {
						paidToSelectMenu.setValue( paymentView.getPaidTo().toString() );
					}
					else
					{
						htmlOtherBeneficiary.setValue( paymentView.getOthers() );
						paidToSelectMenu.setValue("-1");
					}
					if( null!=paymentView.getReferenceNo() ) {
						htmlReferenceNumber.setValue(paymentView.getReferenceNo());
					}
					if( null!=paymentView.getAccountNo() ) {
						htmlAccountNumber.setValue(paymentView.getAccountNo());
					}
					if( null!=paymentView.getBankId() ) {
						bankSelectMenu.setValue(paymentView.getBankId().toString());
					}
					if( null!=paymentView.getRefDate() ) {
						clndrCreatedOn.setValue(paymentView.getRefDate());
					}
					
				}
				else if(sessionMap.containsKey(RESEARCH_RECOMMENDATION_ROW)) 
				{
					ResearchRecommendationView researchRecommView = (ResearchRecommendationView)sessionMap.remove( RESEARCH_RECOMMENDATION_ROW);;
					
					viewMap.put(RESEARCH_RECOMMENDATION_ROW, researchRecommView);					
					//researchRecommView.getPeriodId()
					if(researchRecommView.getPaymentDetailFormView()!=null)
					{
						if( null!=researchRecommView.getPaymentDetailFormView().getPaidTo() ) {
							paidToSelectMenu.setValue(researchRecommView.getPaymentDetailFormView().getPaidTo());
						}
						if( null!=researchRecommView.getPaymentDetailFormView().getPaymentMode() ) {
							paymentModeSelectMenu.setValue(researchRecommView.getPaymentDetailFormView().getPaymentMode().toString());
						}
						if( null!=researchRecommView.getPaymentDetailFormView().getReferenceNumber() ) {
							htmlReferenceNumber.setValue(researchRecommView.getPaymentDetailFormView().getReferenceNumber());
						}
						if( null!=researchRecommView.getPaymentDetailFormView().getAccountNumber() ) {
							htmlAccountNumber.setValue(researchRecommView.getPaymentDetailFormView().getAccountNumber());
						}
						if( null!=researchRecommView.getPaymentDetailFormView().getBankId() ) {
							bankSelectMenu.setValue(researchRecommView.getPaymentDetailFormView().getBankId().toString());
						}
						if( null!=researchRecommView.getPaymentDetailFormView().getDate() ) {
							clndrCreatedOn.setValue(researchRecommView.getPaymentDetailFormView().getDate());
						}
						if( null!=researchRecommView.getPaymentDetailFormView().getOther() ) {
							htmlOtherBeneficiary.setValue(researchRecommView.getPaymentDetailFormView().getOther());
							paidToSelectMenu.setValue("-1");
						}
					}
					
				}
				
			}

		} catch (Exception es) {
			logger.LogException( "init| Crashed ", es);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}

	}

	private void disableControls() {
		
		paymentModeSelectMenu.setDisabled(true);
		htmlReferenceNumber.setDisabled(true);
		htmlAccountNumber.setDisabled(true);
		bankSelectMenu.setDisabled(true);
		paidToSelectMenu.setDisabled(true);			
		clndrCreatedOn.setDisabled(true);
		paymentModeSelectMenu.setStyleClass("READONLY");
		htmlReferenceNumber.setStyleClass("READONLY");
		htmlAccountNumber.setStyleClass("READONLY");
		bankSelectMenu.setStyleClass("READONLY");
		paidToSelectMenu.setStyleClass("READONLY");
		clndrCreatedOn.setStyleClass("READONLY");
	}
	
	public boolean getIsContextCollection()
	{
	
		if(viewRootMap.get("context")!=null)
			if(viewRootMap.get("context").toString().compareToIgnoreCase("Collection")==0)
				return false;
			else
				return true;
		else
			return true;
	}
	

	private boolean getRenderingFalse() {
		// TODO Auto-generated method stub
		paidToSelectMenu.setRendered(false);
		return false;
		
	}

	private void putPeriodicDisViewValuesInControl(PeriodicDisbursementGridView parentRow) 
	{

		paymentModeSelectMenu.setValue(parentRow.getPaymentDetailFormView().getPaymentMethodId() != null ? 
																			parentRow.getPaymentDetailFormView().getPaymentMethodId().toString() : "-1");
		bankSelectMenu.setValue(parentRow.getPaymentDetailFormView().getBankId() != null ? parentRow.getPaymentDetailFormView().getBankId().toString() : "-1");
		
		paidToSelectMenu.setValue(parentRow.getPaymentDetailFormView().getPaidTo() != null ? parentRow.getPaymentDetailFormView().getPaidTo(): "-1");
		htmlReferenceNumber.setValue(parentRow.getPaymentDetailFormView().getReferenceNumber());
		htmlAccountNumber.setValue(
										parentRow.getPaymentDetailFormView().getAccountNumber()!=null?
											  parentRow.getPaymentDetailFormView().getAccountNumber():
											  parentRow.getAccountNumber()
			
								  );
		clndrCreatedOn.setValue(parentRow.getPaymentDetailFormView().getDate());
		if(parentRow.getPaymentDetailFormView().getOther()!=null)
		{
			htmlOtherBeneficiary.setValue(parentRow.getPaymentDetailFormView().getOther());
			paidToSelectMenu.setValue("-1");
		}
	}

	private void loadBanksMethod() {

		this.setBankList(new ApplicationBean().getBankList());
		// TODO Auto-generated method stub

	}
	public Boolean getPageModeConfirm()
	{
		Boolean returnVal = false;
		if (viewRootMap.get("PageModeConfirm")!=null)
			returnVal=Boolean.valueOf(viewRootMap.get("PageModeConfirm").toString());
		return returnVal;
			
	}

	private void loadPaymentMethod() throws Exception {

		List<SelectItem> items = new ArrayList<SelectItem>();
		DisbursementService service = new DisbursementService();
		List<PaymentMethodView> paymentMethodView = service
				.getPaymentMethodList();

		for (PaymentMethodView paymentMethodView2 : paymentMethodView) 
		{
			if(paymentMethodView2.getPaymentMethodId().compareTo(WebConstants.PAYMENT_METHOD_CASH_ID)!=0
					&&paymentMethodView2.getPaymentMethodId().compareTo(WebConstants.PAYMENT_METHOD_CREDIT_CARD_ID)!=0 )
			{
				items.add(new SelectItem(paymentMethodView2.getPaymentMethodId().toString(), paymentMethodView2.getDescription()));
			}
				
		}
		this.setPaymentModeList(items);

		// TODO Auto-generated method stub

	}

	private void loadPaidToList( long hasMultipleBeneficiaries) throws PimsBusinessException, Exception 
	{
    	List<SelectItem> selectItems = new ArrayList<SelectItem>();
    	Long personId =  null;
		if ( viewRootMap.containsKey(WebConstants.PERSON_ID ) ) 
		{
		  personId = Long.parseLong( viewRootMap.get(WebConstants.PERSON_ID).toString() );
		}
		HashMap<Long, Person> paidToList = null;
		if( hasMultipleBeneficiaries == 0 )
		{
			if( personId == null  && viewRootMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null)
			{
				paidToList = new UtilityService().getRelatedPersonsByFileId(  viewRootMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID).toString() );
			}
			else 
			{
				paidToList = new UtilityService().getRelatedPersonsByPersonId( personId );
			}
			
		}
		else 
		{
			if( viewMap.get( WebConstants.DISBURSEMENT_DETAIL_ID )!= null )
			{
			  paidToList = new UtilityService().getRelatedPersonsByDisbursememtDetailsId( Long.valueOf( viewMap.get( WebConstants.DISBURSEMENT_DETAIL_ID ).toString() ) );
			}
			if( ( paidToList == null || paidToList.size() == 0 ) && 
				viewRootMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID) != null
			  )
			{
				paidToList = new UtilityService().getRelatedPersonsByFileId(  viewRootMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_ID).toString() );
			}
		}
		Iterator i = paidToList.keySet().iterator();
		while (i.hasNext()) 
		{
			Long key = Long.parseLong( i.next().toString() );
			Person person = paidToList.get(key);
			String append="";
			String name = person.getFullName();
			if(personId!= null && personId.equals(person.getPersonId()))
			{
				if(person.getBank() != null && person.getBank().getBankId() != null)
				{
				 bankSelectMenu.setValue(person.getBank().getBankId().toString());
				}
				else
				{
					bankSelectMenu.setValue("-1");
				}
				htmlAccountNumber.setValue(person.getAccountNumber());
			}
			if( person.getRelation() != null )
			{
				if(person.getRelation().equals(Constant.Relation.NURSE))
				{
					append = CommonUtil.getBundleMessage("commons.nurse");
				}
				else if(person.getRelation().equals(Constant.Relation.GUARDIAN))
				{
					append = CommonUtil.getBundleMessage("commons.guardian");
				}
			}
			else if(person.getIsMinor() != null && person.getIsMinor().equals(1l))
			{
				append = CommonUtil.getBundleMessage("commons.minor");
			}
			
			
			if( append.trim().length()>0 )
			{
			 name+="( " +append+" )";
			}
			selectItems.add( new SelectItem( person.getPersonId().toString(), name) );
		}
		this.setPaidToList(selectItems);
		if( personId!= null )
		{
			paidToSelectMenu.setValue(personId.toString());
		}
	} 

	
	@SuppressWarnings( "unchecked" )
	public void onPaidToChanged()
	{
		try	
		{	
			errorMessages = new ArrayList<String>();
			if(  paymentModeSelectMenu.getValue() == null || 
				!paymentModeSelectMenu.getValue().toString().equals(WebConstants.PAYMENT_METHOD_BANK_TRANSFER_ID.toString())
			  )
			{
				bankSelectMenu.setValue("-1");
				htmlAccountNumber.setValue("");
				htmlOtherBeneficiary.setValue("");
				return;
			}
			String paidTo =paidToSelectMenu.getValue().toString().trim();
			PropertyService ps  = new PropertyService();
			PersonView person = ps.getPersonById(  new Long(paidTo));
			htmlAccountNumber.setValue( person.getAccountNumber() );
			if(person.getBankId() != null)
			{
			  bankSelectMenu.setValue(person.getBankId().toString());
			}
			else
			{
				bankSelectMenu.setValue("-1");
			}
		}
		catch (Exception exception) 
		{
			logger.LogException( "onPaidToChanged --- CRASHED --- ", exception);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}
	public boolean getIsViewModePopUp() {
		boolean returnVal = false;

		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null) {
			if (getIsPageModeSelectOnePopUp() || getIsPageModeSelectManyPopUp())
				returnVal = true;
		}

		else {
			returnVal = false;
		}

		return returnVal;

	}

	public boolean getIsPageModeSelectOnePopUp() {
		isPageModeSelectOnePopUp = false;
		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null
				&& viewRootMap.get(VIEW_MODE).toString().trim()
						.equalsIgnoreCase(MODE_SELECT_ONE_POPUP)) {
			isPageModeSelectOnePopUp = true;
		}
		return isPageModeSelectOnePopUp;

	}

	public boolean getIsPageModeSelectManyPopUp() {
		isPageModeSelectManyPopUp = false;
		if (viewRootMap.containsKey(VIEW_MODE)
				&& viewRootMap.get(VIEW_MODE) != null
				&& viewRootMap.get(VIEW_MODE).toString().trim()
						.equalsIgnoreCase(MODE_SELECT_MANY_POPUP)) {
			isPageModeSelectManyPopUp = true;
		}
		return isPageModeSelectManyPopUp;
	}

	public List<SelectItem> getAssetTypesList() {
		List<SelectItem> assetTypes = new ArrayList<SelectItem>();
		try {

			assetTypes = CommonUtil.getAssetTypesList();

		} catch (PimsBusinessException e) {
			logger.LogException("getAssetTypesList() crashed", e);
		}
		return assetTypes;
	}

	public void doSearch() {
		try {

			// loadDataList();
		} catch (Exception e) {
			System.out.println("Exception doSearch" + e);
		}
	}

	// private List<AssetsGridView> loadDataList() {
	// String methodName = "loadDataList";
	// if (dataList != null)
	// dataList.clear();
	//
	// List<AssetsGridView> list = new ArrayList();
	//
	// try {
	// searchAssetsMap = getSearchCriteria();
	// SearchAssetsService searchService = new SearchAssetsService();
	// //
	// //
	// // /////////////////////////////////////////////// For server side
	// // paging paging/////////////////////////////////////////
	// int totalRows = searchService
	// .searchAssetsGetTotalNumberOfRecords(searchAssetsMap);
	// setTotalRows(totalRows);
	// doPagingComputations();
	// //
	// //////////////////////////////////////////////////////////////////////////////////////////////
	// //
	// //
	//
	// list = searchService.getAssetsByCriteria(searchAssetsMap,
	// getRowsPerPage(), getCurrentPage(), getSortField(),
	// isSortItemListAscending());
	// //
	// if (list.isEmpty() || list == null) {
	// errorMessages = new ArrayList<String>();
	// errorMessages
	// .add(CommonUtil
	// .getBundleMessage(WebConstants.PropertyKeys.Commons.NO_RECORD_FOUND));
	// }
	// Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
	// .getAttributes();
	// viewMap.put("assetList", list);
	// recordSize = totalRows;
	// viewMap.put("recordSize", totalRows);
	// paginatorRows = getPaginatorRows();
	// paginatorMaxPages = recordSize / paginatorRows;
	// if ((recordSize % paginatorRows) > 0)
	// paginatorMaxPages++;
	// if (paginatorMaxPages >= WebConstants.SEARCH_RESULTS_MAX_PAGES)
	// paginatorMaxPages = WebConstants.SEARCH_RESULTS_MAX_PAGES;
	// viewMap.put("paginatorMaxPages", paginatorMaxPages);
	// } catch (Exception ex) {
	// ex.printStackTrace();
	// }
	// return list;
	// // TODO Auto-generated method stub
	//
	// }

	public Integer getPaginatorRows() {
		paginatorRows = WebConstants.RECORDS_PER_PAGE;
		return paginatorRows;
	}

	public ArrayList<SelectItem> getFileStatusList() {
		if (viewRootMap.get("fileStatusList") != null)
			return (ArrayList<SelectItem>) viewRootMap.get("fileStatusList");
		return new ArrayList<SelectItem>();
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getFileTypeList() {
		if (viewRootMap.get("fileTypeList") != null)
			return (ArrayList<SelectItem>) viewRootMap.get("fileTypeList");
		return new ArrayList<SelectItem>();
	}

	public List<AssetsGridView> getDataList() {

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot()
				.getAttributes();
		dataList = (List<AssetsGridView>) viewMap.get("assetList");
		if (dataList == null)
			dataList = new ArrayList<AssetsGridView>();
		return dataList;
	}

	public void setDataList(List<AssetsGridView> dataList) {
		this.dataList = dataList;
	}

	public List<AssetsGridView> getSelectedAssets() {
		logger.logInfo("getSelectedAssets started...");
		List<AssetsGridView> selectedAssetsList = new ArrayList<AssetsGridView>(
				0);
		try {
			dataList = getDataList();
			if (dataList != null && dataList.size() > 0) {
				for (AssetsGridView assetView : dataList)

					if (assetView.getSelected()) {
						selectedAssetsList.add(assetView);
					}

			}

			logger.logInfo("getSelectedAssets completed successfully!!!");
		} catch (Exception exception) {
			logger.LogException("getSelectedAssets() crashed ", exception);
		}
		return selectedAssetsList;
	}

	@SuppressWarnings("unchecked")
	public void sendManyAssetsInfoToParent(javax.faces.event.ActionEvent event)// many
	// assets
	// selected
	{

		List<AssetsGridView> selectedAssets = getSelectedAssets();
		if (!selectedAssets.isEmpty()) {
			// if(getIsContextAuctionUnits())
			// {
			// setResultsForAuctionUnits();
			// }
			// else if(getIsContextInspectionUnits())
			// {
			// setResultsForInspectionUnits();
			// }
			// else
			// {
			sessionMap
					.put(
							WebConstants.AssetSearchOutcomes.ASSETS_SEARCH_SELECTED_MANY_ASSET,
							selectedAssets);

			// }
			String javaScriptText = "javascript:closeWindowSubmit();";
			sendToParent(javaScriptText);
		} else {
			errorMessages = new ArrayList<String>();

		}

	}

	public void sendPaymentInfoToParent(javax.faces.event.ActionEvent event) // single
	{
	// asset
	// selected
		final String methodName = "sendPaymentInfoToParent";
		try
		{
				PaymentCriteriaView currentFormView = getFormParams();
				
				if (viewMap.containsKey(WebConstants.REVIEW_CONFIRM_DISB.REVIEW_CONFIRM_DISB_SELECTED_ROW)) 
				{
					if(validateInput())
					{
					PeriodicDisbursementDetailsView parentRow = null;
					
					parentRow = (PeriodicDisbursementDetailsView) viewMap.get(WebConstants.REVIEW_CONFIRM_DISB.REVIEW_CONFIRM_DISB_SELECTED_ROW);
					
					currentFormView.setAmount(parentRow.getAmount());
					
					parentRow.setPaymentDetailFormView(currentFormView);
					
					sessionMap.put(WebConstants.REVIEW_CONFIRM_DISB.REVIEW_CONFIRM_DISB_SELECTED_ROW,parentRow);
		
					String javaScriptText = "window.opener.populatePaymentDetails(); window.close();";
		
					Map sessionMap = FacesContext.getCurrentInstance()
							.getExternalContext().getSessionMap();
					sessionMap.put(WebConstants.PAYMENT_DETAILS_FORM_CRITERIA.PAYMENT_DETAIL_FORM_VIEW,
									parentRow);
					sendToParent(javaScriptText);
					}
					
				} else if (viewMap.containsKey(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.SELECTED_ROW)) 
				{
					if(validateInput())
					{
					
					PeriodicDisbursementGridView parentRow = null;
					
					parentRow = (PeriodicDisbursementGridView) viewMap.get(WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.SELECTED_ROW);
					
					currentFormView.setAmount(parentRow.getAmount());
					
					parentRow.setPaymentDetailFormView(currentFormView);
					
					sessionMap.put(	WebConstants.SEARCH_PERIODIC_DISBURSEMENT_CRITERIA.SELECTED_ROW,parentRow);
		
					String javaScriptText = "window.opener.populatePaymentDetails(); "
							+ "window.close();";
		
					Map sessionMap = FacesContext.getCurrentInstance()
							.getExternalContext().getSessionMap();
					sessionMap.put(WebConstants.PAYMENT_DETAILS_FORM_CRITERIA.PAYMENT_DETAIL_FORM_VIEW,
									parentRow);
					sendToParent(javaScriptText);
					}
		
				} else if ( viewMap.containsKey( WebConstants.MemsNormalDisbursements.PYMT_DTLS_FRM_POPUP) ) {
					if( validateInput() ) {
						currentFormView.setPaymentMode( paymentModeSelectMenu.getValue().toString());
						PaymentDetailsView paymentView = (PaymentDetailsView) viewMap.get( WebConstants.MemsNormalDisbursements.PYMT_DTLS_FRM_POPUP);
						viewMap.remove( WebConstants.MemsNormalDisbursements.PYMT_DTLS_FRM_POPUP);				
						if( null!=paymentView ) {
							paymentView.setAccountNo( currentFormView.getAccountNumber());
							paymentView.setAmount( currentFormView.getAmount());
							paymentView.setBankId( currentFormView.getBankId());
							if(currentFormView.getPaidTo() != null && currentFormView.getPaidTo().trim().length() > 0 && !currentFormView.getPaidTo().equals("-1") )
							{
							 paymentView.setPaidTo( new Long(currentFormView.getPaidTo()));
							}
							else
							{
								paymentView.setOthers( currentFormView.getOther() );
							}
							paymentView.setRefDate( currentFormView.getDate());
							paymentView.setReferenceNo( currentFormView.getReferenceNumber());	
							paymentView.setPaymentMethod( new Long( currentFormView.getPaymentMode()));
							sessionMap.put( WebConstants.MemsNormalDisbursements.PYMT_DTLS_FRM_POPUP, paymentView);
							String javaScriptText = "window.opener.populatePaymentDetails(); " + "window.close();";
							sendToParent( javaScriptText);
						}
					}							
				} else if ( viewMap.containsKey( WebConstants.LOAD_PAYMENT_DETAILS)) {
					if( validateInput() ) {
						DisbursementDetailsView parentRow = null;			
						parentRow = (DisbursementDetailsView ) viewMap.get(WebConstants.LOAD_PAYMENT_DETAILS);
						currentFormView.setPaymentMode(paymentModeSelectMenu.getValue().toString());
						parentRow.setPaymentCriteriaView(currentFormView);
						if( viewMap.containsKey( WebConstants.FinanceTabPublishKeys.IS_FINANCE)) {
							sessionMap.put( WebConstants.FinanceTabPublishKeys.IS_FINANCE, true);
						}
						sessionMap.put( WebConstants.LOAD_PAYMENT_DETAILS, parentRow);
						String javaScriptText = "window.opener.populatePaymentDetails(); window.close();";	
						sendToParent(javaScriptText);
					}			
				}
				else if(sessionMap.containsKey("CollectionTransactionBatch"))
				{		
					if( validateInput() ) {				
						CollectionProcedureView collView = (CollectionProcedureView)sessionMap.get("CollectionTransactionBatch");
						collView .setPaymentCriteriaView(currentFormView);				
						String javaScriptText = "window.opener.populatePaymentDetails(); " + "window.close();";	
						sendToParent(javaScriptText);
					}			
				}
				else if(viewMap.containsKey(RESEARCH_RECOMMENDATION_ROW))
				{		
					if( validateInput() ) {				
						ResearchRecommendationView rrView = (ResearchRecommendationView)viewMap.get(RESEARCH_RECOMMENDATION_ROW);
						currentFormView.setAmount( rrView.getAmount() );
						if( rrView.getPaymentDetailFormView()!= null )
						{
						currentFormView.setPaymentDetailId( rrView.getPaymentDetailFormView().getPaymentDetailId() );
						}
						rrView.setPaymentDetailFormView(currentFormView);
						
						sessionMap.put(RESEARCH_RECOMMENDATION_ROW,rrView);
						String javaScriptText = "window.opener.populatePaymentDetails(); " + "window.close();";	
						sendToParent(javaScriptText);
					}			
				}
		}
		catch (Exception exception) 
		{
			logger.LogException(methodName + " --- CRASHED --- ", exception);
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}


	}
	public void paymentModeChanged()
	{
		String methodName = "paymentModeChanged|";
		errorMessages = new ArrayList<String>(0);
		try {

			Long valueReturn = new Long(paymentModeSelectMenu.getValue().toString());

			if (valueReturn != null&& valueReturn.longValue() == WebConstants.PAYMENT_METHOD_BANK_TRANSFER_ID)
			{
				viewRootMap.put(WebConstants.PAYMENT_METHOD_BANK_TRANSFER_DISPLAY, true);
				viewRootMap.remove(WebConstants.PAYMENT_METHOD_CHEQUE_DISPLAY);
			}
			else if((valueReturn != null&& valueReturn.longValue() == WebConstants.PAYMENT_METHOD_CHEQUE_ID))
			{
				viewRootMap.put(WebConstants.PAYMENT_METHOD_CHEQUE_DISPLAY, true);
				viewRootMap.remove(WebConstants.PAYMENT_METHOD_BANK_TRANSFER_DISPLAY);
			
			}
			
		} catch (Exception e) {
			errorMessages.add(ResourceUtil.getInstance().getProperty(
					MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
			logger.LogException(methodName + " Error Occured...", e);

		}

		
	}


	private boolean validateInput() throws Exception 
	{
		boolean bValid = true;
			String emptyValue="-1";
			Long valueReturn = new Long(paymentModeSelectMenu.getValue().toString());
//			if(valueReturn != null&& valueReturn.longValue() == WebConstants.PAYMENT_METHOD_BANK_TRANSFER_ID)
//			{
//				if(htmlAccountNumber==null || htmlAccountNumber.getValue().toString().equals(""))
//				{
//					errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentPopupMsgs.ERR_ACCT_NO_REQ));
//					bValid=false;
//				}
//			}
			
//			if((valueReturn != null&& valueReturn.longValue() == WebConstants.PAYMENT_METHOD_CHEQUE_ID))
//			{
//				if(htmlReferenceNumber==null || htmlReferenceNumber.getValue().toString().equals(""))
//				{
//					errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentPopupMsgs.ERR_REF_NO_REQ));
//					bValid=false;
//				}
//			}
			String paidTo =paidToSelectMenu.getValue().toString().trim();
			if(paidTo!=null && paidTo.equals(emptyValue))
			{
				if(htmlOtherBeneficiary.getValue().toString().trim().equals(""))
				{
					errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentPopupMsgs.ERR_NO_BENEFICIARY_NAME));
					bValid=false;	
				}
			}
								
		
		return bValid;
	}
	
	private boolean validateInputForCollProc() {
		logger.logInfo("[validateInput() .. Starts]");
		boolean bValid = true;
		try {
			if( null==htmlReferenceNumber.getValue() || 0==htmlReferenceNumber.getValue().toString().trim().length() ) {
				errorMessages.add(CommonUtil.getBundleMessage(
										MessageConstants.MemsPaymentPopupMsgs.ERR_REF_NO_REQ));
				bValid=false;
			}
			
			if( null==htmlAccountNumber.getValue() || 0==htmlAccountNumber.getValue().toString().trim().length()) {
				errorMessages.add(CommonUtil.getBundleMessage(
									MessageConstants.MemsPaymentPopupMsgs.ERR_ACCT_NO_REQ));	
				bValid=false;
			}
			if( null==bankSelectMenu.getValue() || bankSelectMenu.getValue().toString().equals("-1") ) {
				errorMessages.add(CommonUtil.getBundleMessage(
						MessageConstants.MemsPaymentPopupMsgs.ERR_BANK_ID_REQ));	
				bValid=false;
			}
			
			if( null==clndrCreatedOn.getValue() || clndrCreatedOn.getValue().toString().equals("") ) {
				errorMessages.add(CommonUtil.getBundleMessage(
						MessageConstants.MemsPaymentPopupMsgs.ERR_REF_DATE_REQ));				
				bValid=false;
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
		logger.logInfo("[validateInput() .. Ends]");
		return bValid;
	}
	
	
	@SuppressWarnings("unchecked")
	private PaymentCriteriaView getFormParams() {
		formParamMap = new HashMap<String, Object>();
		PaymentCriteriaView formView = new PaymentCriteriaView();
		String emptyValue = "-1";

		Object paymentMode = paymentModeSelectMenu.getValue();
		Object paidTo = paidToSelectMenu.getValue();
		Object otherBeneficiaryName = htmlOtherBeneficiary.getValue();
		Object bank = bankSelectMenu.getValue();
		Object referenceNumber = htmlReferenceNumber.getValue();
		Date date =  (Date)clndrCreatedOn.getValue();
		Object accountNumber = htmlAccountNumber.getValue();
		try {

			if (paymentMode != null
					&& !paymentMode.toString().trim().equals("")
					&& !paymentMode.toString().trim().equals(emptyValue)) {
				formView.setPaymentMethodId(Long.parseLong(paymentMode
						.toString()));
				formParamMap.put(
						WebConstants.PAYMENT_MODE_FORM_PARAMS.PAY_MODE,
						paymentMode.toString());
			}
			if (paidTo != null && !paidTo.toString().trim().equals("")&& !paidTo.toString().trim().equals(emptyValue)) 
			{
				formView.setPaidTo(paidTo.toString());
				formParamMap.put(WebConstants.PAYMENT_MODE_FORM_PARAMS.PAID_TO,
						paidTo.toString());
			}
			else if (paidTo.toString().trim().equals(emptyValue))
			{
				if(otherBeneficiaryName!=null && !otherBeneficiaryName.toString().trim().equals("") )
				{
					formView.setOther(otherBeneficiaryName.toString());
				}
				
			}
			if (bank != null && !bank.toString().trim().equals("")
					&& !bank.toString().trim().equals(emptyValue)) {
				formView.setBankId(Long.parseLong(bank.toString()));
				formParamMap.put(WebConstants.PAYMENT_MODE_FORM_PARAMS.BANK,
						bank.toString());
			}
			if (referenceNumber != null
					&& !referenceNumber.toString().trim().equals("")
					&& !referenceNumber.toString().trim().equals(emptyValue)) {
				formView.setReferenceNumber(referenceNumber.toString());
				formParamMap.put(
						WebConstants.PAYMENT_MODE_FORM_PARAMS.REFERENCE_NUMBER,
						referenceNumber.toString());
			}

			if (date != null && !date.toString().trim().equals("")
					&& !date.toString().trim().equals(emptyValue)) {
				DateFormat df = new SimpleDateFormat(getDateFormat());
				formView.setDate(date);
				formParamMap.put(WebConstants.PAYMENT_MODE_FORM_PARAMS.DATE,
						date.toString());
			}
			if (accountNumber != null
					&& !accountNumber.toString().trim().equals("")
					&& !accountNumber.toString().trim().equals(emptyValue)) {
				formView.setAccountNumber(accountNumber.toString());
				formParamMap.put(
						WebConstants.PAYMENT_MODE_FORM_PARAMS.ACCOUNT_NUMBER,
						accountNumber.toString());
			}
		} catch (Exception ex) {
			System.out.println(ex);
		}

		return formView;
	}


	public String sendToParent(String javaScript) {
		final String viewId = "/ReviewConfirmDisbursements.jsp";
		// logger.logInfo(methodName+"|"+"Start..");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ViewHandler viewHandler = facesContext.getApplication()
				.getViewHandler();
		String actionUrl = viewHandler.getActionURL(facesContext, viewId);
		String javaScriptText = javaScript;

		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,
				AddResource.HEADER_BEGIN, javaScriptText);
		// logger.logInfo(methodName+"|"+"Finish..");
		return "";
	}

	public HtmlSelectOneMenu getPaymentModeSelectMenu() {
		return paymentModeSelectMenu;
	}

	public void setPaymentModeSelectMenu(HtmlSelectOneMenu paymentModeSelectMenu) {
		this.paymentModeSelectMenu = paymentModeSelectMenu;
	}

	public HtmlSelectOneMenu getPaidToSelectMenu() {
		return paidToSelectMenu;
	}

	public void setPaidToSelectMenu(HtmlSelectOneMenu paidToSelectMenu) {
		this.paidToSelectMenu = paidToSelectMenu;
	}

	public HtmlInputText getHtmlReferenceNumber() {
		return htmlReferenceNumber;
	}

	public void setHtmlReferenceNumber(HtmlInputText htmlReferenceNumber) {
		this.htmlReferenceNumber = htmlReferenceNumber;
	}

	public HtmlSelectOneMenu getBankSelectMenu() {
		return bankSelectMenu;
	}

	public void setBankSelectMenu(HtmlSelectOneMenu bankSelectMenu) {
		this.bankSelectMenu = bankSelectMenu;
	}

	public HtmlInputText getHtmlDate() {
		return htmlDate;
	}

	public void setHtmlDate(HtmlInputText htmlDate) {
		this.htmlDate = htmlDate;
	}

	public HtmlInputText getHtmlAccountNumber() {
		return htmlAccountNumber;
	}

	public void setHtmlAccountNumber(HtmlInputText htmlAccountNumber) {
		this.htmlAccountNumber = htmlAccountNumber;
	}

	public List<SelectItem> getPaidToList() {
		if (viewRootMap.get("paidToList") != null)
			return (ArrayList<SelectItem>) viewRootMap.get("paidToList");
		return new ArrayList<SelectItem>();

	}

	public void setPaidToList(List<SelectItem> paidToList) {
		this.paidToList = paidToList;
		if (this.paidToList != null)
			viewRootMap.put("paidToList", this.paidToList);
	}

	public List<SelectItem> getPaymentModeList() {

		if (viewRootMap.get("paymentMethodList") != null)
			return (ArrayList<SelectItem>) viewRootMap.get("paymentMethodList");
		return new ArrayList<SelectItem>();

	}

	public void setPaymentModeList(List<SelectItem> paymentModeList) {
		this.paymentModeList = paymentModeList;
		if (this.paymentModeList != null)
			viewRootMap.put("paymentMethodList", this.paymentModeList);

	}

	public List<SelectItem> getBankList() {
		if (viewRootMap.get("bankList") != null)
			return (ArrayList<SelectItem>) viewRootMap.get("bankList");
		return new ArrayList<SelectItem>();
	}

	public void setBankList(List<SelectItem> bankList) {
		this.bankList = bankList;
		if (this.bankList != null)
			viewRootMap.put("bankList", this.bankList);
	}

	public HtmlCalendar getClndrCreatedOn() {
		return clndrCreatedOn;
	}

	public void setClndrCreatedOn(HtmlCalendar clndrCreatedOn) {
		this.clndrCreatedOn = clndrCreatedOn;
	}

	public HtmlInputText getHtmlOtherBeneficiary() {
		return htmlOtherBeneficiary;
	}

	public void setHtmlOtherBeneficiary(HtmlInputText htmlOtherBeneficiary) {
		this.htmlOtherBeneficiary = htmlOtherBeneficiary;
	}
	
//	public Boolean paidToListChanged()
//	{
//		if(paidToSelectMenu.getValue().toString().trim().equals("-1"))
//			return true;
//		else 
//			return false;
//			
//			
//	}

	public HtmlOutputLabel getHtmlOtherBeneficiaryLbl() {
		return htmlOtherBeneficiaryLbl;
	}

	public void setHtmlOtherBeneficiaryLbl(HtmlOutputLabel htmlOtherBeneficiaryLbl) {
		this.htmlOtherBeneficiaryLbl = htmlOtherBeneficiaryLbl;
	}

	public HtmlInputText getHtmlReadOnlyBankName() {
		return htmlReadOnlyBankName;
	}

	public void setHtmlReadOnlyBankName(HtmlInputText htmlReadOnlyBankName) {
		this.htmlReadOnlyBankName = htmlReadOnlyBankName;
	}
	

}
