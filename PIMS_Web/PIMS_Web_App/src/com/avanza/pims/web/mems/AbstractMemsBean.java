package com.avanza.pims.web.mems;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.dao.GRPManager;
import com.avanza.pims.entity.FinancialTransaction;
import com.avanza.pims.entity.UnusedUnblocked;
import com.avanza.pims.entity.XxArReceiptAmaf;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.GRP.GRPService;
import com.avanza.pims.ws.mems.BlockingAmountService;
import com.avanza.ui.util.ResourceUtil;

@SuppressWarnings("serial")
public class AbstractMemsBean extends AbstractController implements Serializable{

	FacesContext context;
	@SuppressWarnings("unchecked")
	protected Map sessionMap;
	@SuppressWarnings("unchecked")
	protected Map viewMap;
	protected HtmlInputText txtFileOwner = new HtmlInputText();
	protected List<String> errorMessages;
	protected List<String> successMessages;
	protected String dateFormat;
	protected String locale;
	protected Logger logger;
	protected Integer paginatorMaxPages = 0;
	protected Integer paginatorRows = 0;
	protected Integer recordSize = 0;
	protected HtmlInputText availableBalance = new HtmlInputText();
	protected HtmlInputText requestedAmount = new HtmlInputText();
	public void init() 
	{
	}
	public AbstractMemsBean()
	{
		context = FacesContext.getCurrentInstance();
		sessionMap = context.getExternalContext().getSessionMap();
		viewMap = getFacesContext().getViewRoot().getAttributes();
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		logger = Logger.getLogger(this.getClass());
	}
	public String getErrorMessages()
	{
		return CommonUtil.getErrorMessages(errorMessages);
	}
	public String getSuccessMessages()
	{
		String messageList="";
		if ((successMessages== null) || (successMessages.size() == 0)) 
		{
			messageList = "";
		}
		else
		{
			
			for (String message : successMessages) 
				{
					messageList +=  "<LI>" +message+ "<br></br>" ;
			    }
			
		}
		return (messageList);
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public void setSuccessMessages(List<String> successMessages) {
		this.successMessages = successMessages;
	}
	public String getDateFormat()
	{
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	@SuppressWarnings("static-access")
	public String getLocale(){
		return new CommonUtil().getLocale();
	}
	public boolean isEnglishLocale()
	{
		return CommonUtil.getIsEnglishLocale();
	}
	
	public Integer getPaginatorRows() {		
		return WebConstants.RECORDS_PER_PAGE;
	}
	
	public Integer getPaginatorMaxPages() {
		return WebConstants.SEARCH_RESULTS_MAX_PAGES;
	}
	public Integer getRecordSize() {
		return recordSize;
	}
	public void setRecordSize(Integer recordSize) {
		this.recordSize = recordSize;
	}
	public void setPaginatorMaxPages(Integer paginatorMaxPages) {
		this.paginatorMaxPages = paginatorMaxPages;
	}
	public void setPaginatorRows(Integer paginatorRows) {
		this.paginatorRows = paginatorRows;
	}
	
	public TimeZone getTimeZone()
	{
		    return CommonUtil.getTimeZone();
	}
	public String executeJavaScript(String javaScript) throws Exception
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		AddResource addResource = AddResourceFactory.getInstance(facesContext);
		addResource.addInlineScriptAtPosition(facesContext,AddResource.HEADER_BEGIN, javaScript);
		return "";
	}
	

	/**
	 *  // 0 means no error
	    // 1 means balance is not enough
		// 2 means balance - blocking is not enough. In this case at starting point we will only show message and proceed with request
		// 3 means exception
		
	 */
	public int validateBalance( 
			double balanceAmnt,
			double blockingAmnt ,
			double pendingDisbursementAmount, 
			double amount  ,
			Long personId,
			String name,
		    Long inheritanceFileId,
		    Long requestTypeId
		) throws Exception
	{
		return validateAccountBalance(balanceAmnt, blockingAmnt,
		pendingDisbursementAmount, amount, personId, name,
		inheritanceFileId,requestTypeId);
	}
	public int validateBalance( 
								double balanceAmnt,
								double blockingAmnt ,
								double pendingDisbursementAmount, 
								double amount  ,
								Long personId,
								String name,
							    Long inheritanceFileId	
							) throws Exception
	{
			return validateAccountBalance(balanceAmnt, blockingAmnt,
					pendingDisbursementAmount, amount, personId, name,
					inheritanceFileId,null);
	}
	/**
	 * @param balanceAmnt
	 * @param blockingAmnt
	 * @param pendingDisbursementAmount
	 * @param amount
	 * @param personId
	 * @param name
	 * @param inheritanceFileId
	 * @return
	 * @throws Exception
	 */
	private int validateAccountBalance(double balanceAmnt, double blockingAmnt,
			double pendingDisbursementAmount, double amount, Long personId,
			String name, Long inheritanceFileId,Long requestTypeId) throws Exception 
	{
		try 
		{
			if( 
				(
				 requestTypeId == null ||
				 requestTypeId.compareTo(WebConstants.MemsRequestType.COLLECTION_DISBURSEMENT) != 0 
				)
				&&	
				amount > balanceAmnt 
			  ) 
			{
				if( name == null || name.length() <= 0  )
				{
					errorMessages.add( CommonUtil.getBundleMessage( MessageConstants.MemsPaymentDisbursementMsgs.ERR_AMNT_GREATER));
				}
				else
				{
					
					
					String msg = java.text.MessageFormat.format(
															CommonUtil.getBundleMessage(  "disburseFinance.msg.balanceNotEnough"  ),
															name+" ("+balanceAmnt+")"
														);
					errorMessages.add( msg  );

				}
				return 1;
			}
			else 
			{
				String msg ;
				if( name == null || name.length() <= 0 )
				{
					msg = CommonUtil.getBundleMessage(  "mems.payment.err.msg.amntGreaterThanBalanceBlocking"  );
				}
				else
				{
					msg = CommonUtil.getBundleMessage(  "mems.payment.err.msg.amntGreaterThanBalanceBlockingWithName"  );
				}	
				boolean error =false;
				DecimalFormat df = new DecimalFormat("#.00");
				//if balance is greater than sum of blocking and pending disbursement amount
				if ( balanceAmnt > (blockingAmnt + pendingDisbursementAmount) )
				{
					//if current disbursement amount is less than balanceAmnt- ( blockingAmnt + pendingDisbursementAmount ) then can not proceed
					error =  amount > new Double( df.format( balanceAmnt- ( blockingAmnt + pendingDisbursementAmount ) ) );
				}
				//if balance is less then blocking amount and pendingDisbursementAmount then can not proceed
				else 
				{
					error  = true;
				}
				//Even if error is true we will try to check if there is unused unblocked amount and its equal to or greater than request amount
				// then we will allow. This check is made for the case when blocking amount is more than balance and some of blocking amount
				// has been unblocked for e.g. 
				//balance:500
				//blocking initial:800
				//unblocked :200
				//blocking after unblock : 600
				// if requested amount :200 then in this case we should allow disbursement but if only above validations are done and below arent then
			    // system will not allow disbursement
				//if error is true then check 
			
				if( error )
				{
					error  = false;
					//if current disb amount is greater then balance - pendingDisbursementAmount amount then can not proceed
					if (
							(
								requestTypeId == null ||
							    requestTypeId.compareTo(WebConstants.MemsRequestType.COLLECTION_DISBURSEMENT) != 0 
							 )
							 &&
							amount > new Double( df.format( balanceAmnt- pendingDisbursementAmount  ) ) )
					{
						error = true;
					}
					else if( 
										requestTypeId != null &&
										requestTypeId.compareTo(WebConstants.MemsRequestType.COLLECTION_DISBURSEMENT) == 0 &&
										 amount > new Double( df.format( balanceAmnt- ( blockingAmnt + pendingDisbursementAmount ) ) )
						   )
					{
						error = true;			
					}
					//if amount is less or equal then check
					else 
					{
						UnusedUnblocked unusedUnblocked = BlockingAmountService.getUnusedUnblocked(personId,null,inheritanceFileId);
						//if unused unblock amount is not present or amount is greater than unused unblock amount then cannot proceed
						//else allow
						
						if(unusedUnblocked == null || amount > unusedUnblocked.getAmount())
						{
							error = true;
						}
					}
					
				}
				if( error )
				{
					if( name == null || name.length() <= 0 )
					{
						msg = java.text.MessageFormat.format(
															  msg, 
															  amount,balanceAmnt,
															  blockingAmnt,
															  new Double( df.format( balanceAmnt-(blockingAmnt + pendingDisbursementAmount) ) ),
															  pendingDisbursementAmount
															 ) ;
					}
					else
					{
						msg = java.text.MessageFormat.format(
															   msg, 
															   amount,
															   balanceAmnt,
															   blockingAmnt,
															   new Double( df.format( balanceAmnt-(blockingAmnt + pendingDisbursementAmount)) ),
															   name ,
															   pendingDisbursementAmount
															) ;
					}
					errorMessages.add( msg  );
					return 2;
				}

				return 0;
			}
		} 
		catch(NumberFormatException ex) 
		{
			errorMessages.add(CommonUtil.getBundleMessage(MessageConstants.MemsPaymentDisbursementMsgs.ERR_AMNT_NUM));
			return 3;
		}
	}
	
	public HtmlInputText getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(HtmlInputText availableBalance) {
		this.availableBalance = availableBalance;
	}
	public HtmlInputText getRequestedAmount() {
		return requestedAmount;
	}
	public void setRequestedAmount(HtmlInputText requestedAmount) {
		this.requestedAmount = requestedAmount;
	}	

	
	public Boolean getIsTaskPresent()
	{
		return viewMap.get( WebConstants.TASK_LIST_SELECTED_USER_TASK )!= null;
//		return true;
	}
	public HtmlInputText getTxtFileOwner() {
		return txtFileOwner;
	}
	public void setTxtFileOwner(HtmlInputText txtFileOwner) {
		this.txtFileOwner = txtFileOwner;
	}

	
	protected boolean hasPaymentARPostedToGRP(long collectionTrxId)throws Exception
	{
		boolean hasError  = true;
		
		FinancialTransaction ftAR =  GRPManager.getARFinTrxForGL(collectionTrxId);
		boolean arFTNotPosted = (ftAR == null ||ftAR.getFinancialTransactionId() == null || ftAR.getGrpStatusId().compareTo( 10801l ) == 0);
		if( arFTNotPosted )
		{
			errorMessages.add(ResourceUtil.getInstance().getProperty("grp.msg.ARForPaymentNotPostedToGRP"));
			hasError = false;
		}
		else if( ftAR != null && ( ftAR.getMigrated() == null || ftAR.getMigrated().compareTo(0l)==0 ) ) 
		{
			logger.logDebug("hasPaymentARPostedToGRP| financialTransactionId:%s",ftAR.getFinancialTransactionId() );
			XxArReceiptAmaf xx = GRPManager.getGRPReceiptForFinancialTransaction(ftAR);
			if(xx == null || xx.getAttribute1()== null)
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty("grp.msg.ARForPaymentPostedToGRPNotProcessed"));
				hasError = false;
			}
		}
		
		return hasError;
	}
}
