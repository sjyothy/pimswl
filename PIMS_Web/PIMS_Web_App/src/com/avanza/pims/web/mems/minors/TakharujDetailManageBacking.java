package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.ApplicationBean;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.InheritedAssetSharesView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.TakharujDetailView;
import com.avanza.pims.ws.vo.mems.TakharujInheritedAssetView;

public class TakharujDetailManageBacking extends AbstractMemsBean {
	
	private static final long serialVersionUID = 6740905670990704192L;
	
	private TakharujInheritedAssetView takharujInheritedAssetView;
	private List<TakharujDetailView> takharujDetailViewList;
	private Map<String, InheritedAssetSharesView> inheritedAssetSharesViewMap;
	private String selectedFromInheritedAssetSharesId;
	private String selectedFromShare;
	private PersonView toPersonView;
	private String selectedToInheritedAssetSharesId;
	private String selectedToShare;
	private String takharujShare;
	private Boolean popModeView;
	private HtmlDataTable takharujDetailViewDataTable;
	private List<SelectItem> beneficiarySelectItems;
	private String remainingShare;
	
	private PropertyServiceAgent propertyServiceAgent;
	
	public TakharujDetailManageBacking() {		
		
		takharujInheritedAssetView = new TakharujInheritedAssetView();
		takharujDetailViewList = new ArrayList<TakharujDetailView>(0);
		selectedFromInheritedAssetSharesId = "";
		selectedFromShare = "";
		toPersonView = new PersonView();
		selectedToInheritedAssetSharesId = "";
		selectedToShare = "";
		takharujShare = "";
		remainingShare = "";
		takharujDetailViewDataTable = new HtmlDataTable();
		beneficiarySelectItems = new ArrayList<SelectItem>(0);
		
		propertyServiceAgent = new PropertyServiceAgent();
	}
	
	@Override
	public void init() 
	{
		String METHOD_NAME = "init()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAKHARUJ INHERITED ASSET VIEW
		if ( viewMap.get( WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW ) != null )
		{
			takharujInheritedAssetView = ( TakharujInheritedAssetView ) viewMap.get( WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW );
		}
		else if ( sessionMap.get( WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW ) != null )
		{
			takharujInheritedAssetView = ( TakharujInheritedAssetView ) sessionMap.get( WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW );
			sessionMap.remove( WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW );
			
			if ( takharujInheritedAssetView != null && takharujInheritedAssetView.getTakharujDetailViewList() != null )
			{
				takharujDetailViewList = takharujInheritedAssetView.getTakharujDetailViewList();
			}
		}
		if( sessionMap.get( WebConstants.Takharuj.TAKHARUJ_DETAIL_POPUP_VIEW ) != null )
		{
			
			popModeView = true;
			sessionMap.remove( WebConstants.Takharuj.TAKHARUJ_DETAIL_POPUP_VIEW );
		}
		// TAKHARUJ DETAIL VIEW LIST
		if ( viewMap.get( WebConstants.Takharuj.TAKHARUJ_DETAIL_VIEW_LIST ) != null )
		{
			takharujDetailViewList = ( List<TakharujDetailView> ) viewMap.get( WebConstants.Takharuj.TAKHARUJ_DETAIL_VIEW_LIST );
		}
		
		// FROM BENEFICIARY SELECT ITEMS
		if ( viewMap.get( WebConstants.Takharuj.BENEFICIARY_SELECT_ITEMS ) != null )
		{
			beneficiarySelectItems = (List<SelectItem>) viewMap.get( WebConstants.Takharuj.BENEFICIARY_SELECT_ITEMS );
		}		
		
		// INHERITED ASSET SHARE VIEW MAP
		if ( viewMap.get( WebConstants.Takharuj.INHERITED_ASSET_SHARES_VIEW_MAP ) != null )
		{
			inheritedAssetSharesViewMap = (Map<String, InheritedAssetSharesView>) viewMap.get( WebConstants.Takharuj.INHERITED_ASSET_SHARES_VIEW_MAP );
		}
		else if ( takharujInheritedAssetView != null && takharujInheritedAssetView.getInheritedAssetView() != null && takharujInheritedAssetView.getInheritedAssetView().getInheritedAssetId() != null )
		{
			InheritedAssetSharesView inheritedAssetSharesView = new InheritedAssetSharesView();
			inheritedAssetSharesView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
			inheritedAssetSharesView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
			inheritedAssetSharesView.setInheritedAssetView( takharujInheritedAssetView.getInheritedAssetView() );
			
			inheritedAssetSharesViewMap = ApplicationBean.getInheritedAssetShareViewMap(inheritedAssetSharesView, beneficiarySelectItems);
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAKHARUJ INHERITED ASSET VIEW
		if ( takharujInheritedAssetView != null )
		{
			viewMap.put( WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW, takharujInheritedAssetView );
		}
		
		// TAKHARUJ DETAIL VIEW LIST
		if ( takharujDetailViewList != null )
		{
			viewMap.put( WebConstants.Takharuj.TAKHARUJ_DETAIL_VIEW_LIST, takharujDetailViewList );
		}
		
		// FROM BENEFICIARY SELECT ITEMS
		if ( beneficiarySelectItems != null )
		{
			viewMap.put( WebConstants.Takharuj.BENEFICIARY_SELECT_ITEMS, beneficiarySelectItems );
		}		
		
		// INHERITED ASSET SHARE VIEW MAP
		if ( inheritedAssetSharesViewMap != null )
		{
			viewMap.put( WebConstants.Takharuj.INHERITED_ASSET_SHARES_VIEW_MAP, inheritedAssetSharesViewMap );
		}		
		viewMap.put( WebConstants.Takharuj.TAKHARUJ_DETAIL_POPUP_VIEW , popModeView != null  ?true:false );
	}
	
	public int getTakharujDetailViewListRecordSize()
	{
		int size = 0;
		
		for ( TakharujDetailView takharujDetailView : takharujDetailViewList )
			if ( takharujDetailView.getIsDeleted().equals( WebConstants.DEFAULT_IS_DELETED ) )
				size++;
		
		return size;
	}
	
	public void onFromBeneficiaryChange()
	{
		if ( selectedFromInheritedAssetSharesId != null && ! selectedFromInheritedAssetSharesId.equalsIgnoreCase("") )
		{
			InheritedAssetSharesView selectedInheritedAssetSharesView = inheritedAssetSharesViewMap.get( selectedFromInheritedAssetSharesId );
			selectedFromShare = String.valueOf( selectedInheritedAssetSharesView.getSharePercentage() );
			try {
				remainingShare = ""+ (selectedInheritedAssetSharesView.getSharePercentage() - getTakharujedFromBeneficiaryShare(selectedInheritedAssetSharesView.getInheritanceBeneficiaryView().getBeneficiary().getPersonId()));
			} catch (Exception e) {
				logger.logError("onFromBeneficiaryChange crashed |"+e);
			}
		}
		else
		{
			selectedFromShare = "";
			remainingShare= "";
		}
	}
	
	@SuppressWarnings("unchecked")
	public void onToBeneficiaryChange() throws PimsBusinessException
	{
		if ( toPersonView != null && toPersonView.getPersonId() != null )
		{
			toPersonView = propertyServiceAgent.getPersonInformation( toPersonView.getPersonId() );
			selectedToInheritedAssetSharesId = "";
			selectedToShare = "0.00";
			
			if ( inheritedAssetSharesViewMap != null && inheritedAssetSharesViewMap.size() > 0 )
			{
				for ( InheritedAssetSharesView inheritedAssetSharesView : inheritedAssetSharesViewMap.values() )
				{
					// Checking if the selected to person is already a beneficiary in the selected file & selected asset
					if ( inheritedAssetSharesView != null 
							&& inheritedAssetSharesView.getInheritedAssetSharesId() != null
							&& inheritedAssetSharesView.getInheritanceBeneficiaryView() != null 
							&& inheritedAssetSharesView.getInheritanceBeneficiaryView().getBeneficiary() != null 
							&& inheritedAssetSharesView.getInheritanceBeneficiaryView().getBeneficiary().getPersonId() != null							
							&& toPersonView.getPersonId().equals( inheritedAssetSharesView.getInheritanceBeneficiaryView().getBeneficiary().getPersonId() ) )
					{
						selectedToInheritedAssetSharesId = String.valueOf( inheritedAssetSharesView.getInheritedAssetSharesId() );
						selectedToShare = String.valueOf( inheritedAssetSharesView.getSharePercentage() );
						break;
					}
				}
			}
			viewMap.put("SELECTED_INHERITED_ASSET_SHARE_ID", selectedToInheritedAssetSharesId);
		}
	}
	
	private boolean isValid() 
	{
		boolean isValid = true;
		
		if ( selectedFromInheritedAssetSharesId == null || selectedFromInheritedAssetSharesId.equalsIgnoreCase("") )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.takharuj.detail.from.beneficiary.empty" ) );
			isValid = false;
		}
		
		if ( toPersonView == null || toPersonView.getPersonId() == null )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.takharuj.detail.to.beneficiary.empty" ) );
			isValid = false;
		}
		
		if ( takharujShare == null || takharujShare.equalsIgnoreCase("") )
		{
			errorMessages.add( CommonUtil.getBundleMessage( "err.takharuj.detail.takharuj.share.empty" ) );
			isValid = false;
		}
		
		return isValid;
	}
	
	public String onTakharujDetailAdd()
	{
		final String METHOD_NAME = "onTakharujDetailAdd()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			if ( isValid() )
			{
				TakharujDetailView takharujDetailView = new TakharujDetailView();
				
				
				// Takharuj Inherited Asset
				takharujDetailView.setTakharujInheritedAssetId( takharujInheritedAssetView.getTakharujInheritedAssetId() );
				
				
				// From Beneficiary
				if ( selectedFromInheritedAssetSharesId != null && ! selectedFromInheritedAssetSharesId.equalsIgnoreCase("") )
				{
					InheritedAssetSharesView fromInheritedAssetSharesView = inheritedAssetSharesViewMap.get( selectedFromInheritedAssetSharesId );
					
					if ( fromInheritedAssetSharesView != null && fromInheritedAssetSharesView.getInheritedAssetSharesId() != null )
					{
						takharujDetailView.setFromInheritedAssetSharesView( fromInheritedAssetSharesView );
					}
				}
				
				// From Share
				if ( selectedFromShare != null && ! selectedFromShare.equalsIgnoreCase("") )
				{
					takharujDetailView.setFromSharePercentage( Double.valueOf( selectedFromShare ) );
				}			
				
				
				// To Beneficiary			
				if ( viewMap.get("SELECTED_INHERITED_ASSET_SHARE_ID") != null && viewMap.get("SELECTED_INHERITED_ASSET_SHARE_ID").toString().length() > 0)
				{
					InheritedAssetSharesView toInheritedAssetSharesView = inheritedAssetSharesViewMap.get( viewMap.get("SELECTED_INHERITED_ASSET_SHARE_ID").toString() );
					
					if ( toInheritedAssetSharesView != null && toInheritedAssetSharesView.getInheritedAssetSharesId() != null )
					{
						takharujDetailView.setToInheritedAssetSharesView( toInheritedAssetSharesView );
					}
				}
				
				// To Share			
				if ( selectedToShare != null && ! selectedToShare.equalsIgnoreCase("") )
				{
					takharujDetailView.setToSharePercentage( Double.valueOf( selectedToShare ) );
				}
				
				
				// To Person
				if ( toPersonView != null && toPersonView.getPersonId() != null )
				{
					toPersonView = propertyServiceAgent.getPersonInformation( toPersonView.getPersonId() );
					takharujDetailView.setToPersonView( toPersonView );
				}
				
				
				// Takharuj Share
				if ( takharujShare != null && ! takharujShare.equalsIgnoreCase("") )
				{
					takharujDetailView.setTakharujShare( Double.valueOf( takharujShare ) );				
				}
				
				takharujDetailView.setCreatedBy( CommonUtil.getLoggedInUser() );
				takharujDetailView.setCreatedOn( new Date() );
				takharujDetailView.setUpdatedBy( CommonUtil.getLoggedInUser() );
				takharujDetailView.setUpdatedOn( new Date() );
				takharujDetailView.setIsDeleted( WebConstants.DEFAULT_IS_DELETED );
				takharujDetailView.setRecordStatus( WebConstants.DEFAULT_RECORD_STATUS );
				
				takharujDetailViewList.add( takharujDetailView );
				
				selectedFromInheritedAssetSharesId = "";
				selectedFromShare = "";
				toPersonView = new PersonView();
				selectedToInheritedAssetSharesId = "";
				selectedToShare = "";
				takharujShare = "";
				
				updateValuesToMaps();
			}
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return null;
	}
	
	public String onTakharujDetailDelete()
	{
		TakharujDetailView selectedTakharujDetailView = (TakharujDetailView) takharujDetailViewDataTable.getRowData();
		
		if ( selectedTakharujDetailView.getTakharujDetailId() == null )
			takharujDetailViewList.remove( selectedTakharujDetailView );
		else
			selectedTakharujDetailView.setIsDeleted( 1L );
		
		updateValuesToMaps();
		
		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	public String onTakharujDetailDone()
	{	
		sessionMap.put(WebConstants.Takharuj.TAKHARUJ_INHERITED_ASSET_VIEW, takharujInheritedAssetView);
		executeJavascript("closeAndPassTakharujInheritedAssetView();");
		return null;
	}
	
	private void executeJavascript(String javascript) 
	{
		String METHOD_NAME = "executeJavascript()"; 
		logger.logInfo(METHOD_NAME + " --- STARTED --- ");
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	public TakharujInheritedAssetView getTakharujInheritedAssetView() {
		return takharujInheritedAssetView;
	}

	public void setTakharujInheritedAssetView(
			TakharujInheritedAssetView takharujInheritedAssetView) {
		this.takharujInheritedAssetView = takharujInheritedAssetView;
	}

	public List<TakharujDetailView> getTakharujDetailViewList() {
		return takharujDetailViewList;
	}

	public void setTakharujDetailViewList(
			List<TakharujDetailView> takharujDetailViewList) {
		this.takharujDetailViewList = takharujDetailViewList;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public HtmlDataTable getTakharujDetailViewDataTable() {
		return takharujDetailViewDataTable;
	}

	public void setTakharujDetailViewDataTable(
			HtmlDataTable takharujDetailViewDataTable) {
		this.takharujDetailViewDataTable = takharujDetailViewDataTable;
	}
	
	public Map<String, InheritedAssetSharesView> getInheritedAssetSharesViewMap() {
		return inheritedAssetSharesViewMap;
	}

	public void setInheritedAssetSharesViewMap(
			Map<String, InheritedAssetSharesView> inheritedAssetSharesViewMap) {
		this.inheritedAssetSharesViewMap = inheritedAssetSharesViewMap;
	}

	public List<SelectItem> getBeneficiarySelectItems() {
		return beneficiarySelectItems;
	}

	public void setBeneficiarySelectItems(
			List<SelectItem> beneficiarySelectItems) {
		this.beneficiarySelectItems = beneficiarySelectItems;
	}

	public String getSelectedFromInheritedAssetSharesId() {
		return selectedFromInheritedAssetSharesId;
	}

	public void setSelectedFromInheritedAssetSharesId(
			String selectedFromInheritedAssetSharesId) {
		this.selectedFromInheritedAssetSharesId = selectedFromInheritedAssetSharesId;
	}

	public String getSelectedFromShare() {
		return selectedFromShare;
	}

	public void setSelectedFromShare(String selectedFromShare) {
		this.selectedFromShare = selectedFromShare;
	}

	public PersonView getToPersonView() {
		return toPersonView;
	}

	public void setToPersonView(PersonView toPersonView) {
		this.toPersonView = toPersonView;
	}

	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}

	public String getSelectedToInheritedAssetSharesId() {
		return selectedToInheritedAssetSharesId;
	}

	public void setSelectedToInheritedAssetSharesId(
			String selectedToInheritedAssetSharesId) {
		this.selectedToInheritedAssetSharesId = selectedToInheritedAssetSharesId;
	}

	public String getSelectedToShare() {
		return selectedToShare;
	}

	public void setSelectedToShare(String selectedToShare) {
		this.selectedToShare = selectedToShare;
	}

	public String getTakharujShare() {
		return takharujShare;
	}

	public void setTakharujShare(String takharujShare) {
		this.takharujShare = takharujShare;
	}

	public Boolean getPopModeView() {
		return popModeView;
	}

	public void setPopModeView(Boolean popModeView) {
		this.popModeView = popModeView;
	}

	public String getRemainingShare() {
		return remainingShare;
	}

	public void setRemainingShare(String remainingShare) {
		this.remainingShare = remainingShare;
	}
	private Double getTakharujedFromBeneficiaryShare(Long id) throws Exception{
		updateValuesFromMaps();
		Double shareTakharujed=0.0D;
		if(takharujDetailViewList != null && takharujDetailViewList.size() > 0){
			for(TakharujDetailView takharujDetail : takharujDetailViewList){
				if(takharujDetail.getIsDeleted().compareTo(WebConstants.DEFAULT_IS_DELETED) == 0 &&
				   takharujDetail.getFromInheritedAssetSharesView().getInheritanceBeneficiaryView().getBeneficiary().getPersonId().compareTo(id) == 0)
				{
					shareTakharujed += takharujDetail.getTakharujShare();
				}
			}
		}
		return shareTakharujed;
	}
}
