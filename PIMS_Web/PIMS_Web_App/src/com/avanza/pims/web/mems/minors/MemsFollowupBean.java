package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.List;

import org.richfaces.component.html.HtmlTabPanel;

import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.mems.MemsFollowupView;

public class MemsFollowupBean extends AbstractMemsBean 
{
	private static final long serialVersionUID = 1L;
	private final String SINGLE_RECOMMENDATION = "SINGLE_RECOMMENDATION";
	private final String TASK_TYPE = "TASK_TYPE";
	private final String ACTIVATED_FROM = "ACTIVATED_FROM";
	private final String ACTIVATED_FROM_CORRESPONDENCE = "ACTIVATED_FROM_CORRESPONDENCE";
	private final String ACTIVATED_FROM_FOLLOWUP = "ACTIVATED_FROM_FOLLOWUP";
	private final String ACTIVATED_FROM_SOCIAL_RESEARCH = "ACTIVATED_FROM_SOCIAL_RESEARCH";
	private DomainDataView monthsDD = new DomainDataView();
	private DomainDataView yearsDD = new DomainDataView();
	private DomainDataView daysDD = new DomainDataView();
	private HtmlTabPanel tabPanelMemsFollowup = new HtmlTabPanel();
	@SuppressWarnings("unchecked")
	@Override
	public void init()
	{
		super.init();
		if(!isPostBack())
		{
			canAddAttachmentsNotes(true);
			if(sessionMap.get(WebConstants.PAGE_MODE) != null)
			{
				String pageMode = sessionMap.get(WebConstants.PAGE_MODE).toString();
				sessionMap.remove(WebConstants.PAGE_MODE);
				if(pageMode.compareTo(WebConstants.InheritanceFilePageMode.IS_POPUP) == 0)
					viewMap.put("IS_POPUP", true);
				else if (pageMode.compareTo(WebConstants.InheritanceFilePageMode.PAGE_MODE_IS_EDIT_POPUP) == 0)
					{
						viewMap.put("IS_POPUP", true);
						viewMap.put("IS_EDIT", true);
						viewMap.put(WebConstants.PAGE_MODE_EDIT, true);
					}
			}
			if(sessionMap.get(WebConstants.VIEW_MODE) != null)
			{
				boolean viewMode = (Boolean) sessionMap.get(WebConstants.VIEW_MODE);
				sessionMap.remove(WebConstants.VIEW_MODE);
				viewMap.put(WebConstants.VIEW_MODE, viewMode);
				canAddAttachmentsNotes(false);
			}
			if(sessionMap.get(TASK_TYPE) != null)
			{
				viewMap.put(TASK_TYPE, sessionMap.get(TASK_TYPE));
				sessionMap.remove(TASK_TYPE);
			}
			
			if(sessionMap.containsKey(WebConstants.MemsFollowupTab.OFFICIAL_CORRESPONDENCE_ID))
			{
				viewMap.put(ACTIVATED_FROM, ACTIVATED_FROM_CORRESPONDENCE);
				tabPanelMemsFollowup.setSelectedTab("followupTabId");
			}
			else
			if(sessionMap.containsKey(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW))
			{
				viewMap.put(ACTIVATED_FROM, ACTIVATED_FROM_FOLLOWUP);
				viewMap.put(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW, sessionMap.get(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW));
				viewMap.put(WebConstants.InheritanceFile.FILE_ID, sessionMap.get(WebConstants.InheritanceFile.FILE_ID));
				sessionMap.remove(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW);
				sessionMap.remove(WebConstants.InheritanceFile.FILE_ID);
				tabPanelMemsFollowup.setSelectedTab("socialRecommTabId");
			}
			
			if(sessionMap.containsKey(WebConstants.MemsFollowupTab.FOLLOW_UP_SINGLE_RECOMMENDATION))
			{
				viewMap.put(ACTIVATED_FROM, ACTIVATED_FROM_SOCIAL_RESEARCH);
				viewMap.put(SINGLE_RECOMMENDATION, sessionMap.get(WebConstants.MemsFollowupTab.FOLLOW_UP_SINGLE_RECOMMENDATION));				
				tabPanelMemsFollowup.setSelectedTab("socialRecommTabId");
				
			}
			loadAttachments(null);
		}
		else
		{
			canAddAttachmentsNotes(true);
		}
	}
	@SuppressWarnings("unchecked")
	private void canAddAttachmentsNotes(boolean canAdd) {
		viewMap.put("canAddAttachment",canAdd);
		viewMap.put("canAddNote", canAdd);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void prerender() 
	{
		super.prerender();
		if(viewMap.get(WebConstants.MemsFollowupTab.ERR_MSG_RECOMM_TAB) != null)
		{
			List<String> msg = new ArrayList<String>();
			msg = (List<String>) viewMap.get(WebConstants.MemsFollowupTab.ERR_MSG_RECOMM_TAB);
			errorMessages.addAll(msg);
			viewMap.remove(WebConstants.MemsFollowupTab.ERR_MSG_RECOMM_TAB);
		}
	}
	
	public boolean isViewModePopup()
	{
		boolean isView = false;
		isView = viewMap.containsKey("IS_POPUP");
		return isView;
	}
	@SuppressWarnings("unchecked")
	public void loadAttachments(Long objectId)
	{
		
		//common settings
		viewMap.put(WebConstants.Attachment.PROCEDURE_KEY, WebConstants.InheritanceFile.PROCEDURE_KEY_OPEN_FILE);
    	viewMap.put(WebConstants.Attachment.REPOSITORY_ID, WebConstants.Attachment.PIMS_REPOSITORY_ID);
    	//viewMap.put(WebConstants.ADD_NOTE_DIRECTLY, WebConstants.ADD_NOTE_DIRECTLY); 
    	//common settings
		// if opened against followup
		if(viewMap.get(WebConstants.MemsFollowupTab.FOLLOW_UP_VIEW) != null)
		{
			viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.MemsFollowupTab.EXTERNAL_ID_FOLLOW_UP);
			viewMap.put("noteowner", WebConstants.MemsFollowupTab.NOTES_OWNER_FOLLOW_UP);
		}
		else // if opened against official correspondence
		{
			viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.MemsFollowupTab.EXTERNAL_ID_OFF_CORR);
			viewMap.put("noteowner", WebConstants.MemsFollowupTab.NOTES_OWNER_OFF_CORR);
		}
    	
		if(objectId!= null)
		{
			viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, objectId.toString());
			viewMap.put("entityId", objectId.toString());
		}
	}
	@SuppressWarnings("unchecked")
	public void onSave()
	{
		MemsFollowupView followupToUpdate =null;
		if(viewMap.get( "memsFollowupView" ) != null)
		{
			followupToUpdate = (MemsFollowupView) viewMap.get("memsFollowupView");
		}
			
	}
	public boolean isActivatedFromFollowup()
	{
		boolean isFromFollowup = false;
		if(viewMap.containsKey(ACTIVATED_FROM))
		isFromFollowup = (viewMap.get(ACTIVATED_FROM).toString().compareToIgnoreCase(ACTIVATED_FROM_FOLLOWUP) == 0 || 
				viewMap.get(ACTIVATED_FROM).toString().compareToIgnoreCase(ACTIVATED_FROM_SOCIAL_RESEARCH) == 0);
		return isFromFollowup;
	}
	public boolean isActivatedFromCorrespondence()
	{
		boolean isFromCorrespondence = false;
		isFromCorrespondence = (viewMap.get(ACTIVATED_FROM).toString().compareToIgnoreCase(ACTIVATED_FROM_CORRESPONDENCE) == 0);
		return isFromCorrespondence;
	}
	public DomainDataView getMonthsDD() {
		if(viewMap.get(WebConstants.GracePeriodDataValues.MONTHS) != null)
			monthsDD = (DomainDataView) viewMap.get(WebConstants.GracePeriodDataValues.MONTHS);
		return monthsDD;
	}

	@SuppressWarnings("unchecked")
	public void setMonthsDD(DomainDataView monthsDD) 
	{
		if(monthsDD != null)
			viewMap.put(WebConstants.GracePeriodDataValues.MONTHS,monthsDD);
		this.monthsDD = monthsDD;
	}

	public DomainDataView getYearsDD() {
		if(viewMap.get(WebConstants.GracePeriodDataValues.YEARS) != null)
			yearsDD = (DomainDataView) viewMap.get(WebConstants.GracePeriodDataValues.YEARS);
		return yearsDD;
	}

	@SuppressWarnings("unchecked")
	public void setYearsDD(DomainDataView yearsDD) {
		if(yearsDD != null)
			viewMap.put(WebConstants.GracePeriodDataValues.YEARS,yearsDD);
		this.yearsDD = yearsDD;
	}

	public DomainDataView getDaysDD() {
		if(viewMap.get(WebConstants.GracePeriodDataValues.DAYS) != null)
			daysDD = (DomainDataView) viewMap.get(WebConstants.GracePeriodDataValues.DAYS);
		return daysDD;
	}

	@SuppressWarnings("unchecked")
	public void setDaysDD(DomainDataView daysDD) {
		if(daysDD != null)
			viewMap.put(WebConstants.GracePeriodDataValues.DAYS,daysDD);
		this.daysDD = daysDD;
	}
	public HtmlTabPanel getTabPanelMemsFollowup() {
		return tabPanelMemsFollowup;
	}
	public void setTabPanelMemsFollowup(HtmlTabPanel tabPanelMemsFollowup) {
		this.tabPanelMemsFollowup = tabPanelMemsFollowup;
	}
	
}
