package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.backingbeans.NotesController;
import com.avanza.pims.web.backingbeans.RequestHistoryController;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.SocialResearchService;
import com.avanza.pims.ws.vo.BeneficiaryGridView;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.ResearchRecommendationView;
import com.avanza.ui.util.ResourceUtil;

public class ManageBeneficiaryBean extends AbstractMemsBean {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private transient Logger logger=Logger.getLogger(ManageBeneficiaryBean.class);
	private String hdnApplicantId;
	private HtmlInputText txtFileNumber;
	private HtmlInputText txtFileStatus;
	private HtmlSelectOneMenu cboFileType;
	private HtmlCalendar clndrCreatedOn;
	private String VIEW_MODE = "pageMode";
	private String MODE_SELECT_ONE_POPUP = "MODE_SELECT_ONE_POPUP";
	private String newCC,oldCC;
	public ManageBeneficiaryBean() {
		txtFileNumber = new HtmlInputText();
		txtFileStatus = new HtmlInputText();
		cboFileType = new HtmlSelectOneMenu();
		clndrCreatedOn = new HtmlCalendar();
	}

	@SuppressWarnings("unchecked")
	public void init() {
		super.init();
		if (!isPostBack()) 
		{
			
			viewMap.put("canAddAttachment", true); 
		    viewMap.put("canAddNote", true);
			viewMap.put(WebConstants.Attachment.EXTERNAL_ID, WebConstants.Attachment.EXTERNAL_ID_PERSON);
			
			
			if(sessionMap.get("beneficiaryGridRow")!=null)
			{
//				loadDataList();
				BeneficiaryGridView row =(BeneficiaryGridView)sessionMap.get("beneficiaryGridRow");
				viewMap.put(WebConstants.PERSON_ID,row.getBeneficiaryPersonId());
			}	
			
			HttpServletRequest request = (HttpServletRequest) this.getFacesContext().getExternalContext().getRequest();
			
			 if (request.getParameter(VIEW_MODE)!=null )
	    	 {
	    		if(request.getParameter(VIEW_MODE).equals(MODE_SELECT_ONE_POPUP)) 
	    		 { 	      	    		
      	    		viewMap.put(VIEW_MODE, MODE_SELECT_ONE_POPUP);
      	    	}
	    		
	    		if(request.getParameter(WebConstants.PERSON_ID)!=null) 
	    		 { 	      	    		
	    			Long personId =Long.valueOf(request.getParameter(WebConstants.PERSON_ID).toString());
					viewMap.put(WebConstants.PERSON_ID,personId);
					loadDataListForRecommendationTab(personId);
     	    	}
	    		
	          
	    	 }
			 if(request.getParameter("CC_NEW") != null || request.getParameter("CC_OLD") != null)
			 {
				 newCC = request.getParameter("CC_NEW");
				 oldCC = request.getParameter("CC_OLD");
				 if(StringHelper.isNotEmpty(newCC) && newCC.compareTo("null") != 0)
					 viewMap.put("CC_NEW",newCC);
				 if(StringHelper.isNotEmpty(oldCC)  && oldCC.compareTo("null") != 0)
					 viewMap.put("CC_OLD",oldCC);
			 }
			 loadAttachmentsAndComments();
		}
		
	}

	@SuppressWarnings("unchecked")
	private void loadDataListForRecommendationTab(Long personId) 
	{

		 
		 
		List<ResearchRecommendationView> list = new ArrayList<ResearchRecommendationView>();
		 
		try
		{
			SocialResearchService researchService = new SocialResearchService();
			HashMap<String, String> criteria = new HashMap<String, String>();
			criteria.put(WebConstants.ResearchRecommendationCriteria.PERSON_ID, personId.toString() );
			list=researchService.getResearchRecommendation( criteria );
			viewMap.put("researchRecommendation", list);
	    }
		catch (Exception ex) 
		{
			logger.LogException( "loadDataListForRecommendationTab| crashed", ex);
	    } 
	}

	public String getHdnApplicantId() {
		return hdnApplicantId;
	}
	@SuppressWarnings("unchecked")
	public void loadAttachmentsAndComments() 
	{

		Map viewMap = FacesContext.getCurrentInstance().getViewRoot().getAttributes();
		
		String entityId = viewMap.get(WebConstants.PERSON_ID ).toString();
		String externalId = WebConstants.Attachment.EXTERNAL_ID_PERSON;
		
		viewMap.put(WebConstants.Attachment.REPOSITORY_ID, WebConstants.Attachment.PIMS_REPOSITORY_ID);
		viewMap.put(WebConstants.Attachment.EXTERNAL_ID, externalId);
		viewMap.put("noteowner", WebConstants.NOTES_OWNER_PERSON);
		viewMap.put(WebConstants.Attachment.ASSOCIATED_OBJECT_ID, entityId);
		viewMap.put("entityId", entityId);
	}

	public void setHdnApplicantId(String hdnApplicantId) {
		this.hdnApplicantId = hdnApplicantId;
	}
	public Boolean saveComments(Long referenceId)
    {
		Boolean success = false;
    	try{
	    	NotesController.saveNotes(WebConstants.NOTES_OWNER_PERSON, referenceId);
	    	success = true;
    	}
    	catch (Throwable throwable) {
			logger.LogException( "saveComments| crashed ", throwable);
		}
    	return success;
    }
	public Boolean saveAttachments(String referenceId)
    {
		Boolean success = false;
    	try{
	    	if(referenceId!=null)
	    	{
		    	success = CommonUtil.updateDocuments();
	    	}
    	}
    	catch (Throwable throwable) {
    		success = false;
    		logger.LogException("saveAtttachments crashed ", throwable);
		}
    	
    	return success;
    }
	
	public boolean getIsViewModePopUp() {
		boolean returnVal = false;

		if (viewMap.containsKey(VIEW_MODE)
				&& viewMap.get(VIEW_MODE) != null) {
			
				returnVal = true;
		}

		else {
			returnVal = false;
		}

		return returnVal;

	}

	@SuppressWarnings("unchecked")
	private void populateApplicantDetails(PersonView pv) {
		if (pv.getPersonId() != null)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_ID,
					pv.getPersonId());
		if (pv.getPersonFullName() != null
				&& pv.getPersonFullName().trim().length() > 0)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
					pv.getPersonFullName());
		else if (pv.getCompanyName() != null
				&& pv.getCompanyName().trim().length() > 0)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_NAME,
					pv.getCompanyName());
		String personType = getTenantType(pv);
		if (personType != null)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_TYPE,
					personType);
		if (pv.getCellNumber() != null)
			viewMap.put(
					WebConstants.ApplicationDetails.APPLICATION_APPLICANT_CELL,
					pv.getCellNumber());
		if (pv.getPersonId() != null) {
			viewMap.put(WebConstants.ApplicationDetails.APPLICATION_ID, pv
					.getPersonId());
			hdnApplicantId = pv.getPersonId().toString();
		}
		viewMap.put(WebConstants.ApplicationDetails.APPLICATION_DATE,
				new Date());

	}

	@SuppressWarnings("unchecked")
	@Override
	public void prerender() {
		super.prerender();
	}
	
	@SuppressWarnings("unchecked")
	private String getTenantType(PersonView tenantView) {

		String method = "getTenantType";
		logger.logDebug(method + "|" + "Start...");
		DomainDataView ddv;
		if (tenantView != null && tenantView.getPersonId() != null
				&& tenantView.getIsCompany() != null) {
			if (tenantView.getIsCompany().compareTo(new Long(1)) == 0)
				ddv = (DomainDataView) viewMap
						.get(WebConstants.TENANT_TYPE_COMPANY);
			else
				ddv = (DomainDataView) viewMap
						.get(WebConstants.TENANT_TYPE_INDIVIDUAL);
			if (CommonUtil.getIsArabicLocale())
				return ddv.getDataDescAr();
			else if (CommonUtil.getIsEnglishLocale())
				return ddv.getDataDescEn();
		}
		logger.logDebug(method + "|" + "Finish...");
		return "";

	}

	public HtmlInputText getTxtFileNumber() {
		return txtFileNumber;
	}

	public void setTxtFileNumber(HtmlInputText txtFileNumber) {
		this.txtFileNumber = txtFileNumber;
	}

	public HtmlInputText getTxtFileStatus() {
		return txtFileStatus;
	}

	public void setTxtFileStatus(HtmlInputText txtFileStatus) {
		this.txtFileStatus = txtFileStatus;
	}

	public HtmlSelectOneMenu getCboFileType() {
		return cboFileType;
	}

	public void setCboFileType(HtmlSelectOneMenu cboFileType) {
		this.cboFileType = cboFileType;
	}

	public HtmlCalendar getClndrCreatedOn() {
		return clndrCreatedOn;
	}

	public void setClndrCreatedOn(HtmlCalendar clndrCreatedOn) {
		this.clndrCreatedOn = clndrCreatedOn;
	}

	public void countryChanged() {
		System.out.println("hi");
		System.out.println("hi");
		System.out.println("hi");
		System.out.println("hi");
	}

	public String getNewCC() {
		if(viewMap.get("CC_NEW") != null)
			newCC = viewMap.get("CC_NEW").toString();
		return newCC;
	}

	public void setNewCC(String newCC) {
		this.newCC = newCC;
	}

	public String getOldCC() {
		if(viewMap.get("CC_OLD") != null)
			oldCC = viewMap.get("CC_OLD").toString();
		return oldCC;
	}

	public void setOldCC(String oldCC) {
		this.oldCC = oldCC;
	}

	@SuppressWarnings("unchecked")
	public void onRetainedProfitDetailsTab()
	{
		try
		{
			sessionMap.put(WebConstants.RetainedProfits.RetainedProfitsPersonId, viewMap.get(WebConstants.PERSON_ID));
			RetainedProfitDetailsTab bean = (RetainedProfitDetailsTab)getBean("pages$RetainedProfitDetailsTab");
			bean.updateValuesFromMaps();
		} 
		catch (Exception ex) 
		{
			logger.LogException( "onRetainedProfitDetailsTab|Error Occured ", ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
		}
	}
	@SuppressWarnings("unchecked")
	public void openFinancialAspectsPopup()
	{
		try
		{
			sessionMap.put(WebConstants.PERSON_ID, viewMap.get(WebConstants.PERSON_ID));
			executeJavascript("javaScript:openFinancialAspectsPopup();");
		} 
		catch (Exception ex) 
		{
			logger.LogException( "openFinancialAspectsPopup|Error Occured ", ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
		}
		
	}
	@SuppressWarnings("unchecked")
	public void openResearchFrom() 
	{
		try
		{
			if(viewMap.get(WebConstants.PERSON_ID) != null)
			{
				sessionMap.put(WebConstants.PERSON_ID, viewMap.get(WebConstants.PERSON_ID));
	
				executeJavascript("openResearchForm();");
			}
		} catch (Exception ex) {
			logger.LogException( "openResearchFrom|Error Occured ", ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add( ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR) );
	
		}
	}
	public void onRequestHistoryTabClick()
	{
    	try	
    	{
    		
    	  if( viewMap.get(WebConstants.PERSON_ID) == null) return;
    	  
    	  RequestHistoryController rhc=new RequestHistoryController();
    	  Long personId = new Long(viewMap.get(WebConstants.PERSON_ID).toString());
    	  rhc.getAllRequestTasksForRequest(WebConstants.NOTES_OWNER_PERSON,personId .toString());
    	
    	}
    	catch(Exception ex)
    	{
    		logger.LogException( "onRequestHistoryTabClick|Error Occured..",ex);
    		errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
    	}
	}
	
	private void executeJavascript(String javascript)throws Exception 
	{
		
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
		
	}
}
