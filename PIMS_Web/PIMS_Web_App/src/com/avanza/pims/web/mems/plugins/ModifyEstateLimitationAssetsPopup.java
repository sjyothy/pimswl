package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.component.html.ext.HtmlPanelGrid;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.StringHelper;
import com.avanza.pims.entity.AssetType;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.Person;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.InheritedAssetService;
import com.avanza.pims.ws.vo.InheritanceFileView;
import com.avanza.pims.ws.vo.mems.AssetMemsView;
import com.avanza.pims.ws.vo.mems.AssetTypeView;
import com.avanza.pims.ws.vo.mems.InheritedAssetView;
import com.avanza.pims.ws.vo.mems.ModifyEstateLimitationAssetSharesView;
import com.avanza.ui.util.ResourceUtil;

public class ModifyEstateLimitationAssetsPopup extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	
	private String pageMode;
	
	private String endowmentTypeId;
	private String endowmentCategoryId;
	private String endowmentPurposeId;
	private HtmlSelectBooleanCheckbox chkIsManagerAmaf;
//	EndFileAsso endowmentFileAsso;
	AssetMemsView  assetMemsView;
	InheritedAssetView inheritedAssetView;
	private String hdnPersonId;
	private String hdnPropertyId;
	private String hdnPersonName;
	private  HtmlPanelGrid gridTransportation = new HtmlPanelGrid();
	private HtmlPanelGrid gridVehicles = new HtmlPanelGrid();
	private HtmlPanelGrid gridAnimals = new HtmlPanelGrid();
	private HtmlPanelGrid gridLandProperties = new HtmlPanelGrid();
	private HtmlPanelGrid gridLicenses = new HtmlPanelGrid();
	private HtmlPanelGrid gridStockshares  =new HtmlPanelGrid();
	private HtmlPanelGrid gridCash = new HtmlPanelGrid();
	private HtmlPanelGrid gridCheque = new HtmlPanelGrid();
	private HtmlPanelGrid gridBankTransfer = new HtmlPanelGrid();
	private HtmlPanelGrid gridPension = new HtmlPanelGrid();
	private HtmlPanelGrid gridJewellery = new HtmlPanelGrid();
	List<ModifyEstateLimitationAssetSharesView > listShares;	
	private HtmlDataTable dataTableShares = new HtmlDataTable();
	HttpServletRequest httpServletRequest =
		 (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
	public ModifyEstateLimitationAssetsPopup() 
	{
		pageMode                = "edit";
		chkIsManagerAmaf        = new HtmlSelectBooleanCheckbox(); 
	}
	
	@Override
	public void init() 
	{
		try	
		{	
//			if( !isPostBack() )
			{
				updateValuesFromMaps();
			}
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages = new ArrayList<String>();
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if( sessionMap.get( WebConstants.PAGE_MODE ) != null )
		{
			pageMode = sessionMap.remove( WebConstants.PAGE_MODE ).toString(); 
			viewMap.put( WebConstants.PAGE_MODE, pageMode );
		}
		else if ( viewMap.get( WebConstants.PAGE_MODE ) != null )
		{
			pageMode = (String) viewMap.get(WebConstants.PAGE_MODE);
		}
		else if( httpServletRequest.getParameter("pageMode") != null)
		{
			pageMode = httpServletRequest.getParameter("pageMode");
		}
		if( viewMap.get( WebConstants.ModifyEstateLimitation.InheritedAssetView ) != null )
		{
			inheritedAssetView     = ( InheritedAssetView )viewMap.get( WebConstants.ModifyEstateLimitation.InheritedAssetView );
		}
		else if( sessionMap.get( WebConstants.ModifyEstateLimitation.InheritedAssetView ) != null )
		{
			inheritedAssetView = ( InheritedAssetView )sessionMap.remove( WebConstants.ModifyEstateLimitation.InheritedAssetView );
			populatePage();
		}
		else if( !isPostBack() )
		{
			inheritedAssetView = new InheritedAssetView();
			inheritedAssetView.setMyHashCode( inheritedAssetView.hashCode() );
			assetMemsView = new AssetMemsView();
			populateListShares();
		}
		if( viewMap.get( "assetMems") != null )
		{
			assetMemsView =  (AssetMemsView )viewMap.get( "assetMems") ;
		}
		if ( viewMap.get( "listShares" ) != null )
		{
			listShares = (ArrayList <ModifyEstateLimitationAssetSharesView> )viewMap.get( "listShares" );
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( pageMode != null )
		{
			viewMap.put( WebConstants.PAGE_MODE, pageMode);
		}
		if( inheritedAssetView != null  )
		{
			viewMap.put( WebConstants.ModifyEstateLimitation.InheritedAssetView , inheritedAssetView );
			
		}
		if( assetMemsView != null )
		{
			viewMap.put( "assetMems", assetMemsView);
		}
		if( listShares != null )
		{
			viewMap.put( "listShares", listShares );
		}
	}	
	

	public void populatePage( ) throws Exception
	{
		assetMemsView =  inheritedAssetView.getAssetMemsView();
		
		if( assetMemsView.getManagerId() == null )
		{
			chkIsManagerAmaf.setValue( true );
			hdnPersonId           = "";
			hdnPersonName         = "";
		}
		else if( assetMemsView.getManagerId() != null  )
		{
			chkIsManagerAmaf.setValue(false);
			hdnPersonId           = assetMemsView.getManagerId().toString();
			hdnPersonName         = assetMemsView.getManagerName() ;
		}
		populateListShares();
		
		displayExtraFieldsGridsBasedOnType();
	}

	/**
	 * @throws Exception
	 */
	private void populateListShares() throws Exception 
	{
		InheritanceFileView inheritanceFileView = ( InheritanceFileView )sessionMap.remove( WebConstants.InheritanceFile.INHERITANCE_FILE_VIEW );
		if( inheritedAssetView != null && inheritedAssetView.getInheritedAssetId() == null && inheritedAssetView.getParentInheritedAssetId() != null  )
		{
			listShares = InheritedAssetService.getAssetSharesForModifyEstateLimitation( 
																						inheritanceFileView.getInheritanceFileId(),
																						inheritedAssetView.getParentInheritedAssetId(),
																						true 
																					   );
		}
		else if ( inheritedAssetView != null )
		{
			listShares = InheritedAssetService.getAssetSharesForModifyEstateLimitation( 
																						inheritanceFileView.getInheritanceFileId(),
																						inheritedAssetView.getInheritedAssetId(),
																						false 
																					  );
		}
	}
	
	
		
	public void onMessageFromSearchPerson()
	{
		try	
		{
			updateValuesFromMaps();
			if( hdnPersonId != null  )
			{
				chkIsManagerAmaf.setValue(false);
				assetMemsView.setManagerId( new Long( hdnPersonId ) );
				assetMemsView.setManagerName( hdnPersonName );
			}
			updateValuesToMaps();
		}
		catch(Exception ex)
		{
			logger.LogException("onMessageFromSearchPerson|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public void onManagerAmafClick()
	{
		try 
		{
		
			if( chkIsManagerAmaf.getValue() != null && (Boolean)chkIsManagerAmaf.getValue() )
			{
				viewMap.put("SHOW_SEARCH_MANAGER_IMAGE",false);
                 hdnPersonId   = "";
                 hdnPersonName = "";
			}
		else
		{
		}
		} 
		catch (Exception e) 
		{
			logger.logError("onManagerAmafClick|crashed | ",e);
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	}
	

	@SuppressWarnings("unchecked")
	private boolean isValidated() throws Exception
	{
		boolean validate = true;
		
		
		if( isAssetMemsValidated( assetMemsView,false  ) ) 
		{
			Double sumAssetShares  = 0.0d;
			Double totalAssetShare = 0.0d;
			if (
					
					inheritedAssetView.getTotalShareValueString() == null || 
					inheritedAssetView.getTotalShareValueString().trim().equals("") ||
					Double.valueOf( inheritedAssetView.getTotalShareValueString() ).compareTo( 0d ) <= 0 
			   ) 
			{
				errorMessages.add(ResourceUtil.getInstance().getProperty( "modifyEstateLimitationAsset.msg.assetShareRequired" ) );
				return false;
			}
			else
			{
			   totalAssetShare =  Double.valueOf( inheritedAssetView.getTotalShareValueString() );
			}
			if( listShares != null )
			{
				for ( ModifyEstateLimitationAssetSharesView share: listShares) 
				{
					if( share.getAssetShareNew() != null )
					{
						sumAssetShares += share.getAssetShareNew();		
					}
				}
			}
			if( listShares == null || sumAssetShares.compareTo( totalAssetShare ) != 0 )
			{
				String msg = java.text.MessageFormat.format( 
															 ResourceUtil.getInstance().getProperty( "modifyEstateLimitationAsset.msg.totalAssetShareNotEqualToSumOfDistribution" ),
															 totalAssetShare,
															 sumAssetShares
														   );
				errorMessages.add( msg );
				return false;
			}
			return validate;
		}
		else
		{
			if(errorMessages != null && errorMessages.size() > 0)
			validate = false;
		}
		
		return validate;
	}
	
	@SuppressWarnings("unchecked")
	public List<Long> getAssetTypeIdsWhereDepttMandatory()
	{
		errorMessages = new ArrayList<String>(0);
		List<Long> assetIdList = null; 
		try	
    	{
			if( viewMap.get("ASSET_TYPE_IDS_WHERE_DEPT_MANDATORY")  != null)
			{
				assetIdList = (List<Long>) viewMap.get("ASSET_TYPE_IDS_WHERE_DEPT_MANDATORY"); 
			}
			else
			{
				assetIdList = CommonUtil.getAssetTypeIdWhereDepttMandatory();
				if(assetIdList != null && assetIdList.size() > 0)
				{
					viewMap.put("ASSET_TYPE_IDS_WHERE_DEPT_MANDATORY",assetIdList);
				}
			}
    	}
    	catch(Exception ex)
    	{
    		logger.LogException("getAssetTypeIdsWhereDepttMandatory|Error Occured..",ex);
    		errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
    	}
    	return assetIdList;
	}
	
	private boolean isDepttMandatory(Long assetTypeId)
	{
		List<Long> assetTypeIds = null;
		boolean isMandatory = false;
		assetTypeIds = getAssetTypeIdsWhereDepttMandatory();
		if(assetTypeIds != null && assetTypeIds.size() > 0 && assetTypeIds.contains(assetTypeId))
		{
			isMandatory = true;
		}
		return isMandatory;
	}


	private boolean isAssetMemsValidated( AssetMemsView asset,Boolean validateCompleteAssetDetails ) throws Exception
	{
			boolean validate = true;
			if(asset == null)
			{
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.assetInfoReq"));
					return false;
			}
			if(asset.getAssetTypeView() == null ||asset.getAssetTypeView().getAssetTypeIdString().compareTo("-1") == 0)
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.assettypeReq"));
			}	
			if(StringHelper.isEmpty(asset.getAssetNameAr()))
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.aasetNameReq"));
			}
			//Rest of the details will
			if( !validateCompleteAssetDetails   )
			{
				return validate;
			}
			
			if(isCash() && !isCashValidated(asset))//Check cash  validations
			{
				validate = false;
			}
			else if(isCheque() && !isChequeValidated(asset))
			{
				validate = false;
			}
			else if( isBankTransfer() && !isBankTransferValidated(asset))
			{
				validate = false;
			}
			else if( isLoanLiabilities() && !isLoanLiabilityValidated(asset))
			{
				validate = false;
			}
			else if( isPension() && !isPensionValidated( asset ) )
			{
				validate = false;
			}
			if( !  ( asset.getTotalAssetShareInFile()   == null || 
					 StringHelper.isEmpty(asset.getTotalAssetShareInFile().toString()) ||
					 asset.getTotalAssetShareInFile().equals("0") 
					)
			  )
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("inheritanceFile.errMsg.totalAssetShareRequired"));
			}
			//provide other validations below
		
		return validate;
	}

	private boolean isLoanLiabilityValidated(	AssetMemsView asset	) 
	{
		boolean validate = true;
		if(StringHelper.isEmpty(asset.getAmountString()))
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountReq"));
		}
		else
		{
			try 
			{	
				Double amount = new Double(asset.getAmountString());
				if(amount.compareTo(0D) == -1)
				{
					validate = false;
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountGTZero"));
				}
			}
			catch (Exception e) 
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountNumeric"));
				return validate;
			}
		}
		if( ! StringHelper.isEmpty( asset.getFieldParty() ) )
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.partyNameReq"));
		}
		if( asset.getDueDate() == null)
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.dueDateReq"));
		}
		return validate;
	}
	private boolean isPensionValidated(AssetMemsView asset) 
	{
		boolean validate = true;
		if(StringHelper.isEmpty(asset.getAmountString()))
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountReq"));
		}
		else
		{
			try 
			{	
				Double amount = new Double(asset.getAmountString());
				if(amount.compareTo(0D) == -1)
				{
					validate = false;
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountGTZero"));
				}
			}
			catch (Exception e) 
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountNumeric"));
				return validate;
			}
		}
		
		return validate;
	}

	private boolean isBankTransferValidated(AssetMemsView asset) 
	{
		boolean validate = true;
		if(StringHelper.isEmpty(asset.getAmountString()))
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.bankTransAmountReq"));
		}
		else
		{
			try 
			{	
				Double amount = new Double(asset.getAmountString());
				if(amount.compareTo(0D) == -1)
				{
					validate = false;
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountGTZero"));
				}
			}
			catch (Exception e) 
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountNumeric"));
				return validate;
			}
		}
		if( asset.getDueDate() == null)
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.bankTransDateReq"));
		}
		
		if(asset.getBankView() == null || StringHelper.isEmpty(asset.getBankView().getBankIdString()) || asset.getBankView().getBankIdString().compareTo("-1") == 0)
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.bankReq"));
		}
		return validate;
	}

	private boolean isChequeValidated(AssetMemsView asset)
	{
		boolean validate = true;
		if(StringHelper.isEmpty(asset.getAmountString()))
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.chequeAmountReq"));
		}
		else
		{
			try 
			{	
				Double amount = new Double(asset.getAmountString());
				if(amount.compareTo(0D) == -1)
				{
					validate = false;
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountGTZero"));
				}
			}
			catch (Exception e) 
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountNumeric"));
				return validate;
			}
		}
		
		if( StringHelper.isEmpty( asset.getFieldNumber() ) )
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.cheqNumberReq"));
		}
		
		if(asset.getDueDate() == null)
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.cheqDateReq"));
		}
		
		if(asset.getBankView() == null || StringHelper.isEmpty(asset.getBankView().getBankIdString()) || asset.getBankView().getBankIdString().compareTo("-1") == 0)
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.bankReq"));
		}
		return validate;
	}

	private boolean isCashValidated(AssetMemsView asset)  throws Exception
	{
		boolean validate= true;
		if(StringHelper.isEmpty(asset.getAmountString()))
		{
			validate = false;
			errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.cashAmountReq"));
		}
		else
		{
			try 
			{	
				Double amount = new Double(asset.getAmountString());
				if(amount.compareTo(0D) == -1)
				{
					validate = false;
					errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountGTZero"));
				}
			}
			catch (Exception e) 
			{
				validate = false;
				errorMessages.add(CommonUtil.getBundleMessage("mems.inheritanceFile.amountNumeric"));
				return validate;
			}
		}
		return validate;
	}
	
	@SuppressWarnings( "unchecked" )
	public void onClose()
	{
		errorMessages = new ArrayList<String>();		
		try
		{
			updateValuesFromMaps();
			sessionMap.put( WebConstants.ModifyEstateLimitation.InheritedAssetView, inheritedAssetView  );
			executeJavaScript( "sendDataToParent(); window.close();");
		}
		catch ( Exception e )
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException( "onClose--- EXCEPTION --- ", e );
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
	
		
	}
    @SuppressWarnings( "unchecked" )
	public void onSave()
	{
		errorMessages = new ArrayList<String>();		
		try
		{
			updateValuesFromMaps();
			
			if( !isValidated() ){return;}
			
			inheritedAssetView.setTotalShareValue( Double.valueOf( inheritedAssetView.getTotalShareValueString() ) );
			
			InheritanceFileService fileService = new InheritanceFileService();
			
			ApplicationContext.getContext().getTxnContext().beginTransaction();

			  fileService.persistModifyEstateLimitationAsset( null, inheritedAssetView, listShares,getLoggedInUserId() );
			
			ApplicationContext.getContext().getTxnContext().commit();
			successMessages.add( ResourceUtil.getInstance().getProperty( "commons.messages.SaveSuccess" ) ) ;
			sessionMap.put( WebConstants.ModifyEstateLimitation.InheritedAssetView, inheritedAssetView  );
			executeJavaScript( "sendDataToParent();");
		}
		catch ( Exception e )
		{
			ApplicationContext.getContext().getTxnContext().rollback();
			logger.LogException( "onSave--- EXCEPTION --- ", e );
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		finally
		{
			ApplicationContext.getContext().getTxnContext().release();
		}
		
	}
    
    @SuppressWarnings( "unchecked" )
	public void onAdd()
	{
		errorMessages = new ArrayList<String>();		
		try
		{
			updateValuesFromMaps();
			
			if( !isValidated() ){return;}
			AssetType assetType = EntityManager.getBroker().findById( AssetType.class, assetMemsView.getAssetTypeView().getAssetTypeId() );
			AssetTypeView assetTypeView = new AssetTypeView();
			assetTypeView.setAssetTypeNameAr( assetType.getAssetTypeNameAr() );
			assetTypeView.setAssetTypeNameEn( assetType.getAssetTypeNameEn() );
			assetTypeView.setAssetTypeId( assetType.getAssetTypeId() );
			assetTypeView.setAssetTypeIdString( assetType.getAssetTypeId().toString() );
			assetMemsView.setAssetTypeView( assetTypeView );
			inheritedAssetView.setAssetMemsView( assetMemsView );
			inheritedAssetView.setTotalShareValue( Double.valueOf( inheritedAssetView.getTotalShareValueString() ) );
			inheritedAssetView.setIsSelected( true );
			inheritedAssetView.setCreatedBy( getLoggedInUserId() );
			inheritedAssetView.setUpdatedBy( getLoggedInUserId() );
			inheritedAssetView.setUpdatedOn( new Date() );
			inheritedAssetView.setCreatedOn( new Date() );
			inheritedAssetView.setModifyEstateLimitataionListShares(listShares); 
			sessionMap.put( WebConstants.ModifyEstateLimitation.InheritedAssetView, inheritedAssetView  );
			executeJavaScript( "sendDataToParent();window.close();");
		}
		catch ( Exception e )
		{
			logger.LogException( "onAdd--- EXCEPTION --- ", e );
			errorMessages.add(ResourceUtil.getInstance().getProperty(MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR));
		}
		
	}

	
    @SuppressWarnings( "unchecked" )
	public void onAssetTypeChange(ValueChangeEvent evt)
	{
		try	
		{
			updateValuesFromMaps();
	
			endowmentTypeId =  evt.getNewValue().toString();
			if ( assetMemsView  == null ) return;
			
			AssetTypeView atView = new AssetTypeView ();
			atView.setAssetTypeId( new Long (endowmentTypeId ) );
			assetMemsView.setAssetTypeView( atView );
			displayExtraFieldsGridsBasedOnType();
		}
		catch(Exception ex)
		{
			logger.LogException("onAssetTypeChange|Error Occured..",ex);
			errorMessages = new ArrayList<String>();
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	/**
	 * 
	 */
	private void displayExtraFieldsGridsBasedOnType()throws Exception
	{
		gridLandProperties.setRendered(false);
		gridStockshares.setRendered(false);
		gridTransportation.setRendered(false);
		gridVehicles.setRendered(false);
		gridAnimals.setRendered(false);
		gridLicenses.setRendered(false);
		gridCheque.setRendered(false);
		gridCash.setRendered(false);
		gridBankTransfer.setRendered(false);
		gridPension.setRendered(false);
		gridJewellery.setRendered(false);
		
		if(isLandProperties())
		{
			gridLandProperties.setRendered(true);
		}
		if(isLicenses())
		{
			gridLicenses.setRendered(true);
		}
		else if( isTransportation() )
		{
			gridTransportation.setRendered(true);
		}
		else if ( isVehicles() )
		{
			gridVehicles.setRendered(true);
		}
		else if (  isStockshares() )
		{
			gridStockshares.setRendered(true);
		}
		else if( isAnimals()  )
		{
			gridAnimals.setRendered(true);
		}
		else if( isCash() )
		{
			gridCash.setRendered(true);
		}
		else if( isCheque() )
		{
			gridCheque.setRendered(true);
		}
		else if( isBankTransfer() )
		{
			gridBankTransfer.setRendered(true);
		}
		else if( isPension() )
		{
			gridPension.setRendered(true);
		}
		else if( isJewellery()  )
		{
			gridJewellery.setRendered(true);
		}
	}
	
	public boolean isTransportation() 
	{
		return 	assetMemsView != null && assetMemsView.getAssetTypeView() != null && assetMemsView.getAssetTypeView().getAssetTypeId() != null  &&
				assetMemsView.getAssetTypeView().getAssetTypeId().longValue() == WebConstants.AssetType.TRANSPORTATIONS ;
	}
	public boolean isBankTransfer() 
	{
		return 	assetMemsView != null && assetMemsView.getAssetTypeView() != null && assetMemsView.getAssetTypeView().getAssetTypeId() != null  && 
				assetMemsView.getAssetTypeView().getAssetTypeId().longValue() == WebConstants.AssetType.BANK_TRANSFER ;
	}
	public boolean isJewellery() 
	{
		return 	assetMemsView != null && assetMemsView.getAssetTypeView() != null && assetMemsView.getAssetTypeView().getAssetTypeId() != null  && 
				assetMemsView.getAssetTypeView().getAssetTypeId().longValue() == WebConstants.AssetType.JEWELLERY ;
	}
	public boolean isPension() 
	{
		return 	assetMemsView != null && assetMemsView.getAssetTypeView() != null && assetMemsView.getAssetTypeView().getAssetTypeId() != null  &&
				assetMemsView.getAssetTypeView().getAssetTypeId().longValue() == WebConstants.AssetType.PENSION ;
	}
	public boolean isCheque() 
	{
		return 	assetMemsView != null && assetMemsView.getAssetTypeView() != null && assetMemsView.getAssetTypeView().getAssetTypeId() != null  && 
				assetMemsView.getAssetTypeView().getAssetTypeId().longValue() == WebConstants.AssetType.CHEQUE ;
	}
	public boolean isCash() 
	{
		return 	assetMemsView != null && assetMemsView.getAssetTypeView() != null && assetMemsView.getAssetTypeView().getAssetTypeId() != null  && 
				assetMemsView.getAssetTypeView().getAssetTypeId().longValue() == WebConstants.AssetType.CASH ;
	}
	public boolean isVehicles() 
	{
		return 	assetMemsView != null && assetMemsView.getAssetTypeView() != null && assetMemsView.getAssetTypeView().getAssetTypeId() != null  && 
				assetMemsView.getAssetTypeView().getAssetTypeId().longValue() == WebConstants.AssetType.VEHICLES ;
	}
	public boolean isAnimals() 
	{
		return 	assetMemsView != null && assetMemsView.getAssetTypeView() != null && assetMemsView.getAssetTypeView().getAssetTypeId() != null  && 
				assetMemsView.getAssetTypeView().getAssetTypeId().longValue() == WebConstants.AssetType.ANIMAL ;
	}
	public boolean isStockshares() 
	{
		return 	assetMemsView != null && assetMemsView.getAssetTypeView() != null && assetMemsView.getAssetTypeView().getAssetTypeId() != null  &&
				assetMemsView.getAssetTypeView().getAssetTypeId().longValue() == WebConstants.AssetType.STOCK_SHARES ;
	}
	public boolean isLicenses() 
	{
		return 	assetMemsView != null && assetMemsView.getAssetTypeView() != null && assetMemsView.getAssetTypeView().getAssetTypeId() != null &&
				assetMemsView.getAssetTypeView().getAssetTypeId() .longValue() ==  WebConstants.AssetType.LICENSES ;
	}

    public boolean isLandProperties() 
    {
		return	assetMemsView != null && assetMemsView.getAssetTypeView() != null && assetMemsView.getAssetTypeView().getAssetTypeId() != null  && 
				assetMemsView.getAssetTypeView().getAssetTypeId().longValue() == WebConstants.AssetType.LAND_PROPERTIES 	;
	}
    public boolean isLoanLiabilities() 
    {
		return	assetMemsView != null && assetMemsView.getAssetTypeView() != null && assetMemsView.getAssetTypeView().getAssetTypeId() != null  && 
				assetMemsView.getAssetTypeView().getAssetTypeId() .longValue() == WebConstants.AssetType.LOAN_LIABILITIES ;
	}


	public boolean getIsPageModeUpdatable()
	{
		boolean isPageModeUpdatable = false;
		
		if ( pageMode.equalsIgnoreCase( WebConstants.PAGE_MODE_EDIT ) )
			isPageModeUpdatable = true;
		
		return isPageModeUpdatable;
	}

	public String getPageMode() 
	{
		return pageMode;
	}

	public void setPageMode(String pageMode) 
	{
		this.pageMode = pageMode;
	}

	public String getEndowmentTypeId() 
	{
		return endowmentTypeId;
	}

	public void setEndowmentTypeId(String endowmentTypeId) 
	{
		this.endowmentTypeId = endowmentTypeId;
	}

	public String getEndowmentCategoryId() 
	{
		return endowmentCategoryId;
	}

	public void setEndowmentCategoryId(String endowmentCategoryId) 
	{
		this.endowmentCategoryId = endowmentCategoryId;
	}

	public String getEndowmentPurposeId() 
	{
		return endowmentPurposeId;
	}

	public void setEndowmentPurposeId(String endowmentPurposeId) 
	{
		this.endowmentPurposeId = endowmentPurposeId;
	}

	public HtmlSelectBooleanCheckbox getChkIsManagerAmaf() {
		return chkIsManagerAmaf;
	}

	public void setChkIsManagerAmaf(HtmlSelectBooleanCheckbox chkIsManagerAmaf) {
		this.chkIsManagerAmaf = chkIsManagerAmaf;
	}

	public String getHdnPersonId() {
		return hdnPersonId;
	}

	public void setHdnPersonId(String hdnPersonId) {
		this.hdnPersonId = hdnPersonId;
	}

	public String getHdnPersonName() {
		return hdnPersonName;
	}

	public void setHdnPersonName(String hdnPersonName) {
		this.hdnPersonName = hdnPersonName;
	}

	public String getHdnPropertyId() {
		return hdnPropertyId;
	}

	public void setHdnPropertyId(String hdnPropertyId) {
		this.hdnPropertyId = hdnPropertyId;
	}

	public HtmlPanelGrid getGridLandProperties() {
		return gridLandProperties;
	}

	public void setGridLandProperties(HtmlPanelGrid gridLandProperties) {
		this.gridLandProperties = gridLandProperties;
	}

	public HtmlPanelGrid getGridStockshares() {
		return gridStockshares;
	}

	public void setGridStockshares(HtmlPanelGrid gridStockshares) {
		this.gridStockshares = gridStockshares;
	}

	public HtmlPanelGrid getGridLicenses() {
		return gridLicenses;
	}

	public void setGridLicenses(HtmlPanelGrid gridLicenses) {
		this.gridLicenses = gridLicenses;
	}

	public HtmlPanelGrid getGridAnimals() {
		return gridAnimals;
	}

	public void setGridAnimals(HtmlPanelGrid gridAnimals) {
		this.gridAnimals = gridAnimals;
	}

	public HtmlPanelGrid getGridVehicles() {
		return gridVehicles;
	}

	public void setGridVehicles(HtmlPanelGrid gridVehicles) {
		this.gridVehicles = gridVehicles;
	}

	public HtmlPanelGrid getGridTransportation() {
		return gridTransportation;
	}

	public void setGridTransportation(HtmlPanelGrid gridTransportation) {
		this.gridTransportation = gridTransportation;
	}

	public HtmlPanelGrid getGridCash() {
		return gridCash;
	}

	public void setGridCash(HtmlPanelGrid gridCash) {
		this.gridCash = gridCash;
	}

	public HtmlPanelGrid getGridCheque() {
		return gridCheque;
	}

	public void setGridCheque(HtmlPanelGrid gridCheque) {
		this.gridCheque = gridCheque;
	}

	public HtmlPanelGrid getGridBankTransfer() {
		return gridBankTransfer;
	}

	public void setGridBankTransfer(HtmlPanelGrid gridBankTransfer) {
		this.gridBankTransfer = gridBankTransfer;
	}

	public HtmlPanelGrid getGridPension() {
		return gridPension;
	}

	public void setGridPension(HtmlPanelGrid gridPension) {
		this.gridPension = gridPension;
	}

	public HtmlPanelGrid getGridJewellery() {
		return gridJewellery;
	}

	public void setGridJewellery(HtmlPanelGrid gridJewellery) {
		this.gridJewellery = gridJewellery;
	}

	public AssetMemsView getAssetMemsView() {
		return assetMemsView;
	}

	public void setAssetMemsView(AssetMemsView assetMemsView) {
		this.assetMemsView = assetMemsView;
	}

	public InheritedAssetView getInheritedAssetView() {
		return inheritedAssetView;
	}

	public void setInheritedAssetView(InheritedAssetView inheritedAssetView) {
		this.inheritedAssetView = inheritedAssetView;
	}

	public List<ModifyEstateLimitationAssetSharesView> getListShares() {
		return listShares;
	}

	public void setListShares(List<ModifyEstateLimitationAssetSharesView> listShares) {
		this.listShares = listShares;
	}

	public HtmlDataTable getDataTableShares() {
		return dataTableShares;
	}

	public void setDataTableShares(HtmlDataTable dataTableShares) {
		this.dataTableShares = dataTableShares;
	}

	}
