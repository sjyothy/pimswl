package com.avanza.pims.web.mems.plugins;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.vo.RequestView;

public class ApplicationDetailsTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -1339219658861615704L;
	
	private RequestView requestView;
	
	private PropertyServiceAgent propertyServiceAgent;

	public ApplicationDetailsTabBacking() 
	{
		requestView = new RequestView();
		
		propertyServiceAgent = new PropertyServiceAgent();
	}
	
	@Override
	public void init() 
	{
		String METHOD_NAME = "init()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			updateValuesFromMaps();
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws PimsBusinessException
	{
		// REQUEST VIEW
		if ( viewMap.get( WebConstants.REQUEST_VIEW ) != null )
		{
			requestView = (RequestView) viewMap.get( WebConstants.REQUEST_VIEW );
		}
		
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// REQUEST VIEW
		if ( requestView != null )
		{
			viewMap.put( WebConstants.REQUEST_VIEW , requestView );
		}
	}
	
	public String onReceiveApplicant()
	{
		final String METHOD_NAME = "onReceiveApplicant()";
		
		try	
		{	
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			
			if ( requestView != null &&  requestView.getApplicantView() != null && requestView.getApplicantView().getPersonId() != null )
			{
				requestView.setApplicantView( propertyServiceAgent.getPersonInformation( requestView.getApplicantView().getPersonId() ) );
				updateValuesToMaps();
			}
			
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");
		}
		catch (Exception exception) 
		{
			logger.LogException(METHOD_NAME + " --- CRASHED --- ", exception);
		}
		
		return null;
	}
	
	public RequestView getRequestView() {
		return requestView;
	}

	public void setRequestView(RequestView requestView) {
		this.requestView = requestView;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public PropertyServiceAgent getPropertyServiceAgent() {
		return propertyServiceAgent;
	}

	public void setPropertyServiceAgent(PropertyServiceAgent propertyServiceAgent) {
		this.propertyServiceAgent = propertyServiceAgent;
	}	
}
