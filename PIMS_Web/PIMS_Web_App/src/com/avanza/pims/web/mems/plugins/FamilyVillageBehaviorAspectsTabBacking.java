package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.List;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.avanza.pims.constant.Constant;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.ws.mems.FamilyVillageBehavioralAspectService;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.mems.BehaviorAspectsGeneralView;
import com.avanza.pims.ws.vo.mems.BehaviorAspectsOthersView;
import com.avanza.ui.util.ResourceUtil;

public class FamilyVillageBehaviorAspectsTabBacking extends AbstractMemsBean
{	
	private static final long serialVersionUID = -4983101685930099314L;
	
	private List< BehaviorAspectsGeneralView> dataListBehaviorAspectsGeneral;
	private HtmlDataTable dataTableBehaviorAspectsGeneral;
	private List< BehaviorAspectsOthersView> dataListBehaviorAspectsOthers;
	private HtmlDataTable dataTableBehaviorAspectsOthers;
	
	private String tabMode;
	private Long personId;
	private Integer recordSizeBehaviorAspectsGeneral;
	private Integer recordSizeBehaviorAspectsOthers;
	public FamilyVillageBehaviorAspectsTabBacking() 
	{
		dataTableBehaviorAspectsGeneral             =  new HtmlDataTable();
		dataListBehaviorAspectsGeneral		        =  new ArrayList<BehaviorAspectsGeneralView>();
		dataTableBehaviorAspectsOthers             =  new HtmlDataTable();
		dataListBehaviorAspectsOthers		        =  new ArrayList<BehaviorAspectsOthersView>();
		recordSizeBehaviorAspectsGeneral  		=  0;
		recordSizeBehaviorAspectsOthers  		=  0;
		tabMode          = Constant.EndowmentFile.MODE_READ_ONLY;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void init() 
	{
		try	
		{	
			updateValuesFromMaps();
		}
		catch (Exception exception) 
		{
			logger.LogException(" init--- CRASHED --- ", exception);
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.BEHAVIORAL_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.BEHAVIORAL_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesFromMaps() throws Exception
	{
		// TAB MODE
		if ( viewMap.get( Constant.EndowmentFile.MODE_UPDATABLE) != null )
		{
			tabMode = (String) viewMap.get(Constant.EndowmentFile.MODE_UPDATABLE);
		}
		if( viewMap.get( "dataListBehaviorAspectsGeneral" ) != null )
		{
			dataListBehaviorAspectsGeneral = (ArrayList<BehaviorAspectsGeneralView>)viewMap.get( "dataListBehaviorAspectsGeneral" );
		}
				
		if( viewMap.get( "recordSizeBehaviorAspectsGeneral" ) != null )
		{
			recordSizeBehaviorAspectsGeneral = Integer.parseInt( viewMap.get( "recordSizeBehaviorAspectsGeneral" ).toString() );
		}
		if( viewMap.get( "dataListBehaviorAspectsOthers" ) != null )
		{
			dataListBehaviorAspectsOthers = (ArrayList<BehaviorAspectsOthersView>)viewMap.get( "dataListBehaviorAspectsOthers" );
		}
				
		if( viewMap.get( "recordSizeBehaviorAspectsOthers" ) != null )
		{
			recordSizeBehaviorAspectsOthers = Integer.parseInt( viewMap.get( "recordSizeBehaviorAspectsOthers" ).toString() );
		}

		if( viewMap.get(WebConstants.PERSON_ID )!= null )
		{
			personId = Long.valueOf( viewMap.get(WebConstants.PERSON_ID ).toString() );
		}
		updateValuesToMaps();
	}
	
	@SuppressWarnings("unchecked")
	private void updateValuesToMaps()
	{
		// TAB MODE
		if ( tabMode != null )
		{
			viewMap.put( Constant.EndowmentFile.MODE_UPDATABLE, tabMode );
		}
		if(recordSizeBehaviorAspectsGeneral !=null)
		{
			viewMap.put("recordSizeBehaviorAspectsGeneral",recordSizeBehaviorAspectsGeneral);
		}
		if( dataListBehaviorAspectsGeneral != null && dataListBehaviorAspectsGeneral.size()  > 0  )
		{
			viewMap.put( "dataListBehaviorAspectsGeneral", dataListBehaviorAspectsGeneral );
		}
		if(recordSizeBehaviorAspectsOthers !=null)
		{
			viewMap.put("recordSizeBehaviorAspectsOthers",recordSizeBehaviorAspectsOthers);
		}
		if( dataListBehaviorAspectsOthers != null && dataListBehaviorAspectsOthers.size()  > 0  )
		{
			viewMap.put( "dataListBehaviorAspectsOthers", dataListBehaviorAspectsOthers );
		}
			
		
		if( personId != null )
		{
			viewMap.put(WebConstants.PERSON_ID, personId);
		}
	}	
	
	

	public void populateTab( PersonView personView ) throws Exception
	{
		updateValuesFromMaps();
		personId = personView.getPersonId();
		setPageDataDefaults();
		loadDataListBehaviorAspectsGeneral( personId );
		loadDataListBehaviorAspectsOthers( personId );
		
		updateValuesToMaps();
	}
	
	private void setPageDataDefaults()throws Exception
	{
		PersonView person  = new PersonView();
		person.setPersonId(personId);
	}

	private void loadDataListBehaviorAspectsGeneral(Long personId) throws Exception {
		
		dataListBehaviorAspectsGeneral= FamilyVillageBehavioralAspectService.getBehaviorAspectsGenerals( personId );
		recordSizeBehaviorAspectsGeneral=dataListBehaviorAspectsGeneral.size();
	}
    private void loadDataListBehaviorAspectsOthers(Long personId) throws Exception {
		
		dataListBehaviorAspectsOthers= FamilyVillageBehavioralAspectService.getBehaviorAspectsOtherss( personId );
		recordSizeBehaviorAspectsOthers=dataListBehaviorAspectsOthers.size();
	}


    
    @SuppressWarnings( "unchecked" )
	public String onNavigateToBehaviorMonitoringForm()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
			sessionMap.put(WebConstants.PERSON_ID, personId);
			return "behaviormonitoringform";
		}
		catch ( Exception e )
		{
			logger.LogException( "onNavigateToBehaviorMonitoringForm--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
			return "";
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.BEHAVIORAL_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.BEHAVIORAL_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
		
	}
    
    @SuppressWarnings( "unchecked" )
	public String onNavigateToBehaviorAssessmentDuringActiviesForm()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
			sessionMap.put(WebConstants.PERSON_ID, personId);
			return "behaviorduringActivities";
		}
		catch ( Exception e )
		{
			logger.LogException( "onNavigateToBehaviorAssessmentDuringActiviesForm--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.BEHAVIORAL_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.BEHAVIORAL_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		return "";
		
	}
	@SuppressWarnings( "unchecked" )
	public void onDeleteBehaviorAspectsGeneral()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteBehaviorAspectsGeneral--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.BEHAVIORAL_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.BEHAVIORAL_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
	}
	
	@SuppressWarnings( "unchecked" )
	public void onDeleteBehaviorAspectsOthers()
	{
		errorMessages = new ArrayList<String>();
		successMessages = new ArrayList<String>();
		try
		{
			successMessages = new ArrayList<String>();
		}
		catch ( Exception e )
		{
			logger.LogException( "onDeleteBehaviorAspectsOthers--- EXCEPTION --- ", e );
			errorMessages.add( ResourceUtil.getInstance().getProperty( MessageConstants.CommonErrorMessage.MSG_COMMON_ERROR ) );
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.BEHAVIORAL_ASPECT_TAB_ERRORS, errorMessages);
			}
			else if(	successMessages != null && successMessages.size() > 0)
			{
				viewMap.put(WebConstants.ResearchFamilyVillageBeneficiary.BEHAVIORAL_ASPECT_TAB_SUCCESS, successMessages );
			}
		}
		
	}
	
	
	
	public boolean getIsTabModeUpdatable()
	{
		boolean isTabModeUpdatable = false;
		
		if ( tabMode.equalsIgnoreCase( Constant.EndowmentFile.MODE_UPDATABLE) )
			isTabModeUpdatable = true;
		
		return isTabModeUpdatable;
	}


	public String getTabMode() {
		return tabMode;
	}

	public void setTabMode(String tabMode) {
		this.tabMode = tabMode;
	}
	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public List<BehaviorAspectsGeneralView> getDataListBehaviorAspectsGeneral() {
		return dataListBehaviorAspectsGeneral;
	}

	public void setDataListBehaviorAspectsGeneral(
			List<BehaviorAspectsGeneralView> dataListBehaviorAspectsGeneral) {
		this.dataListBehaviorAspectsGeneral = dataListBehaviorAspectsGeneral;
	}

	public HtmlDataTable getDataTableBehaviorAspectsGeneral() {
		return dataTableBehaviorAspectsGeneral;
	}

	public void setDataTableBehaviorAspectsGeneral(
			HtmlDataTable dataTableBehaviorAspectsGeneral) {
		this.dataTableBehaviorAspectsGeneral = dataTableBehaviorAspectsGeneral;
	}

	public Integer getRecordSizeBehaviorAspectsGeneral() {
		return recordSizeBehaviorAspectsGeneral;
	}

	public void setRecordSizeBehaviorAspectsGeneral(
			Integer recordSizeBehaviorAspectsGeneral) {
		this.recordSizeBehaviorAspectsGeneral = recordSizeBehaviorAspectsGeneral;
	}

	public List<BehaviorAspectsOthersView> getDataListBehaviorAspectsOthers() {
		return dataListBehaviorAspectsOthers;
	}

	public void setDataListBehaviorAspectsOthers(
			List<BehaviorAspectsOthersView> dataListBehaviorAspectsOthers) {
		this.dataListBehaviorAspectsOthers = dataListBehaviorAspectsOthers;
	}

	public HtmlDataTable getDataTableBehaviorAspectsOthers() {
		return dataTableBehaviorAspectsOthers;
	}

	public void setDataTableBehaviorAspectsOthers(
			HtmlDataTable dataTableBehaviorAspectsOthers) {
		this.dataTableBehaviorAspectsOthers = dataTableBehaviorAspectsOthers;
	}

	public Integer getRecordSizeBehaviorAspectsOthers() {
		return recordSizeBehaviorAspectsOthers;
	}

	public void setRecordSizeBehaviorAspectsOthers(
			Integer recordSizeBehaviorAspectsOthers) {
		this.recordSizeBehaviorAspectsOthers = recordSizeBehaviorAspectsOthers;
	}

}
