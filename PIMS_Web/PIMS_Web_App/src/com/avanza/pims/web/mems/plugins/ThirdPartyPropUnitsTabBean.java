package com.avanza.pims.web.mems.plugins;

import static com.avanza.pims.web.util.validator.ThirdPartyPropUnitValidator.isFloorNumberValidated;
import static com.avanza.pims.web.util.validator.ThirdPartyPropUnitValidator.isUnitNumberValidated;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.context.FacesContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.pims.business.exceptions.PimsBusinessException;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.entity.Endowment;
import com.avanza.pims.web.MessageConstants;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.ThirdPartyPropUnitsService;
import com.avanza.pims.ws.mems.endowment.EndowmentService;
import com.avanza.pims.ws.vo.ThirdPartyPropUnitsView;
import com.avanza.pims.ws.vo.mems.AssetMemsView;
public class ThirdPartyPropUnitsTabBean extends AbstractMemsBean {
	private static final long serialVersionUID = 1L;
	private ThirdPartyPropUnitsService thirdPartyService = new ThirdPartyPropUnitsService();;
	private HtmlDataTable dtThirdPartyPropUnit;
	private List<ThirdPartyPropUnitsView> thirdPartyPropUnits = new ArrayList<ThirdPartyPropUnitsView>();
	private ThirdPartyPropUnitsView thirdPartyPropUnit = new ThirdPartyPropUnitsView();
	private HashMap<String, Object> dataMap = new HashMap<String, Object>();;
	private HtmlInputText txtUnitNumber = new HtmlInputText();
	private HtmlInputText txtFloorNumber = new HtmlInputText();
	private HtmlInputTextarea txtDescription = new HtmlInputTextarea();
	private Boolean viewModePopup =false;
	public static class Keys{
		static String SELECTED_UNIT = "SELECTED_UNIT";
		static String ALL_UNITS = "ALL_UNITS";
		static String ASSET_ID = "ASSET_ID";
		static String THIRD_PARTY_UNIT = "THIRD_PARTY_UNIT";
		static String ASSET_MEMS = "ASSET_MEMS";
		static String VIEW_MODE_POPUP = "VIEW_MODE_POPUP";
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void init() 
	{
		super.init();
		if (isPostBack()) {return;}
		try 
		{
			populateDataFromParent();
		}
		catch (Exception e) 
		{
			logger.LogException("init() Crashed", e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}

	@SuppressWarnings( "unchecked" )
	private void populateDataFromParent() throws PimsBusinessException,Exception 
	{
		if (viewMap.get(WebConstants.ASSET_ID) != null) 
		{
			populateDataFromInheritanceAsset(
												viewMap.get(WebConstants.ASSET_ID).toString()
											);
		}
		else if (sessionMap.get(Constant.Endowments.ENDOWMENT) != null)
		{
			String endowmentId = sessionMap.remove(Constant.Endowments.ENDOWMENT).toString();
			viewMap.put(Constant.Endowments.ENDOWMENT,endowmentId); 
			populateDataFromEndowment(endowmentId);
		}
		if( sessionMap.get(WebConstants.VIEW_MODE) != null)
		{
			viewMap.put( WebConstants.VIEW_MODE,sessionMap.remove(WebConstants.VIEW_MODE) );  
		}
			 
	}
	@SuppressWarnings( "unchecked" )
	private void populateDataFromInheritanceAsset(String inheritanceAssetId )throws PimsBusinessException, Exception 
	{
		dataMap.put(WebConstants.ASSET_ID, inheritanceAssetId);
		setThirdPartyPropUnits(thirdPartyService.getAllThirdPartyUnitsByCriteria(dataMap));
		AssetMemsView view = new InheritanceFileService().getAssetMemsById(
				                      										Long.valueOf(	inheritanceAssetId	)
				                      									  );
		if(view != null && view.getAssetId() != null)
		{
			thirdPartyPropUnit.setPropertyNameEn(view.getAssetNameEn());
			thirdPartyPropUnit.setPropertyNameAr(view.getAssetNameAr());
			thirdPartyPropUnit.setPropertyRefNumber(view.getAssetNumber());
			setThirdPartyPropUnit(thirdPartyPropUnit);
		}
	}
	@SuppressWarnings( "unchecked" )
	private void populateDataFromEndowment(String endowmentId )throws Exception 
	{
		dataMap.put( Constant.Endowments.ENDOWMENT, endowmentId );
		setThirdPartyPropUnits(	thirdPartyService.getAllThirdPartyUnitsByCriteria(	dataMap	)	);
		Endowment endowment = new EndowmentService().getEndowmentById(
			                      										Long.valueOf(	endowmentId )
			                      									  );
		if(endowment != null  )
		{
			thirdPartyPropUnit.setPropertyNameEn(	endowment.getEndowmentName() );
			thirdPartyPropUnit.setPropertyNameAr(	endowment.getEndowmentName() );
			thirdPartyPropUnit.setPropertyRefNumber(	endowment.getEndowmentNum() );
			setThirdPartyPropUnit(thirdPartyPropUnit);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public void prerender() {
		super.prerender();
	}
	@SuppressWarnings( "unchecked" )
	public void onEdit()
	{
		try 
		{
			thirdPartyPropUnit = (ThirdPartyPropUnitsView) dtThirdPartyPropUnit.getRowData();
			int selectedIndex = dtThirdPartyPropUnit.getRowIndex();
			viewMap.put("SELECTED_INDEX", selectedIndex);
			setThirdPartyPropUnit(thirdPartyPropUnit);
			thirdPartyPropUnits = getThirdPartyPropUnits();
			
			if(thirdPartyPropUnits == null && thirdPartyPropUnits.size() <= 0){return;}
			for(ThirdPartyPropUnitsView unit :  thirdPartyPropUnits)
			{
				unit.setSelected(false);
			}
			thirdPartyPropUnit.setSelected(true);
			
		} 
		catch (Exception e) 
		{
			logger.LogException("onEdit() Crashed",e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void onDelete(){
		try {
			ThirdPartyPropUnitsView itemToBeDeleted =  (ThirdPartyPropUnitsView) dtThirdPartyPropUnit.getRowData();
			thirdPartyPropUnits = getThirdPartyPropUnits();
			
			if(thirdPartyPropUnits != null && thirdPartyPropUnits.size() > 0){
				thirdPartyPropUnits.remove(itemToBeDeleted);
			}
			itemToBeDeleted.setUpdatedBy(CommonUtil.getLoggedInUser());
			thirdPartyService.deleteThirdPartyUnits(itemToBeDeleted);
			
			setThirdPartyPropUnits(thirdPartyPropUnits);
			saveSystemComments(MessageConstants.ManageAssetEvent.UNIT_DELETED);
			successMessages.add(CommonUtil.getBundleMessage("thirdPartyUnit.successMsg.thirdPartyUniDeleted"));
			
		} catch (Exception e) {
			logger.LogException("onDelete() Crashed",e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put("ERROR_MESSAGES", errorMessages);
			}
			if(successMessages != null && successMessages.size() > 0)
			{
				viewMap.put("SUCCESS_MESSAGES", successMessages);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void onSave()
	{
		try 
		{
			ThirdPartyPropUnitsView thirdPartyPropUnitsView = null;
			thirdPartyPropUnitsView = (ThirdPartyPropUnitsView) BeanUtils.cloneBean( getThirdPartyPropUnit() );
			boolean isFirstTime = true;
			isFirstTime = thirdPartyPropUnitsView.getThirdPartyPropUnitId() == null;
			if( !isValidated(thirdPartyPropUnit) ){return;}
			thirdPartyService.persistThirdPartyUnits(
														getPopulatedThirdPartyUnit(thirdPartyPropUnitsView)
													);
			
			//getAll from table
			thirdPartyPropUnits = getThirdPartyPropUnits();
			
			if(viewMap.get("SELECTED_INDEX") != null)
			{
				int selectedIndex = Integer.valueOf(viewMap.remove("SELECTED_INDEX").toString());
				thirdPartyPropUnits.set(selectedIndex, thirdPartyPropUnitsView);
			}
			else
			{
				//Add new To Table
				thirdPartyPropUnits.add(thirdPartyPropUnitsView);
			}
			//Add in viewMap
			if(thirdPartyPropUnits != null && thirdPartyPropUnits.size() > 0)
			{
				for(ThirdPartyPropUnitsView unit :  thirdPartyPropUnits)
				{
					unit.setSelected(false);
				}
			}
			setThirdPartyPropUnits(thirdPartyPropUnits);
			
			if(isFirstTime)
			{
				saveSystemComments(MessageConstants.ManageAssetEvent.UNIT_ADDED);
				successMessages.add(CommonUtil.getBundleMessage("thirdPartyUnit.successMsg.thirdPartyUniAdded"));
			}
			else
			{
				saveSystemComments(MessageConstants.ManageAssetEvent.UNIT_UPDATED);
				successMessages.add(CommonUtil.getBundleMessage("thirdPartyUnit.successMsg.thirdPartyUniUpdated"));
			}
			clearFields();
				
		} 
		catch (Exception e) 
		{
			logger.LogException("onSave() Crashed",e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		finally
		{
			if(errorMessages != null && errorMessages.size() > 0)
			{
				viewMap.put("ERROR_MESSAGES", errorMessages);
			}
			if(successMessages != null && successMessages.size() > 0)
			{
				viewMap.put("SUCCESS_MESSAGES", successMessages);
			}
		}
	}

	private void clearFields() {
		thirdPartyPropUnit = getThirdPartyPropUnit();
		thirdPartyPropUnit.setUnitNumber(null);
		thirdPartyPropUnit.setFloorNumber(null);
		thirdPartyPropUnit.setDescription(null);
		setThirdPartyPropUnit(thirdPartyPropUnit);
	}

public ThirdPartyPropUnitsView getPopulatedThirdPartyUnit(ThirdPartyPropUnitsView thirdPartyPropUnit)
{
		Date today = new Date();
		if( viewMap.get(WebConstants.ASSET_ID) != null )
		{
			thirdPartyPropUnit.setAssetId(Long.parseLong(viewMap.get(WebConstants.ASSET_ID).toString()));
		}
		else if ( viewMap.get(Constant.Endowments.ENDOWMENT) != null)
		{
			thirdPartyPropUnit.setEndowmentId(
		 										Long.parseLong( viewMap.get(Constant.Endowments.ENDOWMENT).toString()	)
					                         );
		}
		thirdPartyPropUnit.setCreatedBy(CommonUtil.getLoggedInUser());
		thirdPartyPropUnit.setUpdatedBy(CommonUtil.getLoggedInUser());
		thirdPartyPropUnit.setCreatedOn(today);
		thirdPartyPropUnit.setUpdatedOn(today);
		thirdPartyPropUnit.setIsDeleted(0L);
		thirdPartyPropUnit.setRecordStatus(1L);
	return thirdPartyPropUnit;
}
	
	private boolean isValidated(ThirdPartyPropUnitsView unit) throws Exception {
		if(unit != null){
			if( !isUnitNumberValidated(unit.getUnitNumber())){
				errorMessages.add(CommonUtil.getBundleMessage("thirdPartyUnit.errMsg.unitNumberNotNull"));
				return false;
			}
			
			if( !isFloorNumberValidated(unit.getFloorNumber())){
				errorMessages.add(CommonUtil.getBundleMessage("thirdPartyUnit.errMsg.floorNumberNotNull"));
				return false;
			}
		}
		else{
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		return true;
	}

	public HtmlDataTable getDtThirdPartyPropUnit() {
		return dtThirdPartyPropUnit;
	}

	public void setDtThirdPartyPropUnit(HtmlDataTable dtThirdPartyPropUnit) {
		this.dtThirdPartyPropUnit = dtThirdPartyPropUnit;
	}

	@SuppressWarnings("unchecked")
	public List<ThirdPartyPropUnitsView> getThirdPartyPropUnits() 
	{
		if(viewMap.get(Keys.ALL_UNITS) != null)
		{
			thirdPartyPropUnits = (List<ThirdPartyPropUnitsView>) viewMap.get(Keys.ALL_UNITS); 
		}
		return thirdPartyPropUnits;
	}

	@SuppressWarnings("unchecked")
	public void setThirdPartyPropUnits(
			List<ThirdPartyPropUnitsView> thirdPartyPropUnits) {
		if(thirdPartyPropUnits != null && thirdPartyPropUnits.size() > 0){
			viewMap.put(Keys.ALL_UNITS, thirdPartyPropUnits);
		}
		this.thirdPartyPropUnits = thirdPartyPropUnits;
	}

	public ThirdPartyPropUnitsView getThirdPartyPropUnit() 
	{
		if(viewMap.get(Keys.THIRD_PARTY_UNIT) != null)
		{
			thirdPartyPropUnit = (ThirdPartyPropUnitsView) viewMap.get(Keys.THIRD_PARTY_UNIT); 
		}
		return thirdPartyPropUnit;
	}

	@SuppressWarnings("unchecked")
	public void setThirdPartyPropUnit(ThirdPartyPropUnitsView thirdPartyPropUnit) {
		if(thirdPartyPropUnit != null){
			viewMap.put(Keys.THIRD_PARTY_UNIT, thirdPartyPropUnit);
		}
		this.thirdPartyPropUnit = thirdPartyPropUnit;
	}
	public boolean isEnglishLocale(){
		return CommonUtil.getIsEnglishLocale();
	}
	private void saveSystemComments(String sysNote) throws Exception 
	{
		if(viewMap.get(WebConstants.ASSET_ID)!= null)
		{
			Long assetId = new Long(viewMap.get(WebConstants.ASSET_ID).toString());
			CommonUtil.saveSystemComments(WebConstants.ManageAssets.NOTES_OWNER, sysNote, assetId);
		}
	}
	private void executeJavascript(String javascript) 
	{
		final String METHOD_NAME = "executeJavascript()";
		try 
		{
			logger.logInfo(METHOD_NAME + " --- STARTED --- ");
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
			logger.logInfo(METHOD_NAME + " --- COMPLETED SUCCESSFULLY --- ");			
		} 
		catch (Exception exception) 
		{			
			logger.LogException( METHOD_NAME + " --- CRASHED --- ", exception);
		}
	}

	public HtmlInputText getTxtUnitNumber() {
		return txtUnitNumber;
	}

	public void setTxtUnitNumber(HtmlInputText txtUnitNumber) {
		this.txtUnitNumber = txtUnitNumber;
	}

	public HtmlInputText getTxtFloorNumber() {
		return txtFloorNumber;
	}

	public void setTxtFloorNumber(HtmlInputText txtFloorNumber) {
		this.txtFloorNumber = txtFloorNumber;
	}

	public HtmlInputTextarea getTxtDescription() {
		return txtDescription;
	}

	public void setTxtDescription(HtmlInputTextarea txtDescription) {
		this.txtDescription = txtDescription;
	}

	@SuppressWarnings("unchecked")
	public boolean isViewModePopup() 
	{
		if( viewMap.get(WebConstants.VIEW_MODE)!= null && 
		    viewMap.get(WebConstants.VIEW_MODE).toString().equals( Keys.VIEW_MODE_POPUP )
		  )
		{
		 return true;	
		}
		return false;
	}

	public void setViewModePopup(boolean viewModePopup) {
		
		this.viewModePopup = viewModePopup;
	}

	public Boolean getViewModePopup() {
		return viewModePopup;
	}

	public void setViewModePopup(Boolean viewModePopup) {
		this.viewModePopup = viewModePopup;
	}
}
