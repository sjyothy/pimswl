/**
 * 
 */
package com.avanza.pims.web.mems.minors;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.IncomeCategoryThresholdService;
import com.avanza.pims.ws.vo.IncomeCategoryThresholdView;

/**
 * @author Imran Ali
 *
 */
public class IncomeThresholdPopupBean extends AbstractMemsBean{

	private List<IncomeCategoryThresholdView> incomeCategoryThresholdList = new ArrayList<IncomeCategoryThresholdView>();
	private IncomeCategoryThresholdService categoryThresholdService = new IncomeCategoryThresholdService();
	private static Logger logger = Logger.getLogger("IncomeThresholdPopupBean");
	 
		/**
		 * @return the incomeCategoryThresholdList
		 */
		public List<IncomeCategoryThresholdView> getIncomeCategoryThresholdList() {
			try	
			{	
				incomeCategoryThresholdList = categoryThresholdService.getAllIncomeCategoryThreshold();
			
			}catch (Exception exception) 
			{
				
				logger.LogException( "onOpenIncomeThreshold --- CRASHED --- ", exception);
				errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
			}
			return incomeCategoryThresholdList;
		}
		/**
		 * @param incomeCategoryThresholdList the incomeCategoryThresholdList to set
		 */
		public void setIncomeCategoryThresholdList(
				List<IncomeCategoryThresholdView> incomeCategoryThresholdList) {
			this.incomeCategoryThresholdList = incomeCategoryThresholdList;
		}
		
}
