package com.avanza.pims.web.mems.plugins;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.richfaces.component.html.HtmlCalendar;

import com.avanza.core.util.StringHelper;
import com.avanza.pims.business.services.PropertyServiceAgent;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.InheritanceFile;
import com.avanza.pims.report.constant.ReportConstant;
import com.avanza.pims.report.criteria.StatementOfAccountCriteria;
import com.avanza.pims.util.list.ListComparator;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.mems.AbstractMemsBean;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.vo.DomainDataView;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegionView;

public class InheritanceFileBasicInfoTabBean extends AbstractMemsBean {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private HtmlInputText txtFileNumber;
	private HtmlInputText txtFilePerson;
	private HtmlInputText txtSponser;
	private HtmlSelectOneMenu cboFileType;
	private HtmlCalendar clndrMissingFrom;
	private HtmlCalendar clndrCreatedOn;
	private List<SelectItem> cityList;
	private List<SelectItem> passportCityList;
	
	private List<SelectItem> cityListSponsor;
	private boolean isVisible;
	private PersonView filePerson;
	private PersonView sponsor;
	private String testingId;

	private List<DomainDataView> fileTypeListDD;
	private List<DomainDataView> tenantTypeListDD;
	private DomainDataView companyDDV;
	private DomainDataView individualDDV;
	private DomainDataView deceasedDDV;
	private DomainDataView underGuardianshipDDV;
	private DomainDataView legalAssitanceDDV;
	private DomainDataView foundingDDV;
	private DomainDataView absentDDV;
	private static final String DECEASED = "DECEASED";
	private static final String UNDER_GUARDIANSHIP = "UNDER_GUARDIANSHIP";
	private static final String LEGAL_ASSISTANCE = "LEGAL_ASSISTANCE";
	private static final String FOUNDING = "FOUNDING";
	private static final String ABSENT = "ABSENT";
	private boolean isDeceased;
	private boolean isFounding;
	private boolean isLegalAsstnc;
	private boolean isAbsent;
	private boolean isUnderGuradian;
	private PersonView filePersonBeneficiary;
	private PersonView filePersonNurse;
	private PersonView filePersonGuardian;
	 private StatementOfAccountCriteria statmntReportCriteria;

	public void init() {
		super.init();
		if (!isPostBack()) {
			putDomainDataInViewMap();
		}
	}
    @SuppressWarnings("unchecked")
	public void afterUpdateModelValues()
    {
//    	setFilePerson(filePerson);
//    	
//    	 setFilePersonBeneficiary(filePersonBeneficiary);
//    	 setFilePersonGuardian(filePersonGuardian);
//    		setFilePersonNurse(filePersonNurse);
    }
//	public void renderFilePersonFieldLabel() {
//		if (viewMap.get(WebConstants.InheritanceFile.FILE_TYPE_ID) != null) {
//			Long selectedFileTypeId = -1L;
//			selectedFileTypeId = Long.parseLong(viewMap.get(
//					WebConstants.InheritanceFile.FILE_TYPE_ID).toString());
//			if (selectedFileTypeId.compareTo(-1L) != 0) {
//				if (viewMap.get(WebConstants.InheritanceFileType.INH_FILE_TYPE) != null) {
//					checkSelectedFileTypeId(selectedFileTypeId);
//				}
//			}
//		}
//	}

	private void putDomainDataInViewMap() {

	}

	public InheritanceFileBasicInfoTabBean() {
		txtFileNumber = new HtmlInputText();
		txtFilePerson = new HtmlInputText();
		txtSponser = new HtmlInputText();
		cboFileType = new HtmlSelectOneMenu();
		clndrMissingFrom = new HtmlCalendar();
		clndrCreatedOn = new HtmlCalendar();
		cityList = new ArrayList<SelectItem>();
		passportCityList = new ArrayList<SelectItem>();
		cityListSponsor = new ArrayList<SelectItem>();
		
		filePerson = new PersonView();
		sponsor = new PersonView();
		fileTypeListDD = new ArrayList<DomainDataView>();
		tenantTypeListDD = new ArrayList<DomainDataView>();
		companyDDV = new DomainDataView();
		individualDDV = new DomainDataView();
		deceasedDDV = new DomainDataView();
		underGuardianshipDDV = new DomainDataView();
		legalAssitanceDDV = new DomainDataView();
		foundingDDV = new DomainDataView();
		absentDDV = new DomainDataView();
		filePersonBeneficiary = new PersonView();
		filePersonNurse = new PersonView();
		filePersonGuardian = new PersonView();
	}

	public HtmlInputText getTxtFileNumber() {
		return txtFileNumber;
	}

	public void setTxtFileNumber(HtmlInputText txtFileNumber) {
		this.txtFileNumber = txtFileNumber;
	}

	public HtmlInputText getTxtFilePerson() {
		return txtFilePerson;
	}

	public void setTxtFilePerson(HtmlInputText txtFilePerson) {
		this.txtFilePerson = txtFilePerson;
	}

	public HtmlInputText getTxtSponser() {
		return txtSponser;
	}

	public void setTxtSponser(HtmlInputText txtSponser) {
		this.txtSponser = txtSponser;
	}

	public HtmlSelectOneMenu getCboFileType() {
		return cboFileType;
	}

	public void setCboFileType(HtmlSelectOneMenu cboFileType) {
		this.cboFileType = cboFileType;
	}

	public HtmlCalendar getClndrMissingFrom() {
		return clndrMissingFrom;
	}

	public void setClndrMissingFrom(HtmlCalendar clndrMissingFrom) {
		this.clndrMissingFrom = clndrMissingFrom;
	}

	public HtmlCalendar getClndrCreatedOn() {
		return clndrCreatedOn;
	}

	public void setClndrCreatedOn(HtmlCalendar clndrCreatedOn) {
		this.clndrCreatedOn = clndrCreatedOn;
	}

	@SuppressWarnings("unchecked")
	public void showControls() {
		viewMap.put("IS_VISIBLE", true);
	}

	public boolean isVisible() {
		if (viewMap.get("IS_VISIBLE") != null)
			isVisible = (Boolean) viewMap.get("IS_VISIBLE");
		else
			isVisible = false;
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

	@SuppressWarnings("unchecked")
	public void loadCity(Long selectedCoutryId, String personType) {
		PropertyServiceAgent psa = new PropertyServiceAgent();
		List<SelectItem> cmbCityList = new ArrayList<SelectItem>(0);
		try {

			Set<RegionView> regionViewList = null;
			if (selectedCoutryId != null && selectedCoutryId.compareTo(new Long(-1)) != 0) 
			{
				regionViewList = psa.getCity(selectedCoutryId);
				Iterator itrator = regionViewList.iterator();
			for (int i = 0; i < regionViewList.size(); i++) {
				RegionView rV = (RegionView) itrator.next();
				SelectItem item;
				if (CommonUtil.getIsEnglishLocale()) {
					item = new SelectItem(rV.getRegionId().toString(), rV
							.getDescriptionEn());
				} else
					item = new SelectItem(rV.getRegionId().toString(), rV
							.getDescriptionAr());

				cmbCityList.add(item);
			}
			Collections.sort(cmbCityList, ListComparator.LIST_COMPARE);
			
			if(personType.equals( "PassportIssuePlace" ) )
			{
				setPassportCityList(cmbCityList);
			}
			if(personType.compareToIgnoreCase(WebConstants.InheritanceFile.PERSON_TYPE_FILE_PERSON) == 0)
				viewMap.put(WebConstants.InheritanceFile.CITY_LIST, cmbCityList);
			else
				if(personType.compareToIgnoreCase(WebConstants.InheritanceFile.PERSON_TYPE_INHER_FILE_SPONSOR) == 0)
					viewMap.put(WebConstants.InheritanceFile.CITY_LIST_SPONSOR, cmbCityList);
		}
		} catch (Exception ex) {
			logger.LogException( "loadCity|Error Occured ", ex);

		}

	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getCityList() {
		if (viewMap.get(WebConstants.InheritanceFile.CITY_LIST) != null)
			cityList = (List<SelectItem>) viewMap
					.get(WebConstants.InheritanceFile.CITY_LIST);
		return cityList;
	}

	public void setCityList(List<SelectItem> cityList) {
		this.cityList = cityList;
	}

	public void countryChanged() 
	{
		if (filePerson.getCountryId() != null && filePerson.getCountryId().trim().length() > 0 )
			loadCity(Long.parseLong(filePerson.getCountryId()), WebConstants.InheritanceFile.PERSON_TYPE_FILE_PERSON);
//		Uncomment if u want to show the city e.r.t country
//		if(sponsor.getCountryId() != null)
//			loadCity(Long.parseLong(sponsor.getCountryId()), WebConstants.InheritanceFile.PERSON_TYPE_INHER_FILE_SPONSOR);
	}
	public void passportCountryChanged() 
	{
		if (getFilePerson().getPassportIssuePlaceIdString() != null && getFilePerson().getPassportIssuePlaceIdString().trim().length() > 0 )
			loadCity(Long.parseLong(getFilePerson().getPassportIssuePlaceIdString()), "PassportIssuePlace");
		getPassportCityList();
//		Uncomment if u want to show the city e.r.t country
//		if(sponsor.getCountryId() != null)
//			loadCity(Long.parseLong(sponsor.getCountryId()), WebConstants.InheritanceFile.PERSON_TYPE_INHER_FILE_SPONSOR);
	}
	public PersonView getFilePerson() {
		if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW) != null)
		{
			filePerson = (PersonView) viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW);
			
		}
		if(filePerson  != null )
		{
			if ( filePerson.getPassportName()==null || filePerson.getPassportName().trim().equals("") )
			{
				filePerson.setPassportName(filePerson.getPersonFullName());
			}
			setFilePerson(filePerson);
		}
		
		return filePerson;
	}

	@SuppressWarnings("unchecked")
	public void setFilePerson(PersonView filePerson) {
		if (filePerson != null )
			viewMap.put(
					WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW,
					filePerson);
		this.filePerson = filePerson;
	}

	public String getTestingId() {
		return testingId;
	}

	public void setTestingId(String testingId) {
		this.testingId = testingId;
	}

	@SuppressWarnings("unchecked")
	public void prerender() {
		super.prerender();
		if (filePerson.getCountryId() != null && filePerson.getCountryId().trim().length() > 0 )
		{
			loadCity(Long.parseLong(filePerson.getCountryId()),WebConstants.InheritanceFile.PERSON_TYPE_FILE_PERSON);
			
		}
		if (filePerson.getPassportIssuePlaceIdString() != null && filePerson.getPassportIssuePlaceIdString().trim().length() > 0 )
		{
			loadCity(Long.parseLong(filePerson.getPassportIssuePlaceIdString()),"PassportIssuePlace");
			
		}
	}
//TODO: moves to container bean
//	@SuppressWarnings("unchecked")
//	private void checkSelectedFileTypeId(Long selectedFileTypeId) {
//		populateFileTypesDDFromViewMap();
//		if (selectedFileTypeId.compareTo(absentDDV.getDomainDataId()) == 0) {
//			viewMap
//					.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
//							ABSENT);
//		} else if (selectedFileTypeId.compareTo(foundingDDV.getDomainDataId()) == 0) {
//			viewMap.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
//					FOUNDING);
//		} else if (selectedFileTypeId.compareTo(underGuardianshipDDV
//				.getDomainDataId()) == 0) {
//			viewMap.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
//					UNDER_GUARDIANSHIP);
//		} else if (selectedFileTypeId.compareTo(legalAssitanceDDV
//				.getDomainDataId()) == 0) {
//			viewMap.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
//					LEGAL_ASSISTANCE);
//		} else if (selectedFileTypeId.compareTo(deceasedDDV.getDomainDataId()) == 0) {
//			viewMap.put(WebConstants.InheritanceFile.SELECTED_FILE_TYPE,
//					DECEASED);
//		}
//
//	}

	
//TODO: moves to container bean
//	private void populateFileTypesDDFromViewMap() {
//		if (viewMap.get(WebConstants.InheritanceFileType.ABSENT) != null) {
//			absentDDV = (DomainDataView) viewMap
//					.get(WebConstants.InheritanceFileType.ABSENT);
//		}
//
//		if (viewMap.get(WebConstants.InheritanceFileType.FOUNDING) != null) {
//			foundingDDV = (DomainDataView) viewMap
//					.get(WebConstants.InheritanceFileType.FOUNDING);
//		}
//
//		if (viewMap.get(WebConstants.InheritanceFileType.LEGAL_ASSISTANCE) != null) {
//			legalAssitanceDDV = (DomainDataView) viewMap
//					.get(WebConstants.InheritanceFileType.LEGAL_ASSISTANCE);
//		}
//
//		if (viewMap.get(WebConstants.InheritanceFileType.DECEASED) != null) {
//			deceasedDDV = (DomainDataView) viewMap
//					.get(WebConstants.InheritanceFileType.DECEASED);
//		}
//
//		if (viewMap.get(WebConstants.InheritanceFileType.UNDER_GUARDIANSHIP) != null) {
//			underGuardianshipDDV = (DomainDataView) viewMap
//					.get(WebConstants.InheritanceFileType.UNDER_GUARDIANSHIP);
//		}
//	}

	public List<DomainDataView> getFileTypeListDD() {
		return fileTypeListDD;
	}

	public void setFileTypeListDD(List<DomainDataView> fileTypeListDD) {
		this.fileTypeListDD = fileTypeListDD;
	}

	public List<DomainDataView> getTenantTypeListDD() {
		return tenantTypeListDD;
	}

	public void setTenantTypeListDD(List<DomainDataView> tenantTypeListDD) {
		this.tenantTypeListDD = tenantTypeListDD;
	}

	public DomainDataView getCompanyDDV() {
		return companyDDV;
	}

	public void setCompanyDDV(DomainDataView companyDDV) {
		this.companyDDV = companyDDV;
	}

	public DomainDataView getIndividualDDV() {
		return individualDDV;
	}

	public void setIndividualDDV(DomainDataView individualDDV) {
		this.individualDDV = individualDDV;
	}

	public DomainDataView getDeceasedDDV() {
		return deceasedDDV;
	}

	public void setDeceasedDDV(DomainDataView deceasedDDV) {
		this.deceasedDDV = deceasedDDV;
	}

	public DomainDataView getUnderGuardianshipDDV() {
		return underGuardianshipDDV;
	}

	public void setUnderGuardianshipDDV(DomainDataView underGuardianshipDDV) {
		this.underGuardianshipDDV = underGuardianshipDDV;
	}

	public DomainDataView getLegalAssitanceDDV() {
		return legalAssitanceDDV;
	}

	public void setLegalAssitanceDDV(DomainDataView legalAssitanceDDV) {
		this.legalAssitanceDDV = legalAssitanceDDV;
	}

	public DomainDataView getFoundingDDV() {
		return foundingDDV;
	}

	public void setFoundingDDV(DomainDataView foundingDDV) {
		this.foundingDDV = foundingDDV;
	}

	public DomainDataView getAbsentDDV() {
		return absentDDV;
	}

	public void setAbsentDDV(DomainDataView absentDDV) {
		this.absentDDV = absentDDV;
	}

	public boolean isDeceased() {
		isDeceased = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) {
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null && StringHelper.isNotEmpty(fileType)
					&& fileType.compareTo(DECEASED) == 0)
				isDeceased = true;
		}
		return isDeceased;
	}

	public void setDeceased(boolean isDeceased) {
		this.isDeceased = isDeceased;
	}

	public boolean isFounding() {
		isFounding = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) {
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null && StringHelper.isNotEmpty(fileType)
					&& fileType.compareTo(FOUNDING) == 0)
				isFounding = true;
		}
		return isFounding;
	}

	public void setFounding(boolean isFounding) {
		this.isFounding = isFounding;
	}

	public boolean isLegalAsstnc() {
		isLegalAsstnc = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) {
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null && StringHelper.isNotEmpty(fileType)
					&& fileType.compareTo(LEGAL_ASSISTANCE) == 0)
				isLegalAsstnc = true;
		}
		return isLegalAsstnc;
	}

	public void setLegalAsstnc(boolean isLegalAsstnc) {
		this.isLegalAsstnc = isLegalAsstnc;
	}

	public boolean isAbsent() {
		isAbsent = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) {
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null && StringHelper.isNotEmpty(fileType)
					&& fileType.compareTo(ABSENT) == 0)
				isAbsent = true;
		}
		return isAbsent;
	}

	public void setAbsent(boolean isAbsent) {
		this.isAbsent = isAbsent;
	}

	public boolean isUnderGuradian() {
		isUnderGuradian = false;
		if (viewMap.get(WebConstants.InheritanceFile.SELECTED_FILE_TYPE) != null) {
			String fileType;
			fileType = viewMap.get(
					WebConstants.InheritanceFile.SELECTED_FILE_TYPE).toString();
			if (fileType != null && StringHelper.isNotEmpty(fileType)
					&& fileType.compareTo(UNDER_GUARDIANSHIP) == 0)
				isUnderGuradian = true;
		}
		return isUnderGuradian;
	}

	public void setUnderGuradian(boolean isUnderGuradian) {
		this.isUnderGuradian = isUnderGuradian;
	}

	public PersonView getSponsor() 
	{
		if (viewMap.get(WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW) != null)
			sponsor = (PersonView) viewMap.get(WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW);
		if (sponsor.getCountryId() != null && sponsor.getCountryId().trim().length() > 0 )
		{
			loadCity(Long.parseLong(sponsor.getCountryId()),WebConstants.InheritanceFile.PERSON_TYPE_INHER_FILE_SPONSOR);
		}
		return sponsor;
	}

	@SuppressWarnings("unchecked")
	public void setSponsor(PersonView sponsor) {
		if(sponsor != null && sponsor.getPersonId() != null)
			viewMap.put(WebConstants.InheritanceFile.INHERITANCE_SPONSOR_VIEW, sponsor);
		this.sponsor = sponsor;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getCityListSponsor() 
	{
		if (viewMap.get(WebConstants.InheritanceFile.CITY_LIST_SPONSOR) != null)
			cityListSponsor = (List<SelectItem>) viewMap.get(WebConstants.InheritanceFile.CITY_LIST_SPONSOR);
		return cityListSponsor;
	}

	public void setCityListSponsor(List<SelectItem> cityListSponsor) {
		this.cityListSponsor = cityListSponsor;
	}
	public boolean isPageModeView()
	{
		boolean pageModeView = false;
		if(viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE) != null)
		{
			String pageMode = viewMap.get(WebConstants.InheritanceFilePageMode.PAGE_MODE).toString();
			if(pageMode.equalsIgnoreCase(WebConstants.InheritanceFilePageMode.PAGE_MODE_READ_ONLY))
				pageModeView = true;
			
		}
		return pageModeView;
	}
	public void openStatmntOfAccount()
	{
		PersonView filePerson = null;
		if(viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW) != null)
		{
			filePerson = (PersonView) viewMap.get(WebConstants.InheritanceFile.INHERITANCE_FILE_PERSON_VIEW);
			if(filePerson != null && filePerson.getPersonId() != null )
			{
				statmntReportCriteria= new StatementOfAccountCriteria(ReportConstant.Report.STATEMENT_OF_ACCOUNT,ReportConstant.Processor.STATEMENT_OF_ACCOUNT,CommonUtil	.getLoggedInUser());
				statmntReportCriteria.setPersonId(filePerson.getPersonId());
				HttpServletRequest request = (HttpServletRequest)getFacesContext().getExternalContext().getRequest();
		    	request.getSession().setAttribute(ReportConstant.Keys.REPORT_CRITERIA_KEY, statmntReportCriteria);
				executeJavascript("openLoadReportPopup('" + ReportConstant.LOAD_REPORT_JSP + "');");
			}
		}
	}
	private void executeJavascript(String javascript) 
	{
		try 
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();			
			AddResource addResource = AddResourceFactory.getInstance(facesContext);
			addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javascript);
		} 
		catch (Exception exception) 
		{			
			logger.LogException(  " executeJavascript--- CRASHED --- ", exception);
		}
	}

	public PersonView getFilePersonBeneficiary() 
	{
		if (viewMap	.get(WebConstants.InheritanceFile.FILE_PERSON_BEENFICIARY) != null)
		{
			filePersonBeneficiary = (PersonView) viewMap.get(WebConstants.InheritanceFile.FILE_PERSON_BEENFICIARY);
		}
		if(filePersonBeneficiary != null)
		{
			setFilePersonBeneficiary(filePersonBeneficiary);
		}
		return filePersonBeneficiary;
	}

	@SuppressWarnings("unchecked")
	public void setFilePersonBeneficiary(PersonView filePersonBeneficiary) 
	{
		if(filePersonBeneficiary != null && filePersonBeneficiary.getPersonId() != null)
			viewMap.put(WebConstants.InheritanceFile.FILE_PERSON_BEENFICIARY, filePersonBeneficiary);
		this.filePersonBeneficiary = filePersonBeneficiary;
	}

	public PersonView getFilePersonNurse() 
	{
		if (viewMap	.get(WebConstants.InheritanceFile.FILE_PERSON_NURSE) != null)
		{
			filePersonNurse = (PersonView) viewMap.get(WebConstants.InheritanceFile.FILE_PERSON_NURSE);
		}
		if(filePersonNurse != null)
		{
			setFilePersonNurse(filePersonNurse);
		}
		return filePersonNurse;
	}

	@SuppressWarnings("unchecked")
	public void setFilePersonNurse(PersonView filePersonNurse) 
	{
		if(filePersonNurse != null && filePersonNurse.getPersonId() != null)
			viewMap.put(WebConstants.InheritanceFile.FILE_PERSON_NURSE, filePersonNurse);
		this.filePersonNurse = filePersonNurse;
	}

	public PersonView getFilePersonGuardian() 
	{
		if (viewMap	.get(WebConstants.InheritanceFile.FILE_PERSON_GUARDIAN) != null)
		{
			filePersonGuardian = (PersonView) viewMap.get(WebConstants.InheritanceFile.FILE_PERSON_GUARDIAN);
		}
		if(filePersonGuardian != null )
		{
			setFilePersonGuardian(filePersonGuardian);
		}
		return filePersonGuardian;
	}

	@SuppressWarnings("unchecked")
	public void setFilePersonGuardian(PersonView filePersonGuardian) 
	{
		if(filePersonGuardian != null && filePersonGuardian.getPersonId() != null)
			viewMap.put(WebConstants.InheritanceFile.FILE_PERSON_GUARDIAN, filePersonGuardian);
		this.filePersonGuardian = filePersonGuardian;
	}
	@SuppressWarnings("unchecked")
	public void openResearcherForm()
	{
		try {
			if (viewMap.get(WebConstants.InheritanceFile.FILE_ID) != null) {
				long fileId = Long.valueOf(viewMap.get(WebConstants.InheritanceFile.FILE_ID).toString());
				long personId;
				InheritanceFile file = EntityManager.getBroker().findById(InheritanceFile.class, fileId);
				personId = file.getFileOwner().getPersonId();
				sessionMap.put(WebConstants.PERSON_ID, personId);
				sessionMap.put(
						WebConstants.InheritanceFile.INHERITANCE_FILE_ID,
						fileId);
				sessionMap.put(WebConstants.PERSON_TYPE, WebConstants.PERSON_TYPE_OWNER);
				executeJavascript("openResearchForm();");
			}
			else
			{
				errorMessages.add(CommonUtil.getBundleMessage("errMsg.inheritanceFile.fileMustBeSaved"));
			}
		} catch (Exception e) {
			logger.LogException("openResearcherForm |Error Occured ", e);
			errorMessages.add(CommonUtil.getBundleMessage("commons.ErrorMessage"));
		}
		if(errorMessages != null && errorMessages.size() > 0)
			viewMap.put(WebConstants.InherFileErrMsg.FILE_PERSON_BASIC_INFO_ERR, errorMessages);
	}
	public void deleteNurseFromBeneDetail(){
		if(viewMap	.get(WebConstants.InheritanceFile.FILE_PERSON_NURSE) != null){
			viewMap.remove(WebConstants.InheritanceFile.FILE_PERSON_NURSE);
			filePersonNurse = new PersonView();
		}
	}
	@SuppressWarnings("unchecked")
	public void deleteGuardianFromBeneDetail(){
		if(viewMap	.get(WebConstants.InheritanceFile.FILE_PERSON_GUARDIAN) != null){
			viewMap.remove(WebConstants.InheritanceFile.FILE_PERSON_GUARDIAN);
			filePersonGuardian = new PersonView();
		}
	}
	public List<SelectItem> getPassportCityList() {
		if ( viewMap.get("PassportCityList") != null )
		{
			passportCityList = (ArrayList<SelectItem>)viewMap.get("PassportCityList");
		}
		return passportCityList;
	}
	public void setPassportCityList(List<SelectItem> passportCityList) {
		this.passportCityList = passportCityList;
		if(this.passportCityList != null )
		{
			viewMap.put("PassportCityList",this.passportCityList); 
		}
	}
}
