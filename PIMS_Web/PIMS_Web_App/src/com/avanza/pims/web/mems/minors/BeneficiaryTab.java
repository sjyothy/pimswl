package com.avanza.pims.web.mems.minors;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.pims.business.services.UtilityServiceAgent;
import com.avanza.pims.web.WebConstants;
import com.avanza.pims.web.controller.AbstractController;
import com.avanza.pims.web.util.CommonUtil;
import com.avanza.pims.web.util.SystemParameters;
import com.avanza.pims.ws.mems.AssetMemsService;
import com.avanza.pims.ws.vo.PaymentReceiptDetailView;
import com.avanza.pims.ws.vo.PaymentReceiptView;
import com.avanza.pims.ws.vo.PaymentScheduleView;
import com.avanza.pims.ws.vo.mems.BeneficiaryTabView;

public class BeneficiaryTab extends AbstractController


{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5160927008167503230L;
	UtilityServiceAgent objUtilityServiceAgent = new UtilityServiceAgent();
	PaymentReceiptView objPaymentReceiptView = new PaymentReceiptView();
	PaymentReceiptDetailView objPaymentReceiptDetailView = new PaymentReceiptDetailView();
	PaymentReceiptDetailView dataItem = new PaymentReceiptDetailView();
	private HtmlInputText chequeNo = new HtmlInputText();
	private HtmlInputHidden chequeNoHidden = new HtmlInputHidden();
	private HtmlInputHidden accountNoHidden = new HtmlInputHidden();
	private HtmlInputHidden transactionAmountHidden = new HtmlInputHidden();
	private HtmlInputHidden chequeDateHidden = new HtmlInputHidden();
	private HtmlInputHidden bankIdHidden = new HtmlInputHidden();
	private HtmlInputHidden cashAmountHidden = new HtmlInputHidden();
	SystemParameters parameters = SystemParameters.getInstance();
	  DateFormat df = new SimpleDateFormat(parameters.getParameter(WebConstants.SHORT_DATE_FORMAT));

	private HtmlSelectOneMenu banksMenu = new HtmlSelectOneMenu();
	private String selectOneBank ;
	private String BANK_VIEW_LIST="bankViewList";
	private Double chequeAmountReceived;
	
	private HtmlInputText chequeDate =new HtmlInputText();
	private HtmlInputText transactionAmount = new HtmlInputText();
	private HtmlInputText cashAmount = new HtmlInputText();
	private HtmlInputText totalAmount = new HtmlInputText();
	private HtmlInputText totalAmountReturned = new HtmlInputText();
	private HtmlInputText receiptAmount  = new HtmlInputText();
	private HtmlSelectOneMenu paymentInstrument = new HtmlSelectOneMenu();
	private HtmlSelectOneMenu currencyMenu= new HtmlSelectOneMenu();
	private List<SelectItem> banks = new ArrayList<SelectItem>();
	private List<SelectItem> paymentMode = new ArrayList<SelectItem>();
	private List<SelectItem> currencies = new ArrayList<SelectItem>();
	private HtmlInputText transactionalComment = new HtmlInputText();
	private HtmlInputText exchangeRate = new HtmlInputText();
	private HtmlInputText accountNo = new HtmlInputText();


	private HtmlInputHidden editMode = new HtmlInputHidden();
	private HtmlInputText totalCashAmount = new HtmlInputText();
	private HtmlInputText totalChequeAmount = new HtmlInputText();
	private HtmlInputText totalReceiptAmount = new HtmlInputText();
	private HtmlInputText totalPaidBackAmount = new HtmlInputText();

	
///End of variables used in tabs

	private HtmlCommandButton buttonSave = new HtmlCommandButton();
	
	private HtmlCommandButton buttonOk = new HtmlCommandButton();
	private ResourceBundle bundle;
	private HtmlCommandButton buttonCancel = new HtmlCommandButton();
	private HtmlInputHidden add = new HtmlInputHidden();
//	private List<PaymentReceiptDetailView> dataList = new ArrayList<PaymentReceiptDetailView>();
	private List<BeneficiaryTabView> dataList = new ArrayList<BeneficiaryTabView>();
//	private List<PaymentReceiptDetailView> dataList2 = new ArrayList<PaymentReceiptDetailView>();
	private HtmlDataTable dataTable;	
	
	private HtmlInputHidden hdnParentVarName = new HtmlInputHidden();
	private transient Logger logger = Logger.getLogger(BeneficiaryTab.class);
	
	//added for richfaces calendar
	private Date chequeDateDt;
	//added for richfaces calendar
	
	//For Multiple Paymments
	private HtmlDataTable dataTablePaymentSchedule;
	private List<PaymentScheduleView> paymentSchedules = new ArrayList<PaymentScheduleView>();
	////////////////////////////
	
	//Amounts ///////////////////
	private Double totalCashAmountToBePaid;
	private double totalBankTransferAmountToBePaid;
	private double totalChqAmountToBePaid;
	private double totalCashAmountPaid;
	private double totalChqAmountPaid;
	/////////////////////////////
	
	private List<String> errorMessages = new ArrayList<String>();
	
	public String chqType;
	
	private String todayDateString;
	
	private String dateFormatForDataTable;
	
	private boolean isEnglishLocale = false;
	private boolean isArabicLocale = false;
	private Boolean chequeTabRendered ;
	
	
	// BankTransfer Fields End
	
	
	FacesContext context = FacesContext.getCurrentInstance();
	@SuppressWarnings("unchecked")
	Map viewRootMap=context.getViewRoot().getAttributes();
	@SuppressWarnings("unchecked")
	Map session = getFacesContext().getExternalContext().getSessionMap();

	
	public boolean getIsArabicLocale()
	{
		isArabicLocale = !getIsEnglishLocale();
		
		return isArabicLocale;
	}
	
	public boolean getIsEnglishLocale()
	{

		WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		isEnglishLocale =  localeInfo.getLanguageCode().equalsIgnoreCase("en");
		
		return isEnglishLocale;
	}
	@SuppressWarnings( "unchecked" )
	public void init(){
		
		super.init();
		try
		{
			Map viewMap = viewRootMap;
						
			if(!isPostBack())
			{
				
			
				if(viewMap.get(WebConstants.ASSET_ID)!=null)
				{
					loadDataList(Long.parseLong(viewMap.get(WebConstants.ASSET_ID).toString()));
					}
				
			
			
				
			}

			



		}
		catch(Exception e){
			logger.LogException("init|Error Occured", e );
		}
//		loadDataList();

	}
	@SuppressWarnings("unchecked")
	public void preprocess()
	{

		super.preprocess();
	

	}


		
	public List<BeneficiaryTabView> getAssetDataList()
	{
		dataList = (List<BeneficiaryTabView>)viewRootMap.get("beneficiaryList");
		if(dataList==null)
			dataList = new ArrayList<BeneficiaryTabView>();
		return dataList;
	}

	
	private void loadDataList(Long assetId) 
	{
		 if(dataList != null)
			 dataList.clear();
		 
		List<BeneficiaryTabView> list = new ArrayList<BeneficiaryTabView>();
		 
		try
		{
//			
			
			AssetMemsService assetMems = new AssetMemsService();		
			
			//
			list =  assetMems.getBeneficiariesByAssetId(assetId,CommonUtil.getIsEnglishLocale());
//			
			viewRootMap.put("beneficiaryList", list);
	    }
		catch (Exception ex) 
		{
	        ex.printStackTrace();
	    } 
		
	}
	
    
    
	
    @SuppressWarnings("unchecked")
	public void prerender(){
		super.prerender();
		
	}
	
    @SuppressWarnings("unchecked")
    private String getLoggedInUser() 
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext()
				.getSession(true);
		String loggedInUser = "";

		if (session.getAttribute(WebConstants.USER_IN_SESSION) != null) {
			UserDbImpl user = (UserDbImpl) session
					.getAttribute(WebConstants.USER_IN_SESSION);
			loggedInUser = user.getLoginId();
		}

		return loggedInUser;
	}
    @SuppressWarnings("unchecked")    
	public String getLocale()
    {
		WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = ctx.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getLanguageCode();
	}
	
	public String getDateFormat(){
    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
		LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
		return localeInfo.getDateFormat();
    }
	
	public String getStringFromDate(Date dateVal){
		String pattern = getDateFormat();
		DateFormat formatter = new SimpleDateFormat(pattern);
		String dateStr = "";
		try{
			if(dateVal!=null)
				dateStr = formatter.format(dateVal);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		return dateStr;
	}
	@SuppressWarnings("unchecked")
	public Date getChequeDateDt() {
		if(viewRootMap.containsKey("chequeDateDt") && 
				viewRootMap.get("chequeDateDt")!=null &&
				viewRootMap.get("chequeDateDt").toString().trim().length()>0)
			chequeDateDt=(Date)viewRootMap.get("chequeDateDt");
		else
			chequeDateDt=null;
		return chequeDateDt;
	}
	@SuppressWarnings("unchecked")
	public void setChequeDateDt(Date chequeDateDt) {
		this.chequeDateDt = chequeDateDt;
		if(this.chequeDateDt !=null)
		viewRootMap.put("chequeDateDt",this.chequeDateDt);
		else
			viewRootMap.put("chequeDateDt","");
	}
	///////////added for richfaces calender

	//////////added for showing record size with the paginator
	public Integer getRecordSize(){
		if (dataList != null) {
			return dataList.size();
		}
		else{
			return 0;
		}
	}
	//////////added for showing record size with the paginator
	
	
	public HtmlCommandButton getButtonSave() {
		return buttonSave;
	}
	public void setButtonSave(HtmlCommandButton buttonSave) {
		this.buttonSave = buttonSave;
	}
	public HtmlInputText getCashAmount() {
		if(totalCashAmountToBePaid!=null && totalCashAmountToBePaid >0)
		   cashAmount.setValue(totalCashAmountToBePaid);
		if(cashAmount.getValue()==null||cashAmount.getValue().toString().trim().length()==0)
		   cashAmount.setValue("0.00");
		
		return cashAmount;
	}
	public void setCashAmount(HtmlInputText cashAmount) {
		this.cashAmount = cashAmount;
	}
	public HtmlInputText getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(HtmlInputText chequeDate) {
		this.chequeDate = chequeDate;
	}
	public HtmlInputText getChequeNo() {
		return chequeNo;
	}
	public void setChequeNo(HtmlInputText chequeNo) {
		this.chequeNo = chequeNo;
	}
	public List<SelectItem> getCurrencies() {
		return currencies;
	}
	public void setCurrencies(List<SelectItem> currencies) {
		this.currencies = currencies;
	}
	public List<BeneficiaryTabView> getDataList() {
		return dataList;
	}
	public void setDataList(List<BeneficiaryTabView> dataList) {
		this.dataList = dataList;
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public HtmlInputHidden getEditMode() {
		return editMode;
	}
	public void setEditMode(HtmlInputHidden editMode) {
		this.editMode = editMode;
	}
	public HtmlSelectOneMenu getPaymentInstrument() {
		return paymentInstrument;
	}
	public void setPaymentInstrument(HtmlSelectOneMenu paymentInstrument) {
		this.paymentInstrument = paymentInstrument;
	}
	public List<SelectItem> getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(List<SelectItem> paymentMode) {
		this.paymentMode = paymentMode;
	}
	public HtmlInputText getReceiptAmount() {
		if(receiptAmount.getValue()==null||receiptAmount.getValue().toString().trim().length()==0)
			receiptAmount.setValue("0.00");
		return receiptAmount;

	}
	public void setReceiptAmount(HtmlInputText receiptAmount) {
		this.receiptAmount = receiptAmount;
	}


	public HtmlInputText getTotalAmount() {
		if(totalAmount.getValue()==null||totalAmount.getValue().toString().trim().length()==0){

			totalAmount.setValue("0.00");
		}
		return totalAmount;
	}
	public void setTotalAmount(HtmlInputText totalAmount) {
		this.totalAmount = totalAmount;
	}
	public HtmlInputText getTotalAmountReturned() {
		return totalAmountReturned;
	}
	public void setTotalAmountReturned(HtmlInputText totalAmountReturned) {
		this.totalAmountReturned = totalAmountReturned;
	}
	public HtmlInputText getTransactionalComment() {
		return transactionalComment;
	}
	public void setTransactionalComment(HtmlInputText transactionalComment) {
		this.transactionalComment = transactionalComment;
	}
	public HtmlInputText getTransactionAmount() {
		if(transactionAmount.getValue()==null||transactionAmount.getValue().toString().trim().length()==0){

			transactionAmount.setValue("0.00");
		}
		return transactionAmount;
	}
	public void setTransactionAmount(HtmlInputText transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public List<SelectItem> getBanks() {
		return banks;
	}
	public void setBanks(List<SelectItem> banks) {
		this.banks = banks;
	}
	public HtmlSelectOneMenu getBanksMenu() {
		return banksMenu;
	}
	public void setBanksMenu(HtmlSelectOneMenu banksMenu) {
		this.banksMenu = banksMenu;
	}
	public HtmlSelectOneMenu getCurrencyMenu() {
		return currencyMenu;
	}
	public void setCurrencyMenu(HtmlSelectOneMenu currencyMenu) {
		this.currencyMenu = currencyMenu;
	}
	public HtmlInputText getTotalCashAmount() {
		if(totalCashAmount.getValue()==null||totalCashAmount.getValue().toString().trim().length()==0){

			totalCashAmount.setValue("0.00");
		}

		return totalCashAmount;
	}
	public void setTotalCashAmount(HtmlInputText totalCashAmount) {
		this.totalCashAmount = totalCashAmount;
	}
	public HtmlInputText getTotalChequeAmount() {
		if(totalChequeAmount.getValue()==null||totalChequeAmount.getValue().toString().trim().length()==0){

			totalChequeAmount.setValue("0.00");
		}

		return totalChequeAmount;
	}
	public void setTotalChequeAmount(HtmlInputText totalChequeAmount) {
		this.totalChequeAmount = totalChequeAmount;
	}
	public HtmlInputText getTotalPaidBackAmount() {
		if(totalPaidBackAmount.getValue()==null||totalPaidBackAmount.getValue().toString().trim().length()==0){

			totalPaidBackAmount.setValue("0.00");
		}

		return totalPaidBackAmount;
	}
	public void setTotalPaidBackAmount(HtmlInputText totalPaidBackAmount) {
		this.totalPaidBackAmount = totalPaidBackAmount;
	}
	public HtmlInputText getTotalReceiptAmount() {
		if(totalReceiptAmount.getValue()==null||totalReceiptAmount.getValue().toString().trim().length()==0){

			totalReceiptAmount.setValue("0.00");
		}

		return totalReceiptAmount;
	}
	public void setTotalReceiptAmount(HtmlInputText totalReceiptAmount) {
		this.totalReceiptAmount = totalReceiptAmount;
	}
	public HtmlInputText getExchangeRate() {
		if(exchangeRate.getValue()==null||exchangeRate.getValue().toString().trim().length()==0){

			exchangeRate.setValue("1.00");
		}

		return exchangeRate;
	}
	public void setExchangeRate(HtmlInputText exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public HtmlCommandButton getButtonCancel() {
		return buttonCancel;
	}

	public void setButtonCancel(HtmlCommandButton buttonCancel) {
		this.buttonCancel = buttonCancel;
	}

	public HtmlCommandButton getButtonOk() {
		return buttonOk;
	}

	public void setButtonOk(HtmlCommandButton buttonOk) {
		this.buttonOk = buttonOk;
	}

	public HtmlInputText getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(HtmlInputText accountNo) {
		this.accountNo = accountNo;
	}


	public HtmlInputHidden getAccountNoHidden() {
		return accountNoHidden;
	}
	public void setAccountNoHidden(HtmlInputHidden accountNoHidden) {
		this.accountNoHidden = accountNoHidden;
	}
	public HtmlInputHidden getBankIdHidden() {
		return bankIdHidden;
	}
	public void setBankIdHidden(HtmlInputHidden bankIdHidden) {
		this.bankIdHidden = bankIdHidden;
	}
	public HtmlInputHidden getChequeDateHidden() {
		return chequeDateHidden;
	}
	public void setChequeDateHidden(HtmlInputHidden chequeDateHidden) {
		this.chequeDateHidden = chequeDateHidden;
	}
	public HtmlInputHidden getChequeNoHidden() {
		return chequeNoHidden;
	}
	public void setChequeNoHidden(HtmlInputHidden chequeNoHidden) {
		this.chequeNoHidden = chequeNoHidden;
	}
	public HtmlInputHidden getTransactionAmountHidden() {
		return transactionAmountHidden;
	}
	public void setTransactionAmountHidden(HtmlInputHidden transactionAmountHidden) {
		this.transactionAmountHidden = transactionAmountHidden;
	}
	public HtmlInputHidden getAdd() {
		return add;
	}
	public void setAdd(HtmlInputHidden add) {
		this.add = add;
	}
	public HtmlInputHidden getCashAmountHidden() {
		return cashAmountHidden;
	}
	public void setCashAmountHidden(HtmlInputHidden cashAmountHidden) {
		this.cashAmountHidden = cashAmountHidden;
	}
	public HtmlInputHidden getHdnParentVarName() {
		return hdnParentVarName;
	}
	public void setHdnParentVarName(HtmlInputHidden hdnParentVarName) {
		this.hdnParentVarName = hdnParentVarName;
	}
	public HtmlDataTable getDataTablePaymentSchedule() {
		return dataTablePaymentSchedule;
	}
	public void setDataTablePaymentSchedule(HtmlDataTable dataTablePaymentSchedule) {
		this.dataTablePaymentSchedule = dataTablePaymentSchedule;
	}
	@SuppressWarnings("unchecked")
	public List<PaymentScheduleView> getPaymentSchedules() {
		
		paymentSchedules = (List<PaymentScheduleView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION);
		return paymentSchedules;
	}
	public void setPaymentSchedules(List<PaymentScheduleView> paymentSchedules) {
		this.paymentSchedules = paymentSchedules;
	}
	public Double getTotalCashAmountToBePaid() {
		return totalCashAmountToBePaid;
	}
	public void setTotalCashAmountToBePaid(Double totalCashAmountToBePaid) {
		this.totalCashAmountToBePaid = totalCashAmountToBePaid;
	}
	public double getTotalChqAmountToBePaid() {
		return totalChqAmountToBePaid;
	}
	public void setTotalChqAmountToBePaid(double totalChqAmountToBePaid) {
		this.totalChqAmountToBePaid = totalChqAmountToBePaid;
	}
	public double getTotalCashAmountPaid() {
		return totalCashAmountPaid;
	}
	public void setTotalCashAmountPaid(double totalCashAmountPaid) {
		this.totalCashAmountPaid = totalCashAmountPaid;
	}
	public double getTotalChqAmountPaid() {
		return totalChqAmountPaid;
	}
	public void setTotalChqAmountPaid(double totalChqAmountPaid) {
		this.totalChqAmountPaid = totalChqAmountPaid;
	}

	@SuppressWarnings("unchecked")
	public void calculateSummaray()
	{
		paymentSchedules = (List<PaymentScheduleView>) viewRootMap.get(WebConstants.SESSION_CONTRACT_SELECTED_PAYMENT_SCHEDULE_FOR_COLLECTION);
		
		Long cashId = (Long) viewRootMap.get("cashId");
		Long chqId = (Long) viewRootMap.get("chqId");
		Long bankTransferID = (Long) viewRootMap.get("bankTransferID");
		totalCashAmountToBePaid = 0d;
		
		for (int i=0; i<paymentSchedules.size(); i++)
		{
			
			
			//Checking Mode
			if (paymentSchedules.get(i).getPaymentModeId() != null)
				{
					if (paymentSchedules.get(i).getPaymentModeId().longValue() == cashId.longValue())
					{
						totalCashAmountToBePaid = totalCashAmountToBePaid +  paymentSchedules.get(i).getAmount();
					}
					else if (paymentSchedules.get(i).getPaymentModeId().longValue() == chqId.longValue())
					{
						totalChqAmountToBePaid = totalChqAmountToBePaid + paymentSchedules.get(i).getAmount();
					}
					//cashAmount.setValue(totalCashAmountToBePaid);
					//calculate sum of BankTransfer amount
					else if (paymentSchedules.get(i).getPaymentModeId().longValue() == bankTransferID.longValue())
					{
						totalBankTransferAmountToBePaid += paymentSchedules.get(i).getAmount();
						viewRootMap.put("TOTAL_BANK_TRANSFER_AMOUNT_TO_BE_PAID", totalBankTransferAmountToBePaid);
					}
				}
		}
	}
	public String getTodayDateString() {
		return todayDateString;
	}
	public void setTodayDateString(String todayDateString) {
		this.todayDateString = todayDateString;
	}
	

	
    
}
